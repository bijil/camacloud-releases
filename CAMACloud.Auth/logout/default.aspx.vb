﻿Public Class _default1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FormsAuthentication.SignOut()

        Dim fac As New HttpCookie(FormsAuthentication.FormsCookieName, "")
        fac.Domain = FormsAuthentication.CookieDomain
        fac.Expires = DateTime.Now.AddYears(-1)
        Response.Cookies.Add(fac)

        Dim ans As New HttpCookie("ASP.NET_SessionId", "")
        ans.Expires = DateTime.Now.AddYears(-1)
        Response.Cookies.Add(ans)

        Session.RemoveAll()
        Response.Redirect("~/")
    End Sub

End Class