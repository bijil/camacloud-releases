﻿Imports System.ServiceModel
Imports System.ServiceModel.Activation
Imports System.ServiceModel.Web
Imports CAMACloud.Security

<ServiceContract(Namespace:="")>
<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)>
Public Class ClientAccess

    <OperationContract(), WebGet>
    Public Function validate() As StandardAuthResponse
        Dim application As HttpApplication = HttpContext.Current.ApplicationInstance
        Dim resp As New StandardAuthResponse
        Dim _noLicense = True
        Try
            DeviceLicense.ValidateRequest(application)
            resp.success = True
            _noLicense = False
        Catch ex As Exceptions.LicenseBreachException
            resp.statusMessage = "license-breach"
            resp.statusCode = "401"
            resp.redirectUrl = ApplicationSettings.CALAuthorityPageUrl
        Catch ex2 As Exceptions.UnlicensedAccessException
            resp.statusMessage = "unlicensed-device"
            resp.statusCode = "401"
            resp.redirectUrl = ApplicationSettings.CALAuthorityPageUrl
        Catch ex3 As Exceptions.LicenseMissingInfoException
            resp.statusMessage = "missing-info"     'Obsolete case
            resp.statusCode = "412"
            resp.redirectUrl = ApplicationSettings.CALUserInfoPageUrl
        Catch ex4 As Exceptions.UnlicensedApplicationAccessException
            resp.statusMessage = "invalid-url"
            resp.statusCode = "403"
        Catch ex5 As Exceptions.InvalidUrlException
            resp.statusMessage = "access-restricted"
            resp.statusCode = "403"
        Catch ex As Exception
            resp.statusMessage = "error"
            resp.statusCode = "500"
            resp.errorMessage = ex.Message
            resp.error = True
        End Try
        Return resp
    End Function

End Class

<DataContractFormat>
Public Class StandardAuthResponse
    Public success As Boolean = False
    Public [error] As Boolean = False
    Public statusCode As String
    Public statusMessage As String
    Public data As String
    Public redirectUrl As String
    Public errorMessage As String = Nothing
End Class
