﻿Public Class ExceptionDisplay
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            lblException.Text = "Account has been closed temporarily for maintenance. All data services will reinstantiate after " & Request.QueryString(0).ToString() & " minutes"
        End If
    End Sub

End Class