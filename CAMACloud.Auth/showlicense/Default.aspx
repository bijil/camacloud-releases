﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/SecurityPage.master" AutoEventWireup="false" Inherits="CAMACloud.Auth.showlicense_Default" Codebehind="Default.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div style="color: White; font-size: 18pt; padding: 50px 0px;text-align:center;">
        <div>
            Your license key is:
        </div>
        <div style="padding:25px 0px;font-size:23pt;font-weight:bold;">
            <asp:Label runat="server" ID="license" Text="Unlicensed Device" />
        </div>
        <div style="font-size:10pt;font-weight:bold;text-align:center;color:white;">      
                <asp:Label runat="server" ID="lblLicenseType" Text="" />
          </div>
        <table style="width:100%;padding-top:15px; border-spacing: 5px 15px; white-space: nowrap;">
            <tr>
                <td style="text-align:left;">Device: </td>
                <td style="text-align:right;font-weight:bold;font-size: 15pt;"><asp:Label runat="server" ID="lblNick" Text="Unregistered" /></td>
            </tr>
            <tr>
                <td style="text-align:left;">Email ID: </td>
                <td style="text-align:right;font-weight:bold;font-size: 15pt;"><asp:Label runat="server" ID="lblmailid" Text="" /></td>
            </tr>
        </table>
    </div>
</asp:Content>
