﻿<%@ Page Title="CAMA Cloud&#174; - Requires Client Access License" Language="VB" MasterPageFile="~/App_MasterPages/SecurityPage.master" AutoEventWireup="false" Inherits="CAMACloud.Auth.cal"
    CodeBehind="cal.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <meta name="viewport" content="user-scalable=no,initial-scale=1.0,maximum-scale=1.0" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="static/app/apple-touch-icon-precomposed-72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="static/app/apple-touch-icon-precomposed-144.png" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="CAMA Cloud&#174;">
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <style type="text/css">
        .reqd
        {
            padding-left: 5px;
            color: White;
            font-size: 24pt;
            vertical-align: text-bottom;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <p>
        You are trying to access <b style="white-space: nowrap;">CAMA Cloud<sup>&#174;</sup></b>, but this device is not yet licensed to use the software. If you have a valid license key, and
        a One-Time-Password, please enter them below to gain access to the software.</p>
    <p class="error" runat="server" visible="false" id="pError">
        You have entered an invalid OTP.</p>
    <div class="login-inputs">
        <div class="input-box">
            <asp:TextBox ID="txtLicenseKey" runat="server" CssClass="text" watermark="License Key" />
            <asp:RequiredFieldValidator runat="server" ID="rfv1" ControlToValidate="txtLicenseKey" ValidationGroup="license" Display="Dynamic" CssClass="reqd" ErrorMessage="*" />
        </div>
        <div class="input-box">
            <asp:TextBox ID="txtOTP" TextMode="Password" runat="server" CssClass="text" watermark="One Time Password" />
            <asp:RequiredFieldValidator runat="server" ID="rfv2" ControlToValidate="txtOTP" ValidationGroup="license" Display="Dynamic" CssClass="reqd" ErrorMessage="*" />
        </div>
        <div class="input-box">
            <asp:TextBox ID="txtEmail" runat="server" CssClass="text" watermark="Your valid email address" ValidationGroup="license" />
            <asp:RequiredFieldValidator runat="server" ID="rfv3" ControlToValidate="txtEmail" ValidationGroup="license" Display="Dynamic" CssClass="reqd" ErrorMessage="*" />
        </div>
        <div class="input-box">
            <asp:TextBox ID="txtNick" runat="server" CssClass="text" watermark="Unique device nickname" ValidationGroup="license" MaxLength="30" />
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtNick" ValidationGroup="license" Display="Dynamic" CssClass="reqd" ErrorMessage="*" />
        </div>
    </div>
    <div style="text-align: center;">
        <asp:LinkButton runat="server" CssClass="login-button" ID="lbActivate" ValidationGroup="license"><span>Activate Client</span></asp:LinkButton>
    </div>
    <p style="text-align: right; font-weight: bold;">
        <a href="info/forgot.aspx" style="color: Yellow;font-size: 16px;">Forgot License Key or OTP?</a>
    </p>
    <p>
        Note: If you are still getting this form on an already licensed machine, it means that your license data has been erased from this machine or your existing license has been revoked.
        Please contact <b style="white-space: nowrap;">Data Cloud Solutions, LLC.</b>, or an authorized <b>CAMA Cloud<sup>&#174;</sup></b> software vendor, for your license key and one-time-password
        to activate your device.</p>
    <p>
        Your email address and device nickname are important for license recovery in case it gets erased from your browser.</p>

</asp:Content>
