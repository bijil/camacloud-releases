﻿Imports CAMACloud
Imports CAMACloud.Security
Imports CAMACloud.Data
Partial Class _Default
    Inherits System.Web.UI.Page

    'Protected Sub Login_Authenticate(sender As Object, e As System.Web.UI.WebControls.AuthenticateEventArgs) Handles Login.Authenticate
    '	Throw New Exception(Membership.ApplicationName)
    'End Sub
    Dim UserLastLogin
    Dim agreementContent
    Dim agreement As String = ""
    Property agr() As String
       Get
           Return  agreement    
       End Get
       Set(value As String)
             agreement  = value
       End Set
    End Property
    
    Protected Sub Login_LoggingIn(sender As Object, e As System.EventArgs) Handles Login.LoggingIn
    	If Membership.GetUser(Login.UserName) IsNot Nothing Then
            UserLastLogin = Membership.GetUser(Login.UserName).LastLoginDate.ToString("yyyy-MM-dd HH:mm:ss")
        End If
    End Sub
    Protected Sub Login_LoggedIn(sender As Object, e As System.EventArgs) Handles Login.LoggedIn
    	'Membership.ValidateUser(Login.UserName, Login.Password)
    	Dim userId = Membership.GetUser(Login.UserName).ProviderUserKey.ToString()
    	Dim username =  Database.System.GetStringValue("SELECT au.UserName FROM  aspnet_Users au INNER JOIN aspnet_Applications a On a.ApplicationId=au.ApplicationId WHERE  a.ApplicationName='" & Membership.ApplicationName & "' And au.UserId='" &  userId & "'")
        Dim orgId As String = HttpContext.Current.GetCAMASession.OrganizationId
        Dim sql As String = " IF EXISTS(SELECT * FROM LoginDetails WHERE UserId='" + userId + "' and ApplicationId = 2 ) UPDATE LoginDetails SET OrgId = '" + orgId + "',LastLoginTime = LoginTime,LoginTime = GETUTCDATE() WHERE UserId ='" + userId + "' and ApplicationId = 2 ELSE INSERT INTO LoginDetails(OrgId, ApplicationId,UserId,LoginTime,LastLoginTime) VALUES('" + orgId + "',2,'" + userId + "',GETUTCDATE(),'"+ UserLastLogin +"');"
		Database.System.Execute(sql)
        Dim returnUrl As String = "https://console.camacloud.com/"

        Dim hostName As String = Request.Url.Host
        If hostName.Trim.EndsWith(".beta2.camacloud.com") Then
            returnUrl = "http://console.beta2.camacloud.com/"
        ElseIf hostName.Trim.EndsWith(".beta1.camacloud.com") Then
            returnUrl = "http://console.beta1.camacloud.com/"
        ElseIf hostName.Trim.EndsWith(".pacsdemo.camacloud.com") Then
            returnUrl = "http://console.pacsdemo.camacloud.com/"
        ElseIf hostName.Trim.EndsWith(".pacsbeta.camacloud.com") Then
            returnUrl = "http://console.pacsbeta.camacloud.com/"
        ElseIf hostName.Trim.EndsWith(".pacs.camacloud.com") Then
            returnUrl = "https://console.pacs.camacloud.com/"
        ElseIf hostName.Trim.EndsWith(".vgsi.camacloud.com") Then
        	returnUrl = "https://console.vgsi.camacloud.com/"
        ElseIf hostName.Trim.EndsWith(".pvt.camacloud.com") Then
            returnUrl = "https://console.pvt.camacloud.com/"
        ElseIf hostName.Trim.EndsWith(".alpha.camacloud.com") Then
            returnUrl = "https://console.alpha.camacloud.com/"
        ElseIf hostName.Trim.EndsWith(".ca.camacloud.com") Then
            returnUrl = "https://console.ca.camacloud.com/"
        ElseIf hostName.Trim.EndsWith(".ca-beta.camacloud.com") Then
            returnUrl = "https://console.ca-beta.camacloud.com/"
        ElseIf hostName = "localhost" Then
            returnUrl = "http://localhost:301/"
        End If

        Dim maintenanceURL As String = ApplicationSettings.MaintenanceURL
        If (maintenanceURL <> "False") Then
            FormsAuthentication.SignOut()
            returnUrl = maintenanceURL
        Else
            'CAMASession.RegisterUserOnDeviceLicense(Login.UserName)
            CAMASession.RegisterUserOnDeviceLicense(username)

            'UserAuditTrail.CreateEvent(Login.UserName, UserAuditTrail.ApplicationType.Auth, UserAuditTrail.EventType.Login, "User logged in.")
            UserAuditTrail.CreateEvent(username, UserAuditTrail.ApplicationType.Auth, UserAuditTrail.EventType.Login, "User logged in.")

            FormsAuthentication.SetAuthCookie(Login.UserName, True)

            If Request("ReturnUrl") <> "" Then
                returnUrl += Request("ReturnUrl").TrimStart("/")
            End If
        End If
        Response.Redirect(returnUrl)
    End Sub

    Protected Sub Login_LoginError(sender As Object, e As System.EventArgs) Handles Login.LoginError
        Dim loginErrorReason As String = "User Login attempt failed."

        Dim testUser As MembershipUser = Membership.GetUser(Login.UserName)
        If testUser Is Nothing Then
            loginErrorReason = "Requested user '" + Login.UserName + "' does not exist."
        ElseIf Not testUser.IsApproved Then
            loginErrorReason = "Your account has not been approved by the administrators."
        ElseIf testUser.IsLockedOut Then
            loginErrorReason = "Your account has been locked out."
        Else
            loginErrorReason = "Log in attempt failed due to incorrect credentials. Please re-attempt with correct password or contact administrator to reset your password."
        End If
        Dim orgId As String = HttpContext.Current.GetCAMASession.OrganizationId
        If orgId = -1 Then
            loginErrorReason = "Log in attempt failed.Please select an Environment."
        End If

        divError.InnerText = loginErrorReason
        divError.Visible = True
        'ErrorMailer.ReportException(Login.UserName, New Exception(loginErrorReason), Request)
    End Sub

Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        If Request.Cookies("MACID") IsNot Nothing Then
            Dim dr As DataRow = CAMACloud.Data.Database.System.GetTopRow("SELECT * FROM DeviceLicense WHERE MachineKey = '" + Request.Cookies("MACID").Value + "'")
            If dr IsNot Nothing Then
                If dr.GetString("OrganizationId") <> "" Then
                    agr = CAMACloud.Data.Database.System.GetStringValue("SELECT v.AgreementContent FROM Organization o INNER JOIN Vendor v ON o.VendorId = v.Id WHERE o.Id = " + dr.GetString("OrganizationId"))
                End If
                Dim isSuper As Integer = dr.GetBoolean("IsSuper")
                Dim isVendor As Integer = dr.GetBoolean("IsVendor")

                If isSuper Or isVendor Then
                    Dim changeTxt As String = "<span style='color: yellow;'>Change</span>"
                    LkChgOrg.Text = HttpContext.Current.GetCAMASession.OrganizationName + "  (" + changeTxt + ")"
                Else
                    LkChgOrg.Text = HttpContext.Current.GetCAMASession.OrganizationName
                End If
            End If
        End If
        Response.AddHeader("Access-Control-Allow-Origin", "*")
        Try
            DeviceLicense.ClearLicenseCache(HttpContext.Current)
            DeviceLicense.ValidateRequest(HttpContext.Current.ApplicationInstance)
        Catch exmi As Exceptions.LicenseMissingInfoException
            Response.Redirect(ApplicationSettings.GetCALUserInfoPageUrl(HttpContext.Current))
            Exit Sub
        Catch ex As Exception
            Response.Redirect("http://www.camacloud.com/")
            Exit Sub
        End Try
        If Not IsPostBack Then
            FormsAuthentication.SignOut()
            CloudUserProvider.VerifyOrganizationUsers()
        End If
    End Sub

    Public Function GetOrganizationName() As String
        If Request.Cookies("MACID") IsNot Nothing Then
            Return CAMACloud.Data.Database.System.GetStringValue("SELECT COALESCE(o.Name, '** Master License **') As OrgName FROM DeviceLicense dl LEFT OUTER JOIN Organization o ON dl.OrganizationId = o.Id WHERE dl.MachineKey = '" + Request.Cookies("MACID").Value + "'")
        Else
            Response.Redirect("http://www.camacloud.com/")
            Return "No License Installed."
        End If
    End Function
    Protected Sub LkChgOrg_Click(sender As Object, e As EventArgs) Handles LkChgOrg.Click
        Dim returnUrl As String = "https://auth.camacloud.com/change/?callUrl=livecamaCloud"
        Dim hostName As String = HttpContext.Current.Request.Url.Host
        If hostName.Trim.EndsWith(".beta2.camacloud.com") Then
            returnUrl = "https://auth.beta2.camacloud.com/change/?callUrl=beta2camaCloud"
        ElseIf hostName.Trim.EndsWith(".beta1.camacloud.com") Then
            returnUrl = "https://auth.beta1.camacloud.com/change/?callUrl=beta1camaCloud"
        ElseIf hostName.Trim.EndsWith(".pacsdemo.camacloud.com") Then
            returnUrl = "https://auth.pacsdemo.camacloud.com/change/?callUrl=pacsDemocamaCloud"
        ElseIf hostName.Trim.EndsWith(".pacsbeta.camacloud.com") Then
            returnUrl = "https://auth.pacsbeta.camacloud.com/change/?callUrl=pacsbetacamaCloud"
        ElseIf hostName.Trim.EndsWith(".pacs.camacloud.com") Then
            returnUrl = "https://auth.pacs.camacloud.com/change/?callUrl=pacscamaCloud"
        ElseIf hostName.Trim.EndsWith(".vgsi.camacloud.com") Then
            returnUrl = "https://auth.vgsi.camacloud.com/change/?callUrl=vgsicamaCloud"
        ElseIf hostName.Trim.EndsWith(".pvt.camacloud.com") Then
            returnUrl = "https://auth.pvt.camacloud.com/change/?callUrl=pvtcamaCloud"    
        ElseIf hostName.Trim.EndsWith(".alpha.camacloud.com") Then
            returnUrl = "https://auth.alpha.camacloud.com/change/?callUrl=alphacamaCloud"
        ElseIf hostName.Trim.EndsWith(".ca.camacloud.com") Then
            returnUrl = "https://auth.ca.camacloud.com/change/?callUrl=cacamaCloud"
        ElseIf hostName.Trim.EndsWith(".ca-beta.camacloud.com") Then
            returnUrl = "https://auth.ca-beta.camacloud.com/change/?callUrl=cabetacamaCloud"
        ElseIf hostName = "localhost" Then
            returnUrl = "http://localhost:309/change/?callUrl=localHost"
        End If
        Response.Redirect(returnUrl)
    End Sub

End Class
