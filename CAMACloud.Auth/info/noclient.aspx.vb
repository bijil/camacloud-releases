﻿Public Class noclient
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.QueryString("license") IsNot Nothing Then
        	MultiOrganizationLicense.Style("display") = "none"
        	WrongMAUrl.Style("display") = "none"
            UnLicensed.Style.Remove("display")
        End If
        If Request.QueryString("wrongmaurl") IsNot Nothing Then
            MultiOrganizationLicense.Style("display") = "none"
            UnLicensed.Style("display") = "none"
            WrongMAUrl.Style.Remove("display")
        End If
    End Sub

End Class