﻿<%@ Page Title="Register User Data" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/SecurityPage.master" CodeBehind="nick.aspx.vb" Inherits="CAMACloud.Auth.nick" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
	<meta name="viewport" content="user-scalable=no,initial-scale=1.0,maximum-scale=1.0" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="static/app/apple-touch-icon-precomposed-72.png" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="static/app/apple-touch-icon-precomposed-144.png" />
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="CAMA Cloud&#8480;">
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<style type="text/css">
		.reqd
		{
			padding-left:5px;
			color:White;
			font-size:24pt;
			vertical-align:text-bottom;
		}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h3 style="color: White;">
        Register User Data</h3>
    <p>
        You are required to provide an email address and unique nickname for this device. The device nickname along with the email address are important for license recovery in case it gets
        erased from your browser.</p>
    <p class="error" runat="server" visible="false" id="pError">
        You have entered an invalid OTP.</p>
    <div class="login-inputs">
        <div class="input-box">
            <asp:TextBox ID="txtEmail" runat="server" CssClass="text" watermark="Your valid email address" ValidationGroup="license" />
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtEmail" ValidationGroup="license" Display="Dynamic" CssClass="reqd" ErrorMessage="*" />
        </div>
        <div class="input-box">
            <asp:TextBox ID="txtNick" runat="server" CssClass="text" watermark="Unique device nickname" ValidationGroup="license" MaxLength="30" />
            <asp:RequiredFieldValidator runat="server" ID="rfv3" ControlToValidate="txtNick" ValidationGroup="license" Display="Dynamic" CssClass="reqd" ErrorMessage="*" />
        </div>
    </div>
    <div style="text-align: center;">
        <asp:LinkButton runat="server" CssClass="login-button" ID="lbRegister" ValidationGroup="license"><span>Register & Continue</span></asp:LinkButton>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" runat="server">
</asp:Content>
