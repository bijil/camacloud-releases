﻿Imports System.Net.Mail
Imports CAMACloud.Security
Imports CAMACloud.Data

Public Class nick
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.AddHeader("Access-Control-Allow-Origin", "*")

        If Not IsPostBack Then
            Try
                DeviceLicense.ClearLicenseCache(HttpContext.Current)
                Dim dl As DeviceLicense = DeviceLicense.GetLicense
                txtEmail.Text = dl.RegisteredEmail
                txtNick.Text = dl.RegisteredNickName
            Catch ex As Exceptions.LicenseMissingInfoException
                If Context.HasKeys("MACID") Then
                    Dim dr As DataRow = Database.System.GetTopRow("SELECT * FROM DeviceLicense WHERE MachineKey = " + Context.GetKeyValue("MACID").ToSqlValue)
                    txtEmail.Text = dr.GetString("RegisteredEmail")
                    txtNick.Text = dr.GetString("RegisteredNick")
                End If
            End Try
        End If

    End Sub


    Sub RedirectToApplication()
        Dim schema As String = "http://"
        If Request("s") = "1" Then
            schema = "https://"
        End If
        Dim appHost As String = Request("return")
        If appHost = "" Then
            appHost = "console.camacloud.com"
        End If

        Response.Redirect(schema + appHost + "/")
    End Sub


    Private Sub lbRegister_Click(sender As Object, e As System.EventArgs) Handles lbRegister.Click

        Try
            Dim toEmail As New MailAddress(txtEmail.Text)
        Catch ex As Exception
            pError.InnerHtml = "Invalid email address!"
            pError.Visible = True
            Return
        End Try

        If txtNick.Text.Length < 5 Then
            pError.InnerHtml = "The device nickname should have more than 5 characters."
            pError.Visible = True
            Return
        End If

        Try
            Dim dl As DeviceLicense = DeviceLicense.GetLicense
            dl.RegisterUserInformation(txtEmail.Text, "", txtNick.Text)
            RedirectToApplication()
        Catch ex As Exception
            pError.InnerHtml = ex.Message
            pError.Visible = True
            Return
        End Try
        
    End Sub
End Class