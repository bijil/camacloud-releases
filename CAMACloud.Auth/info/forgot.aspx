﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/SecurityPage.master" CodeBehind="forgot.aspx.vb" Inherits="CAMACloud.Auth.forgot" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <meta name="viewport" content="user-scalable=no,initial-scale=1.0,maximum-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="no">
    <style type="text/css">
        .reqd
        {
            padding-left: 5px;
            color: White;
            font-size: 24pt;
            vertical-align: text-bottom;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <h3 style="color: White;">
        Recover License Credentials</h3>
    <p>
        To recover your license key and OTP, enter your registered email address and unique device nickname below. If it is found valid, the requested information will be sent to that email address.</p>
    <p class="error" runat="server" visible="false" id="pError">
        Invalid email address.</p>
    <div class="login-inputs">
        <div class="input-box">
            <asp:TextBox ID="txtEmail" runat="server" CssClass="text" watermark="Your registered email address" ValidationGroup="license" />
            <asp:RequiredFieldValidator runat="server" ID="rfv3" ControlToValidate="txtEmail" ValidationGroup="license" Display="Dynamic" CssClass="reqd" ErrorMessage="*" />
        </div>
        <div class="input-box">
            <asp:TextBox ID="txtNick" runat="server" CssClass="text" watermark="Unique device nickname" ValidationGroup="license" />
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtNick" ValidationGroup="license" Display="Dynamic" CssClass="reqd" ErrorMessage="*" />
        </div>
    </div>
    <div style="text-align: center;">
        <asp:LinkButton runat="server" CssClass="login-button" ID="lbRecover" ValidationGroup="license"><span>Recover Credentials</span></asp:LinkButton>
    </div>
</asp:Content>
