﻿Imports System.Data.SqlClient
Imports Amazon
Imports Amazon.S3
Imports Amazon.S3.Model

Public Class PhotoCleaner

    Public Shared Sub ProcessDeleteQueue()
        Try
            Dim conn As New SqlConnection(My.Settings.ConnectionString)
            conn.Open()
            Dim da As New SqlDataAdapter("SELECT TOP 1000 * from PhotoDeleteQueue ORDER BY ROWID", conn)
            Dim dt As New DataTable
            da.Fill(dt)

            If dt.Rows.Count = 0 Then
                conn.Close()
                Return
            End If

            Dim rowIds As New List(Of Integer)
            Dim deletePaths As New List(Of String)
            For Each dr As DataRow In dt.Rows
                rowIds.Add(dr("ROWID"))
                deletePaths.Add(dr("Path"))
            Next

            Dim s3Client As New AmazonS3Client("AKIAJTXLG7VBED5VS7PA", "AJVUPdT37Si1+opsEcpgRijQXeRZ+dtNHORri4I4", RegionEndpoint.USEast1)
            Dim deleteRequest As New DeleteObjectsRequest
            deleteRequest.BucketName = "storage.camacloud.com"
            For Each dp In deletePaths
                deleteRequest.AddKey(dp)
            Next

            Dim resp = s3Client.DeleteObjects(deleteRequest)
            If resp.HttpStatusCode = Net.HttpStatusCode.OK Then
                Dim deleteFromQueue = conn.CreateCommand()
                deleteFromQueue.CommandText = "DELETE FROM PhotoDeleteQueue WHERE ROWID IN (" + String.Join(",", rowIds.ToArray) + ")"
                deleteFromQueue.ExecuteNonQuery()
                conn.Close()
            Else
                conn.Close()
                Return
            End If

            Threading.Thread.Sleep(1000)
            ProcessDeleteQueue()
        Catch ex As Exception
            EventLog.WriteEntry("CC.PhotoCleaner", ex.Message, EventLogEntryType.Error)
        End Try

    End Sub

End Class
