﻿Option Strict Off
Imports System.ServiceModel.Activation
Imports CAMACloud.IntegrationAPI.ErrorHandling
Imports CAMACloud.IntegrationAPI.ServiceContracts
Imports System.ServiceModel.Web
Imports CAMACloud.Exceptions
Imports CAMACloud.Data
Imports CAMACloud.Authentication
Imports CAMACloud.BusinessLogic.RemoteIntegration
Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts
Imports CAMACloud.BusinessLogic
Imports CAMACloud.BusinessLogic.Sketching
Imports System.IO

Namespace CAMACloud.IntegrationAPI.ServiceControllers
    <AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)> _
    <StandardErrorModel()> _
    Public Class DataIntegrationService
        Implements IDataIntegrationService


#Region "IDataIntegrationService Members"


#Region "/transfer/{task}/{action}"
        <WebInvoke(UriTemplate:="/transfer/{task}/{action}", Method:="*", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Bare)> _
        Public Function ProcessTransferRequest(task As String, action As String, data As DataTransferRequest) As StandardResponse Implements IDataIntegrationService.ProcessTransferRequest
            Return ProcessTaskAction("transfer", task, action, data)
        End Function
#End Region

#Region "/setup/{task} - Schema Converted"
        <WebInvoke(UriTemplate:="/setup/{task}", Method:="*", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Bare)> _
        Public Function ProcessFinalizeRequest(task As String, data As SchemaRequest) As StandardResponse Implements IDataIntegrationService.ProcessFinalizeRequest
            Return ProcessTaskAction("setup", task, Nothing, data)
        End Function
#End Region

#Region "/setup/{task}/{action} - Schema Converted"
        <WebInvoke(UriTemplate:="/setup/{task}/{action}", Method:="*", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Bare)> _
        Public Function ProcessSetupRequest(task As String, action As String, data As SchemaRequest) As StandardResponse Implements IDataIntegrationService.ProcessSetupRequest
            Return ProcessTaskAction("setup", task, action, data)
        End Function
#End Region

#Region "/rebuild/{task} - Neighborhood settings"
        <WebInvoke(UriTemplate:="/rebuild/{task}", Method:="*", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Bare)> _
        Public Function ProcessNeighborhoodRequest(task As String, data As NeighbourhoodRequest) As StandardResponse Implements IDataIntegrationService.ProcessNeighborhoodRequest
            Return ProcessTaskAction("rebuild", task, Nothing, data)
        End Function
#End Region

#Region "/data/template/{action}"
        <WebInvoke(UriTemplate:="/data/template/{action}", Method:="*", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Bare)> _
        Public Function ProcessTemplateRequest(action As String, data As TemplateRequest) As StandardResponse Implements IDataIntegrationService.ProcessTemplateRequest
            Return ProcessTaskAction("data", "template", action, data)
        End Function
#End Region

#Region "/transfer/sketch/{action}"
        <WebInvoke(UriTemplate:="/transfer/sketch/{action}", Method:="*", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Bare)> _
        Public Function ProcessSketchDataRequest(action As String, data As SketchTransferRequest) As StandardResponse Implements IDataIntegrationService.ProcessSketchDataRequest
            Return ProcessTaskAction("transfer", "sketch", action, data)
        End Function
#End Region

#Region "/data/{task}/{action}"
        <WebInvoke(UriTemplate:="/data/{task}/{action}", Method:="*", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Bare)> _
        Public Function ProcessParcelRequest(task As String, action As String, data As ParcelRequest) As StandardResponse Implements IDataIntegrationService.ProcessParcelRequest
            Return ProcessTaskAction("data", task, action, data)
        End Function

#End Region

#Region "/content/{task}/{action}"
        <WebInvoke(UriTemplate:="/content/{task}/{action}", Method:="*", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Bare)> _
        Public Function ProcessContentRequest(task As String, action As String, data As PhotoSyncRequest) As StandardResponse Implements IDataIntegrationService.ProcessContentRequest
            Return ProcessTaskAction("content", task, action, data)
        End Function
#End Region

#Region "/system/{task}/{action}"
        <WebInvoke(UriTemplate:="/system/{task}/{action}", Method:="*", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Bare)> _
        Public Function ProcessConfigurationRequest(task As String, action As String, data As ApplicationConfigRequest) As StandardResponse Implements IDataIntegrationService.ProcessConfigurationRequest
            Return ProcessTaskAction("system", task, action, data)
        End Function
#End Region

#Region "/system/{task}"
        <WebInvoke(UriTemplate:="/system/{task}", Method:="*", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Bare)> _
        Public Function ProcessSystemRequest(task As String, data As SystemRequest) As StandardResponse Implements IDataIntegrationService.ProcessSystemRequest
            Return ProcessTaskAction("system", task, Nothing, data)
        End Function
#End Region

#Region "/access/{task}/{action}"
        <WebInvoke(UriTemplate:="/access/{task}/{action}", Method:="*", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Bare)>
        Public Function ProcessAccessRequest(task As String, action As String, data As AccessRequest) As StandardResponse Implements IDataIntegrationService.ProcessAccessRequest
            Return ProcessTaskAction("access", task, action, data)
        End Function
#End Region

#Region "/mailreciver/{task}"
        <WebInvoke(UriTemplate:="/mailreciver/{task}", Method:="*", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Bare)>
        Public Function ProcessMailerRequest(task As String, data As MailReceiverRequest) As StandardResponse Implements IDataIntegrationService.ProcessMailerRequest
            Dim targetDatabase As Database = Database.System
            Return executeMailerRequest(targetDatabase, task, data)
        End Function
#End Region

#Region "/temp/{task}/{action}"
        <WebInvoke(UriTemplate:="/temp/{task}/{action}", Method:="*", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Bare)> _
        Public Function ProcessTempRequest(task As String, action As String, data As StandardRequest) As StandardResponse Implements IDataIntegrationService.ProcessTempRequest
            Return ProcessTaskAction("temp", task, action, data)
        End Function
#End Region

        'General Templates
#Region "/{service}/{task}/{action} -> Main Entry Here"
        <WebInvoke(UriTemplate:="/{service}/{task}/{action}", Method:="*", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Bare)> _
        Public Function ProcessTaskAction(service As String, task As String, action As String, data As StandardRequest) As StandardResponse Implements IDataIntegrationService.ProcessTaskAction
            Dim method = WebOperationContext.Current.IncomingRequest.Method
            Dim requestPath As String = service
            If task IsNot Nothing Then
                requestPath += "/" & task
            End If
            If action IsNot Nothing Then
                requestPath += "/" & action
            End If

            If method = "GET" Then
                Return handleGetRequestRedirection(requestPath)
            End If

            Return parseAndExecuteWebRequest(requestPath, service, task, action, data)
        End Function
#End Region

#Region "/{service}/{task}"
        <WebInvoke(UriTemplate:="/{service}/{task}", Method:="*", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Bare)> _
        Public Function ProcessTask(service As String, task As String, data As StandardRequest) As StandardResponse Implements IDataIntegrationService.ProcessTask
            Return ProcessTaskAction(service, task, Nothing, data)
        End Function
#End Region

#Region "/{action}"
        <WebInvoke(UriTemplate:="/{action}", Method:="*", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Bare)> _
        Public Function ProcessAction(action As String, data As StandardRequest) As StandardResponse Implements IDataIntegrationService.ProcessAction
            Return ProcessTaskAction(action, Nothing, Nothing, data)
        End Function
#End Region

#Region "(root)"
        <WebInvoke(UriTemplate:="/", Method:="*", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Bare)> _
        Public Function ProcessHomepage(data As StandardRequest) As StandardResponse Implements IDataIntegrationService.ProcessHomepage
            Dim method = WebOperationContext.Current.IncomingRequest.Method
            If method = "GET" Then
                Return handleGetRequestRedirection("", True)
            End If
            Return New StandardResponse("200")
        End Function
#End Region

#End Region

        Private Function handleGetRequestRedirection(requestPath As String, Optional home As Boolean = False) As StandardResponse
            Dim response As OutgoingWebResponseContext = WebOperationContext.Current.OutgoingResponse
            response.StatusCode = System.Net.HttpStatusCode.Moved
            If Not home Then
                response.Location = "/gethandler.aspx?content=" & requestPath
            Else
                response.Location = "/index.htm"
            End If
            response.SuppressEntityBody = True
            Return New StandardResponse()
        End Function

        Public Function parseAndExecuteWebRequest(requestPath As String, service As String, task As String, action As String, data As Object) As StandardResponse
            Dim targetDatabase As Database = Database.System
            Dim pathId As Integer = -1, traceId As Integer = -1

            Dim contentLength As Long = WebOperationContext.Current.IncomingRequest.ContentLength

            If service = "check" Then
                Dim vi As New VersionInfo()
                vi.version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString()
                vi.application = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                Dim lastModifiedDate As String = System.IO.File.GetLastWriteTime(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase.Replace("file:///", "").Replace("/", "\")).ToString()
                Dim chk As New StandardResponse("200")
                chk.description = "Check API call executed successfully."
                chk.messages.Add("Welcome to CAMA Cloud Integration API")
                chk.messages.Add("Last updated on " & lastModifiedDate & " UTC")
                'chk.messages.Add(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                chk.header = vi
                Return chk
            End If

            If data Is Nothing Then
                Return New StandardResponse("403")
            End If
            If data IsNot Nothing Then
                If data.[GetType]().Name = GetType(StandardRequest).Name OrElse data.[GetType]().IsSubclassOf(GetType(StandardRequest)) Then
                    Dim accessKey As String = DirectCast(data, StandardRequest).accessKey
                    Dim passKey As String = DirectCast(data, StandardRequest).passKey
                    Dim MACAddress As String = DirectCast(data, StandardRequest).MACAddress
                    Try
                        targetDatabase = ApplicationAccess.ConnectToClientDatabase(accessKey, passKey, MACAddress)

                    Catch maintenanceException As EnvironmentMaintenanceException
                        Dim resp As New StandardResponse("901")
                        resp.AddMessage(maintenanceException.Message)
                        resp.data = maintenanceException.DurationInMinutes
                        Return resp
                    Catch generatedExceptionName As AuthenticationFailedException
                        Return New StandardResponse("401")
                    End Try
                End If
            End If

            traceId = APIRequestTrace.StartTrace(targetDatabase, requestPath, pathId)

            Dim apiResponse As New StandardResponse()
            apiResponse.statusCode = "404"
            apiResponse.description = "Unknown request format """ & requestPath & """"

            Try
                Select Case service
                    Case "test"
                        Dim testResp As New StandardResponse("200")
                        Dim org As DataRow = Database.System.GetTopRow("SELECT * FROM Organization WHERE Id IN (SELECT OrganizationId FROM ApplicationAccess WHERE AccessKey = '" + DirectCast(data, StandardRequest).accessKey + "')")
                        Dim parcelCount As Integer = targetDatabase.GetIntegerValue("SELECT COUNT(*) FROM Parcel")
                        Dim asmtGroups As Integer = targetDatabase.GetIntegerValue("SELECT COUNT(*) FROM Neighborhood")
                        Dim unassigned As Integer = targetDatabase.GetIntegerValue("SELECT COUNT(*) FROM Parcel WHERE NeighborhoodId IS NULL")
                        Dim totalPhotos As Integer = targetDatabase.GetIntegerValue("SELECT COUNT(*) FROM ParcelImages")
                        testResp.header = New EnvironmentInfo With {
                                                            .environment = org.GetString("Name"),
                                                            .dbName = org.GetString("DBName"),
                                                            .parcels = parcelCount,
                                                            .assignmentGroups = asmtGroups,
                                                            .unassignedParcels = unassigned,
                                                            .totalPhotos = totalPhotos
                                                    }
                        apiResponse = testResp
                    Case "error"
                        targetDatabase.Execute("EXEC sp_NonExistentStoredProc_ByAPIError")
                    Case "system"
                        apiResponse = executeSystemRequest(targetDatabase, task, action, data)
                    Case "setup"
                        apiResponse = executeSetupRequest(targetDatabase, task, action, data)
                    Case "rebuild"
                        'Return executeNeighborhoodRequest(targetDatabase, task, action, data)
                        apiResponse = executeRebuildRequest(targetDatabase, task, action, data)
                    Case "transfer"
                        apiResponse = executeTransferRequest(targetDatabase, task, action, data)
                    Case "data"
                        'Return executeParcelRequest(targetDatabase, task, action, data)
                        apiResponse = executeDataRequest(targetDatabase, task, action, data)
                    Case "content"
                        apiResponse = executeContentRequest(targetDatabase, task, action, data)
                    Case "access"
                        apiResponse = executeAccessRequest(targetDatabase, task, action, data)
                    Case "temp"
                        apiResponse = executeTempRequest(targetDatabase, task, action, data)
                End Select

                APIRequestTrace.SetSuccess(targetDatabase, pathId, traceId)

                Return apiResponse

            Catch ex As Exception

                APIRequestTrace.SetError(targetDatabase, pathId, traceId, ex)

                If ex.[GetType]().IsSubclassOf(GetType(ServiceExceptionBase)) Then
                    Dim se As ServiceExceptionBase = DirectCast(ex, ServiceExceptionBase)
                    se.ServiceData = data
                    Throw se
                Else
                    Throw ex
                End If
            End Try

        End Function

#Region "Specific Request Handlers"

#Region "/system"
        Private Function executeSystemRequest(db As Database, task As String, action As String, data As Object) As StandardResponse
            Select Case task
                Case "statusquery"
                    Return AccountManager.GetSystemQuery(db, data)
                Case "audittrail"
                    Return AccountManager.GetAuditTrail(db, data)
                Case "reset"
                    Return New StandardResponse("999")
                Case "config"
                    Select Case action
                        Case "set"
                            Return ApplicationConfigManagement.SetApplicationConfig(db, data)
                        Case "get"
                            Return ApplicationConfigManagement.GetApplicationConfig(db, data)

                    End Select
                    Exit Select
            End Select
            Dim resp As New StandardResponse("404")
            Return resp
        End Function
#End Region

#Region "/setup"
        Private Function executeSetupRequest(db As Database, task As String, action As String, data As StandardRequest) As StandardResponse
            Select Case task
                Case "schema"
                    If True Then
                        Select Case action
                            Case "reset"
                                Return SchemaTransfer.ResetSchema(db, data)
                            Case "transfer"
                                Return SchemaTransfer.Copy(db, data)
                            Case "relation"
                                Return SchemaTransfer.RelateTables(db, data)
                            Case "get"
                                Return SchemaTransfer.Download(db, data)
                            Case "getrelationships"
                                Return SchemaTransfer.DownloadRelationships(db, data)
                            Case "update"
                                Return SchemaTransfer.UpdateSchema(db, data)
                            Case "updatetable"
                                Return SchemaTransfer.UpdateTableProperties(db, data)

                            Case "updatefield"
                                Return SchemaTransfer.UpdateFieldProperties(db, data)
                            Case "repair"
                                Return SchemaTransfer.RepairSchemaMissingTables(db, data)
                        End Select
                        Exit Select
                    End If
                Case "finalize"
                    Return FinalizeNeighborhood.FinalizeData(db, data)
                Case "test"
                    Select Case action
                        Case "relupdate"
                            Return DataTransfer.TestUpdateSql(db, data)
                    End Select
                Case "settings"
                    Select Case action
                        Case "set"
                            Return DataTransfer.SetAllsettings(db, data)
                        Case "get"
                            Return DataTransfer.GetAllsettings(db, data)
                    End Select
                Case "lookup"
                    Select Case action
                        Case "get"
                            Return DataTransfer.DownloadLookUp(db, data)
                    End Select
            End Select

            Dim resp As New StandardResponse("404")
            Return resp
        End Function
#End Region


#Region "/data"
        Private Function executeDataRequest(db As Database, task As String, action As String, data As Object) As StandardResponse
            Select Case task
                Case "parcels"
                    If True Then
                        Select Case action
                            Case "map"
                                Return ParcelTransfer.GetParcel(db, data)
                        End Select
                        Exit Select
                    End If
                Case "parcel"
                    If True Then
                        Select Case action
                            Case "map"
                                Return ParcelTransfer.GetParcel(db, data)
                            Case "commit"
                                Return ParcelTransfer.ExecuteParcelCommit(db, data)
                            Case "clear", "delete"
                                Return ParcelTransfer.ClearAllParcelData(db, data)
                            Case "trackedgroupcommit"
                                Return ParcelTransfer.ExecuteTrackedValueCommit(db, data)
                            Case "updatekey"
                                Return ParcelTransfer.ExecuteParcelUpdateKey(db, data)
                                'Case "delete"
                                '    Return ParcelTransfer.ClearAllParcelData(db, data)
                        End Select
                        Exit Select
                    End If
                Case "template"
                    Select Case action
                        Case "set"
                            Return TemplateController.SetTemplate(db, data)
                        Case "clear"
                            Return TemplateController.ClearTemplate(db, data)
                        Case "clearall"
                            Return TemplateController.ClearAllTemplates(db, data)
                    End Select
                Case "groups"
                    Select Case action
                        Case "deleteempty"
                            Return GroupProcessor.DeleteEmptyGroups(db, data)
                    End Select
            End Select

            Dim resp As New StandardResponse("404")
            Return resp
        End Function
#End Region

#Region "/rebuild"
        Private Function executeRebuildRequest(db As Database, task As String, action As String, data As Object) As StandardResponse
            Select Case task
                Case "neighborhoods"
                    If True Then
                        Return RebuildNeighbourhood.BuildNeighbourhood(db, data)
                    End If
            End Select

            Dim resp As New StandardResponse("404")
            Return resp
        End Function
#End Region

#Region "/transfer"
        Private Function executeTransferRequest(db As Database, task As String, action As String, data As StandardRequest) As StandardResponse
            Select Case task
                Case "info"
                    Select Case action
                        Case "getsequence"
                            Return DataTransfer.GetSequenceOfExport(db, data)
                        Case "jobs"
                            Return DataTransfer.GetActiveJobs(db, data)
                    End Select
                    Exit Select
                Case "data"
                    Select Case action
                        Case "purge"
                            Return DataTransfer.SystemReset(db, data)
                        Case "runscript"
                            Return DataTransfer.RunScriptOnData(db, data)
                    End Select
                    Exit Select
                Case "job"
                    Select Case action
                        Case "cancelall"
                            Return DataTransfer.CancelAllJobs(db, data)
                    End Select
                    Exit Select
                Case "reset", "refresh", "change", "clean"

                    If True Then
						Dim ttype As BulkDataTransferType = BulkDataTransferType.Reset
						Select Case task
							Case "reset"
								ttype = BulkDataTransferType.Reset
								Exit Select
							Case "refresh"
								ttype = BulkDataTransferType.Refresh
								Exit Select
							Case "change"
								ttype = BulkDataTransferType.Change
								Exit Select
							Case "clean"
								ttype = BulkDataTransferType.Clean
								Exit Select
						End Select
						Select Case action
							Case "init"
								Return DataTransfer.Initialize(ttype, db, data)
							Case "data"
								Return DataTransfer.ProcessDataPage(ttype, db, data)
							Case "commit"
								Return DataTransfer.Commit(ttype, db, data)
							Case "cancel"
                                Return DataTransfer.Cancel(ttype, db, data)
                            Case "table"
                                Return DataTransfer.CleanTable(ttype, db, data)
                        End Select
					End If
					Return (New StandardResponse("404"))

					'Case "gis"
					'    Select Case action
					'        Case "data"
					'           Return DataTransfer.UploadGIS(db, data)   ''' Not in use
					'    End Select
					'    Exit Select
                Case "shape"
                    Select Case action
                        Case "reset"
                            Return GISTransfer.ResetMapData(db, data)
                        Case "getparcelfields"
                            Return GISTransfer.GetParcelFields(db, data)
                        Case "track"
                            Return GISTransfer.UpdateStatus(db, data)
                        Case "data"
                            Return GISTransfer.ProcessShapeData(db, data)

                        Case "getlayerconfig"
                            Return DataTransfer.GetLayerConfiguration(db, data)
                        Case "setlayerconfig"
                            Return DataTransfer.SetLayerConfiguration(db, data)
                            'Case "extract"
                            '    Return DataTransfer.ExtractShapeLayers(db, data)   ''' Not in use
                    End Select
                    Exit Select
                Case "lookup"
                    Select Case action
                        Case "data"
                            Return DataTransfer.UploadLookUp(db, data)
                    End Select
                    Exit Select
                Case "update"
                    Select Case action
                        Case "downsync"
                            Return DataTransfer.DownsyncChanges(db, data)
                        Case "downsync2"
                            Return DataTransfer.DownloadParcelChanges(db, data)
                        Case "upload", "upsync"
                            Return DataTransfer.UpsyncChanges(db, data)
                        Case "rollback"
                            Return DataTransfer.RollBackParcelChanges(db, data)
                        Case "reject"
                            Return DataTransfer.RejectParcelChanges(db, data)
                        Case "defer"
                            Return DataTransfer.DeferParcelChanges(db, data)
                    End Select
                    Exit Select
                Case "sync"
                    Select Case action
                        Case "fieldlist"
                            Return DataSyncProcessor.GetFieldList(db, data)
                        Case "auxorder"
                            Return DataSyncProcessor.GetAuxTableEditSequence(db, data)
                        Case "statusquery"
                            Return DataSyncProcessor.GetStatusQuery(db, data)
                    End Select
                Case "sketch"
                    Dim sketchReq As SketchTransferRequest = data
                    If action = "data" Then
                        Dim sketchProcessor As ISketchProcessor
                        If sketchReq.sketchFormat Is Nothing OrElse sketchReq.sketchFormat.Trim = "" Then
                            Throw New Exception("sketchFormat is a required parameter.")
                        End If
                        If sketchReq.sketchFormat.Trim.ToLower = "rapidsketch" Then
                            sketchProcessor = New CAMACloud.Plugins.RapidSketch.RapidSketchProcessor
                            sketchReq.sketchFormat = "RapidSketch"
                        Else
                            Throw New Exception("Unrecognized sketch format.")
                        End If
                        sketchReq.sketchProcessor = sketchProcessor
                    End If

                    Select Case action
                        Case "init"
                            Return SketchDataTransfer.Initialize(db, sketchReq)
                        Case "data"
                            Return SketchDataTransfer.ProcessPage(db, sketchReq)
                        Case "commit"
                            Return SketchDataTransfer.Commit(db, sketchReq)
                        Case "cancel"
                            Return SketchDataTransfer.Cancel(db, sketchReq)
                    End Select
            End Select
            Dim resp As New StandardResponse("404")
            Return resp
        End Function
#End Region

#Region "/access"
        Private Function executeAccessRequest(db As Database, task As String, action As String, data As AccessRequest) As StandardResponse
            Select Case task
                Case "user"
                    If True Then
                        Select Case action
                            Case "create"
                                Return AccessManagement.CreateUser(db, data)
                            Case "edit"
                                Return AccessManagement.EditUser(db, data)
                            Case "list"
                                Return AccessManagement.ListUsers(db, data)
                                'return UserManagement.ProcessRequest(action, data, task);
                        End Select
                        Exit Select
                    End If

            End Select
            Dim resp As New StandardResponse("404")
            Return resp
        End Function
#End Region

#Region "/content"
        Private Function executeContentRequest(db As Database, task As String, action As String, data As PhotoSyncRequest) As StandardResponse
            Try
                Select Case task
                    Case "photos"
                        Select Case action
                            'Photo downsync API without paging start
                            Case "download"
                                Return ContentTransfer.DownSync(db, data)
                                'Photo downsync API without paging end
                            Case "upload"
                                Return ContentTransfer.UploadPhotos(db, data)
                            Case "setmainflag"
                                Return ContentTransfer.SetPhotoFlag(db, data)
                            Case "clientmap"
                                Return ContentTransfer.UpdatePhotoClientReferenceId(db, data)
                                'Photo downsync API with paging start
                            Case "init"
                                Return ContentTransfer.PhotoDownSync_Init(db, data)
                            Case "data"
                                Return ContentTransfer.PhotoDownSync_ProcessData(db, data)
                            Case "commit"
                                Return ContentTransfer.PhotoDownSync_Commit(BulkDataTransferType.PhotoDownSync, db, data)
                            Case "commitpage"
                                Return ContentTransfer.PhotoDownSync_CommitPage(BulkDataTransferType.PhotoDownSync, db, data)
                            Case "reject"
                                Return ContentTransfer.PhotoDownSync_Reject(BulkDataTransferType.PhotoDownSync, db, data)
                            Case "cancel"
                                Return ContentTransfer.PhotoDownSync_Cancel(BulkDataTransferType.PhotoDownSync, db, data)
                                'Photo downsync API with paging  end
                            Case "backup"
                                Return ContentTransfer.BackupPhotoIndex(db, data)
                            Case "restore"
                                Return ContentTransfer.RestorePhotoIndex(db, data)
                            Case "emptyparcels"
                                Return ContentTransfer.ListEmptyParcels(db, data)
                            Case "delete"
                                Return ContentTransfer.DeletePhotos(db, data)
                        End Select
                        Exit Select
                    Case "photomap"
                        Select Case action
                            Case "download"
                                Return ContentTransfer.PhotoMap_Download(db, data)
                            Case "upload"
                                Return ContentTransfer.PhotoMap_Upload(db, data)
                        End Select
                        Exit Select

                End Select
                Dim resp As New StandardResponse("404")
                Return resp
            Catch exs As CAMACloud.Exceptions.ServiceExceptionBase
                Throw exs
            Catch ex As Exception
                Throw New CAMACloud.Exceptions.ServiceFailure(ex)
            End Try

        End Function
#End Region

#Region "/temp"
        Private Function executeTempRequest(db As Database, task As String, action As String, data As StandardRequest) As StandardResponse
            Select Case task

                Case "pacs"
                    Select Case action
                        Case "finalize"
                            Return FinalizeNeighborhood.FinalizeTAData(db, data)
                    End Select
                    Exit Select
            End Select
            Dim resp As New StandardResponse("404")
            Return resp
        End Function
#End Region

#Region "/mailreciver"
        Private Function executeMailerRequest(db As Database, task As String, data As MailReceiverRequest) As StandardResponse
            Select Case task
                Case "msgrecive"
                    Try
                        Dim sender As String = data.sender
                        Dim subject As String = data.subject
                        Dim content As String = data.content
                        Dim datime As String = data.dateTime

                        Dim documentsPath As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
                        If String.IsNullOrEmpty(documentsPath) Then
                            documentsPath = "C:\DefaultDocumentsPath"
                        End If
                        Dim folderName As String = "Messages"
                        Dim fileName As String = $"{DateTime.Now.ToString("yyyyMMddHHmmss")}.txt"
                        Dim folderPath As String = Path.Combine(documentsPath, folderName)
                        If Not Directory.Exists(folderPath) Then
                            Directory.CreateDirectory(folderPath)
                        End If
                        Dim filePath As String = Path.Combine(folderPath, fileName)
                        Dim fileContent As String = $"Sender: {sender}{Environment.NewLine}Subject: {subject}{Environment.NewLine}Content: {content}{Environment.NewLine}DateTime: {datime}"
                        File.WriteAllText(filePath, fileContent)
                        Return New StandardResponse("200")
                    Catch ex As exception

                    End Try
            End Select
            Dim resp As New StandardResponse("404")
            Return resp
        End Function
#End Region

#End Region



    End Class
End Namespace
