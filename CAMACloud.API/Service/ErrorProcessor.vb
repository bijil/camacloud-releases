﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts

Namespace CAMACloud.IntegrationAPI.ErrorHandling
    Public Class ErrorProcessor

        Public Shared Function TranslateToResponse(exception As Exception) As StandardResponse
            Dim resp As New StandardResponse("500")

            Dim ex As Exception = exception

            While ex IsNot Nothing
                resp.AddMessage(ex.Message)
                ex = ex.InnerException
            End While

            resp.responseFormat = "json"

            If HttpContext.Current.Request.QueryString("debug") = "true" Then
                resp.data = exception.StackTrace
            End If
            '
            Return resp
        End Function

    End Class
End Namespace
