﻿Option Strict Off
Imports CAMACloud.RemoteIntegration
Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts

Namespace CAMACloud.IntegrationAPI.ServiceContracts
    <ServiceContract()> _
    Interface IDataIntegrationService
        'CONVERTED
        <OperationContract()> _
        Function ProcessTransferRequest(task As String, action As String, data As DataTransferRequest) As StandardResponse

        <OperationContract()> _
        Function ProcessSetupRequest(task As String, action As String, data As SchemaRequest) As StandardResponse

        <OperationContract()> _
        Function ProcessNeighborhoodRequest(task As String, data As NeighbourhoodRequest) As StandardResponse

        <OperationContract()> _
        Function ProcessFinalizeRequest(task As String, data As SchemaRequest) As StandardResponse


        'TO BE CONVERTED

        <OperationContract()> _
        Function ProcessConfigurationRequest(task As String, action As String, data As ApplicationConfigRequest) As StandardResponse

        <OperationContract()> _
        Function ProcessSystemRequest(task As String, data As SystemRequest) As StandardResponse

        <OperationContract()> _
        Function ProcessAccessRequest(task As String, action As String, data As AccessRequest) As StandardResponse

        <OperationContract()>
        Function ProcessMailerRequest(task As String, data As MailReceiverRequest) As StandardResponse

        <OperationContract()> _
        Function ProcessContentRequest(task As String, action As String, data As PhotoSyncRequest) As StandardResponse

        <OperationContract()> _
        Function ProcessTempRequest(task As String, action As String, data As StandardRequest) As StandardResponse

        <OperationContract()> _
        Function ProcessParcelRequest(task As String, action As String, data As ParcelRequest) As StandardResponse

        <OperationContract()> _
        Function ProcessTemplateRequest(action As String, data As TemplateRequest) As StandardResponse

        <OperationContract()> _
        Function ProcessSketchDataRequest(action As String, data As SketchTransferRequest) As StandardResponse

        'GENERIC TEMPLATES
        <OperationContract()> _
        Function ProcessTaskAction(service As String, task As String, action As String, data As StandardRequest) As StandardResponse

        <OperationContract()> _
        Function ProcessTask(service As String, task As String, data As StandardRequest) As StandardResponse

        <OperationContract()> _
        Function ProcessAction(action As String, data As StandardRequest) As StandardResponse

        <OperationContract()> _
        Function ProcessHomepage(data As StandardRequest) As StandardResponse

    End Interface
End Namespace
