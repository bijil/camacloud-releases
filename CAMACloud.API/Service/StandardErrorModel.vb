﻿Imports System.Configuration
Imports System.IO
Imports System.Net
Imports System.Runtime.Serialization.Json
Imports System.ServiceModel
Imports System.ServiceModel.Channels
Imports System.ServiceModel.Description
Imports System.ServiceModel.Dispatcher
Imports System.ServiceModel.Web
Imports System.Web
Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts
Imports CAMACloud.Exceptions

Namespace CAMACloud.IntegrationAPI.ErrorHandling
    <AttributeUsage(AttributeTargets.[Class])> _
    Public Class StandardErrorModelAttribute
        Inherits Attribute
        Implements IErrorHandler
        Implements IServiceBehavior


#Region "IErrorHandler Members"
        Public Function HandleError([error] As Exception) As Boolean Implements IErrorHandler.HandleError
            'Console.WriteLine("HandleError called.");
            ' Returning true indicates you performed your behavior. 

            Dim context As OperationContext = OperationContext.Current
            Dim accessKey = ""

            If [error].[GetType]().IsSubclassOf(GetType(ServiceExceptionBase)) Then
                Dim errorFileTag As String = DateTime.Now.ToString("yyyy-MM-dd-HH") & "\" & DateTime.Now.ToString("mm-ss") & HttpContext.Current.Request.Url.AbsolutePath.Replace("/"c, "-"c)
                Dim se As ServiceExceptionBase = DirectCast([error], ServiceExceptionBase)
                Dim sr As StandardRequest = DirectCast(se.ServiceData, StandardRequest)

                If sr.accessKey = "" Then
                    accessKey = sr.accessKey
                End If

                Dim requestBody As String = ""
                If se.ServiceData.[GetType]().Name = GetType(SchemaRequest).Name Then
                    requestBody = DataTools.JSON.Serialize(Of SchemaRequest)(DirectCast(se.ServiceData, SchemaRequest))
                ElseIf se.ServiceData.[GetType]().Name = GetType(DataTransferRequest).Name Then
                    requestBody = DataTools.JSON.Serialize(Of DataTransferRequest)(DirectCast(se.ServiceData, DataTransferRequest))
                End If
                If requestBody <> "" Then
                    Dim errorLogRoot As String = ConfigurationManager.AppSettings("ErrorLogRoot")
                    Dim errorPath As String = Path.Combine(errorLogRoot, "APIBody", errorFileTag & ".txt")
                    If Not Directory.Exists(Path.GetDirectoryName(errorPath)) Then
                        Directory.CreateDirectory(Path.GetDirectoryName(errorPath))
                    End If
                    File.WriteAllText(errorPath, requestBody)
                End If
                If sr.extractedData <> "" Then
                    Dim errorLogRoot As String = ConfigurationManager.AppSettings("ErrorLogRoot")
                    Dim errorPath As String = Path.Combine(errorLogRoot, "APIBody", errorFileTag & "-data.txt")
                    If Not Directory.Exists(Path.GetDirectoryName(errorPath)) Then
                        Directory.CreateDirectory(Path.GetDirectoryName(errorPath))
                    End If
                    File.WriteAllText(errorPath, sr.extractedData)
                End If
                If se.ErrorMailRequired Then
                    CAMACloud.ErrorMailer.ReportWCFException("CAMACloud-API", [error], context, System.Web.HttpContext.Current.Request, accessKey)
                End If
            Else
                CAMACloud.ErrorMailer.ReportWCFException("CAMACloud-API", [error], context, System.Web.HttpContext.Current.Request, accessKey)
            End If





            Return True
        End Function

        ' This is a trivial implementation that converts Exception to FaultException<GreetingFault>. 
        Public Sub ProvideFault([error] As Exception, version As MessageVersion, ByRef message__1 As Message) Implements IErrorHandler.ProvideFault
            Dim resp As StandardResponse = ErrorProcessor.TranslateToResponse([error])
            message__1 = Message.CreateMessage(version, "", resp, New DataContractJsonSerializer(resp.[GetType]()))

            Dim wbf = New WebBodyFormatMessageProperty(WebContentFormat.Json)
            message__1.Properties.Add(WebBodyFormatMessageProperty.Name, wbf)

            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json"
            WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.InternalServerError
        End Sub

#End Region

#Region "IServiceBehavior Members"
        Public Sub AddBindingParameters(description As ServiceDescription, serviceHostBase As ServiceHostBase, endpoints As System.Collections.ObjectModel.Collection(Of ServiceEndpoint), parameters As System.ServiceModel.Channels.BindingParameterCollection) Implements IServiceBehavior.AddBindingParameters
            For Each ep As ServiceEndpoint In endpoints
                Dim httpBinding As WebHttpBinding = DirectCast(ep.Binding, WebHttpBinding)

                httpBinding.MaxReceivedMessageSize = 2147483647
            Next
            Return
        End Sub

        ' This behavior is an IErrorHandler implementation and  
        ' must be applied to each ChannelDispatcher. 
        Public Sub ApplyDispatchBehavior(description As ServiceDescription, serviceHostBase As ServiceHostBase) Implements System.ServiceModel.Description.IServiceBehavior.ApplyDispatchBehavior
            Console.WriteLine("The EnforceGreetingFaultBehavior has been applied.")
            For Each chanDisp As ChannelDispatcher In serviceHostBase.ChannelDispatchers
                chanDisp.ErrorHandlers.Add(Me)
            Next
        End Sub

        ' This behavior requires that the contract have a SOAP fault with a detail type of GreetingFault. 
        Public Sub Validate(description As ServiceDescription, serviceHostBase As ServiceHostBase) Implements IServiceBehavior.Validate

        End Sub
#End Region

    End Class
End Namespace
