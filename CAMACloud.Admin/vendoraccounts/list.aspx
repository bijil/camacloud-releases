﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/ClientAdmin.master" CodeBehind="list.aspx.vb" Inherits="CAMACloud.Admin._default1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
        .search-panel
         {
            padding: 7px;
            border: 1px solid #CFCFCF;
            width: 97%;
            background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#CFCFCF), to(#F8F8F8));
         }
        .ui-autocomplete{
        
   	 		overflow: auto;
    		height: 500px;
		}
        .search-panel label
        {
            margin-right: 5px;
        }
       .vendor-edit-table
         {
	      height:160px;
	      width:489px;
         }

         .vendor-edit-form
         {
	
         }

       .vendor-edit-form tr td:first-child
         {
	      width:130px;
         }


       .vendor-edit-table input[type="submit"]
         {
	       padding:5px 20px;
         }

        .vendor-edit-table input[type="submit"]:first-child
         {
	       font-weight:bold;
         }

        .vendor-edit-form tr td span
         {
	                 margin-left:8px;
         }

         .vendor-edit-form tr td .validation-error
         {
	                 font-size:large;
         }
    </style>  
    <script type="text/javascript">   
     function showPopup(title) {
        $('.editvendor').dialog({
            modal: true,
            width: 850,
            height: 455,
            resizable: false,
            title: title,
            open: function (type, data) {
                $(this).parent().appendTo("form");
            }
        });
    }

    function hidePopup() {
        $('.editvendor').dialog('close');
    }
 $(function () {
 $('#<%=txtName.ClientID%>').autocomplete({
     source: function (request, response) {
         var term = request.term;
         if (!term) { return false; }
    $.ajax({
                        url: "list.aspx/Getvendor",
                        data: "{ 'vendorname':'" + term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    value: item
                              }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(JSON.stringify(XMLHttpRequest));
                        }
                    });
                },
            });

        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <h1>
        CAMA Cloud - Vendor Account</h1>
    <div class="search-panel">
        <table>
            <tr>
                <td>
                    <label>
                        Vendor Name:</label>
                    <asp:TextBox runat="server" class="autosuggest" ID="txtName" Width="300px" MaxLength="30" CssClass="textboxAuto"  />
                </td>
                <td style="width: 10px;">
                </td>
                <td>
                    <asp:Button runat="server" ID="btnSearch" Text=" Search " />
                </td>
                <td style="width: 10px;">
                </td>
                 <td>
                   <asp:Button runat="server" ID="btnNewVendor" Text="Add New Vendor" OnClientClick="return true;"/>
                </td>
            </tr>
        </table>
    </div>        
    <asp:GridView runat="server" ID="results" Width="80%" AllowPaging="True" PageSize="20">
        <Columns>
             <asp:TemplateField>
                <ItemStyle Width="10px" />
                <HeaderTemplate>
                    Vendor Id</HeaderTemplate>
                <ItemTemplate>
                    <%# Eval("Id")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemStyle Width="140px" />
                <HeaderTemplate>
                    Vendor Name </HeaderTemplate>
                <ItemTemplate>
                    <asp:LinkButton runat="server" ID="lbEditVendor" Font-Size="9pt" CommandName="EditVendor" CommandArgument='<%# Eval("Id")%>' OnClientClick="return true"> <%# Eval("Name")%></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemStyle Width="60px" />
                <HeaderTemplate>
                    Website </HeaderTemplate>
                <ItemTemplate>
                    <%# Eval("Website")%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <%--by haresh--%>
    <%--<div class="editvendor" style="display:none;height: 500px; width: 80%;height:350px;">
        <asp:UpdatePanel ID="uplEdit" runat="server">
            <ContentTemplate>
                <asp:Panel runat="server" ID="pnlUserEdit" CssClass="vendor-edit-panel" Style="width: 819px; height: 228px;">
                 <asp:HiddenField ID="hdnVendorId" runat="server" />
                    <table class="vendor-edit-table">
                        <tr>
                            <td style="width: 8px;"></td>
                            <td>
                                <h3>
                                    Vendor Properties</h3>
                                <table class="vendor-edit-form">
                                    <tr>
                                        <td>Common Name: </td>
                                        <td>
                                            <asp:TextBox ID="txtCommonName" runat="server" autocomplete="off" Width="250px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvtxtCommonName" runat="server" ForeColor="Red" Display="Dynamic" ErrorMessage="*" ControlToValidate="txtCommonName"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Name: </td>
                                        <td>
                                            <asp:TextBox ID="txtVendorName" runat="server" autocomplete="off" Width="250px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvtxtVendorName" runat="server" ForeColor="Red" Display="Dynamic" ErrorMessage="*" ControlToValidate="txtVendorName"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                        <td>Website : </td>
                                        <td>
                                            <asp:TextBox ID="txtWebSite" runat="server" autocomplete="off" Width="250px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvtxtWebSite" runat="server" ForeColor="Red" ErrorMessage="*" ControlToValidate="txtWebSite" Display="Dynamic"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="revtxtWebSite" runat="server" ControlToValidate="txtWebSite" ErrorMessage="Invalid Web Site!" ForeColor="#D20B0E"
                                                ValidationExpression="^((http|https)://)?([\w-]+\.)+[\w]+(/[\w- ./?]*)?$" Display="Dynamic"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Vendor Information: </td>
                                        <td>
                                            <asp:TextBox ID="txtVendorInfo" runat="server" TextMode="MultiLine" Rows="2" Width="250px" autocomplete="off"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvttxtVendorInfo" runat="server" ForeColor="Red" Display="Dynamic" ErrorMessage="*" ControlToValidate="txtVendorInfo"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>License Agreement: </td>
                                        <td>
                                            <asp:TextBox ID="txtLicenseAgreement" runat="server" TextMode="MultiLine" Rows="5" Width="400px"></asp:TextBox>
                                        </td>
                                    </tr>
 
                                </table>
                                <div style="margin-top: 10px; margin-bottom: 15px;">
                                    <asp:Button runat="server" ID="btnSaveVendor" Text="Save Vendor" />
                                    <asp:Button runat="server" ID="btnCancel" Text="Cancel" OnClientClick="return true;"/>
                                </div>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>--%>
</asp:Content>

