﻿Imports System.Web.Services
Imports System.Web.Script.Services
Imports System.Data.SqlClient
Imports System.Collections.Generic



Public Class _default1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadGrid()
        End If
        LoadGrid()
    End Sub
    Protected Sub results_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles results.PageIndexChanging
        LoadGrid(e.NewPageIndex)
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        LoadGrid()
    End Sub
    Sub LoadGrid(Optional ByVal pageIndex As Integer = 0)
        Dim sqlFilter As String = ""
        If txtName.Text.Trim <> "" Then
            sqlFilter = "Where Name LIKE '%{0}%'".FormatString(txtName.Text.Trim.ToSqlValue.Trim("'"))
        End If
        results.PageIndex = pageIndex
        results.DataSource = Database.System.GetDataTable("SELECT * FROM dbo.Vendor " + sqlFilter)
        results.DataBind()
    End Sub
    <WebMethod()> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Shared Function Getvendor(ByVal vendorname As String) As List(Of String)
        Dim Vendors As New List(Of String)()
        Dim conn As New SqlConnection
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ControlDB").ConnectionString
        Dim cmd As New SqlCommand()
        cmd.CommandText = "select Name,Id from Vendor where " & "Name like @SearchText + '%'"
        cmd.Parameters.AddWithValue("@SearchText", vendorname)
        cmd.Connection = conn
        conn.Open()
        Dim sdreader As SqlDataReader = cmd.ExecuteReader()
        While sdreader.Read()
            Vendors.Add(sdreader("Name"))
        End While
        conn.Close()
        Return Vendors
    End Function

    'Protected Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
    ''    RunScript("hidePopup();")
    ''End Sub
    'Protected Sub btnSaveVendor_Click(sender As Object, e As System.EventArgs) Handles btnSaveVendor.Click
    '    Try
    '        If hdnVendorId.Value = "" Then
    '        End If
    '        Database.System.Execute("UPDATE Vendor SET CommonName={0},Name={1},Website={2},VendorInfo={3},AgreementContent={4}) VALUES({0},{1},{2},{3},{4}) Where Id ={5}".SqlFormatString(txtCommonName.Text, txtVendorName.Text, txtWebSite.Text, txtVendorInfo.Text, txtLicenseAgreement.Text, hdnVendorId.Value))
    '        Alert("Vendor updated successfully.\n\nClick Close to go back to the vendor list.")
    '        Response.Redirect("~/vendoraccounts/list.aspx")
    '    Catch ex As Exception
    '        Alert(ex.Message)
    '    End Try
    'End Sub

    Protected Sub btnNewVendor_Click(sender As Object, e As EventArgs) Handles btnNewVendor.Click
        Response.Redirect("~/vendoraccounts/CreateVendor.aspx")
    End Sub
    Protected Sub results_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles results.RowCommand
        Dim VendorId As Integer = e.CommandArgument

        Try
            If e.CommandName = "EditVendor" Then
                Response.Redirect("~/settings/licenseagreement.aspx?Vendorid=" + VendorId.ToString + "")
                'LoadVendorForEdit(e.CommandArgument)
            End If
        Catch ex As Exception
            Alert(ex.Message)
        End Try
    End Sub
    'Sub LoadVendorForEdit(vendorId As Integer)
    '    hdnVendorId.Value = vendorId
    '    Dim dr As DataRow = Database.System.GetTopRow("SELECT * FROM dbo.Vendor WHERE Id='" + vendorId + "'")
    '    Try
    '        If dr IsNot Nothing Then
    '            txtCommonName.Text = dr.GetString("CommonName")
    '            txtVendorName.Text = dr.GetString("Name")
    '            txtWebSite.Text = dr.GetString("Website")
    '            txtVendorInfo.Text = dr.GetString("VendorInfo")
    '            txtLicenseAgreement.Text = dr.GetString("AgreementContent")
    '        End If
    '    Catch ex As Exception
    '        Alert(ex.Message)
    '    End Try
    'End Sub
    'Sub ClearForm()
    '    txtCommonName.Text = ""
    '    txtVendorName.Text = ""
    '    txtWebSite.Text = ""
    '    txtVendorInfo.Text = ""
    '    txtLicenseAgreement.Text = ""
    'End Sub

    Private Shared Function pre() As Object
        Throw New NotImplementedException
    End Function

End Class
