﻿Public Class CreateVendor
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub btnSaveVendor_Click(sender As Object, e As System.EventArgs) Handles btnSaveVendor.Click
        Try
            If Not IsNothing(txtVendorName) Then
                Database.System.Execute("INSERT INTO dbo.Vendor(CommonName,Name,Website,VendorInfo,AgreementContent) VALUES({0},{1},{2},{3},{4})".SqlFormatString(txtCommonName.Text, txtVendorName.Text, txtWebSite.Text, txtVendorInfo.Text, txtLicenseAgreement.Text))
            End If
            Alert("Vendor added successfully.\n\nClick Close to go back to the vendor list.")
            Response.Redirect("~/vendoraccounts/list.aspx")
        Catch ex As Exception
            Alert(ex.Message)
        End Try
    End Sub
    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Response.Redirect("~/vendoraccounts/list.aspx")
    End Sub

End Class