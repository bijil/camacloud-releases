﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/ClientAdmin.master" CodeBehind="CreateVendor.aspx.vb" Inherits="CAMACloud.Admin.CreateVendor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .vendor-edit-table
         {
	      height:350px;
	      border:1px solid #CFCFCF;
	      width:inherit;
         }

         .vendor-edit-form
         {
	
         }

       .vendor-edit-form tr td:first-child
         {
	      width:130px;
         }


       .vendor-edit-table input[type="submit"]
         {
	       padding:5px 20px;
         }

        .vendor-edit-table input[type="submit"]:first-child
         {
	       font-weight:bold;
         }

        .vendor-edit-form tr td span
         {
	                 margin-left:8px;
         }

         .vendor-edit-form tr td .validation-error
         {
	                 font-size:large;
         }
    </style>  
    <script type="text/javascript">
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <h1>
        CAMA Cloud - Create Vendor Account</h1>
     <div class="addnewvendor" style=" height: 500px; width: 80%;height:350px;">
        <asp:UpdatePanel ID="uplEdit" runat="server">
            <ContentTemplate>
                <asp:Panel runat="server" ID="pnlUserEdit" CssClass="vendor-edit-panel" Style="width: 819px; height: 228px;">
                    <table class="vendor-edit-table">
                        <tr>
                            <td style="width: 8px;"></td>
                            <td>
                                <h3>
                                    Vendor Properties</h3>
                                <table class="vendor-edit-form">
                                    <tr>
                                        <td>Common Name: </td>
                                        <td>
                                            <asp:TextBox ID="txtCommonName" runat="server" autocomplete="off" Width="250px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvtxtCommonName" runat="server" ForeColor="Red" Display="Dynamic" ErrorMessage="*" ControlToValidate="txtCommonName"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Name: </td>
                                        <td>
                                            <asp:TextBox ID="txtVendorName" runat="server" autocomplete="off" Width="250px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvtxtVendorName" runat="server" ForeColor="Red" Display="Dynamic" ErrorMessage="*" ControlToValidate="txtVendorName"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                        <td>Website : </td>
                                        <td>
                                            <asp:TextBox ID="txtWebSite" runat="server" autocomplete="off" Width="250px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvtxtWebSite" runat="server" ForeColor="Red" ErrorMessage="*" ControlToValidate="txtWebSite" Display="Dynamic"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="revtxtWebSite" runat="server" ControlToValidate="txtWebSite" ErrorMessage="Invalid Web Site!" ForeColor="#D20B0E"
                                                ValidationExpression="^((http|https)://)?([\w-]+\.)+[\w]+(/[\w- ./?]*)?$" Display="Dynamic"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Vendor Information: </td>
                                        <td>
                                            <asp:TextBox ID="txtVendorInfo" runat="server" TextMode="MultiLine" Rows="2" Width="500px" autocomplete="off"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvttxtVendorInfo" runat="server" ForeColor="Red" Display="Dynamic" ErrorMessage="*" ControlToValidate="txtVendorInfo"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>License Agreement: </td>
                                        <td>
                                            <asp:TextBox ID="txtLicenseAgreement" runat="server" TextMode="MultiLine" Rows="5" Width="500px"></asp:TextBox>
                                        </td>
                                    </tr>
 
                                </table>
                                <div style="margin-top: 10px; margin-bottom: 15px;">
                                    <asp:Button runat="server" ID="btnSaveVendor" Text="Save Vendor" />
                                    <asp:Button runat="server" ID="btnCancel" Text="Cancel" OnClientClick="return true;"/>
                                </div>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

