﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Web.Services

Public Class forceapplicationupdates
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    <System.Web.Services.WebMethod()>
    Public Shared Function getCounties() As JSONTable
        Dim dt As DataTable = Database.System.GetDataTable("SELECT * FROM Organization")
        Return dt.ConvertToList()
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function getCamaSystem() As JSONTable
        Dim dt As DataTable = Database.System.GetDataTable("SELECT * FROM CamaSystem")
        Return dt.ConvertToList()
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function getVendor() As JSONTable
        Dim dt As DataTable = Database.System.GetDataTable("SELECT * FROM Vendor")
        Return dt.ConvertToList()
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function getStage() As JSONTable
        Dim dt As DataTable = Database.System.GetDataTable("SELECT * FROM OrganizationStage")
        Return dt.ConvertToList()
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function saveForceUpdate(ByVal updateType As String, ByVal updateStage As String, ByVal county As String, ByVal allEnv As String) As String
        Try
            Dim sql As String = "INSERT INTO ForceUpdateApplication([EventDate], [LoginId], [UpdateType], [Stage], [UpdateAllEnv]) VALUES (GETUTCDATE(), '" + Membership.GetUser().ToString() + "', '" + updateType + "', '" + updateStage + "', '" + allEnv + "'); SELECT CAST(@@IDENTITY  AS INT) As NewId;"
            Dim newId As Integer = Database.System.GetIntegerValue(sql)
            Dim selSql As String = "SELECT " + newId.ToString() + ", Id, 0 FROM Organization WHERE " + IIf(allEnv = "1", "Stage Not IN (6, 7)", "Id IN (" + county + ")")
            sql = "INSERT INTO ForceUpdateApplicationStatus([UpdateId], [OrgId], [Status]) " + selSql
            Database.System.Execute(sql)
            Return "Sucess"
        Catch Ex As Exception
            Return Ex.Message
        End Try
    End Function
End Class