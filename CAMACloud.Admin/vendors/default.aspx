﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/ClientAdmin.master" CodeBehind="default.aspx.vb" Inherits="CAMACloud.Admin._default1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
        .search-panel
        {
            padding: 7px;
            border: 1px solid #CFCFCF;
            width: 97%;
            background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#CFCFCF), to(#F8F8F8));
        }
        
        .search-panel label
        {
            margin-right: 5px;
        }
    </style>
    <script type="text/javascript">
        function showPopup(title) {
            $('.user-edit-panel').html('');
            $('.edit-popup').dialog({
                modal: true,
                width: 825,
                height: 455,
                resizable: false,
                title: title,
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        }

        function hidePopup() {
            $('.edit-popup').dialog('close');
        }

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <h1>
        CAMA Cloud - Vendor Account</h1>
    <div class="search-panel">
        <table>
            <tr>
                <td>
                    <label>
                        Vendor Name:</label>
                    <asp:TextBox runat="server" ID="txtName" Width="300px" MaxLength="30" CssClass="textboxAuto" />
                </td>
                <td style="width: 10px;">
                </td>
                <td>
                    <asp:Button runat="server" ID="btnSearch" Text=" Search " />
                </td>
            </tr>
        </table>
    </div>
    <div class="link-panel">
                <asp:LinkButton runat="server" ID="lbNewUser" Text="Add New Vendor" OnClientClick="showPopup('Create new vendor');" />
            </div>
    <asp:GridView runat="server" ID="results" Width="100%" AllowPaging="True" PageSize="20">
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="ID" ItemStyle-Width="30px">
                <ItemStyle Width="10px"></ItemStyle>
            </asp:BoundField>
            <asp:TemplateField>
                <ItemStyle Width="140px" />
                <HeaderTemplate>
                    Vendor Name</HeaderTemplate>
                <ItemTemplate>
                    <%# Eval("Name")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemStyle Width="60px" />
                <HeaderTemplate>
                    Web Site </HeaderTemplate>
                <ItemTemplate>
                    <%# Eval("Website")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemStyle Width="220px" />
                <HeaderTemplate>
                    Vendor Information </HeaderTemplate>
                <ItemTemplate>
                    <%# Eval("VendorInfo")%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
<div class="edit-popup" style="display: none;">
        <asp:UpdatePanel ID="uplEdit" runat="server">
            <ContentTemplate>
                <asp:Panel runat="server" ID="pnlUserEdit" CssClass="user-edit-panel" Style="width: 800px; height: 410px;">
                    <asp:HiddenField ID="hdnUserId" runat="server" />
                    <table class="user-edit-table">
                        <tr>
                            <td style="width: 8px;"></td>
                            <td>
                                <h3>
                                    Vendor Properties</h3>
                                <table class="user-edit-form">
                                    <tr>
                                        <td>Name: </td>
                                        <td>
                                            <asp:TextBox ID="txtFirstName" runat="server" autocomplete="off"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvtxtFirstName" runat="server" ForeColor="Red" Display="Dynamic" ErrorMessage="*" ControlToValidate="txtFirstName"
                                                ValidationGroup="UserProp"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                        <td>Web Site : </td>
                                        <td>
                                            <asp:TextBox ID="txtUserMail" runat="server" autocomplete="off"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvtxtUserMail" runat="server" ForeColor="Red" ErrorMessage="*" ControlToValidate="txtUserMail" Display="Dynamic"
                                                ValidationGroup="UserProp"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="revtxtUserMail" runat="server" ControlToValidate="txtUserMail" ErrorMessage="Invalid Email!" ForeColor="#D20B0E"
                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic" ValidationGroup="UserProp"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
 
                                </table>
                                <div style="margin-top: 10px; margin-bottom: 15px;">
                                    <asp:Button runat="server" ID="btnSaveUser" Text="Save User" ValidationGroup="UserProp2" />
                                    <asp:Button runat="server" ID="btnCancel" Text="Cancel" />
                                </div>
                            </td>
                            <td class="v-split" style="width: 12px;"></td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

