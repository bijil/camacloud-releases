﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/ClientAdmin.master"
    AutoEventWireup="false" Inherits="CAMACloud.Admin.clientaccounts_edit" Codebehind="edit.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .panel
        {
            margin-top: 40px;
            margin-left: 20px;
            margin-right: 20px;
            padding: 8px;
            border: 1px solid #bfbdad;
            background: #e0eaf0;
            border-top-left-radius: 4px;
            border-bottom-right-radius: 4px;
            border-bottom-left-radius: 4px;
            border-top-right-radius: 4px;
            background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#e0eaf0), to(#fff));
        }
        .Header
        {
            margin-left: 10px;
            margin-right: 10px;
            margin-top: 10px;
        }
        
        .search-panel label
        {
            margin-right: 5px;
        }
        .dentry td
        {
            margin-right: 0px;
        }
        #tabcontent
        {
            padding: 7px;
            width: 98%;
            float: left;
            margin-left: 0px;
            margin-right: 10px;
        }
        #org_content
        {
            padding: 7px;
            margin-left: 10px;
            margin-right: 10px;
        }
        .content_tab
        {
            margin-left: 8px;
            margin-top: 7px;
            padding: 8px;
            border: 1px solid #bfbdad;
            border-top-left-radius: 4px;
            border-bottom-right-radius: 4px;
            border-bottom-left-radius: 4px;
            border-top-right-radius: 4px;
            height: 240px;
        }
        .content_tab_head
        {
            margin-left: 8px;
            margin-right: 0px;
            padding: 7px;
            background: #e0eaf0;
            border: 1px solid #bfbdad;
            border-top-left-radius: 3px 9px;
            border-bottom-right-radius: 3px 9px;
            border-bottom-left-radius: 3px 9px;
            border-top-right-radius: 3px 9px;
            background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#e0eaf0), to(#fff));
        }
    </style>
    <script type="text/javascript">
         $(function () {
             $("#tabbed-editor").tabs({
            show: function() {
                var sel = $('#tabbed-editor').tabs('select', $("#<%= hidLastTab.ClientID %>").val(sel));
 
            },
            selected: <%= hidLastTab.Value %>
         });
         });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="Header">
        <asp:HiddenField ID="hidLastTab" runat="server" Value="0" />
        <div class="panel">
            <asp:Label ID="lbltiltle" runat="server" Font-Bold="true" Text="Client Account Management"></asp:Label>
        </div>
        <asp:HiddenField runat="server" ID="hdnOrgid" Value="" />
        <asp:HiddenField runat="server" ID="hdnOrgname" Value="" />
        <asp:UpdatePanel ID="updlMainContent" runat="server">
            <ContentTemplate>
                <div id="org_content">
                    <table>
                        <tr>
                            <td>
                                Name:
                            </td>
                            <td>
                                County:
                            </td>
                            <td>
                                State:
                            </td>
                            <td>
                                City:
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtName" runat="server" AutoPostBack="True" Width="256px"></asp:TextBox><asp:RequiredFieldValidator
                                    ID="NameValidator" runat="server" ForeColor="Red" ErrorMessage="*" ControlToValidate="txtName"
                                    ValidationGroup="CreateAcc"></asp:RequiredFieldValidator><asp:Label ID="lblNameNotValid"
                                        runat="server" Text="Name already exists" Visible="false" ForeColor="Red"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtCounty" runat="server" Width="256px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtState" runat="server" Width="50px"></asp:TextBox>
                                <asp:RegularExpressionValidator ValidationExpression="^[a-zA-Z0-9]{1,2}$" ID="StateValidator"
                                    Display="Dynamic" ValidationGroup="CreateAcc" runat="server" ForeColor="Red"
                                    ErrorMessage="Only Two Letters" ControlToValidate="txtState" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Button ID="btnSaveAccount" runat="server" Text="Save Account" ValidationGroup="CreateAcc" />
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div id="tabcontent">
            <table>
                <tr>
                    <td>
                        <div class="content_tab_head">
                            Server Settings</div>
                        <div class="content_tab">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <table class="comparable-edit">
                                        <tr>
                                            <td class="dentry">
                                                Host Name:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="DrpHost" runat="server" Width="195px">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="HostNameValidator" runat="server" ControlToValidate="DrpHost"
                                                    ErrorMessage="*" ForeColor="Red" InitialValue="0" ValidationGroup="ServerSettings"></asp:RequiredFieldValidator>
                                                <asp:Label ID="lblHidden" runat="server" Style="display: none;"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="dentry">
                                                Server&nbsp; Name:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="drpServer" runat="server" AutoPostBack="True" Width="195px">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="ServerValidator" runat="server" ControlToValidate="drpServer"
                                                    ErrorMessage="*" ForeColor="Red" InitialValue="0" ValidationGroup="ServerSettings" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="dentry">
                                                Database Name :
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtDbName" runat="server" Width="190px" AutoPostBack="true"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="dentry">
                                                User Name:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtUserName" runat="server" ReadOnly="True" Width="190px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="dentry">
                                                Password:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtPwd" runat="server" ReadOnly="True" TextMode="Password" Width="190px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="dentry">
                                                <asp:UpdateProgress ID="updProgress" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
                                                    <ProgressTemplate>
                                                        <img alt="progress" src="../App_Static/images/progress.gif" />
                                                        Configuring...
                                                    </ProgressTemplate>
                                                </asp:UpdateProgress>
                                            </td>
                                            <td>
                                                <div style="margin-top: 10px; margin-bottom: 15px; margin-left: -2px;">
                                                    <asp:Button ID="btnServerConfig" runat="server" Text="Config Server" ValidationGroup="ServerSettings" />
                                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" />
                                                    <asp:Label ID="lblalert" runat="server" Visible="False"></asp:Label>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </td>
                    <td>
                        <div class="content_tab_head">
                            License Keys</div>
                        <div class="content_tab">
                            <%--     <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                          <ContentTemplate>--%>
                            <table class="comparable-edit">
                                <tr>
                                    <td class="dentry">
                                        Number of licenses
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="ddlNumber" Width="50px" />
                                        <asp:Button ID="btnCreate" runat="server" Visible="false" Text="Generate" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Email
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtEmail" type="email" Width="200px" CssClass="txt-email" />
                                        <asp:RequiredFieldValidator ID="efvTxtEmail" ForeColor="Red" runat="server"  Display="Dynamic" ControlToValidate="txtEmail"
                                            ErrorMessage="*" ValidationGroup="grpEmail"></asp:RequiredFieldValidator>
                                          
                                    </td>
                                </tr>
                                <tr>
                                <td>&nbsp;

                                </td>
                                <td>
                                   <asp:RegularExpressionValidator ID="revtxtUserMail" runat="server" ControlToValidate="txtEmail"
                                                ErrorMessage="Invalid Email!" ForeColor="#D20B0E" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                Display="Dynamic" ValidationGroup="grpEmail"></asp:RegularExpressionValidator>
                                </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:Button Text=" Email Licenses " runat="server" ValidationGroup="grpEmail" ID="btnEmail"
                                            Style="height: 26px" />
                                    </td>
                                </tr>
                            </table>
                            <%--</ContentTemplate> </asp:UpdatePanel> --%>
                        </div>
                    </td>
                    <td>
                        <div class="content_tab_head">
                            Roles</div>
                        <div class="content_tab">
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                    <table class="comparable-edit">
                                        <tr>
                                            <td class="v-split" style="width: 12px;">
                                            </td>
                                            <td style="width: 200px; vertical-align: top;">
                                                <h3>
                                                    Select Roles</h3>
                                                <asp:CheckBoxList ID="cblRoles" runat="server">
                                                </asp:CheckBoxList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                <div style="margin-top: 10px; margin-bottom: 15px;">
                                                    <asp:Button runat="server" ID="btnRoles" Text="Save" />
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
