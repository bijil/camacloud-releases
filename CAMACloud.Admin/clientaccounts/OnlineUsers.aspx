﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/ClientAdmin.master" CodeBehind="OnlineUsers.aspx.vb" Inherits="CAMACloud.Admin.OnlineUsers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <style type="text/css">
        .search-panel
        {
            padding: 7px;
            padding-right:0px;
            padding-left:0px;
            border: 1px solid #CFCFCF;
            width: 97%;
            background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#CFCFCF), to(#F8F8F8));
        }
        
        .search-panel label
        {
            margin-right: 5px;
        }
        .gridresults
        {
           margin-left:1px;
           margin-right:1px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <h1>
        CAMA Cloud -Online Users</h1>
    <div class="search-panel">
        <table>
            <tr>
                <td>
                    <label>
                       Date :</label>
                    <asp:TextBox runat="server" type="date" ID="txtdate" Width="300px" MaxLength="30" />
                </td>
                <td style="width: 10px;"></td>
                <td>
                    <asp:Button runat="server" ID="btnSearch" Text=" Search " />
                </td>
            </tr>
        </table>
    </div>
    <asp:GridView runat="server" ID="results" Width="97%" AllowPaging="true" CssClass="gridresults" PageSize="50">
        <Columns>
            <asp:BoundField DataField="OrganizationName" HeaderText="County Name"  ItemStyle-Width="200px"/>
            <asp:BoundField DataField="NumberOfConsoleUser" HeaderText="No.Console Users" ItemStyle-Width="50px" />
            <asp:BoundField DataField="NumberOfMAUser" HeaderText="No.MA Users" ItemStyle-Width="50px"/>
             <asp:BoundField DataField="Total" HeaderText="Total Users"  ItemStyle-Width="50px" />
           
        </Columns>
        <EmptyDataTemplate>
            No online users found...
        </EmptyDataTemplate>

    </asp:GridView>
</asp:Content>
