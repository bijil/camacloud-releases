﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/ClientAdmin.master" CodeBehind="Reports.aspx.vb" Inherits="CAMACloud.Admin.Reports" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link rel="stylesheet" href="/App_Static/css/reports.css?<%= Now.Ticks %>" />
 <style type="text/css">
        .tblReportStats {
            width: 100%;
        }

        .txtLSide {
            width: 86%;
            margin-bottom: 3%;
        }

        .txtRSide {
            width: 87%;
        }

        .ddlLSide {
            width: 88%;
            height: 25px;
        }

        .ddlRSide {
            width: 88%;
            height: 25px;
        }

        .txtLContainer {
            width: 35%;
        }

        .txtRContainer {
            width: 35%;
        }

        .lLabel {
            width: 15%;
        }

        .RLabel {
            width: 18%;
    		height: 37px;
            
        }

        .txtAlign {
            margin-bottom: 3%;
        }

        .dropFiled {
            color: initial;
            font: 13.3333px Arial;
        }

        .btnGenerate{float: right; margin-right: 95px;}

        #MainContent_MainContent_gvTotalMACbyUser td {
            border-right: solid 1px #0ab0c4 !important;
        }

       #MainContent_MainContent_gvTotalMACbyUser .pgr td { border-left: none; }
        
		.masklayer {
	    background: black;
	    opacity: 0.5;
	    top: 0%;
	    width: 100%;
	    height: 100%;
	    bottom: 0%;
	    position: absolute;
	    display: none;
		}
		.info {
            width: 850px;
            border-left: 8px solid #CFCFCF;
            padding-left: 10px;
            line-height: 15pt;
            padding-top: 5px;
            padding-bottom: 5px;
        }
        .btnDownload{
        	background-image: url(../App_Static/css/icon16/download.png);
		    background-repeat: no-repeat;
		    background-position: center left 5px;
		    width: 110px;
		    float: right;
		    margin-bottom:10px;
        }
        
     	
		.report-item{
					
		    width: 120px;
		    height: 140px;
		    margin: 6px;
		    border: 1px solid #EFEFEF;
		    display:block;
		    cursor:pointer;
		}

		.report-item:hover{

    		border: 1px solid #CFCFCF;
		}

		.report-item .icon{

		    width: 70px;
		    height: 64px;
		    margin:25px;
		    margin-bottom:7px;
		    background-image: url(../App_Static/css/Reports/report-64.png);
		    background-position:center center;
		    background-repeat:no-repeat;
		    display:block;
		}

		.report-item .text{

		    display:block;
		    text-align:center;
		    font-size:9pt;
		    margin:5px 0px;
		}
		
		.reports
		{
		display:block;
		}
		
		.hide
		{
		display:none !important;
		}
		.show_table
		{
		display:table !important;
		}
		.show_tablerow
		{
		display:table-row !important;
		}

    </style>
    
  <script type="text/javascript">
       function loadReport(type) {
         $('.reports').toggleClass("hide");
            if(type!=3){
            $('.environmentstats').toggleClass("hide");
            $('.datestats').toggleClass("hide");
            }else{
            $('.environmentstats').toggleClass("show_table");
            $('.datestats').toggleClass("show_tablerow");
            }
             $("#<%=reportType.ClientID%>").val(type);

        }
        
        function searchValidation ()
        { //Search button click - data search and data vilidation
            var startDate = $( '.txtFromDate' ).val();
            var endDate = $( '.txtToDate' ).val();
            var strtyr = startDate.split("-")[0];
            var endyr = endDate.split("-")[0];           
            var vendorId = $( '.ddlVendor' ).val();
            if ( vendorId == '' ) { alert( 'Please select a vendor' ); return false; } //Null filed validation
            if ( startDate != '' && endDate != '' )
            { //Date field validation
                var start = new Date( startDate );
                var end = new Date( endDate );
                if ( start > end ) { alert( 'Start date cannot be greater than End date' ); return false; }
                if ( strtyr < 1800 || endyr < 1800)
                	{
                alert( 'Please Check the Dates' ); return false;
               		 }
            }
            if ( startDate == '' && endDate != '')
            {
           		 if ( endyr < 1800)
                	{
                alert( 'Please Check the Dates' ); return false;
               		 }
            
            }

            if ( startDate != '' && endDate == '' )
            {
                var todayDate = new Date();
                var month = todayDate.getMonth() + 1;
                var day = todayDate.getDate();
                var year = todayDate.getFullYear();  
				if ( strtyr < 1800 ) 
                	{
                	alert( 'Please check dates' ); return false;
                	}                
                if ( month < 10 )
                    month = '0' + month.toString();
                if ( day < 10 )
                    day = '0' + day.toString();
                var maxDate = year + '-' + month + '-' + day;
                $( '.txtToDate' ).val( maxDate );
                
            } //End date field validation
           // $( '.masklayer' ).css( "display", "block" );
            $( 'body' ).scrollTop( 0 );
            $( 'body' ).css( 'overflow', 'hidden' );
            return true;
        }
        
  </script> 



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
 <asp:HiddenField ID="reportType" value="0" runat="server" />
		<h1 style="padding-left:15px;">Reports</h1>
		<div id="Report1" class = "reports" style="padding:0px;padding-top:0px;">
		
		    <a class="fl report-item tip" tip-margin-left="-100" max-width="300" abbr=" " OnClick="loadReport('1')">
		        <div class="icon"></div>
		        <span class="text">Environment Details</span>
		    </a>
		</div>
		<div id="Report2" class = "reports"  style="padding:0px;padding-top:0px;">
		
		    <a class="fl report-item tip" tip-margin-left="-200" max-width="300" abbr=" " OnClick="loadReport('2')">
		        <div class="icon"></div>
		        <span class="text">License Count</span>
		    </a>
		</div>
		<div id="Report3" class = "reports" style="padding:0px;padding-top:0px;">
		
		    <a class="fl report-item tip" tip-margin-left="-300" max-width="300" abbr=" " OnClick="loadReport('3')">
		        <div class="icon"></div>
		        <span class="text">License Audit Trail</span>
		    </a>
		</div>
        <div id="Report4" class = "reports" style="padding:0px;padding-top:0px;">
		
		    <a class="fl report-item tip" tip-margin-left="-300" max-width="300" abbr=" " OnClick="loadReport('4')">
		        <div class="icon"></div>
		        <span class="text">Field Reviewed Summary</span>
		    </a>
		</div>
        <div id="Report5" class = "reports" style="padding:0px;padding-top:0px;">
		
		    <a class="fl report-item tip" tip-margin-left="-300" max-width="300" abbr=" " OnClick="loadReport('4')">
		        <div class="icon"></div>
		        <span class="text">QC Approved</span>
		    </a>
		</div>
        <div id="Report6" class = "reports" style="padding:0px;padding-top:0px;">
		
		    <a class="fl report-item tip" tip-margin-left="-300" max-width="300" abbr=" " OnClick="loadReport('4')">
		        <div class="icon"></div>
		        <span class="text">Field Work Details</span>
		    </a>
		</div>
        <div id="Report7" class = "reports" style="padding:0px;padding-top:0px;">
		
		    <a class="fl report-item tip" tip-margin-left="-300" max-width="300" abbr=" " OnClick="loadReport('4')">
		        <div class="icon"></div>
		        <span class="text">Data Edit Details</span>
		    </a>
		</div>
   
   <div id="En" class = "environmentstats hide" >
    <p class="info">
  		<strong>Notes :</strong>Please be patient, it may take a few minutes to prepare the report.
    </p>
   <div  style="width: 95%;" >
            <table class="tblEnvironmentStats"  style="width: 99%;" >
                <tr>
                  <asp:UpdatePanel ID="UpdatePanel1" runat="server">  
	               <ContentTemplate>
                    <td class="lLabel">Vendor Name</td>
                    <td class="txtLContainer">
                        <asp:DropDownList ID="ddlVendor" runat="server" AutoPostBack="True" OnClientClick="return false;" CssClass="ddlVendor ddlLSide txtAlign dropFiled"></asp:DropDownList></td>

                    <td class="RLabel">Environment Name</td>
                    <td class="txtRContainer">
                        <asp:DropDownList ID="ddlEnvironment" runat="server" AutoPostBack="false" CssClass="ddlEnvironment ddlRSide txtAlign dropFiled"></asp:DropDownList></td>
                
                        </ContentTemplate>
                   </asp:UpdatePanel>
                </tr>                
                <tr class="datestats">
                    <td class="lLabel">From Date</td>
                    <td class="txtContainer">
                        <input type="date" id="txtFromDate" name="FromDate" runat="server" class="txtLSide txtFromDate" /></td>
                    <td class="RLabel">To Date</td>
                    <td class="txtRContainer">
                        <input type="date" id="txtToDate" name="FromDate" runat="server" class="txtRSide txtToDate" /></td>
                </tr>
                <tr class="datestats">
                	<td class="RAction">Action</td>
                	<td class="ActionContainer">
                		<asp:DropDownList ID="ddlAction" runat="server" AutoPostBack="false" CssClass="ddlAction ddlLSide txtAlign dropFiled">
                		 	<asp:ListItem Enabled="true" Text="Select Action" Value=""></asp:ListItem>
    						<asp:ListItem Text="New" Value="New"></asp:ListItem>
    						<asp:ListItem Text="Installed" Value="Installed"></asp:ListItem>
    						<asp:ListItem Text="Revoked" Value="Revoked"></asp:ListItem>
    						<asp:ListItem Text="Deleted" Value="Deleted"></asp:ListItem>
    				   </asp:DropDownList></td>
                </tr>
            </table>
        </div>
        
        	<div style="margin-top: 1%;width:100%;float:left;">
            <asp:Button ID="btnGenerate" CssClass="btnGenerate" runat="server" OnClientClick="return searchValidation(); " Text="Generate" />
        	</div>
        </div>

        <div id="StatsReport"  runat="server" visible="false" style="width: 99%;">
    <div style="float:left;height:auto;width: 100%;">
            <div style="width: 100%; min-height: 356px; float: left;">
            <h4>Device License Reports</h4>
            <div style="font-weight: bold; width: 100%;float: left;">
                <span id="PageCountDeviceLicense" runat="server"></span>
                <asp:Button ID="btnDLSDownload"  CssClass="btnDownload" runat="server" Text="Download" />
            </div>
            <asp:GridView ID="gvDeviceLicense" AutoGenerateColumns="false" runat="server" ShowHeaderWhenEmpty="True" EmptyDataText="No record found" Width="100%" AllowPaging="true" OnPageIndexChanging="gvDeviceLicensePageChanging" PageSize="10">
                <Columns>	
                	<asp:BoundField DataField="VendorName" HeaderText="Vendor" />
                	<asp:BoundField DataField="OrganizationName" HeaderText="Organization" />
                	<asp:BoundField DataField="Common" HeaderText="Common" />
                    <asp:BoundField DataField="Console" HeaderText="Console" />
                    <asp:BoundField DataField="iPad" HeaderText="iPad" />
                    <asp:BoundField DataField="MAChrome" HeaderText="MAChrome" />
                    <asp:BoundField DataField="MACommon" HeaderText="MACommon" />
                </Columns>
            </asp:GridView>
        </div>
        </div>
        </div>
        <div id="OrganizationReport"  runat="server" visible="false" style="width: 99%;">
        <div style="float:left;height:auto;width: 100%;">
        <div style="width: 100%; min-height: 356px; float: right;">
            <h4>Organization</h4>
            <div style="font-weight: bold; width: 100%;float: left;">
                <span id="PageCountTotalMACbyUser" runat="server"></span>
                 <asp:Button ID="btnUserCount"  CssClass="btnDownload" runat="server" Text="Download" />
            </div>
            <asp:GridView ID="gvTotalCount"  runat="server" AutoGenerateColumns="False" CellPadding="4" ShowHeaderWhenEmpty="True" EmptyDataText="No record found"  AllowPaging="true" OnPageIndexChanging="gvTotalCountPageChanging" PageSize="10" Width="100%">
                <Columns>
                	
                	<asp:BoundField DataField="OrganizationName" HeaderText="Organization" SortExpression="Organization" />
                    <asp:BoundField DataField="ParcelCount" HeaderText="Count Of Parcels" SortExpression="Count Of Parcels" />
                    <asp:BoundField DataField="AssignmentGroup" HeaderText="Assignment Groups" SortExpression="Assignment Groups" />
                    <asp:BoundField DataField="UserCount" HeaderText="Users" SortExpression="Users" />
                    <asp:TemplateField HeaderText="SyncImages/UnSyncImages" >
                    	<ItemTemplate>
                    		<%# string.concat(Eval("SyncImages"),"/",Eval("UnSyncImages")) %>
                    	</ItemTemplate>
                   	</asp:TemplateField>
                   	<asp:BoundField DataField="TotalPhotos" HeaderText="Total Photos" SortExpression="Total Photos" />
                </Columns>
            </asp:GridView>
        </div>
        </div>
    </div>
    <div id="AuditReport"  runat="server" visible="false" style="width: 99%;">
    <div style="float:left;height:auto;width: 100%;">
        <div style="width: 100%; min-height: 356px; float: left;">
            <h4>License Audit Trail</h4>
            <div style="font-weight: bold; width: 100%;float: left;">
                <span id="PageCountPhotosTakenMA" runat="server"></span>
                 <asp:Button ID="btnAuditReport"  CssClass="btnDownload" runat="server" Text="Download" />
            </div>
            <asp:GridView ID="LicenseAudit" AutoGenerateColumns="false" runat="server" ShowHeaderWhenEmpty="True" EmptyDataText="No record found" Width="100%" AllowPaging="true" OnPageIndexChanging="LicenseAuditPageChanging" PageSize="10">
                <Columns>
                	<asp:BoundField DataField="VendorName" HeaderText="Vendor" />
                    <asp:BoundField DataField="OrganizationName" HeaderText="Organization" />
                    <asp:BoundField DataField="EventTime" HeaderText="EventTime" />
                    <asp:BoundField DataField="LicenseKey" HeaderText="LicenseKey" />
                    <asp:BoundField DataField="Action" HeaderText="Action" />
                </Columns>
            </asp:GridView>
        	</div>
     	</div>
    </div>
  </asp:Content>