﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/ClientAdmin.master" AutoEventWireup="false" Inherits="CAMACloud.Admin.clientaccounts_Search" Codebehind="Search.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .search-panel {
            padding:7px;
            border:1px solid #CFCFCF;
            width: 95.3%;
            background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#CFCFCF), to(#F8F8F8)); 
        }
        
        .search-panel label {
            margin-right:5px;
        }
        
        .ui-autocomplete {
			overflow: auto;
			max-height: 420px;
		}
		
		.mGrid .empty-row-template TD {
            text-align: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1>Search Client Accounts</h1>
    <div class="search-panel">
        <table style="width: 100%;">
            <tr>
                <td style="width: 60%;">
                    <label>County Name:</label>
                    <asp:TextBox runat="server" ID="txtName" Width="300px" MaxLength="30" CssClass="textboxAuto" />
                    <asp:Button runat="server" ID="btnSearch" Text=" Search " />
                </td>
                <td style="text-align: right;">
                    <asp:ImageButton ID="ibtnExportToExcel" ImageUrl="~/App_Static/images/excel.png" Width="28px" Height="25px" Style="margin-right: 3px;" ToolTip="Export To Excel" runat="server" />
                </td>
            </tr>
        </table>
    </div>

    <asp:GridView runat="server" ID="results" Width="97%" AllowPaging="true" PageSize="20">
        <Columns>
            <asp:TemplateField HeaderText="County/Organization - Click to select">
                <ItemTemplate>
                    <asp:LinkButton runat="server" ID="lbSelect" Text='<%# Eval("Name") %>' CommandName="SelectAccount" CommandArgument='<%# Eval("UID") %>' Font-Size="9pt" Font-Bold="true" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="City" HeaderText="City" ItemStyle-Width="140px" />
            <asp:BoundField DataField="County" HeaderText="County" ItemStyle-Width="170px" />
            <asp:BoundField DataField="State" HeaderText="State" ItemStyle-Width="60px" />
            <asp:BoundField DataField="CreatedDate" HeaderText="CreatedDate" DataFormatString="{0:MM-dd-yyyy hh:mm tt}" ItemStyle-Width="140px" />
            <asp:BoundField DataField="KeyCount" HeaderText="Key Count" ItemStyle-Width="75px" ItemStyle-HorizontalAlign="Center" />
        </Columns>
        <EmptyDataTemplate>
                <label style="color: Red; font-weight: bold">No Results Found !</label>
        </EmptyDataTemplate>
    </asp:GridView>

    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script language="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtName.ClientID%>').autocomplete({
                source: function (request, response) {
                    var term = request.term.replace(/[\'\\]/g, "");
                    if (!term) { return false; }
                    $.ajax({
                        url: "Search.aspx/Getcounty",
                        data: "{ 'pre':'" + term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    value: item
                              }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(JSON.stringify(XMLHttpRequest));
                        }
                    });
                },
            });

        });
    </script>
</asp:Content>

