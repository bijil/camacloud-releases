﻿Imports System.IO
Public Class Reports
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
        	
            dllBindVendor()
            dllEnvironment()
        Else
        	Dim Type As String= reportType.Value
        	ViewState("Type") = Type
        	RunScript("loadReport('"+Type+"');")
        	
        End If
    End Sub
  
    Private Sub dllBindVendor()
        Dim dtVendor As DataTable = Database.System.GetDataTable("SELECT  Id, Name FROM Vendor ORDER BY Name")
        If dtVendor.Rows.Count > 0 Then
            ddlVendor.FillFromTable(dtVendor, True, "--- Select Vendor ---")
        End If
    End Sub

    Private Sub dllEnvironment(Optional ByVal vendorId As String = "")
        Dim dtEnvironment As DataTable = Database.System.GetDataTable("SELECT id,Name FROM Organization WHERE " & IIf(vendorId = "", "1=1", "vendorId =" + vendorId) & " ORDER BY Name")
        If dtEnvironment.Rows.Count > 0 Then
            ddlEnvironment.FillFromTable(dtEnvironment, True, "--- ALL ---")

        Else
            ddlEnvironment.FillFromTable(dtEnvironment, True, "--- No Environments ---")
        End If
    End Sub
    Private Sub ddlVendor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlVendor.SelectedIndexChanged
        dllEnvironment(ddlVendor.SelectedValue)
        StatsReport.Visible = False
        OrganizationReport.Visible = False
        AuditReport.Visible = False
    End Sub

    Protected Sub btnGenerate_Click(sender As Object, e As EventArgs) Handles btnGenerate.Click
        LicenseAudit.PageIndex = 0
        gvDeviceLicense.PageIndex = 0
        gvTotalCount.PageIndex = 0
        
        loadDeviceLicense()

    End Sub

    Private Sub loadDeviceLicense() 'Function for Data grid bindings
        Dim vendorId As String = ddlVendor.SelectedValue
        Dim orgId As String = ddlEnvironment.SelectedValue
        Dim FromDate As String = txtFromDate.Value
        Dim ToDate As String = txtToDate.Value
        Dim Action As String = ddlAction.SelectedValue
       	Dim Rtype As String = ViewState("Type")
       	If Rtype = 1 Then
       		Dim dsEnvStats As DataTable = Database.System.GetDataTable("EXEC report_Environmentdetails {0},{1}".SqlFormatString(vendorId,orgId))
       		ViewState("EnvironmentStats") = dsEnvStats
       		OrganizationReport.Visible = True
       		gvTotalCount.PageIndex = 0
        	BindTotalCount(dsEnvStats)
        End If
        
        If Rtype = 2 Then
        	Dim dsEnvStats As DataTable = Database.System.GetDataTable("EXEC reports_LicenseCount {0},{1}".SqlFormatString(vendorId, orgId))
        	ViewState("EnvironmentStats") = dsEnvStats
        	StatsReport.Visible = True
        	gvDeviceLicense.PageIndex = 0
        	BindDeviceLicense(dsEnvStats)
        End If
        
        If Rtype = 3 Then
        	Dim dsEnvStats As DataTable = Database.System.GetDataTable("EXEC reports_LicenseAudit {0},{1},{2},{3},{4}".SqlFormatString(vendorId,orgId,FromDate,ToDate,Action))
        	ViewState("EnvironmentStats") = dsEnvStats
        	AuditReport.Visible = True
        	LicenseAudit.PageIndex = 0
        	BindLicenseAudit(dsEnvStats)
        End If
        
    End Sub
    Public Sub BindDeviceLicense(ByVal dt As DataTable)
        gvDeviceLicense.DataSource = dt
        gvDeviceLicense.DataBind()
        If gvDeviceLicense.Rows.Count > 0 Then
            PageCountDeviceLicense.InnerHtml = "Displaying Records {0} to {1} of {2}".FormatString(gvDeviceLicense.PageIndex * gvDeviceLicense.PageSize + 1, gvDeviceLicense.PageIndex * gvDeviceLicense.PageSize + gvDeviceLicense.Rows.Count, dt.Rows.Count)
        	btnDLSDownload.Visible = True
        Else
            PageCountDeviceLicense.InnerHtml = "&nbsp"
            btnDLSDownload.Visible = False
        End If
    End Sub

    Public Sub BindTotalCount(ByVal dt As DataTable)
        gvTotalCount.DataSource = dt
        gvTotalCount.DataBind()
        If gvTotalCount.Rows.Count > 0 Then
            PageCountTotalMACbyUser.InnerHtml = "Displaying Records {0} to {1} of {2}".FormatString(gvTotalCount.PageIndex * gvTotalCount.PageSize + 1, gvTotalCount.PageIndex * gvTotalCount.PageSize + gvTotalCount.Rows.Count, dt.Rows.Count)
        	btnUserCount.Visible = True
        Else
            PageCountTotalMACbyUser.InnerHtml = "&nbsp"
            btnUserCount.Visible = False
        End If
        Dim RowSpan As Integer = 2
        For i As Integer = gvTotalCount.Rows.Count - 2 To 0 Step -1
            Dim currRow As GridViewRow = gvTotalCount.Rows(i)
            Dim prevRow As GridViewRow = gvTotalCount.Rows(i + 1)
            If currRow.Cells(0).Text = prevRow.Cells(0).Text Then
                currRow.Cells(0).RowSpan = RowSpan
                prevRow.Cells(0).Visible = False
                RowSpan += 1
            Else
                RowSpan = 2
            End If
        Next

    End Sub

    Public Sub BindLicenseAudit(ByVal dt As DataTable)
        LicenseAudit.DataSource = dt
        LicenseAudit.DataBind()
        If LicenseAudit.Rows.Count > 0 Then
            PageCountPhotosTakenMA.InnerHtml = "Displaying Records {0} to {1} of {2}".FormatString(LicenseAudit.PageIndex * LicenseAudit.PageSize + 1, LicenseAudit.PageIndex * LicenseAudit.PageSize + LicenseAudit.Rows.Count, dt.Rows.Count)
        	btnAuditReport.Visible = True
        Else
            PageCountPhotosTakenMA.InnerHtml = "&nbsp"
            btnAuditReport.Visible = False
        End If
    End Sub

    
    Protected Sub gvDeviceLicensePageChanging(sender As Object, e As GridViewPageEventArgs)
        gvDeviceLicense.PageIndex = e.NewPageIndex
        Dim ds As DataTable = ViewState("EnvironmentStats")
        BindDeviceLicense(ds)
    End Sub


    Protected Sub gvTotalCountPageChanging(sender As Object, e As GridViewPageEventArgs)
        gvTotalCount.PageIndex = e.NewPageIndex
        Dim ds As DataTable = ViewState("EnvironmentStats")
        BindTotalCount(ds)
    End Sub

    Protected Sub LicenseAuditPageChanging(sender As Object, e As GridViewPageEventArgs)
        LicenseAudit.PageIndex = e.NewPageIndex
        Dim ds As DataTable = ViewState("EnvironmentStats")
        BindLicenseAudit(ds)
    End Sub
    
    Public Function ExportGridView(ByVal dt As DataTable, Optional ByVal formatColumn As String = Nothing, Optional ByVal format As String = Nothing,Optional ByVal removeColumn As String = Nothing,Optional ByVal formatCell As Boolean = false) As GridView
		'Create a dummy GridView
		Dim GridView1 As New GridView()
		GridView1.AllowPaging = False
		GridView1.AutoGenerateColumns = False
		Dim bfieldName As String
		Dim bHeaderText As String
		GridView1.Columns.Clear()
		For Each column As DataColumn In dt.Columns
		    bfieldName = column.ColumnName
		    bHeaderText = column.ColumnName
		    Dim bfield As New BoundField()
		    bfield.HeaderText = bHeaderText
		    bfield.DataField = bfieldName
		    If (column.ColumnName = formatColumn) Then
		        bfield.DataFormatString = format
		    End If
		    If (column.ColumnName <> removeColumn) Then
		        GridView1.Columns.Add(bfield)
		    End If
		Next
		GridView1.DataSource = Nothing
		GridView1.DataSource = dt
		GridView1.DataBind()
		If formatCell= True Then
			Dim RowSpan As Integer = 2
	        For i As Integer = GridView1.Rows.Count - 2 To 0 Step -1
	            Dim currRow As GridViewRow = GridView1.Rows(i)
	            Dim prevRow As GridViewRow = GridView1.Rows(i + 1)
	            If currRow.Cells(0).Text = prevRow.Cells(0).Text Then
	                currRow.Cells(0).RowSpan = RowSpan
	                prevRow.Cells(0).Visible = False
	                RowSpan += 1
	            Else
	                RowSpan = 2
	            End If
	        Next
		End If

		Return GridView1
	End Function
	Protected Sub btnDLSDownload_Click(sender As Object, e As EventArgs) Handles btnDLSDownload.Click
        Dim ds As DataTable = ViewState("EnvironmentStats")
        Dim GridView1 As GridView = ExportGridView(ds)
        ExportToExcel(GridView1, "DeviceLicenseStats.xls")
    End Sub
    Protected Sub btnUserCount_Click(sender As Object, e As EventArgs) Handles btnUserCount.Click
		Dim ds As DataTable = ViewState("EnvironmentStats")
        Dim GridView1 As GridView = ExportGridView(ds,,,"OrgId",true)
        ExportToExcel(GridView1, "TotalCount.xls")
    End Sub
    
    Protected Sub btnAuditReport_Click(sender As Object, e As EventArgs) Handles btnAuditReport.Click
        Dim ds As DataTable = ViewState("EnvironmentStats")
        Dim GridView1 As GridView = ExportGridView(ds,,,"OrgId")
        ExportToExcel(GridView1, "LicenseAudit.xls")
    End Sub
    
 
    Public Sub ExportToExcel(GridView1 As GridView, ByVal filename As String)
        Response.Clear()
        Response.Buffer = True
        Response.ClearContent()
        Response.ClearHeaders()
        Response.AddHeader("Content-Disposition", "attachment;filename=" + filename)
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)
        GridView1.RenderControl(hw)
        ''style to format numbers to string
        'Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"
        'Response.Write(style)
        Response.Output.Write(sw.ToString())
        Response.Flush()
        Response.End()
    End Sub
    
    End Class