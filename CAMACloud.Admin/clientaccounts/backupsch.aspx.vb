﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Web.Services

Public Class backupsch
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            bindDropdownList()
            ''LoadGrid()
            ddlServer.SelectedIndex = 0
            ScheduleDay()
            ScheduleTime()
            RepeatFactor()
        End If
    End Sub

    'Sub LoadGrid()
    '    Dim dtBackupSchedule As New DataTable
    '    dtBackupSchedule = Database.System.GetDataTable("SELECT  BackupId,RepeatFactor,ScheduleDay,ScheduleTime FROM BackupSchedule WHERE OrganizationId = 1334")
    '    grid.DataSource = dtBackupSchedule
    '    grid.DataBind()
    'End Sub
    Sub bindDropdownList()
        ddlServer.FillFromSqlWithDatabase(Database.System, "SELECT Host, Name FROM [dbo].[DatabaseServer] WHERE NAME <> 'DBServer-I'", True)
    End Sub

    <WebMethod>
    Sub CountyName()
        ddlCountyName.FillFromSqlWithDatabase(Database.System, "SELECT PrivateIP, Name FROM [dbo].[DatabaseServer]", True)
    End Sub
    'Private Sub ddlServer_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlServer.SelectedIndexChanged
    '    ddlCountyName.FillFromSqlWithDatabase(Database.System, "SELECT ID, Name As Name FROM Organization where DBHost='" + ddlServer.SelectedItem.Value + "'", True)
    'End Sub
    Sub ScheduleDay()
        ddlScheduleDay.InsertItem("Monday", "Monday", 0)
        ddlScheduleDay.InsertItem("Tuesday", "Tuesday", 1)
        ddlScheduleDay.InsertItem("Wednesday", "Wednesday", 2)
        ddlScheduleDay.InsertItem("Thursday", "Thursday", 3)
        ddlScheduleDay.InsertItem("Friday", "Friday", 4)
        ddlScheduleDay.InsertItem("Saturday", "Saturday", 5)
    End Sub
    Sub ScheduleTime()
        ddlScheduleTime.InsertItem("----Select-----", "----Select-----", 0)
        ddlScheduleTime.InsertItem("00:00", "00:00", 1)
        ddlScheduleTime.InsertItem("06:00", "06:00", 2)
        ddlScheduleTime.InsertItem("12:00", "12:00", 3)
        ddlScheduleTime.InsertItem("23:00", "23:00", 4)
    End Sub

    Sub RepeatFactor()
        ddlRepeatfactor.InsertItem("----Select-----", "----Select-----", 0)
        ddlRepeatfactor.InsertItem("Every", "Every", 1)
        ddlRepeatfactor.InsertItem("First", "First", 2)
        ddlRepeatfactor.InsertItem("Second", "Second", 3)
        ddlRepeatfactor.InsertItem("Third", "Third", 4)
        ddlRepeatfactor.InsertItem("Fourth", "Fourth", 5)
        ddlRepeatfactor.InsertItem("Last", "Last", 6)
    End Sub

    <System.Web.Services.WebMethod()>
    Public Shared Function saveBackup(ByVal server As String, ByVal county As String, ByVal bkDays As String, ByVal bkTime As String, ByVal repeat As String) As String
        Dim sql As String = "DELETE b FROM BackupSchedule b JOIN Organization o ON b.OrganizationId = o.Id JOIN DatabaseServer d ON b.ServerId = d.Id"
        Dim conditions As String = " WHERE o.Name NOT LIKE '%sample%' AND o.Name NOT LIKE '%CCMODEL%'"
        Dim asql As String = "INSERT INTO [AdminSettingsAuditTrail](EventTime, LoginId, County, Description ) VALUES (GETUTCDATE(), '" + Membership.GetUser().ToString() + "', 'Backup Schedule', "
        If server <> "" Then
            conditions += " AND o.DBHost = '" + server + "'"
            If county <> "" Then
                conditions += " AND o.Id IN (" + county + ")"
                asql += "'New backup schedule updated in enviornments, Organization Ids : " + county
            Else
                asql += "'New backup schedule updated in (" + server + ") server enviornments"
            End If
        Else
            asql += "'New backup schedule updated in all enviornments"
        End If

        asql += ", BackupSchedule days: " + bkDays + ", Backup Time: " + bkTime + ", Repeact: " + repeat + "');"
        sql += conditions
        For Each d In bkDays.Split(New Char() {","c})
            sql += "; INSERT INTO BackupSchedule(ServerId, OrganizationId, DatabaseName, RepeatFactor, ScheduleDay, ScheduleTime)"
            sql += " SELECT d.Id, o.Id, o.DBName, '" + repeat + "', '" + d + "', '" + bkTime + "' FROM Organization o JOIN DatabaseServer d ON o.DBHost = d.Host"
            sql += conditions
        Next
        sql += "; " + asql
        Database.System.Execute(sql)
        Return "Sucess"
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function getCounties() As JSONTable
        Dim dt As DataTable = Database.System.GetDataTable("SELECT ID, Name As Name, DBHost FROM Organization WHERE Name NOT LIKE '%sample%' AND Name NOT LIKE '%CCMODEL%'")
        Return dt.ConvertToList()
    End Function

End Class

