﻿Imports System.IO
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports System.Net.Mail


Partial Class clientaccounts_ClientPage
    Inherits System.Web.UI.Page
    Dim dbstatus As String
    Public Enum AccountStatus
        Pending
        Active
        NewCounty
    End Enum

    Public Shared AppState As AccountStatus


    Sub SetFormTitle()
        If hdnOrgname.Value = "" Then
            lblMainTitle.Text = "Account - Settings"
        Else
            lblMainTitle.Text = "Account Settings - " + hdnOrgname.Value
        End If
    End Sub
  
    Private Sub LoadRolesList()
        Dim dtOrgRoles As DataTable = Database.System.GetDataTable("SELECT Name, Id FROM AppRoles WHERE ID IN (SELECT RoleId FROM OrganizationRoles )")
        cblRoles.DataSource = dtOrgRoles
        cblRoles.DataTextField = "Name"
        cblRoles.DataValueField = "Id"
        cblRoles.DataBind()
    End Sub
    Private Sub LoadOrganizationDetailsForEdit(orgId As String)

    End Sub

    'Protected Sub drpServer_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpServer.SelectedIndexChanged
    'Dim dtserver As DataTable = Database.System.GetDataTable("Select HostName,DBUser,DBPassword from DatabaseInfo where id=" + drpServer.SelectedItem.Value + "")
    'If (dtserver.Rows.Count > 0) Then
    '    DrpHost.Items.Add(dtserver.Rows(0)("HostName").ToString())
    '    txtUserName.Text = dtserver.Rows(0)("DBUser").ToString()
    '    txtPwd.Text = dtserver.Rows(0)("DBPassword").ToString()
    'End If
    'End Sub
    Private Sub ClearForm()
        'hdnOrgid.Value = ""
        txtorgname.Text = String.Empty
        txtcity.Text = String.Empty
        txtcounty.Text = String.Empty
        txtstate.Text = String.Empty
        txtDbName.Text = String.Empty
        txtUserName.Text = String.Empty
        txtPwd.Text = String.Empty
        drpServer.SelectedIndex = 0
        DrpHost.SelectedIndex=0
        For Each li As ListItem In cblRoles.Items
            li.Selected = False
        Next
        '    '' DrpHost.Items.re= String.Empty
    End Sub
    Sub AssignRolesFromList(ByVal Orgid As String)
        For Each li As ListItem In cblRoles.Items
            If li.Selected Then
                If Not IsOrganizationInRole(Orgid, li.Value) Then
                    AddOrgToRole(Orgid, li.Value)
                End If
            Else
                If IsOrganizationInRole(Orgid, li.Value) Then
                    RemoveOrganizationFromRole(Orgid, li.Value)
                End If
            End If
        Next
    End Sub
    Private Sub RemoveOrganizationFromRole(ByVal orgid As String, ByVal roleid As String)
        Dim sql As String = String.Format("DELETE FROM OrganizationRoles WHERE OrganizationId={0} AND RoleId={1}", orgid, roleid)
        Database.System.Execute(sql)
    End Sub
    Private Sub LoadClientDetailsForEdit()

        LoadRolesList()
        If hdnOrgid.Value <> "" Then

            Dim dt As DataTable = Database.System.GetDataTable("SELECT * FROM Organization where Id=" + hdnOrgid.Value + "")
            If (dt.Rows.Count > 0) Then
                txtorgname.Text = dt.Rows(0)("Name").ToString()
                txtcounty.Text = dt.Rows(0)("county").ToString()
                txtstate.Text = dt.Rows(0)("State").ToString()
                txtcity.Text = dt.Rows(0)("city").ToString()
                dbstatus = dt.Rows(0)("status").ToString()
                If (dbstatus = "pending") Then
                    GenerateDbName(txtorgname.Text)
                    txtDbName.Enabled = True
                Else
                    txtDbName.Text = dt.Rows(0)("DBName").ToString()
                    'txtDbName.Enabled = False
                    drpServer.SelectedIndex = drpServer.Items.IndexOf(drpServer.Items.FindByText(dt.Rows(0)("DBHost").ToString()))
                    If dt.Rows(0)("CCHost").ToString() <> "" Or IsDBNull(dt.Rows(0)("CCHost").ToString()) = False Then
                        DrpHost.Items.Add(dt.Rows(0)("CCHost").ToString())
                    End If
                    txtUserName.Text = dt.Rows(0)("DBUser").ToString()
                    txtPwd.Text = dt.Rows(0)("DBPassword").ToString()
                    txtDbName.ReadOnly = True
                End If


            End If
            For Each li As ListItem In cblRoles.Items
                If IsOrganizationInRole(hdnOrgid.Value, li.Value) Then
                    li.Selected = True
                Else
                    li.Selected = False
                End If
            Next
            lblalert.Text = "Updated changes successfully.."
        Else
            ClearForm()
        End If
        GetAccountStatus()
    End Sub
  
    Protected Sub btnSaveAccount_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAccount.Click
        SaveOrganization()
        hdnOrgname.Value = txtorgname.Text
        Alert("Account details saved successfully")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadGrid()
            LoadHost()
            LoadServerList()
            LoadClientDetailsForEdit()
            SetFormTitle()
            ddlNumber.FillNumbers(1, 10)
        End If
    End Sub

    Protected Sub results_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)

    End Sub

    Protected Sub results_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles results.PageIndexChanging
        Dim pageindex As Integer = e.NewPageIndex
        LoadGrid(pageindex)
    End Sub


    Private Sub btnClear_Click(sender As Object, e As System.EventArgs) Handles btnClear.Click
        hdnOrgid.Value = ""
        hdnOrgname.Value = ""
        LoadClientDetailsForEdit()
    End Sub

    Protected Sub btnServerConfig_Click(sender As Object, e As EventArgs) Handles btnServerConfig.Click

        If AppState = AccountStatus.Pending Then
            If (isDbExists(txtDbName.Text)) Then
                Alert("Database '" + txtDbName.Text + "' already exists. Choose a different database name.")
                Return
            End If

            If (BackUpAndRestoreDatabase("backup")) Then
                If (BackUpAndRestoreDatabase("restore")) Then
                    System.Threading.Thread.Sleep(1000)
                    Alert("Server configuration completed successfully")
                    Database.System.Execute("update Organization set DBName='" + txtDbName.Text + "',DBUser='" + IIf(txtUserName.Text = "", DBNull.Value, txtUserName.Text) + "' ,DBPassword='" + IIf(txtPwd.Text = "", DBNull.Value, txtPwd.Text) + "',DBHost='" + IIf(drpServer.SelectedItem.Value = "0", "NULL", drpServer.SelectedItem.Text) + "',CCHost='" + IIf(DrpHost.SelectedItem.Value = "0", "NULL", DrpHost.SelectedItem.Text) + "',DBUser='" + IIf(txtUserName.Text = "", "NULL", txtUserName.Text) + "',DBPassword='" + IIf(txtPwd.Text = "", "NULL", txtPwd.Text) + "',status='Active' where id='" + hdnOrgid.Value + "'")
                End If
            End If
        End If
        If AppState = AccountStatus.Active Then
            System.Threading.Thread.Sleep(1000)
            Database.System.Execute("update Organization set DBName='" + txtDbName.Text + "',DBUser='" + IIf(txtUserName.Text = "", DBNull.Value, txtUserName.Text) + "' ,DBPassword='" + IIf(txtPwd.Text = "", DBNull.Value, txtPwd.Text) + "',DBHost='" + IIf(drpServer.SelectedItem.Value = "0", "NULL", drpServer.SelectedItem.Text) + "',CCHost='" + IIf(DrpHost.SelectedItem.Value = "0", "NULL", DrpHost.SelectedItem.Text) + "',DBUser='" + IIf(txtUserName.Text = "", "NULL", txtUserName.Text) + "',DBPassword='" + IIf(txtPwd.Text = "", "NULL", txtPwd.Text) + "',status='Active' where id='" + hdnOrgid.Value + "'")
            Alert("Configuration details updated successfully")
        End If
        If AppState = AccountStatus.NewCounty Then
            Alert("Complete registration process before config server.")
        End If

    End Sub

    Private Function BackUpAndRestoreDatabase(ByVal keyword As String)
        Try
            Database.Tenant.Execute("BackUpRestoreDatabase {0}".SqlFormatString(txtDbName.Text))
            Return True
        Catch ex As Exception
            Alert(ex.Message)
            Return False
        End Try
    End Function

    Protected Sub lbSelect_Command(sender As Object, e As System.Web.UI.WebControls.CommandEventArgs)
        hdnOrgid.Value = e.CommandArgument.ToString()
        hdnOrgname.Value = Database.System.GetStringValue("SELECT NAME FROM Organization WHERE Id=" + hdnOrgid.Value.ToSqlValue)
        LoadClientDetailsForEdit()
        SetFormTitle()
    End Sub

    Private Sub imagActive_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imagActive.Click
        LoadGrid(0, " WHERE status='Active'")
    End Sub

    Private Sub imgPending_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imgPending.Click
        LoadGrid(0, " WHERE status='Pending'")
    End Sub

    Private Sub imgAll_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imgAll.Click
        LoadGrid()
    End Sub

    Private Sub btnRoles_Click(sender As Object, e As System.EventArgs) Handles btnRoles.Click
        If hdnOrgid.Value <> "" Then
            AssignRolesFromList(hdnOrgid.Value)
            Alert("'" + hdnOrgname.Value + "'  Roles updated successfully.")
        Else
            Alert("Create organization before assign roles.")
        End If
    End Sub

    Private Sub imgSearch_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imgSearch.Click
        If txtSearch.Text <> "" Then
            LoadGrid(0, " WHERE Name LIKE '%{0}%' OR County LIKE '%{0}%'".FormatString(txtSearch.Text.Trim.ToSqlValue.Trim("'")))
        End If
    End Sub

    Private Sub btnEmail_Click(sender As Object, e As System.EventArgs) Handles btnEmail.Click

        If (GetAccountStatus() = "Active") Then
            If txtEmail.Text.Trim = "" Then
                Return
            End If
            Try
                Dim ta As New MailAddress(txtEmail.Text)
            Catch ex As Exception
                Alert("Invalid email address.")
                Return
            End Try
            generateLicense()
            Dim mail As New MailMessage
            mail.From = New MailAddress(ClientSettings.DefaultSender, "Access@CAMACloud")

            mail.To.Add(txtEmail.Text)
            '  mail.Bcc.Add("consultsarath@gmail.com")
            '  mail.Bcc.Add("consultsarath@gmail.com")
            mail.Subject = Now.ToString("yyyyMMdd\HHHmmss") + " : CAMA Cloud License Keys for " + hdnOrgname.Value
            mail.Body = composeEmail()
            mail.IsBodyHtml = True

            AWSMailer.SendMail(mail)
            Alert("An email has been sent to " + txtEmail.Text + " with the selected license keys and their OTPs.")
        Else
            Alert("Inactive client account")
        End If
    End Sub

    Protected Sub Imgedit_Command(sender As Object, e As System.Web.UI.WebControls.CommandEventArgs)
        hdnOrgid.Value = e.CommandArgument.ToString()
        hdnOrgname.Value = Database.System.GetStringValue("SELECT NAME FROM Organization WHERE Id=" + hdnOrgid.Value.ToSqlValue)
        LoadClientDetailsForEdit()
        SetFormTitle()
        'RunScript("showPopup('Edit Organization-','.newaccount',this);")
    End Sub

    Protected Sub Imglicense_Command(sender As Object, e As System.Web.UI.WebControls.CommandEventArgs)
        'hdnOrgid.Value = e.CommandArgument.ToString()
        'hdnOrgname.Value = Database.System.GetStringValue("SELECT NAME FROM Organization WHERE Id=" + hdnOrgid.Value.ToSqlValue)
        'LoadClientDetailsForEdit()
        'SetFormTitle()
        Response.Redirect("~/license/?ouid=" + e.CommandArgument)
    End Sub

    Sub SaveOrganization()
        Dim sql As String
        If hdnOrgid.Value = "" Then
            dbstatus = "pending"
            sql = "insert into Organization(Name,City,County,State,status) values ({1}, {2},{3},{4},{5}); select cast(@@identity as int) As NeewId;"
            hdnOrgid.Value = Database.System.GetIntegerValue(sql.SqlFormat(False, hdnOrgid, txtorgname, txtcity, txtcounty, txtstate, dbstatus))
        Else
            sql = "update Organization set Name={1},city={2},County={3},State={4} Where Id = {0};SELECT {0} As Id;"
            hdnOrgid.Value = Database.System.GetIntegerValue(sql.SqlFormat(False, hdnOrgid, txtorgname, txtcity, txtcounty, txtstate))
        End If
        LoadClientDetailsForEdit()
        'ClearForm()
    End Sub

    Function GetAccountStatus() As String
        Dim dbstatus = Database.System.GetStringValue("select status from Organization where id='" + hdnOrgid.Value + "'")
        Select Case dbstatus.ToLower()
            Case ""
                AppState = AccountStatus.NewCounty
            Case "pending"
                AppState = AccountStatus.Pending
            Case "active"
                AppState = AccountStatus.Active
        End Select
        Return dbstatus
    End Function

    Sub GenerateDbName(ByVal Name As String)
        Dim s As String() = Name.Split(New Char() {" "c})
        If (s.Length > 1) Then
            txtDbName.Text = s(0)
        Else
            txtDbName.Text = Name
        End If
    End Sub

    Function IsOrganizationInRole(ByVal orgid As String, ByVal rolid As String) As Boolean
        Dim dt As DataTable = Database.System.GetDataTable("SELECT * FROM OrganizationRoles WHERE OrganizationId=" + orgid + " AND Roleid=" + rolid + "")
        If (dt.Rows.Count > 0) Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub AddOrgToRole(ByVal orgid As String, ByVal roleid As String)
        Dim sql As String = String.Format("INSERT INTO OrganizationRoles (OrganizationId,RoleId) VALUES({0},{1})", orgid, roleid)
        Database.System.Execute(sql)
    End Sub
    'NAN----License Functions Begin

    Sub LicenseTable(ByVal licensekey As String, ByVal otp As String)
        Dim dtLicense As New DataTable()
        If (ViewState("dtLicense") Is Nothing) Then
            dtLicense.Columns.Add("LicenseKey")
            dtLicense.Columns.Add("OTP")
            Dim dr = dtLicense.NewRow()
            dr("LicenseKey") = licensekey
            dr("OTP") = otp
            dtLicense.Rows.Add(dr)
        Else
            dtLicense = ViewState("dtLicense")
            Dim dr = dtLicense.NewRow()
            dr("LicenseKey") = licensekey
            dr("OTP") = otp
            dtLicense.Rows.Add(dr)
        End If
        ViewState("dtLicense") = dtLicense
    End Sub


    Private Function RandomString(ByVal size As Integer) As String
        Dim builder As New StringBuilder()
        Dim licenseKey As String
        Dim random As New Random()
        Dim ch As Char
        Dim i As Integer
        For i = 0 To size - 1
            ch = Convert.ToChar(Convert.ToInt32((25 * random.NextDouble() + 65)))
            builder.Append(ch)
        Next
        licenseKey = builder.ToString()
        Dim sql As String = String.Format("SELECT count(*) FROM DeviceLicense WHERE LicenseKey = '{0}'", licenseKey)
        If (Database.System.GetIntegerValue(sql) > 0) Then
            licenseKey = RandomString(16)
        End If

        Return licenseKey
    End Function
    Private Function RandomNumber() As Integer
        Dim random As New Random()
        Dim otp = random.Next(100000, 999999)
        Return otp
    End Function

    Private Sub generateLicense()
        If hdnOrgid.Value <> "" Then
            For i = 1 To CInt(ddlNumber.SelectedValue)
                Dim license As String = RandomString(16)
                Dim otp As Integer = RandomNumber()
                Dim sql As String = String.Format("INSERT INTO DeviceLicense (OrganizationId,LicenseKey,OTP,CreatedDate) VALUES({0},'{1}',{2},'{3}')", hdnOrgid.Value, license, otp, Date.UtcNow.ToString("yyyy-MM-dd HH:mm:ss"))
                Database.System.Execute(sql)
                LicenseTable(license, otp)
            Next
        Else
            Alert("Invalid attempt to create license before registering organization.")
        End If
    End Sub

    'NAN----License Functions End

    Sub LoadGrid(Optional ByVal pageIndex As Integer = 0, Optional ByVal sqlFilter As String = "")
        results.PageIndex = pageIndex
        results.DataSource = Database.System.GetDataTable("SELECT * FROM Organization" + sqlFilter)
        results.DataBind()
    End Sub

    Private Sub LoadHost()
        Dim dthost As DataTable = Database.System.GetDataTable("Select id,HostName from HostSettings")
        DrpHost.DataSource = dthost
        DrpHost.DataTextField = "HostName"
        DrpHost.DataValueField = "id"
        DrpHost.DataBind()
        DrpHost.InsertItem("--Select--", "0", 0)
    End Sub

    Private Sub LoadServerList()
        Dim dtserver As DataTable = Database.System.GetDataTable("Select id,DbServer from DatabaseConfig")
        drpServer.DataSource = dtserver
        drpServer.DataTextField = "DbServer"
        drpServer.DataValueField = "id"
        drpServer.DataBind()
        drpServer.InsertItem("--Select--", "0", 0)
    End Sub

    Private Function isDbExists(ByVal name As String)
        If (Database.System.GetIntegerValue("SELECT Count(*) FROM master.dbo.sysdatabases WHERE ('[' + name + ']' ={0} OR name ={0})".SqlFormatString(name)) > 0) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function composeEmail() As String
        Dim mailContent As XElement = <html>
                                          <body>
                                              <div style="font-family:Arial;width:600px;">
                                                  <h1 style="border-bottom:1px solid #075297;color:#075297;">CAMA Cloud - Access License</h1>
                                                  <h2></h2>
                                                  <p style="font-size:10pt;">Greetings from CAMA Cloud,</p>
                                                  <p style="font-size:10pt;">The following license keys can be used to access CAMA Cloud applications for <b>Organization</b>.</p>
                                                  <table style="font-size:10pt;font-family:Arial;">
                                                      <thead>
                                                          <tr>
                                                              <td style='font-weight:bold;width:200px;'>License Key</td>
                                                              <td style='font-weight:bold;width:80px;'>OTP</td>
                                                          </tr>
                                                      </thead>
                                                      <tbody>

                                                      </tbody>
                                                  </table>
                                                  <p style="font-size:10pt;">The OTP or One-Time-Password can be used only for authenticating a new device/browser. This implies that each license/OTP pair is valid to be used on one target device only.</p>
                                                  <p style="font-size:10pt;">Sincererly, <br/>CAMA Cloud</p>
                                                  <hr/>
                                                  <p style="font-size:8pt;margin-top:0px;">
                                                      <a href="http://www.camacloud.com/">CAMA Cloud<sup style="font-size:6pt;">&#174;</sup></a> powered by <a href="http://www.datacloudsolutions.net/">Data Cloud Solutions, LLC.</a>
                                                  </p>
                                              </div>
                                          </body>
                                      </html>

        Dim dataFormat As XElement = <tr>
                                         <td></td>
                                         <td></td>
                                     </tr>

        mailContent...<body>...<div>...<p>(1)...<b>.Value = hdnOrgname.Value
        mailContent...<body>...<div>...<h2>.Value = hdnOrgname.Value
        Dim dtLicense As DataTable = ViewState("dtLicense")
        For Each dr In dtLicense.Rows
            Dim dataLine As XElement = XElement.Parse(dataFormat.ToString)
            dataLine...<td>(0).Value = dr("LicenseKey")
            dataLine...<td>(1).Value = dr("OTP")
            mailContent...<body>...<div>...<table>...<tbody>(0).Add(dataLine)
        Next
        Return mailContent.ToString
    End Function

End Class

