﻿Imports System.Web.Services
Imports System.Web.Script.Services
Imports System.Data.SqlClient
Imports System.IO

Partial Class clientaccounts_Search
    Inherits System.Web.UI.Page

    ReadOnly Property PageAction As String
        Get
            Dim sPageAction As String = Coalesce(Request("pageaction"), "")
            Select Case sPageAction.ToLower
                Case "license"
                    Return "license"
                Case "edit"
                    Return "edit"
                Case "appaccess"
                    Return "appaccess"
                Case Else
                    Return "edit"
            End Select
        End Get
    End Property

    Sub LoadGrid(Optional ByVal pageIndex As Integer = 0)
        Dim sqlFilter As String = ""
        If txtName.Text.Trim <> "" Then
            sqlFilter = " WHERE Name LIKE '%{0}%' OR County LIKE '%{0}%'".FormatString(txtName.Text.Trim.ToSqlValue.Trim("'"))
        End If
        Dim Sql As String
        Dim dt As DataTable = Nothing
        Select Case PageAction
            Case "license"
                Sql = " SELECT o.Id,o.UID,o.Name,o.City,o.County,o.State,o.CreatedDate, count(d.id) AS KeyCount  FROM   Organization as o LEFT JOIN DeviceLicense as d on o.Id=d.OrganizationId AND  d.IsSuper=0 AND d.IsVendor=0 " + sqlFilter + " GROUP BY o.Id, d.OrganizationId,o.UID,o.Name,o.City,o.County,o.State,o.CreatedDate ORDER BY Name"
                dt = Database.System.GetDataTable(Sql)
                ViewState("dtDeviceLicense") = dt
            Case "appaccess"
                Sql = "SELECT o.Id,o.UID,o.Name,o.City,o.County,o.State,o.CreatedDate, count(a.id) AS KeyCount  FROM   Organization as o LEFT JOIN ApplicationAccess as a on o.Id=a.OrganizationId " + sqlFilter + " GROUP BY o.Id, a.OrganizationId,o.UID,o.Name,o.City,o.County,o.State,o.CreatedDate ORDER BY Name"
                dt = Database.System.GetDataTable(Sql)
                ViewState("dtApplicationAccess") = dt
        End Select
        results.PageIndex = pageIndex
        results.DataSource = dt
        results.DataBind()
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadGrid()
        End If
    End Sub

    Protected Sub results_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles results.PageIndexChanging
        LoadGrid(e.NewPageIndex)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        LoadGrid()
    End Sub

    Protected Sub results_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles results.RowCommand
        Select Case e.CommandName
            Case "SelectAccount"
                Select Case PageAction
                    Case "license"
                        Response.Redirect("~/license/?ouid=" + e.CommandArgument)
                    Case "edit"
                        Response.Redirect("~/clientaccounts/edit.aspx?ouid=" + e.CommandArgument)
                    Case "appaccess"
                        Response.Redirect("~/license/applicationaccess.aspx?ouid=" + e.CommandArgument)
                End Select
        End Select
    End Sub

    <WebMethod()>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Shared Function Getcounty(pre As String) As List(Of String)
        Dim Counties As New List(Of String)()
        Dim conn As New SqlConnection
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ControlDB").ConnectionString
        Dim cmd As New SqlCommand()
        cmd.CommandText = "select Name, Id from Organization where " & "Name like @SearchText + '%'"

        cmd.Parameters.AddWithValue("@SearchText", pre)
        cmd.Connection = conn
        conn.Open()
        Dim sdr As SqlDataReader = cmd.ExecuteReader()
        While sdr.Read()
            Counties.Add(sdr("Name"))
        End While
        conn.Close()
        Return Counties
    End Function
    Protected Sub ibtnExportToExcel_Click(sender As Object, e As System.EventArgs) Handles ibtnExportToExcel.Click
        Dim dt As DataTable = Nothing
        Dim headers As New Dictionary(Of String, String)()
        headers.Add("Name", "Name")
        headers.Add("City", "City")
        headers.Add("County", "County")
        headers.Add("State", "State")
        headers.Add("CreatedDate", "Created Date")
        Select Case PageAction
            Case "license"
                dt = ViewState("dtDeviceLicense")
            Case "appaccess"
                dt = ViewState("dtApplicationAccess")
        End Select
		dt.Columns.Remove("Id")
        dt.Columns.Remove("UID")
        Dim gridView As New GridView()
        For Each keyvalue As KeyValuePair(Of String, String) In headers
            Dim gridfield As New BoundField()
            gridfield.DataField = keyvalue.Key
            gridfield.HeaderText = keyvalue.Value

            Select Case keyvalue.Key
                Case "City"
                    gridfield.ItemStyle.Width = 140
                Case "County"
                    gridfield.ItemStyle.Width = 170
                Case "State"
                    gridfield.ItemStyle.Width = 60
                Case "CreatedDate"
                    gridfield.ItemStyle.Width = 160
                Case "KeyCount"
                    gridfield.ItemStyle.Width = 75
                Case "Name"
                    gridfield.ItemStyle.Width = 200
            End Select
            gridView.Columns.Add(gridfield)
        Next
        gridView.DataSource = dt
        gridView.DataBind()
        Dim fileName As String = "DeviceLicenseList.xlsx"
        Response.Clear()
        Response.Buffer = True
        Response.ClearContent()
        Response.ClearHeaders()
        Response.AddHeader("Content-Disposition", "attachment;filename=" + fileName)
        Response.Charset = ""
        Response.ContentType = "application/vnd.openxml.formats-officedocument.spreadsheetml.sheet"
        Dim excelStream = ExcelGenerator.ExportGrid(gridView, dt, "Device License List", "Device License List")
        excelStream.CopyTo(Response.OutputStream)
        Response.End()
    End Sub

End Class
