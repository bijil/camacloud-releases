﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/ClientAdmin.master" AutoEventWireup="false" Inherits="CAMACloud.Admin.clientaccounts_Account" Codebehind="Default.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
  
    <style type="text/css">

	.vtop
	{
		border-spacing: 0px;
	}
	
	</style>
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1>Client Account Details-Dashboard
    </h1>

<table style="border-spacing: 0px; width: 100%;">
	<tr>
		<td style="width: 50%; vertical-align: top;">
			<h2>
				License Status</h2>
			<table>
				<tr>
		 <td>
          <asp:GridView runat="server" ID="grdlicense" Width="97%" AllowPaging="true" PageSize="20">
        <Columns>
            <asp:BoundField DataField="Name" HeaderText="County/Organization" ItemStyle-Width="200px" />
            <asp:BoundField DataField="UsedLicense" HeaderText="Used License" ItemStyle-Width="140px" />
            <asp:BoundField DataField="UnusedLicense" HeaderText="Unused License" ItemStyle-Width="140px" />
       </Columns>
    </asp:GridView>
		</td>
	</tr>
    <tr><td style="vertical-align: top; width: 50%;">
							<h3>
								Account activated today:</h3>
							<asp:Repeater runat="server" ID="rpToday">
								<ItemTemplate>
									<span style="white-space:nowrap;"><strong><%# Eval("Name")%></strong>
									</span>
								</ItemTemplate>
								<SeparatorTemplate>
									,&nbsp;
								</SeparatorTemplate>
							</asp:Repeater>
							<asp:Label runat="server" ID="lblNoneToday" Text="None" />
							<h3>
								Account activated last day:</h3>
							<asp:Repeater runat="server" ID="rpLast">
								<ItemTemplate>
									<span style="white-space:nowrap;"><strong><%# Eval("Name")%></strong>
								</span>
								</ItemTemplate>
								<SeparatorTemplate>
									,&nbsp;
								</SeparatorTemplate>
							</asp:Repeater>
							<asp:Label runat="server" ID="lblNoneLast" Text="None" />
						</td><td></td><td></td></tr>
</table>
</td>
</tr> 
</table> 

</asp:Content>

