﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class manageclient

    '''<summary>
    '''hdnOrganizationName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnOrganizationName As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hdnOrganizationId control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnOrganizationId As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''HyperLink1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents HyperLink1 As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''clientTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents clientTitle As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''txtName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtName As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtCity control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCity As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtCounty control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCounty As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtState control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtState As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''ddlHostServer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlHostServer As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtMAHostKey control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtMAHostKey As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtMaxMAParcels control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtMaxMAParcels As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSave As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''gridModules control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents gridModules As Global.System.Web.UI.WebControls.GridView
End Class
