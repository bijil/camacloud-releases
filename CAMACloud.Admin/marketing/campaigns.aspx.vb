﻿
Partial Class marketing_campaigns
    Inherits System.Web.UI.Page

	Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
		If Not IsPostBack Then
			bindCampaigns()
			bindOrganizations()
		End If
	End Sub

	Private Sub bindOrganizations()
		ddlOrganization.FillFromSqlWithDatabase(Database.System, "SELECT Id, Name FROM Organization ORDER BY Name", True)
	End Sub

	Private Sub bindCampaigns()
		camps.DataSource = Database.System.GetDataTable("SELECT ig.Id, ig.UID, ig.Name, o.Name As OrganizationName, ig.StartDate, ig.EndDate FROM InvitationGroup ig LEFT OUTER JOIN Organization o ON ig.OrganizationId = o.Id ORDER BY StartDate")
		camps.DataBind()
	End Sub

	Protected Sub btnCreate_Click(sender As Object, e As System.EventArgs) Handles btnCreate.Click
		Dim compexp As Date = DateTime.UtcNow.AddDays(ddlPeriod.SelectedValue)
		Dim sql As String = "INSERT INTO InvitationGroup (Name, OrganizationId, ExpirationDays, EndDate) VALUES ({0}, {1}, {2}, {3});SELECT CAST(@@IDENTITY AS INT);".SqlFormat(True, txtName, ddlOrganization, ddlExpiration, compexp)
		Dim igid As Integer = Database.System.GetIntegerValue(sql)
		Dim iguid As String = Database.System.GetStringValue("SELECT LOWER(CAST(UID AS VARCHAR(50))) FROM InvitationGroup WHERE Id = " & igid)
		Response.Redirect("invitees.aspx?uid=" & iguid)
	End Sub
End Class
