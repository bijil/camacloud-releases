﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/ClientAdmin.master"
	AutoEventWireup="false" Inherits="CAMACloud.Admin.marketing_campaigns" Codebehind="campaigns.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<h1>
		Marketing Campaigns - Grant Demo Licenses</h1>
	<asp:Panel runat="server" ID="pnlNew" Visible="true">
		<table>
			<tr>
				<td>Campaign Name: </td>
				<td>
					<asp:TextBox runat="server" ID="txtName" Width="250px" />
					<asp:RequiredFieldValidator runat="server" ControlToValidate="txtName" ErrorMessage="*" ValidationGroup="CreateNew" />
				</td>
			</tr>
			<tr>
				<td>Organization: </td>
				<td>
					<asp:DropDownList ID="ddlOrganization" runat="server" Width="256px" />
					<asp:RequiredFieldValidator  runat="server" ControlToValidate="ddlOrganization" ErrorMessage="*" ValidationGroup="CreateNew"  />
				</td>
			</tr>
			<tr>
				<td style="width: 120px;">Campaign Period: </td>
				<td>
					<asp:DropDownList ID="ddlPeriod" runat="server" Width="256px">
						<asp:ListItem Value="" Text="-- Select --" />
						<asp:ListItem Value="3" Text="3 days" />
						<asp:ListItem Value="7" Text="1 week" />
						<asp:ListItem Value="15" Text="2 weeks" />
						<asp:ListItem Value="30" Text="1 month" />
						<asp:ListItem Value="60" Text="2 months" />
						<asp:ListItem Value="90" Text="3 months" />
					</asp:DropDownList>
					<asp:RequiredFieldValidator  runat="server" ControlToValidate="ddlPeriod" ErrorMessage="*" ValidationGroup="CreateNew" />
				</td>
			</tr>
			<tr>
				<td>User Expiration: </td>
				<td>
					<asp:DropDownList ID="ddlExpiration" runat="server" Width="256px">
						<asp:ListItem Value="" Text="-- Select --" />
						<asp:ListItem Value="3" Text="3 days" />
						<asp:ListItem Value="7" Text="1 week" />
						<asp:ListItem Value="15" Text="2 weeks" />
						<asp:ListItem Value="30" Text="1 month" />
					</asp:DropDownList>
					<asp:RequiredFieldValidator  runat="server" ControlToValidate="ddlExpiration" ErrorMessage="*" ValidationGroup="CreateNew" />
				</td>
			</tr>
		</table>
		<asp:Button runat="server" ID="btnCreate" Text="Create Campaign & Continue" ValidationGroup="CreateNew" />
	</asp:Panel>
	<asp:GridView runat="server" ID="camps">
		<Columns>
			<asp:BoundField DataField="Name" HeaderText="Campaign Name" ItemStyle-Width="200px" />
			<asp:BoundField DataField="OrganizationName" HeaderText="Organization" ItemStyle-Width="180px" />
			<asp:BoundField DataField="EndDate" HeaderText="Valid Until" ItemStyle-Width="90px"
				DataFormatString="{0:MMM d, yyyy}" />
			<asp:HyperLinkField DataTextField="Id" DataTextFormatString="View/Edit" DataNavigateUrlFields="UID"
				DataNavigateUrlFormatString="invitees.aspx?uid={0}" ItemStyle-Width="90px" />
		</Columns>
	</asp:GridView>
</asp:Content>
