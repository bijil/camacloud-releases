﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/ClientAdmin.master" AutoEventWireup="false" Inherits="CAMACloud.Admin.marketing_invitees" Codebehind="invitees.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
	<h1>Campaign Details - Manage Invitees</h1>
	<p style="font-size:larger;font-weight:bold;"><asp:HyperLink runat="server" ID="lblLink" Target="_blank" /></p>
	<div>
		<p>Add invitees to the campaign by entering one or more email addresses into this text area. The email addresses can be added either line by line, or separated by comma or space.</p>
		<p>
			<asp:TextBox runat="server" ID="txtEmails" TextMode="MultiLine" Rows="12" Columns="90" />
			<br />
			<asp:Button runat="server" ID="btnAdd" Text="Click here to add invitees" />
		</p>
	</div>
	<asp:Panel runat="server" ScrollBars="Vertical" Height="300px" >
		<asp:GridView runat="server" ID="grid" AllowPaging="true" PageSize="20">
			<Columns>
				<asp:BoundField DataField="Email" HeaderText="Email" ItemStyle-Width="190px" />
				<asp:BoundField DataField="Name" HeaderText="Invitee Name" ItemStyle-Width="170px" /> 
				<asp:BoundField DataField="AcceptedDate" HeaderText="Accepted" DataFormatString="{0:MMM d, yyyy}" ItemStyle-Width="90px" />
				<asp:BoundField DataField="Phone" HeaderText="Phone" ItemStyle-Width="120px" /> 
				<asp:BoundField DataField="Jurisdiction" HeaderText="Jurisdiction" ItemStyle-Width="120px" /> 
				<asp:BoundField DataField="State" HeaderText="State" ItemStyle-Width="120px" /> 
			</Columns>
		</asp:GridView>	
	</asp:Panel>
</asp:Content>

