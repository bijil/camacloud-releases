

<!DOCTYPE html>
<html>
<head><title>
	CAMA Cloud&#174; - Mass Appraisal made Easy!
</title>
    
    <script type="text/javascript" src="/App_Static/jslib/jquery.js"></script>
    <script type="text/javascript" src="/App_Static/jslib/jquery.cookies.js"></script>
    <script type="text/javascript" src="/App_Static/jslib/jquery-ui-1.8.20.custom.min.js"></script>
    <script type="text/javascript" src="/App_Static/js/system.js"></script>
    <script type="text/javascript" src="/App_Static/js/layout.js?2"></script>
    
 <style type="text/css">
		.masklayer {
		    background: black;
		    opacity: 0.5;
		    top: 0%;
		    width: 100%;
		    height: 100%;
		    bottom: 0%;
		    position: fixed;
		    overflow:hidden;
		    display: none;
		}
    </style>
    <script type="text/javascript">
        function showMask(info) {
			document.body.style.overflow = 'hidden';        
		    $('.masklayer').height($(document).height())
		    $('.masklayer').show();		    
		        info = 'Please wait ...'
		}
    </script> 
    <link rel="Stylesheet" href="/App_Static/css/datasetup.css" />
    <script type="text/javascript" src="/App_Static/js/datasetup.js"></script>
    

    <link href="/App_Static/css/datatables/jquery.dataTables.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src = "/App_Static/js/datatables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src = "/App_Static/js/datatables/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src = "/App_Static/js/datatables/buttons.html5.min.js"></script>
    <script type="text/javascript" src = "/App_Static/js/datatables/moment.min.js"></script>
    <script type="text/javascript" src="/App_Static/jslib/jszip.min.js"></script>

    
    <script>
        $(function () {
            $.ajax({
                type: "POST",
                url: "EnvironmentAuditTrail.aspx/GetsatReport",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccess,
                failure: function (response) {
                    alert(response.d);
                },
                error: function (response) {
                    alert(response.d);
                }
            });
        });
        function OnSuccess(response) {
            $("[id*=gvSettingsAuditTrail]").DataTable(
                {
                    bLengthChange: true,
                    "pageLength": 20,
                    lengthMenu: [[10, 20, 50, 100, 250], [10, 20, 50, 100, 250]],
                    bFilter: true,
                    bSort: true,
                    bPaginate: true,
                    data: response.d,
                    "order" : [0,'desc'],
                    columns: [
                        {
                            'data': 'Id',
                            'searchable': false,  
                            'sortable': false,
                             visible: false
           
                        },
                        {
                            'data': 'EventTime',
                            'searchable': false,
                             width: '20%',
                            'sortable': false,
                            "render": function (data, type, row) { return moment(data).format("DD/MM/YYYY hh:mm:ss A"); }                // to format the data in required format            
                        },
                        {
                            'data': 'LoginId',
                            'searchable': true,  // to set if the column should appear in seach
                            'sortable': false,    // to set sort button on the column header
                             width: '15%'
                        },
                        {
                            'data': 'County',
                            'searchable': true,  // to set if the column should appear in seach
                            'sortable': false,    // to set sort button on the column header
                            width: '25%'
                        },
                        {
                            'data': 'Description',
                            'sortable': false
                        }],

                    dom: '<"top"<"left-col"l><"right-col"Bf>>rtip',      // to set the order of buttons, searchbar, info etc.
                    buttons: [{
                        extend: 'excel',
                        text: 'Export to Excel',
                        className: 'btn btn-default',
                        exportOptions: {
                            columns: 'th:not(:first)'
                        },
                        filename: 'Settings Audit Trail Report'
                    },]
                  
                });
        };

    </script>

<style type="text/css">

.left-col {
    float: left;
    width: 25%;
}

 
.right-col {
    float: right;
}

table.dataTable tbody td {
  padding: 5px 9px;
  
}

.dataTables_filter  {
     margin-top: 20px;
     margin-bottom: 20px;
}

.buttons-excel {
        float: right;
        
}

.break {
	word-break: break-all;
}

.hiddencol
  {
    display: none;
  }

</style>




    <script type="text/javascript">

        $(window).ready(function () {
           $('#lbLogout').click(function(){
             localStorage.setItem('sideState',0);
             localStorage.setItem($('#accordion').attr('name') + '-accordion', 0);
             });
            setScreenDimensions();
            var a = $('#accordion')
            a.each(function () {
                var aa = localStorage.getItem($(this).attr('name') + '-accordion');
                if (aa == null)
                    aa = 0;
                $(this).accordion({
                    active: parseInt(aa),
                    beforeActivate: function (event, ui) {
                        var index = $(this).children('h3').index(ui.newHeader);
                        localStorage.setItem($(event.target).attr('name') + '-accordion', index);
                    },
                    change: function (event, ui) {
                        var index = $(this).children('h3').index(ui.newHeader);
                        localStorage.setItem($(event.target).attr('name') + '-accordion', index);
                    }
                });
            });
            if ($('.report-accordians').length == 1) {
                a.accordion({
                    active: 0
                });
            }
           
        });

        $(window).resize(function () {
            setScreenDimensions();
        });
    </script>
    <script type="text/javascript">
    	var is_ca=false;
    	$( document ).ready(function() {
    		if(window.location.href.indexOf("admin.ca.camacloud") > -1){
    			is_ca=true;
    			$('#switchca').html("Switch to US");
    		}
			else {
				is_ca=false;
				$('#switchca').html("Switch to CA");
			}
			if(localStorage.getItem('sideState') == 1) hideSidePanel();
			
		});
    function callfunct(){
		if(!is_ca)
			window.location.href="https://admin.ca.camacloud.com/clientaccounts/list.aspx";
		else
			window.location.href ="https://admin.camacloud.com/clientaccounts/list.aspx";
	}
	function hideSidePanel() {
		    $('.left-panel').hide();
		    $('.hideSidePanel').hide();
		    $('.left-panel2').show();
		    $('.showSidePanel').show();
		    localStorage.setItem('sideState',1)
		    setScreenDimensions();
		}
	function showSidePanel() {
		    $('.showSidePanel').hide();
		    $('.left-panel2').hide();
		    $('.hideSidePanel').show();
		    $('.left-panel').show();
		    localStorage.setItem('sideState',0)
			setScreenDimensions();		    
		}
    </script>
    <style>
       .hideSidePanel {
			background: url(/App_Static/images/close.png);
		    background-size: 107%;
		    background-repeat: no-repeat;
		    cursor: pointer;
		    float: right;
		    width: 24px;
		    height: 26px;
		    opacity: 0.6;
		 }
		 
		.hideSidePanel:hover{
		 	opacity: 1;
		}
        .left-panel2 {
			width: 4px;
			background-repeat: repeat;
			background-image: url(/App_Themes/Cloud/images/left-col-bottom-repeat.png);
			min-width: 4px;
			opacity: 0.7;
			display:none;
		}
       .showSidePanel {
		    background: url(/App_Static/images/imgopen.png);
		    background-size: 82%;
		    background-repeat: no-repeat;
		    cursor: pointer;
		    display:none;
		    float: left;
		    width: 33px;
		    height: 29px;
		    z-index: 19;
		    top: 50%;
		    bottom : 50%;
		    left:2px;
		    position : fixed;
		    margin-left: -1px;
		    background-position-x: 4px;
		    background-position-y: 1px;
		    opacity: 0.7;
		}
		
	    .showSidePanel:hover {
		 	opacity: 1;
		}		
    </style>
<link href="../App_Themes/Cloud/00-layout.css" type="text/css" rel="stylesheet" /><link href="../App_Themes/Cloud/01-mnemonics.css" type="text/css" rel="stylesheet" /><link href="../App_Themes/Cloud/03-buttons.css" type="text/css" rel="stylesheet" /><link href="../App_Themes/Cloud/06-grid.css" type="text/css" rel="stylesheet" /><link href="../App_Themes/Cloud/cupertino-dark/jquery-ui-1.8.20.custom.css" type="text/css" rel="stylesheet" /><link href="../App_Themes/Cloud/PopUpStyle.css" type="text/css" rel="stylesheet" /></head>
<body>
    <form method="post" action="./EnvironmentAuditTrail.aspx" id="form1">
<div class="aspNetHidden">
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTEwNzcwNDM1NTIPFgIeE1ZhbGlkYXRlUmVxdWVzdE1vZGUCARYCZg9kFgJmD2QWAgIDD2QWBAIHD2QWAgIbD2QWBAIBDxYCHgVzdHlsZQUORGlzcGxheTpibG9jaztkAgcPFgIfAQUORGlzcGxheTpibG9jaztkAgkPZBYCAgEPZBYCAgMPZBYCAgEPPCsAEQMADxYEHgtfIURhdGFCb3VuZGceC18hSXRlbUNvdW50AgFkARAWABYAFgAMFCsAABYCZg9kFgZmDw8WAh4MVGFibGVTZWN0aW9uCyopU3lzdGVtLldlYi5VSS5XZWJDb250cm9scy5UYWJsZVJvd1NlY3Rpb24AZGQCAQ9kFgpmDw8WAh4EVGV4dAUGJm5ic3A7ZGQCAQ8PFgIfBQUGJm5ic3A7ZGQCAg8PFgIfBQUGJm5ic3A7ZGQCAw8PFgIfBQUGJm5ic3A7ZGQCBA8PFgIfBQUGJm5ic3A7ZGQCAg8PFgIeB1Zpc2libGVoZGQYAQU4Y3RsMDAkY3RsMDAkTWFpbkNvbnRlbnQkTWFpbkNvbnRlbnQkZ3ZTZXR0aW5nc0F1ZGl0VHJhaWwPPCsADAEIAgFk4oLfTeZ9ZGIVZs8dD0k3OHCgadM=" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['form1'];
if (!theForm) {
    theForm = document.form1;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=s9r3A4muQcaRLORu8XhHi7acyskIvScinfKT-QB2KXYyib9Gu95mubPovd3rmKzsbPTRvPEJh36gU_Ir1cHKJNKhqJo1&amp;t=637814660020000000" type="text/javascript"></script>


<script src="/ScriptResource.axd?d=u5nL6vF89yijGbWOoeSvLMjl96MtkiVOzw3TkK-IgEG8vUH6E3xVhNfu8wW32CfOR1AL_BKhBp6DEEQZ2NW4Dw6PWrVn-5VcV2GarXQThZ1CfHFyofXy_42DgnqsyXDuZdmLqiY3_FYK-XjJVWdXKvS_tkRX1XwsQEmDtDu9x9HzMYvJ0&amp;t=49337fe8" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
if (typeof(Sys) === 'undefined') throw new Error('ASP.NET Ajax client-side framework failed to load.');
//]]>
</script>

<script src="/ScriptResource.axd?d=bgYeEUZA3EEx1-bBgenqc7aW85sKwCZuzMgj2ZduSigcWbT4wTw8EAxfQdhhtQje6AmmIMdAquGlQ2PJpjDxPB1F9ZRV-V1D-LTEohWgH79TLYFoxU08aD9Y7mhBj9aVH5hs2JNgHgexdUq80rGJWMmoaJzaA-qAnLaQ7OuEZTJwxLvwRkxaxJjAqqPLtp9ufpI5dg2&amp;t=49337fe8" type="text/javascript"></script>
<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="C85D1F90" />
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdAAIiA76U9d/PCs78Dz98GxZgZKiK31S9rRPcT642qZ5Atc9xzM3ZedPSPAGOItB8WKcAISog" />
</div>
    <script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ctl00$ctl00$ctl02', 'form1', [], [], [], 90, 'ctl00$ctl00');
//]]>
</script>

    <div id="topNavBar" class="min-width">
        <div>
            <table style="border-spacing: 0px; width: 100%;">
                <tr>
                    <td style="text-align: left; font-weight: bold;">
                        Welcome,
                        <span>admin</span>
                        <span class="pipe">|</span><a id="lbLogout" href="javascript:__doPostBack(&#39;ctl00$ctl00$lbLogout&#39;,&#39;&#39;)">Logout</a>
                        <a href="/clientaccounts/list.aspx" style="margin-left: 65%;">Admin</a>
                        <a  href="/reports/Default.aspx" style="margin-left: 24px;">Report</a>
                        <a href="#" id="switchca" onclick="callfunct();" style="margin-left: 24px;">Switch to CA</a>
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <table id="contentTable" class="min-width">
        <tr>
            <td class="left-panel2">
            <td class="left-panel">
                <div class="left-content-area">
                 <div class="hideSidePanel" onclick="hideSidePanel();"></div>
                    <div class="logo">
                    </div>
                    <div id="leftContentPanel">
                        <div style="margin: 5px 10px;">
                            
    <div id="accordion" name="parcelmgr">
        

        <h3 class="clientAccount"><a style="padding-left: 2.2em;" >Client Accounts</a></h3>
        <div class="clientAccount">
            <ul class="group-menu">
                <li>
                    <a id="LeftContent_HyperLink9" href="list.aspx">Account List</a></li>
                <li>
                    <a id="LeftContent_HyperLink11" href="OnlineUsers.aspx">Online Users</a></li>
                <li>
                    <a id="LeftContent_HyperLink15" href="../environment/Default.aspx">Environment Creation</a></li>
                <li>
                    <a id="LeftContent_HyperLink16" href="EnvironmentStats.aspx">Environment Stats</a></li>
                <li>
                    <a id="LeftContent_HyperLink20" href="EnvironmentAuditTrail.aspx">Environment Audit Trail</a></li>
            </ul>
        </div>

        <h3 class='LicenseHead license'><a style="padding-left: 2.2em;" >License</a></h3>
        <div class="license">
            <ul class="group-menu">
                <li><a id="LeftContent_hlHome" href="search.aspx?pageaction=license">License Keys</a></li>
                <li><a id="LeftContent_HyperLink4" href="../license/ApplicationAccess.aspx">API Access Keys</a></li>
                <li><a id="LeftContent_HyperLink1" href="../license/superaccess.aspx">Super Admin Keys</a></li>
                <li><a id="LeftContent_HyperLink2" href="../license/vendoraccess.aspx">Vendor Licenses</a></li>
                <li><a id="LeftContent_HyperLink10" href="../license/query.aspx">Search License</a></li>
            </ul>
        </div>

        <h3 id="LeftContent_marketHead" class="marketing"><a style="padding-left: 2.2em;" >Marketing</a></h3>
        <div id="LeftContent_marketBody" class="marketing">
            <ul class="group-menu">
                <li><a href="../marketing/campaigns.aspx">Campaigns</a></li>
                <li><a href="../marketing/invitees.aspx">Invitees</a></li>
            </ul>
        </div>

        <h3 id="LeftContent_otherSettingsHead" class="otherSettings"><a style="padding-left: 2.2em;" >Other Settings</a></h3>
        <div id="LeftContent_otherSettingsBody" class="otherSettings" style="padding-left: 2em;">
            <ul class="group-menu">
                <li id="LeftContent_hideManageUser" style="Display:block;"><a id="LeftContent_HyperLink6" href="../settings/users.aspx">Manage Users</a></li>
                <li><a id="LeftContent_hlMobileCarriers" href="../settings/mobilecarrier.aspx">Mobile Carriers</a></li>
                <li><a id="LeftContent_HyperLink3" href="../settings/licenseagreement.aspx">Mobile License Agreement</a></li>
                <li id="LeftContent_hideMassUpdate" style="Display:block;"><a id="LeftContent_HyperLink5" href="../settings/sqlexec.aspx">Mass Update Environments</a></li>
                <li><a id="LeftContent_HyperLink12" href="../settings/massupdatequeue.aspx">Mass Update - Pending</a></li>
                
            </ul>
        </div>

        <h3 id="LeftContent_maintenanceToolHead" class="maintenanceTools"><a style="padding-left: 2.2em;" >Maintenance Tools</a></h3>
        <div id="LeftContent_maintenanceToolBody" class="maintenanceTools" style="overflow: inherit; padding: 1em 2em;">
            <ul class="group-menu">
                <li><a id="LeftContent_HyperLink7" onclick="showMask(this)" href="../tools/syncerrors.aspx">DownSync Errors</a></li>
                <li><a id="LeftContent_HyperLink8" onclick="showMask()" href="../tools/masyncerrors.aspx">MA Failed Syncs</a></li>
                 <li><a id="LeftContent_HyperLink17" href="../tools/devicesyncerrors.aspx">Device Failed Syncs</a></li>
                <li id="LeftContent_hideEnvironmentCleanup"><a id="LeftContent_HyperLink13" href="../tools/masscleanup.aspx">Environment Cleanup</a></li>
                <li id="LeftContent_hideDatabaseOccupancy"><a id="LeftContent_HyperLink14" href="../tools/databaseoccupancy.aspx">Database Occupancy</a></li>
                <li id="LeftContent_OrphanRecords"><a id="LeftContent_HyperLink18" href="../tools/OrphansList.aspx">Orphaned Records</a></li>
                <li><a id="LeftContent_HyperLink19" href="../tools/Errormessagenotifications.aspx">System Error Message Notifications</a></li>
            </ul>
        </div>
    </div>

                        </div>
                    </div>
                </div>
            </td>
            <td class="panel-spacer">
            </td>
            <td>
                <div class="main-content-area">
                    
    

    <h1>Environment Settings Audit Trail Report</h1><br />

    <table Style="Width:99.5%"><tr><td></td> </tr>
    <tr><td colspan='2'  >
        
        <div id="MainContent_MainContent_pnlSettingsAuditTrail">
	
          
            <div>
		<table class="mGrid app-content" cellspacing="0" id="MainContent_MainContent_gvSettingsAuditTrail" style="border-collapse:collapse;Width:100%">
			<thead>
				<tr class="ui-state-default">
					<th class="hiddencol" scope="col">Id</th><th scope="col">Event Time</th><th scope="col">LoginId</th><th scope="col">County</th><th scope="col">Description</th>
				</tr>
			</thead><tbody>
				<tr>
					<td class="hiddencol" style="width:10px;">&nbsp;</td><td style="width:150px;">&nbsp;</td><td style="width:190px;">&nbsp;</td><td style="width:200px;">&nbsp;</td><td class="break" style="width:450px;">&nbsp;</td>
				</tr>
			</tbody>
		</table>
	</div>
        
        
</div>
        
        </td></tr></table>
    


                </div>
            </td>
        </tr>
    </table>
    <div class="showSidePanel"  onclick="showSidePanel();"></div>
    <div id="footer" class="min-width">
        
<span>v1.0.103.6036</span>
    </div>
    </form>
    <div class="masklayer" style="display: none; z-index: 20">
        <span style="margin-left: 47%; margin-top: 22%; float: left">
            <img src="/App_Static/images/comploader.gif" />
            <span style='color: wheat; float: right; margin-left: 12px; margin-top: 20px;' class="app-mask-layer-info">Please
                wait...</span></span>
    </div>
</body>
</html>
