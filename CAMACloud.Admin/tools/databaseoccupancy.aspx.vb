﻿Imports System.IO

Public Class databaseoccupancy
    Inherits System.Web.UI.Page
    Public Event ItemDataBound As RepeaterItemEventHandler
    Protected Repeater1 As System.Web.UI.WebControls.Repeater
    Protected counter As Integer = 0
    Protected columnCount As Integer = 3
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            bindDropdownList()
            PanelDisplayContent.Visible = False
        End If
    End Sub
    Private Sub bindDropdownList()
        ddlHostServer.FillFromSqlWithDatabase(Database.System, "SELECT Id,Name FROM [dbo].[DatabaseServer]", True)
    End Sub
    Protected Sub ddlHostServer_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlHostServer.SelectedIndexChanged
        Dim DBHost As String
        Dim drDBHost As DataRow = Database.System.GetTopRow("SELECT * FROM [dbo].[DatabaseServer] where Id= '" & ddlHostServer.SelectedValue & "'")
        DBHost = drDBHost.GetString("Host")
        Dim dsDBInfo As DataSet = Database.System.GetDataSet("EXEC sp_DataBaseServerInfo {0}".SqlFormatString(DBHost))
        ViewState("ViewStateDateSet") = dsDBInfo
        Dim databaseTableInUse = ConvertDataTabledtDataBaseToHTML(dsDBInfo.Tables(0))
        ph_TableInUse.Controls.Add(New Literal() With { _
               .Text = databaseTableInUse.ToString() _
             })

        Dim databaseTableNotInUse = ConvertDataTabledtDataBaseToHTML(dsDBInfo.Tables(1))

        If databaseTableNotInUse IsNot Nothing Then
            ph_TableNotInUse.Controls.Add(New Literal() With { _
               .Text = databaseTableNotInUse.ToString() _
             })

            ServerDetails()
        End If
        PanelDisplayContent.Visible = True
    End Sub
    Public Shared Function ConvertDataTabledtDataBaseToHTML(dt As DataTable) As String
        Dim htmlTableStart As String = "<table width='100%';>"
        Dim htmlTableEnd As String = "</table>"
        Dim htmlTableContent As String = String.Empty
        Dim content As String
        htmlTableContent += "<tr>"
        For i As Integer = 0 To dt.Columns.Count - 1
            htmlTableContent += "<th style='text-align:left;'>" + dt.Columns(i).ColumnName + "</th>"
        Next
        htmlTableContent += "</tr>"
        For i As Integer = 0 To dt.Rows.Count - 1
            htmlTableContent += "<tr>"
            For j As Integer = 0 To dt.Columns.Count - 1
                htmlTableContent += "<td style='text-align:left;padding-left:2px;'>" + dt.Rows(i)(j).ToString() + "</td>"
            Next
            htmlTableContent += "</tr>"
        Next
        Dim html As New StringBuilder
        html.Append(htmlTableStart)
        html.Append(htmlTableContent)
        html.Append(htmlTableEnd)
        content = html.ToString
        Return content
    End Function
    Private Sub ServerDetails()
        Dim drDBHost As DataRow = Database.System.GetTopRow("SELECT * FROM [dbo].[DatabaseServer] WHERE Id= '" & ddlHostServer.SelectedValue & "'")
        Dim HostId = drDBHost.GetString("Host").ToString()
        lblServerName.Text = drDBHost.GetString("Name").ToString()
        lblHostName.Text = drDBHost.GetString("HostName").ToString()
        If Not drDBHost.GetString("PrivateIP") = "" Then
            lblPrivateIP.Text = drDBHost.GetString("PrivateIP").ToString()
        Else
            lblPrivateIP.Text = "N/A"
        End If
        If Not drDBHost.GetString("PublicIP") = "" Then
            lblPublicIP.Text = drDBHost.GetString("PublicIP").ToString()
        Else
            lblPublicIP.Text = "N/A"
        End If
        Dim dsDBInfo As DataSet = Database.System.GetDataSet("EXEC sp_DataBaseServerInfo {0}".SqlFormatString(HostId))
        lblTotalDb.Text = dsDBInfo.Tables(2).Rows(0)(0)
        lblTotalDBSize.Text = dsDBInfo.Tables(3).Rows(0)(0)
    End Sub

    Protected Sub btnBackupServerNow_Click(sender As Object, e As EventArgs) Handles btnBackupServerNow.Click
        Dim DBHost As String
        DBHost = lblPrivateIP.Text
        Dim serverName As String
        serverName = lblServerName.Text
        serverName = serverName.Replace(" ", "")
        Dim sqlQuery As String = String.Format("EXEC [" & DBHost & "].[Master].[dbo].[sp_BackupServer] {0}".SqlFormatString(serverName))
        Database.System.Execute(sqlQuery, 6000)
        Alert("Backup Server successfully processed")
        ReloadDetails()
    End Sub

    Private Sub ReloadDetails()
        If IsPostBack Then
            Dim ds As DataSet = DirectCast(ViewState("ViewStateDateSet"), DataSet)
            Dim databaseTableInUse = ConvertDataTabledtDataBaseToHTML(ds.Tables(0))
            ph_TableInUse.Controls.Add(New Literal() With { _
                   .Text = databaseTableInUse.ToString() _
                 })
            Dim databaseTableNotInUse = ConvertDataTabledtDataBaseToHTML(ds.Tables(1))
            If databaseTableNotInUse IsNot Nothing Then
                ph_TableNotInUse.Controls.Add(New Literal() With { _
                   .Text = databaseTableNotInUse.ToString() _
                 })
                ServerDetails()
            End If
        End If
    End Sub

    Protected Sub exportcsv_Click(sender As Object, e As EventArgs) Handles exportcsv.Click
        Dim DBHost As String
        Dim drDBHost As DataRow = Database.System.GetTopRow("SELECT * FROM [dbo].[DatabaseServer] where Id= '" & ddlHostServer.SelectedValue & "'")
        DBHost = drDBHost.GetString("Host")
        Dim dsDBInfo As DataSet = Database.System.GetDataSet("EXEC sp_DataBaseServerInfo {0}".SqlFormatString(DBHost))
        Dim dtExport As New DataTable
        dtExport = dsDBInfo.Tables(4)


            Dim gvExport As New GridView()

            gvExport.AllowPaging = False
            gvExport.DataSource = dtExport
            gvExport.DataBind()

            gvExport.AllowPaging = False
           
            Response.Clear()
            Response.Buffer = True
        Response.AddHeader("content-disposition", _
             "attachment;filename=DBOCCUPANCY.xls")
            Response.Charset = ""
            Response.ContentType = "application/vnd.ms-excel"
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)

            For i As Integer = 0 To gvExport.Rows.Count - 1

                gvExport.Rows(i).Attributes.Add("class", "textmode")
            Next
            gvExport.RenderControl(hw)


            Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"
            Response.Write(style)
            Response.Output.Write(sw.ToString())
            Response.Flush()
            Response.End()

    End Sub
End Class