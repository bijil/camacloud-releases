﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.IO.Compression

Public Class bulkexport
    Inherits System.Web.UI.Page

    Function ProcessAllCounties(ByRef dt As DataTable, ByRef counties As Integer, ByRef failed As Integer) As Stream
        counties = 0
        failed = 0

        Dim success As Integer = 0

        Dim errorMessage As String = ""
        Dim query As String = txtQuery.Text.Trim
        'If Not query.ToUpper.Replace(vbCr, " ").Replace(vbLf, "").StartsWith("EXEC") Then
        '    Throw New Exception("Not a valid procedure call")
        'End If

        Dim orgSql As String = "SELECT * FROM Organization ORDER BY Name"
        If ddlVendor.SelectedIndex > 0 Then
            orgSql = "SELECT * FROM Organization WHERE VendorId = " & ddlVendor.SelectedValue & " ORDER BY Name"
        End If

        Dim orgs As DataTable = Database.System.GetDataTable(orgSql)



        dt = Database.System.GetDataTable("SELECT Name, DBName, 0 As Count FROM Organization WHERE 0 = 1 ORDER BY Name")

        Dim tempFile As String = IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile, Environment.SpecialFolderOption.Create), "CAMACloud-Admin\admin-export.zip")

        If Not IO.Directory.Exists(IO.Path.GetDirectoryName(tempFile)) Then
            IO.Directory.CreateDirectory(IO.Path.GetDirectoryName(tempFile))
        End If
        Dim fs As New FileStream(tempFile, FileMode.CreateNew)

        'Dim zipStream As New MemoryStream

        Using zip As New ZipArchive(fs, ZipArchiveMode.Create)


            For Each org As DataRow In orgs.Rows

                Dim count As Integer = -1


                Dim connStr As String = Database.GetConnectionStringFromOrganization(org.GetInteger("Id"))

                Dim dr As DataRow = dt.NewRow
                dr("Name") = org.GetString("Name")
                dr("DBName") = org.GetString("DBName")

                Try
                    Dim conn As New SqlClient.SqlConnection(connStr)
                    conn.Open()
                    Dim cmd As New SqlClient.SqlCommand(query, conn)

                    Dim da As New SqlDataAdapter(cmd)
                    Dim pdt As New DataTable
                    da.Fill(pdt)

                    If pdt.Rows.Count > 10000 And pdt.Columns.Count > 10 Then
                        Throw New Exception("Limits exceeded - Max records - 10000 for Max columns - 10")
                    End If
                    count = pdt.Rows.Count
                    If count > 0 Then
                        Dim csv As ZipArchiveEntry = zip.CreateEntry(dr("Name") + ".csv")
                        Using csvStream = csv.Open
                            Using sw As New StreamWriter(csvStream)
                                sw.Write(ExportDataTableAsCSV(pdt))
                            End Using
                        End Using
                    End If

                    conn.Close()
                    counties += 1
                    success += 1
                Catch sqlex As SqlClient.SqlException
                    errorMessage = sqlex.Message
                    count = -9
                    failed += 1
                Catch ex As Exception
                    errorMessage = ex.Message
                    count = -10
                    failed += 1
                End Try

                dr("Count") = count

                If count > 0 Then
                    dt.Rows.Add(dr)
                End If

            Next

        End Using

        lblError.Text = errorMessage

        fs = New FileStream(tempFile, FileMode.Open)
        Dim zipStream As New MemoryStream
        fs.CopyTo(zipStream)
        fs.Close()
        IO.File.Delete(tempFile)
        Return zipStream
    End Function

    Public Function ExportDataTableAsCSV(dt As DataTable) As String
        Dim sb As New StringBuilder
        Dim header As String = ""
        For Each dc As DataColumn In dt.Columns
            header.Append(dc.ColumnName, ",")
        Next
        sb.AppendLine(header)
        For Each dr As DataRow In dt.Rows
            Dim line As String = ""
            For Each dc As DataColumn In dt.Columns
                line.Append(dr.GetString(dc.ColumnName), ",")
            Next
            sb.AppendLine(line)
        Next
        Return sb.ToString
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ddlVendor.FillFromSqlWithDatabase(Database.System, "SELECT Id, Name FROM Vendor", True, "All")
        End If
    End Sub

    Private Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
        Dim dt As DataTable
        Dim counties As Integer = 0
        Dim failed As Integer = 0
        Dim zipStream = ProcessAllCounties(dt, counties, failed)
        grid.DataSource = dt
        grid.DataBind()

        Dim exportName As String = "export-" + DateTime.Now.ToString("yyyyMMdd-HHmmss")
        If txtJobName.Text <> "" Then
            exportName = txtJobName.Text.ToLower.Replace(" ", "-") + "-" + DateTime.Now.ToString("yyyyMMdd-HHmmss")
        End If
        Dim logSql As String = "INSERT INTO BulkExportLog (JobName, Query, Success, Failure, RequesterIP) VALUES ({0}, {1}, {2}, {3}, {4});".SqlFormat(False, txtJobName, txtQuery, counties - failed, failed, Request.UserHostAddress)
        Database.LogDB.Execute(logSql)

        If counties = 0 Then
            lblError.Text = "No counties found with matching conditions."
        Else
            Response.Clear()
            Response.ContentType = "application/zip"
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + exportName + ".zip")
            zipStream.Position = 0
            zipStream.CopyTo(Response.OutputStream)
            Response.Flush()
            Response.End()
        End If


    End Sub
End Class