﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/ClientAdmin.master" CodeBehind="runcounts.aspx.vb" Inherits="CAMACloud.Admin.runcounts" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Run COUNT query on all environments</h1>
    <table>
        <tr>
            <td>
                COUNT Query (e.g., SELECT COUNT(*) FROM Parcel):<br />
                <asp:TextBox runat="server" ID="txtQuery" TextMode="MultiLine" Rows="5" Columns="120" /><br />
                <asp:Button runat="server" ID="btnDisplay" Text="Show List" />&nbsp;&nbsp;
                <asp:Button runat="server" ID="btnExport" Text="Export as CSV" />&nbsp;&nbsp;
                <asp:CheckBox runat="server" ID="chkProductionOnly" Text="Production Environments only" />
                <asp:CheckBox runat="server" ID="txtExcludeZeros" Text="Exclude zeros" />
                <asp:CheckBox runat="server" ID="txtExcludeErrors" Text="Exclude errors" />
            </td>
        </tr>
    </table>

    <asp:Label runat="server" ID="lblError" ForeColor="Red" style="padding:5px 0px;display:block;" />

    <asp:GridView runat="server" ID="grid">
        <Columns>
<%--            <asp:BoundField DataField="Name" HeaderText="County" ItemStyle-Width="300px" />
            <asp:BoundField DataField="DBName" HeaderText="Database" ItemStyle-Width="140px" />
            <asp:BoundField DataField="Count" HeaderText="Count" ItemStyle-width="60px" />--%>
        </Columns>
    </asp:GridView>
</asp:Content>
