﻿Imports System.Data
Imports System.Data.SqlClient
Public Class masyncerrors
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim dt As DataTable = Database.System.GetDataTable("EXEC [Sp_MasyncFailure_Envs]")

        gvErrors.DataSource = dt
		gvErrors.DataBind()
		RunScript("$('.masklayer').hide();")
    End Sub

    Protected Sub gvErrors_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvErrors.RowCommand

        Select Case e.CommandName
            Case "SelectError"
                Response.Redirect("~/tool/errordetails.aspx?id=" + e.CommandArgument)
        End Select

    End Sub
End Class