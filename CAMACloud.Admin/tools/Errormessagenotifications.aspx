﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/ClientAdmin.master" AutoEventWireup="false" EnableEventValidation="false" 
    CodeBehind="Errormessagenotifications.aspx.vb" Inherits="CAMACloud.Admin.Errormessagenotifications"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>System Error Message Notifications</h1>

    <h3><asp:Label ID ="lblHeadText" runat="server">Add New Email</asp:Label></h3>
             <table ID="table1" runat="server">
                <tr>
                    <td class ="col1">
                        Name:
                    </td>
                    <td>
                    	<asp:HiddenField ID="hname" runat="server" Value="" />
                        <asp:TextBox runat="server" ID="txtName" Width="150px" MaxLength="50" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName"
                            ErrorMessage="*" ValidationGroup="Form" />
                    </td>
                </tr>
                <tr>
                    <td class="col1">
                        Email:
                    </td>
                    <td>
                        <asp:HiddenField ID="Hemail" runat="server" Value="" />
                        <asp:TextBox runat="server" ID="txtEmail" Type = "Email" Width="250px" MaxLength="100"/>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEmail"
                            ErrorMessage="*" ValidationGroup="Form" />
                    </td>
                 </tr>
                 <tr >
                    <td >&nbsp;</td>
                </tr>
                <tr style="position:relative">
                    <td colspan="2">
                        <asp:Button runat="server" ID="btnSave" Text=" Add " Font-Bold="true"
                            ValidationGroup="Form"  />
                        <asp:Button runat="server" ID="btnCancel" Text=" Cancel " Visible="false" />
                    </td>
                </tr>
            </table>

         <asp:GridView runat="server" ID="grid" >
                <Columns>
                    <asp:TemplateField>
                        <ItemStyle Width="25px" />
                        <ItemTemplate>
                        <%# (Container.DataItemIndex) + 1 %>.
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Name" HeaderText="Name"  ItemStyle-Width="250px"/>
                    <asp:BoundField DataField="Email" HeaderText="Email" ItemStyle-Width="400px" ItemStyle-CssClass="wbreak"/>
                    <asp:TemplateField>
                        <ItemStyle Width="80px"/>
                        <ItemTemplate>
                            <asp:LinkButton runat="server" ID="lbEdit" Text="Edit" CommandName='EditItem' CommandArgument='<%# Eval("Email") %>'  />
                            <asp:LinkButton runat="server" ID="lbDelete" Text="Delete" CommandName='DeleteItem'
                                CommandArgument='<%# Eval("Email")%>' OnClientClick='return confirm("Are you sure you want to delete?")' />
                           
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
</asp:Content>
