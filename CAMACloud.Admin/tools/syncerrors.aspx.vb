﻿Imports System.IO

Public Class syncerrors
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    	If IsPostBack = False Then    		
            gvOrganizationsBindData()
    	End If
    	runscript("hideMask();")
    End Sub
    Sub gvOrganizationsBindData()
        Dim dtErrors As New DataTable
        dtErrors.Columns.Add("VendorName")
        dtErrors.Columns.Add("Id", GetType(Integer))
        dtErrors.Columns.Add("Name")
        dtErrors.Columns.Add("CamaSystemName")
        dtErrors.Columns.Add("TotalErrors", GetType(Integer))
        Dim sql As String
        Dim sql1 As String
        If txtKey.Text.Trim <> "" Then
            sql = "SELECT COUNT(*) FROM Parcel WHERE FailedOnDownSync = 1 And DownSyncRejectReason like '%" & txtKey.Text.Trim.ToSqlValue.Trim("'") & "%'"
        Else
            sql = "SELECT COUNT(*) FROM Parcel WHERE FailedOnDownSync = 1"
        End If
        sql1 = "SELECT v.Name as VendorName,c.Name as CamaSystem, o.* from dbo.Organization o inner join dbo.CAMASystem c on o.CAMASystem = c.Id inner join dbo.Vendor v on o.VendorId = V.Id INNER JOIN DatabaseServer ds on ds.Host = o.DBHost"
        If chkPro.Checked Then
            sql1 = sql1 + " WHERE ( o.IsProduction = 'true' Or o.IsProduction = 1 ) "
        End If
        Dim failedCount As Integer
        Dim orgs As DataTable = Database.System.GetDataTable(sql1)
        For Each org As DataRow In orgs.Rows
            Dim connStr As String = Database.GetConnectionStringFromOrganization(org.GetInteger("Id"))
            Try
                Dim conn As New SqlClient.SqlConnection(connStr)
                conn.Open()
                Dim cmd As New SqlClient.SqlCommand(sql, conn)
                failedCount = cmd.ExecuteScalar()
                conn.Close()
            Catch sqlex As SqlClient.SqlException
            Catch ex As Exception
            End Try
            If failedCount > 0 Then
                Dim dr As DataRow = dtErrors.NewRow
                dr("VendorName") = org.GetString("VendorName")
                dr("Id") = org.GetInteger("Id")
                dr("Name") = org.GetString("Name")
                dr("CamaSystemName") = org.GetString("CamaSystem")
                dr("TotalErrors") = failedCount
                dtErrors.Rows.Add(dr)
            End If
        Next
        gvOrganizationsBind(dtErrors)
    End Sub
    Public Sub gvOrganizationsBind(dt As DataTable)
        Dim SortExpression As String = ddlOrderBy.SelectedValue
        dt.DefaultView.Sort = SortExpression + " ASC"
        ViewState("dtData1") = dt
        gvOrganizations.DataSource = dt
        gvOrganizations.DataBind()
        If gvOrganizations.Rows.Count > 0 Then
            Filter.Visible = True
        Else
            Filter.Visible = False
        End If
    End Sub
    Protected Sub OnParentGrid_PageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        gvOrganizations.PageIndex = e.NewPageIndex
        Dim dt As DataTable = ViewState("dtData1")
        gvOrganizationsBind(dt)
    End Sub

    Protected Sub Show_Hide_ChildGrid(sender As Object, e As EventArgs)
        Dim imgShowHide As ImageButton = TryCast(sender, ImageButton)
        Dim row As GridViewRow = TryCast(imgShowHide.NamingContainer, GridViewRow)
        If imgShowHide.CommandArgument = "Show" Then
            row.FindControl("pnlErrors").Visible = True
            imgShowHide.CommandArgument = "Hide"
            imgShowHide.ImageUrl = "~/App_Static/images/minus.png"
            Dim Id As String = gvOrganizations.DataKeys(row.RowIndex).Value.ToString()
            Dim gvOrders As GridView = TryCast(row.FindControl("gvErrors"), GridView)
            BindErrors(Id, gvOrders)
        Else
            row.FindControl("pnlErrors").Visible = False
            imgShowHide.CommandArgument = "Show"
            imgShowHide.ImageUrl = "~/App_Static/images/plus.png"
        End If
    End Sub

    Private Sub BindErrors(OrganizationId As String, gvErrors As GridView)
        gvErrors.ToolTip = OrganizationId
        Dim dr As DataRow = Database.System.GetTopRow(String.Format("SELECT DBHost,DBName FROM Organization WHERE Id ={0}", OrganizationId))
        If Not dr Is Nothing Then
            Dim sql As String
            If txtKey.Text.Trim <> "" Then
                sql = String.Format("SELECT DownSyncRejectReason,COUNT(*) AS ErrorCount FROM [{0}].[{1}].[dbo].Parcel  WHERE FailedOnDownSync = 1 And DownSyncRejectReason like '%{2}%' GROUP BY DownSyncRejectReason ORDER BY ErrorCount ", dr.GetString("DBHost"), dr.GetString("DBName"), txtKey.Text.Trim.ToSqlValue.Trim("'"))
            Else
                sql = String.Format("SELECT DownSyncRejectReason,COUNT(*) AS ErrorCount FROM [{0}].[{1}].[dbo].Parcel  WHERE FailedOnDownSync = 1 GROUP BY DownSyncRejectReason ORDER BY ErrorCount", dr.GetString("DBHost"), dr.GetString("DBName"))
            End If
            Dim dtChild As DataTable = Database.System.GetDataTable(sql)
            If dtChild.Rows.Count > 0 Then
                gvErrors.PageSize = ddlPageSize.SelectedValue
                gvErrors.DataSource = dtChild
                gvErrors.DataBind()
            End If
        End If
    End Sub
    Protected Sub OnChildGrid_PageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        Dim gvErrors As GridView = TryCast(sender, GridView)
        gvErrors.PageIndex = e.NewPageIndex
        BindErrors(gvErrors.ToolTip, gvErrors)
    End Sub
    Private Sub ibtnExportToExcel_Click(sender As Object, e As ImageClickEventArgs) Handles ibtnExportToExcel.Click
        Dim dtOrganization As DataTable = ViewState("dtData1")
        Dim dtPrint As New DataTable
        dtPrint.Columns.Add("VendorName")
        dtPrint.Columns.Add("Id", GetType(Integer))
        dtPrint.Columns.Add("Name")
        dtPrint.Columns.Add("CamaSystemName")
        dtPrint.Columns.Add("DownSyncRejectReason")
        dtPrint.Columns.Add("ErrorCount", GetType(Integer))
        dtPrint.Columns.Add("TotalErrors", GetType(Integer))
        For Each drOrganization As DataRow In dtOrganization.Rows
            Dim drOrg As DataRow = Database.System.GetTopRow(String.Format("SELECT DBHost,DBName FROM Organization WHERE Id ={0}", drOrganization.GetInteger("Id")))
            If Not drOrg Is Nothing Then
                Dim sql As String
                If txtKey.Text.Trim <> "" Then
                    sql = String.Format("SELECT DownSyncRejectReason,COUNT(*) AS ErrorCount FROM [{0}].[{1}].[dbo].Parcel  WHERE FailedOnDownSync = 1 And DownSyncRejectReason like '%{2}%' GROUP BY DownSyncRejectReason ORDER BY ErrorCount ", drOrg.GetString("DBHost"), drOrg.GetString("DBName"), txtKey.Text.Trim.ToSqlValue.Trim("'"))
                Else
                    sql = String.Format("SELECT DownSyncRejectReason,COUNT(*) AS ErrorCount FROM [{0}].[{1}].[dbo].Parcel  WHERE FailedOnDownSync = 1 GROUP BY DownSyncRejectReason ORDER BY ErrorCount", drOrg.GetString("DBHost"), drOrg.GetString("DBName"))
                End If
                Dim dtChild As DataTable = Database.System.GetDataTable(sql)
                If dtChild.Rows.Count > 0 Then
                    For i As Integer = 0 To dtChild.Rows.Count - 1
                        If i <> dtChild.Rows.Count - 1 Then
                            dtPrint.Rows.Add(drOrganization.GetString("VendorName"), drOrganization.GetInteger("Id"), drOrganization.GetString("Name"),drOrganization.GetString("CamaSystemName"), dtChild.Rows(i).GetString("DownSyncRejectReason"), dtChild.Rows(i).GetString("ErrorCount"))
                        Else
                            dtPrint.Rows.Add(drOrganization.GetString("VendorName"), drOrganization.GetInteger("Id"), drOrganization.GetString("Name"),drOrganization.GetString("CamaSystemName"), dtChild.Rows(i).GetString("DownSyncRejectReason"), dtChild.Rows(i).GetString("ErrorCount"), drOrganization.GetString("TotalErrors"))
                        End If
                    Next
                End If
            End If
        Next
        Dim SortExpression As String = ddlOrderBy.SelectedValue
        dtPrint.DefaultView.Sort = SortExpression + " ASC"
        ExportGridView(dtPrint, "SyncErrors.xls")
    End Sub
    Public Sub ExportGridView(ByVal dt As DataTable, ByVal filename As String, Optional ByVal formatColumn As String = Nothing, Optional ByVal format As String = Nothing)
        'Create a dummy GridView
        Dim GridView1 As New GridView()
        GridView1.AllowPaging = False
        GridView1.AutoGenerateColumns = False
        Dim bfieldName As String
        Dim bHeaderText As String
        GridView1.Columns.Clear()
        For Each column As DataColumn In dt.Columns
            bfieldName = column.ColumnName
            bHeaderText = column.ColumnName
            Dim bfield As New BoundField()
            bfield.HeaderText = bHeaderText
            bfield.DataField = bfieldName
            Select Case bHeaderText
                Case "VendorName"
                    bfield.HeaderText = "Vendor Name"
                    bfield.ItemStyle.Width = 150
                Case "Name"
                    bfield.ItemStyle.Width = 200
                Case "CamaSystemName"
                    bfield.HeaderText = "Cama System"
                    bfield.ItemStyle.Width = 50
                Case "DownSyncRejectReason"
                    bfield.HeaderText = "DownSync Reject Reason"
                    bfield.ItemStyle.Width = 250
                Case "ErrorCount"
                    bfield.HeaderText = "Errors"
                    bfield.ItemStyle.Width = 50
                Case "TotalErrors"
                    bfield.HeaderText = "Total Errors"
                    bfield.ItemStyle.Width = 50
            End Select

            If (column.ColumnName = formatColumn) Then
                bfield.DataFormatString = format
            End If
            GridView1.Columns.Add(bfield)
        Next
        GridView1.DataSource = Nothing
        GridView1.DataSource = dt
        GridView1.DataBind()
        Dim fName As String = "SyncErrors.xlsx"
        Response.Clear()
        Response.Buffer = True
        Response.ClearContent()
        Response.ClearHeaders()
        Response.AddHeader("Content-Disposition", "attachment;filename=" + fName)
        Response.Charset = ""
        Response.ContentType = "application/vnd.openxml.formats-officedocument.spreadsheetml.sheet"
        Dim excelStream = ExcelGenerator.ExportGrid(GridView1, dt, "Sync Errors", "SyncErrors")
        excelStream.CopyTo(Response.OutputStream)
        Response.End()
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        gvOrganizationsBindData()
    End Sub
    Protected Sub ddlPageSize_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged, ddlOrderBy.SelectedIndexChanged
        If (sender.Id = "ddlOrderBy") Then
            Dim dt As DataTable = ViewState("dtData1")
            gvOrganizationsBind(dt)
        End If
        If (sender.id = "ddlPageSize") Then
            gvOrganizations.PageSize = ddlPageSize.SelectedValue
            gvOrganizations.PageIndex = 0
            Dim dt As DataTable = ViewState("dtData1")
            gvOrganizationsBind(dt)
        End If
    End Sub

    Protected Sub chkPro_CheckedChanged(sender As Object, e As EventArgs) Handles chkPro.CheckedChanged
        gvOrganizationsBindData()
    End Sub
End Class