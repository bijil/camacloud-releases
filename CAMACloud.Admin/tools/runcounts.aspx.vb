﻿Imports System.Data.SqlClient

Public Class runcounts
    Inherits System.Web.UI.Page

    Function GetScriptOutputFromAllDatabases() As DataTable
        Dim errorMessage As String = ""
        Dim query As String = txtQuery.Text.Trim

        Dim checkQuery = query.ToUpper.Replace(vbCr, " ").Replace(vbLf, "")

        If Not checkQuery.StartsWith("SELECT COUNT(") AndAlso Not checkQuery.StartsWith("EXEC COUNT_") Then
            Throw New Exception("Not a valid COUNT query or procedure call")
        End If

        Dim orgSql As String = "SELECT * FROM Organization"
        If chkProductionOnly.Checked Then
            orgSql += " WHERE IsProduction = 1"
        End If

        Dim orgs As DataTable = Database.System.GetDataTable(orgSql)

        Dim output As DataTable = Database.System.GetDataTable("SELECT Name, DBName As [Database] FROM Organization WHERE 0 = 1 ORDER BY Name")

        Dim fieldsPopulated As Boolean = False

        For Each org As DataRow In orgs.Rows

            Dim count As Integer = -1, record As DataRow = Nothing

            Dim connStr As String = Database.GetConnectionStringFromOrganization(org.GetInteger("Id"))

            Dim dr As DataRow = output.NewRow
            dr("Name") = org.GetString("Name")
            dr("Database") = org.GetString("DBName")

            Try

                Dim conn As New SqlConnection(connStr)
                conn.Open()
                Dim cmd As New SqlCommand(query, conn)
                Dim ada As New SqlDataAdapter(cmd)
                Dim dtResult As New DataTable
                ada.Fill(dtResult)

                If dtResult.Columns.Count = 1 AndAlso dtResult.Columns(0).ColumnName = "Column1" Then
                    dtResult.Columns(0).ColumnName = "Count"
                End If

                If Not fieldsPopulated Then
                    For Each dc As DataColumn In dtResult.Columns
                        output.Columns.Add(dc.ColumnName, GetType(Integer))
                    Next

                    fieldsPopulated = True
                End If

                If dtResult.Rows.Count > 0 Then
                    record = dtResult.Rows(0)
                    For Each dc As DataColumn In dtResult.Columns
                        Dim value As Integer = record(dc)
                        count += value
                        dr(dc.ColumnName) = value
                    Next
                End If


                count = cmd.ExecuteScalar()
                conn.Close()
            Catch sqlex As SqlClient.SqlException
                errorMessage = sqlex.Message
                count = -9
            Catch ex As Exception
                errorMessage = ex.Message
                count = -10
            End Try

            'dr("Count") = count

            If count = 0 And txtExcludeZeros.Checked Then

            ElseIf count < 0 And txtExcludeErrors.Checked Then

            Else
                output.Rows.Add(dr)
            End If

        Next



        lblError.Text = errorMessage
        Return output
    End Function


    'Function GetScriptOutputFromAllDatabases() As DataTable
    '    Dim errorMessage As String = ""
    '    Dim query As String = txtQuery.Text.Trim

    '    Dim checkQuery = query.ToUpper.Replace(vbCr, " ").Replace(vbLf, "")

    '    If Not checkQuery.StartsWith("SELECT COUNT(") AndAlso Not checkQuery.StartsWith("EXEC count_") Then
    '        Throw New Exception("Not a valid COUNT query or procedure call")
    '    End If

    '    Dim orgs As DataTable = Database.System.GetDataTable("SELECT * FROM Organization")

    '    Dim dt As DataTable = Database.System.GetDataTable("SELECT Name, DBName, 0 As Count FROM Organization WHERE 0 = 1 ORDER BY Name")

    '    Dim fieldsPopulated As Boolean = False

    '    For Each org As DataRow In orgs.Rows

    '        Dim count As Integer = -1

    '        Dim connStr As String = Database.GetConnectionStringFromOrganization(org.GetInteger("Id"))

    '        Dim dr As DataRow = dt.NewRow
    '        dr("Name") = org.GetString("Name")
    '        dr("DBName") = org.GetString("DBName")

    '        Try
    '            Dim conn As New SqlClient.SqlConnection(connStr)
    '            conn.Open()
    '            Dim cmd As New SqlClient.SqlCommand(query, conn)
    '            count = cmd.ExecuteScalar()
    '            conn.Close()
    '        Catch sqlex As SqlClient.SqlException
    '            errorMessage = sqlex.Message
    '            count = -9
    '        Catch ex As Exception
    '            errorMessage = ex.Message
    '            count = -10
    '        End Try

    '        dr("Count") = count

    '        If count = 0 And txtExcludeZeros.Checked Then

    '        ElseIf count < 0 And txtExcludeErrors.Checked Then

    '        Else
    '            dt.Rows.Add(dr)
    '        End If

    '    Next
    '    lblError.Text = errorMessage
    '    Return dt
    'End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private Sub btnDisplay_Click(sender As Object, e As EventArgs) Handles btnDisplay.Click
        grid.DataSource = Nothing
        grid.AutoGenerateColumns = True

        If txtQuery.Text.Trim = "" Then
            Return
        End If

        Try
            grid.DataSource = GetScriptOutputFromAllDatabases()

        Catch ex As Exception
            Alert(ex.Message)
        End Try

        grid.DataBind()

    End Sub

    Private Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
        grid.DataSource = Nothing
        If txtQuery.Text.Trim = "" Then
            Return
        End If
        Try
            Dim csv As New StringBuilder
            Dim exportData = GetScriptOutputFromAllDatabases()

            Dim titleLine As String = "Name,Database"
            Dim columnNames As New Dictionary(Of Integer, String)
            For Each col As DataColumn In exportData.Columns
                If col.Ordinal > 1 Then
                    columnNames.Add(col.Ordinal, col.ColumnName)
                    titleLine += "," + col.ColumnName
                End If
            Next

            csv.AppendLine(titleLine)


            For Each dr As DataRow In exportData.Rows
                Dim dataLine As String = """{0}"",{1}".FormatString(dr(0), dr(1))
                For Each colIndex In columnNames.Keys
                    dataLine += "," & dr.GetString(columnNames(colIndex))
                Next
                'csv.AppendLine("""{0}"",{1},{2}".FormatString(dr(0), dr(1), dr(2)))
                csv.AppendLine(dataLine)
            Next

            Response.Clear()
            Response.ContentType = "text/csv"
            Response.AppendHeader("Content-Disposition", "attachment; filename=counts-" + DateTime.Now.ToString("yyyyMMdd-HHmmss") + ".csv")
            Response.Write(csv.ToString)
            Response.Flush()
            Response.End()
        Catch ex As Exception
            Alert(ex.Message)
        End Try
        grid.DataBind()
    End Sub
End Class