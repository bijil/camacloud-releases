﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/ClientAdmin.master" CodeBehind="databaseoccupancy.aspx.vb" Inherits="CAMACloud.Admin.databaseoccupancy" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
       <h1>Database Occupancy</h1>
      </div>

      <div id="dvContents" style="overflow:auto; width:100%;">
            <table class="comparable-edit">  
               <tr>
                 <td>Select Server</td>
                 <td><strong>:</strong></td>
                 <td>
                 <asp:DropDownList ID="ddlHostServer" runat="server" AutoPostBack="True">
                  </asp:DropDownList>
                 </td>
<%--                 <td>
                 <asp:Label ID="lblCurrentTime" runat="server" Style="font-weight: bold; font-size: 60px;
                                margin-left: 330px; color: #34AEFA;"></asp:Label>
                 </td>--%>
               </tr>  
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table>
     </div>

<asp:Panel ID= "PanelDisplayContent"  runat = "server">
<div id="dvOutput" style="overflow:auto; width:100%;">
      <table style="border-spacing: 0px; width: 100%;">
	<tbody>
    <tr>
    <td colspan="2">
        <strong>
        <asp:LinkButton ID="exportcsv" runat="server">Export DBOccupancy</asp:LinkButton>
        </strong>
        <asp:Button ID="btnBackupServerNow" runat="server" style="margin-left:5px; margin-right: 20px; float:right" Text="Backup Server" />
    </td>
    </tr>
    <tr>
		<td>
               	<h3 align="center" style="font-size: 15px;font-weight: bold;color: #000000">Database In Use</h3>
                    <div class="content_tab2">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                               <div id="Div1" style="overflow:auto; width:100%;">
                                    <asp:Panel id="pnlContentsInUse" runat = "server">
                                    <div>
                                     <asp:PlaceHolder ID = "ph_TableInUse" runat="server" />
                                    </div>
                                    
                                    </asp:Panel> 
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
            </td>



       





        <td>
			<h3 align="center" style="font-size: 15px;font-weight: bold;color: #000000">Server Details</h3>
                    <div class="content_tab2">
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <table class="comparable-edit" width='100%'>
                                    <tr>
                                        <td>
                                            Server Name
                                        </td>
                                        <td>
                                            <strong>:</strong>
                                        </td>
                                        <td class="style11">
                                           <asp:Label ID="lblServerName" runat="server" Text="Label"></asp:Label> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Host Name
                                        </td>
                                        <td>
                                            <strong>:</strong>
                                        </td>
                                        <td class="style11">
                                           <asp:Label ID="lblHostName" runat="server" Text="Label"></asp:Label> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                           Private IP
                                        </td>
                                        <td>
                                            <strong>:</strong>
                                        </td>
                                        <td>
                                           <asp:Label ID="lblPrivateIP" runat="server" Text="Label"></asp:Label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                           Public IP
                                        </td>
                                        <td>
                                            <strong>:</strong>
                                        </td>
                                        <td>
                                           <asp:Label ID="lblPublicIP" runat="server" Text="Label"></asp:Label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            Total Database's                                          
                                        </td>
                                        <td>
                                            <strong>:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblTotalDb" runat="server" Text="Label"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Total DatabaseSize
                                        </td>
                                        <td>
                                            <strong>:</strong>
                                        </td>
                                        <td>
                                           <asp:Label ID="lblTotalDBSize" runat="server" Text="Label"></asp:Label> 
                                        </td>
                                    </tr>
                                    
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                     <h3 align="center" style="font-size: 15px;font-weight: bold;color: #000000">Database Not In Use</h3>
                    <div class="content_tab2">
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
                                    <div id="Div2" style="overflow:auto; width:100%;">
                                    <asp:Panel id="pnlContentsNotInUse" runat = "server">
                                    <div>
                                     <asp:PlaceHolder ID = "ph_TableNotInUse" runat="server" />
                                    </div>
                                    
                                    </asp:Panel> 
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>

                </td>
	</tr>
</tbody></table>
      </div>        
</asp:Panel>
   
    
    <asp:Panel runat="server" ID="pnlExport" Visible="false">  
     <style type="text/css">
				
                 .vdrname2, .vdrname3
				{
					width: 120px;
					text-align: center !important;
				}
				
				.orgid2, .orgid3
				{
					width: 90px;
					text-align: center !important;
				}
				
				.environment2, .environment3
				{
					width: 90px;
					text-align: center !important
				}
				
				.errors2, errors3
				{
					width: 90px;
					text-align: center !important
				}
				
                .schedule1, schedule2
				{
					width: 90px;
					text-align: center !important
				}
				
				
			</style>
     <form method="post" action="databaseoccupancy.aspx" id="fexport" runat="server">
     <asp:GridView runat="server" ID="gvExport" AllowPaging="false">
        <Columns>
           
            <asp:BoundField DataField="ServerName" HeaderText="Server Name" ItemStyle-Width="150px" ItemStyle-CssClass="vdrname2-col col-vendor"
                HeaderStyle-CssClass="vdrname3-col head-vendor" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"  />
            <asp:BoundField DataField="VendorName" HeaderText="Vendor Name" ItemStyle-Width="50px" ItemStyle-CssClass="orgid2-col col-orgid"
                 HeaderStyle-CssClass="orgid3-col head-orgid" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"  />
            <asp:BoundField DataField="ClientName" HeaderText="Client Name" ItemStyle-Width="300px" ItemStyle-CssClass="environment2-col col-environment" 
                HeaderStyle-CssClass="environment3-col head-environment" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"  />
            <asp:BoundField DataField="Count" HeaderText="ParcelCount" ItemStyle-Width="60px" ItemStyle-CssClass="errors2-col col-errors" 
                HeaderStyle-CssClass="errors3-col head-errors" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="left"  />
            <asp:BoundField DataField="Schedule" HeaderText="Schedule" ItemStyle-Width="60px" ItemStyle-CssClass="schedule1-col col-schedule   " 
                HeaderStyle-CssClass="schedule2-col head-schedule   " ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="left"  />
           
        </Columns>
        
    </asp:GridView>
         </form>
      </asp:Panel>  
</asp:Content>

