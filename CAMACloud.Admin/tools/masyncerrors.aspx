﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/ClientAdmin.master" CodeBehind="masyncerrors.aspx.vb" Inherits="CAMACloud.Admin.masyncerrors" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Outstanding MobileAssessor Sync Errors - Need Attention</h1>
    <asp:GridView runat="server" ID="gvErrors">
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="Org. Id" ItemStyle-Width="50px" >
			
            </asp:BoundField>
            <asp:BoundField DataField="Name" HeaderText="Environment" 
                ItemStyle-Width="300px" >
 
            </asp:BoundField>
            <asp:BoundField DataField="User" HeaderText="User" 
                ItemStyle-Width="250px" >

            </asp:BoundField>
            <%--<asp:TemplateField>
                <HeaderTemplate>
                    Error Counts</HeaderTemplate>
                <ItemTemplate>
                    <asp:LinkButton runat="server" ID="lbSelect" Text='<%# Eval("ErrorCount") %>' CommandName="SelectError"
                        CommandArgument='<%# Eval("Id") %>' Font-Size="9pt" Font-Bold="true" />
                </ItemTemplate>
            </asp:TemplateField>--%>
            <asp:BoundField DataField="ErrorCount" HeaderText="Errors" 
                ItemStyle-Width="60px" >
<ItemStyle Width="60px"></ItemStyle>
            </asp:BoundField>
        </Columns>
        <EmptyDataTemplate>
            No counties having failed MA Sync attempts
        </EmptyDataTemplate>
    </asp:GridView>

    
</asp:Content>
