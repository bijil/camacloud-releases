﻿Imports System.Data.SqlClient

Public Class errordetails
    Inherits System.Web.UI.Page
    ReadOnly Property OrganizationId As Integer
        Get
            If hdnOrganizationId.Value = "" Then
                If Request("id") Is Nothing Then
                    Response.Redirect("~/tools/masyncerrors.aspx")
                    hdnOrganizationId.Value = -1
                Else
                    hdnOrganizationId.Value = Database.System.GetIntegerValueOrInvalid("SELECT ID FROM Organization WHERE UID = {0}".SqlFormatString(Request("ouid")))
                    hdnOrganizationName.Value = Database.System.GetStringValue("SELECT Name FROM Organization WHERE UID = {0}".SqlFormatString(Request("ouid")))
                End If
                If hdnOrganizationId.Value.ToString = "-1" Then
                    Response.Redirect("~/tools/masyncerrors.aspx")
                    Response.End()
                End If
            End If
            Return hdnOrganizationId.Value
        End Get
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        LoadOrganization(OrganizationId)
    End Sub
    Sub LoadOrganization(ByVal id As Integer)
        Dim dtErrors As New DataTable
        dtErrors.Columns.Add("Name")
        dtErrors.Columns.Add("ErrorMessage")
        Dim errorMessage As String


        Dim orgs As DataTable = Database.System.GetDataTable("SELECT * FROM Organization WHERE Id = " & id)

        For Each org As DataRow In orgs.Rows
            lblClient.Text = org.GetString("Name")
            Dim connStr As String = Database.GetConnectionStringFromOrganization(org.GetInteger("Id"))


            Dim query As String = "SELECT ErrorMessage FROM UserParcelChanges WHERE ExecStatus = 9"


            Try
                Dim conn As New SqlClient.SqlConnection(connStr)
                conn.Open()
                Dim cmd As New SqlClient.SqlCommand(query, conn)
                errorMessage = cmd.ExecuteScalar()
                conn.Close()
            Catch sqlex As SqlClient.SqlException

            Catch ex As Exception

            End Try


            Dim dr As DataRow = dtErrors.NewRow
            dr("Name") = org.GetString("Name")
            dr("ErrorMessage") = errorMessage
            dtErrors.Rows.Add(dr)


        Next
        gvErrorsMessage.DataSource = dtErrors
        gvErrorsMessage.DataBind()
    End Sub
End Class