﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/ClientAdmin.master" CodeBehind="bulkexport.aspx.vb" Inherits="CAMACloud.Admin.bulkexport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Bulk Export - All Counties</h1>
    <table>
        <tr>
            <td style="width: 120px;">Job Name (optional): 
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtJobName" Width="240px"  MaxLength="50"/>
            </td>
        </tr>
        <tr>
            <td style="width: 120px;">Select Vendor: 
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlVendor" Width="240px" />

            </td>
        </tr>
        <tr>
            <td colspan="2">Procudure Code (e.g., EXEC procedureName @parameters, ...):<br />
                <asp:TextBox runat="server" ID="txtQuery" TextMode="MultiLine" Rows="5" Columns="120" /><br />
                <asp:Button runat="server" ID="btnExport" Text="Export Results" />&nbsp;&nbsp;
            </td>
        </tr>
    </table>
    <asp:Label runat="server" ID="lblError" ForeColor="Red" Style="padding: 5px 0px; display: block;" />

    <asp:GridView runat="server" ID="grid">
        <Columns>
            <asp:BoundField DataField="Name" HeaderText="County" ItemStyle-Width="300px" />
            <asp:BoundField DataField="DBName" HeaderText="Database" ItemStyle-Width="140px" />
            <asp:BoundField DataField="Count" HeaderText="Count" ItemStyle-Width="60px" />
        </Columns>
    </asp:GridView>
</asp:Content>
