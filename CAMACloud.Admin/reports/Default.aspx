﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/CAMACloud.master" CodeBehind="Default.aspx.vb" Inherits="CAMACloud.Admin.reports_Default" %>
<%@ Register Src="~/App_Controls/app/AdminReportViewer.ascx" TagName="AdminReport" TagPrefix="Report" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/App_Static/css/reports.css?<%= Now.Ticks %>" />
 <style>
        .panel-spacer
        {
            display: none;
        }
        
        .report-filter-container
        {
            margin: 4px 0px;
        }
        .report-filter-container .leftSpan
        {
    		margin-right: 24%;
    		margin-top: 1px;
            min-width :40%
        }
        .rightSpan
        {
            margin-left: 5%
        }
        .report-control-head
        {
            font-size: 11pt;
            font-weight: bold;
            border-bottom: 1px solid #999;
            margin-top: 15px;
            margin-bottom: 10px;
        }
        
        .report-button
        {
            padding: 8px 25px !important;
            font-weight: bold;
            margin-top: 10px;
        }
        
        .validation-error
        {
            font-size: 1.3em;
            vertical-align: middle;
        }
        
        .report-viewer td
        {
            vertical-align:middle;
        }
        .hidden
        {
            display:none;
        }
        .rowstyle {
            border-color: #c1c1c1 !important;
        }
        .masklayer{
                height: 100%;
                position: absolute;
                top: 0px;
                left: 0px;
                width: 100%;
                z-index: 100;
                opacity: 0.5;
                background-color: black;
        }
        .report-view-row{
                height: 470px !important;
        }
        .div_reportSelect{
               height: 215px !important;
               overflow-y: auto !important;
        }
         .div_reportSelect::-webkit-scrollbar {
            width: 5px;
        }
         .panelControls::-webkit-scrollbar{
            width: 5px;
         }
         .ui-accordion-content-active{
             padding-right: 0 !important;
         }
		 #btnBack{		 
 		   display: inline-block;
           float: right;
           cursor: pointer;
           font-size: 10pt;
           margin-left:80px;
           color:#1f1a1a;
		 }
		 
    </style>
<script type="text/javascript">
    var validDate = true;
    function openReport(rid) {
        $('#ddlReport').val(rid);
        $('#btnRefresh').click();
    }
    function showMask(){
    $('.masklayer').height($(document).height());
	$('.masklayer').show();
	console.log("show");
    }
    function hideMask(){
    $('.masklayer').hide();
    console.log("hide");
    }
    
    $(document).ready(function () {
        var dp = $("#LeftContent_report_filter_TimeSelector");
        if (dp.length) {
            var lblval = $("#LeftContent_report_filter_TimeSelector :selected").val();
            if (lblval == "") {
                $("#LeftContent_div_lb_TimeFrame").hide();
                $("#LeftContent_div_TimeFrame").hide();
            }
            else {
                $("#LeftContent_div_lb_TimeFrame").show();
                $("#LeftContent_div_TimeFrame").show();
            }
        }
    })
    
    function dateValidate(dateString) {
            var label = document.getElementById('<%=Label1.ClientID%>');
            var dateOf = "#" + dateString.id;
            var dateVal = $(dateOf).val();
            var validat = true;
            //var pattern = '^((18)|(19)|(20))[0-9]{2}([-/.])((0|1))([1-9])([-/.])([0-3][0-9])$';
            //var pattern=/^((18)|(19)|(20))[0-9]{2}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/;
            var pattern=/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/;
            var dtRegex = new RegExp(pattern);

            if (dateString.validity.valid==true || dtRegex.test(dateVal)){
            $("input:[type='date']").each(function(){
               
                if (!this.validity.valid || (!dtRegex.test($(this).val()) && $(this).val()!= ""))
                  {
                   validat = false;
                   }
               });
                if (validat == false)
                   {                   
                     label.innerText = "Invalid date format";
                     label.style["margin-left"] = "0%";
                     label.style["margin-bottom"] = "5px";
                     validDate = false;
                      return false;
                    }
                else
                   {
                     label.innerText = "";
                     validDate = true;                  
                     return true;
                    
                   }
           
            }
            
            else if (dateString.validity.valid!=true && !dtRegex.test(dateVal)){
                label.innerText = "Invalid date format";
                validDate = false;
                
                return false;
            }
        }
    function GenerateMask() {
        var dtRegex = new RegExp(/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/);
        if (validDate == "false" || validDate == false) {
            return false;
        }
        $('.div_reportSelect input[type="date"]').each(function () {
            if (dtRegex.test($(this).val()) || $(this).val() == '') {
                validDate = true
            }
            else {
                validDate = false;
                return false;
            }
        });


        if (Page_ClientValidate('reportquery')) {
            var currentSelectedReport = $("#ddlReport option:selected").val();
            if (currentSelectedReport == "7") {
                var date = $("#LeftContent_report_filter_StartDate").val();
                var endDate = $("#LeftContent_report_filter_EndDate").val();
                if (date != "" || endDate != "") {
                    var vnd = $("#LeftContent_report_filter_Vendor").val();
                    var end = $("#LeftContent_report_filter_Org").val();
                    if (vnd == "" && end == "") {
                        alert('Please choose a Vendor Name or an Environment Name ');
                        return false;
                    };
                };
            };
            $('.masklayer').height($(document).height());
            $('.masklayer').show();
            return true;
        }
        Page_BlockSubmit = false;
        return false;
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftContent" runat="server">
     <div id="accordion" name="parcelmgr" class="report-accordians">
    <asp:PlaceHolder runat="server" ID="phBasicSettings">
    
        <h3 aria-expanded="true">
            <a>Reports</a></h3> 
        <div>
            <div style="margin: 10px 0px; margin-top: 0px;">
                <div class="report-control-head">
                    Select Report:</div>
                <div>
                    <asp:Button runat="server" ID="btnRefresh" ClientIDMode="Static" Text="Refresh" style='display:none;'/>
                    <asp:DropDownList ClientIDMode="Static" runat="server" Onchange="showMask()" ID="ddlReport" Width="220px" AutoPostBack="true" name="ddlReport">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator runat="server" ID="rfv_ddlReport" ControlToValidate="ddlReport" ErrorMessage="*" SetFocusOnError="True" ValidationGroup="reportquery" EnableClientScript="true" />
                </div>
                 <div class="report-control-head" runat="server" id="lblReportParameters">
                        Report Parameters:</div>
                <asp:Panel runat="server" ID="reportParams" class="div_reportSelect">
                    <asp:HiddenField runat="server" ID="HasFilters" />
                    <asp:PlaceHolder runat="server" ID="ph"></asp:PlaceHolder> 
                   <br />                    
                </asp:Panel>
                <asp:Label ID="Label1" runat="server" Text="" ForeColor="Red"></asp:Label>
                <div>
                    <asp:Button runat="server" ID="btnGenerate" Text="Generate Report" CssClass="report-button" OnClientClick="javascript:return GenerateMask();" OnClick="btnGenerate_Click" ValidationGroup="reportquery"  />
                </div>
            </div>
        </div>
        
    
    </asp:PlaceHolder>
    
   </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Panel runat="server" ID="reportsHome">
        <h1 style="padding-left: 15px;">Reports</h1>
        <div style="padding: 15px; padding-top: 0px;">
            <asp:Repeater runat="server" ID="rptReports" >
                <ItemTemplate>
                    <a class="fl report-item tip" tip-margin-left="-100" max-width="300" abbr="<%# Eval("Description")%>" onclick='openReport(<%# Eval("Id") %>)'>
                        <div class="icon"></div>
                        <span class="text"><%# Eval("Name")%></span>
                    </a>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </asp:Panel>
	<div id="customReportPanel" runat="server" style="width:100%; position:relative;" >
        <Report:AdminReport id ="Uc_CustomReport" runat ="server"></Report:AdminReport>
    </div>
    <asp:Panel runat="server" ID="pnl_nofile" Visible="false">
        <h1 style="padding-left:2%;width:98%">Reports</h1>
        <div style="padding:15px;padding-top:0px;">
             <div class="report-viewer">
                 <asp:Label runat="server" ID="lblnoFile"  Text="Sorry, it appears there is no data matching your criteria. Please adjust your search parameters and try again."></asp:Label>
             </div>
        </div>     
    </asp:Panel>
</asp:Content>
