﻿
Partial Class App_MasterPages_ClientAdmin
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim Username = Membership.GetUser().ToString()

            'Dim check As Integer = Database.System.GetIntegerValue("SELECT COUNT(*) FROM aspnet_UsersInRoles uir INNER JOIN aspnet_Users au on uir.UserId = au.UserId INNER JOIN aspnet_Roles ar on uir.RoleId = ar.RoleId WHERE au.UserName = '" & Username & "' AND ar.RoleName = 'Support'")

            If Username = "admin" Then
                Session("loggedUserType") = "admin"
                adminLink()
            ElseIf Username = "support" Then
                Session("loggedUserType") = "support"
                userLink()
            Else
                Dim uType As String = Database.System.GetStringValue("SELECT UserType FROM AdminUserSettings where LoginId='" + Username + "'")

                Session("loggedUserType") = uType

                If uType = "support" Then
                    userLink()
                ElseIf uType = "ProductManagement" Then
                    ProductManagementLink()
                Else
                    adminLink()
                End If
            End If
        End If
    End Sub

    Sub adminLink()
        HyperLink5.Visible = True
        hideMassUpdate.Style("Display") = "block"
        HyperLink6.Visible = True
        hideManageUser.Style("Display") = "block"
        marketHead.Visible = True
        marketBody.Visible = True
        otherSettingsHead.Visible = True
        otherSettingsBody.Visible = True
        maintenanceToolHead.Visible = True
        maintenanceToolBody.Visible = True
        productManagementHead.Visible = False
        productManagementBody.Visible = False
    End Sub

    Sub userLink()
        HyperLink5.Visible = False
        hideMassUpdate.Style("Display") = "none"
        HyperLink6.Visible = False
        hideManageUser.Style("Display") = "none"
        marketHead.Visible = False
        marketBody.Visible = False
        otherSettingsHead.Visible = False
        otherSettingsBody.Visible = False
        maintenanceToolHead.Visible = True
        maintenanceToolBody.Visible = True
        HyperLink13.Visible = False
        HyperLink14.Visible = False
        hideDatabaseOccupancy.Style("Display") = "none"
        hideEnvironmentCleanup.Style("Display") = "none"
        productManagementHead.Visible = False
        productManagementBody.Visible = False
    End Sub
    Sub ProductManagementLink()
        HyperLink5.Visible = False
        hideMassUpdate.Style("Display") = "none"
        HyperLink6.Visible = False
        hideManageUser.Style("Display") = "none"
        marketHead.Visible = False
        marketBody.Visible = False
        otherSettingsHead.Visible = False
        otherSettingsBody.Visible = False
        maintenanceToolHead.Visible = True
        maintenanceToolBody.Visible = True
        HyperLink13.Visible = False
        HyperLink14.Visible = False
        hideDatabaseOccupancy.Style("Display") = "none"
        hideEnvironmentCleanup.Style("Display") = "none"
        productManagementHead.Visible = True
        productManagementBody.Visible = True
    End Sub
End Class