﻿Imports System.Net.Mail
Imports System.IO

Partial Class license_ApplicationAccess
    Inherits System.Web.UI.Page

    ReadOnly Property OrganizationId As Integer
        Get
            If hdnOrganizationId.Value = "" Then
                If Request("ouid") Is Nothing Then
                    Response.Redirect("~/clientaccounts/search.aspx?pageaction=appaccess")
                    hdnOrganizationId.Value = -1
                Else
                    hdnOrganizationId.Value = Database.System.GetIntegerValueOrInvalid("SELECT ID FROM Organization WHERE UID = {0}".SqlFormatString(Request("ouid")))
                    hdnOrganizationName.Value = Database.System.GetStringValue("SELECT Name FROM Organization WHERE UID = {0}".SqlFormatString(Request("ouid")))
                End If
                If hdnOrganizationId.Value.ToString = "-1" Then
                    Response.Redirect("~/clientaccounts/search.aspx?pageaction=appaccess")
                    Response.End()
                End If
            End If
            Return hdnOrganizationId.Value
        End Get
    End Property

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim scriptManager__1 As ScriptManager = ScriptManager.GetCurrent(Me.Page)
        scriptManager__1.RegisterPostBackControl(Me.ibtnExportToExcel)
        scriptManager__1.RegisterPostBackControl(Me.ibtnExportToCSV)
        If Not IsPostBack Then
            Dim orgId As Integer = OrganizationId
            ddlNumber.FillNumbers(1, 3)
            LoadAccessKeysGrid()
            pageTitle.InnerHtml = "Application Access Keys - " + hdnOrganizationName.Value

            'Checking the Logged user and change the permissions
            If Session("loggedUserType") Is Nothing Then
                Alert("Session Experied, Please LogIn again")
                Response.Redirect("~/auth/Default.aspx")
            Else
                Dim loggedUserType As String = Session("loggedUserType").ToString()

                If loggedUserType = "support" Then
                    lbDeleteSelected.Visible = False
                    'gvAccess.Columns(0).Visible = False
                End If
            End If
        End If
    End Sub

    Protected Sub btnGenerate_Click(sender As Object, e As System.EventArgs) Handles btnGenerate.Click
        Try
            Dim numberOfAccess As Integer = Convert.ToInt32(ddlNumber.SelectedValue)
            For value As Integer = 1 To numberOfAccess
                Dim accessKey As String = RandomString(16, "ApplicationAccess", "AccessKey")
                Dim passKey As String = RandomString(30, "ApplicationAccess", "PassKey", 48, 90)
                Dim sql As String = "INSERT INTO ApplicationAccess (OrganizationId,AccessKey,PassKey,Note) VALUES ({0},{1},{2},{3})"
                If (Database.System.Execute(sql.SqlFormatString(OrganizationId, accessKey, passKey, txtNote.Text)) > 0) Then
                    LoadAccessKeysGrid()
                End If
            Next
        Catch ex As Exception
            Alert(ex.Message)
        End Try
    End Sub

    Sub LoadAccessKeysGrid()
        gvAccess.DataSource = Database.System.GetDataTable("SELECT * FROM ApplicationAccess WHERE OrganizationId = " & OrganizationId & " ORDER BY CreatedDate")
        gvAccess.DataBind()

        Dim sql1 As String = String.Format("SELECT AccessKey as [Access Key],PassKey as[Pass Key],LastAccessIP as [Last IP Address]  FROM ApplicationAccess WHERE OrganizationId = " & OrganizationId & " ORDER BY CreatedDate")
        Dim dt1 As DataTable = Database.System.GetDataTable(sql1)
        ViewState("dtData") = dt1

        If gvAccess.Rows.Count > 0 Then
            tLicenseOps.Visible = True
        Else
            tLicenseOps.Visible = False
        End If

    End Sub

    Private Function RandomString(size As Integer, checkSource As String, checkField As String, Optional asciiStart As Integer = 65, Optional asciiEnd As Integer = 90) As String
        Dim builder As New StringBuilder()
        Dim licenseKey As String
        Dim random As New Random()
        Dim ch As Char
        Dim i As Integer
        For i = 0 To size - 1
            ch = Convert.ToChar(Convert.ToInt32(((asciiEnd - asciiStart) * random.NextDouble() + asciiStart)))
            builder.Append(ch)
        Next
        licenseKey = builder.ToString()
        Dim sql As String = String.Format("SELECT count(*) FROM " + checkSource + " WHERE " + checkField + " = '{0}'", licenseKey)
        If (Database.System.GetIntegerValue(sql) > 0) Then
            licenseKey = RandomString(size, checkSource, checkField, asciiStart, asciiEnd)
        End If

        Return licenseKey
    End Function



    Protected Sub btnEmail_Click(sender As Object, e As System.EventArgs) Handles btnEmail.Click
        If txtEmail.Text.Trim = "" Then
            Return
        End If
        Try
            Dim ta As New MailAddress(txtEmail.Text)
        Catch ex As Exception
            Alert("Invalid email address.")
            Return
        End Try

        Dim mail As New MailMessage
        mail.From = New MailAddress(ClientSettings.DefaultSender, "Access@CAMACloud")
        mail.To.Add(txtEmail.Text)
        mail.Bcc.Add("consultsarath@gmail.com")
        mail.Subject = Now.ToString("yyyyMMdd\HHHmmss") + " : CAMA Cloud API Access Keys for " + hdnOrganizationName.Value.Trim()
        mail.Body = composeEmail()
        mail.IsBodyHtml = True

        AWSMailer.SendMail(mail)
        Alert("An email has been sent to " + txtEmail.Text + " with the selected keys.")
    End Sub


    Private Function composeEmail() As String
        Dim mailContent As XElement = <html>
                                          <body>
                                              <div style="font-family:Arial;width:600px;">
                                                  <h1 style="border-bottom:1px solid #075297;color:#075297;">CAMA Cloud - Application Access Keys</h1>
                                                  <h2></h2>
                                                  <p style="font-size:10pt;">Greetings from CAMA Cloud,</p>
                                                  <p style="font-size:10pt;">The following access keys can be used to access CAMA Cloud API Access for <b>Organization</b>.</p>
                                                  <table style="font-size:10pt;font-family:Arial;">
                                                      <thead>
                                                          <tr>
                                                              <td style='font-weight:bold;width:200px;'>Access Key</td>
                                                              <td style='font-weight:bold;width:80px;'>Pass Key</td>
                                                          </tr>
                                                      </thead>
                                                      <tbody>

                                                      </tbody>
                                                  </table>
                                                  <p style="font-size:10pt;">Sincererly, <br/>CAMA Cloud</p>
                                                  <hr/>
                                                  <p style="font-size:8pt;margin-top:0px;">
                                                      <a href="http://www.camacloud.com/">CAMA Cloud<sup style="font-size:6pt;">&#174;</sup></a> powered by <a href="http://www.datacloudsolutions.net/">Data Cloud Solutions, LLC.</a>
                                                  </p>
                                              </div>
                                          </body>
                                      </html>

        Dim dataFormat As XElement = <tr>
                                         <td></td>
                                         <td></td>
                                     </tr>

        mailContent...<body>...<div>...<p>(1)...<b>.Value = hdnOrganizationName.Value
        mailContent...<body>...<div>...<h2>.Value = hdnOrganizationName.Value
        For Each gr As GridViewRow In gvAccess.Rows
            If gr.GetChecked("chkSel") Then
                Dim dataLine As XElement = XElement.Parse(dataFormat.ToString)
                dataLine...<td>(0).Value = gr.GetHiddenValue("AKEY")
                dataLine...<td>(1).Value = gr.GetHiddenValue("PKEY")
                mailContent...<body>...<div>...<table>...<tbody>(0).Add(dataLine)
            End If
        Next

        Return mailContent.ToString
    End Function

    Private Function newPara(line As String) As String
        Return "<p>" + line + "</p>"
    End Function

    Protected Function GetSelectedIds() As String
        Dim selection As String = "0"
        For Each gr As GridViewRow In gvAccess.Rows
            If gr.GetChecked("chkSel") Then
                selection += "," + gr.GetHiddenValue("LID")
            End If
        Next
        Return selection
    End Function

    Sub DeleteLicenses(ids As String)
        Database.System.Execute("DELETE FROM ApplicationAccess WHERE Id IN (" & ids & ")")
        LoadAccessKeysGrid()
    End Sub

    Protected Sub lbDeleteSelected_Click(sender As Object, e As EventArgs) Handles lbDeleteSelected.Click
        DeleteLicenses(GetSelectedIds)
    End Sub

    Protected Sub lbLicence_Click(sender As Object, e As EventArgs) Handles lbLicence.Click
        Dim var As String
        Dim org As DataRow = Database.System.GetTopRow("SELECT * FROM Organization WHERE Id=" & Val(OrganizationId))
        var = org.GetString("UID")
        Response.Redirect("~/license/?ouid=" + var)
    End Sub

    Protected Sub lblClientDtls_Click(sender As Object, e As EventArgs) Handles lblClientDtls.Click
        Dim var As String
        Dim org As DataRow = Database.System.GetTopRow("SELECT * FROM Organization WHERE Id=" & Val(OrganizationId))
        var = org.GetString("UID")
        Response.Redirect("~/clientaccounts/manageclient.aspx/?ouid=" + var)
    End Sub
    Protected Sub ibtnExportToExcel_Click(sender As Object, e As System.EventArgs) Handles ibtnExportToExcel.Click
        Dim dt As DataTable = ViewState("dtData")
        Dim GridView1 As GridView = ExportGridView(dt, "Last Access", "{0:MM/dd/yyyy h:mm tt}")
        ExportToExcel(GridView1, "APIAccessKeys.xls")
    End Sub

    Public Sub ExportToExcel(GridView1 As GridView, ByVal filename As String)
        Try
            Response.Clear()
            Response.Buffer = True
            Response.ClearContent()
            Response.ClearHeaders()
            Response.AddHeader("Content-Disposition", "attachment;filename=" + filename)
            Response.Charset = ""
            Response.ContentType = "application/vnd.ms-excel"
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            GridView1.RenderControl(hw)
            ''style to format numbers to string
            'Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"
            'Response.Write(style)
            Response.Output.Write(sw.ToString())
            Response.Flush()
            Response.End()
        Catch ex As Exception

        End Try

    End Sub

    Public Function ExportGridView(ByVal dt As DataTable, Optional ByVal formatColumn As String = Nothing, Optional ByVal format As String = Nothing) As GridView
        'Create a dummy GridView
        Dim GridView1 As New GridView()
        GridView1.AllowPaging = False
        GridView1.AutoGenerateColumns = False
        Dim bfieldName As String
        Dim bHeaderText As String
        GridView1.Columns.Clear()
        For Each column As DataColumn In dt.Columns
            bfieldName = column.ColumnName
            bHeaderText = column.ColumnName
            Dim bfield As New BoundField()
            bfield.HeaderText = bHeaderText
            bfield.DataField = bfieldName
            If (column.ColumnName = formatColumn) Then
                bfield.DataFormatString = format
            End If
            GridView1.Columns.Add(bfield)
        Next
        GridView1.DataSource = Nothing
        GridView1.DataSource = dt
        GridView1.DataBind()
        Return GridView1
    End Function
    Protected Sub ibtnExportToCSV_Click(sender As Object, e As System.EventArgs) Handles ibtnExportToCSV.Click
        Dim dt As DataTable = ViewState("dtData")
        Dim GridView1 As GridView = ExportGridView(dt, "Last Access", "{0:MM/dd/yyyy h:mm tt}")
        ExportToCSV(GridView1, "APIAccessKeys.csv")
    End Sub

    Protected Sub ExportToCSV(ByVal GridView1 As GridView, ByVal filename As String)
        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("Content-Disposition", "attachment;filename=" + filename)
        Response.Charset = ""
        Response.ContentType = "application/text"
        Dim sBuilder As StringBuilder = New System.Text.StringBuilder()
        For index As Integer = 0 To GridView1.Columns.Count - 1
            sBuilder.Append(GridView1.Columns(index).HeaderText + ","c)
        Next
        sBuilder.Append(vbCr & vbLf)
        For i As Integer = 0 To GridView1.Rows.Count - 1
            For k As Integer = 0 To GridView1.HeaderRow.Cells.Count - 1
                sBuilder.Append(GridView1.Rows(i).Cells(k).Text.Replace("&nbsp;", "") + ",")
            Next
            sBuilder.Append(vbCr & vbLf)
        Next
        Response.Output.Write(sBuilder.ToString())
        Response.Flush()
        Response.[End]()
    End Sub

    Protected Sub gvAccess_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvAccess.RowCommand
        Select Case e.CommandName
            Case "edit mac"
                Database.System.Execute("update  ApplicationAccess set  ServerMACAddress =NULL WHERE Id={0}".SqlFormatString(e.CommandArgument))
                LoadAccessKeysGrid()
        End Select


    End Sub
    
Protected Sub chkIsEnabled_CheckedChanged(sender As Object, e As EventArgs)
	Dim chkStatus As CheckBox = DirectCast(sender, CheckBox)
	Dim ID As Int64 = Convert.ToInt64(gvAccess.DataKeys(DirectCast(chkStatus.NamingContainer, GridViewRow).RowIndex).Value)
	Database.System.Execute("UPDATE ApplicationAccess SET IsEnabled={0} WHERE Id={1}".SqlFormatString(chkStatus.Checked, ID))
	If chkStatus.Checked="True" Then
		Alert("This API Key has been enabled.")
	Else
		Alert("This API Key has been disabled.")
	End If
End Sub

End Class
