﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/ClientAdmin.master" CodeBehind="superaccess.aspx.vb" Inherits="CAMACloud.Admin.superaccess" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .link-button
        {
            font-weight: bold;
            text-decoration: none;
            padding: 5px 15px;
            margin: 5px;
            margin-left: 0px;
            margin-right: 15px;
            background: #CFCFCF;
            border-radius: 8px;
            display: inline-block;
            cursor: pointer;
        }
        .link-button:hover
        {
            color: White;
            background: #065a9f;
        }
        
        .keys pre
        {
            margin: 0px !important;
            padding: 0px !important;
            font-weight: bold;
            font-family: Lucida Sans Typewriter;
        }
        
        .simple-form tr td
        {
            vertical-align: middle;
        }
        
        .masklayer {
		    background: black;
		    opacity: 0.5;
		    top: 0%;
		    width: 100%;
		    height: 100%;
		    bottom: 0%;
		    position: fixed;
		    overflow:hidden;
		    display: none;
		}
</style>
    <script type="text/javascript">
    
        function checkSelection() {
            var totalChecked = 0;
            $('.grid-selection input').each(function () {
                if (this.checked)
                    totalChecked++;
            });
            if (totalChecked == 0) {
                alert('You have not selected any license row.');
                return false
            }
            return true;
        }
        function confirmMultiDelete() {
            if (!checkSelection()) return false;
            return confirm("Are you sure you want to delete selected device licenses permanently?")
        }

        function confirmMultiRevoke() {
            if (!checkSelection()) return false;
            return confirm("You are cancelling the selected installed device licenses and allowing it to be used again. Click OK to continue.")
        }

        function confirmEmail() {
            if (!checkSelection()) return false;
            if ($('.txt-email').val() == '') {
                alert('Please enter a valid email address to email the licenses.');
                return false
            };
            return true;
        }

        function copyToClipboard() {
            if (window.clipboardData) {

            }
        }

        function bulkSelect(s) {
            $('.grid-selection input').each(function (i, x) { x.checked = s; });
        }

		function showMask(info) {		
		    $('.masklayer').height($(document).height())
		    $('.masklayer').show();		    
		        info = 'Please wait ...'
		}
		
		function hideMask() {
   		$('.masklayer').hide();
		}
		function checkboxcheck() {
		    var chk = $('.grid-selection input[type = checkbox]');
		    var chkChecked = $('.grid-selection input[type = checkbox]:checked');
		    if (chk.length == chkChecked.length) {
		        $('#chkHead').prop("checked", true);
		    }
		    else {
		        $('#chkHead').prop("checked", false);
		    }
		    
		}
</script>  
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <h1 runat="server" id="pageTitle">
        Super Access License Keys - For Administrative Use Only</h1>
    <p class="info">
        Any client user can access the CAMA Cloud application and data only after validating
        their browser with a unique license key and one time password.</p>
    <asp:HiddenField runat="server" ID="hdnOrganizationName" />
    <asp:HiddenField runat="server" ID="hdnOrganizationId" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        <div id="masklayer">
            <table class="simple-form">
                <tr runat="server" id="trLicenseStatus">
                    <td>
                        License Status:
                    </td>
                    <td>                   
                        <asp:DropDownList ID="ddlUsageStatus" runat="server" AutoPostBack="true" onchange="showMask(this)" Width="250px">
                            <asp:ListItem Value="0">Unused licenses</asp:ListItem>
                            <asp:ListItem Value="1">Licenses in-use</asp:ListItem>
                            <asp:ListItem Value="" >All</asp:ListItem>
						</asp:DropDownList>
					<asp:Button ID="btnRefresh" runat="server" Text="Refresh" />
                    </td>
                </tr>
                <tr runat="server" id="trNewLicense">
                    <td>
                        Generate new licenses:
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlNumber" Width="50px" />
                        <asp:Button ID="btnCreate" runat="server" Text="Generate" />
                    </td>
                </tr>
            </table>
            </div>
            <table style="width: 100%;" runat="server" id="tLicenseOps">
                <tr style="width: 920px;">
                    <td class="link-panel">
                        <asp:LinkButton runat="server" ID="lbDeleteSelected" Text="Delete Selected" OnClientClick="return confirmMultiDelete()" />
                        <asp:LinkButton runat="server" ID="lbRevokeSelected" Text="Revoke Selected" OnClientClick="return confirmMultiRevoke()" />
                    </td>
                    <td style="text-align: right;">
                        <asp:TextBox runat="server" ID="txtEmail" type="email" Width="200px" CssClass="txt-email" />
                        <asp:Button Text=" Email Licenses " runat="server" ID="btnEmail" OnClientClick="return confirmEmail();" />
                    </td>
                </tr>
                <tr style="width: 100%;">
                    <td style="text-align: right;" colspan="2">
                        <asp:ImageButton ID="ibtnExportToExcel" ImageUrl="~/App_Static/images/excel.png" Width="28px" Height="25px" Style="margin-right: 3px;" ToolTip="Export To Excel" runat="server" />
                        <asp:ImageButton ID="ibtnExportToCSV" ImageUrl="~/App_Static/images/csv.png" Width="28px" Height="25px" Style="margin-right: 3px;" ToolTip="Export To CSV" runat="server" />
                    </td>
                </tr>
            </table>
            <asp:GridView ID="gvLicense" runat="server">
                <Columns>
                    <asp:TemplateField>
                        <ItemStyle Width="18px" />
                        <ItemTemplate>
                            <asp:CheckBox ID="chkSel" runat="server" Checked="false" CssClass="grid-selection" onchange ="checkboxcheck()"/>
                            <asp:HiddenField ID="LID" runat="server" Value='<%# Eval("Id") %>' />
                            <asp:HiddenField ID="LKEY" runat="server" Value='<%# EvalLicenseKey() %>' />
                            <asp:HiddenField ID="OTP" runat="server" Value='<%# Eval("OTP") %>' />
                            <asp:HiddenField ID="DEVICE" runat="server" Value='<%# Eval("TargetDevice") %>' />
                        </ItemTemplate>
                        <HeaderTemplate>
                            <input type="checkbox" onchange='bulkSelect(this.checked);' id="chkHead" />
                        </HeaderTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="License Key">
                        <ItemStyle CssClass="keys" Width="250px" />
                        <ItemTemplate>
                            <pre style=""><%# EvalLicenseKey() %></pre>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="SCODE" HeaderText="SCODE" ItemStyle-Width="80px" ItemStyle-Font-Bold="true" />
                    <asp:BoundField DataField="OTP" HeaderText="OTP" ItemStyle-Width="80px" ItemStyle-Font-Bold="true" />
                    <asp:BoundField DataField="LastUsedLoginId" HeaderText="CAMA User" ItemStyle-Width="120px" />
                    <asp:BoundField DataField="LastUsedIPAddress" HeaderText="IP Address" ItemStyle-Width="120px" />
                    <asp:BoundField DataField="RegisteredEmail" HeaderText="Registered Email" ItemStyle-Width="250px" />
                    <asp:BoundField DataField="RegisteredNick" HeaderText="Nickname" ItemStyle-Width="120px" />
                    <asp:TemplateField HeaderText="Last Access">
                        <ItemStyle CssClass="keys" Width="120px" />
                        <ItemTemplate>
                            <%# Eval("LastAccessTime", "{0:MM/dd/yyyy hh:mm tt}") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="CreatedDate" HeaderText="CreatedDate" ItemStyle-Width="180px" />
                     <asp:BoundField DataField="CreatedBy" HeaderText="CreatedBy" ItemStyle-Width="80px" />
                    <asp:TemplateField>
                        <ItemStyle Width="120px" />
                        <ItemTemplate>
                            <asp:LinkButton runat="server" ID="lbDelete" Text="Delete" CommandName="DeleteLicense"
                                CommandArgument='<%# Eval("Id") %>' OnClientClick='return confirm("Are you sure you want to delete this device license permanently?")' />&nbsp;
                            <asp:LinkButton runat="server" ID="lbRevoke" Text="Revoke" CommandName="RevokeLicense"
                                CommandArgument='<%# Eval("Id") %>' Visible='<%# Eval("InUse") %>' OnClientClick='return confirm("You are cancelling an installed device license and allowing it to be used again. Click OK to continue.")' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemStyle Width="18px" />
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" CssClass="a16 lock16" Visible='<%# Eval("InUse") %>' ToolTip="License Key in use" />
                            <asp:Label ID="Label2" runat="server" CssClass="a16 newborn16" Visible='<%# ((System.DateTime.UtcNow - CDate(Eval("CreatedDate"))).TotalSeconds < 30) And Not Eval("InUse") %>'
                                ToolTip="Newly created" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    <div class="info">
                        No licenses found.
                    </div>
                </EmptyDataTemplate>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div style="display: none;">
        <textarea id="clipboardAgent"></textarea>
    </div>
</asp:Content>