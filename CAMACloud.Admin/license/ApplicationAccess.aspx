﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/ClientAdmin.master"
    AutoEventWireup="false" Inherits="CAMACloud.Admin.license_ApplicationAccess"
    ValidateRequest="false" CodeBehind="ApplicationAccess.aspx.vb" %>

<asp:Content ID="LicenseContent" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .link-button
        {
            font-weight: bold;
            text-decoration: none;
            padding: 5px 15px;
            margin: 5px;
            margin-left: 0px;
            margin-right: 15px;
            background: #CFCFCF;
            border-radius: 8px;
            display: inline-block;
            cursor: pointer;
        }
        .link-button:hover
        {
            color: White;
            background: #065a9f;
        }
        
        .keys pre
        {
            margin: 0px !important;
            padding: 0px !important;
            font-weight: bold;
            font-family: Lucida Sans Typewriter;
        }
        
        .simple-form tr td
        {
            vertical-align: middle;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtNote.ClientID%>").keyup(function () {
                var note = $("#<%=txtNote.ClientID%>").val();
                if (note.length > 500) {
                    alert('Exceeding maximum number of characters.Please short your notes to 500 characters.');
                };
            });
        });
        function checkSelection() {
            var totalChecked = 0;
            $('.grid-selection input').each(function () {
                if (this.checked)
                    totalChecked++;
            });
            if (totalChecked == 0) {
                alert('You have not selected any key.');
                return false
            }
            return true;
        }
        function confirmMultiDelete() {
            if (!checkSelection()) return false;
            return confirm("Are you sure you want to delete selected keys permanently?")
        }

        function confirmEmail() {
            if (!checkSelection()) return false;
            if ($('.txt-email').val() == '') {
                alert('Please enter a valid email address to email the keys.');
                return false
            };
            return true;
        }

        function copyToClipboard() {
            if (window.clipboardData) {

            }
        }

        function bulkSelect(s) {
            $('.grid-selection input').each(function (i, x) { x.checked = s; });
        }

        function ValidateNote() {
            var note = $("#<%=txtNote.ClientID%>").val();
            if (note.length > 500) {
                alert('Please short your notes to 500 characters.');
                return false;
            }
            else{
                return true;
            }

        }

        function checkboxcheck() {
            var chk = $('.grid-selection input[type = checkbox]');
            var chkChecked = $('.grid-selection input[type = checkbox]:checked');
            if (chk.length == chkChecked.length) {
                $('#chkHead').prop("checked", true);
            }
            else {
                $('#chkHead').prop("checked", false);
            }

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField runat="server" ID="hdnOrganizationName" />
    <asp:HiddenField runat="server" ID="hdnOrganizationId" />
    <table width="100%">
        <tr>
            <td>
                <div style="width: 100%; float: left; height: 60px">
                    <h1 runat="server" id="pageTitle">
                        Application Access Keys
                    </h1>
                    <div style="position: relative; top: -54px;">
                        <asp:LinkButton ID="lblClientDtls" runat="server" CommandName="Client Details" Text="Client Details"
                            Style="margin-left: 5px; float: right; text-decoration: None" />
                        <asp:LinkButton ID="lbLicence" runat="server" CommandName="License" Text="License Keys"
                            Style="float: right; text-decoration: None; margin-right: 15px" />
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="updlMain" runat="server">
        <ContentTemplate>
            <div>
                <table>
                    <tr>
                        <td style="vertical-align: middle;">
                            Generate New Keys:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlNumber" runat="server" Width="60px" />
                            <asp:TextBox ID="txtNote" runat="server" ToolTip="Give some notes" placeholder="Note"></asp:TextBox>
                            <asp:Button ID="btnGenerate" runat="server" Text="Generate" onClientClick ="return ValidateNote()"/>
                        </td>
                    </tr>
                </table>
            </div>
            <div>
                <table style="width: 1020px;" runat="server" id="tLicenseOps">
                    <tr>
                        <td class="link-panel">
                            <asp:LinkButton runat="server" ID="lbDeleteSelected" Text="Delete Selected" OnClientClick="return confirmMultiDelete()" />
                        </td>
                        <td style="text-align: right;">
                            <asp:TextBox runat="server" ID="txtEmail" type="email" Width="200px" CssClass="txt-email" />
                            <asp:Button Text=" Email Keys " runat="server" ID="btnEmail" OnClientClick="return confirmEmail();" />
                        </td>
                    </tr>
                <tr>
                    <td style="text-align: right;" colspan="2">
                        <asp:ImageButton ID="ibtnExportToExcel" ImageUrl="~/App_Static/images/excel.png" Width="28px" Height="25px" Style="margin-right: 3px;" ToolTip="Export To Excel" runat="server" />
                        <asp:ImageButton ID="ibtnExportToCSV" ImageUrl="~/App_Static/images/csv.png" Width="28px" Height="25px" Style="margin-right: 3px;" ToolTip="Export To CSV" runat="server" />
                    </td>
                </tr>
                </table>
                <asp:GridView runat="server" ID="gvAccess" AllowPaging="true" PageSize="20" DataKeyNames="Id">
                    <Columns>
                        <asp:TemplateField>
                            <ItemStyle Width="18px" />
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSel" runat="server" Checked="false" CssClass="grid-selection" onchange="checkboxcheck()"/>
                                <asp:HiddenField ID="LID" runat="server" Value='<%# Eval("Id") %>' />
                                <asp:HiddenField ID="AKEY" runat="server" Value='<%# Eval("AccessKey") %>' />
                                <asp:HiddenField ID="PKEY" runat="server" Value='<%# Eval("PassKey") %>' />
                            </ItemTemplate>
                            <HeaderTemplate>
                                <input type="checkbox" onchange='bulkSelect(this.checked);' id="chkHead"/>
                            </HeaderTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Access Key">
                            <ItemStyle CssClass="keys" Width="200px" />
                            <ItemTemplate>
                                <code>
                                    <%# Eval("AccessKey") %></code>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Pass Key">
                            <ItemStyle CssClass="keys" Width="250px" />
                            <ItemTemplate>
                                <code>
                                    <%# HttpUtility.HtmlEncode(Eval("PassKey"))%></code>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Last IP Address">
                            <ItemStyle CssClass="keys" Width="140px" />
                            <ItemTemplate>
                                <code>
                                    <%# Eval("LastAccessIP")%></code>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemStyle Width="18px" />
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" CssClass="a16 newborn16" Visible='<%# ((System.DateTime.UtcNow - CDate(Eval("CreatedDate"))).TotalSeconds < 30) %>'
                                    ToolTip="Newly created" />
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField>
                            <ItemStyle />
                            <ItemTemplate>
                                <asp:LinkButton ID="lb_reset_serverAddress" runat="server" CommandName="edit mac"
                            CommandArgument='<%# Eval("Id") %>'  Visible='<%# (Not IsDBNull(Eval("ServerMACAddress")) AndAlso Not String.IsNullOrEmpty(Eval("ServerMACAddress"))) %>'>Reset MAC address</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                       <asp:TemplateField HeaderText ="Note">
                            <ItemStyle CssClass="keys" Width="290px" />
                            <ItemTemplate>
                                <code>
                                    <%# Eval("Note") %>
                                </code>
                            </ItemTemplate>
                        </asp:TemplateField>
						<asp:TemplateField HeaderText="Enable">
							<ItemTemplate>
							<asp:CheckBox runat="server" AutoPostBack="true" ID="chkIsEnabled" Checked='<%#Eval("IsEnabled")%>' oncheckedchanged="chkIsEnabled_CheckedChanged"  />
							</ItemTemplate>
							<HeaderStyle Width="30px" />
						</asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
