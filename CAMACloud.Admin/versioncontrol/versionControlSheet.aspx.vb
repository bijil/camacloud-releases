﻿

Public Class versionControlSheet
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            dllBindStage()
            dllBindApplication()
            searchApplication()
            searchStage()
            gridVersionControl()
        End If
    End Sub

    Private Sub dllBindStage() 'Stage Dropdown data bindings
        ddlStage.DataSource = Data.Database.System.GetDataTable("SELECT  StageId, StageName FROM ApplicationStage")
        ddlStage.DataTextField = "StageName"
        ddlStage.DataValueField = "StageId"
        ddlStage.DataBind()
        ddlStage.Items.Insert(0, New ListItem("--Select Stage--", "0"))
    End Sub

    Private Sub dllBindApplication() 'Application Dropdown data bindings
        ddlApplication.DataSource = Data.Database.System.GetDataTable("SELECT AppId,AppName FROM Application")
        ddlApplication.DataTextField = "AppName"
        ddlApplication.DataValueField = "AppId"
        ddlApplication.DataBind()
        ddlApplication.Items.Insert(0, New ListItem("--Select Application--", "0"))
    End Sub

    Private Sub searchApplication() 'Search Application Dropdown data bindings
        ddlSearchApplication.DataSource = Data.Database.System.GetDataTable("SELECT AppId,AppName FROM Application")
        ddlSearchApplication.DataTextField = "AppName"
        ddlSearchApplication.DataValueField = "AppId"
        ddlSearchApplication.DataBind()
        ddlSearchApplication.Items.Insert(0, New ListItem("--Select Application--", "0"))
    End Sub

    Private Sub searchStage() 'Search Stage Dropdown data bindings
        ddlSearchStage.DataSource = Data.Database.System.GetDataTable("SELECT  StageId, StageName FROM ApplicationStage")
        ddlSearchStage.DataTextField = "StageName"
        ddlSearchStage.DataValueField = "StageId"
        ddlSearchStage.DataBind()
        ddlSearchStage.Items.Insert(0, New ListItem("--Select Stage--", "0"))
    End Sub

    Private Sub gridVersionControl() 'Function for Data grid bindings
        Dim application As String = ddlSearchApplication.SelectedValue
        Dim stage As String = ddlSearchStage.SelectedValue
        Dim applicationQuery, stageQuery, releaseDate As String

        colorIndicatorContainer.Style("display") = "none"

        'Concatenate the Search Query for result
        If ddlSearchApplication.SelectedValue <> "0" Then
            applicationQuery = "AND VersionControl.Application = '" + application + "'"
        Else
            applicationQuery = ""
        End If

        If ddlSearchStage.SelectedValue <> "0" Then
            stageQuery = "AND VersionControl.Stage = '" + stage + "'"
        Else
            stageQuery = ""
        End If

        If txtSearchStartDate.Value <> "" And txtSearchEndDate.Value <> "" Then
            releaseDate = "AND VersionControl.ReleaseDate between '" + txtSearchStartDate.Value + "' AND '" + txtSearchEndDate.Value + "'"
        Else
            releaseDate = ""
        End If


        Dim dt As DataTable = Data.Database.System.GetDataTable("SELECT VersionControl.Id,VersionControl.ReleaseDate, VersionControl.ActualRevision,VersionControl.TrimRevision, VersionControl.Version, VersionControl.CodeUpdatedBy, VersionControl.QCApprovedBy, VersionControl.ReleaseNote, (SELECT AppName FROM Application WHERE AppId=VersionControl.Application) AS AppName, (SELECT StageName FROM ApplicationStage WHERE StageId=VersionControl.Stage) AS StageName FROM VersionControl, Application, ApplicationStage WHERE VersionControl.Application = Application.AppId AND VersionControl.Stage = ApplicationStage.StageId " + applicationQuery + " " + stageQuery + " " + releaseDate + " ORDER BY VersionControl.Id DESC")
        gridViewVersionControl.DataSource = dt
        gridViewVersionControl.DataBind()
        If gridViewVersionControl.Rows.Count > 0 Then
            gridViewVersionControl.Columns(0).Visible = True
            PageCount.Visible = True
            SelectCount.Visible = True
            PageCount.InnerHtml = "Displaying records {0} to {1} of {2}".FormatString(gridViewVersionControl.PageIndex * gridViewVersionControl.PageSize + 1, gridViewVersionControl.PageIndex * gridViewVersionControl.PageSize + gridViewVersionControl.Rows.Count, dt.Rows.Count)
        Else
            gridViewVersionControl.Columns(0).Visible = False
            PageCount.Visible = False
            SelectCount.Visible = False
        End If
        If (hfCount.Value <> "") Then
            RunScript("selectedVersions();")
            RunScript("getSelectedVersions();")
        End If
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click 'Passing field values to Stored procedure and Saving
        Dim version As String = "v" + txtMajor.Text + "." + txtMinor.Text + "." + txtBuild.Text + "." + txtTrimRevision.Text
        Dim query = "EXEC [sp_VersionControl] @ReleaseDate = {0}, @Application = {1}, @Stage = {2}, @Major = {3}, @Minor = {4}, @Build = {5}, @ActualRevision = {6},@TrimRevision ={7}, @Version = {8}, @HasDBChanges = {9}, @ReleaseNote = {10}, @CodeUpdatedBy = {11}, @QCApprovedBy = {12}".SqlFormat(True, Request.Form("releaseDate"), ddlApplication.SelectedValue, ddlStage.SelectedValue, txtMajor.Text, txtMinor.Text, txtBuild.Text, txtActualRevision.Text,txtTrimRevision.Text, version, ddlHasDBChanges.SelectedValue, txtReleaseNote.Text, txtCodeUpdatedBy.Text, txtQCApprovedBy.Text)
        Data.Database.System.Execute(query)
        gridVersionControl() 'Reload the Grid view
        CheckBox1.Attributes.Add("style", "display:none")
        colorIndicatorContainer.Style("display") = "none"
        Response.Redirect(Request.Url.AbsoluteUri)
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        hfCount.Value = Nothing
        gridViewVersionControl.PageIndex = 0
        gridVersionControl()
        If gridViewVersionControl.Rows.Count > 0 Then 'Color changing the Top row of new Version
            If ddlSearchStage.SelectedValue <> "0" Or ddlSearchApplication.SelectedValue <> "0" Or txtSearchEndDate.Value <> "" Or txtSearchStartDate.Value <> "" Then
                gridViewVersionControl.Rows(0).BackColor = System.Drawing.Color.FromName("#EEEFB5")
                colorIndicatorContainer.Style("display") = "inline"
            End If
        Else
            colorIndicatorContainer.Style("display") = "none"
        End If
        CheckBox1.Attributes.Add("style", "display:inline")
        CheckBox1.Checked = False
    End Sub

    Protected Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox1.CheckedChanged
        hfCount.Value = Nothing
        ddlSearchStage.SelectedValue = 0
        ddlSearchApplication.SelectedValue = 0
        txtSearchEndDate.Value = ""
        txtSearchStartDate.Value = ""
        gridVersionControl()
        CheckBox1.Attributes.Add("style", "display:none")
        colorIndicatorContainer.Style("display") = "none"

    End Sub

    Protected Sub gridViewVersionControlPageChanging(sender As Object, e As GridViewPageEventArgs)
        gridViewVersionControl.PageIndex = e.NewPageIndex
        gridVersionControl()
    End Sub

    <System.Web.Services.WebMethod()> _
    Public Shared Function setDefaultValue(ByVal ApplicationId As String) As String
        Dim dt As DataTable = Database.System.GetDataTable("SELECT top 1 Major,Minor,Build FROM [dbo].[VersionControl] WHERE  Application=" + ApplicationId + "ORDER BY ActualRevision DESC")
        If dt.Rows.Count > 0 Then
            Dim jstr As String = "{"
            For Each dr As DataRow In dt.Rows
                For Each dc As DataColumn In dr.Table.Columns
                    Dim a = dr.Item(dc.ColumnName)
                    jstr += """" + dc.ColumnName + """:""" + Convert.ToString(a) + ""","
                Next
            Next
            jstr = jstr.Remove(jstr.LastIndexOf(","))
            jstr += "}"
            Return jstr
        Else
            Return Nothing
        End If
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function getAllReleaseNote(ByVal VersionControlIds As String) As List(Of String)
        Dim dtReleaseNote As DataTable = Database.System.GetDataTable("SELECT ReleaseNote FROM [dbo].[VersionControl] WHERE Id in(" & VersionControlIds & ") ORDER BY Id DESC")
        Dim releaseNote As New List(Of String)()
        Dim note As String
        If dtReleaseNote.Rows.Count > 0 Then
            For i As Integer = dtReleaseNote.Rows.Count - 1 To 0 Step -1
                note = dtReleaseNote.Rows(i).Item("ReleaseNote")
                If note <> "" Then
                    note = note.TrimStart().TrimEnd()
                    releaseNote.Add(note)
                End If
            Next
            Return releaseNote
        Else
            Return Nothing
        End If
    End Function
End Class