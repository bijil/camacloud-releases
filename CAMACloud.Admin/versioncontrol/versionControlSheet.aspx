﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/ClientAdmin.master" CodeBehind="versionControlSheet.aspx.vb" Inherits="CAMACloud.Admin.versionControlSheet" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
       <link rel="Stylesheet" href="/App_Static/css/version-control.css" />
    <script type="text/javascript" src="/App_Static/js/version-control.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Software version control sheet</h1>
    <div style="margin-bottom: 2%;">
        <div style="width: 90.7%; border: 1px solid #BEBEBE; background-color: #F5F1F1;">
            <table class="searchTable">
                <tr>
                    <td>Start Date</td>
                    <td>End Date</td>
                    <td>Application</td>
                    <td>Stage</td>
                </tr>
                <tr>
                    <td class="tdSearchDate">
                        <input type="date" id="txtSearchStartDate" name="searchStartDate" runat="server" class="searchDate txtSearchStartDate" /></td>
                    <td class="tdSearchDate">
                        <input type="date" id="txtSearchEndDate" name="searchEndDate" runat="server"  class="searchDate txtSearchEndDate" /></td>
                    <td class="tdDdlSearchApplication">
                        <asp:DropDownList ID="ddlSearchApplication" CssClass="searchDrop ddlSearchApplication" runat="server"></asp:DropDownList></td>
                    <td class="tdDdlSearchStage">
                        <asp:DropDownList ID="ddlSearchStage" CssClass="searchDrop ddlSearchStage" runat="server"></asp:DropDownList></td>
                    <td>
                        <asp:Button runat="server" Text="Search" ID="btnSearch" class="btnSearch" autopostback="false"  OnClick="btnSearch_Click" />
                    </td>
                    <td>
                        <input type="button" class="btnAddNewVersion" value="Add New Version" onclick="callDialogBox();" /></td>
                </tr>
            </table>
        </div>
    </div>
    <input type="hidden" id="hfCount" class="hfCount" runat="server" />
    <div style="margin-bottom: 1%; float: left; width: 100%">
        <asp:Button ID="btnGetReleaseNote" CssClass="btnGetReleaseNote" OnClientClick="javascript:return GetAllReleaseNotes();" Style="float: left;" runat="server" UseSubmitBehavior="false" Text="Get Release Note" disabled="disabled" />

        <div style="width: 75% !important; margin-left: 161px;">
            <asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="true" CssClass="chkDisplayAllData" Text="Show All" OnCheckedChanged="CheckBox1_CheckedChanged" />
            <span class="colorIndicatorContainer" runat="server" id="colorIndicatorContainer">
                <label style="float: right;">&nbsp;Indicates the latest version</label>
                <span class="colorIndicator"></span>
            </span>
        </div>
    </div>
    <div>
        <asp:GridView ID="gridViewVersionControl" CssClass="gridVersionControl" runat="server" ShowHeaderWhenEmpty="True" EmptyDataText="No record found" Width="91%" AllowPaging="true" OnPageIndexChanging="gridViewVersionControlPageChanging" PageSize="10">
            <Columns>
                <asp:TemplateField ItemStyle-Width="20px" HeaderText=" ">
                    <HeaderTemplate>
                        <asp:CheckBox ID="chkHeader" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkVersionControl" runat="server" VertionId='<%# Eval("Id")%>' class='cbVersionControl' />
                        <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Eval("Id")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="10%">
                    <HeaderTemplate>Release Date</HeaderTemplate>
                    <ItemTemplate><%# Eval("ReleaseDate",  "{0:MM\/dd\/yyyy}")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="20%">
                    <HeaderTemplate>Application</HeaderTemplate>
                    <ItemTemplate><%# Eval("AppName")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="5%">
                    <HeaderTemplate>Stage</HeaderTemplate>
                    <ItemTemplate><%# Eval("StageName")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="6%">
                    <HeaderTemplate>Revision</HeaderTemplate>
                    <ItemTemplate><%# Eval("ActualRevision")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="">
                    <HeaderTemplate>Version</HeaderTemplate>
                    <ItemTemplate><%# Eval("Version")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="13%">
                    <HeaderTemplate>Code updated by</HeaderTemplate>
                    <ItemTemplate><%# Eval("CodeUpdatedBy")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="12%">
                    <HeaderTemplate>QC approved by</HeaderTemplate>
                    <ItemTemplate><%# Eval("QCApprovedBy")%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>Release Note</HeaderTemplate>
                    <ItemTemplate>
                        <%# If(Eval("ReleaseNote").ToString().Length > 18, Left(Eval("ReleaseNote"), 18) + "...", Eval("ReleaseNote"))%>
                        <asp:LinkButton ID="ReadMoreLinkButton" Text="More.." CssClass="releaseNoteLink" runat="server" Visible='<%# IIf(Eval("ReleaseNote").ToString().Length > 18, "True", "False")%>' OnClientClick="loadReleaseNote(this);return false;"></asp:LinkButton>
                        <span style="display: none;" class="spReleaseNote"><%# Eval("ReleaseNote")%></span>
                        <div id="dynamicPopUp" class="popupbox" style="display: none">
                            <span class="close-btn" title="Close">&times</span>
                            <div class="popupcontent"></div>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>

        <div style="font-weight: bold; width: 91%;">
            <span id="PageCount" runat="server" style="float: left"></span><span id="SelectCount" class="SelectCount" runat="server" style="float: right"></span>
        </div>
    </div>

    <div class="addVersionContainer">
        <table class="outerTable">
            <tr class="outerTableTr">
                <td class="outerTableTd">
                    <table class="innerTable">
                        <tr>
                            <td class="innerTableTdLabel">
                                <asp:Label class="lblReleaseDate" runat="server" Text="Release Date"></asp:Label></td>
                            <td class="innerTableTdControl">
                                <input type="date" id="txtReleaseDate" name="releaseDate" class="txtField txtReleaseDate" /></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label class="lblBuild" runat="server" Text="Build"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtBuild" runat="server" CssClass="txtField txtBuild"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label class="lblMajor" runat="server" Text="Major"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtMajor" runat="server" CssClass="txtField txtMajor"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label class="lblActualRevision" runat="server" Text="Actual Revision"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtActualRevision" runat="server" CssClass="txtField txtRevision"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label class="lblTrimRevision" runat="server" Text="Trim Revision"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtTrimRevision" runat="server" CssClass="txtField txtRevision"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label class="lblCodeUpdatedBy" runat="server" Text="Code updated by"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtCodeUpdatedBy" runat="server" CssClass="txtField txtCodeUpdatedBy"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
                <td class="outerTableTd">
                    <table class="innerTable">
                        <tr>
                            <td class="innerTableTdLabel">
                                <asp:Label class="lblApplication" runat="server" Text="Application"></asp:Label></td>
                            <td class="innerTableTdControl">
                                <asp:DropDownList ID="ddlApplication" CssClass="ddlField ddlApplication" runat="server"></asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label class="lblStage" runat="server" Text="Stage"></asp:Label></td>
                            <td>
                                <asp:DropDownList ID="ddlStage" CssClass="ddlField ddlStage" runat="server"></asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label class="lblMinor" runat="server" Text="Minor"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtMinor" runat="server" CssClass="txtField txtMinor"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label class="lblHasDBChanges" runat="server" Text="Has DB changes"></asp:Label></td>
                            <td>
                                <asp:DropDownList ID="ddlHasDBChanges" runat="server" CssClass="ddlField ddlHasDBChanges">
                                    <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Yes"></asp:ListItem>
                                    <asp:ListItem Text="No"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label class="lblQCApprovedBy" runat="server" Text="QC approved by"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtQCApprovedBy" runat="server" CssClass="txtField txtQCApprovedBy"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="outerTableTr outerTableTrNote">
                <td colspan="2" style="width: 100%;">
                    <table style="width: 100% !important;">
                        <tr>
                            <td style="width: 16.5%;">
                                <asp:Label class="lblNote" runat="server" Text="Release Note"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtReleaseNote" CssClass="txtReleaseNote" TextMode="MultiLine" Rows="6" runat="server"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
                <td></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="btnSave" CssClass="btnSave" runat="server" Text="Save" />
                </td>
            </tr>
        </table>
    </div>

    <div class="releaseNoteContainer">
        <table style="width: 100% !important;">
            <tr>
                <td>
                    <div class="txtAllReleaseNote"></div>
                    <textarea style="display:none" id="txtCopyToClipboard"></textarea>
                </td>
            </tr>
            <tr>
                <td>
                    <span id="messageBox" class="text-success"></span>
                    <div class="copyToClipboard" onclick="copyValueToClipBoard();">
                        <span>Copy To Clipboard</span>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
