﻿function __( o )
{
    if ( window.console )
        console.log( o );
}

function $$$( url, data, callback, errorCallback )
{
    $.ajax( {
        url: url,
        type: "POST",
        dataType: 'json',
        data: data,
        success: function ( data, status )
        {
            //			if (data.LoginStatus)
            //				if (data.LoginStatus == "401") {
            //					window.location.href = "/auth/?return=" + escape(window.location.pathname + window.location.search);
            //					return false;
            //				}
            //			if (data.LoginStatus == "403") {
            //				window.location.href = "/auth/?return=" + escape(window.location.pathname + window.location.search);
            //				return false;
            //			}
            if ( callback ) callback( data, status );
        },
        error: function ( XMLHttpRequest, textStatus, errorThrown )
        {
            if ( errorCallback ) errorCallback( XMLHttpRequest, textStatus, errorThrown )
        }
    } );
}

function $ds( keyword, data, callback )
{
    $$$( '/datasetup/' + keyword + '.jrq', data, callback );
}

function $admin( keyword, data, callback, errorCallback )
{
    $$$( '/admin/' + keyword + '.jrq', data, callback, errorCallback );
}

