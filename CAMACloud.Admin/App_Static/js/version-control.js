﻿var copyReleaseNote;
var Versions = [];
$(document).ready(function () {
    $(window).load(function () {
        var todayDate = new Date();
        var month = todayDate.getMonth() + 1;
        var day = todayDate.getDate();
        var year = todayDate.getFullYear();
        if (month < 10)
            month = '0' + month.toString();
        if (day < 10)
            day = '0' + day.toString();
        var maxDate = year + '-' + month + '-' + day;
        $('.txtSearchStartDate').attr('max', maxDate);
        $('.txtSearchEndDate').attr('max', maxDate);
        $('.txtReleaseDate').attr('max', maxDate);
    });


    //$(".txtSearchStartDate").change(function () {
    //    var startDate = $('.txtSearchStartDate').val();
    //    $('.txtReleaseDate').attr('min', startDate);
    //});

    $('.btnSearch').click(function () { //Search button click - data search and data vilidation
        var startDate = $('.txtSearchStartDate').val();
        var endDate = $('.txtSearchEndDate').val();
        var searchApplication = $('.ddlSearchApplication').val();
        var searchType = $('.ddlSearchStage').val();
        if (startDate == '' && endDate == '' && searchApplication == '0' && searchType == '0') { alert('Please select a search option'); return false; } //Null filed validation
        if (startDate !== '' && endDate !== '') { //Date field validation
            var start = new Date(startDate);
            var end = new Date(endDate);
            if (start > end) { alert('Start date cannot be greater than End date'); return false; }
        }

        if (startDate !== '' && endDate == '') {
            var todayDate = new Date();
            var month = todayDate.getMonth() + 1;
            var day = todayDate.getDate();
            var year = todayDate.getFullYear();
            if (month < 10)
                month = '0' + month.toString();
            if (day < 10)
                day = '0' + day.toString();
            var maxDate = year + '-' + month + '-' + day;
            $('.txtSearchEndDate').val(maxDate);
        } //End date field validation

        if (startDate == '' && endDate !== '') { alert('Please enter the Start date'); return false; } //Start date field validation
        Versions = [];
        $('.hfCount').val('');
        enableButton();

    });

    $('.txtBuild, .txtMajor, .txtMinor, .txtRevision').keydown(function (e) { //Only allow numbers in the text field
        -1 !== $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) || /65|67|86|88/.test(e.keyCode) && (!0 === e.ctrlKey || !0 === e.metaKey) || 35 <= e.keyCode && 40 >= e.keyCode || (e.shiftKey || 48 > e.keyCode || 57 < e.keyCode) && (96 > e.keyCode || 105 < e.keyCode) && e.preventDefault()
    });

    $('.btnSave').click(function () { //User date save button - data insertion and data validation
        var releaseDate = $('.txtReleaseDate').val();
        var build = $('.txtBuild').val();
        var major = $('.txtMajor').val();
        var revision = $('.txtRevision').val();
        var codeUpdatedBy = $('.txtCodeUpdatedBy').val();
        var Application = $('.ddlApplication option:selected').val();
        var Stage = $('.ddlStage option:selected').val();
        var minor = $('.txtMinor').val();
        var hasDBChanges = $('.ddlHasDBChanges option:selected').val();
        var qCApprovedBy = $('.txtQCApprovedBy').val();
        //var releaseNote   = $('.txtReleaseNote').val();  /*|| releaseNote === ''*/

        if (releaseDate === '' || build === '' || major === '' || revision === '' || Application === '0' || Stage === '0' || minor === '' || hasDBChanges === '0' || qCApprovedBy === '') {
            if (releaseDate === '') { alert('Release Date cannot be blank'); return false; }
            if (build === '') { alert('Build cannot be blank'); return false; }
            if (major === '') { alert('Major cannot be blank'); return false; }
            if (revision === '') { alert('Revision cannot be blank'); return false; }
            if (Application === '0') { alert('Application cannot be blank'); return false; }
            if (Stage === '0') { alert('Release Type cannot be blank'); return false; }
            if (minor === '') { alert('Minor cannot be blank'); return false; }
            if (hasDBChanges === '0') { alert('Has DB Changes cannot be blank'); return false; }
            if (qCApprovedBy === '') { alert('QC Approved By cannot be blank'); return false; }
            //if (releaseNote === '') { alert('Release Note cannot be blank'); return false; }
        }
    });

    $('.ddlApplication').change(function () {
        var idx = this.selectedIndex;
        if (idx != 0) {
            $.ajax({
                type: "POST",
                url: "versionControlSheet.aspx/setDefaultValue",
                data: "{ 'ApplicationId':'" + idx + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var str = JSON.parse(data.d);
                    if (str != null) {
                        $('.txtBuild').val(str.Build);
                        $('.txtMajor').val(str.Major);
                        $('.txtMinor').val(str.Minor);
                    }
                    else {
                        $('.txtBuild').val('');
                        $('.txtMajor').val('');
                        $('.txtMinor').val('');
                    }
                }
            });
        }
    });

    $('.close-btn').click(function () {
        $('.popupbox').fadeOut('fast');
    });
    enableButton();

    $("[id*=chkHeader]").live("click", function () {
        var chkHeader = $(this);
        var grid = $(this).closest("table");
        $("input[type=checkbox]", grid).each(function () {
            if (chkHeader.is(":checked")) 
                $(this).attr("checked", "checked");
            else 
                $(this).removeAttr("checked");
        });
        addOrRemoveList();
        enableButton();
    });

    $("[id*=chkVersionControl]").live("click", function () {
        var grid = $(this).closest("table");
        var chkHeader = $("[id*=chkHeader]", grid);
        if (!$(this).is(":checked"))
            chkHeader.removeAttr("checked");
        else {
            if ($("[id*=chkVersionControl]", grid).length == $("[id*=chkVersionControl]:checked", grid).length) {
                chkHeader.attr("checked", "checked");
            }
        }
        addOrRemoveList();
        enableButton();
    });

    $('.txtAllReleaseNote').bind('cut copy paste', function (e) {
        e.preventDefault(); //disable cut,copy,paste
    });

});

$(document).mouseup(function (e) {
    var container = $(".popupbox");
    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
        $('.popupbox').fadeOut('slow');
    }
});

function callDialogBox() { // Function for binding dialog box
    $('.txtField, .txtReleaseNote').val('');
    $('select.ddlField').val('0');

    var dialog = $('.addVersionContainer').dialog({
        modal: true,
        height: 'auto',
        width: '57%',
        top: '120.5px !important',
        resizable: false,
        title: "Add New Version"
    });

    dialog.parent().appendTo(jQuery('form:first')); //Setting the dialog containing Div to the start of the Form tag
}

function loadReleaseNote(source) {
    $('.popupbox').css("display", "none");
    var str = $(source).parent().find('.spReleaseNote').text();
    str = str.replace(/(?:\r\n|\r|\n)/g, '<br />');
    if (str != undefined && str != '') {
        var top;
        var left;
        var pos = [];
        var sourceTop = $(source).offset().top;
        var sourceLeft = $(source).offset().left;
        var windowWidth = $(window).width();
        var windowHght = $(window).height();
        $(source).parent().find('.popupcontent').html(str);
        $(source).parent().find('.popupbox').css("display", "inline-block");
        left = sourceLeft - 283;
        if ((windowHght - sourceTop) > 180) {
            top = sourceTop;
            $(source).parent().find('.popupbox').removeClass('changed');
        } else {
            top = sourceTop - 140;
            $(source).parent().find('.popupbox').addClass('changed');
        }
        pos[0] = top;
        pos[1] = left;
        $(source).parent().find('.popupbox').css({ 'top': pos[0] + 'px', 'left': pos[1] + 'px' });
    }
}
function addOrRemoveList() {
    $('.cbVersionControl input').each(function () {
        var c = this;
        var selectedVId = $(this).parent().attr("VertionId")
        if (c.checked) {
            if (Versions.indexOf(selectedVId) == -1) 
                Versions.push(selectedVId);
        }
        else {
            if (Versions.indexOf(selectedVId) != -1) 
                Versions.splice(Versions.indexOf(selectedVId.toString()), 1)
        }
    });
    $('.hfCount').val(Versions);
    $('.SelectCount').text("Selected Records: " + Versions.length);
}

function enableButton() {
    var vc = [];
    var cc = $('.hfCount').val();
    if (cc)
        vc = cc.split(',');
    var count = vc.length;
    if (count > 0) 
        $('.btnGetReleaseNote').attr('disabled', false);
    else 
        $('.btnGetReleaseNote').attr('disabled', true);
}

function selectedVersions() {
    Versions = $('.hfCount').val().split(',');
    $('.SelectCount').text("Selected Records: " + Versions.length);
}

function getSelectedVersions() {
    $('.cbVersionControl  input').each(function () {
        var c = this;
        var vId = $(this).parent().attr("vertionid");
        if (Versions.indexOf(vId) != -1) 
            c.checked = true;
        else 
            c.checked = false;
    });
    if ($(".cbVersionControl input:checkbox:not(:checked)").length == 0)
        $("[id*=chkHeader]").attr("checked", "checked");
}

function GetAllReleaseNotes() { // Function for get All Release Notes
    var message;
    var count = 0;
    var selectedVersions = [];
    var cc = $('.hfCount').val();
    $('.imgCopiedToClipboard').attr('checked', false);
    if (cc)
    selectedVersionIds = cc.split(',');
    $.each(selectedVersionIds, function (key, value) {
        var selectedVersionId = value
        var version = selectedVersionId.split(",").pop();
        if (version != "")
            count = count + 1;
    });
    if (count > 0) {
        $.ajax({
            type: "POST",
            url: "versionControlSheet.aspx/getAllReleaseNote",
            data: "{ 'VersionControlIds':'" + selectedVersionIds + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var str = data.d;
                callAllReleaseNoteDialogBox()
                $('.copytextarea').val(str);
                $('.txtAllReleaseNote').empty();
                $('.txtAllReleaseNote').children().remove();
                $('.txtAllReleaseNote').append("<ol id='newList'></ol>");
                var s = '';
                $.each(str, function (index, value) {
                    $("#newList").append("<li>" + value + ".</li>");
                    if (index != str.length - 1) 
                        s += value + '.\n';
                    else
                        s += value + '.';
                });
                copyReleaseNote = '';
                copyReleaseNote = s;
            }
        });
    }
}

function callAllReleaseNoteDialogBox() { // Function for binding All Release Note dialog box
    $('.txtAllReleaseNote').val('');
    var dialog = $('.releaseNoteContainer').dialog({
        modal: true,
        height: 'auto',
        width: '57%',
        top: '120.5px !important',
        resizable: false,
        title: "Release Notes"
    });
    dialog.parent().appendTo(jQuery('form:first')); //Setting the dialog containing Div to the start of the Form tag
}

function copyValueToClipBoard() {
    var $temp = $("<textarea>");
    $("body").append($temp);
    $temp.val(copyReleaseNote).select();
    document.execCommand("copy");
    $temp.remove();
    $('#messageBox').text("Copied to Clipboard").show().fadeOut(1500);
};