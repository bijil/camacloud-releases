﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class AdminReportViewer
    
    '''<summary>
    '''exportOptions control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents exportOptions As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''ddlPageSize control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlPageSize As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''ibtnExportToExcel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ibtnExportToExcel As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''reportHead control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents reportHead As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''lblReportHead control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblReportHead As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''reportViewer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents reportViewer As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''gvCustomReport control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents gvCustomReport As Global.System.Web.UI.WebControls.GridView
End Class
