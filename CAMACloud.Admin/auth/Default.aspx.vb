﻿Imports CAMACloud

Partial Class _Default
	Inherits System.Web.UI.Page

	'Protected Sub Login_Authenticate(sender As Object, e As System.Web.UI.WebControls.AuthenticateEventArgs) Handles Login.Authenticate
	'	Throw New Exception(Membership.ApplicationName)
	'End Sub

	Protected Sub Login_LoggedIn(sender As Object, e As System.EventArgs) Handles Login.LoggedIn
		Dim returnUrl As String = FormsAuthentication.DefaultUrl
		If Request("ReturnUrl") <> "" Then
			returnUrl += Request("ReturnUrl").TrimStart("/")
        End If

        'Session("loggedUser") = Login.UserName

        Response.Redirect(returnUrl)
	End Sub

	Protected Sub Login_LoginError(sender As Object, e As System.EventArgs) Handles Login.LoginError
        divError.InnerText = "Your Log in attempt failed. Please try again."
		divError.Visible = True
	End Sub

	Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Membership.GetAllUsers.Count = 0 Then
                Membership.CreateUser("admin", "Reset123#", "admin@camacloud.com")
                Alert("New user created with default password.")
            End If

            If Membership.GetAllUsers.Count = 0 Then
                Membership.CreateUser("support", "Help@247", "support@camacloud.com")
                Alert("New user created with minimum access.")
            End If

            If Membership.GetUser("admin") Is Nothing Then
                CloudUserProvider.VerifyOrganizationUsers()
            Else
                If Membership.GetUser("support") Is Nothing Then
                    Dim newUser = Membership.CreateUser("support", "Help@247", "adminsupport@camacloud.com")

                    If Roles.IsUserInRole("support", "support") Then
                        Roles.RemoveUserFromRole("support", "support")
                        Roles.AddUserToRole("support", "support")
                    Else
                        Roles.AddUserToRole("support", "support")
                    End If

                    If Database.System.GetIntegerValue("SELECT COUNT(*) FROM AdminUserSettings WHERE LoginId = 'support'") = 0 Then
                        Database.System.Execute("INSERT INTO AdminUserSettings (LoginId, FirstName) VALUES ('support', 'support')")
                    End If
                Else
                    If Not Roles.IsUserInRole("support", "support") Then
                        Roles.AddUserToRole("support", "support")
                    End If
                End If
            End If
        End If
    End Sub

    Protected Sub Login_Click(sender As Object, e As System.EventArgs)

    End Sub
End Class
