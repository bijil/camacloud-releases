﻿<%@ Page Title="CAMA Cloud&#174; - Login" Language="VB" MasterPageFile="~/App_MasterPages/SecurityPage.master"
	AutoEventWireup="false" Inherits="CAMACloud.Admin._Default" Codebehind="Default.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
 <meta name="viewport" content="user-scalable=no,initial-scale=1.0,maximum-scale=1.0" />
	<meta name="apple-mobile-web-app-capable" content="no"/>
	<head>
<script>
function doLogin(logoutCall){
			if(logoutCall){
				$('#agreement').hide();
				$('#MainContent_divError').show();
				$('#Footer_LkChgOrg').show();
				$('#loginContent').show();
			} else {
				if($('#MainContent_Login_UserName').val() != "" && $('#MainContent_Login_Password').val()  != ""){
					$('#loginContent').hide();
					$('#MainContent_divError').hide();
					$('#Footer_LkChgOrg').hide();
					$('#agreement').show();
					$('.License-Agreement').scrollTop(0);
				}
			}
		}
</script>
</head>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="error" runat="server" id="divError" visible="false">
	</div>
	<asp:Login runat="server" ID="Login">
		<LayoutTemplate>
			<div id="loginContent">
				<div class="login-inputs">
					<div class="input-box">
						<asp:TextBox ID="UserName" runat="server" CssClass="text" watermark="Your Login ID" />
						<asp:RequiredFieldValidator ID="UsernameValidate" runat="server" ErrorMessage="*" ControlToValidate="UserName"  ForeColor="Red"></asp:RequiredFieldValidator> 
					</div>
					<div class="input-box">
						<asp:TextBox ID="Password" TextMode="Password" runat="server" CssClass="text" watermark="Password" />
						<asp:RequiredFieldValidator ID="PasswordValidate" runat="server" ErrorMessage="*" ControlToValidate="Password"  ForeColor="Red"></asp:RequiredFieldValidator>
					</div>
				</div>
				<div style="text-align: center;">
					<asp:LinkButton runat="server" CssClass="login-button" ID="ProceedLogin" OnClientClick="doLogin(); return false;"><span>Log in</span></asp:LinkButton>
				</div>
			</div>
		<div id="agreement" style="display: none;">
		<div class="text-content" style="padding: 10px 0px;">
		    <h1 style="text-align: center; color:white;">
		        Disclaimer</h1>
		   <div class="License-Agreement" style="height: 180px;">
		    <P>
		     The CC Admin Site is extremely confidential and sensitive to our operations.
		     Use extreme caution and attention to detail when navigating throughout the site.</p>
 
			<p>Peer review is required for any license management and user access (creating, deleting, revoking). 
				Any license management and user access includes,
				but not limited to, customer licenses, vendor licenses, API licenses, CC Admin user account access.
		    </P>
		        </div>
		</div>
		<div style="text-align: center;">
			<asp:LinkButton runat="server" CssClass="login-button" ID="Login" CommandName="Login" onclick="Login_Click"><span> I Agree</span></asp:LinkButton>
			<asp:LinkButton runat="server" CssClass="login-button" ID="Logout" OnClientClick="doLogin(true); return false;"><span>Logout</span></asp:LinkButton>
		</div>
		</div>
		</LayoutTemplate>
	</asp:Login>
</asp:Content>
<asp:Content ContentPlaceHolderID="Footer" runat="server">
	<div>
		<strong>
			<asp:Label runat="server" ID="lblOrgName" /></strong>
	</div>
</asp:Content>
