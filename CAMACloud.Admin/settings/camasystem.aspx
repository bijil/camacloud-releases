﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" CodeBehind="camasystem.aspx.vb" Inherits="CAMACloud.Admin.camasystem" MasterPageFile="~/App_MasterPages/ClientAdmin.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <style type="text/css">

        .btnCAMASystem {
            width: 144px;
            margin-left: 5px; 
            float: right;
            margin-right:10px;
            margin-top:10px;
        }
        .widthStyle{
           width:254px
        }
        
        .masklayer {
		    background: black;
		    opacity: 0.5;
		    top: 0%;
		    width: 100%;
		    height: 100%;
		    bottom: 0%;
		    position: absolute;
		    display: none;
		}

        .btnEdit:hover img, .btnDelete:hover img{       
        transform: scale(1.1);
        }

        .btnEdit, .btnDelete{  
        cursor : pointer;
        }

        .dataTables_wrapper {
            min-height : 400px;
            }

        .btnManageCAMA {
            width: 40%;
            height: 100%;
            padding: 8px 0 0 8px;
        }

        @media (min-width:1000px) and (max-width:1500px) {
            .btnManageCAMA {
                width: 50%;
            }
        }


    </style>

    <link href="/App_Static/css/datatables/jquery.dataTables.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src = "/App_Static/js/datatables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src = "/App_Static/js/datatables/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src = "/App_Static/js/datatables/buttons.html5.min.js"></script>
    <script type="text/javascript" src = "/App_Static/js/datatables/moment.min.js"></script>
    <script type="text/javascript" src="/App_Static/jslib/jszip.min.js"></script>

  
    <script type="text/javascript">
        var CamaDatatable;
        $(function () {
            showDialogBox();
            GetCamaData();

        });


        GetCamaData = (callback) => {
            $.ajax({
                url: "camasystem.aspx/GetCamaRecords",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    $('#gvCamaSystem').dataTable().fnDestroy();          
                    GetDataTable(data, callback);
                }
            });
        }

        GetDataTable = (data, callback) => {
            CamaDatatable = $('#gvCamaSystem').DataTable({
                "data": data.d,
               /* "iDisplayStart": "1",*/
                //"sScrollY": "350px",
                //"bPaginate": false,
                //"bJQueryUI": true,
                "select": true,
                "columns": [
                    {
                        'data': 'Id',
                        'searchable': false,
                        'sortable': false,
                        visible: false
                    },
                    {
                        'data': 'VId',
                        'searchable': false,
                        'sortable': false,
                        visible: false
                    },
                    {
                        'data': 'CamaSystem',
                        'searchable': true,
                        'sortable': true,
                        "className": "dt-left",
                        'width': '30%'
                    },

                    {
                        'data': 'VendorName',
                        'searchable': false,
                        'sortable': true,
                        "className": "dt-left",
                        'width': '50%'
                    },
                    {
                        'data': null,
                        'width': '10%',
                        mRender: (data, type, row) => {
                            return '<a class="btnEdit"  data-id="' + row.Id + '"  onclick="ButtonEdit(' + row.Id + ')" ><img src="/App_Static/css/icon16/editDt.svg" alt="edit"></a>'
                        }
                    },
                    {
                        'data': null,
                        'width': '10%',
                        mRender: (data, type, row) => {
                            return '<a class="btnDelete" data-id="' + row.Id + '" onclick="ButtonDelete(' + row.Id + ')" ><img src="/App_Static/css/icon16/deleteDt.svg" alt="edit"></a>'
                        }
                    }
                ],
            });
            $('#gvCamaSystem_length').hide();
            $('#gvCamaSystem tbody').off('click');
            $('#gvCamaSystem tbody').on('click', 'tr', function () {
                //event && event.preventDefault();
                if ($(this).hasClass('selected')) {
                    $(this).removeClass('selected');
                } else {
                    CamaDatatable.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            });

            //CamaDatatable.columns.adjust().draw();
            if (callback) callback();
        }

        ButtonEdit = (camaid) => {
            var CamaUpdatedVal = CamaDatatable.data().filter((x) => { return x.Id == camaid })[0].CamaSystem;
            var DDVendorValue = CamaDatatable.data().filter((x) => { return x.Id == camaid })[0].VId;
            $(".btnCancel").prop('value', 'Cancel');
            $('.ddlVendor').prop('selectedIndex', DDVendorValue);
            $('.ddlVendor').prop('disabled', true);
            $('.txtCAMASystem').val(CamaUpdatedVal);
            $('.txtCAMASystem').focus();
            $('.btnSave').prop('disabled', false);
            $('.btnAdd').prop('disabled', true);
            localStorage.setItem('CamaSmId', camaid);
        }

        ButtonDelete = (camaid) => {
            $('.ddlVendor').prop('disabled', false);
            $('.btnAdd').prop('disabled', false);
            $('.btnSave').prop('disabled', true);
            $('.txtCAMASystem').val("");

            var ans = confirm('Are you sure you wish to remove this record?');
            if (ans) {
                var rowIndexes = ''
                CamaDatatable.rows(function (idx, data, node) {
                    if (data.Id === camaid) {
                        rowIndexes = idx;
                    }
                    return false;
                });
                if (rowIndexes != '') {
                    CamaDatatable.row(rowIndexes).remove().draw();
                }
                $admin('deletecamasystem', { CamaId: camaid }, function (data) {
                    if (data.message) {
                        alert(data.message)
                    }
                    else {
                        alert('CAMA System record is removed');
                    }
                });
            }
            return false;
        }

        showDialogBox = () => {
            $('.txtCAMASystem').val("");
            $('.btnAdd').prop('disabled', false);
            $('.btnSave').prop('disabled', true);

            $admin('getvendordetails', null, function (data) {
                var ddlVendor = $("[id*=ddlVendor]");
                ddlVendor.empty().append('<option selected="selected" value="0">Please select</option>');
                data.forEach(function (d) {
                    ddlVendor.append('<option value=' + d.Id + '>' + d.Name + '</option>');
                })
            });
            return false;
        }

        SaveCamaSystem = () => {
            var camaSystem = $('.txtCAMASystem').val();
            var CamaId = localStorage.getItem('CamaSmId');
            if (camaSystem == '') {
                alert('Please enter a valid Cama System');
                return false;
            };
            showMask(this);
            $admin('savecamasystem', { CamaId: CamaId, camaSystem: camaSystem }, function (data) {
                if (data.message) { alert(data.message) }
                else alert('Cama System Updated Successfully');
                
                GetCamaData(() => {
                    hideMask();
                });
            });

        }

        RemoveCamasystem = () => {
            $('.txtCAMASystem').val("");
            $(".btnCancel").prop('value', 'Clear');
            CamaDatatable.$('tr.selected').removeClass('selected');
            $('.ddlVendor').prop('disabled', false);
            $('.btnAdd').prop('disabled', false);
            $('.btnSave').prop('disabled', true);
            return false;
        }

        function validation(el) {
            var isValid = true;
            if ($(el).hasClass('required')) {
                if ($(el).val() == null || $(el).val() == '' || ($(el).hasClass('ddlVendor') && $(el).val() == 0)) {
                    if ($(el).next(".red").length == 0) {
                        $(el).after('<span class="red" style="color:Red;font-weight: bolder;font-size: large;position: absolute;">*</span>');
                    }
                    isValid = false;
                }
                else {
                    $(el).next(".red").remove();
                }
            }
            return isValid;
        }

        function confirmAlert() {
            if (validation($( '#<%=ddlVendorName.ClientID %>')) &&  validation( $( '.txtCAMASystem' ) )  ){
                var vendorId= $( '#<%=ddlVendorName.ClientID %>').val();
                var camaSystem = $('.txtCAMASystem').val();

                $admin('addcamasystem', { vendorId: vendorId, camaSystem: camaSystem }, function (data) {
                    if (data.message) { alert(data.message) }
                    else {
                        alert('Created Successfully');
                        GetCamaData();
                    }
                });          
                return false;
            }
            else return false;
        }

        function showMask(info) {
            $('.masklayer').height($(document).height())
            $('.masklayer').show();
            info = 'Please wait ...'
        }
    </script>

</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <h1>Manage CAMA Systems</h1>  
    <div class="SettingsContainer">
        <table class="btnManageCAMA">
            <tr>
                <td style="margin-left: 8px;">
                    <table style="margin-bottom: 10px;padding: 10px;width:100%;border: 1px solid #cfcfcf;">
                        <tr class="CAMASystem">
                            <td>Vendor Name <strong>:</strong></td>
                            <td>
                                <asp:DropDownList ID="ddlVendorName" CssClass="ddlVendor required" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="CAMASystem">
                            <td>CAMA System <strong>:</strong></td>
                            <td>
                                <asp:TextBox ID="txtCAMASystem" Class="txtCAMASystem validator required widthStyle" runat="server" autocomplete="off"></asp:TextBox>
                            </td>
                        </tr>

                    </table>
                    <div id="btnContainer" style="margin-bottom: 10px;">                                     
                        <asp:Button runat="server" ID="btnAdd" CssClass="btnAdd" OnClientClick="return confirmAlert()" onchange="showMask(this)" Text="Add CAMA System" />
                        <asp:Button runat="server" ID="btnSave" CssClass="btnSave" OnClientClick="return SaveCamaSystem()" Text="Save" />
                        <asp:Button runat="server" ID="btnCancel" CssClass="btnCancel" OnClientClick="return RemoveCamasystem()" Text="Clear" />
                    </div>
                    <table id="gvCamaSystem" class="row-border" style="width:100%; border-collapse: collapse">
                        <thead>
                            <tr>
                                <th>Id</th>
                                 <th>VId</th>
                                <th>CAMA System</th>
                                <th>Vendor</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>

                    </table>    
                </td>
            </tr>   
        </table>
    </div>    
</asp:Content>


