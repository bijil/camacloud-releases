﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/ClientAdmin.master"
    CodeBehind="MassUpdateQueue.aspx.vb" Inherits="CAMACloud.Admin.MassUpdateQueue" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>
        Mass Update - Process Pending Queue</h1>

    <div>
                        <asp:Label runat="server" ID="lblStatus" Font-Bold="true" /><br />
                    <br />
                    <asp:Label runat="server" ID="lblError" ForeColor="Red" />
    </div>
    <asp:GridView runat="server" ID="gridPending">
        <Columns>
            <asp:BoundField HeaderText="#" DataField="Id" ItemStyle-Width="25px" DataFormatString="{0}." />
            <asp:TemplateField HeaderText="Date">
                <ItemStyle Width="90px" />
                <ItemTemplate>
                    <%# Eval("EventDate", "{0:MMM d, yyyy}")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField HeaderText="Updated By" DataField="UpdatedBy" ItemStyle-Width="120px" />
            <asp:BoundField HeaderText="Notes" DataField="Notes" ItemStyle-Width="250px" />
            <asp:TemplateField>
                <ItemStyle Width="120px" />
                <ItemTemplate>
                    <asp:LinkButton ID="lnkBtnUpdate" runat="server" Text="Update Pending" CommandName="UpdatePending" CommandArgument='<%# Eval("Id") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemStyle Width="90px" />
                <ItemTemplate>
                    <asp:LinkButton ID="lnlBtnUpdateAll" runat="server" Text="Update All" CommandName="UpdateAll" CommandArgument='<%# Eval("Id") %>'
                        OnClientClick="return confirm('Are you sure you want to update all pending environments with this update?')" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:Panel runat="server" ID="pnlUpdatePending" Visible="false">
        <asp:HiddenField runat="server" ID="hdnUpdateId" />
        <div style="font-weight: bold;">
            Select environment hosts to update:</div>
        <div style="padding: 3px 15px">
            <asp:CheckBoxList runat="server" ID="chlHosts" />
        </div>
        <table cellspacing="0" cellpadding="0">
            <tr>
                <td>
                    <asp:Button runat="server" ID="btnUpdate" Text=" Update Environments " ValidationGroup="Enqueue" />
                    <asp:Button runat="server" ID="btnCancel" Text=" Cancel " />
                </td>
                <td>

                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
