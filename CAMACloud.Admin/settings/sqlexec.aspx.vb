﻿Public Class sqlexec
    Inherits System.Web.UI.Page
    Dim OrgName As New List(Of String)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            chlHosts.DataSource = Database.System.GetDataTable("SELECT * FROM ApplicationHost")
            chlHosts.DataValueField = "Id"
            chlHosts.DataTextField = "Name"
            chlHosts.DataBind()
            countyList.DataSource = Database.System.GetDataTable("SELECT * FROM Organization")
            countyList.DataValueField = "Id"
            countyList.DataTextField = "Name"
            countyList.DataBind()
            chlCAMASystem.DataSource = Database.System.GetDataTable("SELECT * FROM CAMASystem")
            chlCAMASystem.DataValueField = "Id"
            chlCAMASystem.DataTextField = "Name"
            chlCAMASystem.DataBind()
            chlHosts.Visible = False
            countyList.Visible = False
            chlCAMASystem.Visible = False
        End If
    End Sub

    Function ValidateSQL(sql As String) As Boolean
        Dim sqlLower As String = sql.ToLower

        If sqlLower.Contains("create") Or sqlLower.Contains("alter") Then

        Else
            Alert("The SQL should contain CREATE or ALTER statements. Please avoid using this for data updates, as it would affect all environments together.")
            Return False
        End If

        Return True
    End Function

    Private Sub btnUpdate_Click(sender As Object, e As System.EventArgs) Handles btnUpdate.Click
        lblStatus.Text = ""
        lblError.Text = ""

        Dim updateId As Integer

        If txtSql.Text.Trim = "" Then
            Return
        End If

        Dim sqls() As String = GetSQLBatches(txtSql.Text)

        For Each ssql As String In sqls
            If Not ValidateSQL(ssql) Then
                Return
            End If
        Next

        Dim targetFilter As String = "1 = 1"
        Dim pendingFilter As String = "1 = 0"
        Select Case rblTarget.SelectedValue
            Case "S"
                Dim hostIds As String = "0"
                For Each li As ListItem In chlHosts.Items
                    If li.Selected Then
                        hostIds.Append(li.Value, ", ")
                    End If
                Next

                targetFilter = "ApplicationHostId IN (" + hostIds + ")"
                pendingFilter = "Id NOT IN (" + hostIds + ")"
            Case "D"
                targetFilter = "ApplicationHostId IN (SELECT Id FROM ApplicationHost WHERE IsProduction = 0)"
                pendingFilter = " IsProduction = 1"
            Case "T"
                targetFilter = "DBName like 'TEST_sample%'"
            Case "SB"
                targetFilter = "(Name LIKE '%sandbox%' OR DBName LIKE '%sandbox%' OR DBName LIKE '%$_SB%' ESCAPE '$')"
            Case "E"
                targetFilter = "Id IN (" + selectedcountyList.Value + ")"
            Case "C"
                targetFilter = "CAMASystem IN (" + CAMASystemList.Value + ")"
        End Select

        If Not chkDoNotQueue.Checked Then
            Dim sqlUpdate As String = "INSERT INTO EnvironmentUpdates (UpdatedBy, Notes, UpdateSQL) VALUES ({0}, {1}, {2}); SELECT @@IDENTITY As NewId".SqlFormatString(txtUpdatedBy.Text, txtNotes.Text+"-"+rblTarget.SelectedItem.Text, txtSql.Text)
            updateId = Database.System.GetIntegerValue(sqlUpdate)

            Dim sqlPending As String = "INSERT INTO EnvironmentUpdatesPending (EnvironmentUpdateId, ApplicationHostId) SELECT " & updateId & ", Id FROM ApplicationHost WHERE " + pendingFilter
            Database.System.Execute(sqlPending)

            lblStatus.Text = "The query has been queued successfully. Update ID: " & updateId & "<br/>"

            txtUpdatedBy.Text = ""
            txtNotes.Text = ""
        End If

        If chkQueueOnly.Checked Then
            Return
        End If

        Dim success As Integer = 0
        Dim failed As Integer = 0
        Dim lastError As String = ""

        Dim orgs As DataTable = Database.System.GetDataTable("SELECT * FROM Organization WHERE " + targetFilter)
        ExecuteSQL(orgs, sqls, False, success, failed, lastError)


        If success > 0 Then
            lblStatus.Text += success & " environments updated successfully. <br/>"
        End If
        If failed > 0 Then
            lblStatus.Text += failed.ToString + " environments failed. <br/>"
            For Each Name In OrgName
                lblStatus.Text += Name + ", "
            Next
        End If

        If failed > 0 Then
            lblError.Text = lastError
        End If

    End Sub


    Sub ExecuteSQL(orgs As DataTable, sqls() As String, useDemo As Boolean, ByRef success As Integer, ByRef failed As Integer, ByRef lastError As String)
        For Each org As DataRow In orgs.Rows
            Dim Dbname As String = org.GetString("DBName")
            Dim connStr As String = Database.GetConnectionStringFromOrganization(org.GetInteger("Id"), useDemo)

            Try
                Dim conn As New SqlClient.SqlConnection(connStr)
                conn.Open()
                For Each ssql As String In sqls
                    Dim cmd As New SqlClient.SqlCommand(ssql, conn)
                    cmd.ExecuteNonQuery()
                Next
                conn.Close()
                success += 1
            Catch sqlex As SqlClient.SqlException
                failed += 1
                lastError = sqlex.Message
                OrgName.Add(Dbname)
            Catch ex As Exception
                failed += 1
            End Try
        Next

    End Sub

    Function GetSQLBatches(sql As String) As String()
        Dim batches As New List(Of String)
        Dim currentSql As String = ""
        Dim sqlLines As String() = sql.Split(vbLf)
        For Each line In sqlLines
            If line.Trim.ToLower = "go" Then
                batches.Add(currentSql)
                currentSql = ""
            Else
                currentSql += line + vbLf
            End If
        Next
        If currentSql <> "" Then
            batches.Add(currentSql)
        End If
        Return batches.ToArray
    End Function

    Private Sub rblTarget_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles rblTarget.SelectedIndexChanged
        Select Case rblTarget.SelectedValue
            Case "*"
                chlHosts.Visible = False
                countyList.Visible = False
                chlCAMASystem.Visible = False
            Case "S"
                chlHosts.Visible = True
                countyList.Visible = False
                chlCAMASystem.Visible = False
            Case "D"
                chlHosts.Visible = False
                countyList.Visible = False
                chlCAMASystem.Visible = False
            Case "T"
                chlHosts.Visible = False
                countyList.Visible = False
                chlCAMASystem.Visible = False
            Case "SB"
                chlHosts.Visible = False
                countyList.Visible = False
                chlCAMASystem.Visible = False
            Case "E"
                chlHosts.Visible = False
                countyList.Visible = True
                chlCAMASystem.Visible = False
            Case "C"
                chlHosts.Visible = False
                countyList.Visible = False
                chlCAMASystem.Visible = True
        End Select
    End Sub

    Private Sub chkQueueOnly_CheckedChanged(sender As Object, e As System.EventArgs) Handles chkQueueOnly.CheckedChanged
        If chkQueueOnly.Checked Then
            chkDoNotQueue.Checked = False
            chkDoNotQueue.Enabled = False
        Else
            chkDoNotQueue.Enabled = True
        End If
    End Sub

    Private Sub chkDoNotQueue_CheckedChanged(sender As Object, e As System.EventArgs) Handles chkDoNotQueue.CheckedChanged
        If chkDoNotQueue.Checked Then
            chkQueueOnly.Checked = False
            chkQueueOnly.Enabled = False
        Else
            chkQueueOnly.Enabled = True
        End If
    End Sub

End Class