﻿Partial Class App_MasterPages_DataSetup
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim Username = Membership.GetUser().ToString
        Dim organizationId As Integer = HttpContext.Current.GetCAMASession.OrganizationId
        If Not IsPostBack Then
            If Session("roleLink") Is Nothing Then 'Check the role - "Basic Settings" session created - { Starts }
                phBasicSettings.Visible = False

                If Database.Tenant.Application.IsAPISyncModel Then
                    phFileDataModel.Visible = False
                    phSyncDataModel.Visible = True
                Else
                    phFileDataModel.Visible = True
                    phSyncDataModel.Visible = False
                End If


                'If Username = "vendor-cds" Then
                '    liManageUser.Style("Display") = "none"
                'End If

                Dim check As Integer = Database.System.GetIntegerValue("SELECT COUNT(*) FROM aspnet_UsersInRoles uir INNER JOIN aspnet_Users au on uir.UserId = au.UserId INNER JOIN aspnet_Roles ar on uir.RoleId = ar.RoleId WHERE au.UserName = '" & Username & "' AND ar.RoleName = 'Support' AND au.UserName not in ('dcs-support', 'dcs-qa', 'dcs-rd', 'dcs-integration', 'dcs-ps')")

                If check > 0 Then
                    pcSupport.Visible = True
                    If ClientOrganization.HasModule(organizationId, "DTR") Then
                        pcDTR.Visible = True
                        Dim cv As Integer = Database.Tenant.GetIntegerValue("SELECT Value FROM ClientSettings WHERE Name = 'DTRWorkflowNew'")
                        If cv <> 1 Then
                            pworkflowtype.Style("Display") = "none"
                            pworkflowlist.Style("Display") = "none"
                        End If
                    End If
                    Dim o As DataRow = Database.System.GetTopRow("SELECT EnableXMLAuditTrail FROM OrganizationSettings WHERE OrganizationId = " & HttpContext.Current.GetCAMASession().OrganizationId)
                    liSettingsAuditTrailReport.Visible = IIf(o.GetBoolean("EnableXMLAuditTrail"), True, False)
                    If (o.GetBoolean("EnableXMLAuditTrail")) = False Then
                        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
                        If path.Contains("settingsaudittrailreport.aspx") Then
                            Response.Redirect("~/protected/datasetup/Default.aspx")
                        End If
                    End If
                Else
                    If Username.ToLower = "dcs-qa" Or Username.ToLower = "dcs-rd" Or Username.ToLower = "dcs-integration" Or Username.ToLower = "admin" Or Username.ToLower = "dcs-ps" Or Username.ToLower = "dcs-support" Then
                        pcSupport.Visible = True
                        If Username.ToLower = "dcs-rd" Then
                            For Each con As Control In ulSupport.Controls
                                If con.ID = "liprioritylistbackups" Or con.ID = "prioritylistbackups" Then
                                    con.Visible = False
                                Else
                                    con.Visible = True
                                End If
                            Next
                        End If
                        Dim o As DataRow = Database.System.GetTopRow("SELECT EnableXMLAuditTrail FROM OrganizationSettings WHERE OrganizationId = " & HttpContext.Current.GetCAMASession().OrganizationId)
                        liSettingsAuditTrailReport.Visible = IIf(o.GetBoolean("EnableXMLAuditTrail"), True, False)
                        If (o.GetBoolean("EnableXMLAuditTrail")) = False Then
                            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
                            If path.Contains("settingsaudittrailreport.aspx") Then
                                Response.Redirect("~/protected/datasetup/Default.aspx")
                            End If
                        End If
                    Else
                        pcSupport.Visible = False
                    End If
                    pcCompSales.Visible = ClientOrganization.HasModule(organizationId, "CompSales")
                    If ClientOrganization.HasModule(organizationId, "DTR") Then
                        pcDTR.Visible = True
                        Dim cv As Integer = Database.Tenant.GetIntegerValue("SELECT Value FROM ClientSettings WHERE Name = 'DTRWorkflowNew'")
                        If cv <> 1 Then
                            pworkflowtype.Style("Display") = "none"
                            pworkflowlist.Style("Display") = "none"
                        End If
                    End If

                    Dim rw As DataRow = Database.System.GetTopRow("SELECT ADEEnabled FROM Organization WHERE Id = " & HttpContext.Current.GetCAMASession().OrganizationId)
                    If rw.GetBoolean("ADEEnabled") = False AndAlso (Roles.RoleExists("CompSales") = False) Then
                        PCApplications.Visible = False
                    ElseIf (rw.GetBoolean("ADEEnabled") AndAlso (Roles.RoleExists("CompSales") = True)) Then
                        PCApplications.Visible = True
                    ElseIf (rw.GetBoolean("ADEEnabled") AndAlso (Roles.RoleExists("CompSales") = False)) Then
                        hlADE.Visible = True
                        hlComparables.Visible = False
                        liComb.Style("Display") = "none"
                        PCApplications.Visible = True
                    ElseIf (rw.GetBoolean("ADEEnabled") = False AndAlso (Roles.RoleExists("CompSales") = True)) Then
                        hlADE.Visible = False
                        liADE.Style("Display") = "none"
                        PCApplications.Visible = True
                        hlComparables.Visible = True
                    End If
                    'End If
                End If

                Dim default_ManagerUser As String() = {"admin", "dcs-support", "vendor-cds", "dcs-qa", "System", "dcs-rd", "malite_admin", "dcs-integration", "dcs-ps"} ''Enable Comparable Properties - Settings FD_21638 
                If default_ManagerUser.Contains(Username) AndAlso ClientOrganization.HasModule(organizationId, "CompSales") Then
                    pcCompSales.Visible = True
                End If

            Else 'Only for the role - "Basic Settings" - { Starts }
                Dim roleSession = Session("roleLink").ToString()
                If roleSession = "BasicSettings" Then
                    phFileDataModel.Visible = False
                    phSyncDataModel.Visible = True
                    phDataSetup.Visible = False
                    phBasicSettings.Visible = True
                End If
            End If
            '{ Ends }
        End If

        If Username.ToLower = "vendor-cds" Then
            pcSupport.Visible = True
            For Each con As Control In ulSupport.Controls
                If con.ID = "liclientSettings" Or con.ID = "clientSettings" Then
                    con.Visible = False
                Else If con.ID = "lisketchSettings" Or con.ID = "skecthSettings" Then
                	con.Visible = False
                Else
                    con.Visible = True
                End If                
            Next
        End If
        
        'malite_admin Exclude two options 'prioritylistbackups','clientSettings'
        
        If Username.ToLower = "malite_admin" Then   
            pcSupport.Visible = True
            For Each con As Control In ulSupport.Controls
                If con.ID = "liclientSettings" Or con.ID = "clientSettings" Or con.ID = "prioritylistbackups" Or con.ID = "liprioritylistbackups"  Then
                    con.Visible = False
                Else If con.ID = "lisketchSettings" Or con.ID = "skecthSettings" Then
                	con.Visible = False
                Else
                    con.Visible = True
                End If                
            Next 
        End If
        
        
        
        
        
        
        
        If Roles.IsUserInRole(Username, "BasicSettings") Then
            If (ClientOrganization.HasModule(organizationId, "DTR")) Then
                pcDTR.Visible = True
                pcCompSales.Visible = False
                Dim cv As Integer = Database.Tenant.GetIntegerValue("SELECT Value FROM ClientSettings WHERE Name = 'DTRWorkflowNew'")
                For Each con As Control In ulDTRSettings.Controls
                    If con.ID = "liParcelStatusList" Or con.ID = "hlParcelStatusList" Or con.ID = "liUserroles" Or con.ID = "hlUserroles" Or (con.ID = "pworkflowtype" And cv = "1") Or (con.ID = "pworkflowlist" And cv = "1") Then
                        con.Visible = True
                    Else
                        con.Visible = False
                    End If
                Next
            Else
                pcDTR.Visible = False
            End If

            ''''''Miscellaneous for BasicSettings
            For Each con As Control In ulDataSettings.Controls
            	'If con.ID = "liMiscellaneous" Or con.ID = "hlMiscellaneous" Then
            	If con.ID = "liAnnual" Or con.ID = "ds_hl_annualProcess" Or con.ID = "liSearchOptions" Or con.ID = "hlSearchOptions" Or con.ID = "liAutoPriorityRule" Or con.ID = "h1AutoPriorityRule" Then
                    con.Visible = True
                Else
                    con.Visible = False
                End If
            Next
            
            
            
        End If
        Dim value As Integer = Database.Tenant.GetIntegerValue("SELECT Value FROM ClientSettings WHERE Name = 'EnableAutoPriority'")
            If value<> 1 Then
            	For Each con As Control In ulDataSettings.Controls
	                If con.ID = "liAutoPrioritySettings" Or con.ID = "h1AutoPrioritySettings" Or con.ID = "liAutoPriorityRule" Or con.ID = "h1AutoPriorityRule" Or con.ID = "liAutoPriorityLog" Or con.ID = "h1AutoPriorityLog" Then
	                    con.Visible = false
	                End If
            	Next
            End If
    End Sub
End Class

