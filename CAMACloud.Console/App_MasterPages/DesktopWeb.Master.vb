﻿Public Class DesktopWeb
    Inherits System.Web.UI.MasterPage

    Dim basePageTitle As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Membership.ApplicationName <> HttpContext.Current.GetCAMASession.TenantKey Then
            CAMASession.Logout(HttpContext.Current)
            Exit Sub
        End If

        basePageTitle = HttpContext.Current.GetCAMASession.OrganizationName + " :: CAMA Cloud&#174;"

        If Not IsPostBack Then
			Dim Username = Membership.GetUser().ToString ''added condition to check role assigned to user
			Dim allowedRoles As String = ConfigurationManager.AppSettings("AllowedRole")
			If allowedRoles <> "" Then
                Dim restricted As Boolean = True
                For Each allowedRole In allowedRoles.Split(",")
                    allowedRole = allowedRole.Trim
                    If allowedRole <> "" Then
                        If Roles.IsUserInRole(allowedRole) Then
                            restricted = False
                            Exit For
                        End If
                    End If
                Next
                If restricted Then
                    Response.Redirect("~/web/403.aspx")
                End If
            End If
            LoadMainMenu()
        End If

    End Sub

    Public Sub LoadMainMenu()
        Dim strAppRoleId As String = System.Configuration.ConfigurationManager.AppSettings("AppRoleId").ToString()
        Dim strhtmlCntntsModule As String = ""
        Dim strhtmlCntntsMenu As String = ""
        Dim Username = Membership.GetUser().ToString
        Dim ApplicationName = Membership.ApplicationName
        Dim dtModule As DataTable = Database.System.GetDataTable("SELECT ar.Name, ar.Path, ar.Id FROM aspnet_Users u INNER JOIN aspnet_Applications a ON u.ApplicationId = a.ApplicationId INNER JOIN aspnet_UsersInRoles ur ON u.UserId = ur.UserId INNER JOIN aspnet_Roles r ON ur.RoleId = r.RoleId INNER JOIN AppRoles ar ON r.RoleName = ar.ASPRoleName WHERE u.UserName = '" + Username + "' AND a.ApplicationName = '" + ApplicationName + "' AND ar.Path IS NOT NULL ORDER BY ar.Ordinal")
        If dtModule.Rows.Count > 0 Then
            Dim intModuleRowCount As Integer = 0
            Dim intModuleRowCount2 As Integer = 0
            For Each row As DataRow In dtModule.Rows
                If (strAppRoleId <> "") Then
                    If strAppRoleId = row("Id").ToString Then
                        intModuleRowCount = intModuleRowCount2
                    Else
                        intModuleRowCount2 = (intModuleRowCount2 + 1)
                    End If
                End If

                Dim strModuleId As String = row("Id")
                Dim strModuleName As String = row("Name")
                Dim strModulePath As String = row("Path")
                strhtmlCntntsModule += "<a href=" + strModulePath + " >" + strModuleName + "</a>"

            Next row
            lblModuleName.InnerText = dtModule.Rows(intModuleRowCount)("Name").ToString
            Dim intModuleId As String = dtModule.Rows(intModuleRowCount)("Id")
            Dim dtMenu As DataTable = Database.System.GetDataTable("SELECT arm.id,arm.MenuText,arm.Path,arm.RequireRoles,arm.ParentId FROM AppRolesMenu arm WHERE AppRoleId='" + intModuleId + "' AND ParentId IS NULL ORDER BY ordinal")
            Dim dtSubMenu As DataTable = Database.System.GetDataTable("SELECT arm.id,arm.MenuText,arm.Path,arm.RequireRoles,arm.ParentId FROM AppRolesMenu arm WHERE AppRoleId='" + intModuleId + "' AND ParentId IS NOT NULL ORDER BY ordinal")
            If dtMenu.Rows.Count > 0 Then
                For Each Menurow As DataRow In dtMenu.Rows
                    Dim strMenuId As String = Menurow("id").ToString
                    Dim strMenuPath As String = Menurow("Path").ToString
                    Dim strMenuName As String = Menurow("MenuText").ToString
                    Dim strRolesReq As String = Menurow("RequireRoles").ToString
                    Dim dcsUser As Boolean = True
                    Dim shpExpo As Boolean = True ' hard coded ,show export shape for dcs users 
                    Dim isUserHasRole As Boolean = True
                    If (Username.ToLower <> "dcs-qa" And Username.ToLower <> "dcs-rd" And Username.ToLower <> "admin" And Username.ToLower <> "dcs-support" And Username.ToLower <> "dcs-ps") Then
                        dcsUser = False
                    End If

                    If (strMenuName = "Export Shape" And dcsUser = False) Then
                        shpExpo = False
                    End If

                    If dcsUser = False Then
                        If (strMenuName = "Administration") Then
                            If Roles.IsUserInRole("SVAdmin") Then
                            Else
                                isUserHasRole = False
                            End If
                        End If
                        If (strMenuName = "History") Then
                            If Roles.IsUserInRole("SVReviewer") Then
                            Else
                                isUserHasRole = False
                            End If
                        End If
                        If (strMenuName = "Reports") Then
                            If Roles.IsUserInRole("SVReport") Then
                            Else
                                isUserHasRole = False
                            End If
                        End If
                    End If

                    If (strMenuName = "Desktop Review Tasks" And Not ClientOrganization.HasModule(HttpContext.Current.GetCAMASession.OrganizationId, "DTR")) Then
                        isUserHasRole = False
                    End If

                    If (strMenuName = "Mobile Task Management" And Not ClientOrganization.HasModule(HttpContext.Current.GetCAMASession.OrganizationId, "MobileAssessor")) Then
                        isUserHasRole = False
                    End If

                    If IsDBNull(Menurow("ParentId")) And shpExpo = True And isUserHasRole = True Then

                        strhtmlCntntsMenu += "<li><img class=""imgArw"" src=""\App_Static\images\right-arrow.png"" ><a"
                        If (strMenuPath <> "") Then
                            strhtmlCntntsMenu += " href=" + strMenuPath + ""
                        End If
                        strhtmlCntntsMenu += ">" + strMenuName + "</a>"
                        Dim MenuVisibleRole As Boolean = True
                        If dtSubMenu.Rows.Count > 0 Then
                            strhtmlCntntsMenu += "<ul class=""third-level-menu"" >"

                            For Each SubMenuRow As DataRow In dtSubMenu.Rows
                                If SubMenuRow("ParentId") = strMenuId Then
                                    Dim strSubMenuId As String = SubMenuRow("id").ToString
                                    Dim strSubMenuPath As String = SubMenuRow("Path").ToString
                                    Dim strSubMenuName As String = SubMenuRow("MenuText").ToString

                                    If Not IsDBNull(Menurow("RequireRoles")) Then
                                        MenuVisibleRole = False
                                        Dim RequireRoles As String = Menurow("RequireRoles").ToString
                                        For Each RoleCheckRow As DataRow In dtMenu.Rows
                                            Dim strChkMenuId = RoleCheckRow("id").ToString
                                            Dim SplitRoles As String() = strRolesReq.Split(New Char() {","c})

                                            Dim EachRole As String
                                            For Each EachRole In SplitRoles
                                                If EachRole = strMenuId Then
                                                    MenuVisibleRole = True
                                                End If
                                            Next EachRole
                                        Next RoleCheckRow

                                    End If
                                    If MenuVisibleRole = True Then
                                        If IsDBNull(SubMenuRow("Path")) Then
                                            strhtmlCntntsMenu += "<li><img class=""imgArw"" src=""\App_Static\images\right-arrow.png"" ><a>" + strSubMenuName + "</a>"
                                        Else
                                            strhtmlCntntsMenu += "<li><img class=""imgArw"" src=""\App_Static\images\right-arrow.png"" ><a href=" + strSubMenuPath + " >" + strSubMenuName + "</a>"
                                        End If



                                        If dtSubMenu.Rows.Count > 0 Then
                                            strhtmlCntntsMenu += "<ul class=""fourth-level-menu"" >"

                                            For Each SubMenuRows As DataRow In dtSubMenu.Rows
                                                If SubMenuRows("ParentId") = strSubMenuId Then
                                                    Dim strsSubMenuId As String = SubMenuRows("id").ToString
                                                    Dim strsSubMenuPath As String = SubMenuRows("Path").ToString
                                                    Dim strsSubMenuName As String = SubMenuRows("MenuText").ToString

                                                    If Not IsDBNull(Menurow("RequireRoles")) Then
                                                        MenuVisibleRole = False
                                                        Dim RequireRoles As String = Menurow("RequireRoles").ToString
                                                        For Each RoleCheckRow As DataRow In dtMenu.Rows
                                                            Dim strChkMenuId = RoleCheckRow("id").ToString
                                                            Dim SplitRoles As String() = strRolesReq.Split(New Char() {","c})

                                                            Dim EachRole As String
                                                            For Each EachRole In SplitRoles
                                                                If EachRole = strMenuId Then
                                                                    MenuVisibleRole = True
                                                                End If
                                                            Next EachRole
                                                        Next RoleCheckRow

                                                    End If
                                                    If MenuVisibleRole = True Then
                                                        strhtmlCntntsMenu += "<li><img class=""imgArw"" src=""\App_Static\images\right-arrow.png"" ><a href=" + strsSubMenuPath + " >" + strsSubMenuName + "</a></li>"
                                                    End If
                                                End If
                                            Next SubMenuRows
                                            strhtmlCntntsMenu += "</ul>"
                                        End If
                                        strhtmlCntntsMenu += "</li>"
                                    End If
                                End If
                            Next SubMenuRow
                            strhtmlCntntsMenu += "</ul>"
                        End If
                        strhtmlCntntsMenu += "</li>"

                    End If
                Next Menurow

            End If
        End If

        divModuleDrop.InnerHtml = strhtmlCntntsModule
        divMainMenuDrop.InnerHtml = strhtmlCntntsMenu
    End Sub

    Private Sub lbLogout_Click(sender As Object, e As System.EventArgs) Handles lbLogout.Click
        CAMASession.Logout(HttpContext.Current)
    End Sub

    Private Sub CAMACloudNew_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Dim customTitle = Me.Page.Title
        If customTitle Is Nothing Then
            spanMainHeading.Text = ""
            Me.Page.Title = basePageTitle
        Else
            spanMainHeading.Text = customTitle
            Me.Page.Title = customTitle + " :: " + basePageTitle
        End If

    End Sub
End Class