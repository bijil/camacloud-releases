﻿Imports CAMACloud.Security
Imports CAMACloud.Exceptions

Partial Class cal
    Inherits System.Web.UI.Page

	Protected Sub lbActivate_Click(sender As Object, e As System.EventArgs) Handles lbActivate.Click
		Dim licenseKey As String = txtLicenseKey.Text
		Dim otp As String = txtOTP.Text

		pError.Visible = False

		Try
            DeviceLicense.Authenticate(licenseKey, otp, "this_is_not_used@camacloud.com", "dummy", HttpContext.Current)
			RedirectToApplication()
		Catch lex As InvalidLicenseException
			ErrorMailer.ReportException("license", lex, Request)
			pError.InnerHtml = lex.Message
			pError.Visible = True
		Catch ex As Exception
			ErrorMailer.ReportException("license-unknown", ex, Request)
			pError.InnerHtml = ex.Message
			pError.Visible = True
		End Try
	End Sub

	Sub RedirectToApplication()
		Dim schema As String = "http://"
		If Request("s") = "1" Then
			schema = "https://"
		End If
        Dim appHost As String = Request("return")
        If appHost = "" Then
            appHost = "client.camacloud.com"
        End If

		Response.Redirect(schema + appHost + "/")
	End Sub
End Class
