﻿Imports CAMACloud
Partial Class _Default
	Inherits System.Web.UI.Page

	'Protected Sub Login_Authenticate(sender As Object, e As System.Web.UI.WebControls.AuthenticateEventArgs) Handles Login.Authenticate
	'	Throw New Exception(Membership.ApplicationName)
	'End Sub

	Protected Sub Login_LoggedIn(sender As Object, e As System.EventArgs) Handles Login.LoggedIn
        CAMASession.RegisterUserOnDeviceLicense(Login.UserName)
        UserAuditTrail.CreateEvent(Login.UserName, UserAuditTrail.ApplicationType.Auth, UserAuditTrail.EventType.Login, "User logged in.")
		Dim returnUrl As String = FormsAuthentication.DefaultUrl
		If Request("ReturnUrl") <> "" Then
			returnUrl += Request("ReturnUrl").TrimStart("/")
		End If
		Response.Redirect(returnUrl)
	End Sub

	Protected Sub Login_LoginError(sender As Object, e As System.EventArgs) Handles Login.LoginError
        divError.InnerText = "Your Login attempt failed. Please try again."
		divError.Visible = True
        'ErrorMailer.ReportException(Login.UserName, New Exception("Login attempt failed"), Request)
    End Sub

	Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Try
                ' lblOrgName.Text = HttpContext.Current.GetCAMASession.OrganizationName
                LkChgOrg.Text = HttpContext.Current.GetCAMASession.OrganizationName
            Catch ex As Exception
                'lblOrgName.Text = "CAMA Cloud"
            End Try

            CloudUserProvider.VerifyOrganizationUsers()
        End If
    End Sub
    Protected Sub LkChgOrg_Click(sender As Object, e As EventArgs) Handles LkChgOrg.Click
        Dim returnUrl As String = "https://auth.camacloud.com/change"
        Dim hostName As String = HttpContext.Current.Request.Url.Host
        If hostName.Trim.EndsWith(".beta2.camacloud.com") Then
            returnUrl = "http://auth.beta2.camacloud.com/change"
        ElseIf hostName.Trim.EndsWith(".beta1.camacloud.com") Then
            returnUrl = "http://auth.beta1.camacloud.com/change"
        ElseIf hostName.Trim.EndsWith(".pacsdemo.camacloud.com") Then
            returnUrl = "http://auth.pacsdemo.camacloud.com/change"
        ElseIf hostName.Trim.EndsWith(".pacsbeta.camacloud.com") Then
            returnUrl = "http://auth.pacsbeta.camacloud.com/change"
        ElseIf hostName.Trim.EndsWith(".pacs.camacloud.com") Then
            returnUrl = "https://auth.pacs.camacloud.com/change"
        ElseIf hostName.Trim.EndsWith(".vgsi.camacloud.com") Then
        	returnUrl = "https://auth.vgsi.camacloud.com/change"
        ElseIf hostName.Trim.EndsWith(".pvt.camacloud.com") Then
            returnUrl = "https://auth.pvt.camacloud.com/change"
        ElseIf hostName.Trim.EndsWith(".alpha.camacloud.com") Then
            returnUrl = "https://auth.alpha.camacloud.com/change"
        ElseIf hostName.Trim.EndsWith(".ca.camacloud.com") Then
            returnUrl = "https://auth.ca.camacloud.com/change"
        ElseIf hostName.Trim.EndsWith(".ca-beta.camacloud.com") Then
            returnUrl = "https://auth.ca-beta.camacloud.com/change"
        ElseIf hostName = "localhost" Then
            returnUrl = "http://localhost:309/showlicense/"
        End If
        Response.Redirect(returnUrl)
    End Sub
End Class
