﻿<%@ Page Title="CAMA Cloud&#174; - Requires Client Access License" Language="VB"
	MasterPageFile="~/App_MasterPages/SecurityPage.master" AutoEventWireup="false" Inherits="CAMACloud.Console.cal" Codebehind="cal.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<p>
		You are trying to access <b style="white-space: nowrap;">CAMA Cloud<sup>&#174;</sup></b>,
		but this device is not yet licensed to use the software. If you have a valid license
		key, and a One-Time-Password, please enter them below to gain access to the software.</p>
	<p class="error" runat="server" visible="false" id="pError">You have entered an invalid OTP.</p>
	<div class="login-inputs">
		<div class="input-box">
			<asp:TextBox ID="txtLicenseKey" runat="server" CssClass="text" watermark="License Key" />
		</div>
		<div class="input-box">
			<asp:TextBox ID="txtOTP" TextMode="Password" runat="server" CssClass="text" watermark="One Time Password" />
		</div>
	</div>
	<div style="text-align: center;">
		<asp:LinkButton runat="server" CssClass="login-button" ID="lbActivate"><span>Activate Client</span></asp:LinkButton>
	</div>
	<p>
		Note: If you are still getting this screen on an already licensed machine, it means
		that your license data has been erased from this machine or your existing license
		has been revoked. Please contact <b style="white-space: nowrap;">Data Cloud Solutions,
			LLC.</b>, or an authorized <b>CAMA Cloud<sup>&#174;</sup></b> software vendor, for
		your license key and one-time-password to activate your device.</p>
</asp:Content>
