﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/CAMACloud.master" AutoEventWireup="false" Inherits="CAMACloud.Console.web_403" Codebehind="403.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
	<h1>Access Denied!</h1>
	<div style="width:800px">
		<p>You are restricted from accessing this page or module. Please click on any of the links on the top right of the page to continue working on this website.</p>
	</div>
</asp:Content>

