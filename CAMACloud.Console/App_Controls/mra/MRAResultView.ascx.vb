﻿Public Class MRAResultView
    Inherits System.Web.UI.UserControl

    Public T As DataRow

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Request("_tid") IsNot Nothing AndAlso IsNumeric(Request("_tid")) Then
                OpenResults(Request("_tid"))
            End If
        End If
    End Sub

    Public Sub OpenResults(templateId As Integer)
        Dim ds As DataSet = Database.Tenant.GetDataSet("EXEC MRA_GetResultView " & templateId)

        If ds.Tables(0).Rows.Count > 0 Then
            T = ds.Tables(0).Rows(0)

            lblModelName.Text = T.GetString("Name")
            lblFormula.Text = "{0} = {1}".FormatString(T.GetString("OutputFieldName"), T.GetString("CostFormula"))

            Dim dtStats = ds.Tables(1)
            Dim dtANOVA = ds.Tables(2)
            Dim dtCoeffs = ds.Tables(3)

            gvStats.DataSource = dtStats
            gvStats.DataBind()

            gvANOVA.DataSource = dtANOVA
            gvANOVA.DataBind()

            gvCoefficients.DataSource = dtCoeffs
            gvCoefficients.DataBind()

        End If



    End Sub

End Class