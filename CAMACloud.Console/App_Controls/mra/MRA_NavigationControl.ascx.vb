﻿Public Class MRA_NavigationControl
	Inherits System.Web.UI.UserControl
	Public Shared Event ddlChanged As EventHandler
	Dim Public Shared __mraDDLItem As Integer = 0
'	  Public  readonly Property ddlSelected As DropDownList 
'	  	Get
'			Return Me.ddlModel 
'        End Get
'    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim templateId As Integer = -18
        Dim i As Integer =Request("_tid")
        If Not IsPostBack Then
        	If Integer.TryParse(Request("_tid"), templateId) Then
            	ddlModel.FillFromSql("SELECT ID, Name FROM MRA_Templates ORDER BY Name", True)
            	 hlcalculator.NavigateUrl = "~/protected/mra/calculator.aspx?_tid=" & templateId
            	hlgraph.NavigateUrl ="~/protected/mra/graphview.aspx?_tid=" & templateId
            	hlanalysisResult.NavigateUrl = "~/protected/mra/resultview.aspx?_tid=" & templateId
            	hldataView.NavigateUrl =  "~/protected/mra/dataview.aspx?_tid=" & templateId
            	hleditView.NavigateUrl =  "~/protected/mra/design.aspx?_tid=" & templateId
            Else
                ' ddlModel.FillFromSql("SELECT ID, Name FROM MRA_Templates WHERE LastExecutionTime IS NOT NULL ORDER BY Name", True)
                hlcalculator.visible=False
            	hlgraph.visible=False
            	hlanalysisResult.visible=False
            	hldataView.visible=False
            	hleditView.Visible=False
            End If
        End If
    End Sub
    Protected Sub ddlModel_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlModel.SelectedIndexChanged
		If ddlModel.SelectedIndex = 0 Then
              	hlcalculator.visible=False
            	hlgraph.visible=False
            	hlanalysisResult.visible=False
            	hldataView.visible=False
            	hleditView.Visible=False
            	__mraDDLItem = 0
            	 If(ddlChangedEvent IsNot Nothing ) 
            	 	RaiseEvent ddlChanged(sender,e)
            	 End IF
    	Else
			hlcalculator.visible= True 
            hlgraph.visible= True 
            hlanalysisResult.visible=True 
            hldataView.visible= True
            hleditView.Visible= True
            hlcalculator.NavigateUrl = "~/protected/mra/calculator.aspx?_tid=" & convert.ToInt32(ddlModel.SelectedValue)
            hlgraph.NavigateUrl = "~/protected/mra/graphview.aspx?_tid=" & convert.ToInt32(ddlModel.SelectedValue)
            hlanalysisResult.NavigateUrl = "~/protected/mra/resultview.aspx?_tid=" & convert.ToInt32(ddlModel.SelectedValue)
            hldataView.NavigateUrl = "~/protected/mra/dataview.aspx?_tid=" & convert.ToInt32(ddlModel.SelectedValue)
            hleditView.NavigateUrl =  "~/protected/mra/design.aspx?_tid=" & convert.ToInt32(ddlModel.SelectedValue)
            __mraDDLItem = ddlModel.SelectedValue
            If(ddlChangedEvent IsNot Nothing )        	
            	RaiseEvent ddlChanged(sender,e)
            End IF
        End If
    End Sub

End Class