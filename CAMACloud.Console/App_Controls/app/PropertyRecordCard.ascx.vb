﻿
Partial Class App_Controls_app_PropertyRecordCard
    Inherits System.Web.UI.UserControl

    Public Property PRCTemplateName As String = "property-record-card"
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim template As String = ClientSettings.ContentTemplate(PRCTemplateName)
            If template <> "" Then
            	'ContentTemplate2.Controls.Clear()
                ContentTemplate.Controls.Clear()
                ContentTemplate.Controls.Add(New LiteralControl(template))
            End If
             Dim template_bpp As String = ClientSettings.ContentTemplate("property-record-card-PersonalProperty")
             If template_bpp <> "" Then
             	'ContentTemplate.Controls.Clear()
                ContentTemplate2.Controls.Clear()
                ContentTemplate2.Controls.Add(New LiteralControl(template_bpp))
            End If
        End If
    End Sub

End Class
