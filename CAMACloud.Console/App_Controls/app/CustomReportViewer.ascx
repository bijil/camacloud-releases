﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CustomReportViewer.ascx.vb" Inherits="CAMACloud.Console.CustomReportViewer" %>
<%@ Import Namespace="CAMACloud"%>
<%@ Import Namespace="CAMACloud.Data" %>
<style type="text/css">
        .custom-report-viewer {
            width: 99%;
            overflow-x: auto;
            //position: absolute;
            padding: 0 5px 0px 5px;
        }       
        .report-head{
            font-family: Cambria, Georgia, serif;
	        font-size: 26px;       
            text-align: center;
        }  
       .exportIcon{    
            text-align: right;
            background-color: #ECE9D8;
        }
        .gvHeaderStyle{
            white-space:nowrap; 
            /*text-align: center !important;*/
            border-right: 1px solid !important;
            border-right-style: inset !important;
        }
        .gvItemStyle{
            white-space:nowrap;
            text-align: left;
            min-width: 140px;
        }
        .gvNonCountItemStyle{       
             text-align: left;
             white-space:nowrap;
             min-width: 150px;
        }
        .gvFooterStyle{
            text-align: left;
        }
</style>
   <div>
     <div Id="exportOptions" class="exportIcon" runat="server" Visible ="False">
         <asp:ImageButton ID="ibtnExportToExcel" ImageUrl="~/App_Static/images/excel.png" width="28px" height="25px" style="margin-right:3px;" ToolTip="Export To Excel" runat="server" />
     </div>
     <div ID="reportHead"  class="report-head"  runat="server" Visible ="False">
        <asp:Label ID="lblReportHead" runat="server" Text="Label"></asp:Label>
     </div>
     <div class="custom-report-viewer" Id="reportViewer" runat="server" Visible ="False">
         <asp:GridView ID="gvCustomReport"  runat="server" width=" 100%" AllowPaging="true" AutoGenerateColumns="false"  ShowFooter = "true" OnPageIndexChanging = "gvCustomReportPageChanging" PageSize="19" >
         </asp:GridView>			
     </div>
   </div>

