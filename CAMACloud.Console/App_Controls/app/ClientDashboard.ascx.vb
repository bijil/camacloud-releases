﻿Imports CAMACloud.Data

Partial Class App_Controls_app_ClientDashboard
    Inherits System.Web.UI.UserControl

    Public o As DataRow

Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
   		o = Database.System.GetTopRow("SELECT * FROM Organization WHERE Id = " & HttpContext.Current.GetCAMASession().OrganizationId)
   		Dim userId = Membership.GetUser().ProviderUserKey.ToString()
   		Dim createDate = Membership.GetUser().CreationDate.ToString()
   		Dim BPPCheck As Boolean = Database.Tenant.GetIntegerValue("select BPPEnabled from application")
		Dim LastLoginTime As String = Database.System.GetStringValue("IF EXISTS(SELECT * FROM LoginDetails WHERE UserId='" + userId + "' and ApplicationId = 2 ) SELECT LastLoginTime from LoginDetails where UserId ='" + userId + "' and ApplicationId = 2")
        If LastLoginTime <> createDate AndAlso Not String.IsNullOrWhiteSpace(LastLoginTime) Then
            Dim LastLoginTime_dateTime As DateTime = Convert.ToDateTime(LastLoginTime)
            Dim LocalTime = Database.Tenant.GetStringValue("SELECT dbo.GetLocalDate('" + LastLoginTime_dateTime.ToString("yyyy-MM-dd HH:mm:ss") + "' )")
            lblLogin.Text = LocalTime
        Else
            lblLogin.Text = "Never"
        End If
        If BPPCheck Then
         Try	
        	Dim dr As DataRow = Database.Tenant.GetTopRow("select BPPCondition,RPCondition from application")
        	Dim BPPCondition As String = dr.GetString("BPPCondition")
        	Dim RPCondition As String = dr.GetString("RPCondition")
        	If BPPCondition <> ""  Then
        		Dim BPPCount As Integer = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM ParcelData WHERE ClientROWUID IS NOT NULL OR "+ BPPCondition )
        		BppCnt.Text = "Total Business Property:"
        		lblBppc.Text = BPPCount.ToString("N0")
        	End If
        	If RPCondition <> "" Then
        		Dim RPCount As Integer = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM ParcelData WHERE "+ RPCondition )
        		RpCnt.Text = "Total Real Property:"
        		lblRpc.Text = RPCount.ToString("N0")
        	End If
         Catch ex As Exception
         	BppCnt.Text = ""
         	lblBppc.Text = ""
         	RpCnt.Text = ""
            lblRpc.Text = ""
         	
          End Try
        End If
    End Sub

    Public ReadOnly Property UserFullName As String
        Get
            Dim loginId As String = Page.UserName
            Dim fullName As String = Database.Tenant.GetStringValue("SELECT COALESCE(FirstName, '') + COALESCE(' ' + LastName, '') FROM UserSettings WHERE LoginId = {0}".SqlFormatString(loginId))
            If fullName = "" Then
                Return loginId
            Else
                Return fullName
            End If
        End Get
    End Property
    Protected Sub lbRedirect_Click(sender As Object, e As EventArgs) Handles lbRedirect.Click
        Response.Redirect("~/protected/datasetup/data/datastatus.aspx")
    End Sub
End Class
