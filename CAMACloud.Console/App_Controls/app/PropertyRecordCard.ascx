﻿<%@ Control Language="VB" AutoEventWireup="false"
    Inherits="CAMACloud.Console.App_Controls_app_PropertyRecordCard" Codebehind="PropertyRecordCard.ascx.vb" %>
    <%@ Import Namespace="CAMACloud"%>
<%@ Import Namespace="CAMACloud.Data" %>
<div id="prc-template" hidden>
    <!-- PRC TEMPLATE -->
    <asp:PlaceHolder runat="server" ID="ContentTemplate">
        <div class="prc-format">
            <div style="text-align:center;margin:30px;font-weight:bold;">The PRC template has not been initialized.</div>
        </div>
    </asp:PlaceHolder>
    <!-- END OF PRC TEMPLATE -->
</div>
<div id="prc-template-bpp" hidden>
    <!-- PRC TEMPLATE BPP -->
    <asp:PlaceHolder runat="server" ID="ContentTemplate2">
        <div class="prc-format">
            <div style="text-align:center;margin:30px;font-weight:bold;">The PRC template has not been initialized.</div>
        </div>
    </asp:PlaceHolder>
    <!-- END OF PRC TEMPLATE BPP -->
</div>
