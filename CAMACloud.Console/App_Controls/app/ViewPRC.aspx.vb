﻿Public Class ViewPRC
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim req As String = Request.QueryString("Params")
        Dim parts() As String = req.Split("/"c)
        If parts.Length >= 3 Then
            Dim prcId As String = parts(2) ' "10001"
            Dim PrcImage As String = parts(3) ' "16.html"
            PrcImage = PrcImage.Replace(".html", "")
            PrcImage = PrcImage.Replace("beforeMAC", "")
            RunScript("downloadPrcHtml(" + PrcImage + ")")
        End If


    End Sub

End Class