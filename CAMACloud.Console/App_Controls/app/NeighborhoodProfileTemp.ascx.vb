﻿Public Class NeighborhoodProfileTemp
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim template As String = ClientSettings.ContentTemplate("nbhd-profile")
            If template <> "" Then
                ContentTemplate.Controls.Clear()
                ContentTemplate.Controls.Add(New LiteralControl(template))
            End If
        End If
    End Sub

End Class