﻿
Partial Class App_Controls_app_WebFooter
    Inherits System.Web.UI.UserControl

	Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
		If Not IsPostBack Then
            Dim vi As VendorInfo = HttpContext.Current.GetCAMASession.VendorInfo
            If vi.ShowVendorInfo Then
                hlVendor.Text = vi.Name
                hlVendor.NavigateUrl = vi.Website
                lblVendorInfo.Text = vi.Info
            End If

		End If
    End Sub

    Public Function AssemblyVersion() As String
        Return My.Application.Info.Version.ToString
    End Function
End Class
