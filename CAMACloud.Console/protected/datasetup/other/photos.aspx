﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/DataSetup.master" AutoEventWireup="false" Inherits="CAMACloud.Console.datasetup_other_photos" Codebehind="photos.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        function hookup() {

        }
       function newid() {
            var S4 = function () {
                return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
            };
            return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
        }

        var kb = 1024;
        var mb = kb * kb;
        var gb = kb * kb * kb;
        var blobslicer = File.prototype.mozSlice || File.prototype.webkitSlice || File.prototype.slice;
        var blobbuilder = window.MozBlobBuilder || window.WebKitBlobBuilder || window.MSBlobBuilder || window.BlobBuilder;

        function selectFile() {
            document.getElementById('upload').click();
        }

        function fileSize(file) {
            var bytes = file.size;
            if (bytes >= gb) {
                return ((Math.round(bytes / gb * 100) / 100) + ' GB');
            } else if (bytes > 10 * mb) {
                return (Math.round(bytes / mb) + ' MB');
            } else if (bytes >= 1 * mb) {
                return (Math.round(bytes / mb * 10) / 10 + ' MB');
            } else if (bytes > 10 * kb) {
                return (Math.round(bytes / kb) + ' kB');
            } else if (bytes >= kb) {
                return (Math.round(bytes / kb * 10) / 10 + ' kB');
            } else {
                return (bytes + ' bytes');
            }
        }

        var uploadReader;

        function handleUpload(uploader) {
            var files = uploader.files;
            var file = files[0];
            var filename = file.name.toLowerCase();
            var ext = filename.substr(filename.lastIndexOf(".")).substr(1);
            if ((ext == "zip") || (ext == "rar")) {
                // Okay
            } else {
                alert('You have selected an invalid type of file.');
                return;
            }
            var reader = new FileReader();


            var chunkSize = 2 * mb;
            var chunks = Math.ceil(file.size / chunkSize);
            var start = 0;
            var ccount = 0;
            //start = 1535115264 + chunkSize;
            if (localStorage.getItem('data-file-upload-started') == 1) {
                if ((localStorage.getItem('data-file-upload-name') == file.name) && (localStorage.getItem('data-file-upload-size') == file.size)) {
                    start = parseInt(localStorage.getItem('data-file-upload-start'));
                }
            }

            if (start == 0) {
                localStorage.setItem('data-file-upload-uuid', newid());
            }

            function startUpload() {
                console.log('Starting');
                $('.upload-link').hide();
                $('.upload-progress').show();
                $('.upload-title').html('Uploading <b>' + file.name + '</b>, ' + fileSize(file));
                $('.upload-meter div').css({ 'width': '0%' });
                $('.upload-meter meter').mousedown();

                localStorage.setItem('data-file-upload-started', 1);
                localStorage.setItem('data-file-upload-name', file.name);
                localStorage.setItem('data-file-upload-size', file.size);
            }

            function clearFlags() {
                $('.upload-link').show();
                $('.upload-progress').hide();
                $('#upload').val('');
                localStorage.removeItem('data-file-upload-started');
                localStorage.removeItem('data-file-upload-start');
                localStorage.removeItem('data-file-upload-name');
                localStorage.removeItem('data-file-upload-size');
                localStorage.removeItem('data-file-upload-uuid');
            }

            function finishUpload() {
                clearFlags();
                alert('The data source file has been uploaded successfully now. The page will refresh now.')
                window.location.reload();
            }

            function abortUpload() {
                alert('Uploaded is aborted due to an error at server.');
                clearFlags();
            }

            function displayError() {

            }

            function readNext() {

                ccount++;
                var stop = Math.min(start + chunkSize - 1, file.size);

                reader.onloadend = function (e) {
                    if (e.target.readyState == FileReader.DONE) {
                        try {
                            var progress = Math.floor(stop / file.size * 100);
                            var form = new FormData();

                            var blob;
                            if (blobbuilder) {
                                var bb = new blobbuilder();
                                bb.append(e.target.result);
                                blob = bb.getBlob("application/octet-stream");
                            } else {
                                blob = new Blob([e.target.result]);
                            }


                            form.append(file.name, blob, file.name);
                            form.append("name", localStorage.getItem('data-file-upload-name'));
                            form.append("uuid", localStorage.getItem('data-file-upload-uuid'));
                            form.append("start", start);
                            form.append("stop", stop);
                            form.append("size", file.size);
                            form.append("progress", progress)

                            var xhr = new XMLHttpRequest();
                            xhr.open('POST', '/uploads/catch-photos.aspx', true);
                            xhr.onload = function (e) {
                                if (this.status == 200) {
                                    $('.upload-meter div').css({ 'width': progress + '%' });
                                    start += chunkSize;
                                    localStorage.setItem('data-file-upload-start', start);
                                    if (start < file.size) {
                                        uploadReader = readNext;
                                        var wait = 0;
                                        window.setTimeout('uploadReader()', wait);
                                    } else {
                                        finishUpload();
                                    }
                                } else if (this.status == 500) {
                                    abortUpload();
                                }
                            }
                            xhr.send(form);
                        }
                        catch (ex) {
                            console.log(ex);
                        }
                    }
                };

                try {
                    var blob = blobslicer.call(file, start, stop + 1);
                    reader.readAsArrayBuffer(blob);
                }
                catch (ex2) {
                    alert(ex2);
                }
            }

            startUpload();
            readNext();

        }

        function wl(message) {
            $('.output').append($('<div>' + message + '</div>'));
        }

        $(hookup);
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1>
        Upload Photos</h1>
        	<p class="info">
			You can add parcel images to CAMA Cloud by uploading files here.Image files must be in(*.Jpeg) format and compressed as (*.zip) file format.</p>
		<p class="info">
			<b>Note:</b>CAMA Cloud accepts files for upload that are less than 2 gigabyte (GB). Uploading large files is prone to crashes in Google Chrome browser.
			In such cases, reload the page from the browser address bar, upload the same file.
			Uploading process will resume from the discontinued position.</p>
    <div id="upload-div">
        <p style="display: none;" class="html-uploader">
            <input type="file" id="upload" onchange="handleUpload(this);" />
            <%--<input type="file" id="File1" onchange="handleUpload(this);" accept="application/msaccess" />--%>
        </p>
        <div class="link-panel upload-link">
            <a onclick="selectFile();">Upload File</a>
            <asp:LinkButton runat="server" ID="lbRecover" Text="Recover last upload" Visible="false" />
        </div>
        <div class="upload-progress">
            <div class="upload-title">
                Uploading</div>
            <div class="upload-meter">
                <div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

