﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/DataSetup.master"
    AutoEventWireup="false" Inherits="CAMACloud.Console.datasetup_apps_adeconfig" Codebehind="adeconfig.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .col1
        {
            width: 25px;
            text-align: right;
            padding-right: 10px !important;
        }
        
        .second-link:before
        {
            content: "|";
            margin-right: 8px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <h1>
        Automatic Data Entry - Manage Configurations</h1>
    
    <asp:Panel runat="server" ID="pnlEditor">
        <table>
            <tr>
                <td colspan="2" style="font-weight: bold; font-size: 13pt;">
                    <asp:Label runat="server" ID="lblTitle" Text="Create new configuration" />
                    <asp:HiddenField runat="server" ID="hdnConfigId" Value="" />
                </td>
            </tr>
            <tr>
                <td style="vertical-align: top; width: 120px;">
                    Name:
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtName" Width="300px" MaxLength="50" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="txtName" ValidationGroup="SaveConfig" />
                </td>
            </tr>
            <tr>
                <td style="vertical-align: top;">
                    Description:
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtDescription" Width="300px" TextMode="MultiLine"
                        Rows="4" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="txtDescription" ValidationGroup="SaveConfig" />
                </td>
            </tr>
            <tr>
                <td>
                    Config Type:</td>
                <td>
                    <asp:DropDownList ID="ddlConfigType" runat="server">
                        <asp:ListItem Value="1">Windows</asp:ListItem>
                        <asp:ListItem Value="2">Website</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:Button ID="btnSave" runat="server" Text="  Save  " 
                        ValidationGroup="SaveConfig" />
                    <asp:Button ID="btnCancel" runat="server" Text=" Cancel " />
                </td>
            </tr>
        </table>
        <asp:LinkButton runat="server" ID="lblSwitchImport" Text="Import configuration ..."  Font-Bold="true" Font-Size="Smaller" />
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlImport" Visible="false">
        <p style="font-weight: bold; font-size: 13pt;">Import Configuration from file</p>
        <p>Upload CAMA Cloud ADE Configuration file (<b>*.ccade</b>) to create or update configurations.</p>
        <p>
            <asp:FileUpload runat="server" ID="CCADE" Width="400px" />
            <asp:Button runat="server" ID="btnImport" Text=" Import Configuration File " />
        </p>
        <p>
        
            <asp:RadioButtonList ID="rdbImportType" runat="server" 
                RepeatDirection="Horizontal">
                <asp:ListItem Value="0">Import config as a copy</asp:ListItem>
                <asp:ListItem Value="1" Selected="True">Create new config</asp:ListItem>
            </asp:RadioButtonList>
        
        </p>
        <asp:LinkButton runat="server" ID="lblSwitchEditor" Text="Configuration Editor ..."  Font-Bold="true" Font-Size="Smaller" />
    </asp:Panel>
    <div style="height: 30px;">
    </div>
    <asp:GridView runat="server" ID="grid">
        <Columns>
            <asp:TemplateField>
                <ItemStyle CssClass="col1" />
                <ItemTemplate>
                    #<%# Eval("Id")%>.</ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Name" HeaderText="Config. Name" ItemStyle-Width="300px" />
            <asp:TemplateField>
                <ItemStyle Width="90px" HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:LinkButton runat="server" Text="Edit" CommandName='EditItem' CommandArgument='<%# Eval("Id") %>' /><asp:Label
                        runat="server" CssClass="second-link" Visible='<%# Not Eval("IsDefault") %>'><asp:LinkButton runat="server" Text="Delete" CommandName='DeleteItem' CommandArgument='<%# Eval("Id") %>' OnClientClick="return confirm('Are you sure you want to delete this configuration and its settings permanently?')" /></asp:Label></ItemTemplate></asp:TemplateField><asp:TemplateField>
                <ItemStyle Width="90px" HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:LinkButton runat="server" Text="Set as Default" CommandName='SetAsDefault' CommandArgument='<%# Eval("Id") %>'
                        Visible='<%# Not Eval("IsDefault") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemStyle Width="90px" HorizontalAlign="Center" />
                <ItemTemplate>
                    <a href='adestep.aspx?cid=<%# Eval("Id") %>'>Edit Steps</a></ItemTemplate></asp:TemplateField><asp:TemplateField>
                <ItemStyle Width="90px" HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:LinkButton runat="server" Text="Export Config" CommandName='ExportAsXML' CommandArgument='<%# Eval("Id") %>' />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>
