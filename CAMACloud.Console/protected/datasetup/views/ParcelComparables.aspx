﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master" CodeBehind="ParcelComparables.aspx.vb" Inherits="CAMACloud.Console.ParcelComparables" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script>
        function maskShow() {
            showMask(); return true;
        }

        function maskHide(reload) {
            hideMask();
            if (reload) {
                alert("Parcel comparables created successfully.");
            }
        }
    </script>
    <style type="text/css">
        .col1 {
            width: 480px;
            text-align: left;
        }
        
        .col2 {
            width: 420px;
            text-align: left;
        }

        .btnUpdate {
            position: absolute;
            left: 50%;
            right: 50%;
            width: 100px;
            height: 30px;
        }
        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <h1>
        Parcel Comparables
    </h1>
    <div class="div-cat-create ui-state-default ui-corner-all" style="display: flex;">
        <label style="margin-left: 10px;">Select Table: </label>
        <asp:DropDownList runat="server" ID="ddlTable" Style="width: 220px; margin-left: 10px;" AutoPostBack="true" />
        <span style="margin-left: 175px;">
            <asp:RadioButtonList ID="radioList" runat="server"  RepeatDirection="Horizontal">
                <asp:ListItem Value="K" Text="KeyField1"></asp:ListItem>
                <asp:ListItem Text="Alternate Field" Value="A"></asp:ListItem>
            </asp:RadioButtonList>
        </span> 
    </div>
    <section class="comparable comparable-header">
        <table style="border-spacing: 3px; font-weight: bold;">
            <tr>
                <td class="col1" style ="font-size: 15px"> Comparable Parcel1</td>
                <td class="col2" style ="font-size: 15px"> 
                    <asp:DropDownList runat="server" ID="DropDownList1" Style="width: 300px; height: 25px;" />
                </td>
            </tr>
            <tr>
                <td class="col1" style ="font-size: 15px"> Comparable Parcel2</td>
                <td class="col2" style ="font-size: 15px"> 
                    <asp:DropDownList runat="server" ID="DropDownList2" Style="width: 300px; height: 25px;" />
                </td>
            </tr>
            <tr>
                <td class="col1" style ="font-size: 15px"> Comparable Parcel3</td>
                <td class="col2" style ="font-size: 15px"> 
                    <asp:DropDownList runat="server" ID="DropDownList3" Style="width: 300px; height: 25px;" />
                </td>
            </tr>
            <tr>
                <td class="col1" style ="font-size: 15px"> Comparable Parcel4</td>
                <td class="col2" style ="font-size: 15px"> 
                    <asp:DropDownList runat="server" ID="DropDownList4" Style="width: 300px; height: 25px;" />
                </td>
            </tr>
            <tr>
                <td class="col1" style ="font-size: 15px"> Comparable Parcel5</td>
                <td class="col2" style ="font-size: 15px"> 
                    <asp:DropDownList runat="server" ID="DropDownList5" Style="width: 300px; height: 25px;" />
                </td>
            </tr>
        </table>
    </section>  
    <asp:Button runat="server" ID="btnUpdate" CssClass="btnUpdate" OnClientClick="return maskShow();" Text=" Update " />
</asp:Content>