﻿Imports System.Data.OleDb
Imports CAMACloud.BusinessLogic

Partial Class datasetup_data_tables
	Inherits System.Web.UI.Page

	Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
		If Not IsPostBack Then
			LoadTablesList()
		End If
	End Sub

	Sub LoadTablesList()
		Using x As Repeater = rpParcelData
			x.DataSource = Database.Tenant.GetDataTable("SELECT * FROM DataSourceTable WHERE ImportType = 0 AND DoNotImport = 0 ORDER BY Name")
			x.DataBind()
		End Using
		Using x As Repeater = rpSubTables
			x.DataSource = Database.Tenant.GetDataTable("SELECT * FROM DataSourceTable WHERE ImportType = 1 AND DoNotImport = 0 ORDER BY Name")
			x.DataBind()
		End Using
		Using x As Repeater = rpLookup
			x.DataSource = Database.Tenant.GetDataTable("SELECT * FROM DataSourceTable WHERE ImportType = 2 AND DoNotImport = 0 ORDER BY Name")
			x.DataBind()
		End Using
		Using x As Repeater = rpNbhd
			x.DataSource = Database.Tenant.GetDataTable("SELECT * FROM DataSourceTable WHERE ImportType = 3 AND DoNotImport = 0 ORDER BY Name")
			x.DataBind()
		End Using
		Using x As Repeater = rpDNI
			x.DataSource = Database.Tenant.GetDataTable("SELECT * FROM DataSourceTable WHERE DoNotImport = 1 ORDER BY Name")
			x.DataBind()
		End Using
		
	End Sub

	
End Class
