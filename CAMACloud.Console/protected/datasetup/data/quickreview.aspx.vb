﻿Imports CAMACloud.BusinessLogic

Public Class quickreview
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim parcelTableId As Integer = Database.Tenant.GetIntegerValueOrInvalid("SELECT Id FROM DataSourceTable WHERE Name = " + Database.Tenant.Application.ParcelTable.ToSqlValue)
            LoadFields(parcelTableId)

            rpQuickReview.DataSource = Database.Tenant.GetDataTable("SELECT * FROM DataSourceField WHERE IsFavorite = 1 ORDER BY FavoriteOrdinal")
            rpQuickReview.DataBind()
        End If
    End Sub

    Sub LoadFields(tableId As Integer)
        Dim dtField As DataTable
        Try
            dtField = Database.Tenant.GetDataTable("EXEC cc_GetAllDataFieldsFromTableAndAssociated " & tableId)
        Catch ex As Exception
            dtField = Database.Tenant.GetDataTable("SELECT *, TableId As FieldTableId FROM DataSourceField WHERE TableId = " & tableId & " ORDER BY Name")
        End Try
        rpFields.DataSource = dtField
        rpFields.DataBind()
    End Sub

    Public Function MasterFieldClass() As String
        Dim isFav = Eval("IsFavorite")
        If isFav Then
            Return "cat-added"
        Else
            Return ""
        End If
    End Function

    Protected Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        If IsPostBack Then
            RunScript("finalizePage();")
        End If
    End Sub

End Class