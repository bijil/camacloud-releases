﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/DataSetup.master"
	AutoEventWireup="false" Inherits="CAMACloud.Console.datasetup_data_importstatus" Codebehind="importstatus.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
	<style type="text/css">
		.import-header tr td
		{
		}
		
		.import-header tr td span
		{
			font-weight: bold;
			color: Black;
		}
	</style>
	<script type="text/javascript">
		function refreshPage() {
			$('#btnRefresh').click();
		}

	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<asp:HiddenField runat="server" ID="hdnImportJobId" />
	<asp:UpdatePanel runat="server" UpdateMode="Always">
		<ContentTemplate>
			<h1>
				CAMA Data Import Status</h1>
			<p>
				<asp:Button runat="server" ID="btnStart" Text=" Start Import Job " />
				<asp:Button runat="server" ID="btnCancel" Text=" Cancel Job " />
				<asp:Button runat="server" ID="btnRefresh" Text=" Refresh Status " ClientIDMode="Static" /></p>
			<asp:FormView runat="server" ID="fvJob">
				<ItemTemplate>
					<table class="import-header">
						<tr>
							<td style="width: 160px">Job #: <span>
								<%#Eval("Id")%></span></td>
							<td style="width: 230px">Created By: <span>
								<%# Eval("CreatedBy")%></span></td>
							<td style="width: 230px">Created Date: <span>
								<%# Eval("CreatedDate", "{0: MMM d, yyyy HH\:mm}")%></span></td>
						</tr>
						<tr>
							<td>Status: <span>
								<%#Eval("StatusName")%></span></td>
							<td>Started Time: <span>
								<%# Eval("StartedTime", "{0: MMM d, yyyy HH\:mm}")%></span></td>
							<td>Time Elapsed: <span>
								<%# Eval("TimeElapsed")%></span></td>
						</tr>
					</table>
				</ItemTemplate>
			</asp:FormView>
			<asp:FormView runat="server" ID="fvPrimary">
				<ItemTemplate>
					<h3>
						Primary Table</h3>
					<table class="import-header">
						<tr>
							<td style="width: 390px">Table Name: <span>
								<%# ClientSettings.PrimaryTable%></span></td>
							<td style="width: 140px">Total Rows: <span>
								<%# Eval("PrimaryTotalRows")%></span></td>
							<td style="width: 140px">Imported: <span>
								<%# Eval("PrimaryImportedRows")%></span></td>
						</tr>
					</table>
				</ItemTemplate>
			</asp:FormView>
			<h3>
				Data Tables</h3>
			<asp:GridView runat="server" ID="gvTables">
				<Columns>
					<asp:BoundField DataField="Name" HeaderText="Table Name" ItemStyle-Width="300px" />
					<asp:BoundField DataField="ImportTypeName" HeaderText="Type" ItemStyle-Width="100px" />
					<asp:TemplateField HeaderText="Started">
						<ItemStyle Width="70px" />
						<ItemTemplate>
							<%# Eval("ImportStartTime", "{0:HH\:mm}")%>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="Completed">
						<ItemStyle Width="80px" />
						<ItemTemplate>
							<%# Eval("ImportEndTime", "{0:HH\:mm}")%>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:BoundField DataField="TimeElapsed" HeaderText="Elapsed" ItemStyle-Width="70px" />
					<asp:BoundField DataField="TotalRows" HeaderText="Rows" ItemStyle-Width="70px" />
					<asp:BoundField DataField="ImportedRows" HeaderText="Imported" ItemStyle-Width="70px" />
					<asp:BoundField DataField="PercentComplete" HeaderText="Percent" DataFormatString="{0}%"
						ItemStyle-Width="70px" />
                    <asp:BoundField DataField="TimePending" HeaderText="Pending" ItemStyle-Width="70px" />
				</Columns>
				<EmptyDataTemplate>
					No data/auxiliary/lookup tables in queue.
				</EmptyDataTemplate>
			</asp:GridView>
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>
