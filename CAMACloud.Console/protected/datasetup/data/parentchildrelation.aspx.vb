﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports CAMACloud.BusinessLogic
Imports System.Web.Services
Imports System.Drawing

Public Class parentchildrelation
    Inherits System.Web.UI.Page
    Dim mainTable As New DataTable()
    Dim dtReferenceKey As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            mainTable = DataSource.GetParentChildTable(True)
            BindTree()
            TreeView1.ExpandAll()
            PanelDisplayContent.Visible = False
        End If
    End Sub
    Private Sub BindTree()
        Dim dt As New DataTable
        Dim tn As New TreeNode()
        tn.Text = "Parcel"
        tn.Value = "Parcel"
        TreeView1.Nodes.Add(tn)
        PopulateSubLevel("Parcel", tn)
    End Sub
    Private Sub PopulateNodes(ByVal dt As DataTable, _
  ByVal nodes As TreeNodeCollection)
        For Each dr As DataRow In dt.Rows
            Dim tn As New TreeNode()
            tn.Text = dr("ChildTable").ToString()
            tn.Value = dr("Id").ToString()
            nodes.Add(tn)
            Dim k As DataRow()
            k = mainTable.Select("ParentTable='" + dr("ChildTable").ToString() + "'")

            If (CInt(k.Length) > 0) Then
                PopulateSubLevel(dr("ChildTable"), tn)
            End If
        Next
    End Sub
    Private Sub PopulateSubLevel(ByVal TableName As String, _
  ByVal parentNode As TreeNode)
        Dim dt As New DataTable()
        dt = mainTable.Select("ParentTable='" + TableName + "'").CopyToDataTable()
        PopulateNodes(dt, parentNode.ChildNodes)
    End Sub
    Protected Sub treeview1_DataBound(sender As Object, e As EventArgs)
        TreeView1.CollapseAll()
        Dim tn As TreeNode = TreeView1.SelectedNode
        tn.Expand()
        While tn.Parent IsNot Nothing
            tn = tn.Parent
            tn.Expand()
        End While
    End Sub

    Protected Sub TreeView1_SelectedNodeChanged(sender As Object, e As EventArgs) Handles TreeView1.SelectedNodeChanged
        Dim nodeValue As String
        Dim TableName As String
        nodeValue = TreeView1.SelectedNode.Value
        TableName = TreeView1.SelectedNode.Text
        Dim str As String = TreeView1.SelectedNode.Text + " Table Key Relationship"
        lblHead.Text = str
        lblHead.Visible = True
        BindTableGrid(nodeValue)
        BindReferenceKeyGrid(nodeValue)
        PanelDisplayContent.Visible = True
    End Sub
    Private Sub BindTableGrid(tableId As String)
        Dim dtTable As New DataTable
        Dim sqlstr As String
        sqlstr = " SELECT DISTINCT df.TableId,df.SourceTable,df.Id,df.Name AS FieldName,dk.IsPrimaryKey,df.DisplayLabel,df.DataType  FROM DataSourceField df LEFT JOIN DataSourceKeys dk ON df.TableId=dk.TableId  and Df.Id=dk.FieldId WHERE df.TableId=" + tableId + " and dk.IsPrimaryKey Is Null UNION ALL "
        sqlstr += "SELECT DISTINCT df.TableId,df.SourceTable,df.Id,df.Name AS FieldName,dk.IsPrimaryKey,df.DisplayLabel,df.DataType  FROM DataSourceField df LEFT JOIN DataSourceKeys dk ON df.TableId=dk.TableId  and Df.Id=dk.FieldId WHERE df.TableId=" + tableId + " and dk.IsPrimaryKey =1 ORDER BY df.Id "
        dtTable = Database.Tenant.GetDataTable(sqlstr)
        If dtTable IsNot Nothing Then
            gvDataSourceTable.DataSource = dtTable
            gvDataSourceTable.DataBind()
        End If
    End Sub

    Protected Sub GridOnDataBound(sender As Object, e As EventArgs)
        Dim row As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal)
        Dim cell As New TableHeaderCell()
        cell.Text = TreeView1.SelectedNode.Text
        cell.ColumnSpan = 5
        row.Controls.Add(cell)
        row.BackColor = ColorTranslator.FromHtml("#3AC0F2")
        gvDataSourceTable.HeaderRow.Parent.Controls.AddAt(0, row)
    End Sub

    Protected Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        If IsPostBack Then
            RunScript("setRelation();")
        End If
    End Sub
    Private Sub BindReferenceKeyGrid(tableId As String)
        Dim sqlstr As String
        sqlstr = "select dd.TableId,dd.SourceTable,dd.Name AS Field,df.SourceTable As ReferenceTable,df.name AS ReferenceField from DataSourceKeys dk inner join DataSourceField df on df.id=dk.ReferenceFieldId inner join DataSourceField dd on dd.Id=dk.FieldId where IsReferenceKey = 1 And dk.TableId = " + tableId
        dtReferenceKey = Database.Tenant.GetDataTable(sqlstr)
        Try
            If dtReferenceKey.Rows.Count = 0 Then
                dtReferenceKey.Rows.Add(dtReferenceKey.NewRow())
                gvReferenceRelationships.DataSource = dtReferenceKey
                gvReferenceRelationships.DataBind()
            Else
                gvReferenceRelationships.DataSource = dtReferenceKey
                gvReferenceRelationships.DataBind()
            End If
            ViewState("dtReferenceKey") = dtReferenceKey
        Catch ex As Exception
        End Try
    End Sub


    Protected Sub gvReferenceRelationships_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvReferenceRelationships.RowDataBound
        Dim SourceTable As String = dtReferenceKey.Rows(0)("SourceTable").ToString()
        Dim ReferenceTable As String = dtReferenceKey.Rows(0)("ReferenceTable").ToString()
        Dim sql As String
        Sql = "select  name from DataSourceField where SourceTable ={0}".SqlFormatString(SourceTable)
        Dim dtSourceField As DataTable = Database.Tenant.GetDataTable(sql)
        sql = ""
        sql = "select  name from DataSourceField where SourceTable ={0}".SqlFormatString(ReferenceTable)
        Dim dtReferenceField As DataTable = Database.Tenant.GetDataTable(sql)

        If e.Row.RowType = DataControlRowType.Header Then
            Dim lblSourceTable As Label = DirectCast(e.Row.FindControl("lblSourceTable"), Label)
            lblSourceTable.Text = SourceTable
            Dim lblReferenceTable As Label = DirectCast(e.Row.FindControl("lblReferenceTable"), Label)
            lblReferenceTable.Text = ReferenceTable
        End If

        If e.Row.RowType = DataControlRowType.Footer Then

            Dim ddlAddSourceField As DropDownList = DirectCast(e.Row.FindControl("ddlAddSourceField"), DropDownList)
            Dim ddlAddReferenceField As DropDownList = DirectCast(e.Row.FindControl("ddlAddReferenceField"), DropDownList)

            ddlAddSourceField.Items.Clear()
            ddlAddSourceField.FillFromTable(dtSourceField, True, )

            ddlAddReferenceField.Items.Clear()
            ddlAddReferenceField.FillFromTable(dtReferenceField, True, )
        End If
        If e.Row.RowType = DataControlRowType.DataRow Then
            If (e.Row.RowState And DataControlRowState.Edit) > 0 Then
                Dim ddlEditSourceField As DropDownList = DirectCast(e.Row.FindControl("ddlEditSourceField"), DropDownList)
                Dim ddlEditReferenceField As DropDownList = DirectCast(e.Row.FindControl("ddlEditReferenceField"), DropDownList)

                ddlEditSourceField.Items.Clear()
                ddlEditSourceField.FillFromTable(dtSourceField, True, )

                ddlEditReferenceField.Items.Clear()
                ddlEditReferenceField.FillFromTable(dtReferenceField, True, )

                Dim dr As DataRowView = TryCast(e.Row.DataItem, DataRowView)
                ddlEditSourceField.SelectedItem.Text = dr(2).ToString()
                ddlEditReferenceField.SelectedItem.Text = dr(4).ToString()
            End If
        End If
    End Sub

    Protected Sub gvReferenceRelationships_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvReferenceRelationships.RowCommand
        If e.CommandName.Equals("AddRelationship") Then
            Dim dt As DataTable
            dt = ViewState("dtReferenceKey")
            If dt.Rows.Count > 0 Then
                Dim tableId As String = dt.Rows(0)("TableId").ToString()
                Dim SourceTable As String = dt.Rows(0)("SourceTable").ToString()
                Dim ReferenceTable As String = dt.Rows(0)("ReferenceTable").ToString()
                Dim SourceField As String = DirectCast(gvReferenceRelationships.FooterRow.FindControl("ddlAddSourceField"), DropDownList).SelectedItem.Text
                Dim ReferenceField As String = DirectCast(gvReferenceRelationships.FooterRow.FindControl("ddlAddReferenceField"), DropDownList).SelectedItem.Text
                If SourceField <> "-- Select --" And ReferenceField <> "-- Select --" Then
                    Dim sql As String
                    sql = "EXEC ccad_AddReferanceKey {0},{1},{2},{3}".SqlFormatString(SourceTable, SourceField, ReferenceTable, ReferenceField)
                    Dim Msg As String = Database.Tenant.GetStringValue(sql)
                    Alert(Msg)
                    BindReferenceKeyGrid(tableId)
                Else
                    Alert("You should select both field values.")
                End If
            End If
        End If
    End Sub

    Protected Sub gvReferenceRelationships_RowEditing(sender As Object, e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvReferenceRelationships.RowEditing
        gvReferenceRelationships.EditIndex = e.NewEditIndex
        ViewState("oldSourceField") = DirectCast(gvReferenceRelationships.Rows(e.NewEditIndex).FindControl("lblFieldName"), Label).Text
        ViewState("oldReferenceField") = DirectCast(gvReferenceRelationships.Rows(e.NewEditIndex).FindControl("lblReferenceField"), Label).Text
        Dim dt As DataTable
        dt = ViewState("dtReferenceKey")
        If dt.Rows.Count > 0 Then
            Dim tableId As String = dt.Rows(0)("TableId").ToString()
            BindReferenceKeyGrid(tableId)
        End If
    End Sub

    Protected Sub gvReferenceRelationships_RowCancelingEdit(sender As Object, e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles gvReferenceRelationships.RowCancelingEdit
        gvReferenceRelationships.EditIndex = -1
        Dim dt As DataTable
        dt = ViewState("dtReferenceKey")
        If dt.Rows.Count > 0 Then
            Dim tableId As String = dt.Rows(0)("TableId").ToString()
            BindReferenceKeyGrid(tableId)
        End If
    End Sub

    Protected Sub gvReferenceRelationships_RowUpdating(sender As Object, e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles gvReferenceRelationships.RowUpdating
        Dim dt As DataTable
        dt = ViewState("dtReferenceKey")
        If dt.Rows.Count > 0 Then
            Dim tableId As String = dt.Rows(0)("TableId").ToString()
            Dim SourceTable As String = dt.Rows(0)("SourceTable").ToString()
            Dim ReferenceTable As String = dt.Rows(0)("ReferenceTable").ToString()
            Dim OldSourceField As String = ViewState("oldSourceField")
            Dim OldReferenceField As String = ViewState("oldReferenceField")
            Dim NewSourceField As String = DirectCast(gvReferenceRelationships.Rows(e.RowIndex).FindControl("ddlEditSourceField"), DropDownList).SelectedItem.Text
            Dim NewReferenceField As String = DirectCast(gvReferenceRelationships.Rows(e.RowIndex).FindControl("ddlEditReferenceField"), DropDownList).SelectedItem.Text
            Dim sql As String
            sql = "EXEC ccad_UpdateReferanceKey {0},{1},{2},{3},{4},{5}".SqlFormatString(SourceTable, OldSourceField, NewSourceField, ReferenceTable, OldReferenceField, NewReferenceField)
            Dim Msg As String = Database.Tenant.GetStringValue(sql)
            Alert(Msg)
            ViewState("oldSourceField") = ""
            ViewState("oldReferenceField") = ""
            gvReferenceRelationships.EditIndex = -1
            BindReferenceKeyGrid(tableId)
        End If
    End Sub

    Protected Sub gvReferenceRelationships_RowDeleting(sender As Object, e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvReferenceRelationships.RowDeleting
        Dim dt As DataTable
        dt = ViewState("dtReferenceKey")
        If dt.Rows.Count > 1 Then
            Dim tableId As String = dt.Rows(0)("TableId").ToString()
            Dim SourceTable As String = dt.Rows(0)("SourceTable").ToString()
            Dim ReferenceTable As String = dt.Rows(0)("ReferenceTable").ToString()
            Dim SourceField As String = DirectCast(gvReferenceRelationships.Rows(e.RowIndex).FindControl("lblFieldName"), Label).Text
            Dim ReferenceField As String = DirectCast(gvReferenceRelationships.Rows(e.RowIndex).FindControl("lblReferenceField"), Label).Text
            Dim row As GridViewRow = DirectCast(gvReferenceRelationships.Rows(e.RowIndex), GridViewRow)
            Dim sql As String
            sql = "EXEC ccad_RemoveReferanceKey {0},{1},{2},{3}".SqlFormatString(SourceTable, SourceField, ReferenceTable, ReferenceField)
            Dim Msg As String = Database.Tenant.GetStringValue(sql)
            Alert(Msg)
            BindReferenceKeyGrid(tableId)
        Else
            Alert("You should keep at least one.")
        End If
    End Sub

End Class
