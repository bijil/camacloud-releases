﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/DataSetup.master" ValidateRequest="false"
     AutoEventWireup="false" Inherits="CAMACloud.Console.datasetup_data_categories" Codebehind="categories.aspx.vb" %>

<%@ Import Namespace="CAMACloud.Console" %>
<%@ Import Namespace="CAMACloud" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
<style>
.masklayers {
		    background: black;
		    opacity: 0.5;
		    top: 0%;
		    width: 100%;
		    height: 100%;
		    bottom: 0%;
		    position: absolute;
		    display: none;
		}
		
.maskinglayer {
		   
		    top: 0%;
		    width: 97%;
		    height: auto;
		    bottom: 0%;
		    position: absolute;
		    display: none;
		}
		
.txtNote{
			width:500px;
			max-height:auto;
		}
.disableClosebtn{
   pointer-events: none;
}
.cat-field {
    user-select: none;
}
    .cat-field[multi-selected = "1"] {
        border: 1px solid blue;
        cursor:pointer !important;
    }
    .cat-field[multi-selected = "1"] .a16.del16{
        filter: grayscale(0.65);
    }
    .multi-remove {
        display: none;
        cursor: pointer;
        align-items: center;
    }
    .multi-remove .del16 {
        display: inline-flex;
        width: 16px;
        height: 16px;
        margin-right: 3px;
    }
    .multi-remove>span {
        font-weight: 600;
    }
</style>
  
    <script type="text/javascript">

        var nbsetting = '<%=ClientSettings.NeighborhoodDisplayLabelFullName%>';
        var catHeight;
        var dialogBoxEnabled = false;
        function setPageDimensions(cheight) {
            var dtl = cheight - 40;
            $('.fields-list').height(dtl);
            catheight = dtl + 75;
            $('.category-list').height(catheight);
        }

        $(document).keypress(
            function (event) {
                if (event.which == '13' && document.activeElement.tagName.toString() != "TEXTAREA") {
                    event.preventDefault();
                }
            });

        $(function () {
            Sys.Application.add_load(function () {
                $(".category-list").css("height", catheight);
            });
            bindHighlightEvent();

        });

    </script>
    <script type="text/javascript">
        var sortOldIndex;
        function editCategory(b) {
            var co = $(b).parents('.category');
            $(co).addClass('editmode');
            $(".cat-num", co).hide();
            $('input[type="text"]', co).val($.trim($('.cat-name', co).text()));
            return false;
        }


        function bindHighlightEvent() {
            $('.mfield.cat-added').mouseover(function () {
                var fieldids = $(this).attr('fieldid');
                $('.cat-field[fieldid="' + fieldids + '"]').css("background-color", "#56b0e3");
            });

            $('.mfield.cat-added').mouseout(function () {
                var fieldids = $(this).attr('fieldid');
                $('.cat-field[fieldid="' + fieldids + '"]').css("background-color", "#F0F0F0");
            });
        }

        function deleteCategory(b) {
            var co = $(b).parents('.category');
            var catName = $.trim($('.cat-name', co).text());
            var catId = $(co).attr('catid');
            if (!confirm('Are you sure you want to delete the ' + catName + ' category and release the contained fields?')) { return false; }
            $ds('removecategory', {
                CatId: catId,
                cname: catName
            }, function (data) {
                if (data.status == "OK") {
                    $(co).fadeOut();
                    $('.cat-added[catid="' + catId + '"]').removeClass('cat-added');
                    bindHighlightEvent();
                }
                window.location.href = "/protected/datasetup/data/categories.aspx";
            });
        }

        function SaveChanges() {
            EnableCloseBtn(false);
            mask();
            var HeaderInfo = $('#<%= txtHeaderInfo.ClientID()%>').val();
            if (HeaderInfo) {
                HeaderInfo = HeaderInfo.replace(/</g, '##').replace(/>/g, '@@')
                $('#<%= txtHeaderInfo.ClientID()%>').val(HeaderInfo);
            }
        }

        function saveCatName(b) {
            var dupe = false;
            //var c = $('.category').find('.cat-name').text();
            let c = []
            $('.category').find('.cat-name').each((i, el) => { c.push($(el)[0].innerText) })
            console.log(c)
            //var res = c.split(" ");
            var co = $(b).parents('.category');
            var oldName = $.trim($('.cat-name', co).text());
            var newName = $('input[type="text"]', co).val();
            if (newName == '') {
                alert(" Category name cannot be blank");
                return cancelEdit(b);
            }
            if ((oldName == newName) || (newName.length > 50)) {
                return cancelEdit(b);
            }
            dupe = c.includes(newName);
            if (dupe == true) {
                alert(" The category name already exists");
                return cancelEdit(b);
            }

            var catId = $(co).attr('catid');
            $ds('renamecategory', {
                CatId: catId,
                NewName: newName,
                OldName: oldName
            }, function (data) {
                if (data.status == "OK") {
                    $('.cat-name', co).html(newName);
                    $('.controls input', co).attr('onclick', 'editCatProperties("' + catId + '", "' + newName + '");')
                    $(".cat-num", co).show();
                    return cancelEdit(b);
                }
            });
        }
        function toggleExpand() {
            var currentTog = $('.cat-expand-collpase').hasClass('min') ? 'min' : 'max';
            $('.category-list').children('div.category').find('a.minmax').each(function (r) {
                if ($('.cat-expand-collpase').hasClass('min')) {
                    $(this).parents('.category').removeClass('minimized');
                    $(this).switchClass('max16', 'min16');
                }
                else {
                    $(this).parents('.category').addClass('minimized');
                    $(this).switchClass('min16', 'max16');
                }
                maxChosser(this, currentTog);
            });
            if ($('.cat-expand-collpase').hasClass('min')) {
                $('.cat-expand-collpase').switchClass('min', 'max');
                $('.cat-expand-collpase').find('span').html('Collapse All');
                $('.exp-col').switchClass('max16', 'min16');
            }
            else {
                $('.cat-expand-collpase').switchClass('max', 'min');
                $('.cat-expand-collpase').find('span').html('Expand All');
                $('.exp-col').switchClass('min16', 'max16');
            }
        }
        function cancelEdit(b) {
            $(b).parents('.category').removeClass('editmode');
            $(".cat-num", $(b).parents('.category')).show();
            return false;
        }


        function toggleMinMax(b) {
            if ($(b).hasClass('max16')) {
                $(b).parents('.category').removeClass('minimized');
                $(b).switchClass('max16', 'min16');
            } else {
                $(b).parents('.category').addClass('minimized');
                $(b).switchClass('min16', 'max16');
            }
            return false;
        }

        $(finalizePage);
        function makeFieldsDraggable() {
            $('.master-field').sortable({ containment: ".app-page-table" }).disableSelection();
            $('.cat-fields').droppable({
                drop: function (event, ui) {
                    var src = event.srcElement;
                    if ($(src).hasClass('mfield') && (!$(src).hasClass('cat-added'))) {
                        var fieldId = $(src).attr('fieldid');
                        var tableId = $(src).attr('tableid');
                        var tname = $(src).attr('tablename');
                        var target = event.target;
                        var catId = $(target).attr('catid');
                        var cname = $('.category[catid=' + catId + '] span.cat-name.view').text();
                        var fname = $('.master-field span[fieldid=' + fieldId + ']').html();
                        var e, l, label;
                        $('select').each(function () { var selected = $(':selected', this); e = (selected.closest('optgroup').attr('label')); l = ((selected.closest('optgroup').text())) });
                        if (e == 'Parcel Data' && l.split(',').length > 1) {
                            label = tname + '.' + fname;
                        }
                        else {
                            label = fname;
                        }
                        var postdata = {
                            FieldId: fieldId,
                            CatId: catId,
                            TableId: tableId,
                            Catname: cname,
                            Fieldname: fname
                        };
                        console.log(postdata);
                        $ds('addfieldtocategory', postdata, function (resp) {
                            if (resp.status == "OK") {
                                var fName = $(src).html();
                                var newField = '<div class="cat-field" fieldid="' + fieldId + '"><a class="a16 del16" onclick="removeField(' + fieldId + ',' + catId + ', $(this).parent())"></a><span>' + label + '</span></div>';
                                $(target).html($(target).html() + newField);
                                $(src).attr('catid', catId);
                                $(src).addClass('cat-added');
                                bindHighlightEvent();
                            } else {
                                console.log(resp.message);
                            }
                        }, function (message) {
                            alert(message)
                        });
                    }

                }
            });

            $('.category-list').sortable({
                containment: ".app-page-table",
                start: function (event, ui) {
                    var o = ui.item[0];
                    sortOldIndex = $(o).index();

                    clearSelection();
                },
                stop: function (event, ui) {
                    var o = ui.item[0];
                    var catId = $(o).attr('catid')
                    var index = $(o).index();
                    var newIndex = (index + 1) * 10;

                    console.log(sortOldIndex, index);

                    if (sortOldIndex > index) {
                        newIndex -= 5;
                    } else {
                        newIndex += 5;
                    }

                    $ds('movecategory', {
                        NewIndex: newIndex,
                        CatId: catId
                    }, function (data) {
                        if (data.status == "OK") {

                        }
                    });

                }
            });

            $('.cat-fields').sortable({
                containment: ".app-page-table",
                start: function (event, ui) {
                    var o = ui.item[0];
                    sortOldIndex = $(o).index();

                    clearSelection();
                },
                stop: function (event, ui) {
                    var o = ui.item[0];
                    var fieldId = $(o).attr('fieldid')
                    var index = $(o).index();
                    var flength = $('.cat-field', this).length - 1;
                    if (flength < 0) flength = 0;
                    var fieldslist = "";
                    $('.cat-field', this).each(function (index) {
                        var fieldids = $(this).attr('fieldid')
                        fieldslist += fieldids;
                        if (flength != index)
                            fieldslist += ",";
                    })
                    var newIndex = (index + 1) * 10;
                    if (sortOldIndex > index) {
                        newIndex -= 5;
                    } else {
                        newIndex += 5;
                    }

                    $ds('movefield', {
                        Fieldslist: fieldslist,
                        NewIndex: newIndex,
                        FieldId: fieldId
                    }, function (data) {
                        if (data.status == "OK") {

                        }
                    });

                }
            });
            if (dialogBoxEnabled) {
                $('.edit-Cat-popup').dialog({
                    draggable: true
                });
                dialogBoxEnabled = false;
            }
        }

        $(document).on('click', function (e) {
            if (e.shiftKey) {
                if ($(e.target).parents(".category").length && ($(e.target).parents('.cat-field').length || $(e.target).hasClass('cat-field'))) {
                    let el = $(e.target).hasClass('cat-field') ? $(e.target) : $(e.target).parents('.cat-field');

                    let catId = el.parents('.cat-fields').attr('catid');
                    let previousCatId = $('.cat-field[multi-selected=1]').parents('.cat-fields').attr('catid');

                    if (previousCatId != catId) {
                        $(`.cat-fields[catid=${previousCatId}] .cat-field[multi-selected]`).removeAttr('multi-selected');
                        $('.multi-remove', $(`.category[catid=${previousCatId}]`)).css('display', 'none');
                    }

                    if (el.attr('multi-selected') == 1) el.removeAttr('multi-selected');
                    else el.attr('multi-selected', 1).droppable("disable");

                    if ($('.cat-field[multi-selected=1]').length) $('.multi-remove', el.parents('.category')).css('display', 'flex');
                    else $('.multi-remove').css('display', 'none');
                }

            } else clearSelection();
        });

        function clearSelection() {
            $('.multi-remove').css('display', 'none');
            $('.cat-field').removeAttr('multi-selected').droppable("enable");
        }

        function removeMultiFields() {
            if (!confirm("Are you sure you want to remove the selected fields from the Field Category?\n\nField Settings will remain in-tact if the fields are added back to the Field Category later.")) return false;

            let fieldIds = [], fNames = "",
                catId = $('.cat-field[multi-selected=1]').parents('.cat-fields').attr('catid');


            $('.cat-field[multi-selected=1]').each((i, elem) => {
                fieldIds.push($(elem).attr('fieldid'));
                fNames += $('span', $(elem)).text() + ",";
            })

            if (fieldIds && catId) {

                let cName = $('.category[catid=' + catId + '] span.cat-name.view').text();

                $ds('removemultiplefields', {
                    CatName: cName,
                    FieldNames: fNames.slice(0, -1),
                    FieldId: fieldIds.toString(),
                    CatId: catId
                }, function (data) {
                    if (data.status == "OK") {
                        fieldIds.forEach((fid) => {
                            $('.cat-field[fieldid=' + fid + ']').fadeOut();
                            $('span[fieldid=' + fid + ']').removeClass('cat-added');
                        })
                        bindHighlightEvent();
                    }
                });
            }

        }

        function removeField(fieldId, catId, cf) {
            if ($('.cat-field[fieldid=' + fieldId + '][multi-selected=1]').length <= 0) {
                if (!confirm("Are you sure you want to remove this field from the Field Category?\n\nField Settings will remain in-tact if this field is added back to the Field Category later.")) { return false; }
                var cname = $('.category[catid=' + catId + '] span.cat-name.view').text();
                var fname = $('.cat-field[fieldid=' + fieldId + '] span').html();
                $ds('removefield', {
                    Catname: cname,
                    Fieldname: fname,
                    FieldId: fieldId,
                    CatId: catId
                }, function (data) {
                    if (data.status == "OK") {
                        $(cf).fadeOut();
                        $('span[fieldid=' + fieldId + ']').removeClass('cat-added');
                        bindHighlightEvent();
                        $(cf).remove();
                    }
                });
            }
        }

        function setTableListStyles() {
            var aux = '[AUX]';
            var nbhd = '[NBHD]';
            var ddl = $('#ddlDataSource');
            $(ddl).append('<optgroup data label="Parcel Data"></optgroup>');
            $(ddl).append('<optgroup aux label="Auxiliary Table"></optgroup>');
            $(ddl).append('<optgroup nbhd label="' + nbsetting + ' Data"></optgroup>');
            var gd = $('optgroup[data]', ddl);
            var ga = $('optgroup[aux]', ddl);
            var gn = $('optgroup[nbhd]', ddl);
            $('option', ddl).each(function () {
                var li = this;
                var text = $(li).text();
                if (text.startsWith(aux)) {
                    text = text.replace(aux, '');
                    $(li).text(text);
                    $(li).attr('aux', 'true');
                    $(ga).append(li);
                } else if (text.startsWith(nbhd)) {
                    text = text.replace(nbhd, '');
                    $(li).text(text);
                    $(li).attr('nbhd', 'true');
                    $(gn).append(li);
                }
                else {
                    $(gd).append(li);
                }
            });
        }

        function finalizePage() {
            makeFieldsDraggable();
            setTableListStyles();
            minMaxChange();
            setTimeout(() => { setDialogTop(), 0 });
        }

        function showCatPopup(title) {
            $('.edit-Cat-panel').html('');
            $('.edit-Cat-popup').dialog({
                modal: true,
                width: 675,
                height: 'auto',
                minHeight: "auto",
                position: ['center', 70],
                draggable: false,
                resizable: false,
                title: title,
                open: function (type, data) {

                    EnableCloseBtn(false);

                    $(this).parent().appendTo("form");
                    if ($('.showSidePanel').is(":visible"))
                        $('.showSidePanel').css('z-index', '1000');
                },
                close: function (event, ui) {
                    if ($('.showSidePanel').is(":visible"))
                        $('.showSidePanel').css('z-index', '5000');
                }
            });
            setDialogTop();
        }

        function EnableCloseBtn(flg) {
            if (flg)
                $('.ui-dialog-titlebar-close').removeClass("disableClosebtn");
            else
                $('.ui-dialog-titlebar-close').addClass("disableClosebtn");
        }

        function setDialogTop() {
            var wtop = window.scrollY;
            var lftHgt = $('.left-panel2').height();
            var visble = $('.left-panel').is(':visible');
            if (wtop < 80) { if (lftHgt < 600) if (!visble) { wtop = 0; } else { wtop = 70; } }
            if (document.documentElement.scrollHeight > $('body').height())
                wtop -= (document.documentElement.scrollHeight - $('body').height())
            $('.ui-dialog').css('top', wtop);
        }
        window.onkeydown = function (e) { if (e.ctrlKey && (e.which == 65 || e.which == 97)) { var v = document.activeElement.scrollHeight; $(document.activeElement).height(v); } }

        function setHeight(idname) {
            idname.addEventListener('keydown', autosize);
        }

        function setH(idname) {
            idname.style.cssText = 'height:49px';
            $('.edit-Cat-panel').height('auto');
        }

        function hideCatPopup() {
            $('.edit-Cat-popup').dialog('close');
        }

        function editCatProperties(fieldId, name) {
            try {
                dialogBoxEnabled = true;
                showCatPopup('Edit Properties - ' + name)
            } catch (e) {

            }
            return false;
        }

        var minCat = [];

        function maxChosser(cat, minOrMaxTog) {
            var catClass = $(cat).parents('.category').attr('catid');
            var index = minCat.indexOf('' + catClass + '');
            if (minOrMaxTog) {
                if (minOrMaxTog == 'min') {
                    minCat.splice(index, 1);
                    $('#<%= catMinMax.ClientID()%>').val(minCat);
                }
                else{
                    if (index == '-1') {
                        minCat.push(catClass);
                        $('#<%= catMinMax.ClientID()%>').val(minCat);
                    }
                }
            }
            else {
            	if (cat.className.match('min16') != null) {
	                if (index == '-1') {
	                    minCat.push(catClass);
	                    $('#<%= catMinMax.ClientID()%>').val(minCat);
	                }
	            }
	            else { 
	                minCat.splice(index, 1);
	                $('#<%= catMinMax.ClientID()%>').val(minCat);
	            }
            } 
        }
        
        function minMaxChange() {
            var hfId=[];
            hfId = $('#<%= catMinMax.ClientID()%>').val();
            var myval = hfId.split(',');
            $.each(myval, function (key, catId) {
                $('.category[catid=' + catId + ']').addClass('minimized');
                $('.category[catid=' + catId + '] .min16').switchClass('min16', 'max16');
            });
            if (hfId != "") {
                $('.cat-expand-collpase').switchClass('max', 'min');
                $('.cat-expand-collpase').find('span').html('Expand All');
                $('.exp-col').switchClass('min16', 'max16');
            }
        }

        function mask(info, error, errorActionName, callback) {
            $('.edit-Cat-popup').addClass('masklayers');
            //$('.masklayers').height($(window).height());

            $('.masklayers').show();
            $('.maskinglayer').show();
        }
        function CloseDialog() {
            $('.maskinglayer').hide();
            $('.edit-Cat-popup').removeClass('masklayers');
        }

        function allowOnlyNumber(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function showLoadMask(info) {
            $('.masklayer').height($(document).height())
            $('.masklayer').show();
            info = 'Please wait ...'
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <h1>
        Setup Field Categories</h1>
    <asp:HiddenField ID="catMinMax" runat="server" Value="" />

    <table class="app-page-table">
        <tr>
            <td class="td-field-master">
                <div class="field-master" >
                    <h2>
                        Data Source Tables</h2>
                    <p>
                        Select data source table to list the fields. You can drag and drop fields to appropriate categories.</p>
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <p>
                                <asp:DropDownList runat="server" ID="ddlDataSource" AutoPostBack="true" ClientIDMode="Static" onchange="showLoadMask(this)" CssClass="select-dst"/>
                            </p>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <h3>
                        Available Fields</h3>
                    <asp:UpdatePanel runat="server" class="fields-list drop-box flist" ID="FieldList" ClientIDMode="Static">
                        <ContentTemplate>
                            <asp:Repeater runat="server" ID="rpFields" >
                                <ItemTemplate>
                                    <div class="master-field">
                                        <span fieldid='<%# Eval("Id") %>' class='mfield <%# MasterFieldClass() %>' catid='<%# Eval("CategoryId") %>' tableid='<%# Eval("FieldTableId") %>' tablename='<%# Eval("SourceTable") %>'>
                                            <%# Eval("Name")%></span></div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger EventName="SelectedIndexChanged" ControlID="ddlDataSource" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </td>
            <td>
                <div class="cat-editor">
                    <h2>
                        Manage Categories</h2>
                    <div class="div-cat-create ui-state-default ui-corner-all">
                        <div style="height: 28px;">
                            <div class="fl">
                                <label>
                                    Add new category:</label>
                                <asp:TextBox runat="server" ID="txtNewCategory"  MaxLength="50"/>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtNewCategory" ErrorMessage="*" ValidationGroup="Create" CssClass="validation-error" />
                                <asp:Button runat="server" ID="btnCreateCategory" Text="Create" ValidationGroup="Create" />
                            </div>
                            <div class="fr">
                                <asp:LinkButton runat="server" ID="lbExport" Text="Export Settings" />&nbsp;&nbsp;<span style="font-weight:normal;color:Black;">|</span>&nbsp&nbsp;
                                <asp:LinkButton runat="server" ID="lbImport" Text="Import" OnClientClick="document.getElementById('fuImportFile').click();return false;"/>
                            </div>
                        </div>
                        <div class="clear">
                            <div style="display:none;">
                                <asp:FileUpload runat="server" ID="fuImportFile" ClientIDMode="Static" onchange="document.getElementById('btnUploadImportFile').click()"  accept="application/xml" />
                                <asp:Button runat="server" ID="btnUploadImportFile" ClientIDMode="Static" />
                            </div>
                        </div>
                    </div>
                    <div class="cat-expand-collpase max" style="padding: 4px 0px 10px 8px;"> 
                    <a class="exp-col a16 min16" style="float:left;" onclick="toggleExpand();"></a>
                    <span> Collapse All</span>
                    </div>
                                            <asp:UpdatePanel ID="UpdatePanelCatFields" runat="server">
                            <ContentTemplate>
                    <div class="category-list">

                        <asp:Repeater runat="server" ID="rpCategories">
                            <ItemTemplate>
                                <div class="category" catid='<%# Eval("Id") %>'>
                                    <div class="props ca">
                                        <div class="fl">
                                            <a class="a16 minmax min16 view" onclick="toggleMinMax(this);maxChosser(this);"></a>
                                            <span class="cat-name view" title='<%# Eval("SourceTableName") %>'>
                                                <%# Eval("Name")%>  </span><span class="cat-num">(<%# Eval("Id") %>)</span><span class="cat-edit edit">
                                                    <input type="text" MaxLength = "50" />
                                                    <button onclick="saveCatName(this);return false;">Save</button>
                                                    <button onclick="return cancelEdit(this);">Cancel</button>
                                                </span>
                                        </div>
                                        <div class="fr view ">
                                            <div class="controls">
                                                <a class="a16 ed16" onclick="return editCategory(this);"></a>
                                                <a class="a16 del16" onclick="deleteCategory(this);return false;"></a>
                                                <asp:ImageButton ID="catSettings" class="a16 set16" CommandName='EditProperties' CommandArgument='<%# Eval("Id") %>' OnClientClick='<%# EditCatPropertiesScript()%>' runat="server" BorderStyle="None" ImageUrl="~/App_Static/css/icons16/settings.png" ImageAlign="Bottom" />
                                            </div>
                                            <a class="multi-remove" onclick="return removeMultiFields();"><span class="del16"></span><span>Remove Fields</span></a>
                                            
                                        </div>
                                    </div>
                                    <asp:HiddenField runat="server" ID="CatId" Value='<%# Eval("Id") %>' />
                                    <div class="cat-fields drop-box ca" catid='<%# Eval("Id") %>'>
                                        <asp:Repeater runat="server" ID="rpFields">
                                            <ItemTemplate>
                                                <div class="cat-field" title='<%# Eval("SourceTable") + "." + Eval("Name") %>' fieldid='<%# Eval("Id") %>'>
                                                    <a class="a16 del16" onclick='removeField(<%# Eval("Id") %>, <%# Eval("CategoryId") %>, $(this).parent())'></a>
                                                    <span><%#Eval("dispName")%></span></div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                       </ContentTemplate>
                      </asp:UpdatePanel>
                </div>
            </td>
        </tr>
    </table>

    <div runat="server" class="edit-Cat-popup" id="edit_Cat_popup" style="display: none;">
    <div class="maskinglayer" style="display: none; z-index: 20">
    <span style="margin-left: 45%; margin-top: 30%; align-items: center;position:relative;top:34%" class="app-mask-layer-info">
    <img src="/App_Static/images/loadingimg.gif"/><br>
    <span style="color: black; margin-left: 45%; margin-top: 20px;" >Please wait...</span></span>
   	</div>
		<asp:UpdatePanel ID="uplEdit" runat="server">
			<ContentTemplate>
				<asp:Panel runat="server" ID="pnlCatEdit" CssClass="edit-Cat-panel" Style="">
					<table>
						<tr runat="server" id="trHideExp">
							<td style="padding-right: 10px;">Hide Expression: </td>
							<td colspan="3">
                                <asp:HiddenField ID="hcId" runat="server" Value="" />
								<asp:TextBox runat="server" ID="txtHideExp" MaxLength="30" Width="500px" TextMode="MultiLine"
									Rows="3" Style="overflow-y: auto; resize: none;" />
							</td>
						</tr>
                        <tr runat="server" id="trHideRecExp">
							<td style="padding-right: 10px;">Hide Record Expression: </td>
							<td colspan="3">
								<asp:TextBox runat="server" ID="txtHideRecExp" MaxLength="30" Width="500px" TextMode="MultiLine"
									Rows="3" Style="overflow-y: auto; resize: none;" />
							</td>
						</tr>
                        <tr runat="server" id="trHearderInfo">
							<td style="padding-right: 10px;">Header Info: </td>
							<td colspan="3">
								<asp:TextBox runat="server" ID="txtHeaderInfo" MaxLength="30" Width="500px" TextMode="MultiLine"
									Rows="3" Style="overflow-y: auto; resize: none;" />
							</td>
						</tr>
                        <tr runat="server" id="trSrtExp">
							<td style="padding-right: 10px;">Sort Expression: </td>
							<td colspan="3">
								<asp:TextBox runat="server" ID="txtSrtExp" MaxLength="30" Width="500px" TextMode="MultiLine"
									Rows="3" Style="overflow-y: auto; resize: none;" />
							</td>
						</tr> 
						 <tr runat="server" id="trUniqueSiblingFields">
							<td style="padding-right: 10px;">Unique Fields In Siblings: </td>
							<td colspan="3">
								<asp:TextBox runat="server" ID="txtUniqueSiblingFields" MaxLength="30" Width="500px" TextMode="MultiLine"
									Rows="3" Style="overflow-y: auto; resize: none;" />
							</td>
						</tr>
						<tr>
                            <td style="padding-right: 30px;">Note: </td>
                            <td colspan="3">
                                <asp:TextBox runat="server" ID="txtNote" CssClass="txtNote" TextMode="MultiLine"  
                                Rows="3" Style="overflow-y: auto; resize: none;" onblur="setH(this);" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-right: 5px;">Disable Copy Record Expression: </td>
                            <td colspan="3">
                                <asp:TextBox runat="server" ID="txtDisablecpyRecrdExp" MaxLength="30" Width="500px" TextMode="MultiLine"  
                                Rows="3" Style="overflow-y: auto; resize: none;" />
                            </td>
                        </tr>
                        <tr runat="server" id="trClsCalcParam" visible ="false">
                            <td style="padding-right:10px">Class calculator parameters:</td>
                            <td colspan="3">
                                <asp:TextBox runat="server" ID="txtClsCalcParam" MaxLength="30" Width="500px" TextMode="MultiLine"
                                    Rows="3" Style="overflow-y: auto; resize: none;"></asp:TextBox>
                                </td>
                            </tr>
                            
                        <tr runat="server" id="trFilterField" style="height: 43px">
                              <td style="padding-right: 10px;">Filter Fields :</td>
                              <td>
                                <asp:TextBox runat="server" ID="txtFilterField" MaxLength="30" Width="143px" TextMode="MultiLine"
									Rows="2" Style="overflow-y: auto; resize: none;margin-right: 19px" />
							</td>
                            <td style="padding-right: 10px;">Unique Keys :</td>
                              <td>
                                <asp:TextBox runat="server" ID="txtUniqueKeys" MaxLength="30" Width="143px" TextMode="MultiLine"
									Rows="2" Style="overflow-y: auto; resize: none;" />
							</td>
                        </tr>
                        <tr runat="server" id="rdDP" visible ="false">
                            <td style="padding-right:10px; padding-top: 5px;">Display For:</td>
                            <td colspan="3" style="padding-top: 5px;padding-bottom: 10px;">
                                <asp:RadioButtonList runat="server" ID="rdDPlist" RepeatDirection="Horizontal">
                                	<asp:ListItem Text="Real Properties" Value="0" />
    								<asp:ListItem Text="Business Properties" Value="1" />
    								<asp:ListItem Text="All Properties" Value="2" />
    								</asp:RadioButtonList>
                                </td>
                            </tr>                           
                         <tr runat="server" id="trMinRec" style="height: 29px">
							<td style="padding-right: 10px;">Minimum Records: </td>
							<td>
								<asp:TextBox runat="server" ID="txtMinRec" MaxLength="9" Width="50px" onkeypress="return allowOnlyNumber(event);" TextMode="SingleLine"
									Rows="1" Style="resize: none;"/>
							</td>

                             <td style="padding-right: 10px;">Maximum Records: </td>
							<td colspan="3">
								<asp:TextBox runat="server"  ID="txtMaxRec"  MaxLength="9" Width="50px" onkeypress="return allowOnlyNumber(event);" TextMode="SingleLine"
									Rows="1" Style="resize: none;" />
							</td>
						</tr>
                         <tr style="height:24px">
                      		<td style="padding-right: 10px;">
								<asp:CheckBox runat="server" ID="chkShwcatgry" Text="Show Category" />
							</td>
						    <td style="padding-right: 10px;">
								<asp:CheckBox runat="server" ID="chkTabdata" Text="Tabular Data" />
							</td>
						    <td style="padding-right: 10px;">
								<asp:CheckBox runat="server" ID="chkEditexst" Text="Do Not Allow Edit Exist" />
							</td>
							<td style="padding-right: 10px;">
								<asp:CheckBox runat="server" ID="chkReadOnly" Text="Read Only" />
							</td>
                        </tr>
                        <tr style="height:24px">
                      		<td style="padding-right: 10px; white-space:nowrap">
								<asp:CheckBox runat="server" ID="chkAllowAddDelete" Text="Allow Add Delete" />
							</td>
						    <td style="padding-right: 10px;">
								<asp:CheckBox runat="server" ID="chkMultiGridStub" Text="Multi Grid Stub" />
							</td>
						    <td style="padding-right: 4px; white-space:nowrap">
								<asp:CheckBox runat="server" ID="chkDoNotShowInMultiGrid" Text="Do Not Show In MultiGrid" />
							</td>
							<td style="padding-right: 4px; white-space:nowrap">
								<asp:CheckBox runat="server" ID="chkAllowDeleteOnly" Text="Allow Delete Only" />
							</td>
							
                        </tr>
                     </table>
                     <asp:Button runat="server" ID="btnSaveChanges" Text="Save Changes" OnClientClick ="SaveChanges()"/>
                    </asp:Panel>
			</ContentTemplate>
		</asp:UpdatePanel>
	</div>
</asp:Content>
