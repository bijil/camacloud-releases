﻿Imports System.Net

Partial Class datasetup_data_import
	Inherits System.Web.UI.Page

	Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
		If Not IsPostBack Then
			chkImportPendingOnly.Enabled = chkImportParcelData.Checked

			If Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM ImportJob WHERE Status IN (0,1)") > 0 Then
				Response.Redirect("importstatus.aspx")
			End If
		End If
	End Sub

	Protected Sub chkImportParcelData_CheckedChanged(sender As Object, e As System.EventArgs) Handles chkImportParcelData.CheckedChanged, chkImportAuxiliaryData.CheckedChanged, chkNeighborhood.CheckedChanged
		If Not chkImportPendingOnly.Enabled Then
			chkImportPendingOnly.Checked = chkImportParcelData.Checked Or chkImportAuxiliaryData.Checked Or chkNeighborhood.Checked
		End If
		chkImportPendingOnly.Enabled = chkImportParcelData.Checked Or chkImportAuxiliaryData.Checked Or chkNeighborhood.Checked
	End Sub


	Protected Sub btnImport_Click(sender As Object, e As System.EventArgs) Handles btnImport.Click
		Dim errorList As New List(Of String)
		Dim warningList As New List(Of String)
		lblStatus.Text = ""


		Dim dt As DataTable = Database.Tenant.GetDataTable("EXEC [cc_CheckImportErrors] @ImportPrimary = {1}, @ImportData = {2}, @ImportAuxiliary = {3}, @ImportNeighborhood = {4}, @ImportLookup = {5}, @PendingOnly = {6}".FormatString(
		 User.Identity.Name,
		 chkImportPrimary.Checked.GetHashCode,
		 chkImportParcelData.Checked.GetHashCode,
		 chkImportAuxiliaryData.Checked.GetHashCode,
		 chkNeighborhood.Checked.GetHashCode,
		 chkLookUpTable.Checked.GetHashCode,
		 chkImportPendingOnly.Checked.GetHashCode()
		 ))

		For Each dr As DataRow In dt.Rows
			If dr.GetInteger("ErrorType") = 0 Then
				warningList.Add(dr.GetString("ErrorMessage"))
			Else
				errorList.Add(dr.GetString("ErrorMessage"))
			End If
		Next
		If errorList.Count > 0 Then
			lblStatus.Text = "You import request failed to start, because there are errors in your data setup."
			blErrors.DataSource = errorList
			blErrors.DataBind()
			Return
		ElseIf warningList.Count > 0 Then
			lblStatus.Text = "Your data setup has warnings. Click Import Anyway button to continue anyway, or make corrections and try again."
			blWarnings.DataSource = warningList
			blWarnings.DataBind()
			btnImport.Visible = False
			btnImportWithWarnings.Visible = True
			btnChangeOptions.Visible = True
			CheckBoxesEnabled = False
		Else
			InitiateImport()
		End If

	End Sub

	Protected WriteOnly Property CheckBoxesEnabled As Boolean
		Set(value As Boolean)
			chkImportPrimary.Enabled = value
			chkImportParcelData.Enabled = value
			chkImportAuxiliaryData.Enabled = value
			chkImportPendingOnly.Enabled = value
			chkLookUpTable.Enabled = value
			If value Then
				chkImportPendingOnly.Enabled = chkImportParcelData.Checked Or chkImportAuxiliaryData.Checked Or chkNeighborhood.Checked
			End If
		End Set
	End Property


	Protected Sub btnImportWithWarnings_Click(sender As Object, e As System.EventArgs) Handles btnImportWithWarnings.Click
		InitiateImport()
	End Sub

	Protected Sub InitiateImport()
		Database.Tenant.Execute("EXEC cc_PrepareDataTables")
		Dim res As DataRow = Database.Tenant.GetTopRow(
		 "EXEC cc_CreateImportJob @CreatedBy = {0}, @ImportPrimary = {1}, @ImportData = {2}, @ImportAuxiliary = {3}, @ImportNeighborhood = {4}, @ImportLookup = {5}, @PendingOnly = {6}".FormatString(
		  User.Identity.Name,
		  chkImportPrimary.Checked.GetHashCode,
		  chkImportParcelData.Checked.GetHashCode,
		  chkImportAuxiliaryData.Checked.GetHashCode,
		  chkNeighborhood.Checked.GetHashCode(),
		  chkLookUpTable.Checked.GetHashCode,
		  chkImportPendingOnly.Checked.GetHashCode()
		  ))
		Dim importResp As String = res.GetString("Message")
		Dim status As String = res.GetString("Status")
		Dim success As Boolean = (status = "OK")

		If success Then
			'Dim srq As New ServiceRequest("StartDataImport")
			'With srq
			'	.Add("OrganizationId", HttpContext.Current.GetCAMASession().OrganizationId)
			'	.Add("DatabaseId", 0)
			'End With
			'Dim sc As New ServiceConnector(IPAddress.Loopback, 9001)
			'Dim resp As ServiceResponse = sc.SendRequest(srq)
			'If resp.Success Then
			'	
			'Else
			'	Alert(resp.ErrorMessage)
			'	Database.Tenant.Execute("DELETE FROM ImportJob WHERE Id = " & res.GetInteger("JobId"))
			'End If
			Response.Redirect("importstatus.aspx")
		Else
			Alert(importResp)
		End If
	End Sub

	Protected Sub btnChangeOptions_Click(sender As Object, e As System.EventArgs) Handles btnChangeOptions.Click
		CheckBoxesEnabled = True
		btnChangeOptions.Visible = False
		btnImportWithWarnings.Visible = False
		btnImport.Visible = True
		blWarnings.Items.Clear()
		blErrors.Items.Clear()
		lblStatus.Text = ""
	End Sub
End Class


