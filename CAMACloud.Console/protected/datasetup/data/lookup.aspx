﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/DataSetup.master"
	AutoEventWireup="false" Inherits="CAMACloud.Console.datasetup_data_lookup" Codebehind="lookup.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
	<script type="text/javascript">
		$(setSelectColors);
		$(function () {
			$('.field-ismasterlookup').each(function () {
				$('input', this).change(function () {
					setTableField(this, 'IsMasterLookup', 'checkbox');
				});
			});
		});
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<h1>
		Lookup Table - Properties</h1>
	<p class="info">
		The ID, Name, Description fields from the Lookup Table will be automatically set
		when the tables are setup in <b>Table Layout</b> page. You may do any corrections
		to the fields if required.</p>
	<p class="info">
		<strong>Group:</strong> Specify Group field column if the lookup table is a Master Lookup table which contains several lookup groups. In some cases, the group name would be same as referred column name.
	</p>
	<p class="info">
		<strong>Reference:</strong> Specify the referred table name, in case there are duplicate group names containing similar items. In some cases, the referred table name will be part of master lookup name.
	</p>
	<asp:GridView runat="server" ID="gvAux">
		<RowStyle CssClass="dst-row" />
		<Columns>
			<asp:BoundField HeaderText="Lookup Table" DataField="Name" ItemStyle-Width="200px" />
			<asp:TemplateField HeaderText="ID Field">
				<ItemStyle Width="120px" />
				<ItemTemplate>
					<asp:HiddenField runat="server" ID="TID" Value='<%# Eval("Id") %>' ClientIDMode="Static" />
					<asp:DropDownList runat="server" ID="ddlCF1" onchange="setTableField(this, 'LookupIDField')" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Name Field">
				<ItemStyle Width="120px" />
				<ItemTemplate>
					<asp:DropDownList runat="server" ID="ddlCF2" onchange="setTableField(this, 'LookupNameField')" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Desc Field">
				<ItemStyle Width="120px" />
				<ItemTemplate>
					<asp:DropDownList runat="server" ID="ddlCF3" onchange="setTableField(this, 'LookupDescField')" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Value Field">
				<ItemStyle Width="120px" />
				<ItemTemplate>
					<asp:DropDownList runat="server" ID="ddlLV" onchange="setTableField(this, 'LookupValueField')" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Master">
				<ItemStyle Width="50px" HorizontalAlign="Center" />
				<HeaderStyle HorizontalAlign="Center" />
				<ItemTemplate>
					<asp:CheckBox runat="server" ID="chkMaster" Checked='<%# Eval("IsMasterLookup") %>' CssClass="field-ismasterlookup" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Group">
				<ItemStyle Width="150px" />
				<ItemTemplate>
					<asp:DropDownList runat="server" ID="ddlGroup" onchange="setTableField(this, 'LookupGroupField')" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Reference">
				<ItemStyle Width="150px" />
				<ItemTemplate>
					<asp:DropDownList runat="server" ID="ddlRef" onchange="setTableField(this, 'LookupReferredTableField')" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Sort Field">
				<ItemStyle Width="150px" />
				<ItemTemplate>
					<asp:DropDownList runat="server" ID="ddlSort" onchange="setTableField(this, 'LookupOrdinalField')" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
		<EmptyDataTemplate>
			<div>
				No tables available</div>
		</EmptyDataTemplate>
	</asp:GridView>
</asp:Content>
