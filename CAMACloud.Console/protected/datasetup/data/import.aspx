﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/DataSetup.master"
	AutoEventWireup="false" Inherits="CAMACloud.Console.datasetup_data_import" Codebehind="import.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
	<script type="text/javascript">

	</script>
	<style type="text/css">
		
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<h1>
		Import CAMA Data</h1>
	<asp:UpdatePanel ID="UpdatePanel1" runat="server">
		<ContentTemplate>
			<div class="import-job-panel" id="importJobPanel">
				<div class="b">
					<asp:Label runat="server" ID="lblStatus" Font-Bold="true" />
				</div>
				<asp:BulletedList runat="server" ID="blErrors" CssClass="import-errors" DisplayMode="Text">
				</asp:BulletedList>
				<asp:BulletedList runat="server" ID="blWarnings" CssClass="import-errors import-warnings">
				</asp:BulletedList>
				<asp:CheckBox runat="server" ID="chkImportPrimary" Text="Import list of parcels from the primary table."
					CssClass="job-setting" />
				<asp:CheckBox runat="server" ID="chkImportParcelData" Text="Import parcel data from the tables grouped under Parcel Data for the fields selected for inclusion."
					AutoPostBack="True" CssClass="job-setting" />
				<asp:CheckBox runat="server" ID="chkImportAuxiliaryData" Text="Import Auxiliary Data for all Selected Data Entry fields"
					AutoPostBack="True" CssClass="job-setting" />
				<asp:CheckBox runat="server" ID="chkNeighborhood" Text="Import neighborhood data" CssClass="job-setting" AutoPostBack="true" />
				<asp:CheckBox runat="server" ID="chkImportPendingOnly" Text="Import only pending data tables. All previously imported tables will not be re-imported."
					CssClass="job-setting" />
				
				<asp:CheckBox runat="server" ID="chkLookUpTable" Text="Import lookup tables" CssClass="job-setting" />
				<div class="link-panel" style="margin-top:10px;">
					<asp:LinkButton ID="btnImport" runat="server" Text="Start Import"  OnClientClick2="return confirm('The import process may take few hours depending on the size of your data.\n\n\nAre you sure you want to start data import now?')"/>
					<asp:LinkButton ID="btnImportWithWarnings" runat="server" Text="Import Anyway" Visible="false" OnClientClick2="return confirm('The import process may take few hours depending on the size of your data.\n\n\nAre you sure you want to continue running data import ignoring the warnings?')"/>
					<asp:LinkButton ID="btnChangeOptions" runat="server" Text="Change Options" Visible="false" />
				</div>
			</div>
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>
