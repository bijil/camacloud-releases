﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master" CodeBehind="datastatus.aspx.vb" Inherits="CAMACloud.Console.datastatus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .schema-node {
            padding-left: 2px;
        }

        h1, h2 {
            margin-bottom: 5px;
        }

        .trc {
            color: blue;
            font-weight: bold;
        }

            .trc[num="0"] {
                font-weight: normal;
                color: black;
            }

        .nrc {
            color: red;
            font-weight: bold;
        }

        .nrc[num="0"] {
            font-weight: normal;
            color: black;
            }

        .popup-box {
            display: none;
            background: #eee;
            border: 1px solid #ccc;
            padding: 10px;
            border-radius: 8px;
            box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
            position: absolute;
            min-width: 200px;
            width:auto;
            max-height: 190px;
        }
        .popup-body {
            padding: 0 10px 0 10px;
            overflow-y: auto;
            overflow-x: hidden;
            max-height: 170px;
            height: 90%;
            margin-top: 2px;
        }
        .popup-header {
            background-color: #545758;
            color: #FFF;
        }

        .popup-header h4 {
            margin: 0;
            padding: 0 5px 0 10px;
            line-height: 22px;
        }

        .popup-header h4 .popup-close {
            float: right;
            text-decoration: none;
            color: #FFF;
            margin-left:10px;
        }

        .popup-header h4 .popup-close:hover {
            color: #000;
        }

        .bottom:after {
            bottom: 12px;
            }

        .bottom:before {
            bottom: 13px;
            }

        .top:after {
             top: 14px;
            }

        .top:before {
            top: 13px;
            }

        
        .left:after {
            position: absolute;
            display: inline-block;
            border-top: 6px solid transparent;
            border-right: 6px solid #eee;
            border-bottom: 6px solid transparent;
            left: -6px;
            content: '';
            }

        .left:before {
            position: absolute;
            display: inline-block;
            border-top: 7px solid transparent;
            border-right: 7px solid #eee;
            border-bottom: 7px solid transparent;
            border-right-color: rgba(0, 0, 0, 0.2);
            left: -8px;
            content: '';
            }

        .right:after,
        .right:before {
            content: '';
            display: block;
            position: absolute;
            left: 100%;
            width: 0;
            height: 0;
            border-style: solid;
}

        .right:after {
            border-color: transparent transparent transparent  #eee;
            border-width: 10px;
        }

        .right:before {
            border-color: transparent transparent transparent #bdbdbd;
            border-width: 11px;
        }

         .popup-body::-webkit-scrollbar {
            width: 7px;
            height:7px;
        }

         .popup-body::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
            border-radius: 10px;
        }

         .popup-body::-webkit-scrollbar-thumb {
            border-radius: 10px;
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.5);
        }
         #report td, th {    
            border: 1px solid #ddd;
            text-align: left;
            padding: 2px;
        }
        #report {
            border: 1px solid #ddd;
            text-align: left;
            border-collapse: collapse;
            width: 100%;
        }
        .ParcelListReport{
			color: red;
			font-weight: bolder;
        }

    </style>
    <script type="text/javascript">
        var dataReoprt;
        $( document ).mouseup( function ( e )
        {
            var container = $(".popup-box");
            if (!container.is(e.target) && !$(e.target).hasClass('btnParcelList') && container.has(e.target).length === 0 && $(e.target).parents('.schema-node:first').length == 0 && $(e.target)) 
            {
                $('.popup-box').fadeOut('slow');
            }
            if ($(e.target).hasClass('btnParcelList')) $('.btnParcelList').click();
        });
        $(function () {
            $(".schema-node a").bind("click", function () {
                var source = $(this).offset();
                var sourceWidth = $(this).outerWidth();
                var tableName = $(this).find('b').text();
                var TableInfo = {
                    TableName: tableName
                };
                $ds('showcolumns', TableInfo, function (resp) {
                    loadColumns(tableName, source, sourceWidth, resp)
                });
                return false;
            });

            $(".schema-node").closest('td').prev('td').bind("click", function () {
                return false;
            });

            $('.popup-header h4').on('click', '.popup-close', function() {
                $('.popup-box').fadeOut('slow');
            } );

            
            $(".schema-node input[type=checkbox]").bind("click", function () {
                setVisibilityButton()
            });
        })

         function setVisibilityButton() {
         	getCount(function(count){
          		if (count > 0) 
              		$( '.ParcelList' ).css( 'visibility', 'visible' );
            	else 
              		$( '.ParcelList' ).css( 'visibility', 'hidden' );
              		 $('.popup-box').fadeOut('fast');
            });
         }
         function getCount(callback)
         {
             var count = 0, targetTable = [];
             $( "[id*=tvTables] input[type=checkbox]" ).each( function ()
             {
                 var isChecked = $( this ).is( ":checked" );
                 if ( isChecked )
                 {
                     count = count + 1;
                     var tableName=$( this ).attr('title').split('\n')[1].split(':')[1]
                     if(targetTable.indexOf(tableName) == -1) targetTable.push(tableName);
                 }
             } );
             if (callback) callback(count, targetTable)
         }
         
         function getParcelList() {
         getCount(function(count, targetTable) {
            if (count > 0) {
                $ds('getparcellist', {
                    TableNames: targetTable.toString(),
                }, function ( data, status ) {
                    var str
                    if (data && data.length > 0 )
                    {
                    	$( '.popup-body' ).removeClass( "ParcelListReport" );
                        dataReoprt = data;
                        str = "<table id='report' >"
                        $.each( data, function ( key, value )
                        {
                                str += "<tr><td>"+( value.ParcelId ) + "</td></tr>"
                        } );
                        str += "</table>"
                        $( '.ibtnExportToCSV' ).show()
                    }
                    else
                    {
                        str = "No Results Found !"
                        $( '.popup-body' ).addClass( "ParcelListReport" );
                        $('.ibtnExportToCSV' ).hide();
                    }
                    $( '.popup-body' ).html( str );
                    var cache = $( '.popup-header h4' ).children();
                    $( '.popup-header h4' ).empty();
                    $( '.popup-header h4' ).append($('#<%= hfKeyValue1.ClientID%>').val());
                    $( '.popup-header h4' ).append( cache );
                   
                    $( '.popup-box' ).css( "display", "inline-block" );

                    var pos = [];
                    pos[0] = 180;
                    pos[1] = $('.ParcelList').position().left - 58;
                    $( '.popup-box' ).removeClass( 'bottom' );
                    $( '.popup-box' ).removeClass( 'top' );
                    $( '.popup-box' ).removeClass( 'right' );
                    $( '.popup-box' ).removeClass( 'left' );
                    $( '.popup-box' ).css( { 'top': pos[0] + 'px', 'left': pos[1] + 'px' } );
                });
            }
            });
            return false;
         }
         
        function loadColumns(tableName, source, sourceWidth, data) {
            var str
            $( '.popup-body' ).removeClass( "ParcelListReport" );
            $.each(data, function (key, value) {
                if (str != undefined) 
                    str += (key + 1) + '.' + (value.Name) + "<br>"
                else
                    str = (key + 1) + '.' + (value.Name) + "<br>"

            });
            if (str != undefined && str != '') {
                var top;
                var left;
                var pos = [];
   
                $('.popup-body').html(str);
                var cache = $('.popup-header h4').children();
                $('.popup-header h4').empty();
                $('.popup-header h4').append(tableName);
                $( '.popup-header h4' ).append( cache );
                $('.ibtnExportToCSV' ).hide();
                $('.popup-box').css("display", "inline-block");

                //set top
                var sourceTop = source.top; //(parseFloat(source.top / $(window).height()) * 100).toFixed(2);
                var balanceWindowHeight = $(window).height() - sourceTop; //(parseFloat(100 - sourceTop)).toFixed(2);
                var contentHeight = $('.popup-box').outerHeight(); //(parseFloat($('.popup-box').outerHeight() / $(window).height()) * 100).toFixed(2);
                if (parseFloat(balanceWindowHeight) > parseFloat(contentHeight)) {
                    $('.popup-box').removeClass('bottom');
                    $('.popup-box').addClass('top');
                    top = (parseFloat(sourceTop - 10)).toFixed(2); //(parseFloat(sourceTop - 1.7)).toFixed(2);
                }
                else {
                    $('.popup-box').addClass('bottom');
                    $('.popup-box').removeClass('top');
                    top = (parseFloat(sourceTop - contentHeight + 25)).toFixed(2);  //(parseFloat(sourceTop - contentHeight + 3.5)).toFixed(2);        
                }

                //setLeft
                var sourceLeft = source.left; //(parseFloat(source.left / $(window).width()) * 100).toFixed(2);
                var contentWidth = $('.popup-box').outerWidth(); //(parseFloat($('.popup-box').outerWidth() / $(window).width()) * 100).toFixed(2);
                var sourceLength = sourceWidth; // (parseFloat(sourceWidth / $(window).width()) * 100).toFixed(2);
                var sourceEnd = parseFloat(sourceLeft) + parseFloat(sourceLength);
                var balanceWindowWidth = $(window).width() - sourceEnd; //(parseFloat(100 - sourceEnd)).toFixed(2);
                if (parseFloat(contentWidth) < parseFloat(balanceWindowWidth)) {//a l
                    left = sourceEnd + 10;//sourceEnd + 1;
                    $('.popup-box').removeClass('right');
                    $('.popup-box').addClass('left');
                }
                else {// a r
                    left = sourceLeft - contentWidth - 39; //sourceLeft - contentWidth - 3;
                    $('.popup-box').removeClass('left');
                    $('.popup-box').addClass('right');
                }   
                pos[0] = top;
                pos[1] = left;
                $('.popup-box').css({ 'top': pos[0] + 'px', 'left': pos[1] + 'px' }); //$('.popup-box').css({ 'top': pos[0] + '%', 'left': pos[1] + '%' });
                $('.popup-body').scrollTop(0);
            }
            return false
        }
        function exportToCSV(replaceKeyvalue1)
        {
        	var rpKeyvalue1;
        	if(replaceKeyvalue1)
        		rpKeyvalue1 = $('#MainContent_MainContent_hfKeyValue1')[0]? $('#MainContent_MainContent_hfKeyValue1').val(): 'ParcelId';
            JSONToCSVConvertor( dataReoprt, true, rpKeyvalue1 );
           return false
        }

        function JSONToCSVConvertor( JSONData, ShowLabel, rpKeyvalue1 )
        {
            var arrData = typeof JSONData != 'object' ? JSON.parse( JSONData ) : JSONData;
            var CSV = '';
            //CSV += ReportTitle + '\r\n\n';
            if ( ShowLabel )
            {
                var row = "";
                if (rpKeyvalue1)
                	row += rpKeyvalue1;
                else {
	                for ( var index in arrData[0] ) {
	                    row += index + ',';
	                }
	                row = row.slice( 0, -1 );
                }
                CSV += row + '\r\n';
            }

            for ( var i = 0; i < arrData.length; i++ )
            {
                var row = "";
                for ( var index in arrData[i] ){
                    row += '' + arrData[i][index] + ',';
                }

                row.slice( 0, row.length - 1 );
                CSV += row + '\r\n';
            }

            if ( CSV == '' )
            {
                alert( "Invalid data" );
                return;
            }
            var fileName = "ParcelList";
            //fileName += ReportTitle.replace( / /g, "_" );
            var uri = 'data:text/csv;charset=utf-8,' + escape( CSV );
            var link = document.createElement( "a" );
            link.href = uri;
            link.style = "visibility:hidden";
            link.download = fileName + ".csv";
            document.body.appendChild( link );
            document.body.appendChild( link );
            link.click();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1 style="margin-bottom: 5px;">Data Counts - Schema Tables</h1>
    <asp:HiddenField ID="hfKeyValue1" runat="server"  />
    <table style="width: 100%">
        <tr>
            <td style="width: 60%; min-width: 600px;">
                <h2>Parcel Tables</h2>
                <div style="width: 560px;">
                    <div style="margin: 3px 0px; border: 1px solid #b2b2b2; padding: 3px; width: 410px; background: #EFEFEF; float: left">
                        Search Parcel:
                    <asp:TextBox runat="server" MaxLength="30" ID="txtKeyValue1"  CssClass="txtKeyValue1" Width="150px" />
                        <asp:RequiredFieldValidator runat="server" ID="rfv" ErrorMessage="*" ControlToValidate="txtKeyValue1" ValidationGroup="kv" />
                        <asp:Button runat="server" ID="btnSearch" Text="Search" ValidationGroup="kv" />
                        <asp:Button runat="server" ID="btnClear" Text="Clear" />
                    </div>
                    <div class="ParcelList" style="margin: 3px 0px; border: 1px solid #b2b2b2; visibility:hidden; padding: 3px; width: 120px; background: #EFEFEF; float: right">
                        <asp:Button ID="btnParcelList" CssClass="btnParcelList" runat="server" OnClientClick="javascript:return getParcelList();" Font-Bold="true" Width="120px" Text="Parcel List" />
                    </div>
                </div>
                <asp:Panel runat="server" ID="pnlParcel" style="font-size:1.1em;font-weight:bold;margin:3px 0px;">Parcel: <asp:Label runat="server" ID="lblParcel" /></asp:Panel>
                <asp:TreeView runat="server" ID="tvTables" ShowExpandCollapse="false" ShowLines="true" ShowCheckBoxes="All" Font-Size="0.9em">
                    <NodeStyle CssClass="schema-node" Height="20px" ForeColor="Black" />
                </asp:TreeView>
            </td>
            <td style="width: 40%; min-width: 400px;" runat="server" id="tdOtherTables">
                <h2>Group & Lookup Tables</h2>
                <asp:TreeView runat="server" ID="tvOtherTables" ShowExpandCollapse="false" ShowLines="true" Font-Size="0.9em">
                    <NodeStyle CssClass="schema-node" Height="20px" ForeColor="Black" />
                </asp:TreeView>

            </td>
        </tr>

    </table>

    <div  class="popup-box">
        <div class="popup-header">
            <h4>
                <a class="popup-close" href="#" title="Close">&#10006;</a>
                <asp:ImageButton ID="ibtnExportToCSV" CssClass="ibtnExportToCSV"  ImageUrl="~/App_Static/images/csv.png" Width="20px" Height="20px" style="margin-top:2px; float:right"  ToolTip="Export To CSV"  OnClientClick="javascript:return exportToCSV(true);" runat="server" />
            </h4>
        </div>
        <div class="popup-body">
        </div>
    </div>
</asp:Content>
