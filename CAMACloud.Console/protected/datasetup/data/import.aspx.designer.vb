﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class datasetup_data_import

    '''<summary>
    '''UpdatePanel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanel1 As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''lblStatus control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblStatus As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''blErrors control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents blErrors As Global.System.Web.UI.WebControls.BulletedList

    '''<summary>
    '''blWarnings control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents blWarnings As Global.System.Web.UI.WebControls.BulletedList

    '''<summary>
    '''chkImportPrimary control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkImportPrimary As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''chkImportParcelData control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkImportParcelData As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''chkImportAuxiliaryData control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkImportAuxiliaryData As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''chkNeighborhood control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkNeighborhood As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''chkImportPendingOnly control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkImportPendingOnly As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''chkLookUpTable control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkLookUpTable As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''btnImport control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnImport As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''btnImportWithWarnings control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnImportWithWarnings As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''btnChangeOptions control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnChangeOptions As Global.System.Web.UI.WebControls.LinkButton
End Class
