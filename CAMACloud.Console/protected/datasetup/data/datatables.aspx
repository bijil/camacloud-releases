﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/DataSetup.master" AutoEventWireup="false" Inherits="CAMACloud.Console.datasetup_data_datatables" Codebehind="datatables.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
	<script type="text/javascript">
		$(setSelectColors);
	</script>	
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<h1>
		Parcel Data Tables</h1>
	<p class="info">
		The key fields of Parcel Data Tables are automatically set when the Common Key Fields are set on Import Settings. Please make corrections if necessary. However, the key fields will be reset if Import Settings are changed again.</p>
	<asp:GridView runat="server" ID="gvAux">
		<RowStyle CssClass="dst-row" />
		<Columns>
			<asp:BoundField HeaderText="Table Name" DataField="Name" ItemStyle-Width="200px" />
			<asp:TemplateField HeaderText="Key Field 1">
				<ItemStyle Width="120px" />
				<ItemTemplate>
					<asp:HiddenField runat="server" ID="TID" Value='<%# Eval("Id") %>' ClientIDMode="Static" />
					<asp:DropDownList runat="server" ID="ddlCF1" onchange="setTableField(this, 'KeyField1')" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Key Field 2">
				<ItemStyle Width="120px" />
				<ItemTemplate>
					<asp:DropDownList runat="server" ID="ddlCF2" onchange="setTableField(this, 'KeyField2')" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Key Field 3">
				<ItemStyle Width="120px" />
				<ItemTemplate>
					<asp:DropDownList runat="server" ID="ddlCF3" onchange="setTableField(this, 'KeyField3')" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
		<EmptyDataTemplate>
			<div>No tables available</div>
		</EmptyDataTemplate>
	</asp:GridView>
</asp:Content>

