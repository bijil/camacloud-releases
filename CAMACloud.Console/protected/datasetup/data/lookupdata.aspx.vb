﻿Public Class lookupdata
    Inherits System.Web.UI.Page
    Public o As DataRow
    Public _sketchConfigName As String



    Sub LoadAndShowGrid()
        pnlFormView.Visible = False
        pnlListView.Visible = True
        gvLookups.DataSource = CAMACloud.BusinessLogic.ParcelDataLookup.GetLookupTableList
        gvLookups.DataBind()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadAndShowGrid()
        End If
    End Sub

    Private Sub lbAddLookup_Click(sender As Object, e As System.EventArgs) Handles lbAddLookup.Click
        txtLookupName.Text = ""
        pnlFormView.Visible = True
        pnlListView.Visible = False
        RefreshImportForm()
    End Sub

    Sub RefreshImportForm()
        If rblImportType.SelectedValue = "xml" Then
            pnlImportXML.Visible = True
            pnlImportCSV.Visible = False
            pnlImportSktXML.Visible = False
            apexAreaOptions.Visible = False
        Else If rblImportType.SelectedValue = "csv" Then
        	pnlImportXML.Visible = False
			pnlImportSktXML.Visible = False        	
			pnlImportCSV.Visible = True
			apexAreaOptions.Visible = False
        Else
        	pnlImportXML.Visible = False
        	pnlImportCSV.Visible = False
        	pnlImportSktXML.Visible = True
        	apexAreaOptions.Visible = True
            _sketchConfigName = Database.Tenant.GetStringValue("select value from clientsettings where name='SketchConfig'")

            RunScript("updateApexLookupName();")
        End If
    End Sub

    Private Sub rblImportType_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles rblImportType.SelectedIndexChanged
        RefreshImportForm()
        txtLookupName.Text = ""
    End Sub

    Protected Sub gvLookups_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvLookups.PageIndexChanging
        gvLookups.PageIndex = e.NewPageIndex
        LoadAndShowGrid()
    End Sub


    Private Sub lbReturnToList_Click(sender As Object, e As System.EventArgs) Handles lbReturnToList.Click
        LoadAndShowGrid()
    End Sub

    Private Sub gvLookups_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvLookups.RowCommand
    	
    	Dim exportfilename As String
    	exportfilename=e.CommandArgument
    	
    	If e.CommandName = "Exportxml" Then
            Response.Clear()
            o = Database.System.GetTopRow("SELECT * FROM Organization WHERE Id = " & HttpContext.Current.GetCAMASession().OrganizationId)
            Dim orgName As String = o.GetString("Name")
            Dim exportName As String = orgName.ToLower() + "-lookup-" + e.CommandArgument + "-" + Now.ToString("yyMM-dd-HHmmss") + ".xml"
            Response.ContentType = "application/xml"
            Response.WriteAsAttachment(CAMACloud.BusinessLogic.ParcelDataLookup.ExportLookup(e.CommandArgument), "" & exportName & "")
        End If
        
        If e.CommandName = "Exportcsv" Then
        	
        	Dim searchvalue As String
        	searchvalue=e.CommandArgument
        	If searchvalue.IndexOf("'")>=0 Then
        		searchvalue=searchvalue.Replace("'","''")
        	End If
        	
	        Dim sqlquery As String
	        sqlquery = "SELECT IdValue as Id,NameValue as Name,DescValue as Description,Ordinal,ISNULL(Value,0) as NumericValue,colorcode,AdditionalValue1,AdditionalValue2 FROM ParcelDataLookup WHERE LookupName = '" + searchvalue +"'"
	        
	     
	        
	        Dim dt As DataTable = Database.Tenant.GetDataTable(sqlquery)
	
	        Dim csv As String = String.Empty
	        For Each column As DataColumn In dt.Columns
	            csv += column.ColumnName + ","c
	        Next
	        csv = csv.Trim().Remove(csv.LastIndexOf(","))
	        csv += vbCr & vbLf
	
	        For Each row As DataRow In dt.Rows
	            For Each column As DataColumn In dt.Columns
	                csv += row(column.ColumnName).ToString().Replace(",", ";") + ","c
	            Next
	            csv = csv.Trim().Remove(csv.LastIndexOf(","))
	            csv += vbCr & vbLf
	        Next
	
	        Response.Clear()
	        Response.Buffer = True
	        Response.AddHeader("content-disposition", "attachment;filename="+exportfilename+".csv")
	        Response.Charset = ""
	        Response.ContentType = "application/text"
	        Response.Output.Write(csv)
	        Response.Flush()
	        Response.End()
        End If
        
        If e.CommandName = "DeleteItem" Then
            CAMACloud.BusinessLogic.ParcelDataLookup.DeleteLookupTable(e.CommandArgument)
            LoadAndShowGrid()
        End If
    End Sub
	Private Sub btnUpload_Click(sender As Object, e As System.EventArgs) Handles btnUpload.Click
		Try		
			Dim fileExtention = System.IO.Path.GetExtension(fuImportFile.PostedFile.FileName)
			Dim count As Integer = 0
			Dim Description As String
	        If rblImportType.SelectedValue = "xml" And fileExtention = ".xml" Then
	        	count = CAMACloud.BusinessLogic.ParcelDataLookup.ImportLookupFromXml(fuImportFile.FileContent, chkTrim.Checked)  
	        	NotificationMailer.SendNotificationToVendorCDS(Membership.GetUser().ToString, IIf(rblImportType.SelectedValue = "xml", 1, 3), "Manage Lookup Table Data")
	        	txtLookupName.Text = ""
	        	Description = "Lookup data updated via XML."
	        	SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
	        	Alert(count & " lookup items imported successfully.")
	        Else If rblImportType.SelectedValue = "csv" And fileExtention =".csv" Then
	        	Dim textVal As String = txtLookupName.Text
	        	txtLookupName.Text	= ""
	        	count = CAMACloud.BusinessLogic.ParcelDataLookup.ImportLookupFromCSV(fuImportFile.FileContent, textVal,chkFirstLineHeaders.Checked, chkTrim.Checked)    
	        	NotificationMailer.SendNotificationToVendorCDS(Membership.GetUser().ToString, IIf(rblImportType.SelectedValue = "xml", 1, 3), "Manage Lookup Table Data")
	        	Description = "Lookup data updated via CSV."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
                Alert(count & " lookup items imported successfully.")
	        Else If rblImportType.SelectedValue = "skt_xml" And ( fileExtention = ".xml" Or fileExtention = ".XML" ) Then
	        	count = CAMACloud.BusinessLogic.ParcelDataLookup.ImportLookupFromSketchCodesXml(fuImportFile.FileContent, chkTrim.Checked, txtLookupsktXML.Text, apexAreaOptions.SelectedValue)  
	        	NotificationMailer.SendNotificationToVendorCDS(Membership.GetUser().ToString, 1, "Manage Lookup Table Data")
	        	'txtLookupName.Text = ""
	        	Description = "Lookup data updated via Sketch Codes XML."
	        	SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
	        	Alert(count & " lookup items imported successfully.")
	        	RefreshImportForm()
	        Else If fileExtention ="" Then
	        	Alert("Please choose a file!")
	        	If rblImportType.SelectedValue = "skt_xml" Then
	        		RefreshImportForm()
	        	 Else If rblImportType.SelectedValue = "csv" Then
	        		txtLookupName.Text	= ""
	        	End If
	        Else
	        	Alert("Invalide file type! Please choose a supporting file")
	        	If rblImportType.SelectedValue = "skt_xml" Then
	        		RefreshImportForm()
	        	Else If rblImportType.SelectedValue = "csv" Then
	        		txtLookupName.Text	= ""
	        	End If
	        End If
        Catch ex As Exception
            Alert(ex.Message)
        End Try
    End Sub

    Private Sub lbExportAll_Click(sender As Object, e As EventArgs) Handles lbExportAll.Click
        Response.Clear()
        o = Database.System.GetTopRow("SELECT * FROM Organization WHERE Id = " & HttpContext.Current.GetCAMASession().OrganizationId)
        Dim orgName As String = o.GetString("Name")
        Dim exportName As String = orgName.ToLower().Replace(" ", "_") + "-lookup-all-" + Now.ToString("yyMM-dd-HHmmss") + ".xml"
        Response.ContentType = "application/xml"
        Response.WriteAsAttachment(CAMACloud.BusinessLogic.ParcelDataLookup.ExportLookupData(Database.Tenant), "" & exportName & "")

    End Sub
    
    Private Sub lbExportAllcsv_Click(sender As Object, e As EventArgs) Handles lbExportAllcsv.Click
        Response.Clear()
        o = Database.System.GetTopRow("SELECT * FROM Organization WHERE Id = " & HttpContext.Current.GetCAMASession().OrganizationId)
        Dim orgName As String = o.GetString("Name")
        Dim exportName As String = orgName.ToLower().Replace(" ", "_") + "-lookup-all-" + Now.ToString("yyMM-dd-HHmmss") + ".csv"
        Response.ContentType = "application/csv"
        Response.WriteAsAttachment(CAMACloud.BusinessLogic.ParcelDataLookup.ExportLookupData(Database.Tenant), "" & exportName & "")
    End Sub
    
End Class