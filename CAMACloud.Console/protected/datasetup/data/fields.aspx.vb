﻿Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Partial Class datasetup_data_fields
    Inherits System.Web.UI.Page

    Public Function HasModule(role As String) As Boolean
        Return CAMACloud.ClientOrganization.HasModule(HttpContext.Current.GetCAMASession.OrganizationId, role)
    End Function

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
    	If Not IsPostBack Then
    		SetMaxlen()
            Database.Tenant.Execute("UPDATE DataSourceField SET IsRequiredDTROnly = 0 WHERE IsRequiredDTROnly IS NULL")
            Database.Tenant.Execute("UPDATE DataSourceField SET DoNotShowOnDTR = 0 WHERE DoNotShowOnDTR IS NULL")
            ddlCategory.FillFromSql("SELECT Id, Name + ISNULL(' - ' + SourceTableName,'') FROM FieldCategory ORDER BY Ordinal")
            ddlCategory.InsertItem("[Quick Review Tab]", "-1", 0)
            ddlCategory.AddItem("[Photo Properties Fields]", "-2")
            loginId.Value = Membership.GetUser.ToString()
            LoadFields()
        End If
    End Sub
  	Private Sub SetMaxlen()
    	Try
	    	txtFieldCalcExp.Attributes.Add("maxlength", "500")
	    	txtfieldoverridecalc.Attributes.Add("maxlength", "255")	
	    	txtrequiredSum.Attributes.Add("maxlength", "500")	
	    	txtDefaultValue.Attributes.Add("maxlength", "500")	
		Catch ex As Exception
    	End Try
    End Sub
    Sub LoadFields()
        If ddlCategory.Items.Count = 0 Then
            Return
        End If
        If ddlCategory.SelectedIndex = 0 Then
            rpFields.DataSource = Database.Tenant.GetDataTable("SELECT * FROM DataSourceField WHERE IsFavorite = 1 AND ISNULL(ExclusiveForSS,0)<>1 ORDER BY FavoriteOrdinal".FormatString(ddlCategory.SelectedValue))
            rpFields.DataBind()
        ElseIf ddlCategory.SelectedValue = "-2" Then
            rpFields.DataSource = Database.Tenant.GetDataTable("SELECT * FROM DataSourceField WHERE SourceTable = '_photo_meta_data' ORDER BY AssignedName".FormatString(ddlCategory.SelectedValue))
            rpFields.DataBind()
        Else
            rpFields.DataSource = Database.Tenant.GetDataTable("SELECT * FROM DataSourceField WHERE CategoryId = {0} AND ISNULL(ExclusiveForSS,0)<>1 ORDER BY CategoryOrdinal".FormatString(ddlCategory.SelectedValue))
            rpFields.DataBind()
        End If

        If ddlCategory.SelectedValue = -1 Or ddlCategory.SelectedValue = -2 Then
            custom_panel.Visible = False
        Else
            custom_panel.Visible = True
        End If
        RunScript("hideMask()")
        
    End Sub
    Sub UpdateFields()
    	Database.Tenant.Execute("if exists(SELECT * FROM ClientSettings WHERE Name = 'LastSchemaUpdatedTime') UPDATE ClientSettings SET Value = CONVERT(NVARCHAR(24),GETUTCDATE(),121) WHERE Name = 'LastSchemaUpdatedTime' else INSERT INTO ClientSettings(name,Value) Values('LastSchemaUpdatedTime',CONVERT(NVARCHAR(24),GETUTCDATE(),121))")
    	Database.Tenant.Execute("if exists(SELECT * FROM ClientSettings WHERE Name = 'LastSchemaUpdatedCounter') UPDATE ClientSettings SET Value = Value + 1 WHERE Name = 'LastSchemaUpdatedCounter' else INSERT INTO ClientSettings(name,Value) Values('LastSchemaUpdatedCounter','1')")
    End Sub

    Protected Sub ddlCategory_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlCategory.SelectedIndexChanged
        LoadFields()
    End Sub

    Dim dtInputTypes, dtLookupTables As DataTable

    Private Sub rpFields_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rpFields.ItemCommand
        Select Case e.CommandName
        	Case "EditProperties"
                hcId.Value = e.CommandArgument.ToString
                Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM DataSourceField WHERE Id = " & e.CommandArgument)
                hcType.Value = dr.GetInteger("DataType")
                hReadOnly.Value = dr.GetBoolean("IsReadOnly").ToString.ToLower
                txtFieldCalcExp.Text = dr.GetString("CalculationExpression")
                txtMaxLength.Text = dr.GetInteger("MaxLength")
                txtReadOnlyExp.Text = dr.GetString("ReadOnlyExpression")
                txtfieldoverridecalc.Text = dr.GetString("CalculationOverrideExpression")
                txtVisibilityExp.Text = dr.GetString("VisibilityExpression")
                txtrequiredExp.Text = dr.GetString("RequiredExpression")
                cbl_autoselectfirst.Checked = dr.GetBoolean("AutoSelectFirstitem")
                cbl_autonumber.Checked = dr.GetBoolean("autoNumber")
                txtCalcExp.Enabled = True
                txtNote.Text = dr.GetString("CC_Note")
                txtPrecision.Text = dr.GetString("NumericScale")
             	txtPrecision1.Text = dr.GetString("NumericPrecision")
             	isRequiredIfEdited.Checked = dr.GetBoolean("RequiredIfRecordEdited")
             	txtrequiredSum.Text = Database.Tenant.GetStringValue ("SELECT Value FROM DataSourceFieldProperties WHERE PropertyName= 'RequiredSum' AND FieldId =" & e.CommandArgument)
                'txtVisibilityExp.Enabled = True
                'If (txtReadOnlyExp.Text = "" AndAlso txtVisibilityExp.Text <> "") Then
                '    txtReadOnlyExp.Enabled = False
                'End If

                'If (txtVisibilityExp.Text = "" AndAlso txtReadOnlyExp.Text <> "") Then
                '    txtVisibilityExp.Enabled = False
                'End If

                hIsCustomLookup.Value = "0"

                If (dr.GetInteger("DataType") = 5 Or dr.GetInteger("DataType") = 11) AndAlso dr.GetString("LookupTable") <> "$QUERY$" Then
                    ddlDefaultValue.Visible = True
                    txtDefaultValue.Visible = False

                    ddlDefaultValue.FillFromSql("SELECT IdValue, NameValue FROM ParcelDataLookup WHERE LookupName = " + dr.GetString("LookupTable").ToSqlValue, True, , dr.GetString("Defaultvalue"))

                    txtDefaultValue.Text = ""
                ElseIf dr.GetInteger("DataType") = 3 Then
                    ddlDefaultValue.Visible = True
                    txtDefaultValue.Visible = False
                    ddlDefaultValue.FillFromSql("SELECT 'false' As Id, 'No' As Name UNION SELECT 'true' As Id, 'Yes' As Name", True, , dr.GetString("Defaultvalue"))

                    txtDefaultValue.Text = ""
                Else
                    ddlDefaultValue.Visible = False
                    txtDefaultValue.Visible = True
                    txtDefaultValue.Text = dr.GetString("Defaultvalue")
                End If

                If dr.GetInteger("DataType") = 5 AndAlso dr.GetString("LookupTable") = "$QUERY$" Then
                    hIsCustomLookup.Value = "1"
                    cbLargeLookup.Visible = True
                Else
                    cbLargeLookup.Visible = False
                End If
                If dr.GetInteger("DataType") = 2 Or dr.GetInteger("DataType") = 7 Or dr.GetInteger("DataType") = 8 Or dr.GetInteger("DataType") = 9 Then
                	spnPrecision.Attributes.Add("style","display:inherit")
                	spnPrecision.Attributes.Add("datatype","2")
                	txtPrecision1.Attributes.Add("readonly","readonly")
                	txtPrecision1.Attributes.Add("disabled","disabled")
                	txtPrecision.Attributes.Add("readonly","readonly")
                	txtPrecision.Attributes.Add("disabled","disabled")
                Else
                	spnPrecision.Attributes.Add("style","display:none")
                	spnPrecision.Attributes.Remove("datatype")
                End If

                If dr.GetInteger("DataType") = 8 Then
                    cbl_autonumber.Visible = True
                Else
                    cbl_autonumber.Visible = False
                End If

                If dr.GetInteger("DataType") = 5 Or dr.GetInteger("DataType") = 3 Or dr.GetInteger("DataType") = 11 Then
                    cbl_autoselectfirst.Visible = True
                Else
                    cbl_autoselectfirst.Visible = False
                End If

                If dr.GetInteger("DataType") = 5 Or dr.GetInteger("DataType") = 11 Or dr.GetInteger("DataType") = 3 Or dr.GetInteger("DataType") = 4 Then
                    trCalcExp.Visible = False
                    trCalcoverrideExp.Visible = False
                Else
                    trCalcExp.Visible = True
                    trCalcoverrideExp.Visible = True
                End If

                If dr.GetInteger("DataType") = 5 And dr.GetString("LookupTable") = "$QUERY$" Then
                    txtLookupQuery.Text = dr.GetString("LookupQuery")
                    trLookupQuery.Visible = True
                Else
                    trLookupQuery.Visible = False
                End If

                If dr.GetInteger("DataType") = 1 Then
                    DoNotAllowSpecialCharacters.Checked = dr.GetBoolean("DoNotAllowSpecialCharacters")
                    DoNotAllowSpecialCharacters.Visible = True
                Else
                    DoNotAllowSpecialCharacters.Checked = False
                    DoNotAllowSpecialCharacters.Visible = False
                End If

                Dim isClsCalcAttrvis = Database.Tenant.GetStringValue("select ClassCalculatorParameters from FieldCategory where ClassCalculatorParameters is not null and Id = " & ddlCategory.SelectedValue)
                'Dim isClsCalcAttrvis As String
                If (isClsCalcAttrvis <> "") Then
                    isClsCalcAttr.Checked = dr.GetBoolean("IsClassCalculatorAttribute")
                    isClsCalcAttr.Visible = True
                Else
                    isClsCalcAttr.Visible = False
                End If
                chkUnique.Checked = dr.GetBoolean("IsUniqueInSiblings")
                chkMassUpdate.Checked = dr.GetBoolean("IsMassUpdateField")
                cbLargeLookup.Checked = dr.GetBoolean("IsLargeLookup")
                isCustomInputType.Checked = dr.GetBoolean("CustomInputType")
                DoNotAllowSpecialCharacters.Checked = dr.GetBoolean("DoNotAllowSpecialCharacters")
                txtPrecision1.Text = dr.GetString("NumericPrecision")
                txtPrecision.Text = dr.GetString("NumericScale")
                RunScript("fieldPropMaskHandler(true);")
        End Select
    End Sub
    
    Protected Sub rpFields_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpFields.ItemDataBound
        Dim dr As DataRowView = e.Item.DataItem
        If dtInputTypes Is Nothing Then
            dtInputTypes = Database.System.GetDataTable("SELECT * FROM GlobalFieldInputTypes")
        End If
        Using ddl As DropDownList = e.Item.FindControl("ddlInputTypes")
            ddl.FillFromTable(dtInputTypes)
            ddl.SelectedValue = dr("DataType")     
        End Using

        If dtLookupTables Is Nothing Then
            dtLookupTables = Database.Tenant.GetDataTable("SELECT * FROM cc_LookupSources")
        End If
        Using ddl As DropDownList = e.Item.FindControl("ddlLookupSource")
            ddl.FillFromTable(dtLookupTables, True)
            ddl.AddItem("[Custom Query]", "$QUERY$")
            ddl.SelectedValue = dr.Row.GetString("LookupTable")
            If dr("DataType") <> 5 Then
                ddl.CssClass = "lookup-hide"
            End If
        End Using

    End Sub

    Protected Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        If IsPostBack Then
        	LoadFields()
        	 If isCustomInputType.Checked Then
                	spnPrecision.Attributes.Add("style","display:inherit")
                	txtPrecision1.Attributes.Remove("disabled")
		    		txtPrecision1.Attributes.Remove("readonly") 
		    		txtPrecision.Attributes.Remove("disabled")
		    		txtPrecision.Attributes.Remove("readonly")
                End If
                RunScript("hookFields();")
                RunScript("setDialogueTop();") 
            If Not HasModule("DTR") Then
                RunScript("hideRoleProperties('dtr');")
            End If
            If Not HasModule("MobileAssessor") Then
                RunScript("hideRoleProperties('ma');")
            End If
        End If
    End Sub

    Protected Sub lblSwitchView_Click(sender As Object, e As System.EventArgs) Handles lblSwitchView.Click
        If hdnView.Value = "0" Then
            hdnView.Value = "1"
            lblSwitchView.Text = "Switch to simple view"
        Else
            hdnView.Value = "0"
            lblSwitchView.Text = "Switch to detailed view"
        End If
        LoadFields()
    End Sub


    Public Function ShortenedText(fieldName As String, Optional maxLength As Integer = 8)
        If Eval(fieldName) Is DBNull.Value Then
            Return ""
        End If
        Dim text As String = Eval(fieldName)
        Dim l As Integer = text.Length
        If l < maxLength + 3 Then
            Return text
        Else
            Dim p2 As Integer = 3
            Dim p1 As Integer = maxLength - p2
            Return text.Substring(0, p1) + "..." + text.Substring(l - p2, p2)
        End If
    End Function

    Private Sub btnAddCustomField_Click(sender As Object, e As System.EventArgs) Handles btnAddCustomField.Click
    	Try
    		Dim cattext As String=ddlCategory.SelectedItem.ToString
            Dim sourceTable As String = ""
            txtFieldName.Text = txtFieldName.Text.Replace(" ", "").Replace("+", "").Replace("-", "").Replace("*", "").Replace("/", "").Replace("'", "")
            cattext=cattext.Replace("'", "''")
            Dim pattern = "^[a-zA-Z_][a-zA-Z0-9_]*$"
            Dim FieldNameCheck = Regex.Matches(txtFieldName.Text,pattern)
            ViewState("name")=txtFieldName.Text
            If txtCalcExp.Text.Length > 500 Then
            	Alert("Maximum length of Calculation Expression exceeded. Please change appropriately.")
            	Return
            End If
            'Dim Description As String = "New Custom Field "+txtFieldName.Text+" created under "+cattext+" category"
            ''SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
            If FieldNameCheck.Count = 0 Then
            	Alert("Field name is not supported. Please change appropriately.")
            	Return
            End If
            
            CAMACloud.BusinessLogic.FieldCategory.AddCustomField(txtFieldName.Text, IIf(txtCalcExp.Text = "", "", txtCalcExp.Text), ddlCategory.SelectedValue, sourceTable)
            Alert("New custom field added")
            LoadFields()
            Dim Description As String = "New Custom Field "+txtFieldName.Text+" created under "+cattext+" category"
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
            txtCalcExp.Text = ""
            txtFieldName.Text = ""  
        Catch ex As Exception
            Alert(ex.Message)
        End Try

    End Sub

    Public Function EditPropertiesScript()
        Return "editProperties(" & Eval("Id") & ", '" + Eval("Name") + "','" + Eval("DisplayLabel") + "');"
    End Function

    Private Sub btnSaveChanges_Click(sender As Object, e As System.EventArgs) Handles btnSaveChanges.Click

        If chkUnique.Checked Then
            If txtDefaultValue.Text.Trim <> "" Or ddlDefaultValue.SelectedIndex > 0 Then
            	Alert("You cannot set a default value for the field if this is marked as a unique value field.")
            	 RunScript("CloseDialog()")
                Return
            End If
            If chkMassUpdate.Checked Then
            	Alert("You are not allowed to select a unique value field for Mass Update options.")
            	 RunScript("CloseDialog()")
                Return
            End If
        End If
        If isCustomInputType.Checked Then
        	If txtPrecision.Text.Contains(".") OR txtPrecision1.Text.Contains(".") Then
	        		Alert("You cannot set Decimal values for Scale and Precision.")
	        		 RunScript("CloseDialog()")
	        		Return
	        End If
        	If txtPrecision.Text <>"" And txtPrecision1.Text <> "" Then
	        	If Integer.Parse( txtPrecision.Text) >Integer.Parse(txtPrecision1.Text) Then
	        		Alert("Scale should be less than or equal to precision.")
	        		 RunScript("CloseDialog()")
	        		Return
	        	End If
        	End If
        	If txtPrecision.Text <>"" And txtPrecision1.Text = "" Then
        		Alert("You cannot set Scale for the field without specifying the Precision.")
	        		 RunScript("CloseDialog()")
	        		Return
        	End If
        End If

        Dim defaultValue As String = txtDefaultValue.Text
        Dim isReadOnly As Boolean = Boolean.Parse(hReadOnly.Value)
        Dim isCalculated As Boolean = False
        If hcType.Value = "3" Or hcType.Value = "5" Or hcType.Value = "11" Then
            defaultValue = ddlDefaultValue.SelectedValue
        End If
        If hIsCustomLookup.Value = "1" Then
            defaultValue = txtDefaultValue.Text
        End If

		If txtFieldCalcExp.Text.Trim() <> "" Then
			If txtfieldoverridecalc.Text.Trim() = "" Then
				isReadOnly = True
			Else
				isReadOnly = False
			End If
            isCalculated = True
        Else
            'If CInt(ddlCategory.SelectedValue) >= 0 Then
            '    Dim tableId = ""
            '    Dim sourceTable = ""
            '    Dim fieldName = Database.Tenant.GetStringValue("SELECT NAME FROM DataSourceField WHERE Id={0}".SqlFormatString(hcId.Value))
            '    CAMACloud.BusinessLogic.FieldCategory.CreateCustomField(ddlCategory.SelectedValue, fieldName, tableId, sourceTable)
            '    If sourceTable <> "" AndAlso tableId <> "" Then
            '        Database.Tenant.Execute("UPDATE DataSourceField SET SourceTable={0},TableId={1} WHERE Id={2}".SqlFormatString(sourceTable, tableId, hcId.Value))
            '    End If
            'End If
        End If

        Dim readonlyExp = ""
        Dim visibilityExp = ""
        Dim requiredExp = ""
        Dim requiredSumExp=""
        Dim Precision As Integer
        Dim sql As String = ""
        If txtReadOnlyExp.Text <> "" Then
            readonlyExp = txtReadOnlyExp.Text
        End If
        If txtrequiredExp.Text <> "" Then
            requiredExp = txtrequiredExp.Text
        End If
        If txtVisibilityExp.Text <> "" Then
            visibilityExp = txtVisibilityExp.Text
        End If
        If txtrequiredSum .Text <> "" Then
           requiredSumExp = txtrequiredSum.Text
        End If
        If isCustomInputType.Checked Then
        	If txtPrecision1.Text <> "" And txtPrecision.Text <> "" Then
                sql = "UPDATE DataSourceField SET CalculationExpression={1},MaxLength={2}, DefaultValue = {3},IsReadOnly={4},IsCalculated={5},ReadOnlyExpression={6},VisibilityExpression={7}, IsUniqueInSiblings = {8}, IsMassUpdateField = {9}, LookupQuery={10},RequiredExpression={11},AutoSelectFirstitem={12},CalculationOverrideExpression={13},autoNumber={14},CC_Note={15},IsLargeLookup={16},IsClassCalculatorAttribute={17},RequiredIfRecordEdited ={18},CustomInputType={19},NumericPrecision={20},NumericScale={21}, DoNotAllowSpecialCharacters = {22}  where Id ={0}".SqlFormatStringWithNulls(hcId.Value, txtFieldCalcExp.Text, txtMaxLength.Text, defaultValue, isReadOnly, isCalculated, readonlyExp, visibilityExp, chkUnique.Checked.GetHashCode, chkMassUpdate.Checked.GetHashCode, txtLookupQuery.Text, requiredExp, cbl_autoselectfirst.Checked, txtfieldoverridecalc.Text, cbl_autonumber.Checked, txtNote.Text, cbLargeLookup.Checked.GetHashCode, isClsCalcAttr.Checked, isRequiredIfEdited.Checked.GetHashCode, isCustomInputType.Checked.GetHashCode, txtPrecision1.Text, txtPrecision.Text, DoNotAllowSpecialCharacters.Checked.GetHashCode)
            Else
                sql = "UPDATE DataSourceField SET CalculationExpression={1},MaxLength={2}, DefaultValue = {3},IsReadOnly={4},IsCalculated={5},ReadOnlyExpression={6},VisibilityExpression={7}, IsUniqueInSiblings = {8}, IsMassUpdateField = {9}, LookupQuery={10},RequiredExpression={11},AutoSelectFirstitem={12},CalculationOverrideExpression={13},autoNumber={14},CC_Note={15},IsLargeLookup={16},IsClassCalculatorAttribute={17},RequiredIfRecordEdited ={18},CustomInputType={19}, DoNotAllowSpecialCharacters = {20}  where Id ={0}".SqlFormatStringWithNulls(hcId.Value, txtFieldCalcExp.Text, txtMaxLength.Text, defaultValue, isReadOnly, isCalculated, readonlyExp, visibilityExp, chkUnique.Checked.GetHashCode, chkMassUpdate.Checked.GetHashCode, txtLookupQuery.Text, requiredExp, cbl_autoselectfirst.Checked, txtfieldoverridecalc.Text, cbl_autonumber.Checked, txtNote.Text, cbLargeLookup.Checked.GetHashCode, isClsCalcAttr.Checked, isRequiredIfEdited.Checked.GetHashCode, isCustomInputType.Checked.GetHashCode, DoNotAllowSpecialCharacters.Checked.GetHashCode)
            End If
        Else
            sql = "UPDATE DataSourceField SET CalculationExpression={1},MaxLength={2}, DefaultValue = {3},IsReadOnly={4},IsCalculated={5},ReadOnlyExpression={6},VisibilityExpression={7}, IsUniqueInSiblings = {8}, IsMassUpdateField = {9}, LookupQuery={10},RequiredExpression={11},AutoSelectFirstitem={12},CalculationOverrideExpression={13},autoNumber={14},CC_Note={15},IsLargeLookup={16},IsClassCalculatorAttribute={17},RequiredIfRecordEdited ={18},CustomInputType={19}, DoNotAllowSpecialCharacters = {20}  where Id ={0}".SqlFormatStringWithNulls(hcId.Value, txtFieldCalcExp.Text, txtMaxLength.Text, defaultValue, isReadOnly, isCalculated, readonlyExp, visibilityExp, chkUnique.Checked.GetHashCode, chkMassUpdate.Checked.GetHashCode, txtLookupQuery.Text, requiredExp, cbl_autoselectfirst.Checked, txtfieldoverridecalc.Text, cbl_autonumber.Checked, txtNote.Text, cbLargeLookup.Checked.GetHashCode, isClsCalcAttr.Checked, isRequiredIfEdited.Checked.GetHashCode, isCustomInputType.Checked.GetHashCode, DoNotAllowSpecialCharacters.Checked.GetHashCode)

        End If
        Dim query=""
        If requiredSumExp <> "" Then
        	If (Database .Tenant.GetTopRow ("Select * From DataSourceFieldProperties where PropertyName='RequiredSum' and FieldId={0}".SqlFormatString(hcId.Value ,requiredSumExp ))) IsNot Nothing Then
        		query = "UPDATE DataSourceFieldProperties set Value={1} Where FieldId={0} and PropertyName='RequiredSum'".SqlFormatString(hcId.Value ,requiredSumExp )
        	Else
        		query = "INSERT INTO DataSourceFieldProperties Values ({0},{1},{2})".SqlFormatString(hcId.Value,"RequiredSum" ,requiredSumExp ) 
        	End If
        Else
        	query = "Delete from DataSourceFieldProperties where FieldId={0} and PropertyName='RequiredSum'".SqlFormatString(hcId.Value)
        End If
        If (query <> "") Then
        	Database.Tenant.Execute(query)
        End If
        Dim dtField As DataTable = Database.Tenant.GetDataTable("SELECT CalculationExpression,MaxLength,DefaultValue,IsReadOnly,IsCalculated,ReadOnlyExpression,VisibilityExpression, IsUniqueInSiblings, IsMassUpdateField, LookupQuery,RequiredExpression,AutoSelectFirstitem,CalculationOverrideExpression,autoNumber,CC_Note,IsLargeLookup,Precision,IsClassCalculatorAttribute,RequiredIfRecordEdited,NumericPrecision,NumericScale FROM DataSourceField WHERE Id = {0}".SqlFormatStringWithNulls(hcId.Value))
        Dim fieldId As Integer = SqlFormatStringWithNulls(hcId.Value)
        If (Database.Tenant.Execute(sql) > 0) Then
            RunScript("hidePopup();alert('Field Settings Modified!');")
            txtMaxLength.Text = ""
            txtFieldCalcExp.Text = ""
            hcId.Value = ""
            txtDefaultValue.Text = ""
            ddlDefaultValue.SelectedValue = ""
            txtPrecision.Text = ""  
            txtPrecision1.Text = ""  
        End If
        LoadFields()
        UpdateFields()
        dtField.Merge(Database.Tenant.GetDataTable("SELECT CalculationExpression,MaxLength,DefaultValue,IsReadOnly,IsCalculated,ReadOnlyExpression,VisibilityExpression, IsUniqueInSiblings, IsMassUpdateField, LookupQuery,RequiredExpression,AutoSelectFirstitem,CalculationOverrideExpression,autoNumber,CC_Note,IsLargeLookup,Precision,IsClassCalculatorAttribute,RequiredIfRecordEdited,NumericPrecision,NumericScale FROM DataSourceField WHERE Id = {0}".SqlFormatStringWithNulls(fieldId)))
        Dim loginId = Membership.GetUser().ToString()
        Dim o As DataRow = Database.System.GetTopRow("SELECT EnableXMLAuditTrail FROM OrganizationSettings WHERE OrganizationId = " & HttpContext.Current.GetCAMASession().OrganizationId)
        If (o.GetBoolean("EnableXMLAuditTrail")) Then
            SettingsAuditTrail.updateSettingsAuditTrail(dtField, fieldId, loginId)
        End If
        NotificationMailer.SendNotificationToVendorCDS(Membership.GetUser().ToString, 2, "Field Settings")
        RunScript("CloseDialog()")
    End Sub
    
End Class
