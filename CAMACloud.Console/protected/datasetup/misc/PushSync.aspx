﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master" CodeBehind="PushSync.aspx.vb" Inherits="CAMACloud.Console.PushSync" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>
        Push pending changes from MA/Sync</h1>
    <asp:GridView runat="server" ID="pending">
        <Columns>
            <asp:BoundField DataField="LoginId" HeaderText="Login ID" ItemStyle-Width="200px" />
            <asp:TemplateField>
                <ItemStyle Width="100px" />
                <ItemTemplate>
                    <asp:LinkButton runat="server" ID="btnPush" Text="Push Changes" CommandArgument='<%# Eval("LoginId") %>' CommandName="PushItem" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ErrorMessage" HeaderText="Error Message" ItemStyle-Width="600px" />
        </Columns>
        <EmptyDataTemplate>
            No pending changes from users.
        </EmptyDataTemplate>
    </asp:GridView>
    <br />
    <asp:Button ID="btnPendingPhotos" runat="server" Text="Push Pending Photos" />
</asp:Content>
