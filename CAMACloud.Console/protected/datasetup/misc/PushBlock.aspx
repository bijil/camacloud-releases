﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master" CodeBehind="PushBlock.aspx.vb" Inherits="CAMACloud.Console.PushBlock" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Re-queue Mobile Changes for Process</h1>
    <table>
        <tr>
            <td style="width:100px;">Login ID:<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="LoginID" ValidationGroup="X" /></td>
            <td><asp:TextBox runat="server" ID="LoginID" />
                
            </td>
        </tr>
        <tr>
            <td>Mobile Data: <asp:RequiredFieldValidator runat="server" ControlToValidate="txtMobilePCI" ValidationGroup="X" /></td>
            <td>
                <asp:TextBox runat="server" ID="txtMobilePCI" TextMode="MultiLine" Rows="35" Columns="110" />
                
            </td>
        </tr>
        <tr>
            <td></td>
            <td><asp:Button runat="server" ID="btnProcess" Text="Process Data" ValidationGroup="X" /></td>
        </tr>
    </table>
</asp:Content>
