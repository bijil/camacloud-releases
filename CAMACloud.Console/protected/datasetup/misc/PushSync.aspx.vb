﻿

Imports System.IO

Public Class PushSync
    Inherits System.Web.UI.Page

    Sub LoadGrid()
  
        pending.DataSource = d_("EXEC ccad_GetPendingUserChanges")
        pending.DataBind() 
        Dim dr As DataTable = Database.Tenant.GetDataTable("select * from PendingPhotos")
        Dim CountPhoto As String = dr.Rows.Count
        btnPendingPhotos.text ="Push Pending Photos ("+CountPhoto+")"
        
    End Sub

Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

	If Not IsPostBack Then
            LoadGrid()
        End If
    End Sub

    Private Sub pending_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles pending.RowCommand
        If e.CommandName = "PushItem" Then
            CAMACloud.BusinessLogic.MobileSyncUploadProcessor.ProcessParcelChangeBatches(e.CommandArgument, False)
            LoadGrid()
        End If
    End Sub

    Protected Sub btnPendingPhotos_Click(sender As Object, e As EventArgs) Handles btnPendingPhotos.Click
		Dim CountPhoto As String
        Try
        	Dim dr As DataTable = Database.Tenant.GetDataTable("select * from PendingPhotos")
        	Dim rkey As String 
            Dim pkey As String '= Request("keyvalue")
            Dim pid As String 
            Dim type As String 
            Dim dataUrl As String 
            Dim timeStamp As Long 
            Dim UserName As String 
            CountPhoto = dr.Rows.Count
            If(dr.Rows.Count > 0) Then
	            For i As Integer = 0 To dr.Rows.Count - 1
	            	Dim dt = dr.Rows(i)
	            	rkey = dt.GetString("Rowuid")
	            	pid = dt.GetString("parcelid")
	            	type = dt.GetString("Type")
	            	dataUrl = dt.GetString("Photo")
	            	timeStamp =CLng (dt.GetString("timestamp"))
	            	UserName = dt.GetString("LoginId") 
	            	Dim ClientROWUID As String = Nothing
	            	
	
		            'Dim dr1 As DataTable = Database.Tenant.GetDataTable("select * from PendingPhotos")
		           ' Dim CountPhoto As String = dr1.Rows.Count
		            'btnPendingPhotos.text = "Push Pending Photos ("+CountPhoto+")"
		            If String.IsNullOrEmpty(pid) OrElse String.IsNullOrEmpty(dataUrl) OrElse String.IsNullOrEmpty(timeStamp) Then
		                'Try
		                '    Throw New Exception("Invalid photo upload request from MA")
		                'Catch ex As Exception
		                '    ErrorMailer.ReportException("MA-Photo-Sync", ex, Request)
		                'End Try
		                
		                Return
		            End If
		            If IsNumeric(pid) Then
		            	If (pid < 0) Then
		                    pid = Database.Tenant.GetIntegerValue("SELECT TOP 1 CC_ParcelID FROM ParcelData WHERE ClientROWUID={0}".SqlFormatString(pid.ToString()))
		                End If
		            End If
		            
		            If dt.GetString("BppClientROWUID") <> "" Then
		            	ClientROWUID = dt.GetString("BppClientROWUID")
		            End If
		            'Dim timeStamp As Long = Request("timestamp")
		
		            'Dim localId As String = Request("localid")
		            Dim localId As String = If(dt.GetString("LocalId") <> "", dt.GetString("LocalId"), "")
		            Dim existingPId = Database.Tenant.GetIntegerValue("SELECT Id FROM ParcelImages WHERE LocalId = " & localId.ToSqlValue)
		            If (localId <> "" And existingPId > 0) Then	
		            	Database.Tenant.Execute("DELETE  FROM PendingPhotos WHERE Rowuid ="&rkey)	
		            	CountPhoto = CountPhoto -1
		                Continue For
		            End If
		            pkey = Database.Tenant.GetStringValue("SELECT KeyValue1 FROM Parcel WHERE Id = " & pid)	            
		
		            Dim application As Integer = IIf(type = "", 1, type)
		            Dim base64  As String = dataUrl.Substring(dataUrl.IndexOf(";base64,") + 8)
		            Dim imageType As String = dataUrl.Substring(0, dataUrl.IndexOf(";base64,")).Trim(";").Split(":")(1).Split("/")(1)
		            base64 = base64.Replace("_", "/").Replace("-", "+").Replace(" ", "+")
		            If imageType = "jpeg" Then
		                imageType = "jpg"
		            End If
		            Dim fileName As String = pid + Now.ToString("-yyyyMMdd-HHmmssfff") + "." + imageType
		            Dim s3 As New S3FileManager()
		            Dim bytes() As Byte = System.Convert.FromBase64String(base64)
		            Dim ms As New MemoryStream(bytes)
		            Dim s3Path As String = HttpContext.Current.GetCAMASession.TenantKey + "/pc/"
		
		            For Each count In New Integer() {2, 3, 3, 3}
		                If pkey.Length > 0 Then
		                    Dim part As String = pkey.Substring(0, Math.Min(count, pkey.Length))
		                    pkey = pkey.Substring(part.Length)
							If part(part.Length - 1) = "." Or part(part.Length - 1) = " " Or part(part.Length - 1) = "-" Then
								part = part.Remove(part.Length - 1, 1)
							End If
		                    s3Path += part + "/"
		                End If
		            Next
		            s3Path += fileName
		            s3.UploadFile(ms, s3Path)
		            Dim eventDate As Date = #1/1/1970#
		            If timeStamp.ToString.Length > 13 Then
		                timeStamp = Long.Parse(timeStamp.ToString.Substring(0, 13))
		            End If
		            eventDate = eventDate.AddMilliseconds(timeStamp)
					Dim sql = "EXEC cc_Parcel_AddPhoto  {0}, {1}, 0, {2}, {3}, {4}, {5}, {6},@AppType = 'MA'".SqlFormat(True, UserName, pid, s3Path, application, eventDate, localId, ClientROWUID)

					'Dim imageId As Integer = Database.Tenant.GetIntegerValue("INSERT INTO ParcelImages (ParcelId, IsSketch, Storage, Path) VALUES ({0}, 0, 1, {1});SELECT CAST(@@IDENTITY AS INT) As NewId".SqlFormat(True, pid, s3Path))
					Dim imageId As Integer = Database.Tenant.GetIntegerValue(sql)
		
		            'Database.Tenant.Execute("UPDATE ParcelImages SET LocalId={0} WHERE Id={1}".SqlFormatString(localId, imageId))
		            CAMACloud.BusinessLogic.MobileSyncUploadProcessor.insertNewRecordDetails(Database.Tenant, pid, imageId, "Images", localId, eventDate, UserName)
		            UserAuditTrail.CreateEvent(UserName, UserAuditTrail.ApplicationType.MobileAssessor, UserAuditTrail.EventType.UploadPhoto, "Uploaded photo - " + localId + " (" & imageId & ")")
		            Database.Tenant.Execute("DELETE  FROM PendingPhotos WHERE Rowuid ="&rkey)
		            CountPhoto= CountPhoto -1
	            Next
            End If
            btnPendingPhotos.text = "Push Pending Photos ("+CountPhoto+")"
           
        Catch ex As Exception
            'Dim sql As String = "INSERT INTO PendingPhotos(parcelid,[Type],photo,[timestamp],LoginId) VALUES({0},{1},{2},{3},{4})Select @@IDENTITY".SqlFormatString(Request("parcelid"), Request("Type"), Request("photo"), Request("timestamp"), SqlUserName)
            'Dim Rowuid = Database.Tenant.GetIntegerValue(sql)
            btnPendingPhotos.text = "Push Pending Photos ("+CountPhoto+")"
            Alert(ex.Message)
            'Throw New Exception(ex.Message)
        End Try
    End Sub

End Class