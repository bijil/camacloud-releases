﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="CAMACloud.Console.sketchv_map" CodeBehind="map.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="/App_Static/css/Svo/measure.css?t=1" />
    <link rel="stylesheet" type="text/css" href="/App_Static/css/parcelmanager.css?t=1" />
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=geometry&region=US&key=AIzaSyC7e5YrZlKqWtaY4ehsDwMgCis4IfNs7Xw"></script>
    <script type="text/javascript" src="/App_Static/jslib/jquery.js"></script>
    <script type="text/javascript" src="/App_Static/jslib/jquery-ui-1.8.20.custom.min.js"></script>
    <script type="text/javascript" src="/App_Static/jslib/jquery.rotate.js"></script>
    <script src="../../App_Static/js/qc/010-conversions.js"></script>
    <script type="text/javascript" src="/App_Static/jslib/tipr.js?t=0111234598"></script>
    <script type="text/javascript" src="/App_Static/js/sketch/14-ccsketcheditor.js?t=21894"></script>
    <script type="text/javascript" src="/App_Static/jslib/underscore-min.js"></script>
    <script type="text/javascript" src="/App_Static/jslib/base64.js"></script>
    <script type="text/javascript" src="/App_Static/js/Svo/lib2/base64.js"></script>
    <cc:JavaScript ID="JavaScript1" runat="server" IncludeFolder="/App_Static/js/sketch/sketchlib/" />
    <script type="text/javascript" src="/App_Static/js/Svo/sigmasketch.js?t=964569831944299999992"></script>
    <script type="text/javascript" src="/App_Static/js/Svo/mainview.js?t=76269845527786519942">

    </script>
    <script type="text/javascript" src="/App_Static/js/Svo/ref/svutils.js?t=96456983192">
    </script>
    <style type="text/css">
        .overlay {
            width: 150px;
            height: 150px;
            position: relative;
            z-index: 3147483647 !important;
            cursor: pointer;
        }

        .pointer {
            pointer-events: none;
        }

        /*img#sketch {
            border: 1px solid blue;
        }

        div#resizable-wrapper {
            border: 2px dotted fuchsia;
        }*/
    </style>
</head>
<body style="overflow: hidden; background: #777; margin: 0px;">
    <form id="form1" runat="server">
        <div id="map">
        </div>
        <div style="display: none;">
            <canvas height="300" width="300" class="ccse-img"></canvas>
        </div>
    </form>
     <script type="text/javascript">
		var nearmapApiKey = '<%= NearmapAPIKey%>';
		var nearmapAccess = '<%= NearmapAccess%>';
		var nearmapWMSApiKey = '<%= NearmapWMSAPIKey%>';
        var nearmapWMSAccess = '<%= NearmapWMSAccess%>';										  																				 
		var woolpertApikey = '<%= woolpertApikey%>';
		var woolpert6inchId = '<%= woolpert6inchId%>';
		var woolpert3inchId = '<%= woolpert3inchId%>';
		var woolpertAccess = '<%= woolpertAccess%>';
		var customWMS = '<%= customWMS%>';
		var customMapIsImage = '<%= customMapIsImage%>';
		var customWMSAccess = '<%= customWMSAccess%>';
	</script>
</body>
</html>
