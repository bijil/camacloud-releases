﻿Public Class prc
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim template As String = ClientSettings.ContentTemplate("sketch-validation-prc")
            If template <> "" Then
            	prc.InnerHtml = template
            Else
            	template = ClientSettings.ContentTemplate("property-record-card")
            	prc.InnerHtml = template
            End If
        End If
    End Sub

End Class