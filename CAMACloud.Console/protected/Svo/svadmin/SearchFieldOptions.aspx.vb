﻿Public Class SearchFieldOptions
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim UserName = Membership.GetUser().ToString
			Dim dcsUser As Boolean = True
            If UserName.ToLower <> "dcs-qa" And UserName.ToLower <> "dcs-rd" And UserName.ToLower <> "admin" And UserName.ToLower <> "dcs-support" And UserName.ToLower <> "dcs-ps" Then
                dcsUser = False
            End If
            If Roles.IsUserInRole("SVAdmin") Then
			Else
				If dcsUser = False Then
                	Response.Redirect("~/web/403.aspx")
				End If
			End If
            loadSearchOptionFilterFields()
            RunScript("UpdateGridchecked();")
        End If

    End Sub

    Sub loadSearchOptionFilterFields
        Dim keyField As String = DataBase.Tenant.GetStringValue("SELECT KeyField1 FROM Application")
    	Dim Sql As String = "SELECT f.Id,f.Name,f.DisplayLabel,f.IncludeInSearchFilter FROM DataSourceField f INNER JOIN DataSourceTable t ON f.TableId = t.Id INNER JOIN FieldCategory fc ON fc.Id = f.CategoryId WHERE t.ImportType = 0 AND f.CategoryId IS NOT NULL AND f.IsCalculated = 0 AND f.Name <> {0} ORDER BY ordinal".SqlFormatString(keyField)
    	searchOptionGrid.DataSource = Database.Tenant.GetDataTable(Sql)
    	searchOptionGrid.DataBind()
    End Sub	
    
    
    Protected Sub searchOptionGrid_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles searchOptionGrid.PageIndexChanging
    	searchOptionGrid.PageIndex = e.NewPageIndex   
    	loadSearchOptionFilterFields()
    	RunScript("UpdateGridchecked();")
    	
    End Sub
    
    Protected Sub ddlPageSize_SelectedIndexChanged (sender As Object, e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
    	searchOptionGrid.PageSize = ddlPageSize.SelectedValue
    	searchOptionGrid.PageIndex = 0
    	loadSearchOptionFilterFields()
    	RunScript("UpdateGridchecked();")
    End Sub

End Class