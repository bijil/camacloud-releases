﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DesktopWeb.Master" CodeBehind="SearchFieldOptions.aspx.vb" Inherits="CAMACloud.Console.SearchFieldOptions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">
            function UpdateGridchecked(){
            $('input[type="checkbox"]').each(function(){
				$(this).change(function(){
					var value = this.checked ? 1: 0;
					var fieldId = $(this).parent().attr('field');
		    			var data = {
				                Id: fieldId,
				                Value: value,
				            };              
					$ds('updatefilterfields', data, function (resp) {
		            			
		            		});
				})
			})
		}
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div style="margin-left:10px">
<h1>Search Options</h1>
<p class="info">
        The Search Options page should list the parcel-level fields.
</p>
Number of items per page : 
<asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="true" width="50px">  
        <asp:ListItem Text="15" Value="15" />  
        <asp:ListItem Text="30" Value="30" />  
        <asp:ListItem Text="45" Value="45" />
</asp:DropDownList>  
</br>
<asp:GridView ID="searchOptionGrid" runat="server" AllowPaging="true" PageSize="15" OnPageIndexChanging="searchOptionGrid_PageIndexChanging">
	<Columns>
	        <asp:BoundField ItemStyle-Width="180px" DataField="Name" HeaderText="Name" />
	        <asp:BoundField ItemStyle-Width="180px" DataField="DisplayLabel" HeaderText="Display Label" /> 
	        <asp:TemplateField HeaderText="" HeaderStyle-Width="80px">
                    	<ItemTemplate>
                    		<asp:CheckBox runat="server" ID="enableFilterField"  Checked='<%# Convert.ToBoolean(Eval("IncludeInSearchFilter"))%>' field='<%# Eval("Id")%>' />
                    	</ItemTemplate>
                  </asp:TemplateField>
        </Columns>
</asp:GridView>
</div>
</asp:Content>

