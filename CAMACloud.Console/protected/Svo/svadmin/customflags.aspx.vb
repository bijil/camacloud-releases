﻿Imports System.Web.Script.Services
Imports System.Web.Services
Partial Class svadmin_customflags
    Inherits System.Web.UI.Page

    Sub LoadGrid()
        grid.DataSource = Database.Tenant.GetDataTable("SELECT * FROM SketchReviewFlags ORDER BY Name")
        grid.DataBind()
    End Sub

'	Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
'    	If txtFlagName.Text.Trim <> "" Then
'    		Dim validateqry As String = "SELECT COUNT(*) FROM SketchReviewFlags Where name ='" + txtFlagName.Text +"'"
'            Dim Namevalidate As Integer = Database.Tenant.GetIntegerValue(validateqry)
'            If Namevalidate > 0 Then
'            	Alert("Flag already exists")
'            	txtFlagName.Text = ""
'            	LoadGrid()
'            	Return
'            End If
'            Database.Tenant.Execute("INSERT INTO SketchReviewFlags (Name) VALUES (" + txtFlagName.Text.ToSqlValue + ");")
'            LoadGrid()
'            txtFlagName.Text = ""
'    	End If
'    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    	If Not IsPostBack Then
            Dim UserName = Membership.GetUser().ToString
			Dim dcsUser As Boolean = True
            If UserName.ToLower <> "dcs-qa" And UserName.ToLower <> "dcs-rd" And UserName.ToLower <> "admin" And UserName.ToLower <> "dcs-support" And UserName.ToLower <> "dcs-ps" Then
                dcsUser = False
            End If
            If Roles.IsUserInRole("SVAdmin") Then
			Else
				If dcsUser = False Then
                	Response.Redirect("~/web/403.aspx")
				End If
			End If
            LoadGrid()
        End If
    End Sub

    Protected Sub grid_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grid.RowCommand
        If e.CommandName = "DeleteFlag" Then
            Database.Tenant.Execute("DELETE FROM SketchReviewFlags WHERE Id = " & e.CommandArgument)
            LoadGrid()
        End If
    End Sub
    
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Shared Function savecustomflag(flagName As String) As String
    	Dim validateqry As String = "SELECT COUNT(*) FROM SketchReviewFlags Where name ='" + flagName +"'"
    	Dim Namevalidate As Integer = Database.Tenant.GetIntegerValue(validateqry)
        If Namevalidate > 0 Then
        	Return "Flag already exists"
        End If
        Database.Tenant.Execute("INSERT INTO SketchReviewFlags (Name) VALUES (" + flagName.ToSqlValue + ");")
    	Return ""
     End Function
    
End Class
