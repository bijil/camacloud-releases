﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/DesktopWeb.master"
	AutoEventWireup="false" Inherits="CAMACloud.Console.svadmin_flags" Codebehind="flags.aspx.vb" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="Server">
<link rel="Stylesheet" type="text/css" href="/App_Static/css/Svo/SketchValidation.css" />
<link rel="Stylesheet" href="/App_Static/css/colorPicker.css" type="text/css" />
<script type="text/javascript" src="/App_Static/jslib/jquery.colorPicker.js"></script>
<script type="text/javascript" src="/App_Static/jslib/bootstrap-colorpicker.min.js"></script>
<link href="/App_Static/css/bootstrap-colorpicker.min.css" rel="stylesheet" type="text/css" />	
	<script type="text/javascript">
		function adminPageFunctions() {
			$('.colorpicker').colorPicker();
		}
		$(function () {
$("#spanMainHeading").html('Sketch Status Flags - Color Codes');
})

$(function () {
            $('.pickcolor').colorpicker();

        });
        
	</script>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="MainContent" runat="Server">
<div style="float:left;width:94%;margin-left:3%;margin-top: 1%;">
	<asp:GridView runat="server" ID="grid" AutoGenerateColumns="false" BorderStyle="None"
		GridLines="None" ShowHeader="false" SkinID="NoStyles">
		<RowStyle Height="30px" />
		<Columns>
			<asp:BoundField DataField="Name" HeaderText="Flag Name" ItemStyle-Width="200px" />
			<asp:TemplateField>
				<ItemStyle Width="200px" />
				<ItemTemplate>
					<asp:HiddenField runat="server" ID="FID" Value='<%# Eval("Id") %>' />
					<div class="input-group pickcolor">	
						<asp:TextBox runat="server" ID="ColorCode" Text='<%# Eval("ColorCode") %>'  CssClass="colorpicker" Style ="Width: 110px; padding: 0px; border-radius:0px" />
						<span class="input-group-addon"><i style="margin-bottom: 0px; width: 19px; height: 19px; display: inline-block; background-color: #00ffff;"></i></span>
					</div>
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</asp:GridView>
	<div style="margin-top: 20px;">
		<asp:Button runat="server" class="btnNrml" ID="btnSave" Text=" Update Changes " style="font-weight:bold;height: 25px;width: 150px;" />
		<asp:Button runat="server" class="btnNrml" ID="btnCancel" Text=" Cancel " style="font-weight:bold;height: 25px;width: 100px;" />
	</div>
	</div>
</asp:Content>
