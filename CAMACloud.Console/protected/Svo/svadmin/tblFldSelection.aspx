﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DesktopWeb.Master" CodeBehind="tblFldSelection.aspx.vb" Inherits="CAMACloud.Console.tblFldSelection" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link rel="Stylesheet" type="text/css" href="/App_Static/css/Svo/SketchValidation.css" />
<link rel="Stylesheet" href="/App_Static/css/colorPicker.css" type="text/css" />
<script type="text/javascript" src="/App_Static/jslib/jquery.colorPicker.js"></script>
<script type="text/javascript" src="/App_Static/jslib/bootstrap-colorpicker.min.js"></script>
<link href="/App_Static/css/bootstrap-colorpicker.min.css" rel="stylesheet" type="text/css" />	
	<style>
	
::-webkit-scrollbar
{
width:3px;
height:12px;
}
.mGrid{
margin-top: 0px !important;
}
 .hiddencol
  {
    display: none;
        
  }
  .style3{
  display: none;
  }
	</style>
	<script type="text/javascript">
		function adminPageFunctions() {
			$('.colorpicker').colorPicker();
		}
		
		$(function () {
			$("#spanMainHeading").html('Grid Configuration');
		})

		$(function () {
            $('.pickcolor').colorpicker();
        });
		function CheckFields(){
            $('.cbPrimary input').each(function () {
                $(this).change(function () {
            		checboxselected();
                });
            });
        }
        function checboxselected(){
            var total = 0
            total=$('input:checked').length;
            if(total > 4){
            $('.cbPrimary input').attr('disabled', 'disabled');
        	$('.cbPrimary input:checked').removeAttr('disabled');
		    return false;
             }
              else{
            $('.cbPrimary input').removeAttr('disabled');
                  	}
        }
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="NavBarContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
	<div style="margin-left: 3%;margin-top: 20px;margin-bottom: 10px;">
		<label id="lblField" style="font-weight: bolder;margin-right: 10px;">Select table: </label>
    	<asp:DropDownList ID="ddlTable" runat="server" AutoPostBack="true" Width="271px"/>
    	<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlTable" ErrorMessage="*" ValidationGroup="CustomAction" />
 	</div>
    <div class="tabcontent" style="border-top: 0px none; padding: 2px 10px 0px 132px; height: 311px; width: 273px; overflow-y: auto; overflow-x: none;">
        <asp:GridView ID="gvDataSourceTable" runat="server" DataKeyNames="Id" AutoGenerateColumns="false">
            <Columns>
                <asp:TemplateField ItemStyle-Width="25px">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBox1" runat="server" TableId='<%# Eval("TableId") %>' FieldId='<%# Eval("Id") %>' SourceTable='<%# Eval("SourceTable") %>' FieldName='<%# Eval("FieldName") %>' Checked='<%# IIF(Eval("Status") Is DbNull.Value,"False","True") %>' class='cbPrimary' />         		
                   </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="DisplayLabel" ItemStyle-Width="243px" HeaderText="Field Name" />
                <asp:BoundField DataField="FieldName" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="style3"/>
            </Columns>
        </asp:GridView>
    </div>
	<div id="btn" style="margin-top: 10px;margin-left: 176px;">
		<asp:Button runat="server" class="btnNrml" ID="btnSave" Text=" Save " style="font-weight:bold;height: 25px;width: 100px;" />
		<asp:Button runat="server" class="btnNrml" ID="btnCancel" Text=" Cancel " style="font-weight:bold;height: 25px;width: 100px;" />
	</div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FootContents" runat="server">
</asp:Content>
