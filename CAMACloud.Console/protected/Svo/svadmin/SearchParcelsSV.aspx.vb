﻿Partial Class SearchParcelsSV
    Inherits System.Web.UI.Page
    Public organizationName As String
    Public stateName As String
    Public _organizationId As String
    Public NearmapAPIKey As String
    Public NearmapAccess As String
    Public NearmapWMSAPIKey As String
    Public NearmapWMSAccess As String
    Public woolpertApikey As String
    Public woolpert6inchId As String
    Public woolpert3inchId As String
    Public woolpertAccess As String
    Public customWMS As String
    Public customMapIsImage As String
    Public customWMSAccess As String

    Sub LoadUsers()
        Dim cUsers = Membership.GetAllUsers
        Dim removeList As New List(Of String)
        For Each u As MembershipUser In cUsers
            If Not Roles.IsUserInRole(u.UserName, "SVAdmin") AndAlso Not Roles.IsUserInRole(u.UserName, "SVReviewer") Then
                removeList.Add(u.UserName)
            End If
        Next
        For Each un In removeList
            cUsers.Remove(un)
        Next
        ddlUsers.DataSource = cUsers
        ddlUsers.DataTextField = "UserName"
        ddlUsers.DataValueField = "UserName"
        ddlUsers.DataBind()
    End Sub

    Public ReadOnly Property DoNotChangeTrueFalseInPCI As Boolean
        Get
            Dim ChangeTrueFalse = False
            Dim dr As String = Database.Tenant.GetStringValue("SELECT Value FROM ClientSettings WHERE Name = 'DoNotChangeTrueFalseInPCI'")
            If dr = "1" Then
                ChangeTrueFalse = True
            Else
                ChangeTrueFalse = False
            End If
            Return ChangeTrueFalse
        End Get
    End Property

    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        LoadUsers()
        Dim keyField As String = Database.Tenant.GetStringValue("SELECT KeyField1 FROM Application")
        Dim table As DataTable = Database.Tenant.GetDataTable("select concat (f.Name,'/',f.datatype,'/',f.SchemaDataType,'/',f.id),f.DisplayLabel, f.IncludeInSearchFilter FROM DataSourceField f INNER JOIN DataSourceTable t ON f.TableId = t.Id INNER JOIN FieldCategory fc ON fc.Id = f.CategoryId WHERE t.ImportType = 0 AND f.CategoryId IS NOT NULL AND f.IsCalculated = 0 AND f.IncludeInSearchFilter = 1 AND f.Name <> {0} ORDER BY ordinal".SqlFormatString(keyField))
        ddlUsersSV.FillFromTable(table, True)
        ddlUsersSV.InsertItem("Parcel", "0", 1)
        ddlUsersSV.InsertItem("Assignment Group", "1", 2)
        ddlUsersSV.InsertItem("CC Tag", "2", 3)
        ddlUsersSV.InsertItem("Sketch Reviewed Date", "SketchReviewedDate/4/sketchReviewed/", 4)
        ddlUsersSV.InsertItem("─────────────────────────────────", "", 5)
        ddlUsersSV.Items(5).Attributes("disabled") = "disabled"
        Dim flagCount As Integer = SrchFlag.Items.Count
        Dim otherFlagTable As DataTable = Database.Tenant.GetDataTable("SELECT concat('OF-', Id) as OFId, Name FROM SketchReviewFlags ORDER BY Id")
        For Each otherFlagRow As DataRow In otherFlagTable.Rows
            SrchFlag.InsertItem(otherFlagRow("Name"), otherFlagRow("OFId"), flagCount)
            flagCount = flagCount + 1
        Next
        Dim aggregatefields As DataTable = Database.Tenant.GetDataTable("SELECT * FROM AggregateFieldSettings ORDER BY TableName")

        Dim count As Integer = table.Rows.Count + 5
        For Each dr As DataRow In aggregatefields.Rows
            Dim rowuid As String = dr(0).ToString()
            Dim tableName As String = dr(1).ToString()
            Dim colName As String = dr(2).ToString()
            Dim functName As String = dr(3).ToString()
            Dim fieldName As String
            Dim tableid As String = Database.Tenant.GetStringValue("SELECT id FROM DataSourceTable WHERE name ='" + tableName + "'")
            Dim datatype As String = Database.Tenant.GetStringValue("SELECT datatype FROM DataSourceField WHERE tableid ='" + tableid + "' AND Name = '" + colName + "'")
            Dim schemadatatype As String = Database.Tenant.GetStringValue("SELECT SchemaDataType FROM DataSourceField WHERE tableid ='" + tableid + "' AND Name = '" + colName + "'")
            Dim uniqueName As String = dr(6).ToString()
            If uniqueName <> "" Then
                fieldName = uniqueName
            Else
                fieldName = functName + "_" + tableName + "_" + colName
            End If
            ddlUsersSV.InsertItem(fieldName, "aggfieldId#" + rowuid + "/" + datatype + "/" + schemadatatype, count)
            count = count + 1
        Next

        Dim reviewFlagsTable As DataTable = Database.Tenant.GetDataTable("SELECT concat('OF-', Id) as OFId, Name FROM SketchReviewFlags ORDER BY Name")
        Dim statusFlagsTable As DataTable = Database.Tenant.GetDataTable("SELECT Id as OFId, Name FROM SketchStatusFlags ORDER BY Name")

        Dim allItems As New List(Of ListItem)
        allItems.Add(New ListItem("None", "9"))
        allItems.Add(New ListItem("All", "all"))
        allItems.Add(New ListItem("Is NULL", "7"))
        allItems.Add(New ListItem("Is NOT NULL", "8"))

        For Each reviewFlagRow As DataRow In reviewFlagsTable.Rows
            Dim newItem As New ListItem(reviewFlagRow("Name").ToString(), reviewFlagRow("OFId").ToString())
            allItems.Add(newItem)
        Next
        For Each statusFlagRow As DataRow In statusFlagsTable.Rows
            Dim newItem As New ListItem(statusFlagRow("Name").ToString(), statusFlagRow("OFId").ToString())
            allItems.Add(newItem)
        Next
        Dim sortedItems = allItems.Skip(2).OrderBy(Function(item) item.Text).ToList()
        SrchFlag.Items.Clear()
        SrchFlag.Items.AddRange(allItems.Take(2).ToArray())
        SrchFlag.Items.AddRange(sortedItems.ToArray())
        SrchFlag.Items.FindByText("None").Selected = True

        rpFlagStyles.DataSource = Database.Tenant.GetDataTable("SELECT * FROM SketchStatusFlags ORDER BY Id")
        rpFlagStyles.DataBind()

        organizationName = ClientOrganization.PropertyValue("County", HttpContext.Current.GetCAMASession().OrganizationId)
        stateName = ClientOrganization.PropertyValue("State", HttpContext.Current.GetCAMASession().OrganizationId)
        _organizationId = HttpContext.Current.GetCAMASession().OrganizationId
    End Sub

End Class
