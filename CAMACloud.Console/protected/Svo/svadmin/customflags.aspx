﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/DesktopWeb.master" AutoEventWireup="false" Inherits="CAMACloud.Console.svadmin_customflags" Codebehind="customflags.aspx.vb" %>


<asp:Content ID="Content3" ContentPlaceHolderID="head" Runat="Server">
<script>
	$(function () {
		$("#spanMainHeading").html('Sketch Review - User-defined Flags');
	})
	
	function checkforName() {
        event && event.preventDefault();
        let flgname1 = $('#MainContent_txtFlagName').val().trim(), pattern = /[!-\/:-@[-`{-~]/;

		if (flgname1 == "") {
			alert("Please enter the flag name.");
			return false;
		}
		
	    if (pattern.test(flgname1)) { 
		    alert('Please use Alphabet or Numbers as flag name.'); 
		    return false; 
	    }

	    $.ajax({
            url: "customflags.aspx/savecustomflag",
            data: "{ 'flagName':'" + flgname1 + "'}",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
            	if(data.d)
            		alert(data.d);
            	$('#MainContent_txtFlagName').val("");
            	window.location.reload();
            	return false;
            },
            error: function (response) {
            	$('#MainContent_txtFlagName').val("");
                alert('Save failed');
                return false;
            }
        });
        return false;
	}
</script>
<link rel="Stylesheet" type="text/css" href="/App_Static/css/Svo/SketchValidation.css" />
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="MainContent" Runat="Server">
   <div style="float: left;width:94%;margin-left:3%;">
    <h2>Sketch Review - User-defined Flags</h2>
    <p>
        New Flag: 
        <asp:TextBox runat="server" ID="txtFlagName" Width="100px" MaxLength="20" />
        <asp:Button runat="server" ID="btnAdd" onclientclick="return checkforName();" class="btnNrml" style="width: 75px;height: 21px;" Text="  Add  " />
    </p>
    <asp:GridView runat="server" ID="grid">
        <Columns>
            <asp:BoundField DataField="Name" HeaderText="Flag" ItemStyle-Width="200px" />
            <asp:TemplateField>
                <ItemStyle Width="70px" />
                <ItemTemplate>
                    <asp:LinkButton runat="server" ID="lbDelete" Text="Delete" CommandName="DeleteFlag" CommandArgument='<%# Eval("Id") %>' OnClientClick="return confirm('Are you sure you want to delete this record?');" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    </div>
</asp:Content>

