﻿<%@ Page Title="" Language="VB" MasterPageFile="/App_MasterPages/DesktopWeb.Master" AutoEventWireup="false" Inherits="CAMACloud.Console.SV" CodeBehind="sv.aspx.vb" %>
<asp:Content runat="server" ContentPlaceHolderID="head">
<meta name="viewport" content="user-scalable=no,initial-scale=1.0,maximum-scale=1.0,width=device-width,height=device-height" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default" />
<meta name="format-detection" content="telephone=no">
<link rel="Stylesheet" type="text/css" href="/App_Static/css/Svo/SketchValidation.css?1267992" />
  
<link rel="Stylesheet" href="/App_Static/slider/winclassic.css" type="text/css" />

  <link rel="Stylesheet" href="/App_Static/css/Svo/iphonestyle.css?1233" type="text/css" />
  <script type="text/javascript" src="/App_Static/jslib/sqllite.js"></script>
  <script type="text/javascript" src="/App_Static/jslib/iphonestyle.js?1232"></script>
  <script type="text/javascript" src="/App_Static/js/Svo/commands.js?<%= Now.Ticks %>"></script>
  <script type="text/javascript" src="../../App_Static/jslib/infobubble.js"></script>
  
  
<script type="text/javascript">
    var TrueFalseInPCI = '<%= DoNotChangeTrueFalseInPCI %>'
    var google = false;
    var smap, map;
    var theForm;
    var currentInput;
    var activeParcel;
    var Hammer = false;
    var __DTR = false;
    var appType = "sv";
    var activeTab;
    var isBeforeUnload = false;
    var ascParcel = true;
    var ascAddress = true;;
    var countyName = '<%= organizationName %>'
    var _organizationId = '<%=  _organizationId %>'
    var stateName = '<%= stateName%>'
    function clearVectorlabelPosition() {
        vectorLabelPositions = [];
    }
</script>
<style>
    #divSubHeader {
        height: 0px;
    }
    
    html{
    overflow-x:hidden;
    }
    
    #divContentArea {
        top: 45px;
    }

    #btnKML:hover, #btnSHP:hover {
        color: #803434;
        cursor: pointer;
        font-weight: 600;
    }
    #showSketchOptions {
		background:#fff;
		border:1px solid #aaa;
		position:absolute;
		top:18px;
		left: 60px;
    	max-width: 225px;
	}
	#showSketchOptions span {
		display:block;
		padding:2px ;
		font-size:12px;
		font-family:sans-serif;
		height:30px;
		overflow: hidden;
	}
	#showSketchOptions span:first-child {
		background:#4484bb;
		color:#fff;
	}
	.tipr_container_left {
		left:55%  !important;
		margin-left: 0px !important;
	}
	
	.showSidePanel {
		z-index: 1000 !important;
	}
	
	.masklayer {
		z-index: 1001 !important;
	}

	.sv-prompt {
            display: none;
            position: fixed;
            z-index: 100;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            overflow: auto;
            background-color: rgba(0,0,0,0.4);
        }

        .sv-prompt-content {
            background-color: #fefefe;
            margin: 15% auto;
            padding: 20px;
            border: 3px solid #348eeb;
            width: 560px;
			border-radius: 10px;
			height: 200px;

        }

		.sv-prompt-ok {
			background-color: #348eeb;
			width: 75px;
			border: none;
			border-radius: 5px;
			height: 35px;
			color: white;
			float: right;
			margin-right: 20px;
		}

        .sv-prompt-close {
            color: #348eeb;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .sv-prompt-close:hover,
        .sv-prompt-close:focus {
            color: black;
            text-decoration: none;
            cursor: pointer;
        }

</style>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=geometry&sensor=false&region=US&key=AIzaSyC7e5YrZlKqWtaY4ehsDwMgCis4IfNs7Xw"></script>
<script type="text/javascript" src="/App_Static/js/Svo/system.js?t=14"></script>
<script type="text/javascript" src="../../App_Static/js/qc/000-window.js?t=14"></script>

<script type="text/javascript" src="/App_Static/js/qc/010-quality.js?<%= Now.Ticks %>"></script>
<asp:Literal runat="server" Text="<style type='text/css'>" />
<asp:Repeater runat="server" ID="rpFlagStyles">
<itemtemplate>
label[for="rblStatus_<%# Container.ItemIndex%>"]:before {
background: <%# Eval("ColorCode")%>;
}

</itemtemplate>
</asp:Repeater>
<asp:Literal runat="server" Text="</style>" />
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="SwitchMode">
	<div id = "lightWeight" runat="server" class="switchLNMode tip" style="display:none;" abbr="The Light mode is used to reduce the load in SV. This will enable only limited functionalities.</br>
We will be able to access only the MAP and its sketch editor , once we enable the SV LM.</br> Light Mode shows shapefile view of sketch after save" data-mode="left" >
		<label class="switch">
			<input class="switch-input" type="checkbox" onchange="return enableLightMode();" />
			<span class="switch-label" data-on="LM" data-off="NM"></span> 
			<span class="switch-handle"></span> 
		</label>
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="MainContent">
	<input type="hidden" id="sketchid" name="sketchid" value="">
	<span class = "showSidePanel"></span>
	<div id="divSearchCntnr">
		<div id="divSearchTop">
			<span class= "hideSidePanel"></span>
			
			
		</div>
		<div id="divSearchTop1" Style="display:none">
		</div>
		<div class="addclrfltr" style="margin-right: 0px;">
				<a class="addfilter">Add Filter</a>
				<a class="clearfilter"> Clear Filter</a>
			</div>
		<div id="section1" style="display: none;">
    		<h2 style="font-size: 20px;text-decoration: underline;">Add New Filter</h2>
			<div style="display:flex;margin-top: 13px;">
					<div style="flex:50%;padding-top: 0px;">
							<h2 >Search By</h2>
					</div>
					<div style="flex:55%;margin-right: 20px;">
						 <div class="eachDivL1" style="width: 112%;">
						 <asp:DropDownList runat="server" ID="ddlUsersSV" CssClass="ddlUsersSV srchSlct">
						 </asp:DropDownList>
						</div>
					</div>
			</div>
				
				<div style="display:flex;padding-top: 4px;">
					<div style="flex:50%">
					 	<h2 >Filter condition</h2>
					</div>				
					<div style="flex: 64%;margin-right: -5px;">
						<div style="width: 97%;">
							<asp:DropDownList runat="server" ID="Operator" CssClass="Operator srchSlct" AppendDataBoundItems="true">
								<asp:ListItem Text="--Select--" Value="select"></asp:ListItem>
								<asp:ListItem Text="Contains" Value=" LIKE "></asp:ListItem>
								<asp:ListItem Text="Equal to" Value=" = "></asp:ListItem>
								<asp:ListItem Text="Not equal to" Value=" <> "></asp:ListItem>
								<asp:ListItem Text="Starting with" Value="startwith"></asp:ListItem>
								<asp:ListItem Text="Not starting with" Value="notstrtwith"></asp:ListItem>
								<asp:ListItem Text="Ending with" Value="endwith"></asp:ListItem>
								<asp:ListItem Text="Less Than" Value=" < "></asp:ListItem>
								<asp:ListItem Text="Greater Than" Value=" > "></asp:ListItem>
								<asp:ListItem Text="Less than or equal" Value=" <= "></asp:ListItem>
								<asp:ListItem Text="Greater than or equal" Value=" >= "></asp:ListItem>
								<asp:ListItem Text="Is NOT NULL" Value=" IS NOT NULL "></asp:ListItem>
								<asp:ListItem Text="Is NULL" Value=" IS NULL "></asp:ListItem>
								
							</asp:DropDownList>
						</div>
					</div>
				</div>
				<div style="display:flex;padding-top: 4px;padding-bottom: 7px;">
					<div style="flex:50%;">
						<h2 >ID/Value</h2>
					</div>
					<div style="flex:50%;margin-right: -5px;">
						<div class="eachDivR"style="width:112%">
							<input id="SrchId" type="text" autocorrect="off" autocapitalize="off" maxlength="50" class="srchtxt" />
							<select id="SrchId2" type="text" autocorrect="off" autocapitalize="off" maxlength="50" class="srchtxt" style="display: none;"></select>
						</div>
					</div>
				</div>
				<div class="divbtn">
					<input type="button" class="btnrch" value="Close" id="closefilter" style="float: right;margin-right: 22px;"></button>
					<input type="button" value="Add Filter" class="btnrch" id="addcstmfltr" style="float: right;margin-right: 10px;width: 81px;"></input>
			</div>
		</div>
		<div id="section2">
			<div class="eachDivR">
				<h2 >Reviewed By</h2>
				<asp:DropDownList runat="server" ID="ddlUsers" CssClass="ddlUsers srchSlct" AppendDataBoundItems="true">
					<asp:ListItem Text="All" Value="all"></asp:ListItem>
					<asp:ListItem Text="Is NULL" Value="None"></asp:ListItem>					
				</asp:DropDownList>
			</div>
			
			<div class="eachDivL2">
				<h2>Flag Status</h2>
				<asp:DropDownList runat="server" ID="SrchFlag" CssClass="SrchFlag srchSlct" AppendDataBoundItems="true">
					<asp:ListItem Text="All" Value="all"></asp:ListItem>
					<asp:ListItem Text="Field Inspection" Value="1"></asp:ListItem>
					<asp:ListItem Text="Data Entry" Value="2"></asp:ListItem>
					<asp:ListItem Text="Further Review" Value="3"></asp:ListItem>
					<asp:ListItem Text="Complete" Value="4"></asp:ListItem>
					<asp:ListItem Text="Quality Control" Value="5"></asp:ListItem>
					<asp:ListItem Text="Internal Question" Value="6"></asp:ListItem>
					<asp:ListItem Text="Is NULL" Value="7"></asp:ListItem>
					<asp:ListItem Text="Is Not NULL" Value="8"></asp:ListItem>				
				</asp:DropDownList>
				</div>
			<div class="divbtn">
				<button class="btnrch" id="btnSearch" >Search</button>
				<button class="btnrch" id="btnClear" >Clear</button>
				<button class="btnrch" id="btnRefresh">Refresh</button>
			</div>
		</div>
	<div id="divSearchBtm">
	
	<table class="tablesrch" id="TabSearchContentsHd">
	<thead>
	<tr>
	<td width="10%"></td>
	<td  width="45%"><span class="hdspn">Parcel Id</span></td>
	<td  width="45%"><span class="hdspn">Address</span></td>
	</tr>
	</thead>
	</table>
	<div id="divtab">
	<table class="tablesrch" id="TabSearchContents">
	<tbody>
	<tr>
	<td colspan="3" class="TdNodata" >No Data Available</td>
	</tr>
	</tbody>
	</table>
	
	</div>
	<%--<div id="divprclcnt" style="float: left;margin-left: 22%;bottom: 4px;display:none;padding-top: 10px;padding-bottom: 10px;"><b style="color: #561a1a;"> Total Number of Parcels:</b> <label style="font-weight: bold;" id="lblCntParcl"></label> </div>--%>
		
	<div id="divprclcnt" class="parcel-info">
    <b>Total Number of Parcels:
    <label id="lblCntParcl"></label></b>


<div class="search-container" style="display:none">
    <div class="pagination-controls">
        <span id="btnPreviousPage" class="pagination-button">Previous</span>
        <select id="pageSelector" class="page-selector"></select>
        <span id="btnNextPage" class="pagination-button">Next</span>

        <label for="recordsPerPageSelector" class="page-size-label">Page</label>
        <select id="recordsPerPageSelector" class="records-per-page-selector">
            <option value="25">25</option>
            <option value="50">50</option>
            <option value="75">75</option>
            <option value="100">100</option>
			<option value="500">500</option>
			<option value="1000">1000</option>
        </select>
    </div>
</div>
			</div>
	</div>
	</div>
	<div id="divContents" style="float: left;border-left: 1px solid #ddf2ff;border-right:4px solid #ddf2ff;">
	<section class="screen" id="sketch-validation">
	
	<div class="searchframe">
	<div id="search-map" style="position:absolute">
	</div>
	</div>
	<div class="resultframe">
	<table class="map-table">
	<asp:HiddenField ID="hdnUser" runat="Server" Value="" />
	<tr>
	<td class="mapframe">
	<iframe width="100%" frameborder="0" src="map.aspx?123475597" id="mapFrame"></iframe>
	</td>
	</tr>
	</table>
	</div>
	</section>
	
	
	
	</div>
	<div id="divRghtCntnr" >
	<div id="divSketchReview" style="height:72%; overflow-y:auto;">
	<div class="sketchload-div">
		<span style="float:left;width:100%;position: relative;">
			<span style="float:left;width:100%"><span style="width: 50px;float: left;">Sketches</span><select class="sketch-select" onchange="previewSketch()" style="min-width: 40px; max-width: 180px;"></select></span>
			<span style="float:left;width:100%;margin-top: 4px;"><span style="width: 50px;float: left;">Sections</span><select onchange="previewSketch()" class="section-select" style="min-width: 40px;"></select></span>
			<span id="showSketchOptions" style="position: absolute;display:none"></span>
		</span>
	</div>
	<img src="/App_Static/css/images/close.png" class="TrnsbackClose" onclick="return closeParcel(true)" Search By*="close parcel">
	
	<div id="divRhtHead"><label class="LblRghthd">Sketch Validation & Review </label> </div>
	
	<div id="divlblIds" style="float:left;width:96%;margin-left: 2%;">
	<div class="parcel-header-data">
	<div>
	Loading ...
	</div>
	</div>
	</div>
	<div class="aux-data"></div>
	<div id="divRighCntnts"style>
	<h2 >Notes :</h2>
	<textarea style="width: 100%; height: 60px; resize: none;" id="txtRevNote" class="review-note"></textarea>
	
	<asp:RadioButtonList runat="server" ID="rblStatus" style="width:100%;font-weight: 500;" RepeatColumns="2"
	RepeatDirection="Horizontal" ClientIDMode="Static">
	<asp:ListItem Text="Field Inspection" Value="1" />
	<asp:ListItem Text="Data Entry" Value="2" />
	<asp:ListItem Text="Further Review" Value="3" />
	<asp:ListItem Text="Completed" Value="4" />
	<asp:ListItem Text="Internal Question" Value="6" />
	</asp:RadioButtonList>

	<!--<asp:RadioButtonList runat="server" ID="rblQc" RepeatDirection="Horizontal" ClientIDMode="Static">
    <asp:ListItem Text="QC Passed" Value="5" />
    </asp:RadioButtonList>	-->

	<div class="qc-passed-switch" style="margin: 5px 0px;">
		<label class="sw-qc-label">
			<input type="radio" name="qc-passed" id="toggleSwitch"/> QC passed
		</label>
		<!--<label class="sw-square-switch">
			<input type="checkbox" id="toggleSwitch"/>
			<span class="sw-slider">
				<span class="sw-text sw-on">YES</span>
				<span class="sw-text sw-off">NO</span>
			</span>
		</label>-->
	</div>

	<div class="otherflags" id="divOtherFlags" style="min-height:20px;" runat="server">
	<span style="float:left;width:100%"><h2>Other flags :</h2></span>
	<table class="flag-fields">
	<asp:Repeater runat="server" ID='rptOtherFlags'>
	<ItemTemplate>
	<tr>
	<td >
	<%#Eval("Name") %>
	</td>
	<td style="width: 50px;">
	<input type="checkbox" flag='<%# Eval("Id") %>' style="width: 40px;" />
	</td>
	</tr>
	</ItemTemplate>
	</asp:Repeater>
	</table>
	
	</div>
	
	
	<div id="divRightButtons">
		<button class="btnrch" id="btnSaveRvw" >Save</button>
		<button class="btnrch" id="btnSaveNext" style="width: 100px;">Save & Next</button>
		<button class="btnrch" id="btnRstRvw" >Reset</button>
	</div>
	</div>
	
	
	
	</div>
	<div id="divExtraMap">
	<div id="divexport" style="display:none;text-align: center;float: left;width: 100%; margin-bottom: 5px;display: none;">
		<a id="btnKML" style="font-weight: bold;margin-right: 15px;text-decoration: underline;">Export as KML</a>
		<a id="btnSHP" style="font-weight: bold;text-decoration: underline;">Export as SHP</a>
	</div>
	<button class="btnrch btnextraMap btnpict" onclick=" loadPictometry();return false;" style="display:none"> Show In EagleView</button>
	<button class="btnrch btnextraMap btnEv" onclick=" loadEagleView();return false;" style="display:none"> Show In EagleViewNew</button>
	<button class="btnrch btnextraMap btnSanBorn" onclick=" loadSanborn();return false;" style="display:none"> Show In Sanborn</button>
	<button class="btnrch btnextraMap btnqPublic" onclick=" loadQPublic({ sv: true, autoOpen: false });return false;" style="display:none"> Show In qPublic</button>
	<button class="btnrch btnextraMap btnCyclomedia" onclick=" loadCyclomedia();return false;" style="display:none"> Show In Cyclomedia</button>
	<button class="btnrch btnextraMap btnNearmap" onclick=" loadNearmap();return false;" style="display:none"> Show In Nearmap</button>
	<button class="btnrch btnextraMap btnNearmapWMS" onclick=" loadNearmapWMS();return false;" style="display:none"> Show In WMS Nearmap</button>
	<button class="btnrch btnextraMap btnCustomWMS" onclick=" loadcustomWMS();return false;" style="display:none"> Show In CustomMap</button>
	<button class="btnrch btnextraMap btnWoolpert" onclick=" loadWoolpert();return false;" style="display:none"> Show Woolpert Imagery</button>
	</div>
	<div id="divNewWndvBtns" >
	<div id="divNavigate" >
	<button id="btnPrev" class="btnrch btnnav tip" data-mode="top" onclick=" movePrev();return false;" abbr="Keyboard shortcut combination for this button is <b>'<'</b>"> Previous </button>
	<button id="btnNext" style="margin-left: 5%;" class="btnrch btnnav tip" data-mode="top" onclick=" moveNxt(); return false;" abbr="Keyboard shortcut combination for this button is <b>'>'</b>" > Next </button>
	</div>
	</div>
	</div>
	<div id="svprompt" class="sv-prompt" style="display: none;">
		<div class="sv-prompt-content">
			<span class="sv-prompt-close" onclick="svPromptClose()">&times;</span>
			<h2>Caution, there are unsaved changes to the property! Please select the desired option below:</h2>
			<div style="padding: 10px; font-size: 16px;">
				<label>
					<input type="radio" name="svprompt" value="save" checked ="checked"/> Save changes and continue
				</label>
			</div>
			<div style="padding: 10px; font-size: 16px;">
				<label>
					<input type="radio" name="svprompt" value="discard"/> Discard changes and continue 
				</label>
			</div>
			<button type="button" class="sv-prompt-ok">OK</button>
		</div>
	</div>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="FootContents">

<div id="divFootCntnr" style="float: left;width:100%;display:none; text-align: right;">
<div class="tip" abbr="Up and Down arrows on the keyboard will zoom in and out." data-mode="top"  style="float: left;width:215px;">
<span class="SpnFootEach">Zoom: <span class="sketch-zoom-value">100%</span></span>&nbsp;
<input type="range" min="0.1" max="2.5" value="1.0" step="0.01" class="sketch-resize sketch-tool" onchange="paintSketch();" />
</div>
<div class="tip" abbr="Left and Right arrows on the keyboard will rotate the sketch left and right." data-mode="top" style="float: left;margin-left: 4%;width:222px;">
<span class="SpnFootEach" >Rotate: <span class="sketch-rotate-value">0&deg;</span></span>&nbsp;
<input type="range" min="-180" max="180" value="0" step="1" class="sketch-rotate sketch-tool" onchange="paintSketch();" />
</div>
<button  style="margin-right: 1%;"class="buttonAslink" onclick="loadMapView();return false;">Open Map</button>
<button  style="margin-right: 1%;"class="buttonAslink" onclick="return OpenSketchNewWndw(1);"> Open Sketch Editor </button>
<button  style="margin-right: 2%;" id="onprc" class="buttonAslink" style="display: none;" onclick="return OpenSketchNewWndw(2);" > Open PRC </button>
</div>
<div class="parcel-header-template" style="display: none;">
<div class="street-address" style="word-break: break-all;">
${StreetAddress}
</div>
<table id="TabRght" cellpadding="0" cellspacing="0" >
<tr>
<td class="TabLeftTd"><h2 >Parcel ID :</h2></td><td class="TabRghtTd" ><label id="lblPrclId" style="word-break: break-all;">${KeyValue1}</label></td>
</tr>
<tr>
<td class="TabLeftTd"><h2 style="width: max-content;">Neighborhood :</h2></td><td class="TabRghtTd"><label id="lblNbrhdId" style="word-break: break-all;">${CC_NBDH}</label></td>
</tr>
</table>
<div runat="server" id="sv_template">
</div>

<div id="divBigSketchCntnr" class="ClsBigSketchCntnr" style="display: none;">
<div id="divSketchBgHead">Parcel Id : <label id="lblBgSkthPrclId"></label></div>
<div class="sketch-big" style="position: absolute;top: 0;height: 435px;width: 440px;background-repeat: no-repeat;background-color: white;background-position: center;"></div>
</div>
<div id="divSketchBgDetail"></div>
</div>

        <script src="../../App_Static/js/qc/010-constants.js"></script>
<script type="text/javascript" src="/App_Static/slider/range.js"></script>
<script type="text/javascript" src="/App_Static/slider/timer.js"></script>
<script type="text/javascript" src="/App_Static/slider/slider.js"></script>
<script type="text/javascript" src="/App_Static/js/Svo/map.js?4"></script>
<script type="text/javascript" src="/App_Static/js/Svo/search.js?<%= Now.Ticks %>"></script>
<script type="text/javascript" src="../../App_Static/js/qc/000-objects.js?636960322665895356"></script>
<script type="text/javascript" src="/App_Static/js/Svo/parcel.js?<%= Now.Ticks %>"></script>
<script type="text/javascript" src="/App_Static/js/Svo/sketch.js?148"></script>
<script type="text/javascript" src="/App_Static/js/Svo/lib/jsrepeater.js?13"></script>
    <script src="../../App_Static/js/qc/000-utillib.js"></script>
    <script src="../../App_Static/js/qc/010-conversions.js"></script>
    <script src="../../App_Static/js/system.js"></script>
    <script src="../../App_Static/js/sketch/sketchlib/base64.js"></script>
<script type="text/javascript">
    var iResize, iRotate;
    var userAccess = document.getElementById("<%=hdnUser.ClientID%>").value;
    var nearmapApiKey = '<%= NearmapAPIKey%>';
	var nearmapAccess = '<%= NearmapAccess%>';
    var nearmapWMSApiKey = '<%= NearmapWMSAPIKey%>';
    var nearmapWMSAccess = '<%= NearmapWMSAccess%>';
    var woolpertApikey = '<%= woolpertApikey%>';
	var woolpert6inchId = '<%= woolpert6inchId%>';
	var woolpert3inchId = '<%= woolpert3inchId%>';
	var woolpertAccess = '<%= woolpertAccess%>';
	var customWMS = '<%= customWMS%>';
	var customMapIsImage = '<%= customMapIsImage%>';
	var customWMSAccess = '<%= customWMSAccess%>';
    $(window).ready(function () {
        getParcelViaHash();
        $(window).keydown(function(event){ 
        	if( event.keyCode == 13 && $(document.activeElement).attr('type') == "radio" ) 
    			return false;
        });   
    });
</script>
</asp:Content>