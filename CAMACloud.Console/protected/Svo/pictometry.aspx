﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pictometry.aspx.vb" Inherits="CAMACloud.Console.pictometry" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <title>EagleView</title>
    <style>
        #content
        {
            margin-left: auto;
            margin-right: auto;
            border-style: solid;
            width: 95%;
            height: 4.5em;
            text-align: center;
        }
        
        #pictometry
        {
            position: absolute;
		    top: 0;
		    left: 0;
		    width: 99.96%;
		    height: 99.96%;
		    padding: 0;
		    margin: 0;
		    overflow: hidden;
        }
    </style>
    <script type="text/javascript" src="/App_Static/crypto/hmac-md5.js"></script>
    <script type="text/javascript" src="/App_Static/crypto/hmac-sha1.js"></script>
    <script type="text/javascript" src="/App_Static/crypto/hmac-sha256.js"></script>
    <script type="text/javascript" src="/App_Static/crypto/hmac-sha512.js"></script>
    <script type="text/javascript" src="/App_Static/crypto/enc-base64-min.js"></script>
    <!--[if lt IE 9]>
  <script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
</head>
<body>
    <div id="content" style='display: none'>
        <h1 style="display: inline">
            IPA Demo</h1>
        <br>
        <a href='#' onclick="gotoEastmanHouse()">Go to Eastman House</a> | <a href='#' onclick="getLocation()">
            Get Current Location</a> | Set Location:
        <input type="text" id="locationText" disabled="disabled" onkeypress="if (event.keyCode == 13) setLocation();" />
        <button type="button" onclick="setLocation();">
            Go</button>
        | Goto Address:
        <input type="text" id="addressText" disabled="disabled" onkeypress="if (event.keyCode == 13) gotoAddress();" />
        <button type="button" onclick="gotoAddress();">
            Go</button>
    </div>
    <div id="pictometry">
        <iframe id="pictometry_ipa" width="100%" height="100%" src="#"></iframe>
    </div>
    <script type="text/javascript">

        function getApiKey() {
            return "<%=APIKey%>"
        }

        function getIPALoadUrl() {
            return "<%=IPALoadUrl%>"
        }

        function getSecretKey() {
            return "<%=SecretKey%>"

        }

        function getFrameId() {
            return "pictometry_ipa";
        }

        function loadScript(url, callback) {
            // Adding the script tag to the head as suggested before
            var head = document.getElementsByTagName('head')[0];
            var script = document.createElement('script'), loaded;
            script.type = 'text/javascript';
            script.src = url;

            // Then bind the event to the callback function.
            // There are several events for cross browser compatibility.
            //   script.onreadystatechange = callback;
            if (callback) {
                script.onreadystatechange = script.onload = function () {
                    if (!loaded) {
                        callback();
                    }
                    loaded = true;
                };
            }
            //   script.onload = callback;

            // Fire the loading
            head.appendChild(script);
        }

        function signedUrl() {
            // create timestamp
            var d = new Date();
            var n = d.getTime();
            var ts = Math.trunc(Math.floor(n / 1000))

            // create the url
            var url = getIPALoadUrl() + "?apikey=" + getApiKey() + "&ts=" + ts
            // generate the hash
            var hash = CryptoJS.HmacMD5(url, getSecretKey());

            // convert hash to digital signature string
            var signature = hash.toString().replace("-", "").toLowerCase()

            // create the signed url
            finalUrl = url + "&ds=" + signature + "&app_id=" + getFrameId()

            return finalUrl
        }

        var ipa, setLocation;
        var activeParcel = {};
        var defaultZoom = 19;
        if (window.opener != null) {
	        if(window.opener.clientSettings['EagleViewDefaultZoom']){
				var defaultZoom = window.opener.clientSettings['EagleViewDefaultZoom'];
				if(defaultZoom > 23){
					defaultZoom =23;
				}else if(defaultZoom < 15){
					defaultZoom =15;	
				}					
			}
            activeParcel.MapPoints = window.opener.activeParcel._mapPoints;
            activeParcel.mapCenter = window.opener.activeParcel.Center();
            if ( window.opener.appType == 'sv' && ( window.opener.prevEagle == 0 ||  window.opener.prevEagle == 1 ) ) {
            	localStorage.setItem('eagleViewDisabled', window.opener.prevEagle);
            	if ( !window.onunload ) {
            		window.onunload = function() { 
            			if(!window.opener.isBeforeUnload)
							localStorage.setItem('eagleViewDisabled', "1");
					}  
            	}
            }
        }

        if (getApiKey() != '')
            loadScript("https://pol.pictometry.com/ipa/v1/embed/host.php?apikey=" + getApiKey(), function () {

                ipa = new PictometryHost(getFrameId(), 'https://pol.pictometry.com/ipa/v1/load.php');

                ipa.ready = function () {
                    document.getElementById('locationText').disabled = false;
                    document.getElementById('addressText').disabled = false;
                    ipa.addListener('location', function (location) {
                        alert(location.y + ", " + location.x);
                    });
                    SetParcelCenter();
                    //ipa.setZoomLevel(20);
                    createShapes();
                    // First setup a listener to catch the search result event
                    ipa.addListener('searchresult', function (result) {

                        if (result.success) {

                            if (result.entityType == ipa.ENTITY_TYPE.PARCEL) {

                                alert('Parcel match found:\n' + result.description);

                            } else {

                                alert('Address found:\n' + result.description);

                            }

                        } else {

                            alert('Address not found');

                        }

                    });
                };

                function SetParcelCenter() {
                    if (activeParcel.mapCenter) {
                        ipa.setLocation({
                            y: activeParcel.mapCenter.lat(),       // Latitude
                            x: activeParcel.mapCenter.lng(),      // Longitude
                            zoom: defaultZoom             // Optional Zoom level
                        });
                    }

                    return false;
                }

                //            setLocation = function setLocation() {
                //                //                var location = document.getElementById('locationText');
                //                //                var loc = location.value.split(',');
                //                //                var lat = loc[0].replace(/^\s+|\s+$/g, "");
                //                //                var lon = loc[1].replace(/^\s+|\s+$/g, "");

                //                // Alternate syntax to pass parameters
                //                ipa.setLocation(40.2339, -83.3664, 17);

                //                return false;
                //            }

                function gotoAddress() {
                    var address = document.getElementById('addressText').value;
                    ipa.gotoAddress(address);
                    return false;
                }

             function createShapes() {
                    if (activeParcel.MapPoints.length == 0)
                        return;
                    var shapes = [];
                    for (x in activeParcel.MapPoints.sort(function (x, y) { return x.Ordinal - y.Ordinal })) {
                        var p = activeParcel.MapPoints[x];
                        if (shapes[p.RecordNumber || 0] == null) {
                            shapes[p.RecordNumber || 0] = [];
                        }
                        shapes[p.RecordNumber || 0].push({ y: p.Latitude, x: p.Longitude });
                    };
                    var polygon = [];
                    shapes.forEach(function (w) {
                        polygon.push({
                            type: ipa.SHAPE_TYPE.POLYGON,
                            coordinates: w,
                            strokeColor: "#FFFF00",
                            strokeOpacity: 0.75,
                            strokeWeight: 2,
                            visible: true
                        });
                    });
                    var shapeIds = [];
                    ipa.addShapes(polygon, function (result) {

                        for (var i = 0; i < result.length; i++) {
                            if (result[i].success === false) {
                                alert(result[i].error);
                            } else {
                                shapeIds.push(result[i].shapeId);
                            }
                        }
                    });

                }

                function getLocation() {
                    ipa.getLocation();
                }

                // set the iframe src to load the IPA
                var iframe = document.getElementById(getFrameId());
                iframe.setAttribute('src', signedUrl());

            });


            window.onresize = function () {
                if (window.onafterresize)
                    window.onafterresize();
            }

            var winTop = screenTop, winLeft = screenLeft;
            window.setInterval(function () {
                if (winTop != screenTop || winLeft != screenLeft) {
                    if (window.onpositionchange)
                        window.onpositionchange();
                }

                winTop = screenTop;
                winLeft = screenLeft;
            }, 500);
        
    </script>
</body>
</html>
