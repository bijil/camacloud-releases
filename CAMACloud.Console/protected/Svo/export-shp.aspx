﻿<%@ Page Title="Georeferencing :: Export Parcels As SHP" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DesktopWeb.Master" CodeBehind="export-shp.aspx.vb" Inherits="CAMACloud.Console.export_shp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="NavBarContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div style="padding: 5px 15px;">
        <h1>Georeference: Export Shapefiles</h1>
        <asp:Button runat="server" ID="btnExport" Text="Request New Export" />
        <p>Note: The exported files automatically expire after a week. Please download the files as soon as they are created.</p>
        <asp:GridView runat="server" ID="grid">
            <Columns>
                <asp:BoundField DataField="RequestDate" HeaderText="Requested on" DataFormatString="{0:MM\/d\/yyyy hh\:mm tt}" ItemStyle-Width="150px" />
                <asp:BoundField DataField="StatusText" HeaderText="Status" ItemStyle-Width="120px" />
                <asp:BoundField DataField="Message" HeaderText="Message" ItemStyle-Width="280px" />
                <asp:HyperLinkField DataTextField="DownloadFileName" HeaderText="Download Link" Target="_blank"
                    DataNavigateUrlFields="DownloadUrl" ItemStyle-Width="300px" />
                <asp:BoundField DataField="ProcessingTime" HeaderText="Processing Time" DataFormatString="{0:hh\:mm\:ss}" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
            </Columns>
        </asp:GridView>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FootContents" runat="server">
</asp:Content>
