﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="prc.aspx.vb" Inherits="CAMACloud.Console.prc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
        <script type="text/javascript" src="../../App_Static/jslib/jquery.js"></script>
        <link rel="stylesheet" href="/App_Static/css/quality.css?<%= Now.Ticks %>" />
  <script src="https://maps.googleapis.com/maps/api/js?libraries=geometry&key=AIzaSyC7e5YrZlKqWtaY4ehsDwMgCis4IfNs7Xw"></script>
    <title>CAMA Cloud&#174; Parcel PRC</title>
    <script type="text/javascript">
        var activeParcel, activeTab = '', datafields, lookup, LookupMap, extendedLookup = [];
        var openFromPRC = false;
        var listparcels = [];
        var offlineTables;
       	var CAMACloud = {};
       	CAMACloud.Sketching = {};
       	CAMACloud.Sketching.Formatters = {};
       	CAMACloud.Sketching.Encoders ={};
        $( function (){
            if ( window.opener.activeParcel ) {
                activeParcel = window.opener.activeParcel;
                datafields = window.opener.datafields;
                lookup = window.opener.lookup;
                fieldCategories = window.opener.fieldCategories;
                LookupMap = window.opener.LookupMap;
                extendedLookup = window.opener.extendedLookup
                //   $('.digital-prc').html($('#prc-template').html());
                openFromPRC = true;
                $( '.digital-prc' ).fillTemplate( [activeParcel] );
                $( "button:contains('Mark As Complete')" ).hide();
                if( activeParcel._gridSourceTable ) {
                	var _gridSourceTable = activeParcel._gridSourceTable;
                	if( $('tbody[context='+ _gridSourceTable +']').offset() )
                		window.scrollTo(0,$('tbody[context='+ _gridSourceTable +']').offset().top-200);
                	else if( $('table[context='+ _gridSourceTable +']').offset() )
                		window.scrollTo(0,$('table[context='+ _gridSourceTable +']').offset().top-200);
                	else if( $('td[context='+ _gridSourceTable +']').offset() )
                		window.scrollTo(0,$('td[context='+ _gridSourceTable +']').offset().top-200);
                }
            }
        })
        
       $(window).on('load', function() { 
       			function $$$(url, data, callback, errorCallback) {
    					$.ajax({
        				url: url,
        				type: "POST",
        				dataType: 'json',
        				data: data,
        				success: function (data, status) {
            			if (data && data.failed) {
                			if (errorCallback) {
                    			errorCallback(data.message);
                    			console.warn(data);
                				}
                			else
                    			console.log(data);
            			} else
                			if (callback) callback(data, status);
        				}
        				, error: function (xhr, ajaxOptions, thrownError) {
            			if (errorCallback)
                			errorCallback(thrownError);
            			else
                			console.log(xhr);
        				}
    				});
				}
				
				function $qc(keyword, data, callback, errorCallback) {
    					$$$('/quality/' + keyword + '.jrq', data, callback, function (x) {
        					if (errorCallback) errorCallback(x)
        				else {
            				showMask('An error occured!', true);
            				alert(x);
        						}
    						});
						}
				
    			$qc('getofflinetables', null, function (data) {
        			offlineTables = data; });
       });
    </script>
   
      <script type="text/javascript" src="../../App_Static/jslib/underscore-min.js"></script>
      
      <script type="text/javascript" src="../../App_Static/jslib/infobubble.js"></script>
      <script type="text/javascript" src="../../App_Static/jslib/jsrepeater.js?t=1234569"></script>
      <script type="text/javascript" src="../../App_Static/js/qc/010-constants.js"></script>
      <script type="text/javascript" src="/App_Static/js/qc/000-objects.js?23"></script>
      <script type="text/javascript" src="../../App_Static/js/qc/000-utillib.js"></script>     
      <script type="text/javascript" src="../../App_Static/js/qc/010-quality.js"></script>      
      <script type="text/javascript" src="../../App_Static/js/Svo/parcel.js?6"></script>
      <script type="text/javascript" src="../../App_Static/js/qc/010-app-ui-comp.js"></script>
      <script type="text/javascript" src="../../App_Static/js/qc/010-conversions.js"></script>  
	  <cc:JavaScript ID="JavaScript1" runat="server" IncludeFolder="/App_Static/js/sketch/sketchlib/" />
	  <script type="text/javascript" src="/App_Static/js/Svo/sigmasketch.js?t=312749"></script>  
	  <style>
	  	body{
	  		margin: 0px;
		    padding: 0px;
		    font-family: Segoe UI, Arial, Helvetica, sans-serif;
		    font-size: 10pt;
	  	}
	  </style>	  
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server" AsyncPostBackTimeout="360000" />
        <div id="prc" class="digital-prc" runat="server">
    
        </div>
    </form>
</body>
</html>
