﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master" CodeBehind="autoprioritysettings.aspx.vb" Inherits="CAMACloud.Console.autoprioritysettings" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>
        Auto Priority Settings </h1>
        <p class="info">
            Auto Priority Settings are system-level options that control how and when an Auto Priority Rule takes effect. Each option that is turned on below will be available to select when configuring an Auto Priority Rule.
        </p>
    <div runat="server" id="dvAutoprioritySettings">
        <h2>
        Enable Auto Priority On Refresh</h2>
    <p class="info" style="width: auto;">
         Enabling this option will set the priority on properties automatically after a Refresh/Upsync occurs.
    </p>
    <p>
        <asp:HiddenField runat="server" ID="hdnEnableOnRefresh" />
        <div class="link-panel">
            Enable On Refresh:
            <asp:Label runat="server" ID="lblEnableOnRefresh" Text="??" Font-Bold="true" Font-Size="Larger" />&nbsp;&nbsp;&nbsp;<asp:LinkButton
                runat="server" ID="lnkToggleEnableOnrefresh" Text="Click here to toggle" Font-Bold="true" />
        </div>
    </p>
    
    <h2>
        Enable One Time 	</h2>
    <p class="info">
       This will allow you to schedule an Auto Priority rule that would be executed based on a one-time "apply" of the rule.
    </p>
    <p>
        <asp:HiddenField runat="server" ID="hdnOneTime" />
        <div class="link-panel">
             Enable On One Time:
            <asp:Label runat="server" ID="lblOneTime" Text="??" Font-Bold="true" Font-Size="Larger" />&nbsp;&nbsp;&nbsp;<asp:LinkButton
                runat="server" ID="lnkOneTime" Text="Click here to toggle" Font-Bold="true" />
        </div>
    </p>
   
    <h2>
        Enable Date Range	</h2>
    <p class="info">
       This will enable a rule to be executed during a configured time frame.
    </p>
    <p>
        <asp:HiddenField runat="server" ID="hdnDateRange" />
        <div class="link-panel">
             Enable Date Range:
            <asp:Label runat="server" ID="lblDateRange" Text="??" Font-Bold="true" Font-Size="Larger" />&nbsp;&nbsp;&nbsp;<asp:LinkButton
                runat="server" ID="lnkDateRange" Text="Click here to toggle" Font-Bold="true" />
        </div>
    </p>
    
     <h2>
        Exception Management	</h2>
    <p class="info">
       This will enable the option to set exceptions when creating an Auto Priority Rule.
    </p>
    <p>
        <asp:HiddenField runat="server" ID="hdnException" />
        <div class="link-panel">
             Enable Exception:
            <asp:Label runat="server" ID="lblException" Text="??" Font-Bold="true" Font-Size="Larger" />&nbsp;&nbsp;&nbsp;<asp:LinkButton
                runat="server" ID="lnkException" Text="Click here to toggle" Font-Bold="true" />
        </div>
        
    </p>
    </div>
</asp:Content>

