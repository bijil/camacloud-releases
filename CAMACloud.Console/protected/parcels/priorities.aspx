﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/ParcelManager.master"
    AutoEventWireup="false" Inherits="CAMACloud.Console.parcels_priorities" CodeBehind="priorities.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="Stylesheet" href="/App_Static/css/parcelmanager.css?<%=Now.Ticks%>" />
    <script type="text/javascript" src="/App_Static/js/parcelmanager.js?<%=Now.Ticks%>"></script>

    <script type="text/javascript">
        var currentLoginId = '<%= Page.User.Identity.Name %>'
        var isCreateAdhoc = false;
        var isGroupInList = false;
        var appendYesTick = [];
        var EnableNewPriorities = '<%= EnableNewPriorities %>'
        $(document).ready(function () {
            readCsv();
            isCreateAdhoc = (localStorage.getItem('isCreateAdhoc') == 'true') ? true : false;
            console.log(isCreateAdhoc);
            if (isCreateAdhoc) {
                $( "#<%=hdnIsCreateAdhoc.ClientID%>").val("1")
                $( "#<%=hdnAdhocPriority.ClientID%>").val(localStorage.getItem('PriorityType'))
                $('.trfile').hide();

            }
            else {
                $( "#<%=hdnIsCreateAdhoc.ClientID%>").val("0")
                $('.trfile').show();
            }

            $( '#<%=hdnIsCreateAdhoc.ClientID = txtFileName.ClientID %>').attr("disabled", "disabled");
            $('#btnChoose').click(function () {
                $('input[type="file"]').trigger('click');
            });
            $('input[type="file"]').click(function () {
                if ($('input[type="file"]').val() == "") {
                    $('.txtFileName').val('');
                }
            })
            $('input[type="file"]').change(function () {
                $('.txtFileName').val($('input[type="file"]').val().split('\\').pop().split('/').pop())
                $("body").css("overflow-x", "scroll");
            });

            $('.close-btn').click(function () {
                $('.popupbox').fadeOut('fast');
            });
            $('.viewlist-popup').on('dialogclose', function (event) {
                if ($(document).height() > $(window).height()) {
                    $("body").css("overflow-y", "scroll");
                    window.scrollTo(0, 0);
                }
            });

            $('input[type="checkbox"][name="adhc"]').change(function () {
                if (this.checked) {
                    var t = $('#tblNbhd input[type=text]');
                    t.each(function () {
                        if ($(this).val() != "" && isGroupInList) {
                            $(this).attr('disabled', true);
                        }
                        //else if($(this).val() == ""){
                        //    $('#btnContinue').attr('disabled',true);
                        //    }
                    });
                    var select = "";
                    var drp = $('#tblNbhd .secl');
                    $.ajax({
                        type: "POST",
                        url: "priorities.aspx/UserDetails",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var count = data.d;
                            var name = count[0].UserName;

                            for (var i = 0; i < count.length; i++) {
                                select = select + "<option value='" + count[i].LoginId + "'>" + count[i].UserName + "</option>";
                                drp.each(function () {
                                    $(this).append($('<option></option>').val(count[i].LoginId).html(count[i].UserName)
                                    )
                                });
                            }

                        }
                    });

                    $('#tblNbhd').show();
                    if (!isGroupInList) {
                        $('#txtGroupNumber0').focus();
                    }
                }
                else {
                    $('#tblNbhd').hide();
                }
            });

            $("form").submit(function (e) {
                e.preventDefault();
            });

            $('.txtFileName').attr("disabled", true);
        });

        $(document).mouseup(function (e) {
            var container = $(".popupbox");
            if (!container.is(e.target) && container.has(e.target).length === 0)
                $('.popupbox').fadeOut('slow');
        });

        function readCsv() {

            var select = "";
            document.getElementById("MainContent_MainContent_ControlFile").addEventListener('change', function () {
                showMask();
                var fileUpload = $('input[type="file"]');
                if (fileUpload.val() != '') {
                    filePath = fileUpload.val();
                }
                if (filePath == '') {
                    alert('Please choose a .csv file');
                    return false;
                }
                if (filePath.split('.').pop().toLowerCase() != 'csv') {
                    alert("You have uploaded an invalid file. Only'.csv' formats are allowed.");
                    location.reload(true);
                    return false;
                }
                var csvFileName = filePath.split(/(\\|\/)/g).pop().replace(".csv", "");
                $('#chkadhc').prop('checked', false);
                $('#tblNbhd').remove();
                localStorage.removeItem("PrioTableId");
                var reader = new FileReader();
                reader.onload = function (e) {
                    var text = e.target.result.replace(/\r/g, "\n");
                    var re = new RegExp("\n(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                    var row = text.split(re);
                    var keyText = $('#MainContent_MainContent_pHaveCK').text().trim();
                    var keyarray = keyText.split(",");
                    var keys = keyarray.length;
                    var headerCell = row[0].split(new RegExp(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)"));
                    headerCell = headerCell.map(function (x) { return x.trim().toUpperCase() });
                    console.log(headerCell);
                    console.log(keyarray);
                    var gn = false, emptyHeader = false;
                    headerCell.forEach(function (value) {
                        if (value == "") emptyHeader = true;
                        if (value.includes("GROUPNAME")) {
                            gn = true;
                            if (value != "GROUPNAME") {
                                alert('Your list could not be uploaded because the GROUPNAME column header is not correct \n\n Please revise your list and upload again.');
                                location.reload(true);
                                return false;
                            }
                        }
                    });
                    isGroupInList = false;
                    var missingColumns = [];
                    for (var i = 0; i < keys; i++) {
                        if (!headerCell.includes(keyarray[i].trim().toUpperCase())) {
                            missingColumns.push(keyarray[i]);
                        }

                    }
                    if (missingColumns.length > 1) {
                        alert("Your list could not be uploaded because it does not contain '" + missingColumns.toString().trim() + "' as columns.\n\n Please revise your list and upload again.");
                        location.reload(true);
                        return false;
                    }
                    else if (missingColumns.length == 1) {
                        alert("Your list could not be uploaded because it does not contain '" + missingColumns.toString().trim() + "' as a column.\n\n Please revise your list and upload again.");
                        location.reload(true);
                        return false;
                    }

                    if (gn) { keyarray.push("GROUPNAME"); keys = keys + 1; }
                    for (var i = 0; i < keys; i++) {
                        if (keyarray[i].trim().toUpperCase() != headerCell[i].toUpperCase()) {
                            alert("Your list could not be uploaded because its fields are not in a correct order.\n\n Please revise your list and upload again.");
                            location.reload(true);
                            return false;
                        }
                    }

                    if (gn) { keyarray.pop(); keys = keys - 1; }
                    if (emptyHeader) {
                        alert("You have uploaded an invalid list.\n\n Please revise your list and upload again.");
                        location.reload(true);
                        return false;
                    }

                    var count = 0;
                    var grpCode = [];
                    var msgCount = 0;
                    console.log('keylength' + keys);
                    var r = new RegExp(/"((?:""|[^"])*)"/);
                    var format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
                    for (var i = 0; i < row.length; i++) {
                        var cells = row[i].split(new RegExp(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)"));
                        if (cells.length > 1) {
                            var priorityCell = cells[1];
                            if (cells.length == (keys + 1) && i > 0) {
                                grpCode.push(cells[keys].trim());
                            }
                        }
                        //console.log(cells);
                    }
                    if (grpCode.length == 0) {
                        grpCode.push("");
                    }
                    var group = unique(grpCode);
                    if (group.length > 10) {
                        alert("List contains more than 10 groups.Maximum allowed limit is 10.");
                        location.reload(true);
                        return false;
                    }
                    group.forEach(function (x) {
                        if (x.indexOf('@!*!*$#@') > -1) {
                            alert("Group name contains reserved character '@!*!*$#@'.");
                            location.reload(true);
                            return false;
                        }
                        if (x.indexOf('>') > -1 || x.indexOf('<') > -1) {
                            alert("Group name contains special character '>','<'.");
                            location.reload(true);
                            return false;
                        }

                    });

                    if (group.length > 0) {

                        var table = "<table id ='tblNbhd' style='display: none;'>";
                        if (group[0] != "") {
                            isGroupInList = true;

                            for (var i = 0; i < group.length; i++) {
                                table = table + "<tr>"
                                    + "<td><td>Group Code/Number:</td>"
                                    + "<td>"
                                    + "<input type ='text' id ='txtGroupNumber" + i + "' autocomplete='off' CssClass='txt-adhoc-group-name'  MaxLength ='50' value = '" + group[i].replace(/['"]+/g, '') + "'/>"
                                    + "</td>"
                                    + "<td style='padding-left: 25px;'>Assign group to:</td>"
                                    + "<td>"
                                    + "<select class ='secl' style='width:160px' id ='drpAssgn" + i + "'><option value=''>None, will assign later</option>"
                                    + select
                                    + "</select>"
                                    + "</td>"
                                    + "</tr>"
                            }
                            table = table + "</table>"
                        }
                        else {
                            table = table + "<tr>"
                                + "<td><td>Group Code/Number:</td>"
                                + "<td>"
                                + "<input type ='text' id ='txtGroupNumber0' autocomplete='off' CssClass='txt-adhoc-group-name'  MaxLength ='50' value = '" + csvFileName + "'/>"
                                + "</td>"
                                + "<td style='padding-left: 25px;'>Assign group to:</td>"
                                + "<td>"
                                + "<select class ='secl' style='width:160px' id ='drpAssgn0'><option value=''>None, will assign later</option>"
                                + select
                                + "</select>"
                                + "</td>"
                                + "</tr>"
                                + "</table>"
                        }
                    }


                    $('#chkadhc').prop('checked', false);
                    $('#tblNbhd').remove();
                    $('#Nbhdiv').append(table);
                    var list = [];
                    list.push(row);
                    console.log(list);
                    hideMask();
                }
                if (document.getElementById("MainContent_MainContent_ControlFile").files[0]) {
                    reader.readAsText(document.getElementById("MainContent_MainContent_ControlFile").files[0]);
                    reomveLocalStorage();
                }
                else {
                    $('#tblNbhd').remove();
                    hideMask();
                }


            });

        }

        function unique(array) {
            return $.grep(array, function (el, index) {
                return index == $.inArray(el, array);
            });
        }

        function verifyPriorityList(multiGroup) {
            var fileUpload = $('input[type="file"]');
            var filePath = ''
            var adhocgroupname = $('.txt-adhoc-group-name').val();
            if ($('.txt-adhoc-group-name').attr('dType') == 'int') {
                if (isNaN(Number(adhocgroupname))) {
                    alert('Your Ad-Hoc Assignment Group name should be a number.')
                    return false;
                }
            }
            var charCheck = ["{", "}", "(", ")", "'", '"'];
            if (charCheck.some(function (x) { return adhocgroupname.indexOf(x) > -1 })) {
                alert('Please avoid invalid characters from Ad-Hoc Assignment Group name.');
                return false;
            }
            if (fileUpload.val() != '') {
                $( '#<%= hdnFilePath.ClientID%>').val(fileUpload.val());
                filePath = fileUpload.val();
            }
            if (fileUpload.val() == '' && $( '#<%= hdnFilePath.ClientID%>').val() != '') {
                filePath = $( '#<%= hdnFilePath.ClientID%>').val();
            }
            if (filePath == '') {
                alert('Please choose a .csv file');
                return false;
            }
            if (filePath.split('.').pop().toLowerCase() != 'csv') {
                alert("You have uploaded an invalid file. Only'.csv' formats are allowed.");
                return false;
            }
            var fileSize = ''
            if (fileUpload[0].files.length > 0) {
                $( '#<%=  hdnFileSize.ClientID%>').val(fileUpload[0].files[0].size)
                fileSize = fileUpload[0].files[0].size;
            }

            if (fileUpload[0].files.length == 0 && $( '#<%=  hdnFileSize.ClientID%>').val() != '')
                fileSize = $( '#<%=  hdnFileSize.ClientID%>').val();

            if (filePath && filePath != '' && fileSize && fileSize != '' && parseInt(fileSize) > 4194304) {
                alert('File too big ! The maximum size of uploaded file should be 4MB.');
                return false;
            }
            if (($('.chkCreateNbhd input').attr('checked') == 'checked') || ($('.chkCreateMultiNbhd input').attr('checked') == 'checked')) {
                $( '#<%= hdnAdHoc.ClientID%>').val('1');
                var adhocgroupname = $('.txt-adhoc-group-name').val();
                if (adhocgroupname == '' && !($('.chkCreateMultiNbhd input').attr('checked') == 'checked')) {
                    alert('Please enter a unique code/number for the ad-hoc assignment group.');
                    return false;
                }
            }
            else
                $( '#<%= hdnAdHoc.ClientID%>').val('0');
            window.scrollTo(0, 0);
            $("body").css("overflow", "hidden");
            showMask();
            return true;
        }



        function verifyGroupNumber() {
            var groupname = $('.newGroupNumber').val();
            if (groupname == '') {
                alert('Please enter a unique code/number for the ad-hoc assignment group.');

                return false;
            }
            else
                return true;
        }
        function viewList(that) {
            if (!verifyGroupNumber()) return false;
            window.scrollTo(0, 0);
            $("body").css({ "overflow": "hidden" });
            showMask();
            var Name = $(that).attr("Name");
            var Keyfields = ""
            if (Name != 'Assigned')
                //Keyfields = $( that ).attr( "keyfields" );
                var popTitle = "";
            var prioTId = localStorage.getItem("PrioTableId");
            switch (Name) {
                case "Invalid":
                    popTitle = "Invalid Parcels";
                    break;
                case "Duplicated":
                    popTitle = "Duplicated Parcels";
                    break;
                case "NotApproved":
                    popTitle = "Parcels not yet Approved";
                    break;
                case "NotSyncedBack":
                    popTitle = $(that).attr("title");
                    break;
                case "Assigned":
                    popTitle = "Assigned Parcels Details";
                    break;
                case "WorkedInFieldLast30days":
                    popTitle = "Parcels Worked in the Field in the Last 30 Days";
                    break;
            }
            $par('viewlist', { Name: Name, prioTId: prioTId }, function (data) {
                if (data && data.length > 0)
                    fillTable(data, popTitle)
                else
                    alert('No Parcels Found.')
                hideMask();
                $("body").css({ "overflow": "hidden" });
            });
            //$( "body" ).css( "overflow-y", "scroll" );
            return false;
        }
        function groupNumberValidation(that) {
            var len = $(that).val().length;
            var limit = parseInt($(that).attr('maxlength'));
            if (len >= limit && limit > 50)
                alert('Your Ad-Hoc Assignment Group name exceeds the length allowed {' + limit + ' characters}.\n\n Please shorten the name to be 50 characters or less.');
            else if (len >= limit && limit <= 50)
                alert('Your Ad-Hoc Assignment Group name exceeds the length allowed {' + limit + ' characters}.\n\n Please shorten the name to be ' + limit + ' characters or less.');
        }


        function fillTable(data, popTitle) {
            var columns = Object.keys(data[0]).filter(function (d) { return d != 'NbhdId' })
            var header = '', trow = '';
            columns.forEach(function (c) {
                header += '<td ">' + c + '</td>'
            });

            if (popTitle == 'Assigned Parcels Details') {
                header += '<td>Remove from List</td>';
                header += '<td>View in QC</td>';
            }
            header = '<thead><tr>' + header + '</tr></thead>';
            data.forEach(function (d) {
                trow += '<tr>'
                columns.forEach(function (c) {
                    var val = ''
                    if (d[c] === null) { val = '' } else { val = d[c] }
                    trow += '<td >' + val + '</td>'

                });
                if (popTitle == 'Assigned Parcels Details') {
                    trow += '<td style="text-align:center;"><input type="checkbox" class="remove" NbhdId="' + d["NbhdId"] + '"></td><td style="text-align:center;"><input type="checkbox"class="qc" NbhdId="' + d["NbhdId"] + '"></td>'
                }
                trow += '</tr>'
            });

            showViewListPopup(header + trow, popTitle);

        }

        function showViewListPopup(tbody, popTitle) {
            $('#gvResult').html('');
            window.scrollTo(0, 0);
            $("body").css("overflow", "hidden");
            $('.viewlist-popup').dialog('destroy');
            $('.viewlist-popup').dialog({
                modal: true,
                width: 'auto',
                minHeight: "auto",
                height: "auto",
                resizable: false,
                title: popTitle,
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                    $('#gvResult').append(tbody);
                    if (popTitle == 'Assigned Parcels Details') {
                        if ($("#gvResult input[type=checkbox]:checked").length > 0)
                            $("#btnOkay").prop("disabled", false);
                        else
                            $("#btnOkay").prop("disabled", true);
                        $('#gvResult input[type=checkbox]').click(function () {
                            if ($("#gvResult input[type=checkbox]:checked").length > 0)
                                $("#btnOkay").prop("disabled", false);
                            else
                                $("#btnOkay").prop("disabled", true);
                            if (this.checked) {
                                var clas = $('#gvResult input[type=checkbox]:checked').attr("class");
                                (clas == "qc" ? $('#gvResult input[type=checkbox].remove').attr("disabled", true) : $('#gvResult input[type=checkbox].qc').attr("disabled", true));
                            }
                            else {
                                //$( '#gvResult input[type=checkbox]').removeAttr( "disabled" );
                                //$( '#gvResult input[type=checkbox]').removeAttr( 'checked' );
                                $(this).removeAttr('checked');
                                var cls = $(this).attr("class");
                                console.log(cls);
                                if (cls == "qc") {
                                    if ($('#gvResult input[type=checkbox]:checked.qc').length == 0)
                                        $('#gvResult input[type=checkbox].remove').removeAttr("disabled");

                                }
                                else {
                                    if ($('#gvResult input[type=checkbox]:checked.remove').length == 0)
                                        $('#gvResult input[type=checkbox].qc').removeAttr("disabled");
                                }
                            }

                        });
                        $('.btnContainer').css("display", "inline-block");

                    }
                    else {
                        $('.btnContainer').css("display", "none");
                    }

                },
                close: function () {

                    $("body").css("overflow", "auto");
                    window.scrollTo(0, 0);

                }
            });

            $('.viewlist-popup').dialog('open');
            $('.viewlist-popup').dialog('option', 'position', 'center');
            hideMask();
        }


        function moreHeaderInfo(source) {
            var top;
            var left;
            var pos = [];
            var sourceTop = $(source).offset().top;
            var sourceLeft = $(source).offset().left;
            $('.header-info-content').html('')
            left = sourceLeft - 255;
            top = sourceTop + 21;
            pos[0] = top;
            pos[1] = left;
            var header = $(source).attr("keyfields");
            header = header.split(',')
            var hrow = '', frow = '', srow = '', trow = '';
            var a = '@'
            header.forEach(function (h) {
                if (h == "GROUPNAME") return false;
                frow += '<td>' + h + '</td>';

                if (h == '1') {
                    hrow += '<td style="background:#dfdfdf;"></td>';
                    srow += '<td>2</td>';
                    trow += '<td>3</td>';
                }
                else {
                    a = String.fromCharCode(a.charCodeAt() + 1)
                    hrow += '<td style="background: #dfdfdf;">' + a + '</td>';
                    srow += '<td></td>';
                    trow += '<td></td>';
                }
            })
            var table = '<table class="table-header-info"><thead><tr>' + hrow + '</tr></thead><tbody><tr>' + frow + '</tr><tr>' + srow + '</tr><tr>' + trow + '</tr></tbody></table>'
            $('.header-info-content').append(table)
            $('.header-info-content').css("width", "auto");
            $('.header-info').css("display", "inline-block");
            $('.header-info').css({ 'top': pos[0] + 'px', 'left': pos[1] + 'px' });
            return false;
        }

        function moreInfo() {
            $('.file-info').css("display", "inline-block");
            return false;
        }

        function procProgress() {
            $('#progressBar').show();
            var progress = 1;
            var id = setInterval(frame, 7);
            function frame() {
                if (progress >= 95) {
                    clearInterval(id);
                } else {
                    progress++;
                    $('#currentProgress').width(progress + '%');
                }
            }
        }
        function ProceedToUpdate() {
            if (!verifyGroupNumber()) return false;
            if (confirm("Are you sure you want to proceed?")) {
                $('.btnBack').prop("disabled", true);
                $('.btnStep2Close').prop("disabled", true);
                window.scrollTo(0, 0);
                //$( "body" ).css( "overflow", "hidden" );
                showMask();
                procProgress();
                return true;
            }
            else
                $('.btnBack').prop("disabled", false);
            $('.btnStep2Close').prop("disabled", false);
            return false;
        }


        function closeViewList() {
            $('.viewlist-popup').dialog('destroy');
            $("body").css("overflow-y", "scroll");
            window.scrollTo(0, 0);
            return false;
        }


        function reomveLocalStorage(callback) {
            localStorage.removeItem('priorityFileName');
            localStorage.removeItem('isDoNotReset');
            //localStorage.removeItem('isCreateAdhoc');
            localStorage.removeItem('isMultiAdhoc');
            localStorage.removeItem('groupCode');
            localStorage.removeItem('assignTo');
            localStorage.removeItem('isAppend');
            //localStorage.removeItem( 'backupId' );

            localStorage.removeItem("PrioTableId");
            localStorage.removeItem("backUpId");
            localStorage.removeItem("Duplicated");
            localStorage.removeItem("High");
            localStorage.removeItem("Invalid");
            localStorage.removeItem("Normal");
            localStorage.removeItem("Urgent");
            localStorage.removeItem("Medium");
            localStorage.removeItem("Critical");
            localStorage.removeItem("Proximity");
            localStorage.removeItem("TotalParcel");
            localStorage.removeItem("isNotApproved");
            localStorage.removeItem("isAssigned");
            localStorage.removeItem("isNotSyncedBack");
            localStorage.removeItem("isWorkedInFieldLast30days");
            localStorage.removeItem("AdhcAssgn");
            localStorage.removeItem("CAMASystem");
            localStorage.removeItem("isDonotReset");
            localStorage.removeItem("fielName");
            localStorage.removeItem("noPriority");
            localStorage.removeItem("isAdho");
            if (callback) callback();
        }
        function reoveIsCreateAdhoc() {
            localStorage.removeItem('isCreateAdhoc');
            localStorage.removeItem('createAdhocFrom');
        }
        $('.chkCreateNbhd').change(function () { if ($(this).prop("checked")) { } })
        function hideMultiCheck() {
            if ($('.chkCreateNbhd input').prop('checked')) {
                $('.tblmultiNbhdDetails').hide();
                $( '#<%= hdnmultinbhdprocess.ClientID%>').val("0");
            $( '#<%= hdnIsMultipleAdhocs.ClientID%>').val("0");
            $( '#<%= hdnAdHoc.ClientID%>').val("1");
                $('.chkCreateMultiNbhd input').prop("checked", false)
            }
        }
        function PriorityTypeChange() {
            var dialog = $('.alterPopup').dialog({
                modal: true,
                height: 'auto',
                width: 'auto',
                top: '120.5px !important',
                resizable: false,
                closeOnEscape: false,
                title: "Priority List Missing Information!",
                open: function (event, ui) {
                    //$(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
                    //$(".ui-dialog-titlebar").hide();
                    //$('[id$=btnContinue]').attr('disabled', 'disabled');
                },
                close: function () {
                    hideMask();
                    $("body").css("overflow", "scroll");
                    window.scrollTo(0, 0);

                }
            });
            dialog.parent().appendTo(jQuery('form:first')); //Setting the dialog containing Div to the start of the Form tag
        }

        function CheckBoxCheck(rb) {
            var gv = $(".alterPopup");
            var chk = $('input[type="checkbox"]', gv);
            $(chk).removeAttr('checked');
            $(rb).attr('checked', 'checked');
        }
        function ProceedToSecondStep() {
            var gv = $(".alterPopup");
            var chk = $('input[type="checkbox"][checked="checked"]', gv);
            if (chk.length == 0) {
                alert('Please select a check box.')
                return false;
            }
            var priority = chk.val();
            $('.alterPopup').dialog('close');
            window.scrollTo(0, 0);
            $("body").css({ "overflow": "hidden" });
            showMask();
            var fileUpload = $('input[type="file"]');
            //localStorage.removeItem("fileName");
            //localStorage.setItem("fileName",fileUpload.val());
            var grpnameWithAssn = "";
            var chk = document.getElementById("chkadhc");
            if (chk.checked) {
                var text = "";
                var table = document.getElementById('tblNbhd')
                var rowlength = table.rows.length
                for (var i = 0; i < rowlength; i++) {
                    var cells = table.rows.item(i);
                    var t = $('input[type="text"]', cells);
                    var s = $('select', cells);
                    var strUser = s.val();
                    if (grpnameWithAssn != "") {
                        grpnameWithAssn = grpnameWithAssn + ","
                    }
                    grpnameWithAssn = grpnameWithAssn + t.val();
                    grpnameWithAssn = grpnameWithAssn + '@!*!*$#@' + strUser


                }

            }

            $('input[type="checkbox"]', gv).prop("checked", false);
            verifyListForSecondSteps((priority == "High") ? "1" : "2", grpnameWithAssn)
        }
        function verifyListForSecondSteps(priority, grpnameWithAssn) {

            var prioId = localStorage.getItem("PrioTableId")
            console.log("second step loading..")
            $par('priority', { priority: priority, prioId: prioId, grpwithAss: grpnameWithAssn, adhoc: "false", reload: "false", selectedNbhd: "" }, function (res) {
                var t = {};
                t = JSON.parse(JSON.stringify(res));
                console.log(t);
                secondStepCreator(t);
            }, function (res) {
                alert(JSON.parse(JSON.stringify(res)));
                hideMask();
            });
        }
        function CancelSecondStep() {
            $('.alterPopup').dialog('close');
            var gv = $(".alterPopup");
            $('input[type="checkbox"]', gv).prop("checked", false);
            location.reload(true);
        }
        function verifyListAndLoadPrioTable() {
            $('#btnContinue').attr('disabled', 'disabled')
            $('.popupbox').css('display', 'none');
            $(":focus").blur();
            if (!$('.step1').is(":visible")) return false;
            showMask();
            var fileUpload = $('input[type="file"]');
            var filePath = $('input[type="file"]').val();
            if (fileUpload.val() == '' && $( '#<%= hdnFilePath.ClientID%>').val() != '') {
                filePath = $( '#<%= hdnFilePath.ClientID%>').val();
            }
            if (filePath == '') {
                setTimeout(function () { alert('Please choose a .csv file'); }, 0);
                $("#btnContinue").prop("disabled", false);
                hideMask();
                return false;
            }
            if (filePath.split('.').pop().toLowerCase() != 'csv') {
                alert("You have uploaded an invalid file. Only'.csv' formats are allowed.");
                hideMask();
                return false;
            }
            var fileSize = ''
            if (fileUpload[0].files.length > 0) {

                fileSize = fileUpload[0].files[0].size;
            }
            if (filePath && filePath != '' && fileSize && fileSize != '' && parseInt(fileSize) > 4194304) {
                alert('File too big ! The maximum size of uploaded file should be 4MB.');
                hideMask();
                return false;
            }
            localStorage.removeItem("fileName");
            localStorage.setItem("isCreateAdhoc", false);
            localStorage.setItem("fileName", MainContent_MainContent_txtFileName.value);
            localStorage.removeItem("appendYes")
            localStorage.setItem('isDoNotReset', ($('.chkDoNotReset input').attr('checked') == 'checked') ? true : false);
            appendYesTick = [];

            var grpnameWithAssn = "";
            var chk = document.getElementById("chkadhc");
            if ((chk.checked) && ($('#tblNbhd').length == 0)) {
                $('#chkadhc').prop('checked', false);
                $('.txtFileName').val('');
                $("#btnContinue").prop("disabled", false);
                alert("Please upload .csv file again.");
                hideMask();
                return false;
            }

            if (chk.checked) {
                var text = "";
                var table = document.getElementById('tblNbhd')
                localStorage.setItem("CreateNbhd", true);
                var rowlength = table.rows.length
                for (var i = 0; i < rowlength; i++) {
                    var cells = table.rows.item(i);
                    var t = $('input[type="text"]', cells);
                    if (t.val() == "") {
                        alert('Group name/code can not be blank.');
                        $("#btnContinue").prop("disabled", false);
                        hideMask();
                        return false;
                    }
                    if (t.val().indexOf('<') > -1 || t.val().indexOf('>') > -1) {
                        alert("Special characters '>' and '<' can not be used as a Group Name.")
                        $("#btnContinue").prop("disabled", false);
                        hideMask();
                        return false;
                    }
                    if (t.val().indexOf(',') > -1 || t.val().indexOf('|') > -1 || t.val().indexOf('{') > -1 || t.val().indexOf('?') > -1 || t.val().indexOf('}') > -1 || t.val().indexOf('(') > -1 || t.val().indexOf(')') > -1 || t.val().indexOf('"') > -1 || t.val().indexOf("'") > -1) {
                        alert("Please avoid invalid characters from Ad-Hoc Assignment Group name.")
                        $("#btnContinue").prop("disabled", false);
                        hideMask();
                        return false;

                    }
                    var s = $('select', cells);
                    var e = document.getElementById("drpAssgn" + i);
                    var strUser = e.options[e.selectedIndex].value;
                    //var strUser = s.val();
                    if (grpnameWithAssn != "") {
                        grpnameWithAssn = grpnameWithAssn + ","
                    }
                    grpnameWithAssn = grpnameWithAssn + t.val();
                    grpnameWithAssn = grpnameWithAssn + '@!*!*$#@' + strUser


                }

            }
            else {
                localStorage.setItem("CreateNbhd", false);
            }
            var keyField = [];
            var count = 0;
            var grpCode = [];
            var msgCount = 0;
            if (fileUpload.val() != "") {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var text = e.target.result.replace(/\r/g, "\n");
                    var row = e.target.result.split("");
                    var keyText = $('#MainContent_MainContent_pHaveCK').text().trim();
                    var keyarray = keyText.split(",");

                    var prioupload = function (lists) {
                        $par('prioupload', { List: lists, grpwithAss: grpnameWithAssn, user: currentLoginId }, function (resp) {
                            var t = {};
                            console.log(resp)
                            t = JSON.parse(JSON.stringify(resp));
                            console.log(t)
                            localStorage.removeItem("PrioTableId");
                            localStorage.setItem("PrioTableId", t.prioId);
                            var noPriority = t.noPriority;
                            if (t.longMsg == 'True' && t.longMsgKey != "null") {
                                alert('The value in the MESSAGE column for ' + keyarray[0].trim() + ' ' + t.longMsgKey + ' is too long.\n Please enter a value of 500 characters or less.');
                                location.reload(true);
                                return false;
                            }
                            if (t.longMsg == 'True' && t.longMsgKey == "null") {
                                alert('There are multiple values in the MESSAGE column that exceed 500 characters.\n Please enter values of 500 characters or less');
                                location.reload(true);
                                return false;
                            }
                            localStorage.removeItem("noPriority");
                            localStorage.setItem("noPriority", parseInt(noPriority));
                            if (parseInt(noPriority) > 0) {
                                PriorityTypeChange(grpnameWithAssn);
                            }
                            else {
                                verifyListForSecondSteps("", grpnameWithAssn);
                            }
                            takePriorityBackup(t.prioId, false, currentLoginId);
                        }, function (res) {
                            var data = JSON.parse(JSON.stringify(res));
                            alert(data);
                            location.reload(true);
                            hideMask();
                        });

                    }
                    if (text.indexOf('<') > -1 || text.indexOf('>') > -1) {
                        if (confirm("Your list contains special characters '<' and '>' that are not accepted in the Message column. Select 'Ok' to have these special characters removed and to proceed with the upload. Otherwise, select 'Cancel'.")) {
                            var firstReplace = text.replace(/</g, '');
                            var secondReplace = firstReplace.replace(/>/g, '');
                            prioupload(secondReplace);
                        }
                        else {
                            location.reload(true);
                            return false;
                        }
                    }
                    else {
                        prioupload(text);
                    }
                }
                reader.readAsText(document.getElementById("MainContent_MainContent_ControlFile").files[0]);
            }
            else if (localStorage.getItem("PrioTableId") != null) {
                var noPriorityParcel = localStorage.getItem("noPriority");
                if (parseInt(noPriorityParcel) > 0) {
                    PriorityTypeChange(grpnameWithAssn);
                }
                else {

                    verifyListForSecondSteps("", grpnameWithAssn);
                }

            }

            return false;
        }
        function UploadListConfirmation() {
            if (confirm("Are you sure you want to proceed?")) {
                $('.btnBack').prop("disabled", true);
                $('.btnStep2Close').prop("disabled", true);
                //window.scrollTo(0, 0);
                $("body").css("overflow", "hidden");
                showMask();
                UploadFile();
                procProgress();
                return false;
            }
            else {
                $('.btnBack').prop("disabled", false);
                $('.btnStep2Close').prop("disabled", false);
                return false;
            };

        }
        function UploadFile() {
            var removeApprovedParcels;
            var removeNotSyncedBackParcels;
            var removeAssignedParcels;
            var removeWorkedInFieldLast30daysParcels;
            var CreateNbhd = localStorage.getItem("CreateNbhd");
            var DonotReset;
            var grpString = "";
            if (localStorage.getItem('isCreateAdhoc') == null)
                localStorage.getItem("fileName") != null ? localStorage.setItem('isCreateAdhoc', 'false') : localStorage.setItem('isCreateAdhoc', 'true');
            var createAdh = localStorage.getItem("isCreateAdhoc");
            var priorityType = "0";
            removeApprovedParcels = ($("#chkNotApprovedparcel").attr('checked') == 'checked') ? true : false;
            removeNotSyncedBackParcels = ($("#chkNotSyncedparcel").attr('checked') == 'checked') ? true : false;
            removeAssignedParcels = ($("#chkAssignedParcel").attr('checked') == 'checked') ? true : false;
            removeWorkedInFieldLast30daysParcels = ($("#chkWorkedInFieldLast30days").attr('checked') == 'checked') ? true : false;
            var chkadhc = $("#chkadhc");
            if (createAdh == "true" || CreateNbhd == "true")
                chkadhc.prop("checked", true);
            if (chkadhc && chkadhc.prop("checked") == true) {
                CreateNbhd = true;
                var divCollection = $("#appendableDiv").children('div .tableVerify')
                //var rowlength = table.rows.length
                grpString = "";
                divCollection.each(function (index, item) {
                    var l = $(this).$('.leftSide').val();
                    var r = $(this).$('.rightSide').val();
                    if (grpString != "") {
                        grpString = grpString + ","
                    }
                    grpString = grpString + l + "@!*!*$#@" + r;
                });

            }
            else {
                CreateNbhd = false;
            }
            DonotReset = localStorage.getItem('isDonotReset') == '1' ? "1" : "0";  //($("#<%= chkDoNotReset.ClientID%>").attr("checked") == "checked") ? "1" : "0";
            var prioId = localStorage.getItem("PrioTableId");
            var backupId = localStorage.getItem("backupId");
            var Duplicated = localStorage.getItem("Duplicated");
            var High = localStorage.getItem("High");
            var Invalid = localStorage.getItem("Invalid");
            var Normal = localStorage.getItem("Normal");
            var Urgent = localStorage.getItem("Urgent");
            var Medium = EnableNewPriorities == 'True'? localStorage.getItem("Medium"): 0;
            var Critical = EnableNewPriorities == 'True' ? localStorage.getItem("Critical"): 0;
            var Proximity = EnableNewPriorities == 'True' ? localStorage.getItem("Proximity"): 0;
            var TotalParcel = localStorage.getItem("TotalParcel");
            let updateBpp = $('.bppUpdate').is(':visible') ? $('input[name="bppGroup"]:checked').val() : '0';
           //$par('priorityupload', { prioId: prioId, CreateNbhd: CreateNbhd, DonotReset: DonotReset, appendGrp: "", backupId: backupId, grpString: grpString, createAdh: createAdh, priorityType: priorityType, removeApprovedParcels: removeApprovedParcels, removeNotSyncedBackParcels: removeNotSyncedBackParcels, removeAssignedParcels: removeAssignedParcels, removeWorkedInFieldLast30daysParcels: removeWorkedInFieldLast30daysParcels, Duplicated: Duplicated, High: High, Invalid: Invalid, Normal: Normal, Urgent: Urgent, TotalParcel: TotalParcel, updateBpp: updateBpp}, function (res) {
             $par('priorityupload', { prioId: prioId, CreateNbhd: CreateNbhd, DonotReset: DonotReset, appendGrp: "", backupId: backupId, grpString: grpString, createAdh: createAdh, priorityType: priorityType, removeApprovedParcels: removeApprovedParcels, removeNotSyncedBackParcels: removeNotSyncedBackParcels, removeAssignedParcels: removeAssignedParcels, removeWorkedInFieldLast30daysParcels: removeWorkedInFieldLast30daysParcels, Duplicated: Duplicated, High: High, Invalid: Invalid, Normal: Normal, Urgent: Urgent, TotalParcel: TotalParcel, updateBpp: updateBpp, Medium: Medium, Critical: Critical, Proximity: Proximity }, function (res) {
                var t = {};
                t = JSON.parse(JSON.stringify(res));
                var multiple = t.multiGrp;
                var result = t.Result;
                console.log(t);

                $("#lblTotalAffectedparcelCount").html(t.AffectedParcelCount);
                 $("#lblTotalAffectedUrgentCount").html(t.AffectedUrgentCount);
                 $("#lblTotalAffectedHighCount").html(t.AffectedHighCount);
                $("#lblTotalAffectedNormalCount").html(t.AffectedNormalCount);
                 if (EnableNewPriorities == 'True') {
                    $("#lblTotalAffectedCriticalCount").html(t.AffectedCriticalCount);
                    $("#lblTotalAffectedMediumCount").html(t.AffectedMediumCount);
                    $("#lblTotalAffectedProximityCount").html(t.AffectedProximityCount);
                }

                if (multiple == "True") {
                    var table = "<table class ='scroll'>"
                        + "<thead>"
                        + "<td>Group Name</td>"
                        + "<td>Assigned To</td>"
                        + "<td>Numer Of Parcels</td>"
                        + "</thead>"
                    for (var i = 0; i < result.length; i++) {
                        var r = result[i].split(":");
                        table = table + "<tr>"
                            + "<td>" + r[0] + "</td>"
                            + "<td>" + r[1] + "</td>"
                            + "<td>" + r[2] + "</td>"
                            + "</tr>"

                    }
                    table = table + "</table>"
                    $("#grpdetails").append(table);
                    $("#grpdetails").show();
                }
                else {
                    $("#lblSuccess").html('<strong>' + t.resultString + '</strong>');
                }
                $("#SencondStep").hide();
                $("#thirdStep").show();
                reomveLocalStorage();
                $("body").css("overflow-y", "scroll");
                var tempwidth = 0;
                var currWidth;
                $('.scroll tr td:first-child').css("width", "max-content");
                $('.scroll tr td:first-child').each(function () {
                    currWidth = $(this).width();
                    if (currWidth > tempwidth) tempwidth = currWidth;
                });
                $('.scroll tr td:first-child').each(function () {
                    $(this).css('width', (tempwidth + 3) + "px");
                });
                hideMask();

            }, function (res) {
                var data = JSON.parse(JSON.stringify(res));
                alert(data);
                hideMask();
            });

        }
        function CloseThirdStep() {
            $par("updateclient", null, function (res) {
                if (localStorage.getItem('createAdhocFrom') == 'atcMapView') {
                    localStorage.removeItem('isCreateAdhoc');
                    localStorage.removeItem('createAdhocFrom');
                    let params = { Name: 'Search' };
                    let queryString = jQuery.param(params, true);
                    url = window.location.href.split('protected/')[0] + 'protected/parcels/atcmapview.aspx?' + queryString;
                    window.location.href = url;
                    return false;
                }
                else if (localStorage.getItem('isCreateAdhoc') == "true") {
                    localStorage.removeItem('isCreateAdhoc');
                    url = window.location.href.split('protected/')[0] + 'protected/quality/Default.aspx'
                    window.location.href = url;
                    return false;
                }
                else {
                    localStorage.removeItem('isCreateAdhoc');
                    url = window.location.href.split('protected/')[0] + 'protected/parcels/priorities.aspx'
                    window.location.href = url;
                    return false;
                }

            }, function (re) {
                var data = JSON.parse(JSON.stringify(res));
                alert(data);
                hideMask();
            });

        }
        function editGrpCode() {
            $('input[type="radio"]').click(function () {
                if ($(this).is(':checked')) {
                    if ($(this).val() == "0") {
                        var id = $(this).attr("id");
                        var identifier = id.substring(7);
                        $("#txtgrcode" + identifier).prop('disabled', false);
                        $('#btnContinue').attr('disabled', 'disabled');
                        $('#imgdone' + identifier).attr('src', '/App_Static/css/icon16/exclamation.png');
                        groupValidation()
                    }
                    else {
                        var id = $(this).attr("id");
                        var identifier = id.substring(8);
                        appendYesTick.push(identifier);
                        $("#txtgrcode" + identifier).prop('disabled', 'disabled');
                        $('#adhgroupExists' + identifier).hide();
                        $('#imgdone' + identifier).attr('src', '/App_Static/css/icon16/done.png')
                    }

                }
            });
        }
        function groupValidation() {
            var oldName = '';
            $(".newGroupNumber").click(function () {
                oldName = $(this).attr("orginalvalue");
            })
            $(".newGroupNumber").focusout(function () {
                var id = $(this).attr("id");
                var identifier = id.substring(9);
                var adhocgroupname = $(this).val();
                $par('verifyahg', { name: adhocgroupname, prioId: localStorage.getItem("PrioTableId"), oldName: oldName }, function (resp) {
                    if (resp.status == "OK") {
                        if (!resp.exists) {
                            $('#imgdone' + identifier).attr('src', '/App_Static/css/icon16/done.png');
                            $('#adhgroupExists' + identifier).hide();
                            $('#' + id).prop('disabled', 'disabled');

                        } else {
                            var msg = resp.message.replace(/&lt;/g, '<').replace(/&gt;/g, '>');
                            $('#imgdone' + identifier).attr('src', '/App_Static/css/icon16/exclamation.png');
                            //$( '#adcgroupnameExists'+identifier ).html( msg + '.Do you want to append the parcels to the existing list?' );
                            $('#adhgroupExists' + identifier).show();
                        }
                    } else {
                        alert(resp.message);
                    }
                });
            });
        };
        function backToStartPage() {
            $("#SencondStep").hide();
            $("#btnContinue").prop("disabled", false);
            var fileUpload = $('input[type="file"]');

            if (!$('#tblNbhd').length) {
                var nbhdAssgn = localStorage.getItem("AdhcAssgn");
                var select = "";
                $.ajax({
                    type: "POST",
                    url: "priorities.aspx/UserDetails",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var count = data.d;
                        var name = count[0].UserName;

                        for (var i = 0; i < count.length; i++) {
                            select = select + "<option value='" + count[i].LoginId + "'>" + count[i].UserName + "</option>";
                        }
                        //select = select+ "</select>";
                        console.log(select);
                        var group = nbhdAssgn.split(",")
                        var table = "<table id ='tblNbhd' style='display: none;'>";
                        for (var i = 0; i < group.length; i++) {
                            var grp = group[i].split("@!*!*$#@");
                            table = table + "<tr>"
                                + "<td><td>Group Code/Number:</td>"
                                + "<td>"
                                + "<input type ='text' id ='txtGroupNumber" + i + "' autocomplete='off' CssClass='txt-adhoc-group-name'  MaxLength ='50' value = '" + grp[0] + "'/>"
                                + "</td>"
                                + "<td style='padding-left: 25px;'>Assign group to:</td>"
                                + "<td>"
                                + "<select class ='secl' id ='drpAssgn" + i + "'><option value=''>None, will assign later</option>"
                                + select
                                + "</select>"
                                + "</td>"
                                + "</tr>"
                        }
                        table = table + "</table>"
                        $('#Nbhdiv').append(table);
                        for (var t = 0; t < group.length; t++) {
                            var g = group[t].split("@!*!*$#@")
                            $("#drpAssgn" + t).val(g[1])
                        }
                    }
                });

            }

            $("#MainContent_MainContent_step1").show();
            $("#appendableDiv").remove();
            $("#lblTotalParcel").text("");
            $("#lblUrgent").text("");
            $("#lblHigh").text("");
            $("#lblNormal").text("");
            $("#lblInvalid").text("");
            $("#lblDuplicatedParcel").text("");
            $("#chkNotApprovedparcel").prop("checked", false);
            $("#chkNotSyncedparcel").prop("checked", false);
            $("#chkAssignedParcel").prop("checked", false);
            $("#chkWorkedInFieldLast30days").prop("checked", false);
            if (window && window.location) {
                var priorityUrl = window.location.toString(); //CC_2256, when we relaod after upload an invalid file then it redirect to qc dueto query string exists in url, that add from back button in qc search.It's clear when click back buttton of priority page to redirect to first page of priority.
                if (priorityUrl.indexOf("?") > 0) {
                    var clean_uri = priorityUrl.substring(0, priorityUrl.indexOf("?"));
                    window.history.replaceState({}, document.title, clean_uri);
                }
            }
            return false;
        }
        function reloadSecondStep(prioId) {
            showMask();
            var isDoNotReset = (localStorage.getItem('isDoNotReset') == 'true') ? true : false;
            var isAdhoc = (localStorage.getItem('isCreateAdhoc') == 'true') ? true : false;
            var AdhcAssgn = localStorage.getItem("AdhcAssgn");
            if (AdhcAssgn == undefined) {
                var grpCode = localStorage.getItem('groupCode');
                var assign = localStorage.getItem('assignTo');
                AdhcAssgn = grpCode + "@!*!*$#@" + assign;
            }

            takePriorityBackup(prioId, isAdhoc, currentLoginId, function () {
                $par('priority', { adhoc: isAdhoc, grpwithAss: AdhcAssgn, prioId: prioId, reload: "true", selectedNbhd: "" }, function (res) {
                    var t = {};
                    t = JSON.parse(JSON.stringify(res));
                    if (t.TotalParcel == 0) {
                        url = window.location.href.split('protected/')[0] + 'protected/parcels/priorities.aspx'
                        window.location.href = url;
                        return false;
                    }
                    secondStepCreator(t);
                    console.log(t);
                }, function (res) {
                    alert(JSON.parse(JSON.stringify(res)));
                    hideMask();
                });
            })


        }

        function takePriorityBackup(prioId, isAdhoc, user, callback) {
            $par("priobackup", { adhoc: isAdhoc, prioId: prioId, fileName: isAdhoc ? "AdhocFile.csv" : localStorage.getItem("fileName"), user: user }, function (res) {
                var pb = {};
                pb = JSON.parse(JSON.stringify(res));
                localStorage.removeItem("backupId")
                localStorage.setItem("backupId", pb.backupId);
                if (callback) callback();
            }, function (res) {
                alert(JSON.parse(JSON.stringify(res)));
                hideMask();
            })

        }

        function secondStepCreator(t, fromQC) {
            var cama = t.CAMASystem;
            var Duplicated = t.Duplicated;
            var High = t.High == null ? 0 : t.High;
            var Invalid = t.Invalid;
            var Normal = t.Normal == null ? 0 : t.Normal;
            var Urgent = t.Urgent == null ? 0 : t.Urgent;
            let Proximity = t.Proximity == null ? 0 : t.Proximity;
            let Medium = t.Medium == null ? 0 : t.Medium;
            let Critical = t.Critical == null ? 0 : t.Critical;
            var Valid = t.Valid;
            var isAssigned = t.isAssigned;
            var isNotApproved = t.isNotApproved;
            var isNotSyncedBack = t.isNotSyncedBack;
            var isWorkedInFieldLast30days = t.isWorkedInFieldLast30days;
            var TotalParcel = t.TotalParcel;
            var AdhcAssgn = t.AdhcAssgn;
            var CAMASystem = t.CAMASystem;
            var count = 0;
            var prioId = t.prioId;
            var AssignedGroupNames = t.AssignedGroupNames;
            //var backUpId = t.backUpId;
            var isAdho = t.isAdho;
            var bppCount = t.BppCount;
            var isDonotReset = localStorage.getItem('isDoNotReset') == 'true' ? "1" : "0";  //(($("#<%= chkDoNotReset.ClientID%>").attr("checked") == "checked") ? "1" : "0");
            if (parseInt(isAssigned) == 0 && parseInt(isNotApproved) == 0 && parseInt(isNotSyncedBack) == 0 && isWorkedInFieldLast30days == 0)
                $("#MainContent_MainContent_Div3").hide();
            else
                $("#MainContent_MainContent_Div3").show();
            if (parseInt(Duplicated) > 0) {
                $("#Duplicate_Parcels").show();
                $("#lblDuplicatedParcel").text(Duplicated);
            }
            else {
                $("#Duplicate_Parcels").hide();
            }
            if (parseInt(Invalid) > 0) {
                $("#lblInvalid").text(Invalid);
                $("#Invalid_Parcels").show();
            }
            else {
                $("#Invalid_Parcels").hide();
            }
            if (parseInt(isNotApproved) > 0) {
                count += 1;
                $("#trNotApprovedParcel").show();
                $("#lblNotApprovedParcel").html("<b>" + isNotApproved + "</b>" + (isNotApproved == "1") ? " Property In your list Is currently Marked as Complete In the cloud, but<font color='red'> <b> has not been QC Approved</b></font>." : " properties in your list are currently Marked As Complete in the cloud, but<font color='red'> <b> have not been QC Approved</b></font>.");
                $("#trSeparator1").show();
                $("#lblRemoveNotApprovedparcel").html((isNotApproved == "1") ? "Click here to remove the property from the Priority List upload. This property <strong><u>will not</u></strong> have its priority reset" : " Click here to remove the <b>" + isNotApproved + "</b> properties from the Priority List upload. These properties <strong><u>will not</u></strong> have their priorities reset.");
                $("#MainContent_MainContent_lbnApproveParcelsQc").html((isNotApproved == "1") ? "Go to QC to Approve " + isNotApproved + " Parcel" : "Go to QC to Approve " + isNotApproved + " Parcels")
            }
            else {
                $("#trSeparator1").hide();
                $("trNotApprovedParcel").hide();
            }
            if (parseInt(isAssigned) > 0) {
                count += 1;
                $("#trSeparator2").show();
                $("#trAssignedParcel").show();
                $("#lblAssignedParcel").html((isAssigned == "1") ? "Your Priority List includes a property in the following Assignment Group that Is currently being worked in the field:" : "Your Priority List includes properties in the following Assignment Groups that are currently being worked in the field:");
                $("#textareaAssignedGeoups").html("<strong>".concat(AssignedGroupNames, "</strong>"));
                $("#lblRemoveAssignedParcel").html((isAssigned == "1") ? "Click here to remove the property from the Priority List upload.This property <strong><u>will not</u></strong> have its priority reset" : "Click here to remove the <b>" + isAssigned + "</b> properties from the Priority List upload. These properties <strong><u>will not</u></strong> have their priorities reset.");
            }
            else {
                $("#trSeparator3").hide();
                $("#trAssignedParcel").hide();
            }
            if (parseInt(isNotSyncedBack) > 0) {
                count += 1;
                var select = "";
                if (CAMASystem != "") {
                    select = CAMASystem;
                }
                else {
                    select = "CAMASystem";
                }
                $("#trSeparator2").show();
                $("#trNotSyncedParcels").show();
                $("#lblNotSyncedParcel").html("<b>" + isNotSyncedBack + "</b> " + (isNotSyncedBack == "1") ? "Property in your list is currently QC Approved, but <font color='red'> <b> has not synced back to " + select + "</b></font>. " : " properties in your list are currently QC Approved, but <font color='red'><b>have not synced back to " + select + "</b></font>.");
                $("#lblRemoveNotSyncedParcel").html((isNotSyncedBack == "1") ? "Click here to remove the property from the Priority List upload. This property <strong><u>will not</u></strong> have its priority reset" : "Click here to remove the <b>" + isNotSyncedBack + "</b> properties from the Priority List upload. These properties <strong><u>will not</u></strong> have their priorities reset.");
            }
            else {
                $("#trSeparator2").hide();
                $("#trNotSyncedParcels").hide();
            }
            if (parseInt(isWorkedInFieldLast30days) > 0) {
                count += 1;
                $("#trSeparator3").show();
                $("#lblWorkedInFieldLast30days").html("Your Priority List includes <b>" + isWorkedInFieldLast30days + "</b> properties that were worked in the field in the last 30 days.");
                $("#trWorkedInFieldLast30days").show();
                $("#lblRemoveWorkedInFieldLast30days").html((isWorkedInFieldLast30days == "1") ? "Click here to remove the property from the Priority List upload. This property <strong><u>will not</u></strong> have its priority reset" : "Click here to remove the <b>" + isWorkedInFieldLast30days + "</b> properties from the Priority List upload. These properties <strong><u>will not</u></strong> have their priorities reset.");
            }
            else {
                $("#trSeparator3").hide();
                $("#trWorkedInFieldLast30days").hide();
            }
            if (count > 0) {
                $("#imgcompAnalyticWarning").attr("src", "/App_Static/css/icon16/exclamation.png");
            }
            else {
                $("#imgcompAnalyticWarning").attr("src", "/App_Static/css/icon16/done.png");
            }
            var existGrp = "";
            var grCode = "<table class='tableVerify' id ='tableVerify'>";
            var html = "<div id ='appendableDiv'>";
            var select = "";
            if (AdhcAssgn != "") {
                var grpexists = false;
                var spaceadj = '10.5'
                for (var t = 0; t < AdhcAssgn.length; t++) {
                    var grp = AdhcAssgn[t].split("@!*!*$#@");
                    if (grp[2] == "false") {
                        if (existGrp != "") {
                            existGrp = existGrp + ",";

                        }
                        grpexists = true;
                        html = html + "<div class='adhocgroupnameExists a' id ='adhgroupExists" + t + "'>"
                            + "<div class='a' style ='width: 45%;'>"
                            + "<label class ='lblAdhocNameExistsWarnings' id ='adcgroupnameExists" + t + "'>A group named  <strong>" + grp[0] + "</strong> already exists with  parcels. Do you want to append the parcels to the existing list?</label>"
                            + "<br>"
                            + "<strong>Note:</strong><span style='color:red'> If you do not rename the Group Code/Number,it will append the parcels to the existing list.</span>"
                            + "</div>"
                            + "<div class='a radioClass' > "
                            + "<input type = 'radio' name = 'yes" + t + "' value = '1' id = 'rdbtnYes" + t + "' />"
                            + "<label for='rdbtnYes" + t + "'> Yes</label>"
                            + "<input type='radio' name='yes" + t + "' value='0' id='rdbtnNo" + t + "' />"
                            + "<label for='rdbtnNo" + t + "'>No</label>"
                            + "</div >"
                            + "</div>"
                        select = "<img src='/App_Static/css/icon16/exclamation.png'  id='imgdone" + t + "' style='height: 20px; position: absolute; margin-left: 5px;' class='imgGroupNumberVerify' />"
                        existGrp = existGrp + grp[0];

                    }
                    else {
                        select = "<img src='/App_Static/css/icon16/done.png'  id='imgdone" + t + "' style='height: 20px; position: absolute; margin-left: 5px;' class='imgGroupNumberVerify' />"
                    }
                    if (grpexists)
                        spaceadj = 15;
                    else

                        spaceadj = 15;
                    html = html + "<div class ='a tableVerify' Id ='tableVerify" + t + "' >"
                        + "<div class ='a' style ='width: 55%'>"
                        + "<span>Group Code/Number: </span><input type='text' id='txtgrcode" + t + "' style=' width: 224px; ' MaxLength='50' autocomplete='off' class='newGroupNumber leftSide' Enabled='false' disabled value='" + grp[0] + "' orginalValue='" + grp[0] + "' />"
                        + select
                        + "</div>"
                        + " <div class ='a spacingDiv' Id ='spacingDiv" + t + "' style ='width: " + spaceadj + "%' > "
                        + "</div>"
                    html = html + "<div class='a' style ='width: 40%'>"
                        + "<span>Assign Group To: <input type='text' id='txtasgn" + t + "' class='txtAssignTo rightSide' Enabled='false' disabled value='" + grp[1] + "' />"
                        + "</span>"
                        + "</div>"
                    html = html + "</div>"
                }
            }
            html = html + "</div>"
            $("#adhgrpExists").append(html);
            if (existGrp != "") {
                //$("#adcgroupnameExists").text(existGrp);
                $("#adhgroupExists").show();
            }
            if (isAdho == "True") {
                $("#btnBack").hide();
                $("#btnClose").show();
            }
            else {
                $("#btnBack").show();
                $("#btnClose").hide();
            }
            $("#lblTotalParcel").text(TotalParcel);
            $("#lblUrgent").text(Urgent);
            $("#lblHigh").text(High);
            $("#lblNormal").text(Normal);
            if (EnableNewPriorities == 'True') {
                $("#lblCritical").text(Critical);
                $("#lblProximity").text(Proximity);
                $("#lblMedium").text(Medium);
            }

            $('.bppUpdate').hide();
            if (parseInt(bppCount) > 0) {
                $('.bppUpdate').show();
            }
            $("#MainContent_MainContent_step1").hide();
            $("#SencondStep").show();
            $("body").css("overflow -y", "scroll");
            editGrpCode();
            reomveLocalStorage(function () {
                localStorage.setItem("PrioTableId", prioId);
                //localStorage.setItem('isCreateAdhoc', isAdho);
                localStorage.setItem("Duplicated", Duplicated);
                localStorage.setItem("High", High);
                localStorage.setItem("Invalid", Invalid);
                localStorage.setItem("Normal", Normal);
                localStorage.setItem("Urgent", Urgent);
                if (EnableNewPriorities == 'True') {
                    localStorage.setItem("Medium", Medium);
                    localStorage.setItem("Critical", Critical);
                    localStorage.setItem("Proximity", Proximity);
                }
                localStorage.setItem("TotalParcel", TotalParcel);
                localStorage.setItem("isNotApproved", isNotApproved);
                localStorage.setItem("isAssigned", isAssigned);
                localStorage.setItem("isNotSyncedBack", isNotSyncedBack);
                localStorage.setItem("isWorkedInFieldLast30days", isWorkedInFieldLast30days);
                localStorage.setItem("AdhcAssgn", AdhcAssgn);
                //localStorage.setItem("AdhcAssgn","qwerty@!*!*$#@dcs-inteopgration@!*!*$#@true");
                localStorage.setItem("CAMASystem", CAMASystem);
                localStorage.setItem("isDonotReset", isDonotReset);
                hideMask();
            });
        }
        function closeAdhocCreation() {
            if (localStorage.getItem('createAdhocFrom') == 'atcMapView') {
                localStorage.removeItem('isCreateAdhoc');
                localStorage.removeItem('createAdhocFrom');
                let params = { Name: 'Search' };
                let queryString = jQuery.param(params, true);
                url = window.location.href.split('protected/')[0] + 'protected/parcels/atcmapview.aspx?' + queryString;
            }
            else {
                url = window.location.href.split('protected/')[0] + 'protected/quality/Default.aspx'
            }
            window.location.href = url;
            return false;
        }
        function openInQC() {
            window.scrollTo(0, 0);
            $("body").css("overflow", "hidden");
            var url = ''
            showMask();
            var params = { PrioPage: 'PriorityNotApproved' };
            var queryString = jQuery.param(params, true);
            url = window.location.href.split('protected/')[0] + 'protected/quality/Default.aspx?' + queryString
            window.location.href = url;
            return false;
        }
        function reloadFromQC() {
            showMask();
            var prioId = localStorage.getItem("PrioTableId", prioId);
            var AdhcAssgn = localStorage.getItem("AdhcAssgn", AdhcAssgn);
            var appendYes = localStorage.getItem("appendYes")
            if (appendYes) {
                appendYes = appendYes.split(',')
                appendYesTick = appendYes;
            }
            if (localStorage.getItem('isCreateAdhoc') == null)
                localStorage.getItem("fileName") != null ? localStorage.setItem('isCreateAdhoc', 'false') : localStorage.setItem('isCreateAdhoc', 'true');

            $par('priority', { adhoc: localStorage.getItem('isCreateAdhoc'), grpwithAss: AdhcAssgn, prioId: prioId, reload: "true", selectedNbhd: "" }, function (res) {
                var t = {};
                t = JSON.parse(JSON.stringify(res));
                if (appendYes) {
                    appendYes.forEach(function (x) {
                        var tem = t.AdhcAssgn[x]
                        if (tem) {
                            tem = tem.split("@!*!*$#@");
                            if (tem[2] == "false") tem[2] = "true"
                            tem = tem.join("@!*!*$#@");
                            t.AdhcAssgn[x] = tem
                        }
                    });
                }
                secondStepCreator(t);
                console.log(t);
            }, function (res) {
                alert(JSON.parse(JSON.stringify(res)));
                hideMask();
            });
        }
        function viewInQc() {
            showMask();
            var chk = $('#gvResult input[type=checkbox]:checked');
            var selectedNbhd = "";
            var ccGroups = ""
            var prioId = localStorage.getItem("PrioTableId", prioId);
            var grpString = "";
            var divCollection = $("#appendableDiv").children('div .tableVerify')
            divCollection.each(function (index, item) {
                var l = $(this).$('.leftSide').val();
                var r = $(this).$('.rightSide').val();
                if (grpString != "") {
                    grpString = grpString + ","
                }
                grpString = grpString + l + "@!*!*$#@" + r;
            });
            console.log(grpString);
            localStorage.setItem("AdhcAssgn", grpString);
            localStorage.setItem("appendYes", appendYesTick);
            var AdhcAssgn = localStorage.getItem("AdhcAssgn", AdhcAssgn);
            var viewOrRemove;
            var clas = $('#gvResult input[type=checkbox]:checked').attr("class");
            (clas == "qc" ? viewOrRemove = "view" : viewOrRemove = "remove");
            chk.each(function () {
                if (selectedNbhd != "") {
                    selectedNbhd = selectedNbhd + ", ";
                }
                if (ccGroups != "") {
                    ccGroups = ccGroups + ", ";
                }
                selectedNbhd = selectedNbhd + $(this).attr('nbhdid');
                ccGroups = ccGroups + $('td', $(this).parents('tr').eq(0)).eq(0).html();
            });

            if (viewOrRemove == "view") {
                var chk = $('#gvResult input[type=checkbox]')
                chk.prop("disabled", true)
                $par("viewinqc", { prioId: prioId, selectedNbhd: selectedNbhd }, function (resp) {
                    var params = { PrioPage: 'AssignedParcels' };
                    var queryString = jQuery.param(params, true);
                    url = window.location.href.split('protected/')[0] + 'protected/quality/Default.aspx?' + queryString
                    window.location.href = url;
                    hideMask();
                    return false;
                });
            }
            else if (viewOrRemove == "remove") {
                if (confirm("All the " + ccGroups + "'s checked as 'Remove From List' will be removed from the priority list.Do you want to proceed?")) {
                    showMask();
                    $('.masklayer').css('z-index', '1200');
                    var chk = $('#gvResult input[type=checkbox]')
                    chk.prop("disabled", true)
                    $par('priority', { adhoc: localStorage.getItem('isCreateAdhoc'), grpwithAss: AdhcAssgn, prioId: prioId, reload: "true", viewOrRemove: viewOrRemove, selectedNbhd: selectedNbhd }, function (res) {

                        var t = {};
                        t = JSON.parse(JSON.stringify(res));
                        var counts = t.counts;
                        console.log(t);
                        if (counts > 1) {
                            alert(counts + " property(s) are permanently removed from the Priority Parcels List.")
                        }
                        else {
                            alert("A property permanently removed from the Priority Parcels List.")
                        }

                        if (t.TotalParcel == 0 && t.isAdho.toUpperCase() == "TRUE") {
                            url = window.location.href.split('protected/')[0] + 'protected/quality/Default.aspx'
                            window.location.href = url;
                            return false;
                        }
                        else if (t.TotalParcel == 0 && t.isAdho.toUpperCase() == "FALSE") {
                            location.reload(true);
                            return false;
                        }
                        $("#appendableDiv").remove();
                        secondStepCreator(t);
                        $('.viewlist-popup').dialog('close');
                        console.log(t);
                        $('.masklayer').css('z-index', '20');
                        hideMask();
                        return false;
                    }, function (res) {
                        alert(JSON.parse(JSON.stringify(res)));
                        $('.masklayer').css('z-index', '20');
                        hideMask();
                    });
                }
                else
                    hideMask();
                return false;
            }

        }
        function flieChange() {
            document.getElementById("MainContent_MainContent_ControlFile").value = null;
            $("#chkadhc").prop("checked", false);
            $('#tblNbhd').remove();
        }
    </script>
    <style type="text/css">
        .btn {
            font-weight: bold;
            height: 35px;
            width: 120px;
            background-color: #dedede;
            border: 1px solid silver;
        }

        .chbAppendParcel {
            display: none;
        }

        .ControlFile {
            display: none;
        }

        .txtFileName {
            margin-left: 3px;
            min-width: 25%;
            width: auto;
        }

        .link {
            float: right;
        }

        .tdleft {
            text-shadow: 0 0 black;
            width: 48%;
            padding-right: 5%;
            text-align: justify;
        }

        .tdright {
            width: 47%;
            text-align: justify;
            padding-right: 3.5%;
        }

        .label-check {
            display: block;
            padding-left: 15px;
            text-indent: -15px;
        }

        .input-check {
            width: 13px;
            height: 13px;
            padding: 0;
            margin: 0;
            vertical-align: bottom;
            position: relative;
            top: -1px;
            *overflow: hidden;
        }

        .lablebold {
            font-weight: bold;
        }

        .popupbox {
            padding: 10px;
            border: 1px solid rgba(204, 204, 204, 0.5);
            background-color: white;
            position: absolute;
            box-shadow: 2px 2px 8px 2px grey;
            top: 10%;
            left: 30%;
        }

        .popup-header {
            background-color: #545758;
            color: #FFF;
            padding: 5px;
        }

        .close-btn {
            float: right;
            left: 10px;
            position: relative;
            top: -4px;
            font-weight: bold;
            font-size: larger;
            cursor: pointer;
            margin-bottom: 13px;
            margin-right: 15px;
        }

            .close-btn:hover {
                color: red;
            }

        .lblAdhocNameExistsWarnings {
            float: left;
        }

        .count {
            padding-left: 10px;
        }

        .info li {
            padding-top: 3px;
        }

        .tableVerify {
            width: 100%;
            margin-top: 10px;
        }

            .tableVerify tr {
                height: 25px;
            }

        .tdleft1 {
            width: 54%;
        }

        .tdItem {
            width: 9%;
        }

        .tdCount {
            width: 5%;
        }

        .tdLink {
            width: 25%;
        }

        .tableReults {
            padding-top: 10px;
            border-collapse: collapse;
        }

            .tableReults tr {
                height: 25px;
            }

        #progressBar {
            background: #dcdcdc;
            height: 15px;
        }

        .scroll {
            border: 1px solid silver;
            margin: 20px;
            border-collapse: collapse;
        }

            .scroll tr {
                display: flex;
            }

            .scroll td {
                padding: 7px;
                text-align: left;
                flex: 0 auto;
                border: 1px solid #ffffff;
                border-width: 0px 1px 1px 0px;
                width: 148px;
                word-wrap: break;
                word-break: break-all;
            }

            .scroll thead {
                background-color: rgba(193, 187, 187, 0.31);
            }

                .scroll thead td {
                    text-align: center;
                    font-weight: bold;
                }

                .scroll thead tr:after {
                    overflow-y: scroll;
                    visibility: hidden;
                    height: 0;
                }

                .scroll thead th {
                    flex: 0 auto;
                    display: block;
                    border: 1px solid #000;
                }

            .scroll tbody {
                display: block;
                width: 100%;
                overflow-y: scroll;
                max-height: 303px;
            }

        .tr-warnings td {
            border-top: 2px solid #ddd;
            padding: 10px;
        }

        .btnContainer {
            margin-top: 10px;
            margin-bottom: 15px;
            float: right;
            padding-right: 23px;
        }

        .refresh {
            float: right;
            font-size: 15px;
            font-weight: bold;
            margin-right: 10px;
            margin-top: 2px;
        }

        .table-header-info {
            border-collapse: collapse;
            width: 100%;
        }

            .table-header-info td {
                text-align: center;
                padding: 2px;
                border: 1px solid black;
            }

        .priorityTypeContainer {
            display: none;
            min-height: 50px !important;
        }

        .a tableVerify {
            display: table-row;
            width: 100%;
        }

        .a div {
            display: table-cell;
            padding-top: 10px;
        }

        .radioClass {
            padding-left: 20px;
            padding-right: 20px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <h1>Priority Parcels List</h1>
    <div id="borderDiv" style="border: 1px solid silver; width: 99% !important; min-height: 425px; margin-bottom: 20px;float:left;">
        <asp:HiddenField runat="server" ID="hdnIsCreateAdhoc" Value="0" />
        <asp:HiddenField runat="server" ID="hdnAdhocPriority" Value="0" />
        <asp:HiddenField runat="server" ID="hdnIsMultipleAdhocs" Value="0" />
        <asp:HiddenField runat="server" ID="hdnAdhocNames" Value="" />
        <asp:HiddenField runat="server" ID="hdnmultinbhdprocess" Value="0" />
        <asp:HiddenField runat ="server" ID ="hdnKeys" Value ="0" />
        <input id="hfBackupId" type="hidden" runat="server" value="" />
         <input id="hfIsCreateAdhoc" type="hidden" runat="server" value="" />
        <div class="step1" style="min-height: 418px; height:auto;float:left;width: 100%;" id="step1" runat="server">
            <h2 style="border-bottom: 1px solid silver; padding: 5px 5px; background-color:#dedede; margin: 0px;">Step 1- Priority List Format</h2>
    		<div style="padding: 0px 11px; min-height:403px;float:left;width: 97%;" id="step1content">
        		<div style="float:left;width: 100%;">
      				<div style="float:left;width: 100%;">
			            <p runat="server" id="pHaveCK" style="font-size: 18px;">
			                <%=KeyList()%>, <strong>PRIORITY, MESSAGE</strong>
			            </p>
			            <p runat="server" id="pNoCK">
			                Ensure that the column headers in the CSV file match the fields above and appear in the same order.<asp:LinkButton runat="server" Text="More Info" OnClientClick="return moreHeaderInfo(this);" Name="Header" ID="lbnHeaderInfo" Style="margin-left: 5px;"></asp:LinkButton>
			            </p>
			            <div class="popupbox header-info" style="display: none">
			                <div class="popup-header">
			                    How to label column headers in the CSV file
			                          <span class="close-btn" title="Close">&times</span>
			                </div>
			                <div class="popupcontent header-info-content" style="width: 98%; height: 98%;">
			                </div>
			            </div>
			            <p style="line-height: 160%">
                            <%If EnableNewPriorities Then %>
                                <span class="tip" abbr="Setting a property to Normal, Medium, High, Urgent or Critical will indicate this property needs a field check and will download the property in the Priority list screen of mobile.<br/><br/>Setting a property to Proximity with a 0 in the list will de-escalate the priority of a parcel, bringing it from Normal, Medium, High, Urgent or Critical, back down to Proximity.<br/>"><strong>PRIORITY</strong> - The accepted values are: 0 (Proximity), 1 <mark style="background-color: #8944FA;">(Normal)</mark>, 2 <mark style="background-color: #00B0F0;">(Medium)</mark>, 3 <mark style="background-color: #FFFF00;">(High)</mark>, 4 <mark style="background-color: #FFC000;">(Urgent)</mark>, 5 <mark style="background-color: #FF0000;">(Critical)</mark>.</span><br>
			                    <strong>MESSAGE</strong> - This field will set the <i>alert message</i> for the parcels that will be displayed on the Mobile SortScreens (Priority and Proximity) and the Parcel Dashboard on the property in the Quality Control Module for Normal, Medium, High, Urgent and Critical priority parcels. For Proximity parcels, this field will be ignored even if provided and will be updated as blank.<br>
                            <%Else %>
                                <span class="tip" abbr="Setting a property to High or Urgent will indicate this property needs a field check and will download the property in the Priority list screen of mobile.<br/><br/>Setting a property to Normal with a 0 in the list will de-escalate the priority of a parcel, bringing it from High or Urgent, back down to Normal.<br/>The Normal parcels are downloaded as Proximity parcels in mobile."><strong>PRIORITY</strong> - The accepted values are: 0 (Normal), 1 (High), 2 (Urgent).</span><br>
			                    <strong>MESSAGE</strong> - This field will set the <i>alert message</i> for the parcels that will be displayed on the Mobile SortScreens (Priority and Proximity screens) and the Parcel Dashboard on the property in the Quality Control Module for High and Urgent parcels. For Normal parcels, this field will be ignored even if provided and will be updated as blank.<br>
                            <%End If %>
			              
			            </p>
			            <p>
			                <strong style="color: red;">Notes :</strong>
			            </p>
			            <div class="info">
			                <ul>
			                    <li>The maximum size of uploaded file should be 4MB.</li>
			                    <li>Be sure to check the column headers for typos or spaces: Example: <asp:Label ID="lblCorrect" style="color:green;font-weight: bold;" runat="server" Text=""></asp:Label><span>&#44; </span><asp:Label ID="lblInCorrect" style="color:red;font-weight: bold;" runat="server" Text=""></asp:Label></li>
			                    <li style="float: left; width: 100%;">Be sure that the file is an Excel .csv file. You can save your Excel file as a .csv file through the File &gt; Save As options.<a style="margin-left: 5px;" href="#" onclick="return moreInfo('1');">More Info</a></li>
			                </ul>
			            </div>
			            <div id="dynamicPopUp" class="popupbox file-info" style="display: none">
			                <div class="popup-header">
			                    How to save a CSV file in Excel
			                          <span class="close-btn" title="Close">&times</span>
			                </div>
			                <div class="popupcontent" style="width: 98%; height: 98%;">
			                    <p>
			                        <ul>
			                            <li>In the "Save as type" form, click the file format CSV (Comma Delimited(*.csv))</li>
			                        </ul>
			                    </p>
			                    <img src="/App_Static/images/file-info.png" style="width: 512px; height: 420px;">
			                </div>
			            </div>
			            <p style="padding-top: 20px;">
			                <asp:LinkButton ID="lbDowload" CssClass="lablebold" OnClick="ExportCSV" runat="server">Click here to download a sample Priority List file</asp:LinkButton>
			            </p>
			            <p>
			                 <span class="tip" abbr="By default, any property in the Priority List will have its Field Reviewed By, Field Reviewed On, QC Approved By and QC Approved On information cleared.<br/>This will make the property available on the field device to be worked in mobile. If you check this box, any property that is currently marked as complete <br/>and/or QC Approved but has not synced back to CAMA will be ignored in the list.<br /><br />This option is a protection against any mistakes in the Priority List (example - a property was visited yesterday for a permit, but that property was still in the <br/>Priority List that is being uploaded. In this case, the field agent should not be sent back to that property. The system will see that the property is currently<br/>marked as complete and will ignore that in the Priority List upload)."><strong>Important: </strong>The Field Review and QC approval status of all parcels in the list will be reset
			            to NULL.</span><br />
			                <asp:CheckBox runat="server" ID="chkDoNotReset" CssClass="chkDoNotReset" Text="Do not reset review and QC approval status for the parcels in the uploaded list." />
			            </p>
           				<p style="margin: 5px 0px; padding: 12px 5px; background: #DFDFDF; border: 1px solid #CFCFCF; width: width:99%;">
			                <asp:FileUpload runat="server" ID="ControlFile" CssClass="ControlFile" Width="400px" accept=".csv"  />
			                <input id='btnChoose' type='button' value='Choose File' onclick="flieChange()"/><asp:TextBox runat="server" ID="txtFileName" CssClass="txtFileName" onkeypress="groupNumberValidation(this)" />
			            </p>				            
			            <asp:HiddenField runat="server" ID="hdnAdHoc" Value="0" />
			            <asp:HiddenField runat="server" ID="HiddenField1" Value="0" />
			            <asp:HiddenField runat="server" ID="hdnGroupAppend" Value="0" />
			            <asp:HiddenField runat="server" ID="hdnFilePath" Value="" />
			            <asp:HiddenField runat="server" ID="hdnFileSize" Value="" />
			            <asp:HiddenField runat="server" ID="hdnFirstLoad" Value="1" />
				        <asp:PlaceHolder runat="server" ID="phAdhoc">			            		                          
				            <p>
				                <span class="tip" tip-margin-top="-321" abbr="Ad-Hoc Assignment Groups are temporary Assignment Groups in which properties can be placed by pulling them from their original Assignment Group.<br/>Once the properties have been worked, the user can Relink the properties and they will be placed back in their original Assignment Group.<br/>(Relinking can be done through 'Relink Selected' under Appraisal Task Control > Manage Assignment Groups > Manage Ad-Hoc Groups.)<br /><br />Ad-Hoc groups can be useful in several different scenarios:<br /><br />- A residential appraiser is working in an Assignment Group and the AG/CUV appraiser needs to review some properties in the same Assignment Group.<br/>The AG/CUV properties can be extracted in to their own Ad-Hoc group, allowing both appraisers to visit properties in the same location.<br /><br />- There are 15 Protest/Appeal properties that need to be checked spread throughout the county and other properties in the area will not need to be visited.<br />The Protest/Appeal properties can be extracted and placed in to an Ad-Hoc group, allowing the appraiser visit these 15 properties with no Proximity parcels<br />downloaded to mobile. Once the field checks are completed the Ad-Hoc group will be relinked and the properties placed backed in their original Assignment<br />Groups."> <strong>Ad-hoc Assignment Group:</strong> Create an ad-hoc assignment group for the uploaded list or move the list to an already created assignment group.</span>
				                </p>
				                <asp:UpdatePanel runat="server">
				                <ContentTemplate>
				                   <p>
                                       <input type="checkbox" name="adhc" value="High"  id="chkadhc"/>Use ad-hoc assignment group.
				                        <%--<asp:CheckBox runat="server" ID="chkCreateNbhd" CssClass="chkCreateNbhd" Text="Use ad-hoc assignment group." 
				                            AutoPostBack="true" />--%>
				                   </p>  
                                    <div id ="Nbhdiv">

                                    </div> 
				                    <table runat="server" id="tblNbhdDetails" style="display: none;">
				                        <tr>
				                            <td>Group Code/Number:
				                            </td>
				                            <td>
				                                <asp:TextBox runat="server" ID="txtGroupNumber"  autocomplete="off" CssClass="txt-adhoc-group-name" onkeypress="groupNumberValidation(this)" style="display:none" MaxLength ="50"/>
				                            </td>
				                            <td style="padding-left: 25px;">Assign group to:
				                            </td>
				                            <td>
				                                <asp:DropDownList runat="server" ID="ddlAssignTo" Width="200px" />
				                            </td>
				                        </tr>
				                    </table>
                                    
				                </ContentTemplate>
				                </asp:UpdatePanel>
				                <p>
				     					<asp:CheckBox runat="server" ID="chkCreateMultiNbhd" CssClass="chkCreateMultiNbhd"  Text="Use ad-hoc assignment group for <strong><i>multiple</i></strong> Ad-Hocs."
				                            AutoPostBack="true"  style="display: none;"/>
				                            </p>
				                              <asp:Panel runat="server" id="tblmultiNbhdDetails" class="tblmultiNbhdDetails" style="display: none;">
				                        	<table>
				                        	<tr>
				                            <td>Group Codes/Numbers:
				                            </td>
				                            <td>
				                                <asp:TextBox runat="server" ID="txtMultiGroupNumber" TextMode="MultiLine" Columns="30" Rows="5" autocomplete="off" CssClass="txt-multi-adhoc-group-name" ReadOnly="true"  style="display:none;background-color: #e4e4e4;"/>
				                            </td>
				                            <td style="padding-left: 25px;">Assign groups to:
				                            </td>
				                            <td>
				                                <asp:DropDownList runat="server" ID="ddlmultiAssignTo" Width="200px" />
				                            </td>
				                        </tr>
				                        </table>
				                    </asp:Panel>
				                    
				        </asp:PlaceHolder>
        			</div>
         		</div>
       			<div style="float:left;width: 100%;">
	                <p style="text-align:right;">
                        <button id ="btnContinue" onclick="return verifyListAndLoadPrioTable()" style="height:35px;width:120px" >Continue</button>
	                    
	                </p>
        		</div>
            </div>
        </div>
        
        <div class="step2" id ="SencondStep" style="min-height: 418px; height:auto;float:left;width: 100%; display :none">
            <h2 style="border-bottom: 1px solid silver; padding: 5px 5px; background-color: #dedede; margin: 0px;">Step 2- Verify Priority List<span class="refresh"></span></h2>
                <%--<asp:LinkButton runat="server" Text="Refresh" Name="Invalid" ID="LinkButton1"></asp:LinkButton>--%>
            <div style="padding: 0px 11px; min-height: 403px;float: left;width: 98%;" id="SencondStepcontent" >
                <div style="min-height:451px;float:left;width: 100%;">
                    <div style="width: 100%; height: auto;float: left;">
                        <div style="width: 48%;float:left;padding-right: 5%;">
                            <table class="tableVerify">
			                    <tr>
			                        <td class="tdleft1"><strong>Verifying uploaded list...</strong></td> 
			                    </tr>
			                    <tr class="trfile">
			                        <td class="tdleft1">Checking file format as CSV
			                            <img src="/App_Static/css/icon16/done.png" style="margin-left: 5px;" /></td>
			                    </tr>
			                    <tr class="trfile">
			                        <td class="tdleft1">Checking file size is less than 4 MB
			                        <img src="/App_Static/css/icon16/done.png" style="margin-left: 5px;" /></td>
			                    </tr>
			                    <tr class="trfile">
			                        <td class="tdleft1">Checking Column Headers
			                        <img src="/App_Static/css/icon16/done.png" style="margin-left: 5px;" /></td>
			                    </tr>
			                    <tr>
			                        <td class="tdleft1">Compiling Analytics and Warnings
			                        <img src="/App_Static/css/icon16/done.png" id="imgcompAnalyticWarning"  style="margin-left: 5px;" /></td>
			                    </tr>
	                		</table>
                        </div>
                        <div style="width: 47%;float:left;">
		               		<table class="tableVerify">
			                    <tr>
			                        <td colspan="3"><strong>Priority List Analytic Results</strong> </td>
			                    </tr>
			                    <tr>
			                        <td class="tdItem">Total Parcels:</td>
			                        <td class="tdCount"><label class ="lablebold count" id ="lblTotalParcel"></label></td>
			                        <td class="tdLink"></td>
			                    </tr>
                                <%If EnableNewPriorities Then %>
                                <tr>
			                        <td class="tdItem">Critical Priority(s):</td>
			                        <td class="tdCount"><label class ="lablebold count" id ="lblCritical"></label></td>
			                        <td class="tdLink"></td>
			                    </tr>
                                <%End if %>
			                    <tr>
			                        <td class="tdItem">Urgent Priority(s):</td>
			                        <td class="tdCount"><label class ="lablebold count" id ="lblUrgent"></label></td>
			                        <td class="tdLink"></td>
			                    </tr>
			                    <tr>
			                        <td class="tdItem">High Priority(s):</td>
			                        <td class="tdCount"><label class ="lablebold count" id ="lblHigh"></label></td>
			                        <td class="tdLink"></td>
			                    </tr>
                                <%If EnableNewPriorities Then %>
                                    <tr>
			                            <td class="tdItem">Medium Priority(s):</td>
			                            <td class="tdCount"><label class ="lablebold count" id ="lblMedium"></label></td>
			                            <td class="tdLink"></td>
			                        </tr>
                                <%End If %>
			                    <tr>
			                        <td class="tdItem">Normal Priority(s):</td>
			                        <td class="tdCount"><label class ="lablebold count" id ="lblNormal"></label></td>
			                        <td class="tdLink"></td>
			                    </tr>
                                <%If EnableNewPriorities Then %>
                                    <tr>
			                            <td class="tdItem">Proximity Priority(s):</td>
			                            <td class="tdCount"><label class ="lablebold count" id ="lblProximity"></label></td>
			                            <td class="tdLink"></td>
			                        </tr>
                                <%End If %>
			                    <tr id="Invalid_Parcels" style="display:none">
			                        <td class="tdItem">Invalid Parcels:</td>
			                        <td class="tdCount"><label class ="lablebold count" id ="lblInvalid"></label></td>
			                        <td class="tdLink">
			                            <asp:LinkButton runat="server" Text="View" OnClientClick="return viewList(this);" Name="Invalid" ID="LinkButton2"></asp:LinkButton></td>
			                    </tr>
			                    <tr id="Duplicate_Parcels" style="display:none">
			                        <td class="tdItem">Duplicated Parcels:</td>
			                        <td class="tdCount"><label class ="lablebold count" id ="lblDuplicatedParcel"></label></td>
			                        <td class="tdLink">
			                            <asp:LinkButton runat="server" Text="View" OnClientClick="return viewList(this);" Name="Duplicated" ID="LinkButton3"></asp:LinkButton></td>
			                    </tr>
		                	</table>
	                	</div>
                    </div>
                    <div id="adhgrpExists"  style="width: 100%; height: auto;float: left;" class ="adhcgrpExists">
	                    
                </div>
                    <div style="width: 100%; height: auto;float: left;padding-bottom: 10px;padding-top: 10px;">
	                	<asp:CheckBox runat="server" ID="CheckBox1" CssClass="chkDoNotReset1" Enabled="false" Visible="False" Text="Do not reset review and QC approval status for the parcels in the uploaded list." />
	                </div>
                    <div style="width: 100%; height: auto;float:left;">
		                <div id="Div3" runat="server">
		                    <strong style="color: red; margin-top: 20px">Warnings</strong>
		                    <table style="margin-top: 15px;width:100%;">
		                        <tr id="trNotApprovedParcel" style="display:none">
		                            <td>
		                                <img src="/App_Static/images/warning-sign.png" width="30" height="30"></td>
		                            <td class="tdleft">
		                                <label id ="lblNotApprovedParcel"></label>
		                                <br />
		                                <asp:LinkButton runat="server" CssClass="link" Text="View" OnClientClick="return viewList(this);" ID="lbnApproveParcels" Name="NotApproved"></asp:LinkButton>
		                            </td>
		                            <td class="tdright">
		                                <input type="checkbox" name="adhc" value="High"  id="chkNotApprovedparcel"/>
                                        <label  id ="lblRemoveNotApprovedparcel"></label>
		                                <br />
		                                <asp:LinkButton runat="server" CssClass="link lablebold" Text="" ID="lbnApproveParcelsQc" OnClientClick="return openInQC();"></asp:LinkButton>
		                            </td>
		                        </tr>
		                        <tr id="trSeparator1" class="tr-warnings" style="display:none">
		                            <td colspan="4"></td>
		                        </tr>
		                        <tr id="trNotSyncedParcels" style="display:none">
		                            <td>
		                                <img src="/App_Static/images/warning-sign.png" width="30" height="30"></td>
		                            <td class="tdleft">
		                                <label  id ="lblNotSyncedParcel"></label>
		                                <br />
		                                <asp:LinkButton runat="server" CssClass="link" Text="View" OnClientClick="return viewList(this);" ID="LinkButton6" Name="NotSyncedBack"></asp:LinkButton>
		                            </td>
		
		                            <td class="tdright">
		                                <input type="checkbox" name="adhc" value="High"  id="chkNotSyncedparcel"/>
                                        <label  id ="lblRemoveNotSyncedParcel"></label>
		                            </td>
		                        </tr>
		                        <tr id="trSeparator2" class="tr-warnings" style="display:none">
		                            <td colspan="4"></td>
		                        </tr>
		                        <tr id="trAssignedParcel" style="display:none">
		                            <td>
		                                <img src="/App_Static/images/warning-sign.png" width="30" height="30"></td>
		                            <td class="tdleft">
		                                <label  id ="lblAssignedParcel"></label>
		                                <div id="textareaAssignedGeoups" style="height: auto; width: 400px; word-break: break-all;" ></div>
		                                <asp:LinkButton runat="server" CssClass="link" Text="View" OnClientClick="return viewList(this);" ID="LinkButton7" Name="Assigned"></asp:LinkButton>
		                            </td>
		
		                            <td class="tdright">
		                                <input type="checkbox" name="adhc" value="High"  id="chkAssignedParcel"/>
                                        <label  id ="lblRemoveAssignedParcel"></label>
		                            </td>
		                        </tr>
		                        <tr id="trSeparator3" class="tr-warnings" style="display:none">
		                            <td colspan="4"></td>
		                        </tr>
		                        <tr id="trWorkedInFieldLast30days" style="display:none">
		                            <td>
		                                <img src="/App_Static/images/warning-sign.png" width="30" height="30"></td>
		                            <td class="tdleft">
		                                <label  id ="lblWorkedInFieldLast30days"></label>
		                                <br />
		                                <asp:LinkButton runat="server" CssClass="link" Text="View" OnClientClick="return viewList(this);" ID="LinkButton8" Name="WorkedInFieldLast30days"></asp:LinkButton>
		                            </td>
		
		                            <td class="tdright">
		                                <input type="checkbox" name="adhc" value="High"  id="chkWorkedInFieldLast30days"/>
                                        <label  id ="lblRemoveWorkedInFieldLast30days"></label>
		                            </td>
		                        </tr>
		                    </table>
		                </div>
	                </div>
                </div>
                <div class="bppUpdate" style="display: none;">
                    <hr style="border-top: 1px solid #ddd;">
                    <p style="font-weight: bold;">There are Real Properties related to the Personal Property accounts in this list. Do you want these Real Properties to be visited along with the Personal Property?</p>
                    <div style="display: block;">
                    <%If EnableNewPriorities Then %>
                          <input type="radio" id="pr6" name="bppGroup" value="6" style="margin-bottom: 10px;" checked>
                          <label for="pr6"><b>Yes</b> -  Set the Real Property's Priority the same as the Personal Property's Priority </label>
                          <br>
                          <input type="radio" id="pr5" name="bppGroup" value="5" style="margin-bottom: 10px;" checked>
                          <label for="pr5"><b>Yes</b> - Set as Critical priority </label>
                          <br>
                          <input type="radio" id="pr4" name="bppGroup" value="4" style="margin-bottom: 10px;" checked>
                          <label for="pr4"><b>Yes</b> - Set as Urgent priority </label>
                          <br>
                          <input type="radio" id="pr3"  value="3" style="margin-bottom: 10px;" name="bppGroup">
                          <label for="pr3"><b>Yes</b> - Set as High priority </label>
                          <br>
                          <input type="radio" id="pr2" value="2" style="margin-bottom: 10px;" name="bppGroup">
                          <label for="pr2"><b>Yes</b> - Set as Medium priority </label>
                          <br>
                          <input type="radio" id="pr1" value="1" style="margin-bottom: 10px;" name="bppGroup">
                          <label for="pr1"><b>Yes</b> - Set as Normal priority </label>
                          <br>
                          <input type="radio" id="pr0" value="0" style="margin-bottom: 10px;" name="bppGroup">
                          <label for="pr0"><b>No</b> - Do not flag Real Properties to be visited</label>
                    <%Else %>
                          <input type="radio" id="pr3" name="bppGroup" value="3" style="margin-bottom: 10px;" checked>
                          <label for="pr3"><b>Yes</b> -  Set the Real Property's Priority the same as the Personal Property's Priority </label>
                          <br>
                          <input type="radio" id="pr2" name="bppGroup" value="2" style="margin-bottom: 10px;" checked>
                          <label for="pr2"><b>Yes</b> - Set as Urgent priority </label>
                          <br>
                          <input type="radio" id="pr1" value="1" style="margin-bottom: 10px;" name="bppGroup">
                          <label for="pr1"><b>Yes</b> - Set as High priority </label>
                          <br>
                          <input type="radio" id="pr0" value="0" style="margin-bottom: 10px;" name="bppGroup">
                          <label for="pr0"><b>No</b> - Do not flag Real Properties to be visited</label>
                    <%End if %>
                      
                   </div>
                </div>
                <div style="float:left;width: 100%;">
                    <div id="statusBar" style="display: none; position: relative; width: 100%;">
                		<div id="currentStatus" style="position: relative; width: 0%; height: 100%; background: #2164df;"></div>
            		</div>
                    <p style="text-align:right;">
                        <button  id="btnBack" class="btnBack" onmousedown="return backToStartPage()" style="height:35px;width:120px;display:none" >Back</button>
                        <button id="btnUpload" style="height:35px;width:120px;font:bold" onclick= "return UploadListConfirmation()">Upload </button>
                        <button id ="btnClose" style="height:35px;width:120px;font:bold; display:none"; onclick ="return closeAdhocCreation()">Close</button>
                    </p>
                </div>
            </div>
        </div>
       
        <div class="step3" id="thirdStep" style="min-height: 418px; height:auto;float:left;width: 100%; display :none">
            <h2 style="border-bottom: 1px solid silver; padding: 5px 5px; background-color: #dedede; margin: 0px;">Step 3- Priority List Upload Results</h2>
            <div style="padding: 0px 11px; min-height: 403px;float: left;width: 98%;" id="Div2" runat="server">
            	<div style="min-height:451px;float:left;width: 100%;">
	                <div style="height: auto;float: left;padding: 0px 40px;margin-top: 35px;">
		                <span>
		                    <label  id ="lblSuccess" Style="font-size: 20px;"></label>
		                </span>
		                <table class="tableReults">
		                    <tr>
		                        <td><strong>Parcel Affected</strong></td>
		                    </tr>
		                    <tr>
		                        <td>Total Parcels</td>
		                        <td>:<label  id ="lblTotalAffectedparcelCount" class ="lablebold count"></label></td>
		
		                    </tr>
                            <%If EnableNewPriorities Then %>
		                        <tr>
		                            <td>Critical Priority(s)</td>
		                            <td>:<label id ="lblTotalAffectedCriticalCount" class ="lablebold count"></label></td>
		                        </tr>
                            <%End If %>
		                    <tr>
		                        <td>Urgent Priority(s)</td>
		                        <td>:<label id ="lblTotalAffectedUrgentCount" class ="lablebold count"></label></td>
		                    </tr>
		                    <tr>
		                        <td>High Priority(s)</td>
		                        <td>:<label id ="lblTotalAffectedHighCount" class ="lablebold count"></label></td>
		                    </tr>
                            <%If EnableNewPriorities Then %>
                                <tr>
		                            <td>Medium Priority(s)</td>
		                            <td>:<label id ="lblTotalAffectedMediumCount" class ="lablebold count"></label></td>
		                        </tr>
                            <%End If %>
		                    <tr>
		                        <td>Normal Priority(s)</td>
		                        <td>:<label id ="lblTotalAffectedNormalCount" class ="lablebold count"></label></td>
		                    </tr>
                            <%If EnableNewPriorities Then %>
                                <tr>
		                            <td>Proximity Priority(s)</td>
		                            <td>:<label id ="lblTotalAffectedProximityCount" class ="lablebold count"></label></td>
		                        </tr>
                            <%End If %>
		                </table>
                        <div style="height: auto;float: left;margin-top: 35px; display:none" id="grpdetails">

                        </div>		            
                    </div>
	            </div>
                
            	<div style="float:left;width: 100%;">
                <p style="text-align:right;">
                    <button id="btnClosePrioUpload" style="height:35px;width:120px;" onclick="return CloseThirdStep()">Close</button>
                </p>
            </div>
            </div>
        </div>
    </div>
    <div class="viewlist-popup" style="display: none;">
        <asp:HiddenField runat="server" ID="hdnViewInQCNbhdIds" Value="" />
        <asp:HiddenField runat="server" ID="hdnRemoveNbhdIds" Value="" />
        <div class="viewlist-panel">
            <table id="gvResult" class="scroll">
                <thead>
                    <tr>
                        <td>temp</td>
                    </tr>
                </thead>
            </table>
            <div id="btnContainer" class="btnContainer">
                <asp:HiddenField ID="checkBtnClick" runat="server" />
                <button id="btnOkay" onclick="return viewInQc()" disabled="disabled">Okay</button>
                <button id ="btncancel" onclick="return closeViewList();" Style="margin-left: 10px;">Cancel</button>
            </div>
        </div>
    </div>
    <div class="alterPopup" style ="display:none">
        <p>Some properties in the Priority List being uploaded <b>do not have a Priority set in Column B</b>.</p>
        <p>Please assign a Priority to all properties and then upload the Priorities list again.</p>
        <br />
        <p>Choose one of the options below or the cancel option if you would like to assign Priorities <br />
             to the properties, and then upload the Priority List again.</p>
        
        <input type="checkbox" name="high" value="High" onchange="CheckBoxCheck(this)"/>Set these properties as High Priority.<br>
        <input type="checkbox" name="urgent" value="Urgent" onchange ="CheckBoxCheck(this)"/>Set these properties as Urgent Priority.<br>
        <div style="float:right">
            <button type="button" name ="Proceed" id ="btnProceed" onclick ="ProceedToSecondStep()">Proceed</button>
            <button type="button" name ="Cancel" id ="btnCancelPop" onclick ="CancelSecondStep()">Cancel</button>
        </div>

    </div>
</asp:Content>


