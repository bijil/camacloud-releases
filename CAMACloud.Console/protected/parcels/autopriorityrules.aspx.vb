﻿Imports System.IO

Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Data

Imports System.Web.Services
Imports CAMACloud.BusinessLogic
Imports System.Text.RegularExpressions

Public Class autopriorityrules
    Inherits System.Web.UI.Page
    Public ReadOnly Property EnableNewPriorities As Boolean
        Get
            Dim EnableNewPriority = False
            Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM ClientSettings WHERE Name = 'EnableNewPriorities' AND Value = 1")
            If dr IsNot Nothing Then
                EnableNewPriority = True
            End If
            Return EnableNewPriority
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim scriptManager As ScriptManager = ScriptManager.GetCurrent(Me.Page)
        scriptManager.RegisterPostBackControl(Me.gvRuleList)
        If Not IsPostBack Then
            LoadPriorityDDL()
            LoadDataGrid()

            FinalResult.Visible = False
            parcelgrid.Visible = False
        End If
        'scriptManager.RegisterPostBackControl(Me.lbDowload)
        'scriptManager.RegisterPostBackControl(Me.lbMpsDowload)
        Dim ruleID As Integer
        Dim PageMode As String = "New"


        If Not [String].IsNullOrEmpty(Request("__EVENTTARGET")) AndAlso Not [String].IsNullOrEmpty(Request("__EVENTARGUMENT")) Then
            If Request("__EVENTTARGET") = "updatesave" Then
                HideLinkBt_Click(sender, e)
            End If
        End If
        If Not IsPostBack Then

            Me.ViewState("EditRowNo") = -1
            Dim tableExist = Database.Tenant.GetIntegerValue("Select TOP 1 1 FROM AutoPrioritySettings ")
            If tableExist = 1 Then
            Else
                Database.Tenant.Execute("INSERT INTO AutoPrioritySettings(EnableOnRefresh,EnableOneTime,EnableDateRange,EnableException) VALUES (1,1,1,1)")
                Database.Tenant.Execute("UPDATE Parcel SET Mps=1 WHERE priority in(1,2)")
            End If
            If Not Request.QueryString("Id") Is Nothing Then
                ruleID = Request.QueryString("Id")
                Me.ViewState.Add("RuleId", ruleID)
                PageMode = "Edit"
            End If
            LoadInitial()

        Else
            If (Me.ViewState("RuleId") IsNot Nothing) Then
                hdnRuleId.Value = Me.ViewState("RuleId")
                hdnUpdateFlag.Value = "1"
            End If
        End If
        RunScript("makeFieldsDraggable();")
    End Sub

    Sub LoadPriorityDDL()

        If EnableNewPriorities Then
            ddlPriority.InsertItem("Normal", 1, 0)
            ddlPriority.InsertItem("Medium", 2, 1)
            ddlPriority.InsertItem("High", 3, 2)
            ddlPriority.InsertItem("Urgent", 4, 3)
            ddlPriority.InsertItem("Critical", 5, 4)
        Else
            ddlPriority.InsertItem("High", 1, 0)
            ddlPriority.InsertItem("Urgent", 2, 1)
        End If

    End Sub

    Enum ViewMode
        List
        Edit
    End Enum
    Protected Sub addnew_Click(sender As Object, e As System.EventArgs) Handles addnewrule.Click
        ClearForm()
        Me.ViewState("RuleId") = 0
        setfeilds(0)
        SwitchMode(ViewMode.Edit)
        CheckEnable.Checked = True
        LoadFields()
    End Sub
    Protected Sub recancel_Click(sender As Object, e As System.EventArgs) Handles reOrderCancel.Click
        gvRuleList.PageSize = 10
        LoadDataGrid()
        RunScript("refreshSortButtons();")
    End Sub

    Protected Sub reOrder_Click(sender As Object, e As System.EventArgs) Handles reOrder.Click
        Dim count As Integer = 10
        If (Me.ViewState("RulesHdr") IsNot Nothing) Then
            Dim dt As New DataTable
            dt = Database.Tenant.GetDataTable("SELECT Id AS RuleId,Title,CASE WHEN Priority=5 THEN 'Critical' WHEN Priority=4 THEN 'Urgent' WHEN Priority=3 THEN 'High' WHEN Priority=2 THEN 'Medium'  WHEN Priority=1 THEN 'Normal' ELSE 'Proximity' END AS priority,dbo.GetLocalDate(CreatedDate) AS CreatedDate,IsActive,ApplyRuleOneTime,CASE WHEN ApplyRuleOneTime = 1 THEN 1 WHEN IsDateApplicable = 1 AND GETUTCDATE() BETWEEN DateApplicableFrom AND DateApplicableTo THEN 1 ELSE 0 END AS ShowApply FROM AutopriorityRuleHdr ORDER BY ExecutionOrder")
            If dt.Rows.Count > 10 Then
                count = dt.Rows.Count + 5
            End If
            gvRuleList.PageSize = count
            gvRuleList.DataSource = dt
            gvRuleList.DataBind()
        End If
        RunScript("reOrderRows();")
    End Sub

    Sub SwitchMode(mode As ViewMode)
        pnl1.Visible = mode = ViewMode.List
        editpanel.Visible = mode = ViewMode.Edit
    End Sub

    Protected Sub btnReturntolist_Click(sender As Object, e As System.EventArgs) Handles returnToList.Click

        SwitchMode(ViewMode.List)
        ClearForm()
        LoadDataGrid()
    End Sub
    Sub ClearForm()
        txtTitle.Text = ""
        CheckEnable.Checked = False
        ddlFieldCategory.SelectedIndex = 0
        ddlField.Items.Clear()
        ddlCondition.SelectedIndex = 0
        txtValuesString.Text = ""
        txtValuesNumber.Text = ""
        txtValuesDate.Text = ""
        txtValuesNumber2.Text = ""
        txtValuesDate2.Text = ""
        txtValuesString.Visible = True
        txtValuesNumber.Visible = False
        txtValuesDate.Visible = False
        txtValuesNumber2.Visible = False
        txtValuesDate2.Visible = False
        DdlOperator.SelectedIndex = 0
        ddlPriority.SelectedIndex = 0
        txtAlert1.Text = ""
        txtAlert.Text = ""
        ApplyDateRange.Checked = False
        ApplyAutomatic.Checked = False
        txtReviewAgeingDays.Text = ""
        dtFrom.Text = ""
        dtTo.Text = ""
        divDateRangeSub.Visible = False
        txtReviewAgeingDays.Text = ""
        divExceptionsSub.Visible = False
        enableExceptions.Checked = False
        lblAdd.Text = "Add"
        ddlCondition.Items.FindByValue("3").Enabled = True
        ddlCondition.Items.FindByValue("4").Enabled = True
        ddlCondition.Items.FindByValue("5").Enabled = True
        ddlCondition.Items.FindByValue("6").Enabled = True
        ddlCondition.Items.FindByValue("10").Enabled = True
        txtValuesString.Enabled = True
        txtValuesNumber.Enabled = True
        txtValuesDate.Enabled = True
        txtValuesNumber2.Enabled = True
        txtValuesDate2.Enabled = True

    End Sub

    Sub LoadDataGrid()
        If dtRules Is Nothing Then

            If EnableNewPriorities Then
                dtRules = Database.Tenant.GetDataTable("SELECT Id AS RuleId,Title,CASE WHEN Priority=5 THEN 'Critical' WHEN Priority=4 THEN 'Urgent' WHEN Priority=3 THEN 'High' WHEN Priority=2 THEN 'Medium'  WHEN Priority=1 THEN 'Normal' ELSE 'Proximity' END AS priority,dbo.GetLocalDate(CreatedDate) AS CreatedDate,IsActive,ApplyRuleOneTime,CASE WHEN ApplyRuleOneTime = 1 THEN 1 WHEN IsDateApplicable = 1 AND GETUTCDATE() BETWEEN DateApplicableFrom AND DateApplicableTo THEN 1 ELSE 0 END AS ShowApply FROM AutopriorityRuleHdr ORDER BY ExecutionOrder")
            Else
                dtRules = Database.Tenant.GetDataTable("SELECT Id AS RuleId,Title,CASE  WHEN Priority=2 THEN 'Urgent' WHEN Priority=1 THEN 'High'  ELSE 'Proximity' END AS priority,dbo.GetLocalDate(CreatedDate) AS CreatedDate,IsActive,ApplyRuleOneTime,CASE WHEN ApplyRuleOneTime = 1 THEN 1 WHEN IsDateApplicable = 1 AND GETUTCDATE() BETWEEN DateApplicableFrom AND DateApplicableTo THEN 1 ELSE 0 END AS ShowApply FROM AutopriorityRuleHdr ORDER BY ExecutionOrder")

            End If
        End If
        If dtRules.Rows.Count = 0 Then
            norulemsg.Text = "No Rules are Created."
        Else
            Me.ViewState.Add("RulesHdr", dtRules)
            norulemsg.Text = ""
            gvRuleList.DataSource = dtRules
            gvRuleList.DataBind()
        End If

        If (dtRules.Rows.Count > 1) Then
            reOrder.Visible = True
        Else
            reOrder.Visible = False
        End If
    End Sub

    Sub LoadInitial()
        Dim dtCategory As DataTable = Database.Tenant.GetDataTable("SELECT DISTINCT Id,Name FROM FieldCategory UNION ALL SELECT DISTINCT id, 'Parcel Table' as Name FROM DataSourceTable WHERE Name = " + Database.Tenant.Application.ParcelTable.ToSqlValue)
        ddlFieldCategory.FillFromTable(dtCategory, True, "---  Field Category  ---")

        divRuleResult.Visible = False
        divMpsResult.Visible = False
        If Database.Tenant.GetTopRow("SELECT TOP 1 EnableOnRefresh FROM AutoPrioritySettings")(0) = True Then
            divAutomatic.Visible = True
        Else
            divAutomatic.Visible = False
        End If

        If Database.Tenant.GetTopRow("SELECT TOP 1 EnableOnRefresh FROM AutoPrioritySettings")(0) = True Then
            divAutomatic.Visible = True
        Else
            divAutomatic.Visible = False
        End If
        If Database.Tenant.GetTopRow("SELECT  TOP 1 EnableDateRange FROM AutoPrioritySettings")(0) = True Then
            divDateRange.Visible = True
            divDateRangeSub.Visible = False
        Else
            divDateRange.Visible = False
            divDateRangeSub.Visible = False
        End If

        If Database.Tenant.GetTopRow("SELECT  TOP 1 EnableException FROM AutoPrioritySettings")(0) = True Then
            divExceptions.Visible = True
            divExceptionsSub.Visible = False
        Else
            divExceptions.Visible = False
            divExceptionsSub.Visible = False
        End If
        txtValuesString.Visible = True
        txtValuesNumber.Visible = False
        txtValuesDate.Visible = False
        txtValuesNumber2.Visible = False
        txtValuesDate2.Visible = False

        If (Me.ViewState("RuleId") IsNot Nothing) Then
            hdnRuleId.Value = Me.ViewState("RuleId")
        End If

        Dim parcelTableId As Integer = Database.Tenant.GetIntegerValueOrInvalid("SELECT Id FROM DataSourceTable WHERE Name = " + Database.Tenant.Application.ParcelTable.ToSqlValue)
        Dim dtField As DataTable
        dtField = Database.Tenant.GetDataTable("SELECT *, TableId As FieldTableId FROM DataSourceField WHERE TableId = " & parcelTableId & " ORDER BY Name")

        rpFieldslist.DataSource = dtField
        rpFieldslist.DataBind()
    End Sub

    Private Sub ddlFieldCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFieldCategory.SelectedIndexChanged
        LoadFields()
        RunScript("finalizePage();")
        RunScript("hideMask();")
    End Sub

    Sub LoadFields(Optional FirstLoad As Integer = 0)

        Dim dtFields As DataTable
        Dim query As String

        If ddlFieldCategory.Items.Count = 0 Then
            Return
        End If
        If ddlFieldCategory.SelectedIndex = 0 Then
            query = "SELECT Id,COALESCE(DisplayLabel, '') +' ('+COALESCE('' + AssignedName, '')+')' FROM DataSourceField WHERE IsFavorite = 1 ORDER BY FavoriteOrdinal".FormatString(ddlFieldCategory.SelectedValue)
            dtFields = Database.Tenant.GetDataTable(query)
            ddlField.FillFromTable(dtFields, True, "---  Select Field  ---")
            Dim ruleid = Me.ViewState("RuleId")
            Dim parcelTableId As Integer = Database.Tenant.GetIntegerValueOrInvalid("SELECT Id FROM DataSourceTable WHERE Name = " + Database.Tenant.Application.ParcelTable.ToSqlValue)
            Dim dtField As DataTable
            dtField = Database.Tenant.GetDataTable("SELECT *, TableId As FieldTableId FROM DataSourceField WHERE TableId = " & parcelTableId & " and CategoryId is not null ORDER BY Name")
            If FirstLoad = 1 Then
                Dim DistTable As DataTable = Database.Tenant.GetDataTable("SELECT distinct TableID from AutopriorityRuleDtl where Id={0}".SqlFormatString(ruleid))
                If DistTable.Rows.Count = 1 Then
                    parcelTableId = DistTable.Rows(0)(0)
                    dtField = Database.Tenant.GetDataTable("SELECT *, TableId As FieldTableId FROM DataSourceField WHERE TableId = " & parcelTableId & " and CategoryId is not null ORDER BY Name")
                End If
            End If
            rpFieldslist.DataSource = dtField
            rpFieldslist.DataBind()

        ElseIf ddlFieldCategory.SelectedValue = "-2" Then
            query = "SELECT Id,COALESCE(DisplayLabel, '') +' ('+COALESCE('' + AssignedName, '')+')' FROM DataSourceField WHERE SourceTable = '_photo_meta_data' ORDER BY AssignedName".FormatString(ddlFieldCategory.SelectedValue)
            dtFields = Database.Tenant.GetDataTable(query)
            ddlField.FillFromTable(dtFields, True, "---  Select Field  ---")
        ElseIf ddlFieldCategory.SelectedItem.Text.Trim() = "Parcel Table" Then
            query = "SELECT Id,COALESCE(DisplayLabel, '') +' ('+COALESCE('' + AssignedName, '')+')' FROM datasourcefield WHERE SourceTable = " + Database.Tenant.Application.ParcelTable.ToSqlValue + " ORDER BY CategoryOrdinal"
            dtFields = Database.Tenant.GetDataTable(query)
            ddlField.FillFromTable(dtFields, True, "---  Select Field  ---")
            Dim parcelTableId As Integer = Database.Tenant.GetIntegerValueOrInvalid("SELECT Id FROM DataSourceTable WHERE Name = " + Database.Tenant.Application.ParcelTable.ToSqlValue)
            Dim dtField As DataTable
            dtField = Database.Tenant.GetDataTable("SELECT *, TableId As FieldTableId FROM DataSourceField WHERE TableId = " & parcelTableId & " ORDER BY Name")
            rpFieldslist.DataSource = dtField
            rpFieldslist.DataBind()
        Else
            query = "SELECT Id,COALESCE(DisplayLabel, '') +' ('+COALESCE('' + AssignedName, '')+')' FROM datasourcefield WHERE CategoryId = {0} ORDER BY CategoryOrdinal".FormatString(ddlFieldCategory.SelectedValue)
            dtFields = Database.Tenant.GetDataTable(query)
            ddlField.FillFromTable(dtFields, True, "---  Select Field  ---")

            Dim dtField As DataTable
            query = "SELECT *, TableId As FieldTableId FROM DataSourceField WHERE CategoryId = {0} ORDER BY Name".FormatString(ddlFieldCategory.SelectedValue)
            dtField = Database.Tenant.GetDataTable(query)
            rpFieldslist.DataSource = Nothing
            rpFieldslist.DataSource = dtField
            rpFieldslist.DataBind()
        End If
    End Sub

    Private Sub ddlField_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlField.SelectedIndexChanged
        ddlCondition.SelectedIndex = 0
        txtValuesString.Text = ""
        txtValuesNumber.Text = ""
        txtValuesDate.Text = ""
        txtValuesNumber2.Text = ""
        txtValuesDate2.Text = ""
        LoadConditions()
        RunScript("finalizePage();")
        RunScript("hideMask();")
    End Sub

    Private Sub ddlCondition_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCondition.SelectedIndexChanged
        Dim query As String
        txtValuesString.Enabled = True
        txtValuesNumber.Enabled = True
        txtValuesDate.Enabled = True
        txtValuesNumber2.Enabled = True
        txtValuesDate2.Enabled = True
        LoadConditions()
        If (ddlCondition.SelectedValue = "10") Then
            If ddlField.SelectedIndex = 0 Or ddlField.SelectedIndex = -1 Then
            ElseIf ddlField.SelectedValue = "-2" Then
            Else
                query = "Select CASE WHEN  Datatype IN(2,3,7,8,9,10) THEN 2  WHEN Datatype=4 THEN 3 ELSE 1 END AS TYPE1  FROM datasourcefield WHERE Id = {0}".FormatString(ddlField.SelectedValue)
                Dim DataType As Integer = Database.Tenant.GetIntegerValue(query)
                Select Case DataType
                    Case 2
                        ddlCondition.Items.FindByValue("3").Enabled = True
                        ddlCondition.Items.FindByValue("4").Enabled = True
                        ddlCondition.Items.FindByValue("5").Enabled = True
                        ddlCondition.Items.FindByValue("6").Enabled = True
                        ddlCondition.Items.FindByValue("10").Enabled = True
                        txtValuesString.Visible = False
                        txtValuesNumber.Visible = True
                        txtValuesDate.Visible = False
                        txtValuesNumber2.Visible = True
                        txtValuesDate2.Visible = False
                    Case 3
                        ddlCondition.Items.FindByValue("3").Enabled = True
                        ddlCondition.Items.FindByValue("4").Enabled = True
                        ddlCondition.Items.FindByValue("5").Enabled = True
                        ddlCondition.Items.FindByValue("6").Enabled = True
                        ddlCondition.Items.FindByValue("10").Enabled = True
                        txtValuesString.Visible = False
                        txtValuesNumber.Visible = False
                        txtValuesDate.Visible = True
                        txtValuesNumber2.Visible = False
                        txtValuesDate2.Visible = True
                End Select
            End If
        End If
        If (ddlCondition.SelectedValue = "8" Or ddlCondition.SelectedValue = "9") Then
            txtValuesString.Enabled = False
            txtValuesString.Text = ""
            txtValuesNumber.Enabled = False
            txtValuesNumber.Text = ""
            txtValuesDate.Enabled = False
            txtValuesDate.Text = ""
            txtValuesNumber2.Enabled = False
            txtValuesNumber2.Text = ""
            txtValuesDate2.Enabled = False
            txtValuesDate2.Text = ""
        End If
        RunScript("finalizePage();")
        RunScript("hideMask();")
    End Sub



    Sub LoadConditions()
        Dim query As String

        If ddlField.Items.Count = 0 Then
            Return
        End If

        If ddlField.SelectedIndex = 0 Then
        ElseIf ddlField.SelectedValue = "-2" Then
        Else
            query = "Select CASE WHEN  Datatype IN(2,3,7,8,9,10) THEN 2  WHEN Datatype=4 THEN 3 ELSE 1 END AS TYPE1  FROM datasourcefield WHERE Id = {0}".FormatString(ddlField.SelectedValue)
            Dim DataType As Integer = Database.Tenant.GetIntegerValue(query)
            Select Case DataType
                Case 1
                    ddlCondition.Items.FindByValue("3").Enabled = False
                    ddlCondition.Items.FindByValue("4").Enabled = False
                    ddlCondition.Items.FindByValue("5").Enabled = False
                    ddlCondition.Items.FindByValue("6").Enabled = False
                    ddlCondition.Items.FindByValue("10").Enabled = False
                    txtValuesString.Visible = True
                    txtValuesNumber.Visible = False
                    txtValuesDate.Visible = False
                    txtValuesNumber2.Visible = False
                    txtValuesDate2.Visible = False
                Case 2
                    ddlCondition.Items.FindByValue("3").Enabled = True
                    ddlCondition.Items.FindByValue("4").Enabled = True
                    ddlCondition.Items.FindByValue("5").Enabled = True
                    ddlCondition.Items.FindByValue("6").Enabled = True
                    ddlCondition.Items.FindByValue("10").Enabled = True
                    txtValuesString.Visible = False
                    txtValuesNumber.Visible = True
                    txtValuesDate.Visible = False
                    txtValuesNumber2.Visible = False
                    txtValuesDate2.Visible = False
                Case 3
                    ddlCondition.Items.FindByValue("3").Enabled = True
                    ddlCondition.Items.FindByValue("4").Enabled = True
                    ddlCondition.Items.FindByValue("5").Enabled = True
                    ddlCondition.Items.FindByValue("6").Enabled = True
                    ddlCondition.Items.FindByValue("10").Enabled = True
                    txtValuesString.Visible = False
                    txtValuesNumber.Visible = False
                    txtValuesDate.Visible = True
                    txtValuesNumber2.Visible = False
                    txtValuesDate2.Visible = False
            End Select
        End If
    End Sub

    Public Sub LinkAdd_Click(sender As Object, e As EventArgs)
        RunScript("showMask();")
        Dim condition As String = ""
        Dim fieldName As String = ""
        Dim fieldID As Integer
        Dim tableID As Integer
        Dim dt As New DataTable
        Dim dt1 As New DataTable
        Dim Rowno As Integer = 1
        Dim conditionID As Integer
        Dim value1 As String
        Dim value2 As String
        Dim EditRowno, i As Integer
        Dim Operatorstr As String = ""
        Dim RowExist As Boolean = False

        value1 = DBNull.Value.ToString()
        value2 = DBNull.Value.ToString()

        If ddlCondition.SelectedIndex <> 0 And ddlCondition.SelectedValue <> "-2" And ddlField.SelectedIndex <> 0 And ddlField.SelectedValue <> "-2" And ddlField.SelectedIndex <> "-1" Then
            If (Me.ViewState("ConditionViewState") IsNot Nothing) Then
                dt1 = Me.ViewState("ConditionViewState")
                ''Rowno = dt.Rows.Count + 1
            Else
                'dt.Columns.AddRange(New DataColumn(5) {New DataColumn("Rowno"), New DataColumn("TableId"), New DataColumn("FieldId"), New DataColumn("FieldName"), New DataColumn("Condition"), New DataColumn("ValueString")})
                dt.Columns.AddRange(New DataColumn(9) {New DataColumn("Rowno"), New DataColumn("TableId"), New DataColumn("FieldId"), New DataColumn("FieldName"), New DataColumn("Condition"), New DataColumn("ValueString"), New DataColumn("ConditionId"), New DataColumn("Value1"), New DataColumn("Value2"), New DataColumn("Operatorstr")})
                dt1.Columns.AddRange(New DataColumn(10) {New DataColumn("ActRowno"), New DataColumn("Rowno"), New DataColumn("TableId"), New DataColumn("FieldId"), New DataColumn("FieldName"), New DataColumn("Condition"), New DataColumn("ValueString"), New DataColumn("ConditionId"), New DataColumn("Value1"), New DataColumn("Value2"), New DataColumn("Operatorstr")})
            End If

            If ddlField.SelectedIndex = 0 Then
            ElseIf ddlField.SelectedValue = "-2" Then
            Else
                fieldName = Database.Tenant.GetStringValue("Select Name FROM datasourcefield WHERE Id = {0} ".FormatString(ddlField.SelectedValue))
                fieldID = ddlField.SelectedValue
                tableID = Database.Tenant.GetIntegerValue("Select TableId FROM datasourcefield WHERE Id = {0} ".FormatString(ddlField.SelectedValue))
            End If

            If ddlCondition.SelectedIndex <> 0 And ddlCondition.SelectedValue <> "-2" Then
                condition = getConditionSqlString(ddlCondition.SelectedItem.Text)
                conditionID = ddlCondition.SelectedValue
            End If

            If DdlOperator.SelectedIndex <> 0 Then
                Operatorstr = DdlOperator.SelectedItem.Text.Trim()
            End If

            Dim values As String
            Dim query As String
            query = "Select CASE WHEN  Datatype IN(2,3,7,8,9,10) THEN 2  WHEN Datatype=4 THEN 3 ELSE 1 END AS TYPE1  FROM datasourcefield WHERE Id = {0}".FormatString(ddlField.SelectedValue)
            Dim DataType As Integer = Database.Tenant.GetIntegerValue(query)
            Select Case DataType
                Case 1
                    values = txtValuesString.Text
                    value1 = txtValuesString.Text.ToString()
                    value2 = DBNull.Value.ToString()
                Case 2
                    If condition = "BETWEEN" Then
                        values = txtValuesNumber.Text + " AND " + txtValuesNumber2.Text
                        value1 = txtValuesNumber.Text.ToString()
                        value2 = txtValuesNumber2.Text.ToString()
                    ElseIf condition = "IN" Then
                        values = txtValuesNumber.Text
                        values = "('" + values + "')"
                        value1 = txtValuesNumber.Text.ToString()
                        value2 = DBNull.Value.ToString()
                    Else
                        values = txtValuesNumber.Text
                        value1 = txtValuesNumber.Text.ToString()
                        value2 = DBNull.Value.ToString()
                    End If
                Case 3
                    If condition = "BETWEEN" Then
                        values = "'" + txtValuesDate.Text + "' AND '" + txtValuesDate2.Text + "'"
                        value1 = txtValuesDate.Text.ToString()
                        value2 = txtValuesDate2.Text.ToString()
                    ElseIf condition = "=" Then
                        values = txtValuesDate.Text
                        value1 = txtValuesDate.Text.ToString()
                        value2 = DBNull.Value.ToString()
                    Else
                        values = txtValuesDate.Text
                        value1 = txtValuesDate.Text.ToString()
                        value2 = DBNull.Value.ToString()
                    End If
            End Select

            If values.Trim <> "" Then
                values = getValueSqlString(condition, values, DataType)
            End If
            Rowno = dt1.Rows.Count + 1

            If Me.ViewState("EditRowNo") >= 0 And dt1.Rows.Count > 0 Then
                If Me.ViewState("EditRowNo") Is Nothing Then
                    Me.ViewState("EditRowNo") = dt1.Rows.Count + 1
                End If
                EditRowno = 0
                For Each row As DataRow In dt1.Rows
                    If row("ActRowno") = Me.ViewState("EditRowNo") Then
                        EditRowno = row("Rowno") - 1
                        RowExist = True
                    End If
                Next

                Dim dr As DataRow
                dr = dt1.NewRow()
                dr(0) = Me.ViewState("EditRowNo")
                dr(1) = Me.ViewState("EditRowNo")
                dr(2) = tableID
                dr(3) = fieldID
                dr(4) = fieldName
                dr(5) = condition
                dr(6) = values
                dr(7) = conditionID
                dr(8) = value1
                dr(9) = value2
                dr(10) = Operatorstr
                Me.ViewState.Add("Value1", value1)
                Me.ViewState.Add("Value2", value2)
                Me.ViewState.Add("Operator", Operatorstr)
                If RowExist = True Then
                    dt1.Rows.RemoveAt(EditRowno)
                    dt1.Rows.InsertAt(dr, EditRowno)
                    ConditionAudit("edit", txtTitle.Text, EditRowno + 1)
                Else
                    dt1.Rows.InsertAt(dr, Me.ViewState("EditRowNo") - 1)
                End If
                Me.ViewState("RemovedRowNo") = Nothing
                dt1.AcceptChanges()
                i = 1
                For Each row As DataRow In dt1.Rows
                    row("Rowno") = i
                    i += 1
                Next
                Me.ViewState("EditRowNo") = -1
            Else
                dt1.Rows.Add(Rowno, Rowno, tableID, fieldID, fieldName, condition, values, conditionID, value1, value2, Operatorstr)
                Me.ViewState("EditRowNo") = -1
                Dim title As String = txtTitle.Text
                ConditionAudit("editnew", title, Rowno)
            End If
            Me.ViewState.Add("ConditionViewState", dt1)
            gvCondition.DataSource = dt1
            gvCondition.DataBind()
            ddlField.SelectedIndex = 0
            ddlCondition.SelectedIndex = 0
            DdlOperator.SelectedIndex = 0
            txtValuesString.Text = ""
            txtValuesNumber.Text = ""
            txtValuesNumber2.Text = ""
            txtValuesDate.Text = ""
            txtValuesDate2.Text = ""
            txtValuesString.Visible = True
            txtValuesNumber.Visible = False
            txtValuesDate.Visible = False
            txtValuesNumber2.Visible = False
            txtValuesDate2.Visible = False
            ddlCondition.Items.FindByValue("3").Enabled = True
            ddlCondition.Items.FindByValue("4").Enabled = True
            ddlCondition.Items.FindByValue("5").Enabled = True
            ddlCondition.Items.FindByValue("6").Enabled = True
            ddlCondition.Items.FindByValue("10").Enabled = True
            Rowno = +1
        End If
        lblAdd.Text = "Add"
        RunScript("hideMask();")
        RunScript("finalizePage();")
    End Sub

    Public Function getConditionSqlString(ByVal Condition As String) As String
        Dim SqlString As String
        Select Case Condition
            Case "Equal to"
                SqlString = "="
            Case "Not Equal to"
                SqlString = "<>"
            Case "Greater than"
                SqlString = ">"
            Case "Less than"
                SqlString = "<"
            Case "Greater than or Equal to"
                SqlString = ">="
            Case "Less than or equal to"
                SqlString = "<="
            Case "IN"
                SqlString = "IN"
            Case "Is NULL"
                SqlString = "IS NULL"
            Case "Is Not NULL"
                SqlString = "IS NOT NULL"
            Case "BETWEEN"
                SqlString = "BETWEEN"

        End Select

        Return SqlString

    End Function


    Public Function getValueSqlString(ByVal Condition As String, ByVal Values As String, ByVal DataType As Integer) As String

        If (DataType = 1 Or DataType = 3) Then
            Select Case Condition
                Case "="
                    Values = "'" + Values + "'"
                Case "<>"
                    Values = "'" + Values + "'"
                Case "IN"
                    Values = "('" + Values.Replace(",", "','") + "')"
            End Select

            If (DataType = 3 And (Condition = ">" Or Condition = "<" Or Condition = ">=" Or Condition = "<=")) Then
                Values = "'" + Values + "'"
            End If

        End If

        Return Values

    End Function


    Public Sub btnVerify_Click(sender As Object, e As EventArgs)
        Dim ruleId As Integer
        Dim dtResult As New DataTable
        Dim datetFrom As String
        Dim datetTo As String
        Dim datetFrom1 As Date
        Dim datetTo1 As Date
        Dim includeReviewed As Integer
        Dim reviewAgeingDays As Integer
        Dim applyRuleOneTime As Integer
        Dim applyOnRefresh As Integer
        Dim IsDateApplicable As Integer
        Dim dtMpsResult As New DataTable
        Dim IsActive As Integer = 0

        RunScript("showMask();")

        If (Me.ViewState("ConditionViewState") IsNot Nothing) Then
            Dim dtDtl1 As New DataTable
            Dim dtDtl As New DataTable
            dtDtl.Columns.AddRange(New DataColumn(9) {New DataColumn("Rowno"), New DataColumn("TableId"), New DataColumn("FieldId"), New DataColumn("FieldName"), New DataColumn("Condition"), New DataColumn("ValueString"), New DataColumn("ConditionId"), New DataColumn("Value1"), New DataColumn("Value2"), New DataColumn("Operatorstr")})
            Dim DR As DataRow
            dtDtl1 = Me.ViewState("ConditionViewState")

            Dim i As Integer = 1
            For Each row As DataRow In dtDtl1.Rows
                DR = dtDtl.NewRow()
                DR(0) = i
                DR(1) = row(2)
                DR(2) = row(3)
                DR(3) = row(4)
                DR(4) = row(5)
                DR(5) = row(6)
                DR(6) = row(7)
                DR(7) = row(8)
                DR(8) = row(9)
                DR(9) = row(10)
                dtDtl.Rows.Add(DR)
                i += 1
            Next
            dtDtl.AcceptChanges()
            Dim dtHdr As New DataTable
            Dim title As String = txtTitle.Text
            Dim priority As Integer = ddlPriority.SelectedValue
            Dim alert As String = txtAlert1.Text
            Dim CurrDate As Date = Today

            If enableExceptions.Checked = True Then
                includeReviewed = 1
                If txtReviewAgeingDays.Text.Trim <> "" Then
                    reviewAgeingDays = Convert.ToInt32(txtReviewAgeingDays.Text)
                End If
            Else
                includeReviewed = 0
            End If
            If ApplyAutomatic.Checked = False And ApplyDateRange.Checked = False Then
                applyRuleOneTime = 1
            Else
                applyRuleOneTime = 0
            End If
            If ApplyAutomatic.Checked = True Then
                applyOnRefresh = 1
            Else
                applyOnRefresh = 0
            End If

            If ApplyDateRange.Checked = True Then
                IsDateApplicable = 1
                datetFrom = dtFrom.Text
                datetTo = dtTo.Text
                datetFrom1 = dtFrom.Text
                datetTo1 = dtTo.Text
            Else
                IsDateApplicable = 0
                datetFrom = ""
                datetTo = ""
            End If
            If CheckEnable.Checked = True Then
                IsActive = 1
            End If
            If (Me.ViewState("RuleId") IsNot Nothing) Then
                ruleId = Me.ViewState("RuleId")
            End If
            Dim checkRule = Database.Tenant.GetIntegerValue("SELECT 1 FROM AutopriorityRuleHdr WHERE Title='" & title & "' AND Id <>'" & ruleId & "' ")
            If checkRule = 1 Then
                Dim ErrMsg As String = ""
                ErrMsg = "There is already a rule exist with name " + title + " ."
                RunScript("alertMsg('" + ErrMsg + "');")
                RunScript("hideMask();")
                Exit Sub
            End If
            Dim createdBy As String = Database.System.GetStringValue("SELECT au.UserName FROM  aspnet_Users au INNER JOIN aspnet_Applications a On a.ApplicationId=au.ApplicationId WHERE  a.ApplicationName='" & Membership.ApplicationName & "' And au.UserName='" & Membership.GetUser().ToString() & "'")
            Dim CurrentDate As String = Database.Tenant.GetStringValue("SELECT GETUTCDATE()")
            'Dim createdDate As String = CurrentDate
            'Dim lastModDatetime As String = CurrentDate
            dtHdr.Columns.AddRange(New DataColumn(13) {New DataColumn("RuleId"), New DataColumn("Title"), New DataColumn("Priority"), New DataColumn("Alert"), New DataColumn("IsDateApplicable"), New DataColumn("DateApplicableFrom"), New DataColumn("DateApplicableTo"), New DataColumn("IncludeReviewed"), New DataColumn("ReviewAgeingDays"), New DataColumn("CreatedBy"), New DataColumn("CreatedDate"), New DataColumn("ApplyRuleOneTime"), New DataColumn("ApplyOnRefresh"), New DataColumn("LastModDateTime")})
            dtHdr.Rows.Add(ruleId, title, priority, alert, IsDateApplicable, datetFrom, datetTo, includeReviewed, reviewAgeingDays, createdBy, CurrentDate, applyRuleOneTime, applyOnRefresh, CurrentDate)

            Dim dtResultOld As New DataTable

            If dtDtl.Rows.Count > 0 Then
                Try
                    If ApplyDateRange.Checked = True Then
                        If ((datetFrom1 <= CurrDate) And (datetTo1 >= CurrDate)) Then
                        Else
                            If (Me.ViewState("RuleId") IsNot Nothing) Then
                                ruleId = Me.ViewState("RuleId")
                            End If
                            CheckforEdit(ruleId)
                            Database.Tenant.ExecuteWithDataTable("cc_autoPriorityRule", dtHdr, dtDtl)
                            ruleId = Database.Tenant.GetIntegerValue("SELECT Id FROM AutopriorityRuleHdr WHERE Title='" & title & "'")
                            If ddlFieldCategory.SelectedIndex > 0 Then
                                Database.Tenant.Execute("UPDATE AutopriorityRuleHdr SET  Category='" + ddlFieldCategory.SelectedItem.ToString().Replace("'", "''") + "' where Id={0} ".SqlFormatString(ruleId))
                            End If
                            Database.Tenant.Execute("UPDATE AutopriorityRuleHdr SET IsActive=" + IsActive.ToString() + " where Id={0} ".SqlFormatString(ruleId))
                            Dim DescList As New List(Of String)()
                            If (Me.ViewState("DescrptionViewState") IsNot Nothing) Then
                                DescList = Me.ViewState("DescrptionViewState")
                                For Each Desc As String In DescList
                                    Dim description = Desc
                                    SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), description)
                                Next
                            End If
                            Me.ViewState("DescrptionViewState") = Nothing
                            Me.ViewState.Add("RuleId", ruleId)
                            hdnRuleId.Value = Me.ViewState("RuleId")
                            Dim ErrMsg As String = ""
                            ErrMsg = "Rule has been saved. The rule will only afftect the parcels while saving the rule within the dates given in the Date Range."
                            RunScript("alertMsg('" + ErrMsg + "');")
                            RunScript("hideMask();")
                            Exit Sub
                        End If
                    End If
                    dtResult = Database.Tenant.GetDTWithDT("cc_autoProrityParcelList", dtDtl, dtHdr, "count")
                    Me.ViewState.Add("verifyResult", dtResult.Rows(0)(0))

                    dtMpsResult = Database.Tenant.GetDTWithDT("cc_autoProrityMPSProc", dtDtl, dtHdr, "count")
                    Me.ViewState.Add("MpsResult", dtMpsResult.Rows(0)(0))


                    Me.ViewState.Add("SaveHdr", dtHdr)
                    Me.ViewState.Add("SaveDtl", dtDtl)
                    If (ruleId = 0 And dtResult.Rows.Count = 0) Then
                        Dim ErrMsg As String = ""
                        ErrMsg = "No parcels found by this rule."
                        RunScript("alertMsg('" + ErrMsg + "');")
                    End If
                    If ruleId = 0 Then
                        Database.Tenant.ExecuteWithDataTable("cc_autoPriorityRule", dtHdr, dtDtl)
                        Dim Description As String = "New AutoPriority rule - " + title + " created"
                        SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
                        ruleId = Database.Tenant.GetIntegerValue("SELECT Id FROM AutopriorityRuleHdr WHERE Title='" & title & "'")
                        If ddlFieldCategory.SelectedIndex > 0 Then
                            Database.Tenant.Execute("UPDATE AutopriorityRuleHdr SET  Category='" + ddlFieldCategory.SelectedItem.ToString().Replace("'", "''") + "' where Id={0} ".SqlFormatString(ruleId))
                        End If
                        Database.Tenant.Execute("UPDATE AutopriorityRuleHdr SET IsActive=" + IsActive.ToString() + " where Id={0} ".SqlFormatString(ruleId))
                        Newruleaudit()
                        Dim DescList As New List(Of String)()
                        If (Me.ViewState("DescrptionViewState") IsNot Nothing) Then
                            DescList = Me.ViewState("DescrptionViewState")
                            For Each Desc As String In DescList
                                Dim description1 = Desc
                                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), description1)
                            Next
                        End If
                        Me.ViewState("DescrptionViewState") = Nothing
                        Me.ViewState.Add("RuleId", ruleId)
                        If dtMpsResult.Rows.Count > 0 And dtMpsResult.Rows(0)(0) > 0 Then
                            lblMpsCount.Text = dtMpsResult.Rows(0)(0).ToString()

                            FinalResult.Visible = True
                            divMpsResult.Visible = True
                            RunScript("automask();")
                        Else
                            RunScript("hideMask();")
                            divMpsResult.Visible = False
                            FinalResult.Visible = False
                        End If

                        lblCount.Text = dtResult.Rows(0)(0).ToString()
                        divRuleResult.Visible = True
                        If dtResult.Rows.Count > 0 And dtResult.Rows(0)(0) > 0 Then

                            FinalResult.Visible = True
                            lbDowload.Visible = True
                            RunScript("automask();")
                        Else
                            RunScript("hideMask();")
                            lbDowload.Visible = False
                            FinalResult.Visible = False
                        End If
                    Else
                        Dim editedCount = dtResult.Rows(0)(0)
                        dtResultOld = Database.Tenant.GetDataTable("exec cc_autoProrityListWithId @RuleId=" & ruleId & ", @flag='count'")
                        Dim prevCount = dtResultOld.Rows(0)(0)

                        FinalResult.Visible = False
                        Dim waringString = "The previous Rule identified " + prevCount.ToString + " property(s) and the newly edited Rule identifies " + editedCount.ToString + " property(s). Press OK to proceed with saving your changes or Cancel to go back and review the Rule."

                        If hdnUpdateFlag.Value = "1" Then
                            RunScript("setTimeout(function(){Confirm(" + editedCount.ToString() + "," + prevCount.ToString() + ");}, 1000);")
                            RunScript("hideMask();")
                        End If
                    End If

                Catch ex As Exception
                    Dim ErrMsg As String = ""
                    If ex.Message.Contains("UNIQUE KEY") Then
                        ErrMsg = "There is already a rule exist with name " + title + " ."
                    End If
                    RunScript("alertMsg('" + ErrMsg + "');")
                    RunScript("hideMask();")
                End Try

            Else
                Dim ErrMsg As String = ""
                ErrMsg = "Fields and conditions are empty for creating rule. Rule not generated."
                RunScript("alertMsg('" + ErrMsg + "');")
                RunScript("hideMask();")
            End If
        Else
            Dim ErrMsg As String = ""
            ErrMsg = "Fields and conditions are empty for creating rule. Rule not generated."
            RunScript("alertMsg('" + ErrMsg + "');")
            RunScript("hideMask();")
        End If

    End Sub

    Protected Sub HideLinkBt_Click(sender As Object, e As EventArgs) Handles HideLinkBt.Click
        If (Me.ViewState("SaveHdr") IsNot Nothing) Then
            Dim title As String = txtTitle.Text
            Dim ruleId As Integer = 0
            Dim dtSaveHdr As New DataTable
            Dim dtSaveDtl As New DataTable
            Dim IsActive As Integer = 0
            If CheckEnable.Checked = True Then
                IsActive = 1
            End If
            dtSaveHdr = Me.ViewState("SaveHdr")
            dtSaveDtl = Me.ViewState("SaveDtl")
            If (Me.ViewState("RuleId") IsNot Nothing) Then
                ruleId = Me.ViewState("RuleId")
            End If
            CheckforEdit(ruleId)
            Database.Tenant.ExecuteWithDataTable("cc_autoPriorityRule", dtSaveHdr, dtSaveDtl)
            ruleId = Database.Tenant.GetIntegerValue("SELECT Id FROM AutopriorityRuleHdr WHERE Title='" & title & "'")
            Me.ViewState.Add("RuleId", ruleId)
            If ddlFieldCategory.SelectedIndex > 0 Then
                Database.Tenant.Execute("UPDATE AutopriorityRuleHdr SET  Category='" + ddlFieldCategory.SelectedItem.ToString().Replace("'", "''") + "' where Id={0} ".SqlFormatString(ruleId))
            End If
            Database.Tenant.Execute("UPDATE AutopriorityRuleHdr SET IsActive=" + IsActive.ToString() + " where Id={0} ".SqlFormatString(ruleId))
            Dim DescList As New List(Of String)()
            If (Me.ViewState("DescrptionViewState") IsNot Nothing) Then
                DescList = Me.ViewState("DescrptionViewState")
                For Each Desc As String In DescList
                    Dim description = Desc
                    SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), description)
                Next
            End If
            Me.ViewState("DescrptionViewState") = Nothing
            hdnRuleId.Value = Me.ViewState("RuleId")
            Dim dtm = ViewState("MpsResult")
            Dim dtr = ViewState("verifyResult")
            If dtm > 0 Then
                lblMpsCount.Text = dtm
                FinalResult.Visible = True
                divMpsResult.Visible = True
                RunScript("automask();")
            Else
                RunScript("hideMask();")
                divMpsResult.Visible = False
                FinalResult.Visible = False
            End If

            lblCount.Text = dtr
            divRuleResult.Visible = True
            If dtr > 0 Then

                FinalResult.Visible = True
                lbDowload.Visible = True
                RunScript("automask();")
            Else
                RunScript("hideMask();")
                RunScript("hideautomask();")
                lbDowload.Visible = False
                FinalResult.Visible = False
            End If

        End If
    End Sub

    Public Sub CheckforEdit(rule As Integer)
        Dim ruleDtl As New DataTable
        Dim Description As String
        Dim ruleNo = rule
        ruleDtl = Database.Tenant.GetDataTable("select * from autopriorityruleHdr where Id =" + ruleNo.ToString() + "")
        For Each row As DataRow In ruleDtl.Rows
            Dim Title = row.Item("Title")
            Dim Active As Boolean = row.Item("IsActive")
            Dim Priority = row.Item("Priority")
            Dim AlertMsg = row.Item("Alert")
            Dim Automatic As Boolean = row.Item("applyOnRefresh")
            Dim DateChecked As Boolean = row.Item("IsDateApplicable")
            Dim Exception As Boolean = row.Item("IncludeReviewed")
            Dim CategorySaved = row.Item("Category")
            If Title.ToString() <> txtTitle.Text Then
                Description = "Title name of Auto Priority Rule changed from " + Title.ToString() + " to " + txtTitle.Text + "."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
            End If
            If Active <> CheckEnable.Checked Then
                If CheckEnable.Checked.GetHashCode = 1 Then
                    Description = "Enabled the Rule - " + txtTitle.Text + "."
                Else
                    Description = "Disabled the Rule - " + txtTitle.Text + "."
                End If
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
            End If
            If ddlFieldCategory.SelectedIndex > 0 Then
                If CategorySaved.ToString() <> ddlFieldCategory.SelectedItem.ToString() Then
                    Description = "Field Category of the Rule - " + txtTitle.Text + " changed from " + CategorySaved.ToString() + " to " + ddlFieldCategory.SelectedItem.ToString() + "."
                    SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
                End If
            End If
            If Priority <> ddlPriority.SelectedValue Then
                Dim oldPrio As String = ""
                If EnableNewPriorities Then
                    Select Case Priority
                        Case 1
                            oldPrio = "Normal"
                        Case 2
                            oldPrio = "Medium"
                        Case 3
                            oldPrio = "High"
                        Case 4
                            oldPrio = "Urgent"
                        Case 5
                            oldPrio = "Critical"
                    End Select

                    Description = "Changed the Priority of the Rule - " + txtTitle.Text + " from " + oldPrio + " to " + ddlPriority.SelectedItem.ToString() + "."
                Else
                    oldPrio = "High"
                    If Priority = 2 Then
                        oldPrio = "Urgent"
                    End If
                    Description = "Changed the Priority of the Rule - " + txtTitle.Text + " from " + oldPrio + " to " + ddlPriority.SelectedItem.ToString() + "."
                End If
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
            End If
            If AlertMsg.ToString() <> txtAlert1.Text Then
                Description = "Alert of the Rule - " + txtTitle.Text + " changed from " + AlertMsg.ToString() + " to " + txtAlert1.Text + "."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
            End If
            If Automatic <> ApplyAutomatic.Checked Then
                If ApplyAutomatic.Checked.GetHashCode = 1 Then
                    Description = "Enabled Automatic rule apply on the Rule - " + txtTitle.Text + "."
                Else
                    Description = "Disabled Automatic rule apply on the Rule - " + txtTitle.Text + "."
                End If
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
            End If
            If DateChecked <> ApplyDateRange.Checked Then
                If ApplyDateRange.Checked.GetHashCode = 1 Then
                    Description = "Date Range is enabled on the Rule - " + txtTitle.Text + "."
                Else
                    Description = "Date Range is disabled on the Rule - " + txtTitle.Text + "."
                End If
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
            End If
            If Exception <> enableExceptions.Checked Then
                If enableExceptions.Checked.GetHashCode = 1 Then
                    Description = "Exception is enabled on the Rule - " + txtTitle.Text + "."
                Else
                    Description = "Exception is disabled on the Rule - " + txtTitle.Text + "."
                End If
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
            End If
        Next

    End Sub

    ''Public Function EditshowAffetedScript()

    ''  Return "ShowAffetedParcel(" & Eval("RuleId") & ");"
    ''End Function

    Public Sub ConditionAudit(check As String, title As String, Optional ByVal edrow As Integer = Nothing)
        Dim descList As New List(Of String)()
        Select Case check
            Case "edit"
                Dim dtSaveDtl As New DataTable
                Dim Val1 As String = ""
                If Me.ViewState("Value1") IsNot Nothing Then
                    Val1 = Me.ViewState("Value1")
                End If
                Dim Val2 As String = ""
                If Me.ViewState("Value2") IsNot Nothing Then
                    Val2 = Me.ViewState("Value2")
                End If
                Dim oper As String = ""
                If Me.ViewState("Operator") IsNot Nothing Then
                    oper = Me.ViewState("Operator")
                End If
                dtSaveDtl = Database.Tenant.GetDataTable("select *,df.name As Fieldname,dt.name As Tablename from autopriorityruledtl dtl join autopriorityruleHdr hdr on hdr.id = dtl.id join DataSourceField df on df.id =dtl.FieldID join Datasourcetable dt on dt.id=dtl.TableID where hdr.id = '" + Me.ViewState("RuleId").ToString() + "' and dtl.Rowno =" + edrow.ToString() + "")
                For Each row As DataRow In dtSaveDtl.Rows
                    Dim Fieldval = row.Item("Fieldname")
                    Dim fieldid = row.Item("FieldID")
                    Dim Tableval = row.Item("Tablename")
                    Dim Cond = row.Item("ConditionID")
                    Dim CondStr = row.Item("ConditionStr")
                    Dim Value1 = row.Item("Value1")
                    Dim Value2 = row.Item("Value2")
                    Dim OperatorVal = row.Item("Operator")
                    If fieldid <> ddlField.SelectedValue Then
                        Dim DescriptionField As String
                        DescriptionField = "Field of rule condition" + edrow.ToString() + " of rule " + title + " changed from " + Fieldval.ToString() + " to " + ddlField.SelectedItem.ToString() + ""
                        descSave(DescriptionField)
                    End If
                    If Cond <> ddlCondition.SelectedValue Then
                        Dim DescriptionCond As String
                        DescriptionCond = "Condiion of rule condition" + edrow.ToString() + " of rule " + title + " changed from " + CondStr.ToString() + " to " + ddlCondition.SelectedItem.ToString() + ""
                        descSave(DescriptionCond)
                    End If
                    If Value1.ToString() <> Val1.ToString() Then
                        Dim DescriptionVal1 As String
                        DescriptionVal1 = "Value of rule condition" + edrow.ToString() + " of rule " + title + " changed from " + Value1.ToString() + " to " + Val1.ToString() + ""
                        descSave(DescriptionVal1)
                    End If
                    If Value2.ToString() <> Val2.ToString() Then
                        Dim DescriptionVal2 As String
                        DescriptionVal2 = "Value-2 of rule condition" + edrow.ToString() + " of rule " + title + " changed from " + Value2.ToString() + " to " + Val2.ToString() + ""
                        descSave(DescriptionVal2)
                    End If
                    If OperatorVal.ToString() <> oper.ToString() Then
                        Dim DescriptionOpr As String
                        DescriptionOpr = "Operator of rule condition" + edrow.ToString() + " of rule " + title + " changed from " + OperatorVal.ToString() + " to " + oper.ToString() + ""
                        descSave(DescriptionOpr)
                    End If
                Next
            Case "editnew"
                Dim dtSaveDtl As New DataTable
                dtSaveDtl = Me.ViewState("ConditionViewState")
                For Each row As DataRow In dtSaveDtl.Rows
                    Dim description As String
                    If row(1) = edrow Then

                        Dim Tablename = Database.Tenant.GetStringValue("Select name From datasourcetable where ID=" + row(2).ToString() + "")
                        ''Dim fieldname = Database.Tenant.GetStringValue("Select name From datasourcefield where ID="+row(3)+"") 
                        description = "New Condition added in Rule : " + title + "  Tablename - " + Tablename + ", Field - " + row(4).ToString() + ",Condition- " + row(5).ToString() + ","
                        If row(8).ToString().Length > 0 Then
                            description = description + "value-" + row(8).ToString() + ","
                        End If
                        If row(9).ToString().Length > 0 Then
                            description = description + "value2-" + row(9).ToString() + ","
                        End If
                        If row(10).ToString().Length > 0 Then
                            description = description + "Operator-" + row(10).ToString() + "."
                        End If

                        descSave(description)
                    End If
                Next
        End Select
    End Sub

    Public Sub descSave(EditDesc As String)
        Dim descList As New List(Of String)()
        If (Me.ViewState("DescrptionViewState") IsNot Nothing) Then
            descList = Me.ViewState("DescrptionViewState")
            descList.Add(EditDesc)
            Me.ViewState.Add("DescrptionViewState", descList)
        Else
            descList.Add(EditDesc)
            Me.ViewState.Add("DescrptionViewState", descList)
        End If
    End Sub

    Public Sub Newruleaudit()
        Dim Description As String
        If CheckEnable.Checked Then
            Description = "Enabled the Rule - " + txtTitle.Text + "."
        Else
            Description = "Disabled the Rule - " + txtTitle.Text + "."
        End If
        SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
        If ddlFieldCategory.SelectedIndex > 0 Then
            Description = "Selected Field Category of the Rule - " + txtTitle.Text + " is " + ddlFieldCategory.SelectedItem.ToString() + "."
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
        End If
        Description = "Selected Priority of the Rule - " + txtTitle.Text + " is " + ddlPriority.SelectedItem.ToString() + "."
        SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
        If txtAlert1.Text <> "" Then
            Description = "Alert given to the Rule - " + txtTitle.Text + " is " + txtAlert1.Text + "."
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
        End If
        If ApplyAutomatic.Checked Then
            Description = "Enabled Automatic rule apply on the Rule - " + txtTitle.Text + "."
        Else
            Description = "Disabled Automatic rule apply on the Rule - " + txtTitle.Text + "."
        End If
        SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
        If ApplyDateRange.Checked Then
            Description = "Date Range is enabled on the Rule - " + txtTitle.Text + "."
        Else
            Description = "Date Range is disabled on the Rule - " + txtTitle.Text + "."
        End If
        SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
        If enableExceptions.Checked Then
            Description = "Exception is enabled on the Rule - " + txtTitle.Text + "."
        Else
            Description = "Exception is disabled on the Rule - " + txtTitle.Text + "."
        End If
        SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
    End Sub

    Public Sub applyRule(ruleid As Integer)
        Dim dTtDtl As DataTable = Database.Tenant.GetDataTable("SELECT Rowno AS ActRowno,Rowno AS Rowno,APD.TableID AS TableId,FieldID AS FieldId,DF.Name AS fieldName,ConditionStr AS Condition,ValueStr AS ValueString,ConditionID AS  ConditionID,Value1 AS Value1,value2 as Value2, Operator as Operatorstr FROM AutopriorityRuleDtl APD JOIN DataSourceField DF ON APD.FieldID=DF.Id WHERE APD.Id=" & ruleid)
        Me.ViewState.Add("ConditionViewState", dTtDtl)
        If (Me.ViewState("ConditionViewState") IsNot Nothing) Then
            Dim dtDtl As New DataTable
            dtDtl = Me.ViewState("ConditionViewState")
            If dtDtl.Rows.Count > 0 Then
                Dim dtHdr As New DataTable
                Dim title As String = txtTitle.Text
                Dim Count As Integer


                Try
                    Dim uName As String = Database.System.GetStringValue("SELECT au.UserName FROM  aspnet_Users au INNER JOIN aspnet_Applications a On a.ApplicationId=au.ApplicationId WHERE  a.ApplicationName='" & Membership.ApplicationName & "' And au.UserName='" & Page.User.Identity.Name & "'")
                    Dim dtResult = Database.Tenant.GetDataTable("EXEC cc_autoProrityParcelList @RuleId=" & ruleid)
                    If Convert.ToInt64(dtResult.Rows.Count) > 0 Then
                        Database.Tenant.Execute("exec cc_autoProritySetting  @RuleId={0},@UserName={1},@Reset={2}".SqlFormatString(ruleid, uName, 0))
                        RunScript("notificationandhideMask('Auto priority rule applied successfully.');")
                    Else
                        Dim ErrMsg As String = ""
                        ErrMsg = "No parcel found for applying rule."
                        RunScript("setTimeout(function(){alertMsg('" + ErrMsg + "');}, 1000);")
                    End If

                Catch
                    RunScript("hideMask();")
                End Try
            Else
                Dim ErrMsg As String = ""
                ErrMsg = "Fields and conditions are empty for creating rule. Rule not generated."
                RunScript("alertMsg('" + ErrMsg + "');")
            End If
        Else
            Dim ErrMsg As String = ""
            ErrMsg = "Fields and conditions are empty for creating rule. Rule not generated."
            RunScript("alertMsg('" + ErrMsg + "');")
        End If
        RunScript("hideMask();")
    End Sub


    Public Sub lbnRevoke_Click(sender As Object, e As EventArgs)
        Try
            Dim dtHdr As New DataTable
            Dim title As String = txtTitle.Text
            Dim ruleId As Integer
            Dim uName As String = Database.System.GetStringValue("SELECT au.UserName FROM  aspnet_Users au INNER JOIN aspnet_Applications a On a.ApplicationId=au.ApplicationId WHERE  a.ApplicationName='" & Membership.ApplicationName & "' And au.UserName='" & Page.User.Identity.Name & "'")
            ruleId = Database.Tenant.GetIntegerValue("SELECT Id FROM AutopriorityRuleHdr WHERE Title='" & title & "'")
            If ruleId = 0 Then
                Dim ErrMsg As String = ""
                ErrMsg = "Please save the rule before revoking"
                RunScript("alertMsg('" + ErrMsg + "');")
            Else
                Database.Tenant.Execute("exec cc_autoProritySetting @RuleId={0},@UserName={1},@Reset={2}".SqlFormatString(ruleId, uName, 1))
                Dim dtResult = Database.Tenant.GetDataTable("EXEC cc_autoProrityParcelList @RuleId=" & ruleId)
                If dtResult.Rows.Count > 0 Then
                    RunScript("notificationandhideMask('Auto priority rule revoked successfully');")
                Else
                    RunScript("notificationandhideMask('No Parcel found by this rule');")
                End If
            End If
        Catch
            RunScript("hideMask();")
        End Try
    End Sub


    Sub closefinalmsg()

        FinalResult.Visible = False
        RunScript("hideautomask();")

    End Sub

    Sub closegirdmsg()
        parcelgrid.Visible = False
        RunScript("hideautomask();")
    End Sub

    Public Sub chkDaterange_CheckedChanged(sender As Object, e As EventArgs)
        Dim chk As CheckBox = sender

        If (chk.Checked = True) Then
            divDateRangeSub.Visible = True
        Else
            divDateRangeSub.Visible = False
        End If
        RunScript("hideMask();")
    End Sub



    Public Sub chkException_CheckedChanged(sender As Object, e As EventArgs)
        Dim chk As CheckBox = sender

        If (chk.Checked = True) Then
            divExceptionsSub.Visible = True
        Else
            divExceptionsSub.Visible = False
        End If

        If (Me.ViewState("RuleId") IsNot Nothing) Then
            hdnRuleId.Value = "1"
        End If
        RunScript("hideMask();")
    End Sub

    Protected Sub gvCondition_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvCondition.RowCommand
        If e.CommandName = "remove" Then

            If (Me.ViewState("ConditionViewState") IsNot Nothing) Then
                Dim dtDtl As New DataTable
                dtDtl = Me.ViewState("ConditionViewState")

                Dim i As Integer = 0
                Dim rowNo As Integer
                For Each row As DataRow In dtDtl.Rows
                    If row("Rowno") = e.CommandArgument Then
                        rowNo = row("Rowno") - 1
                    End If
                Next
                dtDtl.Rows.RemoveAt(rowNo)
                dtDtl.AcceptChanges()
                Dim Description As String
                Dim remRow = rowNo + 1
                Dim descList As New List(Of String)()
                Description = "Removed the Condition - " + remRow.ToString() + " of the Rule : " + txtTitle.Text + "."
                If (Me.ViewState("DescrptionViewState") IsNot Nothing) Then
                    descList = Me.ViewState("DescrptionViewState")
                    descList.Add(Description)
                    Me.ViewState.Add("DescrptionViewState", descList)
                Else
                    descList.Add(Description)
                    Me.ViewState.Add("DescrptionViewState", descList)
                End If
                i = 1
                For Each row As DataRow In dtDtl.Rows
                    row("Rowno") = i
                    row("ActRowno") = i
                    i += 1
                Next
                Me.ViewState("RemovedRowNo") = e.CommandArgument
                Me.ViewState("ConditionViewState") = dtDtl
                gvCondition.DataSource = dtDtl
                gvCondition.DataBind()


            End If

        End If

        If e.CommandName = "EditRule" Then
            If Me.ViewState("ConditionViewState") IsNot Nothing Then
                Dim field, condition, rowno, i As Integer
                Dim val1, val2, conditionstr, operatorStr As String
                Dim TempDt As DataTable
                Dim TempDt1() As DataRow
                Dim TempResult As DataTable
                val1 = ""
                val2 = ""
                conditionstr = ""
                operatorStr = ""
                TempDt = Me.ViewState("ConditionViewState")
                TempDt1 = TempDt.Select("Rowno = " & e.CommandArgument)
                If TempDt1.Count <> 0 Then
                    For Each row As DataRow In TempDt.Rows
                        If row("Rowno") = e.CommandArgument Then
                            rowno = row("Rowno") - 1
                            field = row("FieldId")
                            condition = IIf(row("ConditionId").Equals(DBNull.Value), 0, row("ConditionId"))
                            conditionstr = row("Condition")
                            val1 = IIf(row("Value1").Equals(DBNull.Value), DBNull.Value.ToString(), row("Value1"))
                            val2 = IIf(row("Value2").Equals(DBNull.Value), DBNull.Value.ToString(), row("Value2"))
                            operatorStr = IIf(row("Operatorstr").Equals(DBNull.Value), "-- Select --", row("Operatorstr"))
                        End If
                    Next
                    TempResult = Database.Tenant.GetDataTable("Select FieldCategory.Id, FieldCategory.Name FROM FieldCategory JOIN DataSourceField On FieldCategory.Id = CategoryId WHERE DataSourceField.Id = " & field)
                    For Each row As DataRow In TempResult.Rows
                        ddlFieldCategory.SelectedValue = row("Id")
                    Next
                    LoadFields()
                    ddlField.SelectedValue = field
                    LoadConditions()
                    ddlCondition.SelectedValue = condition
                    If operatorStr = "AND" Then
                        DdlOperator.SelectedValue = 1
                    ElseIf operatorStr = "OR" Then
                        DdlOperator.SelectedValue = 2
                    Else
                        DdlOperator.SelectedValue = 0
                    End If

                    Dim query As String
                    query = "Select Case When  Datatype In(2, 3, 7, 8, 9, 10) Then 2  When Datatype= 4 Then 3 Else 1 End As TYPE1  FROM datasourcefield WHERE Id = {0}".FormatString(ddlField.SelectedValue)
                    Dim DataType As Integer = Database.Tenant.GetIntegerValue(query)
                    txtValuesString.Visible = False
                    txtValuesNumber.Visible = False
                    txtValuesDate.Visible = False
                    txtValuesNumber2.Visible = False
                    txtValuesDate2.Visible = False
                    Select Case DataType
                        Case 1
                            txtValuesString.Visible = True
                            txtValuesString.Text = val1
                        Case 2
                            If conditionstr = "BETWEEN" Then
                                txtValuesNumber.Visible = True
                                txtValuesNumber2.Visible = True
                                txtValuesNumber.Text = val1
                                txtValuesNumber2.Text = val2
                            Else
                                txtValuesNumber.Visible = True
                                txtValuesNumber.Text = val1
                            End If
                        Case 3
                            If conditionstr = "BETWEEN" Then
                                txtValuesDate.Visible = True
                                txtValuesDate2.Visible = True
                                txtValuesDate.Text = val1
                                txtValuesDate2.Text = val2
                            Else
                                txtValuesDate.Visible = True
                                txtValuesDate.Text = val1
                            End If
                    End Select
                    lblAdd.Text = "Update"
                    RunScript("hideMask();")
                    Me.ViewState("EditRowNo") = e.CommandArgument
                End If
            End If

        End If

    End Sub

    ''     Sub gvproperties_PageIndexChanging(ByVal sender As Object, ByVal e As GridViewPageEventArgs)
    ''			If (Me.ViewState("verifyResult") IsNot Nothing) Then
    ''	       		Dim dtResult As New DataTable 
    ''	       		dtResult= Me.ViewState("verifyResult")
    ''	       		gvproperties.PageIndex = e.NewPageIndex
    ''	       		gvproperties.DataSource = dtResult
    ''	       		gvproperties.DataBind()
    ''       		End if
    ''     End Sub

    Public Sub ExportCSV(sender As Object, e As EventArgs)

        '        If (Me.ViewState("verifyResult") IsNot Nothing) Then
        Dim dtSaveHdr As New DataTable
        Dim dtSaveDtl As New DataTable
        dtSaveHdr = Me.ViewState("SaveHdr")
        dtSaveDtl = Me.ViewState("SaveDtl")
        Dim dt As New DataTable
        dt = Database.Tenant.GetDTWithDT("cc_autoProrityParcelList", dtSaveDtl, dtSaveHdr)
        Export(dt, 0)
        '        End If
    End Sub

    Public Sub UnlockMPS(sender As Object, e As EventArgs)
        If (Me.ViewState("SaveHdr") IsNot Nothing) Then
            Dim dtSaveHdr As New DataTable
            Dim dtSaveDtl As New DataTable
            Dim dtMpsResult As New DataTable
            dtSaveHdr = Me.ViewState("SaveHdr")
            dtSaveDtl = Me.ViewState("SaveDtl")
            dtMpsResult = Database.Tenant.GetDTWithDT("cc_autoProrityMPSProc", dtSaveDtl, dtSaveHdr, "UPDATE")
            If dtMpsResult.Rows.Count > 0 Then
                lblMpsCount.Text = dtMpsResult.Rows.Count.ToString()
                RunScript("automask();")
                FinalResult.Visible = True
                divMpsResult.Visible = True

            Else
                divMpsResult.Visible = False
                FinalResult.Visible = False
            End If
            btnVerify_Click(sender, e)

        End If
    End Sub



    Public Sub ExportMPSCSV(sender As Object, e As EventArgs)

        '        If (Me.ViewState("verifyResult") IsNot Nothing) Then
        Dim dtSaveHdr As New DataTable
        Dim dtSaveDtl As New DataTable
        dtSaveHdr = Me.ViewState("SaveHdr")
        dtSaveDtl = Me.ViewState("SaveDtl")
        Dim dt As New DataTable
        dt = Database.Tenant.GetDTWithDT("cc_autoProrityMPSProc", dtSaveDtl, dtSaveHdr)
        Export(dt, 1)
        '        End If
    End Sub

    Public Sub Export(dt As DataTable, csvType As Integer)
        Dim csv As String = String.Empty
        Dim csvName As String = "ParcelList.csv"
        If csvType = 1 Then
            csvName = "MPSParcelList.csv"
        End If
        For Each column As DataColumn In dt.Columns
            csv += column.ColumnName + ","c
        Next
        csv = csv.Trim().Remove(csv.LastIndexOf(","))
        csv += vbCr & vbLf

        For Each row As DataRow In dt.Rows
            For Each column As DataColumn In dt.Columns
                csv += row(column.ColumnName).ToString().Replace(",", ";") + ","c
            Next
            csv = csv.Trim().Remove(csv.LastIndexOf(","))
            csv += vbCr & vbLf
        Next

        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=" + csvName + "")
        Response.Charset = ""
        Response.ContentType = "application/text"
        Response.Output.Write(csv)
        Response.Flush()
        Response.End()
    End Sub

    'Public Function MasterFieldClass() As String
    '    Dim isFav = Eval("IsFavorite")
    '    If isFav Then
    '        Return "cat-added"
    '    Else
    '        Return ""
    '    End If
    'End Function


    Dim dt As DataTable
    Dim dtRules As DataTable
    'Sub LoadData()
    '    If dtRules Is Nothing Then
    '        dtRules = Database.Tenant.GetDataTable("Select Id As RuleId,Title,Case When Priority=1 Then 'High' WHEN Priority=2 THEN 'Urgent' ELSE 'Normal' END AS priority,dbo.GetLocalDate(CreatedDate) AS CreatedDate,IsActive FROM AutopriorityRuleHdr ORDER BY CreatedDate DESC")
    '    End If
    '    Me.ViewState.Add("RulesHdr", dtRules)
    '    gvRuleList.DataSource = dtRules
    '    gvRuleList.DataBind()
    'End Sub

    Public Sub gvRuleList_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvRuleList.RowCommand
        Dim dtRules As DataTable
        Dim ruleId As Integer
        Select Case e.CommandName.ToString
            Case "Delete1"
                ruleId = e.CommandArgument
                Dim rulename = Database.Tenant.GetStringValue("SELECT Title from AutopriorityRuleHdr WHERE Id={0}".SqlFormatString(ruleId))
                Dim Description As String = "Deleted AutoPriority rule - " + rulename.ToString() + "."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
                Database.Tenant.Execute("DELETE FROM  AutopriorityRuleDtl WHERE Id={0}".SqlFormatString(ruleId))
                Database.Tenant.Execute("DELETE FROM  AutopriorityRuleHdr WHERE Id={0}".SqlFormatString(ruleId))
                If EnableNewPriorities Then
                    dtRules = Database.Tenant.GetDataTable("SELECT Id AS RuleId,Title, CASE WHEN Priority = 5 THEN 'Critical' WHEN Priority = 4 THEN 'Urgent' WHEN Priority = 3 THEN 'High' WHEN Priority = 2 THEN 'Medium' WHEN Priority = 1 THEN 'Normal' ELSE 'Proximity' END AS priority,dbo.GetLocalDate(CreatedDate) AS CreatedDate,IsActive,ApplyRuleOneTime,CASE WHEN ApplyRuleOneTime = 1 THEN 1 WHEN IsDateApplicable = 1 AND GETUTCDATE() BETWEEN DateApplicableFrom AND DateApplicableTo THEN 1 ELSE 0 END AS ShowApply FROM AutopriorityRuleHdr ORDER BY ExecutionOrder")
                Else
                    dtRules = Database.Tenant.GetDataTable("SELECT Id AS RuleId,Title,CASE WHEN Priority=1 THEN 'High' WHEN Priority=2 THEN 'Urgent' ELSE 'Normal' END AS priority,dbo.GetLocalDate(CreatedDate) AS CreatedDate,IsActive,ApplyRuleOneTime,CASE WHEN ApplyRuleOneTime = 1 THEN 1 WHEN IsDateApplicable = 1 AND GETUTCDATE() BETWEEN DateApplicableFrom AND DateApplicableTo THEN 1 ELSE 0 END AS ShowApply FROM AutopriorityRuleHdr ORDER BY ExecutionOrder")
                End If

                dtRules.AcceptChanges()
                If dtRules.Rows.Count = 0 Then
                    norulemsg.Text = "No Rules are Created."
                    gvRuleList.DataSource = dtRules
                    gvRuleList.DataBind()
                Else
                    norulemsg.Text = ""
                    gvRuleList.DataSource = dtRules
                    gvRuleList.DataBind()
                    'LoadData()
                End If
                If (dtRules.Rows.Count > 1) Then
                    reOrder.Visible = True
                Else
                    reOrder.Visible = False
                End If
            Case "Apply"
                ruleId = e.CommandArgument
                applyRule(ruleId)
                'Database.Tenant.Execute("UPDATE AutopriorityRuleHdr SET  IsActive=(select ~IsActive from AutopriorityRuleHdr where Id={0}) where Id={0} ".SqlFormatString(e.CommandArgument.ToString))
                'dtRules = Database.Tenant.GetDataTable("SELECT Id AS RuleId,Title,CASE WHEN Priority=1 THEN 'High' WHEN Priority=2 THEN 'Urgent' ELSE 'Normal' END AS priority,CreatedDate,IsActive FROM AutopriorityRuleHdr ORDER BY CreatedDate DESC")
                'dtRules.AcceptChanges()
                'gvRuleList.DataSource = dtRules
                'gvRuleList.DataBind()
            Case "Edit1"
                ruleId = e.CommandArgument
                Me.ViewState.Add("RuleId", ruleId)
                Me.ViewState("DescrptionViewState") = Nothing
                'dtRules = Database.Tenant.GetDataTable("SELECT Id AS RuleId,Title,CASE WHEN Priority=1 THEN 'High' WHEN Priority=2 THEN 'Urgent' ELSE 'Normal' END AS priority,CreatedDate,IsActive FROM AutopriorityRuleHdr ORDER BY CreatedDate DESC")
                'dtRules.AcceptChanges()
                setfeilds(ruleId)
                LoadFields(1)
            Case "Parcel"
                ruleId = e.CommandArgument
                ddlRows.SelectedIndex = 0
                gvAuditTrail.PageIndex = 0
                showAffectedParcels(ruleId, "10")

        End Select
    End Sub

    Private Sub setfeilds(id As Integer)
        Dim dt = Database.Tenant.GetTopRow("SELECT * From AutopriorityRuleHdr WHERE Id={0}".SqlFormatString(id))
        If dt IsNot Nothing Then
            txtTitle.Text = dt.GetString("Title")
            ddlPriority.SelectedValue = dt.GetString("Priority")
            txtAlert1.Text = dt.GetString("Alert")
            If dt.GetBoolean("IsActive") = True Then
                CheckEnable.Checked = True
            Else
                CheckEnable.Checked = False
            End If

            If dt.GetBoolean("ApplyOnRefresh") = True Then
                ApplyAutomatic.Checked = True
            Else
                ApplyAutomatic.Checked = False
            End If
            If dt.GetBoolean("IsDateApplicable") = True Then
                ApplyDateRange.Checked = True
                divDateRangeSub.Visible = True
                Dim dateFrom As Date = dt.GetDate("DateApplicableFrom")
                Dim dateTo As Date = dt.GetDate("DateApplicableTo")
                If dateFrom <> DateTime.MinValue Then
                    dtFrom.Text = dateFrom.ToString("yyyy-MM-dd")
                End If
                If dateTo <> DateTime.MinValue Then
                    dtTo.Text = dateTo.ToString("yyyy-MM-dd")
                End If
            Else
                divDateRangeSub.Visible = False
            End If
            If dt.GetBoolean("IncludeReviewed") = True Then
                divExceptions.Visible = True
                enableExceptions.Checked = True
                divExceptionsSub.Visible = True
                txtReviewAgeingDays.Text = dt.GetString("ReviewAgeingDays")

            Else
                enableExceptions.Checked = False
            End If
            ''Try
            '' Dim catText = dt.GetString("Category")
            ''  ddlFieldCategory.SelectedIndex = ddlFieldCategory.Items.IndexOf(ddlFieldCategory.Items.FindByText(catText))
            '' Catch ex As Exception
            '' 	ddlFieldCategory.SelectedIndex = 0
            ''   End Try
        End If
        SwitchMode(ViewMode.Edit)
        Dim dTDtl As DataTable = Database.Tenant.GetDataTable("SELECT Rowno AS ActRowno,Rowno AS Rowno,APD.TableID AS TableId,FieldID AS FieldId,DF.Name AS fieldName,ConditionStr AS Condition,ValueStr AS ValueString,ConditionID AS  ConditionID,Value1 AS Value1,value2 as Value2, Operator as Operatorstr FROM AutopriorityRuleDtl APD JOIN DataSourceField DF ON APD.FieldID=DF.Id WHERE APD.Id=" & id)
        Me.ViewState.Add("ConditionViewState", dTDtl)
        gvCondition.DataSource = dTDtl
        gvCondition.DataBind()
    End Sub

    Sub gvRuleList_PageIndexChanging(ByVal sender As Object, ByVal e As GridViewPageEventArgs)
        If (Me.ViewState("RulesHdr") IsNot Nothing) Then
            Dim dt As New DataTable
            dt = Me.ViewState("RulesHdr")
            gvRuleList.PageIndex = e.NewPageIndex
            gvRuleList.DataSource = dt
            gvRuleList.DataBind()
        End If
    End Sub


    Sub gvCondition_PageIndexChanging(ByVal sender As Object, ByVal e As GridViewPageEventArgs)
        If (Me.ViewState("ConditionViewState") IsNot Nothing) Then
            Dim dt1 As New DataTable
            dt1 = Me.ViewState("ConditionViewState")
            gvCondition.PageIndex = e.NewPageIndex
            gvCondition.DataSource = dt1
            gvCondition.DataBind()
        End If
    End Sub

    Private Sub ddlfilter_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlRows.SelectedIndexChanged
        Dim id As Integer
        Dim rows As String = ""
        If ddlRows.SelectedValue <> "-2" Then
            rows = ddlRows.SelectedItem.Text
        End If
        id = hiddenrule.Value
        showAffectedParcels(id, rows)
    End Sub

    ''<System.web.Services.WebMethod()> _
    ''Public Shared Function showAff()
    ''Dim rule As String =  Request("id")
    ''End Function

    Private Sub showAffectedParcels(id As Integer, row As String)
        hiddenrule.Value = id
        Dim ruleId As Integer
        Dim rows As String = ""
        Dim RowSelect As String
        'If ddlRules.SelectedIndex <> 0 And ddlRules.SelectedValue <> "-2" And ddlRules.SelectedValue <> "" Then
        ruleId = id
        parcelgrid.Visible = False
        rows = row
        If rows <> "" And rows <> "Export All" Then
            RowSelect = "TOP " + rows + " "
        Else
            RowSelect = ""
        End If
        Dim dt As New DataTable
        Dim alternatefield = Database.Tenant.Application.AlternateKeyfield
        Dim keyfield = Database.Tenant.Application.KeyField(1)
        Dim selectField As String
        Dim joiningCondition As String
        If alternatefield IsNot Nothing Then
            selectField = "pd." + alternatefield + " AS KeyField"
            joiningCondition = "INNER JOIN Parceldata pd ON pd.CC_ParcelId = P.Id"
        Else
            selectField = "P.KeyValue1 AS KeyField"
            joiningCondition = ""
        End If
        If EnableNewPriorities Then
            If ruleId = 0 Then
                dt = Database.Tenant.GetDataTable("SELECT  " + RowSelect + " P.Id AS ParcelID," + selectField + ",dbo.GetLocalDate(AP.EventTime)  AS EventTime,CASE WHEN OldPriority = 5 THEN 'Critical' WHEN OldPriority = 4 THEN 'Urgent' WHEN OldPriority = 3 THEN 'High' WHEN OldPriority = 2 THEN 'Medium' WHEN OldPriority = 1 THEN 'Normal' ELSE 'Proximity'	END AS OldPriority  ,AP.[Description] FROM AutoPriorityAuditTrail AP JOIN Parcel P ON AP.ParcelId=P.Id " + joiningCondition + " ORDER BY AP.EventTime DESC")
            Else
                dt = Database.Tenant.GetDataTable("SELECT  " + RowSelect + " P.Id AS ParcelID," + selectField + ",dbo.GetLocalDate(AP.EventTime)  AS EventTime,CASE WHEN OldPriority = 5 THEN 'Critical' WHEN OldPriority = 4 THEN 'Urgent' WHEN OldPriority = 3 THEN 'High' WHEN OldPriority = 2 THEN 'Medium' WHEN OldPriority = 1 THEN 'Normal' ELSE 'Proximity'	END AS OldPriority  ,AP.[Description] FROM AutoPriorityAuditTrail AP JOIN Parcel P ON AP.ParcelId=P.Id " + joiningCondition + " WHERE AP.APSId='" + ruleId.ToString + "'  ORDER BY AP.EventTime DESC")
            End If
        Else
            If ruleId = 0 Then
                dt = Database.Tenant.GetDataTable("SELECT  " + RowSelect + " P.Id AS ParcelID," + selectField + ",dbo.GetLocalDate(AP.EventTime)  AS EventTime,CASE	WHEN OldPriority = 1 THEN 'High' WHEN OldPriority = 2 THEN 'Urgent' ELSE 'Normal'	END AS OldPriority  ,AP.[Description] FROM AutoPriorityAuditTrail AP JOIN Parcel P ON AP.ParcelId=P.Id " + joiningCondition + " ORDER BY AP.EventTime DESC")
            Else
                dt = Database.Tenant.GetDataTable("SELECT  " + RowSelect + " P.Id AS ParcelID," + selectField + ",dbo.GetLocalDate(AP.EventTime)  AS EventTime,CASE	WHEN OldPriority = 1 THEN 'High' WHEN OldPriority = 2 THEN 'Urgent' ELSE 'Normal'	END AS OldPriority  ,AP.[Description] FROM AutoPriorityAuditTrail AP JOIN Parcel P ON AP.ParcelId=P.Id " + joiningCondition + " WHERE AP.APSId='" + ruleId.ToString + "'  ORDER BY AP.EventTime DESC")
            End If
        End If

        If dt.Rows.Count = 0 Then
            RunScript("notificationandhideMask('No affected parcel of selected rule found');")
        Else
            Me.ViewState.Add("AuditTrail", dt)
            gvAuditTrail.DataSource = dt
            gvAuditTrail.DataBind()
            parcelgrid.Visible = True
            RunScript("automask();")
        End If
    End Sub

    Sub gvAuditTrail_PageIndexChanging(ByVal sender As Object, ByVal e As GridViewPageEventArgs)
        If (Me.ViewState("AuditTrail") IsNot Nothing) Then
            Dim dt As New DataTable
            dt = Me.ViewState("AuditTrail")
            gvAuditTrail.PageIndex = e.NewPageIndex
            gvAuditTrail.DataSource = dt
            gvAuditTrail.DataBind()
            RunScript("automask();")
        End If
    End Sub


    Protected Sub gvAuditTrail_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvAuditTrail.RowDataBound

        If e.Row.RowType = DataControlRowType.Header Then
            If Database.Tenant.Application.AlternateKeyfield IsNot Nothing Then
                e.Row.Cells(2).Text = Database.Tenant.Application.AlternateKeyfield.ToString()
            Else
                e.Row.Cells(2).Text = Database.Tenant.Application.KeyField(1).ToString()
            End If
        End If
    End Sub

    Protected Sub export_All()
        Dim ruleId As Integer = hiddenrule.Value
        Dim dt As New DataTable
        Dim alternatefield = Database.Tenant.Application.AlternateKeyfield
        Dim keyfield = Database.Tenant.Application.KeyField(1)
        Dim selectField As String
        Dim joiningCondition As String
        If alternatefield IsNot Nothing Then
            selectField = "pd." + alternatefield + " AS " + alternatefield
            joiningCondition = "INNER JOIN Parceldata pd ON pd.CC_ParcelId = P.Id"
        Else
            selectField = "P.KeyValue1 AS " + keyfield
            joiningCondition = ""
        End If

        If EnableNewPriorities Then
            If ruleId = 0 Then
                dt = Database.Tenant.GetDataTable("SELECT P.Id AS ParcelID," + selectField + ",dbo.GetLocalDate(AP.EventTime)  AS EventTime, CASE WHEN OldPriority = 5 THEN 'Critical' WHEN OldPriority = 4 THEN 'Urgent' WHEN OldPriority = 3 THEN 'High' WHEN OldPriority = 2 THEN 'Medium' WHEN OldPriority = 1 THEN 'Normal' ELSE 'Proximity'	END AS OldPriority  ,AP.[Description] FROM AutoPriorityAuditTrail AP JOIN Parcel P ON AP.ParcelId=P.Id " + joiningCondition + " ORDER BY AP.EventTime DESC")
            Else
                dt = Database.Tenant.GetDataTable("SELECT P.Id AS ParcelID," + selectField + ",dbo.GetLocalDate(AP.EventTime)  AS EventTime, CASE WHEN OldPriority = 5 THEN 'Critical' WHEN OldPriority = 4 THEN 'Urgent' WHEN OldPriority = 3 THEN 'High' WHEN OldPriority = 2 THEN 'Medium' WHEN OldPriority = 1 THEN 'Normal' ELSE 'Proximity' END AS OldPriority  ,AP.[Description] FROM AutoPriorityAuditTrail AP JOIN Parcel P ON AP.ParcelId=P.Id " + joiningCondition + " WHERE AP.APSId='" + ruleId.ToString + "'  ORDER BY AP.EventTime DESC")
            End If
        Else
            If ruleId = 0 Then
                dt = Database.Tenant.GetDataTable("SELECT P.Id AS ParcelID," + selectField + ",dbo.GetLocalDate(AP.EventTime)  AS EventTime,CASE	WHEN OldPriority = 1 THEN 'High' WHEN OldPriority = 2 THEN 'Urgent' ELSE 'Normal'	END AS OldPriority  ,AP.[Description] FROM AutoPriorityAuditTrail AP JOIN Parcel P ON AP.ParcelId=P.Id " + joiningCondition + " ORDER BY AP.EventTime DESC")
            Else
                dt = Database.Tenant.GetDataTable("SELECT P.Id AS ParcelID," + selectField + ",dbo.GetLocalDate(AP.EventTime)  AS EventTime,CASE	WHEN OldPriority = 1 THEN 'High' WHEN OldPriority = 2 THEN 'Urgent' ELSE 'Normal'	END AS OldPriority  ,AP.[Description] FROM AutoPriorityAuditTrail AP JOIN Parcel P ON AP.ParcelId=P.Id " + joiningCondition + " WHERE AP.APSId='" + ruleId.ToString + "'  ORDER BY AP.EventTime DESC")
            End If
        End If

        Dim GridView1 As New GridView
        Page.Controls.Add(GridView1)
        GridView1.AllowPaging = False
        GridView1.AutoGenerateColumns = False
        Dim bfieldName As String
        Dim bHeaderText As String
        GridView1.Columns.Clear()
        For Each column As DataColumn In dt.Columns
            bfieldName = column.ColumnName
            bHeaderText = column.ColumnName
            Dim bfield As New BoundField()
            bfield.HeaderText = bHeaderText
            bfield.DataField = bfieldName
            If (column.ColumnName = "EventTime") Then
                bfield.DataFormatString = "{0:dd/MM/yyyy}"
            End If
            bfield.ItemStyle.CssClass = "gvNonCountItemStyle"
            bfield.HeaderStyle.CssClass = "gvHeaderStyle"
            If bfieldName = "Description" Then
                bfield.ItemStyle.Width = 300
            Else
                bfield.ItemStyle.Width = 180
            End If
            GridView1.Columns.Add(bfield)
        Next
        GridView1.DataSource = Nothing
        GridView1.DataSource = dt
        GridView1.DataBind()
        Dim org As DataRow = Database.Tenant.GetTopRow("SELECT Title FROM AutopriorityRuleHdr WHERE Id = " & ruleId)
        Dim FileName As String = org.GetString("Title").ToLower() + " rule -Affected Parcel list.xlsx"
        Response.Clear()
        Response.Buffer = True
        Response.ClearContent()
        Response.ClearHeaders()
        Response.ContentType = "application/vnd.openxml.formats-officedocument.spreadsheetml.sheet"
        Response.AddHeader("Content-Disposition", "attachment;filename=""" & FileName & "")
        GridView1.ControlStyle.Width = 130
        Dim excelStream = ExcelGenerator.ExportGrid(GridView1, dt, org.GetString("Title").ToLower() + "rule Affected Parcel list", org.GetString("Title").ToLower() + "rule Affected Parcel list")
        excelStream.CopyTo(Response.OutputStream)
        Response.End()
    End Sub

    <System.Web.Services.WebMethod()>
    Public Shared Function updateExecutionOrder(ByVal IdList As String) As String
        Dim i As Integer = 1
        Dim updateSql = ""
        For Each itm As String In IdList.Split(","c)
            updateSql += "UPDATE AutopriorityRuleHdr SET ExecutionOrder = " + i.ToString() + " WHERE Id = " + itm + ";"
            i = i + 1
        Next
        Try
            Database.Tenant.Execute(updateSql)
        Catch ex As Exception
            Return "failed!"
        End Try
        Return "sucess"
    End Function
End Class

