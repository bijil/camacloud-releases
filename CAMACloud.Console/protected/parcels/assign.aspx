﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/ParcelManager.master" AutoEventWireup="false" Inherits="CAMACloud.Console.parcels_assign" Codebehind="assign.aspx.vb" %>

<%@ Import Namespace="CAMACloud" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<style>
	.alerttxt 
	{
      text-overflow: ellipsis;
      overflow : hidden;
      white-space: nowrap;
	}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
	<h1>Assign <%=CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName() %>s</h1>
	<asp:Panel runat="server" ID="pnlUsers">
		<p  class="info">
			Below is the list of users who are assigned with <strong>Field Agent</strong> role. Select a user to assign <%=CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName().ToLower() %>s.
		</p>
		<asp:GridView runat="server" AllowPaging="true" PageSize="20" ID="gvUsers">
			<Columns>
				<asp:TemplateField HeaderText="Full Name">
						<ItemStyle Width="250px" />
						<ItemTemplate>
							<%# Eval("UserName")%>
						</ItemTemplate>
					</asp:TemplateField>
				<asp:TemplateField HeaderText="Click to assign">
					<ItemStyle Width="180px" />
					<ItemTemplate>
						<asp:LinkButton runat="server" Text='<%#Eval("UserName") %>' CommandName="Assign" CommandArgument='<%# Eval("LoginId") %>' />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</asp:GridView>
	</asp:Panel>
	<asp:Panel runat="server" ID="pnlAssign" Visible="false">
		<asp:HiddenField runat="server" ID="hdnUserName" />
        <asp:HiddenField runat="server" ID="hdnFullUserName" />
		<asp:HiddenField runat="server" ID="hdnNeighborhoodId" />
		<h3>Assign <%=ClientSettings.NeighborhoodDisplayLabelFullName.ToLower%>s to <asp:Label runat="server" ID="lblName" /></h3>
		<asp:LinkButton runat="server" ID="lbBack" Text="Return to List" />
		<p>
			Select <%=ClientSettings.NeighborhoodDisplayLabelFullName%>: 
			<asp:DropDownList runat="server" ID="ddlNbhd1" Width="45px" AutoPostBack="true" />
			<asp:DropDownList runat="server" ID="ddlNbhd2" Width="60px" AutoPostBack="true" />
			<asp:Button runat="server" ID="btnAssign" Text=" Assign to User " />
		</p>
		<p runat="server" id="nbhdDetails">
			<%=ClientSettings.NeighborhoodDisplayLabelFullName%>: <asp:Label runat="server" ID="lblNbhdName" Font-Bold="true" /><br />
			Currently assigned to: <asp:Label runat="server" ID="lblAssignedTo" Font-Bold="true" />
		</p>
		<asp:GridView runat="server" ID="gvNbhds">
			<Columns>
				<asp:BoundField DataField="Number" HeaderText="Assignment Group" ItemStyle-Width="140px" ItemStyle-CssClass="alerttxt" />
				<asp:TemplateField ItemStyle-Width="80px">
					<ItemTemplate>
						<asp:LinkButton runat="server" Text="Remove" CommandName='Unassign' CommandArgument='<%# Eval("Id") %>' OnClientClick="return confirm('Are you sure you want to remove the selected assignment group from the current user\'s assignments?')" />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
			<EmptyDataTemplate>
				No <%=ClientSettings.NeighborhoodDisplayLabelFullName.ToLower%> assigned currently.
			</EmptyDataTemplate>
		</asp:GridView>
		<p>
			<asp:LinkButton runat="server" ID="lbRemoveAll" Text="Remove all assignments" OnClientClick="return confirm('All assigned assignment groups will be removed from the current user. Are you sure you want to continue?')" />
		</p>
	</asp:Panel>
</asp:Content>

