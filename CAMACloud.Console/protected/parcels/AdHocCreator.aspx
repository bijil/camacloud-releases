﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/ParcelManager.master" CodeBehind="AdHocCreator.aspx.vb" Inherits="CAMACloud.Console.AdHocCreator" %>
<%@Import Namespace="System.Web.Security" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  
  
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyAZFyiCVUNdZfzu1V2IP-qhlApYsi6G4Qw&libraries=drawing"></script> 
<style>
	#map{
            width: 99%;
            height: 500px;
            box-shadow: 10px 10px 5px #888888;
            margin-top:15px;
	}
	.modal {
	    display: none; /* Hidden by default */
	    position: fixed; /* Stay in place */
	    z-index: 1; /* Sit on top */
	    left: 0;
	    top: 0;
	    width: 100%; /* Full width */
	    height: 100%; /* Full height */
	    overflow: auto; /* Enable scroll if needed */
	    background-color: rgb(0,0,0); /* Fallback color */
	    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
	}

	/* Modal Content/Box */
	.modal-content {
	    background-color: #fefefe;
	    margin: 3% auto; /* 15% from the top and centered */
	    padding: 20px;
	    border: 1px solid #888;
	    width: 90%; /* Could be more or less, depending on screen size */
	    height:520px;
	}

/* The Close Button */
.close {
    color: #aaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: black;
    text-decoration: none;
    cursor: pointer;
}
	</style>
	<style>
.button {
  display: inline-block;
  border-radius: 4px;
  background-color: #f4511e;
  border: none;
  color: #FFFFFF;
  text-align: center;
  font-size: 15px;
  padding: 8px;
  width: 100px;
  transition: all 0.5s;
  cursor: pointer;
}

.button span {
  cursor: pointer;
  display: inline-block;
  position: relative;
  transition: 0.5s;
}

.button span:after {
  content: '\00bb';
  position: absolute;
  opacity: 0;
  top: 0;
  right: -20px;
  transition: 0.5s;
}

.button:hover span {
  padding-right: 25px;
}

.button:hover span:after {
  opacity: 1;
  right: 0;
}
</style>
<style>
.scroll {
   width: 200px;
   height: 400px;
 
   overflow-y: scroll;
}
.scroll::-webkit-scrollbar {
    width: 12px;
}

.scroll::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3); 
    border-radius: 10px;
}

.scroll::-webkit-scrollbar-thumb {
    border-radius: 10px;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.5); 
}
</style>
<style>
.modal-body a {
    text-decoration: none;
    display: inline-block;
    padding: 8px 16px;
}

.modal-body a:hover {
    background-color: #888888;
    color: black;
}
 .mapmenu a {
    text-decoration: none;
    display: inline-block;
    padding: 8px 16px;
}

 .mapmenu a:hover {
    background-color: #888888;
    color: black;
}
.previous {
    background-color: #f1f1f1;
    color: black;
}
</style>
<style>
input[type=text], select {
    width: 20%;
    padding: 5px 10px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}
 #progressBar {
            background: #dcdcdc;
            height: 11px;
        }
        #checkboxes {
  display: none;
  border: 1px #dadada solid;
}
.selectParcelCheckbox {
    background: url(/App_Static/css/images/checkbox.png) no-repeat;
    height: 20px;
    width: 20px;
    float: left;
    background-size: 573%;
    background-position-x: -19px;
}
.selectedCheckbox{
 	background-position-x: 0px !important;
}
.disabled {
   background-color: #dddddd;
}
</style>
      <script type="text/javascript" src="/App_Static/js/dexie.js"></script> 
	  <script type="text/javascript" src="/App_Static/js/adhocCreator.js?a=1234"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<input type="hidden" id="LoginId" value="<%=Membership.GetUser()  %>">
<h1>Ad-Hoc Creator</h1>
<div class="mapmenu">
    <a href="#" class="previous " style="float:left;display:none" id="delete-button" onclick="return deleteSelectedShape()"> Delete Selected Shape</a>
   <!-- <a href="#" class="previous " style="float:left"  > Open Modal</a> -->
    <a href="#" class="previous " style="float:left;" onclick="return initialize()" > Refresh</a>
    <a href="#" class="previous" id="myBtn" style="float:left;display:none" onclick="return getAdhocParcels()" > Open Modal</a>
    <p class="parcelcount"  style="margin-right:15px;float:right;margin-bottom: 0px;display:none"><p>
</div>   
   <div id="map">
    </div>
   <div id="progressBar" style="display: none; position: relative; width: 99%;">
                <div id="currentProgress" style="position: relative; width: 0%; height: 100%; background: #2164df;"></div>
   </div>
    <div id="myModal" class="modal">
        <div class="modal-content">
            <div class="modal-header">
                <span class="close">&times;</span>
            <div class="modal-body" style="height: 380px">
            <p class="parcelcount" style="margin-top:auto;float:left"></p>
            <p id="selectedparcel" style="margin-top:auto;padding-left:24%;">SELECTED PARCELS : </p>
            <label style="margin-left:500px;">Change the priority of all parcels</label><br>
            <select id="bulkchange" onchange="return bulkOperation()">
             <option value="">None</option>
             <option value="Normal">Normal</option>
             <option value="High">High</option>
             <option value="Urgent">Urgent</option>
            </select>
             <div style="float:left;margin-right:30px;">
            <!--   <div id="output" style="height:470px; width: 250px;" >
           
               </div>
              <!--   <div style="width:240px " >
                  <button onclick="return AddAdHoc()">>></button>
                    <button onclick="return RemoveAdHoc()"><<</button>
                   <a href="#" class="previous" style="float:left" onclick="return selectall()"> Select All</a>
                    <a href="#" class="previous" style="float:right" onclick="return deselectall()"> Deselect All </a> 
                    
                   
                </div> -->
                </div>
                <div style="float:left;margin-right:30px;">
                <div class="scroller" style="height:455px;width:470px;"><table id="AdHocHolder"></table></div>                
                <a href="#" class="previous" style="float:left" onclick="return selectall()">Select All</a>
                <a href="#" class="previous" style="margin-left:270px" onclick="return deselectall()">Deselect All</a>              
              </div>
                <div>
                <!--   <input type="checkbox" id="adhocassignmentgroup" onclick="adhocassignment()" value=""/><label>Use ad-hoc assignment group.</label> -->
                      <div id="AdHocPanel" style="margin-top:25px">
                          <input type="radio" name="adhocgroup" onclick="viewPanel()" value="Create" checked /><label>Create New Ad-Hoc Group</label>
                          <input type="radio" name="adhocgroup" onclick="viewPanel()" value="Exists" /><label>Add to Existing Ad-Hoc Group</label>
                          <div id="Exist" style="display:none">
                          <select id="loadAdhoc" ><select>
                          </div>
                          <div id="create">
                          <input type="text" id="newadhoc" autocomplete="off"/>
                          </div>
                          
                      </div>
     
                      <a href="#" class="previous" onclick="return createadhoc()">Submit</a>
                </div>
            </div>
            <div class="modal-footer">
               
            </div>
        </div>
    </div>
    <script>
      /*  var modal = document.getElementById('myModal');
        var btn = document.getElementById("myBtn");
        var span = document.getElementsByClassName("close")[0];
        btn.onclick = function () {
        $("#AdHocPanel").attr("disabled", "disabled");
            modal.style.display = "block";
            return false;
        }
        span.onclick = function () {
            modal.style.display = "none";
        }
       window.onclick = function(event) {
      if (event.target == modal) {
        modal.style.display = "none";
    }
} */
</script>
<script>
var expanded = false;

function showCheckboxes() {
  var checkboxes = document.getElementById("checkboxes");
  if (!expanded) {
    checkboxes.style.display = "block";
    expanded = true;
  } else {
    checkboxes.style.display = "none";
    expanded = false;
  }
}

</script>
</asp:Content>
