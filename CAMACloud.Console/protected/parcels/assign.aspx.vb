﻿
Partial Class parcels_assign
	Inherits System.Web.UI.Page

    'TODO Show user's full name in Assign.aspx GridView

	Sub LoadNeighborhoods()
		ddlNbhd1.FillFromSql("SELECT DISTINCT SUBSTRING(Number,1,2) FROM Neighborhood ORDER BY 1", True, "--")
		LoadNeighborhoodSubList()
	End Sub

	Sub LoadNeighborhoodSubList() Handles ddlNbhd1.SelectedIndexChanged
        ddlNbhd2.FillFromSql("SELECT Id, SUBSTRING(Number, 3,100) FROM Neighborhood WHERE (DATALENGTH('" + ddlNbhd1.SelectedItem.Text + "') = 2 AND Number LIKE '" + ddlNbhd1.SelectedItem.Text + "%') OR (DATALENGTH('" + ddlNbhd1.SelectedItem.Text + "') < 2 AND Number = '" + ddlNbhd1.SelectedItem.Text + "') ORDER BY Number", True, "--")
        btnAssign.Enabled = False
        nbhdDetails.Visible = False
	End Sub

	Sub LoadUsers()
		Dim uc = Membership.GetAllUsers
		Dim users(uc.Count - 1) As MembershipUser
		Membership.GetAllUsers().CopyTo(users, 0)
        gvUsers.DataSource = users.Where(Function(x) Roles.IsUserInRole(x.UserName, "MobileAssessor"))
		gvUsers.DataBind()
    End Sub

    Sub LoadUserList()
        'Dim uc = Membership.GetAllUsers
        'Dim users(uc.Count - 1) As MembershipUser
        'Membership.GetAllUsers().CopyTo(users, 0)
        'Dim fielduser = users.Where(Function(x) Roles.IsUserInRole(x.UserName, "MobileAssessor"))
        'Dim dataUsers As New DataTable()
        'dataUsers.Columns.Add("UserName")
        'For Each u As MembershipUser In fielduser
        '    Dim dr As DataRow = dataUsers.NewRow
        '    dr("UserName") = u.UserName
        '    dataUsers.Rows.Add(dr)
        'Next

        'Dim userFromSettings As DataTable = Database.Tenant.GetDataTable("SELECT FirstName,LastName,LoginId FROM UserSettings")
        'Dim query = From A In userFromSettings.AsEnumerable
        '      Join B In dataUsers.AsEnumerable On
        '      A.Field(Of String)("LoginId") Equals B.Field(Of String)("UserName")
        '      Order By If((A.Field(Of String)("FirstName") + " " + A.Field(Of String)("LastName")), B.Field(Of String)("UserName"))
        '      Select New With {
        '      .LoginId = B.Field(Of String)("UserName"),
        '      .UserName = If((A.Field(Of String)("FirstName") + " " + A.Field(Of String)("LastName")), B.Field(Of String)("UserName"))
        ''     }
        'gvUsers.DataSource = query
        Dim ApplicationName As String = System.Web.Security.Membership.Provider.ApplicationName
        Dim query As String
        query = "EXEC sp_GetAllUsersWithSpecificRole @ApplicationName={0}, @OrganizationID={1},@Role={2}".SqlFormatString(ApplicationName, HttpContext.Current.GetCAMASession.OrganizationId.ToString, "MobileAssessor")
        Dim dtUser As DataTable = Database.System.GetDataTable(query)
        If dtUser.Rows.Count > 0 Then
            gvUsers.DataSource = dtUser
            gvUsers.DataBind()
        End If 
    End Sub

   
	Sub LoadUserNbhds()
		gvNbhds.DataSource = Database.Tenant.GetDataTable("SELECT * FROM Neighborhood WHERE AssignedTo = '" + hdnUserName.Value + "' ORDER BY Number")
		gvNbhds.DataBind()

		If gvNbhds.Rows.Count > 1 Then
			lbRemoveAll.Visible = True
		Else
			lbRemoveAll.Visible = False
		End If
	End Sub

	Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
		If Not IsPostBack Then
            'LoadUsers()
            LoadUserList()
		End If
	End Sub

	Protected Sub gvUsers_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvUsers.RowCommand
		If e.CommandName = "Assign" Then
            Dim userName As String = e.CommandArgument
            hdnUserName.Value = userName
            hdnFullUserName.Value = Database.Tenant.GetStringValue("select FirstName +' '+  ISNULL( LastName,'')  FROM UserSettings WHERE LoginId={0}".SqlFormatString(userName))
            lblName.Text = If(hdnFullUserName.Value, userName)
            LoadNeighborhoods()
            LoadUserNbhds()

            pnlAssign.Visible = True
            pnlUsers.Visible = False
        End If
	End Sub

	Protected Sub ddlNbhd2_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlNbhd2.SelectedIndexChanged
		If ddlNbhd2.SelectedValue = "" Then
			btnAssign.Enabled = False
			nbhdDetails.Visible = False
		Else

			btnAssign.Enabled = True
			nbhdDetails.Visible = True
			btnAssign.OnClientClick = ""

			Dim nbhd As String = ddlNbhd1.SelectedItem.Text + ddlNbhd2.SelectedItem.Text
			lblNbhdName.Text = nbhd

            Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT n.*,COALESCE(us.FirstName + COALESCE(' ' + us.LastName, ''),  n.assignedto)As UserAssigned FROM Neighborhood n LEFT OUTER JOIN UserSettings us ON us.LoginId=n.assignedto WHERE n.Id = " + ddlNbhd2.SelectedValue)
			Dim assignedTo As String = dr.GetString("AssignedTo")
            lblAssignedTo.Text = dr.GetString("UserAssigned")
			If assignedTo = "" Then
				lblAssignedTo.Text = "None"
            ElseIf assignedTo = hdnUserName.Value Then
                lblAssignedTo.Text = "You"
                btnAssign.Enabled = True
			Else
                btnAssign.OnClientClick = "return confirm('The selected neighborhood is assigned to another user. Are you sure you want to reassign it to the selected user?')"
			End If

			hdnNeighborhoodId.Value = dr.GetInteger("Id")
		End If
	End Sub

    Protected Sub btnAssign_Click(sender As Object, e As System.EventArgs) Handles btnAssign.Click
        Database.Tenant.Execute("EXEC cc_AssignNbhdToUser " + hdnUserName.Value.ToSqlValue + ", " + hdnNeighborhoodId.Value)
        Dim Neighborhood As New DataTable
        Neighborhood = _getNeighborhoodById(hdnNeighborhoodId.Value)
        If Neighborhood IsNot Nothing Then
            Dim neighborhoodName As String
            Dim txtNeighborhoodName As String = CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName()
            neighborhoodName = Neighborhood.Rows(0)("Name").ToString()
            Try
                NotificationMailer.SendNotification(hdnUserName.Value, txtNeighborhoodName + " assigned", txtNeighborhoodName + " " + neighborhoodName + " assigned to you.")
            Catch ex As Exception
                Alert(ex.Message)
            End Try

        End If
        LoadNeighborhoodSubList()
        LoadUserNbhds()
      

    End Sub

	Protected Sub lbBack_Click(sender As Object, e As System.EventArgs) Handles lbBack.Click
		pnlAssign.Visible = False
		pnlUsers.Visible = True
	End Sub

	Protected Sub gvNbhds_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvNbhds.RowCommand
		If e.CommandName = "Unassign" Then
			Database.Tenant.Execute("EXEC cc_UnassignNbhd " + e.CommandArgument)
			LoadUserNbhds()
			clearform()
		End If
	End Sub

	Protected Sub lbRemoveAll_Click(sender As Object, e As System.EventArgs) Handles lbRemoveAll.Click
		Database.Tenant.Execute("EXEC cc_UnassignUserNbhds " + hdnUserName.Value.ToSqlValue)
		LoadUserNbhds()
    End Sub
    Private Function _getNeighborhoodById(id As String) As DataTable

        Dim dtNeighborhood As DataTable
        dtNeighborhood = Database.Tenant.GetDataTable("SELECT Name,Number FROM Neighborhood where id=" + id)
        Return dtNeighborhood

    End Function

    Protected Sub gvUsers_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvUsers.PageIndexChanging
        gvUsers.PageIndex = e.NewPageIndex
        LoadUserList()
    End Sub
    Sub clearform()
    	ddlNbhd1.SelectedValue = ""
    	ddlNbhd2.SelectedValue = ""
		btnAssign.Enabled = False
		nbhdDetails.Visible = False
    End Sub
End Class
