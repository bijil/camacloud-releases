﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/ParcelManager.master" CodeBehind="delgroups.aspx.vb" Inherits="CAMACloud.Console.manageneighborhood" %>

<%@ Import Namespace="CAMACloud" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
       <script type="text/javascript" src="/App_Static/js/parcelmanager.js"></script>
       <script type="text/javascript">
        	function validateDeleteGroup( type ){
        		var _result = true;
        		if($(".grid-selection input:checkbox").length == 0){
        			//alert('No empty Assignment Groups found.');
		            _result =  false;
        		}
        		else{
		        	if(type == 'deleteSelected'){
		        		if ( $(".grid-selection input:checkbox:checked").length == 0 ) {
			                alert('You have not selected any groups from the list.');
			                _result =  false;
			            }
			            else
		        			_result = confirm('Are you sure you want to delete the selected groups?');
		        	}
		        	else
		        		_result = confirm('Are you sure you want to delete all empty groups?');
	        	}
	            return _result;
           }

           function toggleAscDesc(click) {
               var val = $('#<%= btnAscDesc.ClientID%>').val();
            	//if ($('#<%= ddlOrderBy.ClientID%>').val() == "TotalParcels") $('.asc').hide();
            	//else 
            	//$('.asc').show();
            	if (click) {
	            	if (val == "Descending") {
	            		$('.asc').css('background-position-x','1px');
	            	}
	            	else if (val == "Ascending") {
	            		$('.asc').css('background-position-x','-12px');
	            	}
                   if (click) $('#<%= btnAscDesc.ClientID%>').click();
               }
               else {
                   if (val == "Descending") {
                       $('.asc').css('background-position-x', '-12px');
                   }
                   else if (val == "Ascending") {
                       $('.asc').css('background-position-x', '1px');
                   }
               }
               return true
           }
           

          
       </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1><span class="tip" style="width:200px" abbr="The Assingment Groups listed below are those that do not contain any properties in them. If these Assignment Groups are old codes that will no longer be used they can be selected one-by-one, or deleted at once with 'Delete All Empty Groups'. This will not delete any properties or harm data, but will simply delete the Assignment Group <b>code</b> that has no properties in the group.">
     Cleanup Empty <%=CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName()%>s
    </span></h1>
     <asp:UpdatePanel runat="server" ID="uplGrid">
        <ContentTemplate>
            <table>
               <tr>
                 <td>
                         <div class="link-panel">
                                <asp:LinkButton runat="server" ID="lbDeleteNbhd" Text="Delete Selected" OnClientClick="return validateDeleteGroup('deleteSelected');" />
                         </div>
                 </td>
                 <td>
                      <div class="link-panel">
                           <asp:LinkButton runat="server" ID="lnkDeleteAll" Text="Delete All Empty Groups" OnClientClick="return validateDeleteGroup();" />
                       </div>
                 </td>
                   <td>
                       <span class="spacer"></span>
		Sort by:
		<asp:DropDownList runat="server" ID="ddlOrderBy" AutoPostBack="true">
			<asp:ListItem Text="Assgn Grp" Value="Number" />
			<asp:ListItem Text="Name" Value="Name" />			
		</asp:DropDownList>
        &nbsp;
        <asp:DropDownList runat="server" ID="ddlFilterOperator" AutoPostBack="true">
            <asp:ListItem Text="-- All --" Value="0" />
			<asp:ListItem Text="Equal to" Value="1" />
			<asp:ListItem Text="Contains" Value="2" />
			<asp:ListItem Text="Starts with" Value="3" />
            <asp:ListItem Text="Ends with" Value="4" />
		</asp:DropDownList>
                       &nbsp;
                    <asp:TextBox ID="tbSearchText" runat="server" Width="120px" placeholder="Enter text for search" Visible="false"></asp:TextBox>  
                        &nbsp;   
                    <asp:Button ID="btnSearch" runat="server" Text="Search" Visible="false" />&nbsp;  
                    <asp:Button ID="btnAscDesc" runat="server" style="display: none;" Text="Ascending" OnClick="btnAscDesc_Click" /> 

                   </td>
                   <td>
                     <input type="button" class="asc" style="background:url(/App_Static/images/asc-desc-new.png); background-repeat: no-repeat; background-position-x: 1px; background-size: 206%;"  onclick=" return toggleAscDesc(true);" />
                   </td>
               </tr>
               
             </table>
           
                <asp:GridView ID="gdvEmptyNeighborhoods" runat="server" AutoGenerateColumns="false" AllowPaging="true" PageSize="50">
                <Columns>
                    <asp:TemplateField>
                        <ItemStyle Width="25px" />
                        <ItemTemplate>
                            <%# Container.DataItemIndex + 1 %>.
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemStyle Width="20px" />
                        <ItemTemplate>
                            <asp:CheckBox ID="chkSelect" runat="server" Checked="false" CssClass="grid-selection" />
                             <asp:HiddenField ID="NID" runat="server" Value='<%# Eval("Id") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Assgn Grp." DataField="Number">
                        <ItemStyle Width="120px"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Name" DataField="Name">
                        <ItemStyle Width="300"></ItemStyle>
                    </asp:BoundField>
                                
                                      
                </Columns>
                <EmptyDataTemplate>
                    <div class="info">
                        No empty <%=CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName()%>s found.
                    </div>
                </EmptyDataTemplate>
            </asp:GridView>
         
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
