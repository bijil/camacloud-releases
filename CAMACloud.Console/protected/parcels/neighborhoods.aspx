﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/ParcelManager.master"
	AutoEventWireup="false" Inherits="CAMACloud.Console.parcels_neighborhoods" Codebehind="neighborhoods.aspx.vb" %>

<%@ Import Namespace="CAMACloud" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
		.count-col, .count-head
		{
			width: 90px;
			text-align: center !important;
		}
		
		.date-col, .date-head
		{
			width: 90px;
			text-align: center !important;
		}
		
		.col-total
		{
			background: #EFEFDF;
		}
		
		.col-visited
		{
			background: #DFEFEF;
		}
		
		.col-nbhd
		{
			width: 120px;
			font-weight: bold;
			word-break: break-all;
		}
		
		.spacer
		{
			display: inline-block;
			width: 30px;
		}
        .user-spacer
		{
			display: inline-block;
			width: 39px;
		}
        .user-filter
        {
            visibility:hidden;
        }
        .asc {
        	width: 20px;
        	height: 20px;
        	cursor: pointer;
        	border: 1px solid black;
    	    position: absolute;
        }
        .dsldesc{
    	    height: 285px;
		    overflow-y: auto;
		    background-color: #fec;
		    border: 1px solid #d9ceb8;
		    box-shadow: 2px 2px 2px #e6e3dd;
		    padding: 5px;
		    overflow: auto;
		    position: absolute;
		    z-index: 10000;
		    margin-left: 350px;
		    width: 400px;
        }
        .gnNbhdsFooter {
            background-color: #e9f5cf !important;
        }

        .gnNbhdsFooter td {
            color: #128ee3 !important;
        }
	</style>
    <script src="/App_Static/js/parcelmanager.js"></script>
    <script type="text/javascript">
            var Neighborhoods = [];
            $("[id*=chkHeader]").live("click", function () {
                var chkHeader = $(this);
                var grid = $(this).closest("table");
                $("input[type=checkbox]", grid).each(function () {
                    if (chkHeader.is(":checked")) {
                        $(this).attr("checked", "checked");
                    } else {
                        $(this).removeAttr("checked");
                    }
                });
                AddOrRemoveList();
                EnableAssignButton();
            });
            $("[id*=chkAssign]").live("click", function () {
                var grid = $(this).closest("table");
                var chkHeader = $("[id*=chkHeader]", grid);
                if (!$(this).is(":checked")) {
                    chkHeader.removeAttr("checked");
                } else {
                    if ($("[id*=chkAssign]", grid).length == $("[id*=chkAssign]:checked", grid).length) {
                        chkHeader.attr("checked", "checked");
                    }
                }
                AddOrRemoveList();
                EnableAssignButton();
            });

            function toggleAscDesc(click) {
            	var val = $('#<%= btnAscDesc.ClientID%>').val();
            	//if ($('#<%= ddlOrderBy.ClientID%>').val() == "TotalParcels") $('.asc').hide();
            	//else 
            	//$('.asc').show();
            	if (click) {
	            	if (val == "Descending") {
	            		$('.asc').css('background-position-x','1px');
	            	}
	            	else if (val == "Ascending") {
	            		$('.asc').css('background-position-x','-18px');
	            	}
	            	if (click) $('#<%= btnAscDesc.ClientID%>').click();
            	}
            	else {
            		if (val == "Descending") {
	            		$('.asc').css('background-position-x','-18px');
	            	}
	            	else if (val == "Ascending") {
	            		$('.asc').css('background-position-x','1px');
	            	}
            	}
            }
            
            function AddOrRemoveList() {
                $('.cbAssigned input').each(function () {
                    var c = this;

                    var nbhId = $(this).parent().attr("NeighborhoodId")
                    var assignedTo = $(this).parent().attr("AssignedTo");
                    var selectedNbh = nbhId + '$' + assignedTo
                    if (c.checked) {
                        if (Neighborhoods.indexOf(selectedNbh) == -1) {
                            Neighborhoods.push(selectedNbh);
                        }
                    }
                    else {
                        if (Neighborhoods.indexOf(selectedNbh) != -1) {
                            Neighborhoods.splice(Neighborhoods.indexOf(selectedNbh.toString()), 1)
                        }
                    }
                });
                $('#<%= hfCount.ClientID%>').val(Neighborhoods);
                $('#<%= lblSelectedAGCount.ClientID%>').text(Neighborhoods.length);
            }


            function selectedNeighborhoods() {
            if($('#<%= hfCount.ClientID%>').val()){
                Neighborhoods = $('#<%= hfCount.ClientID%>').val().split(',');
                $('#<%= lblSelectedAGCount.ClientID%>').text(Neighborhoods.length);
                }
            }


        function EnableAssignButton() {
            var currentFilter = $('#<%= ddlFilter.ClientID %>').val();
            var currentSelectedUser = $('#<%= ddlAssignedTo.ClientID %>').val();
            var nbh = [];
            var cc = $('#<%= hfCount.ClientID %>').val();    
            if (cc)
                nbh = cc.split(',');    
            var count = nbh.length;    
            $('#<%= lblSelectedAGCount.ClientID %>').text(count);
            var btnAssign = $('#<%= btnAssign.ClientID %>');
            var btnUnassign = $('#<%= btnUnassign.ClientID %>');
            if (currentSelectedUser !== "" && count > 0) {
                btnAssign.prop('disabled', false);
            } else {
                btnAssign.prop('disabled', true);
                btnUnassign.prop('disabled', true);
            }
            if (currentFilter == 4 || currentFilter == 5) {
                
                btnUnassign.prop('disabled', true);
            } else if (count > 0){
               
                btnUnassign.prop('disabled', false);
            }
        }




            function DisableAssignButton() {
                $('#<%= btnAssign.ClientID%>').attr('disabled', true);
                $('#<%= btnUnassign.ClientID%>').attr('disabled', true);
            }

            function GetSelectedNeighborhood() {
                $('.cbAssigned input').each(function () {
                    var c = this;
                    var nbhId = $(this).parent().attr("NeighborhoodId")
                    var assignedTo = $(this).parent().attr("AssignedTo");
                    var selectedNbh = nbhId + '$' + assignedTo
                    if (Neighborhoods.indexOf(selectedNbh) != -1) {
                        c.checked = true;
                    }
                    else {
                        c.checked = false;
                    }
                });
                if( $(".cbAssigned input:checkbox:not(:checked)").length==0)
                    $("[id*=chkHeader]").attr("checked", "checked");
            }

            function ConfirmAssignNeighborhoods() {
                var ddlAssignedTo = $("[id*=ddlAssignedTo]");
                var selectedUser = ddlAssignedTo.find("option:selected").text();
                var message;
                var count = 0;
                var selectedNbhs = [];
                var cc = $('#<%= hfCount.ClientID%>').val();
                if (cc)
                    selectedNbhs = cc.split(',');
                $.each(selectedNbhs, function (key, value) {
                    var selectedNbh = value
                    var assignedTo = selectedNbh.split("$").pop();
                    if (assignedTo != "") {
                        count = count + 1;
                    }
                });
                if (count > 0) {
                    message = "One or more assignment groups in the selection is currently assigned to a user. Do you wish you proceed?"
                    if (confirm(message)) {
                        //assignedToUser(selectedNbhs, selectedUser)
                        return true;
                    } else {
                        return false;
                    }
                }
                else {
                    var message = "Are you sure you want to assign the selected assignment groups to '" + selectedUser + "'?"
                    if (confirm(message)) {
                        //assignedToUser(selectedNbhs, selectedUser);
                        return true;
                    } else {
                        return false;
                    }
                }
            }
            $(window).bind('click', function () {
        		hideDescription();
			});
            function description(){
             if (!(localStorage.getItem('toolTipEnabled') == 0) && $('#MainContent_MainContent_ddlFilter').is(':focus') == false)
            	$('.dsldesc').show();
            	return false;
            }
            function hideDescription(){
            	$('.dsldesc').hide();
            }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <h1>
		<%=CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName() %>s Status Report
	</h1>
	<div>
		Select filter:
		<asp:DropDownList runat="server" ID="ddlFilter" Width="275px" onmouseover="description();" onmouseleave="hideDescription();" AutoPostBack="true">
			<asp:ListItem Text="Currently Progressing" Value="1" />
			<asp:ListItem Text="Assigned but Not Completed" Value="2" />
			<asp:ListItem Text="Completed Groups" Value="3" />
            <asp:ListItem Text="Unassigned Groups with Pending Priorities" Value="4" />
			<asp:ListItem Text="Unassigned Groups" Value="5" />
			<asp:ListItem Text="No Priorities" Value="6" />
            <asp:ListItem Text="Assignment Groups By User" Value="7" />
			<asp:ListItem Text="Show All" Value="0" />
		</asp:DropDownList>
         <div class="dsldesc" style="display:none;">
         <div><span style="font-weight: bold;">Currently Progressing: </span>Assignment Group has been downloaded by the Appraiser and first priority property marked as complete.</div>
		 <div><span style="font-weight: bold;">Assigned but Not Completed: </span>Assignment Groups have been created and are assigned to Appraisers.</div>
		 <div><span style="font-weight: bold;">Completed Groups: </span>All priority properties are worked and then marked as complete.</div>
		 <div><span style="font-weight: bold;">Unassigned Groups with Pending Priorities: </span>Assignment Groups that have been created with priority parcels but not been assigned to an Appraiser.</div>
		 <div><span style="font-weight: bold;">Unassigned Groups: </span>Shows any group that has a property in need of a field check that has not been assigned to an Appraiser.</div>
		 <div><span style="font-weight: bold;">No Priorities: </span>Properties that need a field check but have not been assigned a priority.</div>
		 <%--<div><span style="font-weight: bold;">No Priorities: </span>Properties that need a field check but have not been assigned a priority of high or urgent.</div>--%>
         <div><span style="font-weight: bold;">Assignment Groups by User: </span>Shows all assignment groups assigned to a specific user.</div>
		 <div><span style="font-weight: bold;">Show All: </span>Displays all Assignment Groups.</div>
		 </div>
		<span class="spacer"></span>
		Sort by:
		<asp:DropDownList runat="server" ID="ddlOrderBy" AutoPostBack="true">
			<asp:ListItem Text="Appraiser" Value="AssignedTo" />
			<asp:ListItem Text="Total Parcels" Value="TotalParcels" />
			<asp:ListItem Text="Start Date" Value="FirstVisitedDate" />
		</asp:DropDownList>
        &nbsp;
        <asp:DropDownList runat="server" ID="ddlFilterOperator" AutoPostBack="true" >
            <asp:ListItem Text="-- All --" Value="0" />
			<asp:ListItem Text="Equal to" Value="1" />
			<asp:ListItem Text="Contains" Value="2" />
			<asp:ListItem Text="Starts with" Value="3" />
            <asp:ListItem Text="Ends with" Value="4" />
		</asp:DropDownList>
        &nbsp;
        <asp:TextBox ID="tbSearchText" runat="server" Visible ="false" Width="120px" placeholder="Enter text for search"></asp:TextBox>  
        &nbsp;   
           <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" Visible="false" />
        &nbsp;   
           <asp:Button ID="btnAscDesc" runat="server" style="display: none;" Text="Ascending" OnClick="btnAscDesc_Click" />           
           <input type="button" class="asc" style="background:url(/App_Static/images/asc-desc-new.png); background-repeat: no-repeat; background-position-x: 1px; background-size: 206%;" onclick="toggleAscDesc(true);" />
           
    </div>
    <div ID="AssignmentGroupsBy" runat="server" style ="margin-top: 5px;">
        Select User:
            <asp:DropDownList ID="ddlUserFilter" runat="server" width="275px" AutoPostBack="true"></asp:DropDownList>
     </div>
    <div  ID="UserFilter" runat="server" style ="margin-top: 5px;">
        Assign To:
        <asp:DropDownList ID="ddlAssignedTo" runat="server"  style="margin-left: 8px; width:275px" AutoPostBack="true"></asp:DropDownList>
        <asp:HiddenField ID="hfCount" runat="server"  />
        <asp:Button  ID="btnAssign"  OnClientClick="javascript:return ConfirmAssignNeighborhoods();"  Visible="false" style ="display: inline-block; margin-left: 30px;" runat="server" Text="Assign to User"  />
        <asp:Button  ID="btnUnassign" style ="display: inline-block; margin-left: 10px;" runat="server" Text="Unassign Selected Groups"  />
        <span ID="selectedCount" runat="server" style="margin-left: 64px;">Selected Assignment Groups Count: </span><asp:Label ID="lblSelectedAGCount" Text="0" runat="server" ></asp:Label>  
    </div>
	<div style="display: inline-block; margin-top: 15px;width: 99%">
		<table style="border-spacing: 0px; width: 100%; font-weight: bold;">
			<tr>
                <td style="font-weight:normal;text-align:left">
                  <asp:Panel ID="PageSize" runat="server">
                 No. of items per page:
		        <asp:DropDownList runat="server" ID="ddlPageSize" AutoPostBack="true">
			       <asp:ListItem Value="25" />
			       <asp:ListItem Value="50" />
			       <asp:ListItem Value="100" />
			      <asp:ListItem Value="250" />
		         </asp:DropDownList>
                 </asp:Panel>
                </td>
				<td style="text-align:center">
					<asp:Label runat="server" ID="lblPage" />
				</td>
				<td style="text-align: right;">
					<asp:LinkButton runat="server" ID="lbExport" Text="Export to Excel (All Records)" />
				</td>
			</tr>
		</table>
        <%If EnableNewPriorities Then %>
            <asp:GridView runat="server" ID="gvNbhdNew" AllowPaging="true"  OnPageIndexChanging = "gvUserPageChanging" PageSize="8" Width="100%">
			    <Columns>
                   <asp:TemplateField ItemStyle-Width = "20px" HeaderText=" ">
                <HeaderTemplate>
                    <asp:CheckBox ID="chkHeader" runat="server" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="chkAssign" runat="server" NeighborhoodId='<%# Eval("Id")%>' AssignedTo='<%# Eval("AssignedTo")%>' class='cbAssigned' />
                     <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Eval("Id")%>' />  
                </ItemTemplate>
                    </asp:TemplateField>                
				    <asp:BoundField DataField="Number" HeaderText="Assgn Grp#" ItemStyle-CssClass="col-nbhd" />
                     <asp:BoundField DataField="FullName" HeaderText="Appraiser" HeaderStyle-CssClass="tip" ItemStyle-Width="120px" AccessibleHeaderText="The Assignment Group’s assigned Appraiser can be filtered on in the Quality Control module." />
				    <asp:BoundField DataField="TotalParcels" HeaderText="Total Parcels" ItemStyle-CssClass="count-col col-total"
					    HeaderStyle-CssClass="count-head head-total" />
                    <asp:BoundField DataField="TotalCritical" HeaderText="Total Critical " ItemStyle-CssClass="count-col col-total"
					    HeaderStyle-CssClass="count-head head-total tip" AccessibleHeaderText="Total Critical priority properties flagged in the Assignment Group." />
				    <asp:BoundField DataField="TotalAlerts" HeaderText="Total Urgent " ItemStyle-CssClass="count-col col-total"
					    HeaderStyle-CssClass="count-head head-total tip" AccessibleHeaderText="Total Urgent priority properties flagged in the Assignment Group." />
                    <asp:BoundField DataField="TotalPriorities" HeaderText="Total High " ItemStyle-CssClass="count-col col-total"
					    HeaderStyle-CssClass="count-head head-total tip" AccessibleHeaderText="Total High priority properties flagged in the Assignment Group." />
                    <asp:BoundField DataField="TotalMedium" HeaderText="Total Medium " ItemStyle-CssClass="count-col col-total"
					    HeaderStyle-CssClass="count-head head-total tip" AccessibleHeaderText="Total Medium priority properties flagged in the Assignment Group." />
                    <asp:BoundField DataField="TotalNormal" HeaderText="Total Normal " ItemStyle-CssClass="count-col col-total"
					    HeaderStyle-CssClass="count-head head-total tip" AccessibleHeaderText="Total Normal priority properties flagged in the Assignment Group." />
                    <asp:BoundField DataField="VisitedParcels" HeaderText="Total Visited Parcels" ItemStyle-CssClass="count-col col-visited"
					    HeaderStyle-CssClass="count-head head-visited  tip" AccessibleHeaderText="Parcels that have been marked as complete." />
                    <asp:BoundField DataField="VisitedCritical" HeaderText="Visited Critical " ItemStyle-CssClass="count-col col-visited"
					    HeaderStyle-CssClass="count-head head-visited tip" AccessibleHeaderText="Critical priority properties that have been marked as complete." />
                    <asp:BoundField DataField="VisitedAlerts" HeaderText="Visited Urgent " ItemStyle-CssClass="count-col col-visited"
					    HeaderStyle-CssClass="count-head head-visited tip" AccessibleHeaderText="Urgent priority properties that have been marked as complete." />
                    <asp:BoundField DataField="VisitedPriorities" HeaderText="Visited High " ItemStyle-CssClass="count-col col-visited"
					    HeaderStyle-CssClass="count-head head-visited tip" AccessibleHeaderText="High priority properties that have been marked as complete." />
                    <asp:BoundField DataField="VisitedMedium" HeaderText="Visited Medium " ItemStyle-CssClass="count-col col-visited"
					    HeaderStyle-CssClass="count-head head-visited tip" AccessibleHeaderText="Medium priority properties that have been marked as complete." />
                    <asp:BoundField DataField="VisitedNormal" HeaderText="Visited Normal " ItemStyle-CssClass="count-col col-visited"
					    HeaderStyle-CssClass="count-head head-visited tip" AccessibleHeaderText="Normal priority properties that have been marked as complete." />
                    <asp:BoundField DataField="PendingPriorities" HeaderText="Pending Priorities" ItemStyle-CssClass="count-col col-visited"
					    HeaderStyle-CssClass="count-head head-visited  tip" AccessibleHeaderText="The total of Priorities remaining to be worked." />
				    <asp:BoundField DataField="LastVisitedDate" HeaderText="Last Activity" DataFormatString="{0:MM/dd/yyyy}"
					    ItemStyle-CssClass="date-col" HeaderStyle-CssClass="date-head" />
			    </Columns>
		    </asp:GridView>
        <%Else %>
            <asp:GridView runat="server" ID="gvNbhd" AllowPaging="true" ShowFooter="true" OnPageIndexChanging = "gvUserPageChanging" PageSize="8" Width="100%">
			    <Columns>
                   <asp:TemplateField ItemStyle-Width = "20px" HeaderText=" ">
                <HeaderTemplate>
                    <asp:CheckBox ID="chkHeader" runat="server" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="chkAssign" runat="server" NeighborhoodId='<%# Eval("Id")%>' AssignedTo='<%# Eval("AssignedTo")%>' class='cbAssigned' />
                     <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Eval("Id")%>' />  
                </ItemTemplate>
                    </asp:TemplateField>                
				    <asp:BoundField DataField="Number" HeaderText="Assgn Grp#" ItemStyle-CssClass="col-nbhd" />
                     <asp:BoundField DataField="FullName" HeaderText="Appraiser" HeaderStyle-CssClass="tip" ItemStyle-Width="120px" AccessibleHeaderText="The Assignment Group’s assigned Appraiser can be filtered on in the Quality Control module." />
				    <asp:BoundField DataField="TotalParcels" HeaderText="Total Parcels" ItemStyle-CssClass="count-col col-total"
					    HeaderStyle-CssClass="count-head head-total" />
				    <asp:BoundField DataField="TotalAlerts" HeaderText="Total Urgent Priorities" ItemStyle-CssClass="count-col col-total"
					    HeaderStyle-CssClass="count-head head-total  tip" AccessibleHeaderText="Total Urgent priority properties flagged in the Assignment Group." />
				    <asp:BoundField DataField="TotalPriorities" HeaderText="Total High Priorities " ItemStyle-CssClass="count-col col-total"
					    HeaderStyle-CssClass="count-head head-total tip" AccessibleHeaderText="Total High priority properties flagged in the Assignment Group." /> 
				    <asp:BoundField DataField="VisitedParcels" HeaderText="Total Visited Parcels" ItemStyle-CssClass="count-col col-visited"
					    HeaderStyle-CssClass="count-head head-visited  tip" AccessibleHeaderText="Parcels that have been marked as complete." />
				    <asp:BoundField DataField="VisitedAlerts" HeaderText="Visited Urgent Priorities" ItemStyle-CssClass="count-col col-visited"
					    HeaderStyle-CssClass="count-head head-visited tip" AccessibleHeaderText="Urgent priority properties that have been marked as complete." />
				    <asp:BoundField DataField="VisitedPriorities" HeaderText="Visited High Priorities" ItemStyle-CssClass="count-col col-visited"
					    HeaderStyle-CssClass="count-head head-visited tip" AccessibleHeaderText="High priority properties that have been marked as complete." /> 
                    <asp:BoundField DataField="PendingPriorities" HeaderText="Pending Priorities" ItemStyle-CssClass="count-col col-visited"
					    HeaderStyle-CssClass="count-head head-visited  tip" AccessibleHeaderText="The total of Priorities remaining to be worked." />
			    <%--	<asp:BoundField DataField="FirstVisitedDate" HeaderText="Start Date" DataFormatString="{0:MM/dd/yyyy}"
					    ItemStyle-CssClass="date-col" HeaderStyle-CssClass="date-head" /> --%>
				    <asp:BoundField DataField="LastVisitedDate" HeaderText="Last Activity" DataFormatString="{0:MM/dd/yyyy}"
					    ItemStyle-CssClass="date-col" HeaderStyle-CssClass="date-head" />
			    </Columns>
                <FooterStyle BackColor="#D2DBAC" Font-Bold="True" ForeColor="#1889D7" HorizontalAlign="Center" CssClass="gnNbhdsFooter" />

		    </asp:GridView>
        <%End If %>
		
		<asp:Panel runat="server" ID="pnlExcel" Visible="false">
			<style type="text/css">
				.print-count-col, .count-head
				{
					width: 120px;
					text-align: center !important;
				}
				
				.date-col, .date-head
				{
					width: 90px;
					text-align: center !important;
				}
				
				.col-total
				{
					background: #EFEFDF;
				}
				
				.col-visited
				{
					background: #DFEFEF;
				}
				
				.col-nbhd
				{
					width: 80px;
					font-weight: bold;
				}
				
				.spacer
				{
					display: inline-block;
					width: 30px;
				}
			</style>
            <% If EnableNewPriorities Then %>
               <asp:GridView runat="server" ID="gvPrintNew" AllowPaging="false">
				    <Columns>
					    <asp:TemplateField HeaderText="Assgn Grp#" ItemStyle-CssClass="col-nbhd" >
						    <ItemTemplate>
							    <%# Eval("Number")%>&nbsp;
						    </ItemTemplate>
					    </asp:TemplateField>
                        <asp:BoundField DataField ="Number" HeaderText="Assgn Grp#" ItemStyle-CssClass="col-nbhd" ItemStyle-Width=80px/>
					    <asp:BoundField DataField="FullName" HeaderText="Appraiser" ItemStyle-Width="120px" />
					    <asp:BoundField DataField="TotalParcels" HeaderText="Total Parcels" ItemStyle-CssClass="print-count-col col-total"
						    HeaderStyle-CssClass="count-head head-total" ItemStyle-HorizontalAlign="Center" ItemStyle-Width= 120px/>
					    <asp:BoundField DataField="TotalCritical" HeaderText="Total Critical" ItemStyle-CssClass="print-count-col col-total"
						    HeaderStyle-CssClass="count-head head-total" ItemStyle-HorizontalAlign="Center" ItemStyle-Width= 120px/>
                        <asp:BoundField DataField="TotalAlerts" HeaderText="Total Urgent" ItemStyle-CssClass="print-count-col col-total"
						    HeaderStyle-CssClass="count-head head-total" ItemStyle-HorizontalAlign="Center" ItemStyle-Width= 120px/>
                        <asp:BoundField DataField="TotalPriorities" HeaderText="Total High" ItemStyle-CssClass="print-count-col col-total"
						    HeaderStyle-CssClass="count-head head-total" ItemStyle-HorizontalAlign="Center" ItemStyle-Width= 120px/>
                        <asp:BoundField DataField="TotalMedium" HeaderText="Total Medium" ItemStyle-CssClass="print-count-col col-total"
						    HeaderStyle-CssClass="count-head head-total" ItemStyle-HorizontalAlign="Center" ItemStyle-Width= 120px/>
                        <asp:BoundField DataField="TotalNormal" HeaderText="Total Normal " ItemStyle-CssClass="print-count-col col-total"
						    HeaderStyle-CssClass="count-head head-total" ItemStyle-HorizontalAlign="Center" ItemStyle-Width= 120px/>
					    <asp:BoundField DataField="VisitedParcels" HeaderText="Visited Parcels" ItemStyle-CssClass="print-count-col col-visited"
						    HeaderStyle-CssClass="count-head head-visited" ItemStyle-HorizontalAlign="Center" ItemStyle-Width= 120px/>
					    <asp:BoundField DataField="VisitedCritical" HeaderText="Visited Critical" ItemStyle-CssClass="print-count-col col-visited"
						    HeaderStyle-CssClass="count-head head-visited" ItemStyle-HorizontalAlign="Center" ItemStyle-Width= 120px/>
                        <asp:BoundField DataField="VisitedAlerts" HeaderText="Visited Urgent" ItemStyle-CssClass="print-count-col col-visited"
						    HeaderStyle-CssClass="count-head head-visited" ItemStyle-HorizontalAlign="Center" ItemStyle-Width= 120px/>
                        <asp:BoundField DataField="VisitedPriorities" HeaderText="Visited High" ItemStyle-CssClass="print-count-col col-visited"
						    HeaderStyle-CssClass="count-head head-visited" ItemStyle-HorizontalAlign="Center" ItemStyle-Width= 120px/>
                        <asp:BoundField DataField="VisitedMedium" HeaderText="Visited Medium" ItemStyle-CssClass="print-count-col col-visited"
						    HeaderStyle-CssClass="count-head head-visited" ItemStyle-HorizontalAlign="Center" ItemStyle-Width= 120px/>
                        <asp:BoundField DataField="VisitedNormal" HeaderText="Visited Normal" ItemStyle-CssClass="print-count-col col-visited"
						    HeaderStyle-CssClass="count-head head-visited" ItemStyle-HorizontalAlign="Center" ItemStyle-Width= 120px/>
                        <asp:BoundField DataField="PendingPriorities" HeaderText="Pending Priorities" ItemStyle-CssClass="count-col col-visited"
					    HeaderStyle-CssClass="count-head head-visited" ItemStyle-Width= 120px/>
				    <%--	<asp:TemplateField ItemStyle-Width="90px" HeaderText="Status">
						    <ItemTemplate>
							    <%# IIf(Eval("Completed") is DBNull.Value orelse Eval("Completed") = False, "", "Completed")%>
						    </ItemTemplate>
					    </asp:TemplateField>
					    <asp:BoundField DataField="FirstVisitedDate" HeaderText="Start Date" DataFormatString="{0:MM/dd/yyyy}"
						    ItemStyle-CssClass="date-col" HeaderStyle-CssClass="date-head" /> --%>
					    <asp:BoundField DataField="LastVisitedTime" HeaderText="Last Activity" 
						    ItemStyle-CssClass="date-col" HeaderStyle-CssClass="date-head" ItemStyle-Width= 90px/>
				    </Columns>
			    </asp:GridView>
            <%Else %>
                <asp:GridView runat="server" ID="gvPrint" AllowPaging="false">
				    <Columns>
					    <asp:TemplateField HeaderText="Assgn Grp#" ItemStyle-CssClass="col-nbhd" >
						    <ItemTemplate>
							    <%# Eval("Number")%>&nbsp;
						    </ItemTemplate>
					    </asp:TemplateField>
                        <asp:BoundField DataField ="Number" HeaderText="Assgn Grp#" ItemStyle-CssClass="col-nbhd" ItemStyle-Width=80px/>
					    <asp:BoundField DataField="FullName" HeaderText="Appraiser" ItemStyle-Width="120px" />
					    <asp:BoundField DataField="TotalParcels" HeaderText="Total Parcels" ItemStyle-CssClass="print-count-col col-total"
						    HeaderStyle-CssClass="count-head head-total" ItemStyle-HorizontalAlign="Center" ItemStyle-Width= 120px/>
					    <asp:BoundField DataField="TotalAlerts" HeaderText="Total Urgent Priorities" ItemStyle-CssClass="print-count-col col-total"
						    HeaderStyle-CssClass="count-head head-total" ItemStyle-HorizontalAlign="Center" ItemStyle-Width= 120px/>
					    <asp:BoundField DataField="TotalPriorities" HeaderText="Total High Priorities" ItemStyle-CssClass="print-count-col col-total"
						    HeaderStyle-CssClass="count-head head-total" ItemStyle-HorizontalAlign="Center" ItemStyle-Width= 120px/>
					    <asp:BoundField DataField="VisitedParcels" HeaderText="Visited Parcels" ItemStyle-CssClass="print-count-col col-visited"
						    HeaderStyle-CssClass="count-head head-visited" ItemStyle-HorizontalAlign="Center" ItemStyle-Width= 120px/>
					    <asp:BoundField DataField="VisitedAlerts" HeaderText="Visited Urgent Priorities" ItemStyle-CssClass="print-count-col col-visited"
						    HeaderStyle-CssClass="count-head head-visited" ItemStyle-HorizontalAlign="Center" ItemStyle-Width= 120px/>
					    <asp:BoundField DataField="VisitedPriorities" HeaderText="Visited High Priorities" ItemStyle-CssClass="print-count-col col-visited"
						    HeaderStyle-CssClass="count-head head-visited" ItemStyle-HorizontalAlign="Center" ItemStyle-Width= 120px/>
                        <asp:BoundField DataField="PendingPriorities" HeaderText="Pending Priorities" ItemStyle-CssClass="count-col col-visited"
					    HeaderStyle-CssClass="count-head head-visited" ItemStyle-Width= 120px/>
				    <%--	<asp:TemplateField ItemStyle-Width="90px" HeaderText="Status">
						    <ItemTemplate>
							    <%# IIf(Eval("Completed") is DBNull.Value orelse Eval("Completed") = False, "", "Completed")%>
						    </ItemTemplate>
					    </asp:TemplateField>
					    <asp:BoundField DataField="FirstVisitedDate" HeaderText="Start Date" DataFormatString="{0:MM/dd/yyyy}"
						    ItemStyle-CssClass="date-col" HeaderStyle-CssClass="date-head" /> --%>
					    <asp:BoundField DataField="LastVisitedTime" HeaderText="Last Activity" 
						    ItemStyle-CssClass="date-col" HeaderStyle-CssClass="date-head" ItemStyle-Width= 90px/>
				    </Columns>
			    </asp:GridView>
            <%End If %>
		</asp:Panel>
	</div>
</asp:Content>

