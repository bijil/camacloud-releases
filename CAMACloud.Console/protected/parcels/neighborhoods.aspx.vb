﻿Imports System.IO
Imports System.Drawing
Imports System.Collections

Partial Class parcels_neighborhoods
    Inherits System.Web.UI.Page
    Public org As DataRow
    Public ReadOnly Property EnableNewPriorities As Boolean
        Get
            Dim EnableNewPriority = False
            Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM ClientSettings WHERE Name = 'EnableNewPriorities' AND Value = 1")
            If dr IsNot Nothing Then
                EnableNewPriority = True
            End If
            Return EnableNewPriority
        End Get
    End Property

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If IsPostBack Then
            RunScript("EnableAssignButton();")
            RunScript("toggleAscDesc();")
        End If
        If EnableNewPriorities Then
            gvNbhdNew.Visible = True
            gvNbhd.Visible = False
        Else
            gvNbhdNew.Visible = False
            gvNbhd.Visible = True
        End If
        If Not IsPostBack Then
            RunScript("DisableAssignButton();")
            If EnableNewPriorities Then
                gvNbhdNew.Columns(1).HeaderText = CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName("shortname") & "#"
            Else
                gvNbhd.Columns(1).HeaderText = CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName("shortname") & "#"
            End If

            ddlOrderBy.InsertItem(CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName(), "Number", 0)
            tbSearchText.Text = ""
            ReloadList()
        End If

        If (ddlOrderBy.SelectedIndex = 0) Then
            ddlFilterOperator.Visible = True
        Else
            ddlFilterOperator.SelectedIndex = 0
            ddlFilterOperator.Visible = False
            tbSearchText.Text = ""
            tbSearchText.Visible = False
            btnSearch.Visible = False
        End If
    End Sub
    Sub ReloadList()
        If EnableNewPriorities Then
            gvNbhdNew.PageSize = ddlPageSize.SelectedValue
            gvNbhdNew.PageIndex = 0
        Else
            gvNbhd.PageSize = ddlPageSize.SelectedValue
            gvNbhd.PageIndex = 0
        End If

        'tbSearchText.Text = ""

        ListNeighborhoods()
    End Sub

    Sub ListNeighborhoods()
        e_("EXEC [cc_UpdateNeighborhoodStatistics] NULL, 0")
        ddlOrderBy.Items.FindByValue("AssignedTo").Enabled = True
        AssignmentGroupsBy.Visible = False
        loadAssignedToUser()
        If Not ViewState("AssignedUser") Is Nothing Then
            ddlAssignedTo.SelectedValue = ViewState("AssignedUser")
        End If
        If Not ViewState("UserFilter") Is Nothing Then
            ddlUserFilter.SelectedValue = ViewState("UserFilter")
        End If
        Dim filter As String = "1 = 1"
        Dim searchField As String = "AND LTRIM(Number) IS NOT NULL"
        If (ddlFilterOperator.SelectedValue > -1) Then
            If (tbSearchText.Visible = True And IsNotEmpty(tbSearchText.Text)) Then
                Dim operatorValue As Integer = ddlFilterOperator.SelectedValue
                Dim searchText As String = tbSearchText.Text.ToString()
                If (tbSearchText.Text.Contains("_") And operatorValue > 1) Then
                    searchText = tbSearchText.Text.Replace("_", "[_]").ToString()
                End If
                Select Case ddlOrderBy.SelectedValue
                    Case "Number" 
                        If (operatorValue = 1) Then
                            searchField = " AND Number = '" + searchText + "'"
                        ElseIf (operatorValue = 2) Then
                            searchField = " AND Number Like '%" + searchText + "%'"
                        ElseIf (operatorValue = 3) Then
                            searchField = " AND Number like '" + searchText + "%'"
                        ElseIf (operatorValue = 4) Then
                            searchField = " AND Number like '%" + searchText + "'"
                        End If
                  
                End Select
            End If
        End If

        Dim hideColumns As String

        Select Case ddlFilter.SelectedValue
            Case "0"
                filter += searchField
                loadNeighborhoods(filter)
            Case "1"
                filter = "Completed = 0 AND AssignedTo IS NOT NULL AND LastVisitedDate IS NOT NULL " + searchField
                loadNeighborhoods(filter)
            Case "2"
                filter = "Completed = 0 AND AssignedTo IS NOT NULL " + searchField
                loadNeighborhoods(filter)
            Case "3"
                filter = "Completed = 1 AND VisitedParcels > 0 AND TotalPriorities > 0 " + searchField
                loadNeighborhoods(filter)
            Case "4"
                filter = "AssignedTo IS NULL AND (TotalAlerts + TotalPriorities > 0) AND PendingPriorities > 0 " + searchField
                If EnableNewPriorities Then
                    filter = "AssignedTo IS NULL AND (TotalPriorityparcels > 0) AND PendingPriorities > 0 " + searchField
                End If
                hideColumns = "Appraiser"
                loadNeighborhoods(filter, hideColumns)
                ddlOrderBy.Items.FindByValue("AssignedTo").Enabled = False

            Case "5"
                filter = "AssignedTo IS NULL " + searchField
                hideColumns = "Appraiser"
                loadNeighborhoods(filter, hideColumns)
                ddlOrderBy.Items.FindByValue("AssignedTo").Enabled = False
            Case "6"
                filter = "TotalPriorities = 0 AND TotalAlerts = 0 " + searchField
                If EnableNewPriorities Then
                    filter = "TotalPriorityparcels = 0 " + searchField
                End If
                hideColumns = "Appraiser"
                loadNeighborhoods(filter, hideColumns)
                ddlOrderBy.Items.FindByValue("AssignedTo").Enabled = False
            Case "7"
                FilterByuser()
                loadNeighborhoods(filter)
                ddlOrderBy.Items.FindByValue("AssignedTo").Enabled = False
        End Select
        If gvNbhd.FooterRow IsNot Nothing Then
            ' Calculate totals for each category
            Dim totalParcels As Integer = 0
            Dim totalUrgentPriorities As Integer = 0
            Dim totalHighPriorities As Integer = 0
            Dim totalVisitedParcels As Integer = 0
            Dim totalVisitedUrgent As Integer = 0
            Dim totalVisitedHigh As Integer = 0
            Dim totalPending As Integer = 0

            For Each row As GridViewRow In gvNbhd.Rows
                ' Increment totals for each category
                totalParcels += Convert.ToInt32(row.Cells(3).Text) ' Assuming 3rd cell is for Total Parcels
                totalUrgentPriorities += Convert.ToInt32(row.Cells(4).Text) ' Assuming 4th cell is for Total Urgent Priorities
                totalHighPriorities += Convert.ToInt32(row.Cells(5).Text) ' Assuming 5th cell is for Total High Priorities
                totalVisitedParcels += Convert.ToInt32(row.Cells(6).Text) ' Assuming 6th cell is for Total Visited Parcels
                totalVisitedUrgent += Convert.ToInt32(row.Cells(7).Text) ' Assuming 7th cell is for Visited Urgent Priorities
                totalVisitedHigh += Convert.ToInt32(row.Cells(8).Text) ' Assuming 8th cell is for Visited High Priorities
                totalPending += Convert.ToInt32(row.Cells(9).Text) ' Assuming 9th cell is for Pending Priorities
            Next

            ' Calculate percentage of unvisited/pending
            Dim totalUnvisitedPending As Integer = totalVisitedUrgent + totalVisitedHigh + totalPending
            Dim totalPercentage As Double = (totalUnvisitedPending / totalParcels) * 100

            ' Display totals in the footer row
            gvNbhd.FooterRow.Cells(2).Text = "Totals"
            gvNbhd.FooterRow.Cells(3).Text = totalParcels.ToString() ' Total Parcels
            gvNbhd.FooterRow.Cells(4).Text = totalUrgentPriorities.ToString() ' Total Urgent Priorities
            gvNbhd.FooterRow.Cells(5).Text = totalHighPriorities.ToString() ' Total High Priorities
            gvNbhd.FooterRow.Cells(6).Text = totalVisitedParcels.ToString() ' Total Visited Parcels
            gvNbhd.FooterRow.Cells(7).Text = totalVisitedUrgent.ToString() ' Visited Urgent Priorities
            gvNbhd.FooterRow.Cells(8).Text = totalVisitedHigh.ToString() ' Visited High Priorities
            gvNbhd.FooterRow.Cells(9).Text = totalPending.ToString() & " / " & totalPercentage.ToString("0.##") & "%" ' Pending Priorities with percentage
        End If




    End Sub

    Sub loadNeighborhoods(ByVal filter As String, Optional ByVal hideColumns As String = "")
        Dim orderBy As String = ddlOrderBy.SelectedValue
        Select Case ddlOrderBy.SelectedValue
            Case "TotalParcels"
                orderBy = IIf(btnAscDesc.Text = "Descending", "TotalParcels DESC", "TotalParcels ASC")
            Case "AssignedTo"
                orderBy = "COALESCE(AssignedTo, 'ZZZZZZ') " + IIf(btnAscDesc.Text = "Descending", " DESC", " ASC")
            Case "FirstVisitedDate"
                orderBy = "COALESCE(FirstVisitedDate, '2999-12-31')"
            Case "Number"
                orderBy = IIf(btnAscDesc.Text = "Descending", "Number DESC", "Number ASC")
                'orderBy ="case when IsNumeric(Name) <> 1 then Name else NULL end" + IIf(btnAscDesc.Text = "Descending", " DESC", " ASC") +" , case when IsNumeric(Name) = 1 then CAST(Name as FLOAT) else NULL end  " +  IIf(btnAscDesc.Text = "Descending", " DESC", " ASC")
        End Select
        Dim hColumns = hideColumns.Split(",")
        If EnableNewPriorities Then
            For Each gvc As DataControlField In gvNbhdNew.Columns
                If hColumns.Contains(gvc.HeaderText) Then
                    gvc.Visible = False
                Else
                    gvc.Visible = True
                End If
            Next

            For Each gvprintc As DataControlField In gvPrintNew.Columns
                If hColumns.Contains(gvprintc.HeaderText) Then
                    gvprintc.Visible = False
                Else
                    gvprintc.Visible = True
                End If
            Next
        Else
            For Each gvc As DataControlField In gvNbhd.Columns
                If hColumns.Contains(gvc.HeaderText) Then
                    gvc.Visible = False
                Else
                    gvc.Visible = True
                End If
            Next

            For Each gvprintc As DataControlField In gvPrint.Columns
                If hColumns.Contains(gvprintc.HeaderText) Then
                    gvprintc.Visible = False
                Else
                    gvprintc.Visible = True
                End If
            Next
        End If


        'Dim dt As DataTable = d_("SELECT * FROM Neighborhood WHERE " + filter + " ORDER BY " + orderBy)
        Dim SortCatogory = orderBy
        If (orderBy = "LTRIM(Number) ASC" Or orderBy = "LTRIM(Number) DESC") Then
            SortCatogory = "CASE
            WHEN ISNUMERIC(LTRIM(Number)) = 1 THEN 1 
            WHEN Number LIKE '[^0-9]%' THEN 2 
            ELSE 3 
            END,
            CASE
            WHEN ISNUMERIC(LTRIM(Number)) = 1 THEN TRY_CAST(LTRIM(Number) AS FLOAT)
            ELSE NULL
            END ASC, " + orderBy
        End If
        Dim dt As DataTable = d_("SELECT FullName=CASE COALESCE(LTRIM(us.FirstName), '') + COALESCE(' ' + LTRIM(us.LastName), '') WHEN '' THEN nbh.AssignedTo ELSE COALESCE(LTRIM(us.FirstName), '') + COALESCE(' ' + LTRIM(us.LastName), '') END , CONVERT(varchar,dbo.GetLocalDate(nbh.LastVisitedDate), 10) AS LastVisitedTime,nbh.* FROM Neighborhood nbh LEFT JOIN UserSettings us ON nbh.AssignedTo=us.LoginId  WHERE " + filter + " ORDER BY " + SortCatogory)
        If EnableNewPriorities Then
            gvNbhdNew.DataSource = dt
            gvNbhdNew.DataBind()
            gvPrintNew.DataSource = dt
            gvPrintNew.DataBind()
        Else
            gvNbhd.DataSource = dt
            gvNbhd.DataBind()
            gvPrint.DataSource = dt
            gvPrint.DataBind()
        End If

        ViewState("gridData") = dt
        If (hfCount.Value <> "") Then
            RunScript("selectedNeighborhoods();")
            RunScript("GetSelectedNeighborhood();")
            RunScript("EnableAssignButton();")
        End If

        If gvNbhd.FooterRow IsNot Nothing Then
            ' Calculate totals for each category
            Dim totalParcels As Integer = 0
            Dim totalUrgentPriorities As Integer = 0
            Dim totalHighPriorities As Integer = 0
            Dim totalVisitedParcels As Integer = 0
            Dim totalVisitedUrgent As Integer = 0
            Dim totalVisitedHigh As Integer = 0
            Dim totalPending As Integer = 0

            For Each row As GridViewRow In gvNbhd.Rows
                ' Increment totals for each category
                totalParcels += Convert.ToInt32(row.Cells(3).Text) ' Assuming 3rd cell is for Total Parcels
                totalUrgentPriorities += Convert.ToInt32(row.Cells(4).Text) ' Assuming 4th cell is for Total Urgent Priorities
                totalHighPriorities += Convert.ToInt32(row.Cells(5).Text) ' Assuming 5th cell is for Total High Priorities
                totalVisitedParcels += Convert.ToInt32(row.Cells(6).Text) ' Assuming 6th cell is for Total Visited Parcels
                totalVisitedUrgent += Convert.ToInt32(row.Cells(7).Text) ' Assuming 7th cell is for Visited Urgent Priorities
                totalVisitedHigh += Convert.ToInt32(row.Cells(8).Text) ' Assuming 8th cell is for Visited High Priorities
                totalPending += Convert.ToInt32(row.Cells(9).Text) ' Assuming 9th cell is for Pending Priorities
            Next

            ' Calculate percentage of unvisited/pending
            Dim totalUnvisitedPending As Integer = totalVisitedUrgent + totalVisitedHigh + totalPending
            Dim totalPercentage As Double = (totalUnvisitedPending / totalParcels) * 100

            ' Display totals in the footer row
            gvNbhd.FooterRow.Cells(2).Text = "Totals"
            gvNbhd.FooterRow.Cells(3).Text = totalParcels.ToString() ' Total Parcels
            gvNbhd.FooterRow.Cells(4).Text = totalUrgentPriorities.ToString() ' Total Urgent Priorities
            gvNbhd.FooterRow.Cells(5).Text = totalHighPriorities.ToString() ' Total High Priorities
            gvNbhd.FooterRow.Cells(6).Text = totalVisitedParcels.ToString() ' Total Visited Parcels
            gvNbhd.FooterRow.Cells(7).Text = totalVisitedUrgent.ToString() ' Visited Urgent Priorities
            gvNbhd.FooterRow.Cells(8).Text = totalVisitedHigh.ToString() ' Visited High Priorities
            gvNbhd.FooterRow.Cells(9).Text = totalPending.ToString() & " / " & totalPercentage.ToString("0.##") & "%" ' Pending Priorities with percentage
        End If



        If EnableNewPriorities Then
            If gvNbhdNew.Rows.Count = 0 Then
                UserFilter.Visible = False
                gvNbhdNew.Visible = False
                lbExport.Visible = False
                PageSize.Visible = False
                lblPage.Visible = True
                lblPage.Text = "Sorry, it appears there is no data matching your criteria. Please adjust your search parameters and try again."
            Else
                UserFilter.Visible = True
                btnAssign.Visible = True
                gvNbhdNew.Visible = True
                lbExport.Visible = True
                lblPage.Visible = True
                PageSize.Visible = True
                selectedCount.Visible = True
                lblSelectedAGCount.Visible = True
                lblPage.Text = "Displaying records {0} to {1} of {2}".FormatString(gvNbhdNew.PageIndex * gvNbhdNew.PageSize + 1, gvNbhdNew.PageIndex * gvNbhdNew.PageSize + gvNbhdNew.Rows.Count, dt.Rows.Count)

            End If

        Else
            If gvNbhd.Rows.Count = 0 Then
                UserFilter.Visible = False
                gvNbhd.Visible = False
                lbExport.Visible = False
                PageSize.Visible = False
                lblPage.Visible = True
                lblPage.Text = "Sorry, it appears there is no data matching your criteria. Please adjust your search parameters and try again."
            Else
                UserFilter.Visible = True
                btnAssign.Visible = True
                gvNbhd.Visible = True
                lbExport.Visible = True
                lblPage.Visible = True
                PageSize.Visible = True
                selectedCount.Visible = True
                lblSelectedAGCount.Visible = True
                lblPage.Text = "Displaying records {0} to {1} of {2}".FormatString(gvNbhd.PageIndex * gvNbhd.PageSize + 1, gvNbhd.PageIndex * gvNbhd.PageSize + gvNbhd.Rows.Count, dt.Rows.Count)
            End If
        End If



    End Sub

    Private Sub FilterByuser()
        UserFilter.Visible = False
        AssignmentGroupsBy.Visible = True
        gvNbhdNew.Visible = False
        gvNbhd.Visible = False
        lblPage.Visible = False
        PageSize.Visible = False
        lbExport.Visible = False
        ddlUserFilter.Items.Clear()
        Dim dtUser As DataTable = Database.Tenant.GetDataTable("SELECT AssignedTo,CASE WHEN FirstName IS NULL AND lastname IS NULL THEN AssignedTo ELSE COALESCE(FirstName, '') + COALESCE(' ' + LastName, '') END AS FullName FROM (SELECT DISTINCT Loginid,firstname,lastname,nbh.AssignedTo FROM UserSettings u RIGHT OUTER JOIN Neighborhood nbh ON nbh.AssignedTo = u.LoginId) rslt WHERE rslt.AssignedTo IS NOT NULL ORDER BY FullName ")
        ddlUserFilter.FillFromTable(dtUser, True, "---  Select User  ---")

    End Sub
    Private Sub loadAssignedToUser()
        ddlAssignedTo.Items.Clear()
        Dim ApplicationName As String = System.Web.Security.Membership.Provider.ApplicationName
        Dim query As String
        query = "EXEC sp_GetAllUsersWithSpecificRole @ApplicationName={0}, @OrganizationID={1},@Role={2}".SqlFormatString(ApplicationName, HttpContext.Current.GetCAMASession.OrganizationId.ToString, "MobileAssessor")
        Dim dtUser As DataTable = Database.System.GetDataTable(query)
        If dtUser.Rows.Count > 0 Then
            ddlAssignedTo.FillFromTable(dtUser, True, "---  Select User  ---")
        End If
    End Sub
    Protected Sub gvUserPageChanging(sender As Object, e As GridViewPageEventArgs)
        If EnableNewPriorities Then
            gvNbhdNew.PageIndex = e.NewPageIndex
        Else
            gvNbhd.PageIndex = e.NewPageIndex
        End If

        Dim selectedFilter = ddlFilter.SelectedItem.ToString
        If (selectedFilter = "Assignment Groups By User") Then
            Dim filter As String
            If Not ViewState("UserFilter") Is Nothing Then
                ddlUserFilter.SelectedValue = ViewState("UserFilter")
                filter = "AssignedTo='" + ddlUserFilter.SelectedValue + "'"
                If (filter <> "") Then
                    loadNeighborhoods(filter)
                End If
            End If
            If Not ViewState("AssignedUser") Is Nothing Then
                ddlAssignedTo.SelectedValue = ViewState("AssignedUser")
            End If
        Else
            ListNeighborhoods()
        End If
    End Sub
    Protected Sub ddlPageSize_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged, ddlFilter.SelectedIndexChanged, ddlOrderBy.SelectedIndexChanged
        Dim selectedFilter = ddlFilter.SelectedItem.ToString
        'To remove selection while sorting
        If (sender.Id = "ddlOrderBy") Then
        	hfCount.Value = Nothing
        	If(ddlOrderBy.SelectedItem.ToString = "Total Parcels") Then
        		btnAscDesc.Text = "Descending"
        	Else
        		btnAscDesc.Text = "Ascending"
        	End IF
        End If
        If (sender.Id = "ddlFilter") Then
            ViewState("AssignedUser") = Nothing
            hfCount.Value = Nothing
            ViewState("UserFilter") = Nothing
            If (selectedFilter = "Assignment Groups By User") Then

                tbSearchText.Text = ""
                ReloadList()
                'FilterByuser()

            Else
                tbSearchText.Text = ""
                ReloadList()
            End If
        Else
            ReloadAssignmentGroupsByUser()
        End If
    End Sub

    Private Sub ddlUserFilter_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlUserFilter.SelectedIndexChanged
        hfCount.Value = Nothing
        ViewState("AssignedUser") = Nothing
        ViewState("UserFilter") = ddlUserFilter.SelectedValue
        loadAssignedToUser()
        Dim selectedFilter = ddlFilter.SelectedItem.ToString
        If (selectedFilter = "Assignment Groups By User") Then
            Dim filter = ddlUserFilter.SelectedValue
            If (filter <> "") Then
                filter = "AssignedTo='" + filter + "'"
                loadNeighborhoods(filter)
                If EnableNewPriorities Then
                    gvNbhdNew.PageSize = ddlPageSize.SelectedValue
                    gvNbhdNew.PageIndex = 0
                Else
                    gvNbhd.PageSize = ddlPageSize.SelectedValue
                    gvNbhd.PageIndex = 0
                    loadNeighborhoods(filter)
                End If

                loadNeighborhoods(filter)
            End If
        End If
    End Sub

    Private Sub ddlAssignedTo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAssignedTo.SelectedIndexChanged

        ViewState("AssignedUser") = ddlAssignedTo.SelectedValue
    End Sub
    Private Sub ddlFilter_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFilter.SelectedIndexChanged
        ddlFilterOperator.SelectedValue = 0
        tbSearchText.Visible = False
        btnSearch.Visible = False
    End Sub
    
    
    Protected Sub btnUnassign_Click(sender As Object, e As EventArgs) Handles btnUnassign.Click
        Dim SelectedNbhs = hfCount.Value
        Dim neighborhoodsArray As String() = SelectedNbhs.Split(New Char() {","c})
        Dim txtNeighborhoodName As String = CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName()
        Dim neighborhoodNameList As String=""
        For Each nbh As String In neighborhoodsArray
			Dim NeighborhoodId As String = nbh.Split("$")(0)
			Dim neighborhoodName = Database.Tenant.GetStringValue("SELECT name FROM Neighborhood WHERE id= " & NeighborhoodId)
			If (NeighborhoodId <> Nothing And neighborhoodName <> "") Then
				Database.Tenant.Execute("EXEC cc_UnassignNbhd " + NeighborhoodId)
				neighborhoodNameList+=neighborhoodName+","   
			End If
        Next
        Try
			neighborhoodNameList = neighborhoodNameList.TrimEnd(","C)
		    NotificationMailer.SendNotification(Membership.GetUser().ToString, txtNeighborhoodName + " Unassigned", txtNeighborhoodName + " " + neighborhoodNameList + " have been unassigned.")         
		Catch ex As Exception
			Alert(ex.Message)
		End Try
       
        Dim Msg As String
        If (neighborhoodsArray.Length > 1) Then
            Msg = neighborhoodsArray.Length.ToString() + " groups have been unassigned from corresponding users" 
        Else
            Msg = neighborhoodsArray.Length.ToString() + " group has been unassigned from corresponding user"
        End If
        Alert(Msg)
        hfCount.Value = Nothing
        ReloadAssignmentGroupsByUser()
    End Sub
    
    Protected Sub btnAssign_Click(sender As Object, e As EventArgs) Handles btnAssign.Click
        Dim SelectedUser As String = ddlAssignedTo.SelectedValue
        Dim SelectedUserName As String = ddlAssignedTo.SelectedItem.Text.Trim()
        Dim SelectedNbhs = hfCount.Value
        Dim neighborhoodsArray As String() = SelectedNbhs.Split(New Char() {","c})
        Dim txtNeighborhoodName As String = CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName()
        Dim neighborhoodNameList As String=""
        For Each nbh As String In neighborhoodsArray
			Dim NeighborhoodId As String = nbh.Split("$")(0)
			Dim neighborhoodName = Database.Tenant.GetStringValue("SELECT COALESCE(NULLIF(Name,''),Number) FROM Neighborhood WHERE id= " & NeighborhoodId)
			If (NeighborhoodId <> Nothing And neighborhoodName <> "" And SelectedUser <> "") Then
				Database.Tenant.Execute("EXEC cc_AssignNbhdToUser " + SelectedUser.ToSqlValue + ", " + NeighborhoodId)
				neighborhoodNameList+=neighborhoodName+","   
			End If
        Next
        Try
			neighborhoodNameList = neighborhoodNameList.TrimEnd(","C)
		    NotificationMailer.SendNotification(SelectedUser, txtNeighborhoodName + " assigned", txtNeighborhoodName + " " + neighborhoodNameList + " assigned to you.")         
		Catch ex As Exception
			Alert(ex.Message)
		End Try
       
        Dim Msg As String
        If (neighborhoodsArray.Length > 1) Then
            Msg = neighborhoodsArray.Length.ToString() + " groups have been assigned to user '" + SelectedUserName + "'"
        Else
            Msg = neighborhoodsArray.Length.ToString() + " group have been assigned to user '" + SelectedUserName + "'"
        End If
        Alert(Msg)
        ViewState("AssignedUser") = Nothing
        hfCount.Value = Nothing
        ReloadAssignmentGroupsByUser()
        ddlAssignedTo.SelectedValue = ViewState("AssignedUser")
    End Sub
    Protected Sub ReloadAssignmentGroupsByUser()
        Dim selectedFilter = ddlFilter.SelectedItem.ToString
        If (selectedFilter = "Assignment Groups By User") Then
            Dim filter As String
            If Not ViewState("UserFilter") Is Nothing Then
                ddlUserFilter.SelectedValue = ViewState("UserFilter")
                filter = "AssignedTo='" + ddlUserFilter.SelectedValue + "'"
                If (filter <> "") Then
                    If EnableNewPriorities Then
                        gvNbhdNew.PageSize = ddlPageSize.SelectedValue
                        gvNbhdNew.PageIndex = 0
                    Else
                        gvNbhd.PageSize = ddlPageSize.SelectedValue
                        gvNbhd.PageIndex = 0
                    End If
                    loadNeighborhoods(filter)
                End If

                Dim filter1 As String = "1 = 1"
                Dim searchField As String = "AND Number IS NOT NULL"
                If (ddlFilterOperator.SelectedValue > -1) Then
                    If (tbSearchText.Visible = True And IsNotEmpty(tbSearchText.Text)) Then
                        Dim operatorValue As Integer = ddlFilterOperator.SelectedValue
                        Dim searchText As String = tbSearchText.Text.ToString()
                        If (tbSearchText.Text.Contains("_") And operatorValue > 1) Then
                            searchText = tbSearchText.Text.Replace("_", "[_]").ToString()
                        End If
                        Select Case ddlOrderBy.SelectedValue
                            Case "Number"
                                If (operatorValue = 1) Then
                                    searchField = filter + " AND Number = '" + searchText + "'"
                                ElseIf (operatorValue = 2) Then
                                    searchField = filter + " AND Number Like '%" + searchText + "%'"
                                ElseIf (operatorValue = 3) Then
                                    searchField = filter + " AND Number like '" + searchText + "%'"
                                ElseIf (operatorValue = 4) Then
                                    searchField = filter + " AND Number like '%" + searchText + "'"
                                End If

                        End Select
                        loadNeighborhoods(searchField)
                    End If
                End If
            End If
        Else
            ReloadList()
        End If
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(control As System.Web.UI.Control)
        If EnableNewPriorities Then
            If control Is pnlExcel Or control Is gvPrintNew Then
                Return
            End If
        Else
            If control Is pnlExcel Or control Is gvPrint Then
                Return
            End If
        End If

        MyBase.VerifyRenderingInServerForm(control)
    End Sub

    Protected Sub lbExport_Click(sender As Object, e As System.EventArgs) Handles lbExport.Click
        pnlExcel.Visible = True
        pnlExcel.Visible = False
        org = Database.System.GetTopRow("SELECT Name FROM Organization WHERE Id = " & HttpContext.Current.GetCAMASession().OrganizationId)
        Dim fileName As String = org.GetString("Name").ToLower().Replace(",", "") + "-assignmentgroup.xlsx"
        Response.Clear()
        Response.Buffer = True
        Response.ClearContent()
        Response.ClearHeaders()
        Response.AddHeader("Content-Disposition", "attachment;filename=" + fileName)
        Response.Charset = ""
        Response.ContentType = "application/vnd.openxml.formats-officedocument.spreadsheetml.sheet"
        Dim excelStream
        If EnableNewPriorities Then
            excelStream = ExcelGenerator.ExportGrid(gvPrintNew, ViewState("gridData"), "Assignment Groups Status Report", "Assignment Groups Status")
        Else
            excelStream = ExcelGenerator.ExportGrid(gvPrint, ViewState("gridData"), "Assignment Groups Status Report", "Assignment Groups Status")
        End If
        excelStream.CopyTo(Response.OutputStream)
        Response.End()
    End Sub


    Private Sub ddlFilterOperator_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFilterOperator.SelectedIndexChanged
        tbSearchText.Text = ""
        If (ddlFilterOperator.SelectedValue > 0) Then
            tbSearchText.Visible = True
            btnSearch.Visible = True
        Else
            tbSearchText.Visible = False
            btnSearch.Visible = False
            ListNeighborhoods()
        End If
    End Sub
    
    Protected Sub btnSearch_Click(sender As Object, e As EventArgs)
        If (tbSearchText.Text = "") Then
            Alert("You have not entered any text to filter in below results.")
        Else
            ListNeighborhoods()
        End If
        ReloadAssignmentGroupsByUser()
    End Sub
    
    Protected Sub btnAscDesc_Click(sender As Object, e As EventArgs)
    	If btnAscDesc.Text = "Ascending" Then
        	btnAscDesc.Text = "Descending"
        Else
        	btnAscDesc.Text = "Ascending"
        End If
    	ListNeighborhoods()
    End Sub
End Class
