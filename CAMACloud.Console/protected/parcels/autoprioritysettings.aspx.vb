﻿Public Class autoprioritysettings
    Inherits System.Web.UI.Page

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Not IsPostBack Then
			RefreshStatus()
		End if
    End Sub
    
    
    Sub RefreshStatus()
    	If Database.Tenant.DoesTableExists("AutoPrioritySettings") Then  
	    	Dim tableExist =  Database.Tenant.GetIntegerValue("Select TOP 1 1 FROM AutoPrioritySettings") 
	    	If tableExist= 1 Then
	    	Else
	    		Database.Tenant.Execute("INSERT INTO AutoPrioritySettings(EnableOnRefresh,EnableOneTime,EnableDateRange,EnableException) VALUES (0,0,0,0)")
	    		Database.Tenant.Execute("UPDATE Parcel SET Mps=1 WHERE priority in(1,2)")
	    	End If
	     End If
    		
       
        If Database.Tenant.GetTopRow("SELECT EnableOnRefresh FROM AutoPrioritySettings WHERE Id = 1")(0) = True  Then
            lblEnableOnRefresh.Text = "ON"
            lnkToggleEnableOnrefresh.Text = "Click here to turn OFF"
            hdnEnableOnRefresh.Value = "1"
        Else
            lblEnableOnRefresh.Text = "OFF"
            lnkToggleEnableOnrefresh.Text = "Click here to turn ON"
            hdnEnableOnRefresh.Value = "0"
        End If
            
        If Database.Tenant.GetTopRow("SELECT EnableOneTime FROM AutoPrioritySettings WHERE Id  = 1")(0) = True  Then
            lblOneTime.Text = "ON"
            lnkOneTime.Text = "Click here to turn OFF"
            hdnOneTime.Value = "1"
        Else
            lblOneTime.Text = "OFF"
            lnkOneTime.Text = "Click here to turn ON"
            hdnOneTime.Value = "0"
        End If
        
         If Database.Tenant.GetTopRow("SELECT EnableDateRange FROM AutoPrioritySettings WHERE Id = 1")(0) = True  Then
            lblDateRange.Text = "ON"
            lnkDateRange.Text = "Click here to turn OFF"
            hdnDateRange.Value = "1"
        Else
            lblDateRange.Text = "OFF"
            lnkDateRange.Text = "Click here to turn ON"
            hdnDateRange.Value = "0"
          End If
            
      	If Database.Tenant.GetTopRow("SELECT EnableException FROM AutoPrioritySettings WHERE Id = 1")(0) = True  Then
            lblException.Text = "ON"
            lnkException.Text = "Click here to turn OFF"
            hdnException.Value = "1"
        Else
            lblException.Text = "OFF"
            lnkException.Text = "Click here to turn ON"
            hdnException.Value = "0"
        End If
 
     End Sub
     
     
     
    Private Sub lnkToggleEnableOnrefresh_Click(sender As Object, e As System.EventArgs) Handles lnkToggleEnableOnrefresh.Click
        If hdnEnableOnRefresh.Value = "1" Then
            Database.Tenant.Execute("UPDATE AutoPrioritySettings SET EnableOnRefresh = 0 WHERE Id  =1")
        Else
            Database.Tenant.Execute("UPDATE AutoPrioritySettings SET EnableOnRefresh = 1 WHERE Id  =1")
        End If
        RefreshStatus()
    End Sub
        
    Private Sub lnkOneTime_Click(sender As Object, e As System.EventArgs) Handles lnkOneTime.Click
        If hdnOneTime.Value = "1" Then
            Database.Tenant.Execute("UPDATE AutoPrioritySettings SET EnableOneTime = 0 WHERE Id  =1")
        Else
            Database.Tenant.Execute("UPDATE AutoPrioritySettings SET EnableOneTime = 1 WHERE Id  =1")
        End If
        RefreshStatus()
    End Sub
    
    
         
    Private Sub lnkDateRange_Click(sender As Object, e As System.EventArgs) Handles lnkDateRange.Click
        If hdnDateRange.Value = "1" Then
            Database.Tenant.Execute("UPDATE AutoPrioritySettings SET EnableDateRange = 0 WHERE Id  =1")
        Else
            Database.Tenant.Execute("UPDATE AutoPrioritySettings SET EnableDateRange = 1 WHERE Id  =1")
        End If
        RefreshStatus()
    End Sub
    
    Private Sub lnkException_Click(sender As Object, e As System.EventArgs) Handles lnkException.Click
        If hdnException.Value = "1" Then
            Database.Tenant.Execute("UPDATE AutoPrioritySettings SET EnableException = 0 WHERE Id  =1")
        Else
            Database.Tenant.Execute("UPDATE AutoPrioritySettings SET EnableException = 1 WHERE Id  =1")
        End If
        RefreshStatus()
    End Sub

End Class