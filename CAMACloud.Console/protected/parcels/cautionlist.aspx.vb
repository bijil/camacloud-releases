﻿Imports System.IO
Imports CAMACloud.BusinessLogic

Partial Class parcels_cautionlist
    Inherits System.Web.UI.Page


    Function KeyLists() As String
        Dim keys As New List(Of String)
        Dim alternatekey = Database.Tenant.Application.AlternateKeyfield
        For i = 1 To 10
            If Database.Tenant.Application.KeyField(i).IsNotEmpty Then
                keys.Add(Database.Tenant.Application.KeyField(i))
            End If
        Next
        If Database.Tenant.Application.ShowKeyValue1 = False Then
            keys.Remove(Database.Tenant.Application.KeyField(1))
        End If
        If Database.Tenant.Application.ShowAlternateField = True Then
            keys.Add(Database.Tenant.Application.AlternateKeyfield)
        End If

        If keys.Count > 0 Then
            pNoCK.Visible = True
        Else
            pNoCK.Visible = False
        End If

        pHaveCK.Visible = keys.Count > 0

        Return String.Join(", ", keys.ToArray.Select(Function(x) "<strong>" + x + "</strong>"))
    End Function
    Protected Sub Uploadbtn_Click(sender As Object, e As System.EventArgs) Handles Uploadbtn.Click
    	If ControlFile.FileBytes.Length = 0 Then
    		Alert("Please select a file to upload!")
            Return
        End If
        If Path.GetExtension(ControlFile.FileName).TrimStart(".").Trim.ToLower <> "csv" Then
            Alert("You have uploaded an invalid file.")
            Return
        End If
        Dim keys As New List(Of String)


        Dim setCount As Integer = 0
        Dim unsetCount As Integer = 0

        Dim dtTemp As New DataTable
        Dim sr As New StreamReader(ControlFile.FileContent)
        Dim joinCondition = ""
        Dim uName As String =Database.System.GetStringValue("SELECT au.UserName FROM  aspnet_Users au INNER JOIN aspnet_Applications a On a.ApplicationId=au.ApplicationId WHERE  a.ApplicationName='" & Membership.ApplicationName & "' And au.UserName='" &  Page.User.Identity.Name & "'")
        If csv_to_datatableConvertion(sr, dtTemp) Then

            If (dtTemp.Columns.Contains("CAUTIONMESSAGE") AndAlso dtTemp.Columns.Contains("CAUTIONFLAG")) Then
                Dim dr As DataRow = Database.Tenant.GetTopRow("EXEC SP_CautionListUpload @UserName = " + uName.ToSqlValue)
                If IsNothing(dr) = False Then
                    setCount = dr.GetInteger("SetCount")
                    unsetCount = dr.GetInteger("UnsetCount")
                End If

            Else
                Alert("CautionFlag or CAUTIONMESSAGE attribute is missing in csv file.")
            End If
        End If

        prioResults.Visible = True
        lblTotal.Text = (setCount + unsetCount).ToString
        lblSets.Text = setCount
        lblUnsets.Text = unsetCount

        Dim confirmationMessage As String = (setCount + unsetCount).ToString + " parcels updated."
        'If nbhdId <> -1 Then
        '    confirmationMessage = (alertCount + priorityCount + revokeCount).ToString + " parcels moved to group " + txtGroupNumber.Text
    End Sub

    Public Function csv_to_datatableConvertion(stream As StreamReader, ByRef dt As DataTable) As Boolean

        Try

            Dim pattern As String = "^\s*""?|""?\s*$"
            Dim rgx As New Regex(pattern)
            Dim Lines As String() = stream.ReadToEnd().Split(Environment.NewLine)
            Dim flg  As Integer
            Dim Fields As String() = Lines(0).Split(New Char() {","c})
            Dim Cols As Integer = Fields.GetLength(0)
            Dim ColumnHeader As New List(Of String)()
            For i = 1 To 10
                If Database.Tenant.Application.KeyField(i).IsNotEmpty Then
                    ColumnHeader.Add(Database.Tenant.Application.KeyField(i).ToUpper())
                End If
            Next
            If Database.Tenant.Application.ShowKeyValue1 = False Then
                ColumnHeader.Remove(Database.Tenant.Application.KeyField(1).ToUpper())
            End If
            If Database.Tenant.Application.ShowAlternateField = True Then
                ColumnHeader.Add(Database.Tenant.Application.AlternateKeyfield)
            End If

            ColumnHeader.Add("CAUTIONFLAG")
            ColumnHeader.Add("CAUTIONMESSAGE")                                                                      
            '1st row must be column names.                                    
            For i As Integer = 0 To Cols - 1
                dt.Columns.Add(rgx.Replace(Fields(i).ToUpper(), ""), GetType(String))
           	Next

			If ColumnHeader.Count = Cols Or ColumnHeader.Count >Cols Then
	            
             For Each value As String In ColumnHeader
                If Not dt.Columns.Contains(value) Then
                    Alert("Your list could not be uploaded because it does not contain '" + value + "' \n as a column .\n\n Please revise your list and upload again.")
                    Return False
                End If
              Next
              For i As Integer = 0 To dt.Columns.Count-1
                    If dt.Columns(i).ToString().ToUpper() <> ColumnHeader(i).ToUpper() Then
                        Alert("Your list could not be uploaded because its fields are not in a correct order.\n\n Please revise your list and upload again.")
                        Return False
                    End If
                Next
            Else
            	Alert("Your list could not be uploaded because it contains extra fields")
            	Return False
            End If
            Dim Row As DataRow
            For i As Integer = 1 To Lines.GetLength(0) - 1
                If Lines(i).Trim.Length = 0 Then
                    Continue For
                End If
	            If ColumnHeader.Count = 3 Then
	            	Fields = Lines(i).Split(New Char() {","c},3)
	            	flg = 1
                End If
                If ColumnHeader.Count = 4 Then
                	Fields = Lines(i).Split(New Char() {","c},4)
                	flg = 2
                End If
                If Fields.Length < Cols Then
                    Continue For
                End If
                Row = dt.NewRow()
                For j As Integer = 0 To Cols - 1
                    If Fields.Length <> Cols Then
                        Exit For
                    End If
                    
                    If flg = 1 And j = 1 Then
                    	If Fields(j).ToString = "0"  Or Fields(j).ToString = "1" Then
                    		Row(j) = rgx.Replace(Fields(j), "")
                    	Else
                    		Alert("Your list could not be uploaded because the flag should be '0' or '1' ")
                    		Return False
                    	End If
                    Else If flg = 2 And j = 2 Then
                    	If Fields(j).ToString = "0"  Or Fields(j).ToString = "1" Then
                    		Row(j) = rgx.Replace(Fields(j), "")
                    	Else
                    		Alert("Your list could not be uploaded because the flag should be '0' or '1' ")
                    		Return False
                    	End If
                    Else
                    	Row(j) = rgx.Replace(Fields(j), "")
                    End If
                   
                Next
                dt.Rows.Add(Row)
            Next
            Database.Tenant.BulkLoadTableFromDataTable("temp_CautionMessage", dt)
            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function
    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            txtcaution.Text = Database.Tenant.GetStringValue("select value from ClientSettings where Name='DefaultCaution'")
        End If

    End Sub

    Protected Sub btncaution_Click(sender As Object, e As EventArgs) Handles btncaution.Click
        'Dim sql As String = "update ClientSettings set Value={0} where name='DefaultCaution'"
        'Database.Tenant.Execute(sql.SqlFormat(False, txtcaution.Text))
        If (txtcaution.Text.Trim() = "") Then
            Alert("Please enter any value for Caution Message!")
            Return
        End If

        Database.Tenant.Execute("IF NOT EXISTS (SELECT * FROM ClientSettings WHERE Name = 'DefaultCaution') INSERT INTO ClientSettings (Name) VALUES ('DefaultCaution');")
		Database.Tenant.Execute("update ClientSettings set Value={0} where name='DefaultCaution'",txtcaution.Text.ToSqlValue)
		Alert("Default text for Caution saved successfully.")
		 prioResults.Visible = False
    End Sub
End Class
