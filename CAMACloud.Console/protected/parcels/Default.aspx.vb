﻿Imports System.IO

Partial Class parcels_Default
    Inherits System.Web.UI.Page

    Public currAssGrp As String = ""
    Sub LoadGrid()
        gvNbhds.Columns(1).HeaderText = "Current " & CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName("shortname")
        Dim query As String = "SELECT ats.*, COALESCE(us.FirstName + COALESCE(' ' + us.LastName, ''), ats.LoginId) As FullName  FROM cc_AppraiserTaskStatus ats LEFT OUTER JOIN UserSettings us ON ats.LoginId = us.LoginId  ORDER BY us.FirstName, ats.NbhdId"
        Dim dt As DataTable = RemoveDuplicateRows(Database.Tenant.GetDataTable(query), "FullName")
        gvNbhds.DataSource = dt
        gvNbhds.DataBind()
        If gvNbhds.FooterRow IsNot Nothing Then
            gvNbhds.FooterRow.Cells(2).Text = "Overall"
            gvNbhds.FooterRow.Cells(3).Text = dt.Compute("Sum(AllNbhds)", "").ToString()
            gvNbhds.FooterRow.Cells(4).Text = dt.Compute("Sum(AllTasks)", "").ToString()
            gvNbhds.FooterRow.Cells(5).Text = dt.Compute("Sum(PendingTasks)", "").ToString()
            gvNbhds.FooterRow.Cells(7).Text = (Math.Round((((dt.Compute("Sum(AllTasks)", "") - dt.Compute("Sum(PendingTasks)", "")) / dt.Compute("Sum(AllTasks)", "")) * 100), 2).ToString() + "%")
        End If
    End Sub

    Sub LoadRecentList()
        Dim dtToday As DataTable = Database.Tenant.GetDataTable("SELECT Number, CONCAT(us.FirstName,' ',us.LastName) AS AssignedTo FROM Neighborhood nd JOIN	UserSettings us ON	us.LoginId = nd.AssignedTo WHERE CAST(dbo.GetLocalDate(LastVisitedDate) AS DATE) = CAST(dbo.GetLocalDate(GETUTCDATE()) AS DATE)")
        Dim dtLast As DataTable = Database.Tenant.GetDataTable("SELECT Number,CONCAT(us.FirstName,' ',us.LastName) AS AssignedTo  FROM Neighborhood nd JOIN	UserSettings us ON	us.LoginId = nd.AssignedTo WHERE CAST(dbo.GetLocalDate(LastVisitedDate) AS DATE) = (SELECT CAST(dbo.GetLocalDate(MAX(LastVisitedDate)) As DATE) FROM Neighborhood WHERE CAST(dbo.GetLocalDate(LastVisitedDate) AS DATE) < CAST(dbo.GetLocalDate(GETUTCDATE()) AS DATE))")
        rpToday.DataSource = dtToday
        rpToday.DataBind()
        rpLast.DataSource = dtLast
        rpLast.DataBind()

        If rpToday.Items.Count > 0 Then
            lblNoneToday.Visible = False
        Else
            lblNoneToday.Visible = True
        End If

        If rpLast.Items.Count > 0 Then
            lblNoneLast.Visible = False
        Else
            lblNoneLast.Visible = True
        End If
    End Sub

    Sub LoadProductivity()
        Dim dtP As DataTable = d_("Exec Get_ProductivityStats")
        gdP.DataSource = dtP 
        gdP.DataBind()
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim organizationId As Integer = HttpContext.Current.GetCAMASession.OrganizationId
            If Not ClientOrganization.HasModule(organizationId, "MobileAssessor") Then
                If ClientOrganization.HasModule(organizationId, "DTR") Then
                    Response.Redirect("~/protected/dtr/tasks")
                End If
            End If

            LoadGrid()
            LoadRecentList()
            LoadProductivity()
         
        End If
        'Dim tooltipon As Boolean = Session("tooltipon")
        'If tooltipon Then
        '    image1.ImageUrl = "~/App_Static/images/images.jpg"
        'Else
        '    image1.ImageUrl = "~/App_Static/images/ibtn.png"
        'End If
    End Sub

    Protected Sub btnRefresh_Click(sender As Object, e As System.EventArgs) Handles btnRefresh.Click
        LoadGrid()
    End Sub

    
    Protected Sub gvNbhds_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvNbhds.RowDataBound
        Dim isMultipleNbhd As String = Database.Tenant.GetStringValue("SELECT Value FROM ClientSettings WHERE Name = 'EnableMultipleNbhd'")
        If isMultipleNbhd = "1" Then
            gvNbhds.Columns(1).Visible = False
            gvNbhds.Columns(2).Visible = True
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim ddl = DirectCast(e.Row.FindControl("ddlCurrentAssgnGrp"), DropDownList)
                Dim assignedTo As String = DirectCast(Me.gvNbhds.DataKeys(e.Row.RowIndex).Value, String)
                'Dim dt As DataTable = Database.Tenant.GetDataTable("SELECT CONCAT(nts.PercentDone,'||',n.AssignedTo) as Percentage, n.Number FROM Neighborhood n INNER JOIN cc_NbhdTaskStatus nts ON n.Id = nts.Id WHERE n.AssignedTo = '" & assignedTo & "' AND n.LastVisitedDate = (SELECT MAX(LastVisitedDate) FROM Neighborhood WHERE AssignedTo = '" & assignedTo & "')")
                Dim dt As DataTable = Database.Tenant.GetDataTable("SELECT CONCAT(nts.PercentDone,'||',n.AssignedTo) as Percentage, n.Number FROM Neighborhood n INNER JOIN cc_NbhdTaskStatus nts ON n.Id = nts.Id WHERE n.AssignedTo = '" & assignedTo & "'")
                ddl.DataSource = dt
                ddl.DataTextField = "Number"
                ddl.DataValueField = "Percentage"
                ddl.DataBind()
            End If
        Else
            gvNbhds.Columns(1).Visible = True
            gvNbhds.Columns(2).Visible = False
        End If
        
    End Sub

    Public Function RemoveDuplicateRows(dTable As DataTable, colName As String) As DataTable
        Dim hTable As New Hashtable()
        Dim duplicateList As New ArrayList()

        'Add list of all the unique item value to hashtable, which stores combination of key, value pair.
        'And add duplicate item value in arraylist.
        For Each drow__1 As DataRow In dTable.Rows
            If hTable.Contains(drow__1(colName)) Then
                duplicateList.Add(drow__1)
            Else
                hTable.Add(drow__1(colName), String.Empty)
            End If
        Next

        'Removing a list of duplicate items from datatable.
        For Each dRow__2 As DataRow In duplicateList
            dTable.Rows.Remove(dRow__2)
        Next

        'Datatable which contains unique records will be return as output.
        Return dTable
    End Function

    Protected Sub gvNbhds_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles gvNbhds.RowCreated

    End Sub

    Protected Sub gvNbhds_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvNbhds.PageIndexChanging
        gvNbhds.PageIndex = e.NewPageIndex
        LoadGrid()
    End Sub

    Protected Sub gvNbhds_SelectedIndexChanged(sender As Object, e As EventArgs) Handles gvNbhds.SelectedIndexChanged

    End Sub
End Class
