﻿Public Class autoprioritylog
	Inherits System.Web.UI.Page
	
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		Dim scriptManager As ScriptManager = scriptManager.GetCurrent(Me.Page)
        scriptManager.RegisterPostBackControl(Me.gvRuleList)
        If Not IsPostBack Then
			LoadData()
			
		End If
	End Sub
	Dim  dt As DataTable
	Dim  dtRules As DataTable
	Sub LoadData()
		Dim rows As String
		Dim RowSelect As String
		
		If dtRules Is Nothing Then
			dtRules = Database.Tenant.GetDataTable("SELECT Id AS RuleId,Title,CASE WHEN Priority=1 THEN 'High' WHEN Priority=2 THEN 'Urgent' ELSE 'Normal' END AS priority,dbo.GetLocalDate(CreatedDate) AS CreatedDate,IsActive FROM AutopriorityRuleHdr ORDER BY CreatedDate DESC")
		End If
		Me.ViewState.Add("RulesHdr", dtRules)
		gvRuleList.DataSource=dtRules
		gvRuleList.DataBind()
		If ((dtRules IsNot Nothing) And (dtRules.Rows.Count >0 )) Then
			ddlRules.FillFromTable(dtRules, True, "---  Select Rule  ---")
		End If
		If  ddlRows.SelectedValue <> "-2" Then
			rows =  ddlRows.SelectedItem.Text
		End If
		
		If rows <> "" And rows <> "ALL" Then
			RowSelect="TOP "+rows+" "
		Else
			RowSelect=""
		End If
        If dt Is Nothing Then
            Dim sql As String
            Dim keyField = Database.Tenant.Application.KeyField(1)
            Dim alternatefield = Database.Tenant.Application.AlternateKeyfield
            If alternatefield IsNot Nothing Then
                sql = "SELECT " + RowSelect + " P.Id AS ParcelID,pd." + alternatefield + " AS KeyField,dbo.GetLocalDate(AP.EventTime) AS EventTime,CASE	WHEN OldPriority = 1 THEN 'High' WHEN OldPriority = 2 THEN 'Urgent' ELSE 'Normal'	END AS OldPriority  ,AP.[Description] FROM AutoPriorityAuditTrail AP JOIN Parcel P ON AP.ParcelId=P.Id INNER JOIN ParcelData pd ON pd.CC_ParcelId = P.Id ORDER BY AP.EventTime DESC"
            Else
                sql = "SELECT " + RowSelect + " P.Id AS ParcelID,P.KeyValue1 AS KeyField,dbo.GetLocalDate(AP.EventTime) AS EventTime,CASE	WHEN OldPriority = 1 THEN 'High' WHEN OldPriority = 2 THEN 'Urgent' ELSE 'Normal'	END AS OldPriority  ,AP.[Description] FROM AutoPriorityAuditTrail AP JOIN Parcel P ON AP.ParcelId=P.Id ORDER BY AP.EventTime DESC"
            End If
            'dt = Database.Tenant.GetDataTable("SELECT " + RowSelect + " P.Id AS ParcelID,P.KeyValue1 AS Keyfield1,dbo.GetLocalDate(AP.EventTime) AS EventTime,CASE	WHEN OldPriority = 1 THEN 'High' WHEN OldPriority = 2 THEN 'Urgent' ELSE 'Normal'	END AS OldPriority  ,AP.[Description] FROM AutoPriorityAuditTrail AP JOIN Parcel P ON AP.ParcelId=P.Id ORDER BY AP.EventTime DESC")
            dt = Database.Tenant.GetDataTable(sql)
        End If
        Me.ViewState.Add("AuditTrail", dt)
		gvAuditTrail.DataSource = dt
		gvAuditTrail.DataBind()
		
	End Sub
	
	Public Sub gvRuleList_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvRuleList.RowCommand
		Dim dtRules As DataTable
        Dim ruleId As Integer
        Select Case e.CommandName.ToString
			Case "Delete1"
				ruleId=e.CommandArgument 
				Database.Tenant.Execute("DELETE FROM  AutopriorityRuleDtl WHERE Id={0}".SqlFormatString(ruleId))
				Database.Tenant.Execute("DELETE FROM  AutopriorityRuleHdr WHERE Id={0}".SqlFormatString(ruleId))
				dtRules = Database.Tenant.GetDataTable("SELECT Id AS RuleId,Title,CASE WHEN Priority=1 THEN 'High' WHEN Priority=2 THEN 'Urgent' ELSE 'Normal' END AS priority,CreatedDate,IsActive FROM AutopriorityRuleHdr ORDER BY CreatedDate DESC")
				dtRules.AcceptChanges
				gvRuleList.DataSource=dtRules
                gvRuleList.DataBind()
                LoadData()
            Case "Enable"
				ruleId=e.CommandArgument 
				Database.Tenant.Execute("UPDATE AutopriorityRuleHdr SET  IsActive=(select ~IsActive from AutopriorityRuleHdr where Id={0}) where Id={0} ".SqlFormatString(e.CommandArgument.ToString))
				dtRules = Database.Tenant.GetDataTable("SELECT Id AS RuleId,Title,CASE WHEN Priority=1 THEN 'High' WHEN Priority=2 THEN 'Urgent' ELSE 'Normal' END AS priority,CreatedDate,IsActive FROM AutopriorityRuleHdr ORDER BY CreatedDate DESC")
				dtRules.AcceptChanges
				gvRuleList.DataSource=dtRules
				gvRuleList.DataBind()
				
		End Select
	End Sub
	
	Sub gvAuditTrail_PageIndexChanging(ByVal sender As Object, ByVal e As GridViewPageEventArgs)
		If (Me.ViewState("AuditTrail") IsNot Nothing) Then
			Dim dt As New DataTable 
			dt= Me.ViewState("AuditTrail")
			gvAuditTrail.PageIndex = e.NewPageIndex
			gvAuditTrail.DataSource = dt
			gvAuditTrail.DataBind()
		End if
	End Sub
	
	Sub gvRuleList_PageIndexChanging(ByVal sender As Object, ByVal e As GridViewPageEventArgs)
		If (Me.ViewState("RulesHdr") IsNot Nothing) Then
			Dim dt As New DataTable 
			dt= Me.ViewState("RulesHdr")
			gvRuleList.PageIndex = e.NewPageIndex
			gvRuleList.DataSource = dt
			gvRuleList.DataBind()
		End if
	End Sub
	
	Private Sub ddlfilter_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlRules.SelectedIndexChanged, ddlRows.SelectedIndexChanged
		Dim ruleId As Integer
		Dim rows As String = ""
		Dim RowSelect As String
		If ddlRules.SelectedIndex <>0  And ddlRules.SelectedValue <> "-2" And ddlRules.SelectedValue<>"" Then
			ruleId = ddlRules.SelectedValue
			
			
			If ddlRows.SelectedValue <> "-2" Then
				rows =  ddlRows.SelectedItem.Text
			End If
			
			If rows <> "" And rows <> "ALL" Then
				RowSelect="TOP "+rows+" "
			Else
				RowSelect=""
			End If
            Dim dt As New DataTable
            Dim alternatefield = Database.Tenant.Application.AlternateKeyfield
            Dim keyfield = Database.Tenant.Application.KeyField(1)
            Dim selectField As String
            Dim joiningCondition As String
            If alternatefield IsNot Nothing Then
                selectField = "pd." + alternatefield + " AS KeyField"
                joiningCondition = "INNER JOIN Parceldata pd ON pd.CC_ParcelId = P.Id"
            Else
                selectField = "P.KeyValue1 AS KeyField"
                joiningCondition = ""
            End If
            If ruleId=0 Then
                dt = Database.Tenant.GetDataTable("SELECT  " + RowSelect + " P.Id AS ParcelID," + selectField + ",dbo.GetLocalDate(AP.EventTime)  AS EventTime,CASE	WHEN OldPriority = 1 THEN 'High' WHEN OldPriority = 2 THEN 'Urgent' ELSE 'Normal'	END AS OldPriority  ,AP.[Description] FROM AutoPriorityAuditTrail AP JOIN Parcel P ON AP.ParcelId=P.Id " + joiningCondition + " ORDER BY AP.EventTime DESC")
            Else
                dt = Database.Tenant.GetDataTable("SELECT  " + RowSelect + " P.Id AS ParcelID," + selectField + ",dbo.GetLocalDate(AP.EventTime)  AS EventTime,CASE	WHEN OldPriority = 1 THEN 'High' WHEN OldPriority = 2 THEN 'Urgent' ELSE 'Normal'	END AS OldPriority  ,AP.[Description] FROM AutoPriorityAuditTrail AP JOIN Parcel P ON AP.ParcelId=P.Id " + joiningCondition + " WHERE AP.APSId='" + ruleId.ToString + "'  ORDER BY AP.EventTime DESC")
            End If
			Me.ViewState.Add("AuditTrail", dt)
			gvAuditTrail.DataSource = dt
			gvAuditTrail.DataBind()
		End If
	End Sub

    Protected Sub gvAuditTrail_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvAuditTrail.RowDataBound

        If e.Row.RowType = DataControlRowType.Header Then
            If Database.Tenant.Application.AlternateKeyfield IsNot Nothing Then
                e.Row.Cells(2).Text = Database.Tenant.Application.AlternateKeyfield.ToString()
            Else
                e.Row.Cells(2).Text = Database.Tenant.Application.KeyField(1).ToString()
            End If
        End If
    End Sub
End Class