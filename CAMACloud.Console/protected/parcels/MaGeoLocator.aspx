﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/ParcelManager.master" CodeBehind="MaGeoLocator.aspx.vb" Inherits="CAMACloud.Console.MaGeoLocator" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyDG0X-Smgx8shc9-JXC23rTmiw--cAhi9Y"></script>
    <style>
        .userstatus {
            margin:5px 5px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   
    <div id="Map" style="min-height:450px; width: 100%;"></div>
    
    <div style="width:200px">
        <div class="userstatus" style="background-color:green; width:10px; height:10px;" ></div>
        <div class="userstatus" style="background-color:yellow; width:10px; height:10px;" ></div>
        <div class="userstatus" style="background-color:orange; width:10px; height:10px;" ></div>
        <div class="userstatus" style="background-color:red; width:10px; height:10px;" ></div>
        <div class="userstatus" style="background-color:gray; width:10px; height:10px;" ></div>
    </div>
    <div>
        <select>
            <option value="All">All</option>
            <option value="Online">Online</option>
            <option value="Recent">Recent</option>
            <option value="Dead">Dead</option>
        </select>
        <button value="Refresh" onclick="return test()">Refresh</button>
    </div>
    <script type="text/javascript">
        window.onload = function () {
           test();
        };
        function test() {
            $.ajax({
                type: "POST",
                contentType: "application/json",
                url: "/parcels/appraiserlocator.jrq",
                datatype: "json",
                success: function (locations) {
                        window.map = new google.maps.Map(document.getElementById("Map"), {
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        });
                        var infowindow = new google.maps.InfoWindow();
                        var bounds = new google.maps.LatLngBounds();
                        var icon = ["/App_Themes/Cloud/images/googlemap_online.png",
                            "/App_Themes/Cloud/images/0-1Hour.png",
                            "/App_Themes/Cloud/images/googlemap_recent.png",
                            "/App_Themes/Cloud/images/googlemap_dead.png",
                            "/App_Themes/Cloud/images/oneDay.png"]
                        var animation = [google.maps.Animation.BOUNCE, google.maps.Animation.DROP]
                        for (i = 0; i < locations.length; i++) {
                            var dataCount = locations.length;
                            var toDay = currentDate().split(" ");
                            var lastDay = locations[i].LastUpdated.split(" ");
                            var dateDifference = DateDiff(lastDay[0], toDay[0]);
                            var status = (dateDifference == 0) ? 0 : 4;
                            if (status == 0) {
                                var time = diff_minutes(locations[i].LastUpdated, currentDate());
                                var hour = time.split(":");
                                if (hour[0] >= 4) {
                                    status = 3;
                                }
                                else if (hour[0] < 4 && hour[0] >1 ) {
                                    status = 2;
                                }
                                else if (hour[0] <= 1) {
                                    if (hour[0] == 1 || hour[1] > 2) {
                                        status = 1;
                                    }
                                    else if(hour[1] <= 1){
                                        status = 0;
                                    }
                                }
                            }
                            marker = new google.maps.Marker({
                                animation: animation[status],
                                position: new google.maps.LatLng(locations[i].Latitude, locations[i].Longitude),
                                map: map,
                                icon: icon[status]
                            });
                            bounds.extend(marker.position);
                            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                                return function () {
                                    infowindow.setContent(locations[i]['LoginId']);
                                    infowindow.open(map, marker);
                                }
                            })(marker, i));
                        }
                        map.fitBounds(bounds);
                },
                error: function (response) {
                    alert(response.status);
                }
            });

          
            return false
        }
        function currentDate() {
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd
            }

            if (mm < 10) {
                mm = '0' + mm
            } ''
            return today = yyyy + '-' + mm + '-' + dd + " " + today.getHours() + ":" + today.getMinutes();
        }
        function DateDiff(date1, date2) {
            dt1 = new Date(date1);
            dt2 = new Date(date2);
            return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));
        }
        function diff_minutes(dt2, dt1) {

            dt2 = new Date(dt2);
            dt1 = new Date(dt1);

            var diff = dt1 - dt2;

            var diffSeconds = diff / 1000;
            var HH = Math.floor(diffSeconds / 3600);
            var MM = Math.floor(diffSeconds % 3600) / 60;

            var formatted = HH + ":" + MM;
            return formatted;

        }
    </script>
</asp:Content>
