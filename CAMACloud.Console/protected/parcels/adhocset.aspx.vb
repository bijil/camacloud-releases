﻿Imports System.IO

Public Class adhocset
    Inherits System.Web.UI.Page

    Sub RefreshPage()
        Dim useOldRelink = Database.Tenant.GetIntegerValueOrInvalid("SELECT Value FROM ClientSettings WHERE Name = 'UseOldRelinkMethod'")
        If useOldRelink = 1 Then
            btnSwitch.Visible = False
        End If
        lblAdHocs.Text = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM Neighborhood WHERE IsAdhoc = 1") & " / " & Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM Neighborhood")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            RefreshPage()
        End If
    End Sub

    Private Sub btnSwitch_Click(sender As Object, e As EventArgs) Handles btnSwitch.Click
        Database.Tenant.Execute("DELETE FROM ClientSettings WHERE Name = 'UseOldRelinkMethod'")
        RefreshPage()
    End Sub

    Private Sub btnUploadList_Click(sender As Object, e As EventArgs) Handles btnUploadList.Click
        If ControlFile.FileBytes.Length = 0 Then
            Return
        End If
        If Path.GetExtension(ControlFile.FileName).TrimStart(".").Trim.ToLower <> "txt" Then
            Alert("You have uploaded an invalid file.")
            Return
        End If

        Dim dtTemp As New DataTable
        Dim sr As New StreamReader(ControlFile.FileContent)

        Dim Lines As String() = sr.ReadToEnd().Split(Environment.NewLine)

        Dim filterString As String = ""
        For Each gname In Lines
            gname = gname.Trim
            filterString += gname.ToSqlValue + ", "
        Next
        filterString += "NULL"

        Dim sql As String = "UPDATE Neighborhood SET IsAdhoc = 1 WHERE Number IN (" + filterString + ")"

        Dim recordsAffected As Integer = Database.Tenant.Execute(sql)

        RefreshPage()
        Alert(recordsAffected & " groups flagged as Ad Hoc.")
    End Sub
End Class