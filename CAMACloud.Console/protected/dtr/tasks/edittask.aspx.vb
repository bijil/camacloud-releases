﻿Imports System.Text.RegularExpressions
Imports System.IO
Public Class edittask
	Inherits System.Web.UI.Page
    Public ReadOnly Property RequestTaskID As Integer
        Get
            If Request("taskid") = "" Then
                Return -1
            End If
            Return CInt(Request("taskid"))
        End Get
    End Property

    Public ReadOnly Property RequestTID As Integer
        Get
            If Request("tid") = "" Then
                Return -1
            End If
            Return CInt(Request("tid"))
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If RequestTaskID <> -1 Then
                Dim compiledSql As String
                Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM DTR_Tasks WHERE ID = " & RequestTaskID)
                txtName.Text = dr.GetString("Name")
                compiledSql = dr.GetString("CompiledSQLFilter")
                txtFilterData.Text = dr.GetString("FilterData")
                Dim sfQuery As String = ""
                Dim tableClause As String = "FROM Parcel p INNER JOIN ParcelData pdata ON pdata.CC_ParcelId = p.Id"
                If txtFilterData.Text.Contains("$ParcelWorkflow") Then
                    tableClause = tableClause + " LEFT OUTER JOIN ParcelWorkFlowStatus pwf ON p.Id = pwf.ParcelId"
                End If
                If txtFilterData.Text.contains("$SketchFlag") Then
                    sfQuery = "SELECT * INTO #temp_sketchSRFlags FROM (SELECT psf.ParcelId As PId, CAST(psf.FlagId As varchar ) As FlagId, srf.Name As Name FROM ParcelSketchFlag psf JOIN SketchReviewFlags srf on psf.FlagId = srf.Id UNION SELECT NULL As PId, CONCAT('S_', Id) As FlagId, Name As Name FROM SketchStatusFlags) SSRF; "
                    tableClause = tableClause + " LEFT OUTER JOIN #temp_sketchSRFlags ssf ON (p.Id = ssf.PId OR CONCAT('S_', ISNULL(p.SketchFlag, '')) = ssf.FlagId)"
                End If
                Dim testSql As String = sfQuery + "SELECT COUNT(DISTINCT(p.Id)) " + tableClause + " WHERE " + compiledSql
                Dim parcelCount As Integer = Database.Tenant.GetIntegerValue(testSql)
                lblcount.Text = "Parcels: " + parcelCount.ToString()
            ElseIf RequestTID <> -1 Then
                Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM DTR_Tasks WHERE ID = " & RequestTID)
                Dim CC = Request("cC")
                If (CC = "dt") Then
                    txtName.Text = dr.GetString("Name")
                    txtFilterData.Text = dr.GetString("FilterData")
                ElseIf (CC = "d") Then
                    txtFilterData.Text = dr.GetString("FilterData")
                ElseIf (CC = "t") Then
                    txtName.Text = dr.GetString("Name")
                End If
            End If
        End If
        If ClientOrganization.HasModule(HttpContext.Current.GetCAMASession().OrganizationId, "SketchValidation") Then
            labelSketchFlag.Visible = True
        End If
        If ClientSettings.PropertyValue("DTRWorkflowNew") = "1" Then
            labelWorkFlow.Visible = True
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("~/protected/dtr/tasks/default.aspx?pI=" + Request.QueryString("pI") + "&pS=" + Request.QueryString("pS") + "&pO=" + Request.QueryString("pO"))
    End Sub

    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Dim taskId As Integer = RequestTaskID

        If (RequestTID <> -1) And hdnTaskID.Value = "" Then
            Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM DTR_Tasks WHERE Name = '" + txtName.Text + "'")
            If dr IsNot Nothing Then
                Alert("Task Name is already exists!")
                Return
            End If
        End If

        If hdnTaskID.Value <> "" Then
            taskId = hdnTaskID.Value
        End If

        Dim filterData As String = txtFilterData.Text
        Dim compiledSql As String = filterData
        Dim sfQuery As String = ""

        Dim parcelFields As String() = Database.Tenant.GetDataTable("Select Name FROM DataSourceField WHERE SourceTable = {0}".SqlFormat(False, Database.Tenant.Application.ParcelTable)).Select.Select(Function(x) x.GetString("Name")).ToArray
        For Each fieldName As String In parcelFields
            compiledSql = Replace(compiledSql, fieldName, "pdata." + fieldName, , , CompareMethod.Binary)
        Next

        Dim parcelFlagFields As String() = {"DTRStatus", "Reviewed", "ReviewDate", "ReviewedBy", "StreetAddress", "QCBy", "QCDate", "QC", "Priority", "ParcelWorkflow", "CautionAlert", "CautionMessage", "CC_Tag", "ParcelAlert", "FieldAlert", "FieldAlertType", "SketchReviewedDate", "SketchReviewedBy", "SketchReviewNote"}
        For Each fieldName As String In parcelFlagFields
            If fieldName = "ParcelWorkflow" Then
                compiledSql = Replace(compiledSql, "$" + fieldName, "pwf.StatusCode", , , CompareMethod.Text)
            ElseIf fieldName = "CautionAlert" Then
                compiledSql = Replace(compiledSql, "$" + fieldName, "p.CautionFlag", , , CompareMethod.Text)
            Else
                compiledSql = Replace(compiledSql, "$" + fieldName, "p." + fieldName, , , CompareMethod.Text)
            End If
        Next

        For Each fieldName As String In {"SketchFlag"}
            If fieldName = "SketchFlag" Then
                compiledSql = Replace(compiledSql, "$" + fieldName, "ssf.Name", , , CompareMethod.Text)
            End If
        Next

        Dim SATValue As String = ""
        Dim SqlQuery As String = ""
        For Each fieldName As String In {"SelectedAppraisalType"}
            If fieldName = "SelectedAppraisalType" Then
                Dim startIndex As Integer = compiledSql.IndexOf(fieldName & " =")
                If startIndex >= 0 Then
                    SATValue = compiledSql.Substring(startIndex + fieldName.Length + 2).Trim()
                End If

            End If

        Next

        'Dim txtNameValue As String = txtName.Text.Trim().ToLower()
        'Dim sql1 As String = "SELECT Name FROM DTR_Tasks WHERE LOWER(Name) = @Name"

        'Dim txtFilterDataValue As String = txtFilterData.Text.Trim()
        'Dim sql2 As String = "SELECT FilterData FROM DTR_Tasks WHERE FilterData = @FilterData"
        'Dim sanitizedSql2 As String = Regex.Replace(sql2, "\s*(=|>|<|>=|<=)\s*", "$1")
        'Dim sanitizedTxtFilterData As String = Regex.Replace(txtFilterDataValue, "\s*(=|>|<|>=|<=)\s*", "$1")

        'Dim dtrname As String = Database.Tenant.GetStringValue(sql1.Replace("@Name", $"'{txtNameValue}'")).ToLower()
        'Dim dtrfilter As String = Database.Tenant.GetStringValue(sanitizedSql2.Replace("@FilterData", $"'{sanitizedTxtFilterData}'"))

        'If dtrname = txtNameValue And dtrfilter = sanitizedTxtFilterData Then
        '    Alert("Task Name already exists. Press OK to continue.")
        'Else
        Try

            If compiledSql.Trim = "" Then
                Throw New Exception("Blank filter expression is not allowed.")
            End If

            Dim tableClause As String = "FROM Parcel p INNER JOIN ParcelData pdata ON pdata.CC_ParcelId = p.Id"

            If filterData.Contains("$ParcelWorkflow") Then
                tableClause = tableClause + " LEFT OUTER JOIN ParcelWorkFlowStatus pwf ON p.Id = pwf.ParcelId"
            End If

            If filterData.Contains("$SketchFlag") Then
                sfQuery = "SELECT * INTO #temp_sketchSRFlags FROM (SELECT psf.ParcelId As PId, CAST(psf.FlagId As varchar ) As FlagId, srf.Name As Name FROM ParcelSketchFlag psf JOIN SketchReviewFlags srf on psf.FlagId = srf.Id UNION SELECT NULL As PId, CONCAT('S_', Id) As FlagId, Name As Name FROM SketchStatusFlags) SSRF; "
                tableClause = tableClause + " LEFT OUTER JOIN #temp_sketchSRFlags ssf ON (p.Id = ssf.PId OR CONCAT('S_', ISNULL(p.SketchFlag, '')) = ssf.FlagId)"
            End If

            If filterData.Contains("SelectedAppraisalType") Then
                SqlQuery = Database.Tenant.GetIntegerValue("select Id from AppraisalType where Name =" + SATValue)
                compiledSql = "SelectedAppraisalType =" + SqlQuery

            End If

            Dim testSql As String = sfQuery + "SELECT COUNT(DISTINCT(p.Id)) " + tableClause + " WHERE " + compiledSql
            Dim parcelCount As Integer = Database.Tenant.GetIntegerValue(testSql)

            Dim completedParcelsSql As String = sfQuery + "SELECT COUNT(DISTINCT(p.Id)) " + tableClause + " WHERE p.Reviewed = 1 AND (" + compiledSql + ")"
            Dim completedCount As Integer = Database.Tenant.GetIntegerValue(completedParcelsSql)

            If taskId = -1 Then
                taskId = Database.Tenant.GetIntegerValue("INSERT INTO DTR_Tasks (CreatedBy) VALUES ({0}); SELECT @@IDENTITY".SqlFormat(False, User.Identity.Name))
            End If

            'Dim txtFilterDataValue1 As String = txtFilterData.Text
            'Dim dtrfilter1 As String = Regex.Replace(txtFilterDataValue, "\s*(=|>|<|>=|<=)\s*", "$1") 'txtFilterDataValue.Replace(" =", "=").Replace("= ", "=").Replace(" = ", "=") 

            Dim sqlUpdate As String = "UPDATE DTR_Tasks SET Name = {1}, FilterData = {2}, CompiledSQLFilter = {3}, TotalParcels = {4}, CompletedParcels = {5} , IsUpdated={6} WHERE ID = {0}".SqlFormat(False, taskId, txtName.Text, txtFilterData.Text, compiledSql, parcelCount, completedCount, 0)
            Database.Tenant.Execute(sqlUpdate)
            Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM DTR_Tasks WHERE ID = " & taskId)
            compiledSql = dr.GetString("CompiledSQLFilter")
            Dim Sql As String = "SELECT COUNT(*) " + tableClause + " WHERE " + compiledSql
            Dim pCount As Integer = Database.Tenant.GetIntegerValue(testSql)
            lblcount.Text = "Parcels: " + pCount.ToString()
            Alert("Task/Filter updated successfully.\n" + pCount.ToString() + " properties affected")

            hdnTaskID.Value = taskId

        Catch ex As Exception
            'Alert("Syntax error in FilterData - " + ex.Message)
            Alert("Invaild Filter Description.")
        End Try
        'End If


    End Sub
End Class
