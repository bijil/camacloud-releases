﻿Public Class assign
    Inherits System.Web.UI.Page

    Private Const PageSize As Integer = 10
    Private Property PageIndex As Integer
    Sub LoadUserList()
        Dim uc = Membership.GetAllUsers
        Dim users(uc.Count - 1) As MembershipUser
        Membership.GetAllUsers().CopyTo(users, 0)
        Dim fielduser = users.Where(Function(x) Roles.IsUserInRole(x.UserName, "DTR-ReadOnly") Or Roles.IsUserInRole(x.UserName, "DTR-Editor") Or Roles.IsUserInRole(x.UserName, "DTR-Manager"))
        Dim dataUsers As New DataTable()
        dataUsers.Columns.Add("UserName")
        For Each u As MembershipUser In fielduser
            Dim dr As DataRow = dataUsers.NewRow
            dr("UserName") = u.UserName
            dataUsers.Rows.Add(dr)
        Next

        Dim userFromSettings As DataTable = Database.Tenant.GetDataTable("SELECT FirstName,LastName,LoginId FROM UserSettings")
        Dim query = From A In userFromSettings.AsEnumerable
                    Join B In dataUsers.AsEnumerable On
              A.Field(Of String)("LoginId") Equals B.Field(Of String)("UserName")
                    Order By If((A.Field(Of String)("FirstName") + " " + A.Field(Of String)("LastName")), B.Field(Of String)("UserName"))
                    Select New With {
              .LoginId = B.Field(Of String)("UserName"),
              .UserName = If((A.Field(Of String)("FirstName") + " " + A.Field(Of String)("LastName")), B.Field(Of String)("UserName"))
        }

        Dim pagedQuery = query.Skip(pageIndex * PageSize).Take(PageSize)
        gvUsers.DataSource = pagedQuery.ToList()
        gvUsers.DataBind()

        Dim totalRecords = query.Count()
        Dim totalPages = Math.Ceiling(totalRecords / PageSize)


    End Sub



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
      If Roles.IsUserInRole("DTR-Manager") Then
    	If Not IsPostBack Then
            'LoadUsers()
            LoadUserList()
    	End If
      Else
      	Alert("You must have the DTR Manager role to access this screen.")
      	gvUsers.Visible=False
      	Page.RunScript("hidedtrAssign();")
      	Return
      End If
    End Sub

    Protected Sub gvUsers_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvUsers.RowCommand
        If e.CommandName = "Assign" Then
            Dim userName As String = e.CommandArgument
            hdnUserName.Value = userName
            hdnFullUserName.Value = Database.Tenant.GetStringValue("select FirstName +' '+  ISNULL( LastName,'')  FROM UserSettings WHERE LoginId={0}".SqlFormatString(userName))
            lblName.Text = If(hdnFullUserName.Value, userName)
            LoadTasks()
            LoadUserTasks()

            pnlAssign.Visible = True
            pnlUsers.Visible = False
        End If
    End Sub


    Sub LoadTasks()
        ddlTask.FillFromSql("SELECT Id, Name FROM DTR_Tasks ORDER BY Name", True, "--")
        btnAssign.Enabled = False
        nbhdDetails.Visible = False
    End Sub

    Sub LoadUserTasks()
        gvNbhds.DataSource = Database.Tenant.GetDataTable("SELECT * FROM DTR_Tasks WHERE AssignedTo = '" + hdnUserName.Value + "' ORDER BY Name")
        gvNbhds.DataBind()

        If gvNbhds.Rows.Count > 1 Then
            lbRemoveAll.Visible = True
        Else
            lbRemoveAll.Visible = False
        End If
    End Sub


    Protected Sub ddlNbhd2_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlTask.SelectedIndexChanged
        If ddlTask.SelectedValue = "" Then
            btnAssign.Enabled = False
            nbhdDetails.Visible = False
        Else

            btnAssign.Enabled = True
            nbhdDetails.Visible = True
            btnAssign.OnClientClick = ""

            Dim nbhd As String = ddlTask.SelectedItem.Text
            lblNbhdName.Text = nbhd

            Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT n.*,COALESCE(us.FirstName + COALESCE(' ' + us.LastName, ''),  n.assignedto) As UserAssigned FROM DTR_Tasks n LEFT OUTER JOIN UserSettings us ON us.LoginId=n.assignedto WHERE n.Id = " + ddlTask.SelectedValue)
            Dim assignedTo As String = dr.GetString("AssignedTo")
            lblAssignedTo.Text = dr.GetString("UserAssigned")
            If assignedTo = "" Then
                lblAssignedTo.Text = "None"
            ElseIf assignedTo = hdnUserName.Value Then
                lblAssignedTo.Text = "You"
                btnAssign.Enabled = True
            Else
                btnAssign.OnClientClick = "return confirm('The selected task is assigned to another user. Are you sure you want to reassign it to the selected user?')"
            End If

            hdnTaskId.Value = dr.GetInteger("Id")
        End If
    End Sub

    Protected Sub btnAssign_Click(sender As Object, e As System.EventArgs) Handles btnAssign.Click
        Database.Tenant.Execute("UPDATE DTR_Tasks SET AssignedTo =  " + hdnUserName.Value.ToSqlValue + " WHERE Id = " + hdnTaskId.Value)
        Try
            NotificationMailer.SendNotification(hdnUserName.Value, "DTR Task assigned", "DTR Task '" + ddlTask.SelectedItem.Text + "' has been assigned to you.")
        Catch ex As Exception
            Alert(ex.Message)
        End Try
        LoadTasks()
        LoadUserTasks()


    End Sub

    Protected Sub lbBack_Click(sender As Object, e As System.EventArgs) Handles lbBack.Click
        pnlAssign.Visible = False
        pnlUsers.Visible = True
    End Sub

    Protected Sub gvNbhds_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvNbhds.RowCommand
        If e.CommandName = "Unassign" Then
            Database.Tenant.Execute("UPDATE DTR_Tasks SET AssignedTo = NULL WHERE Id = " + e.CommandArgument)
            LoadUserTasks()
        End If
    End Sub

    Protected Sub lbRemoveAll_Click(sender As Object, e As System.EventArgs) Handles lbRemoveAll.Click
        Database.Tenant.Execute("UPDATE DTR_Tasks SET AssignedTo = NULL WHERE AssignedTo = " + hdnUserName.Value.ToSqlValue)
        LoadUserTasks()
    End Sub

    Protected Sub gvUsers_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvUsers.PageIndexChanging
        gvUsers.PageIndex = e.NewPageIndex
        LoadUserList()

    End sub
End Class