﻿Public Class _default4
    Inherits System.Web.UI.Page



    Sub LoadGrid(Optional ByVal pageIndex As Integer = 0)
        gvTasks.PageSize = ddlPageSize.SelectedItem.Text
        lblPage.Text = ""
        Dim orderBy As String = "Name"
        Select Case ddlOrderBy.SelectedItem.Value
            Case "1"
                orderBy = "Name"
            Case "2"
                orderBy = "COALESCE(AssignedTo, 'ZZZZZZ')"
            Case "3"
                orderBy = "TotalParcels DESC"
        End Select
        Dim offset As Integer = (gvTasks.PageSize) * gvTasks.PageIndex
        Dim dt As DataTable = d_("SELECT * FROM DTR_Tasks ORDER BY " + orderBy)
        gvTasks.PageIndex = pageIndex
        gvTasks.DataSource = dt
        gvTasks.DataBind()
        If gvTasks.Rows.Count = 0 Then
            lblPage.Text = "Sorry,There is no task has been created on DTR. Please try again."
        Else
            lblPage.Text = "Displaying records {0} to {1} of {2}".FormatString(gvTasks.PageIndex * gvTasks.PageSize + 1, gvTasks.PageIndex * gvTasks.PageSize + gvTasks.Rows.Count, dt.Rows.Count)
        End If
    End Sub

    Sub RefreshTaskStatus()
        Database.Tenant.Execute("EXEC dtr_ManageTasks 'Yes'")
    End Sub
    
    Sub RefreshTaskStatusNil()
        Database.Tenant.Execute("EXEC dtr_ManageTasks 'No'")
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Roles.IsUserInRole("DTR-Manager") Then
            If Not IsPostBack Then
                If Request.QueryString("pI") <> "" Or Request.QueryString("pS") <> "" Or Request.QueryString("pO") <> "" Then
                    Dim pageIndex As Integer = 0
                    If Request.QueryString("pI") <> "" Then
                        pageIndex = Convert.ToInt32(Request.QueryString("pI"))

                        '                'CC_4009
                        '                btnRefresh_Click(sender, e)
                        'If Roles.IsUserInRole("DTR-Manager") Then
                        '    If Not IsPostBack Then
                        '        If Request.QueryString("pI") <> "" Or Request.QueryString("pS") <> "" Or Request.QueryString("pO") <> "" Then
                        '            Dim pageIndex As Integer = 0
                        '            If Request.QueryString("pI") <> "" Then
                        '                pageIndex = Convert.ToInt32(Request.QueryString("pI"))
                        '            End If
                        '            If Request.QueryString("pS") <> "" Then
                        '                ddlPageSize.Items.FindByValue(Request.QueryString("pS")).Selected = True
                        '            End If
                        '            If Request.QueryString("pO") <> "" Then
                        '                ddlOrderBy.Items.FindByValue(Request.QueryString("pO")).Selected = True
                        '            End If
                        '            LoadGrid(pageIndex)
                        '        Else
                        '            Try
                        '                RefreshTaskStatusNil()
                        '                LoadGrid()
                        '            Catch ex As Exception
                        '                LoadGrid()
                        '                Alert("An error occurred while processing DTR Tasks. Found an invalid filter condition in the existing tasks.")
                        '            End Try
                    End If
                    If Request.QueryString("pS") <> "" Then
                        ddlPageSize.Items.FindByValue(Request.QueryString("pS")).Selected = True
                    End If
                    If Request.QueryString("pO") <> "" Then
                        ddlOrderBy.Items.FindByValue(Request.QueryString("pO")).Selected = True
                    End If
                    LoadGrid(pageIndex)
                Else
                    Try
                        RefreshTaskStatusNil()
                        LoadGrid()
                    Catch ex As Exception
                        LoadGrid()
                        Alert("An error occurred while processing DTR Tasks. Found an invalid filter condition in the existing tasks.")
                    End Try
                End If
            End If
        Else
            Alert("You must have the DTR Manager role to access this screen.")
            Page.RunScript("hidedtrtable();")
            Return
        End If

        'Else
        '    Alert("You must have the DTR Manager role to access this screen.")
        '    Page.RunScript("hidedtrtable();")
        '    Return
        'End If
    End Sub

    Private Sub ddlOrderBy_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlOrderBy.SelectedIndexChanged
        LoadGrid(0)
    End Sub

    Private Sub ddlPageSize_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
        LoadGrid()
    End Sub

    Private Sub gvTasks_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTasks.PageIndexChanging
        LoadGrid(e.NewPageIndex)
    End Sub

    Private Sub gvTasks_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvTasks.RowCommand
        If e.CommandName = "DeleteItem" Then
            Database.Tenant.Execute("DELETE FROM DTR_Tasks WHERE Id = " & e.CommandArgument)
            LoadGrid()
        End If
    End Sub

	Protected Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click	
		Try
	        RefreshTaskStatus()
	        LoadGrid()
        Catch ex As Exception 	
        	LoadGrid()
        	Alert("An error occurred while processing DTR Tasks. Found an invalid filter condition in the existing tasks.")
        End Try
    End Sub

    Private Sub gvTasks_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvTasks.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim ID As String = DataBinder.Eval(e.Row.DataItem, "ID")
            Dim hyp As HyperLink = DirectCast(e.Row.FindControl("hyp"), HyperLink)
            hyp.NavigateUrl = String.Format("edittask.aspx?taskid={0}&pI={1}&pS={2}&pO={3}", ID, gvTasks.PageIndex.ToString(), ddlPageSize.SelectedValue, ddlOrderBy.SelectedValue)
        End If
    End Sub
End Class