﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="images.aspx.vb" Inherits="CAMACloud.Console.images" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>CAMA Cloud&#174; Photos</title>
    <link rel="stylesheet" href="/App_Static/css/quality.css?4568" />
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyC7e5YrZlKqWtaY4ehsDwMgCis4IfNs7Xw"></script>
    <script type="text/javascript" src="/App_Static/jslib/infobubble.js"></script>
    <script type="text/javascript" src="/App_Static/jslib/jquery.js"></script>
    <script type="text/javascript" src="/App_Static/js/dtr/010-dtr.js?"></script>
    <script type="text/javascript" src="/App_Static/jslib/underscore-min.js"></script>
    <script type="text/javascript" src="/App_Static/js/qc/010-constants.js"></script>
    <script type="text/javascript" src="../../App_Static/js/qc/020-parcel.js"></script>
    <script type="text/javascript" src="../../App_Static/js/qc/050-photos.js?1238"></script>
    <script type="text/javascript" src="/App_Static/js/system.js"></script>
    <script type="text/javascript" src="/App_Static/js/qc/010-conversions.js"></script>
    <script type="text/javascript" src="/App_Static/js/qc/015-datatypes.js"></script>
    <script type="text/javascript" src="/App_Static/js/qc/080-validations.js"></script>
    <script type="text/javascript" src="/App_Static/js/qc/010-quality.js?123"></script>
    <script type="text/javascript" src="/App_Static/js/qc/000-window.js"></script>
    <script type="text/javascript" src="/App_Static/js/qc/030-parcel-search.js"></script>
    <script type="text/javascript" src="/App_Static/js/qc/055-sketch.js"></script>
    <script type="text/javascript" src="/App_Static/jslib/flexigrid.js"></script>
    <script type="text/javascript"  src="../../App_Static/jslib/tipr.js?01123478"></script>
    <style type="text/css">
            
        .photo-nav-button-popupthumbnail { height: 150px !important;}
        .border {
            background-color:#509ad7;
        }
        .photocss{
		    float: right;
    		border-radius: 5px;
    		padding: 2px 18px;
    		background-color: white;
    		color: #424242;
    		margin-top: 1px;
    		border: 1px solid silver;
    		font-size: 13px;
    		cursor: pointer;
		}
		.photocss:hover {
    		color: Black;
		}
		
    	@media only screen and (max-width: 675px){
        	.add-photo-frame{
            	height:4rem;
		}
			
			#photouploader{
                height:60px;
       }
       }
       @media only screen and (max-width: 548px){
            .add-photo-frame{
            	height:5rem;
		}
            #photouploader{
                    height:80px !important;
           }
            
}
      
    
    </style>
    <script type="text/javascript">

        var __DTR = true;
        window.onunload = function (e) {
            opener.onunloadPoupUp();
        };

        $(window).load(function () {
            opener.hideParcelPhotos();
            showNoTabs();
            deSelectAll();
            // showAllPhotos();
            Thumnailphotos();
          
        });
        var activeParcel = {};
        // var activeParcelData = {}; 
        if (window.opener != null) {
            activeParcel = window.opener.activeParcel;
            //   activeParcelData = window.opener.activeParcelData;
        }
        function reloadParentWindow() {
            if (window.opener != null) {
                window.opener.showPhotoThumbnail();
            }
        }
        function updateRecentAuditTrail()
        {
            if (window.opener != null) {
                window.opener.updateRecentAuditTrail();
            } 
        }
        function checkAll(btn) {
            $('.add-photo-frame').hide();
            var cntChk = $('.ChkSelPhoto:checked:not([disabled])').length;
            var totChk = $('.ChkSelPhoto:not([disabled])').length;
            //             if ($('#SelectAllPh').hasClass('selected') && (cntChk == totChk)) {
            if (cntChk == totChk) {
                $('.ChkSelPhoto:not([disabled])').each(function () { $(this).prop("checked", false); });
                $('#SelectAllPh').html('Select All');
                //                $('#SelectAllPh').removeClass('selected');
            } else {
                $('.ChkSelPhoto:not([disabled])').each(function () { $(this).prop("checked", true); });
                //                $('#SelectAllPh').addClass('selected');
                $('#SelectAllPh').html('Deselect All');
            }
        }
        function btncheckAll(chk) {
            if ($(chk).is(':checked')) {
                var cntChk = $('.ChkSelPhoto:checked').length;
                var totChk = $('.ChkSelPhoto').length;
                if (cntChk == totChk) {
                    $('#SelectAllPh').html('Deselect All');
                    //  $('#SelectAllPh').addClass('selected');
                }
            } else {
                $('#SelectAllPh').html('Select All');
                //     $('#SelectAllPh').removeClass('selected');
            }
        }
        function showPh(divId) {
            var d = $(divId).attr("divId");
           
        //  $('.photo-frame-allPhotos').hide();
        //    $('.add-photo-frame').hide();
        //    $('.photo-Manager-Back').show();
            $('.photo-frame').show();
            showPhoto(d);
          //  $('#imagetest').show();
          //  $('.photo-Manager').hide();
            //var t = $('#imagetest').html();
            //if(t=="")
            //Thumnailphotos();
        }
        function backPhotoMang() {
            getPhotoUpdates();
            $('.photo-frame-allPhotos').show();
            $('.photo-frame').hide();
            $('.photo-Manager').show();
            $('.photo-Manager-Back').hide();
            $('.ccimageId').hide();
         //   $('#imagetest').hide();
        }

        function photUploader() {
            $('.add-photo-frame').show();
            $('.photo-Manager').hide();
            console.log($('#uploadForm'));
            $('#uploadForm').append('<input type="button" id="btnCancel" value="Cancel"/>')
        }
        function showNoTabs(divId) {
            if (activeParcel.Photos.length == 0) {
                var d = $(divId).attr("divId");
                $('.photo-frame-allPhotos').hide();
                $('.add-photo-frame').hide();
                $('.add-photo-frame').show();
                showPhoto(d);
                $('.photo-Manager').hide();
            }
        }
        function deSelectAll() {
            $('#SelectAllPh').html('Select All');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        
            <div class="category-page photo-viewer photo-viewer" categoryid="photo-viewer" style="height: 80vh; margin-top: 7%;" >

                <div class="photo-frame hidden photo-frame-popup" style='height: 400px; width: 100%;'>
                    <a class="photo-nav-button nav-left  photo-nav-button-popup" onclick="prevPhoto(true);" style="margin-left: 15%"></a>
                    <!--<a class="photo-button photo-delete-button"
                                        onclick="deleteCurrentPhoto();"></a>-->
                    <a class="photo-button photo-download-button "></a><a class="photo-nav-button nav-right  photo-nav-button-popup"
                        onclick="nextPhoto(true);" style="margin-right: 15%"></a>
                    <a class="photo-button photo-recover-button hidden" onclick="recoverDeletedPhoto();"></a>
                </div>
                <div style='text-align: center; padding-top:5px;'>
                    <span class='ccimageId' style='position: relative; display: none; color:#968787;margin-right:23%; float:right'></span>
                    <span id="ImageCount"  style='position: relative; color: #968787; margin-left: 23%; float: left'></span>
                </div>
                <div id="comploader">
                    <div id="bgcomploader">
                    </div>
                </div>
                <div style="width: 100%; height: 150px; margin-top: 2%; text-align: center"> 
                    <div class="photo-frame-allPhotos">
                    </div>
                </div>
            </div>
        
        <div class="photo-Manager" style="display: block; padding-top: 15px;">
            <table>
                <tbody>
                    <tr>
                    	<td><button type="button" id="SelectAllPh" class="photocss" onclick="checkAll(this)">Select All</button></td>
                    	<td><button type="button" id="AddPh" class="photocss" onclick="photUploader()">Add Photo</button></td>
                    	<td><button type="button" id="DeletePh" class="photocss" onclick="deleteAllSelectedPhoto(true);">Delete</button></td>
                        <!--<td class="btn btn-2 btn-2g" onclick="checkAll(this)">
                            <a href="#parceldata" id="SelectAllPh">Select All</a>
                        </td>
                        <td class="btn btn-2 btn-2g" onclick="photUploader()">
                            <a href="#parceldata" id="AddPh"></a>Add Photo
                        </td>
                        <td class="btn btn-2 btn-2g deletemages" id="enablephotoframe" onclick="deleteAllSelectedPhoto(true);">
                            <a href="#parceldata" id="DeletePh">Delete</a>-->
                        <%--</td>--%>
                    </tr>
                </tbody>
            </table>
        </div>
          <%If Not Roles.IsUserInRole("DTR-ReadOnly") Then%>
        <div class="add-photo-frame hidden" style="margin-top: 10px;height:2.5rem";>
            <iframe id="photouploader" frameborder="0" style='width: 100%; float: left;height:100%;' src="/protected/tools/upload-photo.aspx?#dtrcall"
                scrolling="no"></iframe>
            <div style='width: 12%; float: right;'>
                <span class='downsycset' style='position: absolute; margin-top: 10px; display: none'>
                    <input id="Checkbox2" class="photo-DownSynced-button" onclick="makeDownSynced(this);"
                        runat="server" type="checkbox" name="vdvsdsvd" />Flag for Resync</span>
            </div>
        </div>
        <%End If %>
    </form>
</body>
</html>
