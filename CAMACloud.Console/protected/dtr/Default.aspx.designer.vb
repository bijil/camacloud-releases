﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class _Default1

    '''<summary>
    '''JavaScript1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents JavaScript1 As Global.CAMACloud.JavaScript

    '''<summary>
    '''ddlDTRTask control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlDTRTask As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''hlSketchViewer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hlSketchViewer As Global.System.Web.UI.HtmlControls.HtmlAnchor

    '''<summary>
    '''ddlBulkDTRStatus control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlBulkDTRStatus As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlSelectedStatus control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlSelectedStatus As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlSelectedEstimate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlSelectedEstimate As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ltParceltab control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ltParceltab As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''rpFieldAllCategories control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rpFieldAllCategories As Global.System.Web.UI.WebControls.Repeater

    '''<summary>
    '''rpFieldCategory control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rpFieldCategory As Global.System.Web.UI.WebControls.Repeater

    '''<summary>
    '''nbhdprofiletemp control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents nbhdprofiletemp As Global.CAMACloud.Console.NeighborhoodProfileTemp

    '''<summary>
    '''rpPhotoProperties control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rpPhotoProperties As Global.System.Web.UI.WebControls.Repeater

    '''<summary>
    '''Checkbox1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Checkbox1 As Global.System.Web.UI.HtmlControls.HtmlInputCheckBox

    '''<summary>
    '''rpCategoryForFields control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rpCategoryForFields As Global.System.Web.UI.WebControls.Repeater

    '''<summary>
    '''rpClassCalcFields control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rpClassCalcFields As Global.System.Web.UI.WebControls.Repeater

    '''<summary>
    '''PropertyRecordCard control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PropertyRecordCard As Global.CAMACloud.Console.App_Controls_app_PropertyRecordCard

    '''<summary>
    '''UpdatePanel3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanel3 As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''pnlUserEdit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlUserEdit As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lbColumnsLeft control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbColumnsLeft As Global.System.Web.UI.WebControls.ListBox

    '''<summary>
    '''lbColumnsRight control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbColumnsRight As Global.System.Web.UI.WebControls.ListBox

    '''<summary>
    '''exportlist control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents exportlist As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''exportto control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents exportto As Global.System.Web.UI.WebControls.Button
End Class
