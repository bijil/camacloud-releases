﻿Imports System.IO

Public Class statusmgmt
    Inherits System.Web.UI.Page


    Dim entityName As String = "Status Type"
    Dim tableName As String = "DTR_StatusType"
    Dim keys As New List(Of String)
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Roles.IsUserInRole("DTR-Manager") Then
            If Not IsPostBack Then
                LoadGrid()
                RunScript("UpdateChecked();")
            End If
            RunScript("enableColorPicker();")
        Else
            Alert("You must have the DTR Manager role to access this screen.")
            Page.RunScript("hidedtrStatus();")
            Return
        End If
    End Sub

    Sub LoadGrid()
        grid.DataSource = Database.Tenant.GetDataTable("SELECT * FROM " + tableName + " ORDER BY Name")
        grid.DataBind()
    End Sub
    
    Private Sub grid_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grid.RowCommand
        Select Case e.CommandName
        	Case "EditItem"
                Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM " + tableName + " WHERE Id = " & e.CommandArgument)
                txtName.Text = dr.GetString("Name")
                hdname.Value = dr.GetString("Name")
                txtDescription.Text = dr.GetString("Description")
                hdnId.Value = dr.GetInteger("Id")
				Dim checkValue As String = txtDescription.Text
                btnSave.Text = "Save " + entityName
                btnCancel.Visible = True
                LoadGrid()
            Case "DeleteItem"
            	Database.Tenant.Execute("DELETE FROM " + tableName + " WHERE Id = " & e.CommandArgument)
            	Dim Description As String = "Parcel status type deleted."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
            	CancelForm()
                LoadGrid()
        End Select
    End Sub

Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Dim unique As String = txtName.Text
        Dim txtDescriptions As String = txtDescription.Text
        unique = Trim(unique)

        If Not Regex.IsMatch(unique(0), "[a-z A-Z 0-9]") Then
			Alert("The status code should be started with alphanumerics.")
			Return
		End If
        
		Dim uniqueData As DataTable = Database.Tenant.GetDataTable("SELECT * from DTR_StatusType where Code = {0}".SqlFormatString(unique))
		If hdnId.Value = "" Then
			If uniqueData.Rows.Count > 0 Then
				Alert("Status Code already exists. Please use a unique status code.")
				Return
			End If
            Dim sql As String = "INSERT INTO " + tableName + " (Name, Description, Code) VALUES ( {0}, {1}, {0})"
            Database.Tenant.Execute(sql.SqlFormat(False, unique, txtDescriptions))
            Alert("Parcel status type added")
            
            Dim Description As String = "New parcel status type added."
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
		Else 
			If uniqueData.Rows.Count > 0 AndAlso Not hdname.Value = unique Then
				Alert("Status Code already exists. Please use a unique status code.")
			Else
                Dim dold As DataTable = Database.Tenant.GetDataTable(" SELECT * FROM  DTR_StatusType WHERE Id = " + hdnId.Value + " and Name ='" + unique + "' and Description='" + txtDescriptions + "'")

                If (dold.Rows.Count > 0) Then
                    hdnId.Value = ""
                    txtName.Text = ""
                    txtDescription.Text = ""
                    btnSave.Text = "Add " + entityName
                    btnCancel.Visible = False
                Else

                    Dim sql As String = "UPDATE " + tableName + " SET Name = {1}, Description = {2}, Code = {1} WHERE Id = {0}"
                    Database.Tenant.Execute(sql.SqlFormat(False, hdnId, unique, txtDescriptions))
                    Alert("Parcel status type modified")
                    Dim Description As String = "Parcel status type updated."
                    SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)

                End If

            End If 
			End If
        CancelForm()
        LoadGrid()
        RunScript("UpdateChecked();")
    End Sub

    Private Sub CancelForm() Handles btnCancel.Click
        hdnId.Value = ""
        txtName.Text = ""
        txtDescription.Text = ""
        btnSave.Text = "Add " + entityName
        btnCancel.Visible = False
    End Sub

    Protected Sub OnPageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        grid.PageIndex = e.NewPageIndex
        Me.LoadGrid()
    End Sub


End Class