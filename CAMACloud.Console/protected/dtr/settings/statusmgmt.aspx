﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master"
    CodeBehind="statusmgmt.aspx.vb" Inherits="CAMACloud.Console.statusmgmt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .col1
        {
            padding-right: 30px;
            padding-left: 5px;
        }

        .gis-color-picker {
            display: inline-block;
            width: 20px; 
            height: 20px; 
            border: 1px solid #ccc;
        }
    </style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.css">
    <script type="text/javascript">
    	function UpdateChecked(){
    		$('input[type="checkbox"]').each(function(){
				$(this).change(function(){
					var value = this.checked ? 1: 0;
					var columnname = $(this).parent().attr('field');
					var id = $(this).parent().attr('loginid');
		    		var data = {
		                Column: columnname,
		                Id: id,
		                Value: value,
		            };              
					$ds('updateddtrstatustype', data, function (resp) {
		            
		            });
				})
			})
        }

		function hidedtrStatus(){        	
        	$('.dtrtable').hide(); 
			$('.info').hide();        	
        }

        function enableColorPicker() {
            $(".gis-color-picker").each(function () {
                let clr = $(this).attr('dtr_color');
                $(this).css('background-color', clr);
                $(this).spectrum({
                    change: function (color) {
                        var hexColor = color.toHexString();
                        $(this).css('background-color', hexColor);
                        $(this).attr('dtr_color', hexColor);
                        var dId = $(this).attr('dtrId');
                        if (hexColor && dId) {
                            var data = { color: hexColor, dId: dId };
                            $ds('updatedtrcolorcode', data, function (resp) { });
                        }
                    },
                    showInput: true,
                    preferredFormat: "hex",
                    color: clr,
                });
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>
        Manage Parcel Status Types</h1>
    <p class="info">
        Manage the parcel status types - these types will be listed in the DTR console header
        to be associated with each parcel.</p>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <table class="dtrtable">
                <tr>
                    <td class="col1">
                        Status Code:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtName" Width="100px" MaxLength="10" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName"
                            ErrorMessage="*" ValidationGroup="Form" />
                    </td>
                </tr>
                <tr>
                    <td class="col1">
                        Description:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtDescription" Width="250px" MaxLength="50" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDescription"
                            ErrorMessage="*" ValidationGroup="Form" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:HiddenField runat="server" ID="hdnId" Value="" />
                        <asp:HiddenField runat="server" ID="hdname" Value="" />
                        <asp:Button runat="server" ID="btnSave" Text=" Add Status Type " Font-Bold="true"  style="margin-top:10px;"
                            ValidationGroup="Form"/>
                        <asp:Button runat="server" ID="btnCancel" Text=" Cancel " Visible="false" />
                    </td>
                </tr>
            </table>
            <asp:GridView runat="server" ID="grid" AllowPaging="true" PageSize="15" OnPageIndexChanging="OnPageIndexChanging">
                <Columns>
                    <asp:TemplateField>
                        <ItemStyle Width="25px" />
                        <ItemTemplate>
                            <%# (Container.DataItemIndex)+1 %>.
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Name" HeaderText="Code" ItemStyle-Width="80px" />
                    <asp:BoundField DataField="Description" HeaderText="Description" ItemStyle-Width="300px" />
                    <asp:TemplateField>
                        <ItemStyle Width="80px" />
                        <ItemTemplate>
                            <asp:LinkButton runat="server" ID="lbEdit" Text="Edit" CommandName='EditItem' CommandArgument='<%# Eval("Id") %>' />
                            <asp:LinkButton runat="server" ID="lbDelete" Text="Delete" CommandName='DeleteItem'
                                CommandArgument='<%# Eval("Id") %>' OnClientClick='return confirm("Are you sure you want to delete?")' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Data Entry" HeaderStyle-Width="80px">
                    	<ItemTemplate>
                    		<asp:CheckBox runat="server" ID="userrole1" class="dataEntryRole" Checked='<%# Convert.ToBoolean(Eval("DTRReadonly"))%>' field='DTRReadOnly' loginid='<%# Eval("Id") %>'/>
                    	</ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remote Verification" HeaderStyle-Width="150px">
                    	<ItemTemplate>
                    		<asp:CheckBox runat="server" ID="userrole2" class="rmVerificationRole" Checked='<%# Convert.ToBoolean(Eval("DTREditor"))%>' field='DTREditor' loginid='<%# Eval("Id") %>'/>
                    	</ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Manager" HeaderStyle-Width="80px">
                    	<ItemTemplate>
                    		<asp:CheckBox runat="server" ID="userrole3" class="managerRole" Checked='<%# Convert.ToBoolean(Eval("DTRManager"))%>' field='DTRManager' loginid='<%# Eval("Id") %>'/>
                    	</ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="GIS Flag Color" HeaderStyle-Width="120px">
                        <ItemTemplate>
                            <span id="GISColorPicker" class="gis-color-picker" dtr_color="<%# If(String.IsNullOrEmpty(Eval("GIS_Flag_Color").ToString()), "#FFFFFF", Eval("GIS_Flag_Color")) %>" dtrId='<%# Eval("Id") %>'></span>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
