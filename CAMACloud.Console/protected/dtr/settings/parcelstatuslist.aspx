﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master"
    CodeBehind="parcelstatuslist.aspx.vb" Inherits="CAMACloud.Console.parcelstatuslist" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .col1
        {
            padding-right: 30px;
            padding-left: 5px;
        }
    </style>
    <script>
    $(document).ready(function(){
    	document.getElementById("MainContent_MainContent_ControlFile").addEventListener('change', function () {
             var fileUpload = $('input[type="file"]');
               if (fileUpload.val() != '') {
                   filePath = fileUpload.val();
               }
               if (filePath == '') {
                   alert('Please choose a .csv file');
                   return false;
               }
               if (filePath.split('.').pop().toLowerCase() != 'csv') {
                   alert("You have uploaded an invalid file. Only'.csv' formats are allowed.");
                   fileUpload.val("")
                   return false;
               }
         });
       });
       function hidedtrStatusList(){
    	$('.stslist').hide();
    	}
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>
        Parcel Status List Upload</h1>
    <div class="stslist">
    <p class="info">
       You can assign DTR Status to parcels by uploading a control list. The control list should be in CSV Format, with the following fields.</p>
    <p runat="server" id="pHaveCK">
        <%=KeyList()%>,<strong>DTR Status Code</strong>
    </p>
    <p runat="server" id="pNoCK" CssClass="info">
        Please set the common key fields before using this feature.
    </p>
     <p class="info">
        <strong>DTR Status Code</strong> -The code value for each description that are defined in Parcel Status Types. <br />
    </p>
    <p>
        <strong>Notes :</strong>
        <ul>
            <li>The maximum size of uploaded file should be 4MB.</li>
        </ul>
    </p>
    <p style="margin: 5px 0px; padding: 12px 5px; background: #DFDFDF; border: 1px solid #CFCFCF; width: 500px;">
        <asp:FileUpload ID="ControlFile" runat="server" Width="400px" accept=".csv"/>
        <asp:Button ID="btnUpload" runat="server" Text="Upload" Height="24px" Width="80px" Font-Bold="true" />
    </p>
    </div>     
</asp:Content>

