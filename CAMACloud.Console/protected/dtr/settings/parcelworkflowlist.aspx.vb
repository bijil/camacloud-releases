﻿Imports System.IO
Imports Microsoft.VisualBasic.FileIO
Public Class parcelworkflowlist
    Inherits System.Web.UI.Page
    Dim dtTemp As New DataTable
    Dim keys As String

    Public ReadOnly Property GetKeyFieldName As String
        Get
            Dim KeyFieldName As String = Database.Tenant.Application.KeyField(1).ToString()
            If Database.Tenant.Application.ShowAlternateField And Database.Tenant.Application.AlternateKeyfield IsNot Nothing And Database.Tenant.Application.AlternateKeyfield <> "" Then
                KeyFieldName = Database.Tenant.Application.AlternateKeyfield.ToString()
            End If
            Return KeyFieldName
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Roles.IsUserInRole("DTR-Manager") Then
            Alert("You must have the DTR Manager role to access this screen.")
            Page.RunScript("hidedtrStatusList();")
            Return
        End If
    End Sub

    Function KeyList() As String
        Dim keys1 As String = GetKeyFieldName
        pHaveCK.Visible = keys1 <> ""

        Return "<strong>" + keys1 + "</strong>"
    End Function

    Private Sub btnUpload_Click(sender As Object, e As EventArgs) Handles btnUpload.Click
        Dim dtTemp As DataTable = New DataTable()
        Dim sr As New StreamReader(ControlFile.FileContent)

        If ControlFile.FileBytes.Length = 0 Then
            Alert("Please select the file to be uploaded.")
            Return
        End If

        Dim Description As String = "New parcel status list uploaded."
        If csv_to_datatableConvertion(sr, dtTemp) Then
            Try
                Database.Tenant.BulkLoadTableFromDataTable("temp_workflowlistupload", dtTemp)
                Dim qry = "DECLARE @RowCount INT; INSERT INTO ParcelAuditTrail(ParcelId, LoginID, EventType, Description,ApplicationType)"
                qry += " SELECT p.CC_ParcelId, '" + Membership.GetUser().ToString() + "', 24, 'Parcel Workflow was set to ' + t.[dtr workflow code] + ' - ' + pt.[Description] + ' by user through the Parcel Workflow List upload','Desktop' FROM temp_workflowlistupload t JOIN ParcelData p ON t.[" + GetKeyFieldName + "] = p.[" + GetKeyFieldName + "] JOIN ParcelWorkFlowStatusType pt ON t.[dtr workflow code] = pt.Code LEFT JOIN ParcelWorkFlowStatus w ON p.CC_ParcelId = w.ParcelId AND t.[dtr workflow code] = w.StatusCode group by p.CC_ParcelId,t.[dtr workflow code] ,pt.[Description];"
                qry += "SET @RowCount = @@ROWCOUNT; INSERT INTO ParcelWorkFlowStatus(ParcelId, StatusCode)"
                qry += " SELECT p.CC_ParcelId, t.[dtr workflow code] FROM temp_workflowlistupload t JOIN ParcelDATA p ON t.[" + GetKeyFieldName + "] = p.[" + GetKeyFieldName + "] JOIN ParcelWorkFlowStatusType pt ON t.[dtr workflow code] = pt.Code LEFT JOIN ParcelWorkFlowStatus w ON p.CC_ParcelId = w.ParcelId AND t.[dtr workflow code] = w.StatusCode WHERE p.CC_ParcelId <> w.ParcelId OR w.ParcelId IS NULL group by p.CC_ParcelId, t.[dtr workflow code];"
                qry += "DROP Table temp_workflowlistupload; SELECT @RowCount AS AffectedRowCount;"
                Dim affcnt As Integer = Database.Tenant.GetIntegerValue(qry)
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
                Alert("New parcel workflow status list uploaded. Inserted Workflow Status count: " + affcnt.ToString())
            Catch ex As Exception
                Alert(ex.Message)
            End Try
        End If

    End Sub

    Private Sub RembtnUpload_Click(sender As Object, e As EventArgs) Handles RemBtnUpload.Click
        Dim dtTemp As DataTable = New DataTable()
        Dim sr As New StreamReader(ControlFileRem.FileContent)

        If ControlFileRem.FileBytes.Length = 0 Then
            Alert("Please select the file to be uploaded.")
            Return
        End If

        Dim Description As String = "Parcel status list has been Removed."
        If csv_to_datatableConvertion(sr, dtTemp) Then
            Try
                Database.Tenant.BulkLoadTableFromDataTable("temp_workflowlistRemupload", dtTemp)
                Dim qry = "DECLARE @RowCount INT; INSERT INTO ParcelAuditTrail(ParcelId, LoginID, EventType, Description,ApplicationType)"
                qry += " SELECT p.CC_ParcelId, '" + Membership.GetUser().ToString() + "', 24, 'Parcel Workflow ' + t.[dtr workflow code] +' - '+ pt.[Description] + ' is removed by user through the Parcel Workflow List upload','Desktop' FROM temp_workflowlistRemupload t JOIN ParcelData p ON t.[" + GetKeyFieldName + "] = p.[" + GetKeyFieldName + "] JOIN ParcelWorkFlowStatusType pt ON t.[dtr workflow code] = pt.Code INNER JOIN ParcelWorkFlowStatus w ON p.CC_ParcelId = w.ParcelId AND t.[dtr workflow code] = w.StatusCode group by  p.CC_ParcelId,t.[dtr workflow code] ,pt.[Description];"
                qry += "SET @RowCount = @@ROWCOUNT; DELETE Pw FROM ParcelWorkFlowStatus Pw join ParcelData pd on Pw.ParcelId = pd.CC_ParcelId join temp_workflowlistRemupload T on T.[" + GetKeyFieldName + "]  = pd.[" + GetKeyFieldName + "]  WHERE T.[dtr workflow code] = Pw.StatusCode ;"
                qry += "DROP Table temp_workflowlistRemupload; SELECT @RowCount AS AffectedRowCount;"
                Dim affcnt As Integer = Database.Tenant.GetIntegerValue(qry)
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
                Alert("Parcel workflow status list uploaded. Removed Workflow Status count: " + affcnt.ToString())
            Catch ex As Exception
                Alert(ex.Message)
            End Try
        End If

    End Sub

    Public Function csv_to_datatableConvertion(stream As StreamReader, ByRef dt As DataTable) As Boolean
        Try
            Dim pattern As String = "^\s*""?|""?\s*$"
            Dim rgx As New Regex(pattern)
            Dim Lines As String() = stream.ReadToEnd().Split(Environment.NewLine)
            Dim AppcolName As String = GetKeyFieldName
            Dim Fields As String() = Lines(0).Split(New Char() {","c})
            Dim Cols As Integer = Fields.GetLength(0)

            If (Lines(0).Split(",").Length <> 2) Then
                Alert("There is should be 2 Fields in uploading CSV File")
                Return False
            End If

            If (Fields(0) <> AppcolName Or Fields(1) <> "DTR Workflow Code") Then
                Alert("Invalid data format please revise your entry")
                Return False
            End If

            For i As Integer = 0 To Cols - 1
                dt.Columns.Add(rgx.Replace(Fields(i).ToUpper(), ""), GetType(String))
            Next

            Dim rgx1 As New Regex("(?!\B""[^""]*),(?![^""]*""\B)")
            Dim Row As DataRow
            Dim PSLCount As Integer = Lines.GetLength(0) - 1
            If (PSLCount = 0) Then
                Alert("No parcels found")
                Return False
            End If

            For i As Integer = 1 To Lines.GetLength(0) - 1
                If Lines(i).Trim.Length = 0 Then
                    Continue For
                End If
                Fields = ParseCsv(Lines(i))
                If Fields.Length < Cols Then
                    Continue For
                End If

                If Fields(0).Trim() <> "" Then
                    Fields(1) = Fields(1).Trim(""""c)
                    For Each value As String In Fields(1).Split(","c)
                        Row = dt.NewRow()
                        For j As Integer = 0 To Cols - 1
                            If Fields.Length <> Cols Then
                                Exit For
                            End If
                            Dim V = IIf(j = 1, value, Fields(j))
                            Row(j) = rgx.Replace(V, "")
                        Next
                        dt.Rows.Add(Row)
                    Next
                End If
            Next
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    Function ParseCsv(csv As String) As String()
        Using parser As New TextFieldParser(New StringReader(csv))
            parser.TextFieldType = FieldType.Delimited
            parser.SetDelimiters(",")
            parser.HasFieldsEnclosedInQuotes = True

            Return parser.ReadFields()
        End Using
    End Function

End Class
