﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master" CodeBehind="parcelworkflowlist.aspx.vb" Inherits="CAMACloud.Console.parcelworkflowlist" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .col1 {
            padding-right: 30px;
            padding-left: 5px;
        }
        
        @media screen and (max-height: 699px) {
            .workflowDiv {
                display: inline-block;
            }

            .workflowTble {
                border-collapse: collapse;
            }

                .workflowTble th {
                    border: 1px solid #ddd;
                    padding: 2px;
                    text-align: center;
                    background-color: #f2f2f2;
                }

                .workflowTble td {
                    border: 1px solid #ddd;
                    padding: 2px;
                    text-align: center;
                }
        }

        @media screen and (min-height: 700px) and (max-height: 799px) {
            .workflowDiv {
                display: inline-block;
            }

            .workflowTble {
                border-collapse: collapse;
            }

                .workflowTble th {
                    border: 1px solid #ddd;
                    padding: 5px;
                    text-align: center;
                    background-color: #f2f2f2;
                }

                .workflowTble td {
                    border: 1px solid #ddd;
                    padding: 5px;
                    text-align: center;
                }
        }

        @media screen and (min-height: 800px) {
            .workflowTble {
                border-collapse: collapse;
            }

                .workflowTble th {
                    border: 1px solid #ddd;
                    padding: 5px;
                    text-align: center;
                    background-color: #f2f2f2;
                }

                .workflowTble td {
                    border: 1px solid #ddd;
                    padding: 5px;
                    text-align: center;
                }
        }

    </style>
    <script>
        $(document).ready(function(){
    	    document.getElementById("MainContent_MainContent_ControlFile").addEventListener('change', function () {
                var fileUpload = $('input[type="file"]');
                    if (fileUpload.val() != '') {
                        filePath = fileUpload.val();
                }

                if (filePath == '') {
                    alert('Please choose a .csv file');
                    return false;
                }

                if (filePath.split('.').pop().toLowerCase() != 'csv') {
                    alert("You have uploaded an invalid file. Only'.csv' formats are allowed.");
                    fileUpload.val("")
                    return false;
                }
            });
        });

        $(document).ready(function () {
            document.getElementById("MainContent_MainContent_ControlFileRem").addEventListener('change', function () {
                var fileUploadRem = $('input[type="file"]');
                
                if (fileUploadRem.val() != '') {
                    var FPath = fileUploadRem.val();
                    if (FPath == '') {
                        alert('Please choose a file');
                        return false;
                    } else if (FPath.split('.').pop().toLowerCase() !== 'csv') {
                        alert("You have uploaded an invalid file. Only '.csv' formats are allowed.");
                        fileUploadRem.val(""); // Clear the file input
                        return false;
                    }
                }
            });
        });

        function hidedtrStatusList(){
            $('.stslist').hide();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>
        Parcel Workflow Upload</h1>
    <div class="stslist">
    <p class="info">
       You can assign Workflow(s) to a parcel by uploading a list in CSV format with the following fields:</p>
    <p runat="server" id="pHaveCK">
        <%=KeyList()%>,<strong>DTR Workflow Code</strong>
    </p>
    <p class="info">
        <strong>DTR Workflow Code</strong> - the code value for each Workflow as defined in the system’s Parcel Workflow Types. <br />
    </p>
    <p>
        <strong>Notes :</strong>
        <ul>
            <li>The maximum size of uploaded file should be 4MB.</li>
            <li>Data can be uploaded in either of the formats shown below.</li>
        </ul> 
    </p>
    <div class="workflowDiv">
        <p>Option 1:</p>
        <div>
            <table class="workflowTble">
                <tr>
                    <th><%=GetKeyFieldName%></th>
                    <th>DTR Workflow Code</th>
                </tr>
                <tr>
                    <td>100028</td>
                    <td>"01,02,03"</td>
                </tr>
                <tr>
                    <td>100029</td>
                    <td>"03,07"</td>
                </tr>
                <tr>
                    <td>100030</td>
                    <td>"03,07"</td>
                </tr>
            </table>
        </div>
    </div>
    <div class="workflowDiv">
        <p>Option 2:</p>
        <div>
            <table class="workflowTble">
                <tr>
                    <th><%=GetKeyFieldName%></th>
                    <th>DTR Workflow Code</th>
                </tr>
                <tr>
                    <td>100028</td>
                    <td>01</td>
                </tr>
                <tr>
                    <td>100028</td>
                    <td>02</td>
                </tr>
                <tr>
                    <td>100028</td>
                    <td>03</td>
                </tr>
                <tr>
                    <td>100029</td>
                    <td>03</td>
                </tr>
                <tr>
                    <td>100029</td>
                    <td>07</td>
                </tr>
                <tr>
                    <td>100030</td>
                    <td>03</td>
                </tr>
                <tr>
                    <td>100030</td>
                    <td>07</td>
                </tr>
            </table>
        </div>
    </div>
        <p style="margin: 5px 0px; padding: 12px 5px; background: #DFDFDF; border: 1px solid #CFCFCF; width: 550px;">
            <asp:FileUpload ID="ControlFile" runat="server" Width="400px" accept=".csv"/>
            <asp:Button ID="btnUpload" runat="server" Text="Upload & Add" Height="24px" Width="120px" Font-Bold="true" />
        </p>
        <p class="info">
            To remove Workflow(s) from a parcel, upload the CSV file with Workflow Codes to be removed.
        </p>
        <p class="info">
            Any Workflow existing on the property not included in the CSV file will not be removed.
        </p>

        <p style="margin: 5px 0px; padding: 12px 5px; background: #DFDFDF; border: 1px solid #CFCFCF; width: 550px;">
            <asp:FileUpload ID="ControlFileRem" runat="server" Width="400px" accept=".csv"/>
            <asp:Button ID="RemBtnUpload" runat="server" Text="Upload & Remove" Height="24px" Width="140px" Font-Bold="true" />
        </p>
    </div>     
</asp:Content>
