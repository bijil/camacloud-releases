﻿Public Class userroles
    Inherits System.Web.UI.Page

    Dim roleNames As String() = {"DTR-ReadOnly", "DTR-Editor", "DTR-Manager"}

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    	If Roles.IsUserInRole("DTR-Manager") Then	
	    	If Not IsPostBack Then
	           LoadUserRoles()
	    	End If
	    Else      	
      	Alert("You must have the DTR Manager role to access this screen.")      	
      	gvUsers.Visible=False
      	Return
      End If
    End Sub
    Protected Sub LoadUserRoles()
        Dim dt As DataTable = Database.Tenant.GetDataTable("SELECT * FROM UserSettings ORDER BY FirstName, MiddleName, LastName")
        Dim default_ManagerUser As String() = {"admin", "dcs-support", "dcs-qa", "dcs-rd", "dcs-integration", "dcs-ps"}
        For i As Integer = dt.Rows.Count - 1 To 0 Step -1
            If Not default_ManagerUser.Contains(dt.Rows(i)("LoginId").ToString().ToLower()) AndAlso Not Roles.IsUserInRole(dt.Rows(i)("LoginId").ToString(), "DTR") Then
                dt.Rows.RemoveAt(i)
            End If
        Next
        gvUsers.DataSource = dt
        gvUsers.DataBind()

    End Sub
    Private Sub gvUsers_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvUsers.PageIndexChanging
    	gvUsers.PageIndex = e.NewPageIndex
    	LoadUserRoles()
    	
    End Sub
    Private Sub _toggleUserRole(chk As CheckBox, roleName As String)
        Dim row As GridViewRow = chk.Parent.Parent
        Dim loginId As String = row.GetHiddenValue("hdnLoginId")
        If chk.Checked Then
            For Each rn In roleNames
                If rn <> roleName AndAlso Roles.IsUserInRole(loginId, rn) Then
                    Roles.RemoveUserFromRole(loginId, rn)
                End If
            Next
            If Not Roles.IsUserInRole(loginId, roleName) Then
                Roles.AddUserToRole(loginId, roleName)
            End If
        End If
        If Not chk.Checked Then
            If Roles.IsUserInRole(loginId, roleName) Then
                Roles.RemoveUserFromRole(loginId, roleName)
            End If
        End If
        Dim Description As String = "New role "+roleName+" assigned to "+loginId+"."
        SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
       
    End Sub

    Public Sub ToggleReadOnlyUser(ByVal sender As Object, ByVal e As System.EventArgs)
        _toggleUserRole(sender, "DTR-ReadOnly")
    End Sub

    Public Sub ToggleEditorUser(ByVal sender As Object, ByVal e As System.EventArgs)
        _toggleUserRole(sender, "DTR-Editor")
    End Sub

    Public Sub ToggleManagerUser(ByVal sender As Object, ByVal e As System.EventArgs)
        _toggleUserRole(sender, "DTR-Manager")
    End Sub

End Class