﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master" CodeBehind="parcelworkflowmgmt.aspx.vb" Inherits="CAMACloud.Console.parcelworkflowmgmt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .col1
        {
            padding-right: 30px;
            padding-left: 5px;
        }
    </style>
    <script type="text/javascript">
    	function UpdateChecked () {
            $('input[type="checkbox"]').each(function () {
                $(this).change(function () {
                    var value = this.checked ? 1 : 0;
                    var columnname = $(this).parent().attr('field');
                    var id = $(this).parent().attr('loginid');
                    var data = {
                        Column: columnname,
                        Id: id,
                        Value: value,
                    };
                    $ds('updateworkflowstatustype', data, function (resp) {

                    });
                });
            });
        }

		function hidedtrStatus () {        	
        	$('.dtrtable').hide(); 
			$('.info').hide();        	
        }

        function deleteConfirmation() {
            return confirm("Are you sure you want to delete? \n\nNote: If you delete an existing Work flow Type, any properties that currently contain this Work flow Type will be removed.");
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>
        Parcel Workflow Types</h1>
    <p class="info">
        Manage the parcel workflow status types - these types will be listed in the DTR console header
        to be associated with each parcel.</p>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <table class="dtrtable">
                <tr>
                    <td class="col1">
                        Status Code:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtName" Width="100px" MaxLength="10" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName"
                            ErrorMessage="*" ValidationGroup="Form" />
                    </td>
                </tr>
                <tr>
                    <td class="col1">
                        Description:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtDescription" Width="250px" MaxLength="250" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDescription"
                            ErrorMessage="*" ValidationGroup="Form" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:HiddenField runat="server" ID="hdnId" Value="" />
                        <asp:HiddenField runat="server" ID="hdname" Value="" />
                        <asp:Button runat="server" ID="btnSave" Text=" Add Status Type " Font-Bold="true"  style="margin-top:10px;"
                            ValidationGroup="Form"/>
                        <asp:Button runat="server" ID="btnCancel" Text=" Cancel " Visible="false" />
                    </td>
                </tr>
            </table>
            <asp:GridView runat="server" ID="grid">
                <Columns>
                    <asp:TemplateField>
                        <ItemStyle Width="25px" />
                        <ItemTemplate>
                            <%# (Container.DataItemIndex)+1 %>.
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Code" HeaderText="Code" ItemStyle-Width="80px" />
                    <asp:BoundField DataField="Description" HeaderText="Description" ItemStyle-Width="300px" />
                    <asp:TemplateField>
                        <ItemStyle Width="80px" />
                        <ItemTemplate>
                            <asp:LinkButton runat="server" ID="lbEdit" Text="Edit" CommandName='EditItem' CommandArgument='<%# Eval("Id") %>' />
                            <asp:LinkButton runat="server" ID="lbDelete" Text="Delete" CommandName='DeleteItem'
                                CommandArgument='<%# Eval("Id") %>' OnClientClick='return deleteConfirmation()' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Data Entry" HeaderStyle-Width="80px">
                    	<ItemTemplate>
                    		<asp:CheckBox runat="server" ID="userrole1" class="dataEntryRole" Checked='<%# Convert.ToBoolean(Eval("DTRReadonly"))%>' field='DTRReadOnly' loginid='<%# Eval("Id") %>'/>
                    	</ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remote Verification" HeaderStyle-Width="150px">
                    	<ItemTemplate>
                    		<asp:CheckBox runat="server" ID="userrole2" class="rmVerificationRole" Checked='<%# Convert.ToBoolean(Eval("DTREditor"))%>' field='DTREditor' loginid='<%# Eval("Id") %>'/>
                    	</ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Manager" HeaderStyle-Width="80px">
                    	<ItemTemplate>
                    		<asp:CheckBox runat="server" ID="userrole3" class="managerRole" Checked='<%# Convert.ToBoolean(Eval("DTRManager"))%>' field='DTRManager' loginid='<%# Eval("Id") %>'/>
                    	</ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>