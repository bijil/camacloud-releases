﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/ParcelManager.master"
    CodeBehind="edittask.aspx.vb" Inherits="CAMACloud.Console.qgtest" %>

<%@ Import Namespace="System.Web.Script.Services" %>
<%@ Import Namespace="System.Web.Services" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="CAMACloud" %>
<%@ Import Namespace="CAMACloud.Console" %>
<%@ Import Namespace="CAMACloud.Data" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="Stylesheet" href="/App_Static/css/01-lookup.css?<%= Now.Ticks %>" />
    <script type="text/javascript" src="/App_Static/js/qc/02-lookup.js?<%= Now.Ticks %>"></script>
    <link rel="stylesheet" type="text/css" href="/App_Static/css/queryGenerator.css?t=1" />
    <script type="text/javascript" src="/App_Static/js/queryGenerator.js?t=3"></script>
    <style>
        .col1
        {
            width: 140px;
        }
        textarea {
			resize: none;
		}
        .filter-table,.filter-table td{
            border  : 1px solid #a29791
        }
        .rTable,.rTable td{
            border  : 1px solid #a29791
        }
        .filter-table{
            border-collapse: collapse;
        }
        .rTable{
            border-collapse: collapse;
            width : 30%;
        }
        .filter-list{
            margin: 10px 0px;
            width: 100%;
            flex: 1;
            overflow: auto;
        }
        .tHead tr{
            font-size: 17px;
            font-weight: 600;
            text-align: center
        }
        .container{
            display: flex;
            height: 80vh;
        }
        .result_container{
            max-height: 100%;
            flex: 0 50%;
            overflow: auto;
        }
        .result_table{
        }
        .qg_container{
            display: flex;
            flex-direction: column;
            overflow: auto;
            flex: 0 50%;
        }
        .result_flex{
            display : flex;
            flex-direction : column;
            margin-left: 50px;
        }
        .filt-parcel{
            width : 115px;
        }
    </style>
    
    <script type="text/javascript">
        var filterId = 0;
    	$(function () {
            loadFilters();
        });

        function saveFilter(){
            var name = $('.filter-name').val();
            var query = qg.results.queryToDisplay;
            var queryObj = JSON.stringify(qg.results.queryObj)
            var valid = true;
            var selectQuery  = qg?.results?.selectQuery
            var joinQuery = qg?.results?.joinQuery
            var whereQuery = qg?.results?.whereQuery
            if(!name || !query || !queryObj) valid = false;
            if(valid){
                PageMethods.saveFilter(name, query, queryObj, filterId, selectQuery, joinQuery, whereQuery, (d) => {
                    if(d.Success) {
                        alert("filter saved")
                        loadFilters();
                    }    
                    else
                        alert(d.ErrorMessage);
                })
            }
            else 
            {
                alert("Please fill all the fields. Use query generator to build condition")
            }    
        }

        function loadFilters(){
            $('.filter-table tbody').html('');
             $('.filter-table thead').html('');
            PageMethods.GetAllFilters((d) => {
                if(d.Success) {
                    fillTable(d.Data && d.Data.Filters)
                }
                   
            });
        }
        var qg = {}
        function openQG(){
        	showMask();
            var obj = $('#txtFilterData').attr('queryObject');
            obj = obj ? obj : '';
            if(obj != ''){
                var qOb = JSON.parse(obj);
                qg = queryGenerator({onSaveCallback : saveCallback},{queryObj : qOb});
            }
            else{
                qg = queryGenerator({onSaveCallback : saveCallback});
            }
        }

        function saveCallback(){
            $('.result_container').hide();
            $('.rTable').empty();
            $('#txtFilterData').attr('queryObject',JSON.stringify(qg.results.queryObj)).val(qg.results.queryToDisplay);
            var q = qg?.results?.selectQuery + qg?.results?.joinQuery +  qg?.results?.whereQuery;
            q = `Select * into #tempqg FROM (${q})x Select Top 100 * from #tempqg  Select count(*) As Count From #tempqg`
            PageMethods.GetResult(q, (d)=>{
                if(d.Success){
                    if(d.Data?.Parcels?.length !=  0)
                        fillResultTable(d.Data && d.Data)
                    else{    
                        $('.result_container').hide(); 
                        alert("No matching parcels found");
                    }
                }
                else {
                    alert(d.ErrorMessage)
                }
            })

        }

        function fillResultTable(result){
            let count = result?.Count[0]?.Count;
            let data = result?.Parcels;
            $('.result_count').text(`Displaying top ${result?.Parcels?.length} Parcels of ${count} results`)
            let rTable = $('<table>').attr({class : "rTable"})
            let thead = $('<tr>').append(
                $('<td>').attr({class : 'filt-id'}).html("Id"),
                $('<td>').attr({class : 'filt-parcel'}).html("Parcel"),
                $('<td>').attr({class : 'filt-parcel'}).html("KeyValue1")
            )    
            rTable.append(thead);
            var index = 1
            for(let x in data){
                var row = $('<tr>').append(
                    $('<td>').html(index),
                    $('<td>').html(data[x]?.ParcelId),
                    $('<td>').html(data[x]?.KeyValue1),
                )
                rTable.append(row)
                index += 1
            }
            $('.result_table').append(rTable);
            $('.result_container').show();
        }

        function clearAll(){
            $('.filter-name').val('');
            $('#txtFilterData').val('');
            $('.result_container').hide();
            $('#txtFilterData').attr("query",'');
            $('#txtFilterData').attr("queryObject",'');
            filterId = 0;
            qg = {};
        }

        function fillTable(data){
            var thead = $('<tr>').append(
                $('<td>').attr({class : 'filt-name'}).html("Name"),
                $('<td>').attr({class : 'filt-open', style : "width:10%"}).html(""),
                $('<td>').attr({class : 'filt-open', style : "width:10%"}).html("")
            )
            $('.filter-table thead').append(thead);
            for(x in data){
                var row = $('<tr>').append(
                    $('<td>').html(data[x] && data[x].Name),
                    $('<td>').attr({class : 'filter-edit',style : "text-align: center;color: green;cursor:pointer;font-size: 13px;", Id : data[x] && data[x].ID }).html('edit'),
                    $('<td>').attr({class : 'filter-delete', style : "text-align: center;color: red;cursor:pointer;font-size: 13px;", Id : data[x] && data[x].ID}).html('delete'),
                    $('<td>').attr({class : 'filter-downlod', style : "text-align: center;color: blue;cursor:pointer;font-size: 13px;", Id : data[x] && data[x].ID}).html('Download Result'),
                )
                $('.filter-table tbody').append(row);
                $('.filter-edit').off('click').on('click', function(){
                    var id  = $(this).attr("Id");
                    PageMethods.GetFilter(id,(d)=>{
                        if(d.Success){
                            var filterData = d.Data.Filters[0];
                            filterId = filterData.ID;
                            $('.filter-name').val(filterData.Name);
                            $('#txtFilterData').attr({query : filterData.Query, queryObject : filterData.QueryObj}).val(filterData.Query);

                        }
                    })
                });
                $('.filter-delete').off('click').on('click', function(){
                    var id  = $(this).attr("Id");
                    PageMethods.deleteFilter(id,(d)=>{
                        loadFilters();
                        if(!d.Success)
                            alert(d.ErrorMessage);
                    });    
                }); 
                $('.filter-downlod').off('click').on('click', function(){
                    var id  = $(this).attr("Id");
                    var url = location.pathname + "/exportParcels";
                    url += '?&s=' + id 
                    location.href = url;  
                });   
            }
        }

        function downloadParcelList(){
            var url = location.pathname + "/exportParcels";
            location.href = url;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Query Generator Test</h1>
    <div class = "container" style="display:flex">
        <div class = "qg_container">
            <div style="width:95%">
                <div style="padding:20px 0px;">
                    <span> Filter Name </span>
                    <input class= "filter-name" type= "text"/>
                </div>    
                <div style="display:flex;flex-direction:row;width:510px;justify-content: space-between;">
                    <div> Filter Condition </div>
                    <div><button class="button" onClick="openQG(); return false;">Open in Query Generator</button></div>
                    <div><button class="button" onClick="clearAll(); return false;">Clear All</button></div>
                </div>
                <textarea  id="txtFilterData" TextMode="MultiLine" Rows="10" style="width: 500px;"></textarea>
                <div>
                <button class="button" onClick="saveFilter();return false;">Save Filter </button></div>
            </div>
            <div class="filter-list">
                <table class= "filter-table">
                    <thead style = "font-size: 17px;font-weight: 600; text-align: center"></thead>
                    <tbody style="font-size:17px;"></tbody>
                </table>
            </div>
        </div>   
        <div class ="result_container">
            <div class="result_flex">
                <div class="result_count" style="margin-bottom:10px"></div>
                <div class="result_table"></div>
            </div>
        </div> 
    </div>        

        

    
</asp:Content>
<script runat="server">

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function saveFilter(name As String, query As String, queryObject As String, filterId As Integer, selectClause As String, joinClause As String, whereClause As String) As ActionResult(Of Boolean)
        Dim l As New ActionResult(Of Boolean)
        Try
            If filterId <> 0 Then
                Dim exist = Database.Tenant.GetIntegerValue("Select count(*) from QueryBuilderFilters where name = {0} AND ID <> {1}".SqlFormat(True,name,filterId))
                If exist > 0 Then
                    l.ErrorMessage = "Already exist filter with the same name."
                    l.Success = False
                Else
                    Database.Tenant.Execute("Update QueryBuilderFilters set Name = {0}, Query = {1}, QueryObj = {2}, SelectQuery = {4},JoinQuery = {5}, WhereQuery = {6} where ID = {3}".SqlFormat(True,name, query, queryObject, filterId, selectClause, joinClause,whereClause))
                    l.Success = True
                End If
            Else
                Dim exist = Database.Tenant.GetIntegerValue("Select count(*) from QueryBuilderFilters where name = {0}".SqlFormat(True,name))
                If exist > 0 Then
                    l.ErrorMessage = "Already exist filter with the same name."
                    l.Success = False
                Else
                    Database.Tenant.Execute("INSERT INTO QueryBuilderFilters(Name,Query,QueryObj,SelectQuery, JoinQuery, WhereQuery) Values({0},{1},{2},{3},{4},{5})".SqlFormat(True,name, query, queryObject,selectClause, joinClause,whereClause))
                    l.Success = True
                End If
            End If
        Catch ex As Exception
            l.ErrorMessage = ex.Message
            l.Success = False
        End Try
        'l.Query = q
        Return l
    End Function

     <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetAllFilters() As ActionResult(Of LoadInfo)
        Dim l As New ActionResult(Of LoadInfo)
        Try
            Dim li As New LoadInfo
            li.Filters = Database.Tenant.GetDataTable("SELECT * FROM QueryBuilderFilters").ConvertToList
            l.Data = li
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
            l.Success = False
        End Try
        'l.Query = q
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function deleteFilter(Id As Integer) As ActionResult(Of Boolean)
        Dim l As New ActionResult(Of Boolean)
        Try
             Database.Tenant.Execute("Delete from QueryBuilderFilters where ID = {0}".SqlFormat(True,Id))
             l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
            l.Success = False
        End Try
        'l.Query = q
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetFilter(Id as Integer) As ActionResult(Of LoadInfo)
        Dim l As New ActionResult(Of LoadInfo)
        Try
            Dim li As New LoadInfo
            li.Filters = Database.Tenant.GetDataTable("SELECT * FROM QueryBuilderFilters where Id = {0}".SqlFormat(True,Id)).ConvertToList
            l.Data = li
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
            l.Success = False
        End Try
        'l.Query = q
        Return l
    End Function


<WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetResult(query As String) As ActionResult(Of SearchResults)
        Dim l As New ActionResult(Of SearchResults)
        Try
            Dim li As New SearchResults
            Dim dt = Database.Tenant.GetDataSet(query)
            li.Parcels = dt.Tables(0).ConvertToList
            li.Count = dt.Tables(1).ConvertToList
            l.Data = li
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
            l.Success = False
        End Try
        'l.Query = q
        Return l
    End Function


    
    Public Class LoadInfo
        Public Property ApplicationSettings As JSONTable
        Public Property Filters As JSONTable
    End Class

    Public Class SearchResults
        Public Property Parcels As JSONTable
        Public Property Count As JSONTable
    End Class
</script>
