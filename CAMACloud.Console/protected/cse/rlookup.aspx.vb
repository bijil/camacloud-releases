﻿Imports CAMACloud.Data
Imports System.Data
Imports CAMACloud.BusinessLogic
Imports CAMACloud.BusinessLogic.Installation
Imports CAMACloud.BusinessLogic.Installation.EnvironmentConfiguration
Partial Class cse_rlookup
	Inherits System.Web.UI.Page
	Enum ViewMode
		List
		Edit
	End Enum

	Sub SwitchMode(mode As ViewMode)
		listPanel.Visible = mode = ViewMode.List
		editPanel.Visible = mode = ViewMode.Edit
	End Sub

	Sub LoadGrid()
		grid.DataSource = Database.Tenant.GetDataTable("SELECT DISTINCT GroupName FROM RangeLookup")
		grid.DataBind()
	End Sub

	Sub LoadLookup()
		editTitle.InnerHtml = "Edit Lookup - " + hdnGroup.Value
		editFactor.InnerHtml = "Increment Factor : " + hdnFactor.Value
		gvLookup.DataSource = Database.Tenant.GetDataTable("SELECT * FROM RangeLookup WHERE GroupName = " + hdnGroup.Value.ToSqlValue + " ORDER BY ValueFrom")
		gvLookup.DataBind()
		txtFrom.Text = Database.Tenant.GetStringValue("SELECT COALESCE(MAX(ValueTo),0) + " +  hdnFactor.Value + " FROM RangeLookup WHERE GroupName = " + hdnGroup.Value.ToSqlValue)
		txtTo.Text = ""
		txtValue.Text = ""
		Me.btnAddRange.Text = "Add Range"
	End Sub

	Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
		If Not IsPostBack Then
			LoadGrid()
			SwitchMode(ViewMode.List)
			hdnlookupid.Value = ""
		End If
	End Sub

Protected Sub btnCreate_Click(sender As Object, e As System.EventArgs) Handles btnCreate.Click
		hdnGroup.Value = txtNewLookupName.Text
		hdnFactor.Value = ddlFactor.SelectedValue
		If Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM RangeLookup WHERE GroupName = " + hdnGroup.Value.ToSqlValue) > 0 Then
			Alert("The entered Lookup name already exists.")
			Return
		End If		
		LoadLookup()
		SwitchMode(ViewMode.Edit)
		txtNewLookupName.Text = ""
	End Sub

	Protected Sub btnAddRange_Click(sender As Object, e As System.EventArgs) Handles btnAddRange.Click
		Dim vfrom, vto, value As Single
		Dim IsNewRange As Boolean = True
		Dim IsRowExist As Boolean = False
		If Single.TryParse(txtFrom.Text, vfrom) AndAlso Single.TryParse(txtTo.Text, vto) AndAlso Single.TryParse(txtValue.Text, value) Then
			If vfrom >= vto Then
				Alert("The 'To' value should not be less than or equal to 'From' value.")
				Return
			End If		
			For Each drow As DataRow In Database.Tenant.GetDataTable("SELECT * FROM RangeLookup WHERE GroupName = " + hdnGroup.Value.ToSqlValue + " ORDER BY ValueFrom").Rows
				If (vfrom > drow.Get("ValueFrom") And vto < drow.Get("ValueTo")) Then
					Alert("The entered values are already included in range : " + drow.Get("ValueFrom").ToString() + " and " + drow.Get("ValueTo").ToString())
					Return
				End If			
			Next			
			For Each dr As DataRow In Database.Tenant.GetDataTable("SELECT * FROM RangeLookup WHERE GroupName = " + hdnGroup.Value.ToSqlValue + " ORDER BY ValueFrom").Rows
				If (vfrom = dr.Get("ValueFrom")) AndAlso (vto >= dr.Get("ValueFrom")) AndAlso (value = dr.Get("RangeValue"))  Then
					IsRowExist = True
				End If			
				If (vfrom >= dr.Get("ValueFrom") And vfrom <= dr.Get("ValueTo")) Or (vto >= dr.Get("ValueFrom") And vto <= dr.Get("ValueTo")) Or (dr.Get("ValueFrom") >= vfrom And dr.Get("ValueFrom") <= vto) Or (dr.Get("ValueTo") >= vfrom And dr.Get("ValueTo") <= vto) Then
					IsNewRange = False
				End If												
				If value = dr.Get("RangeValue") And Not IsRowExist Then
					Alert("The entered value already exists for another range.")
					Return
				End If
			Next
			If IsNewRange Then
				Database.Tenant.Execute("INSERT INTO RangeLookup (GroupName, ValueFrom, ValueTo, RangeValue, IncrementFactor) VALUES ({0}, {1}, {2}, {3}, {4})".FormatString(hdnGroup.Value.ToSqlValue, vfrom, vto, value,  hdnFactor.Value.ToSqlValue))											
			Else
				Database.Tenant.Execute("EXEC Edit_RangeLookup @GRPNAME={0},@VFROM={1},@VTO={2}, @RVALUE = {3},@INCR_FACTOR = {4}".FormatString(hdnGroup.Value.ToSqlValue,vfrom, vto,value, hdnFactor.Value))
			End If
			LoadLookup()
		Else
			Alert("Only numeric values are allowed in the range values.")
		End If
		hdnlookupid.Value = ""			
	End Sub
	
	Protected Sub gvLookup_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvLookup.RowCommand
		Dim dt As New DataTable()
		If e.CommandName = "DeleteRange" Then
			Database.Tenant.Execute("DELETE FROM RangeLookup WHERE Id = " & e.CommandArgument)
			LoadLookup()
		ElseIf e.CommandName = "EditRange" Then
			hdnlookupid.Value = e.CommandArgument
			dt  = Database.Tenant.GetDataTable("SELECT * FROM RangeLookup WHERE GroupName = " + hdnGroup.Value.ToSqlValue + " AND Id = " + e.CommandArgument)
			txtFrom.Text = Convert.ToString(dt.Rows(0)("ValueFrom"))
			txtTo.Text = Convert.ToString(dt.Rows(0)("ValueTo"))
			txtValue.Text = Convert.ToString(dt.Rows(0)("RangeValue"))
			Me.btnAddRange.Text = "Update Range"			
		End If
	End Sub

	Protected Sub grid_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grid.RowCommand
		Select Case e.CommandName
			Case "EditLookup"
				hdnGroup.Value = e.CommandArgument
				hdnFactor.Value = Database.Tenant.GetStringValue("SELECT TOP 1 IncrementFactor FROM RangeLookup WHERE GroupName = " & e.CommandArgument.ToString.ToSqlValue)
				LoadLookup()
				SwitchMode(ViewMode.Edit)
			Case "DeleteLookup"
				Database.Tenant.Execute("DELETE FROM RangeLookup WHERE GroupName = " & e.CommandArgument.ToString.ToSqlValue)
				LoadGrid()
				SwitchMode(ViewMode.List)
		End Select
	End Sub

	Protected Sub lbReturn_Click(sender As Object, e As System.EventArgs) Handles lbReturn.Click
	    LoadGrid()
		SwitchMode(ViewMode.List)
	End Sub

	Protected Sub btnImport_Click(sender As Object, e As System.EventArgs) Handles btnImport.Click
		If importFile.HasFile Then
			Try
				Dim config = New EnvironmentConfiguration
				config.ImportSettings(importFile.FileContent, ConfigFileType.RangeLookups, Database.Tenant, Membership.GetUser().ToString)
				Alert("Imported successfully.")
				LoadGrid()
				SwitchMode(ViewMode.List)
			Catch ex As Exception
				Alert(ex.Message)
				RedirectforResubmission()
                Return
			End Try
		End If
	End Sub
	
	 Public Sub RedirectforResubmission()
       RunScript("RedirectPage();")
	 End Sub
	 
	Protected Sub lnkbtnExp_Click(sender As Object, e As System.EventArgs) Handles lnkbtnExp.Click
		Dim config = New EnvironmentConfiguration
    	config.ExportSettings( ConfigFileType.RangeLookups, Response, "RangeLookup", Database.Tenant)
	End Sub

End Class
