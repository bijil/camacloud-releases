﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/DataSetup.master" AutoEventWireup="false" Inherits="CAMACloud.Console.cse_rules" CodeBehind="rules.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function validate() {
            var valid = true;
            var count = 1;
            $('.mandatory').each(function () {
                if (valid)
                    if ($(this).val() == '') {
                        alert('All input fields are mandatory. Please enter suitable values before attempting to save.');
                        valid = false;
                        $(this).focus();
                        return false;
                    }
            });
			if (!valid){
							$('.mandatory').each(function () {
			                    if ($(this).val() == '') {                        
			                        $('.s'+count).show();
			                    }
			                    else 
			                    	$('.s'+count).hide();
			                    count++;
			            	});
			            }
            return valid;
        }

        function hideInfo() {
            $('.info').hide();
        }
    </script>
    <style type="text/css">
        h2 .rulename
        {
            color: #065a9f;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <h1 style="margin-bottom: 2px;">
        Comparable Sales - Rule Builder</h1>
    <div style="text-align: right; margin-right: 15px;">
        <asp:LinkButton runat="server" ID="lbShowHide" Text="Hide info messages" />
    </div>
    <asp:Literal runat="server" ID="lrHider" Visible="false">
		<style type="text/css">
		    .info
		    {
		        display: none;
		    }
		</style>
    </asp:Literal>
    <asp:Panel runat="server" ID="listPanel" Visible="false">
        <p class="info">
            Listed below are the rules which administer the qualification and ranking of neighborhood and group parcels with any subject parcels. You can add/edit/remove rules. The rules are built
            up with SQL compatible formula phrases, so you need to strictly follow the formats and specifications while writing each rule formula.
        </p>
        <p style="font-weight: bold;">
            Select Rule Group:
            <asp:DropDownList runat="server" ID="ddlGroup" AutoPostBack="true" onchange="showMask();">
                <asp:ListItem Text="Same Neighborhood Rules" Value="0"></asp:ListItem>
                <asp:ListItem Text="Group Rules" Value="1"></asp:ListItem>
            </asp:DropDownList>
        </p>
        <table style="width: 98%;" runat="server" id="lbTable">
                <tr>
                    <td>
                        <asp:LinkButton ID="lbCreateRule" runat="server" Text="Create new rule" Font-Bold="true" CssClass="normal" />
                    </td>
                    <td style="text-align: right;">
                        <asp:LinkButton ID="lbCopyToGroup" runat="server" Text="Copy to Group Rules" Font-Bold="true" OnClientClick="return confirm('Any group rules, if existing, will be deleted and replaced by the same neighborhood rules. Are you sure that you want to proceed?');" CssClass="normal" />
                    </td>
                </tr>
        </table>
        <asp:GridView runat="server" ID="gvRules" Width = "98%">
            <Columns>
                <asp:BoundField DataField="Name" HeaderText="Rule Name" ItemStyle-Width="150px" />
                <asp:TemplateField HeaderText="Fields Used">
                    <ItemStyle Width="280px" />
                    <ItemTemplate>
                        <%# EvalFields() %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Aggregate" ItemStyle-Width="90px">
                    <ItemTemplate>
                        <%# EvalAggregate()%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Depends On">
                    <ItemStyle Width="280px" />
                    <ItemTemplate>
                        <%# EvalRuleDependencies() %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="CreatedBy" HeaderText="Created By" ItemStyle-Width="120px" />
                <asp:TemplateField ItemStyle-Width="120px">
                    <ItemTemplate>
                        <asp:LinkButton runat="server" ID="lbEdit" Text="Edit" CommandName="EditRule" CommandArgument='<%# Eval("Id") %>' />
                        <asp:LinkButton runat="server" ID="lbDelete" Text="Delete" CommandName="DeleteRule" CommandArgument='<%# Eval("Id") %>' OnClientClick="return confirm('Are your sure to permanently delete this rule?');" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                <p>
                    There are no rules available in this group. Click
                    <asp:LinkButton runat="server" Text="here" Font-Bold="true" CssClass="normal" OnClick="lbCreateRule_Click" />
                    to create a new rule.</p>
            </EmptyDataTemplate>
        </asp:GridView>
        <h3>
            Other options:</h3>
        <p>
            <asp:LinkButton runat="server" ID="lbRecompile" Text="Recompile all rules" /></p>
    </asp:Panel>
    <asp:Panel runat="server" ID="editPanel">
        <h2 runat="server" id="editTitle">
            Edit Rule</h2>
        <p class="buttonpara buttonpara-top">
            <asp:Button runat="server" ID="btnSave2" Text="Save" Font-Bold="true" OnClientClick="return validate();" />
            <asp:Button runat="server" ID="btnCancel2" Text="Return to List" />
        </p>
        <asp:HiddenField runat="server" ID="hdnRuleId" Value="" />
        <asp:HiddenField runat="server" ID="hdnUseLookup" Value="0" />
        <asp:HiddenField runat="server" ID="hdnUseRangeLookup" Value="0" />
        <asp:HiddenField runat="server" ID="hdnLookupName" Value="0" />
        <table>
            <tr>
                <td style="width: 140px;">Type: </td>
                <td>
                    <asp:Label runat="server" ID="lblRuleType" />
                </td>
            </tr>
            <tr>
                <td style="width: 140px;">Rule Name: </td>
                <td>
                    <asp:TextBox runat="server" ID="txtName" Width="250px" MaxLength="50" CssClass="mandatory" />
                    <span style="font-size: 13px; color: red; font-weight: bold; display:none;" class="s1"> * </span>
                </td>
            </tr>
            <tr>
                <td style="width: 140px;">Source Table for Data: </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlSourceTable" Width="250px" />
                </td>
            </tr>
            <tr class="info">
                <td colspan="2">
                    <h3>
                        Field Specification:</h3>
                    <p class="info">
                        Provide the comparable field here. This could be a single field to be compared, or a valid formula combining more than one fields. For example: <strong>NBHDNO</strong>, <strong>YEARBLT</strong>,
                        <strong>2012 - YEARBLT</strong>, <strong>FULLBATH + HALFBATH</strong>
                        <br />
                        <br />
                        In case of conditional evalations, use <strong>WHEN <i>condition1</i> THEN <i>result1</i> WHEN <i>condition2</i> THEN <i>result2</i> .... ELSE <i>defaultResult</i></strong>.
                        <br />
                        <br />
                        For example: WHEN 2012 - YEARBLT > 0 THEN 2012 - YEARBLT ELSE 0</p>
                </td>
            </tr>
            <tr>
                <td style="width: 140px;">Field Specification: </td>
                <td>
                    <asp:TextBox runat="server" ID="txtField" Width="400px" MaxLength="100" CssClass="mandatory" TextMode="MultiLine" Rows="3" />
                    <span style="font-size: 13px; color: red; font-weight: bold; display:none;" class="s2"> * </span>
                </td>
            </tr>
            <tr>
                <td style="width: 140px;">Aggregate Function: </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlAggregate" Width="100px">
                        <asp:ListItem Text="-- None --" Value="" />
                        <asp:ListItem Text="Average" Value="AVG" />
                        <asp:ListItem Text="Sum" Value="SUM" />
                        <asp:ListItem Text="Minimum" Value="MIN" />
                        <asp:ListItem Text="Maximum" Value="MAX" />
                        <asp:ListItem Text="(Inline)" Value="INLINE" />
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="width: 140px;">Use Lookup: </td>
                <td>
                    <asp:RadioButtonList runat="server" ID="rblLookup" RepeatDirection="Horizontal" Width="400px" AutoPostBack="true">
                        <asp:ListItem Text="None" Value="0" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Lookup Table" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Range Lookup" Value="2"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:CheckBox runat="server" ID="chkUseZeroIfNull" Text="Use Zero if any field in the specification has NULL value." />
                </td>
            </tr>
            <tr runat="server" id="trLookupTable" visible="false">
                <td>Lookup Table:</td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlLookupTable" Width="250px" CssClass="mandatory" />
                    <span style="font-size: 13px; color: red; font-weight: bold; display:none;" class="s"> * </span>
                </td>
            </tr>
            <tr class="info">
                <td colspan="2">
                    <h3>
                        Rank Formula (Calculation):</h3>
                    <p class="info">
                        Provide the formula for calculating the rank value. In this you need to compare subject parcels and other parcel.
                    <br /><br />
                        The results of above field specification from both parcels will be used. In your formula use <strong>@SUBJ</strong> to denote field/formula value of subject parcel, and <strong>@COMP</strong>
                        to denote other parcel being compared. You can use SQL compatible mathematical functions like <strong>ABS</strong>, <strong>ROUND</strong>, <strong>SGN</strong>, etc.</p>
                </td>
            </tr>
            <tr>
                <td style="width: 140px;">Rank Formula: </td>
                <td>
                    <asp:TextBox runat="server" ID="txtRankFormula" Width="400px" TextMode="MultiLine" Rows="3" CssClass="mandatory" />
                    <span style="font-size: 13px; color: red; font-weight: bold; display:none;" class="s3"> * </span>
                </td>
            </tr>
            <tr class="info">
                <td colspan="2">
                    <h3>
                        Rule Formula (Logical Pass/Fail):</h3>
                    <p class="info">
                        Provide the formula for assessing success of the rule. This is a logical evaluation comparing the rule result with a constant coefficient.
                    <br /><br />
                        The results of field specification from both parcels can be used, along with the rank evaluation from the Rank Formula above. In your formula use <strong>@RANK</strong> to use the
                        above rank value along with <strong>@SUBJ</strong> and <strong>@COMP</strong> as before. The rule formula should be using logical comparison operators like <strong>=</strong>, <strong>
                            &gt;</strong>, <strong>&lt;</strong>, <strong>&gt;=</strong>, <strong>&lt;=</strong>, <strong>&lt;&gt;</strong> (inequality), as well as connect multiple conditions with <strong>AND</strong>
                        and <strong>OR</strong>
                    </p>
                </td>
            </tr>
            <tr>
                <td style="width: 140px;">Rule Formula: </td>
                <td>
                    <asp:TextBox runat="server" ID="txtRuleFormula" Width="400px" TextMode="MultiLine" Rows="3" CssClass="mandatory" />
                    <span style="font-size: 13px; color: red; font-weight: bold; display:none;" class="s4"> * </span>
                </td>
            </tr>
            <tr class="info">
                <td colspan="2">
                    <h3>
                        Rank Percentile Formulas for PASS/FAIL</h3>
                    <p class="info">
                        Provide two different calculation formulas in case of PASS and FAIL.
                    <br /><br />
                        The formulas can use same variables <strong>@RANK</strong>, <strong>@SUBJ</strong>, <strong>@COMP</strong>, and should produce a calculated value upon evaulation.</p>
                </td>
            </tr>
            <tr>
                <td style="width: 180px;">Rank Percentile if PASS: </td>
                <td>
                    <asp:TextBox runat="server" ID="txtPercentPass" Width="400px" TextMode="MultiLine" Rows="2" CssClass="mandatory" />
                    <span style="font-size: 13px; color: red; font-weight: bold; display:none;" class="s5"> * </span>
                </td>
            </tr>
            <tr>
                <td style="width: 180px;">Rank Percentile if FAIL: </td>
                <td>
                    <asp:TextBox runat="server" ID="txtPercentFail" Width="400px" TextMode="MultiLine" Rows="2" CssClass="mandatory" />
                    <span style="font-size: 13px; color: red; font-weight: bold; display:none;" class="s6"> * </span>
                </td>
            </tr>
        </table>
        <p class="buttonpara">
            <asp:Button runat="server" ID="btnSave" Text="Save" Font-Bold="true" OnClientClick="return validate();" />
            <asp:Button runat="server" ID="btnCancel" Text="Return to List" />
        </p>
    </asp:Panel>
</asp:Content>
