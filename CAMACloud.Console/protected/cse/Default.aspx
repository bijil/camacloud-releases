﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/CompSales.master" AutoEventWireup="false" Inherits="CAMACloud.Console.cse_Default" CodeBehind="Default.aspx.vb" %>

<%@ Import Namespace="CAMACloud" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyC7e5YrZlKqWtaY4ehsDwMgCis4IfNs7Xw"></script>
<script src="../../App_Static/js/jslib/jquery.js"> </script>
<script src="../../App_Static/js/jslib/tipr.js"> </script>

    <style type="text/css">
        .parcel-search
        {
        }
        
        .parcel-search td
        {
            vertical-align: top;
        }
        
        .parcel-search td.fname
        {
            vertical-align: middle;
            padding-right: 30px;
        }
        
        .parcel-search td.finput
        {
        }
        
        .parcel-search td.finput input[type="text"]
        {
            width: 200px;
        }
        
        .parcel-details table tr td
        {
        }
        
        .imgover .img2
        {
            display: none;
        }
        
        .imgover:hover .img2
        {
            display: block;
        }
        .auto-style1 {
            width: 262px;
        }
        .LabelPanel{
        	float: right;
    		margin: 3% 21% 0% 0%;
        }
        .gm-style-iw-d
        {
        	overflow: hidden!important;
        	padding: 4px 9px 10px 0px !important;
        }
    </style>
    
    <script type="text/javascript">
     window.onload = function () {
     //initGoogleMap();
        }

function disableclick() {
	$("#MainContent_MainContent_chkMatched").prop("disabled", true);
	$("#MainContent_MainContent_chkGroup").prop("disabled", true);
	$("#MainContent_MainContent_chkSale").prop("disabled", true);
	$("#MainContent_MainContent_chkEstSalePrice").prop("disabled", true);
	$("#MainContent_MainContent_txtKeyValue1").prop("disabled", true);
	$("#MainContent_MainContent_btnSearch").prop("disabled", true);
}    
function enableclick() {
	$("#MainContent_MainContent_chkMatched").prop("disabled", false);
	$("#MainContent_MainContent_chkGroup").prop("disabled", false);
	$("#MainContent_MainContent_chkSale").prop("disabled", false);
	$("#MainContent_MainContent_chkEstSalePrice").prop("disabled", false);
	$("#MainContent_MainContent_txtKeyValue1").prop("disabled", false);
	$("#MainContent_MainContent_btnSearch").prop("disabled", false);
}
  function initGoogleMap(callback) {
    mapZoom = 5;
    mapPosition = new google.maps.LatLng(42.345573, -71.098326);
    mapCenter = new google.maps.LatLng(35.56, -96.84);
    var mapOptions = {
        zoom: mapZoom,
        minZoom: 7,
        mapTypeId: google.maps.MapTypeId.HYBRID,
        center: mapCenter,gestureHandling: 'greedy'
    };
    $('.gmap').height(460);
    $('.gmap').width('79%');
    map = new google.maps.Map(document.getElementById('gmap'), mapOptions);
      console.log('map initialized');
}
function showParcelsInGrid(){
	$('.mapview').hide();
    $('.results').show();
    $('.checkSpan').hide();
	$('.saleSpan').hide();
     OpenInMap = false;
}
function hideprogressMsg(){
	setTimeout(function() { alert("No parcels found matching your search criteria"); }, 100);
 }

function hideInvalidparcelMsg() {
     alert("Please select a valid parcel..."); 
}

function clearMarkers(){
 if (mapmarkers) {
            var keys = Object.keys(mapmarkers);
            for (x in keys) {
                if (mapmarkers[keys[x]].setMap) {
                    mapmarkers[keys[x]].setMap(null);
                    mapmarkers[keys[x]] = null;
                }
            }
            mapmarkers = [];
        }
     //   info.close();
}

function setMapView(showWindow,callback){
	showParcelsInMap(showWindow,function(){showWindow,setMapCentre(function(){if(callback) {callback();}})});
	$('.results').show();
	//return ;
}

window.onload=function(){
var button = document.getElementById("debounce"); 
const debounce = (func, delay) => { 
    let debounceTimer 
    return function() { 
        const context = this
        const args = arguments 
            clearTimeout(debounceTimer) 
                debounceTimer 
            = setTimeout(() => func.apply(context, args), delay) 
    } 
}
// New event listener for show labels
button.addEventListener('click', debounce(function() { 
        $('.results').show();
        showWindow=$('.showInfo').is(":checked");              
        console.log(showWindow);
        if (check) return;
        check=true;
        if(showWindow){
		firstLoad = true;
        
		$('.saleSpan').show();
        $('.showQualifiedSales').prop('checked', false);


		setMapView(showWindow,function(){
		parcelsInMap.forEach(function(e){if(e.addToCompare==true){
			mapmarkers[e.ParcelId].setIcon(icon[1])}
			});
		check=false;
	})
	}
		else {
			$('.saleSpan').hide();
			$('#QualifiedSale').prop('checked',false);
			infoWindows.forEach(function(i) {i.close();});
			check=false;
			console.log('label invisible');}		
                        }, 2000)); 
               
    $('#QualifiedSale').change(()=>{
		if($('#QualifiedSale').prop('checked') == true)
			$('.salesDateInfo').show();
		else
			$('.salesDateInfo').hide();		
	});

}



/* Previous Showlabels function 
function checkChanged(){
	$('.results').show();
	showWindow = $('.showInfo').is(":checked");
	if (check) return;
	check=true;
	if(showWindow){
		firstLoad = true;
		setMapView(showWindow,function(){
		parcelsInMap.forEach(function(e){if(e.addToCompare==true){
			mapmarkers[e.ParcelId].setIcon(icon[1])}
			});
		check=false;
	})
	}
	else {
	infoWindows.forEach(function(i) {i.close();});
	check=false;
     }
	} 
	*/
    
function setMapCentre(callback){
 	if(map && bounds ){
	 	map.setCenter(bounds.getCenter())
     	map.fitBounds(bounds);
    	}
    	$('.checkSpan').show();
	if(callback) callback();
 }
   var icon = ["/App_Themes/Cloud/images/parcelUncheck.png",
                "/App_Themes/Cloud/images/parcelCheck.png",
                "/App_Themes/Cloud/images/subParcel.png"
                ];
var parcelsInMap = [];
var mapmarkers=[];
var OpenInMap = false;
var firstLoad = true;
var bounds = new google.maps.LatLngBounds();
var mapcentre=new google.maps.LatLngBounds();
var infoWindows=[];
var showWindow=false;
var check=false;
function showParcelsInMap(showWindow,callback) {
	var subjectId= $('#<%= PID.ClientID%>').val();
	var parcelids= $('#<%= hdnParcelIdList.ClientID%>').val();
	OpenInMap = true;
    $('.mapview').show();
    $('.results').hide();
    $('.saleSpan')[$('.showInfo').is(":checked") ? 'show': 'hide']();
	if(!firstLoad){
		setMapCentre();
        return;
	}    
    clearMarkers();
    parcelsInMap = [];
    initGoogleMap();
    $.ajax({
        type: "POST",
        url: "/parcels/getcomparablesmappoints.jrq",
        dataType: 'json',
        data:{pids:parcelids},
        success: function(res) {
        if(res.length < 1)
        return;
           
       function setMarker(parcelsInMap,callback){
             for (x in parcelsInMap) {
                parcel = parcelsInMap[x];
                var b=new google.maps.LatLngBounds();
                for (i in parcel.Points) {
                    pstr = parcel.Points[i].split(',');
                    loc = new google.maps.LatLng(parseFloat(pstr[0].replace('[', '')), parseFloat(pstr[1].replace('[', '')));
                    bounds.extend(loc);
                    b.extend(loc)
                }
                var parcelLoc=new google.maps.LatLng(b.getCenter().lat(), b.getCenter().lng());
                if(parcel.isSubject=='1') mapcentre = parcelLoc;
                if(mapmarkers[parcel.ParcelId.toString()]==null){
                 	mapmarkers[parcel.ParcelId.toString()] = new google.maps.Marker({
                    	position: parcelLoc,
                  	 	map: map,
                    	icon: parcel.isSubject == '0'? icon[0] : icon[2],
                    	animation:google.maps.Animation.DROP
                });
                mapmarkers[parcel.ParcelId.toString()].pid = parcel.ParcelId;
                mapmarkers[parcel.ParcelId.toString()].Keyvalue1 = parcel.Keyvalue1;
                var info = new google.maps.InfoWindow();
                if(showWindow){
                var saleDate = parcel.LastQualifiedSale?parcel.LastQualifiedSale.toString() : 'NA'
                var compPerc = parcel.CompPercent ? `<b>Comp%: </b>${parcel.CompPercent * 100}%</br>` : ''
                info.setContent('<b>'+'parcel_id:'+'</b>'+parcel.Keyvalue1.toString()+'</br>'+ compPerc + '<span class = "salesDateInfo" style="display:none;"><b> Qualified Sale Date: </b>'+saleDate+'</span>');
            	info.open(map,mapmarkers[parcel.ParcelId.toString()]);
				infoWindows.push(info)
				}
                google.maps.event.addListener(mapmarkers[parcel.ParcelId.toString()], 'click', (function(e) {
                  var e = this;
                  var pdata = parcelsInMap[this.pid]
	                   // info.close();          		
                    if(pdata.isSubject!="0"){
                    return;
                    }
                    if(!pdata.addToCompare){
                      if (! validateSelection()) return;
                      mapmarkers[pdata.ParcelId.toString()].setIcon(icon[1]); 
                      pdata.addToCompare = true;
                      }
                      else{
                      mapmarkers[pdata.ParcelId.toString()].setIcon(icon[0]); 
                      pdata.addToCompare = false;
                      }
                }));


                    var index1 = 0;

                    for (var key in mapmarkers) {
                        if (mapmarkers.hasOwnProperty(key)) {
                            index1++;
                        }
                    }
                    google.maps.event.addListener(info, 'closeclick', function () {

                        info.close();
                        index1--;
                        if (index1 <= 1) {
                            // Execute statement when index is 1 or 0
                            $('.showInfo').prop('checked', false);
                            $('.showQualifiedSales').prop('checked', false);
                        }
                    });

            }
            if(callback) callback();
         }}

            res = res.filter((x) => { return x.SubjectPId == subjectId || x.ParcelId == subjectId })
            parcelsIn = res;
            var parcels = [];
            var parcel;
            var infowindow = new google.maps.InfoWindow();
            for (x in res) {
                var point = res[x];
                 point.ParcelId.toString() ==  subjectId ? point.isSubject='1' : point.isSubject='0'
                var pid = point.ParcelId.toString();
                if (parcelsInMap[pid] == null)
                    parcelsInMap[pid] = {
                        ParcelId: point.ParcelId,
                        Keyvalue1:point.Keyvalue1,
                        addToCompare:false,
                        LastQualifiedSale : point.LastQualifiedSale,
                        isSubject:point.isSubject,
                        CompPercent : point.CompPercent,
                        Points:[],
                    };
                 parcelsInMap[pid].Points.push(point.Points);
            }
            var totalParcels = Object.keys(parcelsInMap).length;
           setMarker(parcelsInMap,function(){
           		//setMapCentre();
           } ); 
	     	firstLoad=false;
	       if(callback)  callback();
        },
        error: function(response) {
            var test = response;
        }
    });
}				
function hideElements(){
			 $('.mapview').hide();
			 $('.results').hide();
			 $('.checkSpan').hide();
			 $('.saleSpan').hide();
			 $('.showInfo') .prop('checked', false);
}
  function showResultgrid(){
       OpenInMap = false;
       	$('.results').show();
		$('.mapview').hide();
		$('.checkSpan').hide();
		$('.saleSpan').hide();
       }
		function onSearchProgress() {
			firstLoad = true;
			hideElements();
		}
		function ParcelSearchProgress() {
			$('.parcel-details').hide();
			onSearchProgress();
		}
		function disableButtons() {
			$('input[type="submit"]').attr('disabled', 'disabled');
		}
		function searchParcel(){
			 hideElements();
		}
	function validateSelections() {
		if (OpenInMap) {
            var count = parcelsInMap.filter(function(x){return x.addToCompare==true;}).length;
            if(count==0 && !$('.chkEstSalePrice input').is(':checked')) {
                alert('You must select at least one parcel to Generate the Report.');
                    return false;
            }
            if($('.chkEstSalePrice input').is(':checked') && count < 4 )
                {
                    alert('You must select at least 4 parcel to compare with Estimate sale price.');
                    return false;
                }
            var parcelList=[]
            parcelsInMap.filter(function(x){return (x.addToCompare==true)}).forEach(function(x){parcelList.push(x.ParcelId)});
            $('#<%= hdnParcelIds.ClientID%>').val(parcelList.join(','));
            $('#<%= hdnviewType.ClientID%>').val( 'Map');
            return true;
		}
		else{
			if ($(':checked', $('.results')).length == 0 && !$('.chkEstSalePrice input').is(':checked')) {
                alert('You must select at least one parcel to Generate the Report.');
				return false;
			}
			if($('.chkEstSalePrice input').is(':checked') && $(':checked', $('.results')).length < 4  )
			{
			    alert('You must select at least 4 parcel to compare with Estimate sale price.');
			    return false;
			}
			return true;
			}
		}

	function validateSelection(cb) {
		if (OpenInMap) {
		var count = parcelsInMap.filter(function(x){return x.addToCompare==true;}).length + 1;
		if (count > <%= MaxCompCount %>){
		alert('You cannot make more than <%= MaxCompCount %> selections.');
		return false;
		}
		else return true;
		}
		else{
		if ($(':checked', $('.results')).length > <%= MaxCompCount %>) {
				cb.checked = false;
				alert('You cannot make more than <%= MaxCompCount %> selections.');
			}
		}
		}
	function isAlphaNumericKey(evt) {
    		var charCode = evt.keyCode
    		if ( charCode == 39 || charCode == 60 ) {
        		return false;
    		}    		
    		return true;
        }

        function validateInput(event) {
            var element = event.target;
            var inputValue = element.value;
            if (inputValue.includes("'")) {
                element.value = inputValue.replace(/'/g, '');
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
 
    <h1>
        Search for Comparable Properties</h1>
    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="parcel-search">
                <table style="width: 100%;">
                    <tr>
                        <td style="width: 500px;">
                            <table>
                                <tr>
                                    <td class="fname">
                                        <%=If(CAMACloud.Data.Database.Tenant.Application.AlternateKeyLabelfield, CAMACloud.Data.Database.Tenant.Application.KeyField(1))%>: </td>
                                    <td class="finput">
                                        <asp:TextBox runat="server" ID="txtKeyValue1" onkeypress="return isAlphaNumericKey(event)"  oninput="validateInput(event)" />

                                    </td>
                                    <td>
                                        <asp:Button runat="server" ID="btnSearch" Text="  Search Subject  " ValidationGroup="Search" onClientclick='ParcelSearchProgress();' />
                                    </td>
                                </tr>
                                <tr runat="server" id="trKeyField2">
                                    <td class="fname">
                                        <%=CAMACloud.Data.Database.Tenant.Application.KeyField(2)%>: </td>
                                    <td class="finput">
                                        <asp:TextBox runat="server" ID="txtKeyValue2" onkeypress="return isAlphaNumericKey(event)" oninput="validateInput(event)" />
                                    </td>
                                    <td></td>
                                </tr>
                                <tr runat="server" id="trKeyField3">
                                    <td class="fname">
                                        <%=CAMACloud.Data.Database.Tenant.Application.KeyField(3)%>: </td>
                                    <td class="finput">
                                        <asp:TextBox runat="server" ID="txtKeyValue3" />
                                    </td>
                                    <td></td>
                                </tr>
                            </table>
                        </td>
                        <td runat="server" id="parcelDetails" class="parcel-details">
                            <asp:HiddenField runat="server" ID="PID" />
                            <div style="font-weight: bold; font-size: larger;">
                                Address:
                                <asp:Label runat="server" ID="lblStreetAddress" /></div>
                            <table style="width: 90%; border-spacing: 0px;inline-size:auto;" cellspacing="0" cellpadding="3">
                              
                                <tr>
                                    
                                    <td style="padding-left:7px"> <%=If(CAMACloud.Data.Database.Tenant.Application.AlternateKeyLabelfield, CAMACloud.Data.Database.Tenant.Application.KeyField(1))%>:
                                        <asp:Label runat="server" ID="lblKV1" /></td>
                                    <td style="padding-left:7px" > <span runat="server" id="resKeyField2">
                                            <%=CAMACloud.Data.Database.Tenant.Application.KeyField(2)%>:
                                            <asp:Label ID="lblKV2" runat="server" />
                                        </span><span runat="server" id="resKeyField3">
                                            <asp:Label ID="lblKV3" runat="server" /> &nbsp;:
                                            <%=CAMACloud.Data.Database.Tenant.Application.KeyField(3) %>
                                        </span></td>
                                </tr>
                                <tr>
                                    <td class="auto-style1" style="padding-left:7px">Neighborhood:
                                        <asp:Label ID="lblNbhd" runat="server" />
                                    </td>
                                    <td style="padding-left:7px" >Group:
                                        <asp:Label ID="lblGroup" runat="server" />
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style1">
                                        <asp:CheckBox ID="chkMatched" runat="server" CssClass="chkMatched" AutoPostBack="true" OnCheckedChanged="chkEstSalePrice_CheckedChanged"  Text="Search Only for Absolute Comparables" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkGroup" runat="server" CssClass="chkGroup" AutoPostBack="true"  OnCheckedChanged="chkEstSalePrice_CheckedChanged"  Text="Search within Group" />
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style1">
                                        <asp:CheckBox ID="chkSale" runat="server" AutoPostBack="true" OnCheckedChanged="chkEstSalePrice_CheckedChanged"  CssClass="chkSale tip" abbr="In order for an Estimated Sale Price to be calculated, there must be comparables that are 80%+ comparable." Text="List comparables with at least one sale" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkEstSalePrice" runat="server" AutoPostBack="true" OnCheckedChanged="chkEstSalePrice_CheckedChanged" abbr="In order for an Estimated Sale Price to be calculated, there must be comparables that are 80%+ comparable." CssClass="chkEstSalePrice tip"  Text="Estimate Subject's Sale Price" />
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                               
                          </table>
                            <div style="padding-top:15px">
                                <asp:HiddenField runat="server" ID="hdnRecalc" Value="1" />
                                <asp:Button runat="server" ID="btnSearchComparables" Text="  Search Comparables " Font-Bold="true" OnClientClick='onSearchProgress();disableclick();' />
                                <asp:Button runat="server" ID="btnRecalculate" Text="  Recalculate " Visible="false" OnClientClick='onSearchProgress();' />
                                <asp:Button runat="server" ID="btnRefresh" Text="  Refresh " OnClientClick='onSearchProgress();' Visible="false" />
                            </div>
                        </td>
                    </tr>
                </table>
                <asp:GridView runat="server" ID="gvMultiParcel">
                    <Columns>
                        <asp:BoundField HeaderText="Group" DataField="GroupNo" ItemStyle-Width="40px" />
                        <asp:BoundField HeaderText="Nbhd" DataField="NbhdNo" ItemStyle-Width="80px" />
                        <asp:BoundField HeaderText="KeyValue1" DataField="KeyValue1" ItemStyle-Width="130px" />
                        <asp:BoundField HeaderText="KeyValue2" DataField="KeyValue2" ItemStyle-Width="130px" />
                        <asp:BoundField HeaderText="KeyValue3" DataField="KeyValue3" ItemStyle-Width="130px" />
                        <asp:BoundField HeaderText="Street Address" DataField="StreetAddress" ItemStyle-Width="240px" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton runat="server" ID="lbSelect" Text="Select" CommandName='OpenParcel' CommandArgument='<%# Eval("Id") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearchComparables" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdateProgress runat="server">
        <ProgressTemplate>
            <div style="font-weight: bold;">
            	Loading data, please wait ...</div>
        </ProgressTemplate>
    </asp:UpdateProgress> 
    <span class= "LabelPanel">
    	<span style="display:none;padding:5px;" class="checkSpan"><input type="checkbox" class="showInfo" id="debounce" > Show Labels</span>
    	<span style="display:none;padding:5px;" class="saleSpan"><input type="checkbox" class="showQualifiedSales" id="QualifiedSale" > Show Qualified Sale Info </span>
    </span>
    <asp:UpdatePanel runat="server" ID="updResults" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="results" runat="server" visible="false" id="divResults" style="display: inline-block;">
                <table style="border-spacing: 0px; width: 100%; margin-top: 10px;" class="abc">
                    <tr>
                        <td style="vertical-align: bottom; font-weight: bold;">
                            <asp:Button ID="btnGenerateReport"  runat="server" Font-Bold="true" OnClientClick="return validateSelections();" Text="  Generate report for selected parcels "  style="margin-bottom:10px"/>
                           <br />
                            <asp:Label runat="server" ID="lblResult" />  <asp:LinkButton ID="btnMapview" runat="server"  cssClass="parcelsMapView" OnClientClick="setMapView()" style="margin-left: 15px;" >Show In Map</asp:LinkButton>
							  <asp:LinkButton ID="btnGridview" cssClass="listView" runat="server" OnClientClick="showParcelsInGrid()" style="margin-left: 15px;">Show In List</asp:LinkButton></td>
                            <td> </td>
                            <td style="vertical-align: bottom; font-weight: bold;">
                             <asp:HiddenField runat="server" ID="hdnviewType" value="grid" />
							<asp:HiddenField  runat="server" ID="hdnParcelIds" value="" />
							<asp:HiddenField  runat="server" ID="hdnParcelIdList" value="" />
                        <td style="text-align: right;">
                            &nbsp;</td>
                
                    </tr>
                </table>

                <asp:GridView runat="server" ID="gvc" class="comparableGrid" style="margin-bottom: 100px;">
                    <Columns>
                        <asp:TemplateField>
                            <ItemStyle Width="20px" />
                            <ItemTemplate>
                                <asp:HiddenField runat="server" ID="PID" Value='<%# Eval("ID") %>' />
                                <asp:CheckBox runat="server" ID="Selected" CssClass='select-to-report' onclick='validateSelection(this)' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemStyle Width="45px" HorizontalAlign="Center" />
                            <ItemTemplate>
                                <%# EvalCompPhoto() %>
                                <%--<a class="imgover" href="#">
                                    <img style='width: 40px; height: 20px;' src='<%# Eval("ImagePath") %>' />
                                    <img style='width: 210px; height: 100px; position: absolute;' class="img2" src='<%# Eval("ImagePath") %>' />
                                </a>--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Group" DataField="GroupNo" ItemStyle-Width="40px" />
                        <asp:BoundField HeaderText="Nbhd" DataField="NbhdNo" ItemStyle-Width="60px" />
                        <asp:BoundField HeaderText="KeyValue1" DataField="KeyValue1" ItemStyle-Width="200px" />
                        <asp:BoundField HeaderText="KeyValue2" DataField="KeyValue2" ItemStyle-Width="100px" />
                        <asp:BoundField HeaderText="KeyValue3" DataField="KeyValue3" ItemStyle-Width="100px" />
                        <asp:BoundField HeaderText="Street Address" DataField="StreetAddress" ItemStyle-Width="240px" />
                        <asp:BoundField HeaderText="Comp%" DataField="Percentage" ItemStyle-Width="80px" />
                        <asp:BoundField HeaderText="Latest Qualified Sale" DataField="LastQualifiedSale" ItemStyle-Width="200px" DataFormatString="{0:MM-dd-yyyy hh:mm tt}" />
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        <p>
                            No comparable properties are found for the parcel with the current options.
                        </p>
                    </EmptyDataTemplate>
                </asp:GridView>
                </div>

        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearchComparables" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnRecalculate" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnRefresh" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
     <div class="mapview" Id = "mapview" runat="server" style="margin-top: 12px; margin-bottom: 9px; margin-left: 5px; display:none; ">
           <div class="gmap" id="gmap" >
                            
          </div>
   </div>
</asp:Content>
