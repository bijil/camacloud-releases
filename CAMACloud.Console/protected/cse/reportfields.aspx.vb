﻿Imports CAMACloud.BusinessLogic
Imports CAMACloud.BusinessLogic.Installation

Public Class _new
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            txtNewCategory.Text = ""
            loadfields()
            LoadCategories()
            Dim dtInputTypes As DataTable = Database.System.GetDataTable("SELECT * FROM GlobalFieldInputTypes")
            Dim dtLookupSource As DataTable = Database.Tenant.GetDataTable("SELECT * FROM cc_LookupSources")
            ddl_datatype.FillFromTable(dtInputTypes)
            ddl_lookuptable.FillFromTable(dtLookupSource)
            ddl_lookuptable.AddItem("[Custom Query]", "$QUERY$")
        End If
    End Sub
    Protected Sub loadfields()
        'rpFields.DataSource = Database.Tenant.GetDataTable("SELECT f.Id, AssignedName as name,f.CategoryId,TableId as FieldTableId,cf.CategoryId as reportcategoryid,DisplayLabel,DataType FROM DataSourceField f INNER JOIN DataSourceTable t ON (f.TableId = t.Id) left outer join cse.ComparableFields cf on cf.fieldid=f.Id WHERE t.ImportType IN (0,1,3) AND f.CategoryId IS NOT NULL AND cf.FieldType NOT IN(2,3,4) ORDER BY AssignedName")
        rpFields.DataSource = Database.Tenant.GetDataTable("SELECT f.Id, AssignedName as name,f.CategoryId,TableId as FieldTableId,cf.CategoryId as reportcategoryid,DisplayLabel,DataType, t.Name As TableName, ISNULL(cf.LookupTableOverride,ISNULL(f.lookupTable,'$QUERY$')) As LookupTable FROM DataSourceField f INNER JOIN DataSourceTable t ON (f.TableId = t.Id) left outer join cse.ComparableFields cf on cf.fieldid=f.Id WHERE t.ImportType IN (0,1,3) AND f.CategoryId IS NOT NULL ORDER BY t.ImportType, t.Name,  AssignedName")
        rpFields.DataBind()
    End Sub
    Public Function MasterFieldClass() As String
        Dim catId = Eval("reportcategoryid")
        If catId IsNot DBNull.Value AndAlso catId.ToString <> "" Then
            Return "cat-added"
        Else
            Return ""
        End If
    End Function
    Protected Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        If IsPostBack Then
            RunScript("finalizePage();")
        End If
    End Sub
    Sub LoadCategories()
        rpCategories.DataSource = Database.Tenant.GetDataTable("SELECT Id, Category as name FROM cse.FieldCategory ORDER BY Ordinal")
        rpCategories.DataBind()
    End Sub

    Protected Sub rpCategories_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpCategories.ItemDataBound
        Dim cid As HiddenField = e.Item.FindControl("CatId")
        Dim rpFields As Repeater = e.Item.FindControl("rpFields")
        rpFields.DataSource = Database.Tenant.GetDataTable("SELECT cf.categoryid as reportcategory ,cf.id as cseID ,sf.id as fieldorgid, ISNULL(cf.LookupTableOverride,ISNULL(sf.lookupTable,'$QUERY$')) As LookupTable, * FROM cse.ComparableFields cf inner join dbo.DataSourceField sf on cf.fieldid=sf.id WHERE cf.CategoryId = " + cid.Value + "  ORDER BY Ordinal")
        rpFields.DataBind()
    End Sub

    Protected Sub btnCreateCategory_Click(sender As Object, e As EventArgs) Handles btnCreateCategory.Click
    	Try
            Dim unique As String = txtNewCategory.Text
            unique = Trim(unique)

            Database.Tenant.Execute("IF(NOT EXISTS(SELECT TOP 1 1 FROM Cse.FieldCategory)) BEGIN TRUNCATE table Cse.FieldCategory; END")
            Dim uniqueData As DataTable = Database.Tenant.GetDataTable("SELECT * from Cse.FieldCategory where Category = {0}".SqlFormatString(unique))
            If uniqueData.Rows.Count > 0 Then
                Alert("The category name already exists. Duplicate name is not allowed.")

                LoadCategories()
                Return
            Else
                BusinessLogic.CSEFieldCategory.Create(txtNewCategory.Text)

                Dim Description As String = "New Category (" + txtNewCategory.Text + ") has been Created."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)

                txtNewCategory.Text = ""
                LoadCategories()
            End If


        Catch ex As Exceptions.DuplicateEntryExpections

            Alert("The category name already exists. Duplicate name is not allowed.")
        	 LoadCategories()
        End Try
    End Sub

    Protected Sub lbExport_Click(sender As Object, e As EventArgs) Handles lbExport.Click
        Dim gen = New EnvironmentConfiguration
        gen.ExportSettings(ConfigFileType.CSEFieldCategory, Response, "CSEcategories", Database.Tenant)
    End Sub

    Protected Sub btnUploadImportFile_Click(sender As Object, e As EventArgs) Handles btnUploadImportFile.Click
        If fuImportFile.HasFile Then
            Try
                Dim gen = New EnvironmentConfiguration
                gen.ImportSettings(fuImportFile.FileContent, ConfigFileType.CSEFieldCategory, Database.Tenant, Membership.GetUser().ToString, "CSEcategories")
                NotificationMailer.SendNotificationToVendorCDS(Membership.GetUser().ToString, 1, "Setup Comparable Field Categories")
                Alert("The XML file uploaded successfully")
                LoadCategories()
                loadfields()

            Catch ex As Exception
            	Alert(ex.Message)
            	LoadCategories()
                loadfields()
                Return
            End Try
        End If
    End Sub
End Class