﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master" CodeBehind="Settings.aspx.vb" Inherits="CAMACloud.Console.cse_Settings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        ul.cse-warnings {
            width: 600px;
            border: 1px dashed red;
            background-color: #ffffdd;
            padding: 20px;
            list-style-type: square;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Settings</h1>
    <asp:HiddenField runat="server" ID="hdnId" />
    <asp:HiddenField ID="csid" runat="server" Value="" />
    <ul runat="server" id="warnings" visible="false" class="cse-warnings">
    </ul>
    <table style="border-collapse: separate; border-spacing: 0 4px;">
        <tr>
            <td style="padding-right: 20px;">Neighborhood Field:
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlNbhd" Style="width: 240px; margin-left:50px;" />
            </td>
        </tr>
        <tr>
            <td style="padding-right: 20px;">Group Field:
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlGroup" Style="width: 240px; margin-left:50px;" />
            </td>
        </tr>
        <tr>
            <td>Total Value Field:
            </td>
            <td>
                <asp:TextBox ID="txtTtlfldVal" runat="server" value="" Style="width: 236px; margin-left:50px;" />
            </td>
        </tr>
        <tr>
            <td style="padding-right: 20px;">Valuation Table:
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlValuation" Style="width: 240px; margin-left:50px;" />
            </td>
        </tr>
        <tr>
            <td>Appraised Value Label:
            </td>
            <td>
                <asp:TextBox ID="txtAprlbl" runat="server" value="" Style="width: 236px; margin-left:50px;" />
            </td>
        </tr>
        <tr>
            <td style="padding-right: 20px;">
                Sales Table:
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlSales" Style="width: 240px; margin-left:50px;" />
            </td>
        </tr>
        <tr>
            <td style="padding-right: 20px;">
                Sales Date Field:
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlSalesfield" Style="width: 240px; margin-left:50px;" />
            </td>
        </tr>
        <tr>
            <td>Sales Table Filter:
            </td>
            <td>
                <asp:TextBox ID="txtSalesFilter" TextMode="MultiLine" runat="server" value="" Style="width: 400px; margin-left:50px;" Rows="5" />
            </td>
        </tr>



    </table>
    <div>
        <asp:Button ID="btnSave" runat="server" Text="Save" Width="75px" Style="float: left; margin-top: 15px; margin" />
    </div>
</asp:Content>
