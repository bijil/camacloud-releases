﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/DataSetup.master"
	AutoEventWireup="false" Inherits="CAMACloud.Console.cse_rlookup" Codebehind="rlookup.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
	<script type="text/javascript">
		function RedirectPage()
        {
          window.location = "rlookup.aspx";
        }
	</script>
	<style type="text/css">
		.new-range td
		{
			vertical-align:middle;
			padding-right:20px;	
		}
		.new-range
		{
			padding-bottom:20px;
		}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<h1>
		Range Lookup for Comparable Sales Engine</h1>
	<asp:Panel runat="server" ID="listPanel">
		<p class="info">
			Range Lookups can be used to assign weights to range slab of values. This can be
			used as lookups while evaluating comparable sales of any subject parcel.
		</p>
		<table style="border-spacing:2px; width:100%; float:right;" class="new-range">
			<tr>
				<td>
					<strong>New Lookup:</strong>
				</td>
				<td>
					<asp:TextBox runat="server" ID="txtNewLookupName" Width="200px" MaxLength="40" />
					<asp:RequiredFieldValidator runat="server" ControlToValidate="txtNewLookupName" ValidationGroup="NewLookup"
					ErrorMessage="*" />
					<asp:RegularExpressionValidator runat="server" ControlToValidate="txtNewLookupName"
					ValidationExpression="^([a-zA-Z0-9_]*)$" ValidationGroup="NewLookup" ErrorMessage="Invalid lookup name"
					ForeColor="Red" />
				</td>
				
				<td style="text-align:right;">
	     			<asp:LinkButton runat="server" ID="lnkbtnExp" Text="Export Range Lookup" style="padding:3px;margin-left:10px;font-size: 15px;" />	
	     		</td>
	     		<td style="text-align:right;">
	     			<asp:LinkButton runat="server" ID="lnkbtnImp" Text="Import Range Lookup" style="padding:3px;margin-left:10px;font-size: 15px;" OnClientClick="document.getElementById('importFile').click();return false;"/>
	     		</td>
	    		<div style="display:none;">
		      		<asp:FileUpload runat="server" ID="importFile" ClientIDMode="Static" onchange="document.getElementById('btnImport').click()"  accept="application/xml" />
			  		<asp:Button runat="server" ID="btnImport" ClientIDMode="Static" />
				</div>
				
			</tr>
			<tr>
				<td>
					<strong>Increment factor: </strong>
				</td>
				<td>
            		<asp:DropDownList runat="server" ID="ddlFactor" AutoPostBack="true" style="width:80px">
                	<asp:ListItem Text="1" Value="1"></asp:ListItem>
                	<asp:ListItem Text="0.1" Value="0.1"></asp:ListItem>
                	<asp:ListItem Text="0.01" Value="0.01"></asp:ListItem>
                	<asp:ListItem Text="0.001" Value="0.001"></asp:ListItem>
            		</asp:DropDownList>	
				</td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td>
					<asp:Button runat="server" ID="btnCreate" Text=" Create " ValidationGroup="NewLookup" />
				</td>
				<td></td>
				<td></td>
			</tr>
		</table>
		<asp:GridView runat="server" ID="grid">
			<Columns>
				<asp:BoundField DataField="GroupName" HeaderText="Lookup Name" ItemStyle-Width="300px" />
				<asp:TemplateField>
					<ItemTemplate>
						<asp:LinkButton runat="server" ID="lbEdit" Text="Edit" CommandName="EditLookup" CommandArgument='<%# Eval("GroupName") %>' />
						<asp:LinkButton runat="server" ID="lbDelete" Text="Delete" CommandName="DeleteLookup"
							CommandArgument='<%# Eval("GroupName") %>' OnClientClick="return confirm('Are your sure to permanently delete this lookup?');" />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
			<EmptyDataTemplate>
				<p>
					There are no range lookups available.
			</EmptyDataTemplate>
		</asp:GridView>
	</asp:Panel>
	<asp:Panel runat="server" ID="editPanel">
		<h3 runat="server" id="editTitle">
			Edit Lookup</h3>
		<asp:HiddenField runat="server" ID="hdnGroup" Value="" />
		<asp:HiddenField runat="server" ID="hdnFactor" Value="" />
		<p>
			<asp:LinkButton runat="server" ID="lbReturn" Text="Return to List" Font-Bold="true" />
		</p>
		<p runat="server" style="font-weight: bold;" id="editFactor">
		</p>
		<table style="border-spacing:0px;" class="new-range">
			<tr>
				<td><strong>Add new range: </strong></td>
				<td>From: <asp:TextBox runat="server" Width="80px" MaxLength="6" ID="txtFrom" /></td>
				<td>To: <asp:TextBox runat="server" Width="80px" MaxLength="6" ID="txtTo" /></td>
				<td>Value: <asp:TextBox runat="server" Width="80px" MaxLength="6" ID="txtValue" /></td>
				<td><asp:HiddenField runat="server" ID="hdnlookupid" Value="" /></td>
				<td>
					<asp:Button runat="server" ID="btnAddRange" Text=" Add Range " />
				</td>
			</tr>
		</table>
		<asp:GridView runat="server" ID="gvLookup">
			<Columns>
				<asp:BoundField DataField="ValueFrom" HeaderText="From" ItemStyle-Width="120px" />
				<asp:BoundField DataField="ValueTo" HeaderText="To" ItemStyle-Width="120px" />
				<asp:BoundField DataField="RangeValue" HeaderText="Value" ItemStyle-Width="120px" />
				<asp:TemplateField>
					<ItemTemplate>
						<asp:LinkButton runat="server" ID="lbEdit" Text="Edit" CommandName="EditRange" CommandArgument='<%# Eval("Id") %>'/>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField>
					<ItemTemplate>
						<asp:LinkButton runat="server" ID="lbDelete" Text="Delete" CommandName="DeleteRange"
							CommandArgument='<%# Eval("Id") %>' OnClientClick="return confirm('Are your sure to permanently delete this range?');" />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
			<EmptyDataTemplate>
				<p>No values available. Create values by entering into text fields above.</p>
			</EmptyDataTemplate>
		</asp:GridView>
	</asp:Panel>
</asp:Content>
