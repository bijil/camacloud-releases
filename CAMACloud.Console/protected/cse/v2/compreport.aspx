﻿<%@ Page Title="Comparability Report View" Language="VB" MasterPageFile="~/App_MasterPages/DesktopWeb.Master"
    AutoEventWireup="false" Inherits="CAMACloud.Console.CSEV2_compreport" CodeBehind="compreport.aspx.vb" %>

<%@ Import Namespace="System.Web.Script.Services" %>
<%@ Import Namespace="System.Web.Services" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="CAMACloud" %>
<%@ Import Namespace="CAMACloud.Console" %>
<%@ Import Namespace="CAMACloud.Data" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyC7e5YrZlKqWtaY4ehsDwMgCis4IfNs7Xw"></script>
    <script type="text/javascript" src="/App_Static/js-v2/cse/cse-common.js?t=9"></script>
    <script type="text/javascript" src="/App_Static/jslib/datatable.js?t=5"></script>
    <script type="text/javascript" src="/App_Static/js-v2/cse/report-lib.js?t=5"></script>
    <script type="text/javascript" src="/App_Static/js-v2/cse/cse-compreport.js?t=20"></script>

    <link rel="stylesheet" type="text/css" href="/App_Static/css-v2/cse/cse-common.css?t=10" />
    <link rel="stylesheet" type="text/css" href="/App_Static/css-v2/cse/comp-report.css?t=15" />
    <link rel="stylesheet" type="text/css" href="/App_Static/css-v2/cse/report-lib.css?t=5" />



    <%--<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.13/semantic.min.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/se/dt-1.10.25/datatables.min.css" />--%>

    <%--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.13/semantic.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/se/dt-1.10.25/datatables.min.js"></script>--%>
    <style type="text/css">
        @page{
            size: landscape;
        }
    </style>
    <script type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="content-area-wrapper">
        <div class="report-panel">
            <div class="report-title">
                <button class="report-back button no-print" onclick = "redirectToSearch(); return false;">
                    <i class="report-back__icon"></i>
                    Go Back
                </button>
                <div class="result-title-panel">
                    <div class="report-status-panel">
                        <div class="report-status badge"></div>
                        <div class="report-est-sale badge"></div>
                    </div>
                    <div class="button_divs no-print">
                    	<button class="addTo-Fav button" onclick = "addTofav(); return false;">Pin Report</button>
                        <button class="export-report button" onclick = "downloadReport(); return false;">Export to Excel</button>
                        <button class="print button" onclick = "printPage(); return false;">Print</button>
                        <div class="unselectable switch-button">
                            <input class="switch-button-checkbox" type="checkbox" />
                            <label class="switch-button-label" for=""><span class="switch-button-label-span">Report View</span></label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="report-container">
                <div class="report-table-div">
                    <table class='rp__table ui fixed celled table'>
                        <thead></thead>
                        <tbody></tbody>
                        <tfoot></tfoot>
                    </table>
                </div>
                <div class="mapview" id="mapView" style="display: none;">
                    <div id="legend" class="legend"></div>
                    <div class="gmap" id="gmap" style=""></div>
                </div>
            </div>
        </div>
    </div>



</asp:Content>
<script runat="server">

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetReportData(Subject As String, Comparables As String, EstSalePrice As Integer) As ActionResult(of ReportData)
        Dim l As New ActionResult(of ReportData)
        Try
            Dim li As New ReportData
            Dim ds As DataSet
            Dim Sql As String = "EXEC cse.GetComparablesView  {0}, {1} , @EstSalePrice= {2}".FormatString(Subject, Comparables, EstSalePrice)
            ds = Database.Tenant.GetDataSet(Sql)
            li.Report = ds.Tables(0).ConvertToList
            li.Footer = ds.Tables(1).ConvertToList
            Dim dtIndex As Integer = 4
            If ds.Tables.Count < 5 Then
                l.ErrorMessage = "Invalid output from the procedure. No sufficient data tables. 5 tables are expected."
                l.Success = False
                Return l
            End If
            li.UIFooter = ds.Tables(2).ConvertToList
            If EstSalePrice = 1 Then
                If ds.Tables.Count < 6 Then
                    l.ErrorMessage = "Invalid output from the procedure. No sufficient data tables. 6 tables are expected."
                    l.Success = False
                    Return l
                End if
                dtIndex = 5
                li.SaleInfo = ds.Tables(4).ConvertToList
            End If
            li.ParcelsInfo = ds.Tables(dtIndex).ConvertToList
            li.qPublicEnabled = Database.Tenant.GetStringValue("SELECT Value FROM ClientSettings WHERE Name='EnableqPublic'")
            li.qKeyField = Database.Tenant.GetStringValue("SELECT Value FROM ClientSettings WHERE Name='qPublicKeyField'")
            li.qFieldFormatter = Database.Tenant.GetStringValue("SELECT Value FROM ClientSettings WHERE Name='qPublicFormatter'")
            li.organizationName = ClientOrganization.PropertyValue("County", HttpContext.Current.GetCAMASession().OrganizationId)
            li.stateName = ClientOrganization.PropertyValue("State", HttpContext.Current.GetCAMASession().OrganizationId)
            l.Data = li
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
            l.Success = False
        End Try
        'l.Query = q
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetParcelImage(ParcelId As Integer) As ActionResult(Of ParcelImage)
        Dim l As New ActionResult(Of ParcelImage)
        Try
            Dim li As New ParcelImage
            Dim pImage As String = s_("SELECT TOP 1 '/imgsvr/' + Path as Imagepath FROM ParcelImages WHERE parcelId = {0} order by isprimary desc".SqlFormat(True, ParcelId))
            li.Image = pImage
            l.Data = li
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
            l.Success = False
        End Try
        'l.Query = q
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetQPublicLink(ParcelId As Integer) As ActionResult(Of QPublicData)
        Dim l As New ActionResult(Of QPublicData)
        Try
            Dim li As New QPublicData
            Dim pData As DataSet = Database.Tenant.GetDataSet("Select * From ParcelData WHERE cc_parcelId = {0}".SqlFormat(False, ParcelId))
            li.QData = pData.Tables(0).ConvertToList
            l.Data = li
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
            l.Success = False
        End Try
        'l.Query = q
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetComparablesMapPoints(ParcelList As String) As ActionResult(Of MapPoints)
        Dim l As New ActionResult(Of MapPoints)
        Try
            Dim li As New MapPoints
            li.Points = d_("EXEC cse.ComparablesMapPoints {0}".SqlFormat(False, ParcelList)).ConvertToList
            l.Data = li
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
            l.Success = False
        End Try
        'l.Query = q
        Return l
    End Function
    
    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)> Public Shared Function AddToFavourite(ParcelId As Integer, SearchText As String, SearchFilters As String, CompParcels As String) As ActionResult(Of Boolean)
        Dim l As New ActionResult(Of Boolean)
        Try 
            Dim UserName = Membership.GetUser().UserName.ToString().ToLower
            Dim CheckForStatus = Database.Tenant.GetIntegerValue("SELECT count(*) FROM cse.RecentCompared WHERE UserName = {0} AND ParcelId = {1} AND SearchParams = {2} AND SelectedComps = {3} AND IsFav = {4}".SqlFormat(True, UserName, ParcelId, SearchFilters,CompParcels,1))
            If CheckForStatus = 0 Then
              		Database.Tenant.Execute("INSERT INTO cse.RecentCompared(UserName, ParcelId, SearchTerms, SearchParams, SelectedComps, IsFav) Values({0},{1},{2},{3},{4},1)".SqlFormat(True, UserName,ParcelId,SearchText,SearchFilters,CompParcels))
                	l.Data = True
            Else
					Database.Tenant.Execute("DELETE FROM cse.RecentCompared WHERE UserName = {0} AND ParcelId = {1} AND SearchParams = {2} AND SelectedComps = {3} AND IsFav = {4}".SqlFormat(True, UserName, ParcelId, SearchFilters,CompParcels,1))
                	l.Data = False	            
            End If
            l.Success = True
        Catch ex As Exception
            l.Success = False
            l.ErrorMessage = ex.Message
        End Try
        Return l
    End Function
    
    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)> Public Shared Function checkIfFaved(ParcelId As Integer, SearchText As String, SearchFilters As String, CompParcels As String) As ActionResult(Of Boolean)
        Dim l As New ActionResult(Of Boolean)
        Try 
            Dim UserName = Membership.GetUser().UserName.ToString().ToLower
            Dim CheckForStatus = Database.Tenant.GetIntegerValue("SELECT count(*) FROM cse.RecentCompared WHERE UserName = {0} AND ParcelId = {1} AND SearchParams = {2} AND SelectedComps = {3} AND IsFav = {4}".SqlFormat(True, UserName, ParcelId, SearchFilters,CompParcels,1))
            If CheckForStatus = 0 Then
                	l.Data = False
            Else
                	l.Data = True	            
            End If
            l.Success = True
        Catch ex As Exception
            l.Success = False
            l.ErrorMessage = ex.Message
        End Try
        Return l
    End Function

    Public Class ReportData
        Public Property Report As JSONTable
        Public Property Footer As JSONTable
        Public Property UIFooter As JSONTable
        Public Property ParcelsInfo As JSONTable
        Public Property SaleInfo As JSONTable
        Public Property qPublicEnabled As String
        Public Property qKeyField As String
        Public Property qFieldFormatter As String
        Public Property organizationName As String
        Public Property stateName As String
    End Class

    Public Class ParcelImage
        Public Property Image As String
    End Class

    Public Class QPublicData
        Public Property QData As JSONTable
    End Class

    Public Class MapPoints
        Public Property Points As JSONTable
    End Class

</script>

