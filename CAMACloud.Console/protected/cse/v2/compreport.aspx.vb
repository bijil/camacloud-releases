﻿Imports System.Web.Script.Services
Imports System.Web.Services
Imports CAMACloud

Partial Class CSEV2_compreport
    Inherits System.Web.UI.Page
    Public Uniformity As String
    Public organizationName As String
    Public UniformityIndication As String
    Public UniformityId As Integer
    Dim Est
    Dim HideUIF As Boolean
    Dim NoOfComparables As Integer
  	Public Event RunCommand(pageCommand As String)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then

        End If

        If Page.Request.CurrentExecutionFilePath <> Page.Request.Url.AbsolutePath Then
            Dim commandName = Page.Request.Url.AbsolutePath.Replace(Page.Request.CurrentExecutionFilePath, "").TrimStart("/")
            RaiseEvent RunCommand(commandName)
        End If

    End Sub	

    Public Sub Page_PageCommand(commandName As String) Handles Me.RunCommand
        If commandName = "exportreport" Then
             generateReport()
        End If
    End Sub
    
    Public Function generateReport()
    	Dim subject = Request("s")
        Dim comps = Request("c")
        Est = Request("e")
        organizationName = ClientOrganization.PropertyValue("County", HttpContext.Current.GetCAMASession().OrganizationId)
        For Each c As String In comps.Split(",")
            If IsNumeric(c) Then
                NoOfComparables += 1
            End If
        Next
        Dim ds As DataSet
        Try
            ds = Database.Tenant.GetDataSet("EXEC cse.GetComparablesView " + subject + ", " + comps + "," + "@EstSalePrice=" + Est)
        Catch ex As Exception
            Dim message As String
            If TypeOf ex.InnerException Is SqlClient.SqlException Then
                Dim ix As SqlClient.SqlException = ex.InnerException
                message = ix.Message
            Else
                message = ex.Message
            End If
        End Try
        Dim u As DataRow = ds.Tables(2).Rows(0)
        Uniformity = u.GetString("Uniformity")
        UniformityIndication = u.GetString("UniformityIndication")
        UniformityId = u.GetInteger("UniformityId")
        HideUIF = u.GetBoolean("HideUIF")
        Dim headerSpan = (NoOfComparables * 2) + 3
        ''uifooter.Visible = Not u.GetBoolean("HideUIF")
        Dim s As New StringBuilder()
        s.Append("<table style='border-spacing: 0px; border-collapse: collapse; font-size: 0.9em;' class='reportTable' id ='test_table'>")
        s.Append("<tr class='header-row'><th colspan='" + headerSpan.ToString() + "' style='text-align:center;'>Comparability Report</th></tr>")
        s.Append("<tr class='header-row'>")
        s.Append("<th colspan='2' class='" + UniformityStyle() + "'>")
        s.Append("<span>" + Uniformity + "</span></th>")
        s.Append("<th class='" + UniformityStyle() + "'>Subject</th>")
        s.Append(EvalCompHeaders())
        s.Append("<th class='no-border'></th></tr>")
        s.Append(EvalCompColumns(ds.Tables(0)))
        s.Append(EvalCompFooters(ds.Tables(1)))
        s.Append(EvalCompUIFooters())
        s.Append("</table>")
        Response.Clear()
        Response.Buffer = True
        Response.ClearContent()
        Response.ClearHeaders()
        Response.AddHeader("Content-Disposition", "attachment;filename=" + organizationName + "-Comparability-Report.xlsx")
        Response.Charset = ""
        Response.ContentType = "application/vnd.openxml.formats-officedocument.spreadsheetml.sheet"
        Dim temp As TableToExcel = New TableToExcel()
        Dim excelStream = temp.Process(s.ToString())
        excelStream.CopyTo(Response.OutputStream)
        Response.End()
    End Function

    Public Function UniformityStyle() As String
        If (Est = 1) Then
            Return ""
        End If
        Select Case UniformityId
            Case 1
                Return "ind-uni"
            Case 2
                Return "ind-nonuni"
            Case 3
                Return "ind-insuff"
        End Select
        Return ""
    End Function

    Public Function EvalCompHeaders() As String
        Dim columns As String = ""
        For i As Integer = 1 To NoOfComparables
        	columns += "<th colspan='2'>Comparable #" & i & "</th>"	
        Next
        Return columns
    End Function

    Public Function EvalCompColumns(data) As String
    	Dim rowString = ""
    	For Each r As DataRow In data.Rows
    		rowString += "<tr>"
    		Dim First = r.getBoolean("IsFirstItem")
        	If  First = True Then
        		rowString += "<td class='label' rowspan='" & r.Item("CatCount") & "'>" + r.Item("Category").ToString + "</td>"
	        End If
	        rowString += "<td class='label'>" & r.Item("Label") & "</td>"
	        rowString += "<td class='label' style='text-align: center;'>" & r.Item("Subject") & "</td>"
	        For i As Integer = 1 To NoOfComparables
	        	Dim comp = r.Item("Comp" & i &"")
	        	Dim adj = r.Item("Adj" & i &"")
	            If adj Is DBNull.Value Then
	                rowString += "<td colspan='2' class='val-col val-comp val-no-adj' style='text-align:center;'>" + comp.ToString + "</td>"
	            Else
	                rowString += "<td class='val-col val-comp' style='text-align:center;'>" + comp.ToString + "</td><td class='val-col val-adj' style='text-align:center;'>" + adj.ToString + "</td>"
	            End If
	        Next
	        rowString += "</tr>"
    	Next
    	Return rowString
    End Function
    
    Public Function EvalCompFooters(data) As String
    	Dim fooString = ""
    	For Each r As DataRow In data.Rows
    		fooString += "<tr class='footer-row'>"
    		fooString += "<td colspan='2'>" & r.Item("Label") & "</td>"
    		fooString += "<td style='text-align: center;'>" & r.Item("Subject") & "</td>"
    		For i As Integer = 1 To NoOfComparables
    			Dim comp = r.Item("Comp" & i &"")
    			fooString += "<td colspan='2'  class='val-col foot-comp' style='text-align:center;'>" + comp.ToString + "</td>"
    		Next
    		fooString += "</tr>"
    	Next
    	Return fooString
    End Function
    
    Public Function EvalCompUIFooters() As String
        Dim UIFooter = ""
        If Not HideUIF Then
            UIFooter += "<tr class='footer-row'><td colspan='2'>Uniformity Indication </td>"
            UIFooter += "<td style='text-align: center;'> " + UniformityIndication + "</td></tr>"
        End If    
        Return UIFooter
    End Function
    
End Class
