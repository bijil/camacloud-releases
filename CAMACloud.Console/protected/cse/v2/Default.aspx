﻿<%@ Page Title="Comparable Properties - Dashboard" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DesktopWeb.Master" CodeBehind="Default.aspx.vb" Inherits="CAMACloud.Console.CSEV2_Default" %>

<%@ Import Namespace="System.Web.Script.Services" %>
<%@ Import Namespace="System.Web.Services" %>
<%@ Import Namespace="CAMACloud" %>
<%@ Import Namespace="CAMACloud.Console" %>
<%@ Import Namespace="CAMACloud.Data" %>
<%@ Import Namespace="System.Data" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyC7e5YrZlKqWtaY4ehsDwMgCis4IfNs7Xw"></script>
    <script type="text/javascript" src="/App_Static/js-v2/cse/cse-common.js?t=15"></script>
    <script type="text/javascript" src="/App_Static/js-v2/cse/cse-searchcomp.js?t=32"></script>
    <script type="text/javascript" src="/App_Static/jslib/tipr.js"></script>
    <link rel="stylesheet" type="text/css" href="/App_Static/css-v2/cse/cse-common.css?t=19" />
    <link rel="stylesheet" type="text/css" href="/App_Static/css-v2/cse/search-comp.css?t=33" />
    <style>
 </style>
    <script> </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div class="d1001">
        <div class="content-area-wrapper">
            <div class="search-comps" style="display: flex;">
                <div class="dashboard-cell search-body" style="flex: auto">
                    <div class="search-box">
                        <div class="search-field1">
                            <span class="key-fields field1-name"></span>
                            <select class="key-fields key-select-box">
                            </select>
                            <input type="text" name="KeyValue1" class="inputfield input-field1" />
                        </div>
                        <div class="search-button-div">
                            <button class="button search-button" style="display: flex;" onclick="searchParcel(); return false;">
                                Search Subject
                            </button>
                        </div>
                        <a class="comp-clear btn" onclick="clearSearch()" title="Clear Search">
                            <span>Clear Search</span>
                        </a> 
                        <a class="comp-settings btn" onclick="editCompSettings()" title="Edit Comparable Settings">
                            <i class="icon icon-settings"></i>
                            <span>Comparable Settings</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class = "recent-search-list dashboard-cell" style="display:none">
                <h2>Recent Searches</h2>
                <div class= "recent-items"></div>
            </div>
            <div class = "fav-reports-list dashboard-cell" style="display:none">
                <h2>Pinned Reports</h2>
                <table class= "fav-table">
                    <thead>
                        <th>Subject Parcel</th>
                        <th>Additional Settings</th>
                        <th>Est. Sale Price</th>
                        <th>Comparables Selected</th>
                        <th></th>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div class="subject-parcel-list">
            </div>
            <div class="result-panel" style="display: flex;">
                <div class="selection-panel dashboard-cell" style="display: none;">
                    <div class="parcel-details selected-parcel" style="display: flex;">
                        <div class="s-image">
                            <div class="s-source zoom-image" style="width: 100px;"></div>
                        </div>
                        <div class="s-header">
                            <div class="s-addrs-desc s-header-div">
                                <span class="s-header-label s-addrs-label">Address</span>
                                <span class="s-header-value s-address"></span>
                            </div>
                            <div class="s-Keyvaleu1-desc s-header-div">
                                <span class="s-header-label s-Keyvaleu1-label"></span>
                                <span class="s-header-value s-keyvalue1"></span>
                            </div>
                            <div class="s-Keyvaleu2-desc s-header-div">
                                <span class="s-header-label s-Keyvaleu2-label"></span>
                                <span class="s-header-value s-keyvalue2"></span>
                            </div>
                            <div class="s-Keyvaleu3-desc s-header-div">
                                <span class="s-header-label s-Keyvaleu3-label"></span>
                                <span class="s-header-value s-keyvalue3"></span>
                            </div>
                            <div class="s-Neighborhood-desc s-header-div">
                                <span class="s-header-label s-Neighborhood-label">NeighborHood</span>
                                <span class="s-header-value s-Neighborhood"></span>
                            </div>
                            <div class="s-Group-desc s-header-div">
                                <span class="s-header-label s-Group-label">Group</span>
                                <span class="s-header-value s-Group"></span>
                            </div>
                        </div>
                    </div>
                    <div class="selected-list">
                        <div class="selected-list-heading" style="display: flex;">
                            <div>
                                <span class="selected-list-head">Selected Parcels -</span>
                                <span class="slots-free" style="font-size: 14px; font-weight: 600">0/0</span>
                            </div>
                            <button title="If Estimate-Subjects-Sale-Price is selected, 4 Parcels should be selected to generate report.&#10;Otherwise at least 1 parcel should be selected." class="button report-button" onclick="generateReport(); return false;">Generate Report </button>
                        </div>
                        <div class="selected-list-items"></div>
                    </div>
                </div>
                <div class="result-container dashboard-cell" style="flex: 70%">
                    <div class="flex-column" style="height: 100%">
                        <div class="result-title-panel">
                            <div class="unselectable flex align-center justify-between" style="padding: 5px 10px; height: 45px; width: 205px;">
                                <div class="switch-button">
                                    <input class="switch-button-checkbox" type="checkbox" />
                                    <label class="switch-button-label" for=""><span class="switch-button-label-span">Card View</span></label>
                                </div>
                            </div>
                            <div class="show-map-label">
                                <input id="show-map-labels" class="show-map-labels" style="width: 15px; height: 15px;" type="checkbox" />
                                <label for="show-map-labels">Show Labels</label>
                            </div>
                            <div class="show-map-sale">
                                <input id="show-map-sales" class="show-map-sales" style="width: 15px; height: 15px;" type="checkbox" />
                                <label for="show-map-sales">Show Qualified Sale Info</label>
                            </div>
                            <span class="parcel-count result-title-head">Parcel Count : </span>
                        </div>
                        <div class="grid-view">
                            <div class="result-list"></div>
                        </div>
                        <div class="mapview" id="mapview" style="display: none;">
                            <div class="map-container" style="display: flex;">
                                <div class="gmap" id="gmap"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="cpSettingsMask" style="display: none;"></div>
    <div class="filter-box popup popup-center cpSettingsDialog" style="width: auto; display: none;">
        <a class="filter-box-close" onclick="hideSettingsDialog()">✖</a>
        <h1 class="nomar filter-title">Edit Comparability Settings</h1>
        <div style="margin: 10px 0 16px;">
            <div class="cpSettingsDialog__row cpSettingsDialog__row--index">
                <label>Calculate Uniformity Indication based upon Comparability Indexes:</label>
                <select class="cpSettingsDialog__uniPercent">
                    <option value="0.80">&gt;=80%</option>
                    <option value="0.85">&gt;=85%</option>
                    <option value="0.90">&gt;=90%</option>
                </select>
            </div>
            <div class="cpSettingsDialog__row cpSettingsDialog__row--number">
                <label>Maximum number of Selections for Comp Report allowed:</label>
                <select class="cpSettingsDialog__maxCount">
                    <option value="5">5</option>
                    <option value="10">10</option>
                    <option value="15">15</option>
                </select>
            </div>
            <div class="cpSettingsDialog__row cpSettingsDialog__row--univisible">
                <label>Hide Uniformity Indicated Value</label>
                <div>
                    <input type="checkbox" id="chkHideUniformity" class="cpSettingsDialog__hideUniValue" />
                    <label for="chkHideUniformity">Visible</label>
                </div>
            </div>
            <p class="badge badge--grey">You might've to refresh or reload the page for some changes to take effect.</p>
        </div>
        <div class="r filter-button-panel">
            <button class="filter-button filter-box-ok" onclick="saveCompSettings(); return false;">
                Save
            </button>
        </div>
    </div>
</asp:Content>
<script runat="server">
    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetLoadInfo() As ActionResult(Of LoadInfo)
        Dim l As New ActionResult(Of LoadInfo)
        Try
            Dim li As New LoadInfo
            li.ApplicationSettings = Database.Tenant.GetDataTable("SELECT * FROM Application").ConvertToList
            li.Settings = Database.Tenant.GetDataTable("SELECT * FROM cse.Settings").ConvertToList
            li.AlternateKeyLabel = Database.Tenant.Application.AlternateKeyLabelfield
            l.Data = li
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
            l.Success = False
        End Try
        'l.Query = q
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetParcels(txtKeyValue1 As String, txtKeyValue2 As String, txtKeyValue3 As String) As ActionResult(Of Parcel)
        Dim l As New ActionResult(Of Parcel)
        Try
            Dim li As New Parcel
            li.Parcel = Database.Tenant.GetDataTable("EXEC SearchParcel {0}, {1}, {2}".SqlFormat(True, txtKeyValue1, txtKeyValue2, txtKeyValue3)).ConvertToList
            l.Data = li
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
            l.Success = False
        End Try
        'l.Query = q
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetParcelImage(ParcelId As Integer) As ActionResult(Of ParcelImage)
        Dim l As New ActionResult(Of ParcelImage)
        Try
            Dim li As New ParcelImage
            Dim pImage As String = s_("SELECT TOP 1 '/imgsvr/' + Path as Imagepath FROM ParcelImages WHERE parcelId = {0} order by isprimary desc".SqlFormat(True, ParcelId))
            li.Image = pImage
            l.Data = li
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
            l.Success = False
        End Try
        'l.Query = q
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetComparables(ParcelId As Integer, hdnRecalc As Boolean, checkGrp As Boolean, checkMatched As Boolean, checkSale As Boolean, checkEstSale As Boolean) As ActionResult(Of CompParcels)
        Dim l As New ActionResult(Of CompParcels)
        Try
            Dim li As New CompParcels
            li.Parcels = d_("EXEC cse.GetComparablesFromCache {0}, {1}, {2}, {3}, {4}, {5}".SqlFormat(False, ParcelId, hdnRecalc, checkGrp, checkMatched, checkSale, checkEstSale)).ConvertToList
            l.Data = li
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
            l.Success = False
        End Try
        'l.Query = q
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetComparablesMapPoints(ParcelList As String) As ActionResult(Of MapPoints)
        Dim l As New ActionResult(Of MapPoints)
        Try
            Dim li As New MapPoints
            li.Points = d_("EXEC cse.ComparablesMapPoints {0}".SqlFormat(False, ParcelList)).ConvertToList
            l.Data = li
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
            l.Success = False
        End Try
        'l.Query = q
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetComparableSettings() As ActionResult(Of ComparableDashboard)
        Dim l As New ActionResult(Of ComparableDashboard)
        Try
            Dim li As New ComparableDashboard
            Dim count As Integer = Database.Tenant.GetIntegerValue("SELECT COUNT(Id) FROM cse.Settings WHERE IsActive = 1")
            If count = 0 Then
                Database.Tenant.Execute("INSERT INTO cse.Settings (IsActive, UniformityIndicationLowPercent, MaxCompCount, HideUniformityIndicationValue) VALUES (1, 0.8, 5, 0);")
            End If
            li.ApplicationSettings = Database.Tenant.GetDataTable("SELECT * FROM Application").ConvertToList
            li.AlternateKeyLabel = Database.Tenant.Application.AlternateKeyLabelfield
            li.ComparableSettings = Database.Tenant.GetTopRow("SELECT Id, UniformityIndicationLowPercent, MaxCompCount, HideUniformityIndicationValue FROM cse.Settings WHERE IsActive = 1 ORDER BY Id DESC").ToKeyValueList

            l.Data = li
            l.Success = l.Data IsNot Nothing
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)> Public Shared Function SaveComparableSettings(id As Integer, uniPercent As Double, maxCount As Integer, hideUni As Integer) As ActionResult(Of ComparableDashboard)
        Dim l As New ActionResult(Of ComparableDashboard)
        Try
            Dim li As New ComparableDashboard
            Database.Tenant.Execute("UPDATE cse.Settings SET UniformityIndicationLowPercent = {0}, MaxCompCount = {1}, HideUniformityIndicationValue = {2} WHERE Id = {3}".SqlFormat(True, uniPercent, maxCount, hideUni, id))
            li.ComparableSettings = Database.Tenant.GetTopRow("SELECT Id, UniformityIndicationLowPercent, MaxCompCount, HideUniformityIndicationValue FROM cse.Settings WHERE IsActive = 1 ORDER BY Id DESC").ToKeyValueList
            l.Data = li
            l.Success = l.Data IsNot Nothing
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)> Public Shared Function AddToRecent(ParcelId As Integer, SearchText As String, SearchFilters As String) As ActionResult(Of Boolean)
        Dim l As New ActionResult(Of Boolean)
        Try 
            Dim UserName = Membership.GetUser().UserName.ToString().ToLower
            Dim CheckForSame = Database.Tenant.GetIntegerValue("SELECT count(*) FROM cse.RecentCompared WHERE UserName = {0} AND ParcelId = {1} AND SearchParams = {2} ".SqlFormat(True, UserName, ParcelId, SearchFilters))
            If CheckForSame = 0 Then
                Dim count = Database.Tenant.GetIntegerValue("SELECT count(*) FROM cse.RecentCompared WHERE UserName = {0} ".SqlFormat(True, UserName))
                If count >= 5 Then
                    Dim OldEntry = Database.Tenant.GetIntegerValue("Select MIN(ID) FROM cse.RecentCompared Where UserName = {0}".SqlFormat(True, UserName))
                    Database.Tenant.Execute("Delete From cse.RecentCompared Where Id  = {0}".SqlFormat(True, OldEntry))
                End If
                Database.Tenant.Execute("INSERT INTO cse.RecentCompared(UserName, ParcelId, SearchTerms, SearchParams, IsFav) Values({0},{1},{2},{3},0)".SqlFormat(True, UserName,ParcelId,SearchText,SearchFilters))
            End If
            l.Success = True
        Catch ex As Exception
            l.Success = False
            l.ErrorMessage = ex.Message
        End Try
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)> Public Shared Function GetRecent() As ActionResult(Of RecentHistory)
        Dim l As New ActionResult(Of RecentHistory)
        Try 
            Dim li As New RecentHistory
            Dim UserName = Membership.GetUser().UserName.ToString().ToLower
            li. Recents = Database.Tenant.GetDataTable("SELECT TOP 5 * from cse.RecentCompared WHERE UserName = {0} AND IsFav = 0 Order by ID Desc".SqlFormat(True, UserName)).ConvertToList
            l.Data = li
            l.Success = True
        Catch ex As Exception
            l.Success = False
            l.ErrorMessage = ex.Message
        End Try
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)> Public Shared Function DeleteFromFavourite(Id As Integer) As ActionResult(Of Boolean)
        Dim l As New ActionResult(Of Boolean)
        Try 
            Database.Tenant.Execute("DELETE FROM cse.RecentCompared WHERE ID = {0}".SqlFormat(True, Id))	            
            l.Success = True
        Catch ex As Exception
            l.Success = False
            l.ErrorMessage = ex.Message
        End Try
        Return l
    End Function
	
	<WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)> Public Shared Function GetFavItems() As ActionResult(Of RecentHistory)
        Dim l As New ActionResult(Of RecentHistory)
        Try 
            Dim li As New RecentHistory
            Dim showAlternate As String = Database.Tenant.Application.AlternateKeyfield
            Dim UserName = Membership.GetUser().UserName.ToString().ToLower
            
            Dim dt = Database.Tenant.GetDataTable("SELECT  *, CAST(NULL As VARCHAR(MAX))  As ParcelKeyFields from cse.RecentCompared WHERE UserName = {0} AND IsFav = 1 ".SqlFormat(True, UserName))
            For Each gr As DataRow In dt.Rows
                Dim ParcelIds As String = gr.GetString("SelectedComps")
                Dim KeyFields As String = ""
                For Each c As String In ParcelIds.Split(",")
                        Dim Id As String
                        If String.IsNullorEmpty(showAlternate) Then
                            Dim Sql = "SELECT KeyValue1 FROM Parcel WHERE Id = {0}".FormatString(c)
                            Id = Database.Tenant.GetStringValue(Sql)  
                        Else 
                            Dim Sql = "SELECT {0} FROM ParcelData WHERE CC_ParcelId = {1}".FormatString(showAlternate,c)
                            Id = Database.Tenant.GetStringValue(Sql)
                        End If      
                        If KeyFields <> "" Then
                            KeyFields += ", "
                        End If
                        KeyFields += Id
                Next
                gr.Item("ParcelKeyFields") = KeyFields
            Next
            li.Recents = dt.ConvertToList
            l.Data = li
            l.Success = True
        Catch ex As Exception
            l.Success = False
            l.ErrorMessage = ex.Message
        End Try
        Return l
    End Function

    Public Class LoadInfo
        Public Property Settings As JSONTable
        Public Property ApplicationSettings As JSONTable
        Public Property AlternateKeyLabel As String
    End Class

    Public Class Parcel
        Public Property Parcel As JSONTable
    End Class

    Public Class RecentHistory
        Public Property Recents As JSONTable
    End Class

    Public Class MapPoints
        Public Property Points As JSONTable
    End Class

    Public Class CompParcels
        Public Property Parcels As JSONTable
    End Class

    Public Class ParcelImage
        Public Property Image As String
    End Class

    Public Class ParcelDetails
        Public Property Id As Integer
        Public Property KeyValue1 As String
        Public Property KeyValue2 As String
        Public Property KeyValue3 As String
        Public Property StreetAddress As String
        Public Property Latitude As Double
        Public Property Longitude As Double
        Public Property Nbhd As String
        Public Property Group As String
    End Class

    Public Class ComparableDashboard
        Public Property ComparableSettings As Dictionary(Of String, Object)
        Public Property ApplicationSettings As JSONTable
        Public Property AlternateKeyLabel As String
    End Class

    Public Class ComparableSettings
        Public Property Id As Integer
        Public Property UniformityIndicationLowPercent As Double
        Public Property MaxCompCount As Integer
        Public Property HideUniformityIndicationValue As Boolean
    End Class
</script>

