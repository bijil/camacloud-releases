﻿
Partial Class cse_Default
    Inherits System.Web.UI.Page

    Public MaxCompCount As Integer = 5
    Dim searchparams As String = ""
    Dim openInMap As Boolean = False
    Dim noRows As Boolean =False
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
    	
        Dim settings As DataRow = t_("SELECT * FROM cse.Settings WHERE IsActive = 1 ORDER BY Id DESC")
        If settings IsNot Nothing Then
            MaxCompCount = settings.GetInteger("MaxCompCount")
        End If

		If Not IsPostBack Then
			parcelDetails.Visible = False
			gvMultiParcel.Visible = False

			For Each grid As GridView In New GridView() {gvc, gvMultiParcel}
				For Each c As DataControlField In grid.Columns
					Select Case c.HeaderText
						Case "KeyValue1"
							If Database.Tenant.Application.AlternateKeyfield.IsEmpty Then
								c.HeaderText = Database.Tenant.Application.KeyField(1)
							Else
								c.HeaderText = Database.Tenant.Application.AlternateKeyLabelfield
						    End If
						Case "KeyValue2"
                            c.HeaderText = Database.Tenant.Application.KeyField(2)
                            If Database.Tenant.Application.KeyField(2).IsEmpty Then
                                c.Visible = False
                                trKeyField2.Visible = False
                                resKeyField2.Visible = False
                            End If
						Case "KeyValue3"
                            c.HeaderText = Database.Tenant.Application.KeyField(3)
                            If Database.Tenant.Application.KeyField(3).IsEmpty Then
                                c.Visible = False
                                trKeyField3.Visible = False
                                resKeyField3.Visible = False
                            End If
					End Select
                Next
            Next

            If settings IsNot Nothing  Andalso settings.GetString("SalesTable") = ""   Then
                chkSale.Enabled = False
                chkSale.Checked = False
            End If
            If Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM Neighborhood WHERE GroupNo IS NOT NULL") = 0  AndAlso
               Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM cse.Settings WHERE GroupField IS NOT NULL") = 0 Then
                chkGroup.Checked = False
                chkGroup.Visible = False
            End If
            searchparams = Request("searchparams")
            If (searchparams <> "" AndAlso searchparams.Split(",").Length = 7) Then
                Dim values = searchparams.Split(",")
                txtKeyValue1.Text = IIf(values(0) = "0", "", values(0))
                txtKeyValue2.Text = IIf(values(1) = "0", "", values(1))
                txtKeyValue3.Text = IIf(values(2) = "0", "", values(2))
                searchParcel()
                chkGroup.Checked = IIf(values(3) = 1, True, False)
                chkMatched.Checked = IIf(values(4) = 1, True, False)
                chkSale.Checked = IIf(values(5) = 1, True, False)
                chkEstSalePrice.Checked = IIf(values(6) = 1, True, False)
                hdnRecalc.Value = "0"
                SearchComparables()
            End If
		End If
		If openInMap = True Then
			btnGridview .Visible =True
			btnMapview .Visible =False 
		Else 
			btnGridview .Visible =False 
			btnMapview .Visible =True
		End If
		
	RunScript("initTooltip();")
	End Sub

Protected Sub btnSearch_Click(sender As Object, e As System.EventArgs) Handles btnSearch.Click
	searchParcel()
	RunScript("initTooltip();")
	
	End Sub
	Protected Sub btnMapview_Click(sender As Object, e As System.EventArgs) Handles btnMapview.Click
		If noRows Then		
		Else
		gvc.Visible =False
		openInMap=True
		btnGridview .Visible =True 
		btnMapview .Visible =False 
		End If
	End Sub
	Protected Sub btnGridview_Click(sender As Object, e As System.EventArgs) Handles btnGridview.Click
		gvc.Visible =True
		openInMap=False
		btnMapview .Visible =True 
		btnGridview.Visible =False 
	End Sub
	Sub searchParcel()
		'openInMap  = False
    	Dim sql As String = "EXEC SearchParcel {0}, {1}, {2}".SqlFormat(True, txtKeyValue1, txtKeyValue2, txtKeyValue3)
        If (txtKeyValue1.Text = Nothing And txtKeyValue2.Text = Nothing And txtKeyValue3.Text = Nothing) Then
            RunScript("hideInvalidparcelMsg();")
            'Response.Redirect("~/protected/cse/")
            'parcelDetails.Visible = False
            'divResults.Visible = False
            Return
        End If
        Dim dt As DataTable = d_(sql)
        If dt.Rows.Count > 1 Then
            gvMultiParcel.DataSource = dt
            gvMultiParcel.DataBind()
            gvMultiParcel.Visible = True
            parcelDetails.Visible = False
        ElseIf dt.Rows.Count = 1 Then
            OpenParcel(dt.Rows(0).GetInteger("Id"))
        Else
        	RunScript("hideprogressMsg();")
        	parcelDetails.Visible = False
        End If

        gvc.DataSource = Nothing
        gvc.DataBind()
        divResults.Visible = False
    End Sub
    Sub OpenParcel(pid As Integer)
		Dim sql As String = "EXEC SearchParcel @ParcelId = {0}".SqlFormat(True, pid)
		Dim p As DataRow = t_(sql)
		Me.PID.Value = pid
		lblKV1.Text = p.GetString("KeyValue1")
		lblKV2.Text = p.GetString("KeyValue2")
		lblKV3.Text = p.GetString("KeyValue3")

		lblStreetAddress.Text = p.GetString("StreetAddress")
		lblNbhd.Text = p.GetString("NbhdNo")
		lblGroup.Text = p.GetString("GroupNo")

		parcelDetails.Visible = True
		gvMultiParcel.Visible = False
		btnRecalculate.Visible = False

		hdnRecalc.Value = "1"
		chkGroup.Checked = False
		chkMatched.Checked = False
        chkSale.Checked = False
        chkEstSalePrice.Checked = False
    End Sub

	Protected Sub gvMultiParcel_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvMultiParcel.RowCommand
		Select Case e.CommandName
			Case "OpenParcel"
				OpenParcel(e.CommandArgument)
		End Select
	End Sub

    Protected Sub btnSearchComparables_Click(sender As Object, e As System.EventArgs) Handles btnSearchComparables.Click
		Try
			SearchComparables()
        Catch ex As Exception
            Dim message As String
            If TypeOf ex.InnerException Is SqlClient.SqlException Then
                Dim ix As SqlClient.SqlException = ex.InnerException
                message = ix.Message
            Else
                message = ex.Message
            End If
            updResults.Visible = False 
            divResults.Visible = False
            gvc.DataSource = Nothing
        	gvc.DataBind()
			RunScript("enableclick();")
            Alert(message)       
        End Try

    End Sub
    Sub SearchComparables()
    	Dim startTime As DateTime = DateTime.Now
    	If hdnRecalc.Value = "2" Then
    		hdnRecalc.Value = "1"
    	End If
        Dim dt As DataTable = d_("EXEC cse.GetComparablesFromCache {0}, {1}, {2}, {3}, {4}, {5}".SqlFormat(False, PID.Value, hdnRecalc.Value, chkGroup.Checked, chkMatched.Checked, chkSale.Checked, chkEstSalePrice.Checked)) 
        gvc.DataSource = dt
        gvc.DataBind()
        If dt IsNot Nothing Then
        	Dim parcelIds As String = PID.Value.ToString ()+","
        	Dim rowCount As Integer  = dt.Rows.Count - 1
        	For i=0 To rowCount 
        		parcelIds + = dt.Rows(i)(0).ToString () + ","     		
        	Next
        	hdnParcelIdList.Value =parcelIds.TrimEnd(CChar(","))
        End If
        Dim ts As TimeSpan = DateTime.Now - startTime
        If hdnRecalc.Value = "0" Then
            lblResult.Text = "Evaluated {0} comparable properties from cache".FormatString(dt.Rows.Count, ts.TotalSeconds)
        Else
            lblResult.Text = "Evaluated {0} comparable properties in {1} seconds".FormatString(dt.Rows.Count, ts.TotalSeconds)
        End If

        Dim logSql As String = "INSERT INTO cse.ComparisonLog (LoginId, ParcelId, GroupOption, AbsoluteOption, SaleOnlyOption, TotalMatches, ExecutionTime) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6})"
        Dim sql As String = logSql.SqlFormat(False, UserName, PID.Value, chkGroup.Checked, chkMatched.Checked, chkSale.Checked, dt.Rows.Count, IIf(hdnRecalc.Value = "0", -1, CInt(ts.TotalSeconds)))
        e_(sql)

	openInMap  = False
	hdnRecalc.Value = "0"
	updResults .Visible =True
	gvc.Visible =True
	divResults.Visible = True
	RunScript("enableclick();")
    End Sub
    Protected Sub btnRecalculate_Click(sender As Object, e As System.EventArgs) Handles btnRecalculate.Click
		Dim dt As DataTable = d_("EXEC cse.GetComparablesFromCache {0}, {1}, {2}, {3}".SqlFormat(False, PID.Value, 1, chkGroup.Checked, chkMatched.Checked))
		gvc.DataSource = dt
		gvc.DataBind()
		updResults .Visible =True 
		divResults.Visible = True
		
	End Sub

	Protected Sub btnRefresh_Click(sender As Object, e As System.EventArgs) Handles btnRefresh.Click
		Dim dt As DataTable = d_("EXEC cse.GetComparablesFromCache {0}, {1}, {2}, {3}".SqlFormat(False, PID.Value, 0, chkGroup.Checked, chkMatched.Checked))
		gvc.DataSource = dt
		gvc.DataBind()
		updResults .Visible =True 
		divResults.Visible = True
		
	End Sub

Protected Sub btnGenerateReport_Click(sender As Object, e As System.EventArgs) Handles btnGenerateReport.Click
	Dim url As String = "compreport.aspx?s=" + PID.Value + "&c="
	If hdnviewType.Value ="Map" Then 
		url += hdnParcelIds.Value 
	Else
		For Each gr As GridViewRow In gvc.Rows
            If gr.GetChecked("Selected") Then
                url += gr.GetHiddenValue("PID") + ","
            End If
        Next
        url = url.TrimEnd(",")
	End If
        url += "&searchparams=" + IIf(txtKeyValue1.Text = "", "0", txtKeyValue1.Text) + "," + IIf(txtKeyValue2.Text = "", "0", txtKeyValue2.Text) + "," + IIf(txtKeyValue3.Text = "", "0", txtKeyValue3.Text) + "," + IIf(chkGroup.Checked = True, "1", "0") + "," + IIf(chkMatched.Checked = True, "1", "0") + "," + IIf(chkSale.Checked = True, "1", "0") + "," + IIf(chkEstSalePrice.Checked = True, "1", "0")
        Response.Redirect(url)
    End Sub

    Protected Sub chkEstSalePrice_CheckedChanged(sender As Object, e As System.EventArgs) Handles chkGroup.CheckedChanged, chkMatched.CheckedChanged, chkSale.CheckedChanged, chkEstSalePrice.CheckedChanged
    	hdnRecalc.Value = "2"  	
        If (chkEstSalePrice.Checked = True) Then
            chkSale.Checked = True
        End If
    End Sub
    
    Public Function EvalCompPhoto() As String
        Dim columns As String = "", img As String, img2 As String = ""
        Dim parcel As DataRow = t_("SELECT TOP 1 '/imgsvr/' + Path as Imagepath FROM ParcelImages WHERE parcelId = " & Eval("ID") & " order by isprimary desc")
        If Not parcel Is Nothing Then
            img = "<img style='width: 40px; height: 20px;' src='" + parcel.GetString("Imagepath") + "' />"
            img2 = "<img style='width: 210px; height: 100px; position: absolute;' class='img2' src='" + parcel.GetString("Imagepath") + "' />"
        Else
            img = "<img style='width: 40px; height: 20px;' src='/App_Static/images/residential.png' />"
            img2 = "<img style='width: 210px; height: 100px; position: absolute;' class='img2' src='/App_Static/images/residential.png' />"
        End If
        columns += "<a class='imgover' href='#'>" + img + img2 + "</a>"
        Return columns
    End Function
End Class
