﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class cse_compreport

    '''<summary>
    '''pnlResult control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlResult As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Lb_go_back control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Lb_go_back As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''lblExport control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblExport As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''infoData control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents infoData As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''comparableList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents comparableList As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''lblUniInd control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblUniInd As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''report control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents report As Global.System.Web.UI.WebControls.Repeater

    '''<summary>
    '''footer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents footer As Global.System.Web.UI.WebControls.Repeater

    '''<summary>
    '''uifooter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents uifooter As Global.System.Web.UI.WebControls.PlaceHolder

    '''<summary>
    '''div_Sale_price control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents div_Sale_price As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''lbl_sl_label control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbl_sl_label As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lbl_salePrice control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbl_salePrice As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''hid_search_params control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hid_search_params As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hdnUniformityValue control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnUniformityValue As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hdnsalePrice control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnsalePrice As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hideUniformity control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hideUniformity As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''mapView control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents mapView As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''pnlError control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlError As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''errorMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents errorMessage As Global.System.Web.UI.HtmlControls.HtmlGenericControl
End Class
