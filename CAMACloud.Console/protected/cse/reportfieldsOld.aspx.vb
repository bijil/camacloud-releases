﻿
Partial Class cse_reportfields
    Inherits System.Web.UI.Page

	Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
		If Not IsPostBack Then
			ddlField.FillFromSql("SELECT f.Id, AssignedName FROM DataSourceField f INNER JOIN DataSourceTable t ON (f.TableId = t.Id) WHERE t.ImportType IN (0,1,3) AND CategoryId IS NOT NULL ORDER BY AssignedName", True, "-- Select Field --")
			ddlCategory.FillFromSql("SELECT Id, Category FROM cse.FieldCategory ORDER BY Ordinal", True, "-- Select Category --")
			LoadGrid()
		End If
	End Sub

	Protected Sub btnAddField_Click(sender As Object, e As System.EventArgs) Handles btnAddField.Click
		Dim sql As String = "INSERT INTO cse.ComparableFields (CategoryId, FieldId, Label) VALUES ({0}, {1}, {2})".FormatString(ddlCategory.SelectedValue, ddlField.SelectedValue, txtLabel.Text.ToSqlValue)
		Database.Tenant.Execute(sql)
		txtLabel.Text = ""
		LoadGrid()
	End Sub

	Sub LoadGrid()
		grid.DataSource = d_("SELECT f.Id, f.Label, c.Category As CategoryName, df.AssignedName As FieldName FROM cse.ComparableFields f LEFT OUTER JOIN cse.FieldCategory c ON c.Id = f.CategoryId LEFT OUTER JOIN DataSourceField df ON df.Id = f.FieldId ORDER BY f.Ordinal, c.Ordinal")
		grid.DataBind()
	End Sub

	Protected Sub grid_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grid.RowCommand
		If e.CommandName = "DeleteField" Then
			Database.Tenant.Execute("DELETE FROM cse.ComparableFields WHERE Id = " + e.CommandArgument)
			LoadGrid()
		End If
	End Sub
End Class
