﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="CAMACloud.Console.cse_compreport2" Codebehind="compreport2.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Comparables Report</title>
	<style type="text/css">
		body
		{
			margin:0px;
			padding:0px;
			font-family:Arial;
			font-size:10pt;
		}
		.vt
		{
			-moz-transform: rotate(-270deg);
			-moz-transform-origin: bottom left;
			-webkit-transform: rotate(-270deg);
			-webkit-transform-origin: top right;
			-o-transform: rotate(-270deg);
			-o-transform-origin: bottom left;
			width: 50px;
			filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=1);
		}
		
		.report-container
		{
			overflow: scroll;
			overflow-style: panner;
		}
		
		div.actions
		{
			text-align:right;
			padding-right:20px;
			margin-top:-45px;
			margin-bottom:15px;
		}
		
		div.actions a
		{
			margin-left:20px;
			font-weight:bold;
		}
		
	</style>
	<script type="text/javascript">
		function setPageDimensions(cHeight) {
			$('.report-container').height(cHeight - 46);
			$('.report-container').width($(window).width() - 325);

		}

		function exportToExcel() {
			//			var exptable = document.createElement('table');
			//			$(exptable).html($('#report-view').html());
			//			$(exptable).prepend('<caption><div style="font-size:18pt">' + $('.report-title').html() + '</div></caption>');
			var wrapper = document.createElement('div');
			$(wrapper).html($('.report-container').html());
			$('.photo-row', wrapper).html('');
			$('.photo-row', wrapper).replaceWith('');
			window.open("data:application/vnd.ms-excel;charset=utf-8," + escape($(wrapper).html()));
		}
		
	</script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>
</body>
</html>
