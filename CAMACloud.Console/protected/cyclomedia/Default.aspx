﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Default.aspx.vb" Inherits="CAMACloud.Console.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title>Cyclomedia</title>
   	 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/proj4js/2.7.4/proj4.js" ></script>
     <script type="text/javascript" src="https://unpkg.com/react@16.12.0/umd/react.production.min.js"></script>
     <script type="text/javascript" src="https://unpkg.com/react-dom@16.12.0/umd/react-dom.production.min.js"></script>
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/openlayers/4.3.3/ol.js"></script>
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.10/lodash.min.js"></script>
     <script type="text/javascript" src="https://streetsmart.cyclomedia.com/api/v23.2/StreetSmartApi.js?5c39ff70ceb1412a79ba"></script>
     
    <style>
        *[disabled] {
            opacity: 0.5;
        }
        html, body {
             height: 100%;
             overflow: none;
             background: #e5f3ff;
        }

        .panoramaViewerWindow {
             display: inline-block;
             width: 100%;
             height: 100%;
        }


        .dia {
             display: flex;
             min-width: 400px;
             position: absolute;
             min-height: 100px;
             flex-direction: column;
             max-width: 500px;
             justify-content: center;
             align-items: center;
             text-align: center;
             border-radius: 0px 15px 15px 0px;
             top: 50%;
             left: 50%;
             margin-right: -50%;
             transform: translate(-50%, -50%);
             border-radius: 10px;
             background: linear-gradient(315deg, #c5d6e6, #eaffff);
             box-shadow: -8px -8px 28px #a9b7c4, 8px 8px 28px #ffffff;
        }

        .dia-text {
             overflow: hidden;
             padding: 40px 30px 30px;
             border-radius: 10px;
             background: linear-gradient(315deg, #c5d6e6, #eaffff);
             max-width: 400px;
             margin: 20px;
             border: 2px solid #c5c5c5;
        }

        .dia-btn {
             color: white;
             width: 60px;
             height: 30px;
             margin-bottom: 20px;
             border: none;
             border-radius: 5px;
             background: linear-gradient(#065a9f,#019ade);
             /*
 			 box-shadow: inset -4px -4px 13px #3d70e4, inset 4px 4px 13px #4b88ff;
            */
         }

             .dia-btn:active {
                 border-radius: 7px;
                 background: linear-gradient(#019ade,#065a9f);
             }
        .hidden{
            display:none;
        }

    </style>
</head>
<body onload="initApi()" style="border:0px;padding:0px; margin:0px;overflow:hidden;">
<div id="streetsmartApi" class="panoramaViewerWindow"></div>
<div id="dialogue" class ='hidden'>
  <div class='dia-text'>
  There was a problem while loading the Cyclomedia.The return from the Cyclomedia Server is: <b><span id="error_msg"></span>.</b>
    
</div>
<button class='dia-btn' onclick="javascript:window.close('','_parent','');">Close</button>
</div>
<script>
    var username, password, apiKey, orgAddress;
    var parcelAddress;

    function loadProperties() {
        username = "<%=Uname%>";
        password = "<%=Password%>";
        apiKey = "<%=APIKey%>";
        orgAddress = "<%=address%>"

    }

    function initApi() {
        loadProperties();
        proj4.defs([
            ['EPSG:4326', '+title=WGS 84 (long/lat) +proj=longlat +ellps=WGS84 +datum=WGS84 +units=degrees'],
            ['EPSG:2277', '+proj=lcc +lat_1=31.88333333333333 +lat_2=30.11666666666667 +lat_0=29.66666666666667 +lon_0=-100.3333333333333 +x_0=699999.9998983998 +y_0=3000000 +datum=NAD83 +units=us-ft +no_defs ']
        ]);
        //parcelAddress= window.opener.activeParcel.StreetAddress +','+orgAddress;
        var pCentre = window.opener.activeParcel.mapCenter ? window.opener.activeParcel.mapCenter : [-73.935242, 40.730610];
        parcelAddress = proj4('EPSG:4326', 'EPSG:2277', [pCentre[1], pCentre[0]]);
        StreetSmartApi.init({
            targetElement: document.getElementById('streetsmartApi'),
            username: username,
            password: password,
            apiKey: apiKey,
            // srs: "EPSG:26918",
            srs: "EPSG:2277",
            locale: 'en-us',
            configurationUrl: 'https://atlas.cyclomedia.com/configuration',
            addressSettings: {
                locale: "en",
                database: "Nokia"
            }
        }).then(
            function() {
                //var viewerType = StreetSmartApi.ViewerType.PANORAMA
                console.log('Api: init: success!');
                StreetSmartApi.open(parcelAddress[0] + ',' + parcelAddress[1], {
                    viewerType: [StreetSmartApi.ViewerType.PANORAMA, StreetSmartApi.ViewerType.OBLIQUE],
                    //srs: 'EPSG:26918',
                    srs: "EPSG:2277",
                }).then(
                    function(result) {
                        console.log('Created component through API:', result);
                        if (result) {
                            for (let i = 0; i < result.length; i++) {
                                if (result[i].getType() === StreetSmartApi.ViewerType.PANORAMA) window.panoramaViewer = result[i];
                            }
                        }
                    }.bind(this)
                ).catch(
                    function(reason) {
                        console.log('Failed to create component(s) through API: ' + reason);
                    }
                );
            },
            function(err) {
                console.log('Api: init: failed. Error: ', err);
                // alert('Api Init Failed!');
                //  if (err && err.message.indexOf('Logging in failed') > -1)
                showDialogue(true, err.message)
                //alert(`There was a problem while loading the Cyclomedia.The return from the Cyclomedia Server is: ${err.message}.`);
            }
        );
    }
    function showDialogue(val, errMsg) {
        if (val) {
            document.getElementById('streetsmartApi').style.display = "none";
            document.getElementById("error_msg").innerText = errMsg;
            document.getElementById("dialogue").className = "dia";
        }
        else {
            document.getElementById('streetsmartApi').style.display = "block";
            document.getElementsByClassName('dia').style.display = "none";
            document.getElementById("dialogue").className = "";
        }

    }
</script>
</body>
</html>
