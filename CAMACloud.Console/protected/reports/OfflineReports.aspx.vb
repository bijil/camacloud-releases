﻿Public Class OfflineReports
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            loadGrid()
        End If

    End Sub
    Sub loadGrid()
        Dim Id = HttpContext.Current.GetCAMASession.OrganizationId
        Dim Query As String = "select case when ReportID = 1 then 'Audit Trail' when ReportID = 2 then 'Assignment Group Statistics' when ReportID = 3 then 'Visited Assignment Group Statistics' when ReportID = 4 then 'Assignment Group Remarks' when ReportID = 5 then 'Appraiser Field Activity' when ReportID = 6 then 'Parcels Visited' when ReportID = 7 then 'Productivity Statistics' when ReportID = 8 then 'Field Alerts & Messages' when ReportID = 14 then 'Approved Edits - Summary' when ReportID = 13 then 'Parcel Soft Reject Report' when ReportID = 15 then 'Field Category' when ReportID = 16 then 'Productivity Statistics Report Improved' end as ReportName,* from reportjobs where OrganizationID = " + Id.ToString() + " ORDER BY Id DESC"
        Dim dt As DataTable
        dt = Database.System.GetDataTable(Query)
        GrvReports.DataSource = dt
        GrvReports.DataBind()

    End Sub

    Protected Sub GrvReports_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GrvReports.RowCommand
        If e.CommandName = "Download" Then
            Dim id As Integer = e.CommandArgument
            Dim path As String = Database.System.GetStringValue("select path from reportjobs where id =" + id.ToString())
            Dim s3 = New S3FileManager
            Try
                s3.DownloadFile(path, IO.Path.GetFileName(path), Response)
                Response.End()
            Catch ex As Exception
                Alert(ex.Message)
            End Try

        End If
    End Sub

    Protected Sub GrvReports_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GrvReports.PageIndexChanging
        GrvReports.PageIndex = e.NewPageIndex
        loadGrid()
    End Sub
End Class