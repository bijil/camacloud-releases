﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewPRC.aspx.vb" Inherits="CAMACloud.Console.ViewPRC" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/1.4.1/html2canvas.min.js" integrity="sha512-BNaRQnYJYiPSqHHDb58B0yaPfCu+Wgds8Gp/gU33kqBtgNS4tSPHuGibyoeqMV/TJlSKda6FXzoEyYGjTe+vXA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.5.1/jspdf.umd.min.js" integrity="sha512-qZvrmS2ekKPF2mSznTQsxqPgnpkI4DNTlrdUmTzrDgektczlKNRRhy5X5AAOnx5S09ydFYWWNSfcEqDTTHgtNA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script  src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <%-- <script type="text/javascript" src="/App_Static/js/qc/010-quality.js"></script>--%>
    <title></title>
</head>
<body>
  <style>
    .EditedValue {
        color: red;
           }
    #viewPRC {
    width: 100%;
    height: auto;
}

 </style>

    <script type="text/javascript">
        var finalValue;
        var parcelid;
        $(document).ready(function () {
            var urlString = window.location.search;
            
            var parts = urlString.split('/');
            
            var lastPart = parts[parts.length - 1];
            // Remove the '.html' extension
             parcelid = parts[parts.length - 2];
             finalValue = lastPart.replace('.html', '');

             finalValue = finalValue.replace('beforeMAC', '');

            downloadPrcHtml(finalValue, function () {
                generateAndSavePDF(function () {
                    // This code will execute after the PDF generation is complete.
                    console.log("PDF generation is complete!");
                });
            });
            function downloadPrcHtml(prcId, callback) {

                $.ajax({
                    url: '/quality/downloadhtmlfile.jrq',
                    dataType: 'html',
                    data: { PrcId: prcId },
                    success: function (response) {
                        $('#viewPRC').html(response);
                        $("button[onclick='utils.MarkAsComplete()']").hide();
                        if (callback) callback();
                    },
                    error: function (xhr, status, error) {
                        console.log(error);
                        if (callback) callback();
                    }
                });
            }

    
            function generateAndSavePDF() {
                window.jsPDF = window.jspdf.jsPDF;

                var doc = new jsPDF();

                // Source HTMLElement or a string containing HTML.
                var elementHTML = document.querySelector("#viewPRC");

                // Check if the element exists and has content
                if (elementHTML) {
                    // Wait for a short time to ensure content rendering (adjust as needed)
                    setTimeout(function () {
                        doc.html(elementHTML, {
                            callback: function (doc) {
                                // Save the PDF
                                doc.save(parcelid + '-' + finalValue +'.pdf');
                                var blob = doc.output('blob');

                                // Create object URL for the blob
                                var objectURL = URL.createObjectURL(blob);

                                // Open the PDF in a new tab or window
                                window.location.href = objectURL;
                               // window.open(objectURL, '_blank');

                                // Release the object URL after opening the window
                                URL.revokeObjectURL(objectURL);
                            },
                            x: 15,
                            y: 15,
                            width: 170, //target width in the PDF document
                            windowWidth: 650 //window width in CSS pixels
                        });
                    }, 1000); // Wait for 1 second (adjust as needed)
                } else {
                    console.error("#viewPRC element not found or has no content.");
                }
            }
  //alert("Document is ready!");
        });
     
        
    </script>
<style type="text/css">
		.masklayer {
		    height: 100%;
            position: absolute;
            top:0px;
            left:0px;
            width: 100%;
            z-index: 100;
            opacity: 1;
            background-color: grey;
		}
    </style>
    <form id="form1" runat="server" >
       <%-- <div><button id="printPDf" text="Print" onclick="Popup( $('#viewPRC').html(), null, 'DigitalPRC');">Print</button></div>--%>
        <div id="viewPRC" visible="false">
        </div>
      <div class="masklayer" style="z-index: 20; height: 100%; display: block;">
        <span style="margin-left: 47%; margin-top: 22%; float:left">
            <img src="/App_Static/css/images/comploader.gif">
            <span style="color: wheat; float: right; margin-left: 12px; margin-top: 20px;" class="app-mask-layer-info">Please wait ...</span></span>
    </div>
    </form>
    <div> </div>
</body>
</html>
