﻿Imports Microsoft.Reporting.WebForms
Imports System.Net
Imports System.Net.Mail
Imports System.Data.SqlClient
Imports System.IO

Partial Class reporting_Default
    Inherits System.Web.UI.Page
    Public org As DataRow
    Public offline As String = "0"
    Public Shared Property reportDatasource As DataTable
    Public Shared Property reportfileName As String = ""
    Sub LoadReportsHome()
        Dim xsql = <sql>
                SELECT ar.Id,  Name + ' Reports' As GroupTitle
                FROM AppRoles ar
                INNER JOIN OrganizationRoles orr ON ar.Id = orr.RoleId
                WHERE orr.OrganizationId = {0} AND (RoleId IN (SELECT RoleId FROM Reports WHERE RoleId != 8))
                AND ar.ASPRoleName IN ({1})
                ORDER BY Ordinal
            </sql>
        Dim sqlRoles As String = Roles.GetRolesForUser().Select(Function(x) x.ToSqlValue).Aggregate(Function(x, y) x + "," + y)
        Dim sql As String = xsql.Value.FormatString(HttpContext.Current.GetCAMASession.OrganizationId, sqlRoles)
        rptGroups.DataSource = Database.System.GetDataTable(sql)
        rptGroups.DataBind()
    End Sub
    Public Function HasDTRModule() As Boolean
        Return CAMACloud.ClientOrganization.HasModule(HttpContext.Current.GetCAMASession.OrganizationId, "DTR")
    End Function

    Sub LoadReportSelection()
        'Dim dt As New DataTable
        'dt.ReadXml(Server.MapPath("config/Reports.xml"))
        Dim response As XElement = XElement.Load(Server.MapPath("config/Reports.xml"))
        Dim BppEnabled As DataRow = Database.Tenant.GetTopRow("SELECT BPPEnabled FROM Application")
        Dim MaLiteEnabled = ClientSettings.PropertyValue("EnableMALite")
        Dim ProStaticV2 = Database.Tenant.GetStringValue("select Value FROM ClientSettings WHERE Name ='ProductivityStaticReportV2'")
        If Not HasDTRModule() Then
            If Not (BppEnabled.GetString("BPPEnabled") = "1" Or BppEnabled.GetString("BPPEnabled") = "True" Or BppEnabled.GetString("BPPEnabled") = "true") Then
                Dim temp = From uu In response.Descendants("Reports") Where uu.Element("RoleId") = "2" Select Name = uu.Element("Name").Value, ID = uu.Element("Id").Value, Description = uu.Element("Description").Value.Replace("\n", "<br />").Replace("\b", "<b>").Replace("/b", "</b>") Where ID <> 9 And ID <> 10 And ID <> 11 And ID <> 12 And ID <> 14
                ddlReport.DataSource = temp.ToList
            Else
                Dim temp = From uu In response.Descendants("Reports") Where uu.Element("RoleId") = "2" Select Name = uu.Element("Name").Value, ID = uu.Element("Id").Value, Description = uu.Element("Description").Value.Replace("\n", "<br />").Replace("\b", "<b>").Replace("/b", "</b>") Where ID <> 9 And ID <> 10 And ID <> 11 And ID <> 14 
                ddlReport.DataSource = temp.ToList
            End If
            If Not (MaLiteEnabled = "1" Or MaLiteEnabled = "True" Or MaLiteEnabled = "true" )  Then
	              If Not (BppEnabled.GetString("BPPEnabled") = "1" Or BppEnabled.GetString("BPPEnabled") = "True" Or BppEnabled.GetString("BPPEnabled") = "true") Then
	                 Dim temp = From uu In response.Descendants("Reports") Where uu.Element("RoleId") = "2" Select Name = uu.Element("Name").Value, ID = uu.Element("Id").Value, Description = uu.Element("Description").Value.Replace("\n", "<br />").Replace("\b", "<b>").Replace("/b", "</b>") Where ID <> 9 And ID <> 10 And ID <> 11 And ID <> 12 And ID <> 14 
	                 ddlReport.DataSource = temp.ToList
	              Else
	                 Dim temp = From uu In response.Descendants("Reports") Where uu.Element("RoleId") = "2" Select Name = uu.Element("Name").Value, ID = uu.Element("Id").Value, Description = uu.Element("Description").Value.Replace("\n", "<br />").Replace("\b", "<b>").Replace("/b", "</b>") Where ID <> 9 And ID <> 10 And ID <> 11 And ID <> 14 
	                 ddlReport.DataSource = temp.ToList
	              End If
            Else
            	  If Not (BppEnabled.GetString("BPPEnabled") = "1" Or BppEnabled.GetString("BPPEnabled") = "True" Or BppEnabled.GetString("BPPEnabled") = "true") Then
	                 Dim temp = From uu In response.Descendants("Reports") Where uu.Element("RoleId") = "2" Select Name = uu.Element("Name").Value, ID = uu.Element("Id").Value, Description = uu.Element("Description").Value.Replace("\n", "<br />").Replace("\b", "<b>").Replace("/b", "</b>") Where ID <> 9 And ID <> 10 And ID <> 11 And ID <> 12  
	                 ddlReport.DataSource = temp.ToList
	              Else
	                 Dim temp = From uu In response.Descendants("Reports") Where uu.Element("RoleId") = "2" Select Name = uu.Element("Name").Value, ID = uu.Element("Id").Value, Description = uu.Element("Description").Value.Replace("\n", "<br />").Replace("\b", "<b>").Replace("/b", "</b>") Where ID <> 9 And ID <> 10 And ID <> 11 
	                 ddlReport.DataSource = temp.ToList
	              End If
            End If

        Else
            If Not (BppEnabled.GetString("BPPEnabled") = "1" Or BppEnabled.GetString("BPPEnabled") = "True" Or BppEnabled.GetString("BPPEnabled") = "true") Then
                Dim temp1 = From uu In response.Descendants("Reports") Where uu.Element("RoleId") = "2" Select Name = uu.Element("Name").Value, ID = uu.Element("Id").Value, Description = uu.Element("Description").Value.Replace("\n", "<br />").Replace("\b", "<b>").Replace("/b", "</b>") Where ID <> 12
                ddlReport.DataSource = temp1.ToList
            Else
                Dim temp1 = From uu In response.Descendants("Reports") Where uu.Element("RoleId") = "2" Select Name = uu.Element("Name").Value, ID = uu.Element("Id").Value, Description = uu.Element("Description").Value.Replace("\n", "<br />").Replace("\b", "<b>").Replace("/b", "</b>") 
                ddlReport.DataSource = temp1.ToList
            End If
        End If

        ddlReport.DataTextField = "Name"
        ddlReport.DataValueField = "ID"
        ddlReport.DataBind()
        If ProStaticV2 <> "1" Then
            ddlReport.Items.Remove(ddlReport.Items.FindByText("Productivity Statistics Report Improved"))
        End If
        ddlReport.Items.Insert(0, New ListItem("-- Select --", ""))
        Dim PrcReportenableddl = Database.System.GetStringValue("Select PRCReport FROM OrganizationSettings WHERE OrganizationId = " & HttpContext.Current.GetCAMASession().OrganizationId)
        If PrcReportenableddl <> "True" Then
            ddlReport.Items.Remove(ddlReport.Items.FindByText("PRC Report"))
        End If
        Dim enableNewPriority = Database.Tenant.GetStringValue("Select Value FROM ClientSettings WHERE Name = 'EnableNewPriorities'")
        If enableNewPriority = "1" Then
            ddlReport.Items.Remove(ddlReport.Items.FindByText("Assignment Group Statistics"))
            ddlReport.Items.Remove(ddlReport.Items.FindByText("Productivity Statistics"))
            ddlReport.Items.Remove(ddlReport.Items.FindByText("Visited Assignment Group Statistics"))
        Else
            ddlReport.Items.Remove(ddlReport.Items.FindByText("Assignment Group Statistics New Priority"))
            ddlReport.Items.Remove(ddlReport.Items.FindByText("Productivity Statistics New Priority"))
            ddlReport.Items.Remove(ddlReport.Items.FindByText("Visited Assignment Group Statistics New Priority"))
        End If
        '  ddlReport.FillFromSqlWithDatabase(Database.System, "SELECT Id, Name FROM Reports", True)
    End Sub
    Sub LoadReport(reportId As Integer)
        Dim ShowOnGridFooterList As New ArrayList
        ShowOnGridFooterList.Add("Completed")
        Dim response As XElement = XElement.Load(Server.MapPath("config/Reports.xml"))
        Dim orgid = HttpContext.Current.GetCAMASession.OrganizationId

        'get the object of type 'node'

        'this code gives the results expected
        ' in the debugger each XElement appears to have the proper value and childElements
        '    Dim jj = From node In response.Descendants("Reports") Where node.Attribute("id").Value = reportId
        Dim reportPath, dataSetName, dataSource, reportTitle, reportType As String
        Dim reportParameters As New NameValueCollection
        ' Dim dr As DataRow = Database.System.GetTopRow("SELECT * FROM Reports WHERE Id = " & reportId)
        'Dim reportPath As String = dr.GetString("ReportPath")
        'Dim dataSetName As String = dr.GetString("DataSetName")
        'Dim dataSource As String = dr.GetString("DataSource")
        'Dim reportTitle As String = dr.GetString("ReportTitle")
        Dim temp = From uu In response.Elements("Reports") From yy In uu.Elements("Id") Where yy.Value = reportId.ToString Select uu
        ' For Each pp As XElement In temp.Elements
        ' Dim c As String = temp.Elements("Reports").Elements("Id").Value
        reportPath = temp.Elements("ReportPath").Value
        dataSetName = temp.Elements("DataSetName").Value
        dataSource = temp.Elements("DataSource").Value
        reportTitle = temp.Elements("ReportTitle").Value
        reportType = temp.Elements("ReportType").Value
        ' Next


        'Dim c As String = temp.Elements("Reports").Elements("Id").Value
        'Dim reportPath As String = temp.First().Element("ReportPath").Value
        'Dim dataSetName As String = temp.First().Element("DataSetName").Value
        'Dim dataSource As String = temp.First().Element("DataSource").Value
        'Dim reportTitle As String = temp.First().Element("ReportTitle").Value
        Dim reportData As DataTable
        Dim user = Membership.GetUser()
        Dim email = user.Email
        If (reportType = "Custom Report") Then
            Dim parameters As New NameValueCollection
            Dim params As New DataTable
            Dim parameter As String = ""
            params = Database.Tenant.GetDataTable("SELECT * FROM CustomReportParameters WHERE ReportId = " & reportId)
            Dim Count As Integer = 0
            For Each pr As DataRow In params.Rows
                Dim name As String = pr.GetString("Name")
                Dim inputType As Integer = pr.GetIntegerWithDefault("InputType", 0)
                Dim required As String = pr.GetBoolean("Required")
                Dim source As String = pr.GetString("Source")
                Dim value As String = ""
                If (source <> "$QUERY$") Then
                    Dim fc As WebControl = ph.FindControl("report_filter_" + name)
                    Select Case inputType
                        Case 0
                            value = CType(fc, TextBox).Text
                        Case 1
                            value = CType(fc, DropDownList).SelectedValue
                        Case 2
                            value = CType(fc, TextBox).Text
                        Case 3
                            value = CType(fc, CheckBox).Checked.GetHashCode
                        Case 4
                            value = CType(fc, RadioButtonList).SelectedValue
                        Case 5
                            Count = Count + 1
                            Dim strID As String = "gv_" & name & reportId
                            Dim gv As GridView = CType(fc, Panel).FindControl(strID)
                            Dim selecteColumnIds As String = ""
                            If (Not gv Is Nothing) Then
                                'grid
                                If (gv.Rows.Count > 0) Then
                                    For Each row As GridViewRow In gv.Rows
                                        Dim chkRow As CheckBox = DirectCast(row.Cells(0).Controls(0), CheckBox)
                                        If chkRow.Checked Then
                                            Dim selecteColumnId As String = ""
                                            Dim selecteColumnName As String = ""
                                            selecteColumnId = WebUtility.HtmlDecode(row.Cells(1).Text).Trim
                                            selecteColumnName = WebUtility.HtmlDecode(row.Cells(2).Text)
                                            ShowOnGridFooterList.Add(selecteColumnName)
                                            If (selecteColumnIds.Length > 0) Then
                                                selecteColumnIds = selecteColumnIds + "," + selecteColumnId
                                            Else
                                                selecteColumnIds = selecteColumnIds + selecteColumnId
                                            End If
                                        End If
                                    Next
                                End If
                                If (parameter = "") Then
                                    parameter = name + "$" + selecteColumnIds
                                Else
                                    parameter = parameter + "|" + name + "$" + selecteColumnIds
                                End If
                            Else
                                'lbl
                            End If

                    End Select
                    If (inputType <> 5) Then
                        If value = "" Then
                            parameters.Add("@" + name, Nothing)
                        Else
                            parameters.Add("@" + name, value)
                        End If
                    End If
                End If
            Next
            If (Count > 0) Then
                If parameter = "" Then
                    parameters.Add("@FieldNameWithCodes", Nothing)
                Else
                    parameters.Add("@FieldNameWithCodes", parameter)
                End If
            End If
            Try
                reportData = Database.Tenant.GetDataTable(dataSource, CommandType.Text, parameters, 3000)
            Catch ex As Exception
                If ex.Message.Contains("OutOfMemoryException") Then
                    reportsHome.Visible = False
                    lblnoFile.Text = "Sorry, it appears there is too much data matching your criteria. Please adjust your search parameters and try again."
                    pnl_nofile.Visible = True
                    reportViewerPanel.Visible = False
                    customReportPanel.Visible = False
                Else
                    lblnoFile.Text = "Sorry, it appears there is no data matching your criteria. Please adjust your search parameters and try again."
                    reportsHome.Visible = False
                    pnl_nofile.Visible = True
                    reportViewerPanel.Visible = False
                    customReportPanel.Visible = False
                End If
                Exit Sub
            End Try

        Else
            Dim parameters As New NameValueCollection
            Dim params As XElement = XElement.Load(Server.MapPath("config/ReportParameters.xml"))
            Dim temp2 = From uu In params.Elements("ReportParameters") From yy In uu.Elements("ReportId") Where yy.Value = reportId.ToString Select uu
            Dim paramstring As String = ""
            For Each xe As XElement In temp2
                Dim name As String = xe.Element("Name").Value 'pr.GetString("Name")
                Dim inputType As Integer = xe.Element("InputType").Value ' pr.GetIntegerWithDefault("InputType", 0)
                Dim required As String = xe.Element("Required").Value ' pr.GetBoolean("Required")
                Dim value As String = ""
                If name = "Alternate" Then
                    Continue For
                End If
                Dim fc As WebControl = ph.FindControl("report_filter_" + name)
                If paramstring <> "" And name <> "OfflineReport" And name <> "Email" Then
                    paramstring += ","
                End If
                If fc IsNot Nothing Then
                    Select Case inputType
                        Case 0
                            value = CType(fc, TextBox).Text
                        Case 1
                            value = CType(fc, DropDownList).SelectedValue
                        Case 2
                            value = CType(fc, TextBox).Text
                        Case 3
                            value = CType(fc, CheckBox).Checked.GetHashCode
                        Case 4
                            value = CType(fc, RadioButtonList).SelectedValue
                    End Select
                End If

                If name = "OfflineReport" Then
                    offline = value
                End If

                If value = "" Then
                    If Not IsNothing(Page.PreviousPage) Or Not String.IsNullOrEmpty(Request.QueryString("oldReportPId")) Then
                        If Not String.IsNullOrEmpty(Request.QueryString("oldReportPId")) AndAlso name = "KeyValue1" Then
                            value = IIf(Request.QueryString("plKeyVal") Is Nothing, "", Request.QueryString("plKeyVal"))
                            Dim _Alternate As String = IIf(Request.QueryString("plAltVal") Is Nothing, "", Request.QueryString("plAltVal"))
                            Dim Script As String = "$('#LeftContent_report_filter_KeyValue1').val(""" + value.ToString() + """);"
                            Script += "$('#LeftContent_report_filter_Alternate').val(""" + _Alternate.ToString() + """);"
                            Page.ClientScript.RegisterStartupScript(Me.GetType(), "keyval", Script, True)
                            parameters.Add("@" + name, value)
                            reportParameters.Add(name, value)
                            If name <> "OfflineReport" And name <> "Email" Then
                                paramstring += "@" + name + " =" + value + "=" + inputType.ToString()
                            End If
                        ElseIf name = "KeyValue1" Then
                            value = GetKeyValueFromQC()
                            parameters.Add("@" + name, value)
                            reportParameters.Add(name, value)
                            If name <> "OfflineReport" And name <> "Email" Then
                                paramstring += "@" + name + " =" + value + "=" + inputType.ToString()
                            End If
                        ElseIf name = "EndDate" AndAlso Request.QueryString("old") IsNot Nothing AndAlso Request.QueryString("old") = "1" Then
                            Dim dt As Date = DateTime.Now.AddYears(-3)
                            value = dt.ToString("yyyy-MM-dd")
                            Dim Script As String = "$('#LeftContent_report_filter_EndDate').val(""" + value + """);"
                            Page.ClientScript.RegisterStartupScript(Me.GetType(), "EndDate", Script, True)
                            parameters.Add("@" + name, value)
                            reportParameters.Add(name, value)
                            If name <> "OfflineReport" And name <> "Email" Then
                                paramstring += "@" + name + " =" + value + "=" + inputType.ToString()
                            End If
                        End If

                    End If
                    parameters.Add("@" + name, Nothing)
                    reportParameters.Add(name, "")
                    If name <> "OfflineReport" And name <> "Email" Then
                        paramstring += "@" + name + " =" + value + "=" + inputType.ToString()
                    End If

                Else
                    parameters.Add("@" + name, value)
                    reportParameters.Add(name, value)
                    If name <> "OfflineReport" And name <> "Email" Then
                        paramstring += "@" + name + " =" + value + "=" + inputType.ToString()
                    End If
                    If name = "Email" Then
                        If value.Trim.ToString() <> "" Then
                        	email = value.Trim.ToString()
                        	If email.Length() > 100 Then
                        		email = email.Remove(99)
                        	End If
                        End If
                    End If
                End If
            Next
            Dim control As WebControl = ph.FindControl("report_filter_Alternate")
            Dim altValue As String = ""
            If control IsNot Nothing Then
                altValue = CType(control, TextBox).Text

            End If
            If altValue = "" Then
                If Not IsNothing(Page.PreviousPage) Then
                    altValue = GetAlternateFromQC()
                End If

            End If
            If reportId = 1 Or reportId = 11 Or reportId = 13 Or reportId = 14 Or reportId = 17 Or reportId = 23 Then
                parameters.Add("@Alternate", altValue)
                reportParameters.Add("Alternate", altValue)
                paramstring += ",@Alternate=" + altValue + "=0"
                Dim disPlayCntrl As WebControl = ph.FindControl("report_filter_DisplayOptn")
                Dim DisOptn As String = ""
                If disPlayCntrl IsNot Nothing Then
                    DisOptn = CType(disPlayCntrl, DropDownList).SelectedValue
                    reportParameters.Add("DisplayOptn", DisOptn)
                End If
            End If

            If reportId = 1 Then
                Dim EnableInternalView = 0
                Dim dr As DataRow = Database.System.GetTopRow("SELECT * FROM OrganizationSettings WHERE EnableInternalView = 1 AND OrganizationId = " + HttpContext.Current.GetCAMASession().OrganizationId.ToString())
                If dr IsNot Nothing Then
                    EnableInternalView = 1
                End If
                parameters.Add("@InternalView", EnableInternalView)
            End If

            'If control IsNot Nothing Then
            '    Dim value As String = CType(control, TextBox).Text
            '    If value = "" Then
            '        'If name = "Alternate" Then

            '        '    parameters.Add("@" + name, value)
            '        '    reportParameters.Add(name, value)
            '        '    If name <> "OfflineReport" And name <> "Email" Then
            '        '        paramstring += "@" + name + " =" + value + "=" + inputType.ToString()
            '        '    End If
            '        'End If
            '        value = GetAlternateFromQC()
            '    End If
            '    parameters.Add("@Alternate", value)
            '    reportParameters.Add("Alternate", value)
            '    paramstring += "@Alternate=" + value + "=0"
            'End If
            If offline = "1" Then
                Dim query As String = "insert into Reportjobs(ReportID, Query, OrganizationID, email, RequestedBy, status) values({0},{1},{2},{3},{4},{5})".SqlFormatString(reportId, paramstring, orgid, email, user.ToString(), 0)
                Database.System.Execute(query)
                reportData = Nothing
                Exit Sub
            Else
                Try
                    viewer.LocalReport.DataSources.Clear()
                    viewer.Reset()
                    Dim RejectedPerNeighborhoodDataTable As DataTable
                    Dim TotalParcelDataTable As DataTable
                    Dim MacDataTable As DataTable
                    Dim QcDataTable As DataTable
                    If reportId = 13 Then
                        RejectedPerNeighborhoodDataTable = Database.Tenant.GetDataTable("EXEC report_TotaLParcelsSoftRejectedPerNeighborhood @MACuser,@QCUser,@StartDate,@EndDate,@NeighborhoodId,@KeyValue1,@Alternate", CommandType.Text, parameters, 3000)
                        Dim nrs As New ReportDataSource("TotalParcelSoftRejectPerNbhd", RejectedPerNeighborhoodDataTable)
                        viewer.LocalReport.DataSources.Add(nrs)
                        TotalParcelDataTable = Database.Tenant.GetDataTable("EXEC SP_TotalParcelsSoftRejected @MACuser,@QCUser,@StartDate,@EndDate,@NeighborhoodId,@KeyValue1,@Alternate", CommandType.Text, parameters, 3000)
                        Dim thirdrs As New ReportDataSource("TotalParcelCount", TotalParcelDataTable)
                        viewer.LocalReport.DataSources.Add(thirdrs)
                        MacDataTable = Database.Tenant.GetDataTable("EXEC SP_TotalParcelsSoftRejectedByMAC @MACuser,@QCUser,@StartDate,@EndDate,@NeighborhoodId,@KeyValue1,@Alternate", CommandType.Text, parameters, 3000)
                        Dim Macrs As New ReportDataSource("TotalParcelsRejectedByMAC", MacDataTable)
                        viewer.LocalReport.DataSources.Add(Macrs)
                        QcDataTable = Database.Tenant.GetDataTable("EXEC SP_TotalParcelsSoftRejectedByQC @MACuser,@QCUser,@StartDate,@EndDate,@NeighborhoodId,@KeyValue1,@Alternate", CommandType.Text, parameters, 3000)
                        Dim Qcrs As New ReportDataSource("TotalParcelsByQC", QcDataTable)
                        viewer.LocalReport.DataSources.Add(Qcrs)
                    End If
                    reportData = Database.Tenant.GetDataTable(dataSource, CommandType.Text, parameters, 3000)

                Catch ex As Exception
                    If ex.Message.Contains("OutOfMemoryException") Then
                        reportsHome.Visible = False
                        lblnoFile.Text = "Sorry, it appears there is too much data matching your criteria. Please adjust your search parameters and try again."
                        pnl_nofile.Visible = True
                        reportViewerPanel.Visible = False
                        customReportPanel.Visible = False
                    Else
                        lblnoFile.Text = "Sorry, it appears there is no data matching your criteria. Please adjust your search parameters and try again."
                        reportsHome.Visible = False
                        pnl_nofile.Visible = True
                        reportViewerPanel.Visible = False
                        customReportPanel.Visible = False
                    End If
                    Exit Sub
                End Try

            End If
        End If

        If (reportData.Rows.Count <= 0) Then
            reportsHome.Visible = False
            pnl_nofile.Visible = True
            reportViewerPanel.Visible = False
            customReportPanel.Visible = False
            Exit Sub
        End If

        If (reportType = "Custom Report") Then
            reportsHome.Visible = False
            pnl_nofile.Visible = False
            reportViewerPanel.Visible = False
            customReportPanel.Visible = True
            uc_CustomReport.showReport()
            uc_CustomReport.GenerateGrid(reportData, reportTitle, ShowOnGridFooterList)
        Else
            Try
                customReportPanel.Visible = False
                viewer.LocalReport.ReportPath = reportPath
                viewer.LocalReport.Refresh()

                If dataSetName = "" Then
                    For Each ds In viewer.LocalReport.GetDataSourceNames
                        dataSetName = ds
                    Next
                End If
                totalRecords.Value = reportData.Rows.Count

                Dim rs As New ReportDataSource(dataSetName, reportData)

                viewer.ProcessingMode = ProcessingMode.Local

                viewer.LocalReport.DataSources.Add(rs)
                org = Database.System.GetTopRow("Select * FROM Organization WHERE Id = " & HttpContext.Current.GetCAMASession().OrganizationId)
                Dim orgName As String = org.GetString("Name").ToLower()
                viewer.LocalReport.DisplayName = orgName + "-" + reportTitle
                reportfileName = viewer.LocalReport.DisplayName

                If reportId = 14 Or reportId = 17 Then
                    If Database.Tenant.Application.ShowAlternateField = True Then
                        Dim altField = Database.Tenant.Application.AlternateKeyfield
                        Dim parcelTable = Database.Tenant.Application.ParcelTable
                        Dim altFieldname As String = Database.Tenant.GetStringValue("SELECT DisplayLabel FROM DataSourceField WHERE Name ={0} And SourceTable ={1}".SqlFormatString(altField, parcelTable))
                        reportParameters.Add("AltFld", altFieldname + ":")
                    End If
                    If Database.Tenant.Application.ShowKeyValue1 = True Then
                        reportParameters.Add("KeyFld", 1)
                    End If

                    Dim key As String
                    Dim values() As String
                    For Each key In reportParameters.Keys
                        If key = "OfflineReport" Or key = "Email" Then
                            Continue For
                        End If
                        values = reportParameters.GetValues(key)
                        For Each value As String In values
                            Dim parameter As New ReportParameter
                            parameter.Name = key
                            If key = "LoginId" Then
                                value = IIf(value = "", value, Database.Tenant.GetStringValue("Select FirstName +' '+  ISNULL( LastName,'')  FROM UserSettings WHERE LoginId={0}".SqlFormatString(value)))
                            End If
                            If key = "IgnoreCustomField" Then
                                value = IIf(value = "1", "Yes", "No")
                            End If
                            If key = "Action" Then
                                If value = "0" Then
                                    value = "New"
                                ElseIf value = "1" Then
                                    value = "Edit"
                                ElseIf value = "2" Then
                                    value = "Delete"
                                Else
                                    value = "--"
                                End If
                            End If
                            If key = "NeighborhoodId" Then
                                value = IIf(value = "", "--", Database.Tenant.GetStringValue("SELECT Name FROM Neighborhood WHERE Id = {0}".SqlFormatString(value)))
                            End If
                            If value = "" Then
                                value = "--"
                            End If

                            parameter.Values.Add(value)

                            viewer.LocalReport.Refresh()
                            viewer.LocalReport.SetParameters(parameter)



                        Next value
                    Next key
                Else
                    Dim param As New ReportParameter
                    param.Name = "ReportTitle"
                    param.Values.Add(reportTitle)
                    viewer.LocalReport.Refresh()
                    viewer.LocalReport.SetParameters(param)
                End If
            Catch ex As Exception

            End Try

            reportsHome.Visible = False
            reportViewerPanel.Visible = True
            pnl_nofile.Visible = False
        End If
    End Sub
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadReportsHome()
            rfv_ddlReport.ErrorMessage = " *"
            reportViewerPanel.Visible = False
            pnl_nofile.Visible = False
            'View Audit Trail Report from QC 
            If Not IsNothing(Page.PreviousPage) Or Not String.IsNullOrEmpty(Request.QueryString("oldReportPId")) Then
                ddlReport.SelectedValue = 1
                GetReportParameters(1)
                LoadReport(ddlReport.SelectedValue)
            End If
        End If
        If ddlReport.SelectedIndex > 0 And HasFilters.Value = "1" Then
            reportParams.Visible = True
            lblReportParameters.Visible = True
        Else
            reportParams.Visible = False
            lblReportParameters.Visible = False
        End If

    End Sub
    Function GetKeyValueFromQC()
        Dim _KeyValue1 As String = CType((PreviousPage.FindControl("ctl00$ctl00$MainContent$MainContent$hdnKeyValue1")), HiddenField).Value
        Dim _Alternate As String = CType((PreviousPage.FindControl("ctl00$ctl00$MainContent$MainContent$hdnAlternate")), HiddenField).Value
        Dim Script As String = "$('#LeftContent_report_filter_KeyValue1').val(""" + _KeyValue1.ToString() + """);"
        Script += "$('#LeftContent_report_filter_Alternate').val(""" + _Alternate.ToString() + """);"
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "keyval", Script, True)
        Return _KeyValue1
    End Function
    Function GetAlternateFromQC()
        Dim _Alternate As String = CType((PreviousPage.FindControl("ctl00$ctl00$MainContent$MainContent$hdnAlternate")), HiddenField).Value
        'Dim Script As String = "$('#LeftContent_report_filter_Alternate').val(""" + _Alternate.ToString() + """);"
        'Page.ClientScript.RegisterStartupScript(Me.GetType(), "", Script, True)
        Return _Alternate
    End Function


    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles Me.Init
        If Not IsPostBack Then
            LoadReportSelection()
            ph.Controls.Clear()
            reportParams.Visible = False
            lblReportParameters.Visible = False
            pnl_nofile.Visible = False
            'reportViewerPanel.Visible = False
            'reportsHome.Visible = True
        Else
            reportParams.Visible = True
            lblReportParameters.Visible = True
        End If

        Dim srid As String = Request("ctl00$LeftContent$ddlReport")
        If srid Is Nothing OrElse srid = "" Then
            ph.Controls.Clear()
            ph.Controls.Add(New LiteralControl("No parameters"))
            reportParams.Visible = False
            lblReportParameters.Visible = False
        Else
            ph.Controls.Clear()
            ph.EnableViewState = "false"
            reportParams.Visible = True
            lblReportParameters.Visible = True
            Dim rid As Integer = srid
            Dim reportType As String
            Dim response As XElement = XElement.Load(Server.MapPath("config/Reports.xml"))
            Dim temp = From uu In response.Elements("Reports") From yy In uu.Elements("Id") Where yy.Value = rid.ToString Select uu
            reportType = temp.Elements("ReportType").Value
            If (reportType <> "Custom Report") Then
                rid = Request("ctl00$LeftContent$ddlReport")
                GetReportParameters(rid)
            Else
                'GetInputControl From CustomReportParameters Table
                Dim dt As New DataTable
                Database.Tenant.Execute("EXEC reports_GenerateCustomReportParameters @ReportId={0}".SqlFormatString(rid))
                dt = Database.Tenant.GetDataTable("SELECT * FROM CustomReportParameters where ReportId={0}".SqlFormatString(rid))
                If (dt.Rows.Count > 0) Then
                    For Each row As DataRow In dt.Rows
                        Dim name As String = row("Name").ToString()
                        Dim label As String = row("Label").ToString()
                        Dim inputType As Integer = row("InputType").ToString()
                        Dim source As String = row("Source").ToString()
                        Dim required As String = row("Required").ToString()
                        Dim Validationfield As String = row("Validationfield").ToString()
                        Dim ValidationOperator As String = row("ValidationOperator").ToString()
                        If (source <> "$QUERY$") Then
                            Dim ip As Panel = GetInputControl(name, label, inputType, source, Validationfield, ValidationOperator, required, rid)
                            ph.Controls.Add(ip)
                        End If
                    Next
                    HasFilters.Value = 1
                Else
                    ph.Controls.Add(New LiteralControl("No parameters"))
                    HasFilters.Value = 0
                End If
            End If
        End If
    End Sub

    Protected Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        RunScript("setScreenDimensions();")
    End Sub
    Sub GetReportParameters(rid As String)
        ph.Controls.Clear()
        ph.EnableViewState = "false"
        reportParams.Visible = True
        lblReportParameters.Visible = True
        Dim reportType As String
        Dim response As XElement = XElement.Load(Server.MapPath("config/Reports.xml"))
        Dim temp = From uu In response.Elements("Reports") From yy In uu.Elements("Id") Where yy.Value = rid.ToString Select uu
        reportType = temp.Elements("ReportType").Value
        Dim params As XElement = XElement.Load(Server.MapPath("config/ReportParameters.xml"))
        Dim temp2 = From uu In params.Elements("ReportParameters") From yy In uu.Elements("ReportId") Where yy.Value = rid.ToString Select uu
        For Each xe As XElement In temp2
            'Dim name As String = xe.Element("Name").Value 'pr.GetString("Name")
            'Dim inputType As Integer = xe.Element("InputType").Value ' pr.GetIntegerWithDefault("InputType", 0)
            'Dim required As String = xe.Element("Required").Value ' pr.GetBoolean("Required")
            'Dim value As String = ""
            'Dim fc As WebControl = ph.FindControl("report_filter_" + name)
            Dim name As String = xe.Element("Name")
            Dim label As String = xe.Element("Label")
            Dim inputType As Integer = xe.Element("InputType")
            Dim source As String = xe.Element("Source")
            Dim required As String = xe.Element("Required")
            Dim Validationfield As String = xe.Element("Validationfield")
            Dim ValidationOperator As String = xe.Element("ValidationOperator")
            If name = "Alternate" Then
                If (Database.Tenant.Application.ShowAlternateField = True) Then
                    Dim altfield As String = Database.Tenant.Application.AlternateKeyfield
                    Dim parceltable As String = Database.Tenant.Application.ParcelTable
                    Dim field As String = Database.Tenant.GetStringValue("SELECT DisplayLabel FROM DataSourcefield where Name ={0} AND SourceTable ={1}".SqlFormatString(altfield, parceltable))
                    Dim ip As Panel = GetInputControl(name, field, inputType, source, Validationfield, ValidationOperator, required, rid)
                    ph.Controls.Add(ip)
                End If
            ElseIf name = "KeyValue1" Then

                If (Database.Tenant.Application.ShowKeyValue1 = True) Then
                    Dim ip As Panel = GetInputControl(name, label, inputType, source, Validationfield, ValidationOperator, required, rid)
                    ph.Controls.Add(ip)
                End If
            ElseIf name = "ReviewedOn" Then
                If HasDTRModule()Then
                    Dim ip As Panel = GetInputControl(name, label, inputType, source, Validationfield, ValidationOperator, required, rid)
                    ph.Controls.Add(ip)
                End If
            Else
                Dim ip As Panel = GetInputControl(name, label, inputType, source, Validationfield, ValidationOperator, required, rid)
                ph.Controls.Add(ip)
            End If

        Next
        If temp2.Count = 0 Then
            ph.Controls.Add(New LiteralControl("No parameters"))
            HasFilters.Value = 0
        Else
            HasFilters.Value = 1
        End If
    End Sub
    Function GetInputControl(name As String, label As String, type As Integer, source As String, ByVal Validationfield As String, ByVal ValidationOperator As String, Optional mandatory As Boolean = False, Optional reportId As String = Nothing) As Panel
        Dim container As New Panel
        Dim labelContainer As New Panel
        Dim labelSpan As New Label
        labelSpan.Text = label
        labelContainer.Controls.Add(labelSpan)
        labelContainer.CssClass = "b"
        container.Controls.Add(labelContainer)

        Dim c As WebControl = New TextBox
        Select Case type
            Case 0
                c = New TextBox
                c.Width = New Unit(214)
                If name = "Email" Then
                	Dim _validatorFun As String = "_emailValidate(LeftContent_report_filter_" + name + ")"
                	c.Attributes.Add("onblur", _validatorFun)
                End If
           
            Case 1
                c = New DropDownList
                CType(c, DropDownList).FillFromSqlWithDatabase(Database.Tenant, source, True)
                c.Width = New Unit(220)
            Case 2
                c = New TextBox
                c.Attributes.Add("type", "date")
                c.Width = New Unit(214)
                Dim validator As String = "dateValidate(LeftContent_report_filter_" + name + ")"
                c.Attributes.Add("onblur", validator)
            Case 3
                c = New CheckBox
                CType(c, CheckBox).Text = label
                labelContainer.Visible = False
                c.Width = New Unit(220)
                mandatory = False
            Case 4
                c = New RadioButtonList
                CType(c, RadioButtonList).Items.Add("Date Ascending")
                CType(c, RadioButtonList).Items.Add("Date Descending")
                c.Width = New Unit(220)
            Case 5
                Dim dt As DataTable = Database.Tenant.GetDataTable(source)
                If dt.Rows.Count > 0 Then
                    c = New Panel
                    c.Attributes("style") = [String].Format("width:218px; height:auto; z-index:55;")
                    c.Attributes.Add("class", "panelControls")
                    Dim gv = New GridView
                    gv = New GridView
                    gv.Attributes("style") = [String].Format("border-color: #c1c1c1 !important;")
                    gv.ID = "gv_" + name + reportId
                    Dim bfieldName As String
                    Dim bHeaderText As String
                    CType(gv, GridView).Columns.Clear()
                    CType(gv, GridView).AutoGenerateColumns = False
                    Dim col As New TemplateField
                    col.ItemTemplate = New MyTemplate()
                    col.HeaderStyle.CssClass = "hidden"
                    col.ItemStyle.CssClass = "rowstyle"
                    CType(gv, GridView).Columns.Add(col)
                    For Each column As DataColumn In dt.Columns
                        bfieldName = column.ColumnName
                        bHeaderText = column.ColumnName
                        Dim bfield As New BoundField()
                        bfield.HeaderText = bHeaderText
                        bfield.DataField = bfieldName
                        bfield.ItemStyle.CssClass = "rowstyle"
                        bfield.HeaderStyle.CssClass = "hidden"
                        If (column.ColumnName <> "Value") Then
                            bfield.ItemStyle.CssClass = "hidden"
                            bfield.FooterStyle.CssClass = "hidden"
                        End If
                        CType(gv, GridView).Columns.Add(bfield)
                    Next
                    CType(gv, GridView).AllowPaging = False
                    CType(gv, GridView).DataSource = Nothing
                    CType(gv, GridView).DataSource = dt
                    CType(gv, GridView).DataBind()
                    c.Controls.Add(CType(gv, GridView))
                Else
                    c = New Panel
                    c.Attributes("style") = [String].Format("width:max-content; height:30px;")
                    Dim lbl = New Label
                    lbl = New Label
                    lbl.Attributes("style") = [String].Format("color: red; float: left; padding-top: 10px !important;")
                    lbl.ID = "lbl_" + name
                    lbl.Text = "No value for this selection"
                    c.Controls.Add(CType(lbl, Label))
                End If

        End Select
        c.ID = "report_filter_" + name
        Dim inputContainer As New Panel
        inputContainer.Controls.Add(c)

        If mandatory Then
            inputContainer.Controls.Add(New LiteralControl(" "))
            Dim rfv As New RequiredFieldValidator
            rfv.ControlToValidate = c.ID
            rfv.ValidationGroup = "reportquery"
            inputContainer.Controls.Add(rfv)
        End If
        If Validationfield <> "" Then
            inputContainer.Controls.Add(New LiteralControl(" "))
            Dim cfv As New CompareValidator
            cfv.ControlToValidate = c.ID
            cfv.Operator = GetValidationType(ValidationOperator)
            cfv.ControlToCompare = "report_filter_" + Validationfield
            cfv.ValidationGroup = "reportquery"
            cfv.ErrorMessage = "Please check dates"
            cfv.ForeColor = Drawing.Color.Red
            inputContainer.Controls.Add(cfv)
        End If

        container.Controls.Add(inputContainer)

        container.CssClass = "report-filter-container"
        Return container
    End Function
    Private Function GetValidationType(ByVal type As String) As Integer
        Dim value As Integer
        If type = "=" Then
            value = 0
        ElseIf type = ">" Then
            value = 2
        ElseIf type = "<" Then
            value = 4
        ElseIf type = ">=" Then
            value = 3
        ElseIf type = "<=" Then
            value = 5
        Else : value = 1
        End If
        Return value
    End Function
    Protected Sub ddlReport_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlReport.SelectedIndexChanged
        '-- This is handled in Page_PreInit, since the controls are needed to be added dynamically on the page.
        '-- By doing so, the control values are maintained through postback cycles.
    End Sub

    Protected Sub btnGenerate_Click(sender As Object, e As System.EventArgs) Handles btnGenerate.Click
        'If ddlReport.SelectedIndex = 0 Then
        '    ClientScript.RegisterStartupScript(Me.GetType(), "alert", "alert('Please select a report from the dropdown list.');", True)
        'Else
        LoadReport(ddlReport.SelectedValue)
        If offline = "1" Then
            AlertAndRedirect("Report has been added to queue", Request.RawUrl)
        End If
        'End If
    End Sub

    Protected Sub rptGroups_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptGroups.ItemDataBound
        Dim roleId As Integer = CType(e.Item.FindControl("RID"), HiddenField).Value
        Dim rptReports As Repeater = e.Item.FindControl("rptReports")
        Dim response As XElement = XElement.Load(Server.MapPath("config/Reports.xml"))
        Dim BppEnabled As DataRow = Database.Tenant.GetTopRow("SELECT BPPEnabled FROM Application")
        Dim MaLiteEnabled = ClientSettings.PropertyValue("EnableMALite")
        Dim ProStaticV2 = Database.Tenant.GetStringValue("select Value FROM ClientSettings WHERE Name ='ProductivityStaticReportV2'")
        If Not HasDTRModule() Then
            If Not (BppEnabled.GetString("BPPEnabled") = "1" Or BppEnabled.GetString("BPPEnabled") = "True" Or BppEnabled.GetString("BPPEnabled") = "true") Then
                Dim temp = From uu In response.Descendants("Reports") Where uu.Element("RoleId") = "2" Select Name = uu.Element("Name").Value, ID = uu.Element("Id").Value, Description = uu.Element("Description").Value.Replace("\n", "<br />").Replace("\b", "<b>").Replace("/b", "</b>") Where ID <> 9 And ID <> 10 And ID <> 11 And ID <> 12 And ID <> 14
                If ProStaticV2 <> "1" Then
                    rptReports.DataSource = temp.ToList.FindAll(Function(x) x.ID <> 16)
                Else
                    rptReports.DataSource = temp.ToList
                End If
            Else
                Dim temp = From uu In response.Descendants("Reports") Where uu.Element("RoleId") = "2" Select Name = uu.Element("Name").Value, ID = uu.Element("Id").Value, Description = uu.Element("Description").Value.Replace("\n", "<br />").Replace("\b", "<b>").Replace("/b", "</b>") Where ID <> 9 And ID <> 10 And ID <> 11 And ID <> 14
                If ProStaticV2 <> "1" Then
                    rptReports.DataSource = temp.ToList.FindAll(Function(x) x.ID <> 16)
                Else
                    rptReports.DataSource = temp.ToList
                End If
            End If
            If Not (MaLiteEnabled = "1" Or MaLiteEnabled = "True" Or MaLiteEnabled = "true") Then
                If Not (BppEnabled.GetString("BPPEnabled") = "1" Or BppEnabled.GetString("BPPEnabled") = "True" Or BppEnabled.GetString("BPPEnabled") = "true") Then
                    Dim temp = From uu In response.Descendants("Reports") Where uu.Element("RoleId") = "2" Select Name = uu.Element("Name").Value, ID = uu.Element("Id").Value, Description = uu.Element("Description").Value.Replace("\n", "<br />").Replace("\b", "<b>").Replace("/b", "</b>") Where ID <> 9 And ID <> 10 And ID <> 11 And ID <> 12 And ID <> 14
                    If ProStaticV2 <> "1" Then
                        rptReports.DataSource = temp.ToList.FindAll(Function(x) x.ID <> 16)
                    Else
                        rptReports.DataSource = temp.ToList
                    End If
                Else
                    Dim temp = From uu In response.Descendants("Reports") Where uu.Element("RoleId") = "2" Select Name = uu.Element("Name").Value, ID = uu.Element("Id").Value, Description = uu.Element("Description").Value.Replace("\n", "<br />").Replace("\b", "<b>").Replace("/b", "</b>") Where ID <> 9 And ID <> 10 And ID <> 11 And ID <> 14
                    If ProStaticV2 <> "1" Then
                        rptReports.DataSource = temp.ToList.FindAll(Function(x) x.ID <> 16)
                    Else
                        rptReports.DataSource = temp.ToList
                    End If
                End If
            Else
                If Not (BppEnabled.GetString("BPPEnabled") = "1" Or BppEnabled.GetString("BPPEnabled") = "True" Or BppEnabled.GetString("BPPEnabled") = "true") Then
                    Dim temp = From uu In response.Descendants("Reports") Where uu.Element("RoleId") = "2" Select Name = uu.Element("Name").Value, ID = uu.Element("Id").Value, Description = uu.Element("Description").Value.Replace("\n", "<br />").Replace("\b", "<b>").Replace("/b", "</b>") Where ID <> 9 And ID <> 10 And ID <> 11 And ID <> 12
                    If ProStaticV2 <> "1" Then
                        rptReports.DataSource = temp.ToList.FindAll(Function(x) x.ID <> 16)
                    Else
                        rptReports.DataSource = temp.ToList
                    End If
                Else
                    Dim temp = From uu In response.Descendants("Reports") Where uu.Element("RoleId") = "2" Select Name = uu.Element("Name").Value, ID = uu.Element("Id").Value, Description = uu.Element("Description").Value.Replace("\n", "<br />").Replace("\b", "<b>").Replace("/b", "</b>") Where ID <> 9 And ID <> 10 And ID <> 11
                    If ProStaticV2 <> "1" Then
                        rptReports.DataSource = temp.ToList.FindAll(Function(x) x.ID <> 16)
                    Else
                        rptReports.DataSource = temp.ToList
                    End If
                End If
            End If

        Else
            If Not (BppEnabled.GetString("BPPEnabled") = "1" Or BppEnabled.GetString("BPPEnabled") = "True" Or BppEnabled.GetString("BPPEnabled") = "true") Then
                Dim temp1 = From uu In response.Descendants("Reports") Where uu.Element("RoleId") = "2" Select Name = uu.Element("Name").Value, ID = uu.Element("Id").Value, Description = uu.Element("Description").Value.Replace("\n", "<br />").Replace("\b", "<b>").Replace("/b", "</b>") Where ID <> 12
                If ProStaticV2 <> "1" Then
                    rptReports.DataSource = temp1.ToList.FindAll(Function(x) x.ID <> 16)
                Else
                    rptReports.DataSource = temp1.ToList
                End If
            Else
                Dim temp1 = From uu In response.Descendants("Reports") Where uu.Element("RoleId") = "2" Select Name = uu.Element("Name").Value, ID = uu.Element("Id").Value, Description = uu.Element("Description").Value.Replace("\n", "<br />").Replace("\b", "<b>").Replace("/b", "</b>")
                If ProStaticV2 <> "1" Then
                    rptReports.DataSource = temp1.ToList.FindAll(Function(x) x.ID <> 16)
                Else
                    rptReports.DataSource = temp1.ToList
                End If
            End If
        End If
        Dim PrcReportenable = Database.System.GetStringValue("Select PRCReport FROM OrganizationSettings WHERE OrganizationId = " & HttpContext.Current.GetCAMASession().OrganizationId)
        If PrcReportenable <> "True" Then
            Dim allReports = CType(rptReports.DataSource, IEnumerable(Of Object))
            Dim filteredReports = allReports.Where(Function(x) DirectCast(x, Object).ID <> 19)
            rptReports.DataSource = filteredReports.ToList()
        End If
        Dim enableNewPriority = Database.Tenant.GetStringValue("Select Value FROM ClientSettings WHERE Name = 'EnableNewPriorities'")
        If enableNewPriority = "1" Then
            Dim allReports = CType(rptReports.DataSource, IEnumerable(Of Object))
            Dim filteredReports = allReports.Where(Function(x) DirectCast(x, Object).ID <> 2)
            filteredReports = filteredReports.Where(Function(x) DirectCast(x, Object).ID <> 3)
            filteredReports = filteredReports.Where(Function(x) DirectCast(x, Object).ID <> 7)
            rptReports.DataSource = filteredReports.ToList()
        Else
            Dim allReports = CType(rptReports.DataSource, IEnumerable(Of Object))
            Dim filteredReports = allReports.Where(Function(x) DirectCast(x, Object).ID <> 20)
            filteredReports = filteredReports.Where(Function(x) DirectCast(x, Object).ID <> 21)
            filteredReports = filteredReports.Where(Function(x) DirectCast(x, Object).ID <> 22)
            rptReports.DataSource = filteredReports.ToList()
        End If
        If ClientSettings.PropertyValue("DTRWorkflowNew") <> "1" Then
            Dim allReports = CType(rptReports.DataSource, IEnumerable(Of Object))
            Dim filteredReports = allReports.Where(Function(x) DirectCast(x, Object).ID <> 23)
            rptReports.DataSource = filteredReports.ToList()
        End If
        rptReports.DataBind()
    End Sub
    Protected Sub btnsendRptProcessor_Click(sender As Object, e As EventArgs)
        Dim orgId As Integer = HttpContext.Current.GetCAMASession().OrganizationId
        Dim email As String = Database.Tenant.GetStringValue("SELECT Email FROM UserSettings WHERE LoginId='" + Page.UserName.ToString() + "'")
        Dim roleId As Integer = ddlReport.SelectedValue
        Dim response As XElement = XElement.Load(Server.MapPath("config/Reports.xml"))
        Dim dataSetName, dataSource, reportTitle, reportType As String
        Dim temp = From uu In response.Elements("Reports") From yy In uu.Elements("Id") Where yy.Value = roleId.ToString Select uu
        dataSetName = temp.Elements("DataSetName").Value
        dataSource = temp.Elements("DataSource").Value
        reportTitle = temp.Elements("ReportTitle").Value
        reportType = temp.Elements("ReportType").Value
        Dim hostUrl As String = HttpContext.Current.Request.Url.AbsoluteUri.ToString().Replace("Default", "DownloadReport").Replace("default", "DownloadReport").ToString()
        Dim parameters As New NameValueCollection
        Dim params As XElement = XElement.Load(Server.MapPath("config/ReportParameters.xml"))
        Dim temp2 = From uu In params.Elements("ReportParameters") From yy In uu.Elements("ReportId") Where yy.Value = roleId.ToString Select uu
        For Each xe As XElement In temp2
            Dim name As String = xe.Element("Name").Value
            Dim inputType As Integer = xe.Element("InputType").Value
            Dim required As String = xe.Element("Required").Value
            Dim value As String = ""
            Dim fc As WebControl = ph.FindControl("report_filter_" + name)
            Select Case inputType
                Case 0
                    value = CType(fc, TextBox).Text
                Case 1
                    value = CType(fc, DropDownList).SelectedValue
                Case 2
                    value = CType(fc, TextBox).Text
                Case 3
                    value = CType(fc, CheckBox).Checked.GetHashCode
                Case 4
                    value = CType(fc, RadioButtonList).SelectedValue
            End Select
            If value = "" Then
                parameters.Add("@" + name, Nothing)
            Else
                parameters.Add("@" + name, value)
            End If
        Next
        ' Dim src As Array = dataSource.Split("@")
        Dim cmd As New SqlCommand(dataSource)
        If parameters IsNot Nothing Then
            For Each key As String In parameters.AllKeys
                Dim newP As SqlParameter = cmd.CreateParameter()
                newP.ParameterName = key
                If parameters(key) Is Nothing OrElse parameters(key) = "" OrElse parameters(key) = "0001-01-01" Then
                    'newP.Value = DBNull.Value
                    newP.Value = "''''"
                Else
                    If newP.DbType = DbType.AnsiString Or newP.DbType = DbType.Date Or newP.DbType = DbType.DateTime Or newP.DbType = DbType.Guid Or newP.DbType = DbType.String Or newP.DbType = DbType.AnsiStringFixedLength Or vbAbort = DbType.StringFixedLength Then
                        newP.Value = "''" + parameters(key) + "''"
                    Else
                        newP.Value = parameters(key)
                    End If
                End If

                cmd.Parameters.Add(newP)
            Next
        End If
        Dim query As String = cmd.CommandText
        For Each param As SqlParameter In cmd.Parameters

            query = query.Replace(param.ParameterName, param.ParameterName + "=" + param.Value)
        Next
        Dim currentDate As New Date
        currentDate = FormatDateTime(DateTime.Now)
        Dim insertquery As String = "INSERT INTO ReportJobs(ReportID,ReportDescription,CreatedDate,Query,OrganizationID,Format,Email,RequestedBy,Status,DownloadhostUrl) VALUES('" + roleId.ToString() + "','" + reportTitle + "','" + currentDate.ToString("yyyy-MM-dd HH:mm:ss.fff") + "' ,'" + query.ToString() + "','" + orgId.ToString() + "','Excel','" + email.ToString() + "','" + Page.UserName.ToString() + "',0,'" + hostUrl.ToString() + "')  "
        Database.System.Execute(insertquery)
    End Sub
    'Protected Sub exportWithXLSX_Click(sender As Object, e As EventArgs)
    '    Dim Gridview As New GridView
    '    Gridview.DataSource = reportDatasource
    '    Gridview.DataBind()
    '    Response.Clear()
    '    Response.Buffer = True
    '    Response.ClearContent()
    '    Response.ClearHeaders()
    '    Dim FileName As String = reportfileName + ".xlsx"
    '    Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName)
    '    Response.Charset = ""
    '    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    '    Dim sw As New StringWriter()
    '    Dim hw As New HtmlTextWriter(sw)
    '    Gridview.RenderControl(hw)
    '    Response.Output.Write(sw.ToString())
    '    Response.Flush()
    '    Response.End()
    'End Sub

End Class

Public Class MyTemplate
    Implements ITemplate
    Dim itemcount As Integer = 0
    Sub InstantiateIn(ByVal container As Control) _
          Implements ITemplate.InstantiateIn
        Dim cb As New CheckBox
        cb.Checked = False
        container.Controls.Add(cb)
    End Sub
End Class
