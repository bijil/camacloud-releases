﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="mapwms.aspx.vb" Inherits="CAMACloud.Console.mapwms" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title>WMS Nearmap</title>
<style>
    html {
    	height: 100%
    }
    body { 
    	height: 100%; margin: 0; overflow:hidden; 
    }
    .windmapframe {
		width: 100%; height: 100%; overflow:hidden; 
	}


</style>
<link rel="stylesheet" href="/App_Static/css/Svo/measure.css" />
<link rel="stylesheet" type="text/css" href="/App_Static/css/parcelmanager.css?t=1" />
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=geometry&region=US&key=AIzaSyC7e5YrZlKqWtaY4ehsDwMgCis4IfNs7Xw"></script>
<script type="text/javascript" src="/App_Static/jslib/jquery.js"></script>
<script type="text/javascript" src="/App_Static/jslib/jquery-ui-1.8.20.custom.min.js"></script>
<script type="text/javascript" src="/App_Static/jslib/jquery.rotate.js"></script>
<script src="../../App_Static/js/qc/010-conversions.js"></script>
<script type="text/javascript" src="/App_Static/jslib/tipr.js?t=0111234598"></script>
<script type="text/javascript" src="/App_Static/js/sketch/14-ccsketcheditor.js?t=2189"></script>
<script type="text/javascript" src="/App_Static/jslib/underscore-min.js"></script>
<script type="text/javascript" src="/App_Static/jslib/base64.js"></script>
<script type="text/javascript" src="/App_Static/js/Svo/lib2/base64.js"></script>
<cc:JavaScript ID="JavaScript1" runat="server" IncludeFolder="/App_Static/js/sketch/sketchlib/" />
<script type="text/javascript" src="/App_Static/js/Svo/sigmasketch.js?t=96456980196"></script>
<script src="https://unpkg.com/leaflet@1.9.2/dist/leaflet.js" integrity="sha256-o9N1jGDZrf5tS+Ft4gbIK7mYMipq9lqpVJ91xHSyKhg=" crossorigin=""></script>
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.2/dist/leaflet.css" integrity="sha256-sA+zWATbFveLLNqWO2gtiw3HL/lh1giY/Inf1BJ0z14=" crossorigin="" />

<script type="text/javascript">
   
    var Hammer = false;
    var activeParcel = {}
    var map;
    var mapZoom;
    var drag = false;
    var overlay;
    var polygonCenter;
    var degree;
    var scale;
    var RotationDegree;
    var activeParcel;
    var Parcelid;
    var ParcelDataExist = false;
    var parcelPolygon = new google.maps.Polygon();
    var ParcelCenter;
    var alreadyDone;
    var sketchLatLng = '';
    var polygons = [];
    var parcelLoaded = false;
    var sketchRenderer = {};
    var clientSettings = {};
    var sketchSettings = {};
    var adjustHandle = null;
    var adjustHandleOrigin = null;
    var firstTimeLoad = true;
    var adjusting = false;
    var isNewSketchDataFormat = false;
    mapFlag = true;
    var zFlag = false;
    var lookup;
    var container;
    var drawFlag=false;
    var ccma = {
        Sketching: {
            SketchFormatter: null,
            Config: null
        }
    };
    $(function() {
        mapFlag = true;
        if (window) window.parent.$('#btnMapSave').hide();
        setPageDimensions();
        if (window) window.parent.$('.windMask').show();
        sketchRenderer = new CCSketchEditor('.ccse-img', CAMACloud.Sketching.Formatters.TASketch);
        initMap(function() {
            if (parent && parent.window && parent.window.opener) {
                    //if ( parent.window.opener.processHash ) parent.window.opener.processHash();
                    openParcel(function() {
                        /*var z = mapFrame.contentWindow.scale;
                    var d = mapFrame.contentWindow.degree;
                        if ( z ) $( '.sketch-resize' ).val( z );
                        if ( d ) $( '.sketch-zoom-value' ).html( parseInt( parseFloat( z ).toFixed( 2 ) * 100 ) + '%' );*/
                        window.parent.$('.windMask').hide();
                        window.parent.$('#btnMapSave').show();
                    });
            }
        });
    });

    $(window).resize(setPageDimensions);
    $(document).keydown(function(e) {
        if (window.parent.currentInput)
            return true
        var key = e.charCode || e.keyCode;
        window.parent.keybordShortCuts(key, e.shiftKey)
        if (key == 38 || key == 40 || key == 37 || key == 39) {
            window.parent.paintSketch();
            e.preventDefault();
            return false;
        }
    });

    function openSketchWindow() {
        var divText = top.$(".sketch-big").html();
        var myWindow = window.open('', '', 'width=200,height=100');
        var doc = myWindow.document;
        doc.open();
        doc.write(divText);
        doc.close();
    }

    function initMap(callback) {

        var mOptions = nearmapWMSApiKey !=="" ? ["roadmap", "satellite","hybrid","Nearmap"] : ["roadmap", "satellite","hybrid"]
        var options = {
            zoom: 20,
            zoomControl: true,
            center: new google.maps.LatLng(41.655471, -83.534546),
            mapTypeControlOptions: {
	        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
		    mapTypeIds: mOptions
	    },
            minZoom: 15,
            tilt: 0,
            keyboardShortcuts: false,
            gestureHandling: 'greedy',
            fullscreenControl: false
        };
        var mc = document.getElementById('map');
        map = new google.maps.Map(mc, options);
        map.setZoom(20);
        mapZoom = 20;
	    map.setMapTypeId('hybrid');
        if (nearmapWMSApiKey !== "" && nearmapWMSAccess.includes('SV')){
	    const NearmapMapWMSType = new google.maps.ImageMapType({
	    getTileUrl: function(coord, zoom) {
		    const normalizedCoord = getNormalizedCoord(coord, zoom);
		    if (!normalizedCoord) {
		    return "";
		    }
			
	    const bound = Math.pow(2, zoom);
	    return "https://api.nearmap.com/tiles/v3/Vert" +
	    "/" +
	    zoom +
	    "/" +
	    normalizedCoord.x +
	    "/" +
	    (normalizedCoord.y) +
	    ".img?apikey="+nearmapApiKey+"&tertiary=satellite"
	    },
	    tileSize: new google.maps.Size(256, 256),
	    maxZoom: 19,
	    minZoom: 6,
	    radius: 1738000,
	    name: "NearmapWMS"
        });


        function getNormalizedCoord(coord, zoom) {
	        const y = coord.y;
	        let x = coord.x; // tile range in one direction range is dependent on zoom level
	        // 0 = 1 tile, 1 = 2 tiles, 2 = 4 tiles, 3 = 8 tiles, etc
		
	        const tileRange = 1 << zoom; // don't repeat across y-axis (vertically)
		
	        if (y < 0 || y >= tileRange) {
		        return null;
	        } // repeat across x-axis
		
	        if (x < 0 || x >= tileRange) {
		        x = ((x % tileRange) + tileRange) % tileRange;
	        }
		
	        return {
		        x: x,
		        y: y
	        };
	        }
        map.mapTypes.set("NearmapWMS", NearmapMapWMSType);
        map.setMapTypeId("NearmapWMS");
        }
        google.maps.event.addListener(map, 'zoom_changed', function(e) {
            var zoomLevel = map.getZoom();
            if( zoomLevel > 20 || zoomLevel < 15 ) return false; // zoom max value 20 and min value 15
            var zoomdiff = 0;
            sketchZoom = activeParcel.sketchZoom;
		        if (zFlag && mapZoom > zoomLevel) {
		        zoomdiff = mapZoom - zoomLevel; 
	            zoomSketch(sketchZoom / Math.pow(2,zoomdiff), null, false, zoomLevel);
		        }else if (zFlag && mapZoom < zoomLevel) {
		        zoomdiff =  zoomLevel - mapZoom ; 
                zoomSketch(sketchZoom * Math.pow(2,zoomdiff), null, false, zoomLevel);
		        }
            window.setTimeout('setPositionOverlayIfDataExist();', 200);
            mapZoom = zoomLevel;
        });
        google.maps.event.addListener(map.getStreetView(), 'visible_changed', function(e) {
	        map.getStreetView().getVisible() == false ? $('#btnMapSave', parent.document).show() : $('#btnMapSave', parent.document).hide()
        });
        google.maps.event.addListener(map, 'mousemove', function(e) {
            zFlag= true;
        });
            
        var measure;
        var measureDiv = document.createElement('div');
        measure = new MeasurementScale(measureDiv, map);
        measureDiv.index = 1;
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(measureDiv);

        var rzIcon = '/App_Static/cursors/rotatezoom.png';

        adjustHandle = new google.maps.Marker({
            position: map.getCenter(),
            icon: rzIcon,
            draggable: true,
            map: map
        });

        var startDegree = null;
        google.maps.event.addListener(adjustHandle, 'dragstart', (function(e) {
            startDegree = degree;
            adjusting = true;
        }));

        google.maps.event.addListener(adjustHandle, 'drag', (function(e) {
            var ah = LatLngToPoint(e.latLng);
            var c = adjustHandleOrigin;
            var dist = Math.round(Math.sqrt((ah.x - c.x) * (ah.x - c.x) + (ah.y - c.y) * (ah.y - c.y)));
            var zoom = Math.round((activeParcel.zoomSketch || 0.6) * dist / 200 * 100) / 100;
            zoomSketch(zoom);

            var rotateDegree = Math.round(Math.atan2(ah.y - c.y, ah.x - c.x) * 180 / Math.PI) + 135 + startDegree;
            if (rotateDegree > 180) rotateDegree = rotateDegree - 360;
            if (rotateDegree < -180) rotateDegree = rotateDegree + 360;
            rotateSketch(rotateDegree);

        }));

        google.maps.event.addListener(adjustHandle, 'dragend', (function(e) {
            startDegree = null;
            adjusting = false;
            //map.setOptions({ draggableCursor: 'default' });
            //adjustHandle.setVisible(true);
        }));

        google.maps.event.addListener(map, 'click', (function(e) {
            if (measure.on) {
                if (measure.start == null) {
                    measure.start = e.latLng;
                } else {
                    if (measure.end == null) {
                        measure.end = e.latLng;
                    } else {
                        measure.start = e.latLng;
                        measure.end = null;
                    }
                }

                measure.updateGraphics();
                $('.sv-logo', window.parent.document).click().focus();
                e.stop();
            }
        }));
        google.maps.event.addListenerOnce(map, 'idle', function() {
            adjustHandleOrigin = LatLngToPoint(adjustHandle.position)
            if (callback) callback();
            adjusting = false;
        });
    }

    DraggableOverlay.prototype = new google.maps.OverlayView();

    function openParcel(callback) {
        if (parent && parent.window && parent.window.opener) activeParcel = _.clone(parent.window.opener.activeParcel);
        if (parent) lookup = parent.window.opener.lookup;
        if (parent && parent.window && parent.window.opener && parent.window.opener.mapFrame) isNewSketchDataFormat = parent.window.opener.mapFrame.contentWindow.isNewSketchData;
        parcelLoaded = false;
        alreadyDone = true;
        firstTimeLoad = true;
        isSketchChanged= false;
        ParcelDataExist = false;
        firTime = true;
        degree = 0;
        currentOverlayPosition = {};
        parcelId = activeParcel.KeyValue1;

        var _cc , _ss;
        if ( parent && parent.window && parent.window.opener && parent.window.opener.clientSettings ){
            clientSettings = parent.window.opener.clientSettings;
            _cc = 1;
        }
        if( parent && parent.window && parent.window.opener && parent.window.opener.sketchSettings ){
            sketchSettings = parent.window.opener.sketchSettings;
            _ss = 1;
        }
        if ( !( ( _cc && parent.window.opener.clientSettings.SketchConfig ) || ( _ss && parent.window.opener.sketchSettings.SketchConfig ) ) ){
            // clientSettings["SketchConfig"] = activeParcel.sketchconfig
            // clientSettings["SketchFormat"] = activeParcel.sketchformat
            sketchSettings["SketchConfig"] = activeParcel.sketchconfig
            sketchSettings["SketchFormat"] = activeParcel.sketchformat
        }
            
        var points = [],
            shapes = [];
        polygons = [];
        var bounds = new google.maps.LatLngBounds();
        for (x in activeParcel._mapPoints.sort(function(x, y) {
                return x.Ordinal - y.Ordinal
            })) {
            var p = activeParcel._mapPoints[x];
            var latlng = new google.maps.LatLng(p.Latitude, p.Longitude);
            points.push(latlng);
            bounds.extend(latlng);
            if (shapes[p.RecordNumber || 0] == null) {
                shapes[p.RecordNumber || 0] = [];
            }
            shapes[p.RecordNumber || 0].push(latlng);
        }

        shapes.forEach(function(w) {
            if (w != null) {
                polygons.push(new google.maps.Polygon({
                    paths: w,
                    strokeColor: "#FF0000",
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillColor: "#FF0000",
                    fillOpacity: 0.1,
                    visible: true,
                    clickable: false
                }));
                polygons[polygons.length - 1].setMap(map);
            }
        })
        polygonCenter = bounds.getCenter();
        activeParcel["LatLngPoints"] = points;
        activeParcel["Bounds"] = bounds;
        var parcel = activeParcel;
        google.maps.event.trigger(map, 'resize');
        if (parent && parent.window && parent.window.opener) {
            drawParcel(0, 0, parcel.SketchVector, parcel.dataExist ? 'white' : 'yellow', function() {
                if (parcel.dataExist) {
                    // currentOverlayPosition = new google.maps.LatLng(parcel.lat, parcel.lng);
                    setOverLayPosition(parcel.lat, parcel.lng, parcel.sketchZoom, parcel.SketchRotate, parcel.SketchData);
                    ParcelDataExist = true;
                    setPositionOverlayIfDataExist();
                    scale = isNaN(parseFloat(sketchOB.sketchZoom)) ? 0 : parseFloat(sketchOB.sketchZoom);
                    RotationDegree = parseFloat(sketchOB.sketchRotation);
                    map.setCenter(bounds.getCenter());
                    var mZoom = sketchOB.mapZoom? sketchOB.mapZoom: parcel.MapZoom;
                    map.setZoom(parseInt(mZoom));
                    zoomSketch(sketchOB.sketchZoom);
                    rotateSketch(parseFloat(sketchOB.sketchRotation));

                } else {
                    setOverLayPosition(polygonCenter.lat(), polygonCenter.lng(), 0.6, 0);
                    setPositionOverlayIfDataExist();
                    map.setCenter(bounds.getCenter());
                    map.setZoom(20);
                    zoomSketch(0.6);
                    rotateSketch(0);
                }
                mapFlag = false;
                if (callback) callback(parcel);
            });
        }
        //  });

    }

    function DraggableOverlay(map, position, content) {
        if (typeof draw === 'function') {
            this.draw = draw;
        }
        this.setValues({
            position: position,
            container: null,
            content: content,
            map: map
        });
    }

    DraggableOverlay.prototype.onAdd = function() {
        container = document.createElement('div'),
            that = this;
        if (typeof this.get('content').nodeName !== 'undefined')
            container.appendChild(this.get('content'));
        else {
            if (typeof this.get('content') === 'string')
                container.innerHTML = this.get('content');
            else
                return;
        }
        container.style.position = 'absolute';
        container.draggable = true;
        google.maps.event.addDomListener(this.get('map').getDiv(), 'mouseleave', function() {  //MAP
            google.maps.event.trigger(container, 'mouseup');
        });
        google.maps.event.addDomListener(this.get('map').getDiv(), 'click', function() {    //MAP
            google.maps.event.trigger(container, 'mouseup');
        });
        google.maps.event.addDomListener(container, 'mousedown', function(e) {
            this.style.cursor = 'move';
            that.map.set('draggable', false);
            that.set('origin', e);
            that.moveHandler = google.maps.event.addDomListener(that.get('map').getDiv(), 'mousemove', function(e) {  //MAP
                var origin = that.get('origin'),
                left = origin.clientX - e.clientX,
                top = origin.clientY - e.clientY,
                pos = that.getProjection().fromLatLngToDivPixel(that.get('position')),
                latLng = that.getProjection().fromDivPixelToLatLng(new google.maps.Point(pos.x - left, pos.y - top));
                that.set('origin', e);
                that.set('position', latLng);
                isSketchChanged = true;
                firstTimeLoad = false;
                that.draw();
            });
        });
            
        google.maps.event.addDomListener(container, 'mouseup', function() {
            	var skRowuid = parent.window.opener.$('.sketch-select option[value="' + selectedSketch + '"]').attr('skrowuid');
				var skPrefix = parent.window.opener.$('.sketch-select option[value="' + selectedSketch + '"]').attr('skprefix');
				skPrefix = isNewSketchDataFormat && skPrefix? skPrefix: '';
				var newSelectedSketch = isNewSketchDataFormat? skRowuid: selectedSketch;
                if(drawFlag){
                var latLng = currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection].position;
                if(!activeParcel.sketchLatLng && firstTimeLoad){
	                    latLng=LatLngToPoint(latLng);
	                    latLng.x=latLng.x-(400 * activeParcel.sketchZoom /2);
	                    latLng.y=latLng.y-(400 * activeParcel.sketchZoom /2);
	                    latLng=PointToLatLng(latLng)
                    }
                that.set('position', latLng);
                that.draw();
                drawFlag = false;
                }
            else {
                firstTimeLoad = false;
	            that.map.set('draggable', true);
	            this.style.cursor = 'default';
	            google.maps.event.removeListener(that.moveHandler);
	            sketchLatLng = that.position.lat() + ',' + that.position.lng();
	            var zoom = scale;
	            var angle = degree;
	            if(currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix] && selectedSection) {
	            currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection] = {
	                        "position": new google.maps.LatLng(that.position.lat(), that.position.lng()),
	                        "sketchZoom": zoom,
	                        "sketchRotation": parseFloat(angle),
	                        "mapZoom": mapZoom
	                    }
	                }
            }
        });

        this.set('container', container)
        this.getPanes().floatPane.appendChild(container);
        if (activeParcel && activeParcel.dataExist) {
            scale = isNaN(parseFloat(sketchOB.sketchZoom)) ? 0 : parseFloat(sketchOB.sketchZoom);
            RotationDegree = parseFloat(sketchOB.sketchRotation);
            map.setCenter(activeParcel.Bounds.getCenter());
            var mZoom = sketchOB.mapZoom? sketchOB.mapZoom: activeParcel.MapZoom;
            map.setZoom(parseInt(mZoom));
            zoomSketch(sketchOB.sketchZoom);
            rotateSketch(parseFloat(sketchOB.sketchRotation),true);
        } else {
            setOverLayPosition(polygonCenter.lat(), polygonCenter.lng(), 0.6, 0);
            setPositionOverlayIfDataExist();
            map.setCenter(activeParcel.Bounds.getCenter());
            map.setZoom(20);
            zoomSketch(0.6);
            rotateSketch(0);
        }
    };

    DraggableOverlay.prototype.draw = function() {
        var pos = this.getProjection().fromLatLngToDivPixel(this.get('position'));
        if (pos){
        this.get('container').style.left = pos.x + 'px';
        this.get('container').style.top = pos.y + 'px';
        }
    };

    DraggableOverlay.prototype.onRemove = function() {
        this.get('container').parentNode.removeChild(this.get('container'));
        this.set('container', null)
    };

    function setOverLayPosition(lat, lng, zoom, rotation, sketchData) {
        var _adh = true;
        if( parent && parent.window && parent.window.opener && parent.window.opener.mapFrame && parent.window.opener.mapFrame.contentWindow && parent.window.opener.mapFrame.contentWindow.adjustHandle ) {
        	_adh = parent.window.opener.mapFrame.contentWindow.adjustHandle.visible;
        	if( typeof adjustHandle != 'undefined' )
				adjustHandle.visible = _adh;
        }
        	
        if (sketchData && sketchData != "") {
            currentOverlayPosition = $.parseJSON(sketchData);
            for (var key in currentOverlayPosition) {
                for (var key2 in currentOverlayPosition[key]) {
                    currentOverlayPosition[key][key2].position = new google.maps.LatLng(currentOverlayPosition[key][key2].position.lat, currentOverlayPosition[key][key2].position.lng);
                }
            }
            return;
        }
        if(parent.window.opener.mapFrame.contentWindow)  polygonCenter=parent.window.opener.mapFrame.contentWindow.polygonCenter;
            if(polygonCenter) {
	        lng=polygonCenter.lng();
	        lat=polygonCenter.lat(); }
        var sk = sketchRenderer.sketches;
        var skConfig = parent.window.opener && parent.window.opener.ccma?  parent.window.opener.ccma.Sketching.Config: null;
        var mZoom = parent.window.opener.mapFrame.contentWindow && parent.window.opener.mapFrame.contentWindow.mapZoom? parent.window.opener.mapFrame.contentWindow.mapZoom: parcel.MapZoom;
        sk.forEach(function(s, i) {
            var skRowuid = (s.parentRow && s.parentRow.ROWUID)? s.parentRow.ROWUID: s.sid;
        	var skPrefix = (s.config && s.config.SketchLabelPrefixInSv)? s.config.SketchLabelPrefixInSv: '';
			i = isNewSketchDataFormat? skRowuid: i;
            skPrefix = isNewSketchDataFormat? skPrefix: '';
            currentOverlayPosition["sketch-" + i + skPrefix] = {};
            for (var key in s.sections) {
                if (currentOverlayPosition["sketch-" + i + skPrefix][key] === undefined)
                    currentOverlayPosition["sketch-" + i + skPrefix][key] = {
                        "position": new google.maps.LatLng(lat, lng),
                        "sketchZoom": zoom,
                        "sketchRotation": rotation,
                        "mapZoom": mZoom,
                    }
            }
        })
    }

    function getParcelMapAndVector(parcelid, callback) {
        Parcelid = parcelid;
        $.ajax({
            url: '/sv/getparcel.jrq',
            method: 'POST',
            data: {
                parcel: parcelid
            },
            success: function(resp) {
                var points = [];
                var bounds = new google.maps.LatLngBounds();
                for (var i = 0; i < resp.Points.length; i++) {
                    lat = resp.Points[i].Latitude;
                    lng = resp.Points[i].Longitude;
                    latlng = new google.maps.LatLng(lat, lng);
                    points.push(latlng);
                    bounds.extend(latlng);
                }
                polygonCenter = bounds.getCenter();
                resp["LatLngPoints"] = points;
                resp["Bounds"] = bounds;

                if (callback) callback(resp);
            }
        });
    }

    function drawParcel(latitude, longitude, vector, color, callback) {
        previewSketch(color, 1, function(sketchUrl) {
            $('.overlay').parent('div').remove();
            var ltlng = polygonCenter
			if(activeParcel.lat && activeParcel.lng)
				ltlng = new google.maps.LatLng(activeParcel.lat, activeParcel.lng);
            if (activeParcel.sketchLatLng)
                ltlng = new google.maps.LatLng(activeParcel.sketchLatLng.split(',')[0], activeParcel.sketchLatLng.split(',')[1]);
            overlay = new DraggableOverlay(map, ltlng, '<div class="overlay" id="resizable-wrapper"><img id="sketch" src="' + sketchUrl + '"><div>');
            $(".overlay").draggable({
                containment: "#map",
                scroll: false
            });
            $("#map").droppable();
            if (callback) callback();
        });

    }

    function LatLngToPoint(latlng) {
        if (activeParcel.Id && overlay.getProjection != null && overlay.getProjection())
            return overlay.getProjection().fromLatLngToContainerPixel(latlng);
    }

    function PointToLatLng(point) {
        return overlay.getProjection().fromContainerPixelToLatLng(point)
    }

    function moveOverlayBy(topMove, leftMove) {
        var top = parseInt($('.overlay').css("top").replace(/[^-\d\.]/g, ''))
        var left = parseInt($('.overlay').css("left").replace(/[^-\d\.]/g, ''))
        $('.overlay').css({
            'top': top + topMove + 'px',
            'left': left + leftMove + 'px',
            'z-index': '2147483648'
        });

        setCurrentOverlapPos();

    }
    var sketchOB = {};

    function setPositionOverlayIfDataExist(multisketch) {
	    var position, type;
	    if (currentOverlayPosition && selectedSection) {
	        var skRowuid = parent.window.opener.$('.sketch-select option[value="' + selectedSketch + '"]').attr('skrowuid');
		    var skPrefix = parent.window.opener.$('.sketch-select option[value="' + selectedSketch + '"]').attr('skprefix');
		    skPrefix = isNewSketchDataFormat && skPrefix? skPrefix: '';
		    var newSelectedSketch = isNewSketchDataFormat? skRowuid: selectedSketch;
	        sketchOB = currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection]
	        position = LatLngToPoint(sketchOB.position);
	    }
	    else {
	        position = LatLngToPoint(polygonCenter);
	    }
	    if (!position) return;
	    var x = parseFloat(position.x);
	    var y = parseFloat(position.y);
	    var mapH = parseFloat($('#map').height());
	    var divW = parseFloat($('#resizable-wrapper').css("width")) / 2;
	    var divH = parseFloat($('#resizable-wrapper').css("height")) / 2;
	    y = y - mapH - divH;
	    x = (x - divW);
	
	    ParcelCenter = PointToLatLng(new google.maps.Point(position.x, position.y));
	
	    if (!adjusting) {
	        divW = divW > 140 ? 140 : divW;
	        divH = divH > 140 ? 140 : divH;
	        var ahp = PointToLatLng(new google.maps.Point(position.x + divW - 150, position.y + divH - 150));
	        adjustHandle.setPosition(ahp);
	        adjustHandleOrigin = LatLngToPoint(ParcelCenter)
	    }
	    if (multisketch && sketchOB) {
	        zoomSketch(sketchOB.sketchZoom);
	        rotateSketch(sketchOB.sketchRotation);
	    }
	    if (sketchOB) {
	        rotateSketch(sketchOB.sketchRotation, true);
	        zoomSketch(sketchOB.sketchZoom, true);
	        drawFlag = true;
	        google.maps.event.trigger(container, 'mouseup');
	
	    }
	    if (!activeParcel.sketchLatLng && firstTimeLoad) {
	        drawFlag = true;
	        google.maps.event.trigger(container, 'mouseup');
	    }
    }

    function setPageDimensions() {
        $('#map').height($(window).height());
        $('#map').width($(window).width());
        try {
            setPositionOverlayIfDataExist();
        } catch (err) {}
    }

    function zoomSketch(zoom, noSketchpostionChange, changeAll, mZoom) {
        if (isNaN(zoom))
            return;
        zoom = Number(zoom) > 2.5 ? 2.5: ( Number(zoom) < 0.25 ? 0.25: zoom);
        scale = zoom;
        $('#sketch').width(parseInt($('.ccse-img').attr('width')) * zoom);
        $('#sketch').height(parseInt($('.ccse-img').attr('height')) * zoom);
        $('#resizable-wrapper').width(parseInt($('.ccse-img').attr('width')) * zoom);
        $('#resizable-wrapper').height(parseInt($('.ccse-img').attr('height')) * zoom);
        if (!noSketchpostionChange) setPositionOverlayIfDataExist();
        $('.sketch-zoom-value', window.parent.document).html(parseInt(parseFloat(zoom).toFixed(2) * 100) + '%');
        $('.sketch-resize', window.parent.document).val(zoom);
        activeParcel.sketchZoom = zoom;
        if (selectedSection) {
            var skRowuid = parent.window.opener.$('.sketch-select option[value="' + selectedSketch + '"]').attr('skrowuid');
			var skPrefix = parent.window.opener.$('.sketch-select option[value="' + selectedSketch + '"]').attr('skprefix');
			skPrefix = isNewSketchDataFormat && skPrefix? skPrefix: '';
			var newSelectedSketch = isNewSketchDataFormat? skRowuid: selectedSketch;
            currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection].sketchZoom = zoom;
            if (mZoom) currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection].mapZoom = mZoom;
        }
        if (!changeAll) return
        var sk = sketchRenderer.sketches;
        var skConfig = parent.window.opener && parent.window.opener.ccma?  parent.window.opener.ccma.Sketching.Config: null;
        sk.forEach(function(s, i) {
            var skRowuid = (s.parentRow && s.parentRow.ROWUID)? s.parentRow.ROWUID: s.sid;
		    var skPrefix = (s.config && s.config.SketchLabelPrefixInSv)? s.config.SketchLabelPrefixInSv: '';
		    i = isNewSketchDataFormat? skRowuid: i;
			skPrefix = isNewSketchDataFormat? skPrefix: '';
            for (var key in s.sections) {
                if (currentOverlayPosition["sketch-" + i + skPrefix]) {
                    if (currentOverlayPosition["sketch-" + i + skPrefix][key]) {
                        currentOverlayPosition["sketch-" + i + skPrefix][key].sketchZoom = zoom;
                        if (mZoom) currentOverlayPosition["sketch-" + i + skPrefix][key].mapZoom = mZoom;
                    }
                }
            }
        });
    }

    function rotateSketch(deg,sflag) {
        if (isNaN(deg))
            return;
        degree = deg;
        if(!sflag){
            $('#sketch').rotate({
	            animateTo: deg
	        });
	    }else
	        $('#sketch').rotate(deg);
	        
        $('.sketch-rotate', window.parent.document).val(degree);
        $('.sketch-rotate-value', window.parent.document).html(degree + '°');
        if (selectedSection) {
            var skRowuid = parent.window.opener.$('.sketch-select option[value="' + selectedSketch + '"]').attr('skrowuid');
			var skPrefix = parent.window.opener.$('.sketch-select option[value="' + selectedSketch + '"]').attr('skprefix');
			skPrefix = isNewSketchDataFormat && skPrefix? skPrefix: '';
			var newSelectedSketch = isNewSketchDataFormat? skRowuid: selectedSketch;
            currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection].sketchRotation = degree;
        }
    }

    function saveForm(comment, flagChecked, validChecked) {
        $.ajax({
            url: '/sv/saveForm.jrq',
            method: 'POST',
            data: {
                parcel: Parcelid,
                exist: ParcelDataExist,
                comment: comment,
                flag: flagChecked,
                valid: validChecked
            },
            success: function(response) {}
        });
    }

    function getParcelCenter(doNotSetParcelCenter) {
        var map_width = parseFloat($("#map").css("width"));
        var map_height = parseFloat($("#map").css("height"));
        var left = parseFloat($("#resizable-wrapper").parent('div').css("left"));
        var top = parseFloat($("#resizable-wrapper").parent('div').css("top"));
        var x = left + parseFloat($('#sketch').css("width")) / 2;
        var y = map_height + top + parseFloat($('#sketch').css("height")) / 2;
        if (doNotSetParcelCenter)
            return PointToLatLng(new google.maps.Point(x, y));
        ParcelCenter = PointToLatLng(new google.maps.Point(x, y));
        return ParcelCenter;
    }

    function AnchorSketch() {

        var map_width = parseFloat($("#map").css("width"));
        var map_height = parseFloat($("#map").css("height"));
        var left = parseFloat($("#resizable-wrapper").css("left"));
        var top = parseFloat($("#resizable-wrapper").css("top"));
        var x = left + parseFloat($('#sketch').css("width")) / 2;
        var y = map_height + top + parseFloat($('#sketch').css("height")) / 2;
        var canvasSize = parseFloat($('.ccse-img').attr('width'));
        var sketZoom = parseFloat($('#sketch').css("width")) / parseFloat($('.ccse-img').attr('width'));
        var mapZoom = map.zoom;
        ParcelCenter = PointToLatLng(new google.maps.Point(x, y));

        $.ajax({
            url: '/sv/updatesketch.jrq',
            method: 'POST',
            data: {
                parcel: Parcelid,
                exist: ParcelDataExist,
                degree: degree,
                canvasSize: canvasSize,
                sketchZoom: sketZoom,
                mapZoom: mapZoom,
                lat: ParcelCenter.lat(),
                lng: ParcelCenter.lng()

            },
            success: function(resp) {
                new Messi('Sketch anchored.', {
                    autoclose: 700
                });
            }
        });
    }

    function MeasurementScale(div, map) {
        var ms = this;
        this.control = div;
        this.on = false;
        this.start = null;
        this.end = null;

        var mA, mB, line;
        var markIcon = new google.maps.MarkerImage("http://maps.google.com/mapfiles/ms/icons/red-dot.png", new google.maps.Size(27, 28));
        mA = new google.maps.Marker({
            icon: markIcon
        });
        mB = new google.maps.Marker({
            icon: markIcon
        });
        line = new google.maps.Polyline({
            strokeColor: 'Yellow'
        });

        this.clear = function() {
            this.start = null;
            this.end = null;
            this.updateGraphics();
            label.innerHTML = '0.0ft';
        }

        this.updateGraphics = function() {
            mA.setMap(null);
            mB.setMap(null);
            line.setMap(null);

            if (ms.start != null) {
                mA.setPosition(ms.start);
                mA.setMap(map);
            }

            if (ms.end != null) {
                mB.setPosition(ms.end);
                mB.setMap(map);
            }

            if ((ms.start != null) && (ms.end != null)) {
                line.setPath([ms.start, ms.end]);
                line.setMap(map);
                label.innerHTML = Math.round(ms.distance() * 10) / 10 + 'ft';
            }
        }

        div.className = 'measurement-tool measure-off tip';
        $(div).attr({
            "abbr": 'Click on the measuring tool to activate it and measure. Click again to deactivate the tool.',
            "width": '552px;'
        });
        $(div).css("z-index", "3147483591")
        var button = document.createElement('span');
        button.className = 'button';
        div.appendChild(button);

        var label = document.createElement('span');
        label.className = 'distance';
        div.appendChild(label);
        label.innerHTML = '0.0ft'

        google.maps.event.addDomListener(button, 'click', function(e) {
            if (ms.on) {
                map.setOptions({
                    draggableCursor: null
                });
                div.className = 'measurement-tool measure-off tip';
                $(div).attr({
                    "abbr": 'Click on the measuring tool to activate it and measure. Click again to deactivate the tool.',
                    "width": '552px;'
                });
                $('.overlay').removeClass('pointer')
                ms.clear();
            } else {
                map.setOptions({
                    draggableCursor: 'crosshair'
                });
                div.className = 'measurement-tool measure-on tip';
                $(div).attr({
                    "abbr": 'Click on the measuring tool to activate it and measure. Click again to deactivate the tool.',
                    "width": '552px;'
                });
                ms.clear();
                $('.overlay').addClass('pointer')
            }

            ms.on = !ms.on;
            e.preventDefault();
        });

        this.distance = function() {
            if ((ms.start != null) && (ms.end != null)) {
                return google.maps.geometry.spherical.computeDistanceBetween(ms.start, ms.end) * 3.28084;
            } else {
                return 0;
            }
        }

        //this.__defineGetter__("distance", );

    }
    var currentOverlayPosition = {};
</script>
<style type="text/css">
    .overlay {
        width: 150px;
        height: 150px;
        position: relative;
        z-index: 3147483647 !important;
        cursor: pointer;
    }
        
    .pointer {
        pointer-events: none;
    }
</style>

</head>
<body style="overflow: hidden; background: #777; margin: 0px;">
<form id="form1" runat="server">
    <div id="map">
    </div>

    <div style="display: none;">
        <canvas height="300" width="300" class="ccse-img">
        </canvas>
    </div>
</form>
    <script type="text/javascript">
	var nearmapWMSApiKey = '<%= NearmapWMSAPIKey%>';
	var nearmapWMSAccess = '<%= NearmapWMSAccess%>';
    </script>
</body>
</html>
