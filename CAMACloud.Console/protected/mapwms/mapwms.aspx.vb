﻿Public Class mapwms
    Inherits System.Web.UI.Page
    Public NearmapWMSAPIKey As String
    Public NearmapWMSAccess As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetNoStore()
        NearmapWMSAPIKey = ClientSettings.PropertyValue("NearmapWMS.APIKey")
        NearmapWMSAccess = ClientSettings.PropertyValue("EnableNearmapWMS")
    End Sub

End Class