﻿<%@ Page Title="Manage Datasets" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DesktopWeb.Master" CodeBehind="dataset.aspx.vb" Inherits="CAMACloud.Console.datasets" %>

<%@ Import Namespace="System.Web.Script.Services" %>
<%@ Import Namespace="System.Web.Services" %>
<%@ Import Namespace="CAMACloud.Console" %>
<%@ Import Namespace="CAMACloud.BusinessLogic.DataAnalyzer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="https://cdn.jsdelivr.net/npm/openlayers@latest/dist/ol.js"></script>
    <script src="/App_Static/js-v2/OLMap.js"></script>
    <style>
        a {
            cursor: pointer;
        }

        .ol-control {
            display: none;
        }

        .main-content-area, .main-content-area * {
            box-sizing: border-box;
            min-height: auto;
        }

        .lookup-picker {
            float: none;
            position: fixed;
            z-index: 100;
        }

        .v-spacer {
            width: 5px;
        }

        .dataset-layout {
            display: flex;
        }

            .dataset-layout > div:first-child {
                width: 300px;
                padding: 4px;
                background-color: #6b94b7;
            }

                .dataset-layout > div:first-child > .head-links a {
                }

                    .dataset-layout > div:first-child > .head-links a:hover {
                        color: #EEE;
                    }
                .dataset-layout > div:first-child .mGrid{
                    margin-top:0;
                    margin-bottom:1px;
                }
                .dataset-layout > div:first-child .mGrid > tbody > tr > td > a {
                    font-size: 1em;
                    font-weight: 400;
                }

            .dataset-layout > .dataset-editor {
                flex-grow: 1;
            }

                .dataset-layout > .dataset-editor > div:first-child {
                    background-color: #ebeff2;
                    height: 60px;
                    padding: 4px;
                    border-bottom: 1px solid #d5dde4;
                }

                    .dataset-layout > .dataset-editor > div:first-child + div {
                        display: flex;
                    }

                        .dataset-layout > .dataset-editor > div:first-child + div > #mapbox {
                            background-color: #d0d0d0;
                            flex: 1;
                        }

                        .dataset-layout > .dataset-editor > div:first-child + div > .grouplist {
                            width: 300px;
                            padding:0;
                        }

        .grouplist__head {
            padding:5px
        }
        .grouplist-inner {
            border-top: 1px solid #ccc;
            padding: 0 5px;
        }
        .grouplist .group-item {
            margin: 6px 0px;
            word-break: break-all;
            padding: 6px 8px;
            border: 1px solid #e4eaec;
            border-radius: 4px;
        }

            .grouplist .group-item a {
                font-size: 1.075em;
                cursor: pointer;
                display: block;
                margin: 0px;
                color: #247cf0;
            }

            .grouplist .group-item span {
                font-size: 0.9em;
                display: block;
                color: #848484;
                width: 100%;
                text-overflow: ellipsis;
                white-space: pre;
                overflow: hidden;
            }

            .grouplist .btn-include-group {    
                display:inline-block;
                color: #525252;
                font-weight: 500;
                margin:  0 0 2px;
                cursor: pointer;
                padding: 4px;
            }
            .grouplist .btn-include-group span{    
                color: #525252;
                font-weight: 700;
                font-size:1.4em;
                line-height:1;
            }
        .inc-group-table input[type=checkbox].grp-check ,
        .inc-group-table input[type=checkbox].selectAllGrp{
                min-width: auto;
                min-height: auto;
                cursor: pointer;
                height: 16px;
                width: 18px;
        }
        .inc-group-table .grp-name {
            width : 150px;
        }
        .inc-group-table .grp-lookup {
            width : 240px;
        }
        .inc-group-table .grp-count {
            width : 75px;
            text-align:center;
        }

        .dataset-list tr.selected {
            background-color: #6b94b7;
        }

            .dataset-list tr.selected a {
                color: white;
            }

        .head-input {
            float: left;
            margin: 6px;
        }

            .head-input label {
                display: inline-block;
                padding-right: 15px;
                font-size: 1.6em;
            }

            .head-input input {
                width: 400px;
                border-radius: 4px;
                padding: 4px;
                font-size: 1.6em;
            }

            .head-input span {
                display: inline-block;
                margin-left: 20px;
                text-decoration: wavy;
                background-color: #ce7600;
                color: white;
                padding: 5px 10px;
                border-radius: 20px;
            }

        .head-links {
            float: right;
            padding: 5px;
            padding-top: 0px;
        }

            .head-links a {
                font-weight: 500;
                font-size: 1.2em;
                text-decoration: underline;
                margin-left: 10px;
                display: inline-block;
            }

        .dataset-list td > span[d] {
                margin-left:0;
            } 
	
		.filter-op-input input{
			display : inline-block;
			min-width:120px;
		}	
        .map-popup {
            transform:none;
            min-width:130px;
        }
        .map-popup>div {
        	margin: 4px 0px 4px 0px;
        	display:flex;
        	justify-content:space-between;
        }
        .map-popup > div >p {
            margin: 0;
            margin-left: 5px;
            font-weight:500;
            font-size:1.0em;
        }
        .filter-search {
            display:flex;
            align-items:center;
            justify-content:space-between;
            margin:10px 0;
        }
        .filter-panel {
            border:1px solid #ddd;
            padding: 6px;
        }
        .filter-panel-head {
            display: flex;
            align-items:center;
            justify-content:space-between;
            margin-bottom: 8px;
        }
        .filter-panel-head h4 {
            font-weight: 600;
            font-size: 1.1em;
        }
        .filter-panel-head >span {
            cursor: pointer;
        }
        .filter-panel .filter-button {
            float: right;
            padding: 6px 10px;
            border-radius: 4px;
            border: none;
            background-color: #1976d2;
            color: #fff;
            font-weight: 600;
            cursor: pointer;
        }

        .filter-button[disabled] {
            background: #c6c6c6 !important;
        }

        .filter-operator-value2 {
            margin-left: 4px;
        }
    
    </style>
    <script>
        var defaultExtent, defaultCenter; // = [-84.8125131750277, 39.9165279215648, -84.4263206569032, 40.3541929564396];
        var datasetId, datasetGroups, dataGroups;
        var M;
        var activeLookupSearch;
        var saveTemplookup;
        var urlDSID;
        var activeMapData;
        function enableLayout() {
            $('.dataset-layout').removeAttr('disabled');
            $('body').css({ 'cursor': 'default' });
        }

        function disableLayout() {
            $('.dataset-layout').attr('disabled', 'disabled');
            $('body').css({ 'cursor': 'default' })
        }

        function resizeWindow() {
            var H = $('.main-content-area')[0].clientHeight;
            var Hh = $('.dse-header')[0].clientHeight + 1;
            $('#mapbox').height(H - Hh);
            $('.grouplist-inner').height(H - $(".grouplist-inner").box().top);
            $('.dataset-layout').height(H);
            $('.ds-list-div').height(H - $(".ds-list-div").box().top - 1);
        }

        var syncLock = false;
        function openDataSetInMap(extent, callback) {
            if (syncLock) return false;
            var resolution = M.getResolution();
            syncLock = true;
            PageMethods.GetMapData(datasetId, extent, function (d) {
                M.clear();
                if (d.Count > 0) {
                    var slat = 0, slng = 0;
                    var pcount = 0;
                    for (var i in d.Data) {
                        var p = d.Data[i];
                        M.addDataPoint([p.Coordinates[0], p.Coordinates[1]], resolution * p.Range, 5, p.ParcelId, null, null, p.Volume)
                        slng += p.Coordinates[0]; slat += p.Coordinates[1];
                        pcount += p.Volume;
                    }

                    var clng = slng / d.Count, clat = slat / d.Count;
                    if (callback) callback([clng, clat], pcount);
                    syncLock = false;
                } else {
                    if (callback) callback(null, 0);
                    syncLock = false;
                }
            })
        }

        activeMapDataFn = () => {
            PageMethods.GetGISConfig(function (d) {
                if (d.Success) {
                    activeMapData = d.Data.grpData;
                }
            })
        };

        function openLookupPicker(datasetId, groupId, lookupName, label, selSize, includeNull) {
            disableLayout();
            var dialogSelector = '.lookup-picker';
            var lookupSelection = [];

            $(dialogSelector).removeAttr('disabled');
            $(dialogSelector).show();
            $('.filter-input').val('');
            $('.filter-title', dialogSelector).html(label)
            $('.filter-field', dialogSelector).val('')
            $('.filter-op-input[field="filtervalue2"]', dialogSelector).hide();
            $('.filter-panel', dialogSelector).hide();
            $('.filter-toggle', dialogSelector).text('Show Filters');
            $('.filter-results', dialogSelector).css('height', '330px');

            var H = $('.main-content-area').height(), W = $('.main-content-area').width();
            var h = $(dialogSelector).height(), w = $(dialogSelector).width();
            $(dialogSelector).css({ top: (H - h) / 2, left: (W - w) / 2 });

            $('.filter-results').html('');
            $('.show-idvalues', dialogSelector)[0].checked = false;
            $('.selectAll-filtered', dialogSelector)[0].checked = false;
            if (selSize == -1) {
                $('.chk-selectall', dialogSelector)[0].checked = true;
                //  $('.filter-results', dialogSelector).attr('disabled', 'disabled');
                // $('.filter-input', dialogSelector).attr('disabled', 'disabled');
            } else {
                $('.chk-selectall', dialogSelector)[0].checked = false;
                //  $('.filter-input', dialogSelector).removeAttr('disabled');
                //  $('.filter-results', dialogSelector).removeAttr('disabled');
            }
            if (includeNull == 1) {
                $('.chk-IncludeNull', dialogSelector)[0].checked = true;
            } else {
                $('.chk-IncludeNull', dialogSelector)[0].checked = false;
            }

            function doSearch(q, filter) {
                var load = '<span style="font-style: italic; text-align: right;font-size: 0.9em;float: left;">Loading....</span>'
                $('.filter-results').html(load);
                var idChecked = $('.show-idvalues', dialogSelector).prop('checked');
                if (!filter) {
                    PageMethods.FindLookupValues(datasetId, groupId, lookupName, idChecked, q || '', function (d) {
                        if (!d.Success) {
                            alert(d.ErrorMessage);
                            return false;
                        }
                        fillValues(d);
                    })
                } else {
                    var op = $('.filter-operator', dialogSelector).val();
                    var value1 = $('.filter-op-input[field="filtervalue"]', dialogSelector).val();
                    var value2 = $('.filter-op-input[field="filtervalue2"]', dialogSelector).val();
                    PageMethods.FilterLookupValues(datasetId, groupId, lookupName, op, value1 != "" ? parseInt(value1) : 0, value2 != "" ? parseInt(value2) : 0, function (d) {
                        if (!d.Success) {
                            say(d.ErrorMessage);

                            return false;
                        }
                        fillValues(d);
                    })
                }

                var fillValues = (d) => {
                    var html = '';
                    var allCheckboxes = $('.lookup-select');
                    for (var i in d.Data) {
                        with (x = d.Data[i]) {
                            if (activeLookupSearch) {
                                saveTemplookup.filter(function (y) {
                                    var findlookupId = saveTemplookup[y].id;
                                    findlookupId = findlookupId.split('_')[2];
                                    if (findlookupId == d.Data[i].ID) {
                                        x.Selected = saveTemplookup[y].checked ? true : false;
                                        x.checked = x.Selected ? 'checked="checked"' : '';
                                        x.s = x.Selected ? 1 : 0;
                                        html += `<div><input type="checkbox" id="lk_f_${ID}" ${checked} sel="${s}" val="${ID}" class="lookup-select" /><label for="lk_f_${ID}">${idChecked ? '[' + ID + ']  ' + Name : Name}</label></div>`;
                                        if (x.Selected && !lookupSelection.includes(x.ID)) lookupSelection.push(x.ID);
                                    }
                                });
                            }
                            else {
                                x.Selected = $('.chk-selectall', dialogSelector)[0].checked ? true : x.Selected;
                                x.checked = x.Selected ? 'checked="checked"' : '';
                                x.s = x.Selected ? 1 : 0;
                                html += `<div><input type="checkbox" id="lk_f_${ID}" ${checked} sel="${s}" val="${ID}" class="lookup-select" /><label for="lk_f_${ID}">${Name ? Name : '[' + ID.slice(1, -1) + ']  '}</label></div>`;
                                if (x.Selected) lookupSelection.push(x.ID);
                            }
                        }
                    }
                    setTimeout(function () {
                        $('.chk-selectall', dialogSelector)[0].checked = $('.lookup-select').length === $('.lookup-select:checked').length;

                    },500)
                  
                    if (html == "" && d.Data.length == 0) {
                        html = '<span style="font-style: italic; text-align: right;font-size: 0.9em;float: right;">No fields found!</span>'
                    }
                    $('.filter-results').html(html);
                    if (!activeLookupSearch && html != '') {
                        saveTemplookup = $('.filter-results .lookup-select');
                    }
                    $('.lookup-picker :input').attr('disabled', false);
                    $('.lookup-picker :submit').attr('disabled', false);
                    $('.filter-toggle').show();

                    if (saveTemplookup.length != d.length) $('.selectAll-filtered', dialogSelector)[0].checked = false;

                    $('.lookup-select').on('change', function () {
                        var checkdVal = this.checked;
                        var v = $(this).attr('val')
                        if (this.checked) {
                            lookupSelection.push(v);
                            if (saveTemplookup.length == lookupSelection.length)
                                $('.chk-selectall', dialogSelector)[0].checked = true;
                        } else {
                            lookupSelection.splice(lookupSelection.indexOf(v), 1);
                            if (!($(".field-results .lookup-select").length == lookupSelection.length))
                                $('.chk-selectall', dialogSelector)[0].checked = false;
                        }
                        saveTemplookup.filter(function (x) {
                            var findlookupId = saveTemplookup[x].id; findlookupId = findlookupId.split('_')[2];
                            if (findlookupId == v) {
                                saveTemplookup[x].checked = checkdVal;
                            }
                        })
                    })
                }
            }

            doSearch();
            activeLookupSearch = false;

            $('.filter-input', dialogSelector).off('keyup').on('keyup', function () {
                var q = $(this).val();
                activeLookupSearch = true;
                doSearch(q);
            });

            $('.lookup-save', dialogSelector).off('click').on('click', function () {
                loading.show("Combing through Parcels, hang on a minute");
                $(dialogSelector).attr('disabled', 'disabled');
                var all = $('.chk-selectall', dialogSelector)[0].checked ? '-1' : '';
                var incNull = $('.chk-IncludeNull', dialogSelector)[0].checked ? 1 : 0;
                $(dialogSelector).hide();
                PageMethods.SaveLookupValues(datasetId, groupId, all, incNull, lookupSelection, function () {
                    loading.hide();
                    openDataset(datasetId);
                    activeMapDataFn();
                    //openLookupPicker(datasetId, groupId, lookupName, label, selSize);
                })

                return false;
            })

            $('.chk-selectall', dialogSelector).off('change').on('change', function () {
                var checked = this.checked;
                lookupSelection = [];
                saveTemplookup.each(function () {
                    if (checked) {
                        var checkAllValue = $(this).attr('val');
                        lookupSelection.push(checkAllValue);
                    }
                    else {
                        var checkAllValue = $(this).attr('val');
                        lookupSelection.splice(lookupSelection.indexOf(checkAllValue), 1);
                    }
                });
                $(".field-results .lookup-select").each(function () {
                    if (checked) {
                        $(this).attr("sel", 1); $(this).prop("checked", true);
                    }
                    else {
                        $(this).attr("sel", 0); $(this).prop("checked", false);
                    }
                });

            })

            $('.selectAll-filtered', dialogSelector).off('change').on('change', function () {
                var checked = this.checked;
                //lookupSelection = [];
                $(".field-results .lookup-select").each(function () {
                    if (checked) {
                        var checkAllValue = $(this).attr('val');
                        if (!lookupSelection.includes(checkAllValue)) lookupSelection.push(checkAllValue);
                        $(this).attr("sel", 1); $(this).prop("checked", true);
                    }
                    else {
                        var checkAllValue = $(this).attr('val');
                        lookupSelection.splice(lookupSelection.indexOf(checkAllValue), 1);
                        $(this).attr("sel", 0); $(this).prop("checked", false);
                    }
                    saveTemplookup.filter(function (x) {
                        var findlookupId = saveTemplookup[x].id; findlookupId = findlookupId.split('_')[2];
                        if (findlookupId == checkAllValue) {
                            saveTemplookup[x].checked = checked;
                        }
                    });
                });

            })

            $('.chk-selectall, .selectAll-filtered', dialogSelector).on('change', function () {
                if (this.checked) {
                    $('.chk-selectall, .selectAll-filtered', dialogSelector).not(this).prop('checked', false);
                }
            });

            $('.show-idvalues', dialogSelector).off('change').on('change', function () {
                var query = $('.filter-input', dialogSelector).val();
                activeLookupSearch = true
                doSearch(query);
            })

            $('.filter-operator', dialogSelector).off('change').on('change', () => {
                var op = $('.filter-operator', dialogSelector).val();
                $('.filter-op-input[field="filtervalue"]', dialogSelector).val('');
                $('.filter-op-input[field="filtervalue2"]', dialogSelector).val('');
                $('.filter-op-input[field="filtervalue2"]', dialogSelector)[op == "BW" ? 'show' : 'hide']();
            });

            $('.filter-toggle', dialogSelector).off('click').on('click', function () {
                var filterVisible = $('.filter-panel', dialogSelector).is(':visible')
                //$('.filter-field', dialogSelector).val('');
                $('.filter-results', dialogSelector).css('height', filterVisible ? '330px' : '300px');
                $('.filter-toggle', dialogSelector).text(filterVisible ? 'Show Filters' : 'Hide Filters');
                $('.filter-panel', dialogSelector)[filterVisible ? 'hide' : 'show']();
                if (filterVisible) {
                    if (saveTemplookup.length != $(".field-results .lookup-select").length) {
                        activeLookupSearch = true;
                        doSearch();
                    }
                }
            });

            $('.filter-op-input', dialogSelector).off('keydown').on('keydown', function (e) {
                if (e.keyCode === 190 || e.keyCode === 110)
                    e.preventDefault();
                if (e.shiftKey || e.keyCode > 31 && (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105))
                    e.preventDefault();
            });

            $('.filter-ok', dialogSelector).off('click').on('click', function () {
                var valid = true;
                var op = $('.filter-operator', dialogSelector).val();
                var value1 = $('.filter-op-input[field="filtervalue"]', dialogSelector).val();
                var value2 = $('.filter-op-input[field="filtervalue2"]', dialogSelector).val();
                if (op == "")
                    return false;
                if (op != "") {
                    if (value1 == "") valid = false;
                    if (op == "BW")
                        if (value2 == "") valid = false;
                }
                if (!valid) {
                    say("Please fill the values for the condition");
                    return false;
                }
                activeLookupSearch = true;
                $('.lookup-picker :input').attr('disabled', true);
                $('.lookup-picker :submit').attr('disabled', true);
                $('.filter-toggle').hide();
                doSearch('', true);
                return false;
            });
            $('.filter-box-close', dialogSelector).on('click', function () {
                enableLayout();
                $(dialogSelector).hide();
            });
        }

        function openDataset(dsId, selectOnly) {
            urlDSID = dsId;
            history.replaceState(null, null, '?ds=' + dsId);
            loading.show("Combing through Parcels, hang on a minute");
            $('tr', '.dataset-list').removeClass('selected');
            if (selectOnly) {
                $('tr[dsid="' + datasetId + '"]', '.dataset-list').addClass('selected');
                return;
            }
            disableLayout();
            datasetId = dsId;
            datasetGroups = {};
            PageMethods.CheckErrorInSet(datasetId, function (d) {
                var recompile = false;
                if (!d.Success) {
                    ask('There is change done in the Data groups which causes a change in the count of this Dataset. Do you wish to calculate the new count?',
                        () => {
                            recompile = true;
                            PageMethods.GetDataset(datasetId, recompile, function (r) {
                                loadDataSet(r);
                            })
                        },
                        () => {
                            PageMethods.GetDataset(datasetId, recompile, function (r) {
                                loadDataSet(r);
                            })
                        });
                }
                else {
                    PageMethods.GetDataset(datasetId, recompile, function (r) {
                        loadDataSet(r);
                    })
                }
            });
        }
        function loadDataSet(r) {
            $('tr[dsid="' + datasetId + '"]', '.dataset-list').addClass('selected');
            $('.dataset-editor').removeAttr('disabled');
            loading.hide();
            if (!r.Success) {
                alert(r.ErrorMessage);
                return false;
            }
            var ds = r.Data;

            $('.ds-name').val(ds.Name);
            if (!ds.IsEmpty) {
                $('.poolsize').html(ds.PoolSize + ' parcels');
                $('.poolsize').show();
            } else {
                $('.poolsize').hide();
            }
            var poolSize = ds.PoolSize || 0;
            var html = '';
            ds.Groups.forEach(function (x) {
                with (x) {
                    html += `<div class="group-item"><a groupid="${GroupID}" >${Label}</a><span>${SelectionText}</span></div>`;
                }
                datasetGroups[x.GroupID] = x;
            });

            $('.grouplist-inner').html(html);
            $('a', '.grouplist-inner').on('click', function () {
                var a = this, A = $(this);
                var g = A.attr('groupid'), G = datasetGroups[g];
                openLookupPicker(datasetId, g, G.LookupName, G.Label, G.Items, G.IncNull);
            })

            $('.ds-name').off('change').on("change", function () {
                if ($('.ds-name').val().length > 50) {
                    alert('Please enter a Dataset Name within the character limit 40');
                    $('.ds-name').val($('.ds-name').val().substring(0, 50))
                }
                PageMethods.RenameDataset(datasetId, $('.ds-name').val(), function (d) {
                    if (d.Success) {
                        $('.ds-name').val(d.Data);
                        ds.Name = d.Data;
                        $('.selected td a div')[0].innerText = ds.Name;
                        $('.selected td a div')[0].title = ds.Name;
                        /* Commented because it recalculates the whole dataset while remaining
 ul                       refreshDatasets(function () {
                            openDataset(datasetId, true);
                        })*/
                    }
                    else {
                        alert(d.ErrorMessage);
                        $('.ds-name').val(ds.Name);
                    }
                })
            });

            openDataSetInMap(defaultExtent, function (p, c) {
                M.resetView(p);
                enableLayout();
                if (poolSize > 0 && c == 0) {
                    alert('None of the parcels in current dataset criteria have GIS coordinates.')
                }
            })

        }

        function resetView() {
            $('.poolsize').hide();
            $('.ds-name').val('');
            M.clear();
            $('.grouplist-inner').html('');
        }

        function refreshDatasets(callback) {
            disableLayout();
            activeMapDataFn();
            PageMethods.GetDatasets(function (d) {
                var html = '';
                if (d.Success) {
                    let urlIdCheck = false;
                    d.Data.forEach(function (x) {
                        with (x) {
                            html += `<tr dsid='${ID}'><td><a style="width: 215px;display: inline-flex;" dsid='${ID}'><div title="${Name}" style ="white-space: pre;text-overflow: ellipsis;overflow: hidden;">${Name}</div></a><span d="1" onclick="deleteDataset(${ID})"></span></td></tr>`;
                        }
                        if (urlDSID == x?.ID) urlIdCheck = true;
                    });
                    if (!urlIdCheck) urlDSID = d.Data[0]?.ID;
                    $('.dataset-list').html(html);
                    $('a', '.dataset-list').on("click", function () {
                        var dsid = $(this).attr('dsid');
                        openDataset(dsid);
                    })
                    callback && callback(d.Data);

                }
                else {
                    alert(d.ErrorMessage);
                }
                enableLayout();
            })
        }

        function CreateDatasetNewBtnClick() {
            loading.show("Creating New Dataset...");
            createDataset();
        }

        function createDataset() {
            PageMethods.CreateDataset(function (d) {
                if (d.Success) {
                    var newId = d.Data;
                    refreshDatasets(function (d) {
                        loading.hide();
                        openDataset(newId);
                    })
                }
                else {
                    alert(d.ErrorMessage);
                }
            })
        }

        function deleteDataset(dsid) {
            let did = dsid || datasetId;
            let title = $('.dataset-list td>a[dsid=' + did + ']').text();
            title
            ask('Are you sure you want to delete the dataset - ' + title + ' permanently?', () => {
                resetView();
                $('.lookup-picker').hide();
                loading.show('Deleting ' + title);
                PageMethods.DeleteDataset(did, function (d) {
                    if (d.Success) {
                        refreshDatasets(function (d) {
                            loading.hide();
                            if (d.length > 0) {
                                if (did == urlDSID) openDataset(d[0].ID);
                                else openDataset(urlDSID);
                            } else {
                                $('.dataset-editor').attr('disabled', 'disabled');
                            }
                        })
                    }
                    else {
                        alert(d.ErrorMessage);
                    }
                })
            });
        }

        function showMapPopup(val, pd, left, top) {
		    let mp = $(".map-popup").show();
            $('.map-popup-pf').remove();
		    $(name + " span", mp).text(val.Count == 1? "Parcel: " : "No of Parcels: ");
		    val.Count == 1? $(name + " p", mp).html('<a href="/protected/quality/#/parcel/' + val.ParcelId + '" target="_blank">' + val.ParcelId + "</a>"): $(name + " p", mp).text(val.Count);
		    if (val.Count == 1)
            {
                $('.map-popup-p').css('display', 'inline-flex');

                var filteredObjectWithoutDataSetId = pd.map(function (item) {
                    var { DataSetId, CC_parcelid, ...filteredItem} = item;
                    return filteredItem;
                });

                var htmlVal = '<p class="map-popup-pf">'
                var objectToIterate = filteredObjectWithoutDataSetId[0];

                for (var key in objectToIterate) {
                    if (objectToIterate.hasOwnProperty(key)) {
                        htmlVal += key + ': ' + objectToIterate[key] + '<br>';
                    }
                }

                htmlVal += '</p>';
                $('.map-popup-p').after(htmlVal);           
            }
            mp.css({
		        left,
		        top,
		    });
		    $(document)
		    .off("click")
		    .on("click", (e) => {
		        var mapViewDiv = $(".ol-unselectable");
	            if (!mapViewDiv.is(e.target)) {
	                if (!e.target.closest(".map-popup")){ 
                    $('.map-popup-p').css('display', '');
                    hideMapPopup();
                    }
	            }
		    });
		}

        function hideMapPopup() {
            $(".map-popup").hide();
        }

        $(() => {
            urlDSID = Query.get('ds');
            resizeWindow();
            disableLayout();

            PageMethods.GetAllGroup(function (d) {
                if (d.Success) {
                    dataGroups = d;
                }
                else {
                    alert(d.ErrorMessage || 'Data groups are unavailable!')
                }
            })

            PageMethods.GetGISConfig(function (d) {
                if (d.Success) {
                    var px = d.Data;
                    var px1 = d.Data.grpData;
                    defaultExtent = [px.lg1, px.lt1, px.lg2, px.lt2];
                    defaultCenter = [(px.lg1 + px.lg2) / 2, (px.lt1 + px.lt2) / 2];
                    M = new MapView('mapbox', defaultExtent, defaultCenter);
                    M.viewChanged = function (e) {
                        openDataSetInMap(e);
                    }
                    M.viewChangeStart = (e) => {
                        hideMapPopup();
                    };
                    M.mapClick = (e, event) => {
                        let left = event && event.pointerEvent && event.pointerEvent.clientX;
                        let top = event && event.pointerEvent && event.pointerEvent.clientY;
                        let pw = $(".popup-dg.map-popup").outerWidth(),
                            ph = $(".popup-dg.map-popup").outerHeight();
                        left = left > $(window).innerWidth() - pw ? left - pw - 5 : left + 5;
                        top = top > $(window).innerHeight() - ph - 30 ? top - ph - 5 : top + 5;

                        if (e && e.length) {
                            var pxData = activeMapData === undefined  ? px1 : activeMapData;
                            var filteredDataSetObjects = pxData.filter(function (item) {
                                return item.DataSetId == urlDSID && item.CC_parcelid == e[0].values_.ParcelId;
                            });
                            showMapPopup(e[0].values_, filteredDataSetObjects, left, top - 75);
                        }
                        else hideMapPopup();
                    };
                    resetView();

                    refreshDatasets(function (d) {
                        if (d.length > 0) {
                            openDataset(urlDSID);
                        } else {
                            $('.dataset-editor').attr('disabled', 'disabled');
                        }
                    })
                } else {
                    alert(d.ErrorMessage || 'GIS Configuration unavailable!')
                }
            })


            $('.btn-include-group', '.grouplist').on('click', function () {
                $('.dataset-layout').attr('disabled', 'disabled');
                var selectedGroup = [];
                var deletedGroup = [];
                var activeGroups = [];
                var html = '';
                var includedGrp = Object.keys(datasetGroups);
                if (dataGroups.Count == 0) {
                    say("No other data groups are added. Please add one to continue")
                    $('.dataset-layout').removeAttr('disabled');
                    return false;
                }
                $('.inc-group-table tr:not(:first-child)').remove();
                for (var i in dataGroups.Data) {
                    with (x = dataGroups.Data[i]) {
                        html += `<tr><td><input type="checkbox" class ="grp-check" GpId =${Id}></td>`;
                        html += `<td><span class="grp-name" title=${Name}>${Name}</span></td><td><span class="grp-lookup" title=${Lookup}>${Lookup}</span></td>`
                        html += `<td><span class="grp-count">${Datacount}</span></td></tr>`
                    }
                }
                $('.inc-group-table').append(html);
                var activeCount = 0;
                for (var i in datasetGroups) {
                    var currentGrp = datasetGroups[i].GroupID;
                    currentGrp != -1 && activeGroups.push(currentGrp + '');
                    $('.grp-check[gpid=' + currentGrp + ']').attr('checked', 'checked');
                    $('.grp-check[gpid=' + currentGrp + ']').length && activeCount++;
                }
                if (activeCount == dataGroups.Count)
                    $('.selectAllGrp').prop("checked", true);
                else
                    $('.selectAllGrp').prop("checked", false);

                $('.group-picker').show();

                $('.grp-check').off('click').on('click', function () {
                    var checkdgrp = this.checked;
                    var grpId = $(this).attr('gpid')
                    var count = 0;
                    if (this.checked) {
                        (selectedGroup.indexOf(grpId) == -1) && selectedGroup.push(grpId);
                        (deletedGroup.indexOf(grpId) >= 0) && deletedGroup.splice(deletedGroup.indexOf(grpId), 1);
                    }
                    else {
                        (selectedGroup.indexOf(grpId) >= 0) && selectedGroup.splice(selectedGroup.indexOf(grpId), 1);
                        (deletedGroup.indexOf(grpId) == -1) && deletedGroup.push(grpId);
                    }
                    $(".group-picker .grp-check").each(function (x) {
                        count += $(this).attr('checked') ? 1 : 0;
                    });
                    if (count == dataGroups.Count)
                        $('.selectAllGrp').prop("checked", true);
                    else
                        $('.selectAllGrp').prop("checked", false);

                });

                $('.selectAllGrp').off('click').on('click', function () {
                    var checked = this.checked;
                    selectedGroup = [];
                    deletedGroup = [];
                    $(".group-picker .grp-check").each(function () {
                        if (checked) {
                            var checkAllValue = $(this).attr('gpid');
                            selectedGroup.push(checkAllValue);
                            deletedGroup.splice(selectedGroup.indexOf(checkAllValue), 1);
                            $(this).prop("checked", true);
                        }
                        else {
                            var checkAllValue = $(this).attr('gpid');
                            selectedGroup.splice(selectedGroup.indexOf(checkAllValue), 1);
                            deletedGroup.push(checkAllValue);
                            $(this).prop("checked", false);
                        }
                    });
                });

                $('.group-save').off('click').on('click', function () {
                    $('.group-picker').hide();
                    if (selectedGroup.length == 0 && deletedGroup.length == 0) {
                        enableLayout();
                        return false
                    }
                    for (let i in activeGroups) {
                        let grpId = activeGroups[i];
                        (selectedGroup.indexOf(grpId) >= 0) &&
                            selectedGroup.splice(selectedGroup.indexOf(grpId), 1);
                    }


                    loading.show('Saving Group Selection');

                    PageMethods.SaveGroup(datasetId, selectedGroup, deletedGroup, function (d) {
                        if (d.Success) {
                            loading.hide();
                            openDataset(datasetId);
                        }
                        else {
                            alert(d.ErrorMessage);
                        }
                    });

                });
            });

        })

        $(window).on("resize", resizeWindow);
        function filterPastedValueValidation(event, thisValue) {
            var valu = thisValue.value;
            var regex = /^[0-9]+$/;

            if (event.inputType === 'insertFromPaste' && !regex.test(valu)) {
                thisValue.value = '';
            }
        }
        var flag = false;
        function filterClearOnIdView() {
            var checkboxElement = document.getElementById('showId');
            flag = checkboxElement.checked;

            var inputElement = document.getElementById('fltrV1');
            if (flag) {
                inputElement.value = '';
            }
            if (!flag) {
                inputElement.value = '';
            }
        }
    </script>
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div class="dataset-layout">
        <div class="">
            <div class="data-col-head">
                <span>Dataset Name</span>
                <a class="b2 new-btn" onclick="CreateDatasetNewBtnClick();">New</a>
            </div>
            <div class="ds-list-div vscroll thin-scrollbar">
                <table class="mGrid app-content" cellspacing="0" style="width: 100%; border-collapse: collapse;">
                    <tbody class="dataset-list common-list"></tbody>
                </table>
            </div>
        </div>
        <div class="v-spacer" style="display: none;"></div>
        <div class="dataset-editor vscroll">
            <div class="dse-header">
                <div>
                    <div class="head-input">
                        <label>Dataset:</label>
                        <input type="text" class="ds-name" maxlength="40"/>
                        <span class="poolsize"></span>
                    </div>
                    <div class="head-links" onclick="deleteDataset();">
                        <a>Delete Dataset</a>
                    </div>
                </div>
            </div>
            <div>
                <div id="mapbox"></div>
                <div class="v-spacer" style="display: none;"></div>
                <div class="grouplist">
                    <div class="grouplist__head flex align-center justify-between">
                        <h3 class="nomar b2">Grouping Filters</h3>
                        <p class="c nomar">
                            <a href="../settings/groups.aspx" class="l_s1">Manage</a>
                        </p>
                    </div>
                    <p class="btn-include-group" title="Include already added Groups in this DataSet"><span>+</span>Include Groups</p>
                    <div class="grouplist-inner vscroll thin-scrollbar">
                    </div>                    
                </div>
            </div>
        </div>
    </div>

    <div class="filter-box lookup-picker" style="width: 500px; display: none;">
        <div class="filter-head">
            <a class="filter-box-close">✖</a>
            <h1 class="nomar filter-title"  style="white-space: pre;text-overflow: ellipsis;overflow: hidden;":pre>Select</h1>
        </div>
        <div>
            <span class="filter-select-all filter-checkbox">
                <input type="checkbox" id="chkselectall" class="chk-selectall" />
                <label for="chkselectall">Select all from Lookup</label>
            </span>
            <span class="filter-include-null filter-checkbox">
                <input type="checkbox" id="chkIncludeNull" class="chk-IncludeNull" />
                <label for="chkIncludeNull">Include Null Values</label>
            </span>
            <span class="filter-checkbox" style=" float: right;">
                <input type="checkbox" id="chkToggleFilterPanel" style="visibility:hidden" />
                <label for="chkToggleFilterPanel">
                    <a style="" class="filter-toggle">Show Filters</a>
                </label>                
            </span>
        </div>
        <div class= "filter-panel" style ="display:none;">
            <div class="filter-panel-head">
                <h4 class="nomar">Filters - <i style="color:#ff2424;font-size: 0.94em;">Can only be applied on ID values</i></h4>
                <span class="">
                    <input type="checkbox" id="showId" class="show-idvalues" onchange="filterClearOnIdView()"/>
                    <label for="showId">Show ID values</label>
                </span>
            </div>
            <div class="">
                <select class="filter-field filter-operator" field="filterop">
                    <option value="">None</option>
                    <option value="NE">Not Equal To</option>
                    <option value="GT">Greater Than</option>
                    <option value="LT">Less Than</option>
                    <option value="GE">Greater/Equal To</option>
                    <option value="LE">Lesser/Equal To</option>
                    <option value="BW">Between</option>
                </select>
                <div style= "display:inline-flex;"> 
                    <div class="filter-operator-value1">
                        <input id="fltrV1" oninput="filterPastedValueValidation(event,this)" type="text" pattern = "[0-9]+" style ="min-width:80px;width:120px;padding:0 5px;" class="filter-field filter-op-input fltrV" field="filtervalue" maxlength="10" /> 
                    </div>	
                    <div class="filter-operator-value2">
                        <input oninput="filterPastedValueValidation(event,this)" type="text" pattern = "[0-9]+" style ="min-width:80px;width:120px;padding:0 5px;display:none;" class="filter-field filter-op-input fltrV" field="filtervalue2" maxlength="10" />
                    </div>
                </div>
				<button class="filter-button filter-ok">Apply</button>
			</div>
        </div>
        <div class="filter-search" style="display:flex;">	
            <span class="label" style="width : 50px;">Search:</span>
            <input class="filter-input" style="flex-grow:1" />
        </div>
        <div class="field-results filter-results" style="height: 330px;"> </div>
        <div class= "filter-footer" style="display:flex;align-items:center;">
	        <div class= "select-fileterd">
                <input type="checkbox" id="selectAllFiltered" class="selectAll-filtered" />
                <label for="selectAllFiltered">Select all from Filtered</label>
             </div>       
	        <div class="r filter-button-panel" style="flex-grow:1;">
	            <button class="filter-button lookup-save">
	                Save Selection
	            </button>
	        </div>
	    </div>    
    </div>
    <div class="shade dock-full" style="display: none;"></div>
    <div class="popup-dg map-popup" style="display: none;">
        <div class="map-popup-p" count><span>No of Parcels : </span><p></p></div>
    </div>
    <div class="filter-box popup-dg group-picker" style="max-width:700px;display:none">
        <div class="filter-head">
            <a class="filter-box-close" onclick="$('.group-picker').hide();enableLayout()">✖</a>
            <h1 class="nomar filter-title">Select DataGroups</h1>
            <div class="vscroll thin-scrollbar" style="margin: 10px 0 16px;max-height:350px;">
        		<table class="inc-group-table table-new">	
        			<tr>
        				<th>
        					<div style=""><input type="checkbox" class= "selectAllGrp"></div>
    					</th>
        				<th class="grp-name">Name</th>
        				<th class="grp-lookup">Lookup</th>
        				<th class="grp-count">Data Count</th>
        			</tr>
      			</table>
         	</div>
         	<div class="r filter-button-panel">
            <span class="filter-button group-save btn--primary">
                Save
            </span>
        </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FootContents" runat="server">
</asp:Content>
<script runat="server">

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetDataset2(datasetId As Integer) As ActionResult(Of Dataset)
        Dim l As New ActionResult(Of Dataset)
        Try
            l.Data = Dataset.GetDataSet(datasetId)
            If l.Data IsNot Nothing Then
                l.Success = True
                l.Count = 1
                l.FullCount = l.Count + 1
            End If
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        'l.Query = q
        Return l
    End Function

</script>
