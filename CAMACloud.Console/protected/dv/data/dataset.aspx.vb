﻿Imports System.Web.Script.Services
Imports System.Web.Services
Imports CAMACloud.BusinessLogic.DataAnalyzer

Public Class datasets
    Inherits System.Web.UI.Page

#Region "Page Methods"

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetGISConfig() As ActionResult(Of GISConfig)
        Dim l As New ActionResult(Of GISConfig)
        Try
            Dim dt As DataTable = Database.Tenant.GetDataTable("EXEC GIS_GetFilterdata")
            l.Data = Database.Tenant.GetDataTable("EXEC GIS_GetConfiguration").ConvertToList(Of GISConfig).FirstOrDefault

            l.Data.grpData = dt.ConvertToList()
            l.Success = l.Data IsNot Nothing
            l.Count = 1
            l.FullCount = l.Count
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        'l.Query = q
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetDatasets() As ActionResultList(Of Dataset)
        Dim l As New ActionResultList(Of Dataset)
        Try
            l.Data = Database.Tenant.GetDataTable("SELECT *, CAST(CASE WHEN PoolSize IS NULL THEN 1 ELSE 0 END AS BIT) As IsEmpty FROM MRA_DataSet ORDER BY Name").ConvertToList(Of Dataset)
            l.Success = True
            l.Count = l.Data.Count
            l.FullCount = l.Count
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        'l.Query = q
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function CreateDataset() As ActionResult(Of Integer)
        Dim l As New ActionResult(Of Integer)
        Try
            Dim newseq = Database.Tenant.GetIntegerValue("SELECT ISNULL(MAX(Sequence),0) + 1 FROM MRA_DataSet")
            Dim newName = "Dataset" & newseq
            Dim newId = Database.Tenant.GetIntegerValue("INSERT INTO MRA_DataSet (Name, Sequence, CreatedBy) VALUES ({0}, {1}, {2}); SELECT CAST(@@IDENTITY AS INT) AS NewId;".SqlFormat(False, newName, newseq, ""))

            ''Database.Tenant.Execute("INSERT INTO MRA_DataSetGroups SELECT {0}, ID, 0, 0 FROM MRA_DataGroups WHERE IsActive = 1".SqlFormat(True, newId))
            Dim newDataSetId = Database.Tenant.GetIntegerValue("INSERT INTO MRA_DataSetGroups(DataSetID,GroupID,SelectedSize,IncludeNull) SELECT {0}, -1, -1, 0;SELECT CAST(@@IDENTITY AS INT) AS NewId".SqlFormat(True, newId))
            Database.Tenant.Execute("INSERT INTO MRA_DataSetGroupValues(DataSetGroupID,Value) SELECT {0} ,n.Id FROM Neighborhood n INNER JOIN MRA_DataSet d ON d.ID = {1} LEFT OUTER JOIN MRA_DataSetGroups g ON g.DataSetID = d.ID AND g.GroupID = -1 LEFT OUTER JOIN MRA_DataSetGroupValues gv ON gv.DataSetGroupID = g.ID AND gv.Value = CAST(n.Id As VARCHAR(100))".SqlFormat(True, newDataSetId, newId))
            Dataset.CompileDataset(newId)

            l.Data = newId
            l.Success = True
            l.Count = 1
            l.FullCount = l.Count
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        'l.Query = q
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function DeleteDataset(datasetId As Integer) As ActionResult(Of Boolean)
        Dim l As New ActionResult(Of Boolean)
        Try
            l.Data = Dataset.DeleteDataset(datasetId)
            l.Success = True
            l.Count = 0
            l.FullCount = l.Count
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        'l.Query = q
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function CheckErrorInSet(datasetId As Integer) As ActionResult(Of Dataset)
        Dim l As New ActionResult(Of Dataset)
        Try
            Dim ErrorSet = Database.Tenant.GetIntegerValue("SELECT Error FROM MRA_DataSet WHERE ID = {0}".SqlFormat(True, datasetId))
            If ErrorSet = 0 Then
                l.Success = True
            Else
                l.Success = False
            End If
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        'l.Query = q
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetDataset(datasetId As Integer, recompile As Boolean) As ActionResult(Of Dataset)
        Dim l As New ActionResult(Of Dataset)
        Try
            Dim DefaultCheck As Integer = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM MRA_DataSetGroups WHERE DataSetID = {0} and GroupID = -1".SqlFormat(True, datasetId))
            If DefaultCheck = 0 Then
                Dim newDataSetId = Database.Tenant.GetIntegerValue("INSERT INTO MRA_DataSetGroups(DataSetID,GroupID,SelectedSize,IncludeNull) SELECT {0}, -1, -1, 0;SELECT CAST(@@IDENTITY AS INT) AS NewId".SqlFormat(True, datasetId))
                Database.Tenant.Execute("INSERT INTO MRA_DataSetGroupValues(DataSetGroupID,Value) SELECT {0} ,n.Id FROM Neighborhood n INNER JOIN MRA_DataSet d ON d.ID = {1} LEFT OUTER JOIN MRA_DataSetGroups g ON g.DataSetID = d.ID AND g.GroupID = -1 LEFT OUTER JOIN MRA_DataSetGroupValues gv ON gv.DataSetGroupID = g.ID AND gv.Value = CAST(n.Id As VARCHAR(100))".SqlFormat(True, newDataSetId, datasetId))
            End If
            If recompile = True Then
                Dataset.CompileDataset(datasetId)
            End If
            l.Data = Dataset.GetDataSet(datasetId)
            If l.Data IsNot Nothing Then
                l.Success = True
                l.Count = 1
                l.FullCount = l.Count
            End If
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        'l.Query = q
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetDataSetGroups(datasetId As Integer) As ActionResultList(Of DataSetGroupItem)
        Dim l As New ActionResultList(Of DataSetGroupItem)
        Try
            l.Data = Database.Tenant.GetDataTable("EXEC MRA_GetDataGroupsForSet " & datasetId).ConvertToList(Of DataSetGroupItem)
            l.Success = True
            l.Count = l.Data.Count
            l.FullCount = l.Count
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        'l.Query = q
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function FindLookupValues(datasetId As Integer, groupId As Integer, lookupName As String, checkId As Boolean, q As String) As ActionResult(Of List(Of LookupValue))
        Dim l As New ActionResult(Of List(Of LookupValue))
        Try
            Dim dt As DataTable = Database.Tenant.GetDataTable("EXEC MRA_FindLookupValue {0}, {1}, {2}, {3}, {4}".SqlFormat(True, datasetId, groupId, lookupName, q, checkId))
            l.Data = dt.Rows.Cast(Of DataRow).Select(Function(x) New LookupValue With {.ID = x.GetString("Id"), .Selected = x.GetBoolean("Selected"), .Name = x.GetString("Name")}).ToList
            l.Success = True
            l.Count = l.Data.Count
            l.FullCount = l.Count
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        'l.Query = q
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function FilterLookupValues(datasetId As Integer, groupId As Integer, lookupName As String, op As String, value1 As Double, value2 As Double) As ActionResult(Of List(Of LookupValue))
        Dim l As New ActionResult(Of List(Of LookupValue))
        Try
            Dim count As Integer = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM ParcelDataLookup WHERE LookupName = {0} AND ISNUMERIC(IdValue) = 0 AND IdValue <> '' AND IDValue IS NOT NULL".SqlFormat(True, lookupName))
            If count > 0 Then
                l.Success = False
                l.ErrorMessage = "Id value of the lookup has non numeric values. Filtering conditions cannot apply"
            Else
                Dim dt As DataTable = Database.Tenant.GetDataTable("EXEC MRA_FilterLookupValue {0}, {1}, {2}, {3}, {4}, {5}".SqlFormat(True, datasetId, groupId, lookupName, op, value1, value2))
                l.Data = dt.Rows.Cast(Of DataRow).Select(Function(x) New LookupValue With {.ID = x.GetString("Id"), .Selected = x.GetBoolean("Selected"), .Name = x.GetString("Name")}).ToList
                l.Success = True
                l.Count = l.Data.Count
                l.FullCount = l.Count
            End If
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        'l.Query = q
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetAllGroup() As ActionResult(Of List(Of AllDataGroup))
        Dim l As New ActionResult(Of List(Of AllDataGroup))
        Try
            Dim dt As DataTable = Database.Tenant.GetDataTable("SELECT ID,Label,LookupName,ISNULL(PoolSize,0) As PoolSize from MRA_DataGroups")
            l.Data = dt.Rows.Cast(Of DataRow).Select(Function(x) New AllDataGroup With {.Id = x.GetString("ID"), .Name = x.GetString("Label"), .Lookup = x.GetString("LookupName"), .Datacount = x("PoolSize")}).ToList
            l.Success = True
            l.Count = l.Data.Count
            l.FullCount = l.Count
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        'l.Query = q
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function SaveGroup(datasetId As Integer, groupIds() As String, deletedGrps() As String) As ActionResult(Of Boolean)
        Dim l As New ActionResult(Of Boolean)
        Try
            If deletedGrps.Length > 0 Then
                Dim deleteGroupValues = deletedGrps.Select(Function(x) "DELETE FROM MRA_DatasetGroupValues WHERE DataSetGroupID IN (SELECT ID FROM MRA_DataSetGroups WHERE DataSetID ={0} and GroupID= {1})".SqlFormat(False, datasetId, x)).Aggregate(Function(x, y) x + vbNewLine + vbNewLine + y)
                Dim deleteSetGroups = deletedGrps.Select(Function(x) "DELETE FROM MRA_DataSetGroups WHERE DataSetID ={0} and GroupID= {1}".SqlFormat(False, datasetId, x)).Aggregate(Function(x, y) x + vbNewLine + vbNewLine + y)
                Database.Tenant.Execute(deleteGroupValues + vbNewLine + deleteSetGroups)
            End If
            If groupIds.Length > 0 Then
                Dim insertSelectSql = groupIds.Select(Function(x) "SELECT {0}, {1}, 0, 0".SqlFormat(False, datasetId, x)).Aggregate(Function(x, y) x + vbNewLine + " UNION " + vbNewLine + y)
                Dim insertSql = "INSERT INTO MRA_DataSetGroups (DataSetID,GroupID,SelectedSize,IncludeNull) " + vbNewLine + insertSelectSql
                Database.Tenant.Execute(insertSql)
            End If
            Dataset.CompileDataset(datasetId)
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        'l.Query = q
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function SaveLookupValues(datasetId As Integer, groupId As Integer, selectAction As String, includeNull As String, selection() As String) As ActionResult(Of Boolean)
        Dim l As New ActionResult(Of Boolean)
        Try
            l.Data = Dataset.SaveLookupValues(datasetId, groupId, selectAction, includeNull, selection)
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        'l.Query = q
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function RenameDataset(datasetId As Integer, name As String) As ActionResult(Of String)
        Dim l As New ActionResult(Of String)
        Try
            l.Data = Dataset.Rename(datasetId, name)
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        'l.Query = q
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetMapData(datasetId As Integer, range() As Single) As ActionResult(Of List(Of OpenLayerParcelData))
        Dim l As New ActionResult(Of List(Of OpenLayerParcelData))
        Try
            Dim dt As DataTable = Database.Tenant.GetDataTable("EXEC MRA_GetParcelCountStatisticsForMap 'dataset', {0}, {1}, {2}, {3}, {4}".SqlFormat(True, datasetId, range(1), range(3), range(0), range(2)))
            l.Data = dt.Rows.Cast(Of DataRow).Select(Function(x) New OpenLayerParcelData(x("Latitude"), x("Longitude")) With {.Range = x("Range"), .Intensity = x("Intensity"), .Volume = x("Volume"), .ParcelId = x("ParcelId")}).ToList
            l.Success = True
            l.Count = l.Data.Count
            l.FullCount = l.Count
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        'l.Query = q
        Return l
    End Function

#End Region

End Class

Public Class LookupValue
    Public Property ID As String
    Public Property Name As String
    Public Property Selected As Boolean
End Class

Public Class AllDataGroup
    Public Property Id As Integer
    Public Property Name As String
    Public Property Lookup As String
    Public Property Datacount As Integer
End Class

Public Class OpenLayerParcelData
    Public ReadOnly Property Coordinates As Single()
    Public Property Range As Integer
    Public Property Intensity As Integer
    Public Property Volume As Integer
    Public Property ParcelId As Integer
    Public Sub New(latitude As Single, longitude As Single)
        Coordinates = New Single() {longitude, latitude}
    End Sub
End Class

Public Class GISConfig
    Public Property lt1 As Decimal
    Public Property lt2 As Decimal
    Public Property lg1 As Decimal
    Public Property lg2 As Decimal
    Public Property grpData As JSONTable
End Class




