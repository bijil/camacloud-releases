﻿<%@ Page Title="Data Analyzer - Dashboard" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DesktopWeb.Master" CodeBehind="default.aspx.vb" Inherits="CAMACloud.Console._default8" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        //$(function () {
        //    $("#spanMainHeading").html('Cost Prediction / Regression Models');
        //})
    </script>
    <style>
        .main-content-area {
            background-color: #f1f4f5 !important;
            box-sizing: border-box;
        }

            .main-content-area * {
                box-sizing: border-box;
            }

            .main-content-area h1 {
                font-size: 2em;
                font-weight: 300;
                margin: 0px 0px 12px 0px;
            }

            .main-content-area h2 {
                font-size: 1.3em;
                font-weight: 500;
            }

            .main-content-area h3 {
                font-size: 1.1em;
                font-weight: 500;
            }

        .dashboard-model-header {
            background-color: #e3e9f7;
            border: 1px solid #d5d8e1;
            padding: 4px;
            margin-bottom: 8px;
        }

            .dashboard-model-header .header-links {
                float: right;
                padding: 7px 15px;
            }

        .select-level1 {
            font-size: 1.3em;
            padding: 3px 8px;
            border: 1px solid #d5d8e1;
            border-radius: 8px;
            min-width: 400px;
            max-width: 600px;
        }

        .data-group-item {
            min-width: 140px;
            border: 1px solid #d5d8e1;
            border-radius: 8px;
            margin: 8px 15px 5px 0px;
            padding: 5px 15px;
            float: left;
        }

            .data-group-item > div {
                text-align: center;
                font-weight: 500;
            }

            .data-group-item > span {
                display: block;
                text-align: center;
                font-size: 1.6em;
                padding: 6px;
            }

            .data-group-item > a {
            }        

        .dashboard-item-footer {
            clear: both;
            text-align: right;
            font-weight: 500;
        }

            .dashboard-item-footer > a {
                cursor: pointer;
                text-decoration: underline;
                display: inline-block;
                margin: 0px 5px 0px 10px;
            }
    </style>
    <script>
        $(() => {
            $('.model-link').on('click', function () {
                var d = { templateId: $(this).attr('tid') };
                var url = ($(this).attr('editable') == "1" ? 'mra.aspx?' : 'mra-result.aspx?') + URLPack(d) + '&t=' + (new Date()).getTime();
                window.location.href = url;
            })

            $('.clone-link').on('click', function () {
                var d = { templateId: $(this).attr('tid'), clone: true };
                var url = 'mra.aspx?' + URLPack(d) + '&t=' + (new Date()).getTime();
                window.location.href = url;
                return false;
            })

        });

        function verifyDataGroups() {
            var valid = $('.datagroup').length > 0;
            if (!valid) {
                say('Please create data groups before proceeding to this stage.');
                return false;
            }
            return true;
        }

        function verifyDatasets() {
            var valid = $('.dataset').length > 0;
            if (!valid) {
                say('Please create data sets before proceeding to this stage.');
                return false;
            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <div class="content-area-wrapper content-area-wrapper-padded">
            <div class="dashboard-cell">
                <div class="dashboard-cell-header">
                    <h2>Data Groups</h2>
                    <div class="dashboard-cell-header-action">
                        <a class="btn" href="settings/groups.aspx?option=add" title="Add Data Group"> 
                            <span class="icon icon-add icon-brown"></span>Add
                        </a>
                        <a class="btn" href="settings/groups.aspx" title="Manage Data Groups">
                            <span class="icon icon-settings icon-brown"></span>Manage
                        </a>
                    </div>
                </div>
                <asp:Repeater runat="server" ID="rpGroups">
                    <ItemTemplate>
                        <div class="data-group-item datagroup" style="max-width:150px">
                            <div><a href='settings/groups.aspx?group=<%# Eval("ID") %>'><div title="<%# Eval("Label") %>" style="white-space:pre; text-overflow:ellipsis;overflow:hidden;"><%# Eval("Label") %></div></a></div>
                            <asp:Label runat="server" ID="lblGI" Text='<%# Eval("PoolSize") %>' />
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
            <div class="dashboard-cell">
                <div class="dashboard-cell-header">
                    <h2>Datasets</h2>
                    <div class="dashboard-cell-header-action">
                        <a href="data/dataset.aspx"  class="btn" title="Manage DataSets"> 
                            <span class="icon icon-settings icon-brown"></span>Manage
                        </a>
                    </div>
                </div>
                <asp:Repeater runat="server" ID="rpDataSets">
                    <ItemTemplate>
                        <div class="data-group-item dataset" style="max-width:150px">
                            <div><a href='data/dataset.aspx?ds=<%# Eval("ID") %>'><div title="<%# Eval("Name") %>" style="white-space: pre;text-overflow: ellipsis;overflow: hidden;"><%# Eval("Name") %></div></a></div>
                            <asp:Label runat="server" ID="lblGI" Text='<%# Eval("PoolSize") %>' />
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
            <div class="dashboard-cell" style="display: none;">
                <div class="dashboard-model-header">
                    <asp:DropDownList runat="server" ID="ddlModels" CssClass="select-level1" />
                    <div class="header-links">
                        <a>Help</a>
                    </div>
                </div>
                <table>
                    <tr>
                        <td>
                            <h3>Estimated Value / Actual Value</h3>
                            <div style="height: 300px; border: 1px solid gray; margin: 10px 0px;">
                            </div>
                        </td>
                        <td style="width: 400px;">
                            <h3>Cost Calculator</h3>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="dashboard-cell">
                <h2>Regression Models</h2>
                <asp:GridView runat="server" ID="models" Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="Model Name">
                            <ItemStyle Width="240px" />
                            <ItemTemplate>
                                <a class="model-link" tid='<%# Eval("ID") %>' editable='<%# Eval("EditStatus").GetHashCode()%>' href="#"><div title="<%# Eval("Name") %>" style="white-space:pre;text-overflow:ellipsis;overflow:hidden;width:240px"><%# Eval("Name") %></div></a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Status" HeaderText="Status" ItemStyle-Width="80px" />
                        <asp:BoundField DataField="OutputFieldName" HeaderText="Output" />
                        <asp:BoundField DataField="InputFields" HeaderText="Inputs/Factors" />
                        <asp:BoundField DataField="StandardError" HeaderText="Standard Error" DataFormatString="{0:0.0000}" ItemStyle-Width="150px" />
                        <asp:BoundField DataField="Observations" HeaderText="Observations" ItemStyle-Width="90px" />
                        <asp:TemplateField >
                            <ItemStyle Width="80px" />
                            <ItemTemplate>
                                <a runat="server" visible='<%# Eval("Status") <> "Empty" %>' class="clone-link" tid='<%# Eval("ID") %>' editable='<%# Eval("EditStatus").GetHashCode()%>' href="#" title="Make a clone copy of this model">Clone</a>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <div class="dashboard-item-footer">
                    <a href="mra.aspx" onclick="return verifyDatasets();">Create new Model</a>
                    <a href="mra-result.aspx">View All</a>
                </div>
            </div>

        </div>
    </div>

</asp:Content>

