﻿Public Class _default8
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            rpGroups.DataSource = Database.Tenant.GetDataTable("SELECT * FROM MRA_DataGroups WHERE IsActive = 1 ORDER BY Ordinal")
            rpGroups.DataBind()

            ddlModels.FillFromSql("SELECT ID, Name FROM MRA_Templates")

            rpDataSets.DataSource = Database.Tenant.GetDataTable("SELECT * FROM MRA_DataSet WHERE Error = 0 AND PoolSize IS NOT NULL ORDER BY Sequence")
            rpDataSets.DataBind()

            models.DataSource = Database.Tenant.GetDataTable("EXEC MRA_GetAllTemplates")
            models.DataBind()
        End If
    End Sub

End Class