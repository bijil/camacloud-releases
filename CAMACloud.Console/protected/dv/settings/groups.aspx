﻿<%@ Page Title="Manage Data Groups" Language="vb" ValidateRequest="false" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DesktopWeb.Master" CodeBehind="groups.aspx.vb" Inherits="CAMACloud.Console.groups" %>

<%@ Import Namespace="System.Web.Script.Services" %>
<%@ Import Namespace="System.Web.Services" %>
<%@ Import Namespace="CAMACloud" %>
<%@ Import Namespace="CAMACloud.Console" %>
<%@ Import Namespace="CAMACloud.Data" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .focussed {
            box-shadow: 0px 0px 2px blue;
        }

        .lookup-extra-options a {
            display: inline-block;
            margin-right: 15px;
            cursor: pointer;
            font-weight: 500;
            text-decoration: underline;
        }
    </style>
    <script>

        $(document).on('click',(e)=>{
            if($(e.target).closest('.filter-box').length === 0) {
                let nametd = $('input.group-field-name').parent();
                let lookuptd = $('input.group-lookup-table').parent();
                
                if(!($(e.target).closest(nametd).length == 1 ||
                    $(e.target).closest(lookuptd).length == 1)) {
                    resetSearchBoxes();
                }

            }
        })
        
        function enableLayout() {
            $('.form-layout').removeAttr('disabled');
            $('.shade').hide();
        }

        function disableLayout() {
            $('.form-layout').attr('disabled', 'disabled');
            $('.shade').show();
        }

        function showMessage(message) {
            say(message);
        }

        function createNewGroup() {


        }

        var lastSearch;
        var searchResults = [];
        var searchCount;
        function searchField(c) {

            function showSearchResults(d, q) {
                var currentQ = $('.field-filter .filter-input').val();
                if (q != currentQ) { return; }
                searchResults = d;
                searchCount = d.length;

                var tbody = '';
                if (d.length > 0) {

                    for (var f in d) {
                        with (d[f]) {
                            var tr = `<tr><td class="select-field" fid="${ID}">[${SourceTable}].${Name}</td><td>${DataTypeName}</td></tr>`;
                            tbody += tr;
                        }
                    }
                } else {
                    tbody = '<tr><td>No fields found!</td></tr>'
                }


                $('.field-results').html(tbody);
                $('.select-field', '.field-results').on('click', function () {
                    var c = this;
                    var fid = $(c).attr('fid');
                    var field = searchResults.find(function (x) { return x.ID == fid });

                    var fname = field.Name;
                    var table = field.SourceTable;

                    $('.group-field-name').val(table + '.' + fname).trigger('change');
                    $('#fieldid').val(fid);
                    $('.group-field-label').val(field.DisplayLabel).trigger('change');
                    $('.group-lookup-table').val(field.LookupTable).trigger('change');

                    resetSearchBoxes();
                });
            }

            var q = $(c).val();
            if (!q) {
                $('.field-results').html('');
                return;
            }
            if (q && lastSearch && q.length > 3 && q.toLowerCase().startsWith(lastSearch.toLowerCase()) && searchCount < 15) {
                searchResults = searchResults.filter(function (x) { return x.Name.toLowerCase().indexOf(q.toLowerCase()) > -1 });
                showSearchResults(searchResults, q);
            } else {
                PageMethods.FindField(q, function (sr) {
                    var d = sr.Fields;
                    showSearchResults(d, sr.Query);
                })
            }

            lastSearch = q;
        }

        function searchLookup(c) {

            function showSearchResults(d, q) {
                var currentQ = $('.lookup-filter .filter-input').val();
                if (q != currentQ) { return; }
                searchResults = d;
                searchCount = d.length;

                var tbody = '';
                if (d.length > 0) {

                    for (var f in d) {
                        with (d[f]) {
                            var tr = `<tr><td class="select-field">${Name}</td><td>${ValueCount} values</td></tr>`;
                            tbody += tr;
                        }
                    }
                } else {
                    tbody = '<tr><td>No lookups found!</td></tr>'
                }


                $('.lookup-results').html(tbody);
                $('.select-field', '.lookup-results').on('click', function () {
                    var c = this;
                    var lname = $(c).text();
                    selectLookup(lname);
                });
            }

            var q = $(c).val();
            if (!q) {
                $('.lookup-results').html('');
                return;
            }
            if (q && lastSearch && q.length > 3 && q.toLowerCase().startsWith(lastSearch.toLowerCase()) && searchCount < 15) {
                searchResults = searchResults.filter(function (x) { return x.Name.toLowerCase().indexOf(q.toLowerCase()) > -1 });
                showSearchResults(searchResults, q);
            } else {
                PageMethods.FindLookup(q, function (sr) {
                    var d = sr.Values;
                    showSearchResults(d, sr.Query);
                })
            }

            lastSearch = q;
        }

        function selectLookup(l) {
            $('.group-lookup-table').val(l).trigger('change');

            resetSearchBoxes();
        }

        function startUploadLookup() {
            disableLayout();

            function clearFile() {
                $('.lookup-upload').val('');
                $('.lookup-csv-options').hide();
                $('.lookup-upload-options').hide();
            };

            var fu = $('.lookup-upload')[0];
            if (fu.files.length > 0) {
                var file = fu.files[0];
                var data = new FormData();
                data.append("files[]", file, file.name);
                data.append("head_line", ($('#lookupheadline')[0].checked) ? "1" : "0")
                $.ajax({
                    type: 'POST',
                    data: data,
                    dataType: 'json',
                    processData: false,  // tell jQuery not to process the data
                    contentType: false,  // tell jQuery not to set contentType
                    success: function (d) {
                        clearFile();
                        enableLayout();
                        if (d.success && d.records > 0) {
                            if (d.records > 0) {
                                if (d.count == 1) {
                                    selectLookup(d.lookupName);
                                    //if (d.valid) {
                                    //    selectLookup(d.lookupName);
                                    //} else {
                                    //    showMessage("The uploaded lookup does not contains numeric values for computation.")
                                    //}

                                } else {
                                    showMessage('You have uploaded multiple lookups. Please search for the appropriate name to select.');
                                }
                            } else {
                                showMessage('You have uploaded an empty lookup.');
                            }

                        } else {
                            showMessage(d.errorMessage ? d.errorMessage : 'Import declined due to invalid Lookup Data.' );
                        }
                    }
                })
            }
        }

        function cancelUpload() {
            $('.lookup-upload').val('');
            $('.lookup-csv-options').hide();
            $('.lookup-upload-options').hide();
        }

        function lookupFileSelected(c) {
            $('.lookup-csv-options').hide();
            $('.lookup-upload-options').hide();

            if (c.files && c.files.length > 0) {
                var file = c.files[0];
                var nameparts = file.name.split('.');
                if (nameparts.length > 1) {
                    var ext = nameparts[nameparts.length - 1].toLowerCase();
                    if (ext == 'csv') $('.lookup-csv-options').show();

                    if (ext == "csv" || ext == 'xml') {
                        $('.lookup-upload-options').show();
                    } else {
                        say('You have selected an invalid file. Only CSV/XML files are allowed.');
                    }
                }
            }
        }

        function saveDataGroup() {
            $('.form-buttons button').attr('disabled','disabled');
            resetSearchBoxes();
            var gid = $('#groupid').val() || -1;
            var fieldId = $('#fieldid').val();
            var label = $('.group-field-label').val();
            var lookup = $('.group-lookup-table').val();
            var exp = $('#groupexport')[0].checked;

            var valid = true;
            $('[mandatory]', '.data-group-form').each(function (i, x) {
                if (!$(x).val()) {
                    $(x).attr('manderr', '1');
                    valid = false;
                    $('.form-buttons button').removeAttr('disabled');
                }
            })

            if (valid){
                if(!exp) {
                    ask('Are you sure you do not want to include this field in all data exports?',()=> {
                        saveGrp(gid, fieldId, label, lookup, exp);
                    }, ()=> {
                        $('.form-buttons button').removeAttr('disabled');
                        $('#groupexport')[0].checked = true;
                        return false;
                    }); 
                }   
                else  
                    saveGrp(gid, fieldId, label, lookup, exp);
            }
            else
                $('.form-buttons button').removeAttr('disabled');
        }      	

        function saveGrp(gid, fieldId, label, lookup, exp){
            loading.show('Saving the group, please wait.');
            PageMethods.SaveGroup(gid, fieldId, label, lookup, exp, function (d) {
                if (d.Success) {
                    $('#fieldid').val('');
                    $('[field]', '.data-group-form').val('');
                    $('[field]', '.data-group-form').removeAttr('manderr');
                    $('#groupexport')[0].checked = false;
                    window.eval($('.lb-return-list').attr('href').replace('javascript:', ''));
                } else {
                    say(d.ErrorMessage);
                    $('.form-buttons button').removeAttr('disabled');
                }
	        });
        }

        function resetSearchBoxes(a) {
            if(a == 1 || typeof a =='undefined') {
                $('.filter-box').hide();
                $('.filter-box input').val('');
                $('.filter-results').html('');
            }
            if(a == 2 || typeof a =='undefined') {
                $('.lookup-csv-options').hide();
                $('.lookup-upload-options').hide();
                $('.lookup-upload').val('');
            }

            lastSearch = null;
            searchResults = [];
            searchCount = 0;

            enableLayout();
        }
        function openFieldSearchBox() {
            resetSearchBoxes();
            $('.field-filter').show();
            $('.filter-input', '.field-filter').focus();

            $('[field]').removeClass('focussed');
            $('[field].group-field-name').addClass('focussed');
        }

        function openLookupSearchBox() {
            var fieldid = $('#fieldid').val();
            if (!fieldid) {
                say('You have not selected a field yet.');
                openFieldSearchBox();
                return;
            }
            if ($('.group-lookup-table').val())
                $('.btn-download-lookup').show();
            else
                $('.btn-download-lookup').hide();

            resetSearchBoxes();

            $('.lookup-filter').show();
            $('[field]').removeClass('focussed');
            $('[field].group-lookup-table').addClass('focussed');

            $('.filter-input', '.lookup-filter').focus();
        }

        function generateLookupData() {
            disableLayout();
            var f = $('#fieldid').val();
            if (f) {
                PageMethods.GenerateLookup(f, function (d) {
                    if (d.Success) {
                        selectLookup(d.Data);
                    } else {
                        say(d.ErrorMessage);
                        enableLayout();
                    }
                })
            }
        }

        function downloadLookup() {
            var lookupName = $('.group-lookup-table').val();
            if (lookupName) {
                var url = window.location.pathname + '?action=downloadlookup&lookup=' + lookupName;
                window.open(url);
            }

        }
        
        function deleteDataGroup(groupID){
	        var id = groupID;
	        ask('Deleting the Data group may change the count of Datasets associated with this group. Do you wish to continue?',()=>{
	        PageMethods.DeleteGroup(id,(d)=>{
	        	if(d.Success)
        			location.reload();
	        	});
	        });
		}
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div class="content-area-wrapper content-area-wrapper-padded">
        <asp:Panel runat="server" ID="ListView" Visible="true">
            <div class="fr content-top-actions">
                <asp:LinkButton runat="server" ID="lbCreateGroup" Text="Create New Group" />
            </div>
            <asp:GridView runat="server" ID="grid" Width="100%">
                <Columns>
                    <asp:BoundField DataField="TableFieldName" HeaderText="Parcel Field Name" ItemStyle-Width="300px" />
                    <asp:BoundField DataField="Label" HeaderText="Group Label" />
                    <asp:BoundField DataField="LookupName" HeaderText="Lookup Source" ItemStyle-Width="300px" />
                    <asp:BoundField DataField="PoolSize" HeaderText="Data Count" ItemStyle-Width="100px" />
                    <asp:TemplateField>
                        <ItemStyle Width="90px" />
                        <ItemTemplate>
                            <asp:LinkButton runat="server" CommandName="EditItem" CommandArgument='<%# Eval("ID") %>' Text="Edit" />
                            <asp:LinkButton runat="server" CommandName="DeleteItem" CommandArgument='<%# Eval("ID") %>' OnClientClick='<%# DeleteGroupScript() %>' Text="Delete" />&nbsp;&nbsp;
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:Panel>
        <asp:Panel runat="server" ID="FormView" Visible="false">
            <div class="form-layout">
                <div class="fr content-top-actions">
                    <asp:LinkButton runat="server" ID="lbViewList" Text="Return to List" CssClass="lb-return-list" />
                </div>
                <div class="fl">
                    <h1>Data Group -
                    <asp:Label runat="server" ID="lblFieldSpec" /></h1>
                    <asp:HiddenField runat="server" ID="groupid" ClientIDMode="Static" />
                    <table class="form form-2col data-group-form">
                        <tr>
                            <td>Select Field: </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtFieldName" CssClass="group-field-name" field="1" mandatory="1" onclick="openFieldSearchBox();" ReadOnly="true" />
                                <asp:HiddenField runat="server" ID="fieldid" ClientIDMode="Static" />
                                <a class="btn-32 btn-search-32" onclick="openFieldSearchBox();"></a>
                            </td>
                        </tr>
                        <tr>
                            <td>Label/Description: </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtLabel" MaxLength="40" CssClass="group-field-label" field="1" mandatory="1" />
                            </td>
                        </tr>
                        <tr>
                            <td>Lookup Source:</td>
                            <td>
                                <asp:TextBox runat="server" ID="txtLookupTable" CssClass="group-lookup-table" field="1" mandatory="1" onclick="openLookupSearchBox();" ReadOnly="true" />
                                <a class="btn-32 btn-search-32" onclick="openLookupSearchBox();"></a>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:CheckBox runat="server" ID="groupexport" ClientIDMode="Static" Text="Include Group in Data Exports" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="form-buttons">
                                <button action="saveDataGroup();">Save Group</button>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="fl field-filter filter-box" style="margin-top: 24px; display: none">
                    <div class="label">Type field name to search:</div>
                    <input class="filter-input" style="width: 350px" onkeyup="searchField(this);" />
                    <table style="width: 100%;" class="field-results filter-results">
                    </table>
                </div>
                <div class="fl lookup-filter filter-box" style="margin-top: 24px; display: none;">
                    <h3 class="nomar" style="margin-bottom: 8px;">Upload XML/CSV Lookup File:</h3>
                    <div>
                        <input type="file" class="lookup-upload" accept=".xml,.csv" onchange="lookupFileSelected(this);" />
                    </div>
                    <div class="lookup-csv-options">
                        <input type="checkbox" id="lookupheadline" />
                        <label for="lookupheadline">First line contains heading</label>
                    </div>
                    <div style="margin-top: 8px;" class="lookup-upload-options filter-button-panel">
                        <button action="startUploadLookup()">Upload Lookup</button>
                        <button action="cancelUpload()">Cancel</button>
                    </div>
                    <h3>Search Lookup Tables:</h3>
                    <div class="label">Type lookup name to search:</div>
                    <input class="filter-input" style="width: 350px" onkeyup="searchLookup(this);" />
                    <div style="min-height: 150px;">
                        <table style="width: 100%;" class="lookup-results filter-results">
                        </table>
                    </div>
                    <div class="lookup-extra-options">
                        <a onclick="generateLookupData();">Generate lookup from data</a>
                        <a onclick="downloadLookup();" class="btn-download-lookup">Download Lookup</a>
                    </div>
                </div>
            </div>

        </asp:Panel>
    </div>
    <iframe id="dl_agent" style="display: none"></iframe>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FootContents" runat="server">
</asp:Content>
<script runat="server">


    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)> Public Shared Function GenerateLookup(fieldId As Integer) As ActionResult(Of String)
        Dim l As New ActionResult(Of String)
        Try
            Dim lookup = Database.Tenant.GetTopRow("EXEC MRA_GenerateLookup " & fieldId)
            l.Success = lookup.GetBoolean("Success")
            If l.Success Then
                l.Data = lookup.GetString("LookupName")
            Else
                l.ErrorMessage = lookup.GetString("ErrorMessage")
            End If
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        Return l
    End Function

</script>
