﻿<%@ Page Title="Regression - Bulk Calculator" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DesktopWeb.Master" CodeBehind="mra-calc.aspx.vb" Inherits="CAMACloud.Console.mra_calc" %>

<%@ Import Namespace="System.Web.Script.Services" %>
<%@ Import Namespace="System.Web.Services" %>
<%@ Import Namespace="CAMACloud" %>
<%@ Import Namespace="CAMACloud.Console" %>
<%@ Import Namespace="CAMACloud.Data" %>
<%@ Import Namespace="CAMACloud.BusinessLogic.DataAnalyzer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="https://cdn.jsdelivr.net/npm/openlayers@latest/dist/ol.js"></script>
    <script src="/App_Static/js-v2/OLMap.js?t=2"></script>
    <script src="/App_Static/jslib/Chart.min.js"></script>
    <script src="/App_Static/js-v2/ChartManager.js"></script>
    <script src="/App_Static/js-v2/mra/mra-common.js?t=2"></script>
    <style>
        .template-name {
        }

        .template-name > span {
            font-size: 0.6em;
        }

        .ds-selector {
            font-size:1.2em;
            padding:2px;
            border:1px solid #0094ff;
            min-width:300px;
        }
        .chart1-container {
            height:300px;
        }
        .calc-list-div > .mGrid {
            margin-top:0;
            margin-bottom:1px;
        }
        .calc-dialog {
            min-height:204px;
        }
        .calc-row {
            display: flex;
            align-items: center;
            justify-content: space-between;
            margin:8px 0;

        }
            .calc-row input,
            .calc-row  select {
                width:325px;
            }

        
                .calc-info-parent {
                    border: 1px solid #ccc;
                    background-color: #F5F5F5;
                    padding:8px;
                    flex-wrap:wrap;
                }
                .calc-info-parent .calc-info-header {
                    flex: 0 40%;
                    overflow: hidden;
                }
                .calc-info-parent .calc-info-header.tcount {
                    flex: 0 15%; 
                    text-align:center;
                    padding-left:8px;
                }
                .calc-info-header span{
                    font-size : 0.8em;
                }
                .calc-info-header p{
                    font-size: 1.2em;
                    font-weight: 500;
                    display:block;
                    width:100%;
                }
        .output-map {
            display:none;
        }
        .calculation-action {
            display:flex;
            align-items:center;
        }
        .switch-button {
            background: rgba(255, 255, 255, 0.56);
            border-radius: 4px;
            overflow: hidden;
            width: 100px;
            text-align: center;
            font-size: 14px;
            letter-spacing: 1px;
            color: #155FFF;
            position: relative;
            padding-right: 100px;
            position: relative;
            margin-right:6px;
        }
        .switch-button:before {
            content: "MapView";
            position: absolute;
            top: 0;
            bottom: 0;
            right: 0;
            width: 100px;
            display: flex;
            align-items: center;
            justify-content: center;
            z-index: 3;
            pointer-events: none;
        }
        .switch-button-checkbox {
            cursor: pointer;
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            width: 100%;
            height: 100%;
            opacity: 0;
            z-index: 2;
        }
        .switch-button-checkbox:checked + .switch-button-label:before {
            transform: translateX(100px);
            transition: transform 300ms linear;
        }
        .switch-button-checkbox + .switch-button-label {
            position: relative;
            padding: 5px 0;
            display: block;
            user-select: none;
            pointer-events: none;
        }
        .switch-button-checkbox + .switch-button-label:before {
            content: "";
            background: #fff;
            height: 100%;
            width: 100%;
            position: absolute;
            left: 0;
            top: 0;
            border-radius: 4px;
            transform: translateX(0);
            transition: transform 300ms;
        }
        .switch-button-checkbox + .switch-button-label .switch-button-label-span {
            position: relative;
        }
        .switch-button-checkbox:checked + .switch-button:before {
            color:#f00;
        }
        .map-popup {
            transform:none;
            min-width:150px;
        }
        .map-popup>div {
        	margin:4px;
        	display:flex;
        	justify-content:space-between;
        }
        .map-popup > div >p {
            margin: 0;
            font-weight:500;
            font-size:1.1em;
        }
        .open-in-qc{
            color: rgb(22,124,207) !important;
            text-decoration: underline;
            cursor : pointer;
        }
    </style>
    <script>
        $.getScript('/App_Static/js-v2/mra/mra-calc.js?zt=' + (new Date).getTime());
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div class="wui-layout d1001">
        <div class="ca3-bg unselectable" style="width: 300px; padding: 5px;">
            <div class="data-col-head">
                <span>Calculation Models</span>
                <span class="new-btn" onclick="openCalcDialog();">New</span>
            </div>
            <div class="calc-list-div common-list vscroll thin-scrollbar">
                <table class="mGrid app-content unselectable" cellspacing="0" style="width: 100%; border-collapse: collapse;">
                    <tbody class="calculations-list"></tbody>
                </table>
            </div>
        </div>
        <div class="wui-stretch">
            <div class="ca2-bg unselectable flex align-center justify-between" style="padding: 5px 10px;height:50px;">
                <h1 class="nomar calculation-name truncate" style="width:500px"></h1>
                <div class="calculation-action" style="display:none;">
                    <div class="switch-button">
                        <input class="switch-button-checkbox" type="checkbox" />
                        <label class="switch-button-label" for=""><span class="switch-button-label-span">GridView</span></label>
                    </div>
                    <p class="btn nomar" onclick="recalcButton();">Recalculate</p>
                </div>
            </div>
            <div class="ca4-bg p10" style="height:60px;">
               <div class = "calc-info-parent flex justify-between">
                    <div class= "calc-info-header">
                        <span >Regression Model</span><p title="" class="calc-info-model nomar truncate"></p>
                    </div>
                    <div class= "calc-info-header">
                        <span>DataSet</span><p title="" class="calc-info-dataset nomar truncate"></p>
                    </div>
                    <div class= "calc-info-header tcount">
                        <span>Count</span><p title="" class="calc-info-count nomar truncate"></p>
                    </div>
                </div>    
            </div>
            
            <div class="result-scroller output-data vscroll">
                <div class="wui-sub" style="">
                    <div class="wui-stretch chart1-container">
                        <canvas height="300" id="chart1"></canvas>
                        <div class="chart-info" style="display: none;">
                            <a class="close" nosel="1">X</a>
                            <h3 class="nomar" style="margin-bottom: 6px;">Parcel: <span class="iv-parcel b2"></span></h3>
                            <table style="border-spacing: 0px;">
                                <tr>
                                    <td style="width: 80px;">Actual: </td>
                                    <td class="iv-actual iv-field r"></td>
                                </tr>
                                <tr>
                                    <td>Predicted: </td>
                                    <td class="iv-predicted iv-field r"></td>
                                </tr>
                            </table>
                            <div class="r" nosel="1">
                                <a class="b2 iv-qc-link">Open in QC</a>
                            </div>

                        </div>
                    </div>
                    <div style="width: 400px; min-height: 300px;">
                        <canvas height="300" width="400" id="chart2"></canvas>
                    </div>
                </div>
                <div >
                    <div class="ca4-bg ca unselectable" style="padding: 5px 10px;">
                        <h3 class="nomar fl">Residual Output</h3>
                        <span class="rt-count fl" style="margin-left: 10px; display: inline-block; margin-top: 5px; font-weight: 500;">Displaying 0 parcels.</span>
                        <div class="fr report-links">
                            <a style="display:none;" class="link-default">Open in QC</a>
                            <a class="data-download link-default" onclick="downloadReport();">Download Report</a>
                        </div>
                    </div>
                    <div class="rt-container p10">
                        <div class="data-page-links unselectable"></div>
                        <table class="mGrid app-content output-table" style="width: 100%; max-width: 1200px;">
                            <thead>
                                <tr class="thr">
                                    <th scope="col">Parcel</th>
                                    <th scope="col" style="width: 140px;">Actual Value</th>
                                    <th scope="col" style="width: 140px;">Predicted Value</th>
                                    <th scope="col" style="width: 140px;">Residual</th>
                                    <th scope="col" style="width: 90px;text-decoration: underline;cursor: pointer;" onclick = "sortDatabyRatio()">Ratio</th>
                                    <th scope="col" class= "compared" style="width: 110px;">Comparison value</th>
                                    <th scope="col" class= "compared" style="width: 180px;">Comparison/Predicted Ratio</th>
                                </tr>
                            </thead>
                            <tbody class="residual-list"></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="result-scroller output-map">
                <div id="mapbox"></div>
            </div>
        </div>
    </div>
    <div class="shade dock-full" style="display: none;"></div>
    <div class="popup-dg map-popup" style="display: none;">
        <div ar><span>Avg Ratio : </span><p></p></div>
        <div max><span>Max Ratio : </span><p></p></div>
        <div min><span>Min Ratio : </span><p></p></div>
        <div cvr><span>Avg Compared Ratio : </span><p></p></div>
        <div count><span>No of Parcels : </span><p></p></div>
        <div pId><span>Parcel : </span><p></p></div>
    </div>
    <div class="filter-box popup-dg calc-dialog" style="width:600px;display:none">
        <div class="filter-head">
            <a class="filter-box-close" onclick="hideCalcDialog()">✖</a>
            <h1 class="nomar filter-title">Create Calculation</h1>
            <div style="margin: 10px 0 16px;">
                <div class="calc-row flex align-center justify-between">
                    <label>Name</label>
                    <input class = "c-name" type="text" maxlength="50"/>
                </div>
                <div class="calc-row flex align-center justify-between">
                    <label>Regression Model</label>
                    <select class="rm-selector">
                        <option>Select Model</option>
                    </select>
                </div>
                <div class="calc-row flex align-center justify-between">
                    <label>DataSet</label>
                    <select class="ds-selector">
                        <option>Select dataset</option>
                    </select>
                </div>
                <div class="calc-row flex align-center justify-between">
                    <label>Comparison Value</label>
                    <select class="cv-selector">
                        <option>Select Comparison value</option>
                    </select>
                </div>
                <div class="calc-row c-expression flex align-center justify-between" style="display:none;">
                    <label>Custom Expression</label>
                        <input class = "c-exp" type="text"/>
                </div>
         	</div>
            <p class="alert alert-info">
                Only valid regression models which already have output results are listed.
            </p>
         	<div class="r filter-button-panel">
            <button class="filter-button calc-save btn--primary" create="1" onclick="saveCalcInfo();return false;">
                Save
            </button>
        </div>
        </div>
    </div>
</asp:Content>
<script runat="server">
    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetLoadInfo() As ActionResult(Of MRACalcPageInfo)
        Dim l As New ActionResult(Of MRACalcPageInfo)
        Try
            Dim li As New MRACalcPageInfo
            li.GIS = Database.Tenant.GetDataTable("EXEC GIS_GetConfiguration").ConvertToList(Of GISConfig).FirstOrDefault
            li.Datasets = Database.Tenant.GetDataTable("SELECT * FROM MRA_DataSet WHERE ISNULL(PoolSize,0) > 0 AND Error = 0 ORDER BY Name").ConvertToList(Of Dataset)
            li.Calculations = Database.Tenant.GetDataTable("SELECT c.* FROM MRA_Calculations c ORDER BY Id DESC").ConvertToList(Of MRACalculation)
            li.Templates = Database.Tenant.GetDataTable("SELECT t.Id, t.Name,t.DataSetID,ISNULL(t.OutputFieldLabel, f.DisplayLabel) As DisplayLabel, t.NoIntercept, f.Name OutputFieldName, f.SourceTable As OutputFieldTable, 1 As Compiled, f.DataType as DataType FROM MRA_Templates t  INNER JOIN MRA_Result r ON t.ID = r.TemplateID  LEFT OUTER JOIN DataSourceField f ON t.OutputField = f.ID  WHERE t.ID IS NOT NULL AND t.CalculationQuery IS NOT NULL ORDER BY Compiled DESC, Name").ConvertToList(Of MRATemplate)
            li.CompareValues = Database.Tenant.GetDataTable("SELECT ds.ID As ID, CONCAT(dt.Name,'.',ds.Name) As Name from DataSourceField ds INNER JOIN DataSourceTable dt ON ds.TableId = dt.ID Where dt.ImportType IN (0, 1) AND ds.TableId IS NOT NULL AND ds.Datatype IN (2,7,8,9)").ConvertToList
            l.Data = li
            l.Success = l.Data IsNot Nothing
            l.Count = li.Templates.Count
            l.FullCount = l.Count
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        Return l
    End Function
    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)> Public Shared Function CreateNewCalculation(calcName As String, rmId As Integer, dsId As Integer, fId As Integer, custExp As String) As ActionResult(Of Integer)
        Dim l As New ActionResult(Of Integer)
        Dim Expressionstatus As String = "Invalid"
        Try
            If Not String.IsNullOrWhiteSpace(custExp) Then
                Dim tableName As String = Database.Tenant.GetStringValue("SELECT t.CC_TargetTable FROM DataSourceField f INNER JOIN DataSourceTable t ON f.TableId = t.Id WHERE f.ID = " & fId)
                Dim testSql As String = "SELECT TOP 1 MAX(" + custExp + ") FROM " + tableName
                Database.Tenant.GetDataTable(testSql)
            End If
            Expressionstatus = "valid"

            Dim newId As Integer
            Dim status As Integer = Database.Tenant.GetIntegerValue("SELECT COUNT(Name) FROM MRA_Calculations WHERE Name = {0} ".SqlFormat(True, calcName))
            If status <> 0 Then
                Throw New Exception("A Calculation with the same name already exists. Please type in a different name")
            End If
            If fId = 0 
                newId = Database.Tenant.GetIntegerValue("INSERT INTO MRA_Calculations (Name, TemplateId, DataSetId,ComparisonField,CustomExpression) VALUES ({0}, {1}, {2},{3}, {4});SELECT CAST(@@IDENTITY AS INT) As NewID;".SqlFormat(True, calcName, rmId, dsId, DBNull.Value,DBNull.Value))
            Else
                newId = Database.Tenant.GetIntegerValue("INSERT INTO MRA_Calculations (Name, TemplateId, DataSetId,ComparisonField,CustomExpression) VALUES ({0}, {1}, {2},{3}, {4});SELECT CAST(@@IDENTITY AS INT) As NewID;".SqlFormat(True, calcName, rmId, dsId, fId,custExp))
            End If
            l.Data = newId
            l.Success = True
        Catch ex As Exception
            If Expressionstatus <> "valid" Then
                l.ErrorMessage = "Invalid Custom Expression"
            Else
                l.ErrorMessage = ex.Message
            End If
        End Try
        Return l
    End Function
    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)> Public Shared Function UpdateCalculation(id As Integer, calcName As String, rmId As Integer, dsId As Integer, fId As Integer, custExp As String) As ActionResult(Of MRACalcPageInfo)
        Dim l As New ActionResult(Of MRACalcPageInfo)
        Dim Expressionstatus As String = "Invalid"
        Try
            If Not String.IsNullOrWhiteSpace(custExp) Then
                Dim tableName As String = Database.Tenant.GetStringValue("SELECT t.CC_TargetTable FROM DataSourceField f INNER JOIN DataSourceTable t ON f.TableId = t.Id WHERE f.ID = " & fId)
                Dim testSql As String = "SELECT TOP 1 MAX(" + custExp + ") FROM " + tableName
                Database.Tenant.GetDataTable(testSql)
            End If
            Expressionstatus = "Valid"

            Dim status As Integer = Database.Tenant.GetIntegerValue("SELECT COUNT(Name) FROM MRA_Calculations WHERE Name = {0} AND ID <> {1}".SqlFormat(True, calcName, id))
            If status <> 0 Then
                Throw New Exception("A Calculation with the same name already exists. Please type in a different name")
            End If
            Dim li As New MRACalcPageInfo
            If fId = 0 
                Database.Tenant.Execute("UPDATE MRA_Calculations SET Name = {0}, TemplateId	={1}, DataSetId={2},ComparisonField = {4}, CustomExpression = {5} WHERE ID = {3}".SqlFormat(True, calcName, rmId, dsId, id,DBNull.Value,DBNull.Value))
            Else
                Database.Tenant.Execute("UPDATE MRA_Calculations SET Name = {0}, TemplateId	={1}, DataSetId={2}, ComparisonField = {4}, CustomExpression = {5} WHERE ID = {3}".SqlFormat(True, calcName, rmId, dsId, id, fId, custExp))
            End If
            li.Calculations = Database.Tenant.GetDataTable("SELECT * FROM MRA_Calculations WHERE ID = {0}".SqlFormat(True, id)).ConvertToList(Of MRACalculation)
            l.Data = li
            l.Success = True
        Catch ex As Exception
            If Expressionstatus <> "Valid" Then
                l.ErrorMessage = "Invalid Custom Expression"
            Else
                l.ErrorMessage = ex.Message
            End If
            
        End Try
        Return l
    End Function
    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)> Public Shared Function DeleteCalculation(id As Integer) As ActionResult(Of Boolean)
        Dim l As New ActionResult(Of Boolean)
        Try
            Database.Tenant.Execute("DELETE FROM MRA_CalculationResult WHERE CalculationID = {0}; DELETE FROM MRA_Calculations WHERE ID = {0}".SqlFormat(True,id))
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        Return l
    End Function
    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)> Public Shared Function GetCalculationList() As ActionResult(Of MRACalcPageInfo)
        Dim l As New ActionResult(Of MRACalcPageInfo)
        Try
            Dim li As New MRACalcPageInfo
            li.Calculations = Database.Tenant.GetDataTable("SELECT c.* FROM MRA_Calculations c ORDER BY Id DESC").ConvertToList(Of MRACalculation)
            l.Data = li
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        Return l
    End Function
    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)> Public Shared Function RunCalculation(id As Integer) As ActionResult(Of MRACalcPageInfo)
        Dim l As New ActionResult(Of MRACalcPageInfo)
        Try
            Dim li As New MRACalcPageInfo
            li.CalculationResult = Database.Tenant.GetDataTable("EXEC MRA_RunCalculation {0}".SqlFormat(True,id)).ConvertToList(Of MRACalculationResult)
            l.Data = li
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        Return l
    End Function
    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)> Public Shared Function GetCalculation(id As Integer) As ActionResult(Of MRACalcPageInfo)
        Dim l As New ActionResult(Of MRACalcPageInfo)
        Try
            Dim li As New MRACalcPageInfo
            li.CalculationResult = Database.Tenant.GetDataTable("EXEC MRA_GetCalculation {0}".SqlFormat(True, id)).ConvertToList(Of MRACalculationResult)
            li.RatioDistribution = Database.Tenant.GetDataTable("EXEC MRA_GetCalcGraphData 'ratio-distribution', @CalculationID = {0}".SqlFormat(True,id)).ConvertToList
            l.FullCount = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM MRA_CalculationResult WHERE CalculationID = {0}".SqlFormat(True,id))
            l.Data = li
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        Return l
    End Function
    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetGridData(CalculateID As Integer, pageIndex As Integer, pageSize As Integer,RangeSort As Integer) As ActionResultData
        Dim l As New ActionResultData
        Try
            Dim ds = Database.Tenant.GetDataSet("EXEC MRA_GetCalcForExport {0}, 'residuals-view', {1}, {2}, {3}".SqlFormat(False, CalculateID, pageIndex, pageSize,RangeSort))

            l.Data = ds.Tables(0).ConvertToList
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        'l.Query = q
        Return l
    End Function
    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetGISConfig() As ActionResult(Of GISConfig)
        Dim l As New ActionResult(Of GISConfig)
        Try
            l.Data = Database.Tenant.GetDataTable("EXEC GIS_GetConfiguration").ConvertToList(Of GISConfig).FirstOrDefault
            l.Success = l.Data IsNot Nothing
            l.Count = 1
            l.FullCount = l.Count
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        'l.Query = q
        Return l
    End Function
    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetMapData(datasetId As Integer, range() As Single) As ActionResult(Of List(Of MapData))
        Dim l As New ActionResult(Of List(Of MapData))
        Try
            
            l.Data= Database.Tenant.GetDataTable("EXEC MRA_GetCalcMapData {0}, {1}, {2}, {3}, {4}".SqlFormat(True, datasetId, range(1), range(3), range(0), range(2))).ConvertToList(Of MapData)
            
            l.Success = True
            l.Count = l.Data.Count
            l.FullCount = l.Count
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        Return l
    End Function

    Public Sub Page_PageCommand(commandName As String) Handles Me.RunCommand
        Dim calcId As Integer = 0
        If Integer.TryParse(Request("calculationId"), calcId) Then
            Dim reportTitle As String = "Data Export", fileName As String = "report"
            Dim Calculation As System.Data.DataRow = Database.Tenant.GetTopRow("SELECT * FROM MRA_Calculations WHERE ID = " & calcId)
            Dim CalculationName As String = Calculation.GetString("Name")
            Dim timeSignature = Now.ToString("yyyyMMdd") + "_" + Now.ToString("HHmm")
            Dim exportGrid As New GridView
            Page.Controls.Add(exportGrid)

            If commandName = "exportdata" Then
                reportTitle = CalculationName + " - Residuals Report - "
                fileName = {CalculationName, "residuals", timeSignature}.Aggregate(Function(x, y) x + "_" + y)
                Dim data = RegressionAnalyzer.GetCalcReport(calcId,"residuals-export", exportGrid)
                exportGrid.ExportAsExcel(data, reportTitle, fileName, "residuals")
            End If

            Page.Controls.Remove(exportGrid)
        End If
    End Sub
    Public Class MRACalcPageInfo
        Public Property GIS As GISConfig
        Public Property Datasets As List(Of Dataset)
        Public Property Templates As List(Of MRATemplate)
        Public Property CompareValues As JSONTable
        Public Property Calculations As List(Of MRACalculation)
        Public Property CalculationResult As List(Of MRACalculationResult)
        Public Property RatioDistribution As JSONTable
    End Class
    Public Class MRACalculation
        Public Property ID As Integer
        Public Property Name As String
        Public Property DataSetID As Integer
        Public Property TemplateID As Integer
        Public Property ResultCount As Integer
        Public Property FilterExpression As String
        Public Property ComparisonField As Integer
        Public Property CustomExpression As String
    End Class
    Public Class MRACalculationResult
        Public Property cid As Integer
        Public Property pid As Integer
        Public Property pk As String
        Public Property a As Double
        Public Property p As Double
        Public Property c As Double
    End Class
    Public Class MapData
        Public Property Range As Integer
        Public Property Intensity As Integer
        Public Property Volume As Integer
        Public Property ParcelId As Integer
        Public Property Latitude As Double
        Public Property Longitude As Double        
        Public Property AvgRatio As Double        
        Public Property MaxRatio As Double        
        Public Property MinRatio As Double
        Public Property AvgComparedRatio As Double        
    End Class
    
</script>
