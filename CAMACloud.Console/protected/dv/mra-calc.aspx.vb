﻿Public Class mra_calc
    Inherits System.Web.UI.Page

    Public Event RunCommand(pageCommand As String)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    If IsPostBack Then

        End If

        If Page.Request.CurrentExecutionFilePath <> Page.Request.Url.AbsolutePath Then
            Dim commandName = Page.Request.Url.AbsolutePath.Replace(Page.Request.CurrentExecutionFilePath, "").TrimStart("/")
            RaiseEvent RunCommand(commandName)
        End If

    End Sub

End Class