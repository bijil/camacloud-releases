﻿<%@ Page Title="Regression Model" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DesktopWeb.Master" CodeBehind="mra.aspx.vb" Inherits="CAMACloud.Console.mra" %>

<%@ Import Namespace="System.Web.Script.Services" %>
<%@ Import Namespace="System.Web.Services" %>
<%@ Import Namespace="CAMACloud" %>
<%@ Import Namespace="CAMACloud.Console" %>
<%@ Import Namespace="CAMACloud.Data" %>
<%@ Import Namespace="CAMACloud.BusinessLogic.DataAnalyzer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .main-content-area, .main-content-area * {
            box-sizing: border-box;
        }

        .ds-selector {
            font-size: 1.1em;
            padding: 2px 10px;
            border-radius: 6px;
            width: 100%;
        }

        .data-filter-list .delete-filter-item {
            float: right;
            cursor: pointer;
        }

        .data-filter-list .data-filter-item {
            font-size: 1em;
            cursor: pointer;
            font-weight: 500;
        }

        .data-filter-list [ft] span {
            font-size: 0.85em;
            float: right;
        }

        .data-filter-list .delete-filter-item:before {
            content: '✖';
            color: red;
        }

        .head-bar {
            padding: 0px;
        }

            .head-bar > div {
                padding: 6px;
            }

                .head-bar > div > input[type=text] {
                    font-size: 1.1em;
                    padding: 2px 10px;
                    border-radius: 6px;
                    width: 100%;
                    height: 29px;
                }

        .nonparcel-field-props {
            margin-top: 4px;
        }

            .nonparcel-field-props .field-aggregate select {
                margin-left: 20px;
            }

            .nonparcel-field-props .field-table-filter {
                margin-top: 4px;
            }

                .nonparcel-field-props .field-table-filter textarea {
                    width: 100%;
                }

        .input-field {
            display: inline-block;
            /*float: left;*/
            font-size: 1.1em;
            border: 1px solid #CCC;
            padding: 4px 12px;
            border-radius: 20px;
            margin: 4px;
            font-weight: 500;
            cursor: pointer;
        }
        
        #spanMainHeading {
        	text-overflow: ellipsis;
        	overflow: hidden;
        	white-space: pre;
        }

        .input-field-add {
            padding: 0;
        }
        .input-field-add > a {
            padding: 4px 25px;  
            border-radius: 20px;  
            border: 1px solid #CCC;        
        }

        .input-fields-list {
            display: inline;
        }

        .input-field > a {
        }

        .input-field > span {
            display: inline-block;
            margin-left: 10px;
        }

            .input-field > span[d] {
                margin-left: 5px;
            }

                .input-field > span[d]:before {
                    content: '✖';
                    color: red;
                }

            .input-field > span[e]:before {
                content: '✎';
                color: blue;
            }

        .input-field[selected] {
            background-color: #663399;
        }

            .input-field[selected] > a {
                color: white;
            }

            .input-field[selected] > span[e]:before {
                color: white;
            }

        .field-props tr td:first-child {
            min-width: 130px;
        }

        .prop-filter td:last-child select {
            width: 120px;
            padding: 4px;
        }

        .prop-filter td:last-child > div {
            display: inline-block;
        }

        .prop-filter td:last-child input {
        }

        .prop-lookup td:last-child select {
            width: 300px;
            padding: 4px;
        }

        .field-filter-values input {
            min-width: 100px;
            width: 175px;
        }

        .field-filter-values span {
            display: none;
        }

        .field-filter-values input:last-child {
            display: none;
        }

        .filter-input-double {
            margin-top: 5px;
        }

            .filter-input-double input {
                display: inline-block;
                width: 120px !important;
            }

            .filter-input-double span {
                display: inline-block;
                margin: 0px 11px;
            }

            .filter-input-double input:last-child {
                display: inline-block;
            }

        .input-fields-area {
            border-bottom: 1px solid #ccc;
            box-shadow: 0px 0px 5px #666;
        }

            .input-fields-area::after {
                content: '';
                display: block;
                clear: both;
            }

        .filter-add-link, .filter-add-link:hover {
            float: right;
            color: white;
            font-size: 0.9em;
            font-weight: 400;
            text-decoration: underline;
            cursor: pointer;
        }

        .data-view {
            /*overflow-y: scroll;*/
        }

        .pool-size {
            font-weight: bold;
            color: yellow;
        }

        .template-buttons {
            text-align: right;
        }

            .template-buttons button {
                height: 28px;
                padding: 0px 20px;
                border-width: 1px;
                border-style: solid;
                font-weight: 500;
                margin: 0px 0px;
                cursor: pointer;
            }

        .data-filters {
            overflow-y: auto;
        }

        .mra-links a {
            color: yellow;
            margin-bottom: 3px;
            display: inline-block;
            cursor: pointer;
            margin: 3px 10px;
            margin-top: 0px;
        }

            .mra-links a:hover {
                color: lightyellow;
            }

        input[type=date]::-webkit-inner-spin-button {
            -webkit-appearance: none;
            display: none;
        }

        .intercept {
            float: right;
            margin: 5px 10px 0px 20px;
        }

            .intercept > input {
                min-height: 20px;
                min-width: 20px;
            }

            .intercept > label {
                vertical-align: top;
                font-size: 1.1em;
                padding-top: 3px;
                display: inline-block;
                font-weight: 500;
            }

        .lookup-options {
            margin-top: 15px;
            border-top: 1px solid #ccc;
            text-align: center;
            display: none;
        }

            .lookup-options a {
                display: inline-block;
                font-weight: 500;
                text-decoration: underline;
                margin: 3px 8px;
                cursor: pointer;
            }
        .lookup-fields{
            width: 300px;
            height: 275px;
            padding : 5px;
            margin : 2px;
            overflow-y: auto;
            border: 1px solid #DDD;
            scroll-snap-stop: initial;
        }
        
        .close-button {
        	padding: 6px 12px;
		    border-radius: 4px;
		    border: none;
		    background-color: #1976d2;
		    color: #fff;
		    font-weight: 600;
		    line-height: 1.4;
		    font-size: 1.1em;
		    cursor: pointer;
		    text-align: center;
		    min-width: 70px;
        }
        
        .choose-method {
        	display: none;
        	text-align: left;
        	z-index: 103;
        }

        .choose-method-head {
            display: flex;
            align-items:center;
            justify-content: space-between;
        }
        .choose-method-head h2{
            margin: 4px 0; 
            padding: 0px;             
        }

        .close-button-x {
            margin: 0px; 
            padding: 0px 2px; 
            font-weight: 600;
            font-size: 22px;
            cursor: pointer;
        }
        
        .close-button-x:hover {
        	color: #f00;
        	background-color: #eeeeee;
        }

        .choose-button-panel {
            margin:10px 0;
        }

        .choose-button-panel > div {
            min-width: 400px;
            margin: 6px 0;
        }
        .choose-button-forward > div ,
        .choose-button-backward > div {
            display: flex;
            justify-content: space-between;
        }
        
        .choose-button-panel h3{
            font-weight: 600;
            flex: 100%;
            margin: 6px 0;
        }
        .choose-button-panel ul {
            margin: 8px 0;
        }
        .choose-button-panel li {
            font-size: 1.3em;
        }

        @keyframes loading {
		  0% {background-color: orange;}
		  50% {background-color: white;}
		  100% {background-color: orange;}
		}
        
        .step-wise-animation {
        	padding: 6px;
	        animation-name: loading;
		    animation-duration: 2s;
		    animation-iteration-count: infinite;
        	display:none;
            width: 60%;
		    max-width: 80%;
        	max-height: 85vh;
            position:fixed;
        	z-index:102;
        }
        
        .animation-container {
            padding: 10px;
        	background-color: #fff;
            display:flex;
            flex-direction : column;
        }
        .step-wise-head h3{
            font-weight: 600;
            margin: 6px 0;
        }
        .step-wise-buttons {
            display: flex;
            align-items:center;
            justify-content: space-between;
            margin-top: 6px;
        }
        .step-wise-scroll {
        	max-height: 70vh;
        	display: flex;
            flex-wrap: no-wrap;
            overflow:auto;
        }
        .step-wise-report {
        	display: block;
            flex: 4;
            padding-right:20px;
            max-width: 60%;
        }
        .step-wise-info {
            padding: 6px;
            border: 1px solid #eee;
            background: #e1f1ff;
            display: flex;
            flex-wrap: wrap;
            justify-content: space-between;
            margin: 0 0 6px;
        }
        .step-wise-info p {
            margin: 4px 0;
            width: 100%;
            overflow: hidden;
            text-overflow: ellipsis;
        }
        .step-wise-info p span{
            font-weight: 500;
            font-size:1.2em;
        }
        .step-wise-status {
            flex: 100%;    
            margin: 0px 0 6px;
            font-weight: 600 !important;
        }
        
        .animation-head {
        	display: block;
        	justify-content: space-between;
        	margin-left: 10px;
        	height: 40%;
        }
        
        .step-wise-stack {
            margin: 10px 0;
        }
        
        .step-wise-stack-head {
            border: 1px solid #e4eaec;
            border-radius: 3px;
            margin-bottom: 6px;
        }
        
        .step-wise-stack-title {
            font-weight: 500;
        	padding: 6px;
        	text-align: center;
        	margin: 0 2px;
        	white-space: break-spaces;
        	border-bottom: 1px solid #e4eaec;
        }
        
        .field-box {
    		overflow: auto;
            display: flex;
            flex-wrap: wrap;
            padding: 3px 3px;
        }
        
        .field-box::-webkit-scrollbar {
        	width: 6px;
        }
        
        .input-field-instance {
        	border-radius: 100px;
        	background-color: #007bff;
            color: #fff;
        	text-overflow: ellipsis;
		    padding: 6px 12px;
		    margin: 3px;
		    overflow: hidden;
		    white-space: nowrap;
		    max-width: 100%;
		    text-align: center;
        }
        
        .animation-log::-webkit-scrollbar {
        	display: none;
        }
        
        .animation-log {
        	display: block;
        	overflow-y: auto;
            min-height:300px;
            max-height:300px;
        }
        
        .animation-log-div {
        	display: block;
        	max-width: 40%; 
        	min-width: 40%;
            border: 1px solid #e4eaec;
        }

        #animation-log-head {
            padding: 5px; 
            width: 100%; 
            text-align: center;
            margin: 0;
        }
        
        #animation-log-labels {
            display: flex;
            justify-content: space-evenly; 
            align-items: center;
            width: 100%; 
            text-align: center; 
            background-color: #4c4c4c; 
            font-size: 14px; 
            padding: 5px 0;
            font-weight:300;
            color: white;
        }
        
        .animation-log-instance {
        	padding: 5px 10px;
        	display: none;    
            width: 100%;
            text-align: center;
            font-style: italic;
            border-bottom: 1px solid #eee;
        }
        
        .animation-log-instance p {
        	font-size: 12px;
        	width: 20%;
        	margin-top: 5px;
        	margin-bottom: 5px;
        	text-overflow: ellipsis;
        	overflow: hidden;
        }
        
        .animation-log-div p {
        	margin: 0px;
        }
        
        .table-trigger {
            display: flex;
            align-items:center;
		    cursor: pointer;
            margin: 4px 0;
            color: #1a5890;
            font-weight: 600;
        }

        .table-trigger i {
        	background: url(/App_Static/css/images/imgopen.png) center no-repeat;
            background-size: cover;
		    width: 20px;
		    height: 20px;
		    margin-right: 4px;
            transform: scaleX(-1);
        }

        .soureTable-parent {  
            display: flex;
            flex-direction: column; 
        }
        .table-list {
            height:300px;
            flex:auto;
            overflow-y: auto;
            border: 1px solid #DDD; 
            scroll-snap-stop: initial;
        }

        .table-list td >label {
            display: inline-block;
            overflow: hidden;
            text-overflow: ellipsis;
            max-width: 200px;
        }

        span.selected-table {
            max-width: 70%;
            display: flex;
            align-items: center;
        }

        .selected-table span:not(.remove-selection) {
            font-weight:500;
            font-size:1.1em;
            max-width:90%;
            display: inline-block;
            overflow: hidden;
            text-overflow: ellipsis;
        }
        .selected-table .remove-selection {
            cursor: pointer;
            padding-left : 10px;
            font-size: 16px;
        }
        
        .selected-table > span[d]:before {
   			content: '✖';
    		color: red;
		}

        .field-picker-select {
            display: flex;
        }

        .field-picker-table {
            display:none;
            flex: auto;
            padding-left: 10px;
        }        
        .table-selected .field-picker-table{
            display: flex;
            align-items: center;
            justify-content: flex-end;
        }



        
        .field-picker-search .filter-input {
            width: 100%;
        }        

        .filter-results {
            width: 500px;
        }
        .filter-results td label {
            max-width:330px;
            display: inline-block;
            overflow: hidden;
            text-overflow: ellipsis;
        }
        .filter-results td:last-child span {
            max-width:85px;
            display: inline-block;
            overflow: hidden;
            text-overflow: ellipsis;
        }
        
    </style>
    <script src="https://cdn.jsdelivr.net/npm/openlayers@latest/dist/ol.js"></script>
    <script src="/App_Static/jslib/Chart.min.js"></script>
    <script src="/App_Static/js-v2/OLMap.js"></script>
    <script src="/App_Static/js-v2/ChartManager.js"></script>
    <script src="/App_Static/js-v2/mra/mra-common.js"></script>
    <script>

        var M;
        var regressionLog  = {};
        var regressionLogCache = {};
        var templateFields = {};
        var sourceTables = {};
        var lookupTables = {};
        var selectedFieldsStepWise = [];
        var selectedFieldId;
        var currentPoolSize = 0, currentDataStats = [], hasOutput = false;;
        function resizeWindow() {
            var H = $('.main-content-area').height();


            $('canvas', '.chart1-container').attr('width', $('.chart1-container').width() - 10);

            $('.data-view').height(H - $('.mra-header').height() - $('.input-fields-area').height() - 1);
            $('.data-filters').height(H - $('.data-map-area').height() - 2);
            $('.d1001').height(H);
            chm.refreshAll();
        }
        var chm = new ChartManager();

        function enableLayout() {
            $('.d1001').removeAttr('disabled');
            $('.shade').hide();
        }

        function disableLayout() {
            $('body').css({ cursor: null });
            $('.d1001').attr('disabled', 'disabled');
            $('.shade').show();
        }
        
        function resetTableSelection(){
        	$('.selected-table').html('');
            $('.soureTable-parent').hide();
            if(tableSelected) tableSelected = false;
            $('.field-picker-select').removeClass('table-selected');
            $('.table-trigger i').css('transform','scaleX(-1)');
            $('.table-trigger').attr('Active',0);
        }

        function showMessage(message) {
            alert(message);
        }

        function showWait() {
            $('body').css({ cursor: 'wait' });
        }

        function checkError(d, callback) {
            if (d.Success) {
                callback && callback(d);
            } else {
                alert('Operation failed - ' + (d.ErrorMessage || 'No details.'));
                loading.hide();
                enableLayout();
            }
        }


        var cachedFilter = {}, cachedField = {}; var lookupSelection = [], existingSelection = []; var currentTable, currentField, lookupFullResult;
        var tableSelected = false;
        function openFieldPicker(type, options) {
            var originalField, originalFieldOptions, fieldDataType;
            if (!hasOutput && type != "output") {
                alert('Please select an output field for analysis.');
                return false;
            }    
            disableLayout();
            closeLookupView();
            cachedField = {};

            function setFieldFilterOps(val) {
                if ((val == "") || (val =="NN") || (val =="NL") || (val == "IN") || (val == "NIN")) {
                    $('.field-filter-values').hide();
                    $('.field-prop-input[field="filtervalue"]', '.field-props').val('');
					$('.field-prop-input[field="filtervalue2"]', '.field-props').val('');
                } else if (val == "BW") {
                    $('.field-filter-values').addClass('filter-input-double');
                    $('.field-filter-values').show();
                } else {
                    $('.field-filter-values').removeClass('filter-input-double');
                    $('.field-filter-values').show();
                }
            }

            $('.lookup-options').hide();

            options = options || { currentValue: 0 };
            var dialogSelector = '.field-picker';
            var searchResults = [], searchCount = 0;



            $(dialogSelector).removeAttr('disabled');
            $(dialogSelector).show();

            $('.field-prop-input', '.field-props').val('');
            setFieldFilterOps('');
            var tBody =  ""
            $('.table-list table').html(tBody);
        	for (var t in sourceTables){
        			with (sourceTables[t]){
        				var rowTr = `<tr><td><input type="radio" id="tld_${ID}" name="selectTable" value="${ID}" class="select-table"/><label title="${Name}" for="tld_${ID}">${Name}</label></td></tr>`;	
        				tBody += rowTr;
        			}
        	}
			$('.table-list table').html(tBody);
			$('.select-table', '.table-list').off('click').on('click',()=>{
				var tId = $('input[name="selectTable"]:checked').attr('value');
				var tableLabel = $("label[for='tld_"+tId +"']").text();
				$('.filter-input', dialogSelector).val('');
                $('.field-prop-input').val(''); //Resetting all previous input values
                tableSelected = true;
                $('.field-picker-select').addClass('table-selected');
				$('.selected-table').html('<span title="'+tableLabel+'">'+ tableLabel + '</span><span class="remove-selection" d="1"></span>')
				$('.remove-selection', dialogSelector).bindEvent('click', function () {
					$('input[name="selectTable"]').removeAttr('checked');
					$('.selected-table').html('');
                    $('.field-picker-select').removeClass('table-selected');
                    $('.field-prop-input').val('') //Resetting all previous input values
                    tableSelected = false;
					doSearch();
				});
				doSearch();
			});
            var hasExpression = false, hasAggregate = false, hasFilter = false, lasLabel = false, hasLookup = false;
            switch (type) {
                case 'output':
                    $('.filter-title', dialogSelector).html('Select Output Field');
                    if (template) {
                        options.aggregate = template.OutputAggregate;
                        options.displayLabel = template.DisplayLabel;
                        options.currentValue = template.OutputField;
                        options.filterOperator = template.OutputFilterOperator;
                        options.filterValue = template.OutputFilterValue;
                        options.filterValue2 = template.OutputFilterValue2;
                        options.expression = template.OutputFieldExpression;
                    }
                    [hasLabel, hasExpression, hasAggregate, hasFilter, hasLookup] = [true, true, true, true, false];
                    $('.field-prop-input[field="aggregate"] option[value=""]').text('Default (MAX)');
                    break;
                case 'input':
                    $('.filter-title', dialogSelector).html('Select Input/Factor Field');
                    [hasLabel, hasExpression, hasAggregate, hasFilter, hasLookup] = [true, true, true, true, false];
                    $('.field-prop-input[field="aggregate"] option[value=""]').text('Default (MAX)');
                    break;
                case 'filter':
                    $('.filter-title', dialogSelector).html('Select Filter Field');
                    [hasLabel, hasExpression, hasAggregate, hasFilter, hasLookup] = [false, true, true, true, false];
                    $('.field-prop-input[field="aggregate"] option[value=""]').text('Default (MAX)');
                    break;
            }

            $('.nonparcel-field-props').hide();
            $('.prop-label')[hasLabel ? 'show' : 'hide']();
            $('.prop-expression')[hasExpression ? 'show' : 'hide']();
            $('.prop-aggregate')[hasAggregate ? 'show' : 'hide']();
            $('.prop-filter')[hasFilter ? 'show' : 'hide']();
            $('.prop-lookup')[hasLookup ? 'show' : 'hide']();

            $('.field-props').attr('disabled', 'disabled');

            var H = $('.main-content-area').height(), W = $('.main-content-area').width();
            var h = $(dialogSelector).height(), w = $(dialogSelector).width();
            $(dialogSelector).css({ top: (H - h) / 2, left: (W - w) / 2 });

            $('.filter-input', dialogSelector).val('');
            $('.field-aggregate select').val('');
            $('.nonparcel-field-props textarea').val('');
            $('.filter-results table').html('');

            function isFieldOptionChanged() {
                var newo = getFieldOptions();
                var changed = false;
                $('.field-prop-input[field]', '.field-props').each(function (i, x) {
                    var prop = $(x).attr('field');
                    if ((originalFieldOptions[prop] || '') != ($(x).val() || '')) {
                        changed = true;
                    }
                })
                if(JSON.stringify(existingSelection) != JSON.stringify(lookupSelection))
                    changed = true;
                if(originalFieldOptions.aggregateOverExpression != $('#field-prop-swap-button').attr("AOE"))
                    changed = true;    
                return changed;
            }

            function getFieldOptions() {
                var o = {};
                $('.field-prop-input', '.field-props').each(function (i, x) {
                    var prop = $(x).attr('field');
                    o[prop] = $(x).val();
                })
                return o;
            }

            function showSearchResults(d, q, sf) {
                function setFieldProps(field, options) {
                    options = options || {};

                    cachedField[currentField] = cachedField[currentField] || {};
                    for (var x in options) {
                        cachedField[currentField][x] = options[x];
                    }
					if(options && !options.aggregateOverExpression) {
		            	swapAggregateOverExpression("false");
		            } else {
		            	swapAggregateOverExpression("true");
		        	}
                    var numeric = [2, 7, 8, 9, 10].indexOf(field.DataType) > -1;
                    var isdate = field.DataType == 4;
                    $('option', '.field-props .field-prop-input[field="filterop"]').each((i,x)=>{
                        var value = $(x).attr("value") 
                        if(value == "IN" || value == "NIN"){
                            $(x)[field.DataType == 5 ? 'show' : 'hide']();
                            if(options.filterop == "" && field.DataType == 5) options.filterop = "IN"
                        }
                        else {
                            $(x)[field.DataType == 5 ? 'hide' : 'show']();
                        }
                    })
                    $('.field-prop-input', '.field-filter-values').prop('type', numeric ? 'number' : isdate ? 'date' : 'text');
                    if (numeric || isdate) {
                        $('option[filter="text"]', '.field-props .field-prop-input[field="filterop"]').attr('disabled', 'disabled');
                    } else {
                        $('option[filter="text"]', '.field-props .field-prop-input[field="filterop"]').removeAttr('disabled', 'disabled');
                    }

                    $('.field-prop-input', '.field-props').each(function (i, x) {
                        var prop = $(x).attr('field');
                        $(x).val(options[prop]);
                    })
                    setFieldFilterOps(options.filterop);
                    if (type == 'output') {
                        if (field.IsParcelField) {
                            $('.prop-filter').attr('disabled', 'disabled');;
                            $('.prop-aggregate').attr('disabled', 'disabled');;
                        } else {
                            $('.prop-filter').removeAttr('disabled');
                            $('.prop-aggregate').removeAttr('disabled');
                        }
                    }

                    if (field.DataType == 5) {
                        $('.prop-expression').hide();
                        $('#field-prop-swap-button').hide();
                        $('.prop-lookup').show();
                        $('.lookup-options').show();
                        if(options.lookup)
                            setLookupResults(options.lookup,template.ID,currentField,options.filterop);
                    } else {
                        $('.prop-expression')[hasExpression ? 'show' : 'hide']();
                        $('.prop-aggregate')[hasAggregate ? 'show' : 'hide']();
                        $('.prop-filter')[hasFilter ? 'show' : 'hide']();
                        $('.prop-lookup')[hasLookup ? 'show' : 'hide']();
                        $('.lookup-options').hide();
                        $('#field-prop-swap-button').show();
                    }


                    $('.field-props').removeAttr('disabled');
                }



                var currentQ = $('.filter-input', dialogSelector).val();
                if (currentQ && q != currentQ) { return; }
                searchResults = d;
                searchCount = d.length;
                var tbody = '';
                if (d.length > 0) {
                    for (var f in d) {
                        with (d[f]) {
                            var fidArray = [];
                            $('.input-fields-list .input-field').each(function() {
                                var fidValue = $(this).attr('fid');
                                fidArray.push(fidValue.toString());
                            });

                            var found = false;
                            for (var i = 0; i < fidArray.length; i++) {console.log("for arr",fidArray[i],"d(f)",d[f].ID + '-' + d[f].Name);
                                if (d[f].ID == fidArray[i]) {
                                    found = true;
                                    break;
                                }
                            }
                            if (found = false) {
                                if (tbody == '') {
                                    tbody = '<tr><td>No fields found!</td></tr>';
                                }
                            } else {
                                console.log("else");
                                let disName = tableSelected ? '' : `[${SourceTable}].`;
                                var tr = `<tr>
                                              <td>
                                                  <input type="radio" id="fld_${ID}" name="selectfield" dataType="${DataType}" value="${ID}" class="select-field"/>
                                                  <label title="[${SourceTable}].${Name}" for="fld_${ID}">
                                                      ${disName}<span>${Name}</span>
                                                  </label>
                                              </td>
                                              <td>
                                                  <span>${DataTypeName}</span>
                                              </td>
                                          </tr>`;
                                tbody += tr;
                                tbody = tbody.replace('<tr><td>No fields found!<\/td><\/tr>', "");
                            }
                        }
                    }
                } else {
                    tbody = '<tr><td>No fields found!</td></tr>';
                }

                $('.filter-results table').html(tbody);
                $('.select-field', '.filter-results').on('click', function () {
                    closeLookupView();
                    var c = this;
                    var fid = $('input[name="selectfield"]:checked').attr('value');
                    var field = searchResults.find(function (x) { return x.ID == fid });
                    fieldDataType = field.DataType;
                    var fname = field.Name;
                    var table = field.SourceTable;
                    var label = field.DisplayLabel;
                    var pf = field.IsParcelField;
                    if (table != currentTable) {
                        $('.nonparcel-field-props textarea').val('');
                    }
                    currentTable = table;
                    currentField = field.ID;
                    cachedField[field.ID] = cachedField[field.ID] || {};
                    var cf = cachedField[field.ID];
                    setFieldProps(field, {
                        aggregate: cf.aggregate || field.Aggregate,
                        label: cf.label || label,
                        expression: cf.expression || field.Expression,
                        filterop: (cf.filterop === undefined || cf.filterop === null) ? (field.FilterOperator || '') : cf.filterop,
                        filtervalue: cf.filtervalue || field.FilterValue,
                        filtervalue2: cf.filtervalue2 || field.FilterValue2,
                        lookup: cf.lookup || field.LookupTable,
                        aggregateOverExpression : field.AggregateOverExpression
                    });


                });

                $('.nonparcel-field-props textarea').off('change').on('change', function () {
                    cachedFilter[currentTable] = $('.nonparcel-field-props textarea').val();
                })
                $('.field-prop-input', '.field-props').off('change').on('change', function () {
                    var prop = $(this).attr('field'), val = $(this).val();
                    cachedField[currentField][prop] = val;
                    alignFieldPicker();
                    if (prop == "filterop") {
                        setFieldFilterOps(val);
                    }
                    else if(prop == "lookup") {
                      setLookupResults(val,template.ID,currentField);
                    }
                    else if(prop != "aggregate") closeLookupView();
                })

                if (sf) {
                    
                    var field = searchResults.find(function (x) { return x.ID == sf })
                    fieldDataType = field && field.DataType
                    //console.log(field);
                    if (field) {
                        currentField = field.ID;
                        $('.select-field[value="' + sf + '"]').prop('checked', true);
                        var fieldOptions = {
                            aggregate: options.aggregate || field.Aggregate,
                            label: field.DisplayLabel,
                            expression: options.expression || field.Expression,
                            filterop: options.filterOperator || field.FilterOperator || '',
                            filtervalue: options.filterValue || field.FilterValue,
                            filtervalue2: options.filterValue2 || field.FilterValue2,
                            lookup: field.LookupTable || '',
                            aggregateOverExpression : field.AggregateOverExpression
                        };
                        setFieldProps(field, fieldOptions);
                        //console.log(cachedField,   cachedField[currentField])
                        originalFieldOptions = fieldOptions;
                        originalField = field.ID;
                    }


                }  
            }

            function checkSelectAll(lookupCount){
                var count = 0;
                $(".lookup-select").each(function(x) {  
                        count += $(this).attr('checked') ? 1: 0;
                });
                if ( lookupCount == count) 
                    $('.chk-selectall', dialogSelector)[0].checked = true;
                else        
                    $('.chk-selectall', dialogSelector)[0].checked = false;       
            }
            
            function closeLookupView(){
                lookupSelection = [];
                $('.lookup-fields').html('');
                $('.lookup-parent').hide();
                alignFieldPicker();
            }

            function alignFieldPicker(){
                var docWidth = $(document).width();
                var pickerWidth = $('.field-picker').width();
                $('.field-picker').css('left',(docWidth - pickerWidth)/2 + 'px')
            }

			function setLookupResults(lookupName,templateId,fieldId,filteroperator){
				var lookupCount;
                lookupFullResult = [];
                lookupSelection = [];
                $('.chk-selectall', dialogSelector)[0].checked = false;
                $('.lookup-parent').show();  
                alignFieldPicker();
         
                PageMethods.GetLookupValues(lookupName, templateId, fieldId,(d)=>{
                    var html = '';
					if(d.Success){
	                    lookupCount = d.Count;                   
	                    for (var i in d.Data) {
	                        with (x = d.Data[i]){  
	                            x.Selected = $('.chk-selectall', dialogSelector)[0].checked ? true : x.Selected ;
	                            x.Selected = filteroperator == "NIN" ? !x.Selected : x.Selected ;
	                            x.checked = x.Selected ? 'checked="checked"' : '';
	                            x.s = x.Selected ? 1 : 0;
	                            html += `<div><input type="checkbox" id="lk_${ID}" ${checked} sel="${s}" val="${ID}" class="lookup-select" /><label for="lk__${ID}">${Name}</label></div>`;
	                            lookupFullResult.push(x.ID);
	                            if (x.Selected) lookupSelection.push(x.ID);
	                        }
	                    }
	                    existingSelection = [...lookupSelection];
	                    if(html == "" && d.Data.length == 0){
	                     html = '<span style="font-style: italic; text-align: right;font-size: 0.9em;float: right;">No fields found!</span>'
	                    }
	                    $('.lookup-fields').html(html);
	                    checkSelectAll(lookupCount);
	                    $('.lookup-select').off('change').on('change', function () {
	                        var v = $(this).attr('val')
	                        if (this.checked) {
	                            lookupSelection.push(v);
	                        } else {
	                                lookupSelection.splice(lookupSelection.indexOf(v), 1);
	                        }
	                         checkSelectAll(lookupCount);   
	                    })
	                    $('.chk-selectall', dialogSelector).off('change').on('change', function () {
	                        var checked = this.checked;
	                        lookupSelection = [];
	                        $(".lookup-select").each(function() {  
	                            if (checked){
	                                var checkAllValue = $(this).attr('val'); 
	                                lookupSelection.push(checkAllValue);
	                                $(this).attr("sel", 1); $(this).prop("checked",true);
	                            }
	                            else {
	                                var checkAllValue = $(this).attr('val'); 
	                                lookupSelection.splice(lookupSelection.indexOf(checkAllValue), 1);
	                                $(this).attr("sel", 0); $(this).prop("checked",false);
	                            
	                            }
	                        });
	                    });
	              }
				   else{
				  		$('.lookup-fields').html(html);
				  		say("Lookup is not compatible with the data of the selected Field. Please review the lookup.");
				  }				   
				})
            }
            function doSearch(q) {
                currentTable = null;
                $('.nonparcel-field-props').attr('disabled', 'disabled');
                $('.field-aggregate select').val('');
                var tableId = $('input[name="selectTable"]:checked').attr('value');
                PageMethods.FindField(templateId, 'mra-' + type, q, options.currentValue, !tableId?0:tableId, function (sr) {
                	var d = sr.Fields;
                    showSearchResults(d, sr.Query, options.currentValue);
                })
            }

            doSearch();

            $('.filter-input', dialogSelector).off('keyup').on('keyup', function () {
                var q = $(this).val();
                doSearch(q);
            });

            $('.filter-box-ok', dialogSelector).off('click').on('click', function () {


                $(dialogSelector).attr('disabled', 'disabled');
                
                function validateLookupSelection(lookupValues, dataType, op){
                    var validate = true;
                    if(dataType == 5){
                        if(op == 'IN' && lookupValues.length == 0){
                            say('Please select atleast one value from the lookup table value list')
                            validate = false;
                        }
                        else if(op == 'NIN' && lookupValues.length == 0){
                           say('Selecting all values along with the filter NOT IN returns no selection.')
                            validate = false; 
                        }
                    }
                    return validate;
                }

                function validateFieldFilter(op, mandatory) {
                    var valid = true;
                    var numeric = $('.field-prop-input[field="filtervalue"]', '.field-props').prop('type') == "number";
                    switch (op) {
                        case "":
                            if (!mandatory) return true;
                            valid = false;
                            say('Please select a filter condition.');
                            break;
                        case "BW":
                            var val1 = $('.field-prop-input[field="filtervalue"]', '.field-props').val();
                            var val2 = $('.field-prop-input[field="filtervalue2"]', '.field-props').val();
                            if (numeric) {
                                val1 = parseFloat(val1);
                                val2 = parseFloat(val2);
                                if (isNaN(val1) || isNaN(val2) || val1 > val2) {
                                    say('Please provide two values in ascending order for the BETWEEN filter condition.');
                                    valid = false;
                                }
                            } else {
                                if (!val1 || !val2 || val1 > val2) {
                                    say('Please provide two values in ascending order for the BETWEEN filter condition.');
                                    valid = false;
                                }
                            }

                            break;
                        default:
                            var val1 = $('.field-prop-input[field="filtervalue"]', '.field-props').val();
                            if ((op == 'NL' || op == 'NN' || op == 'IN' || op == 'NIN') && val1 == '') {
                                valid = true;
                                return valid;
                            }
                            if (numeric) {
                                val1 = parseFloat(val1);
                                if (isNaN(val1)) {
                                    say('Please provide a valid value for the filter condition.');
                                    valid = false;
                                }
                            } else {
                                if (!val1) {
                                    say('Please provide a valid value for the filter condition.');
                                    valid = false;
                                }
                            }

                            break;

                    }

                    return valid;
                }

                var field = $('input[name="selectfield"]:checked').attr('value');
                if (field) {

                    if (field == originalField && !isFieldOptionChanged()) {
                        $(dialogSelector).hide();
                        resetTableSelection();
                        loading.hide();
                        enableLayout();
                        return false;
                    } else {
                        if (compiled) {
                            if (!confirm('Regression analytics currently exist for this model. Any modifications made to this model will require you to run the regression analysis again. Are you sure you want to modify the existing model? ')) {
                                $(dialogSelector).hide();
                                resetTableSelection();
                                loading.hide();
                                enableLayout();
                                return false;
                            }
                            compiled = false;
                        }
                    }

                    var cf = cachedField[field];
                    console.log("cf",cf);
                    var agg = $('.field-prop-input[field="aggregate"]', '.field-props').val();
                 
                    var filter = $('.nonparcel-field-props textarea').val();
                    var label = $('.field-prop-input[field="label"]').val();
                    var fop = $('.field-prop-input[field="filterop"]', '.field-props').val();
                    var AggregateOverExpression = true;
                    var lookupValues = [...lookupSelection];
                
                    if (cf.filterop == "NIN" && lookupFullResult) { // changed to check lookupFullResult is not null
                        lookupValues = lookupFullResult.filter((x)=>{ return lookupValues.indexOf(x) === -1; });
                    }

                    if($('#field-prop-swap-button').attr("AOE") == "false")
                        AggregateOverExpression = false;
                    switch (type) {
                        case 'output':
                            var valid = validateFieldFilter(fop, false);
                            if (valid) {
                            	loading.show("Please Wait");
                            	disableLayout();
                                PageMethods.UpdateTemplateOutput(templateId, field, label, agg, cf.filterop, cf.filtervalue, cf.filtervalue2, cf.expression, AggregateOverExpression, function (d) {
                                    if (!d.Success && d.ErrorMessage) {
                                        sayError(d.ErrorMessage, d.DebugMessage);
                                        $(dialogSelector).removeAttr('disabled');
                                        return false;
                                    }
                                     
                                    $(dialogSelector).hide();
                                    resetTableSelection();
                                    existingSelection = [...lookupSelection];
                                    openTemplate(templateId, { selectFieldAction: 'refresh' });
                                });
                            } else {
                                $(dialogSelector).removeAttr('disabled');
                            }

                            break;
                        case "input":
                            var valid = validateFieldFilter(fop, false);
                            valid = valid && validateLookupSelection(lookupValues, fieldDataType, fop);
                            if (fieldDataType == 5 || cf.lookup) {
                                if (valid && !cf.lookup) {
                                    alert('Please select a lookup table.');
                                    valid = false;
                                } else {

                                    if (valid && !lookupTables[cf.lookup].hasValues) {
                                        alert('The selected lookup does not have linear graded values.');
                                        valid = false;
                                    }
                                }
                            }
                            if (valid) {
                            	loading.show("Please Wait");
                            	disableLayout();
                                PageMethods.UpdateTemplateField(options.currentValue, templateId, false, field, label, cf.expression, cf.filterop, cf.filtervalue, cf.filtervalue2, cf.lookup, agg, lookupValues, AggregateOverExpression, function (d) {
                                    if (!d.Success && d.ErrorMessage) {
                                            sayError(d.ErrorMessage, d.DebugMessage);
                                            $(dialogSelector).removeAttr('disabled');
                                            return false;
                                     }
                                     
                                    $(dialogSelector).hide();
                                    resetTableSelection();
                                    existingSelection = [...lookupSelection];
                                    openTemplate(templateId, { selectFieldAction: 'field', selectedFieldId: field });
                                })
                            } else {
                                $(dialogSelector).removeAttr('disabled');
                            }

                            break;
                        case "filter":
                            var valid = validateFieldFilter(fop, true);
                            valid = valid && validateLookupSelection(lookupValues, fieldDataType, fop);
                            if (valid) {
                                PageMethods.UpdateTemplateField(options.currentValue, templateId, true, field, label, cf.expression, cf.filterop, cf.filtervalue, cf.filtervalue2, cf.lookup, agg, lookupValues, AggregateOverExpression, function (d) {
                                    if (!d.Success && d.ErrorMessage) {
                                            sayError(d.ErrorMessage, d.DebugMessage);
                                            $(dialogSelector).removeAttr('disabled');
                                            return false;
                                     }
                                     
                                    $(dialogSelector).hide();
                                    resetTableSelection();
                                    existingSelection = [...lookupSelection];
                                    openTemplate(templateId, { selectFieldAction: 'refresh' });
                                })
                            } else {
                                $(dialogSelector).removeAttr('disabled');
                            }

                            break;
                    }

                } else {
                    $(dialogSelector).removeAttr('disabled');
                }

                return false;
            })	

            $('.filter-box-close', dialogSelector).bindEvent('click', function () {
                $(dialogSelector).hide();
                resetTableSelection();
                loading.hide();
                closeLookupView();
                enableLayout();
            });
            
            $('.table-trigger', dialogSelector).off('click').on('click',(e)=>{
                let el = '.table-trigger';
            	var active = $(el).attr('Active');
        		$('.soureTable-parent')[active == "0" ? 'show' : 'hide']();
        		$('i', el).css('transform',active == "0"? 'scaleX(1)': 'scaleX(-1)');
        		$(el).attr('Active',active == "0" ? 1 :0);
            	$('.table-list').scrollTop(0);
        		alignFieldPicker();
            });

            $('.lookup-generate').bindEvent('click', function () {
                if (confirm('Lookup table data will be generated from the selected table/field, but without linear graded values. It would be required to upload the XML lookup data with grade values filled up.\n\nAre you sure you want to continue?')) {
                    PageMethods.GenerateLookup(currentField, function (d) {
                        checkError(d, function () {
                            var lname = d.Data;
                            lookupTables[lname] = { hasValues: false };
                            $('.prop-lookup select').append($(`<option>${lname}<option>`));
                            $('.prop-lookup select').val(lname).trigger('change');
                            cachedField[currentField].lookup = lname;
                            $('.lookup-download').trigger('click');
                        });
                        loading.hide();
                        enableLayout();
                    });
                }
            });

            $('.lookup-download').bindEvent('click', function () {
                if (!currentField) {
                    alert('Please select a field.')
                } else if (!cachedField[currentField]) {
                    alert('Invalid field state.')
                } else if (!cachedField[currentField].lookup) {
                    alert('Please select a lookup field from the list.');
                } else {
                    var url = 'settings/groups.aspx?action=downloadlookup&lookup=' + cachedField[currentField].lookup;
                    location.href = url;
                }

                //cachedField[currentField].lookup = $('.prop-lookup select').val();
            });

            $('.lookup-upload').bindEvent('click', function () {
                $('.lookup-file').bindEvent("change", function () {
                    var confirmAction = function (callback) { callback && callback(); }
                    var c = this;
                    if (c.files && c.files.length > 0) {
                        var file = c.files[0];
                        var headline = false;
                        var nameparts = file.name.split('.');
                        if (nameparts.length > 1) {
                            var ext = nameparts[nameparts.length - 1].toLowerCase();
                            if (ext == 'csv') confirmAction = function (callback) {
                                headline = confirm('Does the first line of your uploaded file contains the column headers?');
                                callback && callback();
                            };

                            if (ext == "csv" || ext == 'xml') {
                                confirmAction(() => {
                                    var data = new FormData();
                                    data.append("files[]", file, file.name);
                                    data.append("head_line", headline ? "1" : "0");
                                    $.ajax({
                                        url: 'settings/groups.aspx',
                                        type: 'POST',
                                        data: data,
                                        dataType: 'json',
                                        processData: false,  // tell jQuery not to process the data
                                        contentType: false,  // tell jQuery not to set contentType
                                        success: function (d) {
                                            $(c).val('');
                                            loading.hide();
                                            enableLayout();
                                            if (d.success && d.records > 0) {
                                                if (d.records > 0) {
                                                    if (d.count == 1) {
                                                        var lname = d.lookupName;
                                                        if (!(lname in lookupTables)) {
                                                            $('.prop-lookup select').append($(`<option>${lname}<option>`));
                                                        }
                                                        $('.prop-lookup select').val(lname).trigger('change');
                                                        cachedField[currentField].lookup = lname;
                                                        lookupTables[lname] = { hasValues: d.valid };

                                                        if (!d.valid) {
                                                            showMessage("The uploaded lookup does not contains numeric values for computation.")
                                                        }
                                                    } else {
                                                        showMessage('You have uploaded multiple lookups. Please search for the appropriate name to select.');
                                                    }
                                                } else {
                                                    showMessage('You have uploaded an empty lookup.');
                                                }

                                            } else {
                                                showMessage(d.errorMessage ? d.errorMessage : 'Import declined due to invalid Lookup Data.');
                                            }
                                        }
                                    })
                                })
                            } else {
                                alert('You have selected an invalid file. Only CSV/XML files are allowed.');
                            }
                        }
                    }
                })
                $('.lookup-file').trigger('click');
            });

            $('.filter-input', dialogSelector).focus();
        }

        function swapAggregateOverExpression(value) {
        	var aggregateIndex =  $('.field-props tr').filter(function(x) {return $('.field-props tr')[x].className == "prop-aggregate"})[0].rowIndex + 1;
    		var filterIndex =  $('.field-props tr').filter(function(x) {return $('.field-props tr')[x].className == "prop-filter"})[0].rowIndex + 1;
        	var text = "";
    		if(value == "false") {
	        	$('.field-props tr:nth-child('+aggregateIndex+')').insertAfter('.field-props tr:nth-child('+filterIndex+')');
	        	$('#field-prop-swap-button').attr('AOE', 'false');
	        	text = "up";
	        } else if(value == "true") {
	        	$('.field-props tr:nth-child('+filterIndex+')').insertAfter('.field-props tr:nth-child('+aggregateIndex+')');
	        	$('#field-prop-swap-button').attr('AOE', 'true');
	        	text = "down";
	        }
	        $('#field-prop-swap-button').html('↑↓');
	        $('#field-prop-swap-button').attr('title', 'Move aggregate ' + text);
        }
        
        function isAggregateOverExpression() {
        	if($('#field-prop-swap-button').attr('AOE') == "true") {
        		swapAggregateOverExpression("false");
        	} else if($('#field-prop-swap-button').attr('AOE') == "false") {
        		swapAggregateOverExpression("true");
        	}
        }

        function openTemplate(tid, openerOptions) {
            openerOptions = openerOptions || {};
            var compileT = Object.keys(openerOptions).length == 0 ? 1 : 0;
            disableLayout();
            loading.show("Opening Model");
            templateId = tid;
            PageMethods.GetTemplate(templateId, compileT, function (d) {
                template = d.Data.Template;
                if (!template) {
                	loading.hide();
                	enableLayout();
                    alert('You have selected an invalid template');
                    window.location.href = '../dv/';
                    return false;
                }
                $('.view-result-link')[template.Compiled ? 'show' : 'hide']();
                compiled = template.Compiled;

                currentPoolSize = d.FullCount;
                $('.pool-size').html(d.FullCount + ' filtered parcels ')
                datasetId = template.DataSetID;
                if (template.DataSetID) {
                    $('.ds-selector').val(template.DataSetID);
                    $('.ds-selector option[value=""]').remove();
                }
				$('#spanMainHeading').html('Regression Model - ' + template.Name)
				$('#spanMainHeading').attr('title',template.Name);
                outputType = template.DataType;
                if (template.OutputField)
                    fieldTypes[template.OutputField] = outputType;

                $('.template-action-copy')[template && template.OutputField ? 'show' : 'hide']();

                $('.sample-download')[currentPoolSize > 0 ? 'show' : 'hide']();

                if (template.OutputField && template.OutputFieldTable && template.OutputFieldName) {
                    $('.model-output').val(template.OutputFieldTable + '.' + template.OutputFieldName);
                    hasOutput = true;
                } else if(template.OutputField) {
                	alert('Invalid Output Field, please reselect an Output Field to continue');
                }

                $('.template-name').val(template.Name);
                $('.template-name').off('change').on('change', function () {
                    var name = $(this).val();
                    if (!name) {
                        $(this).val(template.Name);
                        return false;
                    }
                    PageMethods.UpdateTemplateName(templateId, name, function (d) {
                        if (d.Success) {
                            template.Name = name;
                            $('#spanMainHeading').html('Regression Model - ' + template.Name)
                        }
                        else{
                            $('.template-name').val(template.Name);
                            say(d.ErrorMessage);
                        }
						//loading.hide();
                        //enableLayout();
                    })
                })

                $('#chk_intercept').prop('checked', template.NoIntercept);
                $('#chk_intercept').bindEvent('change', function () {
                    var chk = this;
                    var state = $(this).prop('checked');
                    $(this).attr('disabled', 'disabled');
                    PageMethods.UpdateTemplateIntercept(templateId, state, function (d) {
                        if (d.Success) {
                            template.NoIntercept = state;
                        } else {
                            $(chk).prop('checked', !state);
                        }
                        $(chk).removeAttr('disabled');
                        //loading.hide();
                    	//enableLayout();
                    })
                })

                var stats = d.Data.InputStats;
                if (stats && stats.length > 0) {
                    var html = '';
                    for (var i in stats) {
                        with (sx = stats[i]) {
                            html += `<tr><td><a>${FieldName}</a></td><td>${Count}</td><td>${Maximum}</td><td>${Minimum}</td><td>${Range}</td><td>${Average}</td><td>${StandardDeviation}</td></tr>`;
                        }
                    }
                    $('.input-statistics').html(html);
                    $('.input-stats-table').show();
                } else {
                    $('.input-stats-table').hide();
                }
                currentDataStats = d.Data.InputStats;

                openDataSetInMap(defaultExtent, function (p, c) {
                    M.resetView(p);
                    enableLayout();
                    loading.hide();
                    //if (poolSize > 0 && c == 0) {
                    //    alert('None of the parcels in current dataset criteria have GIS coordinates.')
                    //}
                })
            	if(!d.Success) {
            		loading.hide();
                	enableLayout();
                    confirm('Error opening Regression Model. Please check the input and output filters.\nMore Details: \n' + d.ErrorMessage);
                    return false;
            	}
                loading.hide();

            });

            PageMethods.GetInputFields(templateId, function (d) {
                fieldTypes = fieldTypes || {};
                var fields = d.Data;
                var html = '';
                templateFields = {};
                fields.forEach(function (f) {
                    with (f) {
                        fieldTypes[f.ID] = f.DataType;
                        html += `<div class="input-field" fid="${ID}"><a>${DisplayLabel}</a> <span e="1"></span><span d="1"></span></div>`;
                    }
                    if(f.ID !=-1) templateFields[f.ID] = f;
                });

                $('.input-fields-list').html(html);

                $('.input-fields-list div').first().attr('selected', 'selected');

                $('.input-fields-list a').off('click').on('click', function () {
                    var field = $(this).parent();
                    var fid = $(this).parent().attr('fid');
                    showInputFieldChart(fid);
                });

                $('.input-fields-list span').off('click').on('click', function () {
                    var fid = $(this).parent().attr('fid');
                    if ($(this).attr('e')) {
                        openFieldPicker('input', { currentValue: fid });
                    } else if ($(this).attr('d')) {
                        if (confirm('Are you sure you want to delete the input/factor field from the data model?')) {
                            loading.show("Processing...");
                            PageMethods.DeleteTemplateField(templateId, fid, false, function (d) {
                                loading.hide();
                                openTemplate(templateId, {
                                    selectFieldAction: 'first'
                                });
                            });
                        }
                    }
                });

                if (fields.length > 0) {
                    var selAction = openerOptions.selectFieldAction || 'first';
                    switch (selAction) {
                        case 'first':
                            showInputFieldChart(fields[0].ID);
                            break;
                        case 'refresh':
                            showInputFieldChart(selectedFieldId);
                            break;
                        case 'field':
                            showInputFieldChart(openerOptions.selectedFieldId);
                            break;
                        case 'none':
                            break;
                    }
                } else {
                    chm.clearAll();
                }


            });

            PageMethods.GetDataFilters(templateId, function (d) {
                var fields = d.Data;
                var html = '';
                fields.forEach(function (f) {
                    with (f) {
                        var delOption = '<a class="delete-filter-item"></a>';
                        switch (f.FieldType) {
                            case 'output':
                                delOption = '<span>Output Filter</span>';
                                break;
                            case 'input':
                                delOption = '<span>Input Filter</span>';
                                break;
                        }
                        html += `<tr><td fid="${ID}" ft="${FieldType}"><a class="data-filter-item">${SourceTable}.${Name}</a>${delOption}</td></tr>`;
                    }
                });
                $('.data-filter-list').html(html);

                $('.data-filter-item').off('click').on('click', function () {
                    var fid = $(this).parent().attr('fid');
                    var type = $(this).parent().attr('ft');
                    openFieldPicker(type, { currentValue: fid });
                })

                $('.delete-filter-item').off('click').on('click', function () {
                    if (confirm('Are you sure you want to delete the filter from the data model?')) {
                        var fid = $(this).parent().attr('fid');
                        PageMethods.DeleteTemplateField(templateId, fid, true, function (d) {
                            openTemplate(templateId, {
                                selectFieldAction: 'refresh'
                            });
                        })
                    }
                })

            });
        }
        var graphData = {};
        var lookupValues = [];
        var lookupLabels = [];
        function showInputFieldChart(fieldId) {
            disableLayout();
            loading.show("Opening Model");
            selectedFieldId = fieldId;
            var link = $('.input-field[fid="' + fieldId + '"]');
            $('.input-field').removeAttr('selected');
            $(link).attr('selected', 'selected');

            PageMethods.GetChartData(templateId, fieldId || 0, function (d) {
                enableLayout();
                loading.hide();
                if (!d.Success && d.ErrorMessage) {
                    if (d.ErrorMessage.toUpperCase().indexOf("INVALID COLUMN") != -1 && d.ErrorMessage.indexOf($('.field-prop-input[field="expression"]').val()) != -1) {
                        alert('Invalid Custom Expression. Please revise your entry.');
                       	$('.field-picker').show()
	                    //$('.field	-prop-input[field="expression"]').val('')
	                    $('.field-picker').removeAttr('disabled');  						
                        return false;
                    } else {
                    	sayError('Error while retrieving Chart Data', d.ErrorMessage);
                   	}
               		return false;
                }
                
                var cd = d.Data;
                graphData = cd.IOScatter
                var labels = cd.Labels;
                lookupValues = cd.LookupValues;
                lookupLabels = cd.LookupLabels;
                var y1Total = 0, y2Total = 0, x1Density = 0, x2Density = 0;
                var xMeanDifference = (cd.InputStats[1] - cd.InputStats[0])/2;
                cd.IOScatter.forEach((x) => {if(x.x < cd.InputStats[0]+xMeanDifference) {y1Total += x.y; x1Density++} else {y2Total += x.y; x2Density++}});
                var y1Mean = y1Total/x1Density;
                var y2Mean = y2Total/x2Density;
                var x1Mean = cd.InputStats[0] + (xMeanDifference/2);
                var x2Mean = cd.InputStats[1] - (xMeanDifference/2);
                var slope = (y2Mean - y1Mean)/ (x2Mean - x1Mean);
                var bias = (slope * x1Mean) - y1Mean;
                var y1 = (slope * (cd.InputStats[0]==0?0.1:cd.InputStats[0])) - bias;
                var y2 = (slope * (cd.InputStats[1]==0?0.1:cd.InputStats[1])) - bias;
                
                if (labels) {
                	if(lookupValues.length > 0 && fieldTypes[selectedFieldId] == 5) { 
                		chm.setData('chart1', labels[0], cd.IOScatter, {datatype:5, lookupValues:[], lookupLabels:[], dataX: [lookupValues[0], lookupValues[lookupValues.length-1]], dataY: [y1, y2], lineColor: 'rgb(100, 150, 255)', lineWidth: 3})
            		} else {
                    	chm.setData('chart1', labels[0], cd.IOScatter, {dataX: [cd.InputStats[0], cd.InputStats[1]], dataY: [y1, y2], lineColor: 'rgb(100, 100, 255)', lineWidth: 3});
                    }
                    chm.setData('chart2', labels[1], cd.InputStats);
                }
            });



        }

        var syncLock = false;
        function openDataSetInMap(extent, callback) {
            if (syncLock) return false;
            var resolution = M.getResolution();
            syncLock = true;
            PageMethods.GetMapData(templateId, extent, function (d) {
                M.clear();

                if (d.Count > 0) {
                    var slat = 0, slng = 0;
                    var pcount = 0;
                    for (var i in d.Data) {
                        var p = d.Data[i];
                        M.addPoint(p.Coordinates, resolution * p.Range, p.Intensity);
                        slng += p.Coordinates[0]; slat += p.Coordinates[1];
                        pcount += p.Volume;
                    }
                    var clng = slng / d.Count, clat = slat / d.Count;
                    if (callback) callback([clng, clat], pcount);
                    syncLock = false;
                    $('.map-pool').html(pcount + ' in map view')
                } else {
                    if (callback) callback(null, 0);
                    syncLock = false;
                    $('.map-pool').html('0 in map')
                }
            })
        }

        function runRegression() {
            if (!datasetId) {
            	loading.hide();
                say('Please select a valid Dataset for analysis.');
                return false;
            }
            
            if (!hasOutput) {
            	loading.hide();
                say('Please select an output field for analysis.');
                return false;
            }

            if (currentDataStats && currentDataStats.length == 0) {
            	loading.hide();
                say('Please select at least one input/factor field for analysis.');
                return false;
            }

            for (var x in currentDataStats) {
                var ci = currentDataStats[x];
                if (ci.Minimum == 0 && ci.Range == 0) {
                	loading.hide();
                    say('Input/Factor field \'' + ci.FieldName + '\' do not have sufficient valid data to perform regression analysis. Please choose another field.');
                    return false;
                }
            }

            if (!currentPoolSize) {
            	loading.hide();
                say('Invalid pool size. Please check the data or adjust the filters to get appropriate count of data to proceed with this function.');
                return false;
            }

        	loading.show("Running Regression");

            disableLayout();
            showWait();
            PageMethods.RunMRA(templateId, function (d) {
                var res = d.Data;
                if (d.Success) {
                	loading.hide();
                    var url = "mra-result.aspx?" + URLPack({ templateId: templateId }) + "&t=" + (new Date()).getTime();
                    window.location.href = url;
                } else {
                	loading.hide();
                    say('Operation failed with error: ' + d.ErrorMessage)
                    enableLayout();
                    $('body').css({ cursor: 'default' });
                }

            });
            return false;
        }
        
        function cancelStepWiseRegression() {
        	if(confirm('Are you sure you want to cancel the Process')) {
        		$('.animation-close').attr('value', 'true');
        		$('.step-wise-status').css('color', '#f49414');
            	$('.step-wise-status').html('Status: Cancelling...');
        	}
        }
        
        function chooseStepwiseRegression() {
        	if(selectedFieldsStepWise.length) {
        		if(confirm('Are you sure you want to delete the previous result and run the regression again')) {
        			$('.masklayer').show();
		        	$('.mask-default').hide();
		        	$('.choose-method').show();
        		}
        	} else {
        		$('.masklayer').show();
	        	$('.mask-default').hide();
	        	$('.choose-method').show();
        	}
        }
        
        function viewStepwiseResult() {

        	disableLayout();
        	$('.step-wise-animation').show();
        }
        
        function removeFieldsStepWise() {
        	function remove(fields) {
        		if(!fields.length) {
                    openTemplate(templateId, {
                        selectFieldAction: 'first'
                    });
                    $('.animation-running-fields-instance').remove();
                    $('.step-wise-animation').hide();
                } else {
                	var field = fields[0];
                	fields = fields.slice(1);
                	PageMethods.DeleteTemplateField(templateId, field, false, () => {
                		remove(fields);
                	});
                }
        	}
        	
        	var fieldList = [], fieldNames = [];
            for(x in templateFields) {fieldList.push(x);}
            var fieldsToRemove = fieldList.filter((x) => { return !selectedFieldsStepWise.includes(x); });
            if(!fieldsToRemove.length) {
            	alert('There are no fields to remove');
            	return false;
            }
            for(x in fieldsToRemove) { fieldNames.push(templateFields[fieldsToRemove[x]].Name); }
            if(confirm('Do you want to remove the field' + (fieldsToRemove.length>1? 's ': ' ') + fieldNames.toString().replaceAll(',', ', ') + ' from the model ' + template.Name + '?')) {
            	remove(fieldsToRemove);
            };
        }
        
        function renderInputFields(type, fields) {
        	if(type == 'input') {
        		$('.animation-input-fields-instance').remove();
            	for(x in fields) {
					$('.animation-input-fields').append('<p class="input-field-instance animation-input-fields-instance" id="input' + fields[x] + '" title="' + fields[x] + '">' + fields[x] + '</p>');
				}
			} else if(type == 'running') {
				$('.animation-running-fields-instance').remove();
				for(x in fields) {
					$('.animation-running-fields').append('<p class="input-field-instance animation-running-fields-instance" id="running' + fields[x] + '" title="' + fields[x] + '">' + fields[x] + '</p>');
				}
			} else if(type == 'selected') {
				$('.animation-selected-fields-instance').remove();
				for(x in fields) {
					$('.animation-selected-fields').append('<p class="input-field-instance animation-selected-fields-instance" style="background-color: #11c26d;" id="selected' + fields[x] + '" title="' + fields[x] + '">' + fields[x] + '</p>');
				}
			}
		}
        
        function stepwiseRegression(backward) {
        	if (!datasetId) {
                say('Please select a valid Dataset for analysis.');
                return false;
            }
            
            if (!hasOutput) {
                say('Please select an output field for analysis.');
                return false;
            }

            if (currentDataStats && currentDataStats.length <= 1) {
                say('Please select at least two input/factor field for Step-Wise Regression.');
                return false;
            }
            for (var x in currentDataStats) {
                var ci = currentDataStats[x];
                if (ci.Minimum == 0 && ci.Range == 0) {
                    say('Input/Factor field \'' + ci.FieldName + '\' do not have sufficient valid data to perform stepwise regression analysis. Please choose another field.');
                    return false;
                }
            }

            if (!currentPoolSize) {
                say('Invalid pool size. Please check the data or adjust the filters to get appropriate count of data to proceed with this function.');
                return false;
            }
            
            function getFieldName(fieldId) {
            	return templateFields[fieldId].Name
            }
            
            function getFieldNameList(list) {
            	var fieldNames = [];
            	for(x in list) { fieldNames.push(getFieldName(list[x])) };
            	return fieldNames;
            }
            
            function findPorT(fields, toggle) {
            	var value = 0;
            	if(toggle) {
            		value = Math.max();
	            	for(x in fields) {
	            		if(!backward) if(Math.abs(fields[x].tStat) > value) value = fields[x].tStat; //find largest tStat for Forward Selection (toggle, !backward)
	            		else { if(Math.abs(fields[x].pValue) > value) value = fields[x].pValue; } // find largest pValue for Backward Selection (toggle, backward)
	            	}
	            } else {
	            	value = Math.min();
	            	for(x in fields) {
            			if(!backward) if(Math.abs(fields[x].pValue) < value) value = fields[x].pValue; // find smallest pValue for Forward Selection (!toggle, !backward)
            			else { if(Math.abs(fields[x].tStat) < value) value = fields[x].tStat; } // find smallest tStat for Backward Selection (!toggle, backward)
            		}
	            }
            	return value;
            }
            
            function getAverage(data) {
            	var sum = 0;
            	for(x in data) {
            		sum += Math.abs(data[x].pValue);
            	}
            	return sum/(data.length);
            }
            
            function setStatus(status) {
            	if(status == 'Completed') $('.step-wise-status').css('color', '#058507');
            	else $('.step-wise-status').css('color', '#f49414');
            	$('.step-wise-status').html('Status: ' + status);
            }
            
            function updateOnCompletion(finalFields) {
            	setStatus('Completed');
            	loading.hide();
            	$('.animation-close').html('Close');
				$('.animation-close').attr('onClick', "$('.step-wise-animation').hide(); $('.wui-layout').css('overflow', 'visible'); enableLayout(); loading.hide(); return false;");
				$('.animation-remove-fields').show();
				renderInputFields('running', getFieldNameList(fieldList.filter((x) => { return !finalFields.includes(x)})));
				$('#regression-using').html('Removed Fields');
				$('.animation-running-fields-instance').css('background-color', '#ff4c52');
				renderInputFields('selected', getFieldNameList(finalFields));
				$('.step-wise-animation').css('animation-name', 'Other');
				$('.step-wise-animation').css('background-color', '#3cdf3c');
				addLog('info', 'completed');
				$('.step-wise-result').show();
				regressionLogCache = regressionLog;
				selectedFieldsStepWise = finalFields;
            }
            
            function addLog(type, data, fields) {
            	if(type == 'data') {
            		logId++
	            	var paragraph = '';
	            	for(x in data) {
	            		paragraph += '<div style="display: inline-flex; width: 100%;"><p class="log-fieldNames" style="text-align: left; padding-left: 5px; width: 60%;" title="' + getFieldName(data[x].FieldId) + '">' + getFieldName(data[x].FieldId) + '</p><p class="log-tStat">' + data[x].tStat.toFixed(3) + '</p><p class ="log-pValue">' + data[x].pValue.toFixed(3) + '</p></div>';
	            	}
	            	$('.animation-log').append('<div class="animation-log-instance ' + fields.replaceAll(',','') + '" id="' + logId + '">' + paragraph + '</div>');
	            	$('#' + logId).slideDown();
            	} else if(type == 'info') {
            		logId++;
            		if(data == 'selected') var bgColor ='#28d17c';
            		else if(data == 'completed') var bgColor = '#4c4c4c';
            		else var bgColor = '#f48888';
            		var paragraph = '';
            		for(x in fields) {
            			paragraph += '<p style="width: 100%">' + ((data == 'selected')?'Selected ':(data == 'completed')?'Regression Completed': 'Removed ') + (data == 'completed'?'':getFieldName(fields[x])) + '</p>';
            		}
            		$('.animation-log').append('<div class="animation-log-instance" id="' + logId + '" style="background-color: ' + bgColor + ';">' + paragraph + '</div>');
            		$('#' + logId).slideDown();
            	}
            }
            
	        function stepForward(field) {
	        	if($('.animation-close').attr('value') == 'true') {
        			$('.wui-layout').css('overflow', 'visible');
	        		enableLayout(); 
	        		loading.hide();
	        		$('.step-wise-animation').hide();
	        		regressionLog = {};
	        		selectedFieldsStepWise = []
        			return false;
        		}
	        	var fieldsToRun = selectedFields.toString(); 
	        	fieldsToRun += (fieldsToRun?',':'') + field;
	        	setStatus('Regression in Progress');
	        	renderInputFields('running', (getFieldNameList(selectedFields).toString() + (selectedFields.length>0?',':'') + getFieldName(field).toString()).split(','));
	        	renderInputFields('selected', selectedFields.length?(getFieldNameList(selectedFields)):[]);
	        	PageMethods.RunStepWise(templateId, fieldsToRun, function(d) {
	        		if($('.animation-close').attr('value') == 'true') {
	        			$('.wui-layout').css('overflow', 'visible');
		        		enableLayout(); 
		        		loading.hide();
		        		$('.step-wise-animation').hide();
		        		regressionLog = {};
		        		selectedFieldsStepWise = []
	        			return false;
	        		}
	        		if(!d.Success) {
	        			$('.step-wise-animation').hide();
	        			enableLayout();
	        			say(d.ErrorMessage);
	        			return false;
	        		}
	            	regressionLog[fieldsToRun] = d.Data.FieldData;
	            	addLog('data', d.Data.FieldData, fieldsToRun);
	            	$('#animation-log-head').html('Log');
	            	$('#animation-log-labels').show();
	            	newFieldValues.push(d.Data.FieldData.filter((x) => {return x.FieldId == field})[0]);
	            	if(newFieldValues.length < unSelectedFields.length) {
	            		stepForward(unSelectedFields[newFieldValues.length]);
	            	} else {
	            		var fieldsToAdd = newFieldValues.filter((x)=> {return x.pValue < alphaToEnter});
	            		if(fieldsToAdd.length > 0) {
	            			var fieldSmallestP = fieldsToAdd.filter((x)=> {return x.pValue == findPorT(fieldsToAdd, false)});
	            			var fieldToAdd = {}
	        				fieldSmallestP.length>1? fieldToAdd = fieldsToAdd.filter((x)=> {return x.tStat == findPorT(fieldsToAdd, true)}): fieldToAdd = fieldSmallestP;
	        				if(selectedFields.length > 0) {
	        					var invalidIds = [];
	        					regressionLog[selectedFields.toString()+','+fieldToAdd[0].FieldId].filter((x)=>{return x.pValue >= alphaToRemove}).forEach((y) => {invalidIds.push(y.FieldId.toString())})
	        					selectedFields = selectedFields.filter((x) => {return !invalidIds.includes(x)});
								selectedFields.push((fieldToAdd[0].FieldId).toString());
								addLog('info', 'selected', [fieldToAdd[0].FieldId]);
								if(invalidIds.length) addLog('info', 'removed', [invalidIds]);
	        					unSelectedFields = fieldList.filter((x) => {return !selectedFields.includes(x)});
	        					if(unSelectedFields.length > 0) {
	        						newFieldValues = [];
	        						stepForward(unSelectedFields[0]);
	        					} else {
	        						updateOnCompletion(selectedFields); //Finishing state: No Fields left to Add
	        					}
	        				} else {
	        					selectedFields.push((fieldToAdd[0].FieldId).toString());
	        					addLog('info', 'selected', [fieldToAdd[0].FieldId]);
	        					unSelectedFields = fieldList.filter((x) => {return !selectedFields.includes(x)});
	        					newFieldValues = [];
	        					stepForward(unSelectedFields[0]);
	        				}
	            		} else {
	            			updateOnCompletion(selectedFields); //Finishing state: No Fields left with pValue < alphaToEnter to add
	            		}
	            	}
	            });
	        }
	        
	        function stepBackward(field) {
	        	if($('.animation-close').attr('value') == 'true') {
        			$('.wui-layout').css('overflow', 'visible');
	        		enableLayout(); 
	        		loading.hide();
	        		$('.step-wise-animation').hide();
	        		regressionLog = {};
	        		selectedFieldsStepWise = []
        			return false;
        		}
	        	var fieldsToRun = fieldList.filter((x) => { return !selectedFields.includes(x) && x != field; }).toString();
	        	setStatus('Regression in Progress');
	        	renderInputFields('running', (getFieldNameList(fieldList.filter((x) => { return !selectedFields.includes(x) && x != field; })).toString()).split(','));
	        	renderInputFields('selected', getFieldNameList(fieldList.filter((x) => {return !selectedFields.includes(x)})));
            	$('animation.selected-fields').html(("Selected Fields: " + getFieldNameList(fieldList.filter((x) => { return !selectedFields.includes(x)})).toString()).replaceAll(',', ', '));
	        	PageMethods.RunStepWise(templateId, fieldsToRun, function(d) {
	        		if($('.animation-close').attr('value') == 'true') {
	        			$('.wui-layout').css('overflow', 'visible');
		        		enableLayout(); 
		        		loading.hide();
		        		$('.step-wise-animation').hide();
		        		regressionLog = {};
		        		selectedFieldsStepWise = []
	        			return false;
	        		}
	        		if(!d.Success) {
	        			$('.step-wise-animation').hide();
	        			enableLayout();
	        			say(d.ErrorMessage);
	        			return false;
	        		}
	            	regressionLog[fieldsToRun] = d.Data.FieldData;
	            	addLog('data', d.Data.FieldData, fieldsToRun);
	            	$('#animation-log-head').html('Log');
	            	$('#animation-log-labels').show();
	            	pAverages[fieldsToRun] = getAverage(d.Data.FieldData);
	            	if(Object.keys(pAverages).length < unSelectedFields.length) {
	            		stepBackward(unSelectedFields[Object.keys(pAverages).length]);
	            	} else {
	            		var lowestAvg = Math.min();
	            		var setToProceed = "";
						for(x in pAverages) { 
							if(pAverages[x] < lowestAvg) {
								lowestAvg = pAverages[x];
								setToProceed = x;
							}
						}
						selectedFields = fieldList.filter((x) => { return !setToProceed.split(',').includes(x)});
						var fieldIdRemoved = unSelectedFields.filter((x) => {return selectedFields.includes(x.toString());})
						unSelectedFields = [];
	            		var fieldsToRemove = regressionLog[setToProceed].filter((x)=> { return x.pValue > alphaToRemove; });
	            		fieldsToRemove.filter((x) => { unSelectedFields.push(x.FieldId);});
	            		unSelectedFields = unSelectedFields.filter((x) => {return !selectedFields.includes(x)});
	            		if(fieldIdRemoved.length) addLog('info', 'removed', fieldIdRemoved);
	            		if(unSelectedFields.length > 0) {
	            			pAverages = {};
	            			stepBackward(unSelectedFields[0]);
	            		} else {
	            			updateOnCompletion(fieldList.filter((x) => {return !selectedFields.includes(x)}))
	            		}
	            	}
	            });
	        }
            
            regressionLog = {};
            var cancel = false;
            var logId = 0;
            var alphaToEnter = 0.15, alphaToRemove = 0.15;
            var newFieldValues = [], pAverages = {};
            var fieldList = [], fieldNames = [];
            var selectedFields = [], unSelectedFields = [];
            for(x in templateFields) {fieldList.push(x);fieldNames.push(templateFields[x].Name);}
            
        	disableLayout();
        	$('.masklayer').show();
        	$('.mask-default').hide();
        	renderInputFields('input', fieldNames);
        	renderInputFields('running', []);
        	renderInputFields('selected', []);
        	$('.animation-close').attr('value', '');
        	$('#regression-using').html('Regression using');
			$('.animation-running-fields-instance').css('background-color','#fff');
        	$('.step-wise-output').html(template.OutputFieldName);
        	$('.step-wise-input-num').html(fieldList.length);
        	$('.animation-close').html('Cancel');
        	$('.animation-close').attr('onClick', 'cancelStepWiseRegression(); return false;');
        	$('.animation-remove-fields').hide();
        	$('#animation-log-labels').hide();
        	$('#animation-log-head').html('Awaiting Log');
        	$('.animation-log-instance').remove();
        	$('.step-wise-animation').css('animation-name', 'loading');
        	setStatus('Initiating');
        	$('.wui-layout').css('overflow', 'clip');
        	$('.step-wise-animation').show();
        	$('.step-wise-result').hide();
            if(!backward) {
	            selectedFields = [];
	        	unSelectedFields = fieldList;
            	$('.animation-regression-type').html("Forward Step-Wise Regression");
            	stepForward(unSelectedFields[0]);
            } else {
            	pAverages = {};
            	$('.animation-regression-type').html("Backward Step-Wise Regression");
            	stepBackward([-1]);
            }
        }

        function copyTemplate(sourceTemplateId) {
            if (confirm('Are you sure that you want to create a clone of this regression model?')) {
                PageMethods.CopyTemplate(sourceTemplateId, function (d) {
                    if (d.Success) {
                        window.location.href = "mra.aspx?" + URLPack({ templateId: d.Data });
                    } else {
                        alert(d.ErrorMessage);
                        loading.hide();
                        enableLayout();
                        window.location.href = '../dv/';
                    }
                })
            } else {
                if (!templateId) {
                    window.history.back();
                }
            }
        }

        function viewResult() {
            window.location.href = "mra-result.aspx?" + URLPack({ templateId: templateId });
        }

        function downloadSamples() {
            if (template.OutputField == 0) {
                alert("Please select an Output Field before downloading Samples");
                return false;
            }
            var url = location.pathname + '/exportinput?templateId=' + templateId;
            window.open(url);
        }

        function showCPInfo(pd) {
            var cpi = $('.chart-info');
            $('.iv-parcel', cpi).html(pd.pk);
            let outField = template?.DisplayLabel
            let inField = $('.input-field[selected="selected"] a').html()
            let inFieldID = $('.input-field[selected="selected"]').attr("fid")
            let inFieldDataype = templateFields[inFieldID]?.DataType
            $('.in-field', cpi).html(inField + ':');
            $('.out-name', cpi).html(outField + ':');
            const lbl = lookupLabels[lookupValues.findIndex((c)=>{return c == pd.x})];
            let value = pd.x + (lbl?" ("+lbl + ")":"")
            if(outputType != 8) {
            	if(inFieldDataype == 5){
                    $('.iv-actual', cpi).html(value);
                }
                else
                    $('.iv-actual', cpi).html(numberWithCommas(Math.round($out(pd.x) * 100) / 100));
            	$('.iv-predicted', cpi).html(numberWithCommas(Math.round($out(pd.y) * 100) / 100));
            } else {
                if(inFieldDataype == 5){
                    $('.iv-actual', cpi).html(value);
                }
                else
                    $('.iv-actual', cpi).html($out(pd.x));
                $('.iv-predicted', cpi).html($out(pd.y));
            }
            //$(".iv-actual", cpi).html($out(pd.a));
    		//$(".iv-predicted", cpi).html($out(pd.p));

            $('.iv-parcel', cpi).html(pd.KeyValue1);
            var cb = $(scatterChart.chart.canvas).box();
            var pb = cpi.box();
            var t = -cb.height + (cb.height - pb.height) / 2, l = (cb.width - pb.width) / 2;
            cpi.css({ 'margin-top': t, 'margin-left': l });
            cpi.show();

            var qcurl = '/protected/quality/#/parcel/' + pd.ParcelId;
            $('.iv-qc-link', cpi).attr('href', qcurl);
            $('.iv-qc-link', cpi).attr('target', '_parcel_' + pd.ParcelId);
            $('.iv-qc-link', cpi).attr('rel', 'noopener');

            $(scatterChart.chart.canvas).attr('disabled', 'disabled');

            $('.close', cpi).off('click').on('click', () => {
                $(scatterChart.chart.canvas).removeAttr('disabled');
                cpi.hide();
            })
        }
        var scatterChart
        // function getLabelForValue(val,i) {
        //     const lbl = lookupLabels[lookupValues.findIndex((x)=>{return x == val})];
        //     return val + (lbl?" - "+lbl:"")
        // }

        function getTooltipValue(tooltip,data){
            const lbl = lookupLabels[lookupValues.findIndex((x)=>{return x == tooltip.xLabel})];
            return tooltip.xLabel +(lbl?` (${lbl})`:"") + ","+tooltip.yLabel;
        }
        $(() => {

            var qd = QueryData;
            pageQuery = QueryData;
            resizeWindow();

            scatterChart = chm.createChart('chart1', 'chart1', 'scatter',{
                tooltips: {
                    callbacks: {
                       label: function(tooltipItem, data) {
                          return getTooltipValue(tooltipItem,data);
                       }
                    }
                 }
            });
            chm.createChart('chart2', 'chart2', 'bar', {
                labels: ['Min', 'Max', 'Avg']
            });
            
            scatterChart.onclick = (x, c) => {
                if (x.length > 0) {
                    var ce = x[0];
                    var pd = graphData[ce._index];
                    showCPInfo(pd);
                }
                $(document)
                    .off("click")
                    .on("click", (e) => {
                        var container = $(".chart-info");
                        if (
                            !container.is(e.target) &&
                            container.has(e.target).length === 0
                        ) {
                            if (
                                !$(c.canvas).is(e.target) ||
                                !($(c.canvas).is(e.target) && x.length)
                            ) {
                                $(scatterChart.chart.canvas).removeAttr("disabled");
                                container.hide();
                            }
                        }
                    });
            };

            PageMethods.GetLoadInfo(function (d) {
            	loading.show('Opening Model');
                if (d.Success) {
                    var px = d.Data.GIS;
                    defaultExtent = [px.lg1, px.lt1, px.lg2, px.lt2];
                    defaultCenter = [(px.lg1 + px.lg2) / 2, (px.lt1 + px.lt2) / 2];
					sourceTables = {};
		            d.Data.SourceTables.forEach(function (x, i) {
		                sourceTables[x.ID] = x;
		            });
                    M = new MapView('mapbox', defaultExtent, defaultCenter);
                    M.viewChanged = function (e) {
                        openDataSetInMap(e);
                    }
                    $('.spn_Mainhead').append('<div class="mra-links"><a class="b2 template-action-copy" onclick="copyTemplate(templateId);" style="display:none; font-size: 13px;">Clone Model</a></div>')
					$('.spn_Mainhead').css('display', 'flex');

                    var html = pageQuery.dsid ? '' : '<option value="">-- Select --</option>';
                    d.Data.Datasets.forEach(function (x) {
                        with (x) {
                            html += `<option value="${ID}">${Name} (${PoolSize} parcels)</option>`;
                        }
                    })

                    $('.ds-selector').html(html);

                    $('.ds-selector').on("change", function () {
                        var dsid = $(this).val();
                        if (dsid) {
                            datasetId = dsid;
                        }

                    })
                    d.Data.LookupNames.map(function (x) { lookupTables[x.LookupName] = { hasValues: x.HasValues } });
                    var lookupOptions = '<option value="" disabled="disabled">None</option>' + (d.Data.LookupNames || []).map(function (x) { x.v = x.HasValues ? '' : 'nv'; return `<option ${x.v} value="${x.LookupName}">${x.LookupName}</option>`; }).reduce(function (x, y) { return x + y; })
                    $('.prop-lookup select').html(lookupOptions);
                    if (qd.templateId) {
                        if (qd.clone) {
                            copyTemplate(qd.templateId);
                        } else {
                            openTemplate(qd.templateId);
                        }

                    } else {
                        if (confirm('Do you want to create a new regression model?')) {
                            PageMethods.CreateNewTemplate(qd.datasetId || 0, function (d) {
                                if (d.Success) {
                                    window.location.href = "mra.aspx?" + URLPack({ templateId: d.Data });
                                } else {
                                    alert(d.ErrorMessage);
                                    loading.hide();
                                    enableLayout();
                                    window.location.href = '../dv/';
                                }
                            })
                        } else {
                            /*window.location.href = '../dv/';*/
                            window.history.back();
                        }
                    }

                    $('.ds-selector').on('change', function () {
                    	loading.show("Changing Dataset");
                        var dsid = $(this).val();
                        if (!templateId) {
                            $(this).val('');
                            return false;
                        }

                        PageMethods.UpdateTemplateDataSet(templateId, dsid, function (d) {
                            openTemplate(templateId, { selectFieldAction: 'refresh' });
                            loading.hide();
                        });
                    })

                } else {
                    alert(d.ErrorMessage || 'GIS Configuration unavailable!')
                }
            });
        })
        $(window).on("resize", resizeWindow);
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div class="wui-layout d1001">
        <div style="min-width: 350px; width: 350px; padding: 2px;" class="ca3-bg">
            <div class="data-map-area">
                <div style="padding: 4px;" class="ds-selector-cell">
                    <div style="padding: 2px;">
                        Select Dataset:
                    <div class="fr">
                        <a href="data/dataset.aspx" class="l_l1">Manage Datasets</a>
                    </div>
                    </div>
                    <select class="ds-selector" style="margin-bottom: 1px;">
                    </select>
                </div>
                <div class="r i" style="font-size: 0.9em; padding: 3px 10px;"><span class="pool-size"></span>(<span class="map-pool"></span>)</div>
                <div style="height: 300px; background-color: #EEE; border: 1px solid #CCC;" id="mapbox">
                </div>
            </div>
            <div class="data-filters">
                <table class="mGrid app-content" cellspacing="0" style="width: 100%; border-collapse: collapse;">
                    <thead>
                        <tr class="thr">
                            <th scope="col">Data Filters<a class="filter-add-link" onclick="openFieldPicker('filter')">Add Output Filter</a>
                            </th>
                        </tr>
                    </thead>
                    <tbody class="data-filter-list"></tbody>
                </table>
            </div>
        </div>
        <div class="wui-stretch" style="">
            <div class="ca3-bg mra-header" style="height: 62px;">
                <div class="wui-sub head-bar">
                    <div style="width: 270px" class="ca3-bg">
                        <div style="padding: 2px;">
                            Select Output Field:
                        </div>
                        <input type="text" readonly="readonly" class="model-output" onclick="openFieldPicker('output');" />
                    </div>
                    <div style="width: 250px">
                        <div style="padding: 2px;">
                            Model Name:
                        </div>
                        <input type="text" maxlength="50" class="template-name" />
                    </div>
                    <div class="wui-stretch" style="display: table; height: 60px;">
                        <div style="display: table-cell; vertical-align: bottom" class="template-buttons">
                            <div class="r mra-links">
                            	<a class="b2 step-wise-result" onclick="viewStepwiseResult();" style="display: none; margin-right: 20px;">View Result</a>
                                <a class="b2 sample-download" onclick="downloadSamples();" style="display: none">Download Samples</a>
                                <a class="b2 view-result-link" onclick="viewResult();" style="display: none">View Result &gt;&gt;</a>
                            </div>
                            <button type="button" class="ca6-bg" action="chooseStepwiseRegression();">Run Step-Wise</button>
                            <button type="button" class="ca6-bg" action="runRegression();">Run Regression</button>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div style="" class="ca4-bg input-fields-area">
                    <div class="ca5-bg b2" style="padding: 3px 10px;">Input/Factor Fields:</div>
                    <div class="input-field-add input-field ca6-bg">
                        <a onclick="openFieldPicker('input');">Add</a>
                    </div>
                    <div class="intercept">
                        <input type="checkbox" id="chk_intercept" />
                        <label for="chk_intercept">No Intercept</label>
                    </div>
                    <div class="input-fields-list">
                    </div>
                </div>
                <div class="data-view">
                    <div class="wui-sub clear">
                        <div class="wui-stretch chart1-container">
                            <canvas id="chart1" width="320" height="320"></canvas>
                            <div class="chart-info" style="display: none;">
                                <a class="close" nosel="1">X</a>
                                <h3 class="nomar" style="margin-bottom: 6px;">Parcel: <span class="iv-parcel b2"></span></h3>
                                <table style="border-spacing: 0px;">
                                    <tr>
                                        <td class = "in-field" style="width: 80px;"></td>
                                        <td class="iv-actual iv-field r"></td>
                                    </tr>
                                    <tr>
                                        <td class="out-name"></td>
                                        <td class="iv-predicted iv-field r"></td>
                                    </tr>
                                </table>
                                <div class="r" nosel="1">
                                    <a class="b2 iv-qc-link">Open in QC</a>
                                </div>
    
                            </div>
                        </div>
                        <div style="min-width: 300px; min-height: 320px;" class="">
                            <canvas id="chart2" width="300" height="320"></canvas>
                        </div>
                    </div>
                    <div class="" style="padding: 5px 10px;">
                        <table class="mGrid app-content input-stats-table" cellspacing="0" style="width: 100%; border-collapse: collapse;">
                            <thead>
                                <tr class="thr">
                                    <th scope="col" style="width: 300px;">Input Field
                                    </th>
                                    <th scope="col">Count
                                    </th>
                                    <th scope="col">Maximum
                                    </th>
                                    <th scope="col">Minimum
                                    </th>
                                    <th scope="col">Range
                                    </th>
                                    <th scope="col">Average
                                    </th>
                                    <th scope="col">Standard Deviation
                                    </th>
                                </tr>
                            </thead>
                            <tbody class="input-statistics"></tbody>
                        </table>
                    </div>
                </div>

            </div>

        </div>
    </div>
    <div class="shade dock-full" style="display: none;"></div>
    <div class="filter-box popup field-picker" style="width: auto; display: none;">
        <a class="filter-box-close">✖</a>
        <h1 class="nomar filter-title">Select Output Field</h1>
        <div style="display:flex;">
            <div style = "flex=1;padding-right: 10px;display:none;" class ="soureTable-parent">
            <div class="label" style="padding:2px;font-weight: bold;">Select any Table</div>
            	<div class="table-list">
	                <table style= "width : 100%;">
	                </table>
	             </div>
            </div>
            <div>
                <div class="field-picker-select">
                    <div class="table-trigger" Active= 0>
                        <i>&nbsp;</i>
                        <span>View Tables</span>
                    </div>
                    <div class="field-picker-table">
                        <p class="nomar">Sel. Table : &nbsp;</p>
                        <span class="selected-table"> </span>
                    </div>
                </div>
                <div class="field-picker-search">
                    <p class="nomar label">Type text to search:</p>
                    <input class="filter-input"/>
                </div>
                <div class="filter-results" style="height: 140px; overflow-y: auto; border: 1px solid #DDD; scroll-snap-stop: initial;">
                    <table style="width: 100%;">
                    </table>
                </div>
                <table class="field-props">
                    <tr class="prop-label">
                        <td>Display Label: </td>
                        <td>
                            <input class="field-prop-input" field="label" />
                        </td>
                    </tr>
                    <tr class="prop-expression">
                        <td>Custom Expression: </td>
                        <td>
                            <input class="field-prop-input" field="expression" />
                        </td>
                    </tr>
                    <tr class="prop-aggregate">
                        <td>Aggregate Function: </td>
                        <td>
                            <select class="field-prop-input" field="aggregate">
                                <option value="" disabled="disabled">None</option>
                                <option value="SUM">SUM</option>
                                <option value="AVG">AVG</option>
                                <option value="MAX">MAX</option>
                                <option value="MIN">MIN</option>
                                <option value="dbo.MEDIAN">MEDIAN</option>
                                <option value="dbo.IQR">IQR</option>
                                <option value="dbo.IQRMAX">IQRMAX</option>
                                <option value="dbo.IQRMIN">IQRMIN</option>
                            </select>
                            <button class="field-prop-input" id="field-prop-swap-button" AOE="true" title="Move aggregate down" onclick="isAggregateOverExpression(); return false;">↑↓</button>
                        </td>
                    </tr>
                    <tr class="prop-lookup">
                        <td>Lookup Table: </td>
                        <td>
                            <select class="field-prop-input" field="lookup">
                                <option value="" disabled="disabled">None</option>
                            </select>
                        </td>
                    </tr>
                    <tr class="prop-filter">
                        <td>Field Filter: </td>
                        <td>
                            <select class="field-prop-input" field="filterop">
                                <option value="">None</option>
                                <option value="EQ">Equal To</option>
                                <option value="NE">Not Equal To</option>
                                <option value="GT">Greater Than</option>
                                <option value="LT">Less Than</option>
                                <option value="GE">Greater/Equal To</option>
                                <option value="LE">Lesser/Equal To</option>
                                <option value="BW">Between</option>
                                <option value="CN" filter="text">Contains</option>
                                <option value="NL">Is NULL</option>
                                <option value="NN">Is Not NULL</option>
                                <option value="IN">IN</option>
                                <option value="NIN">NOT IN</option>
                            </select>
                            <div class="field-filter-values">
                                <input class="field-prop-input" field="filtervalue" maxlength="20" />
                                <span>AND</span>
                                <input class="field-prop-input" field="filtervalue2" maxlength="20" />
                            </div>

                        </td>
                    </tr>
                </table>
                <div class="nonparcel-field-props">
                    <div class="field-aggregate">
                        <span>Select aggregate: </span>
                        <select>
                                <option value="" disabled="disabled"></option>
                                <option>MAX</option>
                                <option>MIN</option>
                                <option>SUM</option>
                                <option>AGV</option>
                            </select>
                        </div>
                        <div class="field-table-filter">
                            <div>Table Filter:</div>
                            <textarea rows="4"></textarea>
                        </div>
                    </div>

                    <div class="r filter-button-panel">
                        <button class="filter-button filter-box-ok">
                            Select Field
                        </button>
                    </div>
                    <div class="lookup-options">
                        <a class="lookup-upload">Upload Lookup</a>
                        <a class="lookup-generate">Generate Lookup from Data</a>
                        <a class="lookup-download">Download Lookup</a>
                        <div style="display: none;">
                            <input type="file" class="lookup-file" accept=".xml,.csv" />
                        </div>
                </div>
            </div>
            <div class ="lookup-parent" style="flex:1;display:none;margin-left:10px;"><!--change flexs-->
                <span class="filter-select-all filter-checkbox" style="margin-top: 25px;">
                    <input type="checkbox" id="chkselectall" class="chk-selectall" />
                    <label for="chkselectall">Select all values</label>
                </span>
                <div class="lookup-fields">

                </div>
            </div>
        </div>  
        
    </div>
	<div class="shade dock-full" style="display: none; z-index:100"></div>
	<div class="popup-dg choose-method">
		<div class="choose-method-head">
			<h2> Choose the Selection Method</h2>
			<span class="close-button-x" onclick="$('.choose-method').hide(); loading.hide(); return false;"> ✖ </span>
		</div>
		<div class="choose-button-panel">
			<div class="choose-button-forward">
                <h3>Forward Selection</h3>
                <div>
                    <ul>
                        <li>More Accurate results</li>
                        <li>Takes more time than Backward</li>
                    </ul>
				    <button class="close-button forward-button" onclick="$('.choose-method').hide(); stepwiseRegression(false); return false;"> Start </button>
                </div>
			</div>
			<div class="choose-button-backward">
                <h3>Backward Selection</h3>
                <div>
                    <ul>
                        <li>Faster than Forward</li>
                        <li>Slightly less accurate results</li>
                    </ul>
				    <button class="close-button backward-button" onclick="$('.choose-method').hide(); stepwiseRegression(true); return false;"> Start </button>
                </div>
                
			</div>
		</div>
	</div>
    <div class="popup-dg step-wise-animation">
        <div class="animation-container">
            <div class="step-wise-head">
                <h3 class="animation-regression-type" ></h3>
            </div>
            <div class="step-wise-scroll">
                <div class="step-wise-report">
                    <div class="step-wise-info">
                        <h3 class="step-wise-status">Status: </h3>
                        <p>No of Input Fields : <span class="step-wise-input-num"></span></p>
                        <p>Output Field : <span class="step-wise-output"></span></p>
                    </div>
                    <div class="step-wise-stack">
                        <div class="step-wise-stack-head">
                            <p class="step-wise-stack-title">Input Fields</p>
                            <div class="field-box animation-input-fields" style="overflow-y: auto;"></div>
                        </div>
                        <div class="step-wise-stack-head">
                            <p class="step-wise-stack-title" id="regression-using">Regression using</p>
                            <div class="field-box animation-running-fields" style="overflow-y: auto;"></div>
                        </div>
                        <div class="step-wise-stack-head">
                            <p class="step-wise-stack-title">Selected Fields</p>
                            <div class="field-box animation-selected-fields" style="overflow-y: auto;"></div>
                        </div>
                    </div>                    
                </div>
                <div class="animation-log-div">
                    <h3 id="animation-log-head">Awaiting Log...</h3>
                    <div id="animation-log-labels" style="">
                        <p style="width: 55%; text-align: left">Input Field</p>
                        <p>tStat</p>
                        <p>pValue</p>
                    </div>
                    <div class="animation-log">
                        
                    </div>
        	    </div>
            </div>
            <div class="step-wise-buttons">
                <button class="close-button animation-close" onclick="cancelStepWiseRegression(); return false;">
                    Cancel
                </button>
                <button class="close-button animation-remove-fields" onclick="removeFieldsStepWise(); return false;">
                    Update Input Fields
                </button>
            </div>
        </div>
    </div>
</asp:Content>
<script runat="server">

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetLoadInfo() As ActionResult(Of MRADesignerPageLoadInfo)
        Dim l As New ActionResult(Of MRADesignerPageLoadInfo)
        Try
            Dim li As New MRADesignerPageLoadInfo
            li.GIS = Database.Tenant.GetDataTable("EXEC GIS_GetConfiguration").ConvertToList(Of GISConfig).FirstOrDefault
            li.Datasets = Database.Tenant.GetDataTable("SELECT * FROM MRA_DataSet WHERE ISNULL(PoolSize,0) > 0 AND Error = 0 ORDER BY Name").ConvertToList(Of Dataset)
            li.LookupNames = Database.Tenant.GetDataTable("SELECT LookupName, CAST(CASE WHEN COUNT(NULLIF(Value, 0)) > 0 THEN 1 ELSE 0 END AS BIT) As HasValues FROM ParcelDataLookup GROUP BY LookupName ORDER BY 1").ConvertToList
            li.SourceTables = Database.Tenant.GetDataTable("SELECT ID,Name From DatasourceTable Where ImportType IN(0,1) Order by Name").ConvertToList
            l.Data = li ' 
            l.Success = l.Data IsNot Nothing
            l.Count = li.Datasets.Count
            l.FullCount = l.Count
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        'l.Query = q
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)> Public Shared Function FindField(templateId As Integer, source As String, q As String, value As Integer, tableId As Integer) As FieldSearchResult
        Dim l As New FieldSearchResult
        l.Query = q
        Dim dt = Database.Tenant.GetDataTable("EXEC MRA_FindFields {0}, 0, @Source = {1}, @SelectedValue = {2}, @DataItemId = {3}, @TableID = {4}".SqlFormat(True, q, source, value, templateId, tableId))
        l.Fields = dt.ConvertToList(Of DataField)
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)> Public Shared Function UpdateTemplateOutput(templateId As Integer, fieldId As Integer, label As String, aggregate As String, filterop As String, filtervalue As String, filtervalue2 As String, expression As String, AggOverExp As Boolean) As ActionResult(Of Boolean)
        Dim l As New ActionResult(Of Boolean)
        Dim Status As String = ""
        Try
            If Not String.IsNullOrWhiteSpace(expression) Then
                Dim tableName As String = Database.Tenant.GetStringValue("SELECT t.CC_TargetTable FROM DataSourceField f INNER JOIN DataSourceTable t ON f.TableId = t.Id WHERE f.ID = " & fieldId)
                Dim parsedExpression As String = expression
                parsedExpression = expression.Replace("$.", "").Replace("CURRENT_YEAR", "2020")
                Dim testSql As String = "SELECT TOP 1 MAX(" + parsedExpression + ") FROM " + tableName
                Database.Tenant.GetDataTable(testSql)
            End If
            Status = "ExpressionCorrect"

            Dim currentOutput = Database.Tenant.GetIntegerValue("SELECT OutputField FROM MRA_Templates WHERE ID = {0}".SqlFormat(True, templateId))
            l.Count = Database.Tenant.Execute("UPDATE MRA_Templates SET OutputField = {1}, OutputAggregate = {2}, OutputFieldLabel = {3}, OutputFilterOperator = {4}, OutputFilterValue = {5}, OutputFilterValue2 = {6}, OutputFieldExpression = {7}, OutputAggregateOverExpression = {8} WHERE ID = {0}".SqlFormat(True, templateId, fieldId, aggregate, label, filterop, filtervalue, filtervalue2, expression, AggOverExp))
            Dim poolSize = CAMACloud.BusinessLogic.DataAnalyzer.RegressionAnalyzer.CompileTemplate(templateId)
            If filterop IsNot Nothing Then
                l.FullCount = poolSize
            Else
                l.FullCount = -1
            End If
            l.Success = True
        Catch ex As Exception
            l.Success = False
            l.ErrorMessage += ex.Message
            l.DebugMessage = ex.Message
            If ex.InnerException IsNot Nothing Then
                l.ErrorMessage = ex.InnerException.Message
            End If
            If (Status <> "ExpressionCorrect") Then
                l.ErrorMessage = "Invalid Custom Expression"
                If ex.InnerException IsNot Nothing Then
                    l.DebugMessage = ex.InnerException.Message
                End If
            End If
        End Try
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)> Public Shared Function UpdateTemplateName(templateId As Integer, name As String) As ActionResult(Of Boolean)
        Dim l As New ActionResult(Of Boolean)
        Try
            Dim checkName = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM MRA_TEMPLATES WHERE NAME = {0}".SqlFormat(True, name))
            If checkName > 0 Then
                l.Success = False
                l.ErrorMessage = "Already a model exist with the name {0}. Please use a different name for the model".FormatString(name)
                Return l
            End If
            l.Count = Database.Tenant.Execute("UPDATE MRA_Templates SET Name = {1} WHERE ID = {0}".SqlFormat(True, templateId, name))
            l.Success = True
        Catch ex As Exception
            l.Success = False
            l.ErrorMessage = ex.Message
        End Try
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)> Public Shared Function UpdateTemplateIntercept(templateId As Integer, intercept As Boolean) As ActionResult(Of Boolean)
        Dim l As New ActionResult(Of Boolean)
        Try
            l.Count = Database.Tenant.Execute("UPDATE MRA_Templates SET NoIntercept = {1} WHERE ID = {0}".SqlFormat(True, templateId, intercept))
            l.Success = True
        Catch ex As Exception
            l.Success = False
            l.ErrorMessage = ex.Message
        End Try
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)> Public Shared Function UpdateTemplateField(currentField As Integer, templateId As Integer, isFilter As Boolean, fieldId As Integer, label As String, expression As String, filterop As String, filtervalue As String, filtervalue2 As String, lookup As String, aggregate As String, lookupValues() As String, AggOverExp As Boolean) As ActionResult(Of Boolean)
        Dim l As New ActionResult(Of Boolean)
        Dim Status As String = ""
        Try
            If Not String.IsNullOrWhiteSpace(expression) Then
                Dim tableName As String = Database.Tenant.GetStringValue("SELECT t.CC_TargetTable FROM DataSourceField f INNER JOIN DataSourceTable t ON f.TableId = t.Id WHERE f.ID = " & fieldId)
                Dim parsedExpression As String = expression
                parsedExpression = expression.Replace("$.", "").Replace("CURRENT_YEAR", "2020")
                Dim testSql As String = "SELECT TOP 1 MAX(" + parsedExpression + ") FROM " + tableName
                Database.Tenant.GetDataTable(testSql)
            End If
            Status = "ExpressionCorrect"

            If currentField = 0 AndAlso Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM MRA_TemplateFields WHERE TemplateID = {0} AND FieldID = {1} AND IsFilter = {2};".SqlFormat(True, templateId, fieldId, isFilter)) = 0 Then
                Database.Tenant.Execute("INSERT INTO MRA_TemplateFields (TemplateID, FieldID, IsFilter) VALUES ({0}, {1}, {2})".SqlFormat(True, templateId, fieldId, isFilter))
            Else
                'If currentField <> fieldId AndAlso Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM MRA_TemplateFields WHERE TemplateID = {0} AND FieldID = {1} AND IsFilter = {2};".SqlFormat(True, templateId, fieldId, isFilter)) > 0 Then
                '   l.Success = False
                '   If isFilter = 0 Then
                '     l.ErrorMessage = "Same field cannot be used twice as Input factor"
                '   Else
                '     l.ErrorMessage = "Same field cannot be used twice as filter."
                '   End If  
                '   return l
                'End If
                If currentField <> fieldId Then
                    Database.Tenant.Execute("DELETE FROM MRA_LookupValues WHERE TemplateId = {0} AND TemplateFieldID = {1}; DELETE FROM MRA_LookupValueResults WHERE TemplateId = {0} AND TemplateFieldID = {1}; ".SqlFormat(True, templateId, currentField))
                End If
                Database.Tenant.Execute("UPDATE MRA_TemplateFields SET FieldId = {1}, isFilter = {2}, Label = {3},  Expression = {4}, FilterOperator = {5}, FilterValue = {6}, FilterValue2 = {7}, LookupTable = {8}, Aggregate = {9} where FieldId = {0};".SqlFormat(True, currentField, fieldId, isFilter, label, expression, filterop, filtervalue, filtervalue2, lookup, aggregate))
            End If

            If filterop <> "BW" Then
                filtervalue2 = ""
            End If

            If (filterop = "") Then
                filtervalue = ""
                filtervalue2 = ""
            End If

            Dim sqlUpdate = "UPDATE MRA_TemplateFields SET Label = {3}, Expression = {4}, FilterOperator = {5}, FilterValue = {6}, FilterValue2 = {7}, LookupTable = {8}, AggregateOverExpression = {9}, Aggregate = {10}  WHERE TemplateID = {0} AND FieldID = {1} AND IsFilter = {2}"
            l.Count = Database.Tenant.Execute(sqlUpdate.SqlFormat(True, templateId, fieldId, isFilter, label, expression, filterop, filtervalue, filtervalue2, lookup, AggOverExp, aggregate))
            If lookup <> "" AndAlso lookupValues.Length > 0 Then
                Dim TargetTable As String = Database.Tenant.GetStringValue("SELECT t.CC_TargetTable FROM MRA_TemplateFields tf INNER JOIN DataSourceField f ON tf.FieldID = f.Id INNER JOIN DataSourceTable t ON f.TableId = t.Id WHERE tf.TemplateID = {0} AND tf.FieldID = {1}".SqlFormat(True, templateId, fieldId))
                CAMACloud.BusinessLogic.DataAnalyzer.RegressionAnalyzer.SaveLookupValues(templateId, fieldId, TargetTable, lookup, lookupValues)
            End If
            Dim poolSize = CAMACloud.BusinessLogic.DataAnalyzer.RegressionAnalyzer.CompileTemplate(templateId)
            If filterop IsNot Nothing Then
                l.FullCount = poolSize
            Else
                l.FullCount = -1
            End If
            'l.Count = Database.Tenant.Execute("UPDATE MRA_Templates SET OutputField = {1}, OutputAggregate = {2}, OutputFilter = {3}, OutputFieldLabel = {4} WHERE ID = {0}".SqlFormat(True, templateId, fieldId, aggregate, Filter, label))
            l.Success = True
        Catch ex As Exception
            l.Success = False
            l.ErrorMessage += ex.Message
            l.DebugMessage = ex.Message
            If ex.InnerException IsNot Nothing Then
                l.ErrorMessage = ex.InnerException.Message
            End If
            If (Status <> "ExpressionCorrect") Then
                l.ErrorMessage = "Invalid Custom Expression"
                If ex.InnerException IsNot Nothing Then
                    l.DebugMessage = ex.InnerException.Message
                End If
            End If
        End Try
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)> Public Shared Function UpdateTemplateDataSet(templateId As Integer, datasetId As Integer) As ActionResult(Of Boolean)
        Dim l As New ActionResult(Of Boolean)
        Try
            l.Count = Database.Tenant.Execute("UPDATE MRA_Templates SET DataSetId = {1} WHERE ID = {0}".SqlFormat(True, templateId, datasetId))
            l.FullCount = CAMACloud.BusinessLogic.DataAnalyzer.RegressionAnalyzer.CompileTemplate(templateId)
            l.Success = True
        Catch ex As Exception
            l.Success = False
            l.ErrorMessage = ex.Message
        End Try
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)> Public Shared Function GetTemplate(templateId As Integer, compileModel As Integer) As ActionResult(Of MRAProperties)
        Dim l As New ActionResult(Of MRAProperties)
        Try
            l.Data = New MRAProperties
            If compileModel = 1 Then
                CAMACloud.BusinessLogic.DataAnalyzer.RegressionAnalyzer.CompileTemplate(templateId)
            End If
            l.Data.Template = Database.Tenant.GetDataTable("EXEC MRA_GetTemplate {0}".SqlFormat(False, templateId)).ConvertToList(Of MRATemplate).FirstOrDefault
            l.Data.InputStats = Database.Tenant.GetDataTable("EXEC MRA_GetInputDataStatistics {0}".SqlFormat(False, templateId)).ConvertToList(Of MRAInputStatistics)
            l.FullCount = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM MRA_TemplateParcels WHERE TemplateID = " & templateId)
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
            l.DebugMessage = ex.Message
            If ex.InnerException IsNot Nothing Then
                l.ErrorMessage = ex.InnerException.Message
            End If
        End Try
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)> Public Shared Function GetInputFields(templateId As Integer) As ActionResultList(Of DataField)
        Dim l As New ActionResultList(Of DataField)
        Try
            l.Data = Database.Tenant.GetDataTable("EXEC MRA_FindFields '__!list!__', 0, @Source = 'mra-input', @SelectedValue = null, @DataItemId = {0}".SqlFormat(False, templateId)).ConvertToList(Of DataField)
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)> Public Shared Function GetDataFilters(templateId As Integer) As ActionResultList(Of DataField)
        Dim l As New ActionResultList(Of DataField)
        Try
            l.Data = Database.Tenant.GetDataTable("EXEC MRA_FindFields '__!list!__', 0, @Source = 'mra-filter', @SelectedValue = null, @DataItemId = {0}".SqlFormat(False, templateId)).ConvertToList(Of DataField)
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)> Public Shared Function DeleteTemplateField(templateId As Integer, fieldId As Integer, isFilter As Boolean) As ActionResult(Of Boolean)
        Dim l As New ActionResult(Of Boolean)
        Try
            l.Count = Database.Tenant.Execute("DELETE FROM MRA_TemplateFields WHERE TemplateID = {0} AND FieldId = {1} AND IsFilter = {2};".SqlFormat(False, templateId, fieldId, isFilter))
            l.FullCount = CAMACloud.BusinessLogic.DataAnalyzer.RegressionAnalyzer.CompileTemplate(templateId)
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        Return l
    End Function


    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetMapData(templateId As Integer, range() As Single) As ActionResult(Of List(Of OpenLayerParcelData))
        Dim l As New ActionResult(Of List(Of OpenLayerParcelData))
        Try
            Dim dt = Database.Tenant.GetDataTable("EXEC MRA_GetParcelCountStatisticsForMap 'template', {0}, {1}, {2}, {3}, {4}".SqlFormat(True, templateId, range(1), range(3), range(0), range(2)))
            l.Data = dt.ToRows.Select(Function(x) New OpenLayerParcelData(x("Latitude"), x("Longitude")) With {.Range = x("Range"), .Intensity = x("Intensity"), .Volume = x("Volume")}).ToList
            l.Success = True
            l.Count = l.Data.Count
            l.FullCount = l.Count
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        'l.Query = q
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetChartData(templateId As Integer, fieldId As Integer) As ActionResult(Of MRAInputsChartData)
        Dim l As New ActionResult(Of MRAInputsChartData)
        Try
            l.Data = New MRAInputsChartData

            Dim ds = Database.Tenant.GetDataSet("EXEC MRA_GetTemplateInputGraphs {0}, {1}".SqlFormat(True, templateId, fieldId))

            l.Data.Labels = ds.Tables(0).ToRows.Select(Function(x) x.GetString("Name")).ToList
            l.Data.IOScatter = ds.Tables(1).ConvertToList(Of XY)
            Dim inputStats = ds.Tables(2).ToRows.FirstOrDefault
            l.Data.LookupValues = ds.Tables(3).ToRows.Select(Function(x) x.GetString("Value")).ToList
            l.Data.LookupLabels = ds.Tables(3).ToRows.Select(Function(x) x.GetString("Label")).ToList
            If inputStats IsNot Nothing Then
                l.Data.InputStats = {inputStats.GetInteger("Min"), inputStats.GetInteger("Max"), inputStats.GetInteger("Avg")}.ToList
            Else
                l.Data.InputStats = {0, 0, 0}.ToList
            End If

            l.Success = True
            l.Count = l.Data.IOScatter.Count
            l.FullCount = l.Count
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        'l.Query = q
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)> Public Shared Function RunMRA(templateId As Integer) As ActionResult(Of Boolean)
        Dim l As New ActionResult(Of Boolean)
        Dim RegressStatus As Integer
        Try
            Dim poolSize = CAMACloud.BusinessLogic.DataAnalyzer.RegressionAnalyzer.CompileTemplate(templateId)

            Dim compiledSql = Database.Tenant.GetStringValue("SELECT CompiledInputsSQL FROM MRA_Templates WHERE ID = " & templateId)
            If compiledSql IsNot Nothing Then

                Dim dt = Database.Tenant.GetDataTable(compiledSql)
                RegressStatus = RegressionAnalyzer.EvaluateMultipleRegression(templateId, dt, True, False)
                Select Case RegressStatus
                    Case 1
                        l.Success = True
                    Case 0
                        l.Success = False
                        l.ErrorMessage = "No Template."
                    Case -1
                        l.Success = False
                        l.ErrorMessage = "Not enough data points to perform regression"
                    Case -2
                        l.Success = False
                        l.ErrorMessage = "Not enough Data to run regression"
                    Case -3
                        l.Success = False
                        l.ErrorMessage = "Data is not fit to run regression"
                End Select
                l.Count = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM MRA_ResidualOutput WHERE TemplateID = " & templateId)
            Else
                l.Success = False
            End If
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)> Public Shared Function RunStepWise(templateId As Integer, fieldId As String) As ActionResult(Of MRAResult)
        Dim l As New ActionResult(Of MRAResult)
        Dim RegressStatus As Integer
        Try
            Dim compiledSql = Database.Tenant.GetStringValue("EXEC MRA_CompileSelectedFields {0}, {1}".SqlFormat(True, templateId, fieldId))

            If compiledSql IsNot Nothing Then
                Dim dt = Database.Tenant.GetDataTable(compiledSql)
                RegressStatus = RegressionAnalyzer.EvaluateMultipleRegression(templateId, dt, True, True, fieldId)
                Select Case RegressStatus
                    Case 1
                        l.Success = True
                        Dim li As New MRAResult
                        Dim dat = Database.Tenant.GetDataTable("SELECT FieldID, tStat, pValue FROM MRA_StepWiseCoefficients WHERE templateId = {0} AND FieldID != -1".SqlFormat(True, templateId))
                        li.FieldData = dat.ConvertToList(Of MRACoefficients)
                        li.Fields = fieldId
                        l.Data = li
                    Case 0
                        l.Success = False
                        l.ErrorMessage = "No Template."
                    Case -1
                        l.Success = False
                        l.ErrorMessage = "Not enough data points to perform StepWise regression"
                    Case -2
                        l.Success = False
                        l.ErrorMessage = "Not enough Data to run StepWise regression"
                    Case -3
                        l.Success = False
                        l.ErrorMessage = "Data is not fit to run  StepWise regression"
                End Select
                l.Count = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM MRA_ResidualOutput WHERE TemplateID = " & templateId)
            Else
                l.Success = False
            End If
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)> Public Shared Function CreateNewTemplate(datasetId As Integer) As ActionResult(Of Integer)
        Dim l As New ActionResult(Of Integer)
        Try
            Dim sequenceNumver As Integer = Database.Tenant.GetIntegerValue("SELECT ISNULL(MAX(Sequence), 0) + 1 FROM MRA_Templates")
            Dim strDataSetId As String = If(datasetId > 0, datasetId.ToString, "")
            Dim newId = Database.Tenant.GetIntegerValue("INSERT INTO MRA_Templates (Name, DataSetID, Sequence) VALUES ({0}, {1}, {2});SELECT CAST(@@IDENTITY AS INT) As NewID;".SqlFormat(True, "RegressionModel" & sequenceNumver, strDataSetId, sequenceNumver))
            l.Data = newId
            l.Success = True

        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        Return l
    End Function


    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)> Public Shared Function CopyTemplate(templateId As Integer) As ActionResult(Of Integer)
        Dim l As New ActionResult(Of Integer)
        Try
            Dim TemplateName As String = Database.Tenant.GetStringValue("SELECT Name FROM MRA_Templates where id = {0}".SqlFormat(True, templateId))
            Dim CopyTempName As String = TemplateName & "_COPY"
            Dim CopySequence As Integer = Database.Tenant.GetIntegerValue("SELECT ISNULL(MAX(REPLACE(REPLACE(NAME,{1},''),'_COPY','')), 0)  FROM MRA_Templates where Name like {0}".SqlFormat(False, CopyTempName + "%", TemplateName))
            CopyTempName = CopyTempName & CopySequence + 1
            Dim InsertQuery As String = "INSERT INTO MRA_Templates (Name,InputFields,Filters,PoolSize,LastExecutionTime,CostFormula,CostFormulaSQL,Comments,Groups,DataSetID,OutputField,OutputFieldLabel,OutputAggregate,OutputFilter,OutputFilterOperator,OutputFilterValue,OutputFilterValue2,CompiledInputsSQL,Sequence,NoIntercept,OutputFieldExpression)"
            Dim SelectQuery As String = " SELECT {0},InputFields,Filters,PoolSize,LastExecutionTime,CostFormula,CostFormulaSQL,Comments,Groups,DataSetID,OutputField,OutputFieldLabel,OutputAggregate,OutputFilter,OutputFilterOperator,OutputFilterValue,OutputFilterValue2,CompiledInputsSQL,Sequence,NoIntercept,OutputFieldExpression".SqlFormat(True, CopyTempName)
            Dim WhereQuery As String = " from MRA_Templates where id = {0}".SqlFormat(True, templateId)
            Dim newId = Database.Tenant.GetIntegerValue(InsertQuery + SelectQuery + WhereQuery + " SELECT CAST(@@IDENTITY AS INT) As NewID;")
            InsertQuery = "INSERT INTO MRA_TemplateFields (TemplateID,IsFilter,FieldID,Label,Aggregate,Expression,FilterOperator,FilterValue,FilterValue2,Ordinal,LookupTable)"
            SelectQuery = " SELECT {0},IsFilter,FieldID,Label,Aggregate,Expression,FilterOperator,FilterValue,FilterValue2,Ordinal,LookupTable from MRA_TemplateFields where TemplateId = {1}".SqlFormat(True, newId, templateId)
            Database.Tenant.Execute(InsertQuery + SelectQuery)
            InsertQuery = "Insert into MRA_TemplateParcels (TemplateID,ParcelID,TargetOutput) Select {0},ParcelID,TargetOutput from MRA_TemplateParcels where TemplateId = {1}".SqlFormat(True, newId, templateId)
            Database.Tenant.Execute(InsertQuery)
            InsertQuery = "INSERT INTO MRA_LookupValues (TemplateId, TemplateFieldID, Value, NumericValue) Select {0}, TemplateFieldID, Value, NumericValue from MRA_LookupValues where TemplateId = {1}".SqlFormat(True, newId, templateId)
            Database.Tenant.Execute(InsertQuery)
            InsertQuery = "INSERT INTO MRA_LookupValueResults (TemplateId, TemplateFieldID, ParcelID, Value) Select {0}, TemplateFieldID, ParcelID, Value from MRA_LookupValueResults where TemplateId = {1}".SqlFormat(True, newId, templateId)
            Database.Tenant.Execute(InsertQuery)
            l.Data = newId
            l.Success = True

        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)> Public Shared Function GetLookupValues(LookupName As String, TemplateID As Integer, FieldId As Integer) As ActionResult(Of List(Of MRALookupValue))
        Dim l As New ActionResult(Of List(Of MRALookupValue))
        Try
            Dim dt = Database.Tenant.GetDataTable("SELECT lk.IdValue As Id, lk.NameValue As Name,CAST(CASE WHEN l.ID IS NULL THEN 0 ELSE 1 END AS BIT) As Selected FROM ParcelDataLookup lk INNER JOIN MRA_Templates t on t.ID = {0} AND lk.LookupName = {1} LEFT OUTER JOIN MRA_LookupValues l ON l.TemplateId = t.ID AND l.TemplateFieldID = {2} and l.Value = lk.IdValue ORDER BY  Name".SqlFormat(True, TemplateID, LookupName, FieldId))
            l.Data = dt.ToRows.Select(Function(x) New MRALookupValue With {.ID = x.GetString("Id"), .Name = x.GetString("Name"), .Selected = x.GetBoolean("Selected")}).ToList
            l.Success = True
            l.Count = l.Data.Count
            l.FullCount = l.Count
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)> Public Shared Function GenerateLookup(fieldId As Integer) As ActionResult(Of String)
        Dim l As New ActionResult(Of String)
        Try
            Dim lookup = Database.Tenant.GetTopRow("EXEC MRA_GenerateLookup " & fieldId)
            l.Success = lookup.GetBoolean("Success")
            If l.Success Then
                l.Data = lookup.GetString("LookupName")
            Else
                l.ErrorMessage = lookup.GetString("ErrorMessage")
            End If
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        Return l
    End Function

    Public Sub Page_PageCommand(commandName As String) Handles Me.RunCommand
        Dim templateId As Integer = 0
        If Integer.TryParse(Request("templateId"), templateId) Then
            Dim template As System.Data.DataRow = Database.Tenant.GetTopRow("EXEC [MRA_GetTemplate] " & templateId)
            Dim reportTitle As String = "Data Export", fileName As String = "report", templateName As String = template.GetString("Name"), outputLabel As String = template.GetString("DisplayLabel")

            Dim outputField As String = template.GetString("OutputFieldName")
            Dim timeSignature = Now.ToString("yyyyMMdd") + "_" + Now.ToString("HHmm")
            Dim exportGrid As New GridView
            Page.Controls.Add(exportGrid)

            If commandName = "exportdata" Then

            ElseIf commandName = "exportinput" Then
                reportTitle = templateName + " - Sample Data - " + outputLabel
                fileName = {templateName, outputField, "samples", timeSignature}.Aggregate(Function(x, y) x + "_" + y)
                Dim data = RegressionAnalyzer.GetReportsData(templateId, "input-data-export", exportGrid, False, 0, 0, False)
                exportGrid.ExportAsExcel(data, reportTitle, fileName, "sample-data")
            End If
            Page.Controls.Remove(exportGrid)
        End If
    End Sub


    Public Class MRAInputsChartData
        Public Property Labels As List(Of String)
        Public Property IOScatter As List(Of XY)
        Public Property InputStats As List(Of Integer)
        Public Property LookupValues As List(Of String)
        Public Property LookupLabels As List(Of String)
    End Class

    Public Class MRAProperties
        Public Property Template As MRATemplate
        Public Property InputStats As List(Of MRAInputStatistics)
    End Class

    Public Class MRAInputStatistics
        Public Property FieldId As Integer
        Public Property FieldName As String
        Public Property Count As Integer
        Public Property Minimum As Decimal
        Public Property Maximum As Decimal
        Public Property Range As Decimal
        Public Property Average As Decimal
        Public Property StandardDeviation As Double
    End Class
    Public Class MRALookupValue
        Public Property ID As String
        Public Property Name As String
        Public Property Selected As Boolean
    End Class
    Public Class MRAResult
        Public Property FieldData As List(Of MRACoefficients)
        Public Property Fields As String
    End Class
    Public Class MRACoefficients
        Public Property FieldId As Integer
        Public Property tStat As Double
        Public Property pValue As Double
    End Class

</script>

