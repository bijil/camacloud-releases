﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master" CodeBehind="Default.aspx.vb" Inherits="CAMACloud.Console._Default5" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style type="text/css">
        .license-filters
        {
            margin: 5px;
            margin-top: 15px;
            margin-bottom: 15px;
        }
        
        .license-filters a
        {
            padding-left: 15px;
        }
        
        .license-filters span
        {
        }
        
        .link-button
        {
            font-weight: bold;
            text-decoration: none;
            padding: 5px 15px;
            margin: 5px;
            margin-left: 0px;
            margin-right: 15px;
            background: #CFCFCF;
            border-radius: 8px;
            display: inline-block;
            cursor: pointer;
        }
        .link-button:hover
        {
            color: White;
            background: #065a9f;
        }
        
        .keys pre
        {
            margin: 0px !important;
            padding: 0px !important;
            font-weight: bold;
            font-family: Lucida Sans Typewriter;
        }
        
        .simple-form tr td
        {
            vertical-align: middle;
        }
        
        .mGrid td a
        {
            margin-right: 4px !important;
        }
        
        .sf
        {
            font-size: smaller;
        }
        .main-content-area
        {
           overflow-x: auto; 
        }

        .search-panel {
            padding: 7px;
            border: 1px solid #CFCFCF;
            width: 787px ;
            background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#CFCFCF), to(#F8F8F8));
        }

        .search-panel label {
             margin-right: 5px;

        }
    </style>
    <script type="text/javascript">
   		/* $(document).ready(function() {
 				$('.grid-selection input').on("change", function() {
 					if(!$(this).is(":checked")){ $(".checkall").prop('checked', false); } 
				});
 			});*/

        function checkSelection() {
            var totalChecked = 0;
            $('.grid-selection input').each(function () {
                if (!$(this).hasClass('dvCheck') && this.checked) 
                /* if (this.checked) */

                    totalChecked++;
            });
            if (totalChecked == 0) {
                alert('You have not selected any license row.');
                return false
            }
            return true;
        }

        function confirmMultiRevoke() {
            if (!checkSelection()) return false;
            return confirm("You are cancelling the selected installed device licenses and allowing it to be used again. Click OK to continue.")
        }


        var gridcheckselect = false; 

        function confirmEmail() {
            if (!checkSelection()) return false;
            var mailRegx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; 
            var _email = $('.txt-email').val();
            if ( _email == '' ) {
                alert('Please enter a valid email address to email the licenses.');
                return false
            }
            else if( !( mailRegx.test(String(_email).toLowerCase()) ) ) {
	            alert("Invalid Email!");
	            return false;
            }
            
            if ($('#checkAll')[0].checked) gridcheckselect = true;
            return true;
        }

       function runaftersave() {
            if (gridcheckselect) $('#checkAll')[0].checked = true;
           gridcheckselect = false;
           bindSearch();
        } 


        function copyToClipboard() {
            if (window.clipboardData) {

            }
        }
        
        function checkboxcheck() {
            var chk = $('.grid-selection input[type = checkbox]');
            var chkChecked = $('.grid-selection input[type = checkbox]:checked');
            if (chk.length == chkChecked.length) {
                $(".checkall").prop("checked", true);
            }
            else {
                $(".checkall").prop("checked", false);
            }

        }

        function bulkSelect(s) {
            $('.grid-selection input').each(function (i, x) { x.checked = s; });
        }

        function ValidateSearch() {
            var txt = $('#<%=txtSearch.ClientID%>').val();
            if (txt == null || txt.trim() === "" && ($('.textboxAuto').is(':focus') || $('.btnSearch').is(':focus'))) {
                alert("Please enter a search term.");
                return true;
            }
            else {
                $('.textboxAuto').val(txt).blur();
                return true;
            }
        }

        function shtextBox(resetval) {
            if (!resetval) {
                $('.textboxAuto').val('');
                $('.textdateAuto').val('');
            }

            if ($('.search-cond').val() == 5) {
                $('.textdateAuto').show();
                $('.textboxAuto').hide();
            }
            else {
                $('.textdateAuto').hide();
                $('.textboxAuto').show();
            }
        }

        function bindSearch() {
            $('.search-cond').unbind('change');
            $('.search-cond').bind('change', () => {
                shtextBox();
            });

            $('.textdateAuto').unbind('keydown');
            $('.textdateAuto').bind('keydown', function (e) {
                e.preventDefault();
            });

            $(document).keypress(function (e) {
                if (e.which === 13) { 
                    e.preventDefault(); return false;
                }
            });
        }

        $(document).ready(() => {
            bindSearch();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table width="100%">
        <tr>
            <td>
                <div style="width: 100%; float: left; height: 60px">
                <h1 runat="server" id="pageTitle">
                    License Management</h1>
                </div>
            </td>
        </tr>
    </table>
    <p class="info">
        Any client user can access the CAMA Cloud application and data only after validating
        their browser with a unique license key and one time password.</p>
    <asp:HiddenField runat="server" ID="hdnOrganizationName" />
    <asp:HiddenField runat="server" ID="hdnOrganizationId" />
    <asp:HiddenField runat="server" ID="hdnUseState" Value="0" />
    <div class="license-filters">
        <span>License Status:</span>
        <asp:LinkButton runat="server" ID="lbShowUnused" Text="Unused licenses" CommandArgument="0" Font-Bold="true" />
        <asp:LinkButton runat="server" ID="lbShowInUse" Text="Licenses in-use" CommandArgument="1" />
        <asp:LinkButton runat="server" ID="lbShowAll" Text="All" CommandArgument="" />
    </div>
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table style="width: 98%;" runat="server" id="tLicenseOps">
                <tr>
                    <td class="link-panel">
                        <asp:LinkButton runat="server" ID="lbRevokeSelected" Text="Revoke Selected" OnClientClick="return confirmMultiRevoke()" />
                    </td>
                    <td style="text-align: right;">
                        <asp:TextBox runat="server" ID="txtEmail" type="email" Width="200px" CssClass="txt-email" />
                        <asp:Button Text=" Email Licenses " runat="server" ID="btnEmail" OnClientClick="return confirmEmail();" />
                    </td>
                </tr>
            </table>
            <div class="search-panel">
                <table>
                    <tr>
                        <td>
                            <label>
                                Search By:</label>
                            <asp:DropDownList runat="server" ID="ddlFilter" CssClass="search-cond">
                                <asp:ListItem Text="Device" Value="1" />
                                <asp:ListItem Text="License Key" Value="2" />
                                <asp:ListItem Text="Registered Email" Value="3" />
                                <asp:ListItem Text="Nickname" Value="4" />
                                <asp:ListItem Text="Last Access" Value="5" />
                                <asp:ListItem Text="CAMA User" Value="6" />
                            </asp:DropDownList>

                        </td>
                        <td style="width: 10px;"></td>
                        <td>
                            <asp:TextBox runat="server" ID="txtSearch" Width="300px" MaxLength="30" CssClass="textboxAuto"/>
                            <asp:TextBox runat="server" ID="txtDate" TextMode="Date" Width="300px" MaxLength="30" style="display:none;" CssClass="textdateAuto"/>
                        </td>
                        <td style="width: 10px;">
                        </td>
                        <td>
                            <asp:Button runat="server" ID="btnSearch" Text=" Search " cssClass="btnSearch" />
                        </td>
                    </tr>
                </table>
            </div>
            <div ID="pageSize_div" style = "margin-top:20px">
    		 No. of items per page:
    			<asp:DropDownList runat="server" ID="ddlPageSize" AutoPostBack="true" width="50px">
			        <asp:ListItem Value="10" />
			        <asp:ListItem Value="25" />
			        <asp:ListItem Value="50" />
		         </asp:DropDownList>
    	    </div>
            <asp:GridView ID="gvLicense" runat="server"  AllowPaging="true" PageSize="10" Width = "98%">
                <Columns>
                    <asp:TemplateField>
                        <ItemStyle Width="18px" />
                        <ItemTemplate>
                            <asp:CheckBox ID="chkSel" runat="server" Checked="false" CssClass="grid-selection" onchange="checkboxcheck()" />
                            <asp:HiddenField ID="LID" runat="server" Value='<%# Eval("Id") %>' />
                            <asp:HiddenField ID="LKEY" runat="server" Value='<%# EvalLicenseKey() %>' />
                            <asp:HiddenField ID="OTP" runat="server" Value='<%# Eval("OTP") %>' />
                            <asp:HiddenField ID="DEVICE" runat="server" Value='<%# Eval("TargetDevice") %>' />
                        </ItemTemplate>
                        <HeaderTemplate>
                            <input type="checkbox" class="checkall dvCheck " onchange='bulkSelect(this.checked);' id="checkAll" />
                        </HeaderTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="TargetDevice" HeaderText="Device" ItemStyle-Width="120px" />
                    <asp:TemplateField HeaderText="License Key">
                        <ItemStyle CssClass="keys" Width="180px" />
                        <ItemTemplate>
                            <pre style=""><%# EvalLicenseKey() %></pre>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="OTP" HeaderText="OTP" ItemStyle-Width="60px" ItemStyle-Font-Bold="true" />
                    <asp:BoundField DataField="RegisteredEmail" HeaderText="Registered Email" ItemStyle-Width="250px" />
                    <asp:BoundField DataField="RegisteredNick" HeaderText="Nickname" ItemStyle-Width="150px" />
                     <asp:TemplateField>
                        <ItemStyle Width="60px" />
                        <ItemTemplate>                           
                            <asp:LinkButton runat="server" ID="lbRevoke" Text="Revoke" CommandName="RevokeLicense"
                                CommandArgument='<%# Eval("Id") %>' Visible='<%# Eval("InUse") %>' OnClientClick='return confirm("You are cancelling an installed device license and allowing it to be used again. Click OK to continue.")' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemStyle Width="18px" />
                        <ItemTemplate>
                            <asp:Label runat="server" CssClass="a16 lock16" Visible='<%# Eval("InUse") %>' ToolTip="License Key in use" />
                            <asp:Label runat="server" CssClass="a16 newborn16" Visible='<%# ((System.DateTime.UtcNow - CDate(Eval("CreatedDate"))).TotalSeconds < 30) And Not Eval("InUse") %>'
                                ToolTip="Newly created" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="LastUsedLoginId" HeaderText="CAMA User" ItemStyle-Width="90px"
                        ItemStyle-CssClass="sf" />
                    <asp:BoundField DataField="LastUsedIPAddress" HeaderText="IP Address" ItemStyle-Width="85px"
                        ItemStyle-CssClass="sf" />
                    <asp:TemplateField HeaderText="Last Access">
                        <ItemStyle CssClass="keys sf" Width="90px" />
                        <ItemTemplate>
                            <%# Eval("LastAccessTime", "{0:MM/dd/yyyy hh:mm tt}")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    <div class="info">
                        No licenses found.
                    </div>
                </EmptyDataTemplate>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div style="display: none;">
        <textarea id="clipboardAgent"></textarea>
    </div>
</asp:Content>
