﻿Imports System.Net.Mail
Imports CAMACloud.Security
Imports System.IO
Imports CAMACloud.Data
Public Class _Default5
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            bindOrganizations()
            RefreshLicenseGrid()
        End If
    End Sub
    Private Sub bindOrganizations()
        hdnOrganizationId.Value = Database.System.GetIntegerValueOrInvalid("SELECT ID FROM Organization WHERE Id = {0}".SqlFormatString(HttpContext.Current.GetCAMASession().OrganizationId))
        hdnOrganizationName.Value = Database.System.GetStringValue("SELECT Name FROM Organization WHERE Id= {0}".SqlFormatString(HttpContext.Current.GetCAMASession().OrganizationId))
    End Sub

    Function ConvertDateToFormat(dateString As String) As String
        Dim inputDate As DateTime
        If DateTime.TryParseExact(dateString, "yyyy-MM-dd", Nothing, Globalization.DateTimeStyles.None, inputDate) Then
            Return inputDate.ToString("yyyy-MM-dd")
        Else
            Return dateString
        End If
    End Function

    Private Sub RefreshLicenseGrid()
        If HttpContext.Current.GetCAMASession().OrganizationId <> -1 Then
            tLicenseOps.Visible = True
            gvLicense.Visible = True
        Else
            tLicenseOps.Visible = False
            gvLicense.Visible = False
        End If
        lbRevokeSelected.Visible = hdnUseState.Value <> "0"
        Dim OrganizationId As String = hdnOrganizationId.Value
        Dim inUse As String = IIf(hdnUseState.Value <> "", " AND InUse = " + hdnUseState.Value, "")

        Dim search As String = ""
        Dim textSearch As String = IIf(ddlFilter.SelectedValue = 5, txtDate.Text, txtSearch.Text)
        If textSearch.Trim <> "" Then
            textSearch = textSearch.Replace("%", "[%]").Trim().Replace("'", "''")
            Select Case ddlFilter.SelectedValue
                Case "1"
                    search = "DeviceFilter"
                    'textSearch = textSearch.Replace("MA Chrome", "MAChrome").Replace("Console", "Chrome").Replace("MA Common", "MACommon")
                Case "2"
                    textSearch = textSearch.Replace("-", "")
                    search = "LicenseKey"
                Case "3"
                    search = "RegisteredEmail"
                Case "4"
                    search = "RegisteredNick"
                Case "5"
                    search = "LastAccessTime"
                Case "6"
                    search = "LastUsedLoginId"
            End Select
            If ddlFilter.SelectedValue = 5 Then
                Dim formattedDate As String = ConvertDateToFormat(textSearch)
                search = " AND CONVERT(DATE," + search + ") = '" + formattedDate + "'"
            ElseIf ddlFilter.SelectedValue = 1 Then
                search = " AND ((" + search + " = 'Chrome' AND ('Console' LIKE '%" + textSearch + "%')) OR
                         (" + search + " = 'MAChrome' AND ('MA Chrome' LIKE '%" + textSearch + "%')) OR
                         (" + search + " = 'MACommon' AND ('MA Common' LIKE '%" + textSearch + "%')) OR
                         (" + search + " IN ('SketchPro', 'iPad') AND (" + search + " LIKE '%" + textSearch + "%')))"
            Else
                search = " And " + search + " Like '%" + textSearch + "%'"
            End If

        End If

        Dim sql As String = String.Format("SELECT *, CASE DeviceFilter WHEN 'Chrome' THEN 'Console' WHEN 'MAChrome' THEN 'MA Chrome' WHEN 'MACommon' THEN 'MA Common' ELSE DeviceFilter END As TargetDevice  FROM DeviceLicense WHERE OrganizationId  = {0} AND IsSuper = 0 AND IsVendor = 0 " + inUse + search + " ORDER BY InUse DESC, CreatedDate", OrganizationId)
        gvLicense.DataSource = Database.System.GetDataTable(sql)
        gvLicense.DataBind()
        If gvLicense.Rows.Count > 0 Then
            tLicenseOps.Visible = True
        Else
            tLicenseOps.Visible = False
        End If
        RunScript("bindSearch();")
    End Sub
    Public Function EvalLicenseKey() As String
        Dim key As String = Eval("LicenseKey")
        If key.Length = 16 Then
            key = key.Substring(0, 4) + "-" + key.Substring(4, 4) + "-" + key.Substring(8, 4) + "-" + key.Substring(12, 4)
        End If
        Return key
    End Function
    Protected Sub gvLicense_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvLicense.RowCommand
        Select Case e.CommandName
            Case "RevokeLicense"
                RevokeLicenses(e.CommandArgument)
        End Select
    End Sub
    Protected Function GetSelectedIds() As String
        Dim selection As String = "0"
        For Each gr As GridViewRow In gvLicense.Rows
            If gr.GetChecked("chkSel") Then
                selection += "," + gr.GetHiddenValue("LID")
            End If
        Next
        Return selection
    End Function
    Protected Sub lbRevokeSelected_Click(sender As Object, e As System.EventArgs) Handles lbRevokeSelected.Click
        RevokeLicenses(GetSelectedIds)
    End Sub

    Sub RevokeLicenses(ids As String)
        Database.System.Execute("UPDATE DeviceLicense SET InUse = 0, MachineKey = NULL, SessionKey = NULL WHERE Id IN (" & ids & ")")
        RefreshLicenseGrid()
    End Sub

    Protected Sub btnEmail_Click(sender As Object, e As System.EventArgs) Handles btnEmail.Click
        CAMACloud.Security.DeviceLicense.SendLicenseEmail(Me, gvLicense, txtEmail.Text, hdnOrganizationName.Value, False)
        RunScript("runaftersave();")
    End Sub
    Private Sub lbShowAll_Click(sender As Object, e As System.EventArgs) Handles lbShowAll.Click, lbShowInUse.Click, lbShowUnused.Click
        Dim lb As LinkButton = sender
        lbShowAll.Font.Bold = False
        lbShowInUse.Font.Bold = False
        lbShowUnused.Font.Bold = False
        lb.Font.Bold = True
        hdnUseState.Value = lb.CommandArgument
        ddlPageSize.SelectedValue = 10
        gvLicense.PageSize = ddlPageSize.SelectedValue
        gvLicense.PageIndex = 0
        ddlFilter.SelectedValue = 1
        txtSearch.Text = ""
        txtDate.Text = ""
        RefreshLicenseGrid()
        RunScript("shtextBox();")
    End Sub


    Protected Sub gvLicense_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvLicense.PageIndexChanging
        gvLicense.PageIndex = e.NewPageIndex
        RefreshLicenseGrid()
    End Sub
    Protected Sub ddlPageSize_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
        gvLicense.PageSize = ddlPageSize.SelectedValue
        gvLicense.PageIndex = 0
        RefreshLicenseGrid()
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        RefreshLicenseGrid()
        RunScript("shtextBox(1);")
    End Sub
End Class