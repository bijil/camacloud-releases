﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Default.aspx.vb" Inherits="CAMACloud.Console._Default10" %>


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>WMS Nearmap</title>
    <link rel="stylesheet" href="/App_Static/css/openlayers.css?t=1" />
    <script type="text/javascript" src="/App_Static/jslib/openlayers.js?t=1"></script>
    <style>
        body {
            font-family: 'Oswald', sans-serif;
            margin:0px;
        }

        #map {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }

        .heading-btn {
            position: absolute;
            top: 130px;
            justify-content: space-around;
            border-radius: 10%;
        }

            .heading-btn .center {
                display: flex;
                justify-content: center;
            }

            .heading-btn button {
                margin: 10px;
            }

        .optionDiv {
            position: absolute;
            right: 10px;
            height: 40px;
            display: flex;
            background-color: rgba(0,60,136,.5);
            justify-content: space-around;
            padding: 5px;
            border-radius: 8px;
            color: #fff;
            -webkit-box-shadow: -2px 1px 20px -3px rgba(0,0,0,0.75);
            -moz-box-shadow: -2px 1px 20px -3px rgba(0,0,0,0.75);
            box-shadow: -2px 1px 20px -3px rgba(0,0,0,0.75);
            font-weight: 400;
            border: 3px solid;
            border-color: rgba(255,255,255,.5);
            font-size: 14px;
            margin-top:8px;
        }

        .survey-picker > div {
            margin-right: 5px;
            margin-left: 5px;
            margin-bottom: 5px;
            margin-top:8px;
        }

        .survey-picker select {
            display: block;
            border-radius: 5px;
        }

        .directionButton:focus {
            background: #555;
        }

        .custom-select {
            position: relative;
            display: flex;
            flex-direction: column;
            border-width: 0 2px 0 2px;
            border-style: solid;
            border-color: #394a6d;
            margin: 3px;
        }

        .ol-tooltip {
            position: relative;
            background: rgba(0, 0, 0, 0.5);
            border-radius: 4px;
            color: white;
            padding: 4px 8px;
            opacity: 0.7;
            white-space: nowrap;
            font-size: 12px;
            cursor: default;
            user-select: none;
        }

        .ol-tooltip-measure {
            opacity: 1;
            font-weight: bold;
        }

        .ol-tooltip-static {
            background-color: #e6e6e6;
            color: black;
            border: 1px solid white;
        }

            .ol-tooltip-measure:before,
            .ol-tooltip-static:before {
                border-top: 6px solid rgba(0, 0, 0, 0.5);
                border-right: 6px solid transparent;
                border-left: 6px solid transparent;
                content: "";
                position: absolute;
                bottom: -6px;
                margin-left: -7px;
                left: 50%;
            }

            .ol-tooltip-static:before {
                border-top-color: #ffcc33;
            }

        .noselect {
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        input[type=radio] {
            position: absolute;
            visibility: hidden;
            display: none;
        }

        label {
            color: white;
            display: inline-block;
            cursor: pointer;
            padding: 0 15px;
            line-height: 1.5;
            height: 100%;
        }

            label[for="option-one"] {
                border-bottom-left-radius: 4px;
                border-top-left-radius: 4px;
            }

            label[for="option-three"] {
                border-bottom-right-radius: 4px;
                border-top-right-radius: 4px;
            }

        input[type=radio]:checked + label {
            color: black;
            background: white;
        }

        label + input[type=radio] + label {
            border-left: solid 1px white;
        }

        .radio-group {
            border: solid 1px white;
            text-align: center;
            border-radius: 4px;
            display: flex;
        }

        .button {
            background-color: #f44336; /* Green */
            border: none;
            color: white;
            padding: 5px 5px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 12px;
            margin-left: 8px;
            border-radius: 4px;
            height: 22px;
            cursor: pointer;
        }

        .tooltip {
            position: relative;
            display: inline-block;
            background-image: url("/App_Static/images/info-2.png");
            width: 15px;
            height: 13px;
            z-index: 2;
            background-position: center;
            background-repeat: no-repeat;
            background-size: contain;
            opacity: 0.5;
        }

        .tooltip:hover {
                opacity: 1;
            }

        .tooltiptext {
            visibility: hidden;
            right: 166px;
            width: 350px;
            background-color: rgba(255,255,255,.95);
            color: black;
            padding: 5px 0;
            border-radius: 8px;
            top: 30px;
            position: absolute;
            z-index: 2;
            padding: 15px;
            font-size: 13px;
            margin: 0px;
            line-height: 1.75;
        }

        .tooltiptext h4 {
            margin: 2px 0 6px;
        }

        .tooltiptext > ul {
            list-style-type: decimal;
            padding: 0px 0 0 15px;
            margin: 0;
        }
        .avoidclicks {
  			pointer-events: none;
		}
		.lds-ripple {
		  display: inline-block;
		  position: absolute;
		  top:40%;
		  width: 80px;
		  height: 80px;
		}
		.lds-ripple div {
		  position: absolute;
		  border: 4px solid #fff;
		  opacity: 1;
		  border-radius: 50%;
		  animation: lds-ripple 1s cubic-bezier(0, 0.2, 0.8, 1) infinite;
		}
		.lds-ripple div:nth-child(2) {
		  animation-delay: -0.5s;
		}
		@keyframes lds-ripple {
		  0% {
		    top: 36px;
		    left: 36px;
		    width: 0;
		    height: 0;
		    opacity: 1;
		  }
		  100% {
		    top: 0px;
		    left: 0px;
		    width: 72px;
		    height: 72px;
		    opacity: 0;
		  }
		}
		#mask{
			position: fixed;
		    left: 0px;
		    top: 0px;
		    width: 100%;
		    height: 100%;
		    z-index:-10;
			background-color:rgba(107, 107, 107, .5);
			display: flex;
		  	justify-content: center;
			
		}
    </style>
</head>
<body>
    <div id="map" style="height: 700px;"></div>
    <div class="heading-btn">
        <div class="center">
            <input type="image" title="North" src="/App_Static/images/up.png" id="northElementId" value="North" class="directionButton"></input>
        </div>
        <div style="padding: 5px; height: 32px;">
            <input type="image" title="West" src="/App_Static/images/left.png" id="westElementId" value="West" class="directionButton"></input>
            <input type="image" title="Vertical" src="/App_Static/images/vert.png" id="vertElementId" alt="Vertical" value="Vert" class="directionButton"></input>
            <input type="image" title="East" src="/App_Static/images/right.png" id="eastElementId" value="East" class="directionButton"></input>
        </div>
        <div class="center">
            <input type="image" title="South" src="/App_Static/images/down.png" id="southElementId" class="directionButton" value="South"></input>
        </div>
    </div>
    <div class="help tooltiptext">
        <h4>Steps for Measuring</h4>
        <ul>
            <li>To start a Measurement, Choose a tool(Length/Area).</li>
            <li>Click on the Map to start the drawing.</li>
            <li>Double Click to end the drawing.</li>
            <li>If the tool is Area, Triple click to end the drawing if the polygon has only two points.</li>
            <li>The Clear option will clear all the drawings and reset the Map.</li>
            <li>Changing the projection will also clear all the drawings.</li>

        </ul>
    </div>

    <div class="optionDiv noselect measureTools" style="right: 150px; flex-direction: column">
        <div style="display: flex; justify-content: space-between; padding-left: 3px"><span>Measure</span><span class="tooltip opacity-group">&nbsp;</span></div>
        <div class='measure-tool'>
            <div style="display: flex; flex-direction: row; justify-content: center;">
                <div class="radio-group opacity-group">
                    <input type="radio" id="option-one" name="measure-selector" checked="checked" value="none" />
                    <label for="option-one">None</label>
                    <input type="radio" id="option-two" name="measure-selector" value="length" />
                    <label for="option-two">Length</label>
                    <input type="radio" id="option-three" name="measure-selector" value="area" />
                    <label for="option-three">Area</label>
                </div>
                <button class="clearAll opacity-group button" id="clearAll" onclick="showMask();">Clear</button>
                <button class="drawUndo button" id="drawUndo" style="display: none;" >Undo</button>
            </div>
        </div>
    </div>
    <div class="optionDiv noselect">
        <div>
            <span>Available surveys</span>
            <select class='custom-select' style="min-width: 110px; margin-left: 0px"></select>
        </div>
        <div style="display: none;">
            <span>Selected</span>
            <div id="selectedSurveyElementId"></div>
        </div>
        <div style="display: none;">
            <span>Displayed</span>
            <div id="displayedSurveyElementId"></div>
        </div>
    </div>
    <div id='mask'>
    	<div class="lds-ripple">
		    <div></div>
		    <div></div>
    	</div>
    </div>
    <script type="text/javascript">
        var nearmapWMSApiKey = '<%= APIKey%>';
        var CENTER = [-82.6367814985797, 41.4111097499755];
        var flag = false;
        var measureControls;
        var MIN_ZOOM = 12;
        var MAX_ZOOM = 24;
        var activeParcel = {};
        var shapes = [];
        var ZOOM = 20;
        var draw;
        var helpTooltip;
        var helpTooltipElement;
        var measureTooltipElement;
        var measureTooltip;
        var typeSelect = document.getElementsByName('measure-selector');
        var geoJSON;

        function urlTemplate(z, x, y, survey, layer) {
            var until = '';
            if (survey) {
                until = '&until=' + survey;
            }
            return 'https://api.nearmap.com/tiles/v3/' +
                layer + '/' + z + '/' + x + '/' + y +
                '.img?tertiary=satellite&apikey=' + nearmapWMSApiKey + until;
        };

        function wmsUrlTemplate() {
            return 'https://api.nearmap.com/wms/v1/latest/' +
                '?apikey=' + nearmapWMSApiKey +
                '&limit=1000';
        } 

        function degreesToRadians(deg) {
            return deg * (Math.PI / 180);
        }

        function radiansToDegrees(rad) {
            return rad / (Math.PI / 180);
        }

        function modulus360(deg) {
            if (deg < 0) {
                return 360 + (deg % 360);
            }

            return deg % 360;
        }


        var VERT = 0;
        var NORTH = 0;
        var EAST = 90;
        var SOUTH = 180;
        var WEST = 270;
        var HEADINGS = {
            Vert: VERT,
            North: NORTH,
            East: EAST,
            South: SOUTH,
            West: WEST
        };

        function addProj(code, worldWidth, worldHeight) {
            var projection = new ol.proj.Projection({
                code: code,
                // The extent is used to determine zoom level 0. Recommended values for a
                // projection's validity extent can be found at https://epsg.io/.
                extent: [0, 0, worldWidth, worldHeight],
                worldExtent: [-180, -85, 180, 85],
                // 'degrees', 'ft', 'm', 'pixels', 'tile-pixels' or 'us-ft'
                units: 'pixels'
            });

            ol.proj.addProjection(projection);

            var cx = worldWidth / 2;
            var cy = worldHeight / 2;

            var pixelsPerLonDegree = worldWidth / 360;
            var pixelsPerLatRadian = worldHeight / (2 * Math.PI);

            ol.proj.addCoordinateTransforms(
                'EPSG:4326',
                projection,
                function (coord) {
                    var lng = coord[0];
                    var lat = coord[1];

                    /**
                     *  Mercator Projection
                     *  https://en.wikipedia.org/wiki/Mercator_projection
                     *
                     *   x = longtitude * PI / 180
                     *
                     *   y = ln(tan( PI/4 + θ/2)) (NOTES: θ = latitude * PI / 180)
                     *     = ln(tan(θ) + sec(θ))
                     */

                    var x = cx + lng * pixelsPerLonDegree;

                    var theta = degreesToRadians(lat);
                    var tanTheta = Math.tan(theta);
                    var secTheta = 1 / Math.cos(theta);
                    var latRadians = Math.log(tanTheta + secTheta);
                    var y = cy + latRadians * pixelsPerLatRadian;

                    return [x, y];
                },
                function (coord) {
                    var x = coord[0];
                    var y = coord[1];

                    var lng = (x - cx) / pixelsPerLonDegree;
                    var latRadians = (y - cy) / -pixelsPerLatRadian;
                    var lat = -radiansToDegrees(2 * Math.atan(Math.exp(latRadians)) - Math.PI / 2);

                    return [lng, lat];
                }
            );
        };

        addProj('NMV:000', 256, 256);
        addProj('NMO:NS', 256, 192);
        addProj('NMO:EW', 192, 256);

        var ProjLatLng = ol.proj.get('EPSG:4326');
        var ProjVertical = ol.proj.get('NMV:000');
        var ProjObliqueNS = ol.proj.get('NMO:NS');
        var ProjObliqueEW = ol.proj.get('NMO:EW');

        var PROJECTIONS = {
            North: ProjObliqueNS,
            East: ProjObliqueEW,
            South: ProjObliqueNS,
            West: ProjObliqueEW,
            Vert: ProjVertical
        };


        function fetchImageData(url) {
            return fetch(url, { redirect: 'manual' })
                .then(function (resp) {
                    if (resp.status === 200) {
                        return resp.blob()
                            .then(function (data) {
                                return window.URL.createObjectURL(data);
                            });
                    }
                    else if (!resp.ok) {
                        if (flag == false) {
                            var imag = layerType == 'Vert' ? 'Vertical' : layerType;
                            alert('WMS Nearmap ' + imag + ' imagery is not available for the selected survey.');
                            flag = true;
                        }
                    }

                    // Handle other http status cases here
                    return null;
                })
                .catch(function (reason) {
                    // Handle exceptions here
                    return null;
                });
        }

        function loadImage(data) {
            return new Promise(function (resolve) {
                var img = document.createElement('img');
                img.addEventListener('load', function () { resolve(img); });
                // Falling back to an empty image in case of any errors
                img.addEventListener('error', function () { resolve(img); });

                // Assign image data
                img.src = data;
            });
        }

        function rotateImage(ctx, img, tileWidth, tileHeight, heading) {
            var rotation = degreesToRadians(heading);

            ctx.save();

            ctx.translate(tileWidth / 2, tileHeight / 2);
            ctx.rotate(rotation);

            switch (heading) {
                case NORTH:
                case SOUTH:
                    ctx.drawImage(img, -tileWidth / 2, -tileHeight / 2, tileWidth, tileHeight);
                    break;
                case EAST:
                case WEST:
                    ctx.drawImage(img, -tileHeight / 2, -tileWidth / 2, tileHeight, tileWidth);
                    break;
            }

            ctx.restore();
        }

        function createCanvas(width, height) {
            var canvas = document.createElement('canvas');
            canvas.width = width;
            canvas.height = height;

            return [canvas, canvas.getContext('2d')];
        }

        function rotateTile(data, tileDims, heading) {
            var tileWidth = tileDims[0];
            var tileHeight = tileDims[1];
            var canvasAndCtx = createCanvas(tileWidth, tileHeight);
            var canvas = canvasAndCtx[0];
            var ctx = canvasAndCtx[1];

            return loadImage(data)
                .then(function (img) {
                    rotateImage(ctx, img, tileWidth, tileHeight, heading);

                    return canvas.toDataURL();
                });
        }

        var olMap = null;
        var layerType = 'Vert';
        var availableSurveys = [];
        var dropdownElement = null;
        var selectedDisplayElement = null;
        var displayedDisplayElement = null;
        var northElement = null;
        var westElement = null;
        var southElement = null;
        var eastElement = null;
        var vertElement = null;

        var selectedSurvey = {
            survey: null,
            get value() {
                return this.survey;
            },
            set value(value) {
                this.survey = value;
                selectedDisplayElement.innerHTML = value;
            }
        };

        var displayedSurvey = {
            survey: null,
            get value() {
                return this.survey;
            },
            set value(value) {
                this.survey = value;
                displayedDisplayElement.innerHTML = value;
            }
        };

        var TILE_SIZES = {
            North: [256, 192],
            East: [192, 256],
            South: [256, 192],
            West: [192, 256],
            Vertical: [256, 256]
        };

        function toViewRotation(heading) {
            return degreesToRadians(modulus360(360 - heading));
        }

        /**
         * Provides Nearmap WMS tile URL whenever for selected survey date and layer type
         */
        function tileUrlFunction(tileCoord) {
            var z = tileCoord[0];
            var x = tileCoord[1];
            var y = tileCoord[2];

            return urlTemplate(z, x, y, selectedSurvey.value, layerType);
        }

        /**
         * Provides Nearmap WMS tile rotation mechanism
         */
        function tileLoadFunction(imageTile, src) {
            var img = imageTile.getImage();

            fetchImageData(src)
                .then(function (imgData) {
                    if (imgData && layerType !== 'Vert') {
                        rotateTile(imgData, TILE_SIZES[layerType], HEADINGS[layerType])
                            .then(function (rotatedImgData) {
                                img.src = rotatedImgData || '';
                            });
                    }

                    img.src = imgData || '';
                });
        }

        /**
         * Create Openlayers projection with coresponding rotation and projection
         */
        function createView(zoom, center) {
            zoom = zoom || ZOOM;
            center = center || CENTER;

            return new ol.View({
                projection: PROJECTIONS[layerType],
                rotation: toViewRotation(HEADINGS[layerType]),
                center: ol.proj.fromLonLat(center, PROJECTIONS[layerType]),
                minZoom: MIN_ZOOM,
                maxZoom: MAX_ZOOM,
                zoom: zoom
            });
        }

        /**
         * Create Openlayers tileLayer with coresponding tileSize and projection
         */
        function createLayer() {
            return new ol.layer.Tile({
                source: new ol.source.XYZ({
                    projection: PROJECTIONS[layerType],
                    tileSize: TILE_SIZES[layerType],
                    tileUrlFunction: tileUrlFunction,
                    tileLoadFunction: tileLoadFunction
                })
            });
        }

        /**
         * Called when the selected survey date does not exist in the current list of available survey dates.
         * It returns the closest available date.
         *
         * @param {*} surveys            available surveys returned by the coverage API
         * @param {*} selectedDate       user's selected survey date
         */
        function findClosestDate(surveys, selectedDate) {
            var selectedDateInMs = selectedDate ? +new Date(selectedDate) : +new Date();
            var deltaInMs = surveys.map(function (survey) {
                var surveyDateInMs = +new Date(survey.captureDate);
                return Math.abs(selectedDateInMs - surveyDateInMs);
            });

            var closestDateInMs = Math.min.apply(null, deltaInMs);
            return surveys[deltaInMs.findIndex(function (ms) { return ms === closestDateInMs; })].captureDate;
        };

        /**
         * @param {*} availableSurveys    available surveys returned by the coverage API
         * @param {*} selectedDate        user's selected survey date
         */
        function getSurveyDate(availableSurveys, selectedDate) {
            // No dates available
            if (availableSurveys.length === 0) {
                return null;
            }

            // Selects the selected survey date when available
            if (availableSurveys.find(function (survey) { return selectedDate === survey.captureDate; })) {
                return selectedDate;
            }

            // Searches for the closest available survey date when not available
            if (findClosestDate(availableSurveys, selectedDate)) {
                var value = selectedSurvey.value;
                selectedSurvey.value = findClosestDate(availableSurveys, selectedDate);
                if (value && value !== selectedSurvey.value)
                    refreshView();
            }
            return findClosestDate(availableSurveys, selectedDate);
        }

        /**
         * Fetches Nearmap wmsUrl API.
         */
        function fetchWMS() {
            /*var bounds = getBounds(olMap);*/
            var wmsUrl = wmsUrlTemplate();

            return fetch(wmsUrl)
                .then(function (response) {
                    return response.json();
                });
        }

        /**
         * Refreshes the map tiles whenever the selected survey date changes.
         */
        function refreshTiles() {
            flag = false;
            olMap
                .getLayers()
                .item(0)
                .getSource()
                .refresh();
        }

        /**
         * Refreshes the map tiles whenever projection changes.
         */
        function refreshView() {
        if (layerType == 'Vert') {
        		location.reload(true);
        		
                document.getElementsByClassName('measureTools')[0].style.display = "block";
                document.getElementById('option-one').checked = true;
            } else {
            var currentProject = olMap.getView().getProjection();
            var currentCenter = ol.proj.fromLonLat(CENTER, PROJECTIONS[layerType])
            var zoom = olMap.getView().getZoom();
            var center = ol.proj.toLonLat(currentCenter, currentProject);

            olMap.setView(createView(zoom, center));
            olMap.getLayers().clear();
            olMap.getLayers().push(createLayer());
            
                document.getElementsByClassName('measureTools')[0].style.display = "none";
                if (draw)
                    olMap.removeInteraction(draw);
                if (helpTooltip)
                    olMap.removeOverlay(helpTooltip);
            }
        }


        /**
         * Displays all available survey dates in a dropdown.
         */
        function updateDropDown() {
            // Clears up previous options
            dropdownElement.innerHTML = '';

            // Creates the content of options for select element
            availableSurveys.forEach(function (survey) {
                var optionElement = document.createElement('option');
                optionElement.setAttribute('value', survey.captureDate);
                optionElement.innerText = survey.captureDate;

                dropdownElement.add(optionElement);
            });

            // Assigns default select value
            dropdownElement.value = displayedSurvey.value;
        }

 
        function getAvailableSurveyDates(response) {
            var surveys = response && response.surveys ? response.surveys : [];

            return surveys.filter(function (survey) {
                return (survey.resources.tiles || [])
                    .some(function (tile) { return tile.type === layerType; });
            });
        }

        /**
         * onMapMoveHandler contains logics how to deal with coverage api while map is moving
         */
        function onMapMoveHandler() {
            // Fetches Nearmap coverage API based current view port
            fetchWMS()
                .then(function (response) {
                    // Updates internal `availableSurveys` and `displayedSurvey` members
                    availableSurveys = getAvailableSurveyDates(response);
                    displayedSurvey.value = getSurveyDate(availableSurveys, selectedSurvey.value);

                    // Updates available surveys dropdown options
                    updateDropDown();
                });
        }

        function initUiElements() {
            dropdownElement = document.querySelector('select');
            selectedDisplayElement = document.querySelector('#selectedSurveyElementId');
            displayedDisplayElement = document.querySelector('#displayedSurveyElementId');

            northElement = document.querySelector('#northElementId');
            westElement = document.querySelector('#westElementId');
            southElement = document.querySelector('#southElementId');
            eastElement = document.querySelector('#eastElementId');
            vertElement = document.querySelector('#vertElementId');
        }

        /**
         * Adds event listeners to map instance and UI elements.
         */
        function addEventListeners() {
            // Adds map moving (panning and zooming) listener
            olMap.on('moveend', function () {
                onMapMoveHandler(dropdownElement);
            });

            // Adds "onChange" listener to the dropdown
            dropdownElement.addEventListener('change', function (evt) {
                selectedSurvey.value = evt.target.value;
                displayedSurvey.value = evt.target.value;

                refreshTiles();
            });

            [northElement, westElement, southElement, eastElement, vertElement]
                .forEach(function (element) {
                    element.addEventListener('click', function (evt) {
                        layerType = evt.target.value;
                        flag = false;
                        refreshView();
                        refreshView();
                    });
                });
        }

        function initMap() {
            if (window.opener != null) {
                if (window.opener.activeParcel._mapPoints) {
                    activeParcel.MapPoints = window.opener.activeParcel._mapPoints;
                    CENTER = [window.opener.activeParcel.Center().lng(), window.opener.activeParcel.Center().lat()]
                }
                else {
                    activeParcel.MapPoints = window.opener.activeParcel.MapPoints;
                    CENTER = [window.opener.activeParcel.mapCenter[1], window.opener.activeParcel.mapCenter[0]]
                }

                if (window.opener.appType == 'sv' && (window.opener.prevNearmap == 0 || window.opener.prevNearmap == 1)) {
                    localStorage.setItem('nearmapWMSDisabled', window.opener.prevNearmap);
                    if (!window.onunload) {
                        window.onunload = function () {
                            if (!window.opener.isBeforeUnload)
                                localStorage.setItem('nearmapWMSDisabled', "1");
                        }
                    }
                }
            }
            var VectorSource = ol.source.Vector;
            var { Tile, Vector } = ol.layer; //import {Tile as TileLayer, Vector as VectorLayer} from 'ol/layer.js';
            var TileLayer = Tile;
            var VectorLayer = Vector;
            var { Circle, Fill, Stroke, Style } = ol.style;
            var GeoJSON = ol.format.GeoJSON;
            var MultiPoint = ol.geom.MultiPoint;
            var styles = [
                new Style({
                    stroke: new Stroke({
                        color: '#1ab7ea',
                        width: 3,
                    })
                })];
            if (activeParcel.MapPoints.length > 0) {
                for (x in activeParcel.MapPoints.sort(function (x, y) {
                    return x.Ordinal - y.Ordinal
                })) {
                    var p = activeParcel.MapPoints[x];
                    if (shapes[p.RecordNumber || 0] == null) {
                        shapes[p.RecordNumber || 0] = [];
                    }
                    shapes[p.RecordNumber || 0].push(ol.proj.transform([p.Longitude, p.Latitude], 'EPSG:4326', 'NMV:000'));
                };
            }
            var multiJSON = [];
            shapes.forEach((item) => {
                multiJSON.push({ "type": "Feature", "properties": {}, "geometry": { "type": "Polygon", "coordinates": [item] } })
            })
            geoJSON = new ol.layer.VectorImage({
                source: new ol.source.Vector({
                    features: new ol.format.GeoJSON().readFeatures({
                        "type": "FeatureCollection",
                        "features": multiJSON
                    }),
                }),
                style: styles,
                visible: true
            });

            olMap = new ol.Map({
                target: 'map',
                controls: [new ol.control.Zoom()],
                layers: [createLayer()],
                view: createView()
            });

            olMap.addLayer(geoJSON);

            initUiElements();
            addEventListeners();
            var { getArea, getLength } = ol.sphere;
            var { unByKey } = ol.Observable;
            var sketch;
            var Draw = ol.interaction.Draw;
            var Overlay = ol.Overlay;
            var View = ol.View;
            const CircleStyle = ol.style.Circle;
            const Icon = ol.style.Icon;
            const { LineString, Polygon } = ol.geom;
            const source = new VectorSource();
            const vector = new VectorLayer({
                source: source,
                view: new View({
                    projection: 'EPSG:4326',
                }),
                style: new Style({
                    fill: new Fill({
                        color: 'rgba(255, 255, 255, 0.2)',
                    }),
                    stroke: new Stroke({
                        color: '#ffcc33',
                        width: 2,
                    }),
                    image: new CircleStyle({
                        radius: 7,
                        fill: new Fill({
                            color: '#ffcc33',
                        }),
                    }),
                }),
            });
            olMap.addLayer(vector);

            const continuePolygonMsg = 'Click to continue drawing the polygon';
            const continueLineMsg = 'Click to continue drawing the line';
            const pointerMoveHandler = function (evt) {
                if (evt.dragging) {
                    return;
                }
                let helpMsg = 'Click to start drawing';
                if (sketch) {
                    const geom = sketch.getGeometry();
                    if (geom instanceof Polygon) {
                        helpMsg = continuePolygonMsg;
                    } else if (geom instanceof LineString) {
                        helpMsg = continueLineMsg;
                    }
                }
                helpTooltipElement.innerHTML = helpMsg;
                helpTooltip.setPosition(evt.coordinate);
                helpTooltipElement.classList.remove('hidden');
            };

            function getDistanceBetweenPoints(lat1, lng1, lat2, lng2) {
                // The radius of the planet earth in meters
                let R = 6378137;
                let dLat = degreesToRadians(lat2 - lat1);
                let dLong = degreesToRadians(lng2 - lng1);
                let a = Math.sin(dLat / 2)
                    *
                    Math.sin(dLat / 2)
                    +
                    Math.cos(degreesToRadians(lat1))
                    *
                    Math.cos(degreesToRadians(lat1))
                    *
                    Math.sin(dLong / 2)
                    *
                    Math.sin(dLong / 2);

                let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                let distance = R * c;

                return distance;
            }

            function degreesToRadians(degrees) {
                return degrees * Math.PI / 180;
            }

            var formatLength = function (line) {
                var length;
                var coordinates = line.getCoordinates();
                length = 0;
                var sourceProj = olMap.getView().getProjection();
                for (var i = 0, ii = coordinates.length - 1; i < ii; ++i) {
                    var c1 = ol.proj.transform(coordinates[i], sourceProj, 'EPSG:4326');
                    var c2 = ol.proj.transform(coordinates[i + 1], sourceProj, 'EPSG:4326');
                    length += getDistanceBetweenPoints(c1[1], c1[0], c2[1], c2[0]);
                }

                var output;
                if (length > 100) {
                    output = length / 1000
                        + ' km<br/>' + Math.round(length * 3.2808 * 1000) / 1000 + ' Ft';
                } else {
                    output = Math.round(length * 1000) / 1000
                        + ' M<br/>' + Math.round(length * 3.2808 * 1000) / 1000 + ' Ft';
                }
                return output;
            };

            const formatArea = function (polygon) {
                var op = { 'projection': 'EPSG:4326' };
                const area = getArea(polygon, op);
                let output;
                if (area > 10000) {
                    output = Math.round((area / 1000000) * 100) / 100 + ' ' + ' km<sup>2</sup><br/>' + Math.round(area * 100 * 10.764) / 100 + ' Sq.Ft';
                } else {

                    output = Math.round(area * 100) / 100 + ' ' + ' m<sup>2</sup><br/>' + Math.round(area * 100 * 10.764) / 100 + ' Sq.Ft';
                }
                return output;
            };

            let undoFunc = function () {
                if (document.getElementsByClassName('drawUndo')[0].classList.contains('avoidclicks')) {
                    return;
                }
                else {
                    if (draw?.sketchCoords_) {
                        if (draw.type_ == 'LineString') {
                            if (draw.sketchCoords_.length > 0) draw.removeLastPoint();
                            if (draw.sketchCoords_.length == 1) {
                                document.getElementsByClassName('ol-tooltip-measure')[0].style.display = 'none';
                                document.getElementsByClassName('drawUndo')[0].classList.add("avoidclicks");
                            }
                        }
                        else {
                            if (draw.sketchCoords_[0].length > 0) draw.removeLastPoint();
                            if (draw.sketchCoords_[0].length == 1) {
                                document.getElementsByClassName('ol-tooltip-measure')[0].style.display = 'none';
                                document.getElementsByClassName('drawUndo')[0].classList.add("avoidclicks");
                            }

                        }
                    }
                }
            }


            function addInteraction(typeOp) {
                olMap.on('pointermove', pointerMoveHandler);
                olMap.getViewport().addEventListener('mouseout', function () {
                    helpTooltipElement.classList.add('hidden');
                });
                const type = typeOp == 'area' ? 'Polygon' : 'LineString';
                draw = new Draw({
                    source: source,
                    type: type,
                    style: new Style({
                        fill: new Fill({
                            color: 'rgba(255, 255, 255, 0.2)',
                        }),
                        stroke: new Stroke({
                            color: 'rgba(0, 0, 0, 0.5)',
                            lineDash: [10, 10],
                            width: 2,
                        }),
                        image: new Icon({
                            anchor: [-1, 15],
                            anchorXUnits: 'pixels',
                            anchorYUnits: 'pixels',
                            src: '/App_Static/images/pencil.png',

                        }),
                    }),
                });
                olMap.addInteraction(draw);
                createMeasureTooltip();
                createHelpTooltip();

                let listener;
                
                draw.on('drawstart', function (evt) {
                    sketch = evt.feature;
                    let tooltipCoord = evt.coordinate;
                    let elements = document.getElementsByClassName('opacity-group');
                    for (var i = 0; i < elements.length; i++) {
                        elements[i].classList.add("avoidclicks");
                        elements[i].style.opacity = .3;
                    }
                    document.getElementsByClassName('drawUndo')[0].style.display = "inline-block";
                    document.getElementsByClassName('heading-btn')[0].classList.add("avoidclicks");
                    document.getElementsByClassName('heading-btn')[0].style.opacity = .3;
                    document.getElementsByClassName('drawUndo')[0].removeEventListener('click', undoFunc);
                    document.getElementsByClassName('drawUndo')[0].addEventListener('click', undoFunc);
                    if (document.getElementsByClassName('ol-tooltip-measure')[0] && document.getElementsByClassName('ol-tooltip-measure')[0].style.display == 'none') document.getElementsByClassName('ol-tooltip-measure')[0].style.display = '';
                    listener = sketch.getGeometry().on('change', function (evt) {
                        const geom = evt.target;
                        let output;
                        if (geom instanceof Polygon) {
                            output = formatArea(geom);
                            tooltipCoord = geom.getInteriorPoint().getCoordinates();
                        } else if (geom instanceof LineString) {
                            output = formatLength(geom);
                            tooltipCoord = geom.getLastCoordinate();
                        }
                        measureTooltipElement.innerHTML = output;
                        measureTooltip.setPosition(tooltipCoord);
                        if (draw?.sketchCoords_) {
                            if (draw.type_ == 'LineString' && draw.sketchCoords_.length > 1) {
                                if (document.getElementsByClassName('ol-tooltip-measure')[0] && document.getElementsByClassName('ol-tooltip-measure')[0].style.display == 'none') document.getElementsByClassName('ol-tooltip-measure')[0].style.display = '';
                                document.getElementsByClassName('drawUndo')[0].classList.remove("avoidclicks");
                            }
                            else if (draw.type_ == 'Polygon' && draw.sketchCoords_[0].length > 1) {
                                if (document.getElementsByClassName('ol-tooltip-measure')[0] && document.getElementsByClassName('ol-tooltip-measure')[0].style.display == 'none') document.getElementsByClassName('ol-tooltip-measure')[0].style.display = '';
                                document.getElementsByClassName('drawUndo')[0].classList.remove("avoidclicks")
                            }
                        }
                    });
                }, this);

                draw.on('drawend', function () {
                    measureTooltipElement.className = 'ol-tooltip ol-tooltip-static';
                    let elements = document.getElementsByClassName('opacity-group');
                    for (var i = 0; i < elements.length; i++) {
                        elements[i].classList.remove("avoidclicks");
                        elements[i].style.opacity = 1;
                    }
                    document.getElementsByClassName('drawUndo')[0].style.display = "none";
                    document.getElementsByClassName('heading-btn')[0].classList.remove("avoidclicks");
                    document.getElementsByClassName('heading-btn')[0].style.opacity = 1;
                    measureTooltip.setOffset([0, -15]);
                    sketch = null;
                    measureTooltipElement = null;
                    createMeasureTooltip();
                    unByKey(listener);
                }, this);
            }

            function createHelpTooltip() {
                if (helpTooltipElement) {
                    helpTooltipElement.parentNode.removeChild(helpTooltipElement);
                }
                helpTooltipElement = document.createElement('div');
                helpTooltipElement.className = 'ol-tooltip hidden';
                helpTooltip = new Overlay({
                    element: helpTooltipElement,
                    offset: [15, 0],
                    positioning: 'center-left',
                });
                olMap.addOverlay(helpTooltip);
            }

            function createMeasureTooltip() {
                if (measureTooltipElement) {
                    measureTooltipElement.parentNode.removeChild(measureTooltipElement);
                }
                measureTooltipElement = document.createElement('div');
                measureTooltipElement.className = 'ol-tooltip ol-tooltip-measure';
                measureTooltip = new Overlay({
                    element: measureTooltipElement,
                    offset: [0, -15],
                    positioning: 'bottom-center',
                    stopEvent: false,
                    insertFirst: false,
                });
                olMap.addOverlay(measureTooltip);
            }
            for (var i = 0; i < typeSelect.length; i++) {
                typeSelect[i].addEventListener('change', function () {
                    if (this.value == 'none') {
                        olMap.removeInteraction(draw);
                        olMap.removeOverlay(helpTooltip);
                    } else {
                        olMap.removeInteraction(draw);
                        addInteraction(this.value);
                    }
                });
            }
        }

        window.onload = function () {
            if (nearmapWMSApiKey != '') {
                document.getElementById('map').style.height = window.innerHeight + 'px';
                document.getElementById('map').style.width = window.innerWidth + 'px';
                initMap();
            } else {
                alert('WMS Nearmap is not enabled.');
                window.close();
            }
        }
        window.onresize = function () {
            document.getElementById('map').style.height = window.innerHeight + 'px';
            document.getElementById('map').style.width = window.innerWidth + 'px';
        }

        document.onclick = function (e) {
            var tooltipBtn = document.getElementsByClassName("tooltip")[0];
            var tooltipDiv = document.getElementsByClassName("tooltiptext")[0];

            if (e.target === tooltipBtn) {
                if (tooltipDiv.style.visibility != "visible") {
                    tooltipDiv.style.visibility = "visible";
                    tooltipBtn.style.opacity = 1;
                } else hideTooltip();
            } else {
                if (tooltipDiv.style.visibility == "visible" && !e.path.includes(tooltipDiv)) hideTooltip();
            }
        };

        function hideTooltip() {
            var tooltipBtn = document.getElementsByClassName("tooltip")[0];
            var tooltipDiv = document.getElementsByClassName("tooltiptext")[0];
            tooltipDiv.style.visibility = "hidden";
            tooltipBtn.style.opacity = .5;
        }
        function showMask(){
	    	document.getElementById('mask').style.zIndex='999';
	    	location.reload(true);
   		}
        
        function triggerClick() {
            location.reload();
        }
        
        document.addEventListener("keydown", function(event) {
            if (event.key === "Escape" || event.keyCode === 27) {
                setTimeout(triggerClick, 500);
            }
        });

    </script>
</body>
</html>
