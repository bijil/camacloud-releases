﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wmap.aspx.vb" Inherits="CAMACloud.Console.wmap" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>Woolpert Imagery</title>
    <meta charset="utf-8" />
   <link rel="stylesheet" href="/App_Static/css/openlayers.css?t=1" />
    <script type="text/javascript" src="/App_Static/jslib/openlayers.js?t=1"></script>
    <style> 
    html { height: 100% }
    body { height: 100%; margin: 0; overflow:hidden; }
    #map { width: 100%; height: 100%; overflow:hidden; }
    #imagery {
	  position: absolute;
	  right: 50px;
	  top: 50px;
	   z-index: 3;
	}
	.imagery .selection {
       margin-bottom: .5em;
    }

	.imagery .selection label {
	  display: inline-block;
	  width:5em;
	  height:1.1em;
	  background-color: rgba(0,60,136,.5);
	  border-radius: 3px;
	  color: #ffffff;
	  padding: 0.3em;
	  cursor: pointer;
	  border: 3px solid rgba(187,187,187,0.2);
	}
	
	.imagery .selection label:hover {
	  background-color: rgba(0,60,136,.7);
	  border: 3px solid rgba(187,187,187,0.8);
	}
	
	.imagery .selection input[type=radio] {
	  display: none;
	}
	
	.imagery .selection input[type=radio]:checked ~ label {
	  background-color: rgba(0,60,136,.9);
	  border: 3px solid rgba(187,187,187,1)
	  
	}
	</style>
</head>
<body>
    <div id="map" ></div>
  	
    <script>
    var activeParcel='';
	var CENTER='';
    if (window.opener != null) {
		    	if(window.opener.activeParcel._mapPoints){
		    		activeParcel= window.opener.activeParcel;
		    		activeParcel.MapPoints = window.opener.activeParcel._mapPoints;
		    		CENTER = [window.opener.activeParcel.Center().lng(), window.opener.activeParcel.Center().lat()]
		    	}
		    	else{
		    		activeParcel= window.opener.activeParcel;
		        	activeParcel.MapPoints = window.opener.activeParcel.MapPoints;
		        	CENTER = [window.opener.activeParcel.mapCenter[1], window.opener.activeParcel.mapCenter[0]]
		        }
		        
		        if ( window.opener.appType == 'sv' && ( window.opener.prevWoolpert == 0 ||  window.opener.prevWoolpert == 1 ) ) {
	            	localStorage.setItem('woolpertDisabled', window.opener.prevWoolpert);
	            	if ( !window.onunload ) {
	            		window.onunload = function() { 
	            			if(!window.opener.isBeforeUnload)
								localStorage.setItem('woolpertDisabled', "1");
						}  
	            	}
		        }
		    }
	var woolpertApikey = '<%= woolpertApikey%>';
	var woolpert6inchId = '<%= woolpert6inchId%>';
	var woolpert3inchId = '<%= woolpert3inchId%>';
	
	var shapes = [];
	//var woolpertApikey=window.opener.clientSettings['woolpertApikey'];
    var woolpert6url = 'https://raster.stream.woolpert.io/layers/'+woolpert6inchId+'/tiles/{z}/{x}/{y}?key='+woolpertApikey,
        woolpertAttrib = '&copy; ' + 'https://woolpert.com',
        woolpert3url ='https://raster.stream.woolpert.io/layers/'+woolpert3inchId+'/tiles/{z}/{x}/{y}?key='+woolpertApikey ;

	var {Circle ,Fill, Stroke, Style} = ol.style;
		var GeoJSON =  ol.format.GeoJSON;
		var styles = [
		  new Style({
		    stroke: new Stroke({
		     color: '#EEE01E',
		      width: 3,
		    })
		  })];
	
	var sixLayer= new ol.layer.Tile({
            source: new ol.source.XYZ({
              	url: 'https://raster.stream.woolpert.io/layers/'+ woolpert6inchId +'/tiles/{z}/{x}/{y}?key='+woolpertApikey
             
          }),
        
                visible : true
            });
    var threeLayer=  new ol.layer.Tile({
            source: new ol.source.XYZ({
              	url: 'https://raster.stream.woolpert.io/layers/'+ woolpert3inchId +'/tiles/{z}/{x}/{y}?key='+woolpertApikey
             
          }),
        
                visible : false
            });
	
  	//var CENTER = [activeParcel.mapCenter[1],activeParcel.mapCenter[0]];
	//var CENTER = [window.opener.activeParcel.Center().lng(), window.opener.activeParcel.Center().lat()]
   	
    const map = new ol.Map({
        view : new ol.View({
            //center : [-8912180.687388677, 3104482.73783103],
            center:ol.proj.transform(CENTER,  'EPSG:4326', 'EPSG:3857'),
            zoom : 19,
            maxZoom : 20,
            minZoom : 10
        }),
        layers : [

         sixLayer,threeLayer
        ],
        target : 'map'
    });
    if (activeParcel.MapPoints.length > 0) {
	        for (x in activeParcel.MapPoints.sort(function(x, y) {
	                return x.Ordinal - y.Ordinal
	            })) {
	            var p = activeParcel.MapPoints[x];
	            shapes.push(ol.proj.transform([ p.Longitude, p.Latitude],  'EPSG:4326', 'EPSG:3857'));
	        };
	    }
    
    	geoJSON = new ol.layer.VectorImage({
	        source : new ol.source.Vector({
	            features: new ol.format.GeoJSON().readFeatures({
                "type": "FeatureCollection",
                "features": [
                  {
                    "type": "Feature",
                    "properties": {},
                    "geometry": {
                      "type": "Polygon",
                      "coordinates": [
                        shapes
                      ]
                    }
                  }
                ]
              }),
	        }),
	        style : styles,
	        visible : true
	    });

 	map.addLayer(geoJSON);
   
</script>
<div class="imagery" id="imagery">
  <div class="selection">
    <input id="layertype1" name='layertype' type="radio" onchange="layerSelect();" checked>
    <label for="layertype1">6" Imagery</label>
  </div>
  <div class="selection">
    <input id="layertype2" name="layertype" onchange="layerSelect();" type="radio">
    <label for="layertype2">3" Imagery</label>
  </div>
</div>
<script>
function layerSelect(){
 var threeInch= document.getElementById('layertype2').checked;
 if(threeInch){
 	sixLayer.setVisible(false);
  	threeLayer.setVisible(true);
 }
 else
 {
 	sixLayer.setVisible(true);
  	threeLayer.setVisible(false);
  	}
   }

</script>
</body>
</html>
