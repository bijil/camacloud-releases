﻿Public Class NewEagleView
    Inherits System.Web.UI.Page
    Public EVAPIUrl As String
    Public EVClientID As String
    Public EVClientSecret As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        EVAPIUrl = ClientSettings.PropertyValue("NewEV.APIUrl")
        EVClientID = ClientSettings.PropertyValue("NewEV.ClientID")
        EVClientSecret = ClientSettings.PropertyValue("NewEV.EVClientSecret")
    End Sub

End Class