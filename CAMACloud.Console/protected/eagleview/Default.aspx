﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Default.aspx.vb" Inherits="CAMACloud.Console.NewEagleView" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>EagleView</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script crossorigin src="https://unpkg.com/react@17/umd/react.production.min.js"> </script>
    <script crossorigin src="https://unpkg.com/react-dom@17/umd/react-dom.production.min.js"></script>
    <script crossorigin src="https://embedded-explorer.eagleview.com/static/embedded-explorer-widget.js"></script>
</head>
<body>
    <div id='map' style="height: 100%; width: 100%;"></div>
    <script>
        var apiUrl = "https://api.eagleview.com/auth-service/v1/token", apiClientId = "<%=EVClientID%>", apiClientSecret = "<%=EVClientSecret%>", activeParcel = {}, CENTER,
            authorization = btoa(apiClientId + ":" + apiClientSecret), requestOptions = {
                method: "POST",
                headers: {
                    "Authorization": `Basic ${authorization}`,
                    "Accept": "application/json",
                    "Content-Type": "application/x-www-form-urlencoded"
                },
                body: new URLSearchParams({
                    "grant_type": "client_credentials"
                })
            };

        function getApiToken(callback) {
            fetch(apiUrl, requestOptions)
                .then(response => {
                    if (!response.ok) {
                        throw new Error(`HTTP error! Status: ${response.status}`);
                    }
                    return response.json();
                })
                .then(data => {
                    if (callback) callback(data);
                })
                .catch(error => {
                    console.error(error);
                });
        }

        function createShapes() {
            let shapes = [], polygon = [];
            for (x in activeParcel.MapPoints.sort(function (x, y) { return x.Ordinal - y.Ordinal })) {
                var p = activeParcel.MapPoints[x];
                if (shapes[p.RecordNumber || 0] == null) {
                    shapes[p.RecordNumber || 0] = [];
                }
                shapes[p.RecordNumber || 0].push([p.Longitude, p.Latitude]);
            };

            shapes.forEach(function (w) {
                polygon.push({
                    "type": "Feature",
                    "geometry": {
                        "type": "Polygon",
                        "coordinates": [w]
                    },
                    "properties": {
                        "eagleview": {
                            "style": {
                                "size": 2,
                                "color": "#FFF",
                                "fill": "#f55f5f",
                                "opacity": 0.5,
                                "fillOpacity": 0.3
                            }
                        }
                    }
                });
            });

            return polygon;
        }

        function initMap(token) {
            var map = new window.ev.EmbeddedExplorer().mount('map', {
                authToken: token.access_token,
                view: { lonLat: { lon: CENTER[0], lat: CENTER[1] } }
            });

            map.addFeatures({ geoJson: createShapes() });
        }

        $('document').ready(function () {
            if (apiUrl && apiClientId && apiClientSecret) {
                if (window.opener != null) {
                    if (window.opener.__DTR) {
                        activeParcel.MapPoints = window.opener.activeParcel.MapPoints;
                        CENTER = [window.opener.activeParcel.mapCenter[1], window.opener.activeParcel.mapCenter[0]];
                    }
                    else {
                        activeParcel.MapPoints = window.opener.activeParcel._mapPoints;
                        CENTER = [window.opener.activeParcel.Center().lng(), window.opener.activeParcel.Center().lat()];
                    }
                }

                getApiToken((token) => {
                    initMap(token);
                });
            }
            else alert("credentials not loaded! ");

        });
    </script>
</body>
</html>
