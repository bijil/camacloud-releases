﻿Imports System.Web.Script.Services
Imports System.Web.Services
Imports CAMACloud

Partial Class ATCMapView
	Inherits System.Web.UI.Page
	Public ReadOnly Property countyName As String
		Get
			Dim county = ClientOrganization.PropertyValue("County", HttpContext.Current.GetCAMASession().OrganizationId)
			Return county
		End Get
	End Property
	Public ReadOnly Property countyLatLong As List(Of String)
		Get
			Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT top 1 [Latitude],[Longitude] FROM ParcelMapPoints WHERE Latitude IS NOT NULL AND Longitude IS NOT NULL")
			Dim latlong As New List(Of String)

			If dr IsNot Nothing Then
				latlong.Add(dr.GetString("Latitude"))
				latlong.Add(dr.GetString("Longitude"))
			Else
				latlong.Add("")
				latlong.Add("")
			End If
			Return latlong
		End Get
	End Property

	Public ReadOnly Property DoNotChangeTrueFalseInPCI As Boolean
		Get
			Dim ChangeTrueFalse = False
			Dim dr As String = Database.Tenant.GetStringValue("SELECT Value FROM ClientSettings WHERE Name = 'DoNotChangeTrueFalseInPCI'")
			If dr = "1" Then
				ChangeTrueFalse = True
			Else
				ChangeTrueFalse = False
			End If
			Return ChangeTrueFalse
		End Get
	End Property

	Public ReadOnly Property EnableNewPriorities As Boolean
		Get
			Dim EnableNewPriority = False
			Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM ClientSettings WHERE Name = 'EnableNewPriorities' AND Value = 1")
			If dr IsNot Nothing Then
				EnableNewPriority = True
			End If
			Return EnableNewPriority
		End Get
	End Property

	Public ReadOnly Property hasTaskManagerRole As Boolean
		Get
			Dim hasroles = False
			Dim currentUserRole = Roles.GetRolesForUser()
	        If currentUserRole.Contains("TaskManager") Then
	        	hasroles = True
	        End If
	        Return hasroles
	    End Get
	End Property
End Class