﻿Public Class _Default2
    Inherits System.Web.UI.Page
    Public APIKey As String
    Public SecretKey As String
    Public IPALoadUrl As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim hostName As String = HttpContext.Current.Request.Url.Host, UrlName = ""
        If hostName.Trim.EndsWith(".beta2.camacloud.com") Then
            UrlName = "beta2"
        ElseIf hostName.Trim.EndsWith(".beta1.camacloud.com") Then
            UrlName = "beta1"
        ElseIf hostName.Trim.EndsWith("camacloud.camacloud.com") Then
            UrlName = "Live"
        ElseIf hostName = "localhost" Then
            UrlName = "localhost"
        End If
        'Dim dt As DataRow = Database.System.GetTopRow("select * from PictometrySettings where name={0}".SqlFormatString(UrlName))
        APIKey = ClientSettings.PropertyValue("EagleView.APIKey")  ' dt.GetString("ApiKey")
        SecretKey = ClientSettings.PropertyValue("EagleView.SecretKey") ' dt.GetString("SecretKey")
        IPALoadUrl = ClientSettings.PropertyValue("EagleView.IPALoadURL") ' dt.GetString("IPALoadUrl")

        If APIKey Is Nothing OrElse SecretKey Is Nothing OrElse IPALoadUrl Is Nothing Then
            Response.Redirect("pictometry.install.aspx")
        End If
    End Sub

End Class