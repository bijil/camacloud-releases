﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master" CodeBehind="validations.aspx.vb"
    Inherits="CAMACloud.Console.Validations" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
        
        $('.pgr a').click(function () { showMask(); });
        
        if($( '.txtCondition' ).text()!=""){
           $(".txtCondition").height( $(".txtCondition")[0].scrollHeight );
          }
        if($( '.txtErrorMessage' ).text()!=""){
       		$(".txtErrorMessage").height( $(".txtErrorMessage")[0].scrollHeight );
      	  }
      	if($( '.txtNote' ).text()!=""){
       		$(".txtNote").height( $(".txtNote")[0].scrollHeight );
      	  }
        var totalRows = $("#<%=gvValidations.ClientID %> tr").length;
        	var mainDiv_offset = $("#mainDiv").offset().top;
       		var editDiv_height = $("#div_edit").height();
         	$("#<%=gvValidations.ClientID%> tr>td:nth-child(2)").css('cursor', 'pointer');
        	$("#<%=gvValidations.ClientID%> tr>td:nth-child(2)").click(function(){
        	var gvValidationsRow_offset = $(this).offset().top;
         	if(gvValidationsRow_offset > 728)
         		{
          			//var diff_offset = gvValidationsRow_offset-mainDiv_offset;
        			//var newMargin = diff_offset-editDiv_height;
         			//$("#div_edit").css("margin-top",newMargin);
         		}
         	else
         		{
         			$("#div_edit").css("margin-top",20);
         			
        		 }
        	
        	if(totalRows <= '10')
        		 {
        		 $("#div_edit").css("margin-bottom",8);
        		 }
       
        	var id = $(this).next().next().next().children().eq(0).attr('validatorId')
        	$ds('viewvalidationdata',{Id:id},function(res){
        	$("#txtCondition_edit").height('50px');
            $("#txtErrorMessage_edit").height('50px');
            $("#txtNote_edit").height('50px');           
        	$("#txtName_edit").val(res[0].Name);
        	$("#ddlSourceTable_edit").children().html(res[0].SourceTable);
        	$("#txtCondition_edit").text(res[0].Condition);
        	$("#txtErrorMessage_edit").text(res[0].ErrorMessage);
        	$("#txtNote_edit").text(res[0].Note);
            $("#div_edit").show();
        	$(".gGrid").width('52%');
        	$("#txtCondition_edit").height( $("#txtCondition_edit")[0].scrollHeight );
            $("#txtErrorMessage_edit").height( $("#txtErrorMessage_edit")[0].scrollHeight ); 
         	$("#txtNote_edit").height( $("#txtNote_edit")[0].scrollHeight );
         	});
         	});
         
        $(document).on('click', function (e) {
    	if (($(e.target).closest(".gGrid").length === 0) && ($(e.target).closest("#div_edit").length === 0)){
        $("#div_edit").hide();
        $(".gGrid").width('90%');
        }
		});
 	
            $('.enableToggler').click(function () {
                var id = $(this).attr('validatorId');
                var that = this;
                $ds('toggledisable', { Id: id, TableName: 'ClientValidation' }, function (res) {
                    var text = $(that).text();
                    text = (text == 'Enable') ? 'Disable' : 'Enable';
                    $(that).text(text);
                });
            });
        });
        
        function setHeight(idname) {
			idname.addEventListener('change', autosize);  
			idname.addEventListener('keydown', autosize);
			idname.addEventListener('keypress', autosize);
		function autosize(){
			var el = this;
 			//setTimeout(function(){
   				el.style.cssText = 'height:auto;';
   				el.style.cssText = 'height:' + el.scrollHeight + 'px';	   	
 			//}, 0 );
   			}
       	}
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>
        Data Collection - Validation Rules
    </h1>
    <style>
    	.anchorclass{
			display: inline-block;
			width: 100px;
			Height: auto;
			white-space: nowrap;
			overflow: hidden !important;
			text-overflow: ellipsis;
			text-decoration: none;
			color: #717171;
		}
        .longText {
        	word-break:break-all;
        }
        .headText{
        	padding-left:6px !important;
        }
        .newText {
        	word-break:break-word;
        	padding-left:10px !important;
        }
        span[sep]
        {
            float: right;
            margin-left: 35px;
        }
        textarea
        {
            overflow: auto;
            resize: none;
        }
        .mGrid
        {
            margin-top: 20px;   
        }
        .gGrid
        {
             width:90%;
        }
        
        select
        {
            width: 200px;
        }
        #editTable
        {
			margin-top: 10px;
			padding:5px;
			margin-bottom: 10px;
			
        } 
        #div_edit
        {
       		margin-top:20px;
       		border-style: solid;
       		border-color: #0ab0c4;
       		border-width: 1px;
       		margin-bottom: 10px;
        }
        textarea 
        {
    		resize: none;
    		box-sizing: border-box;
		}
		.txtCondition
		{
			min-width: 350px;
			min-height: 70px;
		}
		.txtErrorMessage
		{
			min-width: 350px;
			min-height: 70px;
		}
		.txtNote
		{
			min-width: 350px;
			min-height: 70px;
		}
    </style>
    <p class="info">
        Define rules for validation of collected data on the field devices. Select appropriate table, and then specify condition as a boolean expression. The fields in the condition expression should be valid fields (case-sensitive) from the selected table or group. Allowed operators are <b>+</b>, <b>-</b>, <b>*</b>, <b>/</b>, <b>&gt;</b>, <b>&lt;</b>, <b>==</b>, <b>!=</b>. Expressions can use round brackets as well. </p>
    <table>
        <tr>
            <td style="padding-right: 50px;">Name:</td>
            <td>
                <asp:HiddenField ID="hcid" runat="server" Value="" />
                <asp:TextBox ID="txtName" runat="server" Width="200px" MaxLength="50"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvTxtName" runat="server" ControlToValidate="txtName" ErrorMessage="*" ValidationGroup="AddValidations" />
            </td>
        </tr>
        <tr>
            <td style="padding-right: 50px;">Source Table: </td>
            <td>
                <asp:DropDownList ID="ddlSourceTable" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="padding-right: 50px;">Condition: </td>
            <td>
                <asp:TextBox ID="txtCondition" cssClass="txtCondition" runat="server" TextMode="MultiLine" Rows="2" Width="350px" onkeydown="setHeight(this);" onchange="setHeight(this);" onkeypress="setHeight(this);"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvtxtCondition" runat="server" ControlToValidate="txtCondition" ErrorMessage="*" ValidationGroup="AddValidations" />
            </td>
        </tr>
        <tr>
            <td style="padding-right: 50px;">Error Message: </td>
            <td>
                <asp:TextBox ID="txtErrorMessage" cssClass="txtErrorMessage" runat="server" TextMode="MultiLine" Rows="2" Width="350px" onkeydown="setHeight(this);" onchange="setHeight(this);" onkeypress="setHeight(this);"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvTxtErrorMessage" runat="server" ControlToValidate="txtErrorMessage" ErrorMessage="*" ValidationGroup="AddValidations" />
            </td>
        </tr>
        <tr>
            <td style="padding-right: 50px;">Note: </td>
            <td>
                <asp:TextBox ID="txtNote" cssClass="txtNote" runat="server" TextMode="MultiLine" Rows="2" Width="350px" onkeydown="setHeight(this);" onchange="setHeight(this);" onkeypress="setHeight(this);"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:CheckBox runat="server" ID="chkValidateFirstOnly" Text="Validate first record only" /> &nbsp;
                <asp:CheckBox runat="server" ID="chkEnableSofWarning" Text="Enable soft warning" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Button runat="server" ID="btnValidations" Text="Add Condition" Font-Bold="true" Width="180px" Height="28px" ValidationGroup="AddValidations" />
                <asp:Button runat="server" ID="btnCancel" Text="Cancel" Width="80px" Height="28px" Visible="false" />
            </td>
        </tr>
    </table>
    <div ID ="mainDiv" style ="width: 99%;" >
    	<div ID="pageSize_div"style = "margin-top:20px">
    		No. of items per page:
    			<asp:DropDownList runat="server" ID="ddlPageSize" AutoPostBack="true" width="50px">
			        <asp:ListItem Value="10" />
			        <asp:ListItem Value="20" />
			        <asp:ListItem Value="50" />
		         </asp:DropDownList>
    	</div>
    <div  ID="gvValidationsDiv"   class="gGrid" style = "float: left;width: 90%;">
    <asp:GridView runat="server" ID="gvValidations" class="mGrid" AutoGenerateColumns="false" AllowPaging="true" AllowSorting="true" PageSize="10" EnableViewState="true" Onsorting = "GridView1_Sorting">
            <Columns>
             <asp:BoundField DataField="Id" HeaderText="#" ItemStyle-Width="25px" ItemStyle-Wrap="false" SortExpression="Id"> <HeaderStyle Font-Underline="True" /></asp:BoundField>      
             <asp:BoundField DataField="Name" HeaderText="Name" ItemStyle-Width="300px" ItemStyle-CssClass="longText"  SortExpression="Name"> <HeaderStyle Font-Underline="True" /></asp:BoundField>          
             <asp:BoundField DataField="SourceTable" HeaderText="SourceTable" ItemStyle-Width="300px" ItemStyle-CssClass="longText" SortExpression="SourceTable"> <HeaderStyle Font-Underline="True" /></asp:BoundField>
             <asp:TemplateField HeaderText="Note" SortExpression="Note">
             	<HeaderStyle Font-Underline="True" cssClass="headText"/>
                <ItemStyle  Width="200px" CssClass="newText"/>
                <ItemTemplate>                        
                	<div title="<%# Eval("Note") %>" class ="anchorclass">
                		<%# Eval("Note") %>   
                	</div>
                               
                </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField  HeaderText="<u>Status</u>"  SortExpression="Enabled">
                <ItemStyle Width="50px" Wrap="False"/>
                <ItemTemplate>
                    <a class="enableToggler" validatorId=<%# Eval("Id")%> style="cursor:pointer" ><%# IIf( Eval("Enabled"),"Disable", "Enable")%></a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemStyle Width="30px" Wrap="False"/>
                <ItemTemplate>
                    <asp:LinkButton ID="lbtnEdit" runat="server" Text="Edit" CommandName='EditAction' CommandArgument='<%# Eval("Id") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemStyle Width="50px" Wrap="False"/>
                <ItemTemplate>
                    <asp:LinkButton ID="lbtnDelete" runat="server" Text="Delete" CommandName='DeleteAction' CommandArgument='<%# Eval("Id") %>' OnClientClick='return confirm("Are you sure you want to delete this action?")' />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    </div>
      <div ID="div_edit" style ="float: right;width: 45%;display: none; ">
    	<table ID ="editTable">
	        <tr>
	            <td style="padding-top:3px;">Name:</td>
	            <td style ="padding:5px">
	             <input type="text" ID="txtName_edit" disabled/>
	              
	            </td>
	        </tr>
	        <tr>
	            <td style="padding-right: 10px;padding-top: 3px">Source Table: </td>
	            <td style ="padding:5px">
	            <select ID="ddlSourceTable_edit" disabled>
	            <option></option>
	            </select>
	               
	            </td>
	        </tr>
	        <tr>
	            <td style="padding-right: 50px;padding-top: 15px;">Condition: </td>
	            <td style ="padding:5px">
	            <textarea ID="txtCondition_edit" cols="200" rows="2" style="width:100%;min-height:55px;" disabled></textarea>
	            </td>
	        </tr>
	        <tr>
	            <td style="padding-right: 10px;padding-top: 15px;">Error Message: </td>
	            <td style ="padding:5px;width:100%">
	            <textarea ID="txtErrorMessage_edit" cols="100" rows="2" style="width:100%;min-height:55px;" disabled></textarea>
	            </td>
	         </tr>
	         <tr>
	            <td style="padding-right: 10px;padding-top: 15px;">Note: </td>
	            <td style ="padding:5px;width:100%">
	            <textarea ID="txtNote_edit" cols="100" rows="2" style="width:100%;min-height:55px;" disabled></textarea>
	            </td>
	         </tr>
	       
     	</table>
      </div>
    </div>
</asp:Content>
