﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master" CodeBehind="apprmethods.aspx.vb"
    Inherits="CAMACloud.Console.apprmethods" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .col1
        {
            padding-right: 30px;
            padding-left:5px;
        }
    </style>
    <script>
    	var submit = 0;
	    function PreventDoubleClick() {
		    if (++submit > 1) {
			     return false;
            }
            submit = 0;
            if (!$('.textDesc').val().match(/[a-zA-Z0-9]/)) {
                alert('Please enter at least one character or number in Description box.');
                return false;
            }
            return true;
        }

        function preventSpecialChar() {
            var textBoxValue = $('.textName').val();
            if (!textBoxValue.match(/^[a-zA-Z0-9\s_]+$/)) {
                $('.textName').val(textBoxValue.replace(/[^\w\s]/gi, ''));
                return false;
            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>
        Appraisal Methods</h1>
    <p class="info">
        Set the county's appraisal methods here. These method names have to be used in MobileAssessor's Estimate Chart for Data Entry.</p>
    <table>
        <tr>
            <td class="col1">Method Name: </td>
            <td>
                <asp:TextBox runat="server" ID="txtName" CssClass="textName" Width="100px" MaxLength="10" oninput="return preventSpecialChar()"/>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName" ErrorMessage="*" ValidationGroup="Form" />
            </td>
        </tr>
        <tr>
            <td class="col1">Description: </td>
            <td>
                <asp:TextBox runat="server" ID="txtDescription" CssClass="textDesc" Width="250px" MaxLength="50" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDescription" ErrorMessage="*" ValidationGroup="Form" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:HiddenField runat="server" ID="hdnId" Value="" />
                <asp:Button runat="server" ID="btnSave" Text=" Add Method " Font-Bold="true" ValidationGroup="Form" OnClientClick="return PreventDoubleClick();"/>
                <asp:Button runat="server" ID="btnCancel" Text=" Cancel " Visible="false" />
            </td>
        </tr>
    </table>
    <asp:GridView runat="server" ID="grid" AllowPaging="true" pageSize="5" OnPageIndexChanging="OnPageIndexChanging">
        <Columns>
            <asp:TemplateField>
                <ItemStyle Width="25px" />
                <ItemTemplate>
                    <%# (Container.DataItemIndex)+1 %>.
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Name" HeaderText="Name" ItemStyle-Width="80px" />
            <asp:BoundField DataField="Description" HeaderText="Description" ItemStyle-Width="300px" />
            <asp:TemplateField>
                <ItemStyle Width="80px" />
                <ItemTemplate>
                    <asp:LinkButton runat="server" ID="lbEdit" Text="Edit" CommandName='EditItem' CommandArgument='<%# Eval("Id") %>' />
                    <asp:LinkButton runat="server" ID="lbDelete" Text="Delete" CommandName='DeleteItem' CommandArgument='<%# Eval("Id") %>' OnClientClick='return confirm("Are you sure you want to delete?")' />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>
