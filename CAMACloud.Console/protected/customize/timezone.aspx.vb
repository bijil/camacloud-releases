﻿Public Class timezone
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            FillChanges()
        End If
        SetCurrentDate()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Dim TimeZoneValue = Database.Tenant.GetStringValue("Update App_TimeZone SET Selected = NULL; Update App_TimeZone SET Selected = 1 Where Id = {0}; SELECT Value From App_TimeZone Where Id = {0}".SqlFormatString(ddlTimeZone.SelectedValue))
        ClientSettUpdate("TimeZone", TimeZoneValue)
        ClientSettUpdate("DST", IIf(cbDST.Checked, "1", "0"))
        ClientSettings.AlterDateFunctions("TimeZone", TimeZoneValue, Database.Tenant)
        ClientSettings.AlterDateFunctions("DST", IIf(cbDST.Checked, "1", "0"), Database.Tenant)
        SetCurrentDate()
        Dim Description As String = "UTC Timezone has been upated to (" + TimeZoneValue + ")."
        SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
        NotificationMailer.SendNotificationToVendorCDS(Membership.GetUser().ToString, 2, "Timezones")
        Alert("TimeZone has been updated successfully.")
    End Sub

    Private Sub ClientSettUpdate(Field As String, FieldValue As String)
        Dim DtStreetAdressTab As DataTable = Database.Tenant.GetDataTable("SELECT Value FROM ClientSettings WHERE Name={0}".SqlFormatString(Field))
        If DtStreetAdressTab.Rows.Count > 0 Then
            Database.Tenant.Execute("UPDATE ClientSettings SET Value={0} WHERE Name={1} ".SqlFormatString(FieldValue, Field))
        Else
            Database.Tenant.Execute("INSERT INTO ClientSettings (Value,Name) VALUES({0},{1}) ".SqlFormatString(FieldValue, Field))
        End If
    End Sub

    Private Sub FillChanges()
        UpdateClientSettingFields("TimeZone", ddlTimeZone)
        UpdateClientSettingFields("DST", cbDST)
    End Sub

    Private Sub UpdateClientSettingFields(Field As String, ddl As DropDownList)
        Dim dtZones As DataTable = Database.Tenant.GetDataTable("SELECT 0 As Id,'--SELECT-- ' As Name UNION SELECT Id,Name FROM App_TimeZone")
        ddl.FillFromTable(dtZones)
        Dim DrClientSettings As DataRow = Database.Tenant.GetTopRow("SELECT ID,Value FROM App_TimeZone WHERE Selected = 1")
        If DrClientSettings Is Nothing Then
            ddl.SelectedValue = "0"
        Else
            ddl.SelectedValue = GetString(DrClientSettings, "ID")

        End If
    End Sub
    Private Sub UpdateClientSettingFields(Field As String, cb As CheckBox)
        Dim DrClientSettings As DataRow = Database.Tenant.GetTopRow("SELECT Value FROM ClientSettings WHERE Name={0}".SqlFormatString(Field))
        If DrClientSettings Is Nothing Then
            cb.Checked = False
        Else
            cb.Checked = IIf(GetString(DrClientSettings, "Value") = "1", True, False)
        End If
    End Sub
    Private Sub SetCurrentDate()
        lblCurrentTime.Text = Database.Tenant.GetStringValue("SELECT LTRIM(RIGHT(CONVERT(VARCHAR(20),dbo.GetLocalDate(GETUTCDATE ()), 100), 7))  as dateN")
    End Sub

    Private Sub refreshBtn_Click(sender As Object, e As System.EventArgs) Handles refreshBtn.Click, Timer1.Tick
        SetCurrentDate()
    End Sub
End Class