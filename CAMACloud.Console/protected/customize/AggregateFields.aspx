﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master" CodeBehind="AggregateFields.aspx.vb" Inherits="CAMACloud.Console.AggregateFields" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<script type="text/javascript">
		function isAlphaNumericKey(evt) {
    		var charCode = evt.keyCode
    		if ((charCode >= 65 && charCode <= 90) || (charCode >= 97 && charCode <= 122) || charCode == 95) {
        		return true;
    		}    		
    		return false;
		}
		
		function DeleteConfirmation() {
        	if (confirm("Are you sure you want to delete this Aggregate Field?"))
            	return true;
        	else
            	return false;
		}
	</script>
	<style>
		.aggrecolumn{
			word-wrap: break-word;
			word-break: break-all;
			white-space: normal;
		}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        <h1>
             Aggregate Fields Setup
        </h1>  
    	<asp:UpdatePanel runat="server" class="up-field-settings">
		<ContentTemplate>  
    <table>
        <tr>
            <td style="padding-right: 20px;">
                Select Table:
            </td>
                <td >
                
                <asp:DropDownList ID="ddlTable" runat="server" AutoPostBack="true" style="Width:240px; margin-left:100px;" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlTable"
                    ErrorMessage="*" ValidationGroup="CustomAction" />
                </td>
     </tr>
     <tr>
             <td style="padding-right: 20px;">
                Select Field:
             </td>
             <td>
                <asp:DropDownList ID="ddlField" runat="server" AutoPostBack="true" style="Width:240px; margin-left:100px;" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" InitialValue="-- Select --" ControlToValidate="ddlField"
                    ErrorMessage="*" ValidationGroup="CustomAction" />
             </td>
     </tr>
    <tr>
        <td style="padding-right: 20px;">
                Select Aggregate Function:
            </td>
        <td>
        <asp:DropDownList ID="ddlFunction" runat="server" AutoPostBack="true" style="Width:240px; margin-left:100px;" >
            <asp:ListItem Value="">-- Select --</asp:ListItem>
            <asp:ListItem Value="AVG">Average</asp:ListItem>
            <asp:ListItem Value="COUNT">Count</asp:ListItem>
            <asp:ListItem Value="FIRST">First</asp:ListItem>
            <asp:ListItem Value="LAST">Last</asp:ListItem>
            <asp:ListItem Value="MAX">Maximum</asp:ListItem>
            <asp:ListItem Value="MIN">Minimum</asp:ListItem>
            <asp:ListItem Value="SUM">Sum</asp:ListItem>
            <asp:ListItem Value="MEDIAN">Median</asp:ListItem>
            </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlFunction"
                    ErrorMessage="*" ValidationGroup="CustomAction" />
            </td>
         </tr>
         <tr>
              <td style="padding-right: 20px;">
                Add Condition:
             </td>
                  <td>
              <asp:TextBox ID="txtCndtn" runat="server" AutoPostBack="true" MaxLength = "500" style="Width:236px; margin-left:100px;" />
                      </td>
         </tr>
         <tr>
         	<asp:Label ID = "labelUnique" runat = "server" visible = "false" AutoPostBack = "true">
         		<td style = "padding-right: 20px;">
         			Unique Name:
         		</td>
         	</asp:Label>
         	<td>
         		<asp:TextBox ID = "txtUnique" runat = "server" onkeypress = "return isAlphaNumericKey(event)" visible = "false" AutoPostBack = "true" MaxLength = "50" style = "width: 236px; margin-left: 100px;"/>
         		<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtUnique"
                    ErrorMessage="*" ValidationGroup="CustomAction" ClientIDMode="Static" />
         	</td>
         </tr>
    <tr>
        
        <td colspan="2">
        
        	<asp:HiddenField ID="editId" runat="server" Value="" />
            <asp:Button ID="btnAddAction" runat="server" Text="Add Field" OnClientClick = "" ValidationGroup="CustomAction" Width="120px" style="float:left; padding : 2px;" />
            <asp:Button ID="btnAddCancel" runat="server" Text="Cancel" Width="120px" style="float:left; padding 2px; margin-left: 10px;" Visible="false" />
        </td>
    </tr>
    </table>
     <asp:GridView runat="server" ID="grid" AutoGenerateColumns="False"  AllowPaging="true" PageSize="10" OnPageIndexChanging="OnPageIndexChanging" AllowSorting="true" EnableViewState="true" Onsorting = "GridView1_Sorting" Width="95%" DataKeyNames="ROWUID" SelectedIndex="0" style = "white-space: nowrap; overflow: hidden">
        <Columns>
            <asp:BoundField DataField="TableName" SortExpression="TableName" HeaderText="Table" ItemStyle-Width="200px" >
            <ItemStyle Width="200px" /><HeaderStyle Font-Underline="True" />
            </asp:BoundField>
            <asp:BoundField DataField="FieldName" SortExpression="FieldName" HeaderText="Field" ItemStyle-Width="200px" >
              <ItemStyle Width="200px" /><HeaderStyle Font-Underline="True" />
            </asp:BoundField>
              <asp:BoundField DataField="FunctionName" SortExpression="FunctionName" HeaderText="Aggregate Function" ItemStyle-Width="250px" >
            <ItemStyle Width="200px" /><HeaderStyle Font-Underline="True" />
            </asp:BoundField>
            <asp:BoundField DataField="Condition" SortExpression="Condition" HeaderText="Condition"  ItemStyle-CssClass = "aggrecolumn" >
            <ItemStyle Wrap = "True"/><HeaderStyle Font-Underline="True" />
            <ItemStyle Width="400px" />
            </asp:BoundField>
            <asp:BoundField DataField="UniqueName" SortExpression="UniqueName" HeaderText="Unique Name" ItemStyle-Width="350px" >
            <ItemStyle Width="350px" /><HeaderStyle Font-Underline="True" />
            </asp:BoundField>
            <asp:TemplateField>
                <ItemTemplate>
                	<asp:LinkButton ID="LinkButton2" runat="server"  CommandName ="EditItem" CommandArgument='<%# Eval("ROWUID") %>'>Edit</asp:LinkButton>
                    <asp:LinkButton ID="LinkButton1" runat="server"  CommandName ="Delete" OnClientClick="if ( ! DeleteConfirmation()) return false;" CommandArgument='<%# Eval("ROWUID") %>'>Delete</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
        </ContentTemplate>
		</asp:UpdatePanel>
</asp:Content>

