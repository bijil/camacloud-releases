﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master" CodeBehind="fieldalerttypes.aspx.vb" Inherits="CAMACloud.Console.fieldalerttypes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .col1
        {
            padding-right: 30px;
            padding-left:5px;
        }
    </style>
    <script type="text/javascript">
        function confirmAlert() {
            if (Page_ClientValidate('Form')) {
                var hdnValue = $("#" + '<%= hdnId.ClientID%>').val();
                if (hdnValue != "") {
                    return confirm("If you edit an existing Field Alert Type, any properties that currently contain this Field Alert Type will be updated with this type. Please select OK to proceed with the update, or Cancel to go back.");
                }
                else {
                    return true;
                }
            }
            return false;
        }

        function deleteConfirmation() {
            return confirm("Are you sure you want to delete? \n\nNote: If you delete an existing Field Alert Type, any properties that currently contain this Field Alert Type will be removed.");
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>
        Field Alert Types</h1>
    <p class="info">
        Set the field alert types here. These types will be used by MobileAssessor to flag parcels.</p>
    <table>
        <tr>
            <td class="col1">Alert Type Name: </td>
            <td>
                <asp:TextBox runat="server" ID="txtName" Width="200px" MaxLength="30" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName" ErrorMessage="*" ValidationGroup="Form" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:HiddenField runat="server" ID="hdnId" Value="" />
                <asp:Button runat="server" ID="btnSave" Text=" Add Type " OnClientClick="return confirmAlert();"  Font-Bold="true" ValidationGroup="Form" />
                <asp:Button runat="server" ID="btnCancel" Text=" Cancel " Visible="false" />
            </td>
        </tr>
    </table>
    <asp:GridView runat="server" AllowPaging="true" PageSize="10" OnPageIndexChanging="OnPageIndexChanging"  ID="grid">
        <Columns>
            <asp:TemplateField>
                <ItemStyle Width="25px" />
                <ItemTemplate>
                    <%# (Container.DataItemIndex)+1 %>.
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Name" HeaderText="Name" ItemStyle-Width="275px" />
            <asp:TemplateField>
                <ItemStyle Width="80px" />
                <ItemTemplate>
                    <asp:LinkButton runat="server" ID="lbEdit" Text="Edit" CommandName='EditItem' CommandArgument='<%# Eval("Id") %>' />
                    <asp:LinkButton runat="server" ID="lbDelete" Text="Delete" CommandName='DeleteItem' CommandArgument='<%# Eval("Id") %>' OnClientClick='return deleteConfirmation()' />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>
