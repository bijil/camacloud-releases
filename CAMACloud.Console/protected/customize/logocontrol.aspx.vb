﻿
Partial Class customize_logocontrol
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            tickValue.Value = Now.Ticks
        End If
    End Sub

    Protected Sub btnUpload_Click(sender As Object, e As System.EventArgs)
        Dim btn As Button = sender
        Dim upload As FileUpload = Nothing
        Dim tag As String = Nothing
        Dim minWidth = 0, minHeight = 0, maxWidth = 0, maxHeight = 0

        Dim s3Path As String = HttpContext.Current.GetCAMASession.TenantKey + "/assets/logo-"

        'Dim testMessage As String
        'testMessage = IIf(btn Is btnMA1, "Uploaded Logo 1", IIf(btn Is btnMA2, "Uploaded logo 2", "No uploads"))
        'testMessage += " : " + btn.CommandName
        'Alert(testMessage)

        Select Case btn.CommandName
            Case "MA1"
                upload = FileMA1
                tag = "ma-app-top-left"
                maxHeight = 85
                maxWidth = 1600
                minHeight = 85
                minWidth = 120
            Case "MA2"
                upload = FileMA2
                tag = "ma-app-top-right"
                maxHeight = 85
                maxWidth = 800
                minHeight = 85
                minWidth = 120
            Case "MA3"
                upload = FileMA3
                tag = "ma-app-splash"
                minHeight = 350
                minWidth = 500
                maxHeight = 1000
                maxWidth = 1000
            Case "MA4"
                upload = FileMA4
                tag = "console-main"
                minHeight = 100
                minWidth = 318
                maxHeight = 100
                maxWidth = 318
        End Select

        'Alert(tag + ", " + IIf(upload Is Nothing, "no upload", "OK") + ", " + IIf(upload.HasFile, "Has File", "No File"))
        If Not String.IsNullOrEmpty(tag) AndAlso upload IsNot Nothing AndAlso upload.HasFile Then
            Dim fileName As String = upload.FileName
            Dim ext As String = IO.Path.GetExtension(fileName)
            s3Path = s3Path + tag + ext.ToLower
            Dim s3 As New S3FileManager
            Try
                Dim bmp As New Drawing.Bitmap(upload.FileContent)
                If (minWidth > 0 AndAlso bmp.Width < minWidth) Or (minHeight > 0 AndAlso bmp.Height < minHeight) Or (maxHeight > 0 AndAlso bmp.Height > maxHeight) Or (maxWidth > 0 AndAlso bmp.Width > maxWidth) Then
                    Throw New Exception("Uploaded image does not match the dimension conditions.")
                End If
                s3.UploadFile(upload.FileContent, s3Path)
                Select Case btn.CommandName
                    Case "MA1"
                        ClientSettings.MobileAssessorTopLeftLogo = s3Path
                        Alert("New Client Logo(MA Top Left) uploaded")
                    Case "MA2"
                        ClientSettings.MobileAssessorTopRightLogo = s3Path
                        Alert("New Client Logo(MA Top Right) uploaded")
                    Case "MA3"
                        ClientSettings.MobileAssessorSplash = s3Path
                        Alert("New Client Logo(MA Splash) uploaded")
                    Case "MA4"
                        ClientSettings.ConsoleMain = s3Path
                        Alert("New Client Logo(Console Main) uploaded")
                End Select

                tickValue.Value = Now.Ticks
                ClientSettings.CustomizationDate = Now
            Catch ex As Exception
                Alert(ex.Message)
            End Try
        Else

        End If

       
    End Sub

    Protected Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        logoMA1.Style("background-image") = "url(/applogo.png?logo=logo-ma-app-top-left&t=" + tickValue.Value + ")"
        logoMA2.Style("background-image") = "url(/applogo.png?logo=logo-ma-app-top-right&t=" + tickValue.Value + ")"
        logoMA3.Style("background-image") = "url(/applogo.png?logo=logo-ma-app-splash&t=" + tickValue.Value + ")"
        logoMA4.Style("background-image") = "url(/applogo.png?logo=console-main&t=" + tickValue.Value + ")"
    End Sub

    Private Sub btnUpdateVersion_Click(sender As Object, e As System.EventArgs) Handles btnUpdateVersion.Click
        ClientSettings.CustomizationDate = Now
        NotificationMailer.SendNotificationToVendorCDS(Membership.GetUser().ToString, 2, "Client Logo Control")
        Alert("Client Logo Control uploaded")
        Dim Description As String = "New Client Logo Control uploaded."
        SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
    End Sub

    Public Sub btnDownload_Click(sender As Object, e As System.EventArgs)
    	Dim lb As LinkButton = sender
    	Try
	        Dim logoPath As String = ""
	        Select Case lb.CommandArgument
	            Case "MA1"
	                logoPath = ClientSettings.PropertyValue("MobileAssessorTopLeftLogo")
	            Case "MA2"
	                logoPath = ClientSettings.PropertyValue("MobileAssessorTopRightLogo")
	            Case "MA3"
	                logoPath = ClientSettings.PropertyValue("MobileAssessorSplash")
	            Case "MA4"
	                logoPath = ClientSettings.PropertyValue("ConsoleMain")
	        End Select
	
	        If logoPath Is Nothing OrElse String.IsNullOrEmpty(logoPath.Trim) Then
	            Alert("No custom logo has been set.")
	            Return
	        End If
	
	        Dim s3 As New S3FileManager
	        s3.DownloadFile(logoPath, IO.Path.GetFileName(logoPath), Response)
	        Response.End()
	    Catch ex As Exception
	  	    Alert(ex.Message)
	    End Try
    End Sub

    Public Sub btnReset_Click(sender As Object, e As System.EventArgs)
        Dim lb As LinkButton = sender
        Dim logoPath As String = ""
        Select Case lb.CommandArgument
            Case "MA1"
                ClientSettings.Delete("MobileAssessorTopLeftLogo")
            Case "MA2"
                ClientSettings.Delete("MobileAssessorTopRightLogo")
            Case "MA3"
                ClientSettings.Delete("MobileAssessorSplash")
            Case "MA4"
                ClientSettings.Delete("ConsoleMain")
        End Select
        tickValue.Value = Now.Ticks
        ClientSettings.CustomizationDate = Now
        NotificationMailer.SendNotificationToVendorCDS(Membership.GetUser().ToString, 2, "Client Logo Control")
    End Sub
End Class
