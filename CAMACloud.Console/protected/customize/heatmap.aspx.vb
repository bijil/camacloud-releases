﻿Imports System.IO
Imports CAMACloud.BusinessLogic
Imports CAMACloud.BusinessLogic.Installation
Imports CAMACloud.BusinessLogic.Installation.EnvironmentConfiguration
Imports CAMACloud.BusinessLogic.RemoteIntegration
Public Class heatmap
    Inherits System.Web.UI.Page
    Dim mapid As Integer
    Dim FromLookup As Boolean = False
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        LoadSavedGrid()
        csvuploadinfo.Visible = False
        displayLblDiv.Visible = False
        note.Visible = False
        If Not IsPostBack Then
            lblmapid.Text = ""
            radioList.Items.FindByValue("P").Selected = True
            aggregateField.Text = "False"
            Dim parcelTableId As Integer = Database.Tenant.GetIntegerValueOrInvalid("SELECT Id FROM DataSourceTable WHERE Name = " + Database.Tenant.Application.ParcelTable.ToSqlValue)
            ddlfields.Items.Clear()
            agrddl.Items.Clear()
            ddlfields.FillFromSql("EXEC cc_GetparcelTableRelatedFields " & parcelTableId, True, "-- Select--")
            agrddl.FillFromTable(Database.Tenant.GetDataTable("SELECT Rowuid as Id,FunctionName +'_'+ Tablename + '_'+ FieldName as Name from AggregateFieldSettings order by FunctionName,TableName,FieldName"), True)
        End If
    End Sub
    Private Function CheckExistance() As Boolean
        lblmapid.Text = ""
        If ((ddlfields.SelectedValue = "" AndAlso aggregateField.Text = "False") OrElse (aggregateField.Text = "True" AndAlso agrddl.SelectedIndex = 0) OrElse ddlcomparison.SelectedIndex = 0) Then
            Alert("Please select Parcel Related Field,Aggregate Field and Comparison Type to upload data or add data from lookup ")
            Return False
        End If

        Dim noOfRecords As Integer
        noOfRecords = savedgrid.Rows.Count()
        Dim fieldid As Integer

        If noOfRecords < 10 Then
            fieldid = IIf(aggregateField.Text = "False", ddlfields.SelectedValue, agrddl.SelectedValue)
            Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * from UI_HeatMap WHERE FieldId= " & fieldid)
            If IsNothing(dr) = False Then
                Alert("This value already exists.")
                Return False
            Else
                Return True
            End If
        Else
            Alert("You cannot add more than 10 records")
            Return False
        End If
    End Function
    Sub LoadSavedGrid()
        Dim dt1 As DataTable = Database.Tenant.GetDataTable("SELECT Id,AssignedName,DisplayLabel FROM DataSourceField where isHeatmap = 1")
        Dim dt2 As DataTable = Database.Tenant.GetDataTable("SELECT Rowuid as Id,FieldName as AssignedName,functionName +'_'+ Tablename + '_'+ FieldName as DisplayLabel from AggregateFieldSettings Where IncludeInHeatMap=1")
        dt1.Merge(dt2)
        savedgrid.DataSource = dt1
        savedgrid.DataBind()
    End Sub

    Private Sub LoadData()
        If FromLookup Then
            gridfield.DataSource = Database.Tenant.GetDataTable("SELECT RowUId,Value1,Value2,ColorCode FROM temp_lookupheat")
            gridfield.DataBind()
            gridfield.Columns(1).Visible = False
        Else
            gridfield.DataSource = Database.Tenant.GetDataTable("SELECT RowUId,Value1,Value2,ColorCode FROM UI_HeatMapLookup WHERE HeatMapId={0} ".SqlFormatString(mapid))
            Dim compType As String = Database.Tenant.GetStringValue("SELECT ComparisonType FROM UI_HeatMap where Id={0}".SqlFormat(False, mapid))
            If compType = "2" Then
                gridfield.Columns(1).Visible = True
            Else
                gridfield.Columns(1).Visible = False
            End If
            gridfield.DataBind()
        End If
        'gridfield.Columns(2).Visible = False
    End Sub

    Protected Sub btnsaveclr_Click(sender As Object, e As EventArgs) Handles btnsaveclr.Click
        Dim ColorFlag As String = ddlColor_.SelectedValue
        Dim heatMapId As String, success = 0
        Dim sql1, sqlquery As String
        Dim fieldId As Integer
        Dim compType As Integer
        Dim rowuid As String = ""
        Dim exist As String = "False"
        Dim sql3 As String = "UPDATE UI_HeatMap SET IsAggregate = 0 WHERE fieldId={0}"
        If Not String.IsNullOrEmpty(lblmapid.Text) Then
            exist = lblmapid.Text.Split(",")(1).ToString()
        End If
        If (exist = "True") Then
            heatMapId = lblmapid.Text.Split(",")(0).ToString()

        Else
            fieldId = IIf(aggregateField.Text = "False", ddlfields.SelectedValue, agrddl.SelectedValue)
            compType = ddlcomparison.SelectedValue
            sql1 = "INSERT INTO UI_HeatMap(FieldId,ComparisonType) VALUES({0},{1})"
            Dim ret As Integer = Database.Tenant.Execute(sql1.SqlFormatString(fieldId, compType))
            If ret > 0 Then
                heatMapId = Database.Tenant.GetIntegerValue("SELECT ID FROM UI_HeatMap where FieldID={0}".SqlFormat(False, fieldId))
            End If
            Dim sql2 As String = "UPDATE DataSourceField SET isHeatmap = 1 WHERE Id={0}"
            If (aggregateField.Text = "True") Then
                sql2 = "UPDATE AggregateFieldSettings SET IncludeInHeatMap = 1 WHERE RowUId ={0}"
                sql3 = "UPDATE UI_HeatMap SET IsAggregate = 1 WHERE fieldId={0}"
            End If
            Dim count As Integer = Database.Tenant.Execute(sql2.SqlFormat(False, fieldId))
            Database.Tenant.Execute(sql3.SqlFormat(False, fieldId))
        End If
        Dim val2 As String

        For Each gvr As GridViewRow In gridfield.Rows()
            If Not IsDBNull(gridfield.DataKeys(gvr.RowIndex)("RowUID")) Then
                rowuid = gridfield.DataKeys(gvr.RowIndex)("RowUID")
            End If
            Dim val1 As String = gvr.Cells(0).Text.Replace("&nbsp;", "")
            If (compType = 2) Then
                val2 = gvr.Cells(1).Text.Replace("&nbsp;", "")
            End If
            Dim tb As TextBox = DirectCast(gvr.FindControl("colorcode"), TextBox)
            Dim txt As String = tb.Text
            If (exist = "True") Then
                sqlquery = "UPDATE UI_HeatMapLookup SET ColorCode={0}  Where RowUID={1}"
                success = Database.Tenant.Execute(sqlquery.SqlFormat(False, txt, rowuid))
            Else
                sqlquery = "INSERT INTO UI_HeatMapLookup (HeatMapID,Value1,Value2,ColorCode) VALUES ({0},{1},{2},{3})"
                success = Database.Tenant.Execute(sqlquery.SqlFormat(False, heatMapId, val1, IIf(val2 = "", DBNull.Value, val2), txt))
            End If
        Next
        If ColorFlag <> "" Then
            Dim Description As String = "Heatmap Display label " + "<i> Color Code has Modified</i>"
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
        End If

        If (exist = "True" AndAlso aggregateField.Text = "False") Then
            Dim sqlQuery2 = "UPDATE DataSourceField SET DisplayLabel={0} WHERE Id={1}"
            Database.Tenant.Execute(sqlQuery2.SqlFormat(False, displayLbl.Text, lblmapid.Text.Split(",")(2).ToString()))
            Dim Description As String = "Heatmap Display label has Edited From " + ViewState("Name") + " to " + displayLbl.Text + "."
            If displayLbl.Text <> "" Then
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
            End If
        End If
        ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "hidePopup('popupcolorpick');", True)
        If success > 0 Then
            Alert("Saved successfully")
            If Database.Tenant.DoesTableExists("temp_lookupheat") Then
                Database.Tenant.Execute("DROP TABLE temp_lookupheat")
            End If
            If Database.Tenant.DoesTableExists("temp_heatmap") Then
                Database.Tenant.Execute("DROP TABLE temp_heatmap")
            End If
        End If
        lblmapid.Text = ""
        LoadSavedGrid()
        Response.Redirect("~/protected/customize/heatmap.aspx")
    End Sub
    Private Sub savedgrid_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles savedgrid.RowCommand
        lblmapid.Text = ""
        Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM UI_HeatMap WHERE fieldId=" & e.CommandArgument)
        Dim isAggregate As Boolean = dr.GetBoolean("IsAggregate")
        Select Case e.CommandName
        	Case "EditItem"
                mapid = Database.Tenant.GetIntegerValue("SELECT Id FROM UI_HeatMap WHERE fieldid=" & e.CommandArgument)
                lblmapid.Text = mapid & ",True," & e.CommandArgument
                If isAggregate = False Then
                    displayLbl.Text = Database.Tenant.GetStringValue("SELECT DisplayLabel FROM DataSourceField WHERE Id= (SELECT FieldId from UI_HeatMap WHERE Id={0})".SqlFormatString(mapid))
                    ViewState("Name")=displayLbl.Text
                    displayLblDiv.Visible = True
                    note.Visible = True
                Else
                    displayLblDiv.Visible = False
                    note.Visible = False
                End If
                LoadData()
                RunScript("$('#maskinglayer').hide();")
                'LoadSavedGrid()
            Case "DeleteItem"
            	mapid = Database.Tenant.GetIntegerValue("SELECT Id FROM UI_HeatMap WHERE fieldid=" & e.CommandArgument)
            	ViewState("Name")= Database.Tenant.GetStringValue("SELECT DisplayLabel FROM DataSourceField WHERE Id= (SELECT FieldId from UI_HeatMap WHERE Id={0})".SqlFormatString(mapid))
                If isAggregate = True Then
                    Database.Tenant.Execute("UPDATE AggregateFieldSettings SET IncludeInHeatMap = 0 WHERE RowUId =" & e.CommandArgument)
                Else
                    Database.Tenant.Execute("UPDATE DatasourceField SET isHeatMap=0 WHERE Id =" & e.CommandArgument)
                End If
                Database.Tenant.Execute("DELETE FROM UI_HeatMap WHERE Id={0}".SqlFormatString(mapid))
                Database.Tenant.Execute("DELETE FROM UI_HeatMapLookup WHERE HeatmapID={0}".SqlFormatString(mapid))
                
                Dim Description As String =" FieldName "+ViewState("Name") + " has deleted From HeatMap Setup."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description) 
                LoadSavedGrid()
                
        End Select
    End Sub


    Protected Sub btncsv_Click(sender As Object, e As EventArgs) 'Handles btncsv.Click
        If (CheckExistance() = True) Then
            csvuploadinfo.Visible = True
        Else
            Return
        End If
    End Sub

    Protected Sub lkpSave_Click(sender As Object, e As EventArgs) Handles lkpSave.Click
        If (lblkpright.Items.Count < 1 OrElse lkpddl.SelectedValue = "") Then
            Return
        End If
        Dim dt As New DataTable
        dt.Columns.Add("RowUID")
        dt.Columns.Add("Value1")
        dt.Columns.Add("Value2")
        dt.Columns.Add("ColorCode")
        For Each item As ListItem In lblkpright.Items
            Dim dr As DataRow = dt.NewRow
            'dr("RowUID") = item.Value
            dr("RowUID") = "NULL"
            dr("Value1") = item.Text
            dr("Value2") = "NULL"
            dr("ColorCode") = ""
            dt.Rows.Add(dr)
        Next
        Database.Tenant.BulkLoadTableFromDataTable("temp_lookupheat", dt)
        FromLookup = True
        LoadData()
        Dim Description As String = "Heatmap Setup has been loaded via lookup."
        SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
        ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "loadPopup('popupcolorpick','Set Color');", True)
    End Sub
    Protected Sub lkpCancel_Click(sender As Object, e As EventArgs) Handles lkpCancel.Click
        If Database.Tenant.DoesTableExists("temp_lookupheat") Then
            Database.Tenant.Execute("DROP TABLE temp_lookupheat")
        End If
        gridfield.DataSource = Nothing

    End Sub

    Protected Sub btncancel_Click(sender As Object, e As EventArgs) Handles btncancel.Click
        If Database.Tenant.DoesTableExists("temp_heatmap") Then
            Database.Tenant.Execute("DROP TABLE temp_heatmap")
        End If
        gridfield.DataSource = Nothing
    End Sub
    Public Shared Function csv_to_datatableConvertion(stream As StreamReader, ByRef dt As DataTable) As Boolean

        Try

            Dim pattern As String = "^\s*""?|""?\s*$"
            Dim rgx As New Regex(pattern)
            Dim Lines As String() = stream.ReadToEnd().Split(Environment.NewLine)

            Dim Fields As String() = Lines(0).Split(New Char() {","c})
            Dim Cols As Integer = Fields.GetLength(0)

            '1st row must be column names. 
            For i As Integer = 0 To Cols - 1
                dt.Columns.Add(rgx.Replace(Fields(i).ToUpper(), ""), GetType(String))
            Next

            Dim Row As DataRow
            For i As Integer = 1 To Lines.GetLength(0) - 1
                If Lines(i).Trim.Length = 0 Then
                    Continue For
                End If
                Fields = Lines(i).Split(New Char() {","c})
                If Fields.Length < Cols Then
                    Continue For
                End If
                Row = dt.NewRow()
                For j As Integer = 0 To Cols - 1
                    If Fields.Length <> Cols Then
                        Exit For
                    End If
                    Row(j) = rgx.Replace(Fields(j), "")
                Next
                dt.Rows.Add(Row)
            Next
            Database.Tenant.BulkLoadTableFromDataTable("temp_heatmap", dt)
            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function

    Protected Sub Uploadbtn_Click(sender As Object, e As EventArgs) Handles Uploadbtn.Click
    	If ControlFile.FileBytes.Length = 0 Then
    		Alert("Please select a file to upload!")
        	Return
        End If
        If Path.GetExtension(ControlFile.FileName).TrimStart(".").Trim.ToLower <> "csv" Then
            Alert("You have uploaded an invalid file.")
            Return
        End If
        Dim keys As New List(Of String)
        Dim temp_heatmap As New DataTable
        Dim sr As New StreamReader(ControlFile.FileContent)
        Dim sql As String = ""
        Dim cond As Boolean = True
        Dim Description As String ="Heatmap Setup has been uploaded via CSV."
        SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description) 
        
        If csv_to_datatableConvertion(sr, temp_heatmap) Then
            gridfield.DataSource = Nothing
            gridfield.Columns(1).Visible = True
            If (temp_heatmap.Columns.Contains("Value1") AndAlso temp_heatmap.Columns.Contains("Value2")) Then
                sql = "SELECT TOP 30 NULL as rowuid,Value1,Value2 ,Null AS ColorCode FROM temp_heatmap"
                cond = False
            ElseIf (temp_heatmap.Columns.Contains("Value1")) Then
                sql = " SELECT TOP 30 NULL AS rowuid,Value1,Null AS Value2,Null AS ColorCode FROM temp_heatmap"
            Else
            	Alert("Values attribute is missing in csv file.")
            	csvuploadinfo.Visible = True
            	Return
            End If
        End If
        FromLookup = False
        'Dim temp_HeatMapDB As DataTable = Database.Tenant.GetDataTable(sql)
        gridfield.DataSource = Database.Tenant.GetDataTable(sql)
        gridfield.DataBind()
        'gridfield.Columns(2).Visible = False
        'If cond Then
        '    gridfield.Columns(1).Visible = False
        'Else
        '    gridfield.Columns(1).Visible = True
        'End If
        ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "loadPopup('popupcolorpick','Set Color');", True)
        Alert("Heatmap setup uploaded successfully")
    End Sub

    Protected Sub btnlookup_Click(sender As Object, e As EventArgs) 'Handles btnlookup.Click
        If (CheckExistance() = True) Then
            lblkpLeft.Items.Clear()
            lblkpright.Items.Clear()
            lkpddl.FillFromSql("SELECT DISTINCT(LookupName) FROM ParcelDataLookup", True, "--Select--")
            ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "loadPopup('lookupload','Choose Lookup')", True)
        Else
            Return
        End If

    End Sub

    Protected Sub lkpddl_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lkpddl.SelectedIndexChanged
        lblkpright.Items.Clear()
        lblkpLeft.DataSource = Database.Tenant.GetDataTable("SELECT IdValue FROM ParcelDataLookup  WHERE LookupName={0}".SqlFormatString(lkpddl.SelectedValue))
        lblkpLeft.DataBind()
    End Sub
    Protected Sub btnLeft_Click(sender As Object, e As System.EventArgs) Handles btnLeft.Click
        Dim Selected As Boolean = False
        Dim count As Integer = 0
        For Each item As ListItem In lblkpright.Items
            If item.Selected Then
                Dim newItem As New ListItem(item.Text, item.Value)
                lblkpLeft.Items.Add(newItem)
                Selected = True
            End If
        Next
        If count > 0 Then
            count = lblkpright.Items.Count - 1
        End If
        If Selected = True Then
            lblkpright.Items.RemoveAt(lblkpright.SelectedIndex)
        End If
    End Sub
    Protected Sub btnRight_Click(sender As Object, e As System.EventArgs) Handles btnRight.Click
        Dim Selected As Boolean = False
        Dim count As Integer = lblkpright.Items.Count + 1
        '  itemCount.InnerText = count
        If count > 30 Then
            Alert("You cannot add more than 30 items")
            Return
        Else
            For Each item As ListItem In lblkpLeft.Items
                If item.Selected Then
                    Dim newItem As New ListItem(item.Text, item.Value)
                    lblkpright.Items.Add(newItem)
                    Selected = True

                End If
            Next
            '  itemCount.InnerText = count
        End If

        If Selected = True Then
            lblkpLeft.Items.RemoveAt(lblkpLeft.SelectedIndex)
        End If
    End Sub

    Protected Sub ddlcomparison_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlcomparison.SelectedIndexChanged
        If (ddlcomparison.SelectedValue = 2) Then
            LinkButton2.Enabled = False
        Else
            LinkButton2.Enabled = True
        End If
    End Sub

    Private Sub radioList_SelectedIndexChanged(sender As Object, e As EventArgs) Handles radioList.SelectedIndexChanged
        If radioList.SelectedItem.Value = "P" Then
            agrField.Visible = False
            parcelField.Visible = True
            aggregateField.Text = "False"
        Else
            agrField.Visible = True
            parcelField.Visible = False
            aggregateField.Text = "True"
        End If
    End Sub
    Protected Sub btnImport_Click(sender As Object, e As System.EventArgs) Handles btnImport.Click
    	Dim dt As New DataTable
		 If importFile.HasFile Then
		 	Try
                Dim config = New EnvironmentConfiguration
                config.ImportSettings(importFile.FileContent, ConfigFileType.HeatmapSettings, Database.Tenant, Membership.GetUser().ToString)
                Database.Tenant.Execute("UPDATE DatasourceField SET isHeatMap=0")
                Database.Tenant.Execute("UPDATE AggregateFieldSettings SET IncludeInHeatMap=0")
              	dt = Database.Tenant.GetDataTable("Select * From UI_HeatMap where IsAggregate=1") 
              	For Each dr As DataRow In dt.Rows
                	Database.Tenant.Execute("UPDATE AggregateFieldSettings SET IncludeInHeatMap=1 where ROWUID={0}".SqlFormatString(dr("FieldId")))
                Next
                dt = Database.Tenant.GetDataTable("Select * From UI_HeatMap where IsAggregate=0")
                For Each dr As DataRow In dt.Rows
                	Database.Tenant.Execute("UPDATE DatasourceField SET isHeatMap=1 where Id={0}".SqlFormatString(dr("FieldId")))
                Next
                Alert("Imported successfully.")
            Catch ex As Exception
               Alert(ex.Message)
               Return
            End Try
           LoadSavedGrid()
        End If   
    End Sub
    Protected Sub lnkbtnExp_Click(sender As Object, e As System.EventArgs) Handles lnkbtnExp.Click
    	Dim config = New EnvironmentConfiguration
    	config.ExportSettings( ConfigFileType.HeatmapSettings, Response, "HeatMapSettings", Database.Tenant)
    End Sub
End Class