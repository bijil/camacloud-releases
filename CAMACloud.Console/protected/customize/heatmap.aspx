﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master"
    CodeBehind="heatmap.aspx.vb" Inherits="CAMACloud.Console.heatmap" EnableEventValidation="false" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1><span>Heat Map Setup</span>
        
    </h1>
    <style type="text/css">
        .hiddencol {
            display: none;
        }

        .hideboundfield {
            display: none;
        }
        .break{
         max-width:300px;
         word-break : break-all;
        }
       .colorpicker{ z-index:50000; }
       info
{
	width:50em;
	border-left:8px solid #CFCFCF;
	padding-left:10px;
	line-height:15pt;
	padding-top:5px;
	padding-bottom:5px;
}
    </style>
    <script>
        $(function () {
            $('.pickcolor').colorpicker();

        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        var avgCount = 0;
        prm.add_endRequest(function () {
            $('.pickcolor').colorpicker();
        });
        function colorband() {
            var clrcode = $('.ddlcolor').val();
            var satPercentage = 20, satAdj = 1, valAdj = 0;
            var darkShade = 0, i = 0;
            var rowscount = $('.popupcolorpick .mGrid tr').length - 1;
            avgCount = Math.round(rowscount / 2);
            var incrVal = (1 / (avgCount + 1));
            satAdj = ((1 / avgCount - 0.01)) * 100;
            if (clrcode != "" && clrcode != " ") {
                if (clrcode == "1") {
                    $(".mGrid .pickcolor").each(function () {

                        var randColor = GetRandomColor();
                        var defRandVal = $(this).colorpicker('getValue', '#ffffff');
                        if (defRandVal == '#ffffff') {
                            $('.pickcolor input').val(randColor);
                            $(this).colorpicker('setValue', randColor);
                        }
                        else {
                            $(this).colorpicker('setValue', randColor);
                        }
                    });
                }
                else {

                    $(".mGrid .pickcolor").each(function () {
                        i = i + 1;
                        if (i < avgCount)
                            darkShade = 0;
                        else
                            darkShade = 1;

                        if (darkShade == "0") {
                            var colColor = hexColorWithSaturation(clrcode, satPercentage);
                            var defVal = $(this).colorpicker('getValue', '#ffffff');
                            if (defVal == '#ffffff') {
                                $('.pickcolor input').val(colColor);
                                $(this).colorpicker('setValue', colColor);
                                satPercentage = satPercentage + satAdj;
                            }
                            else {
                                $(this).colorpicker('setValue', colColor)
                                satPercentage = satPercentage + satAdj;

                            }
                        }
                        else {
                            var valColor = colorLuminance(clrcode, -valAdj);
                            var defdarkVal = $(this).colorpicker('getValue', '#ffffff');
                            if (defdarkVal == '#ffffff') {
                                $('.pickcolor input').val(valColor);
                                $(this).colorpicker('setValue', valColor);
                            }
                            else {
                                $(this).colorpicker('setValue', valColor);
                            }
                            valAdj = valAdj + incrVal;
                        }

                    });
                }
            }
        }
function revertColorBand() {
    var clrcode = $('.ddlcolor').val();
    var satPercentage = 90,satAdj = 1,valAdj = 0.60;
    var darkShade = 0,i = 0;
    var rowscount = $('.popupcolorpick .mGrid tr').length - 1;
    if(rowscount <=1) return;
    avgCount = Math.round(rowscount / 2);
    var incrVal = (1 / (avgCount + 1));
    satAdj = ((1 / (avgCount + 1) - 0.02)) * 100;
    if (clrcode != "") {
        if (clrcode == "1") {
            $(".mGrid .pickcolor").each(function() {
                var randColor = GetRandomColor();
                var defRandVal = $(this).colorpicker('getValue', '#ffffff');
                if (defRandVal == '#ffffff') {
                    $('.pickcolor input').val(randColor);
                    $(this).colorpicker('setValue', randColor);
                } else {
                    $(this).colorpicker('setValue', randColor);
                }
            });
        } else {
            $(".mGrid .pickcolor").each(function() {
                i = i + 1;
                if (i <= avgCount)
                    darkShade = 1;
                else
                    darkShade = 0;

                if (darkShade == "0") {
                    var colColor = hexColorWithSaturation(clrcode, satPercentage);
                    var defVal = $(this).colorpicker('getValue', '#ffffff');
                    if (defVal == '#ffffff') {
                        $('.pickcolor input').val(colColor);
                        $(this).colorpicker('setValue', colColor);
                        satPercentage = satPercentage - satAdj + 0.01;
                    } else {
                        $(this).colorpicker('setValue', colColor)
                        satPercentage = satPercentage - satAdj + 0.01;

                    }
                } else {
                    var valColor = colorLuminance(clrcode, -(valAdj + 0.01));
                    var defdarkVal = $(this).colorpicker('getValue', '#ffffff');
                    if (defdarkVal == '#ffffff') {
                        $('.pickcolor input').val(valColor);
                        $(this).colorpicker('setValue', valColor);
                    } else {
                        $(this).colorpicker('setValue', valColor);
                    }
                    valAdj = valAdj - (incrVal - 0.01);
                }

            });
        }
    }
}
        function hexColorWithSaturation(hex, percent) {
            if (!/^#([0-9a-f]{6})$/i.test(hex)) {
                throw ('Unexpected color format');
            }
            var r = parseInt(hex.substr(1, 2), 16),
        g = parseInt(hex.substr(3, 2), 16),
        b = parseInt(hex.substr(5, 2), 16),
        sorted = [r, g, b].sort(function (a, b) { return a - b; }),
        min = sorted[0],
        med = sorted[1],
        max = sorted[2];

            if (min == max) {
                return hex;
            }

            var max2 = max,
        rel = (max - med) / (med - min),
        min2 = max / 100 * (100 - percent),
        med2 = isFinite(rel) ? (rel * min2 + max2) / (rel + 1) : min2,
        rgb2hex = function (rgb) { return '#' + rgb[0].toString(16) + rgb[1].toString(16) + rgb[2].toString(16); },
        hex2;
            min2 = Math.round(min2);
            med2 = Math.round(med2);

            if (r == min) {
                hex2 = rgb2hex(g == med ? [min2, med2, max2] : [min2, max2, med2]);
            }
            else if (r == med) {
                hex2 = rgb2hex(g == max ? [med2, max2, min2] : [med2, min2, max2]);
            }
            else {
                hex2 = rgb2hex(g == min ? [max2, min2, med2] : [max2, med2, min2]);
            }

            return hex2;
        }

        function loadPopup(popupWindow, title, element) {
            var fldname =' - ' + $($(':nth-child(2)', $(element).parents('tr')[0])[0]).html();
            var windheight=document.documentElement.scrollHeight;            
            $('.' + popupWindow).dialog({
                modal: true,
                width: 610,
                height: 510,
                resizable: false,
                title: title + fldname,
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                    $(this).parent().css('z-index','5004')
                    setTimeout(function(){ 
	                    $('.ui-widget-overlay').css('height',$('body').height()); 
	                    $('.ui-widget-overlay').css('width','100%');
	                 }, 1000);
                },
                close: function () {
                    $('.ddlcolor').val('');
                }
            });
            $('.ui-widget-overlay').css('z-index','5003');
        }

        function hidePopup(poptoclose,event) {
            if (event.target.getAttribute("id") == "MainContent_MainContent_lkpSave") {
                var c = document.getElementById("lblkpright").children.length;
                if (c == 0) {
	                alert("Please choose any value");
	                return false;
                }
            }
            $('.' + poptoclose).dialog('close');
        }

        function ListBoxValid(sender, args) {
            var ctlDropDown = document.getElementById(sender.controltovalidate);
            args.IsValid = ctlDropDown.options.length > 0;
        }
        function Rand(min, max) {
            return parseInt(Math.random() * (max - min + 1), 10) + min;
        }

        function GetRandomColor() {
            var h = Rand(1, 360); // color hue between 1 and 360
            var s = Rand(30, 100); // saturation 30-100%
            var l = Rand(30, 70); // lightness 30-70%
            return 'hsl(' + h + ',' + s + '%,' + l + '%)';
        }
        function colorLuminance(hex, lum) {
            hex = String(hex).replace(/[^0-9a-f]/gi, "");
            if (hex.length < 6) {
                hex = hex.replace(/(.)/g, '$1$1');
            }
            lum = lum || 0;
            var rgb = "#",
                c;
            for (var i = 0; i < 3; ++i) {
                c = parseInt(hex.substr(i * 2, 2), 16);
                c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
                rgb += ("00" + c).substr(c.length);
            }
            return rgb;
        }
        var reverse=true;
        function reverseColor(){
        	if(reverse)
        		revertColorBand();
        	else
        		colorband();
        reverse = !reverse;
        }
    </script>

    
    <div class="heatmapSettings">

        <contenttemplate>
        <div><p class="info">
        Configure CAMA fields to display as a layer in CAMACloud's HeatMaps. Up to 10 fields/layers can be configured.
    </p></div>
            <table style="float:right;">
        <tr>
        <td>
	     	<asp:LinkButton runat="server" ID="lnkbtnExp" Text="Export Settings" style="padding:3px;margin-left:10px;font-size: 15px;" />	
	     </td>
	     <td>
	     <asp:LinkButton runat="server" ID="lnkbtnImp" Text="Import Settings" style="padding:3px;margin-left:10px;font-size: 15px; margin-right:10px;" OnClientClick="document.getElementById('importFile').click();return false;"/>
	     </td>
	    <div style="display:none;">
		      <asp:FileUpload runat="server" ID="importFile" ClientIDMode="Static" onchange="document.getElementById('btnImport').click()"  accept="application/xml" />
			  <asp:Button runat="server" ID="btnImport" ClientIDMode="Static" />
		</div>
		</tr>
        </table>
            
        <table>                
                <tr><td><asp:RadioButtonList ID="radioList" runat="server"  RepeatDirection="Horizontal" AutoPostBack ="true">
                    <asp:ListItem Selected="True" Value="P" Text="Parcel Fields"></asp:ListItem>
                    <asp:ListItem Text="Aggregate Fields" Value="A" Selected="False" ></asp:ListItem>
                        </asp:RadioButtonList>

                

        
            
            
            <div id="agrField" runat ="server" visible ="false" > 
                    <tr >  <td style="padding-right:80px; width: 160px;">   
                   <label id="lblagr" style="margin-top: 20px; margin-bottom: 20px;">
                   Select Aggregate Field: </label></td>
                <td><asp:DropDownList ID="agrddl" runat="server" AutoPostBack="false" Style="width: 300px;">
                                   </asp:DropDownList>
                     </td></tr>
                </div>
                <div id="parcelField" runat="server"><tr>
                 <tr>  <td style="padding-right:80px;  width: 160px;">   
                   <label id="Label2" style="margin-top: 20px; margin-bottom: 20px;">
                   Select Parcel Related Field: </label></td>
                <td><asp:DropDownList ID="ddlfields" runat="server" AutoPostBack="false" Style="width: 300px;">
                                   </asp:DropDownList>
                     </td></tr>
                </div>
                    <asp:Label ID="aggregateField" runat ="server"  Visible ="false" ></asp:Label>
                <tr>
                    <td style="padding-right:80px;">
             <label id="Label3" style="margin-top: 20px; margin-bottom: 20px;">
                   Comparison Type: </label></td>
           <td> <asp:DropDownList ID="ddlcomparison" runat="server" AutoPostBack="True" style="width:200px;">
                <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                <asp:ListItem Text="Equality" Value="1"></asp:ListItem>
                <asp:ListItem Text="Range" Value="2"></asp:ListItem>
            </asp:DropDownList></td></tr>
           <tr> <td><label id="Label4" style="margin-top: 20px; margin-bottom: 20px;">
                  Select Type </label></td>
             <td >  <asp:LinkButton ID="LinkButton1" runat="server" OnClick="btncsv_Click" >Import From CSV</asp:LinkButton>
  <asp:LinkButton ID="LinkButton2" runat="server" OnClick="btnlookup_Click" Enabled="true">Add From Lookup</asp:LinkButton></td>
               </tr> </table>
            <div id="csvuploadinfo" runat="server"><p class="info">
        You can set HeatMap fields by uploading a control list. The control list should be in CSV format, with the
        following fields.
    </p>

    <p runat="server" id="pNoCK">
        Please set the common key fields before using this feature.
    </p>
    <p class="info">
        <strong>Value1 </strong>  For Range comparison type this is the first range value. For Equality this is the value to be equated.<br />
         <strong>Value2 </strong>  For Range comparison type this is the second range value. For Equality this value should be avoided.<br />
           </p>
    <p>
        <strong>Note :</strong> Select control file of (*.csv) format. The maximum size
        of uploaded file should be 4MB.<br />
        <br />
    </p>
    <p style="margin:5px 0px;padding:12px 5px;background:#DFDFDF;border:1px solid #CFCFCF;width:900px;">
        <asp:FileUpload runat="server" ID="ControlFile" Width="400px" accept=".csv" />
    </p>

     <p>  <asp:Button runat="server" ID="Uploadbtn" Text=" Upload " Height="40px" Width="120px"
            Font-Bold="true" /></p> 
                <br />
    </div>

 
        </contenttemplate>

    </div>
    <asp:Label ID="lblmapid" runat="server" Visible="false"></asp:Label>
    <div class="popupcolorpick" style="display: none">
        <asp:UpdatePanel runat="server" ID="Updatepanelcolor">
            <ContentTemplate>
                <table style="padding:9px 0px 0px 68px; border-collapse: separate; border-spacing: 0 8px;">
                    <%-- <asp:Panel runat="server" ID="Panel1" CssClass="color-edit-panel" <%--Style="width: 500px; height: 300px; margin-top: 14px;"--%>
                    <asp:Panel runat="server" ID="pnlcolorEdit" CssClass="color-edit-panel">
                        <tr>
                            <div id="displayLblDiv" runat="server">
                                <td>
                                    <label id="Label2" style="padding: 0px 20px 0px 3px;">Display Label  </label>
                                    <asp:TextBox runat="server" ID="displayLbl" MaxLength="100"></asp:TextBox></td>
                            </div>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <div id="note" runat="server"><strong>Note:</strong>If you change the Display Label value here, it will edit the Data Collection Card label for this field as well.</div>
                            </td>
                        </tr>
                        <tr>
                            <div>
                                <td>
                                    <label id="Label1" style="padding: 0px 22px 0px 100px;">Pick a color</label>
                                    <asp:DropDownList runat="server" ID="ddlColor_" class="ddlcolor" width="240px" onchange="colorband()">
                                        <asp:ListItem Text="-- Select --" Value=""></asp:ListItem>
                                        <asp:ListItem Text="Red" value="#ff0000"></asp:ListItem>
                                        <asp:ListItem Text="Blue" value="#0000ff"></asp:ListItem>
                                        <asp:ListItem Text="Green" Value="#00FF00"></asp:ListItem>
                                        <asp:ListItem Text="Yellow" value="#FFFF00"></asp:ListItem>
                                        <asp:ListItem Text="Violet" value="#ee82ee"></asp:ListItem>
                                        <asp:ListItem Text="Cyan" value="#00FFFF"></asp:ListItem>
                                    </asp:DropDownList>
                                    <label class="revert" onclick='reverseColor()' style="font-size: 17px;cursor: pointer;padding: 8px;">&#x1F503;</label></td>
                            </div>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:GridView runat="server" ID="gridfield" AutoGenerateColumns="False" DataKeyNames="RowUID">
                                    <Columns>
                                        <asp:BoundField DataField="Value1" HeaderText="Value 1" ItemStyle-Width="180px" />
                                        <asp:BoundField DataField="Value2" HeaderText="Value 2" ItemStyle-Width="180px" />
                                        <asp:TemplateField HeaderText="Color Code">
                                        <ItemStyle Width="205px" />
                                              <ItemTemplate>
                                                <div class="input-group pickcolor">
                                                    <asp:TextBox runat="server" ID="colorcode" HeaderText="Color Code" ItemStyle-Width="120px"
                                                        value='<%# Eval("ColorCode") %>' />
                                                    <span class="input-group-addon"><i style="margin-top: -2px; margin-left:1%;"></i></span>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                            <div class="savecolor">
                                <td colspan="2" style="padding-left: 130px;">
                                    <asp:Button ID="btnsaveclr" runat="server" Text="Save" />
                                    <asp:Button ID="btncancel" runat="server" Text="Cancel" OnClientClick="hidePopup('popupcolorpick',event); " /></td>
                            </div>
                        </tr>

                    </asp:Panel>
                </table>
                <div id="maskinglayer"  style="display:none; height: 99%; width: 97%; position: absolute; top: 0;background: #f2f5f7; opacity: 0.5;">
                 <div style="z-index: 20;top: 0%;width: 97%;height: auto;bottom: 0%;position: absolute;">
				   <span style="margin-left: 45%; margin-top: 30%; align-items: center;position:relative;top:34%" class="app-mask-layer-info">
				   <img src="/App_Static/images/loadingimg.gif"><br>
				   <span style="color: black; margin-left: 45%; margin-top: 20px;">Please wait...</span></span>
				  </div>
				 </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div class="lookupload" style="display: none;padding: 16px;">
        <asp:UpdatePanel ID="lookupUpdate" runat="server">
            <ContentTemplate>
                <table>
                    <tr>
                      <td>&nbsp;
                        </td>
                        <td style="padding: 0px 0px 0px 145px;">
                        Select Lookup <asp:DropDownList ID="lkpddl" runat="server" AutoPostBack="True" Style="width: 200px; margin-left: 16px;"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table style="margin-top: 50px;">
                                <tr>
                                    <td>
                                        <asp:ListBox ID="lblkpLeft" runat="server" Rows="18" Width="250px" DataTextField="IdValue" DataValueField="IdValue"></asp:ListBox>
                                    </td>
                                    <td>
                                        <table style="margin-top: 114px;">
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnRight" runat="server" Text=">>  " ToolTip="Move selected to Right" Style="font-weight: bold; font-stretch: extra-condensed;" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnLeft" runat="server" Text="<<  " ToolTip="Move selected to Left" Style="font-weight: bold; font-stretch: extra-condensed;" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>

                                    <td>
                                        <asp:ListBox ID="lblkpright" runat="server" Rows="18" Width="250px" ClientIDMode="Static"></asp:ListBox>
                                        <asp:CustomValidator ID="cvlkpright" runat="server" ValidationGroup="lkpfld" Display="Dynamic" ValidateEmptyText="True" Style="font-weight: bold;" ControlToValidate="lblkpright" ForeColor="Red" ErrorMessage="*" ClientValidationFunction="ListBoxValid"></asp:CustomValidator>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                        </td>
                         <td style="padding: 26px 0px 0px 165px;">
                            <asp:Button ID="lkpSave" runat="server" Text="Save" style="margin-right: 17px; width:100px" ValidationGroup="UserProp" OnClientClick="hidePopup('lookupload',event);" />
                            <asp:Button ID="lkpCancel" runat="server" Text="Cancel" style="width:100px" OnClientClick="hidePopup('lookupload',event);" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div class="savedDiv" style="margin-top: 30px;">
        <asp:UpdatePanel runat="server" ID="savedGridpanel">
            <ContentTemplate>
                <asp:GridView runat="server" ID="savedgrid" AutoGenerateColumns="False" DataKeyNames="id">
                    <Columns>
                        <asp:TemplateField>
                            <ItemStyle Width="25px" />
                            <ItemTemplate>
                                <%# (Container.DataItemIndex)+1 %>.
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="AssignedName" HeaderText="Field Name" ItemStyle-Width="300px" />
                        <asp:BoundField DataField="DisplayLabel" HeaderText="Display Label" ItemStyle-CssClass = "break" ItemStyle-Width="300px"  />
                        <asp:TemplateField>
                            <ItemStyle Width="80px" />
                            <ItemTemplate>
                                <asp:LinkButton runat="server" ID="lblEdit" Text="Edit" CommandName='EditItem' CommandArgument='<%# Eval("Id")%>'
                                    OnClientClick="$('#maskinglayer').show();loadPopup('popupcolorpick','Set Color', this); " />
                                <asp:LinkButton runat="server" ID="lblDelete" Text="Delete" CommandName='DeleteItem'
                                    CommandArgument='<%# Eval("Id") %>' OnClientClick='return confirm("Are you sure you want to delete?")' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
