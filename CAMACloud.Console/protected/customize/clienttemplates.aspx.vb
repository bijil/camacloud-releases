﻿
Partial Class protected_customize_clienttemplates
    Inherits System.Web.UI.Page

#Region "XMLTemplates"


    Dim prcContent As XElement = <div class="prc-format">
                                     <!-- Add your PRC layout and contents here -->
                                 </div>

    Dim sortScreenNormal =
                <table style="width: 100%; border-spacing: 0px;">
                    <tr>
                        <td style="width: 118px; vertical-align: top;">
                            <div class="sort-screen-photo" imgpid="${Id}" pkey="${KeyValue1}">
                            </div>
                        </td>
                        <td style="vertical-align: top;">
                            <div class="ssc-norm-status" style="">
                                <span style="display: ${Priority:showIfAlert};"><a class="alert32"></a></span><span
                                    style="display: ${Priority:showIfPriority};"><a class="priority32"></a></span>
                                <span style="display: ${Reviewed:showIfTrue};"><a class="comp32"></a></span>
                            </div>
                            <div class="ssc-parcel-id">
                                ${KeyValue1}</div>
                            <div>
                                <table style="width:95%;border-spacing:0px;">
                                    <tr>
                                        <td><span class="address">${StreetAddress}<span class="hidden"> - ${DistanceFromMe:toImperialDistance}</span></span></td>
                                        <td class="r"><span class="address b">${KeyValue2}</span></td>
                                    </tr>
                                </table>
                            </div>
                            <div>
                                <span class="alert-normal-view">${ParcelAlert}</span>
                            </div>
                        </td>
                    </tr>
                </table>


    Dim sortScreenExpanded =
					<div>
					    <table style="width: 100%;" class="b bb mb5">
					        <tr>
					            <td style="width: 32px; display: ${Reviewed:showIfTrue};">
					                <a class="comp32"></a>
					            </td>
					            <td style="width: 32px; display: ${Priority:showIfAlert};">
					                <a class="alert32"></a>
					            </td>
					            <td style="width: 32px; display: ${Priority:showIfPriority};">
					                <a class="priority32"></a>
					            </td>
					            <td>
					                                        Nbhd: ${NeighborhoodNo}
					                                    </td>
					            <td>
					                                        PROP ID: <a onclick="showParcel(${Id})">${KeyValue1}</a>
					            </td>
					            <td>
					                                        Sale Price: 
					                                    </td>
					            <td>
					                                        TASP: 
					                                    </td>
					            <td style="width: 32px;">
					                <a class="close32"></a>
					            </td>
					        </tr>
					    </table>
					    <div class="boxed">
					        <div style="width: 170px; text-align: center;">
					            <div class="sort-screen-photo sort-screen-photo-large" imgpid="${Id}" pkey="${KeyValue1}">
					            </div>
					            <div>
					                <select class="select-flag" pid="${Id}" selected="${FieldAlertType}">
					                    <option value="0">No Flag</option>
					                    <option value="1">New Construction</option>
					                    <option value="2">Demolition</option>
					                    <option value="3">Data Discrepancy</option>
					                </select></div>
					            <a class="mark-complete mc-off text-button" pid="${Id}"></a>
					        </div>
					        <div class="extend-panel" style="margin-left: 10px;">
					            <table style="width: 100%">
					                <tr>
					                    <td style="width: 50%">
					                                ${StreetAddress}
					                            </td>
					                    <td context="">
					
					                    </td>
					                </tr>
					            </table>
					            <table style="width: 100%;" class="c4">
					                <tr>
					                    <td>
					                                Class: 
					                            </td>
					                    <td>
					                                CDU:
					                            </td>
					                    <td>
					                                Roof: 
					                            </td>
					                    <td>
					                                Sq. Ft.: 
					                            </td>
					                </tr>
					            </table>
					            <div id="estimate-chart">
					                <table style="width: 100%; border-collapse: collapse; border-color: 1px solid #CFCFCF;" border="1" class="estimate-chart-table">
					                    <tr>
					                        <td colspan="6" class="b c">2013 Estimates of Value </td>
					                    </tr>
					                    <tr class="b c">
					                        <td style="width: 10%;"></td>
					                        <td style="width: 18%;">ASSESSED</td>
					                        <td style="width: 18%;">APPRAISED</td>
					                        <td style="width: 18%;">COST </td>
					                        <td style="width: 18%;">MARKET</td>
					                        <td style="width: 18%;">OPINION</td>
					                    </tr>
					                    <tr>
					                        <td class="b">Land </td>
					                        <td class="d"></td>
					                        <td class="d"></td>
					                        <td class="d">${cost_land_hstd_val+cost_land_non_hstd_val} </td>
					                        <td class="d">${land_hstd_val+land_non_hstd_val}</td>
					                        <td class="d"></td>
					                    </tr>
					                    <tr>
					                        <td class="b">Impr </td>
					                        <td class="d"></td>
					                        <td class="d"></td>
					                        <td class="d">${cost_imprv_hstd_val + cost_imprv_non_hstd_val} </td>
					                        <td class="d">${imprv_hstd_val + imprv_non_hstd_val} </td>
					                        <td class="d"></td>
					                    </tr>
					                    <tr>
					                        <td class="b">Total </td>
					                        <td class="d">${assessed_val}</td>
					                        <td class="d">${appraised_val}</td>
					                        <td class="d">${cost_value} </td>
					                        <td class="d">${market} </td>
					                        <td class="d"></td>
					                    </tr>
					                    <tr>
					                        <td class="b">$/SF </td>
					                        <td class="d"></td>
					                        <td class="d"></td>
					                        <td class="d"></td>
					                        <td class="d"></td>
					                        <td class="d"></td>
					                    </tr>
					                    <tr class="sort-screen-select" pid="${Id}" selectedtype="${SelectedAppraisalType}">
					                        <td></td>
					                        <td></td>
					                        <td></td>
					                        <td></td>
					                        <td></td>
					                        <td></td>
					                    </tr>
					                </table>
					            </div>
					        </div>
					    </div>
					</div>


    Dim estimateChart =
			<table style="width: 100%; border-collapse: collapse; border-color: #CFCFCF;" border="1" class="estimate-chart-table">
			    <tr>
			        <td colspan="6" class="b c">2013 Estimates of Value </td>
			    </tr>
			    <tr class="b c">
			        <td style="width: 10%;"></td>
			        <td style="width: 18%;">ASSESSED</td>
			        <td style="width: 18%;">APPRAISED</td>
			        <td style="width: 18%;">COST </td>
			        <td style="width: 18%;">MARKET</td>
			        <td style="width: 18%;">OPINION</td>
			    </tr>
			    <tr>
			        <td class="b">Land </td>
			        <td class="d"></td>
			        <td class="d"></td>
			        <td class="d">${cost_land_hstd_val+cost_land_non_hstd_val} </td>
			        <td class="d">${land_hstd_val+land_non_hstd_val}</td>
			        <td class="d"></td>
			    </tr>
			    <tr>
			        <td class="b">Impr </td>
			        <td class="d"></td>
			        <td class="d"></td>
			        <td class="d">${cost_imprv_hstd_val + cost_imprv_non_hstd_val} </td>
			        <td class="d">${imprv_hstd_val + imprv_non_hstd_val} </td>
			        <td class="d"></td>
			    </tr>
			    <tr>
			        <td class="b">Total </td>
			        <td class="d">${assessed_val}</td>
			        <td class="d">${appraised_val}</td>
			        <td class="d">${cost_value} </td>
			        <td class="d">${market} </td>
			        <td class="d"></td>
			    </tr>
			    <tr>
			        <td class="b">$/SF </td>
			        <td class="d"></td>
			        <td class="d"></td>
			        <td class="d"></td>
			        <td class="d"></td>
			        <td class="d"></td>
			    </tr>
			    <tr class="sort-screen-select" pid="${Id}" selectedtype="${SelectedAppraisalType}">
			        <td></td>
			        <td></td>
			        <td></td>
			        <td></td>
			        <td></td>
			        <td></td>
			    </tr>
			</table>

	Dim miniSortScreenNormal = 
					<table style="width:100%; border:none">
					    <tr>
					        <td rowspan="3" style="width: 118px;">    
					        	<div class="sort-screen-photo" imgpid="${Id}" pkey="${KeyValue1}"></div>
					        </td>
					        <td style="width:42%">  
					        	<span class="ssc-norm-status" style="">
					                <span style="display: ${CC_Priority:showIfAlert};"><a class="alert32"></a></span>
					                <span style="display: ${CC_Priority:showIfPriority};"><a class="priority32"></a></span>
					                <span style="display: ${AnyChildReviewed};"><a class="comp32"></a></span>
					                <span style="display: ${Reviewed:showIfTrue};"><a class="comp32"></a></span>
					            </span>
					            <span style="font-size:20px"> Parcel No:  <label onclick="showParcel(${Id})" class="ssc-parcel-id">${KeyValue1}</label></span>
					        </td>
					        <td><span>NBHD: ${NeighborhoodNo}</span><span style="float: right;margin-right: 20px;">BPP Count : <span class="Bppcount">${BPPCount}</span></span></td>
					    </tr>
					    <tr>
					        <td colspan ="2"><span style="font-size: 14px;white-space: nowrap" class="address">${StreetAddress}<span class="hidden"> - ${DistanceFromMe:toImperialDistance}</span></span></td>			         
					    </tr>
					    <tr>
					        <td colspan ="2" ><label class="nav-buttons" style="width: 100%;float: left; border-bottom: 1px solid silver; padding-bottom: 10px;    margin-top: 11px;"><a class="mini-sort-screen-bpp" pid = "${Id}">+</a></label></td>
					    </tr>
					</table>

	Dim miniSortScreenExpanded = 
		<div>
			<table style="width: 100%;" class="b bb mb5">
				<tr>
					<td style="width: 32px; display: ${Reviewed:showIfTrue};"><a class="comp32"></a></td>
					<td style="width: 32px; display: ${CC_Priority:showIfAlert};"><a class="alert32"></a></td>
					<td style="width: 32px; display: ${CC_Priority:showIfPriority};"><a class="priority32"></a></td>
					<td>Nbhd: ${NeighborhoodNo}</td>
					<td>PROP ID: <a onclick="showParcel(${Id})">${KeyValue1}</a></td>			 
					<td>Sale Price: </td>
					<td>TASP: </td>
					<td style="width: 32px;"><a class="close32"></a></td>
				</tr>
			</table>
			<div class="boxed">
				<div style="text-align: center;">
					<div class="sort-screen-photo sort-screen-photo-large" imgpid="${Id}" pkey="${KeyValue1}"></div>
					<div>
						<label>BPP Count : <span class="Bppcount">${BPPCount}</span></label>
						<label Class="nav-buttons" style="width: 100%;float: left; border-bottom: 1px solid silver; padding-bottom: 10px; margin-top: 15px;"><a onclick ="createNewParcel(${Id})">+</a></label>
					</div>
				</div>
				<div class="extend-panel" style="margin-left: 10px;">
					<table style="width: 100%">
						<tr>
							<td style="width: 50%">${StreetAddress}	</td>
						</tr>
					</table>
					<table style="width: 100%;" class="c4">	  
						<tr>
							<td>Class: </td>
							<td>CDU: </td>
							<td>Roof: </td>
							<td>Sq. Ft.: </td>
						</tr>
					</table>
					<div id="estimate-chart">
						<table style="width: 100%; border-collapse: collapse; border-color: #CFCFCF;" border="1" class="estimate-chart-table">
							<tr>
								<td colspan="6" class="b c">2013 Estimates of Value </td>
							</tr>
							<tr class="b c">
								<td style="width: 10%;"></td>
								<td style="width: 18%;">ASSESSED</td>
								<td style="width: 18%;">APPRAISED</td>
								<td style="width: 18%;">COST </td>
								<td style="width: 18%;">MARKET</td>
								<td style="width: 18%;">OPINION</td>
							</tr>
							<tr>
								<td class="b">Land </td>
								<td class="d"></td>
								<td class="d"></td>
								<td class="d">${cost_land_hstd_val+cost_land_non_hstd_val} </td>
								<td class="d">${land_hstd_val+land_non_hstd_val}</td>
								<td class="d"></td>
							</tr>
							<tr>
								<td class="b">Impr </td>
								<td class="d"> </td>
								<td class="d"> </td>
								<td class="d">${cost_imprv_hstd_val + cost_imprv_non_hstd_val} </td>
								<td class="d">${imprv_hstd_val + imprv_non_hstd_val} </td>
								<td class="d"> </td>
							</tr>
							<tr>
								<td class="b">Total </td>
								<td class="d">${assessed_val}</td>
								<td class="d">${appraised_val}</td>
								<td class="d">${cost_value} </td>
								<td class="d">${market} </td>
								<td class="d"></td>
							</tr>
							<tr>
								<td class="b">$/SF </td>
								<td class="d"></td>
								<td class="d"></td>
								<td class="d"> </td>
								<td class="d"></td>
								<td class="d"></td>
							</tr>
							<tr class="sort-screen-select" pid="${Id}" selectedtype="${SelectedAppraisalType}">
								<td> </td>
								<td ></td>
								<td ></td>
								<td ></td>
								<td ></td>
								<td ></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>

#End Region

#Region "Codes"

    Sub DownloadTemplate(tag As String)
        Dim defaultContent As String = ""
        Select Case tag
            Case "property-record-card", "dtr-property-record-card","property-record-card-PersonalProperty","sketch-validation-prc"
                defaultContent = prcContent.ToString
            Case "sort-screen-normal","sort-screen-normal-PersonalProperty"
                defaultContent = sortScreenNormal.ToString
            Case "sort-screen-expanded","sort-screen-expanded-PersonalProperty"
                defaultContent = sortScreenExpanded.ToString
            Case "estimate-chart"
                defaultContent = estimateChart.ToString
            Case "parcel-header"
            	defaultContent = ""
            Case "mini-sort-screen-normal-PersonalProperty"
            	defaultContent = miniSortScreenNormal.ToString
            Case "mini-sort-screen-expanded-PersonalProperty"
            	defaultContent = miniSortScreenExpanded.ToString
        End Select
        Dim template As String = ClientSettings.ContentTemplate(tag)
        If template.Trim = "" Then
            DownloadTemplateToResponse(defaultContent, tag)
        Else
            DownloadTemplateToResponse(template, tag)
        End If
    End Sub

    Sub DownloadTemplateToResponse(content As String, fName As String)
        Response.Clear()
        o = Database.System.GetTopRow("SELECT * FROM Organization WHERE Id = " & HttpContext.Current.GetCAMASession().OrganizationId)
        Dim fileName As String = o.GetString("Name").ToLower() + "-" + fName
        Response.ContentType = "text/html; charset=utf-8"
        Response.AddHeader("Content-Disposition", "attachment;filename=""" & fileName & ".htm""")
        Dim bytes() As Byte = System.Text.Encoding.UTF8.GetBytes(content)
        Response.OutputStream.Write(bytes, 0, bytes.Length)
        Response.Flush()
        Response.End()
    End Sub

	Sub UploadTemplate(name As String, tag As String, file As FileUpload) 
		If Not(file.FileName.ToLower.EndsWith("txt") or file.FileName.ToLower.EndsWith("htm") Or file.FileName.ToLower.EndsWith("html") ) Then
			Alert("You have uploaded an invalid file.")
			Return
		End If
			
		Dim template As String = System.Text.Encoding.UTF8.GetString(file.FileBytes)

		If template = "" Then
			If tag = "parcel-header" Then
				
			Else
            	Alert("You have uploaded an empty file.")
            	Return
            End If
        End If
        If template.ToLower.Contains("<body") Or template.ToLower.Contains("<html") Or template.ToLower.Contains("<head") Or template.ToLower.Contains("<script") Or template.ToLower.Contains("<link") Then
            Alert("You have uploaded an invalid file.")
            Return
        End If

        Database.Tenant.Execute("IF NOT EXISTS (SELECT * FROM ClientTemplates WHERE Name = " + tag.ToSqlValue + ") INSERT INTO ClientTemplates (Name) VALUES (" + tag.ToSqlValue + ");")
        Database.Tenant.Execute("UPDATE ClientTemplates SET TemplateContent = {1} WHERE Name = {0}", tag.ToSqlValue, template.ToSqlValue)
        ClientSettings.CustomizationDate = Now
        Dim Description As String = name + " content updated."
        SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
        
        Alert(name + " content updated.")
        
    End Sub

#End Region
    Public o As DataRow
    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim dt As New List(Of NameCodePair)
            dt.Add(New NameCodePair("Digital PRC", "property-record-card"))
            dt.Add(New NameCodePair("SortScreen (Normal)", "sort-screen-normal"))
            dt.Add(New NameCodePair("SortScreen (Expanded)", "sort-screen-expanded"))
            dt.Add(New NameCodePair("Estimate Chart", "estimate-chart"))
            dt.Add(New NameCodePair("" + CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName() + " Profile", "nbhd-profile"))
            dt.Add(New NameCodePair("Parcel Header", "parcel-header"))
            If User.IsInRole("SketchValidation") Then 
            	dt.Add(New NameCodePair("Sketch Validation PRC", "sketch-validation-prc"))
            End If 
            If User.IsInRole("DTR") Then
                dt.Add(New NameCodePair("Desktop Review PRC", "dtr-property-record-card"))
            End If
            Dim BPPEnabled As String= Database.Tenant.GetStringValue("SELECT BPPEnabled FROM Application")
            If (BPPEnabled = "1" OrElse BPPEnabled = "True" OrElse BPPEnabled = "true")  Then
            	dt.Add(New NameCodePair("Digital PRC For Personal Property", "property-record-card-PersonalProperty"))
            	dt.Add(New NameCodePair("SortScreen (Normal) For Personal Property", "sort-screen-normal-PersonalProperty"))
            	dt.Add(New NameCodePair("SortScreen (Expanded) For Personal Property", "sort-screen-expanded-PersonalProperty"))
            	dt.Add(New NameCodePair("MiniSortScreen (Normal)", "mini-sort-screen-normal-PersonalProperty"))
            	dt.Add(New NameCodePair("MiniSortScreen (Expanded)", "mini-sort-screen-expanded-PersonalProperty"))
            End If
            gvTemplate.DataSource = dt
            gvTemplate.DataBind()
        End If
    End Sub

    Public Class NameCodePair
        Public Property Name
        Public Property Code
        Public Sub New(name As String, value As String)
            Me.Name = name
            Me.Code = value
        End Sub
    End Class

    Private Sub gvTemplate_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvTemplate.RowCommand
        Select Case e.CommandName
            Case "Download"
                DownloadTemplate(e.CommandArgument)
            Case "Upload"
                Dim fu As FileUpload = e.GetControl(Of FileUpload)(gvTemplate, "File")
                If IsEmpty(fu.FileName) Then
                    Alert("Please select the file to be uploaded.")
                Else
					UploadTemplate(e.GetHiddenValue("TName"), e.CommandArgument, fu)
					ClientSettings.LastClientTemplateUpdateDate = Now
					NotificationMailer.SendNotificationToVendorCDS(Membership.GetUser().ToString, 2, "Client Templates")
                End If
        End Select
    End Sub
End Class
