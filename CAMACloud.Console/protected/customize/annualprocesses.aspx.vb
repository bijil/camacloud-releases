﻿Imports CAMACloud.BusinessLogic
	Public Class AnnualProcesses
		Inherits System.Web.UI.Page
		
		Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
			If Not IsPostBack Then
				setDate()
			End If	
		End Sub
	Sub setDate()
		Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM APPLICATION WHERE ROWID = 1")
		Dim nylDay = dr("NYLDate")
		Dim currentDate As Date = DateTime.Now
		If nylDay.ToString <> "" Then
			Dim nyld As Date = nylDay
			If nyld > currentDate Then
				Dim nylDt = nyld.ToString("yyyy-MM-dd")
				nylDate.Text = nylDt
			End If
		End If
	End Sub
	'Private Sub nylDate_TextChanged(sender As Object,e As EventArgs) Handles nylDate.TextChanged
	Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
			Dim nDate As String = nylDate.Text
			Dim nDateSplit() As String
			'Dim currentDate As Date = DateTime.Now
			If nDate <> "" Then
				nDateSplit = nDate.Split("-")
				'Dim nDay As Date = nDate
				'If  nDay > currentDate  Then
				If(nDateSplit(0) > 1900 And nDateSplit(0) < 2900) Then
					Database.Tenant.Execute("UPDATE Application SET NYLDate = '"+nDate+"' WHERE ROWID = 1 ")					
				End If
				Dim Description As String = "New Date("+nylDate.Text+") added in Annual Processes."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
				Response.Redirect("~/protected/customize/annualprocesses.aspx")
				
			Else
				Alert("Please select a valid date.")
				setDate()
			End If	
		End Sub
	Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
		Database.Tenant.Execute("UPDATE Application SET NYLDate = NULL WHERE ROWID = 1 ")
		Dim Description As String = "<i>Date cleared in Annual Processes.</i>"
		SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
		Response.Redirect("~/protected/customize/annualprocesses.aspx")

	End Sub
	Private Sub lbResetAllQCParcels_Click(sender As Object, e As System.EventArgs) Handles lbResetAllQCParcels.Click
    	Try
    		Dim pCount = Database.Tenant.GetIntegerValue("SELECT DISTINCT p.Id INTO #changes FROM Parcel p INNER JOIN ParcelChanges pc ON p.Id = pc.ParcelId INNER JOIN DataSourceField f ON pc.FieldId = f.Id WHERE f.IsCustomField = 0 AND p.FailedOnDownSync = 0 AND p.QC = 1 AND p.Reviewed = 1 AND pc.Action <> 'PhotoFlagMain' GROUP BY p.Id HAVING COUNT(pc.Id) > 0;SELECT DISTINCT p.Id INTO #images FROM Parcel p INNER JOIN ParcelImages pi ON p.Id = pi.ParcelId WHERE p.QC is null and pi.DownSynced =0;SELECT COUNT(*) FROM Parcel p LEFT JOIN #changes pci ON p.Id = pci.Id LEFT JOIN #images img ON p.Id = img.Id WHERE (pci.Id IS NOT NULL OR img.Id IS NOT NULL);Drop table #changes;Drop table #images;")
			RunScript("warnResetAllParcels(" + pCount.ToString() + ")")
			Dim Description As String = "Annual Process completed"
			SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
		Catch ex As Exception
            Alert(ex.Message)
            Return
        End Try
		 End Sub 
		 
		Protected Sub btnHidden_Click(sender As Object, e As EventArgs) Handles btnHidden.Click
	        proceedToResetAllParcels()
	    End Sub
	    
	    Private Sub proceedToResetAllParcels()
	    	Try
	    		e_("EXEC cc_ResetAllParcels {0}", True, Membership.GetUser().ToString())
	    	Catch ex As Exception    		
	            Alert(ex.Message)
	            Return
	    	End Try
	        Alert("All parcels have been reset successfully.")
	    End Sub
		 
	End Class