﻿Public Class Validations
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
        	BindSourceTables()
            ViewState("IsAscendingSort") = True
            If Session("GridViewSelectedPage") IsNot Nothing Then
                gvValidations.PageSize = Convert.ToInt32(Session("GridViewSelectedPage"))
                ddlPageSize.SelectedValue = Convert.ToInt32(Session("GridViewSelectedPage"))
                Session.Remove("GridViewSelectedPage")
            End If
            If Session("GridViewPageIndex") IsNot Nothing Then
                gvValidations.PageIndex = Convert.ToInt32(Session("GridViewPageIndex"))
                Session.Remove("GridViewPageIndex")
            End If
            LoadValidations()
            ClearFields()
        End If
    End Sub

    Private Sub btnValidations_Click(sender As Object, e As System.EventArgs) Handles btnValidations.Click
        If (Page.IsValid) Then
            If (hcid.Value = "") Then
                Dim LatestOrdinal As Integer = Database.Tenant.GetIntegerValue(" select max(Ordinal) from ClientValidation")
                If LatestOrdinal = 0 Then
                    LatestOrdinal = 10
                Else
                    LatestOrdinal += 10
                End If
                If txtErrorMessage.Text.Length > 500 Then
                	Alert("Exceeding maximum number of characters.Please short your ErrorMessage to 500 characters or less.")
                	Return
            	Else If txtNote.Text.Length > 500 Then
                	Alert("Exceeding maximum number of characters.Please short your Note to 500 characters or less.")
                	Return
            	End If
                If (Database.Tenant.Execute("insert into ClientValidation (Name,Condition,ErrorMessage,Ordinal,SourceTable, ValidateFirstOnly, IsSoftWarning,Note) values({0},{1},{2},{3},{4}, {5}, {6},{7})".SqlFormatString(txtName.Text, txtCondition.Text, txtErrorMessage.Text, LatestOrdinal, ddlSourceTable.SelectedValue, chkValidateFirstOnly.Checked.GetHashCode, chkEnableSofWarning.Checked.GetHashCode,txtNote.Text)) > 0) Then
                	Alert("Data validation rule added")
                	Dim type As String = txtName.Text
                    type = type.Replace("'","''")
                	Dim Description As String = "New Data validation rule "+type+" added."
                    SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
                    
                    ClearFields()
                    LoadValidations()
                    UpdateValidations()
                     
                Else
                    ClearFields()
                End If
            Else
            	If txtErrorMessage.Text.Length > 500 Then
                	Alert("Exceeding maximum number of characters.Please short your ErrorMessage to 500 characters or less.")
                	Return
            	Else If txtNote.Text.Length > 500 Then
                	Alert("Exceeding maximum number of characters.Please short your notes to 500 characters or less.")
                	Return
            	End If
            	Dim editrule As DataRow = Database.Tenant.GetTopRow("select Name,Condition,ErrorMessage,SourceTable,ValidateFirstOnly, IsSoftWarning,Note from  ClientValidation where Id={0}".SqlFormatString(hcid.Value))
            	Dim cntn As String = txtCondition.Text.Replace(vbCr,"").Replace(vbLf,"")
            	Dim oldcntn As String = GetString(editrule, "Condition").Replace(vbCr,"").Replace(vbLf,"")
            	If cntn <> oldcntn Then
            		 Dim Description As String = "Condition of Data validation rule ("+txtName.Text+") changed from ("+ GetString(editrule, "Condition")+") to ("+txtCondition.Text+")"
                     SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
            	End If
            	Dim ErrMsg As String = txtErrorMessage.Text.Replace(vbCr,"").Replace(vbLf,"")
            	Dim oldErrMsg As String = GetString(editrule, "ErrorMessage").Replace(vbCr,"").Replace(vbLf,"")
                If ErrMsg <> oldErrMsg Then
                    Dim Description As String = "ErrorMessage of Data validation rule (" + txtName.Text + ") changed from (" + GetString(editrule, "ErrorMessage") + ") to (" + txtErrorMessage.Text + ")"
                    SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
                End If
                Dim CurrentNote As String = txtNote.Text.Replace(vbCr, "").Replace(vbLf, "")
                Dim Note As String = GetString(editrule, "Note").Replace(vbCr, "").Replace(vbLf, "")
                If CurrentNote <> Note Then
                    Dim Description As String = "Note of Data validation rule (" + txtName.Text + ") changed from (" + GetString(editrule, "Note") + ") to (" + txtNote.Text + ")"
                    SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
                End If
                Dim validatecheck As Boolean = GetString(editrule, "ValidateFirstOnly")
            	If chkValidateFirstOnly.Checked <> validatecheck Then
            		Dim Description As String = "" 
            		If chkValidateFirstOnly.Checked.GetHashCode = 1 Then
            			Description = "Enabled validate first record only in Data validation rule ("+txtName.Text+")"
            		Else
            			Description = "Disabled validate first record only in Data validation rule ("+txtName.Text+")"
            		End If
            		SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
            	End If
            	Dim softcheck As Boolean = GetString(editrule, "IsSoftWarning")
            	If chkEnableSofWarning.Checked <> softcheck Then
            		Dim Description As String = "" 
            		If chkEnableSofWarning.Checked.GetHashCode = 1 Then
            			Description = "Enabled soft warning in Data validation rule ("+txtName.Text+")"
            		Else
            			Description = "Disabled soft warning in Data validation rule ("+txtName.Text+")"
            		End If
            		SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
            	End If

                Dim uniquetxtName As String = (txtName.Text)
                Dim uniquetxtCondition As String = txtCondition.Text
                Dim uniquetxtErrorMessage As String = txtErrorMessage.Text
                Dim uniqueddlSourceTable As String = ddlSourceTable.SelectedValue
                Dim uniquechkValidateFirstOnly As String = chkValidateFirstOnly.Checked.GetHashCode
                Dim uniquechkEnableSofWarning As String = chkEnableSofWarning.Checked.GetHashCode
                Dim uniquetxtNote As String = txtNote.Text


                Dim oldRule As DataRow = Database.Tenant.GetTopRow("select Name,Condition,ErrorMessage,SourceTable,ValidateFirstOnly, IsSoftWarning,Note from  ClientValidation where Id={0}".SqlFormatString(hcid.Value))
                Dim oldnote = oldRule.GetString("Note")
                Dim oldSourceTable = oldRule.GetString("SourceTable")
                Dim dold As DataRow = Database.Tenant.GetTopRow("SELECT * FROM  ClientValidation WHERE Id ={0}".SqlFormatString(hcid.Value))
                Dim oldName = dold.GetString("Name")
                Dim oldCondition = dold.GetString("Condition")
                Dim oldErrorMessage = dold.GetString("ErrorMessage")
                Dim doldSourceTable = dold.GetString("SourceTable")
                Dim oldValidateFirstOnly = dold.GetString("ValidateFirstOnly")
                Dim oldIsSoftWarning = dold.GetString("IsSoftWarning")
                Dim flag As Boolean = True
                Dim oldValidateFirstOnlyVAlue As Integer = If(oldValidateFirstOnly, 1, 0)
                Dim oldIsSoftWarningValue As Integer = If(oldIsSoftWarning, 1, 0)

                Session("GridViewPageIndex") = gvValidations.PageIndex
                Session("GridViewSelectedPage") = ddlPageSize.SelectedValue
                Dim Alertmsg As String = ""
                If ((oldName = uniquetxtName) And (oldnote = uniquetxtNote) And (oldCondition = uniquetxtCondition) And (oldErrorMessage = uniquetxtErrorMessage) And (doldSourceTable = uniqueddlSourceTable) And (oldValidateFirstOnlyVAlue = uniquechkValidateFirstOnly) And (oldIsSoftWarningValue = uniquechkEnableSofWarning)) Then
                    hcid.Value = ""
                    btnCancel.Visible = False
                    Alertmsg = "No Changes"
                    btnValidations.Text = "Add Condition"
                    ClearFields()
                    LoadValidations()

                Else
                    If (Database.Tenant.Execute("update ClientValidation set Name={1},Condition={2},ErrorMessage={3},SourceTable={4}, ValidateFirstOnly={5}, IsSoftWarning={6},Note={7}  where Id={0}".SqlFormatString(hcid.Value, txtName.Text, txtCondition.Text, txtErrorMessage.Text, ddlSourceTable.SelectedValue, chkValidateFirstOnly.Checked.GetHashCode, chkEnableSofWarning.Checked.GetHashCode, txtNote.Text)) > 0) Then


                        Alertmsg = "Data validation rule modified"
                        hcid.Value = ""
                        btnCancel.Visible = False
                        btnValidations.Text = "Add Condition"
                        Dim type As String = txtName.Text
                        type = type.Replace("'", "''")

                        Dim Description As String = "Data validation rule (" + type + ") updated."
                        SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
                        ClearFields()
                        LoadValidations()
                        UpdateValidations()

                    Else
                        LoadValidations()
                    End If

                End If




                Dim RedirScript As String = "window.onload = function(){ "
                If Alertmsg <> "" Then
                	RedirScript += "alert('" + Alertmsg + "');"
            	End If
			    RedirScript += "window.location = 'validations.aspx';}"
			    
			    RunScript(RedirScript)
            End If
        End If
    End Sub

    Private Sub ClearFields()
        txtName.Text = ""
        txtCondition.Text = ""
        txtErrorMessage.Text = ""
        txtNote.Text = ""
        ddlSourceTable.SelectedIndex = 0
        chkValidateFirstOnly.Checked = False
        chkEnableSofWarning.Checked = False
    End Sub

    Sub LoadValidations()

        Dim dt As New DataTable()

        dt = Database.Tenant.GetDataTable("SELECT * FROM ClientValidation ORDER BY Id asc")
        dt.DefaultView.Sort = SortExpression + " " + If(IsAscendingSort, "ASC", "DESC")
        gvValidations.DataSource = dt.DefaultView
        'gvValidations.DataSource = Database.Tenant.GetDataTable("SELECT * FROM ClientValidation ORDER BY Id desc")
        gvValidations.DataBind()
    End Sub
    Sub UpdateValidations()
    	Database.Tenant.Execute("if exists(SELECT * FROM ClientSettings WHERE Name = 'LastSchemaUpdatedTime') UPDATE ClientSettings SET Value = CONVERT(NVARCHAR(24),GETUTCDATE(),121) WHERE Name = 'LastSchemaUpdatedTime' else INSERT INTO ClientSettings(name,Value) Values('LastSchemaUpdatedTime',CONVERT(NVARCHAR(24),GETUTCDATE(),121))")
    	Database.Tenant.Execute("if exists(SELECT * FROM ClientSettings WHERE Name = 'LastSchemaUpdatedCounter') UPDATE ClientSettings SET Value = Value + 1 WHERE Name = 'LastSchemaUpdatedCounter' else INSERT INTO ClientSettings(name,Value) Values('LastSchemaUpdatedCounter','1')")
    End Sub
    Private Sub gvValidations_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvValidations.PageIndexChanging
        gvValidations.PageIndex = e.NewPageIndex
        LoadValidations()
    End Sub

    Private Sub gvValidations_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvValidations.RowCommand
        e.CommandName.ToLower()

        Select Case e.CommandName.ToLower()
            Case "deleteaction"
             
                Dim tempTable As DataRow = Database.Tenant.GetTopRow("select Name from  ClientValidation where Id=" & e.CommandArgument.ToString)
        		txtName.Text = GetString(tempTable, "Name")
        		Database.Tenant.Execute("DELETE FROM ClientValidation WHERE Id = " & e.CommandArgument.ToString)
        		Dim type As String = txtName.Text
                type = type.Replace("'","''")
            	Dim Description As String = "Data validation rule ("+type+") deleted."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
                Session("GridViewSelectedPage") = ddlPageSize.SelectedValue
                UpdateValidations()
                LoadValidations()
                hcid.Value = ""
                ClearFields()
                Response.Redirect("~/protected/customize/validations.aspx")

            Case "btnenable"
               
                'Database.Tenant.Execute("Update ClientValidation set Enabled=(select ~Enabled from ClientValidation where Id={0}) where Id={0} ".SqlFormatString(e.CommandArgument.ToString))
                LoadValidations()
                UpdateValidations()
                'Response.Redirect(Request.Url.AbsoluteUri)  
            Case "editaction"
                FillFieldForEdit(e.CommandArgument.ToString)
        End Select
    End Sub
    Private Sub FillFieldForEdit(Id As String)
         Dim tempDataTable As DataRow = Database.Tenant.GetTopRow("select Name,Condition,ErrorMessage,SourceTable,ValidateFirstOnly, IsSoftWarning,Note from  ClientValidation where Id={0}".SqlFormatString(Id))
        If tempDataTable Is Nothing Then
        	LoadValidations()
        Else
            txtName.Text = GetString(tempDataTable, "Name")
            txtErrorMessage.Text = GetString(tempDataTable, "ErrorMessage")
            txtCondition.Text = GetString(tempDataTable, "Condition")
            txtNote.Text = GetString(tempDataTable, "Note")
            Try
                ddlSourceTable.SelectedValue = GetString(tempDataTable, "SourceTable")
            Catch ex As Exception
                ddlSourceTable.SelectedValue = ""
            End Try
            hcid.Value = Id
            chkValidateFirstOnly.Checked = tempDataTable.GetBoolean("ValidateFirstOnly")
            chkEnableSofWarning.Checked = tempDataTable.GetBoolean("IsSoftWarning")
            btnValidations.Text = "Save Condition"
            btnCancel.Visible = True
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
        hcid.Value = ""
        btnCancel.Visible = False

        btnValidations.Text = "Add Condition"
        ClearFields()
        LoadValidations()
    End Sub
    Private Sub BindSourceTables()
        ddlSourceTable.DataSource = Database.Tenant.GetDataTable("SELECT DISTINCT SourceTableName, CASE WHEN SourceTableName IS NULL THEN '[ParcelData]' ELSE SourceTableName END AS SourceTable, CASE WHEN SourceTableName IS NULL THEN ' ' ELSE SourceTableName END AS Ordinal FROM FieldCategory ORDER BY 3")
        ddlSourceTable.DataTextField = "SourceTable"
        ddlSourceTable.DataValueField = "SourceTableName"
        ddlSourceTable.DataBind()
    End Sub
    Protected Sub ddlPageSize_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
    	gvValidations.PageSize = ddlPageSize.SelectedValue
    	gvValidations.PageIndex = 0
    	LoadValidations()
    End Sub
    Protected Property SortExpression As String
    Get
        Dim value as Object = ViewState("SortExpression")
        Return If(Not IsNothing(value), CStr(value), "Id")
    End Get
    Set(value As String)
        ViewState("SortExpression") = value
    End Set
   End Property
   Protected Property IsAscendingSort As Boolean
    Get
        Dim value as Object = ViewState("IsAscendingSort")
        Return If(Not IsNothing(value), CBool(value), False)
    End Get
    Set(value As Boolean)
        ViewState("IsAscendingSort") = value
    End Set
   End Property
   Protected Sub GridView1_Sorting(sender As Object, e As GridViewSortEventArgs)
        If e.SortExpression = SortExpression Then
            IsAscendingSort = Not IsAscendingSort
    Else
          SortExpression = e.SortExpression
    End If
    LoadValidations()
End Sub
End Class