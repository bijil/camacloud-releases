﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master" CodeBehind="searchOptions.aspx.vb" Inherits="CAMACloud.Console.searchOptions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">
    	function UpdateChecked(){
    		$('input[type="checkbox"]').each(function(){
				$(this).change(function(){
					var value = this.checked ? 1: 0;
					var fieldId = $(this).parent().attr('field');
                    var displayLabel = $(this).parent().attr('displaylabel');
		    			var data = {
				            Id: fieldId,
							Value: value,
							Name: displayLabel
				            };              
					$ds('updatefilterfields', data, function (resp) {
		            			
		            		});
				})
			})
		}
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<h1>Search Options</h1>
<p class="info">
        You can include or exclude these search filter fields in the QC Filter list.
</p>
Number of items per page : 
<asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="true" width="50px">  
        <asp:ListItem Text="15" Value="15" />  
        <asp:ListItem Text="30" Value="30" />  
        <asp:ListItem Text="45" Value="45" />
</asp:DropDownList>  
</br>
<asp:GridView ID="searchFilterGrid" runat="server" AllowPaging="true" PageSize="15" OnPageIndexChanging="searchFilterGrid_PageIndexChanging">
	<Columns>
	        <asp:BoundField ItemStyle-Width="180px" DataField="DisplayLabel" HeaderText="Name" />
	        <asp:TemplateField HeaderText="" HeaderStyle-Width="80px">
                    	<ItemTemplate>
                    		<asp:CheckBox runat="server" ID="enableFilterField"  Checked='<%# Convert.ToBoolean(Eval("IncludeInSearchFilter"))%>'  field='<%# Eval("Id")%>'  displaylabel='<%# Eval("IfAggFuncShowThen") %>' />
                    	</ItemTemplate>
                  </asp:TemplateField>
        </Columns>
</asp:GridView>
</asp:Content>
