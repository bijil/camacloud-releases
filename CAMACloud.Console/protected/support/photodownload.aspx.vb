﻿Imports System.IO
Imports System.IO.Compression
Imports System.Collections.Generic
Imports System.Net
Imports Ionic.Zip

Public Class photodownload
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    	If Not IsPostBack Then
            rpMetaFields.DataSource = d_("select * from DataSourceField where SourceTable = '_photo_meta_data' ORDER BY AssignedName")
            rpMetaFields.DataBind()
    		lblPhotoCount.Text = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM ParcelImages WHERE UploadedBy IS NOT NULL")
		End If
    End Sub

    Private Sub rbPhotos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rbPhotos.SelectedIndexChanged
        If rbPhotos.SelectedValue = "FIL" Then
            pnlPhotoOps.Visible = True
            For Each rpi As RepeaterItem In rpMetaFields.Items
                Dim chk As CheckBox = rpi.FindControl("chkMetaOption")
                Dim txt As TextBox = rpi.FindControl("txtOptionValue")
                Dim ddl As DropDownList = rpi.FindControl("ddlMetaOption")
                Dim rfv As RequiredFieldValidator = rpi.FindControl("rfv")

                txt.Enabled = chk.Checked AndAlso ddl.SelectedValue <> "NULL"
                ddl.Enabled = chk.Checked
                rfv.Enabled = chk.Checked AndAlso ddl.SelectedValue <> "NULL"
            Next
            rfvDtF.Enabled = chkDateBetween.Checked
            rfvDtT.Enabled = chkDateBetween.Checked
        Else
            pnlPhotoOps.Visible = False
            resetCheckedOptionsInFIL()
        End If

        rfvDtF.Enabled = rbPhotos.SelectedValue = "FIL" AndAlso chkDateBetween.Checked
        rfvDtT.Enabled = rbPhotos.SelectedValue = "FIL" AndAlso chkDateBetween.Checked
    End Sub
    
    Sub resetCheckedOptionsInFIL()
        dtFrom.Text = ""
        dtTo.Text = ""
        For Each rpi As RepeaterItem In rpMetaFields.Items
            Dim chk As CheckBox = rpi.FindControl("chkMetaOption")
            Dim txt As TextBox = rpi.FindControl("txtOptionValue")
            Dim ddl As DropDownList = rpi.FindControl("ddlMetaOption")
            Dim rfv As RequiredFieldValidator = rpi.FindControl("rfv")
            txt.Enabled = False
            ddl.Enabled = False
            rfv.Enabled = False
            txt.Text = ""
            ddl.SelectedIndex = 0
            chk.Checked = False
        Next

        chkDateBetween.Checked = False
        dtFrom.Enabled = False
        dtTo.Enabled = False
    End Sub

    Public Sub chkMetaOption_CheckedChanged(sender As Object, e As EventArgs)
        Dim chk As CheckBox = sender
        Dim rpic = chk.Parent

        While Not (TypeOf rpic Is RepeaterItem)
            rpic = rpic.Parent
        End While

        Dim rpi As RepeaterItem = rpic

        Dim txt As TextBox = rpi.FindControl("txtOptionValue")
        Dim ddl As DropDownList = rpi.FindControl("ddlMetaOption")
        Dim rfv As RequiredFieldValidator = rpi.FindControl("rfv")

        If (chk.Checked = False) Then
            txt.Text = ""
            ddl.SelectedIndex = 0
        End If

        txt.Enabled = chk.Checked AndAlso ddl.SelectedValue <> "NULL"
        ddl.Enabled = chk.Checked
        rfv.Enabled = chk.Checked AndAlso ddl.SelectedValue <> "NULL"

    End Sub

    Private Sub rbParcels_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rbParcels.SelectedIndexChanged
        If rbParcels.SelectedValue = "S" Then
            pnlSelectParcels.Visible = True
            fuParcelList.Enabled = True
            rfvfu.Enabled = hdHasFile.Value = "0"
        Else
            pnlSelectParcels.Visible = False
            fuParcelList.Enabled = False
            rfvfu.Enabled = False
        End If
    End Sub

    Function KeyList() As String
        Dim keys As New List(Of String)

        For i = 1 To 10
            If Database.Tenant.Application.KeyField(i).IsNotEmpty Then
                keys.Add(Database.Tenant.Application.KeyField(i))
            End If
        Next

        Return String.Join(",", keys.ToArray)
    End Function
    
    Private Sub btnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
    	Database.Tenant.DropTable("temp_list_downloadphotos")
        Dim photoDownloadQuery As String = "SELECT p.KeyValue1 AS ParcelId, CAST(pi.Id AS INT) As ImageId, pi.Path FROM ParcelImages pi JOIN Parcel p ON p.Id = pi.ParcelId "
        Dim photoFilter As String = ""
        Select Case rbPhotos.SelectedValue
            Case "ALL"
                'photoDownloadQuery += " (1 = 1)"
            Case "AMF"
                Database.Tenant.Execute("SELECT Id ImageId INTO temp_list_nonprimaryimages FROM ParcelImages WHERE NULLIF(IsPrimary, 0) = 1")
                photoDownloadQuery += " INNER JOIN temp_list_nonprimaryimages li ON pi.Id = li.ImageId"
            Case "FIL"
                photoFilter = evaluatePhotoFilter()
                If (photoFilter = "1=1") Then
                    lblSelectedParcels.Visible = False
                    Alert("Your filter has given 0 results. No photos will be deleted.")
                    Return
                Else
                    If photoFilter.Trim <> "" Then
                        photoFilter = " WHERE " + photoFilter
                    End If
                End If
        End Select

        Dim parcelFilter As String = ""
        Select Case rbParcels.SelectedValue
            Case ""
            Case "S"
                Dim importError As String = ""
                If Not buildParcelList(importError) Then
                    Alert(importError)
                    Return
                End If
                photoDownloadQuery += " INNER JOIN temp_list_photodeleteparcels dp ON pi.ParcelId = dp.DeleteParcelId"
        End Select

        If photoFilter <> "" Then
            photoDownloadQuery += photoFilter
        End If
        
        Dim dt As DataTable = Database.Tenant.GetDataTable(photoDownloadQuery)
        Dim url As String = dt.Rows(0)("Path")
        Dim s3 As New S3FileManager
        '        Alert(s3.GetDownloadURL(url))       
        DownloadFiles(dt)
'        s3.DownloadImage(url, Response)
'        Response.End()
                
    End Sub

    Private Function evaluatePhotoFilter()
        Dim photoFilter As String = "1=1"
        If chkDateBetween.Checked Then
            photoFilter += " AND (pi.CreatedTime BETWEEN dbo.GetUTCOfLocalDate(CONVERT(Datetime, " + dtFrom.Text.ToSqlValue + ",120)) AND dbo.GetUTCOfLocalDate(CONVERT(Datetime, " + dtTo.Text.ToSqlValue + ",120)))"
        End If

        For Each rpi As RepeaterItem In rpMetaFields.Items
            Dim chk As CheckBox = rpi.FindControl("chkMetaOption")
            Dim hd As HiddenField = rpi.FindControl("hdMetaName")
            Dim txt As TextBox = rpi.FindControl("txtOptionValue")
            Dim ddl As DropDownList = rpi.FindControl("ddlMetaOption")

            Dim filterPart As String = ""
            If chk.Checked Then
                filterPart += "pi." + hd.Value.Wrap("[]")
                Dim op = " = "
                Select Case ddl.SelectedValue
                    Case "EQ"
                        filterPart += " = " + txt.Text.ToSqlValue
                    Case "NE"
                        filterPart += " <> " + txt.Text.ToSqlValue
                    Case "SW"
                        filterPart += " LIKE " + (txt.Text + "%").ToSqlValue
                    Case "NSW"
                        filterPart += " NOT LIKE " + (txt.Text + "%").ToSqlValue
                    Case "NULL"
                        filterPart += " IS NULL OR LTRIM(" + "pi." + hd.Value.Wrap("[]") + ") = ''"
                End Select
            End If
            If filterPart <> "" Then
                photoFilter += " AND " + filterPart.Wrap("()")
            End If
        Next

        Return photoFilter
    End Function
    
    Private Function buildParcelList(ByRef errorMessage As String) As Boolean
        If fuParcelList.HasFile AndAlso fuParcelList.FileContent.Length > 0 AndAlso fuParcelList.FileName.ToLower.EndsWith("csv") Then
            Dim sr As New IO.StreamReader(fuParcelList.FileContent)

            Dim headerLine As String = sr.ReadLine
            If headerLine.ToLower <> KeyList().ToLower Then
                errorMessage = "Invalid header line - please follow the same pattern as specified."
                Return False
            End If

            Dim dt As New DataTable
            Dim kindex As Integer = 0
            Dim parcelJoin As String = ""
            For Each kf In KeyList.Split(",")
                kindex += 1
                dt.Columns.Add("KeyValue" & kindex)

                If parcelJoin <> "" Then
                    parcelJoin += " AND "
                End If
                parcelJoin += "(p.KeyValue{0} = li.KeyValue{0})".FormatString(kindex)
            Next
            Dim lineNo As Integer = 0
            While Not sr.EndOfStream
                Dim line As String = sr.ReadLine
                lineNo += 1
                Dim keys = line.Split(",")
                Dim dr As DataRow = dt.NewRow
                If dt.Columns.Count <> keys.Length Then
                    errorMessage = "Parcel list import failed. Invalid number of fields in line " & lineNo & "."
                    Return False
                End If
                For i As Integer = 0 To keys.Length - 1
                    dr(i) = keys(i)
                Next
                dt.Rows.Add(dr)
            End While

            Database.Tenant.DropTable("temp_list_photodeleteparcels")
            Database.Tenant.DropTable("temp_list_photodeleteplist")
            Database.Tenant.BulkLoadTableFromDataTable("temp_list_photodeleteplist", dt)

            Dim photoSql = "SELECT p.Id As DeleteParcelId INTO temp_list_photodeleteparcels FROM Parcel p INNER JOIN temp_list_photodeleteplist li ON " + parcelJoin
            Database.Tenant.Execute(photoSql)

            If dt.Rows.Count > 0 Then
                hdHasFile.Value = "1"
            Else
                hdHasFile.Value = "0"
            End If

            lblSelectedParcels.Text = dt.Rows.Count & " parcels selected."
            Database.Tenant.DropTable("temp_list_photodeleteplist")
            Return True

        Else
            Return False
        End If

    End Function
    
    Private Function getImageFile(ByVal filePath As String, ByVal filename As String, ByVal response As HttpResponse) As Stream
    	response.Clear()
        response.ContentType = "application/octet-stream"
        If Not String.IsNullOrEmpty(filename) Then
            response.AddHeader("Content-Disposition", "attachment;filename=" & filename)
        End If
        Return response.OutputStream
    End Function
    
    Private Sub DownloadFiles(dt As DataTable)
'    	Dim tempFile As String = IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile, Environment.SpecialFolderOption.Create), "CAMACloud-Admin\admin-export.zip")
'    	Dim fs As New FileStream(tempFile, FileMode.CreateNew)
'    	
'		Using zip As New ZipArchive(fs, ZipArchiveMode.Create)
'			For Each row As DataRow In dt.Rows
'				If row("Path").ToString() Then
'					Dim url As String = row("Path").ToString()
'					Dim image As ZipArchiveEntry = zip.CreateEntry("Image")
'					
''					Using imageStream = image.Open
''						Using iStream As New StreamWriter(imageStream)
''							iStream.BaseStream = getImageFile(url, IO.Path.GetFileName(url), Response)
''						End Using
''					End Using
'				End If
'			Next
'		End Using
''		
''		Using outputStream = New PositionWrapperStream(Response.OutputStream)
''			Using archive = New ZipArchive(outputStream, ZipArchiveMode.Create, False)
''				Dim entry = archive.CreateEntry("test")
''				
''			End Using
''		End Using
		Dim s3 As New S3FileManager
'        Dim inputUrl As String
'		Dim zip As New ZipFile()
'		
'		For Each dr As DataRow In dt.Rows
'			inputUrl = s3.GetDownloadURL(dr("Path").ToString())
'			zip.AddFile(inputUrl)
'		Next
'		zip.Save(dt.Rows(0)("Path").ToString().Replace(IO.Path.GetFileName(dt.Rows(0)("Path").ToString()),"") + "Photos.zip")
s3.CreateDirectory("Test1")

s3.TransferFile(dt.Rows(0)("Path").ToString(),"Test1/")
		'/////////////////////////////////////
''		 add this map file into the "images" directory in the zip archive
'		Dim f As New DirectoryInfo("c:\images")
'		Dim a As FileInfo() = f.GetFiles()
'		For i As Integer = 0 To a.Length - 1
'			zip.AddFile("c:\images\" + a(i).Name, "images")
'		Next
'		zip.Save("c:\images\MyZipFile.zip")
'		Dim myWebClient As New WebClient()
'		myWebClient.DownloadFile("c:\images\MyZipFile.zip", "c:\images\test.zip")
		'/////////////////////////////////////
'		Dim filename As String = "c:\images\MyZipFile.zip"
'		Dim fileInfo As New FileInfo(filename)
'		
'		If fileInfo.Exists Then
'			Response.Clear()
'			Response.AddHeader("Content-Disposition", "inline;attachment; filename=" + fileInfo.Name)
'			Response.AddHeader("Content-Length", fileInfo.Length.ToString())
'			Response.ContentType = "application/octet-stream"
'			Response.Flush()
'			Response.WriteFile(fileInfo.FullName)
'			Response.[End]()
'		End If
		
	End Sub
    
End Class