﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master" CodeBehind="settingsaudittrailreport.aspx.vb" Inherits="CAMACloud.Console.settingsaudittrailreport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


    <link href="/App_Static/css/datatables/jquery.dataTables.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src = "/App_Static/js/datatables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src = "/App_Static/js/datatables/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src = "/App_Static/js/datatables/buttons.html5.min.js"></script>
    <script type="text/javascript" src = "/App_Static/js/datatables/moment.min.js"></script>
    
    
    <script>
        var Org_Name = ''

        function OrgName(name) {
            Org_Name = name;
        }

        $(function () {
            $.ajax({
                type: "POST",
                url: "settingsaudittrailreport.aspx/GetsatReport",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccess,
                failure: function (response) {
                    alert(response.d);
                },
                error: function (response) {
                    alert(response.d);
                }
            });
        });
        function OnSuccess(response) {
            $("[id*=gvSettingsAuditTrail]").DataTable(
                {
                    //'createdRow': function (row, data, dataIndex) {    // can be used to apply css property in a column
                    //    $('td:eq(0)', row).css('min-width', '400px');
                    //},

                    bLengthChange: true,
                    "pageLength": 20,
                    lengthMenu: [[10, 20, 50, 100, 250], [10, 20, 50, 100, 250]],
                    bFilter: true,
                    bSort: true,
                    bPaginate: true,
                    data: response.d,
                    "order" : [0,'desc'],
                    columns: [
                        {
                            'data': 'Id',
                            'searchable': false,  
                            'sortable': false,
                             visible : false
                        },
                        {
                            'data': 'EventTime',
                            'searchable': false,
                             width: '20%',
                            'sortable': false,
                            "render": function (data, type, row) { return moment(parseInt(data.match(/\d+/)[0])).utcOffset(0).format("MM/DD/YYYY hh:mm:ss A"); }                // to format the data in required format
                        },
                        {
                            'data': 'LoginId',
                            'searchable': false,  // to set if the column should appear in seach
                            'sortable': false,    // to set sort button on the column header
                             width: '15%'
                        },
                        {
                            'data': 'Description',
                            'sortable': false
                        }],

                    dom: '<"top"<"left-col"l><"right-col"Bf>>rtip',      // to set the order of buttons, searchbar, info etc.
                    buttons: [{
                        extend: 'excel',
                        text: 'Export to Excel',                        // to export the full report into excel
                        filename: Org_Name,
                        exportOptions: {
                            columns: [1, 2, 3]
                        }
                    },]
                  
                });
        };

    </script>

<style type="text/css">

.left-col {
    float: left;
    width: 25%;
}

 
.right-col {
    float: right;
}

table.dataTable tbody td {
  padding: 5px 9px;
  
}

.dataTables_filter  {
     margin-top: 20px;
     margin-bottom: 20px;
}

.buttons-excel {
        float: right;
        
}

.break {
	word-break: break-all;
}

.hiddencol
  {
    display: none;
  }

</style>


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h1>Settings Audit Trail Report</h1><br />

    <table Style="Width:99.5%"><tr><td></td> </tr>
    <tr><td colspan='2'  ><asp:Label ID="lbNotification" runat="server" Text="No Settings Audit Trail Records Available" Visible="false"></asp:Label>
        
        <asp:Panel ID="pnlSettingsAuditTrail" runat="server">
          
            <asp:GridView ID="gvSettingsAuditTrail" runat="server" CssClass="display compact" Style="Width:100%">
                <Columns>
                    <asp:BoundField DataField="Id" HeaderText="Id" ItemStyle-Width="200px" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" />
                    <asp:BoundField DataField="EventTime" HeaderText="Event Time" ItemStyle-Width="200px" />
                    <asp:BoundField DataField="LoginId" HeaderText="LoginId" ItemStyle-Width="100px" />
                    <asp:BoundField DataField="Description" HeaderText="Description" ItemStyle-Width="500px" ItemStyle-CssClass="break" />
                </Columns>
            </asp:GridView>
        
        </asp:Panel>
        
        </td></tr></table>
    
    
</asp:Content>
