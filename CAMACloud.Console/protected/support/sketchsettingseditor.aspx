﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master" CodeBehind="sketchsettingseditor.aspx.vb" Inherits="CAMACloud.Console.sketchsettingseditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .col1
        {
            padding-right: 30px;
            padding-left: 5px;
        }
        .wbreak{
            word-break:break-all;
        }
        textarea {
			resize: none;
		}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>
         Sketch Settings </h1>
     <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:GridView runat="server" ID="grid"  AllowPaging="true" OnPageIndexChanging="OnPageIndexChanging" PageSize="8">
                <Columns>
                    <asp:TemplateField>
                        <ItemStyle Width="25px" />
                        <ItemTemplate>
                            <%# (Container.DataItemIndex)+1 %>.
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Name" HeaderText="Name"  ItemStyle-Width="250px"/>
                    <asp:BoundField DataField="Value" HeaderText="Value"  ItemStyle-Width="400px" ItemStyle-CssClass="wbreak" />
                    <asp:TemplateField>
                        <ItemStyle Width="80px"/>
                        <ItemTemplate>
                            <asp:LinkButton runat="server" ID="lbEdit" Text="Edit" CommandName='EditItem' CommandArgument='<%# Eval("Name") %>' />
                            <asp:LinkButton runat="server" ID="lbDelete" Text="Delete" CommandName='DeleteItem'
                                CommandArgument='<%# Eval("Name")%>' OnClientClick='return confirm("Are you sure you want to delete?")' />
                           
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
             <h3><asp:Label ID ="lblHeadText" runat="server">Add Settings</asp:Label></h3>
             <table>
                <tr>
                    <td class="col1">
                        Name:
                    </td>
                    <td>
                    	<asp:HiddenField ID="hname" runat="server" Value="" />
                        <asp:TextBox runat="server" ID="txtName" Width="250px" MaxLength="100"/>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName"
                            ErrorMessage="*" ValidationGroup="Form" />
                    </td>
                </tr>
                <tr>
                    <td class="col1">
                        Value:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtValue" Width="450px" Height="120px" TextMode="MultiLine"/>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtValue"
                            ErrorMessage="*" ValidationGroup="Form" />
                    </td>
                </tr>
                <tr style="position:relative">
                    <td colspan="2">
                        <asp:Button runat="server" ID="btnSave" Text=" Add " Font-Bold="true"
                            ValidationGroup="Form" />
                        <asp:Button runat="server" ID="btnCancel" Text=" Cancel " Visible="false" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    </asp:Content>