﻿Public Class schemasettings
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            divField.Visible = False
            divTable.Visible = False

            bindDataSourceTable() 'Function call for binding DataSourceTable dropdown
            bindDataSourceField(ddlTableList.SelectedValue) 'Function call for binding DataSourceField gridview, parameter as selected dropdownlist value
        End If
    End Sub

    Private Sub bindDataSourceTable() 'Function call for binding DataSourceTable dropdown
        ddlTableList.DataSource = Database.Tenant.GetDataTable("SELECT Id,Name FROM DataSourceTable")
        ddlTableList.DataTextField = "Name"
        ddlTableList.DataValueField = "Id"
        ddlTableList.DataBind()
    End Sub

    Private Sub bindDataSourceField(tableId As String) 'Function call for binding DataSourceField gridview
        gridDataList.DataSource = Database.Tenant.GetDataTable("select Id,Name from DataSourceField where TableId='" + tableId + "'")
        gridDataList.DataBind()
    End Sub

    Protected Sub ddlTableList_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTableList.SelectedIndexChanged
        bindDataSourceField(ddlTableList.SelectedValue) 'Function call for binding DataSourceField gridview, parameter as selected dropdownlist value
    End Sub

    Protected Sub gridDataList_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gridDataList.RowCommand
        divTable.Visible = False
        divField.Visible = True

        'Changing the row color when row selected --- start
        bindDataSourceField(ddlTableList.SelectedValue)
        Dim gvr As GridViewRow = DirectCast(DirectCast(e.CommandSource, ImageButton).NamingContainer, GridViewRow)
        Dim RowIndex As Integer = gvr.RowIndex
        gridDataList.Rows(RowIndex).BackColor = Drawing.Color.AntiqueWhite
        '--- end

        'Getting Id and Name from edit click event --- start
        Dim rowValue() As String
        rowValue = e.CommandArgument.ToString().Split(",")

        Dim id = rowValue(0)
        Dim name = rowValue(1)
        

        'Assigning select row Name to Header field
        fieldHidden.Value = id
        formHeaderField.InnerHtml = ddlTableList.SelectedItem.ToString() + "." + name
        '--- end

        'Assigning retrieved data to corresponding field --- start
        Dim dr As DataRow = Database.Tenant.GetTopRow("select * from DataSourceField where Id='" + id + "'")

If dr IsNot Nothing Then
	    txtFieldID.Text = dr.GetString("Id")
            txtAssignedName.Text = dr.GetString("AssignedName")            
            txtNullReplacement.Text = dr.GetString("DestinationNullReplacement")
            txtAllias.Text = dr.GetString("AliasSource")
            txtSourceCal.Text = dr.GetString("SourceCalculationExpression")
            txtSync.Text = dr.GetString("SyncProperties")

            If dr.GetString("DoNotInsertOrUpdateAtDestination") = "True" Then
                rblRoles.SelectedIndex = 0
            ElseIf dr.GetString("DoNotInsertOrUpdateAtDestination") = "False" Then
                rblRoles.SelectedIndex = 1
            End If
        End If
        '--- end
    End Sub

    Protected Sub imgTableEdit_Click(sender As Object, e As ImageClickEventArgs) Handles imgTableEdit.Click
        divTable.Visible = True
        divField.Visible = False

        bindDataSourceField(ddlTableList.SelectedValue)

        'Setting DataSourceTable form header --- start
        formHeaderTable.InnerHtml = ddlTableList.SelectedItem.ToString()
        tableHidden.Value = ddlTableList.SelectedValue
        '--- end

        'Assigning retrieved data to corresponding field --- start
        Dim dr As DataRow = Database.Tenant.GetTopRow("select * from DataSourceTable where Id='" + ddlTableList.SelectedValue + "'")

        If dr IsNot Nothing Then
            txtDestAllias.Text = dr.GetString("DestinationAlias")
            txtDestFilter.Text = dr.GetString("DestinationFilter")
            txtAbbrivateName.Text = dr.GetString("AbbreviatedName")
            txtSyncProp.Text = dr.GetString("SyncProperties")
        End If
        '--- end
    End Sub

Protected Sub btnFieldSave_Click(sender As Object, e As EventArgs) Handles btnFieldSave.Click
		Dim dr As DataRow = Database.Tenant.GetTopRow("select * from DataSourceField where id ='" +fieldHidden.Value+ "'")
        'Update DataSourceField property
        If txtAssignedName.Text = "" Then
            Alert("Assigned Name cannot be blank")
        Else
            Dim updateReturnFieldCount As Integer = Database.Tenant.Execute("UPDATE DataSourceField SET AssignedName={1}, DestinationNullReplacement={2}, AliasSource = {3},SyncProperties={4},SourceCalculationExpression={5},DoNotInsertOrUpdateAtDestination={6} where Id ={0}".SqlFormatStringWithNulls(fieldHidden.Value, txtAssignedName.Text, txtNullReplacement.Text, txtAllias.Text, txtSync.Text, txtSourceCal.Text, rblRoles.SelectedValue))

            If updateReturnFieldCount = 0 Then
                Alert("Field Property not Updated")
            Else
              If (txtAssignedName.Text <> dr.GetString("AssignedName")) Then 
         		Dim Description As String = "Assigned Name has been changed from "+dr.GetString("AssignedName")+" to "+txtAssignedName.Text+" for "+ddlTableList.SelectedItem.ToString()+"."+dr.GetString("Name")+" "
        		SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
              End If
               If (txtNullReplacement.Text <> dr.GetString("DestinationNullReplacement")) Then 
         		Dim Description As String = "Destination Null Replacement has been changed from "+dr.GetString("DestinationNullReplacement")+" to "+txtNullReplacement.Text+" for "+ddlTableList.SelectedItem.ToString()+"."+dr.GetString("Name")+" "
        		SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
               End If
               If (txtAllias.Text <> dr.GetString("AliasSource")) Then 
         		Dim Description As String = "Alias Source has been changed from "+dr.GetString("AliasSource")+" to "+txtAllias.Text+" for "+ddlTableList.SelectedItem.ToString()+"."+dr.GetString("Name")+" "
        		SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
               End If
               If (txtSourceCal.Text <> dr.GetString("SourceCalculationExpression")) Then 
         		Dim Description As String = "Source Calculation Expression has been changed from "+dr.GetString("SourceCalculationExpression")+" to "+txtSourceCal.Text+" for "+ddlTableList.SelectedItem.ToString()+"."+dr.GetString("Name")+" "
        		SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
               End If
               If (txtSync.Text <> dr.GetString("SyncProperties")) Then 
         		Dim Description As String = "Sync Properties has been changed from "+dr.GetString("SyncProperties")+" to "+txtSync.Text+" for "+ddlTableList.SelectedItem.ToString()+"."+dr.GetString("Name")+" "
        		SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
               End If
               If (rblRoles.SelectedValue <> dr.GetString("DoNotInsertOrUpdateAtDestination")) Then 
	       		Dim Description As String = "DoNot Insert/Update At Destination has been set to "+rblRoles.SelectedValue.ToString()+" for for "+ddlTableList.SelectedItem.ToString()+"."+dr.GetString("Name")+""
        		SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
        	   End If
                Alert("Field Property Updated Successfully")
            End If
        End If
    End Sub

Protected Sub btnTableSave_Click(sender As Object, e As EventArgs) Handles btnTableSave.Click	
		formHeaderTable.InnerHtml = ddlTableList.SelectedItem.ToString()
		tableHidden.Value = ddlTableList.SelectedValue
		Dim dr As DataRow = Database.Tenant.GetTopRow("select * from DataSourceTable where Id='" + ddlTableList.SelectedValue + "'")
        Dim updateReturnTableCount As Integer = Database.Tenant.Execute("UPDATE DataSourceTable SET DestinationAlias={1},DestinationFilter={2}, AbbreviatedName = {3},SyncProperties={4} where Id ={0}".SqlFormatStringWithNulls(tableHidden.Value, txtDestAllias.Text, txtDestFilter.Text, txtAbbrivateName.Text, txtSyncProp.Text))

        If updateReturnTableCount = 0 Then
            Alert("Table Property not Updated")
        Else
         If (txtDestAllias.Text <> dr.GetString("DestinationAlias")) Then 
        	Dim Description As String = "Destination Alias has been changed from "+dr.GetString("DestinationAlias")+" to "+txtDestAllias.Text+" for "+ddlTableList.SelectedItem.ToString()+" table"
        	SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
         End If
         If (txtDestFilter.Text <> dr.GetString("DestinationFilter")) Then 
        	Dim Description As String = "Destination Filter has been changed from "+dr.GetString("DestinationFilter")+" to "+txtDestFilter.Text+" for "+ddlTableList.SelectedItem.ToString()+" table"
        	SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
         End If
         If (txtAbbrivateName.Text <> dr.GetString("AbbreviatedName")) Then 
        	Dim Description As String = "Abbreviated Name has been changed from "+dr.GetString("AbbreviatedName")+" to "+txtAbbrivateName.Text+" for "+ddlTableList.SelectedItem.ToString()+" table"
        	SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
         End If
         If (txtSyncProp.Text <> dr.GetString("SyncProperties")) Then 
        	Dim Description As String = "Sync Properties has been changed from "+dr.GetString("SyncProperties")+" to "+txtSyncProp.Text+" for "+ddlTableList.SelectedItem.ToString()+" table"
        	SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
         End If
        	Alert("Table Property Updated Successfully")
        End If
    End Sub
End Class