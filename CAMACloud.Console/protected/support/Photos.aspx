﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master" CodeBehind="Photos.aspx.vb" Inherits="CAMACloud.Console.Photos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<style type="text/css">
        .col1
        {
            padding-right: 30px;
            padding-left: 5px;
        }
        .spaced input[type="radio"] {
            margin-left: 4px; /* Or any other value */
        }

    </style>
    <script>
    $(document).ready(function(){
            document.getElementById("MainContent_MainContent_ControlFile").addEventListener('change', function () {
             var fileUpload = $('input[type="file"]');
               if (fileUpload.val() != '') {
                   filePath = fileUpload.val();
               }
               if (filePath == '') {
                   alert('Please choose a .csv file');
                   return false;
               }
               if (filePath.split('.').pop().toLowerCase() != 'csv') {
                   alert("You have uploaded an invalid file. Only'.csv' formats are allowed.");
                   fileUpload.val("")
                   return false;
               }
              });
            });
       function hidedtrStatusList(){
    	$('.stslist').hide();
      	}
      function validateupload(){
			var fileUpload = $('input[type="file"]');
			var filePath = $('input[type="file"]').val();
            if (filePath == '') {
					alert('Please choose a .csv file');
					return false;
				}
		}
        
        function RedirectPage()
        {
          window.location = "Photos.aspx";
        }

    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<h1>Photos</h1>

    <div class="stslist">
    <p class="info">
       You can set the commit option for photo by uploading a control list. The control list should be in CSV Format, with the following fields.</p>
    <p runat="server" id="pHaveCK">
      	<strong>ParcelId,CommittedStatus</strong>
    </p>
	<strong>ParcelId</strong> - This field will set either CC Parcel ID or keyfield1.</span><br>
	<strong>CommittedStatus</strong> - The accepted values are : 'Yes' or 'No'.<br>
    <p runat="server" id="pNoCK" CssClass="info">
        Please select either CC Parcel ID or keyfield1 before using this feature.
    </p>
	<asp:DropDownList runat="server" ID="ddlPhotoCommit" Width="240px" AutoPostBack="true">
                    <asp:ListItem Text="-- Select --" Value="0" />
                    <asp:ListItem Text="CC Parcel Id" Value="ParcelId" />
                    <asp:ListItem Text="keyfield1" Value="keyfield1" />
                </asp:DropDownList>
    <p>
    
    <div class="group-title">Do you want to flag the parcels for downSync ?</div>

    <asp:RadioButtonList ID="rbnFlagParcels" runat="server" CssClass="spaced">
    	<asp:ListItem Value ="1" Text ="Yes"></asp:ListItem>
       	<asp:ListItem Value ="0" Text ="No" Selected ="True"></asp:ListItem>
     </asp:RadioButtonList>
    
        <strong>Notes : The maximum size of uploaded file should be 4MB.</strong>

    </p>
    <p style="margin: 5px 0px; padding: 12px 5px; background: #DFDFDF; border: 1px solid #CFCFCF; width: 598px;">
        <asp:FileUpload ID="ControlFile" runat="server" Width="400px" accept=".csv"/>
        <asp:Button ID="btnUpload" runat="server" onClientClick= "return validateupload();" Text="Upload and Commit" Height="24px" Weight="180px" Font-Bold="true"/>

    </p>
 	</div>  
</asp:Content>
