﻿Public Class photocleanup
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            hdnProcessID.Value = Guid.NewGuid.ToString.Replace("-", "")
            rpMetaFields.DataSource = d_("select * from DataSourceField where SourceTable = '_photo_meta_data' ORDER BY AssignedName")
            rpMetaFields.DataBind()

            hdnTotalCount.Value = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM ParcelImages")
            lblPhotoCount.Text = hdnTotalCount.Value

            If hdnTotalCount.Value = "0" Then
                pnlInitDelete.Visible = False
                pnlConfirm.Visible = False
            End If


            ddlLimit.FillNumbers(20, 2, -1)
        End If
    End Sub

    Private Sub rbParcels_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rbParcels.SelectedIndexChanged
        If rbParcels.SelectedValue = "S" Then
            pnlSelectParcels.Visible = True
            fuParcelList.Enabled = True
            rfvfu.Enabled = hdHasFile.Value = "0"
        Else
            pnlSelectParcels.Visible = False
            fuParcelList.Enabled = False
            rfvfu.Enabled = False
        End If
    End Sub

    Private Sub rbPhotos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rbPhotos.SelectedIndexChanged
        If rbPhotos.SelectedValue = "FIL" Then
            pnlPhotoOps.Visible = True
            pnlLimitOps.Visible = False
            For Each rpi As RepeaterItem In rpMetaFields.Items
                Dim chk As CheckBox = rpi.FindControl("chkMetaOption")
                Dim txt As TextBox = rpi.FindControl("txtOptionValue")
                Dim ddl As DropDownList = rpi.FindControl("ddlMetaOption")
                Dim rfv As RequiredFieldValidator = rpi.FindControl("rfv")

                txt.Enabled = chk.Checked AndAlso ddl.SelectedValue <> "NULL"
                ddl.Enabled = chk.Checked
                rfv.Enabled = chk.Checked AndAlso ddl.SelectedValue <> "NULL"
            Next
            rfvDtF.Enabled = chkDateBetween.Checked
            rfvDtT.Enabled = chkDateBetween.Checked
        ElseIf rbPhotos.SelectedValue = "LIM" Then
            pnlPhotoOps.Visible = False
            pnlLimitOps.Visible = True
            ddlLimitTrimType.SelectedIndex = 0
            ddlLimit.SelectedIndex = 0
            resetCheckedOptionsInFIL()
        Else
            pnlPhotoOps.Visible = False
            pnlLimitOps.Visible = False
            resetCheckedOptionsInFIL()
        End If

        rfvDtF.Enabled = rbPhotos.SelectedValue = "FIL" AndAlso chkDateBetween.Checked
        rfvDtT.Enabled = rbPhotos.SelectedValue = "FIL" AndAlso chkDateBetween.Checked
    End Sub
    Sub resetCheckedOptionsInFIL()
        dtFrom.Text = ""
        dtTo.Text = ""
        For Each rpi As RepeaterItem In rpMetaFields.Items
            Dim chk As CheckBox = rpi.FindControl("chkMetaOption")
            Dim txt As TextBox = rpi.FindControl("txtOptionValue")
            Dim ddl As DropDownList = rpi.FindControl("ddlMetaOption")
            Dim rfv As RequiredFieldValidator = rpi.FindControl("rfv")
            txt.Enabled = False
            ddl.Enabled = False
            rfv.Enabled = False
            txt.Text = ""
            ddl.SelectedIndex = 0
            chk.Checked = False
        Next

        chkDateBetween.Checked = False
        dtFrom.Enabled = False
        dtTo.Enabled = False
    End Sub

    Private Sub chkDateBetween_CheckedChanged(sender As Object, e As EventArgs) Handles chkDateBetween.CheckedChanged
        Dim valid = chkDateBetween.Checked
        If (valid = False) Then
            dtFrom.Text = ""
            dtTo.Text = ""
        End If
        dtFrom.Enabled = valid
        dtTo.Enabled = valid
        rfvDtF.Enabled = valid
        rfvDtT.Enabled = valid

    End Sub

    Public Sub chkMetaOption_CheckedChanged(sender As Object, e As EventArgs)
        Dim chk As CheckBox = sender
        Dim rpic = chk.Parent

        While Not (TypeOf rpic Is RepeaterItem)
            rpic = rpic.Parent
        End While

        Dim rpi As RepeaterItem = rpic

        Dim txt As TextBox = rpi.FindControl("txtOptionValue")
        Dim ddl As DropDownList = rpi.FindControl("ddlMetaOption")
        Dim rfv As RequiredFieldValidator = rpi.FindControl("rfv")

        If (chk.Checked = False) Then
            txt.Text = ""
            ddl.SelectedIndex = 0
        End If

        txt.Enabled = chk.Checked AndAlso ddl.SelectedValue <> "NULL"
        ddl.Enabled = chk.Checked
        rfv.Enabled = chk.Checked AndAlso ddl.SelectedValue <> "NULL"

    End Sub

    Public Sub ddlMetaOption_CheckedChanged(sender As Object, e As EventArgs)
        Dim ddl As DropDownList = sender
        Dim rpic = ddl.Parent

        While Not (TypeOf rpic Is RepeaterItem)
            rpic = rpic.Parent
        End While

        Dim rpi As RepeaterItem = rpic
        Dim txt As TextBox = rpi.FindControl("txtOptionValue")
        Dim rfv As RequiredFieldValidator = rpi.FindControl("rfv")
        If ddl.SelectedValue = "NULL" Then
            txt.Enabled = False
            rfv.Enabled = False
        Else
            txt.Enabled = True
            rfv.Enabled = True
        End If

    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Database.Tenant.DropTable("temp_list_deletephotos")
        Dim photoDeleteQuery As String = "SELECT pi.ParcelId, CAST(pi.Id AS INT) As ImageId, pi.Path,pi.DownSynced INTO temp_list_deletephotos FROM ParcelImages pi "
        Dim photoFilter As String = ""
        Select Case rbPhotos.SelectedValue
            Case "ALL"
                'photoDeleteQuery += " (1 = 1)"
            Case "EMF"
                Database.Tenant.Execute("EXEC cc_GetNonPrimaryImages")
                photoDeleteQuery += " INNER JOIN temp_list_nonprimaryimages li ON pi.Id = li.ImageId"
            Case "LIM"
                Database.Tenant.Execute("EXEC cc_SetImageSequence " + ddlLimitTrimType.SelectedValue)
                photoFilter = " WHERE SequenceNumber > " + ddlLimit.SelectedValue
            Case "FIL"
                photoFilter = evaluatePhotoFilter()
                If (photoFilter = "1=1") Then
                    lblSelectedParcels.Visible = False
                    Alert("Your filter has given 0 results. No photos will be deleted.")
                    Return
                Else
                    If photoFilter.Trim <> "" Then
                        photoFilter = " WHERE " + photoFilter
                    End If
                End If
            Case "ECC"
        		photoDeleteQuery += " WHERE pi.UploadedBy IS NULL"
        End Select

        Dim parcelFilter As String = ""
        Select Case rbParcels.SelectedValue
            Case ""
            Case "S"
                Dim importError As String = ""
                If Not buildParcelList(importError) Then
                    Alert(importError)
                    Return
                End If
                photoDeleteQuery += " INNER JOIN temp_list_photodeleteparcels dp ON pi.ParcelId = dp.DeleteParcelId"
        End Select

        If photoFilter <> "" Then
            photoDeleteQuery += photoFilter
        End If
		Try
			Database.Tenant.Execute(photoDeleteQuery)
		Catch ex As Exception
			Alert("Please enter a valid date.")
			Return
		End Try
        Database.Tenant.Execute("ALTER TABLE temp_list_deletephotos ADD ROWID INT IDENTITY")
        Dim pendingImgCondition As String
        Select Case rbnPendingPhotos.SelectedValue
            Case "1"
                pendingImgCondition = ""
            Case "0"
                pendingImgCondition = "WHERE DownSynced = 1"

        End Select
        If rbnPendingPhotos.SelectedValue = "1" Then
            lblTotalPendingCount.Text = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM temp_list_deletephotos WHERE DownSynced = 0 OR DownSynced IS NULL ")
            pendingPhotoConformationDiv.Visible = True
        Else
            lblTotalPendingCount.Text = ""
            pendingPhotoConformationDiv.Visible = False
            Database.Tenant.Execute("DELETE FROM temp_list_deletephotos WHERE DownSynced = 0 OR DownSynced IS NULL")
        End If

        hdnDeleteCount.Value = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM temp_list_deletephotos")

        If hdnDeleteCount.Value = "0" Then
            lblSelectedParcels.Visible = False
            Alert("Your query has given 0 results. No photos will be deleted.")
            Return

        Else
            lblTotalCount.Text = hdnTotalCount.Value
            lblDeleteCount.Text = hdnDeleteCount.Value

            If hdnTotalCount.Value <> "0" Then
                Dim percent As Integer = CInt(hdnDeleteCount.Value) / CInt(hdnTotalCount.Value) * 100
                If percent = 0 Then
                    lblDeletePercent.Text = ""
                    lblDeletePercentTitle.Text = hdnDeleteCount.Value
                Else
                    lblDeletePercent.Text = "(" & percent & "%)"
                    lblDeletePercentTitle.Text = percent & "%"
                End If


                If percent > 75 Then
                    confirmTitle.Style("color") = "Red"
                Else
                    confirmTitle.Style.Remove("color")
                End If
            End If

            pnlInitDelete.Visible = False
            pnlConfirm.Visible = True
        End If


    End Sub

    Private Function evaluatePhotoFilter()
        Dim photoFilter As String = "1=1"
        If chkDateBetween.Checked Then
            photoFilter += " AND (pi.CreatedTime BETWEEN dbo.GetUTCOfLocalDate(CONVERT(Datetime, " + dtFrom.Text.ToSqlValue + ",120)) AND dbo.GetUTCOfLocalDate(CONVERT(Datetime, " + dtTo.Text.ToSqlValue + ",120)))"
        End If

        For Each rpi As RepeaterItem In rpMetaFields.Items
            Dim chk As CheckBox = rpi.FindControl("chkMetaOption")
            Dim hd As HiddenField = rpi.FindControl("hdMetaName")
            Dim txt As TextBox = rpi.FindControl("txtOptionValue")
            Dim ddl As DropDownList = rpi.FindControl("ddlMetaOption")

            Dim filterPart As String = ""
            If chk.Checked Then
                filterPart += "pi." + hd.Value.Wrap("[]")
                Dim op = " = "
                Select Case ddl.SelectedValue
                    Case "EQ"
                        filterPart += " = " + txt.Text.ToSqlValue
                    Case "NE"
                        filterPart += " <> " + txt.Text.ToSqlValue
                    Case "SW"
                        filterPart += " LIKE " + (txt.Text + "%").ToSqlValue
                    Case "NSW"
                        filterPart += " NOT LIKE " + (txt.Text + "%").ToSqlValue
                    Case "NULL"
                        filterPart += " IS NULL OR LTRIM(" + "pi." + hd.Value.Wrap("[]") + ") = ''"
                End Select
            End If
            If filterPart <> "" Then
                photoFilter += " AND " + filterPart.Wrap("()")
            End If
        Next

        Return photoFilter
    End Function

    Function KeyList() As String
        Dim keys As New List(Of String)

        For i = 1 To 10
            If Database.Tenant.Application.KeyField(i).IsNotEmpty Then
                keys.Add(Database.Tenant.Application.KeyField(i))
            End If
        Next

        Return String.Join(",", keys.ToArray)
    End Function

    Private Function buildParcelList(ByRef errorMessage As String) As Boolean
        If fuParcelList.HasFile AndAlso fuParcelList.FileContent.Length > 0 AndAlso fuParcelList.FileName.ToLower.EndsWith("csv") Then
            Dim sr As New IO.StreamReader(fuParcelList.FileContent)

            Dim headerLine As String = sr.ReadLine
            If headerLine.ToLower <> KeyList().ToLower Then
                errorMessage = "Invalid header line - please follow the same pattern as specified."
                Return False
            End If

            Dim dt As New DataTable
            Dim kindex As Integer = 0
            Dim parcelJoin As String = ""
            For Each kf In KeyList.Split(",")
                kindex += 1
                dt.Columns.Add("KeyValue" & kindex)

                If parcelJoin <> "" Then
                    parcelJoin += " AND "
                End If
                parcelJoin += "(p.KeyValue{0} = li.KeyValue{0})".FormatString(kindex)
            Next
            Dim lineNo As Integer = 0
            While Not sr.EndOfStream
                Dim line As String = sr.ReadLine
                lineNo += 1
                Dim keys = line.Split(",")
                Dim dr As DataRow = dt.NewRow
                If dt.Columns.Count <> keys.Length Then
                    errorMessage = "Parcel list import failed. Invalid number of fields in line " & lineNo & "."
                    Return False
                End If
                For i As Integer = 0 To keys.Length - 1
                    dr(i) = keys(i)
                Next
                dt.Rows.Add(dr)
            End While

            Database.Tenant.DropTable("temp_list_photodeleteparcels")
            Database.Tenant.DropTable("temp_list_photodeleteplist")
            Database.Tenant.BulkLoadTableFromDataTable("temp_list_photodeleteplist", dt)

            Dim photoSql = "SELECT p.Id As DeleteParcelId INTO temp_list_photodeleteparcels FROM Parcel p INNER JOIN temp_list_photodeleteplist li ON " + parcelJoin
            Database.Tenant.Execute(photoSql)

            If dt.Rows.Count > 0 Then
                hdHasFile.Value = "1"
            Else
                hdHasFile.Value = "0"
            End If

            lblSelectedParcels.Text = dt.Rows.Count & " parcels selected."
            Database.Tenant.DropTable("temp_list_photodeleteplist")
            Return True

        Else
        	errorMessage = "Invalid file type - please select as specified."
            Return False
        End If

    End Function

    Private Sub btnConfirm_Click(sender As Object, e As EventArgs) Handles btnConfirm.Click
        Dim pendingCondition As String
        If chkPendingConfirm.Checked Then
            pendingCondition = ""
        Else
            pendingCondition = "WHERE DownSynced = 1"
        End If
        Dim organizationId As Integer = HttpContext.Current.GetCAMASession.OrganizationId
        Dim orgKey As String = HttpContext.Current.GetCAMASession.TenantKey
        Dim orgPrefix = orgKey + "/"

        Dim jobCreateSql As String = "INSERT INTO PhotoDeleteJob (OrganizationID, JobUID, LoginID, IPAddress) VALUES ({0}, {1}, {2}, {3}); SELECT CAST(@@IDENTITY AS INT) As NewId;".SqlFormatString(organizationId, hdnProcessID.Value, Page.UserName, Request.ClientIPAddress)
        Dim jobId As Integer = Database.System.GetIntegerValue(jobCreateSql)
        Dim pageSize As Integer = 2000
        Dim delCount As Integer = hdnDeleteCount.Value
        Dim pages = CInt(Math.Ceiling(CSng(delCount) / pageSize))
        Dim delQueueCount As Integer = 0
        Dim delIgnoreCount As Integer = 0

        For i As Integer = 0 To pages
            Dim pageSql = "SELECT * FROM temp_list_deletephotos " + pendingCondition + " ORDER BY ROWID OFFSET {0} ROWS FETCH NEXT {1} ROWS ONLY".FormatString(i * pageSize, pageSize)
            Dim pageData = Database.Tenant.GetDataTable(pageSql)
            Dim bulkSql As String = ""
            For Each dr As DataRow In pageData.Rows
                Dim path As String = dr.GetString("Path")
                If path.StartsWith(orgPrefix) Then
                    bulkSql += "INSERT INTO PhotoDeleteQueue (OrganizationID, OrganizationPrefix, QueueJobID, ParcelId, ImageId, Path) VALUES ({0}, {1}, {2}, {3}, {4}, {5});".SqlFormatString(organizationId, orgPrefix, jobId, dr.GetInteger("ParcelId"), dr.GetInteger("ImageId"), path) + vbNewLine
                    delQueueCount += 1
                Else
                    delIgnoreCount += 1
                End If
            Next
            If bulkSql <> "" Then
                Database.System.Execute(bulkSql)
            End If
        Next
        Dim sql = "INSERT INTO ParcelAuditTrail (EventDate, EventTime, ParcelId, LoginID, EventType, Description) SELECT DISTINCT GETUTCDATE(), GETUTCDATE(), ParcelId, {0}, 4, CAST(COUNT(*) AS VARCHAR(6)) +' photo(s) were deleted from the photo cleanup process.' FROM ParcelImages WHERE Id IN (SELECT ImageId FROM temp_list_deletephotos " + pendingCondition + " ) GROUP BY ParcelId"
        Database.Tenant.Execute(sql.SqlFormatString(UserName))

        Database.System.Execute("UPDATE PhotoDeleteJob SET QueueCount = {1}, IgnoreCount = {2} WHERE ID = {0}".SqlFormatString(jobId, delQueueCount, delIgnoreCount))
        Database.Tenant.Execute("DELETE FROM ParcelImages WHERE Id IN (SELECT ImageId FROM temp_list_deletephotos " + pendingCondition + " )")

        lblFinalDeleteCount.Text = delQueueCount + delIgnoreCount
        If delIgnoreCount > 0 Then
            lblIgnoreWarning.Text = delIgnoreCount & " photos were not permanently deleted as they appear to be from different environments."
        End If

        pnlConfirm.Visible = False
        pnlFinalResult.Visible = True

        Database.Tenant.DropTable("temp_list_deletephotos")
        hdnTotalCount.Value = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM ParcelImages")
        lblPhotoCount.Text = hdnTotalCount.Value

    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        pnlInitDelete.Visible = True
        pnlConfirm.Visible = False
        lblSelectedParcels.Visible = False
        Database.Tenant.DropTable("temp_list_deletephotos")

    End Sub

    Private Sub lbReturn_Click(sender As Object, e As EventArgs) Handles lbReturn.Click
        Response.Redirect(Request.Path)
    End Sub
End Class