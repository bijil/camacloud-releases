﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports CAMACloud.BusinessLogic
Imports System.Web.Services
Imports System.Drawing
Public Class deletetable
    Inherits System.Web.UI.Page
    Dim dtSchema As DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadSchemaTree()
        End If
        If SchemaTables.Nodes.Count = 0 Then
            lblHead.Text = " No schema tables available"
        End If
    End Sub
    Sub LoadSchemaTree()
        SchemaTables.Nodes.Clear()
        dtSchema = Database.Tenant.GetDataTable("SELECT * FROM vw_DataSourceTableTree ORDER BY TypeNo, ParentTableId, Relationship, TableName")
        LoadSchemaTableNode(SchemaTables.Nodes)
        SchemaTables.ExpandAll()
    End Sub
    Sub LoadSchemaTableNode(collection As TreeNodeCollection, Optional parentTableId As Integer = -1, Optional extraFilter As String = "")
        Dim filterString As String = "ParentTableId " + IIf(parentTableId = -1, "IS NULL", "= " & parentTableId)
        For Each cr As DataRow In dtSchema.Select(filterString + extraFilter)
            Dim cnode As New TreeNode
            cnode.Text = cr.GetString("TableName")
            cnode.Value = cr.GetString("TableId")
            Select Case cr.GetString("Type")
                Case "P"
                    cnode.ImageUrl = "~/App_Static/images/icon16/tree-p.jpg"
                    cnode.ImageToolTip = "Parcel Table"
                Case "N"
                    cnode.ImageUrl = "~/App_Static/images/icon16/tree-n.jpg"
                    cnode.ImageToolTip = "Neighborhood Table"
                Case "L"
                    cnode.ImageUrl = "~/App_Static/images/icon16/tree-l.jpg"
                    cnode.ImageToolTip = "Lookup Table"
                Case Else
                    Select Case cr.GetInteger("Relationship")
                        Case 0
                            cnode.ImageUrl = "~/App_Static/images/icon16/tree-121.jpg"
                            cnode.ImageToolTip = "One-to-one"
                        Case 2
                            cnode.ImageUrl = "~/App_Static/images/icon16/tree-m21.jpg"
                            cnode.ImageToolTip = "Many-to-one"
                    End Select
            End Select
            LoadSchemaTableNode(cnode.ChildNodes, cr.GetInteger("TableId"))
            collection.Add(cnode)
        Next
    End Sub

    Protected Sub TreeView1_SelectedNodeChanged(sender As Object, e As EventArgs) Handles SchemaTables.SelectedNodeChanged
        Dim nodeValue As String
        Dim TableName As String
        nodeValue = SchemaTables.SelectedNode.Value
        ViewState("TableId") = nodeValue
        TableName = SchemaTables.SelectedNode.Text
    End Sub
    Protected Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        If SchemaTables.CheckedNodes.Count > 0 Then
            ''get parcelTable 
            Dim parcelTable As String = Database.Tenant.Application.ParcelTable
            Dim parcelTableId As Integer = DataSource.GetTableId(Database.Tenant, parcelTable)
            ''get NeighborhoodTable 
            Dim NeighborhoodTable As String = Database.Tenant.Application.NeighborhoodTable
            Dim NeighborhoodTableId As Integer = DataSource.GetTableId(Database.Tenant, NeighborhoodTable)
            Dim tableId As Integer = Nothing
            For Each ck_node As TreeNode In SchemaTables.CheckedNodes
                tableId = ck_node.Value
                ''check whether the given table NeighborhoodTable or ParcelTable
                If (tableId = NeighborhoodTableId Or tableId = parcelTableId) Then
                    Alert("Cannot delete NeighborhoodTable or ParcelTable table.")
                Else
                    DataSource.DeleteTable(Database.Tenant, tableId)
                End If
            Next
            RunScript("setVisibilityButton();")
            RunScript("disableMask();")
            LoadSchemaTree()
        End If
    End Sub
End Class

                 