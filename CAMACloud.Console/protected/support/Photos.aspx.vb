﻿Imports System.IO
Imports System.Data.SqlClient
Public Class Photos
	
    Inherits System.Web.UI.Page
    
    Private Sub btnUpload_Click(sender As Object, e As EventArgs) Handles btnUpload.Click
    		
		Dim dtTemp As DataTable = New DataTable()
        Dim sr As New StreamReader(ControlFile.FileContent)
        Dim colName As String = "ParcelId"
        Dim colStatus As String = "CommittedStatus"        
        Dim SyncStatus As Integer = rbnFlagParcels.SelectedValue
        Dim UserName As String = Membership.GetUser().ToString()
        Dim LocalTime As String = Database.Tenant.GetStringValue("Select FORMAT((dbo.GetLocalDate(GETUTCDATE ())),'MM-dd-yyyy hh:mm tt') as LocalTime")
        Dim ParcelType As String
        Dim Tid As Integer
        Dim RecCount As Integer = 0
        Dim ipAddress As String = ""
        Dim ParcelCount As Integer = 0
        Dim csv As String = String.Empty
        Dim Description As String = "New photo commit list uploaded."

        If ControlFile.FileBytes.Length = 0 Then
           Return
        End If
        If HttpContext.Current IsNot Nothing Then
            ipAddress = HttpContext.Current.Request.ClientIPAddress
        End If
        
        Select Case ddlPhotoCommit.SelectedValue
			Case "0"	
				Alert("Please select either CC Parcel ID or keyfield1 before using this feature")
				Return
			Case "ParcelId"
				ParcelType = "P"
			Case "keyfield1"
				ParcelType = "K"			
        End Select
        
        If csv_to_datatableConvertion(sr, dtTemp) Then             
    		For Each dr As DataRow In dtTemp.Rows
    			RecCount += 1
    			If dr(0).ToString <> "" Then
                    If Regex.IsMatch(dr.GetString(colName), "^[0-9a-zA-Z .-]+$") Then
                        If (Convert.ToInt64(dr.GetString(colName)) < 1) Then
                            Alert("Your list could not be uploaded because '" + colName + "' has invalid value.\n Please revise your list and upload again.")
                            Return
                        End If
                    Else
                        Alert("Your list could not be uploaded because '" + colName + "' has invalid value.\n Please revise your list and upload again.")
                		Return
                	End If
                	If ((dr.GetString(colStatus).ToUpper().ToString() <> "YES") and (dr.GetString(colStatus).ToUpper().ToString() <> "NO"))
                		Alert("Your list could not be uploaded because '" + colStatus + "' has invalid value.\n Please revise your list and upload again.")
                		Return
                	End If
                End If
    		Next
    		
            Try 
				Dim changeSql As String = "INSERT INTO PhotoCommitBackup (UserName,IpAddress,InputType,UploadTime,SyncStatus,Status,TotalCount) VALUES ({0}, {1}, {2}, {3}, {4},{5},{6}); SELECT CAST(@@IDENTITY AS INT) As NewId;"
				Tid = Database.Tenant.GetIntegerValue(changeSql.SqlFormat(True, UserName, ipAddress, ParcelType, LocalTime,SyncStatus,"Pending",RecCount))
				
				Dim dt As DataTable = dtTemp
				dt.Columns.Add("PhotoCommitBackupid")
				dt.Columns.Add("CreatedTime")
				dt.Columns.Add("UserId")
				dt.Columns.Add("SyncStatus")
            	For Each ro As DataRow In dt.Rows
            		ro("PhotoCommitBackupid") = Tid
            		ro("CreatedTime") = LocalTime
            		ro("UserId") = UserName
            		ro("SyncStatus") = SyncStatus
            		If ro("committedstatus").ToLower() = "yes"
            			ro("committedstatus") = 1
            		Else If ro("committedstatus").ToLower() = "no"
            			ro("committedstatus") = 0	
            		End if	
            	Next
            	
            	Database.Tenant.LoadTableFromDataTable("PhotoCommit", dt)
                           	
				Dim inputString() As String = {"parcelid","committedstatus","CreatedTime","UserId","SyncStatus"}
				Dim ColumnHeader As List(Of String) =  New List(Of String)(inputString)
				Dim sql = "EXEC [SP_PhotoCommitConvertTableToCSVFormat] {0},{1},{2}".SqlFormat(True, "PhotoCommit", Tid, String.Join(",", ColumnHeader))
				csv = Database.Tenant.GetStringValue(sql)

                Database.Tenant.Execute("EXEC SP_PhotoCommit @BackupId ={0},@InputType = {1},@SyncStatus = {2},@TotalCount = {3},@CSVData = {4}".SqlFormatString(Tid, ParcelType, SyncStatus, RecCount, csv))
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
                Alert("Photo commit is completed.")
            	
           		If ddlPhotoCommit.Items.Count > 0 Then
        			ddlPhotoCommit.SelectedIndex = 0
        		End If

            Catch ex As Exception
                Alert(ex.Message)
            End Try
        Else RedirectforResubmission()    
        End If
        
    End Sub 
    
    
    Public Function csv_to_datatableConvertion(stream As StreamReader, ByRef dt As DataTable) As Boolean

        Try

            Dim pattern As String = "^\s*""?|""?\s*$"
            Dim rgx As New Regex(pattern)
            Dim Lines As String() = stream.ReadToEnd().Split(Environment.NewLine)
            Dim AppcolName As String = ""


            Dim Fields As String() = Lines(0).Split(New Char() {","c})
            Dim Cols As Integer = Fields.GetLength(0)
            For i = 1 To 10
                If Database.Tenant.Application.KeyField(i).IsNotEmpty Then
                    AppcolName = Database.Tenant.Application.KeyField(i)
                    Exit For
                End If
            Next

            If (Lines(0).Split(",").Length <> 2) Then
                Alert("There is should be 2 Fields in uploading CSV File")


                Return False


            End If

            If (Fields(0) <> "ParcelId" Or Fields(1) <> "CommittedStatus") Then
                Alert("Invalid data format please revise your entry")
                Return False
            End If
            '1st row must be column names. 
            For i As Integer = 0 To Cols - 1
                dt.Columns.Add(rgx.Replace(Fields(i).ToLower(), ""), GetType(String))
            Next
            Dim rgx1 As New Regex("(?!\B""[^""]*),(?![^""]*""\B)")    'Regex to split comma not in quotes
            Dim Row As DataRow
            Dim PSLCount As Integer = Lines.GetLength(0) - 1
            If(PSLCount=0)
            	Alert("No parcels found")
            	Return False
        	End If
            For i As Integer = 1 To Lines.GetLength(0) - 1
                If Lines(i).Trim.Length = 0 Then
                    Continue For
                End If
                Fields = rgx1.Split(Lines(i))
                If Fields.Length <> 2 Then
                	Alert("Invalid data format please revise your entry")
                	Return False
                End If

                If Fields.Length < Cols Then
                    Continue For
                End If
                Row = dt.NewRow()
                If Fields(0).Trim() <> "" Then
                    For j As Integer = 0 To Cols - 1
                        If Fields.Length <> Cols Then
                            Exit For
                        End If
                        Row(j) = rgx.Replace(Fields(j), "")
                    Next
                    dt.Rows.Add(Row)
                End If
            Next
            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function
    
    Public Sub RedirectforResubmission()
     RunScript("RedirectPage();")
    End Sub
    
End Class