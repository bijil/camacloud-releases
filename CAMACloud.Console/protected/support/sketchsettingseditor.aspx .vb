﻿Imports System.IO
Public Class sketchsettingseditor
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
        	LoadGrid()
        	CancelForm()
        End If
    End Sub

    Sub LoadGrid()
        Dim Sql As String
        Sql = "SELECT * FROM  SketchSettings  ORDER BY Name"
        grid.DataSource = Database.Tenant.GetDataTable(Sql)
        grid.DataBind()
    End Sub

    Private Sub grid_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grid.RowCommand
        Select Case e.CommandName
        	Case "EditItem"
        		Dim type As String = e.CommandArgument
			    type = type.Replace("'","''")
			    Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM  SketchSettings WHERE Name = '" & type & "'")
			    If dr Is Nothing Then
	            	Alert("Sketch Settings have been modified.Please reload and try again.")
	            Else
	                hname.Value = dr.GetString("Name")
	                txtName.Text = dr.GetString("Name")
	                txtValue.Text = dr.GetString("Value")
	                btnSave.Text = "Save "
	                btnCancel.Visible = True
	                lblHeadText.Text = "Edit Settings"
                End If
        	Case "DeleteItem"
        	    Dim clnt As String = e.CommandArgument
			    clnt = clnt.Replace("'","''")
            	Database.Tenant.Execute("DELETE FROM  SketchSettings  WHERE Name = '" & clnt & "'")
                Dim Description As String = "Sketch Setting (" + clnt + ") has been deleted."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description) 
                CancelForm()
                LoadGrid()
                
                'NotificationMailer.SendNotificationToVendorCDS(Membership.GetUser().ToString, 2, "Client Settings")
        End Select
    End Sub

Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
		Dim isRowExist As Boolean = False
		Dim isSSExist As Boolean = False
		Dim isSSExist1 As Boolean = False
		Dim type As String = txtName.Text.Trim()
		Dim sValue As String = txtValue.Text.Trim()
		If ( type = "DoNotShowLabelCode" And sValue = "1" ) Then
			isSSExist = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM ClientSettings WHERE Value = '1' AND  Name = 'DoNotShowLabelDescriptionSketch' ")
			If Not isSSExist Then
				isSSExist = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM SketchSettings WHERE Value = '1' AND  Name = 'DoNotShowLabelDescriptionSketch' ")
			End If
		Else If ( type = "DoNotShowLabelDescriptionSketch" And sValue = "1" ) Then
			isSSExist = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM SketchSettings WHERE Value = '1' AND Name = 'DoNotShowLabelCode' ")
		End If
		If ( type = "DoNotShowLabelCodeInDropDown" And sValue = "1" ) Then
			isSSExist1 = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM ClientSettings WHERE Value = '1' AND  Name = 'DoNotShowLabelDescription' ")
			If Not isSSExist1 Then
				isSSExist1 = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM SketchSettings WHERE Value = '1' AND  Name = 'DoNotShowLabelDescription' ")
			End If
		Else If ( type = "DoNotShowLabelDescription" And sValue = "1" ) Then
			isSSExist1 = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM SketchSettings WHERE Value = '1' AND Name = 'DoNotShowLabelCodeInDropDown' ")
		End If
				
		If isSSExist Then
			Alert("Unable to save changes! " + "\n" + "Either the Sketch Code (DoNotShowLabelCode) or Sketch Description (DoNotShowLabelDescriptionSketch) must be configured to display on the UI.")
		Else If isSSExist1 Then
			Alert("Unable to save changes! " + "\n" + "Either the Sketch Code (DoNotShowLabelCodeInDropDown) or Sketch Description (DoNotShowLabelDescription) must be configured to display on the UI.")	
		Else
			If (hname.Value = "") Then
				type = type.Replace("'","''")
				isRowExist = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM SketchSettings WHERE Name='" & type & "'")
			Else
				Dim temphname As String = hname.Value
				temphname = temphname.Replace("'","''")
				Dim sssql As String = "SELECT COUNT(*) FROM SketchSettings WHERE Name='" & temphname & "'"
	    	    isRowExist = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM SketchSettings WHERE Name='" & temphname & "'")
	    	End If      	
	        UpdateValue(isRowExist)
	        CancelForm()
	        LoadGrid() 
        End If
    End Sub
    Private Sub UpdateValue(isRowExist As Boolean)
        Dim Sql As String = ""
        If isRowExist Then
            Dim oldval As String
            If (hname.Value = "") Then
                Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM sketchsettings WHERE Name = '" & txtName.Text.Trim() & "'")
                oldval = dr.GetString("Value")
            Else
                Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM sketchsettings WHERE Name = '" & hname.Value & "'")
                oldval = dr.GetString("Value")
            End If

            If (hname.Value = "") Then
                Sql = "UPDATE SketchSettings Set Name ={1},Value = {2}  WHERE Name = {0}"
                Database.Tenant.Execute(Sql.SqlFormat(False, txtName.Text.Trim(), txtName.Text.Trim(), txtValue.Text.Trim()))
            Else
                Sql = "UPDATE SketchSettings Set Name ={1},Value = {2}  WHERE Name = {0}"
                Database.Tenant.Execute(Sql.SqlFormat(False, hname.Value, txtName.Text.Trim(), txtValue.Text.Trim()))
            End If
            If (hname.Value = txtName.Text.Trim()) Then
                If (oldval <> txtValue.Text) Then
                    Dim Description As String = "(" + txtName.Text.Trim() + "," + txtValue.Text.Trim() + ") has been updated in Sketch Settings."
                    SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
                End If
            ElseIf (hname.Value = "") Then
                If (oldval <> txtValue.Text) Then
                    Dim Description As String = "(" + txtName.Text.Trim() + "," + txtValue.Text.Trim() + ") has been updated in Sketch Settings."
                    SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
                End If
            Else
                Dim Description As String = "(" + hname.Value.Trim() + "," + oldval + ") has been updated to (" + txtName.Text.Trim() + "," + txtValue.Text.Trim() + ") in Sketch Settings."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)

            End If
        Else
            Sql = "INSERT INTO SketchSettings (Name, Value) VALUES ( {0}, {1} )"
            Database.Tenant.Execute(Sql.SqlFormat(False, txtName.Text.Trim(), txtValue.Text.Trim()))
            Dim Description As String = "("+txtName.Text.Trim()+","+txtValue.Text.Trim()+") has been Inserted in Sketch Settings."
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description) 
        End If
        Alert("Setting saved successfully")
    End Sub

Private Sub CancelForm() Handles btnCancel.Click
		hname.Value = ""
        txtName.Text = ""
        txtValue.Text = ""
        btnSave.Text = "Add "
        btnCancel.Visible = False
        lblHeadText.Text = "Add Settings"
    End Sub

    Protected Sub OnPageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        grid.PageIndex = e.NewPageIndex
        Me.LoadGrid()
    End Sub
End Class