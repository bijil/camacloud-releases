﻿<%@ Page Title="MRA Result" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/Popup.master" CodeBehind="popup.result.aspx.vb" Inherits="CAMACloud.Console.popup_result" %>
<%@ Register TagPrefix="cc" TagName="MRAResultView" Src="~/App_Controls/mra/MRAResultView.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <cc:MRAResultView runat="server" />
</asp:Content>
