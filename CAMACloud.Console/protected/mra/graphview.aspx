﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DesktopWeb.master"  CodeBehind="graphview.aspx.vb" Inherits="CAMACloud.Console.graphview" %>
<%@ Register TagPrefix="cc" TagName="MRA_NavigationControl" Src="~/App_Controls/mra/MRA_NavigationControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
 <asp:Content ID="Content3" ContentPlaceHolderID="NavBarContent" runat="server">
 <div class="Overlay_Div">
    <cc:MRA_NavigationControl ID="mra_nav" runat="server" />
    </div>
 </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<asp:HiddenField runat="server" ID="ddlselectedId" Value="" />
<asp:HiddenField runat="server" ID="ddlFieldId" Value="" />
  <asp:Panel runat="server" ID="pnlResidual" Visible="false">
            <h3>Residual Output</h3>
            <asp:GridView runat="server" ID="residuals">
                <Columns>
                    <asp:BoundField DataField="ObservationNo" HeaderText="Observation No" ItemStyle-Width="80px" />
                    <asp:BoundField DataField="AssignmentGroup" HeaderText="Assignment Group" ItemStyle-Width="200px" />
                    <asp:BoundField DataField="KeyValue" HeaderText="Parcel" ItemStyle-Width="180px" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-CssClass="th-right" />
                    <asp:BoundField DataField="PredictedValue" HeaderText="Predicted Value" ItemStyle-Width="180px" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-CssClass="th-right" />
                        <asp:BoundField DataField="Residual" HeaderText="Residual" ItemStyle-Width="180px" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-CssClass="th-right" />
                </Columns>
            </asp:GridView>
        </asp:Panel>
        <p>
         
        </p>
    </asp:Panel>
	<script type="text/javascript" src="/App_Static/jslib/chartJS.js"></script>
	<script type="text/javascript" src="/App_Static/jslib/infobubble.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/chartjs-plugin-annotation/0.5.7/chartjs-plugin-annotation.min.js"></script>
    <style>
    #divContentArea {
    top:100px;
    }
    .divNavBtn {
        padding: 4px 0px 4px;
    }
    .canvasClass{ 
    border: 1px solid black;    
    pointer-events: auto;
    }
    .mapButton {
   	background-color: #167ccf;
    border: none;
    color: white;
    padding: 1px 16px;
    text-align: center;
    font-size: 13px;
    margin: 5px 6px 0px 5px;
}
.tdinfo{
    font-weight: bolder;
    font-size: 11px;
    padding: 0px 1px 0px 4px;
    white-space:nowrap;
}
    .mraBtn {  
    font-weight: bold;
    margin-left: 22px;
    border:none;
    border-radius: 0px;
    background: linear-gradient(#88c2f1,#326b84);
    color: white;
    font-family: sans-serif;
    font-size: 12px;
    cursor: pointer;
    height: 22px;
    }
    .infoDiv{
    cursor: default;
    clear: both;
    position: relative; 
    background-color: rgb(250, 250, 250);
}
    </style>
<div style="float: right; margin: 2px 35px 0px 0px;"> 
<asp:button ID="btnExport" runat="server" cssClass="exportbtn BtnAsLink"  text="Export Results" style=" display:none"/>
<button class="mapbtn BtnAsLink" autoPostback=false onclick=" openInMap(); return false;" style="display:none;">Switch to Map</button> 
 <button class="scatterbtn BtnAsLink" autoPostback=false onclick=" OpenScattergraph(); return false;" style="display:none;">Switch to Scatter Plot</button> </div>
<div class="mapview" Id="mapView" runat="server" style="margin-top : 27px;  margin-left: 13px;display:none;float: left; ">
   <div class="gmap" id="gmap" style="height: 88%;width: 98%;position: absolute;overflow: hidden; ">            </div>
</div>
<asp:HiddenField runat="server" ID="hasRoles" Value="0" />
<div class="scatterData Cont_Content">

   <asp:Panel runat="server" ID="pnlSelect" Style="float: left;" cssClass="pnlSelect">
      <table class="fieldselect">
         <tr>
         <%--<div style="display:none;">
          <td style="width: 95px;">Select Model: </td>
            <td>
               <asp:DropDownList runat="server" ID="ddlModel" cssClass="ddlModel" Width="240px" AutoPostBack="true" />
            </td>
            </div>--%>
            <td style="width:113px;">Select Field: </td>
            <td style=" padding-right: 25px;">
               <asp:DropDownList runat="server" ID="ddlFields" cssClass="ddlFields" style="min-width: 152px;" AutoPostBack="false" />
            </td>
            <td style="width: 110px;">No of Samples: </td>
            <td style="width: 90px;">
               <asp:DropDownList runat="server" ID="ddlCount" cssClass="ddlCount" Width="90px" AutoPostBack="false" >
                  <asp:ListItem Text="-- Select --" value=""/>
                  <asp:ListItem Text="100" value="100"/>
                  <asp:ListItem Text="500" value="500"/>
                  <asp:ListItem Text="1000" value="1000"/>
                  <asp:ListItem Text="5000" value="5000"/>
                  <asp:ListItem Text="50000" value="50000"/>
               </asp:DropDownList>
            <td>
               <button  type="button" Class="btnShow mraBtn" onClick="validateSample()" /> Show Plot</button>
            </td>
         </tr>
      </table>
   </asp:Panel>
  
   <body>
      <div class='scatterDiv' id="divscatterData" style="width:100%;float:left;" visible="false">
		<div class="trdownload" style="display:none;float: right;">
			<a id='download' download='ScatterGraph.png' style="float:right;cursor: pointer;">  Download Graph</a>
			<span style="float:right;color:rgb(54, 162, 235);margin-right: 4px;">&#x25BC;</span>
		</div>
		<div class="CanvasDiv"  style="position: relative; height:63vh; width:95vw">
				<canvas id="canvas" Class='canvasClass' style="display:none;"></canvas>
		</div>
      </div>
      <div id="divDrawRect" style="float: left;margin-top: 28px;display:none">
 		<table class='rectTable' style="display:none;background-color: #efefef;">
   			<tr class="rangeFields"> <td>
      			<table style='padding: 13px;'>
         			<tr><td> <span class="xField" style='font-weight:bold;font-size: 13px;'></span> </td> </tr>
         			<tr><td>&emsp;From &emsp;<asp:TextBox runat="server" cssClass="Xfrom" ID="Xfrom" Width="100px"/></td>
         				<td>&emsp;To &emsp;<asp:TextBox runat="server" cssClass="Xto" ID="Xto" Width="100px"/></td>
         			</tr>
         			<tr><td style='font-weight:bold;font-size: 13px;'> Residual (Y) :</td> </tr>
         			<tr><td>&emsp;From &emsp;<asp:TextBox runat="server"  cssClass="Yfrom" ID="Yfrom" Width="100px"/> </td>
            			<td>&emsp;To &emsp;<asp:TextBox runat="server" cssClass="Yto" ID="Yto" Width="100px"/></td>
         			</tr>
      			</table>
      			</td>
   			</tr>
   			<tr><td class='parcelCount' style:'display:none'></td></tr>
   			<tr><td>
         		<button class="btnRange mraBtn" autoPostback=false style='margin-left: 35% !important;' onclick="parcelsRange();return false;" >Draw Rectangle</button> 
      			</td>
  			</tr>
	 </table>
</div>
</div>
    <script>
    $(function(){
      $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }});
      if ($(".ddlFields").val() == ""){
    	$('.ddlCount').val("")
    	}
    setScreen();
    $(window).bind('resize', setScreenLayout);
	$("#spanMainHeading").html('Residuals - Graph View');
	$('.graph').hide();
	showHide();
})
function setScreen(){
	//hgtSide=$(window).height() - 262;
	//$('#canvas').css('height',hgtSide);
}
    function setScreenLayout() {
	//	wdthSide=$(window).width() - 80;
	//$('#canvas').css('width',wdthSide);

}
  document.getElementById('download').addEventListener('click', function() {
    downloadCanvas(this, 'canvas', 'scattergraph.png');
}, false);
 function showHide(){
   		var tempId	= $('.ddlModel').find(":selected").val();
        if(tempId != "") $('.fieldselect').show();
    	else{ $('.fieldselect').hide();  $('.Rght_linkdiv').hide();}
    }
var map;
Chart.pluginService.register({
    beforeDraw: function(chart, easing) {
        if (chart.config.options.chartArea && chart.config.options.chartArea.backgroundColor) {
            var ctx = chart.chart.ctx;
            var chartArea = chart.chartArea;
            ctx.save();
            ctx.fillStyle = chart.config.options.chartArea.backgroundColor;
            ctx.fillRect(chartArea.left - 80, chartArea.top - 50, chartArea.right - (chartArea.left - 100), chartArea.bottom - (chartArea.top - 100));
            ctx.restore();
        }
    }
});
function initGoogleMap(callback) {
    mapZoom = 5;
    mapPosition = new google.maps.LatLng(42.345573, -71.098326);
    mapCenter = new google.maps.LatLng(35.56, -96.84);
    var mapOptions = {
        zoom: mapZoom,
        minZoom: 7,
        mapTypeId: google.maps.MapTypeId.HYBRID,
        center: mapCenter,
        gestureHandling: 'greedy'
    };
    map = new google.maps.Map(document.getElementById('gmap'), mapOptions);
     google.maps.event.addListener(map.getStreetView(),'visible_changed',function(){
          if(this.getVisible()) clearMarkers();
          else  setMapview();   
     })  
}

function downloadCanvas(link, canvasId, filename) {
    link.href = document.getElementById(canvasId).toDataURL();
    link.download = filename;
}
var graphData = [];
var pageSize = 0;
var residualRecords = [];
var firstLoad = true;
var bounds = {};
var maxVal=0;
function changeSelectedField(){
    var tempId=$("#<%=ddlselectedId.ClientID %>").val();
    $('.ddlModel option[value="'+tempId+'"]').prop('selected', true);
    }
   var fId;
function getResidualRecords(callback) {
	residualRecords=[];
    var count = $('.ddlCount').val();
     fId = $('.ddlFields ').val();
     $('#<%=ddlFieldId.ClientID%>').val(fId);
    var tempid =$('#<%= ddlselectedId.ClientID%>').val();  //$('.ddlModel option[selected]').val();
    $.ajax({
        type: 'POST',
        url: "/mra/residualrecords.jrq",
        dataType: 'json',
        data: {
            fieldid: fId,
            count: count,
            tempID: tempid
        },
        success: function(res) {
            if (res) {
                var i = 0
                while (i < res.length) {
                    var item = {
                        x: parseInt(res[i].fieldValue),
                        y: res[i].Residual,
                        pid: res[i].ParcelId
                    }
                    residualRecords.push(item);
                    i++;
                }
            }
            if (callback) callback();
        },

        error: function(response) {
            console.error(response);
        }

    })

}

function getCount(startX, startY, endX, endY, callback) {
    var count = $('.ddlCount').val();
    var fId = $('.ddlFields ').val();
    var tempid = $('#<%= ddlselectedId.ClientID%>').val(); //$('.ddlModel option[selected]').val();
    $.ajax({
        type: 'POST',
        url: "/mra/selectedrecords.jrq",
        dataType: 'json',
        data: {
        	jobType:'count',
            fieldid: fId,
            count: count,
            startX: startX,
            startY: startY,
            tempID: tempid,
            endX: endX,
            endY: endY
        },
        success: function(res) {
            if (callback) 
           		if (res && res.length) callback(res[0].parcelCount);
            	else callback(0);
        },

        error: function(response) {
            console.error(response);
        }

    })

}
var parcelsInMap =[];
var infoBubble = new InfoBubble({
      map: map,
      shadowStyle: 1,
      padding: 2,
      backgroundColor: '#fafafa',
      arrowSize: 10,
      disableAutoPan: true,
      hideCloseButton: true,
      arrowPosition: 30,
      borderRadius:10,
      backgroundClassName: 'infoDiv',
      arrowStyle: 2
});
function clearMarkers(){
 if (mapmarkers) {
            var keys = Object.keys(mapmarkers);
            for (x in keys) {
                if (mapmarkers[keys[x]].setMap) {
                    mapmarkers[keys[x]].setMap(null);
                    mapmarkers[keys[x]] = null;
                }
            }
            mapmarkers = [];
        }
        infoBubble.close();
}
var mapmarkers=[];
function setMapview(){
	clearMarkers();
	var count = $('.ddlCount').val();
    var fId = $('.ddlFields ').val();
    var tempid = $('#<%= ddlselectedId.ClientID%>').val(); ;//$('.ddlModel option[selected]').val();
    var fieldName=$(".ddlFields option[value="+fId+"]").text();//$('.ddlFields option[selected]').text();
    parcelsInMap=[];
    bounds = new google.maps.LatLngBounds();
    $.ajax({
        type: 'POST',
        url: "/mra/selectedrecords.jrq",
        dataType: 'json',
        data: {
        	jobType:'map',
            fieldid: fId,
            count: count,
            startX: startX,
            startY: startY,
            tempID: tempid,
            endX: endX,
            endY: endY
        },
         success: function(res) {
             if (!res )
                 return;
             parcelsIn = res;
             var parcels = [];
             var parcel;
             for (x in res) {
                 var point = res[x];
                 var pid = point.ParcelId.toString();
                 if (parcelsInMap[pid] == null)
                     parcelsInMap[pid] = {
                         ParcelId: point.ParcelId,
                         Keyvalue1: point.Keyvalue1,
                         fieldValue: point.fieldValue, 
                         residual :point.Residual,
                         infoSub: null,
                         Points: []
                     };
                 parcelsInMap[pid].Points.push(point.Points);
             }
             var totalParcels = Object.keys(parcelsInMap).length;
             var pinfo = [];
             for (x in parcelsInMap) {
                 parcel = parcelsInMap[x];
                 var b = new google.maps.LatLngBounds();
				var fvalue= parcel.fieldValue ? parcel.fieldValue.toString():'';
				parcel.infoSub = "<table style='padding: 12px 0px 8px 2px;'><tr><td colspan='2'> <span class='tdinfo'>Parcel# : </span> " + parcel.Keyvalue1.toString() + "</td></tr><tr><td colspan='2'> <span class='tdinfo'>"+ fieldName +" :</span> " + fvalue + "</td></tr><tr><td colspan='2'> <span class='tdinfo'>Residual : </span>" + parcel.residual.toString() + "</td></tr><tr><td><button class='mapButton' onclick='openThisParcel(" + parcel.ParcelId + ");return false;'>Open</button></td><td><button class='mapButton' onclick='infoBubble.close();return false;' style = 'float : right;'>Close</button></td></tr></table>";
                 for (i in parcel.Points) {
                     pstr = parcel.Points[i].split(',');
                     loc = new google.maps.LatLng(parseFloat(pstr[0].replace('[', '')), parseFloat(pstr[1].replace('[', '')));
                     if (parcel.isSubject != "1")
                         bounds.extend(loc);
                     b.extend(loc);
                 }
				if(mapmarkers[parcel.ParcelId.toString()]==null){
                 mapmarkers[parcel.ParcelId.toString()] = new google.maps.Marker({
                     position: new google.maps.LatLng(b.getCenter().lat(), b.getCenter().lng()),
                     map: map,
                     icon:"/App_Themes/Cloud/images/subParcel.png"
                 });
                  mapmarkers[parcel.ParcelId.toString()].pid = parcel.ParcelId;
                mapmarkers[parcel.ParcelId.toString()].Keyvalue1 = parcel.Keyvalue1;
                 }
                 google.maps.event.addListener(mapmarkers[parcel.ParcelId.toString()], 'click', (function() {
                  var e = this;
                  var pdata = parcelsInMap[this.pid]
                         infoBubble.close();
                         infoBubble.setContent(pdata.infoSub.toString());
                         $('.infoDiv').parent().css('height','100%','important');
                		 $('.infoDiv').parent().css('width','100%','important');
                         infoBubble.open(map, e);
                 }));
			
             }
             map.setCenter(bounds.getCenter());
             map.fitBounds(bounds);
             firstLoad = false;
         },
         error: function(response) {
             var test = response;
         }
     });

}
var scatterChartData = {};
var plotObject;
var startX, startY, endX, endY;
startX = 0, startY = 0, endX = 0, endY = 0;
function updateChartData(startX, startY, endX, endY, callback) {
    var ctx = document.getElementById("canvas").getContext("2d");
    scatterChartData = {
    datasets: [{
        label: $(".ddlFields option:selected").text(),
        borderColor: "rgb(54, 162, 235)",
        backgroundColor: "rgb(54, 162, 235)",
        data: residualRecords,
    }]
};
    plotObject = new Chart(ctx, {
        type: 'scatter',
        data: scatterChartData,
        options: {
            responsive: true ,
            maintainAspectRatio: false,
            hoverMode: 'single', // should always use single for a scatter chart			 
            annotation: {
                drawTime: "afterDatasetsDraw",
                annotations: [{
                    type: 'box',
                    xScaleID: 'x-axis-1',
                    yScaleID: 'y-axis-1',
                    xMin: 0,
                    xMax: 0,
                    yMin: 0,
                    yMax: 0,
                    backgroundColor: 'rgba(101, 33, 171, 0.5)',
                    borderColor: 'rgba(255,255,255,1)',
                    borderWidth: 1
                }]
            },
			tooltips:{enabled:false},
            scales: {
                xAxes: [{
                    type: 'linear',
                    gridLines: {
                        zeroLineColor: "#fff"
                    }
                }]
            },
            chartArea: {
                backgroundColor: 'rgba(255,255,255,1)'
            }
        }
    });
    if (callback) callback();
}
var clearFields = true;

function drawScatterPlot() {
$('#canvas').show();
    getResidualRecords(function() {
       // if(endY==0) endY=maxVal;
        updateChartData(startX, startY, endX, endY, function() {
            $('.trdownload').show();
            plotObject.update();
            $('.rectTable').show();
            
            $('.xField').html($(".ddlFields  option[value="+fId+"]").text() + '  (X) :');
            if (clearFields)
                $('.rangeFields input[type="text"]').val('')
			if(disabletooltip &&plotObject)
    			plotObject.config.options.tooltips.enabled=false;
        })
        setScreen();
         setScreenLayout();
    });
     $('.mapbtn').show();
     $('.exportbtn').show();
    $('.scatterbtn').hide();
}
$('#canvas').off("click")
$('#canvas').click(function(evt) {
    if ($('#<%= hasRoles.ClientID%>').val() != "1")
        alert("You do not have the user role to access the Quality Control module. Please speak with your CAMA Cloud administrator on access to the Quality Control module.")
    else {
        if (plotObject) {
            var activePoint = plotObject.getElementAtEvent(evt)[0];
            if (!activePoint || activePoint === undefined) return;
            var data = activePoint._chart.data;
            var datasetIndex = activePoint._datasetIndex;
            var label = data.datasets[datasetIndex].label;
            var value = data.datasets[datasetIndex].data[activePoint._index];
            openThisParcel(value.pid);
        }
    }
});
        var drag = 0, moving = false
      $('#canvas').bind('mousedown', function (evt) {
            drag = 1;
            startY = getYValue(evt);
            startX = getXValue(evt)
        })
        $('#canvas').bind('mousemove', function (evt) {
            if (drag == 1) {
                moving = true;
                endY = getYValue(evt);
                endX = getXValue(evt);
                // console.log( startX, startY, endX, endY );
                updatePlot();
            }
        })
        $('#canvas').bind('mouseup', function (evt) {
            drag = 0
            if (moving) {
                endY = getYValue(evt);
                endX = getXValue(evt);
                //  console.log( startX, startY, endX, endY );
                updatePlot()
            }
        })
        function getYValue(evt) {
            var scaler = plotObject.scales['y-axis-1'];
            var chart_height_px = scaler.bottom + scaler.top;
            var y = evt.clientY - plotObject.canvas.getBoundingClientRect().top - scaler.top;
            var yval = scaler.max - y / scaler.height * (scaler.max - scaler.min);
            return yval
        }
        function getXValue(evt) {
            var xscaler = plotObject.scales['x-axis-1'];
            var chart_height_px2 = xscaler.left + xscaler.right;
            var x = evt.clientX - plotObject.canvas.getBoundingClientRect().left - xscaler.left;
            var xval = x / xscaler.width * (xscaler.max - xscaler.min);
            return xval
        }
        function updatePlot() {
            if (plotObject.options.annotation.annotations && plotObject.options.annotation.annotations.length > 1)
                plotObject.options.annotation.annotations.pop();
            plotObject.options.annotation.annotations.push( {
                type: 'box',
                xScaleID: 'x-axis-1',
                yScaleID: 'y-axis-1',
                xMin: startX,
                xMax: endX,
                yMin: startY,
                yMax: endY,
                backgroundColor: 'rgba(175, 206, 255, 0.5)',
                borderColor: 'rgba(115, 158, 226,1)',
                borderWidth: 1
            } );
			 $( '.Xfrom' ).val(Math.round(startX * 10000) / 10000  )
             $( '.Yfrom' ).val( Math.round(startY * 10000) / 10000    )
             $( '.Xto' ).val(Math.round(endX * 10000) / 10000   ) 
             $( '.Yto' ).val(Math.round(endY * 10000) / 10000   )
			if(endX < startX)
			 {
				$( '.Xfrom' ).val( Math.round(endX * 10000) / 10000    );
				$( '.Xto' ).val(Math.round(startX * 10000) / 10000   ) 
			 }
			 if(endY < startY)
			 {
				 $( '.Yfrom' ).val(Math.round(endY* 10000) / 10000    )
				   $( '.Yto' ).val(Math.round(startY* 10000) / 10000   )
			 }
            plotObject.update()
        }
function openThisParcel(pid){
			var loc = "../quality/#/parcel/" + pid;
            window.open(loc);
}

function checkAllTextfields() {
    var noval = false;
    var textfields = $('.rangeFields input[type="text"]')
    $(textfields).each(function(i) {
        if ($(textfields[i]).val() == "") noval = true;
    })
    return noval;
}

function validateExport() {
    var resp = checkAllTextfields();
    if (resp) {
        alert('Please fill all the fields');
        return false;
    }
    return true;
}
var   disabletooltip=false;;
function parcelsRange() {
    startX = isNaN(parseFloat($('.Xfrom').val())) ? 0 : parseFloat($('.Xfrom').val());
    startY = isNaN(parseFloat($('.Yfrom').val()))? 0 : parseFloat($('.Yfrom').val());
    endX = isNaN(parseFloat($('.Xto').val())) ? 0 :parseFloat($('.Xto').val());
    endY = isNaN(parseFloat($('.Yto').val())) ? 0 : parseFloat($('.Yto').val());
}
//removed 
function setImageDownloadable() {
    var url_base64 = document.getElementById('canvas').toDataURL('image/png');
    link.href = url_base64;
    $('#link').click();
}

function changeColor() {
    plotObject.data.datasets[0].borderColor = '#f39c12';
    plotObject.update()
}

function validateSample() {
    if ($(".ddlModel option:selected").val() == "")
        alert('Please choose a model.');
    else if ($(".ddlFields option:selected").val() == "")
        alert('Please select a field.');
    else if ($(".ddlCount option:selected").val() == "")
        alert('Please select number of samples.');
    else
    	drawScatterPlot();    
}
var OpenInMap = false;
function openInMap() {
	parcelsRange();
	if (startX == endX || startY== endY)
		startX=0,startY=0,endX=0,endY =0;
     initGoogleMap();
    OpenInMap = true
    $('.mapview').show();
    $('.mapbtn').hide();
    $('.scatterbtn').show();
    $('.scatterData').hide()
	setMapview();
	infoBubble.setMinHeight('100%');
	infoBubble.setMinWidth('100%');
}
function OpenScattergraph(){
	OpenInMap = false;
    $('.mapview').hide();
	 $('.mapbtn').show();
    $('.scatterbtn').hide();
    $('.scatterData').show();
  	//setScreen();
   // setScreenLayout();
}
    </script>
    </body>
 </div>
</asp:Content>
