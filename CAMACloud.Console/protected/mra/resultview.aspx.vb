﻿Public Class resultview
    Inherits System.Web.UI.Page
    Dim Private WithEvents mraResult As MRA_NavigationControl
    Dim Private withEvents mra_result_view As MRAResultView 
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    	hTempId.Value = -1
    	mraResult = New MRA_NavigationControl ()
    	AddHandler mraResult.ddlchanged, AddressOf Me.NavControl_ddlmodel_resultView
    	If Not IsPostBack Then
    		hTempId.Value = Request("_tid")		
   		End If
		mra_result_view = New MRAResultView ()
    End Sub
	Protected Sub NavControl_ddlmodel_resultView(ByVal sender As Object, ByVal e As EventArgs) Handles mraResult.ddlchanged
		If (HttpContext.Current.Request.Url.AbsolutePath.Contains("resultview")) Then
			Dim d = MRA_NavigationControl.__mraDDLItem
			If d > 0 Then		
				'mra_result_view.OpenResults(d)
				HttpContext.Current.Response.Redirect("resultview.aspx?_tid=" & d)
				hTempId.Value=d
			End If
		End If
		RemoveHandler mraResult.ddlchanged, AddressOf Me.NavControl_ddlmodel_resultView
    End Sub
End Class