﻿Imports CAMACloud.BusinessLogic.DataAnalyzer
Imports System.IO

Public Class settings
    Inherits System.Web.UI.Page

    Dim fields As List(Of DataSourceField)

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        fields = DataSourceField.GetFields
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
        	LoadGrid()
        	divAddcontnt1.Visible =False
        	divAddcontnt2 .Visible =False 
            'ddlExpFields.InsertItem("--Select fields--", "0", 0)
            'ddlField.FillFromSql("SELECT df.Id AS ID,df.Name As Name FROM DataSourceField df INNER JOIN DataSourceTable dt ON df.TableId = dt.Id WHERE dt.CC_TargetTable = 'ParcelData' AND df.DataType = 5", True)
            ddlLookup.FillFromSql("SELECT DISTINCT LookupName AS ID, LookupName As Name, 0 As Ordinal FROM ParcelDataLookup UNION SELECT '$QUERY$', '[Custom Query]', 1 As Ordinal ORDER BY Ordinal, Name", True)
'            cbl_ExpFields.DataSource = Database.Tenant.GetDataTable("SELECT df.Id, df.AssignedName FROM DataSourceField df INNER JOIN DataSourceTable dt ON df.TableId = dt.Id WHERE dt.CC_TargetTable = {0} ORDER BY df.Id".SqlFormatString("ParcelData"))
'            cbl_ExpFields.DataTextField = "AssignedName"
'            cbl_ExpFields.DataValueField = "Id"
'            cbl_ExpFields.DataBind()
			'lbExportFields.Items.Clear()
			'lbExportFields.DataSource = Database.Tenant.GetDataTable("SELECT df.Id, df.AssignedName FROM DataSourceField df inner join DataSourceTable dt on df.TableId = dt.Id where dt.CC_TargetTable = ''ParcelData'' ORDER BY df.Id")
			'lbExportFields.DataSource = Database.Tenant.GetDataTable("SELECT AssignedName,Id FROM DataSourceField ORDER BY Id")
			'lbExportFields.DataBind()
        End If
    End Sub


    Sub LoadGrid()
        gvSettings.DataSource = Database.Tenant.GetDataTable("SELECT RowId,FieldId,Label,LookupQuery,UseXMLLookup,LookupName FROM MRA_Filters WHERE IsExportField = 0")
        gvSettings.DataBind()

    End Sub

    Private Sub gvSettings_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSettings.PageIndexChanging
        gvSettings.PageIndex = e.NewPageIndex
        LoadGrid()
    End Sub

    Private Sub gvSettings_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSettings.RowCommand
        Dim rowid As Integer = e.CommandArgument
        If e.CommandName = "EditAction" Then
            hcid.Value = rowid
            Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM MRA_Filters where RowId = " & rowid)
            If dr.GetString("UseXMLLookup") = True Then
	                             txtLookupQuery.Style.Remove("Display")
	                             'txtLabel.Attributes.Add("style","margin-left:-295px")
	                     Else
	                             txtLabel.Style.Remove("margin-left")
	                     txtLookupQuery.Attributes.Add("style","Display:none")
	       End If
'	       Dim extraFieldsInExport As String = dr.GetString("extraFieldsInExport")
'	       If extraFieldsInExport.Length > 0
'				Dim array = extraFieldsInExport.Split(",")
'				For Each value As String In array
'					value = value.Substring( 1 )
'	        		cbl_ExpFields.Items.FindByValue(value.Substring(0,value.Length-1)).Selected = True
'				Next
'			End If	
            ddlField.SelectedValue = dr.GetString("FieldId")
            txtLabel.Text = dr.GetString("Label")
            txtLookupQuery.Text = dr.GetString("LookupQuery")
            ddlLookup.SelectedValue = dr.GetString("LookupName")
            divAddcontnt1.Visible =True
            divAddcontnt2.Visible =True 
        End If
        If e.CommandName = "DeleteAction" Then
        	Database.Tenant.Execute("DELETE FROM MRA_Filters where RowId = " & rowid)
        	divAddcontnt1.Visible =False
        	divAddcontnt2.Visible =False
        	btnAdd.Visible = True
        End If
        LoadGrid()
    End Sub

    Private Sub ddlField_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlField.SelectedIndexChanged
        If ddlField.SelectedValue = "" Then
            txtLabel.Text = ""
        Else
            Dim field As DataSourceField = fields.First(Function(x) x.Id.ToString = ddlField.SelectedValue)
            txtLabel.Text = field.DisplayLabel
        End If
    End Sub

    Private Sub ddlLookup_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlLookup.SelectedIndexChanged
        If ddlLookup.SelectedValue = "$QUERY$" Then
            txtLookupQuery.Style.Remove("Display")
            'txtLabel.Attributes.Add("style", "margin-left:-295px")
        Else
            txtLabel.Style.Remove("margin-left")
            txtLookupQuery.Attributes.Add("style", "Display:none")
        End If
    End Sub
    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
    	ddlField.FillFromSql("SELECT df.Id AS ID,df.Name As Name FROM DataSourceField df INNER JOIN DataSourceTable dt ON df.TableId = dt.Id WHERE dt.CC_TargetTable = 'ParcelData' AND df.DataType = 5 AND df.CategoryId IS NOT NULL AND df.Id NOT IN (SELECT FieldId FROM MRA_Filters WHERE IsExportField = 0)", True)
    	btnAdd.Visible = False
    	ddlField.SelectedValue = ""
        ddlLookup.SelectedValue = ""
        txtLabel.Text = ""
        txtLookupQuery.Text = ""
'        For Each item As ListItem In cbl_ExpFields.Items
'            item.Selected = False
'        Next
    	divAddcontnt1.Visible =True 
    	divAddcontnt2 .Visible =True
     End Sub
    
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If ddlField.SelectedValue = "" Then
            Alert("Please select a field")
            Exit Sub
        End If
        Dim fieldId = ""
        Dim label As String = txtLabel.Text
        Dim LookupQuery As String = txtLookupQuery.Text
        If ddlField.SelectedValue <> "" Then
            Dim field As DataSourceField = fields.First(Function(x) x.Id.ToString = ddlField.SelectedValue)
            fieldId = field.Id
        End If
        Dim LookupName As String = ddlLookup.SelectedValue
        Dim UseXMLLookup = IIf(ddlLookup.SelectedValue = "$QUERY$", 1, 0)
'        Dim ExpFields As New ArrayList
'        For Each item As ListItem In cbl_ExpFields.Items
'            If item.Selected Then
'                ExpFields.Add("'" + item.Value + "'")
'            End If
'        Next
'        Dim extraFieldsInExport = ""
'        If ExpFields.Count > 0 Then
'            extraFieldsInExport = Strings.Join(ExpFields.ToArray, ",")
'        End If
        If (hcid.Value = "") Then
            Database.Tenant.Execute("INSERT INTO MRA_Filters (FieldId ,Label ,LookupQuery,LookupName,UseXMLLookup,IsExportField) values ({0},{1},{2},{3},{4},0) ".SqlFormatString(fieldId, label, LookupQuery, LookupName, UseXMLLookup))
        Else
            Database.Tenant.Execute("UPDATE MRA_Filters SET FieldId = {1} ,Label = {2} ,LookupQuery = {3},LookupName = {4},UseXMLLookup = {5} , IsExportField = 0 WHERE RowId = {0}".SqlFormatString(hcid.Value, fieldId, label, LookupQuery, LookupName, UseXMLLookup))
            hcid.Value = ""
        End If
        ddlField.SelectedValue = ""
        ddlLookup.SelectedValue = ""
        txtLabel.Text = ""
        txtLookupQuery.Text = ""
'        For Each item As ListItem In cbl_ExpFields.Items
'            item.Selected = False
'        Next
        divAddcontnt1.Visible =False
        divAddcontnt2.Visible =False
        LoadGrid()
        Alert("Settings have been saved successfully.")
        btnAdd.Visible = True
    End Sub
    
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
    	divAddcontnt1.Visible =False
    	divAddcontnt2.Visible =False
    	btnAdd.Visible = True
    End Sub
    Private Sub linkExpFields_Click(sender As Object, e As EventArgs) Handles linkExpFields.Click
    	Response.Redirect("/protected/mra/exportFileds.aspx")
    End Sub
End Class