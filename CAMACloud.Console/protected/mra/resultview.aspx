﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DesktopWeb.master" CodeBehind="resultview.aspx.vb" Inherits="CAMACloud.Console.resultview" %>
<%@ Register TagPrefix="cc" TagName="MRAResultView" Src="~/App_Controls/mra/MRAResultView.ascx" %>
<%@ Register TagPrefix="cc" TagName="MRA_NavigationControl" Src="~/App_Controls/mra/MRA_NavigationControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script>
$(function(){
$('.analysisResult').hide();
changeSelectedField();
showHide();
});
function changeSelectedField(){
    var tempId=$("#<%=hTempId.ClientID %>").val();
    $('.ddlModel option[value="'+tempId+'"]').prop('selected', true);
    }
    function showHide(){
   		var tempId	= $('.ddlModel').find(":selected").val();
        if(tempId != "") $('.main_result').show();
    	else{ $('.main_result').hide();  $('.Rght_linkdiv').hide(); $("#spanMainHeading").html('- Regression Analysis Result')}
    }
</script>
<style>
 #divContentArea {
    top:100px;
    }
    .divNavBtn {
        padding: 4px 0px 4px;
    }
</style>
</asp:Content>
 <asp:Content ID="Content4" ContentPlaceHolderID="NavBarContent" runat="server">
 <div class="Overlay_Div">
    <cc:MRA_NavigationControl ID="mra_nav" runat="server" />
    </div>
 </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<asp:HiddenField runat="server" id="hTempId" value="-1"/> 
<div class="Cont_Content">
    <div class="main_result" style="float: left;margin-top: 20px;padding: 10px;">
    <cc:MRAResultView runat="server" id="mra_result"/>
    </div>
     <%-- <div class="hideonprint link-panel" style="float:left;margin-left:3%;width: 138px;margin-top: 30px;">
        <a style="width: 90px" href='<%= "design.aspx?_tid=" + Request("_tid") %>'>Edit Model</a>
        <a style="width: 90px;margin-top:10px;" href='<%= "calculator.aspx?_tid=" + Request("_tid") %>'>Cost Calculator</a>
        <a style="width: 90px;margin-top:10px;" href='<%= "dataview.aspx?_tid=" + Request("_tid") %>'>Data View</a>
    </div> --%>
    </div>
</asp:Content>
