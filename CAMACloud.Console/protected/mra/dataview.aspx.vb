﻿Imports CAMACloud.BusinessLogic.DataAnalyzer
Imports System.IO
Imports CAMACloud.Data
Public Class dataview
    Inherits System.Web.UI.Page
    
    Dim Private WithEvents mraDataView  As MRA_NavigationControl
    
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
    	mraDataView = New MRA_NavigationControl ()
    	RemoveHandler mraDataView.ddlChanged , AddressOf Me.NavControl_ddlmodel_dataview 
		AddHandler mraDataView.ddlchanged, AddressOf Me.NavControl_ddlmodel_dataview 
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim templateId As Integer = -1
        If Not IsPostBack Then
            pnlLinks.Visible = False
            pnlOutput.Visible = False
			If Integer.TryParse(Request("_tid"), templateId) Then
              	'ddlModel.FillFromSql("SELECT ID, Name FROM MRA_Templates ORDER BY Name", True)
              	'ddlModel.items.FindByValue(Convert.ToString(templateId)).Selected=True 
              	hdntemplateId.Value=templateId
              	ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "changeSelectedField();", True)
              	OpenTemplate(convert.ToInt32(templateId))
              	hdnLoadOnce.Value = hdnLoadOnce.Value + 1
            Else
                'ddlModel.FillFromSql("SELECT ID, Name FROM MRA_Templates WHERE LastExecutionTime IS NOT NULL ORDER BY Name", True)
                pnlSelect.Visible = True
            End If
        End If

    End Sub

'    Private Sub ddlModel_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlModel.SelectedIndexChanged
'        If ddlModel.SelectedIndex = 0 Then
'            'pnlLinks.Visible = False
'            pnlOutput.Visible = False
'            gvResiduals.DataSource = Nothing
'            gvResiduals.DataBind()
'            gvResiduals.Columns.Clear()
'            tid.Value = "-1"
'        Else
'            OpenTemplate(convert.ToInt32(ddlModel.SelectedValue))
'        End If
'    End Sub
    
    Private Sub NavControl_ddlmodel_dataview(sender As Object, e As EventArgs) Handles mraDataView.ddlchanged
    	If (HttpContext.Current.Request.Url.AbsolutePath.Contains("dataview")) Then
    		If MRA_NavigationControl.__mraDDLItem  = 0 Then
            	pnlLinks.Visible = False
            	pnlOutput.Visible = False
           	 	gvResiduals.DataSource = Nothing
            	gvResiduals.DataBind()
            	gvResiduals.Columns.Clear()
            	tid.Value = "-1"
        Else
        	hdntemplateId.Value = MRA_NavigationControl.__mraDDLItem
        	OpenTemplate(convert.ToInt32(MRA_NavigationControl.__mraDDLItem))
        	hdnLoadOnce.Value = hdnLoadOnce.Value + 1
        	'HttpContext.Current.Response.Redirect("dataview.aspx?_tid=" & MRA_NavigationControl.__mraDDLItem)
        End If
    	End If
    	RemoveHandler mraDataView.ddlChanged , AddressOf Me.NavControl_ddlmodel_dataview 
    End Sub
    
     Sub OpenTemplate(templateId As Integer)
        Dim td = RegressionAnalyzer.GetMRATemplate(templateId)
        If td IsNot Nothing AndAlso td.Get("LastExecutionTime") IsNot Nothing Then     		
            tid.Value = templateId

            Dim usability = RegressionAnalyzer.GetUsability(td.Get("RSquare"))
            lblUsability.Text = usability.Name
            lblUsability.ForeColor = usability.Color



           ' hlCalc.NavigateUrl = "calculator.aspx?_tid=" & templateId
            'hlVisualization.NavigateUrl = "graphview.aspx?_tid=" & templateId
            'hlResult.NavigateUrl = "resultview.aspx?_tid=" & templateId

            pnlLinks.Visible = True
            pnlOutput.Visible = True

            gvResiduals.DataSource = Nothing
            gvResiduals.DataBind()
            gvResiduals.Columns.Clear()

            Dim outputField As String = td.GetString("OutputFieldName")
            Dim outputFieldLabel As String = td.GetString("OutputFieldLabel")
            Dim outputFieldType As Integer = td.GetInteger("OutputFieldType")

            Dim outputFormat As String = "0.###"
            Select Case outputFieldType
                Case 9
                    outputFormat = "###,###,###,##0"
                Case 8
                    outputFormat = "###,###,###,##0"
                Case 7
                    outputFormat = "$###,###,###,##0.00"
                Case 2
                    outputFormat = "#.######"
            End Select
            Dim outputFormatter As String = "{0:" + outputFormat + "}"

            addField(gvResiduals, "ObservationNo", "", 60, HorizontalAlign.Right, "{0}. ")
            addField(gvResiduals, "AssignmentGroup", "Asmt. Group", 0,HorizontalAlign.Right)
            addField(gvResiduals, "KeyValue", "Parcel", 0,HorizontalAlign.Right)
            addField(gvResiduals, "PredictedValue", "Predicted " + outputFieldLabel, 140, HorizontalAlign.Right, outputFormatter)
            addField(gvResiduals, "Residual", "Residual", 140, HorizontalAlign.Right, outputFormatter, "resd")
'            Dim extraFieldsInExport = Database.Tenant.GetStringValue("SELECT TOP 1 ExtraFieldsInExport FROM MRA_Filters WHERE ExtraFieldsInExport IS NOT NULL AND ExtraFieldsInExport <> ''")
'	        If extraFieldsInExport.Length > 0 Then
'		        For Each ef In extraFieldsInExport.Split(",").Select(Function(x) x.Trim)
'		            ef = Replace(ef, "'", "")
'		            Dim fId = Convert.ToInt32(ef)
'		            Dim fName = Database.Tenant.GetStringValue("SELECT AssignedName FROM DataSourceField WHERE Id = "&fId)
'		            addField(gvResiduals, fName, fName, 100, HorizontalAlign.Right)
'		            addField(residuals, fName, fName, 100, HorizontalAlign.Right)
'		        Next
'	        End If
			Dim extraFieldsInExport As DataTable = Database.Tenant.GetDataTable("SELECT FieldId FROM MRA_Filters WHERE IsExportField = 1")
			If extraFieldsInExport IsNot Nothing AndAlso extraFieldsInExport.Rows.Count > 0 Then
					For Each dr As DataRow In extraFieldsInExport.Rows
							Dim fId = dr.Item("FieldId")
				            Dim fName = Database.Tenant.GetStringValue("SELECT AssignedName FROM DataSourceField WHERE Id = "&fId)
				            addField(gvResiduals, fName, fName, 100, HorizontalAlign.Right)
				            If  hdnLoadOnce.Value = 0  Then
				            	addField(residuals, fName, fName, 100, HorizontalAlign.Right)
							End If
					Next
			End If 
	        RefreshDataOnGrid()
	        Dim stderr As String = td.Get("StandardError")
            If  stderr Is Nothing Then
            	HttpContext.Current.Response.Write("<script language='javascript'>window.alert('This template is not usable due to meaningless filters used.');window.location='/protected/mra/dataview.aspx';</script>")
            Else
            	lblStandardError.Text = "+/- " + CType(td("StandardError"), Double).ToString(outputFormat)
            End If
        End If
    End Sub

    Sub RefreshDataOnGrid()
        Dim templateId As Integer = tid.Value
        gvResiduals.DataSource = Database.Tenant.GetDataTable(GetSql(templateId))
        gvResiduals.DataBind()
    End Sub

    Private Sub addField(grid As GridView, dataField As String, headerText As String, Optional width As Integer = 0, Optional align As HorizontalAlign = HorizontalAlign.Left, Optional format As String = "", Optional cssClass As String = "")
        Dim c As New BoundField
        c.DataField = dataField
        If format <> "" Then
            c.DataFormatString = format
        End If
        c.HeaderText = headerText
        c.ItemStyle.HorizontalAlign = align
        c.HeaderStyle.HorizontalAlign = align
        If width > 0 Then
            c.ItemStyle.Width = Unit.Pixel(width)
        Else
            c.ItemStyle.CssClass = "aw"
        End If
        If cssClass <> "" Then
            If c.ItemStyle.CssClass Is Nothing Then
                c.ItemStyle.CssClass = cssClass
            Else
                c.ItemStyle.CssClass += " " + cssClass
            End If
        End If


        'gvResiduals.Columns.Add(c)
        grid.Columns.Add(c)
    End Sub

Function GetSql(templateId As Integer) As String
'	            Dim extraFieldsInExport = Database.Tenant.GetStringValue("SELECT TOP 1 ExtraFieldsInExport FROM MRA_Filters WHERE ExtraFieldsInExport IS NOT NULL AND ExtraFieldsInExport <> ''")
	    Dim extraFieldNames As String = ""
		Dim i = 0
		Dim sql As String = "SELECT p.KeyValue1 As KeyValue, n.Number As AssignmentGroup, r.* FROM MRA_ResidualOutput r INNER JOIN Parcel p ON r.ParcelId = p.Id LEFT OUTER JOIN Neighborhood n ON p.NeighborhoodId = n.Id WHERE r.TemplateID = {0} ORDER BY ObservationNo OFFSET 0 ROWS FETCH NEXT 50 ROWS ONLY".FormatString(templateId)
		Dim extraFieldsInExport As DataTable = Database.Tenant.GetDataTable("SELECT FieldId FROM MRA_Filters WHERE IsExportField = 1")
		If extraFieldsInExport IsNot Nothing AndAlso extraFieldsInExport.Rows.Count > 0 Then
			For Each dr As DataRow In extraFieldsInExport.Rows
		         Dim fId = dr.Item("FieldId")
		         Dim fName = Database.Tenant.GetStringValue("SELECT AssignedName FROM DataSourceField WHERE Id = "&fId)
		         extraFieldNames += IIf(i=0, "pd."+fName,", pd." + fName)
		         i += 1
			Next
			Dim part As String() = Regex.Split(sql," from ",RegexOptions.IgnoreCase)
		    Dim SelectSql As String = part(0) + " ," + extraFieldNames + " FROM " + part(1)
		    Dim part1 As String() = Regex.Split(SelectSql," where ",RegexOptions.IgnoreCase)
		    SelectSql = part1(0) + " INNER JOIN ParcelData pd ON Pd.CC_ParcelId = p.Id" + " WHERE " + part1(1)
		    Return SelectSql
		Else
			Return sql
	    End If
			
'	            If extraFieldsInExport.Length > 0 Then
'	                    For Each ef In extraFieldsInExport.Split(",")
'	                            ef = Replace(ef, "'", "")
'	                            Dim fId = Convert.ToInt32(ef)
'	                            Dim fName = Database.Tenant.GetStringValue("SELECT AssignedName FROM DataSourceField WHERE Id = "&fId)
'	                            extraFieldNames += IIf(i=0, "pd."+fName,", pd." + fName)
'	                            i += 1
'	                    Next
'	                            Dim part As String() = Regex.Split(sql," from ",RegexOptions.IgnoreCase)
'	                            Dim SelectSql As String = part(0) + " ," + extraFieldNames + " FROM " + part(1)
'	                            Dim part1 As String() = Regex.Split(SelectSql," where ",RegexOptions.IgnoreCase)
'	                            SelectSql = part1(0) + " INNER JOIN ParcelData pd ON Pd.CC_ParcelId = p.Id" + " WHERE " + part1(1)
'	                            Return SelectSql
'	            Else  
'	            		Return sql	
    End Function
	
	     Public Overrides Sub VerifyRenderingInServerForm(control As System.Web.UI.Control)
	        If control Is pnlResidual Or control Is residuals Then
	            Return
	        End If
	        MyBase.VerifyRenderingInServerForm(control)
	     End Sub
	    
	     Protected Sub lbExport_Click(sender As Object, e As System.EventArgs) Handles lbExport.Click
	        Dim templateId As Integer = tid.Value
	        residuals.DataSource = Database.Tenant.GetDataTable(GetSql(templateId))
	        residuals.DataBind()
	       ' residuals.DataSource = Nothing        
	        Dim sb As New StringBuilder
	        Dim tw As New StringWriter(sb)
	        Dim hw As New HtmlTextWriter(tw)
	        
	        Dim lr As New Statistics.LinearRegression
	        
	        pnlResidual.Visible = True
	        residuals.RenderControl(hw)
	        Dim html As String = sb.ToString
	        pnlResidual.Visible = False
	
	        Response.Clear()
	        Response.ContentType = "application/vnd.ms-excel"
	        Response.AddHeader("Content-Disposition", "attachment;filename=ResidualsOutput.xls")
	        Response.Write(html)
	        Response.End()
	    End Sub
End Class