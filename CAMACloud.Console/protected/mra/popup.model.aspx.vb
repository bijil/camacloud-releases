﻿Imports CAMACloud.BusinessLogic.DataAnalyzer
Partial Class mra_popup_model
	Inherits System.Web.UI.Page

    Dim fields As List(Of DataSourceField)
    Dim outputField As String
	Dim intercept As Double
	Dim inputFields As New List(Of String)
    Dim inputFieldIds As New List(Of Integer)
    Dim inputSpecs As New List(Of String)
	Dim coefficients As New List(Of Double)
	Dim inputControls As New List(Of WebControl)
	Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
		If Request("params") IsNot Nothing Then
			Dim model As String = ""

            fields = DataSourceField.GetFields

            Dim par = Request("params").Replace("N*","#")
			Dim params() As String = par.Split(";")
			'Dim param5 = params(5)
			outputField = fields.Where(Function(x) x.Id.ToString = params(0)).First.DisplayLabel
            model += outputField + " = "

            btnCalculate.Text = "Calculate " + outputField

            Dim costart As Integer = 2
            If Not params(1).Contains("|") Then
                intercept = Double.Parse(params(1))
                model += intercept.ToString("0.###")
            Else
                model += "0"
                costart = 1
            End If

            For i As Integer = costart To params.Length - 1
                Dim subparam() As String = params(i).Split("|")
                Dim fspec = subparam(0).Split("~")
                Dim fid As Integer = CInt(fspec(0))
                Dim fname As String = fields.Where(Function(x) x.Id = fid).First.AssignedName
                Dim coeff As Double = Double.Parse(subparam(1))
                inputFieldIds.Add(fid)
                inputFields.Add(fname)
                coefficients.Add(coeff)
                If fspec.Length > 1 Then
                    model += " + " + coeff.ToString("0.###") + " * (" + fspec(2) + ")"
                    inputSpecs.Add(fspec(2))
                Else
                    model += " + " + coeff.ToString("0.###") + " * " + fname
                    inputSpecs.Add(fname)
                End If
            Next

			

            For i As Integer = 0 To inputFields.Count - 1
                Dim index As Integer = i
                Dim tr As New TableRow
                Dim td1 As New TableCell
                Dim td2 As New TableCell
                td1.Width = New Unit(120)
                td2.Width = New Unit(240)
                td1.Controls.Add(New LiteralControl(inputFields(index) + " :"))
                Dim field As DataSourceField = fields.Where(Function(x) x.Id = inputFieldIds(index)).First
                Dim rf As New RequiredFieldValidator()
                If field.InputType = 5 Then
                    Dim ddl As New DropDownList
                    ddl.ID = "ddl" + field.AssignedName
                    ddl.Width = New Unit(176)
                    td2.Controls.Add(ddl)
                    ddl.FillFromSql("SELECT Value, DescValue FROM ParcelDataLookup WHERE LookupName = '" + field.LookupTable + "' ORDER BY Ordinal", True)
                    rf.ControlToValidate = "ddl" + field.AssignedName

                    inputControls.Add(ddl)
                Else
                    Dim txt As New TextBox
                    txt.ID = "txt" + field.AssignedName
                    txt.Width = New Unit(170)
                    td2.Controls.Add(txt)

                    rf.ControlToValidate = "txt" + field.AssignedName
                    inputControls.Add(txt)
                End If

                rf.Text = " Required"
                rf.Font.Size = New FontUnit(FontSize.Smaller)
                rf.ValidationGroup = "InputForm"
                td2.Controls.Add(rf)

                tr.Cells.Add(td1)
                tr.Cells.Add(td2)
                tbl.Rows.Add(tr)
            Next

			lblRegressionEquation.Text = model
		End If
	End Sub

	Protected Sub btnCalculate_Click(sender As Object, e As System.EventArgs) Handles btnCalculate.Click
		Dim output As Double = 0
		output += intercept
		For i As Integer = 0 To coefficients.Count - 1
            Dim ctrl As WebControl = inputControls(i)
            Dim spec As String = inputSpecs(i)
            Dim fName As String = inputFields(i)

			If TypeOf ctrl Is TextBox Then
				Dim txt As TextBox = ctrl
                If IsNumeric(txt.Text) Then
                    output += coefficients(i) * Database.Tenant.GetTopRow("SELECT " + spec.Replace(fName, Double.Parse(txt.Text.Trim)))(0)
                Else
                    lblOutput.Text = "Invalid Input!"
                    Return
                End If
			ElseIf TypeOf ctrl Is DropDownList Then
				Dim ddl As DropDownList = ctrl
                If IsNumeric(ddl.SelectedValue) Then
                    output += coefficients(i) * Database.Tenant.GetTopRow("SELECT " + spec.Replace(fName, Double.Parse(ddl.SelectedValue.Trim)))(0)
                Else
                    lblOutput.Text = "Invalid Input!"
                    Return
                End If
			End If
		Next

        lblOutput.Text = (Math.Round(output / 100) * 100).ToString("$#,###,###,##0")
	End Sub
End Class
