﻿Imports System.Web.UI.DataVisualization.Charting
Imports System.IO
Imports CAMACloud.Data
Imports CAMACloud.BusinessLogic.DataAnalyzer

Partial Class mra_analyze
    Inherits System.Web.UI.Page

    Dim fields As List(Of DataSourceField)

    Private Function getDataForRegression(Optional ByVal isTest As Boolean=False ) As DataTable
        If ddlInput.SelectedIndex > 0 Then
            btnAddInput_Click(btnAddInput, Nothing)
        End If
        If ddlFilterField.SelectedIndex > 0 And ddlFilterOp.SelectedIndex > 0 And txtFilterValue.Text <> "" Then
            btnAddFilter_Click(btnAddFilter, Nothing)
        End If
        Dim sql As String = GenerateSql()
        If sql Is Nothing Then
        	Exit Function
        End If
        lblSQL.Text = sql
        Try
            Dim dt As DataTable = Database.Tenant.GetDataTable(sql)
            For Each col As DataColumn In dt.Columns
                Dim colName = col.ColumnName
                If colName.Contains("]") Then
                    colName = colName.Replace("]", "\]")
                End If
                If (dt.Select("[" + colName + "] = '0' OR [" + colName + "] is null").Length = dt.Rows.Count) Then
                    Alert(col.ColumnName & " field does not contain enough data for regression analysis.")
                    Exit Function
                End If
            Next
            If (isTest = True) Then
                Alert(dt.Rows.Count & " rows available for analysis.")
            End If
            Return dt
        Catch ex As Exception
            pnlOutput.Visible = False
        End Try
    End Function
    Private Sub btnTest_Click(sender As Object, e As System.EventArgs) Handles btnTest.Click
        Dim dt As DataTable = getDataForRegression(True)
    End Sub

    Protected Sub btnGenerateMRA_Click(sender As Object, e As System.EventArgs) Handles btnGenerateMRA.Click


        Dim dt As DataTable = getDataForRegression()
        If dt Is Nothing Then
            Exit Sub
        End If

        If dt.Rows.Count > 50000 Then
                Alert("The input data parameters result in a sample size that contains more than 50,000 observations. Regression Analysis cannot be performed. Add or adjust filters to reduce the sample size appropriately.")
                pnlOutput.Visible = False
                Exit Sub
            End If
            Try
            If Not EvaluateRegression(dt, chkConstantIsZero.Checked) Then
                Alert("Regression Analysis cannot be performed. Input/output data insufficient to perform the calculation." + vbNewLine + vbNewLine + "Example: a sample may be available, but there is invalid data (null values not filtered out, missing linearized numeric values in code files, or no correlation found).")
                pnlOutput.Visible = False
            Else
                pnlOutput.Visible = True
            End If
        Catch exi As InvalidOperationException
            Alert("Operation failed due to invalid parameters selected.")
            pnlOutput.Visible = False
        Catch ex As Exception
            Alert(ex.Message)
            pnlOutput.Visible = False
        End Try
    End Sub

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        fields = DataSourceField.GetFields
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            RefreshTemplates()
            pnlOutput.Visible = False
            'ddlNbhd.FillFromSql("SELECT Id, Number FROM Neighborhood ORDER BY Number", True)
            ddlNbhd.InsertItem("All NHBD", "0", 0)
            ddlRegion.InsertItem("All Regions", "0", 0)
            cbl_Nbhd.DataSource = Database.Tenant.GetDataTable("SELECT Id, Number FROM Neighborhood ORDER BY Number")
            cbl_Nbhd.DataTextField = "Number"
            cbl_Nbhd.DataValueField = "Id"
            cbl_Nbhd.DataBind()
            ddlOutput.FillFromSql(GetFieldSql(, "7,9"), True)
            ddlInput.FillFromSql(GetFieldSql(), True)
            ddlFilterField.FillFromSql(GetFieldSql(, "1,2,4,5,7,8,10"), True) 
            'Dim showRegion As String = Database.Tenant.GetStringValue("SELECT Value FROM ClientSettings Where Name = {0}".SqlFormatString("ShowRegionInMRA"))
            'If showRegion = "True" Then
	            cbl_Region.DataSource = Database.Tenant.GetDataTable("SELECT DISTINCT region FROM ParcelData WHERE region IS NOT NULL AND region <> ''")
	            cbl_Region.DataTextField = "region"
	            cbl_Region.DataValueField = "region"
	            cbl_Region.DataBind()
            	'RunScript("showRegion()")
            'End If
            
        End If
        chkConstantIsZero.Checked = False
        Dim showRegion As String = Database.Tenant.GetStringValue("SELECT Value FROM ClientSettings Where Name = {0}".SqlFormatString("ShowRegionInMRA"))
        If showRegion = "True" Then
        RunScript("showRegion()")	
        End If
        'pnlFormView.Visible = False
    End Sub

    Function GetFieldSql(Optional filter As String = "", Optional types As String = "2, 5, 7, 8, 9, 10") As String
        If filter <> "" Then
            filter = " AND " + filter
        End If
        If types <> "" Then
            filter += " AND DataType IN (" + types + ")"
        End If
        Return "SELECT Id, DisplayLabel + ' (' + SourceTable + ')' As DisplayLabel FROM DataSourceField WHERE CategoryId IS NOT NULL" + filter + " ORDER BY DisplayLabel"
    End Function

    Sub RefreshInputs()
        'Handled
        Dim selectedFields As String = txtInputs.Text
        If selectedFields = "" Then
            selectedFields = 0
        Else
        	'selectedFields = String.Join(",", txtInputs.Text.Split(",").Select(Function(x) IIf(IsNumeric(x), x, x.Split("~")(0))).ToArray())
        	Dim dfields As String = ""
        	For Each str As String In txtInputs.Text.Split(",")
        		If IsNumeric(str) Then
        			dfields = IIf(dfields.Length = 0,dfields + str,dfields + "," + str) 
        		Else If str.Contains("~")
        			dfields = IIf(dfields.Length = 0, dfields + str.Split("~")(0),dfields + "," + str.Split("~")(0)) 
        		End If
        	Next
        	selectedFields = dfields
        End If
        ddlInput.FillFromSql(GetFieldSql("Id NOT IN (" + selectedFields + ")"), True)
        Dim inputs As New Collection
        For Each iss In txtInputs.Text.Split(",")
        	If(iss.Contains("~"))
        	 Dim fid As String = iss.Split("~")(0)
            If fid = "" Then
                Continue For
            End If
            Dim label, spec As String
            label = Nothing
            spec = Nothing
            If iss.Contains("~") Then
                label = iss.Split("~")(1)
                spec = iss.Split("~")(2)
            End If
            Try
                Dim field = fields.First(Function(x) x.Id.ToString = fid.Trim)
                'Dim f As Object = New With {.Id = fid, .DisplayLabel = IIf(spec Is Nothing, field.DisplayLabel, spec + " (" + label + ")")}
                Dim f As Object = New With {.Id = fid, .DisplayLabel = IIf(spec Is Nothing, field.DisplayLabel, field.AssignedName + " (" + label + ")")}
                inputs.Add(f)
            Catch ex As Exception
                Throw New Exception(fid)
            End Try	
        	End If
        Next
        rpInputs.DataSource = inputs 'Database.Tenant.GetDataTable(GetFieldSql("Id IN (" + selectedFields + ")"))
        rpInputs.DataBind()

        txtLabel.Text = ""
        txtSpec.Text = ""
    End Sub

    Protected Sub btnAddInput_Click(sender As Object, e As System.EventArgs) Handles btnAddInput.Click
        If txtInputs.Text <> "" Then
            txtInputs.Text += ","
        End If

        'Handled
        txtSpec.Text = Replace(txtSpec.Text, ",", "")
        Dim field As DataSourceField = fields.First(Function(x) x.Id.ToString = ddlInput.SelectedValue)
        If txtLabel.Text = txtSpec.Text And txtLabel.Text = field.DisplayLabel Then
            txtInputs.Text += ddlInput.SelectedValue
        Else
            txtInputs.Text += ddlInput.SelectedValue + "~" + txtLabel.Text + "~" + txtSpec.Text.Trim()
        End If

        RefreshInputs()
    End Sub

    Protected Sub rpInputs_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rpInputs.ItemCommand
    	If e.CommandName = "RemoveX" Then
            Dim ids = txtInputs.Text.Split(",").Select(Function(x) x.Trim).ToList()
            Dim toDelete = ids.First(Function(x) x = e.CommandArgument OrElse (Not IsNumeric(x) AndAlso x.Split("~")(0) = e.CommandArgument))
            ids.Remove(toDelete)
            txtInputs.Text = String.Join(",", ids.ToArray())
            RefreshInputs()
        End If
    End Sub

    Protected Sub btnAddFilter_Click(sender As Object, e As System.EventArgs) Handles btnAddFilter.Click
        If txtFilters.Text <> "" Then
            txtFilters.Text += ";"
        End If
        Dim filter As String = Guid.NewGuid.ToString() + "`" + ddlFilterField.SelectedValue + "`" + ddlFilterField.SelectedItem.Text + "`" + ddlFilterOp.SelectedValue + "`" + txtFilterValue.Text
        txtFilters.Text += filter
        RefreshFilters()

        ddlFilterField.SelectedIndex = 0
        ddlFilterOp.SelectedIndex = 0
        txtFilterValue.Text = ""
    End Sub

    Sub RefreshFilters()
        If txtFilters.Text <> "" Then
            Dim filters = txtFilters.Text.Split(";").Select(Function(x) x.Split("`")).Select(Function(x) New With {.ID = x(0), .Field = x(1), .FieldName = x(2), .Operator = x(3), .Value = x(4)})
            rpFilters.DataSource = filters
        Else
            rpFilters.DataSource = Nothing
        End If


        rpFilters.DataBind()
    End Sub

    Function GenerateSql() As String
        Dim joinFormat As String = " LEFT OUTER JOIN ParcelDataLookup l{0} ON (l{0}.LookupName = '{2}' AND l{0}.IdValue = pd.{1})"
        Dim auxLookupJoinFormat As String = " LEFT OUTER JOIN ParcelDataLookup l{0} ON (l{0}.LookupName = '{2}' AND l{0}.IdValue = au{3}.{1})"
        Dim auxJoinFormat As String = " LEFT OUTER JOIN XT_{1} au{0} ON (au{0}.CC_ParcelId = pd.CC_ParcelId)"
        Dim sql As String = "", joins As String = ""
        Dim sqlfilter As String = ""
        Dim joined As New List(Of String)
        Dim field As DataSourceField
        sql = "SELECT "
        Dim ofield = fields.Where(Function(x) x.Id.ToString() = ddlOutput.SelectedValue).First()
        If ofield.ImportType = 1 Then
            If Not joined.Contains(ofield.AuxJoinId) Then
                joins += auxJoinFormat.FormatString(ofield.TableId, ofield.TableName)
                joined.Add(ofield.AuxJoinId)
            End If
            If ofield.InputType = 5 Then
                If Not joined.Contains(ofield.JoinId) Then
                    joins += auxLookupJoinFormat.FormatString(ofield.Id, ofield.AssignedName, ofield.LookupTable, ofield.TableId)
                    joined.Add(ofield.JoinId)
                End If
                sql += "l{0}.Value AS {1}".FormatString(ofield.Id, ofield.AssignedName.Wrap(""""))
            Else
                sql += "au" & ofield.TableId & "." + ofield.AssignedName
            End If
        Else
            sql += "pd." + ofield.AssignedName
        End If
		Dim label, fieldNameOrFormula As String
		Try
        For Each ifl In txtInputs.Text.Split(",").Select(Function(x) x.Trim)
        	Dim fspec As String()
        	Dim part As String()
	            fspec = ifl.Split("~")
	            Dim ifield As String = fspec(0)
	            field = fields.Where(Function(x) x.Id.ToString() = ifield).First()
	            If fspec.Length > 2 Then
	                label = fspec(1)
	                fieldNameOrFormula = fspec(2)
	            Else
	                label = field.DisplayLabel
	                fieldNameOrFormula = field.AssignedName
	            End If

            Dim sourceField As String = ""

            If field.ImportType = 1 Then
                If Not joined.Contains(field.AuxJoinId) Then
                    joins += auxJoinFormat.FormatString(field.TableId, field.TableName)
                    joined.Add(field.AuxJoinId)
                End If
                If field.InputType = 5 Then
                    If Not joined.Contains(field.JoinId) Then
                        joins += auxLookupJoinFormat.FormatString(field.Id, field.AssignedName, field.LookupTable, field.TableId)
                        joined.Add(field.JoinId)
                    End If
                    sourceField += "l{0}.Value".FormatString(field.Id)
                Else
                    sourceField += "au" & field.TableId & "." + field.AssignedName
                End If
            Else
                If field.InputType = 5 Then
                    If Not joined.Contains(field.JoinId) Then
                        joins += joinFormat.FormatString(field.Id, field.AssignedName, field.LookupTable)
                        joined.Add(field.JoinId)
                    End If
                    sourceField += "l{0}.Value".FormatString(field.Id)
                Else
                    sourceField += "pd." + field.AssignedName
                End If
            End If
            
            If fieldNameOrFormula.ToUpper.Trim.StartsWith("IF ") Then
            	Dim formula As String() = Regex.Split(fieldNameOrFormula,"if ",RegexOptions.IgnoreCase)
            	Dim _sql As String = ""
            	Dim i = 0
            		For Each fl In formula
            			If fl <> "" Then
	            			Dim orSplit As String() = Regex.Split(fl.ToString," OR ",RegexOptions.IgnoreCase)
	            			Dim andSplit As String() = Regex.Split(fl.ToString," AND ",RegexOptions.IgnoreCase)
	            			If orSplit.Length > 1  Then
	            				_sql += IIf(i = 0," CASE WHEN "," WHEN ")
			            		Dim j = 0
								For Each st As String In orSplit
									_sql += IIf(j = 0 , st," OR " +field.AssignedName + " = " + st)
									j += 1
								Next
	            			End If
	            			If andSplit.Length > 1  Then
	            				_sql += IIf(i = 0," CASE WHEN "," WHEN ")
			            		Dim k = 0
								For Each str As String In andSplit
									_sql += IIf(k = 0 , str," AND " +field.AssignedName + " " + str)
									k += 1
								Next
	            			End If
	            			i+=1
            			End If
            			
            		Next
            		sql +=  ", " + _sql.Replace(field.AssignedName, sourceField) + " END AS " + label.Wrap("""")
            Else 	
            	sql += ", " + fieldNameOrFormula.Replace(field.AssignedName, sourceField) + " AS " + label.Wrap("""")
			End If
        Next
		Catch ex As Exception
			Alert("Please Add spaces between words in Specification for the field '"+ field.AssignedName +"'")
			Return Nothing
		End Try
       

        If txtFilters.Text <> "" Then
            Dim filters = txtFilters.Text.Split(";").Select(Function(x) x.Split("`")).Select(Function(x) New QueryFilter With {.ID = x(0), .Field = x(1), .FieldName = x(2), .Operator = x(3), .Value = x(4)})
            For Each f In filters
                Dim qf As QueryFilter = f
                'Dim field As DataSourceField = fields.Where(Function(x) x.Id.ToString() = qf.Field).First()
				field = fields.Where(Function(x) x.Id.ToString() = qf.Field).First()
                Dim filterValue As String = qf.Value
                If Not IsNumeric(filterValue) AndAlso filterValue.Trim().Trim("'") <> filterValue.Trim(filterValue) Then
                    filterValue = "'" + qf.Value + "'"
                End If
                If (qf.Operator = "in") Then
                    filterValue = filterValue.TrimStart("'").TrimEnd("'")
                    filterValue = filterValue.Replace("','", ",")
                    filterValue = "('" + filterValue.Replace(",", "','") + "')"
                End If
                Dim lookupFilterFormat = " AND (l{0}.Value {1} {2} OR (l{0}.Value = 0 AND l{0}.IdValue {1} {2}))"
                Dim lookupTextFilterFormat = " AND (l{0}.Value = 0 AND l{0}.IdValue {1} {2})"

                If Not IsNumeric(filterValue) Then
                    lookupFilterFormat = lookupTextFilterFormat
                End If

                If field.ImportType = 1 Then
                    If Not joined.Contains(field.AuxJoinId) Then
                        joins += auxJoinFormat.FormatString(field.TableId, field.TableName)
                        joined.Add(field.AuxJoinId)
                    End If
                    If field.InputType = 5 Then
                        If Not joined.Contains(field.JoinId) Then
                            joins += auxLookupJoinFormat.FormatString(field.Id, field.AssignedName, field.LookupTable, field.TableId)
                            joined.Add(field.JoinId)
                        End If
                        sqlfilter += lookupFilterFormat.FormatString(field.Id, qf.Operator, filterValue)
                    Else
                        sqlfilter += " AND au" & field.TableId & ".[" + field.AssignedName + "] " + qf.Operator + filterValue
                    End If
                Else
                    If field.InputType = 5 Then
                        If Not joined.Contains(field.JoinId) Then
                            joins += joinFormat.FormatString(field.Id, field.AssignedName, field.LookupTable)
                            joined.Add(field.JoinId)
                        End If
                        sqlfilter += lookupFilterFormat.FormatString(field.Id, qf.Operator, filterValue)
                        'sqlfilter += " AND l" & field.Id & ".Value" + " " + qf.Operator + filterValue
                    Else
                        sqlfilter += " AND pd.[" + field.AssignedName + "] " + qf.Operator + filterValue
                    End If
                End If
            Next
        End If

        If sql.Length > 7 Then
            Dim keyField1 As String = Database.Tenant.Application.KeyField(1)
            Dim checkLabel = fields.Where(Function(x) x.AssignedName = keyField1 And x.AssignedName <> x.DisplayLabel)
            If checkLabel.Count > 0 Then
                keyField1 = checkLabel.First.DisplayLabel
            End If
            sql += ", p.KeyValue1 As """ + keyField1 + """"
        End If

        sql += " FROM ParcelData pd INNER JOIN Parcel p ON pd.CC_ParcelId = p.Id " + joins
        Dim Nbhd As New ArrayList
        For Each item As ListItem In cbl_Nbhd.Items
            If item.Selected Then
                Nbhd.Add("'" + item.Value + "'")
            End If
        Next
        If Nbhd.Count = 0 Then
            sql += " WHERE 1 = 1" + sqlfilter
        Else
            sql += " WHERE pd.CC_ParcelId IN (SELECT Id FROM Parcel WHERE NeighborhoodId in (" + Strings.Join(Nbhd.ToArray, ",") + "))" 
        End If
        Dim Region As New ArrayList
        For Each rg As ListItem In cbl_Region.Items
            If rg.Selected Then
                Region.Add("'" + rg.Value + "'")
            End If
        Next
        If Region.Count <> 0 Then
            sql += " AND pd.CC_ParcelId IN (SELECT Id FROM Parcel WHERE Region in (" + Strings.Join(Region.ToArray, ",") + "))"
        End If
        Return sql + sqlfilter
    End Function

    Function EvaluateRegression(data As DataTable, Optional constantIsZero As Boolean = False) As Boolean
        Dim ch As New Chart
        Dim stats As StatisticFormula = ch.DataManipulator.Statistics

        Dim rowCount As Integer = data.Rows.Count

        Dim N As Integer = data.Columns.Count - 2
        Dim itemLabelIndex As Integer = data.Columns.Count - 1
        Dim y(rowCount - 1) As Double
        Dim x(N, rowCount - 1) As Double
        Dim w(rowCount - 1) As Double

        If constantIsZero Then
            ReDim x(N - 1, rowCount - 1)
        End If

        For i As Integer = 0 To rowCount - 1
            If constantIsZero Then
                For j As Integer = 1 To N
                    x(j - 1, i) = data(i).Get(j)
                Next
            Else
                x(0, i) = 1
                For j As Integer = 1 To N
                    x(j, i) = data(i).Get(j)
                Next
            End If


            y(i) = data(i).Get(0)
            w(i) = 1
        Next



        Dim lr As New Statistics.LinearRegression
        If Not lr.Regress(y, x, w) Then
            Return False
        End If

        'Calculate Overall Mean of Y
        Dim totalY As Double = 0, overallMean As Double
        For i As Integer = 0 To rowCount - 1
            totalY += y(i)
        Next
        overallMean = totalY / rowCount
        Dim ssreg As Double = 0
        Dim ssres As Double = 0
        For i As Integer = 0 To rowCount - 1
            If constantIsZero Then
                ssreg += Math.Pow(lr.CalculatedValues(i), 2)
            Else
                ssreg += Math.Pow(overallMean - lr.CalculatedValues(i), 2)
            End If
            ssres += Math.Pow(lr.Residuals(i), 2)
        Next
        Dim dfreg, dfres As Integer
        Dim msreg, msres As Double
        Dim rsquare As Double
        Dim multipleR, standerror As Double
        Dim regf As Double

        dfreg = N
        dfres = rowCount - lr.Coefficients.Count
        msreg = ssreg / dfreg
        msres = ssres / dfres
        rsquare = ssreg / (ssreg + ssres)
        multipleR = Math.Sqrt(rsquare)
        standerror = Math.Sqrt(msres)
        regf = msreg / msres

        lblRSquare.Text = rsquare.ToString("0.########")
        lblMultipleR.Text = multipleR.ToString("0.########")
        lblStandardError.Text = standerror.ToString("0.########")
        lblObservations.Text = rowCount
        lblAdjustedRSquare.Text = (1 - ((dfreg + dfres) / dfres) * (ssres / (ssres + ssreg))).ToString("0.########")

        lblSSReg.Text = ssreg.ToString("0.###")
        lblMSReg.Text = msreg.ToString("0.###")
        lblSSRes.Text = ssres.ToString("0.###")
        lblMSRes.Text = msres.ToString("0.###")
        lblDFReg.Text = dfreg
        lblDFRes.Text = dfres
        lblFisher.Text = regf.ToString("0.###")
        lblSignificanceF.Text = stats.FDistribution(msreg / msres, dfreg, dfres).ToString("E4")


        lblDFTotal.Text = rowCount - lr.Coefficients.Count + N
        lblSSTotal.Text = (ssreg + ssres).ToString("0.###")


        Dim reg As New DataTable
        reg.Columns.Add("Head")
        reg.Columns.Add("CE")
        reg.Columns.Add("SE")
        reg.Columns.Add("TS")
        reg.Columns.Add("PV")

        For i As Integer = 0 To lr.C.Length - 1
            Dim rr As DataRow = reg.NewRow
            Dim head As String
            If constantIsZero Then
                head = data.Columns(i + 1).ColumnName
            Else
                If i = 0 Then
                    head = "Intercept"
                Else
                    head = data.Columns(i).ColumnName
                End If
            End If


            Dim c As Double = lr.Coefficients(i)
            Dim se As Double = lr.CoefficientsStandardError(i)
            Dim tstat = c / se
            Dim pval = stats.TDistribution(Math.Abs(tstat), dfres, False)
            rr("Head") = head
            rr("CE") = c.ToString("0.########")
            rr("SE") = se.ToString("0.########")
            rr("TS") = tstat.ToString("0.########")
            rr("PV") = pval.ToString("0.########")
            reg.Rows.Add(rr)
        Next
		
        Dim resd As New DataTable
        resd.Columns.Add("ID")
        resd.Columns.Add("Item")
        resd.Columns.Add("PR")
        resd.Columns.Add("RES")
        resd.Columns.Add("ASSGRP")
        Dim extraFields As String = Database.Tenant.GetStringValue("SELECT Value FROM ClientSettings where Name = 'MRAextraFieldsInExport'")



        For i As Integer = 0 To lr.CalculatedValues.Length - 1
            Dim rr As DataRow = resd.NewRow
            rr("ID") = i + 1
            rr("RES") = lr.Residuals(i).ToString()
            rr("Item") = data.Rows(i)(itemLabelIndex)
            Dim parcelId = Database.Tenant.GetIntegerValue("SELECT Id FROM Parcel Where KeyValue1 = {0}".SqlFormatString(rr("Item")))
            Dim nbhdId = Database.Tenant.GetIntegerValue("SELECT NeighborhoodId FROM Parcel Where KeyValue1 = {0}".SqlFormatString(rr("Item")))
            rr("ASSGRP") = Database.Tenant.GetStringValue("SELECT Name FROM Neighborhood WHERE Id = {0}".SqlFormatString(nbhdId))
            ' rr("PR") = lr.CalculatedValues(i).ToString("0.########")
            rr("PR") = (Math.Round(lr.CalculatedValues(i) / 100) * 100).ToString("#,###,###,##0")
            Dim showRegion As String = Database.Tenant.GetStringValue("SELECT Value FROM ClientSettings Where Name = {0}".SqlFormatString("ShowRegionInMRA"))
            If showRegion = "True" Then
            Dim region = Database.Tenant.GetStringValue("SELECT Region FROM ParcelData WHERE CC_ParcelId = {0}".SqlFormatString(parcelId))
            	If Not resd.Columns.Contains("RGN") Then
					resd.Columns.Add("RGN")
					Dim bf As New BoundField()
			        bf.HeaderText = "Region"
			        bf.DataField = Region
			        If hiddenRegion.Value <> ""
			        	residuals.Columns.Add(bf)
			        End If	
            	End If
            	rr("RGN") = region
            End If
            If extraFields.Length > 0 Then
	            For Each ef As String In extraFields.Split(",")
			        	Dim sourceTable = Database.Tenant.GetStringValue("SELECT  dt.CC_TargetTable from DataSourcetable dt inner join DataSourceField df on dt.Id = df.TableId where df.AssignedName = {0} and dt.CC_TargetTable <> 'NeighborhoodData'".SqlFormatString(ef.Trim()))
			        	If Not resd.Columns.Contains(ef) Then
							resd.Columns.Add(ef)
							Dim bfield As New BoundField()
					        bfield.HeaderText = ef
					        bfield.DataField = ef
					        If hiddenRegion.Value <> ""
					        	residuals.Columns.Add(bfield)
					        End If 
					    End If    
						rr(ef) = Database.Tenant.GetStringValue ("SELECT "+ ef +" FROM  "+ sourceTable +" WHERE CC_ParcelId = " +parcelId.ToString())
	            Next
           End If
            resd.Rows.Add(rr)
        Next	
        residuals.Columns(1).HeaderText = data.Columns(itemLabelIndex).ColumnName
        residuals.Columns(2).HeaderText = "Predicted " + data.Columns(0).ColumnName
        regresult.DataSource = reg
        regresult.DataBind()
        residuals.DataSource = resd
        residuals.DataBind()
        hiddenRegion.Value = ""

        Dim modelParam As String = ddlOutput.SelectedValue
        Dim pi As Integer = -1
        If Not constantIsZero Then
            pi += 1
            modelParam += ";" & lr.Coefficients(pi)
        End If
        If txtInputs.Text <> "" Then
        	Dim fieldspec As String = txtInputs.Text
            For Each fi In fieldspec.Split(",")
            	pi += 1
            	Dim ff  = Regex.Split(fi,"~IF ",RegexOptions.IgnoreCase)
            	If ff.Length > 1 Then
	            	Dim fname As String = Regex.Split(ff(1),"[\=|\>|\<]")(0)
	            	fi = ff(0) + "~" + fname
            	End If
                modelParam += ";" + fi + "|" & lr.Coefficients(pi)
            Next
        End If
        modelParam = modelParam.Replace("#","N*")
        Dim modelUrl As String = "javascript:window.open('popup.model.aspx?params=" + modelParam + "', 'popup', 'width=600,height=500,location=no,toolbar=no');"
        hlOpenLink.NavigateUrl = modelUrl

        Return True
    End Function

    Protected Sub lbExport_Click(sender As Object, e As System.EventArgs) Handles lbExport.Click
        Dim sb As New StringBuilder
        Dim tw As New StringWriter(sb)
        Dim hw As New HtmlTextWriter(tw)
        pnlResidual.Visible = True
        residuals.RenderControl(hw)
        Dim html As String = sb.ToString
        pnlResidual.Visible = False

        Response.Clear()
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment;filename=residuals.xls")
        Response.Write(html)
        Response.End()
    End Sub
    
'    Protected Sub lbExportTempalte_Click(sender As Object, e As System.EventArgs) Handles lbExportTemplate.Click
'    	Dim exp As New DataTable
'    	exp.Columns.Add("ID")
'        exp.Columns.Add("Name")
'        exp.Columns.Add("OutputField")
'        exp.Columns.Add("InputFields")
'        exp.Columns.Add("Filters")
'        exp.Columns.Add("CreatedBy")
'        exp.Columns.Add("CreatedDate")
'    	Dim dt As DataTable = Database.Tenant.GetDataTable("SELECT * FROM MRA_Templates")
'    	For Each dr As DataRow In dt.Rows
'    		Dim rr As DataRow = exp.NewRow
'            rr("ID") = dr.GetString("ID") 
'            rr("Name") = dr.GetString("Name") 
'            rr("OutputField") = dr.GetString("OutputField")
'            rr("InputFields") = dr.GetString("InputFields")
'            rr("Filters") = dr.GetString("Filters")
'            rr("CreatedBy") = dr.GetString("CreatedBy")
'            rr("CreatedDate") = dr.GetString("CreatedDate")
'            exp.Rows.Add(rr)
'    	Next
'    	templateExport.DataSource = exp
'        templateExport.DataBind()
'        Dim sb As New StringBuilder
'        Dim tw As New StringWriter(sb)
'        Dim hw As New HtmlTextWriter(tw)
'        pnlTemplateExport.Visible = True
'        templateExport.RenderControl(hw)
'        Dim html As String = sb.ToString
'        pnlTemplateExport.Visible = False
'
'        Response.Clear()
'        Response.ContentType = "application/vnd.ms-excel"
'        Response.AddHeader("Content-Disposition", "attachment;filename=MRA_templates.xls")
'        Response.Write(html)
'        Response.End()
'    End Sub
'    
'    Protected Sub lbImportTempalte_Click(sender As Object, e As System.EventArgs) Handles lbImportTemplate.Click
'    	pnlFormView.Visible = True
'        pnlImportCSV.Visible = True
'    End Sub
'    
'     
'    Private Sub btnUpload_Click(sender As Object, e As System.EventArgs) Handles btnUpload.Click
'	Try		
'		Dim fileExtention = System.IO.Path.GetExtension(fuImportFile.PostedFile.FileName)
'		Dim count As Integer = 0
'		If fileExtention =".csv" Then
'        	ImportMRATemplate(fuImportFile.FileContent)  
'        	Alert(count & " lookup items imported successfully.")
'        Else If fileExtention ="" Then
'        	Alert("Please choose a file!")
'        Else
'        	Alert("Invalide file type! Please choose a supporting file")
'        End If
'        Catch ex As Exception
'            Alert(ex.Message)
'        End Try
'    End Sub	
'    
'    Public Shared Function ImportMRATemplate (stream As IO.Stream)
'    		Dim importCount As Integer = 0
'    		Dim buffer(stream.Length - 1) As Byte
'    		stream.Read(buffer, 0, stream.Length)
'    		Dim content As String = System.Text.Encoding.UTF8.GetString(buffer)
'			'Database.Tenant.Execute("DELETE FROM MRA_Templates")
'            For Each line As String In content.Trim().Split(vbLf)
'            	line = line.Trim
'            	Dim parts() As String = line.Split(",")
'            	Dim Name As String = parts(1)
'            	Dim OutputField As String = parts(2)
'            	Dim InputFields As String = parts(3)
'            	Dim Filters As String = parts(4)
'            	Dim CreatedBy As String = parts(5)
'            	Dim CreatedDate As String = parts(6)
'            	Database.Tenant.Execute("INSERT INTO MRA_Templates(Name,OutputField,InputFields,Filters,CreatedBy,CreatedDate) VALUES ({0},{1},{2},{3},{4},{5})".SqlFormatString(Name,OutputField,InputFields,Filters,CreatedBy,CreatedDate.ToString()))
'            Next
'    End Function

    Public Overrides Sub VerifyRenderingInServerForm(control As System.Web.UI.Control)
    	'If control Is pnlResidual Or control Is residuals Or control Is pnlTemplateExport Or control Is templateExport Then
    	If control Is pnlResidual Or control Is residuals  Then
            Return
        End If
        MyBase.VerifyRenderingInServerForm(control)
    End Sub

    Protected Sub rpFilters_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rpFilters.ItemCommand
        If e.CommandName = "RemoveX" Then
            Dim filters = txtFilters.Text.Split(";").Select(Function(x) x.Split("`")).Select(Function(x) New QueryFilter With {.ID = x(0), .Field = x(1), .FieldName = x(2), .Operator = x(3), .Value = x(4)}).ToList
            Dim selectedFilter As QueryFilter = Nothing
            For Each f In filters
                If f.ID = e.CommandArgument Then
                    selectedFilter = f
                End If
            Next
            If selectedFilter IsNot Nothing Then
                filters.Remove(selectedFilter)
            End If
            txtFilters.Text = String.Join(";", filters.ToArray.Select(Function(x) x.ToString()))
            RefreshFilters()
        End If
    End Sub

    Protected Sub ddlInput_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlInput.SelectedIndexChanged
        If ddlInput.SelectedIndex = 0 Then
            txtLabel.Text = ""
            txtSpec.Text = ""
        Else
            Dim field As DataSourceField = fields.First(Function(x) x.Id.ToString = ddlInput.SelectedValue)
            txtLabel.Text = field.DisplayLabel
            txtSpec.Text = field.AssignedName
        End If
    End Sub
    
    Protected Sub ddlFilterField_SelectedIndexChnged(sender As Object,e As System.EventArgs) Handles ddlFilterField.SelectedIndexChanged
    	Dim fid = ddlFilterField.SelectedValue
    	Dim type = Database.Tenant.GetStringValue("SELECT DataType FROM DataSourceField where Id = "+fid)
    	If type = 1 Then 
			ddlFilterOp.Items.FindByValue(">").Enabled = False
			ddlFilterOp.Items.FindByValue(">=").Enabled = False
			ddlFilterOp.Items.FindByValue("<").Enabled = False
			ddlFilterOp.Items.FindByValue("<=").Enabled = False

    	Else
			ddlFilterOp.Items.FindByValue(">").Enabled = True
			ddlFilterOp.Items.FindByValue(">=").Enabled = True
			ddlFilterOp.Items.FindByValue("<").Enabled = True
			ddlFilterOp.Items.FindByValue("<=").Enabled = True
    	End If
    End Sub
    	
    Private Sub ddlTemplate_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlTemplate.SelectedIndexChanged
        If ddlTemplate.SelectedIndex = 0 Then
            txtNewTemplateName.Visible = True
           ' btnSaveTemplate.Visible = True
            btnDeleteTemplate.Visible = False
            txtFilters.Text = ""
            txtInputs.Text = ""
            ddlOutput.SelectedIndex = 0
        Else
            txtNewTemplateName.Visible = False
           ' btnSaveTemplate.Visible = False
            btnDeleteTemplate.Visible = True
            Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM MRA_Templates WHERE Id = " & ddlTemplate.SelectedValue)
            If dr IsNot Nothing Then
                txtFilters.Text = dr.GetString("Filters")
                txtInputs.Text = dr.GetString("InputFields")
                ddlOutput.SelectedValue = dr.GetString("OutputField")
            Else
                RefreshTemplates()
            End If
        	'txtNewTemplateName.Text = Database.Tenant.GetStringValue("SELECT Name FROM MRA_Templates WHERE Id = " & ddlTemplate.SelectedValue)
        End If
        pnlOutput.Visible = False
        RefreshInputs()
        RefreshFilters()
    End Sub

    Private Sub btnSaveTemplate_Click(sender As Object, e As System.EventArgs) Handles btnSaveTemplate.Click
        Dim fldO As String = ddlOutput.SelectedValue
        Dim fldI As String = txtInputs.Text
        Dim fldF As String = txtFilters.Text

        If fldO = "" Or fldI = "" Then
            Alert("Template cannot be saved without sufficient input/output fields.")
            Exit Sub
        End If
        Dim dt
        Dim count = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM MRA_Templates")
        If count > 20 Then
        	Alert("Maximum limit of templates is 20.")
            Exit Sub
        End If
        If txtNewTemplateName.Visible = True AndAlso txtNewTemplateName.Text.Trim = "" Then
            Alert("Required: Template Name")
            Exit Sub
        End If
		If ddlTemplate.SelectedIndex = 0 Then
		 	Dim temp As DataRow = Database.Tenant.GetTopRow("SELECT * FROM MRA_Templates where Name = {0}".SqlFormatString(txtNewTemplateName.Text.ToString()))
			If temp Is Nothing Then
				Dim sqlSave As String = "INSERT INTO MRA_Templates (Name, OutputField, InputFields, Filters, CreatedBy) VALUES ({0}, {1}, {2}, {3}, {4}); SELECT @@IDENTITY".SqlFormatString(txtNewTemplateName.Text, fldO, fldI, fldF, Page.UserName)	
				Dim newTemplateId As Integer = Database.Tenant.GetIntegerValue(sqlSave)
	        	RefreshTemplates(newTemplateId)
			Else
				Alert("A template with the same name already exists.Please use a different name.")
				txtNewTemplateName.Text = ""
			End If 
		Else
		 	Dim temp1 As DataRow = Database.Tenant.GetTopRow("SELECT * FROM MRA_Templates where Id = "& ddlTemplate.SelectedValue)
			Database.Tenant.Execute("UPDATE MRA_Templates Set Name = {0} ,OutputField = {1} ,InputFields = {2} ,Filters = {3},CreatedBy = {4} WHERE Id = {5}  ".SqlFormatString(temp1.Item("Name"), fldO, fldI, fldF, Page.UserName,ddlTemplate.SelectedValue))
		End If
    End Sub

    Private Sub RefreshTemplates(Optional templateId As Integer = -1)
        ddlTemplate.FillFromSql("SELECT Id, Name FROM MRA_Templates ORDER BY Name", True, "-- New Template --", "-1")
        If templateId = -1 Then
            ddlTemplate.SelectedIndex = 0
            txtNewTemplateName.Text = ""
            txtNewTemplateName.Visible = True
            'btnSaveTemplate.Visible = True
            btnDeleteTemplate.Visible = False
        Else
            ddlTemplate.SelectedValue = templateId
            txtNewTemplateName.Text = ""
            txtNewTemplateName.Visible = False
            'btnSaveTemplate.Visible = False
            btnDeleteTemplate.Visible = True
        End If
    End Sub

    Private Sub btnDeleteTemplate_Click(sender As Object, e As System.EventArgs) Handles btnDeleteTemplate.Click
        Database.Tenant.Execute("DELETE FROM MRA_Templates WHERE Id = " & ddlTemplate.SelectedValue)
        RefreshTemplates()

        txtInputs.Text = ""
        txtFilters.Text = ""
        RefreshFilters()
        RefreshInputs()
        ddlOutput.SelectedIndex = 0
    End Sub
End Class
