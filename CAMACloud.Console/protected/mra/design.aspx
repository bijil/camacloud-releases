﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/DesktopWeb.master" AutoEventWireup="false" Inherits="CAMACloud.Console.mra_design" CodeBehind="design.aspx.vb" %>
<%@ Register TagPrefix="cc" TagName="MRA_NavigationControl" Src="~/App_Controls/mra/MRA_NavigationControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        var expanded = false;
        $(document).on('click', function (e) {
            if ($(e.target).parents('.cbl_show').length == 0)
                $('#checkboxes').hide();
            if ($(e.target).parents('.cbl_show1').length == 0)
                $('#checkboxes1').hide();
        })
        $(document).ready(function(){
		showHide();
});
   function changeSelectedField(){
    	var tempId=$("#<%=hdnTID.ClientID %>").val();
    	$('.ddlModel option[value="'+tempId+'"]').prop('selected', true);
    }
  function showHide(){
   		var tempId	= $('.ddlModel').find(":selected").val();
        if(tempId != "") $('.main_content').show();
    	else {$('.main_content').hide(); $('.Rght_linkdiv').hide();}
    }
        function showCheckboxes() {
            var checkboxes = document.getElementById("checkboxes");
            if (!expanded) {
                checkboxes.style.display = "block";
                expanded = true;
                $('.secNbhd .cbl_show input[type="checkbox"]').unbind('click')
                $('.secNbhd .cbl_show input[type="checkbox"]').bind('click', function (e) {
                    var t = $('#checkboxes input:checked').length;
                    if (t == 0)
                        $(".selectBox select option[value='0']").text('All NBHDs')
                    else
                        $(".selectBox select option[value='0']").text(t + ' selected')
                })
            } else {
                checkboxes.style.display = "none";
                expanded = false;
            }
        }
        
        function showCheckboxes1() {
            var checkboxes = document.getElementById("checkboxes1");
            if (!expanded) {
                checkboxes.style.display = "block";
                expanded = true;
                $('.secFilter  .cbl_show1 input[type="checkbox"]').unbind('click')
                $('.secFilter  .cbl_show1 input[type="checkbox"]').bind('click', function (e) {
                    var t = $('#checkboxes1 input:checked').length;
                    if (t == 0)
                        $(".secFilter .selectBox select option[value='0']").text('All FIELDs')
                    else
                        $(".secFilter .selectBox select option[value='0']").text(t + ' selected')
                })
            } else {
                checkboxes.style.display = "none";
                expanded = false;
            }
        }
        function hide_select() {
            var checkboxes = document.getElementById("checkboxes");
        }
        $(function () {
        	$('.editView').hide();
            $('.mraFilterOp').change(function (e) {
                if ($(this).val() == 'in') {
                    $('.tip').attr('abbr', "Please insert the values as comma separated in case of using 'In' filter operator");
                }
                else {
                    $('.tip').attr('abbr', '');
                }
            })
            var t = $('#checkboxes input:checked').length;
            if (t > 0)
                $(".selectBox select option[value='0']").text(t + ' selected')
        });

        var processing = false;
        var dlg;
        function showFilterDialog(title) {
            dlg = $('.dlg-filter').dialog({
                modal: true,
                title: title, height: 240, width: 380,
                buttons: {
                    "Save": function () {
                       // if (processing) return false;
                        processing = true;
                        $('.btn-add-filter').click();
                    },
                    "Cancel": function () { dlg.dialog("close") }
                },
                close: function () {
                    $('.ui-widget-overlay').remove();
                }
            });
            $('.dlg-filter').parent()[0].style.top = '50%';
            $('.dlg-filter').parent()[0].style.position = 'fixed';
            $('.dlg-filter').parent()[0].style.margin = 'auto';
            $('.dlg-filter').parent()[0].style.transform = 'translateY(-50%)'
            $('.dlg-filter').parent().appendTo($('.dlg-filter-parent'))
        }

        function addFilter() {
            $('#hdnFilterID').val('');
            $('.filter-input').val('');
            showFilterDialog('Add Filter Condition');
        }

        function editFilter() {
            showFilterDialog('Edit Filter Condition');
        }

        function dialogSaved() {
            processing = false;
            dlg && dlg.dialog('close');
            $('.ui-widget-overlay').remove();
        }


        function showInputsDialog(title) {
            dlg = $('.dlg-inputs').dialog({
                modal: true,
                title: title, height: 260, width: 440,
                buttons: {
                    "Save": function () {
                        //if (processing) return false;
                        processing = true;
                        $('.btn-add-inputs').click();
                    },
                    "Cancel": function () { dlg.dialog("close") }
                },
                close: function () {
                    $('.ui-widget-overlay').remove();
                }
            });
            $('.dlg-inputs').parent()[0].style.top = '50%';
            $('.dlg-inputs').parent()[0].style.position = 'fixed';
            $('.dlg-inputs').parent()[0].style.margin = 'auto';
            //$('.dlg-inputs').parent()[0].style.transform = 'translateY(-50%)'
            $('.dlg-inputs').parent().appendTo($('.dlg-inputs-parent'))
        }

        function addInputs() {
            $('.input-field').val('');
            showInputsDialog('Add Input Field/Specification');
        }
        
        //for page heading
        $(function(){
         $("#spanMainHeading").html('Design Regression Model');
        
        })

    </script>
    <style type="text/css">
    #divContentArea {
    top:100px;
    }
    .divNavBtn {
        padding: 4px 0px 4px;
    }
    
        .ui-widget-overlay {
            background: #000 !important;
            opacity: .40 !important;
            filter: Alpha(Opacity=40) !important;
        }

        .reg-stat {
        }

            .reg-stat td {
                width: 120px;
                text-align: right;
                padding-right: 15px;
            }

                .reg-stat td:first-child {
                    width: 120px;
                    text-align: left;
                }

        .anova {
        }

            .anova td {
                text-align: right;
            }

                .anova td:first-child {
                    text-align: left;
                }

        .th-right {
            text-align: right !important;
        }

        .selected-input {
            display: inline-block;
            background: LightBlue;
            border: 1px solid Navy;
            margin: 3px;
            margin-right: 8px;
            padding: 2px 5px;
            border-radius: 4px;
        }

            .selected-input span {
                font-weight: bold;
                margin: 0px 6px;
            }

            .selected-input a {
                color: #1677c5;
                margin-right: 4px;
                text-decoration: none;
            }
            
             .selected-input #d {
                color: Red;
                margin-right: 4px;
                text-decoration: none;
            }
            
         .selected-input-field {
            display: inline-block;
            background: #b6bee6;
            border: 1px solid Navy;
            margin: 3px;
            margin-right: 8px;
            padding: 2px 5px;
            border-radius: 4px;
        }
		          .selected-input-field span {
                font-weight: bold;
                margin: 0px 6px;
            }

            .selected-input-field a {
                color: #1677c5;
                margin-right: 4px;
                text-decoration: none;
            }
            
             .selected-input-field #d {
                color: Red;
                margin-right: 4px;
                text-decoration: none;
            }
            
        .filter-statement {
            display: inline-block;
            background: rgba(255, 216, 0, 0.4);
            border: 1px solid #ff6a00;
            margin: 3px;
            margin-right: 8px;
            padding: 2px 5px;
            border-radius: 4px;
        }

            .filter-statement span {
                font-weight: bold;
                margin: 0px 6px;
                margin-right: 15px;
            }

            .filter-statement a {
                margin-right: 2px;
                text-decoration: none;
            }

        .multiselect {
            width: 200px;
        }

        .selectBox {
            position: relative;
        }



        .overSelect {
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
        }

        #checkboxes {
            display: none;
            border: 1px #dadada solid;
            height: 200px;
            overflow: auto;
            width: 239px;
            position: absolute;
            background: white;
        }
        
           #checkboxes1 {
            display: none;
            border: 1px #dadada solid;
            height: 200px;
            overflow: auto;
            width: 239px;
            position: absolute;
            background: white;
        }
        .btnShow {  
    font-weight: bold;
    margin-left: 22px;
    border:none;
    border-radius: 0px;
    background: linear-gradient(#88c2f1,#326b84);
    color: white;
    font-family: sans-serif;
    font-size: 12px;
    cursor: pointer;
    height: 22px;
    }
    </style>
</asp:Content>
 <asp:Content ID="Content4" ContentPlaceHolderID="NavBarContent" runat="server">
 <div class="Overlay_Div">
    <cc:MRA_NavigationControl ID="mra_nav" runat="server" />
    </div>
 </asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="Cont_Content" style="margin-top:1%;">
   
   </div>
  <asp:Panel class="main_content" ID="main_content" style="margin-left: 2%;" runat="server" >   
    <asp:Panel runat="server" ID="pnlInput">
    <div style="float:left;width:100%" >
        <table style="float:left;max-width: 80%;">
            <tr>
                <td style="width: 180px;">Regression Model Name: 
                </td>
                <td>
                    <asp:HiddenField runat="server" ID="hdnTID" Value="-1" />
                    <asp:TextBox runat="server" ID="txtTemplateName" Width="240px" MaxLength="30" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="txtTemplateName"
                        ValidationGroup="FieldSel" Text="Required" Font-Size="Smaller" />
                </td>
            </tr>
            <tr class = "secNbhd">
                <td>Select Groups:
                </td>
                <td class="cbl_show">
                    <div class="selectBox tip" onclick="showCheckboxes()" abbr="Select the neighborhoods from the drop-down list" style="width: 240px;">
                        <asp:DropDownList runat="server" ID="ddlNbhd" style="width: 100%;" />
                        <div class="overSelect"></div>
                    </div>
                    <div id="checkboxes">
                        <asp:CheckBoxList ID="cbl_Nbhd" runat="server"></asp:CheckBoxList>
                    </div>

                </td>
            </tr>
              <asp:Repeater runat="server" ID="rpFilterFields">
              <ItemTemplate>
              	<tr>
                    <td style="width: 180px;">
                    	<asp:Label runat="server" ID="lblFilterFields" Text = '<%# Eval("DisplayLabel") %> ' />:
                    	<asp:Label runat="server" ID="lblFieldId" style = "Display:none;" Text = '<%# Eval("Id") %> ' />
                    </td>
                    <td>
                    	<div>
                            <asp:ListBox ID="lbFilterFields" runat="server" SelectionMode = "multiple"  Rows="5" Width="240px" DataTextField="DescValue" DataValueField="IdValue"></asp:ListBox>
                        <div>
                    </td>
               </tr>
             </ItemTemplate>
             </asp:Repeater>
            <%--<tr class = "secFilter">
                <td>Select Filter :</td>
                <td class="cbl_show1">
                    <div class="selectBox tip" onclick="showCheckboxes1()" abbr="Select the neighborhoods from the drop-down list">
                        <asp:DropDownList runat="server" ID="ddlSelectedFilters" Width="240px" />
                        <div class="overSelect"></div>
                    </div>
                    <div id="checkboxes1" style = "overflow-y : scroll; width : 240px; height : 200px">
                        <asp:CheckBoxList ID="cbl_Filters" runat="server"></asp:CheckBoxList>
                    </div>

                </td>
            </tr>--%>
                
            <tr>
                <td>Output Field:
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlOutput" Width="240px" />
                    <asp:RequiredFieldValidator runat="server" ID="rfv1" ControlToValidate="ddlOutput"
                        ValidationGroup="FieldSel" Text="Required" Font-Size="Smaller" />
                </td>
            </tr>
            <tr>
           		<td>Comments (If any): </td>
           		<td><asp:TextBox id="txtcomments" TextMode="multiline" Columns="30" Rows="3" runat="server" style="width: 235px;" /></td>
           	</tr>
            <asp:UpdatePanel runat="server" ID="updInputFields" UpdateMode="Conditional">
                <ContentTemplate>
                        <td style="width: 180px;">Input/Factor Fields:
                        </td>
                        <td>
                            <div style="display: none;">
                                <asp:TextBox runat="server" ID="txtInputs" Width="300px" Rows="4" TextMode="MultiLine" />
                            </div>

                            <div>
                                <asp:Repeater runat="server" ID="rpInputs">
                                    <ItemTemplate>
                                        <div class="selected-input">
                                            <span><%# Eval("DisplayLabel")%></span>
                                            <asp:LinkButton runat="server" Text="&#9998;" CommandArgument='<%# Eval("ID") %>'
	                                            CommandName="EditX" ToolTip="Edit Input condition" />
                                            <asp:LinkButton runat="server" ID="d" Text="&#10060;" CommandArgument='<%# Eval("Id") %>'
                                                    CommandName="RemoveX" />
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <a style="font-weight: bold;white-space: nowrap;" onclick="addInputs();return false;" href="#">[+] Add New</a>
                                <%--<asp:LinkButton runat="server" ID="addNewIp" Text="[+] Add New" font-weight: bold; CommandArgument=''
                                                    CommandName="add" />--%>
                            </div>
                                                        <asp:RequiredFieldValidator runat="server" ID="rfv3" ControlToValidate="txtInputs"
                                ValidationGroup="FieldSel" Text="Add input field(s) to proceed." Font-Size="Smaller" Display="Dynamic" />
						
                            <div class="dlg-inputs-parent"></div>
                            <div class="dlg-inputs" style="display: none; padding: 8px;">
                                <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <table>
                                            <tr>
                                                <td style="width: 140px;">Select Field:
                                                </td>
                                                <td>
                                                 <asp:HiddenField runat="server" ID="hdnInputID" ClientIDMode="Static" Value="" />
                                                    <asp:DropDownList runat="server" ID="ddlInput" Width="240px" AutoPostBack="true" />
                                                    <asp:RequiredFieldValidator runat="server" ID="rfv2_2" ControlToValidate="ddlInput"
                                                        ValidationGroup="InputAdd" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Label: 
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" ID="txtLabel" Width="240px" MaxLength="25" />
                                                    <asp:RequiredFieldValidator runat="server" ID="rfv2_1" ControlToValidate="txtLabel"
                                                        ValidationGroup="InputAdd" />
                                                </td>

                                            </tr>
                                            <tr>
                                                <td>Field Specification: 
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" ID="txtSpec" Width="240px" MaxLength="50" TextMode="MultiLine" Rows="4" />
                                                    <asp:RequiredFieldValidator runat="server" ID="rfv2" ControlToValidate="txtSpec"
                                                        ValidationGroup="InputAdd" />
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                                <asp:Button runat="server" ID="btnAddInput" Text=" Add " ValidationGroup="InputAdd" CssClass="btn-add-inputs" Style="display: none;" />

                            </div>
                        </td>
                    </tr>
                </ContentTemplate>
            </asp:UpdatePanel>
        </table>
        
        
         <%--vin-str <div class="hideonprint link-panel" style="float: left;width:130px;">
        <a href='models.aspx' style="margin-top: 10px;">View All Models</a>
        <asp:HyperLink style="margin-top: 10px;" runat="server" ID="hlResult" Text="Analysis Result" Visible="false" />
        <asp:HyperLink style="margin-top: 10px;" runat="server" ID="hlCalc" Text="Cost Calculator" Visible="false" /> vin-end--%>
    </div>
        
        </div>
        <asp:UpdatePanel runat="server" ID="updFilters" UpdateMode="Conditional">
            <ContentTemplate>
                <table style="max-width:80%">
                    <tr>
                        <td style="width: 180px;">Data Filter Conditions:
                        </td>
                        <td>
                            <div style="display: none;">
                                <asp:TextBox runat="server" ID="txtFilters" />
                            </div>
                            <asp:Repeater runat="server" ID="rpFilters">
                                <ItemTemplate>
                                    <div class="filter-statement">
                                        <span>
                                            <%# Eval("FieldName")%>
                                            <%# Eval("Operator")%>
                                            <%# Eval("Value")%></span>
                                        <asp:LinkButton runat="server" Text="&#9998;" CommandArgument='<%# Eval("ID") %>'
                                            CommandName="EditX" ToolTip="Edit filter condition" />
                                        <asp:LinkButton runat="server" ID="d" Text="&#10060;" CommandArgument='<%# Eval("ID") %>'
                                            CommandName="RemoveX" ToolTip="Remove filter condition" />
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                            <a style="font-weight: bold;white-space: nowrap;" onclick="addFilter();return false;" href="#">[+] Add New</a>
                        </td>

                    </tr>
                    <tr>
                        <td>&nbsp;
                        </td>
                        <td>
                            <div class="dlg-filter-parent"></div>
                            <div class="dlg-filter" style="padding: 8px; display: none;">
                            <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                             <ContentTemplate>
                                <asp:HiddenField runat="server" ID="hdnFilterID" ClientIDMode="Static" Value="" />
                                <asp:DropDownList runat="server" ID="ddlFilterField" Width="180px" AutoPostBack="true" CssClass="filter-input" />
                                <asp:RequiredFieldValidator runat="server" ID="f1" ControlToValidate="ddlFilterField"
                                    ValidationGroup="AddFilter" />
                                <asp:DropDownList runat="server" ID="ddlFilterOp" Width="150px" CssClass="mraFilterOp filter-input">
                                    <asp:ListItem Text="-- Select --" Value="" />
                                    <asp:ListItem Text="Equal To" Value="=" />
                                    <asp:ListItem Text="Not Equal To" Value="&lt;&gt;" />
                                    <asp:ListItem Text="Greater Than" Value="&gt;" />
                                    <asp:ListItem Text="Lesser Than" Value="&lt;" />
                                    <asp:ListItem Text="Greater Than Or Equal To" Value="&gt;=" />
                                    <asp:ListItem Text="Lesser Than Or Equal To" Value="&lt;=" />
                                    <asp:ListItem Text="In" Value="in" />
                                    <asp:ListItem Text="Between" Value="between" />
                                    <asp:ListItem Text="Like" Value="like" /> 
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator runat="server" ID="f2" ControlToValidate="ddlFilterOp"
                                    ValidationGroup="AddFilter" />
                                <br />
                                <br />
                                <span class="tip" abbr="">
                                    <asp:TextBox runat="server" ID="txtFilterValue" Width="344px" TextMode="MultiLine" Rows="4" CssClass="filter-input" /></span>
                                <asp:RequiredFieldValidator runat="server" ID="f3" ControlToValidate="txtFilterValue"
                                    ValidationGroup="AddFilter" />   
                               </ContentTemplate>
                              </asp:UpdatePanel>
                                <asp:Button runat="server" ID="btnAddFilter" CssClass="btn-add-filter" Text=" Add " ValidationGroup="AddFilter" Style="display: none;" />
					            <asp:UpdateProgress runat="server" AssociatedUpdatePanelID="updFilters">
					            <ProgressTemplate>
					                <div>
					                    Please wait ...
					                </div>
					            </ProgressTemplate>
					        	</asp:UpdateProgress>
                            </div>

                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress runat="server" AssociatedUpdatePanelID="updInputFields">
            <ProgressTemplate>
                <div>
                    Please wait ...
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
         <%--<asp:UpdateProgress runat="server" AssociatedUpdatePanelID="updFilters">
            <ProgressTemplate>
                <div>
                    Please wait ...
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>--%>
        <asp:Panel runat="server" ID = "pnlWaitMessage" Visible="false">Please wait ...</asp:Panel> 
        <div>
            <asp:CheckBox runat="server" ID="chkConstantIsZero" Text="Constant is zero" Font-Bold="true" />
        </div>
        <p>
            <asp:Button runat="server" ID="btnTest" cssClass="btnShow" Text="Test Data" ValidationGroup="FieldSel" />
            <asp:Button runat="server" ID="btnGenerateMRA" Text="Save & Run Analysis" ValidationGroup="FieldSel" CssClass="b btnShow" />
            <asp:HyperLink runat="server" ID="lbViewOutput" />
        </p>
        <p style="display: none;">
            <asp:Label runat="server" ID="lblSQL" />
        </p>
    </asp:Panel>
    <%--<asp:Panel runat="server" ID="pnlOutput">
        <h2>Multiple Regression Analysis Output</h2>


        <h3>Regression Statistics</h3>
        <table cellspacing="0" style="border-collapse: collapse" class="mGrid app-content reg-stat">
            <tr>
                <td>Multiple R
                </td>
                <td>
                    <asp:Label runat="server" ID="lblMultipleR" Text="0" />
                </td>
            </tr>
            <tr>
                <td>R Square
                </td>
                <td>
                    <asp:Label runat="server" ID="lblRSquare" />
                </td>
            </tr>
            <tr>
                <td>Adjusted R Square
                </td>
                <td>
                    <asp:Label runat="server" ID="lblAdjustedRSquare" />
                </td>
            </tr>
            <tr>
                <td>Standard Error
                </td>
                <td>
                    <asp:Label runat="server" ID="lblStandardError" />
                </td>
            </tr>
            <tr>
                <td>Observations
                </td>
                <td>
                    <asp:Label runat="server" ID="lblObservations" />
                </td>
            </tr>
        </table>
        <p>
            <asp:HyperLink runat="server" ID="hlOpenLink">View Regression Model & Form</asp:HyperLink>
        </p>
        <h3>ANOVA</h3>
        <table cellspacing="0" style="border-collapse: collapse; text-align: center;" class="mGrid app-contents anova">
            <tr class="ui-state-default">
                <th class="th-right" style="width: 80px;"></th>
                <th class="th-right" style="width: 30px;">df
                </th>
                <th style="width: 120px;" class="th-right">SS
                </th>
                <th style="width: 120px;" class="th-right">MS
                </th>
                <th style="width: 120px;" class="th-right">F
                </th>
                <th style="width: 120px;" class="th-right">Significance F
                </th>
            </tr>
            <tr>
                <td>Regression
                </td>
                <td>
                    <asp:Label runat="server" ID="lblDFReg" />
                </td>
                <td>
                    <asp:Label runat="server" ID="lblSSReg" />
                </td>
                <td>
                    <asp:Label runat="server" ID="lblMSReg" />
                </td>
                <td>
                    <asp:Label runat="server" ID="lblFisher" />
                </td>
                <td>
                    <asp:Label runat="server" ID="lblSignificanceF" />
                </td>
            </tr>
            <tr>
                <td>Residual
                </td>
                <td>
                    <asp:Label runat="server" ID="lblDFRes" />
                </td>
                <td>
                    <asp:Label runat="server" ID="lblSSRes" />
                </td>
                <td>
                    <asp:Label runat="server" ID="lblMSRes" />
                </td>
                <td>&nbsp;
                </td>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td>Total
                </td>
                <td>
                    <asp:Label runat="server" ID="lblDFTotal" />
                </td>
                <td>
                    <asp:Label runat="server" ID="lblSSTotal" />
                </td>
                <td>
                    <asp:Label runat="server" ID="lblMSTotal" />
                </td>
                <td>&nbsp;
                </td>
                <td>&nbsp;
                </td>
            </tr>
        </table>
        <h3>Regression Coefficients</h3>
        <asp:GridView runat="server" ID="regresult">
            <Columns>
                <asp:BoundField DataField="Head" />
                <asp:BoundField DataField="CE" HeaderText="Coefficients" ItemStyle-Width="120px"
                    ItemStyle-HorizontalAlign="Right" HeaderStyle-CssClass="th-right" />
                <asp:BoundField DataField="SE" HeaderText="Standard Error" ItemStyle-Width="120px"
                    ItemStyle-HorizontalAlign="Right" HeaderStyle-CssClass="th-right" />
                <asp:BoundField DataField="TS" HeaderText="t Stat" ItemStyle-Width="120px" ItemStyle-HorizontalAlign="Right"
                    HeaderStyle-CssClass="th-right" />
                <asp:BoundField DataField="PV" HeaderText="p Value" ItemStyle-Width="120px" ItemStyle-HorizontalAlign="Right"
                    HeaderStyle-CssClass="th-right" />
            </Columns>
        </asp:GridView>
        <asp:LinkButton runat="server" Text="Export Residual Output" ID="lbExport" />
        <asp:Panel runat="server" ID="pnlResidual" Visible="false">
            <h3>Residual Output</h3>
            <asp:GridView runat="server" ID="residuals">
                <Columns>
                    <asp:BoundField DataField="ID" HeaderText="Obs." ItemStyle-Width="80px" />
                    <asp:BoundField DataField="Item" HeaderText="Item" ItemStyle-Width="200px" />
                    <asp:BoundField DataField="PR" HeaderText="Predicted" ItemStyle-Width="180px" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-CssClass="th-right" />
                    <asp:BoundField DataField="RES" HeaderText="Residual" ItemStyle-Width="180px" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-CssClass="th-right" />
                </Columns>
            </asp:GridView>
        </asp:Panel>
        <p>
            &nbsp;
        </p>
        </div>
    </asp:Panel>--%>
</asp:Panel>
</asp:Content>
