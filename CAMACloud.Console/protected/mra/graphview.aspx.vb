﻿Imports System.IO
Imports CAMACloud.Data
Public Class graphview
	Inherits System.Web.UI.Page
	Dim Private WithEvents mraGraphView As MRA_NavigationControl
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load	
		Dim templateId As Integer = -1
		ddlselectedId.Value = ""
		mraGraphView = New MRA_NavigationControl ()
		RemoveHandler mraGraphView.ddlChanged ,AddressOf Me.NavControl_ddlmodel_selectIndexChanged 
		AddHandler mraGraphView.ddlchanged, AddressOf Me.NavControl_ddlmodel_selectIndexChanged  
	If Not IsPostBack Then
		GetUserRole()
		If Integer.TryParse(Request("_tid"), templateId) Then
				ddlselectedId.Value= templateId
            	'ddlModel.FillFromSql("SELECT ID, Name FROM MRA_Templates WHERE ID={0} ".SqlFormatString (templateId),False)
            	Dim sql1  = "SELECT FieldID, Head FROM MRA_Coefficients WHERE TemplateID = {0} and FieldID is not null AND FieldID > 0 ORDER BY Head"
            	ddlFields.FillFromSql(sql1.FormatString (templateId ), True)
            	ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "changeSelectedField();", True)
            Else
                'ddlModel.FillFromSql("SELECT ID, Name FROM MRA_Templates WHERE LastExecutionTime IS NOT NULL ORDER BY Name", True)
                'pnlSelect.Visible = True
                 ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "showHide();", True)
            End If
        End If
    End Sub

'    Protected Sub btnShow_Click(sender As Object, e As EventArgs) Handles btnShow.Click
'    	If ddlFields.SelectedIndex > 0 AndAlso ddlCount .SelectedIndex > 0 Then
'   			RunScript ("drawScatterPlot()")
'   	 	End If
'    End Sub
    Private Function GetUserRole()
        Dim currentUserRole = Roles.GetRolesForUser()
        If currentUserRole.Contains("QC") Then
            hasroles.Value = "1"
        End If 	
    End Function
   
    Private Function GetSql(templateId As Integer) As String
	            Dim extraFieldsInExport As Datatable = Database.Tenant.GetDataTable("SELECT FieldId FROM MRA_Filters WHERE IsExportField = 1")
	            Dim extraFieldNames As String = ""
				Dim dr As DataRow =Database .Tenant.GetTopRow("Select name,dbo.cc_findTargetTable(sourcetable) SourceTable From datasourceField Where id= {0}" .SqlFormat (False, ddlFields .SelectedValue))
	            Dim StartX As Double =Convert.ToDouble (Xto.Text())
	            'Double.TryParse(Xfrom.Text(),StartX)
	            Dim EndX As Double =Convert.ToDouble (Xto.Text())
	            Dim StartY As Double =Convert.ToDouble (Yfrom.Text())
	            Dim EndY As Double =Convert.ToDouble (Yto.Text())	
	            Dim i = 0
	            Dim sql As String = "SELECT Top {1}  p.KeyValue1 As KeyValue, n.Number As AssignmentGroup, r.* FROM MRA_ResidualOutput r INNER JOIN Parcel p ON r.ParcelId = p.Id LEFT OUTER JOIN Neighborhood n ON p.NeighborhoodId = n.Id WHERE r.TemplateID = {0} ORDER BY ObservationNo ".FormatString(templateId,ddlCount .SelectedValue)
'	            If extraFieldsInExport.Length > 0 Then
'	                    For Each ef In extraFieldsInExport.Split(",")
'	                            ef = Replace(ef, "'", "")
'	                            Dim fId = Convert.ToInt32(ef)
'	                            Dim fName = Database.Tenant.GetStringValue("SELECT AssignedName FROM DataSourceField WHERE Id = "&fId)
'	                            extraFieldNames += IIf(i=0, "pd."+fName,", pd." + fName)
'	                            i += 1
'	                    Next
'	                            Dim part As String() = Regex.Split(sql," from ",RegexOptions.IgnoreCase)
'	                            Dim SelectSql As String = part(0) + " ," + extraFieldNames + " FROM " + part(1)
'	                            Dim part1 As String() = Regex.Split(SelectSql," where ",RegexOptions.IgnoreCase)
'	                            Dim TableSql As String = "Inner Join {0} X on X.cc_parcelid=p.Id ".FormatString (dr.GetString ("SourceTable"))
'	                            Dim andPart As String=" (Residual between {0} AND {1}) AND (X.{4} between {2} AND {3}) AND ".FormatString(StartY,EndY,StartX,EndX,dr.GetString ("name"))
'	                            SelectSql = part1(0) + " INNER JOIN ParcelData pd ON Pd.CC_ParcelId = p.Id " +  TableSql + " WHERE " + andPart + part1(1)
'	                            Return SelectSql
'	            Else  
'	            		Return sql
'	            End If
				If extraFieldsInExport IsNot Nothing AndAlso extraFieldsInExport.Rows.Count > 0 Then
	                    For Each row As DataRow In extraFieldsInExport.Rows
	                            Dim fId = row.Item("FieldId")
	                            Dim fName = Database.Tenant.GetStringValue("SELECT AssignedName FROM DataSourceField WHERE Id = "&fId)
	                            extraFieldNames += IIf(i=0, "pd."+fName,", pd." + fName)
	                            i += 1
	                    Next
	                            Dim part As String() = Regex.Split(sql," from ",RegexOptions.IgnoreCase)
	                            Dim SelectSql As String = part(0) + " ," + extraFieldNames + " FROM " + part(1)
	                            Dim part1 As String() = Regex.Split(SelectSql," where ",RegexOptions.IgnoreCase)
	                            Dim TableSql As String = "Inner Join {0} X on X.cc_parcelid=p.Id ".FormatString (dr.GetString ("SourceTable"))
	                            Dim andPart As String=" (Residual between {0} AND {1}) AND (X.{4} between {2} AND {3}) AND ".FormatString(StartY,EndY,StartX,EndX,dr.GetString ("name"))
	                            SelectSql = part1(0) + " INNER JOIN ParcelData pd ON Pd.CC_ParcelId = p.Id " +  TableSql + " WHERE " + andPart + part1(1)
	                            Return SelectSql
	            Else  
	            		Return sql
	            End If	
    End Function 
    Public overloads Overrides Sub VerifyRenderingInServerForm(Byval control As Control)
    	
    End Sub
    Protected Sub ExportExcel()
    	Dim templateId As Integer =Convert.ToInt32(MRA_NavigationControl.__mraDDLItem )
    	Dim c As Integer
   		Dim jobtype As String ="export"
    	Dim dr As DataRow =Database .Tenant.GetTopRow("Select name,dbo.cc_findTargetTable(sourcetable) SourceTable From datasourceField Where id= {0}" .SqlFormat (False, ddlFieldId.Value))
	    Dim StartX,StartY,EndX,EndY As Double
		Double.TryParse(Xfrom.Text(),StartX)
		Double.TryParse(Yfrom.Text(), StartY)
		Double.TryParse(Xto.Text(), EndX)
		Double.TryParse(Yto.Text(),EndY)
		If (StartY = EndY Or EndX=EndY ) Then
			StartX = 0
			EndX = 0
			StartY = 0
			EndY = 0 
		End If
		Integer.TryParse(ddlCount.SelectedValue, c)
   		Dim dt As DataTable =Database.Tenant .GetDataTable ("EXEC MRA_GraphviewRecords @FieldName={0},@SourceTable={1},@pCount={2},@tempID={3},@StartX={4},@StartY={5},@EndX={6}, @EndY={7}, @jobType={8}".SqlFormatString (dr.GetString ("name"),dr.GetString ("SourceTable"),c,templateId,StartX,StartY,EndX,EndY,jobtype))
			residuals.DataSource = dt
	        residuals.DataBind()     
	        Dim sb As New StringBuilder
	        Dim tw As New StringWriter(sb)
	        Dim hw As New HtmlTextWriter(tw)
	       Dim lr As New Statistics.LinearRegression
	        pnlResidual.Visible = True
	        residuals.RenderControl(hw)
	        Dim html As String = sb.ToString
	        pnlResidual.Visible = False
	        Response.Clear()
	        Response.ContentType = "application/vnd.ms-excel"
	        Response.AddHeader("Content-Disposition", "attachment;filename=residuals.xls")
	        Response.Write(html)
	        Response.End()
End Sub
    Protected Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
    	ExportExcel()
    End Sub
 'Private Sub ddlModel_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlModel.SelectedIndexChanged
      ' If ddlModel.SelectedIndex > 0 Then
           '	Dim sql1  = "SELECT FieldID, Head FROM MRA_Coefficients WHERE TemplateID ='" + ddlModel.SelectedValue +"'  and FieldID is not null AND FieldID > 0 ORDER BY Head"
           	'ddlFields.FillFromSql(sql1, True)
        'Else
        	'ddlFields.Items.Clear ()
       ' End If
' End Sub
Protected Sub NavControl_ddlmodel_selectIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles mraGraphView.ddlchanged
	If (HttpContext.Current.Request.Url.AbsolutePath.Contains("graphview")) Then
		Dim d = MRA_NavigationControl.__mraDDLItem
		ddlselectedId.Value=  MRA_NavigationControl.__mraDDLItem
		If d > 0 Then
			Dim sql1  = "SELECT FieldID, Head FROM MRA_Coefficients WHERE TemplateID = {0} and FieldID is not null AND FieldID > 0 ORDER BY Head"
			ddlFields.FillFromSql(sql1.FormatString (d ), True)
			'HttpContext.Current.Response.Redirect("graphview.aspx?_tid=" & d)
		Else
			ddlFields.Items.Clear()
		 	ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "showHide();", True)
		End If
	End If
	RemoveHandler mraGraphView.ddlChanged ,AddressOf Me.NavControl_ddlmodel_selectIndexChanged 
    End Sub
End Class