﻿Imports CAMACloud.BusinessLogic.DataAnalyzer
Public Class calculator
    Inherits System.Web.UI.Page

    Dim fields As List(Of DataSourceField)
    Dim outputField As String
    'Dim intercept As Double
    Dim inputFields As New Dictionary(Of Integer, InputField)
    'Dim inputFieldIds As New List(Of Integer)
    'Dim inputSpecs As New List(Of String)
    'Dim coefficients As New List(Of Double)
    Dim inputControls As New List(Of WebControl)
	Dim Public WithEvents mraCalcView As MRA_NavigationControl
	Sub OpenTemplate(templateId As Integer)
        Dim t As DataRow = RegressionAnalyzer.GetMRATemplate(templateId)
        Dim c As DataTable = RegressionAnalyzer.GetCoefficients(templateId)

        If t Is Nothing Then
            Response.Redirect("default.aspx")
            Return
        End If

        If c.Rows.Count = 0 Then
            HttpContext.Current.Response.Redirect("design.aspx?_tid=" & templateId)
            Return
        End If

        hdnTID.Value = templateId

        Dim inputSpec = t.GetString("InputFields")
        Dim outputField = t("OutputFieldLabel")
        lblOutputLabel.Text = outputField

        GenerateForm(inputSpec)


    End Sub

    Sub GenerateForm(inputSpec As String)
        calcstate.Value = inputSpec

        inputControls.Clear()
        tbl.Controls.Clear()

        inputFields = InputField.FromSpecString(inputSpec, fields)

        For i As Integer = 1 To inputFields.Count
            Dim fld = inputFields(i)

            Dim tr As New TableRow
            tr.CssClass = "mra_calc_row"
            Dim td1 As New TableCell
            Dim td2 As New TableCell
            td1.CssClass = "mra_calc_label"
            td2.CssClass = "mra_calc_value"
            td1.Controls.Add(New LiteralControl(fld.Label + " :"))
            Dim field As DataSourceField = fields.Where(Function(x) x.Id = fld.FieldID).First
            Dim rf As New RequiredFieldValidator()
            If field.InputType = 5 Then
                Dim ddl As New DropDownList
                ddl.ID = "ddl" + field.AssignedName
                ddl.CssClass = "mraf"
                'ddl.Width = New Unit(176)
                td2.Controls.Add(ddl)
                ddl.FillFromSql("SELECT Value, DescValue FROM ParcelDataLookup WHERE LookupName = '" + field.LookupTable + "' ORDER BY Ordinal", True)
                rf.ControlToValidate = "ddl" + field.AssignedName
                inputControls.Add(ddl)
            Else
                Dim txt As New TextBox
                txt.ID = "txt" + field.AssignedName
                txt.CssClass = "mraf"
                txt.TextMode = TextBoxMode.Number
                'txt.Width = New Unit(170)
                td2.Controls.Add(txt)

                rf.ControlToValidate = "txt" + field.AssignedName
                inputControls.Add(txt)
            End If

            rf.Text = " Required"
            rf.Font.Size = New FontUnit(FontSize.Smaller)
            rf.ValidationGroup = "InputForm"
            td2.Controls.Add(rf)

            tr.Cells.Add(td1)
            tr.Cells.Add(td2)
            tbl.Rows.Add(tr)
        Next

        pnlCalculator.Visible = True
    End Sub

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
    	Dim templateId As Integer = -1
        fields = DataSourceField.GetFields
        If IsPostBack Then
            If Request.Form("calcstate") IsNot Nothing Then
                GenerateForm(Request.Form("calcstate"))
            End If
        End If

    End Sub

    Protected Sub btnCalculate_Click(sender As Object, e As System.EventArgs) Handles btnCalculate.Click
        Dim templateId As Integer = hdnTID.Value
        Dim t As DataRow = Database.Tenant.GetTopRow("SELECT * FROM MRA_Templates WHERE ID = " & templateId)
        Dim sql As String = t.GetString("CostFormulaSQL")	
        Dim params As New System.Collections.Specialized.NameValueCollection
        For i As Integer = 0 To inputControls.Count - 1
            Dim ctrl As WebControl = inputControls(i)
            Dim fld = inputFields(i + 1)
            Dim fieldName = fields.Where(Function(x) x.Id = fld.FieldID).First.AssignedName

            If TypeOf ctrl Is TextBox Then
            	Dim txt As TextBox = ctrl
	                If IsNumeric(txt.Text) Then
	                    params.Add("@" + fieldName, Double.Parse(txt.Text.Trim))
	                Else
	                    lblOutput.Text = "Invalid Input!"
	                    Return
	                End If  
            ElseIf TypeOf ctrl Is DropDownList Then
                Dim ddl As DropDownList = ctrl
                If IsNumeric(ddl.SelectedValue) Then
                    params.Add("@" + fieldName, Double.Parse(ddl.SelectedValue))
                Else
                    lblOutput.Text = "Invalid Input!"
                    Return
                End If
            End If
        Next

        Dim output As Double = Database.Tenant.GetDataTable(sql, CommandType.Text, params).Rows(0)(0)

	lblOutput.Text = (Math.Round(output / 100) * 100).ToString("$#,###,###,##0")
    End Sub

    Private Sub calculator_Load(sender As Object, e As EventArgs) Handles Me.Load
    	Dim templateId As Integer = -1
    	mraCalcView = New MRA_NavigationControl ()
    	hdnModel.Value= -1
    	hdnLoadOnce.Value = hdnLoadOnce.Value + 1 
    	RemoveHandler mraCalcView.ddlchanged, AddressOf Me.NavControl_ddlmodel_selectIndexChanged  
		AddHandler mraCalcView.ddlchanged, AddressOf Me.NavControl_ddlmodel_selectIndexChanged  
		If Not IsPostBack Then
			'hdnLoadOnce.Value = 0
            If Integer.TryParse(Request("_tid"), templateId) Then
            	'pnlSelect.Visible = False
            	hdnModel.Value= Request("_tid")
                OpenTemplate(templateId)
                ' ddlselectedItem.Value = templateId 
                'hdnLoadOnce.Value = hdnLoadOnce.Value + 1 
                'ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "changeSelectedField();", True)
            Else
                'ddlModel.FillFromSql("SELECT ID, Name FROM MRA_Templates WHERE LastExecutionTime IS NOT NULL ORDER BY Name", True)
                'pnlSelect.Visible = True
                 hdnLoadOnce.Value = hdnLoadOnce.Value + 1 
              	ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "showHide();", True)
              	pnlCalculator.Visible = False
            End If
		Else
        		hdnTID.Value= MRA_NavigationControl.__mraDDLItem
            	ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "showHide();", True)
        End If
    End Sub
    Private Sub NavControl_ddlmodel_selectIndexChanged(sender As Object, e As EventArgs) Handles mraCalcView.ddlchanged
    	If (HttpContext.Current.Request.Url.AbsolutePath.Contains("calculator")) AndAlso hdnLoadOnce.Value > 2 Then
    		'ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "showHide();", True)
       		If MRA_NavigationControl.__mraDDLItem  = 0 Then
            	pnlCalculator.Visible = False
            	calcstate.Value = ""
       		Else
       			OpenTemplate(MRA_NavigationControl.__mraDDLItem)
       			hdnModel.Value = MRA_NavigationControl.__mraDDLItem
        		'HttpContext.Current.Response.Redirect("calculator.aspx?_tid=" & MRA_NavigationControl.__mraDDLItem)
            	lblOutput.Text = "-"
        	End If
    	End If
    	'RemoveHandler mraCalcView.ddlchanged, AddressOf Me.NavControl_ddlmodel_selectIndexChanged  
    End Sub
End Class