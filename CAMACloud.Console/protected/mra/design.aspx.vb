﻿Imports System.Web.UI.DataVisualization.Charting
Imports System.IO
Imports CAMACloud.Data
Imports CAMACloud.BusinessLogic.DataAnalyzer

Partial Class mra_design
    Inherits System.Web.UI.Page
	Dim Public WithEvents mradesignView As MRA_NavigationControl
    Dim fields As List(Of DataSourceField)

    Public ReadOnly Property TemplateID As Integer
        Get
            Return hdnTID.Value
        End Get
    End Property

#Region "Template Data Related"

    Sub OpenTemplate(templateId As Integer)
        'hlCalc.Visible = False
        'hlResult.Visible = False

        If templateId = -1  Then

            Return
        End If
        Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM MRA_Templates WHERE ID = " & templateId)
        If dr Is Nothing Then
            Response.Redirect("default.aspx")
            Return
        End If

        btnGenerateMRA.Visible = True
        txtTemplateName.Text = dr.GetString("Name")
	    txtFilters.Text = dr.GetString("Filters")
	    txtInputs.Text = dr.GetString("InputFields")
	    txtcomments .Text=dr.GetString("Comments")
	    ddlOutput.SelectedValue = dr.GetString("OutputField")
	    Dim nbhds As String =dr.GetString ("Groups")
	    If nbhds.Length > 0 Then
	        	Dim nbhdArray = nbhds.Split (",")
	        	For Each Item As String In nbhdArray
						Item = Item.Substring( 1 )
						cbl_Nbhd .Items .FindByValue (Item.Substring(0,Item.Length-1)).Selected = True
				Next
	    End If
        If dr.Get("LastExecutionTime") IsNot Nothing Then
           ' hlCalc.NavigateUrl = "calculator.aspx?_tid=" & templateId
            'hlResult.NavigateUrl = "resultview.aspx?_tid=" & templateId
            'hlCalc.Visible = True
            'hlResult.Visible = True
        End If

        RefreshInputs()
        RefreshFilters()
    End Sub

#End Region

#Region "Process MRA Calculations"
    Private Function getDataForRegression() As DataTable
        If ddlInput.SelectedIndex > 0 Then
            btnAddInput_Click(btnAddInput, Nothing)
        End If
        If ddlFilterField.SelectedIndex > 0 And ddlFilterOp.SelectedIndex > 0 And txtFilterValue.Text <> "" Then
            btnAddFilter_Click(btnAddFilter, Nothing)
        End If
        Dim sql As String = GenerateSql()
        lblSQL.Text = sql
        Try
            Dim dt As DataTable = Database.Tenant.GetDataTable(sql)
            For Each col As DataColumn In dt.Columns
                Dim colName = col.ColumnName
                If colName.Contains("]") Then
                    colName = colName.Replace("]", "\]")
                End If
                If (dt.Select("[" + colName + "] = '0' OR [" + colName + "] is null").Length = dt.Rows.Count) Then
                    Alert(col.ColumnName & " field does not contain enough data for regression analysis.")
                    Return Nothing
                End If
            Next
            Return dt
        Catch ex As Exception
            Alert(ex.Message)
            'pnlOutput.Visible = False
            Return Nothing
        End Try
    End Function

    Function GenerateSql() As String
        Dim joinFormat As String = " LEFT OUTER JOIN ParcelDataLookup l{0} ON (l{0}.LookupName = '{2}' AND l{0}.IdValue = pd.{1})"
        Dim auxLookupJoinFormat As String = " LEFT OUTER JOIN ParcelDataLookup l{0} ON (l{0}.LookupName = '{2}' AND l{0}.IdValue = au{3}.{1})"
        Dim auxJoinFormat As String = " LEFT OUTER JOIN XT_{1} au{0} ON (au{0}.CC_ParcelId = pd.CC_ParcelId)"
        Dim sql As String = "", joins As String = ""
        Dim sqlfilter As String = ""
        Dim joined As New List(Of String)

        sql = "SELECT "
        Dim ofield = fields.Where(Function(x) x.Id.ToString() = ddlOutput.SelectedValue).First()
        If ofield.ImportType = 1 Then
            If Not joined.Contains(ofield.AuxJoinId) Then
                joins += auxJoinFormat.FormatString(ofield.TableId, ofield.TableName)
                joined.Add(ofield.AuxJoinId)
            End If
            If ofield.InputType = 5 Then
                If Not joined.Contains(ofield.JoinId) Then
                    joins += auxLookupJoinFormat.FormatString(ofield.Id, ofield.AssignedName, ofield.LookupTable, ofield.TableId)
                    joined.Add(ofield.JoinId)
                End If
                sql += "l{0}.Value AS {1}".FormatString(ofield.Id, ofield.AssignedName.Wrap(""""))
            Else
                sql += "au" & ofield.TableId & "." + ofield.AssignedName
            End If
        Else
            sql += "pd." + ofield.AssignedName
        End If

        Dim label, fieldNameOrFormula As String
        For Each ifl In txtInputs.Text.Split(",").Select(Function(x) x.Trim)
            Dim fspec = ifl.Split("~")
            Dim ifield As String = fspec(0)
            Dim field As DataSourceField = fields.Where(Function(x) x.Id.ToString() = ifield).First()

            If fspec.Length > 1 Then
                label = fspec(1)
                fieldNameOrFormula = fspec(2)
            Else
                label = field.DisplayLabel
                fieldNameOrFormula = field.AssignedName
            End If

            Dim sourceField As String = ""

            If field.ImportType = 1 Then
                If Not joined.Contains(field.AuxJoinId) Then
                    joins += auxJoinFormat.FormatString(field.TableId, field.TableName)
                    joined.Add(field.AuxJoinId)
                End If
                If field.InputType = 5 Then
                    If Not joined.Contains(field.JoinId) Then
                        joins += auxLookupJoinFormat.FormatString(field.Id, field.AssignedName, field.LookupTable, field.TableId)
                        joined.Add(field.JoinId)
                    End If
                    sourceField += "l{0}.Value".FormatString(field.Id)
                Else
                    sourceField += "au" & field.TableId & "." + field.AssignedName
                End If
            Else
                If field.InputType = 5 Then
                    If Not joined.Contains(field.JoinId) Then
                        joins += joinFormat.FormatString(field.Id, field.AssignedName, field.LookupTable)
                        joined.Add(field.JoinId)
                    End If
                    sourceField += "l{0}.Value".FormatString(field.Id)
                Else
                    sourceField += "pd." + field.AssignedName
                End If
            End If

            sql += ", " + fieldNameOrFormula.Replace(field.AssignedName, sourceField) + " AS " + label.Wrap("""")
        Next

        If txtFilters.Text <> "" Then
            Dim filters = txtFilters.Text.Split(";").Select(Function(x) x.Split("`")).Select(Function(x) New QueryFilter With {.ID = x(0), .Field = x(1), .FieldName = x(2), .Operator = x(3), .Value = x(4)})
            For Each f In filters
                Dim qf As QueryFilter = f
                Dim field As DataSourceField = fields.Where(Function(x) x.Id.ToString() = qf.Field).First()

                Dim filterValue As String = qf.Value
                If Not IsNumeric(filterValue) AndAlso filterValue.Trim().Trim("'") <> filterValue.Trim(filterValue) Then
                    filterValue = "'" + qf.Value + "'"
                End If
                If (qf.Operator = "in") Then
                    filterValue = filterValue.TrimStart("'").TrimEnd("'")
                    filterValue = filterValue.Replace("','", ",")
                    filterValue = "('" + filterValue.Replace(",", "','") + "')"
                	End If
                 If (qf.Operator = "between") Then
                    filterValue = filterValue.TrimStart("'").TrimEnd("'")
                    filterValue = filterValue.Replace("','", ",")
                    filterValue = filterValue.Replace("and", "AND")
                    filterValue = "'" + filterValue.Replace("AND", "'AND'") + "'"
                 	End If	
                 	'If (qf.Operator = "like") Then
                    'filterValue = filterValue.TrimStart("'").TrimEnd("'")
                    'filterValue = " '%" + filterValue + "%'"
                	'End If
                Dim lookupFilterFormat = " AND (l{0}.Value {1} {2} OR (l{0}.Value = 0 AND l{0}.IdValue {1} {2}))"
                Dim lookupTextFilterFormat = " AND (l{0}.Value = 0 AND l{0}.IdValue {1} {2})"

                If Not IsNumeric(filterValue) Then
                    lookupFilterFormat = lookupTextFilterFormat
                End If

                If field.ImportType = 1 Then
                    If Not joined.Contains(field.AuxJoinId) Then
                        joins += auxJoinFormat.FormatString(field.TableId, field.TableName)
                        joined.Add(field.AuxJoinId)
                    End If
                    If field.InputType = 5 Then
                        If Not joined.Contains(field.JoinId) Then
                            joins += auxLookupJoinFormat.FormatString(field.Id, field.AssignedName, field.LookupTable, field.TableId)
                            joined.Add(field.JoinId)
                        End If
                        sqlfilter += lookupFilterFormat.FormatString(field.Id, qf.Operator, filterValue)
                    Else
                        sqlfilter += " AND au" & field.TableId & ".[" + field.AssignedName + "] " + qf.Operator + " "+ filterValue
                    End If
                Else
                    If field.InputType = 5 Then
                        If Not joined.Contains(field.JoinId) Then
                            joins += joinFormat.FormatString(field.Id, field.AssignedName, field.LookupTable)
                            joined.Add(field.JoinId)
                        End If
                        sqlfilter += lookupFilterFormat.FormatString(field.Id, qf.Operator, filterValue)
                        'sqlfilter += " AND l" & field.Id & ".Value" + " " + qf.Operator + filterValue
                    Else If field.InputType = 1 Then
                    	sqlfilter += " AND pd.[" + field.AssignedName + "] " + qf.Operator + "'" + filterValue + "'"
					Else                    		
                        sqlfilter += " AND pd.[" + field.AssignedName + "] " + qf.Operator + filterValue
                    End If
                End If
            Next
        End If

        If sql.Length > 7 Then
            Dim keyField1 As String = Database.Tenant.Application.KeyField(1)
            Dim checkLabel = fields.Where(Function(x) x.AssignedName = keyField1 And x.AssignedName <> x.DisplayLabel)
            If checkLabel.Count > 0 Then
                keyField1 = checkLabel.First.DisplayLabel
            End If
            sql += ", p.KeyValue1 As """ + keyField1 + """"
        End If

        sql += ", pd.CC_ParcelId"

        sql += " FROM ParcelData pd INNER JOIN Parcel p ON pd.CC_ParcelId = p.Id " + joins
        Dim Nbhd As New ArrayList
        For Each item As ListItem In cbl_Nbhd.Items
            If item.Selected Then
                Nbhd.Add("'" + item.Value + "'")
            End If
        Next
        
        If Nbhd.Count = 0 Then
            sql += " WHERE 1 = 1" + sqlfilter
        Else
            sql += " WHERE pd.CC_ParcelId IN (SELECT Id FROM Parcel WHERE NeighborhoodId in (" + Strings.Join(Nbhd.ToArray, ",") + "))" + sqlfilter
        End If
        Return sql
    End Function

    'Function EvaluateRegression(data As DataTable, Optional constantIsZero As Boolean = False) As Boolean
    '    Dim ch As New Chart
    '    Dim stats As StatisticFormula = ch.DataManipulator.Statistics

    '    Dim rowCount As Integer = data.Rows.Count

    '    Dim N As Integer = data.Columns.Count - 3
    '    Dim itemLabelIndex As Integer = data.Columns.Count - 2
    '    Dim y(rowCount - 1) As Double
    '    Dim x(N, rowCount - 1) As Double
    '    Dim w(rowCount - 1) As Double

    '    If constantIsZero Then
    '        ReDim x(N - 1, rowCount - 1)
    '    End If

    '    For i As Integer = 0 To rowCount - 1
    '        If constantIsZero Then
    '            For j As Integer = 1 To N
    '                x(j - 1, i) = data(i).Get(j)
    '            Next
    '        Else
    '            x(0, i) = 1
    '            For j As Integer = 1 To N
    '                x(j, i) = data(i).Get(j)
    '            Next
    '        End If


    '        y(i) = data(i).Get(0)
    '        w(i) = 1
    '    Next



    '    Dim lr As New Statistics.LinearRegression
    '    If Not lr.Regress(y, x, w) Then
    '        Return False
    '    End If

    '    'Calculate Overall Mean of Y
    '    Dim totalY As Double = 0, overallMean As Double
    '    For i As Integer = 0 To rowCount - 1
    '        totalY += y(i)
    '    Next
    '    overallMean = totalY / rowCount
    '    Dim ssreg As Double = 0
    '    Dim ssres As Double = 0
    '    For i As Integer = 0 To rowCount - 1
    '        If constantIsZero Then
    '            ssreg += Math.Pow(lr.CalculatedValues(i), 2)
    '        Else
    '            ssreg += Math.Pow(overallMean - lr.CalculatedValues(i), 2)
    '        End If

    '        ssres += Math.Pow(lr.Residuals(i), 2)
    '    Next

    '    Dim dfreg, dfres As Integer
    '    Dim msreg, msres As Double
    '    Dim rsquare As Double, adjustedR As Double
    '    Dim multipleR, standerror As Double
    '    Dim regf As Double, signf As Double

    '    dfreg = N
    '    dfres = rowCount - lr.Coefficients.Count
    '    msreg = ssreg / dfreg
    '    msres = ssres / dfres
    '    rsquare = ssreg / (ssreg + ssres)
    '    multipleR = Math.Sqrt(rsquare)
    '    standerror = Math.Sqrt(msres)
    '    regf = msreg / msres
    '    adjustedR = (1 - ((dfreg + dfres) / dfres) * (ssres / (ssres + ssreg)))
    '    signf = stats.FDistribution(msreg / msres, dfreg, dfres)

    '    'lblRSquare.Text = rsquare.ToString("0.########")
    '    'lblMultipleR.Text = multipleR.ToString("0.########")
    '    'lblStandardError.Text = standerror.ToString("0.########")
    '    'lblObservations.Text = rowCount
    '    'lblAdjustedRSquare.Text = adjustedR.ToString("0.########")

    '    'lblSSReg.Text = ssreg.ToString("0.###")
    '    'lblMSReg.Text = msreg.ToString("0.###")
    '    'lblSSRes.Text = ssres.ToString("0.###")
    '    'lblMSRes.Text = msres.ToString("0.###")
    '    'lblDFReg.Text = dfreg
    '    'lblDFRes.Text = dfres
    '    'lblFisher.Text = regf.ToString("0.###")
    '    'lblSignificanceF.Text = signf.ToString("E4")


    '    'lblDFTotal.Text = rowCount - lr.Coefficients.Count + N
    '    'lblSSTotal.Text = (ssreg + ssres).ToString("0.###")

    '    Dim sqlSaveResult As String = "DELETE FROM MRA_Result WHERE TemplateID = {0}; INSERT INTO MRA_Result (TemplateID, Observations, RSquare, MultipleR, AdjustedRSquare, StandardError, RegressionMeanSquare, ResidualMeanSquare, RegressionSumOfSquares, ResidualSumOfSquares, RegressionDegreeOfFreedom, ResidualDegreeOfFreedom, RegressionF, SignificantF) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13});"
    '    sqlSaveResult = sqlSaveResult.SqlFormat(False, TemplateID, rowCount, rsquare, multipleR, adjustedR, standerror, msreg, msres, ssreg, ssres, dfreg, dfres, regf, signf)
    '    Database.Tenant.Execute(sqlSaveResult)

    '    'Dim reg As New DataTable
    '    'reg.Columns.Add("Head")
    '    'reg.Columns.Add("CE")
    '    'reg.Columns.Add("SE")
    '    'reg.Columns.Add("TS")
    '    'reg.Columns.Add("PV")

    '    Dim fieldSpecs = Database.Tenant.GetStringValue("SELECT InputFields FROM MRA_Templates WHERE ID = " & TemplateID)
    '    Dim inputFields = InputField.FromSpecString(fieldSpecs, fields)


    '    Dim sqlSaveCoEffs As String = "DELETE FROM MRA_Coefficients WHERE TemplateID = " & TemplateID & "; "

    '    For i As Integer = 0 To lr.C.Length - 1
    '        'Dim rr As DataRow = reg.NewRow
    '        Dim head As String
    '        If constantIsZero Then
    '            head = data.Columns(i + 1).ColumnName
    '        Else
    '            If i = 0 Then
    '                head = "Intercept"
    '            Else
    '                head = data.Columns(i).ColumnName
    '            End If
    '        End If


    '        Dim c As Double = lr.Coefficients(i)
    '        Dim se As Double = lr.CoefficientsStandardError(i)
    '        Dim tstat = c / se
    '        Dim pval = stats.TDistribution(Math.Abs(tstat), dfres, False)
    '        'rr("Head") = head
    '        'rr("CE") = c.ToString("0.########")
    '        'rr("SE") = se.ToString("0.########")
    '        'rr("TS") = tstat.ToString("0.########")
    '        'rr("PV") = pval.ToString("0.########")
    '        'reg.Rows.Add(rr)

    '        Dim ci = If(constantIsZero, i + 1, i)
    '        Dim fieldId As Integer = -1
    '        Dim fieldName As String = "Intercept"
    '        Dim fieldSpec As String = ""
    '        If ci > 0 Then
    '            Dim f = inputFields(ci)
    '            fieldId = f.FieldID
    '            fieldName = f.Label
    '            fieldSpec = f.Expression
    '        End If

    '        Dim sqlInsert As String = "INSERT INTO MRA_Coefficients (TemplateID, Ordinal, FieldId, Head, FieldSpecification, Coefficient, StandardError, tStat, pValue) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}); ".SqlFormat(False, TemplateID, ci, fieldId, head, fieldSpec, c, se, tstat, pval)
    '        sqlSaveCoEffs += sqlInsert
    '    Next

    '    Database.Tenant.Execute(sqlSaveCoEffs)

    '    'Dim resd As New DataTable
    '    'resd.Columns.Add("ID")
    '    'resd.Columns.Add("Item")
    '    'resd.Columns.Add("ItemId")
    '    'resd.Columns.Add("PR")
    '    'resd.Columns.Add("RES")

    '    Dim sqlSaveResiduals As String = "DELETE FROM MRA_ResidualOutput WHERE TemplateID = {0}; ".FormatString(TemplateID)



    '    Dim sqlResidualInserts As New List(Of String)
    '    For i As Integer = 0 To lr.CalculatedValues.Length - 1
    '        'Dim rr As DataRow = resd.NewRow

    '        Dim parcelId As Integer = data.Rows(i)("CC_ParcelId")
    '        Dim pr As Double = lr.CalculatedValues(i)
    '        Dim res As Double = -lr.Residuals(i)
    '        'rr("ID") = i + 1
    '        'rr("Item") = data.Rows(i)(itemLabelIndex)
    '        'rr("ItemId") = parcelId
    '        'rr("PR") = pr.ToString("0.########")
    '        'rr("RES") = (res).ToString("0.########")
    '        'resd.Rows.Add(rr)

    '        Dim sqlInsert As String = "INSERT INTO MRA_ResidualOutput (TemplateID, ObservationNo, ParcelId, PredictedValue, Residual) VALUES ({0}, {1}, {2}, {3}, {4});".SqlFormat(False, TemplateID, i + 1, parcelId, pr, res)
    '        sqlResidualInserts.Add(sqlInsert)
    '    Next

    '    Database.Tenant.Execute(sqlSaveResiduals)
    '    Dim stepSize As Integer = 1000
    '    For i As Integer = 0 To lr.CalculatedValues.Length - 1 Step stepSize
    '        Dim batchSql As String = sqlResidualInserts.Skip(i).Take(stepSize).Aggregate(Function(s1, s2) s1 + vbNewLine + s2)
    '        Database.Tenant.Execute(batchSql)
    '    Next


    '    'residuals.Columns(1).HeaderText = data.Columns(itemLabelIndex).ColumnName
    '    'residuals.Columns(2).HeaderText = "Predicted " + data.Columns(0).ColumnName



    '    'regresult.DataSource = reg
    '    'regresult.DataBind()

    '    'residuals.DataSource = resd
    '    'residuals.DataBind()

    '    Dim costFormula As String = ""
    '    Dim inlineQueryForSql As String = "SELECT 0 As __T"
    '    Dim modelParam As String = ddlOutput.SelectedValue
    '    Dim pi As Integer = -1, fn As Integer = 0
    '    If Not constantIsZero Then
    '        pi += 1
    '        modelParam += ";" & lr.Coefficients(pi)
    '        costFormula += lr.Coefficients(pi).ToString("0.###")
    '    End If
    '    If txtInputs.Text <> "" Then
    '        For Each fi In txtInputs.Text.Split(",")
    '            pi += 1
    '            fn += 1
    '            modelParam += ";" + fi + "|" & lr.Coefficients(pi)

    '            Dim fld = inputFields(fn)
    '            If costFormula <> "" Then costFormula += " + "
    '            Dim coeff = Math.Round(lr.Coefficients(pi), 3)
    '            costFormula += If(coeff < 0, "({0})", "{0}").FormatString(coeff) + " * " + If(fld.Expression.Contains(" "), "({0})", "{0}").FormatString(fld.Expression)
    '            inlineQueryForSql += ", CAST(@{0} AS FLOAT) AS {0}".FormatString(fields.Where(Function(xf) xf.Id = fld.FieldID).First.AssignedName)
    '        Next
    '    End If

    '    Dim costFormulaSql As String = "SELECT " + costFormula + " FROM (" + inlineQueryForSql + ") x"

    '    Database.Tenant.Execute("UPDATE MRA_Templates SET CostFormula = {1}, CostFormulaSQL = {2}, LastExecutionTime = GETUTCDATE() WHERE ID = {0}".SqlFormat(True, TemplateID, costFormula, costFormulaSql))

    '    'Dim modelUrl As String = "javascript:window.open('popup.model.aspx?params=" + modelParam + "', 'popup', 'width=600,height=500,location=no,toolbar=no');"
    '    'hlOpenLink.NavigateUrl = modelUrl

    '    Return True
    'End Function

#End Region

#Region "Page Events"

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
    	fields = DataSourceField.GetFields
    End Sub
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
    	mradesignView = New MRA_NavigationControl ()
    	AddHandler mradesignView.ddlchanged, AddressOf Me.NavControl_ddlmodel_design 
    	If Not IsPostBack Then
			ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "changeSelectedField();", True)
       		If Request.QueryString("_tid") IsNot Nothing AndAlso IsNumeric(Request.QueryString("_tid")) Then
       			hdnTID.Value = Integer.Parse(Request.QueryString("_tid"))
       			'main_content.Visible=True
        	Else
        		mra_nav.Visible =False
            	'main_content.Visible=True
        	End If
        	
          ddlNbhd.InsertItem("All NHBD", "0", 0)
          cbl_Nbhd.DataSource = Database.Tenant.GetDataTable("SELECT Id, Number FROM Neighborhood ORDER BY Number")
          cbl_Nbhd.DataTextField = "Number"
          cbl_Nbhd.DataValueField = "Id"
          cbl_Nbhd.DataBind()
          ddlOutput.FillFromSql(GetFieldSql(, "7,9"), True)
          ddlInput.FillFromSql(GetFieldSql(), True)
          ddlFilterField.FillFromSql(GetFieldSql(, "1,2,4,5,7,8,10"), True)
          OpenTemplate(TemplateID)
          LoadFilterFields()
    	Else
			'NavControl_ddlmodel_design(Nothing ,Nothing)
    	End If
    End Sub
    
    
    
    Sub LoadFilterFields()

	'Dim dt As DataTable = Database.Tenant.GetDataTable("SELECT DISTINCT LookupName FROM ParcelDataLookup pdl INNER JOIN  DataSourceField df on pdl.LookupName = df.LookupTable WHERE Df.Id IN (SELECT FieldId FROM MRA_Filters) order by pdl.LookupName")
	Dim dt As DataTable = Database.Tenant.GetDataTable("SELECT Id,DisplayLabel FROM DataSourceField WHERE Id IN(SELECT FieldId FROM MRA_Filters WHERE IsExportField = 0)")
	rpFilterFields.DataSource = dt
	rpFilterFields.DataBind() 
    
	End Sub   

		Protected Sub rpFilterFields_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles rpFilterFields.ItemDataBound
		    If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
		    	Dim lb As ListBox = CType(e.Item.FindControl("lbFilterFields"), ListBox)
		    	Dim lbl As Label = CType(e.Item.FindControl("lblFieldId"), Label)
		        If lb IsNot Nothing Then
		        	'lb.DataSource = Database.Tenant.GetDataTable("SELECT IdValue,DescValue FROM ParcelDataLookup WHERE LookupName = {0}".SqlFormatString(lbl.Text.ToString()))
		        	Dim lookupTable = Database.Tenant.GetStringValue("SELECT LookupTable FROM DataSourceField WHERE Id ="&lbl.Text)
		        	lb.DataSource = Database.Tenant.GetDataTable("SELECT IdValue,DescValue FROM ParcelDataLookup WHERE LookupName = {0}".SqlFormatString(lookupTable))
		            lb.DataBind()
		        End If
		    End If
		End Sub
    
    
    Public Overrides Sub VerifyRenderingInServerForm(control As System.Web.UI.Control)
        'If control Is pnlResidual Or control Is residuals Then
        '    Return
        'End If
        MyBase.VerifyRenderingInServerForm(control)
    End Sub
    
    
     
#End Region

#Region "UI Data Methods"

    Function GetFieldSql(Optional filter As String = "", Optional types As String = "2, 5, 7, 8, 9, 10") As String
        If filter <> "" Then
            filter = " AND " + filter
        End If
        If types <> "" Then
            filter += " AND DataType IN (" + types + ")"
        End If
        Return "SELECT Id, DisplayLabel + ' (' + SourceTable + ')' As DisplayLabel FROM DataSourceField WHERE CategoryId IS NOT NULL" + filter + " ORDER BY DisplayLabel"
    End Function

    Sub RefreshInputs()
        'Handled
        Dim selectedFields As String = txtInputs.Text
        If selectedFields = "" Then
            selectedFields = 0
        Else
        	selectedFields = String.Join(",", txtInputs.Text.Split(",").Select(Function(x) IIf(IsNumeric(x), x, x.Split("~")(0))).ToArray())
        End If
        ddlInput.FillFromSql(GetFieldSql("Id NOT IN (" + selectedFields + ")"), True)
        Dim inputs As New Collection
        For Each iss In txtInputs.Text.Split(",")
            Dim fid As String = iss.Split("~")(0)
            If fid = "" Then
                Continue For
            End If
            Dim label, spec As String
            label = Nothing
            spec = Nothing
            If iss.Contains("~") Then
                label = iss.Split("~")(1)
                spec = iss.Split("~")(2)
            End If
            Try
                Dim field = fields.First(Function(x) x.Id.ToString = fid.Trim)
                Dim f As Object = New With {.Id = fid, .DisplayLabel = IIf(spec Is Nothing, field.DisplayLabel, spec + " (" + label + ")")}
                inputs.Add(f)
            Catch ex As Exception
                Throw New Exception(fid)
            End Try

        Next
        rpInputs.DataSource = inputs 'Database.Tenant.GetDataTable(GetFieldSql("Id IN (" + selectedFields + ")"))
        rpInputs.DataBind()

        txtLabel.Text = ""
        txtSpec.Text = ""
    End Sub

    Sub RefreshFilters()
        If txtFilters.Text <> "" Then
            Dim filters = txtFilters.Text.Split(";").Select(Function(x) x.Split("`")).Select(Function(x) New With {.ID = x(0), .Field = x(1), .FieldName = x(2), .Operator = x(3), .Value = x(4)})
            rpFilters.DataSource = filters
        Else
            rpFilters.DataSource = Nothing
        End If


        rpFilters.DataBind()
    End Sub
#End Region

#Region "Process Triggers"
    Protected Sub btnGenerateMRA_Click(sender As Object, e As System.EventArgs) Handles btnGenerateMRA.Click

        Dim dt As DataTable = getDataForRegression()
        If dt Is Nothing Then
            Exit Sub
        End If

        If dt.Rows.Count > 50000 Then
            Alert("The input data parameters result in a sample size that contains more than 50,000 observations. Regression Analysis cannot be performed. Add or adjust filters to reduce the sample size appropriately.")
            'pnlOutput.Visible = False
            Exit Sub
        End If

        If Not _saveTemplate() Then
            'pnlOutput.Visible = False
            Exit Sub
        End If

        Try
            If Not RegressionAnalyzer.EvaluateMultipleRegression(TemplateID, dt, chkConstantIsZero.Checked) Then
                Alert("Regression Analysis cannot be performed. Input/output data insufficient to perform the calculation." + vbNewLine + vbNewLine + "Example: a sample may be available, but there is invalid data (null values not filtered out, missing linearized numeric values in code files, or no correlation found).")
                ' pnlOutput.Visible = False
            Else
                Response.Redirect("resultview.aspx?_tid=" & TemplateID)
                'pnlOutput.Visible = True
            End If
        Catch exi As InvalidOperationException
            Alert("Operation failed due to invalid parameters selected.")
            'pnlOutput.Visible = False
        Catch ex As Exception
            Alert(ex.Message)
            'pnlOutput.Visible = False
        End Try
    End Sub
    Private Sub btnTest_Click(sender As Object, e As System.EventArgs) Handles btnTest.Click
        Dim dt As DataTable = getDataForRegression()
        If dt IsNot Nothing Then
            Dim message As String = dt.Rows.Count & " parcels available for regression analysis."
            Alert(message)
        End If
    End Sub
    'Protected Sub lbExport_Click(sender As Object, e As System.EventArgs) Handles lbExport.Click
    '    Dim sb As New StringBuilder
    '    Dim tw As New StringWriter(sb)
    '    Dim hw As New HtmlTextWriter(tw)
    '    pnlResidual.Visible = True
    '    residuals.RenderControl(hw)
    '    Dim html As String = sb.ToString
    '    pnlResidual.Visible = False

    '    Response.Clear()
    '    Response.ContentType = "application/vnd.ms-excel"
    '    Response.AddHeader("Content-Disposition", "attachment;filename=residuals.xls")
    '    Response.Write(html)
    '    Response.End()
    'End Sub

#End Region

#Region "Fields & Filters - Control Management"

Protected Sub btnAddInput_Click(sender As Object, e As System.EventArgs) Handles btnAddInput.Click
        Dim inputs = InputField.FromInputString(txtInputs.Text)
        Dim input As String = ddlInput.SelectedValue + "~" + txtLabel.Text + "~" + txtSpec.Text
        Dim dict1 = inputs.ToDictionary(Of String)(Function(x) x.ID)
        Dim ip As New InputField(input)
        If (txtInputs.Text) <> "" Then
            txtInputs.Text += ","
        End If

'Handled
		If hdnInputID.Value = "" Then
			hdnInputID.Value = ddlInput.SelectedValue
		End If
		 dict1(hdnInputID.Value) = ip
        txtInputs.Text = InputField.ToInputString(dict1.Values.ToList)
'        Dim field As DataSourceField = fields.First(Function(x) x.Id.ToString = ddlInput.SelectedValue)
'        If txtLabel.Text = txtSpec.Text And txtLabel.Text = field.DisplayLabel Then
'            txtInputs.Text += ddlInput.SelectedValue
'        Else
'            txtInputs.Text += ddlInput.SelectedValue + "~" + txtLabel.Text + "~" + txtSpec.Text
'        End If

        RefreshInputs()
        hdnInputID.Value = ""
        ddlInput.SelectedIndex = 0
        txtLabel.Text = ""
        txtSpec.Text = ""
        Page.RunScript("dialogSaved();")
End Sub

    Protected Sub rpInputs_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rpInputs.ItemCommand
        If e.CommandName = "RemoveX" Then
            Dim ids = txtInputs.Text.Split(",").Select(Function(x) x.Trim).ToList()
            Dim toDelete = ids.First(Function(x) x = e.CommandArgument OrElse (Not IsNumeric(x) AndAlso x.Split("~")(0) = e.CommandArgument))
            ids.Remove(toDelete)
            txtInputs.Text = String.Join(",", ids.ToArray())
            RefreshInputs()
        End If
        If e.CommandName = "EditX" Then
			   Dim inputs = InputField.FromInputString(txtInputs.Text)
	           Dim selectedInput As InputField = Nothing
	           For Each i In inputs
                If i.ID = e.CommandArgument Then
                    selectedInput = i
                End If
	           Next
               If selectedInput IsNot Nothing Then
        	   		ddlInput.FillFromSql(GetFieldSql(), True)
	                hdnInputID.Value = selectedInput.ID
	                ddlInput.SelectedValue = selectedInput.ID
	                txtlabel.Text = selectedInput.DisplayLabel
                	txtSpec.Text = selectedInput.FieldName
					RunScript("addInputs();")
               End If
        End If
    End Sub
    Protected Sub btnAddFilter_Click(sender As Object, e As System.EventArgs) Handles btnAddFilter.Click
    	Dim filters = QueryFilter.FromListString(txtFilters.Text)
        Dim filter As String = Guid.NewGuid.ToString() + "`" + ddlFilterField.SelectedValue + "`" + ddlFilterField.SelectedItem.Text + "`" + ddlFilterOp.SelectedValue + "`" + txtFilterValue.Text + "`" 
        Dim dict = filters.ToDictionary(Of String)(Function(x) x.ID)
        Dim qf As New QueryFilter(filter)
        If hdnFilterID.Value = "" Then
            hdnFilterID.Value = Guid.NewGuid.ToString
        End If
        dict(hdnFilterID.Value) = qf


        txtFilters.Text = QueryFilter.ToListString(dict.Values.ToList)
        RefreshFilters()

        ddlFilterField.SelectedIndex = 0
        ddlFilterOp.SelectedIndex = 0
        txtFilterValue.Text = ""
        hdnFilterID.Value = ""

        Page.RunScript("dialogSaved();")
    End Sub
    Protected Sub rpFilters_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rpFilters.ItemCommand
        If e.CommandName = "RemoveX" Then
    		pnlWaitMessage.Visible = True
            Dim filters = QueryFilter.FromListString(txtFilters.Text)
            Dim selectedFilter As QueryFilter = Nothing
            For Each f In filters
                If f.ID = e.CommandArgument Then
                    selectedFilter = f
                End If
            Next
            If selectedFilter IsNot Nothing Then
                filters.Remove(selectedFilter)
            End If
            txtFilters.Text = QueryFilter.ToListString(filters)
            RefreshFilters()
            pnlWaitMessage.Visible = False
        End If
        If e.CommandName = "EditX" Then
        	pnlWaitMessage.Visible = True
            Dim filters = QueryFilter.FromListString(txtFilters.Text)
            Dim selectedFilter As QueryFilter = Nothing
            For Each f In filters
                If f.ID = e.CommandArgument Then
                    selectedFilter = f
                End If
            Next
            If selectedFilter IsNot Nothing Then
                hdnFilterID.Value = selectedFilter.ID
                ddlFilterField.SelectedValue = selectedFilter.Field
                ddlFilterOp.SelectedValue = selectedFilter.Operator
                txtFilterValue.Text = selectedFilter.Value

                RunScript("editFilter();")
    			pnlWaitMessage.Visible = False
            End If
        End If
        End Sub
        
 
    	
    Protected Sub ddlInput_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlInput.SelectedIndexChanged
        If ddlInput.SelectedIndex = 0 Then
            txtLabel.Text = ""
            txtSpec.Text = ""
        Else
            Dim field As DataSourceField = fields.First(Function(x) x.Id.ToString = ddlInput.SelectedValue)
            txtLabel.Text = field.DisplayLabel
            txtSpec.Text = field.AssignedName
        End If
    End Sub
    Protected Sub ddlFilterField_SelectedIndexChanged(sender As Object,e As System.EventArgs) Handles ddlFilterField.SelectedIndexChanged
    	If ddlFilterField.SelectedValue <> ""
    		Dim field As DataSourceField = fields.First(Function(x) x.Id.ToString = ddlFilterField.SelectedValue)
	    	If field.InputType = 1 Then
				ddlFilterOp.Items.FindByValue(">").Enabled = False
				ddlFilterOp.Items.FindByValue("<").Enabled = False
				ddlFilterOp.Items.FindByValue(">=").Enabled = False
				ddlFilterOp.Items.FindByValue("<=").Enabled = False
				ddlFilterOp.Items.FindByValue("between").Enabled = False   
				ddlFilterOp.Items.FindByValue("like").Enabled = True
	    	Else
	    		ddlFilterOp.Items.FindByValue(">").Enabled = True
				ddlFilterOp.Items.FindByValue("<").Enabled = True
				ddlFilterOp.Items.FindByValue(">=").Enabled = True
				ddlFilterOp.Items.FindByValue("<=").Enabled = True
				ddlFilterOp.Items.FindByValue("between").Enabled = True
				ddlFilterOp.Items.FindByValue("like").Enabled = False
	    	End If
    	End If	
    End Sub
    Protected Sub addNewIp_Click(sender As Object,e As EventArgs) Handles addNewIp.Click
    	RunScript("addInputs();return false;")
    End Sub
#End Region


    Private Function _saveTemplate() As Boolean
        Dim fldO As String = ddlOutput.SelectedValue
        Dim fldI As String = txtInputs.Text
        Dim fldF As String = txtFilters.Text

        If fldO = "" Or fldI = "" Then
            Alert("Template cannot be saved without sufficient input/output fields.")
            Return False
        End If

        If txtTemplateName.Text.Trim = "" Then
            Alert("Required: Template Name")
            Return False
        End If
		
        Dim poolSize As Integer = 0
        Dim dt As DataTable = getDataForRegression()
        If dt IsNot Nothing Then
            poolSize = dt.Rows.Count
        End If
        
        Dim nbhdList As New ArrayList 
        For Each item As ListItem In cbl_Nbhd.Items
            If item.Selected Then
                nbhdList.Add("'" + item.Value + "'")
            End If
        Next
        Dim nbhdIds = ""
        If nbhdList.Count > 0 Then
            nbhdIds = Strings.Join(nbhdList.ToArray, ",")
        End If
        
        If TemplateID <= 0 Then
            hdnTID.Value = Database.Tenant.GetIntegerValue("INSERT INTO MRA_Templates (Name, CreatedBy, CreatedDate) VALUES ({0}, {1}, GETUTCDATE()); SELECT CAST(@@IDENTITY AS INT) As NewId;".SqlFormat(False, txtTemplateName, Page.UserName))
        End If

        Dim updateSql As String = "UPDATE MRA_Templates SET Name = {1}, OutputField = {2}, InputFields = {3}, Filters = {4}, CreatedBy = {5}, CreatedDate = GETUTCDATE(), PoolSize = {6},Groups={7},Comments={8} WHERE ID = {0}".SqlFormat(False, TemplateID, txtTemplateName, ddlOutput, txtInputs, txtFilters, Page.UserName, poolSize,nbhdIds,txtcomments)
        Database.Tenant.Execute(updateSql)

        Database.Tenant.Execute("DELETE FROM MRA_Result WHERE TemplateID = {0}; DELETE FROM MRA_Coefficients WHERE TemplateID = {0}; DELETE FROM MRA_ResidualOutput WHERE TemplateID = {0}; ".FormatString(TemplateID))

        Return True
    End Function

    'Private Sub btnSaveTemplate_Click(sender As Object, e As EventArgs) Handles btnSaveTemplate.Click

    '    _saveTemplate()
    '    'Response.Redirect("default.aspx")

    'End Sub

    'Private Sub btnDeleteTemplate_Click(sender As Object, e As System.EventArgs) Handles btnDeleteTemplate.Click
    '    Database.Tenant.Execute("DELETE FROM MRA_Templates WHERE Id = " & ddlTemplate.SelectedValue)
    '    RefreshTemplates()

    '    txtInputs.Text = ""
    '    txtFilters.Text = ""
    '    RefreshFilters()
    '    RefreshInputs()
    '    ddlOutput.SelectedIndex = 0
    'End Sub
    
    Protected Sub NavControl_ddlmodel_design(sender As Object, e As EventArgs) Handles mradesignView.ddlchanged 	
    	If (HttpContext.Current.Request.Url.AbsolutePath.Contains("design")) Then
    		If MRA_NavigationControl.__mraDDLItem  = 0 Then
    			pnlInput.Visible =False
    		Else
    			If hdnTID.Value <> MRA_NavigationControl.__mraDDLItem Then
    				hdnTID.Value = MRA_NavigationControl.__mraDDLItem
        		    OpenTemplate(MRA_NavigationControl.__mraDDLItem)
    			End If
        		pnlInput.Visible = True
        		'main_content.Visible=True
        		'HttpContext.Current.Response.Redirect("design.aspx?_tid=" & MRA_NavigationControl.__mraDDLItem)
        End If
    	End If
    	RemoveHandler mradesignView.ddlchanged, AddressOf Me.NavControl_ddlmodel_design 
    End Sub
End Class
