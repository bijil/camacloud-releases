﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/DesktopWeb.master" AutoEventWireup="false" Inherits="CAMACloud.Console.mra_analyze" Codebehind="analyze.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type ="text/javascript">
        var expanded = false;
        $(document).on('click',function(e){
           if ( $( e.target ).parents( '.cbl_show' ).length == 0 )
              $( '#checkboxes' ).hide();
           if ( $( e.target ).parents( '.cbl_show1' ).length == 0 )
              $( '#checkboxes1' ).hide();
        })
        function showRegion(){
        $('.secRegion').show();
        $( '#checkboxes1' ).hide();
        }
        
        
         function showCheckboxes1()
        {
            var checkboxes1 = document.getElementById( "checkboxes1" );
            if ( !expanded)
            {
                checkboxes1.style.display = "block";
                expanded = true;
                $( '.secRegion .cbl_show1 input[type="checkbox"]' ).unbind( 'click' )
                $('.secRegion .cbl_show1 input[type="checkbox"]').bind('click', function (e) {
                    var t = $( '#checkboxes1 input:checked' ).length;
                    if ( t == 0 )
                        $( " .secRegion .selectBox select option[value='0']" ).text( 'All Regions' )
                    else
                        $( ".secRegion .selectBox select option[value='0']" ).text( t + ' selected' )
                } )
            } else
            { 
                checkboxes1.style.display = "none";
                expanded = false;
            }
        }
        
        
        function showCheckboxes()
        {
            var checkboxes = document.getElementById( "checkboxes" );
            if ( !expanded)
            {
                checkboxes.style.display = "block";
                expanded = true;
                $( '.cbl_show input[type="checkbox"]' ).unbind( 'click' )
                $('.cbl_show input[type="checkbox"]').bind('click', function (e) {
                    var t = $( '#checkboxes input:checked' ).length;
                    if ( t == 0 )
                        $( ".secNbhd .selectBox select option[value='0']" ).text( 'All NBHDs' )
                    else
                        $( ".secNbhd .selectBox select option[value='0']" ).text( t + ' selected' )
                } )
            } else
            {
                checkboxes.style.display = "none";
                expanded = false;
            }
        }
        function hide_select()
        {
            var checkboxes = document.getElementById( "checkboxes" );
        }
        $(function () {
            $('.mraFilterOp').change(function (e) {
                if ($(this).val() == 'in') {
                    $('.tip').attr('abbr', "Please insert the values as comma separated in case of using 'In' filter operator");
                }
                else {
                    $('.tip').attr('abbr', '');
                }
            } )
            var t = $( '#checkboxes input:checked' ).length;
            if(t>0)
                $( ".selectBox select option[value='0']" ).text( t + ' selected' )
        });
    </script>
    <style type="text/css">
        .reg-stat
        {
        }
        
        .reg-stat td
        {
            width: 120px;
            text-align: right;
            padding-right: 15px;
        }
        
        .reg-stat td:first-child
        {
            width: 120px;
            text-align: left;
        }
        
        .anova
        {
        }
        
        .anova td
        {
            text-align: right;
        }
        
        .anova td:first-child
        {
            text-align: left;
        }
        
        .th-right
        {
            text-align: right !important;
        }
        
        .selected-input
        {
            display: inline-block;
            background: LightBlue;
            border: 1px solid Navy;
            margin: 3px;
            margin-right: 8px;
            padding: 2px 5px;
            border-radius: 4px;
        }
        
        .selected-input span
        {
            font-weight: bold;
            margin: 0px 6px;
        }
        
        .selected-input a
        {
            color: Red;
            margin-right: 4px;
            font-size: smaller;
            text-decoration: none;
        }
        
        .filter-statement
        {
        	display: inline-block;
            background: #0a6ec454;
            border: 1px solid Navy;
            margin: 3px;
            margin-right: 8px;
            padding: 2px 5px;
            border-radius: 4px;
        }
        
        .filter-statement span
        {
            font-weight: bold;
            margin: 0px 6px;
        }
        
        .filter-statement a
        {
            color: Red;
            margin-right: 4px;
            font-size: smaller;
            text-decoration: none;
        }
                .multiselect {
          width: 200px;
        }

.selectBox {
  position: relative;
   
}



.overSelect {
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
}

#checkboxes {
  display: none;
  border: 1px #dadada solid;
     height: 200px;
    overflow: auto;
    width: 239px;
    background: white;
}
#checkboxes1 {
  display: none;
  border: 1px #dadada solid;
     height: 200px;
    overflow: auto;
    width: 239px;
    background: white;
}


    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <h1>
        Multiple Regression Analysis</h1>
    <asp:Panel runat="server" ID="pnlInput">
        <h2>
            Select Data for Analysis</h2>
        <table>
            <tr runat="server" id="trSelectTemplate">
                <td style="width: 160px;">
                    Select Template:
                </td>
                <td>
                    <span class="tip" abbr="Select the previously saved templates" ><asp:DropDownList runat="server" ID="ddlTemplate" Width="240px" AutoPostBack="true" /></span>
                    <asp:TextBox runat="server" ID="txtNewTemplateName" Width="200px" MaxLength="50" />
                    <asp:RequiredFieldValidator class = "nameValidate" runat="server" ControlToValidate="txtNewTemplateName" ErrorMessage="*" ValidationGroup="SaveTemplate" />
                    <asp:Button runat="server" ID="btnSaveTemplate" Text="Save" ValidationGroup="SaveTemplate" />
                    <asp:Button runat="server" ID="btnDeleteTemplate" Text="Delete" />
                </td>
            </tr>
            <tr class = "secNbhd">
                <td style="width: 160px;">
                    Select Neighborhood:
                </td>
                <td class="cbl_show">
                    <div class="selectBox tip" onclick="showCheckboxes()" style = "width : 240px;" abbr="Select the neighborhoods from the drop-down list">
                    <asp:DropDownList runat="server" ID="ddlNbhd" Width="240px" />
                         <div class="overSelect" style = "width : 240px;"></div>
                    </div>
                     <div id="checkboxes" style = "overflow-y : scroll; width : 240px; height : 200px">
                    <asp:CheckBoxList ID="cbl_Nbhd" runat="server"></asp:CheckBoxList>
                    </div>
                 
                </td>
            </tr>
             <tr class = "secRegion" style = "display:none">
                <td style="width: 160px;">
                    Select Region:
                </td>
                <td class="cbl_show1">
                    <div class="selectBox tip" style = "width : 240px;" onclick="showCheckboxes1()" abbr="Select the regions from the drop-down list">
                    <asp:DropDownList runat="server" ID="ddlRegion" Width="240px" />
                         <div class="overSelect" style = "width : 240px;"></div>
                    </div>
                     <div id="checkboxes1"style = "overflow-y : scroll; width : 240px; height : 200px">
                    <asp:CheckBoxList ID="cbl_Region" runat="server"></asp:CheckBoxList>
                    </div>
                 
                </td>
            </tr>
            <tr>
                <td style="width: 160px;">
                    Select Output Field:
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlOutput" Width="240px" />
                    <asp:RequiredFieldValidator runat="server" ID="rfv1" ControlToValidate="ddlOutput"
                        ValidationGroup="FieldSel" Text="Required" Font-Size="Smaller" />
                </td>
            </tr>
        </table>
        <asp:UpdatePanel runat="server" ID="updInputFields" UpdateMode="Conditional">
            <ContentTemplate>
                <table>
                    <tr>
                        <td style="width: 160px;">
                            Select Input Fields:
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlInput" Width="240px" AutoPostBack="true" />
                            <asp:RequiredFieldValidator runat="server" ID="rfv2_2" ControlToValidate="ddlInput"
                                ValidationGroup="InputAdd" />
                            <table>
                            <br></br>
                                <tr>
                                    <td>
                                        Label
                                    </td>
                                    <td>
                                        Field Specification
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtLabel" Width="120px" MaxLength="25" />
                                        <asp:RequiredFieldValidator runat="server" ID="rfv2_1" ControlToValidate="txtLabel"
                                            ValidationGroup="InputAdd" />
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtSpec" Width="200px" MaxLength="150" />
                                        <asp:RequiredFieldValidator runat="server" ID="rfv2" ControlToValidate="txtSpec"
                                            ValidationGroup="InputAdd" />
                                    </td>
                                    <td>
                                        <asp:Button runat="server" ID="btnAddInput" Text=" Add " ValidationGroup="InputAdd" />
                                    </td>
                                </tr>
                            </table>
                            <asp:RequiredFieldValidator runat="server" ID="rfv3" ControlToValidate="txtInputs"
                                ValidationGroup="FieldSel" Text="Add input field(s) to proceed." Font-Size="Smaller" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <div style="display: none;">
                                <asp:TextBox runat="server" ID="txtInputs" Width="300px" Rows="4" TextMode="MultiLine" />
                            </div>
                            <div>
                                <asp:Repeater runat="server" ID="rpInputs">
                                    <ItemTemplate>
                                        <div class="selected-input">
                                            <span>
                                                <%# Eval("DisplayLabel")%></span><asp:LinkButton runat="server" ID="d" Text="X" CommandArgument='<%# Eval("Id") %>'
                                                    CommandName="RemoveX" /></div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel runat="server" ID="updFilters" UpdateMode="Conditional">
            <ContentTemplate>
                <table>
                    <tr>
                        <td style="width: 160px;" >
                            Add Filter:
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlFilterField" Width="180px"  AutoPostBack="true" EnableViewState="true" />
                            <asp:RequiredFieldValidator runat="server" ID="f1" ControlToValidate="ddlFilterField"
                                ValidationGroup="AddFilter" />
                            <asp:DropDownList runat="server" ID="ddlFilterOp" Width="150px" CssClass="mraFilterOp" >
                                <asp:ListItem Text="-- Select --" Value="" />
                                <asp:ListItem Text="Equal To" Value="=" />
                                <asp:ListItem Text="Not Equal To" Value="&lt;&gt;" />
                                <asp:ListItem Text="Greater Than" Value="&gt;" />
                                <asp:ListItem Text="Lesser Than" Value="&lt;" />
                                <asp:ListItem Text="Greater Than Or Equal To" Value="&gt;=" />
                                <asp:ListItem Text="Lesser Than Or Equal To" Value="&lt;=" />
                                <asp:ListItem Text="In" Value="in" />
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator runat="server" ID="f2" ControlToValidate="ddlFilterOp"
                                ValidationGroup="AddFilter" />
                            <span class="tip" abbr=""><asp:TextBox runat="server" ID="txtFilterValue" Width="150px" MaxLength="50"/></span>
                            <asp:RequiredFieldValidator runat="server" ID="f3" ControlToValidate="txtFilterValue"
                                ValidationGroup="AddFilter" />
                            <asp:Button runat="server" ID="btnAddFilter" Text=" Add " ValidationGroup="AddFilter" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <div style="display: none;">
                                <asp:TextBox runat="server" ID="txtFilters" />
                            </div>
                            <asp:Repeater runat="server" ID="rpFilters">
                                <ItemTemplate>
                                    <div class="filter-statement">
                                        <span>
                                            <%# Eval("FieldName")%>
                                            <%# Eval("Operator")%>
                                            <%# Eval("Value")%></span>
                                        <asp:LinkButton runat="server" ID="d" Text="X" CommandArgument='<%# Eval("ID") %>'
                                            CommandName="RemoveX" />
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress runat="server" AssociatedUpdatePanelID="updInputFields">
            <ProgressTemplate>
                <div>
                    <b>Please wait ...</b></div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdateProgress runat="server" AssociatedUpdatePanelID="updFilters">
            <ProgressTemplate>
                <div>
                    <b>Please wait ...</b></div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <div>
            <asp:CheckBox runat="server" ID="chkConstantIsZero" Text="Constant is zero" Font-Bold="true" />
        </div>
        <p>
        <asp:Button runat="server" ID="btnTest" Text="Test Data" ValidationGroup="FieldSel" />
        <asp:Button runat="server" ID="btnGenerateMRA" Text="Generate MRA Output" ValidationGroup="FieldSel" /></p>
        <asp:HiddenField    id="hiddenRegion" runat="server" Value="a"/>
        <%--<table>
        	<tr>
        		<td><asp:Button runat="server" ID="btnTest" Text="Test Data" ValidationGroup="FieldSel" /></td>
            	<td><asp:Button runat="server" ID="btnGenerateMRA" Text="Generate MRA Output" ValidationGroup="FieldSel" /></p></td>
            	<td><asp:LinkButton runat="server" Text="Export Templates" ID="lbExportTemplate" /></td>
            	<td><asp:LinkButton runat="server" Text="Import Templates" ID="lbImportTemplate" /></td>
            </tr>
		</table>
		
		<asp:Panel runat="server" ID="pnlTemplateExport" Visible="false">
            <h3>
                MRA Templates</h3>
            <asp:GridView runat="server" ID="templateExport">
                <Columns>
                    <asp:BoundField DataField="ID" HeaderText="ID" ItemStyle-Width="80px" />
                    <asp:BoundField DataField="Name" HeaderText="Name" ItemStyle-Width="400px" />
                    <asp:BoundField DataField="OutputField" HeaderText="Output Field" ItemStyle-Width="180px" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-CssClass="th-right" />
                    <asp:BoundField DataField="InputFields" HeaderText="Input Fields" ItemStyle-Width="500px" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-CssClass="th-right" />
                    <asp:BoundField DataField="Filters" HeaderText="Filters" ItemStyle-Width="500px" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-CssClass="th-right" />
                    <asp:BoundField DataField="CreatedBy" HeaderText="Created By" ItemStyle-Width="380px" ItemStyle-HorizontalAlign="Right"
                    HeaderStyle-CssClass="th-right" />
                    <asp:BoundField DataField="CreatedDate" HeaderText="CreatedDate" ItemStyle-Width="380px" ItemStyle-HorizontalAlign="Right"
                    HeaderStyle-CssClass="th-right" />
                </Columns>
            </asp:GridView>
        </asp:Panel>


		<asp:Panel runat="server" ID="pnlFormView">
        <asp:Panel runat="server" ID="pnlImportCSV">
        </asp:Panel>
        <p>
            <asp:FileUpload runat="server" ID="fuImportFile" />
        </p>
        <p>
            <asp:Button runat="server" ID="btnUpload" Text="Upload Lookup File"  ValidationGroup="Import" />
        </p>
		</asp:Panel>
	--%>

        <p style="display: none;">
            <asp:Label runat="server" ID="lblSQL" />
        </p>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlOutput">
        <h2>
            Multiple Regression Analysis Output</h2>


        <h3>
            Regression Statistics</h3>
        <table cellspacing="0" style="border-collapse: collapse" class="mGrid app-content reg-stat">
            <tr>
                <td>
                    Multiple R
                </td>
                <td>
                    <asp:Label runat="server" ID="lblMultipleR" Text="0" />
                </td>
            </tr>
            <tr>
                <td>
                    R Square
                </td>
                <td>
                    <asp:Label runat="server" ID="lblRSquare" />
                </td>
            </tr>
            <tr>
                <td>
                    Adjusted R Square
                </td>
                <td>
                    <asp:Label runat="server" ID="lblAdjustedRSquare" />
                </td>
            </tr>
            <tr>
                <td>
                    Standard Error
                </td>
                <td>
                    <asp:Label runat="server" ID="lblStandardError" />
                </td>
            </tr>
            <tr>
                <td>
                    Observations
                </td>
                <td>
                    <asp:Label runat="server" ID="lblObservations" />
                </td>
            </tr>
        </table>
        <p>
            <asp:HyperLink runat="server" ID="hlOpenLink">View Regression Model & Form</asp:HyperLink></p>
        <h3>
            ANOVA</h3>
        <table cellspacing="0" style="border-collapse: collapse; text-align: center;" class="mGrid app-contents anova">
            <tr class="ui-state-default">
                <th class="th-right" style="width: 80px;">
                </th>
                <th class="th-right" style="width: 30px;">
                    df
                </th>
                <th style="width: 120px;" class="th-right">
                    SS
                </th>
                <th style="width: 120px;" class="th-right">
                    MS
                </th>
                <th style="width: 120px;" class="th-right">
                    F
                </th>
                <th style="width: 120px;" class="th-right">
                    Significance F
                </th>
            </tr>
            <tr>
                <td>
                    Regression
                </td>
                <td>
                    <asp:Label runat="server" ID="lblDFReg" />
                </td>
                <td>
                    <asp:Label runat="server" ID="lblSSReg" />
                </td>
                <td>
                    <asp:Label runat="server" ID="lblMSReg" />
                </td>
                <td>
                    <asp:Label runat="server" ID="lblFisher" />
                </td>
                <td>
                    <asp:Label runat="server" ID="lblSignificanceF" />
                </td>
            </tr>
            <tr>
                <td>
                    Residual
                </td>
                <td>
                    <asp:Label runat="server" ID="lblDFRes" />
                </td>
                <td>
                    <asp:Label runat="server" ID="lblSSRes" />
                </td>
                <td>
                    <asp:Label runat="server" ID="lblMSRes" />
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    Total
                </td>
                <td>
                    <asp:Label runat="server" ID="lblDFTotal" />
                </td>
                <td>
                    <asp:Label runat="server" ID="lblSSTotal" />
                </td>
                <td>
                    <asp:Label runat="server" ID="lblMSTotal" />
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
        <h3>
            Regression Coefficients</h3>
        <asp:GridView runat="server" ID="regresult">
            <Columns>
                <asp:BoundField DataField="Head" />
                <asp:BoundField DataField="CE" HeaderText="Coefficients" ItemStyle-Width="120px"
                    ItemStyle-HorizontalAlign="Right" HeaderStyle-CssClass="th-right" />
                <asp:BoundField DataField="SE" HeaderText="Standard Error" ItemStyle-Width="120px"
                    ItemStyle-HorizontalAlign="Right" HeaderStyle-CssClass="th-right" />
                <asp:BoundField DataField="TS" HeaderText="t Stat" ItemStyle-Width="120px" ItemStyle-HorizontalAlign="Right"
                    HeaderStyle-CssClass="th-right" />
                <asp:BoundField DataField="PV" HeaderText="p Value" ItemStyle-Width="120px" ItemStyle-HorizontalAlign="Right"
                    HeaderStyle-CssClass="th-right" />
            </Columns>
        </asp:GridView>
        <asp:LinkButton runat="server" Text="Export Residual Output" ID="lbExport" />
        <asp:Panel runat="server" ID="pnlResidual" Visible="false">
            <h3>
                Residual Output</h3>
            <asp:GridView runat="server" ID="residuals">
                <Columns>
                    <asp:BoundField DataField="ID" HeaderText="Obs." ItemStyle-Width="80px" />
                    <asp:BoundField DataField="Item" HeaderText="Item" ItemStyle-Width="200px" />
                    <asp:BoundField DataField="PR" HeaderText="Predicted" ItemStyle-Width="180px" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-CssClass="th-right" />
                    <asp:BoundField DataField="RES" HeaderText="Residual" ItemStyle-Width="180px" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-CssClass="th-right" />
                    <asp:BoundField DataField="ASSGRP" HeaderText="Assignment Group" ItemStyle-Width="180px" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-CssClass="th-right" />
                </Columns>
            </asp:GridView>
        </asp:Panel>
        <p>
            &nbsp;</p>
    </asp:Panel>
</asp:Content>
