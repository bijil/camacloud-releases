﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="CAMACloud.Console.protected_sketched_default"
    CodeBehind="default.aspx.vb" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>CAMA Cloud&#174; Sketch Editor</title>
    <meta name="viewport" content="user-scalable=no,initial-scale=1.0,maximum-scale=1.0,width=device-width,height=device-height" />
    <link rel="stylesheet" href="/App_Static/css/sketched.css?t=123456756786884" />
    <link rel="Stylesheet" href="/App_Static/css/01-lookup.css?t=1" />
    <div class="masklayer dimmer" style="display: none; z-index: 20">
        <span style="margin-left: 47%; margin-top: 22%; float:left">
            <img src="/App_Static/css/images/comploader.gif" />
            <span style='color: wheat; float: right; margin-left: 12px; margin-top: 20px;' class="app-mask-layer-info">Please
                wait...</span></span>
    </div>
    <style>
    body {
   		min-width: 850px !important; 
	}
	.match
	{
	 overflow:auto;
	 height:320px;
	 width:711px;
	 margin-top: 75px;
	 margin-left: 20px;
	}
    .divCodeFile
	{
	display:none;
	width: 750px;
	height: 460px;
	position: absolute;
	top: 88.5px;
	left: 383px;
	z-index: 110;
	background-color: white;
	/*overflow:auto;*/
	}
	.divCodeFile input[type="text"] 
	{
	margin: 20px 0px 0px 22px;
	width: 470px;
	height: 20px;
	font-size: 10pt;
	}
	.divCodeFile button[type="button"] 
	{
	display: inline-block;
	margin: 20px 20px;
	width: 89px;
	height: 27px;
	background-color: rgba(75, 151, 219, 0.81);/*#448fd2;*/ /*rgb(96, 129, 199);*/
	font-size: 10pt;
	padding: 4px 7px;
	}
	ul.customul 
	{
    list-style-type: none;
    padding: 0px;
	}
	.customul li
	{
    border-bottom: 1px solid #ACCCD8;
    padding:5px;
    /* margin:2px;*/   
	}
	.customul li[select="selected"]
	{
    background-color:#ACCCD8;
	}
	div.searchtools button
	{
    float:right;
	}	
	div[searchtools]
	{				
    float:left;
	}
	.searchitem
	{
    float:left;
	}
	.divin
	{
    margin-top: 10px;
	}
    </style>
    <script type="text/javascript" src="/App_Static/jslib/jquery.js"></script>
    <script type="text/javascript" src="/App_Static/jslib/underscore-min.js"></script>
    <script type="text/javascript" src="/App_Static/jslib/hammer.js"></script>
    <script type="text/javascript" src="/App_Static/jslib/mousetrap.js"></script>
    <script type="text/javascript" src="/App_Static/js/qc/02-lookup.js?t=1"></script>
    <cc:JavaScript ID="JavaScript2" runat="server" IncludeFolder="~/App_Static/js/sketch/" />
    <cc:JavaScript ID="JavaScript1" runat="server" IncludeFolder="~/App_Static/js/sketch/sketchlib/" />
    <script>
        var alertLock;
        var _enableWarehouse = false;
        function ccTicks() {
        	var currentYr = new Date().getFullYear(), nextYr =  Math.ceil(currentYr/ 10) * 10;
			nextYr = (((nextYr - currentYr) < 3) ? (nextYr + 10): nextYr);
            var d1 = new Date(nextYr, 12, 31);
            var d2 = new Date();
            var diff = d2.getTime() - d1.getTime();
            return diff;
        }
        window.onerror = function (msg, url, line, ex) {
            console.error(msg);
            if (msg == "TypeError: 'undefined' is not an object") { return false; }
            if (msg == "TypeError: 'undefined' is not a constructor") {
                if (!serviceUnavailabilityWarned) {
                    alert('Some of the online services may be blocked as you are working in offline mode. ');
                    serviceUnavailabilityWarned = true;
                }
                return false;
            }
            url = url.substr(url.lastIndexOf("/") + 1);
            if (url.indexOf('?') > -1)
                url = url.substr(url, url.indexOf('?'));
            alert(msg + "\n\nSource: " + url + " @ line " + line);

        }



        window.messageBox = function (message) {
            var title, okCallback, cancelCallback, options;
            var p2 = arguments[1]; var p3 = arguments[2]; var p4 = arguments[3]; var p5 = arguments[4];
            if (typeof p2 == "string") {
                console.log('P2 is string');
                title = p2;
                if (typeof p3 == "object") {
                    options = p3;
                    okCallback = p4;
                    cancelCallback = p5;
                }
                else {
                    okCallback = p3;
                    cancelCallback = p4;
                }
            }

            if (typeof p2 == "object") {
                options = p2;
                okCallback = p3;
                cancelCallback = p4;
            }

            if (typeof p2 == "function") {
                okCallback = p2;
                cancelCallback = p3;
            }

            if (!options) { options = ["OK"]; }
            if (options.length == 0) { options = ["OK"]; }

            var alertLock = true;
            $('.window-message-box .message-box-title').html(title || 'CAMA Cloud<sup>&#174;</sup>');
            $('.window-message-box .message-box-input').hide();
            $('.window-message-box .message-box-body').html(message);
            if (options.length >= 1) {
                $('.window-message-box .message-box-ok').html(options[0]);
            }
            if (options.length > 1) {
                $('.window-message-box .message-box-cancel').html(options[1]);
                $('.window-message-box .message-box-cancel').show();
            } else {
                $('.window-message-box .message-box-cancel').hide();
            }
            //alert(touchClickEvent);
            var touchClickEvent = 'touchend click';
            $('.mask').show()
            $('.window-message-box').show();
            $('.window-message-box .message-box-ok').unbind(touchClickEvent);
            $('.window-message-box .message-box-ok').bind(touchClickEvent, function (e) {
                e.preventDefault();
                alertLock = false;
                $('.window-message-box').hide();
                $('.mask').hide();
                $("input").blur();
                if (okCallback)
                    okCallback();
            });
            $('.window-message-box .message-box-cancel').unbind(touchClickEvent);
            $('.window-message-box .message-box-cancel').bind(touchClickEvent, function (e) {
                e.preventDefault();
                alertLock = false;
                $('.window-message-box').hide();
                $('.mask').hide();
                $("input").blur();
                if (cancelCallback)
                    cancelCallback();
            });
        }

        window.input = function (message) {
            var singleInput = true;
            var title, okCallback, cancelCallback, values, options, TextboxText, boxType = null, enableKeyDown = null, noteSpecialKeys = null;
            var p2 = arguments[1]; var p3 = arguments[2]; var p4 = arguments[3]; var p5 = arguments[4]; var p6 = arguments[5]; var p7 = arguments[6]; var p8 = arguments[7]; var p9 = arguments[8];
            if (typeof p2 == "string") {
                console.log('P2 is string');
                title = p2;
                if (typeof p3 == "object") {
                    values = p3;
                    okCallback = p4;
                    cancelCallback = p5;
                }
                else {
                    okCallback = p3;
                    cancelCallback = p4;
                    TextboxText = p5;
                }
            }

            if (typeof p2 == "object") {
                values = p2;
                okCallback = p3;
                cancelCallback = p4;
            }

            if (typeof p2 == "function") {
                okCallback = p2;
                cancelCallback = p3;
            }
            let isBxType = false;
			if( p6 && p6.isSqftBox ) {
		        boxType = p6.type;
		        p8 = p6.maxLength;
		        enableKeyDown = true;
                p6 = null;
                isBxType = true;
		    }
			
            options = ["OK", "Cancel"];
            if (cancelCallback == null) {
                options = ["OK"];

            }

            if (values) { singleInput = false; }

            var inputElement
            if (!p6)
                inputElement = singleInput ? $('.window-message-box .message-box-input-text') : $('.window-message-box .message-box-input-sel');
            else {
                inputElement = $('.window-message-box .message-box-input-textarea');
            }
            $('.window-message-box .message-box-input-item').hide();
            $(inputElement).show();
            $(inputElement).val('');
            if (TextboxText)
                $(inputElement).val(TextboxText);
            $('.window-message-box .message-box-input').show();
            var selectedValue = null;
            if (values && values.lookupValues) {
                selectedValue = values.selectedValue;
                values = values.lookupValues;
            }
            if (values) {
                $(inputElement).html('');
                var numElements = 0;
                var firstElementId = "";
                var firstE;
                for (var x in values) {
                    if (typeof values[x] == "object") {
                        var opt = document.createElement('option');
                        opt.innerHTML = values[x].Name;
                        opt.setAttribute('value', values[x].Id);
                        inputElement[0].appendChild(opt);
                        if (numElements == 0) { firstElementId = values[x].Id; firstE = opt; };
                    } else if (typeof values[x] == "string") {
                        var opt = document.createElement('option');
                        opt.innerHTML = values[x];
                        opt.setAttribute('value', values[x]);
                        inputElement[0].appendChild(opt);
                        if (numElements == 0) { firstElementId = values[x]; firstE = opt; };
                    }
                    numElements++;
                }
                if (numElements > 0 && cancelCallback == null) {
                    if (firstElementId != "") {
                        var opt = document.createElement('option');
                        opt.innerHTML = "";
                        opt.setAttribute('value', "");
                        inputElement[0].insertBefore(opt, firstE);
                    }
                }
                if (selectedValue) {
                    $(inputElement).val(selectedValue);
                }
            }
            if (p7 || isBxType) {
                let mCont = isBxType ? 'Please enter a SQFT to continue.': 'Please enter a text to continue.'
                $('.window-message-box .message-box-input').append('<div><span class="validateMessageText" style="display: none;color:Red;">' + mCont +'</span></div>')
                $('.window-message-box .message-box-input-textarea').css('max-width', '290px');
        		$('.window-message-box .message-box-input').bind('keyup', function(){
            		$('.validateMessageText').hide();
        		});
    		}
    		if(p8)
        		$('.window-message-box .message-box-input-textarea').attr("maxlength", p8)
        	
			if (p9 && p9.specialKeys) {
		        noteSpecialKeys = p9.specialKeys;
		        $('.message-box-input .message-box-input-textarea').bind('keydown', function(e) {
		             var e = e || window.event;
		             var k = e.which || e.keyCode;
		             if (noteSpecialKeys) {
		             	var ntspecial = noteSpecialKeys.filter(function(x) { return x == k })[0];
		                if (ntspecial && e.key != '3' && e.key != '.' && e.key != '4' && (ntspecial != 220 || (ntspecial == 220 && e.key == '|')))
		                    return false;
		             }
		        });
		        $('.message-box-input .message-box-input-textarea').bind('keyup', function(e) {
		             if(this && this.value)
		             	this.value = this.value.replaceAll('|', '');
		        });
		    }         	
        		
			if(boxType) {
		        $('.window-message-box .message-box-input-text').prop('type', boxType);
		        if(enableKeyDown) {
		            $('.window-message-box .message-box-input-text').bind('keydown', function() {
		               if( $(inputElement).val().length >= 10 ) {
		                    var sqfttt = $(inputElement).val().slice(0,-1);
			    			$(inputElement).val(sqfttt);
			    		}
		            });
		        }
		    }
			
            var alertLock = true;
            $('.window-message-box .message-box-title').html(title || 'CAMA Cloud<sup>&#174;</sup>');

            $('.window-message-box .message-box-body').html(message);
            if (options.length >= 1) {
                $('.window-message-box .message-box-ok').html(options[0]);
            }
            if (options.length > 1) {
                $('.window-message-box .message-box-cancel').html(options[1]);
                $('.window-message-box .message-box-cancel').show();
            } else {
                $('.window-message-box .message-box-cancel').hide();
            }
            if (p3 == 'messageBox') {
                $('.window-message-box .message-box-input-text').hide();
                $('.window-message-box .message-box-cancel').hide();
            }
            //alert(touchClickEvent);
            var touchClickEvent = 'touchend click';
            $('.mask').show();
            $('.window-message-box').show();
            $('.window-message-box .message-box-ok').unbind(touchClickEvent);
            $('.window-message-box .message-box-ok').bind(touchClickEvent, function (e) {
                e.preventDefault();
                if(p8)
            		$('.window-message-box .message-box-input-textarea').removeAttr("maxlength")
                
           		if (p9 && p9.specialKeys)
                    $('.message-box-input .message-box-input-textarea').unbind('keydown');

                if ((p7 && $(inputElement).val().trim() == '') || (isBxType && $(inputElement).val().trim() == '' && TextboxText != '')) {
             		$('.validateMessageText').show();
             		return false;
        		}
        		else{
                    if (p7 || isBxType) {
        				$('.window-message-box .message-box-input').unbind('keyup');
        				$('.window-message-box .message-box-input-textarea').css('max-width', '');
        				$('.validateMessageText').remove();
        			}
                	alertLock = false;
                	$('.window-message-box').hide();
                	$('.mask').hide()
                	$("input").blur();
                	if (okCallback)
                    	okCallback($(inputElement).val());
                }

                if (enableKeyDown) {
                    $('.window-message-box .message-box-input-text').unbind('keydown');
                    $('.window-message-box .message-box-input-text').prop('type', 'Text');
                }
            });
            $('.window-message-box .message-box-cancel').unbind(touchClickEvent);
            $('.window-message-box .message-box-cancel').bind(touchClickEvent, function (e) {
                e.preventDefault();
                alertLock = false;
                if (p7 || isBxType) {
        			$('.window-message-box .message-box-input').unbind('keyup');
        			$('.window-message-box .message-box-input-textarea').css('max-width', '');
        			$('.validateMessageText').remove();
        		}
        		if(p8)
            		$('.window-message-box .message-box-input-textarea').removeAttr("maxlength")
            	if (p9 && p9.specialKeys)
        			$('.message-box-input .message-box-input-textarea').unbind('keydown');
                if(enableKeyDown) {
            		$('.window-message-box .message-box-input-text').unbind('keydown');
            		$('.window-message-box .message-box-input-text').prop('type', 'Text');
            	}
                $('.window-message-box').hide();
                $('.mask').hide();
                $("input").blur();
                if (cancelCallback)
                    cancelCallback();
            });

        }
function keyUpforSketch(source){
	var searchWord = $('#searchtxt').val().trim().toLowerCase();
    var lk = window.opener.lookup[SketchLookup]?Object.keys(window.opener.lookup[SketchLookup]).map(function (x) { return window.opener.lookup[SketchLookup][x] }):'';
    var skLookup = window.opener.ccma.Data.LookupMap[SketchLookup];
    var isLookUpQueryField=skLookup ? skLookup.LookupTable == '$QUERY$' : false;
    if(lk.length == 0){
		lk = sketchApp.currentSketch.lookUp;
		if(lk == undefined)
			lk = [];
		if (typeof(lk) == 'object' && lk.length == undefined) {
			lk = Object.keys(lk).map(function(x) { return lk[x] }).sort(function(a, b) { return a.Ordinal - b.Ordinal });
		}
    }
    if(lk.length == 0){
    	$('.match').html('No lookup data...');
    	if(window.opener && window.opener.appType != 'sv')
    		window.opener.setScreenDimensions();
    	return false;
    }
    v = (searchWord != '') ? lk.filter(function (d) { return ((d.Id) ? d.Id.toLowerCase() : d.Id) == searchWord || ((d.Description) ? d.Description.toLowerCase().indexOf(searchWord) != -1 : d.Description) || ((d.Name) ? d.Name.toLowerCase().indexOf(searchWord) != -1 : d.Name); }) : [];
    if (v.length == 0) {
        $('.match').html('No matches...');
    }
    else {
        $('.match').html('');
        var ul = document.createElement('ul');
        $(ul).attr("class", "customul");
        $('.match')[0].appendChild(ul);
        for (i in v) {
            $('.match ul')[0].appendChild(createList(v[i], isLookUpQueryField));
        }
    }
    if ($('#searchtxt').val() == '') {
        $('.match').html('');
    }
	if(window.opener && window.opener.appType != 'sv')
    	window.opener.setScreenDimensions();
    return false;
}
function createList(v, isLookUpQueryFields) {
    var li = document.createElement('li');
    $(li).attr('value', v.Id);
    li.innerHTML = isLookUpQueryFields? v.Name : v.Id+" "+ v.Name;
    if(sketchApp && sketchApp.currentSketch && sketchApp.currentSketch && sketchApp.currentSketch.config && sketchApp.currentSketch.config.EnableSwapLookupIdName)
    	li.innerHTML = v.Name +" "+ v.Id;
    $(li).attr('onclick', 'return listClicked(this);');
    $(li).attr('select', '');
    return li;
}
function listClicked(d) {
    $('ul.customul li').removeAttr('select');
    $(d).attr('select', 'selected');
    $('span[selectedcustomddl]').html($('ul.customul li[select]').text());
    return;
}


  ////////// Quickbtn added////////
$(document).ready(function () {
 
    $(".toolbtncontainer >.toolbutton",).hover(function () {
        $(this.parentElement.lastElementChild).css("visibility", "visible");
    },
        function () {
            $(this.parentElement.lastElementChild).css("visibility", "hidden");
        });


    $(".toolbox-control",).hover(function () {

        $(this).css("visibility", "visible");
    },
        function () {
            $(this).css("visibility", "hidden");
        });


    $(".toolbar-openbtn").hover(function () {
        $(".toolbox-control").css("visibility", "visible");

    },
        function () {
            $(".toolbox-control").css("visibility", "hidden");
        });

    var selectValues = { "3": "3", "4": "4", "5": "5", "6": "6", "7": "7", "8": "8", "9": "9", "10": "10" };
      
    $.each(selectValues, function (key, value) {
        $('#polysides')
            .append($("<option></option>")
                .attr("value", key)
                .text(value));
    });

    $('.toolbutton').click(function () {
        $('.toolbutton').removeClass('toolbtn-active').addClass('toolbtn-inactive');
        $(this).removeClass('toolbtn-inactive').addClass('toolbtn-active');
         
            if( $(this).hasClass( "hexbtn"))
                $('#polysides').prop('disabled', false);
                else
                $('#polysides').prop('disabled', true);
                $("#polysides").attr("size", 1);
    });

    $('#polysides').on("change", function () {

        $("#polysides").attr("size", 1);

    });

    document.addEventListener("click", function (e) {
        if (!($(".mask").is(":visible"))) {
            if ($(e.target).hasClass('ccse-options')) {
                e.preventDefault();
            }
            else if ($(e.target).closest('.ccse-option-list').length > 0 || $(e.target).hasClass('sketchImageDownload')) {
                $('.ccse-option-list').show();
            }
            else if ($(e.target).closest('.ccse-option-list').length == 0) {
                $('.ccse-option-list').hide();
            }
        }
    }, true);
});
    
function quicktoolbarUpdate() {
    $('.toolbutton').removeClass('toolbtn-active')
     $("#polysides").attr("size", 1);
      $('#polysides').prop('disabled', true);
}

////Quickbtn script ends////


    </script>
    

</head>
<body class="unselectable">
    <form id="form1" runat="server">
        <div class="ccsketched unselectable">
            <div class="ccse-sketchtoolbar unselectable">
                <div class="ccse-toolbar toolbar unselectable">
                    <div class="ccse-other-tools">
                        <span> Sketch/Segment: </span>
                        <select class="ccse-sketch-select" style="width: 120px; height: 24px;">
                        </select>
                        <select class="ccse-page-select" style="width: 30px; height: 24px; display: none;">
                		</select>
                        <select class="ccse-vector-select" style="width: 120px; height: 24px;">
                        </select>
                        <span class="ccse-add-page" style="display: none;">+</span>
                        <span class="ccse-delete-page" style="display: none;">x</span>
                        <span class="sketch-label" style="font-size: 12pt; margin: 6pt; display: none;">Status</span>
                        <span class="ccse-zoom-value" style="font-size: 12pt; display: inline-block; text-align: center; width: 60px;"><span>100</span>%</span>
                    </div>
                    <a class="ccse-toolbutton ccse-zoom ccse-tool-active"></a><a class="ccse-toolbutton ccse-pan ccse-tool-active"></a><a class="ccse-toolbutton ccse-angle-lock ccse-tool-active"></a>
                    <a class="ccse-toolbutton ccse-options ccse-tool-inactive" onclick="$('.ccse-option-list').show(); return false;" style=""></a>
                    <a class="ccse-preview-sketch ccse-beforesketch"  style="font-size: 29px; color: #ddd;float: right;  margin-right: 10px;margin-left: 10px;">&#x2935;</a>
                    <a class="ccse-preview-sketch  ccse-currentsketch"  style="font-size: 29px; color: #ddd; float: right;  margin-right: 10px;margin-left: 10px;">&#x2934;</a>
                    <div class="fvSwitch" style="display: none;"><label class="switch"><input type="checkbox" class="fv-switch-input" onchange="return enableFullView();"><span class="slider"></span></label></div>
                    <div class="ccse-option-list hidden">
                        <a class="list ccse-download-jpg">Download as JPG</a>
                        <a class="list ccse-download-png">Download as PNG</a>
                    </div>
                    <div class="ccse-toolbar toolbar">
                    
                                            <!-- edit -->
                      
					<div class="ccse-mode toolbar-openbtn-container" style="padding-left: 16px;">
					    <button class="toolbox-control-hexbtn toolbar-openbtn" >
                            <span class="toolbox-add-polygon"></span>
					    </button>
					  </div>
                         <div style="float: left" class="ccse-mode-buttons">
                           <div >
                           <button class="ccse-create-newpage" style="display: none;">
                             Add Page</button>
                           <button  class="ccse-delete-newpage" style="display: none;">
                             Delete Page</button>
                           </div>            
                       </div>
                        <!-- edit -->
                        <div style="float: right" class="ccse-mode-buttons">
                        	<div class="ccse-mode ccse-mode-scale" >
			                	Scale :
			                    <input class="ccse-scale" readonly="readonly" style="width: 50px;" />
			                </div>
                            <div class="ccse-mode ccse-mode-line">
                                Length:
                        		<input class="ccse-meter-length" readonly="readonly" style="width: 55px;" />
                                Angle:
                        		<input class="ccse-meter-angle" readonly="readonly" style="width: 55px;" />
                            </div>
                <!-- rotate btn starts -->
                            <div class="ccse-mode  ccse-mode-quick-rotate ccse-anti-rotate">
                                <button class="ccse-quick-rotate ccse-quick-anticlock" cmd="L">
                                </button>
                            </div>
                            <div class="ccse-mode  ccse-mode-quick-rotate ccse-clock-rotate">
                                <button class="ccse-quick-rotate ccse-quick-clock" cmd="R">
                                </button>
                            </div>
                 <!-- rotate ends -->
                            <div class="ccse-mode ccse-mode-rotate-sketch">
                    			<button class="ccse-rotate-sketch">
                       			</button>
                			</div>
                            <div class="ccse-mode ccse-mode-manual-sketch">
                                <button class="ccse-manual-box">
                                    Add'l Areas
                                </button>
                            </div>
                			<div class="ccse-mode ccse-mode-undo-segment">
			                    <button class="ccse-undo-segment">
			                    </button>
			                </div>
                            <div class="ccse-mode ccse-mode-sketch-details">
                                <button class="ccse-sketch-details">
                                    Details
                                </button>
                            </div>
                            <div class="ccse-mode ccse-mode-select-line">
			                    <button class="ccse-party-wall">
                                    Party Wall
                                </button>
			                </div>
                            <div class="ccse-mode ccse-mode-select-node">
                                <button class="ccse-show-pad">
                                    Keys</button>
                                <button class="ccse-delete-node">
                                    Delete Node</button>
                                <button class="ccse-add-node-before">
                                    Add Node before</button>
                                <button class="ccse-delete-line">
                                    Delete Line</button>
                            </div>
                            <div class="ccse-mode ccse-mode-transfer-segment">
                                <button class="ccse-transfer-vector">
                                    Transfer Segment</button>
                            </div>
                            <div class="ccse-mode ccse-mode-multi-segment">
                                <button class="ccse-add-segment">
                                    Add Segment</button>
                            </div>
                            <div class="ccse-mode ccse-mode-select-vector">
                                <button class="ccse-delete-vector">
                                    Delete Segment</button>
                            </div>
                            <div class="ccse-mode ccse-mode-copy-vector">
                                <button class="ccse-copy-vector">
                                    Copy Segment</button>
                            </div>
                            <div class="ccse-mode ccse-mode-paste-vector" style="display: none">
                                <button class="ccse-paste-vector">
                                    Paste Segment</button>
                            </div>
                            <div class="ccse-mode ccse-mode-select-vector-plus">
                                <button class="ccse-edit-label">
                                    Edit Label</button>
                            </div>
                            <div class="ccse-mode ccse-mode-select-label-move">
                                <button class="ccse-move-label">
                                    Move Label</button>
                            </div>
                            <div class="ccse-custom-buttons">

               				</div>
                            <div class="ccse-mode ccse-mode-select-sketch-plus">
                                <button class="ccse-add-vector">
                                    Add Segment</button>
                            </div>
                            <div class="ccse-mode ccse-mode-select-sketch">
                                <button class="ccse-flip-v">
                                    Flip-V</button>
                                <button class="ccse-flip-h">
                                    Flip-H</button>
                                <button class="ccse-sketch-move" status="off">
                                    Move</button>
                            </div>

                            <div class="ccse-mode ccse-mode-add-segment">
                                <button class="ccse-show-pad">
                                    Keys</button>
                                <button class="ccse-undo">
                                    Undo</button>
                                  <button class="ccse-redo">
                        Redo</button>
                            </div>
                            <div class="ccse-mode ccse-mode-notes">
                                <button class="ccse-notes-toggle">
                                    Hide Notes</button>
                            </div>
                            <div class="ccse-mode ccse-mode-notes-edit">
                                <button class="ccse-notes-edit">
                                    Edit Note</button>
                            </div>
                            <div class="ccse-mode ccse-mode-notes-new">
                                <button class="ccse-notes-new">
                                    New Note</button>
                            </div>
                            <div class="ccse-mode ccse-mode-notes-delete">
                                <button class="ccse-notes-delete">
                                    Delete Note</button>
                            </div>
                            <div class="ccse-mode ccse-mode-sqft-tool">
                    			<button class="ccse-sqft-tool">
                       				SQFT</button>
                			</div>
                            <div class="ccse-mode ccse-mode-visible">
                                <button class="ccse-save">
                                    Save Changes</button>
                                <button class="ccse-close">
                                    Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                
  <!-- QUICK BTN  -->
  
                <div class="toolbox-control" ">
                    <div class="toolboxcontainer">
                        <div class="toolarccontainer">
                            <div class="arcbtn1 toolbtncontainer ">
                                <button class="tool_convexarc toolbutton toolbtn-inactive" id="buttonup">
                                    <span class=""></span>
                                </button>
                                <span class="tooltiptext tooltip-convex-arc" >Concave Arc</span>
                            </div>
                            <div class="arcbtn1 toolbtncontainer">
                                <button class="tool_concavearc toolbutton toolbtn-inactive"  id="buttondown">
                                    <span class=""></span>
                                </button>
                                <span class="tooltiptext tooltip-concave-arc" >Convex Arc</span>
                            </div>
                        </div>
                        <div class="toolshapecontainer">
                            <div class="rectbutton1 toolbtncontainer">
                                <button class="squrebtn toolbutton toolbtn-inactive">
                                    <span></span>
                                </button>
                                <span class="tooltiptext tooltip-squre">Square</span>
                            </div>
                            <div class="rectbutton2 toolbtncontainer">
                                <button class="rectanglebtn toolbutton toolbtn-inactive">
                                    <span></span>
                                </button>
                                <span class="tooltiptext tooltip-rectangle">Rectangle</span>
                            </div>
                        </div>
                        <div class="toolshapecontainer">
                            <div class="hexbtncontainer toolbtncontainer">
                                <button class="hexbtn toolbutton toolbtn-inactive">
                                    <span class="hexbtn_icon"></span>
                                </button>
                                <span class="tooltiptext tooltip-polygon">Polygon</span>
                            </div>
                            <div class="polysidecontainer">
                                <select id="polysides" class="polygonsides" disabled> </select>
                            </div>
                        </div>
                    </div>
                </div>
<!-- QUICK BTN ENDS -->
                
                <div class="pan-control pan-normal control-window" style="display: none;">
                </div>
                <div class="zoom-control control-window">
                    <a class="zoom-minus zoom-button minus">-</a>
                    <div class="zoom-range range">
                    </div>
                    <a class="zoom-plus zoom-button plus">+</a>
                </div>
            </div>
            <div class="sketch-pad unselectable" style="display: none;">
                <div class="movable-title-bar unselectable" unselectable="on">
                    Edit Sketch Segment <a class="hide-sketch-pad"><span></span></a>
                </div>
                <div class="sketch-pad-key-pad">
                    <table>
                        <tr>
                            <td>
                                <span class="sp-text-box box-length" selected="" max="9999" min="0"><span>0</span>'</span>
                            </td>
                            <td>
                                <a class="sketch-pad-btn sketch-pad-cmd sp-add" cmd="+"><span></span></a><a class="sketch-pad-btn sketch-pad-cmd sp-up"
                                    cmd="U" style = "margin-left: 3px"><span></span></a><a class="sketch-pad-btn sketch-pad-cmd sp-angle" cmd="A" style = "margin-left: 3px"><span></span></a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="sp-text-box box-angle" max="180" min="-180"><span>0</span>&deg;</span>
                            </td>
                            <td>
                                <a class="sketch-pad-btn sketch-pad-cmd sp-left" cmd="L"><span></span></a><a class="sketch-pad-btn sketch-pad-cmd sp-down"
                                    cmd="D" style = "margin-left: 3px"><span></span></a><a class="sketch-pad-btn sketch-pad-cmd sp-right" cmd="R" style = "margin-left: 3px"><span></span></a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a class="sketch-pad-btn sketch-pad-btn-wide sketch-pad-cmd sp-circle" cmd="ELL">ELL</a>
                            </td>
                            <td>
                                <a class="sketch-pad-btn sketch-pad-num sp-n7">7</a> <a class="sketch-pad-btn sketch-pad-num sp-n8">8</a> <a class="sketch-pad-btn sketch-pad-num sp-n9">9</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a class="sketch-pad-btn sketch-pad-btn-wide sketch-pad-cmd sp-rect" cmd="BOX">BOX</a>
                            </td>
                            <td>
                                <a class="sketch-pad-btn sketch-pad-num sp-n4">4</a> <a class="sketch-pad-btn sketch-pad-num sp-n5">5</a> <a class="sketch-pad-btn sketch-pad-num sp-n6">6</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a class="sketch-pad-btn sketch-pad-btn-wide sketch-pad-num sp-nc">CLEAR</a>
                            </td>
                            <td>
                                <a class="sketch-pad-btn sketch-pad-num sp-n1">1</a> <a class="sketch-pad-btn sketch-pad-num sp-n2">2</a> <a class="sketch-pad-btn sketch-pad-num sp-n3">3</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a class="sketch-pad-btn sketch-pad-btn-wide sketch-pad-cmd sp-ok" cmd="OK">OK</a>
                            </td>
                            <td>
                                <a class="sketch-pad-btn sketch-pad-num sp-nc">-</a> <a class="sketch-pad-btn sketch-pad-num sp-n0">0</a> <a class="sketch-pad-btn sketch-pad-num sp-np">.</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a class="sketch-pad-btn sketch-pad-cmd sp-recty" cmd="RECT" style ="width:84px">Rectangle</a>
                            </td>
                            <td>
                                <span class="sp-text-box box-lengthR" selected="" max="999" min="0" style ="width:57px; font-size: 16pt; text-align: left">L:<span>0</span></span>
                                <span class="sp-text-box box-width" selected="" max="999" min="0" style ="width:57px; font-size: 16pt; text-align: left">W:<span>0</span></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <select class="sketch-pad-ddl sketch-poly-sides">
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </select>
                            </td>
                            <td>
                                <a class="sketch-pad-btn sketch-pad-btn-wide sketch-pad-cmd sp-poly" cmd="POLY">POLY</a>
                                <a class="sketch-pad-btn sketch-pad-cmd sp-rectx" cmd="BOX">BX</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <canvas class="ccse-canvas"></canvas>
        </div>
        <div class="Current_vector_details hidden">
            <div class="head">Edit Labels</div>
            <div class="dynamic_prop">
            </div>
            <div style="float: right; padding: 5px 45px">
                <button id="Btn_Save_vector_properties">Save </button>
                <button id="Btn_cancel_vector_properties">Cancel </button>
            </div>
        </div>
        <div class="manual-entry-container hidden"  id="manual-entry">
          <button class="manual-hide-button" id="manual-hideBtn">&#9650;</button>
          <div class="manual-popup-header">
            <div class="manual-popup-title">Manually Entered Areas</div>
          </div>
          <div class="manual-entery-popup-content">
          </div>
        </div>
        <div class="cc-drop-pop" style="width: 200px;display:none;">
            <input type="text" class="cc-drop-search" />
            <div class="cc-drop-items"></div>
        </div>
		<div class="divCodeFile" style="left: 0px; right:0px; margin:0 auto;">
	        <div class="divin">
	            <span legend style="margin-left: 20px; font-weight: bold;"></span><span selectedcustomddl></span>
	        </div>
	        <div class="searchtools">
	            <input type="text" style="float: left;" id="searchtxt" onkeyup="return keyUpforSketch(this)">
	            <button type="button" onclick="return cancelClick()" style="display: inline-block;">
	                Cancel</button>
	            <button type="button" onclick="return okClick()" style="display: inline-block; margin-right: -12px;">
	                OK</button>
	        </div>
	        <%-- <div class="searchitem">
	    <span  style="margin-left: 20px;">Search items:</span>
	    </div>--%>
	        <div class="match">
	            <div class="noitem">
	            </div>
	        </div>
    	</div>
        <div class="window-message-box hidden">
            <div class="full-frame">
                <div class="message-box-frame">
                    <div class="message-box-title">
                        CAMA Cloud<sup>&#174;</sup>
                    </div>
                    <div class="message-box-body">
                        ABCD
                    </div>
                    <div class="message-box-input">
                        <input type="text" class="message-box-input-item message-box-input-text" />
                        <select class="message-box-input-item message-box-input-sel">
                        </select>
                        <textarea class="message-box-input-item message-box-input-textarea" cols="20" rows="3"></textarea>
                    </div>
                    <div class="message-box-buttons">
                        <a class="button message-box-ok">OK</a> <a class="button message-box-cancel">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="customNote">
            <div class="note-content">
                <div class="note-header">New Sketch Note</div>
                <div class="custom-note-options"></div>
                <textarea class="note-textarea"></textarea>
                <div class="note-button-container">
                    <button class="note-button note-ok">OK</button>
                    <button class="note-button note-cancel">Cancel</button>
                </div>
            </div>
        </div>
        <div class="mask">
        </div>
    </form>
</body>
</html>
