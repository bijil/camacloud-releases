﻿Imports CAMACloud

Partial Class FeatureTestPage
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.QueryString("test") IsNot Nothing Then
            btnTest.Visible = True
        End If

        If UserName() = "admin" AndAlso Not Roles.RoleExists("DataSetup") Then
            Roles.CreateRole("DataSetup")
            Roles.AddUserToRole(UserName, "DataSetup")
        End If

    End Sub
    'Munir 14/08/2013
    Protected Sub btnTest_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTest.Click
        Try
            NotificationMailer.SendNotification("admin", txtSubject.Text, txtMessage.Text)

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try




    End Sub
End Class
