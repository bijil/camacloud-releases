
/*
Tipr 2.0.1
Copyright (c) 2015 Tipue
Tipr is released under the MIT License
http://www.tipue.com/tipr
*/


(function($) {

     $.fn.tipr = function(options) {
     	var parent = options ? options : document;
      	var set = $.extend( {
               
               'speed': 200,
               'mode': 'bottom'
              
   
             }, options);

          return this.each(function() {

              var tipr_cont = '.tipr_container_' + set.mode;

              $(this).hover( 
                 
                    function (){
                        if (localStorage.getItem('toolTipEnabled') == 0)
                            return false;
                         var d_m = set.mode;
                         if ($(this).attr('abbr') == "") return false;
                         if ($(this).attr('data-mode')){
                             d_m = $(this).attr('data-mode')
                              tipr_cont = '.tipr_container_' + d_m;   
                         }
                        
                        //<div class="tipr_point_' + d_m + '">
                         
                         var out = '<div class="tipr_container_' + d_m + '" onclick="event.stopPropagation();"><div class="tipr_content">' + $(this).attr('abbr') + '</div></div>';
                         
                         $(this).append(out);
                         if ($(this).attr('div-width')) {
                             $(tipr_cont,parent).outerWidth(parseInt($(this).attr('div-width')));
                         }
                         if ($(this).attr('max-width')) {
                             $('.tipr_content').css('max-width','350px');
                         }
                         var w_t = $(tipr_cont,parent).outerWidth();
                         var w_e = $(this).width();
                         var h_t = $(tipr_cont,parent).outerHeight();
                         var m_l = (w_e / 2) - (w_t / 2);
                         var m_b=$(this).attr('tip-bottom') ?$(this).attr('tip-bottom') : '';
                         var m_t = $(this).attr('tip-margin-top') ? $(this).attr('tip-margin-top') : 0;
                         m_l = m_l < 0 ? 0 : m_l;
                         m_l = $(this).attr('tip-margin-left') ? $(this).attr('tip-margin-left') : m_l;
                         $(tipr_cont,parent).css('position','absolute')
                         var parent_tipr = $(this).parents();
                         if(d_m == "top"){
                         	$(tipr_cont).css('margin-top', '-67px');                        	
                         	if($(this).position().left < 120){
                         		$(tipr_cont,parent).css('right', '0px');
                         		$(tipr_cont,parent).css('width', '125%');
                         	}
                         }
                         else if(parent_tipr.find('div#MainContent_reportsHome') && $('#MainContent_reportsHome').width()>1000){
                           $(tipr_cont,parent).css('margin-left','-25px');
                         	$(tipr_cont,parent).css('margin-top', m_t + 'px');
                         }
                         else if($(this).hasClass('specialCase')){
                         	m_t = $('.report-container').height() - $('.report-container').prop('scrollHeight');
                         	$(tipr_cont,parent).css('margin-left', m_l + 'px');
                         	$(tipr_cont,parent).css('margin-top', m_t + 'px');
                         }
                         else{
                         	$(tipr_cont,parent).css('margin-left', m_l + 'px');
                         	$(tipr_cont,parent).css('margin-top', m_t + 'px');
                         }
                         if($(this).attr('width')){
							var size = parseInt($(this).attr('width'));
							$(tipr_cont,parent).css('width',size + 'px');
						 }
                         if(m_b != '')
                         $(tipr_cont,parent).css('bottom', m_b + 'px');
                         if($('#temptitle').length == 0){
	                        	var spanElement = document.createElement('span');
	                        	spanElement.id = "temptitle";
	                        	if($(this).attr('title')) spanElement.setAttribute('title',$(this).attr('title'));
	                        	if($(this).attr('alt')) spanElement.setAttribute('title',$(this).attr('alt'));
	                        	document.body.append(spanElement)
                        	}
                         $(this).removeAttr('title alt');
                         $(tipr_cont,parent).show()
					     if($(document).height() > $('body').height()){
                          var diffScroll = $('body').height() - $(document).height();
                          var copDiffSroll = diffScroll;
                          if(copDiffSroll < 0) copDiffSroll = copDiffSroll * -1;
                          if(copDiffSroll < 100 && copDiffSroll > 3) $(tipr_cont,parent).css('margin-top', diffScroll + 'px');  
                         }
                         $(tipr_cont,parent).hide();
                         
                         $(tipr_cont,parent).fadeIn(set.speed);              
                    },
                    function ()
                    {   
                    	if($('#temptitle').attr('title')) $(this).attr('title', $('#temptitle').attr('title'));
                    	if($('#temptitle').attr('alt')) $(this).attr('title', $('#temptitle').attr('alt'));
                    	$('#temptitle').remove();
                         $(tipr_cont,parent).remove();    
                    }     
               );
                              
          });
     };
     
})(jQuery);
