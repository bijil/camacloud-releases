/**
Javascript Repeater
http://jsrepeater.devprog.com/
Version 1.1
    
Copyright (c) 2008 devprog.com 
This code is licensed under the MIT Licence
http://www.opensource.org/licenses/mit-license.php
    
usage: $('#myDiv').fillTemplate(data);
Please see the site for details on template formatting
**/

(function ($) {
    $.fn.fillTemplate = function (obj) {

        //if ($.fn.fillTemplate.rptInstance == null) { 
        $.fn.fillTemplate.rptInstance = new $.fn.fillTemplate.jsRepeater();
        //}

        return this.each(function () {

            if ((this.id == null) || (this.id == undefined)) {
                this.innerHTML = "Error: id attribute required";
            } else {
                if (($.fn.fillTemplate.rptInstance.templaters[this.id] == null) || ($.fn.fillTemplate.rptInstance.templaters[this.id] == undefined)) {
                    $.fn.fillTemplate.rptInstance.templaters[this.id] = new $.fn.fillTemplate.templater();
                    var node = this.cloneNode(true);
                    $.fn.fillTemplate.rptInstance.templaters[this.id].initialise(this.cloneNode(true));
                }
                this.innerHTML = $.fn.fillTemplate.rptInstance.templaters[this.id].parse(obj);
            }

        });
    };

    $.fn.fillTemplate.rptInstance = null;

    $.fn.fillTemplate.jsRepeater = function () {
        this.templaters = {};
    };

    $.fn.fillTemplate.templater = function () {
        this.templaters = {}; this.Context = null; this.template = "";
        this.isRoot = true; this.mirrorID = null;
        this.ContextSpecifier = null;
        this.Orderby = null;
        this.Limit = null;
        this.Filter = null;
    };

    var templater = $.fn.fillTemplate.templater;

    templater.prototype.initialise = function (rootNode) {
        if ((rootNode.getAttribute) && (rootNode.getAttribute("context"))) {
            this.Context = rootNode.getAttributeNode("context").value;
            if (rootNode.getAttribute("specifier")) {
                this.ContextSpecifier = rootNode.getAttributeNode("specifier").value;
            }
            if (rootNode.getAttribute("orderby")) {
                this.Orderby = rootNode.getAttributeNode("orderby").value;
            }
            if (rootNode.getAttribute("filter")) {
                this.Filter = rootNode.getAttributeNode("filter").value;
            }
            if (rootNode.getAttribute("limit")) {
                this.Limit = rootNode.getAttributeNode("limit").value;
            }
        }

        for (var i = 0; i < rootNode.childNodes.length; i++) {
            this.extractSubTemplates(rootNode, rootNode.childNodes[i]);
        }
        if (!this.isRoot) {
            var tempNode = document.createElement("div");
            tempNode.appendChild(rootNode.cloneNode(true));
            this.template = tempNode.innerHTML;
        } else { this.template = rootNode.innerHTML; }
        this.template = this.template.replace(/%7B/g, "{");
        this.template = this.template.replace(/%7D/g, "}");
    };
    templater.prototype.initialiseMirror = function (rootNode) {
        var tempNode = document.createElement("div");
        tempNode.appendChild(rootNode.cloneNode(false));
        var Marker = document.createTextNode("STATIC");
        tempNode.childNodes[0].appendChild(Marker);
        this.template = tempNode.innerHTML;
    };
    templater.prototype.parseOrdering = function (template, ordinal, total) {
        template = template.replace(/%{([^}]*)}/g,
            function (match, group1) {
                var first = null;
                var alternates = null;
                var last = null;

                if (group1.indexOf("|") > -1) {
                    var ary = group1.split("|");
                    first = ary[0];
                    alternates = ary[1];
                    if (ary.length > 2) { last = ary[2]; }
                } else {
                    alternates = group1;
                }
                alternates = alternates.split(":");

                if ((ordinal == 0) && (first != null)) { return first; }
                if ((ordinal == total - 1) && (last != null)) { return last; }
                return alternates[ordinal % alternates.length];
            });
        return template;
    };
    templater.prototype.parseRecursionOrdering = function (template, data, recursionCount, ob) {
        template = template.replace(/!%{([^}]*)}/g,
            function (match, group1) {
                var first = null;
                var alternates = null;
                var last = null;

                if (group1.indexOf("|") > -1) {
                    var ary = group1.split("|");
                    first = ary[0];
                    alternates = ary[1];
                    if (ary.length > 2) { last = ary[2]; }
                } else {
                    alternates = group1;
                }
                alternates = alternates.split(":");

                if ((recursionCount == 0) && (first != null)) { return first; }
                //if ((ordinal == total - 1) && (last != null)) { return last; }
                return alternates[recursionCount % alternates.length];
            });
        return template;
    };
    templater.prototype.parseNumbering = function (template, ordinal, total) {
        template = template.replace(/#{([^}]*)}/g,
            function (match, group1) {
                return ordinal + 1;
            });
        return template;
    };
    templater.prototype.parseRecursive = function (template, data, recursionCount, ob) {
        //All calls comes here!
        template = template.replace(/!{([^}]*)}/g,
        function (match, group1) {
            if (group1 > recursionCount) {
                if (ob.Context == null) { return ""; }
                var contextData = data[ob.Context];
                if ((contextData == null) || (contextData === undefined)) { return ""; }
                return ob.parse(data, recursionCount + 1);
            } else { return ""; }
        });
        return template;
    };
    templater.prototype.parse = function (data, recursionCount) {
        var result = "";
        var self = this;
        if (this.mirrorID) {
            result += this.template.replace(/(STATIC)/g,
                    function (match, group1) {
                        return document.getElementById(self.mirrorID).innerHTML;
                    });
            return result;
        }
        if ((recursionCount == null) || (recursionCount == undefined)) { recursionCount = 0; }
        var contextData = null;
        var contextName;
        if (this.Context) {
        	var contextName = this.Context;
            contextData =  _.clone(data[this.Context]);
             if(contextData && openFromPRC)  
                contextData= contextData.filter(function(c){return(c.CC_Deleted=="false"||c.CC_Deleted==false)});
            if (this.ContextSpecifier && contextData) {
                switch (this.ContextSpecifier) {
                    case "first":
                        contextData = contextData.slice(0, 1);
                        break;
                    case "top 2":
                        contextData = contextData.slice(0, 2);
                        break;
                    case "top 3":
                        contextData = contextData.slice(0, 3);
                        break;
                    case "top 5":
                        contextData = contextData.slice(0, 5);
                        break;
                }
            }
            if (this.Filter && contextData) {
                var expression = this.Filter;
                for (x in contextData[0]) { var regex = new RegExp(x, "g"); expression = expression.replace(regex, 'thisObject.' + x) };
                contextData = eval('contextData.filter(function (thisObject) { return (' + expression + ') })');
            }
            
            if (this.Orderby && contextData) {
                /*
                    var SortItems = this.Orderby.trim().split(',');
                    var exp = "";
                    SortItems.forEach(function (Item) {
                        var sortType1 = '>';
                        var sortType2 = "<";
                        var SortSpecifier = Item.trim().split(' ');
                        if (SortSpecifier.length > 1) {
                            sortType1 = SortSpecifier[1].toUpperCase() == "ASC" ? '>' : '<';
                            sortType1 == '>' ? sortType2 = '<' : sortType2 = '>';
                        }
    
                        exp += "a." + SortSpecifier[0] + ".Value" + sortType1 + "b." + SortSpecifier[0] + ".Value ?1:" + "a." + SortSpecifier[0] + ".Value" + sortType2 + "b." + SortSpecifier[0] + ".Value ?-1:";
                    }); exp = exp += "0"
                    contextData = eval('contextData.sort(function(a, b){return (' + exp + ') })');
                    */
                let sortPm = this.Orderby.trim().split(" ")[1];
                if (sortPm?.trim() == 'RFSORT') {
                    contextData = provalResSorting(contextData, this.Orderby.trim());
                }
                else {
                    var Source_Data = contextData;
                    sortNull(this.Orderby.trim(), Source_Data, contextName, 1);
                    var sortExp = Sortscript(this.Orderby.trim(), contextName);
                    Source_Data = eval('Source_Data' + sortExp);
                    sortNull(this.Orderby.trim(), Source_Data, contextName, 0);
                    contextData = Source_Data;
                }
            }
            if (this.Limit && contextData && /^[0-9]+$/.test(this.Limit)) {
                contextData = contextData.slice(0, this.Limit);
            }
        }
        else { contextData = data; }
        if ((contextData == null) || (contextData == undefined)) { contextData = {}; }
        if (Object.prototype.toString.call(contextData) ==='[object Array]') {
            for (var i = 0; i < contextData.length; i++) {
                var obj = contextData[i];
                if (obj === undefined)
                    return "";
                result += this.template.replace(/\$\{([^}]*)\}/g,
                    function (match, group1) {
                        var outer = group1.split(":");
                        var val = outer[0];
                        var valspecs = val.split(",");
                        val = valspecs[0];
                        var source = null;
                        if (valspecs.length > 1) {
                            source = valspecs[1];
                        }
                        var f = null;
                        if (outer.length > 1) { f = outer[1].trim(); }
                        var ary = val.split(".");

                        var args = [];
                        if (f != null) {
                            args = f.split(",");
                            if (args.length > 1) {
                                f = args[0];
                            }
                        }
                        var p = null;
                        if (obj.CC_ParcelId || obj.NbhdId) {
                            //var prc = false;
                            //if (activeParcel && activeParcelData) {
                            //    if (obj == activeParcelData) {
                            //        var prc = true; p = obj;
                            //    }
                            //}
                            if (obj == activeParcel) {
                                p = obj;
                            } else {

                                if (obj.Id && obj.KeyValue1) {
                                    var pid = obj.Id.toString();
                                    if (listparcels[pid] == null) {
                                        if ((activeParcel) && (pid == activeParcel.Id)) {
                                            //listparcels[pid] = new Parcel(obj.Id, obj, false);
                                            listparcels[pid] = activeParcel;
                                        } else {
                                            listparcels[pid] = new Parcel(obj.Id, obj, false);
                                        }
                                    }

                                    p = listparcels[pid];
                                }

                            }
                        }

                        if (!p) {
                            if (obj) {
                                if ((obj.constructor.name == "Parcel") || (obj.constructor.name == "Neighborhood")) {
                                    p = obj;
                                }
                            }
                        }


                        if (p) {
                            if (p.Eval) {
                                if (obj.constructor.name == "Neighborhood")
                                    console.log(f);
                                if (f == "Parcel.Eval") {
                                    return p.EvalText(source, val, args[1]);
                                }
                                if (f == "Parcel.Lookup") {
                                    return p.Lookup(val, args[1]);
                                }
                                if (f == "CompData") {
                                    return p.CompData(args[1], val);
                                }
                                if (f == "Eval") {
                                    return p.EvalText(source, val);
                                }
                                if (f == "format") {
                                    if (args[1]) {
                                        var value = p.EvalText(source, val)
                                        if (value) {
                                            value = value.replace(/,/g,'');
                                            return value.formatout(args[1].replace('~', ','));
                                        }
                                        else
                                            return "";
                                    }
                                }
                                if (args.length == 0) {
                                    if (val == 'SketchPreview' || val == 'FirstPhoto') return p.EvalValue(source, val)
                                    else
                                        return p.EvalText(source, val);
                                }
                            }
                        }

                        if (f == "lookup") {     
                            if (args[1]) {
                                var value = obj[val];
                                if (typeof value === 'object' && value && value != undefined)
                                    value = value.Value;
                                var orgVal = obj.Original ? obj.Original[val] : value;
                                var desc;
                                var field =  Object.keys(datafields).filter(function (x) { return source ? ((datafields[x].Name == val) &&(datafields[x].SourceTable == source)) : ((datafields[x].Name == val) && (datafields[x].CategoryId || (datafields[x].QuickReview == "true"))) }).map(function (x) { return datafields[x] })[0];

                                if (field && field.Id && field.UIProperties && field.UIProperties != null) {
                                    value = customFormatValue(value, field.Id);
                                    orgVal = orgVal ? customFormatValue(orgVal, field.Id) : orgVal;
                                }

                                if (args[2] && args[2].toString().toLowerCase() == 'lookupdesc')
                                    desc = evalLookup(args[1], value, true, field);
                                else
                                    desc = evalLookup(args[1], value, null, field);
                                return value == orgVal ? desc : '<span class="EditedValue">' + desc + '</span>';
                                //return evalLookup(args[1], value);
                            }
                        }

                        if (f == "format") {
                            if (args[1]) {
                                var _value = (obj[val] == 0) ? '0' : obj[val];
                                var _orgValue = obj.Original ? obj.Original[val] : null;
                                if (_value) {
                                    if (_value.formatout) {
                                        var _field = Object.keys(datafields).filter(function (x) { return (datafields[x].SourceTable == contextName && datafields[x].Name == val) }).map(function (x) { return datafields[x] })[0];
                                        if (_field && _field.Id && _field.UIProperties && _field.UIProperties != null && _value != "***") {
                                            _value = customFormatValue(_value, _field.Id);
                                            _orgValue = _orgValue ? customFormatValue(_orgValue, _field.Id) : _orgValue;
                                        }

                                        var _reValue = _value.formatout(args[1].replace('~', ','));
                                        if ( contextName && _reValue != "***" ) {
                                    		if( _field ) {
                                    			var _category = getCategory( _field.CategoryId );
                                    			_reValue = ( _value == _orgValue || typeof (_reValue) != "string" || ( (_field.ReadOnly=="true"|| _field.ReadOnly == true) ) || ( _category && ( _category.IsReadOnly == true || _category.IsReadOnly == "true" ) ) ) ? _reValue : '<span class="EditedValue">' + _reValue  + '</span>' ;
                                    		}
                                    		return _reValue;
                                    	}
                                    	else
                                    		return _reValue;
                                    }
                                    else
                                        return "";
                                }
                                else
                                    return "";
                            }
                        }

                        if (f == "gridview") {
                            if (args[1]) {
                                if (val)
                                    return activeParcel.EvalText(args[1], val);
                                else
                                    return "";
                            }
                        }

                        if (f == "object" || f == "table") {
                            if (args[1]) {
                                if (obj && val)
                                    if (args[2]) {  //If specifiers has a third parameter..
                                        switch (args[2].toString().toLowerCase()) {
                                            case "lookupdesc":      //eg., ${FIELDNAME:table,TABLENAME,lookupdesc}
                                                return ccma.Data.Evaluable.EvalObject(obj, val, args[1], true).ChangedValue;
                                        }
                                    } else {
                                        return ccma.Data.Evaluable.EvalObject(obj, val, args[1]).ChangedValue;
                                    }

                                else
                                    return "";
                            }

                        }
                        
                        var newObj = obj;
                        for (var j = 0; j < ary.length; j++) {
                            if (activeTab == 'history') {
                                newObj = newObj[ary[j]];
                                return newObj;
                            }
                            else if(ary[j].match("[+\\-*/&!%]") && contextName){
                            	newObj = ccma.Data.Evaluable.EvalObject(obj, ary[j], contextName).DisplayValue;
								return newObj;                            	
                            }
                            else {
                                newObj = ccma.Data.Evaluable.EvalObjectPRC(obj, ary[j], f,contextName);
                                return newObj;
                            }
                        }

                    });
                var self = this;
                result = this.parseNumbering(result, i, contextData.length);
                result = this.parseRecursionOrdering(result, contextData[i], recursionCount, this);
                result = this.parseOrdering(result, i, contextData.length);

                result = this.parseRecursive(result, contextData[i], recursionCount, this);


                result = result.replace(/\~\{([^}]*)\}/g,
                        function (match, group1) {
                            return self.templaters[group1].parse(contextData[i]);
                        });
            }
        } else {
            var obj = contextData;
            if (_.isEmpty(obj))
                return "";
            result += this.template.replace(/\$\{([^}]*)\}/g,
                    function (match, group1) {
                        var outer = group1.split(":");
                        var val = outer[0];
                        var f = null;
                        if (outer.length > 1) { f = outer[1]; }

                        ary = val.split(".");
                        var newObj = obj;
                        for (var j = 0; j < ary.length; j++) {
                            newObj = newObj[ary[j]];

                            if (newObj == undefined) {
                                if (f != null) {
                                    return eval(f + '(newObj);');
                                }
                                else { return ""; } // "N/A"
                            }
                        }
                        if (f != null) { return eval(f + '(newObj);'); }
                        else { return newObj; }
                    });
            var self = this;
            result = this.parseNumbering(result, i, contextData.length);
            result = this.parseRecursionOrdering(result, contextData, recursionCount, this);
            result = this.parseOrdering(result, 0, 1);

            result = this.parseRecursive(result, contextData, recursionCount, this);


            result = result.replace(/\~\{([^}]*)\}/g,
            function (match, group1) {
                return self.templaters[group1].parse(contextData);
            });
        }

        return result;
    };
    templater.prototype.extractSubTemplates = function (sourceTree, node) {
        var plucked = null;
        var markerID = null;
        var markerNode = null;
        var Subtemplater = null;
        if ((node.getAttribute) && (node.getAttribute("subtemplate"))) {
            plucked = node;
            markerID = this.newGuid();
            markerNode = document.createTextNode("~{" + markerID + "}");
            sourceTree.replaceChild(markerNode, node);
            Subtemplater = new $.fn.fillTemplate.templater();
            Subtemplater.isRoot = false;
            Subtemplater.mirrorID = node.getAttributeNode("ID").nodeValue;
            this.templaters[markerID] = Subtemplater;
            Subtemplater.initialiseMirror(plucked);
            return;
        }
        if ((node.getAttribute) && (node.getAttribute("context"))) {

            plucked = node;
            markerID = this.newGuid();
            markerNode = document.createTextNode("~{" + markerID + "}");

            sourceTree.replaceChild(markerNode, node);

            Subtemplater = new $.fn.fillTemplate.templater();
            Subtemplater.isRoot = false;
            this.templaters[markerID] = Subtemplater;
            Subtemplater.initialise(plucked);
        }
        else {
            for (var i = 0; i < node.childNodes.length; i++) {
                this.extractSubTemplates(node, node.childNodes[i]);
            }
        }
    };
    templater.prototype.S4 = function () {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    templater.prototype.newGuid = function () {
        return (this.S4() + this.S4() + "-" + this.S4() + "-" + this.S4() + "-" + this.S4() + "-" + this.S4() + this.S4() + this.S4()).toUpperCase();
    };


})(jQuery);

