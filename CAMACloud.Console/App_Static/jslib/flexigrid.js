/*
 * Flexigrid for jQuery -  v1.1
 *
 * Copyright (c) 2008 Paulo P. Marinas (code.google.com/p/flexigrid/)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 */

var lastTargetUrl;
var lastPostData;
var isAdhocCreator=false;
var DTRRoles;


(function ($) {
    $.addFlex = function (t, p) {
        if (t.grid) return false; //return if already exist
        p = $.extend({ //apply default properties
            height: 200, //default height
            width: 'auto', //auto width
            striped: true, //apply odd even stripes
            novstripe: false,
            minwidth: 30, //min width of columns
            minheight: 80, //min height of columns
            resizable: true, //allow table resizing
            url: false, //URL if using data from AJAX
            method: 'POST', //data sending method
            dataType: 'xml', //type of data for AJAX, either xml or json
            errormsg: 'Connection Error',
            usepager: false,
            nowrap: true,
            page: 1, //current page
            total: 1, //total data
			pages: 1, // total pages
			newp: 1,
            useRp: false, //use the results per page select box
            rp: 15, //results per page
            rpOptions: [25, 50, 100, 250], //allowed per-page values 
            title: false,
            pagestat: 'Displaying {from} to {to} of {total} parcels',
            pagetext: 'Page',
            outof: 'of',
            findtext: 'Find',
            procmsg: 'Searching parcels, please wait ...',
            query: '',
            qtype: '',
            nomsg: 'No parcels found',
            minColToggle: 1, //minimum allowed column to be hidden
            showToggleBtn: true, //show or hide column toggle popup
            hideOnSubmit: true,
            autoload: false,
            blockOpacity: 0.5,
            preProcess: false,
            onSearch: false,
            onDragCol: false,
            onToggleCol: false,
            onChangeSort: false,
            onSuccess: false,
            onError: false,
            onSubmit: false, //using a custom populate function
            onDblClick: null,
            onSelect: null,
            onColResize: null,
            onNoData: null
        }, p);
        $(t).show() //show if hidden
			.attr({
			    cellPadding: 0,
			    cellSpacing: 0,
			    border: 0
			}) //remove padding and spacing
			.removeAttr('width'); //remove width properties
        //create grid class
        var g = {
            hset: {},
            rePosDrag: function () {
                var cdleft = 0 - this.hDiv.scrollLeft;
                if (this.hDiv.scrollLeft > 0) cdleft -= Math.floor(p.cgwidth / 2);
                $(g.cDrag).css({
                    top: g.hDiv.offsetTop + 1
                });
                var cdpad = this.cdpad;
                $('div', g.cDrag).each(
					function () {
					   $(this).height($(g.bDiv).height() + $(g.hDiv).height());
					}
				);
                $('div', g.cDrag).hide();
                $('thead tr:first th:visible', this.hDiv).each(function () {
                    var n = $('thead tr:first th:visible', g.hDiv).index(this);
                    var cdpos = parseInt($('div', this).width());
                    if (cdleft == 0) cdleft -= Math.floor(p.cgwidth / 2);
                    cdpos = cdpos + cdleft + cdpad;
                    if (isNaN(cdpos)) {
                        cdpos = 0;
                    }
                    $('div:eq(' + n + ')', g.cDrag).css({
                        'left': cdpos + 'px'
                    }).show();
                    cdleft = cdpos;
                });
            },
            fixHeight: function (newH) {
                //newH = false;
                if (!newH) newH = $(g.bDiv).height();
                var hdHeight = $(this.hDiv).height();
                $('div', this.cDrag).each(
					function () {
					    $(this).height(newH + hdHeight);
					}
				);
                var nd = parseInt($(g.nDiv).height());
                if (nd > newH) $(g.nDiv).height(newH).width(200);
                else $(g.nDiv).height('auto').width('auto');
                $(g.block).css({
                    height: newH,
                    marginBottom: (newH * -1)
                });
                var hrH = g.bDiv.offsetTop + newH;
                if (p.height != 'auto' && p.resizable) hrH = g.vDiv.offsetTop;
                $(g.rDiv).css({
                    height: hrH
                });
            },
            setHeight: function(h){
                var extras = g.hDiv.offsetHeight + g.mDiv.offsetHeight + g.pDiv.offsetHeight;
                $(g.bDiv).height(h - extras);
                $(g.nDiv).css({ 'margin-bottom': (-h + extras) + 'px' });
                this.fixHeight();
            },
            setWidth: function(w){
                $(g.bDiv).width(w);
            },
            dragStart: function (dragtype, e, obj) { //default drag function start
                if (dragtype == 'colresize') {//column resize
                    $(g.nDiv).hide();
                    $(g.nBtn).hide();
                    var n = $('div', this.cDrag).index(obj);
                    var ow = $('th:visible div:eq(' + n + ')', this.hDiv).width();
                    $(obj).addClass('dragging').siblings().hide();
                    $(obj).prev().addClass('dragging').show();
                    this.colresize = {
                        startX: e.pageX,
                        ol: parseInt(obj.style.left),
                        ow: ow,
                        n: n
                    };
                    $('body').css('cursor', 'col-resize');
                } else if (dragtype == 'vresize') {//table resize
                    var hgo = false;
                    $('body').css('cursor', 'row-resize');
                    if (obj) {
                        hgo = true;
                        $('body').css('cursor', 'col-resize');
                    }
                    this.vresize = {
                        h: p.height,
                        sy: e.pageY,
                        w: p.width,
                        sx: e.pageX,
                        hgo: hgo
                    };
                } else if (dragtype == 'colMove') {//column header drag
                    $(g.nDiv).hide();
                    $(g.nBtn).hide();
                    this.hset = $(this.hDiv).offset();
                    this.hset.right = this.hset.left + $('table', this.hDiv).width();
                    this.hset.bottom = this.hset.top + $('table', this.hDiv).height();
                    this.dcol = obj;
                    this.dcoln = $('th', this.hDiv).index(obj);
                    this.colCopy = document.createElement("div");
                    this.colCopy.className = "colCopy";
                    this.colCopy.innerHTML = obj.innerHTML;
                    if ($.browser.msie) {
                        this.colCopy.className = "colCopy ie";
                    }
                    $(this.colCopy).css({
                        position: 'absolute',
                        float: 'left',
                        display: 'none',
                        textAlign: obj.align
                    });
                    $('body').append(this.colCopy);
                    $(this.cDrag).hide();
                }
                $('body').noSelect();
            },
            dragMove: function (e) {
                if (this.colresize) {//column resize
                    var n = this.colresize.n;
                    var diff = e.pageX - this.colresize.startX;
                    var nleft = this.colresize.ol + diff;
                    var nw = this.colresize.ow + diff;
                    if (nw > p.minwidth) {
                        $('div:eq(' + n + ')', this.cDrag).css('left', nleft);
                        this.colresize.nw = nw;
                    }
                } else if (this.vresize) {//table resize
                    var v = this.vresize;
                    var y = e.pageY;
                    var diff = y - v.sy;
                    if (!p.defwidth) p.defwidth = p.width;
                    if (p.width != 'auto' && !p.nohresize && v.hgo) {
                        var x = e.pageX;
                        var xdiff = x - v.sx;
                        var newW = v.w + xdiff;
                        if (newW > p.defwidth) {
                            this.gDiv.style.width = newW + 'px';
                            p.width = newW;
                        }
                    }
                    var newH = v.h + diff;
                    if ((newH > p.minheight || p.height < p.minheight) && !v.hgo) {
                        this.bDiv.style.height = newH + 'px';
                        p.height = newH;
                        this.fixHeight(newH);
                    }
                    v = null;
                } else if (this.colCopy) {
                    $(this.dcol).addClass('thMove').removeClass('thOver');
                    if (e.pageX > this.hset.right || e.pageX < this.hset.left || e.pageY > this.hset.bottom || e.pageY < this.hset.top) {
                        //this.dragEnd();
                        $('body').css('cursor', 'move');
                    } else {
                        $('body').css('cursor', 'pointer');
                    }
                    $(this.colCopy).css({
                        top: e.pageY + 10,
                        left: e.pageX + 20,
                        display: 'block'
                    });
                }
            },
            dragEnd: function () {
                if (this.colresize) {
                    var n = this.colresize.n;
                    var nw = this.colresize.nw;
                    $('th:visible div:eq(' + n + ')', this.hDiv).css('width', nw);
                    $('tr', this.bDiv).each(
						function () {
						    $('td:visible div:eq(' + n + ')', this).css('width', nw);
						}
					);
                    this.hDiv.scrollLeft = this.bDiv.scrollLeft;
                    $('div:eq(' + n + ')', this.cDrag).siblings().show();
                    $('.dragging', this.cDrag).removeClass('dragging');
                    this.rePosDrag();
                    this.fixHeight();
                    this.colresize = false;
                    if(p.onColResize)
                        p.onColResize(parseInt($('th:visible div:eq(' + n + ')', this.hDiv).parent().attr('colid')), nw);
                } else if (this.vresize) {
                    this.vresize = false;
                } else if (this.colCopy) {
                    $(this.colCopy).remove();
                    if (this.dcolt != null) {
                        if (this.dcoln > this.dcolt) $('th:eq(' + this.dcolt + ')', this.hDiv).before(this.dcol);
                        else $('th:eq(' + this.dcolt + ')', this.hDiv).after(this.dcol);
                        this.switchCol(this.dcoln, this.dcolt);
                        $(this.cdropleft).remove();
                        $(this.cdropright).remove();
                        this.rePosDrag();
                        if (p.onDragCol) {
                        	 var columnnames= "";
                        	 var hlength = $('.hDivBox thead tr th').length - 1;
                        	 if(hlength < 0) hlength = 0;
                        	 $('.hDivBox thead tr th').each(function (index) {    
	                            var fname = $(this).attr('field');
	                            columnnames += fname;
	                            if(hlength != index)
	                            	columnnames += ","
                              });
                            p.onDragCol(columnnames);                         
                        }
                    }
                    this.dcol = null;
                    this.hset = null;
                    this.dcoln = null;
                    this.dcolt = null;
                    this.colCopy = null;
                    $('.thMove', this.hDiv).removeClass('thMove');
                    $(this.cDrag).show();
                }
                $('body').css('cursor', '');
                $('.flexigrid').css('cursor', 'default');
                $('body').noSelect(false);
            },
            toggleCol: function (cid, visible) {
                var ncol = $("th[axis='col" + cid + "']", this.hDiv)[0];
                var n = $('thead th', g.hDiv).index(ncol);
                var cb = $('input[value=' + cid + ']', g.nDiv)[0];
                if (visible == null) {
                    visible = ncol.hidden;
                }
                if ($('input:checked', g.nDiv).length < p.minColToggle && !visible) {
                    return false;
                }
                if (visible) {
                    ncol.hidden = false;
                    $(ncol).show();
                    cb.checked = true;
                } else {
                    ncol.hidden = true;
                    $(ncol).hide();
                    cb.checked = false;
                }
                $('tbody tr', t).each(
					function () {
					    if (visible) {
					        $('td:eq(' + n + ')', this).show();
					    } else {
					        $('td:eq(' + n + ')', this).hide();
					    }
					}
				);
                this.rePosDrag();
                if (p.onToggleCol) {
                    p.onToggleCol(cid, visible);
                }
                return visible;
            },
            switchCol: function (cdrag, cdrop) { //switch columns
                $('tbody tr', t).each(
					function () {
					    if (cdrag > cdrop) $('td:eq(' + cdrop + ')', this).before($('td:eq(' + cdrag + ')', this));
					    else $('td:eq(' + cdrop + ')', this).after($('td:eq(' + cdrag + ')', this));
					}
				);
                //switch order in nDiv
                if (cdrag > cdrop) {
                    $('tr:eq(' + cdrop + ')', this.nDiv).before($('tr:eq(' + cdrag + ')', this.nDiv));
                } else {
                    $('tr:eq(' + cdrop + ')', this.nDiv).after($('tr:eq(' + cdrag + ')', this.nDiv));
                }
                if ($.browser.msie && $.browser.version < 7.0) {
                    $('tr:eq(' + cdrop + ') input', this.nDiv)[0].checked = true;
                }
                this.hDiv.scrollLeft = this.bDiv.scrollLeft;
            },
            scroll: function () {
                this.hDiv.scrollLeft = this.bDiv.scrollLeft;
                this.rePosDrag();
            },
            addData: function (data, callback) { //parse data
                if (p.dataType == 'json') {
                    data = $.extend({ rows: [], page: 0, total: 0 }, data);
                }
                if (p.preProcess) {
                    data = p.preProcess(data);
                }
                $('.pReload', this.pDiv).removeClass('loading');
                this.loading = false;
                if (!data) {
                    $('.pPageStat', this.pDiv).html(p.errormsg);
                    return false;
                }
                if (p.dataType == 'xml') {
                    p.total = +$('rows total', data).text();
                } else {
                    p.total = data.total;
                }
                if (p.total == 0) {
                    $('tr, a, td, div', t).unbind();
                    $(t).empty();
                    p.pages = 1;
                    p.page = 1;
                    this.buildpager();
                    $('.pPageStat', this.pDiv).html(p.nomsg);
                    if (p.onNoData) {
                        p.onNoData(this);
                    }
                    return false;
                }
                p.pages = Math.ceil(p.total / p.rp);
                if (p.dataType == 'xml') {
                    p.page = +$('rows page', data).text();
                } else {
                    p.page = data.page;
                }
                this.buildpager();
                //build new body
                var tbody = document.createElement('tbody');
                if (p.dataType == 'json') {
                    $.each(data.rows, function (i, row) {
                        var tr = document.createElement('tr');
                        if (i % 2 && p.striped) {
                            tr.className = 'erow';
                        }
                        if (row.id) {
                            tr.id = 'row' + row.id;
                        }
                        if(row.Id||row.Id==0){
                            $(tr).attr('parcelid', row.Id)
                            $(tr).attr('id', 'row' + row.Id)
                        }
                        $('thead tr:first th', g.hDiv).each( //add cell
							function () {
							    var td = document.createElement('td');
							    var idx = $(this).attr('axis').substr(3);
                                var field = $(this).attr('field');
							    td.align = this.align;
							    // If the json elements aren't named (which is typical), use numeric order
//							    if (typeof row.cell[idx] != "undefined") {
//							        td.innerHTML = (row.cell[idx] != null) ? row.cell[idx] : ''; //null-check for Opera-browser
//							    } else {
//							        td.innerHTML = row.cell[p.colModel[idx].name];
//							    }
                                if(row[field] || row[field] === 0){
                                    td.innerHTML = row[field];
                                }
							    $(td).attr('abbr', $(this).attr('abbr'));
							    $(tr).append(td);
							    td = null;
							}
						);
                        if ($('thead', this.gDiv).length < 1) {//handle if grid has no headers
                            for (idx = 0; idx < cell.length; idx++) {
                                var td = document.createElement('td');
                                // If the json elements aren't named (which is typical), use numeric order
                                if (typeof row.cell[idx] != "undefined") {
                                    td.innerHTML = (row.cell[idx] != null) ? row.cell[idx] : ''; //null-check for Opera-browser
                                } else {
                                    td.innerHTML = row.cell[p.colModel[idx].name];
                                }
                                $(tr).append(td);
                                td = null;
                            }
                        }
                        $(tbody).append(tr);
                        tr = null;
                    });
                } else if (p.dataType == 'xml') {
                    var i = 1;
                    $("rows row", data).each(function () {
                        i++;
                        var tr = document.createElement('tr');
                        if (i % 2 && p.striped) {
                            tr.className = 'erow';
                        }
                        var nid = $(this).attr('id');
                        var ntitle = $(this).attr('title');
                        if (nid) {
                            tr.id = 'row' + nid;
                            $(tr).attr('oid', nid);
                        }
                        if (ntitle) {
                            $(tr).attr('title', ntitle);
                        }
                        nid = null;
                        var robj = this;
                        $('thead tr:first th', g.hDiv).each(function () {
                            var td = document.createElement('td');
                            var idx = $(this).attr('axis').substr(3);
                            td.align = this.align;
                            td.innerHTML = $("cell:eq(" + idx + ")", robj).text();
                            $(td).attr('abbr', $(this).attr('abbr'));
                            $(tr).append(td);
                            td = null;
                        });
                        if ($('thead', this.gDiv).length < 1) {//handle if grid has no headers
                            $('cell', this).each(function () {
                                var td = document.createElement('td');
                                td.innerHTML = $(this).text();
                                $(tr).append(td);
                                td = null;
                            });
                        }
                        $(tbody).append(tr);
                        tr = null;
                        robj = null;
                    });
                }
                $('tr', t).unbind();
                $(t).empty();
                $(t).append(tbody);
                this.addCellProp();
                this.addRowProp();
                this.rePosDrag();
                tbody = null;
                data = null;
                i = null;
                if (p.onSuccess) {
                    p.onSuccess(this);
                }
                if (p.hideOnSubmit) {
                    $(g.block).remove();
                }
                this.hDiv.scrollLeft = this.bDiv.scrollLeft;
                if ($.browser.opera) {
                    $(t).css('visibility', 'visible');
                }
				if($('.parcel-results tbody tr').length > 0 ){
					$('.parcel-results tbody tr:first-child td div').each(function(item){
						var abbr = $(this).parent('td').attr('abbr');
						$('.hDivBox thead th[abbr="'+abbr+'"] div').width($(this).width());
					});
				}
                if(callback) callback();
            },
            changeSort: function (th) { //change sortorder
                if (this.loading) {
                    return true;
                }
                let parcelResultsElement = $(".parcel-results")[0];
                if (parcelResultsElement && parcelResultsElement.p && parcelResultsElement.p.total <= 1) {
                    return true;
                }
                $(g.nDiv).hide();
                $(g.nBtn).hide();

                var tbody = $(".parcel-results")
                if (tbody.children().length > 0) {
                    if (p.sortname == "" || p.sortname == undefined)
                        p.sortname = $(th).attr('abbr');
                    if (p.sortorder == "" || p.sortorder == undefined)
                        p.sortorder = 'asc';
                }
                if (p.sortname == $(th).attr('abbr')) {
                    if (p.sortorder == 'asc') {
                        p.sortorder = 'desc';
                    } else {
                        p.sortorder = 'asc';
                    }
                }
                $(th).addClass('sorted').siblings().removeClass('sorted');
                $('.sdesc', this.hDiv).removeClass('sdesc');
                $('.sasc', this.hDiv).removeClass('sasc');
                $('div', th).addClass('s' + p.sortorder);
                p.sortname = $(th).attr('abbr');
                if(p.onSearch)
                    p.onSearch();
                if (p.onChangeSort) {
                    p.onChangeSort(p.sortname, p.sortorder);
                } else {
                    if(__DTR && $('.app-dtr-selected-task option').length == 0)
                        doSearch();
                      else
               	          this.populate();
                }
            },
            buildpager: function () { //rebuild pager based on new properties
                $('.pcontrol input', this.pDiv).val(p.page);
                $('.pcontrol span', this.pDiv).html(p.pages);
                var r1 = (p.page - 1) * p.rp + 1;
                var r2 = r1 + p.rp - 1;
                if (p.total < r2) {
                    r2 = p.total;
                }
                var stat = p.pagestat;
                stat = stat.replace(/{from}/, r1);
                stat = stat.replace(/{to}/, r2);
                stat = stat.replace(/{total}/, p.total);
                //changed region by MSP on 17/08/2013 starts
                if(parseInt( p.pages)>= parseInt(p.newp))
                {
                p.newp=1;
                }
                  //changed region by MSP on 17/08/2013 ends
                $('.pPageStat', this.pDiv).html(stat);
            },
            populate: function (targetUrl, postData, callback, trackerIdEnabled) {//get latest data
                if (this.loading) {
                    return true;
                }
                if (p.onSubmit) {
                    var gh = p.onSubmit();
                    if (!gh) {
                        return false;
                    }
                }
                this.loading = true;
                if (!p.url) {
                    return false;
                }
                $('.pPageStat', this.pDiv).html(p.procmsg);
                $('.pReload', this.pDiv).addClass('loading');
                var gh = $(g.gDiv).height();
				$(g.block).css({
					  width: g.bDiv.style.width,
					  height: gh,
					  background: 'white',
					  position: 'relative',
					  marginBottom: (gh * -1),
					  zIndex: 1,
					  top:  g.bDiv.offsetTop,
					  left: '0px'
				  });
                if (p.hideOnSubmit) {
                    $(this.gDiv).prepend(g.block);
                }
                if ($.browser.opera) {
                    $(t).css('visibility', 'hidden');
                }
                if (!p.newp) {
                    p.newp = 1;
                }
                if (p.page > p.pages) {
                    p.page = p.pages;
                }
                var param = [{
                    name: 'page',
                    value: p.newp
                }, {
                    name: 'pagesize',
                    value: p.rp
                }, {
                    name: 'sortname',
                    value: p.sortname
                }, {
                    name: 'sortorder',
                    value: p.sortorder
                }, {
                    name: 'query',
                    value: p.query
                }, {
                    name: 'qtype',
                    value: p.qtype
                }];
                if (p.params) {
                    for (var pi = 0; pi < p.params.length; pi++) {
                        param[param.length] = p.params[pi];
                    }
                }
                if(postData == null){
                    postData = lastPostData;
                }
                else{
                
                    lastPostData = postData;
                }
                
                if(targetUrl ==null){
                    targetUrl =  lastTargetUrl;
                }else{
                    lastTargetUrl = targetUrl;
                }
                if(postData != null){
                    postData['page'] = p.newp;
                    postData['pagesize'] = p.rp;
                    postData['sortname'] = p.sortname;
                    postData['sortorder'] = p.sortorder;
                }

                if (trackerIdEnabled && randomTrackerId && randomTrackerId != 0 && postData != null) {
                    postData['randomTrackerId'] = randomTrackerId;
                }
                else {
                    postData['randomTrackerId'] = '';
                    randomTrackerId = 0;
                }

                var url = targetUrl ? targetUrl : p.url;
                if(url.indexOf('?') == -1)
                    url = url + '?zt=' + (new Date()).getTime();
                else
                    url = url + '&yt=' + (new Date()).getTime();
                
                $('.grid-export-link').css('display', 'none');
                $('.adhoc-creator-link').css('display', 'none');
                $('.back-to-priority').css('display', 'none');
                $('.bulk-edit-link').css('display', 'none');
                $('.col-seperator-grid').css('display','none');
                $.ajax({
                    type: p.method,
                    url: url,
                    data: postData ? postData : param,
                    dataType: p.dataType,
                    success: function (data) {
                        if(data){
                            if(data.total)
                                if (data.total > 0) {
                                    randomTrackerId = data.TrackerId;
                                    $('.grid-export-link').css('display', 'inline-block');
                                    if (clientSettings.EnableAdhocAssignment && clientSettings.EnableAdhocAssignment == '1')
									    {$('.adhoc-creator-link').css('display', 'inline-block');}
									if (priorityListFlag =='PriorityNotApproved'|| priorityListFlag =='AssignedParcels'){
                                    	if(!(priorityListFlag =='AssignedParcels' && localStorage.getItem("PrioTableId") == null)) $('.adhoc-creator-link').css('display', 'none');
                                    	( localStorage.getItem("PrioTableId") != null ) ? $('.back-to-priority').css('display', 'inline-block') : $('.back-to-priority').css('display', 'none');  //  && ( localStorage.getItem("fileName") != null)
                                    }else
                                     	$('.back-to-priority').css('display', 'none');
                                    if(!__DTR){
                                        $('.bulk-edit-link').css('display', 'inline-block');
                                         $('.col-seperator-grid').css('display','inline-block');
                                         }
                                    else {
                                    		$('.adhoc-creator-link').css('display', 'none');
                                     		$('.back-to-priority').css('display', 'none');
											if(DTRRoles && DTRRoles.Manager){
												$('.bulk-edit-link').css('display', 'inline-block');
												$('.col-seperator-grid').css('display','inline-block');
											}
										}
									var lookupFieldInSearch = offlineTables.SearchFields.filter(function(x){ return x.inputType == "lookup"});
									lookupFieldInSearch.forEach(function(y){ 
										if(data.rows.length > 0 && data.rows[0][y.assignedName] !== undefined){
											data.rows.forEach(function(x){
											     if(x.CautionFlag==1)
												 {
													if(!x.CautionMessage)
													{
														x.CautionMessage = clientSettings.DefaultCaution;
													}
												 }
												 x[y.assignedName] = lookup[y.assignedName] ? lookup[y.assignedName][x[y.assignedName]] ? lookup[y.assignedName][x[y.assignedName]].Description : x[y.assignedName] : x[y.assignedName];
											});
										}
									});
                                }
                        }
                        g.addData(data,callback);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        try {
                            if (p.onError) p.onError(XMLHttpRequest, textStatus, errorThrown);
                        } catch (e) { }
                    }
                });
            },
            doSearch: function () {
              
                p.query = $('input[name=q]', g.sDiv).val();
                p.qtype = $('select[name=qtype]', g.sDiv).val();
               p.newp = 1;
                if(onSearch)
                    p.onSearch();
                this.populate();
              
            },
            customSearch: function (query) {
                p.query = query;
                p.qtype = 'custom';
                p.newp = 1;
                if(onSearch)
                    p.onSearch();
                this.populate();
            },
            powerSearch: function (query) {
                p.query = query;
                p.qtype = 'power';
             p.newp = 1;
                if(onSearch)
                    p.onSearch();
                this.populate();
            },
            resetSearch: function () {
                p.query = '';
                p.qtype = 'reset';
              p.newp = 1;
                if(onSearch)
                    p.onSearch();
                this.populate();
            },
            refresh: function () {
                if(p)
            {
            p.newp= p.page;
            }
                
                this.populate(null, null, null, true);
            },
            customQuery: function (target, postData) {
                this.populate(target, postData);
            },
            changePage: function (ctype, callback) { //change page
            
                if (this.loading) {
                    return true;
                }
                switch (ctype) {
                    case 'first':
                        p.newp = 1;
                        break;
                    case 'prev':
                        if (p.page > 1) {
                            p.newp = parseInt(p.page) - 1;
                        }
                        break;
                    case 'next':
                        if (p.page < p.pages) {
                            p.newp = parseInt(p.page) + 1;
                        }
                        else
                        return false;
                        break;
                    case 'last':
                        p.newp = p.pages;
                        break;
                    case 'input':
                        var nv = parseInt($('.pcontrol input', this.pDiv).val());
                        if (isNaN(nv)) {
                            nv = 1;
                        }
                        if (nv < 1) {
                            nv = 1;
                        } else if (nv > p.pages) {
                            nv = p.pages;
                        }
                        $('.pcontrol input', this.pDiv).val(nv);
                        p.newp = nv;
                        break;
                }
                if (p.newp == p.page) {
                    return false;
                }
                if (p.onChangePage) {
                    p.onChangePage(p.newp);
                } else {
                    this.populate(null, null, callback, true);
                }
            },
            addCellProp: function () {
                $('tbody tr td', g.bDiv).each(function () {
                    var tdDiv = document.createElement('div');
                    var n = $('td', $(this).parent()).index(this);
                    var pth = $('th:eq(' + n + ')', g.hDiv).get(0);
                    if (pth != null) {
                        if (p.sortname == $(pth).attr('abbr') && p.sortname) {
                            this.className = 'sorted';
                        }
                        $(tdDiv).css({
                            textAlign: pth.align,
                            width: $('div:first', pth)[0].style.width
                        });
                        if (pth.hidden) {
                            $(this).css('display', 'none');
                        }
                    }
                    if (p.nowrap == false) {
                        $(tdDiv).css('white-space', 'normal');
                    }
                    if (this.innerHTML == '') {
                        this.innerHTML = '&nbsp;';
                    }
                    tdDiv.innerHTML = this.innerHTML;
                    var prnt = $(this).parent()[0];
                    var pid = false;
                    if (prnt.id) {
                        pid = prnt.id.substr(3);
                    }
                    if (pth != null) {
                        if (pth.process) pth.process(tdDiv, pid);
                    }
                    $(this).empty().append(tdDiv).removeAttr('width'); //wrap content
                });
            },
            getCellDim: function (obj) {// get cell prop for editable event
                var ht = parseInt($(obj).height());
                var pht = parseInt($(obj).parent().height());
                var wt = parseInt(obj.style.width);
                var pwt = parseInt($(obj).parent().width());
                var top = obj.offsetParent.offsetTop;
                var left = obj.offsetParent.offsetLeft;
                var pdl = parseInt($(obj).css('paddingLeft'));
                var pdt = parseInt($(obj).css('paddingTop'));
                return {
                    ht: ht,
                    wt: wt,
                    top: top,
                    left: left,
                    pdl: pdl,
                    pdt: pdt,
                    pht: pht,
                    pwt: pwt
                };
            },
            addRowProp: function () {
                $('tbody tr', g.bDiv).each(function () {
                    $(this).click(function (e) {
                        var obj = (e.target || e.srcElement);
                        if (obj.href || obj.type) return true;
                        $(this).toggleClass('trSelected');
                        if (p.singleSelect) $(this).siblings().removeClass('trSelected');
                        if (p.onSelect != null)
                            p.onSelect(this.id.substr(3), this);
                    }).mousedown(function (e) {
                        if (e.shiftKey) {
                            $(this).toggleClass('trSelected');
                            g.multisel = true;
                            this.focus();
                            $(g.gDiv).noSelect();
                        }
                    }).mouseup(function () {
                        if (g.multisel) {
                            g.multisel = false;
                            $(g.gDiv).noSelect(false);
                        }
                    }).dblclick(function () {
                        if (p.onDblClick != null)
                            p.onDblClick(this.id.substr(3), this);
                    }).hover(function (e) {
                        if (g.multisel) {
                            $(this).toggleClass('trSelected');
                        }
                    }, function () { });
                    if ($.browser.msie && $.browser.version < 7.0) {
                        $(this).hover(function () {
                            $(this).addClass('trOver');
                        }, function () {
                            $(this).removeClass('trOver');
                        });
                    }
                });
            },
            pager: 0
        };
        if (p.colModel) { //create model if any
            thead = document.createElement('thead');
            var tr = document.createElement('tr');
            for (var i = 0; i < p.colModel.length; i++) {
                var cm = p.colModel[i];
                var th = document.createElement('th');
                th.innerHTML = cm.display;
                if (cm.sortname && cm.sortable) {
                    $(th).attr('abbr', cm.sortname);
                }
                $(th).attr('field', cm.name);
                $(th).attr('colid', cm.Id);
                $(th).attr('axis', 'col' + i);
                if (cm.align) {
                    th.align = cm.align;
                }
                if (cm.width) {
                    $(th).attr('width', cm.width);
                }
                if ($(cm).attr('hide')) {
                    th.hidden = true;
                }
                if (cm.process) {
                    th.process = cm.process;
                }
                $(tr).append(th);
            }
            $(thead).append(tr);
            $(t).prepend(thead);
        } // end if p.colmodel
        //init divs
        g.gDiv = document.createElement('div'); //create global container
        g.mDiv = document.createElement('div'); //create title container
        g.hDiv = document.createElement('div'); //create header container
        g.bDiv = document.createElement('div'); //create body container
        g.vDiv = document.createElement('div'); //create grip
        g.rDiv = document.createElement('div'); //create horizontal resizer
        g.cDrag = document.createElement('div'); //create column drag
        g.block = document.createElement('div'); //creat blocker
        g.nDiv = document.createElement('div'); //create column show/hide popup
        g.nBtn = document.createElement('div'); //create column show/hide button
        g.iDiv = document.createElement('div'); //create editable layer
        g.tDiv = document.createElement('div'); //create toolbar
        g.sDiv = document.createElement('div');
        g.pDiv = document.createElement('div'); //create pager container
        if (!p.usepager) {
            g.pDiv.style.display = 'none';
        }
        g.hTable = document.createElement('table');
        g.gDiv.className = 'flexigrid';
        if (p.width != 'auto') {
            g.gDiv.style.width = p.width + 'px';
        }
        //add conditional classes
        if ($.browser.msie) {
            $(g.gDiv).addClass('ie');
        }
        if (p.novstripe) {
            $(g.gDiv).addClass('novstripe');
        }
        $(t).before(g.gDiv);
        $(g.gDiv).append(t);
        //set toolbar
        if (p.buttons) {
            g.tDiv.className = 'tDiv';
            var tDiv2 = document.createElement('div');
            tDiv2.className = 'tDiv2';
            for (var i = 0; i < p.buttons.length; i++) {
                var btn = p.buttons[i];
                if (!btn.separator) {
                    var btnDiv = document.createElement('div');
                    btnDiv.className = 'fbutton';
                    btnDiv.innerHTML = "<div><span>" + btn.name + "</span></div>";
                    if (btn.bclass) $('span', btnDiv).addClass(btn.bclass).css({
                        paddingLeft: 20
                    });
                    btnDiv.onpress = btn.onpress;
                    btnDiv.name = btn.name;
                    if (btn.onpress) {
                        $(btnDiv).click(function () {
                            this.onpress(this.name, g.gDiv);
                        });
                    }
                    $(tDiv2).append(btnDiv);
                    if ($.browser.msie && $.browser.version < 7.0) {
                        $(btnDiv).hover(function () {
                            $(this).addClass('fbOver');
                        }, function () {
                            $(this).removeClass('fbOver');
                        });
                    }
                } else {
                    $(tDiv2).append("<div class='btnseparator'></div>");
                }
            }
            $(g.tDiv).append(tDiv2);
            $(g.tDiv).append("<div style='clear:both'></div>");
            $(g.gDiv).prepend(g.tDiv);
        }
        g.hDiv.className = 'hDiv';
        $(t).before(g.hDiv);
        g.hTable.cellPadding = 0;
        g.hTable.cellSpacing = 0;
        $(g.hDiv).append('<div class="hDivBox"></div>');
        $('div', g.hDiv).append(g.hTable);
        var thead = $("thead:first", t).get(0);
        if (thead) $(g.hTable).append(thead);
        thead = null;
        if (!p.colmodel) var ci = 0;
        $('thead tr:first th', g.hDiv).each(function () {
            var thdiv = document.createElement('div');
            if ($(this).attr('abbr')) {
                $(this).click(function (e) {
                    $(this).addClass('thOver');
                    if (!$(this).hasClass('thOver')) return false;
                    var obj = (e.target || e.srcElement);
                    if (obj.href || obj.type) return true;
                    g.changeSort(this);
                });
                if ($(this).attr('abbr') == p.sortname) {
                    this.className = 'sorted';
                    thdiv.className = 's' + p.sortorder;
                }
            }
            if (this.hidden) {
                $(this).hide();
            }
            if (!p.colmodel) {
                $(this).attr('axis', 'col' + ci++);
            }
            $(thdiv).css({
                textAlign: this.align,
                width: this.width + 'px'
            });
            thdiv.innerHTML = this.innerHTML;
            $(this).empty().append(thdiv).removeAttr('width').mousedown(function (e) {
                g.dragStart('colMove', e, this);
            }).hover(function () {
                if (!g.colresize && !$(this).hasClass('thMove') && !g.colCopy) {
                    $(this).addClass('thOver');
                }
                if ($(this).attr('abbr') != p.sortname && !g.colCopy && !g.colresize && $(this).attr('abbr')) {
                    $('div', this).addClass('s' + p.sortorder);
                } else if ($(this).attr('abbr') == p.sortname && !g.colCopy && !g.colresize && $(this).attr('abbr')) {
                    var no = (p.sortorder == 'asc') ? 'desc' : 'asc';
                    $('div', this).removeClass('s' + p.sortorder).addClass('s' + no);
                }
                if (g.colCopy) {
                    var n = $('th', g.hDiv).index(this);
                    if (n == g.dcoln) {
                        return false;
                    }
                    if (n < g.dcoln) {
                        $(this).append(g.cdropleft);
                    } else {
                        $(this).append(g.cdropright);
                    }
                    g.dcolt = n;
                } else if (!g.colresize) {
                    var nv = $('th:visible', g.hDiv).index(this);
                    var onl = parseInt($('div:eq(' + nv + ')', g.cDrag).css('left'));
                    var nw = jQuery(g.nBtn).outerWidth();
                    var nl = onl - nw + Math.floor(p.cgwidth / 2);
                    $(g.nDiv).hide();
                    $(g.nBtn).hide();
                    $(g.nBtn).css({
                        'left': nl,
                        top: g.hDiv.offsetTop
                    }).show();
                    var ndw = parseInt($(g.nDiv).width());
                    $(g.nDiv).css({
                        top: g.bDiv.offsetTop
                    });
                    if ((nl + ndw) > $(g.gDiv).width()) {
                        $(g.nDiv).css('left', onl - ndw + 1);
                    } else {
                        $(g.nDiv).css('left', nl);
                    }
                    if ($(this).hasClass('sorted')) {
                        $(g.nBtn).addClass('srtd');
                    } else {
                        $(g.nBtn).removeClass('srtd');
                    }
                }
            }, function () {
                $(this).removeClass('thOver');
                if ($(this).attr('abbr') != p.sortname) {
                    $('div', this).removeClass('s' + p.sortorder);
                } else if ($(this).attr('abbr') == p.sortname) {
                    var no = (p.sortorder == 'asc') ? 'desc' : 'asc';
                    $('div', this).addClass('s' + p.sortorder).removeClass('s' + no);
                }
                if (g.colCopy) {
                    $(g.cdropleft).remove();
                    $(g.cdropright).remove();
                    g.dcolt = null;
                }
            }); //wrap content
        });
        //set bDiv
        g.bDiv.className = 'bDiv';
        $(t).before(g.bDiv);
        $(g.bDiv).css({
            height: (p.height == 'auto') ? 'auto' : p.height + "px"
        }).scroll(function (e) {
            g.scroll()
        }).append(t);
        if (p.height == 'auto') {
            $('table', g.bDiv).addClass('autoht');
        }
        //add td & row properties
        g.addCellProp();
        g.addRowProp();
        //set cDrag
        var cdcol = $('thead tr:first th:first', g.hDiv).get(0);
        if (cdcol != null) {
            g.cDrag.className = 'cDrag';
            g.cdpad = 0;
            g.cdpad += (isNaN(parseInt($('div', cdcol).css('borderLeftWidth'))) ? 0 : parseInt($('div', cdcol).css('borderLeftWidth')));
            g.cdpad += (isNaN(parseInt($('div', cdcol).css('borderRightWidth'))) ? 0 : parseInt($('div', cdcol).css('borderRightWidth')));
            g.cdpad += (isNaN(parseInt($('div', cdcol).css('paddingLeft'))) ? 0 : parseInt($('div', cdcol).css('paddingLeft')));
            g.cdpad += (isNaN(parseInt($('div', cdcol).css('paddingRight'))) ? 0 : parseInt($('div', cdcol).css('paddingRight')));
            g.cdpad += (isNaN(parseInt($(cdcol).css('borderLeftWidth'))) ? 0 : parseInt($(cdcol).css('borderLeftWidth')));
            g.cdpad += (isNaN(parseInt($(cdcol).css('borderRightWidth'))) ? 0 : parseInt($(cdcol).css('borderRightWidth')));
            g.cdpad += (isNaN(parseInt($(cdcol).css('paddingLeft'))) ? 0 : parseInt($(cdcol).css('paddingLeft')));
            g.cdpad += (isNaN(parseInt($(cdcol).css('paddingRight'))) ? 0 : parseInt($(cdcol).css('paddingRight')));
            $(g.bDiv).before(g.cDrag);
            var cdheight = $(g.bDiv).height();
            var hdheight = $(g.hDiv).height();
            $(g.cDrag).css({
                top: -hdheight + 'px'
            });
            $('thead tr:first th', g.hDiv).each(function () {
                var cgDiv = document.createElement('div');
                $(g.cDrag).append(cgDiv);
                if (!p.cgwidth) {
                    p.cgwidth = $(cgDiv).width();
                }
                $(cgDiv).css({
                    height: cdheight + hdheight
                }).mousedown(function (e) {
                    g.dragStart('colresize', e, this);
                });
                if ($.browser.msie && $.browser.version < 7.0) {
                    g.fixHeight($(g.gDiv).height());
                    $(cgDiv).hover(function () {
                        g.fixHeight();
                        $(this).addClass('dragging')
                    }, function () {
                        if (!g.colresize) $(this).removeClass('dragging')
                    });
                }
            });
        }
        //add strip
        if (p.striped) {
            $('tbody tr:odd', g.bDiv).addClass('erow');
        }
        if (p.resizable && p.height != 'auto') {
            g.vDiv.className = 'vGrip';
            $(g.vDiv).mousedown(function (e) {
                g.dragStart('vresize', e)
            }).html('<span></span>');
            $(g.bDiv).after(g.vDiv);
        }
        if (p.resizable && p.width != 'auto' && !p.nohresize) {
            g.rDiv.className = 'hGrip';
            $(g.rDiv).mousedown(function (e) {
                g.dragStart('vresize', e, true);
            }).html('<span></span>').css('height', $(g.gDiv).height());
            if ($.browser.msie && $.browser.version < 7.0) {
                $(g.rDiv).hover(function () {
                    $(this).addClass('hgOver');
                }, function () {
                    $(this).removeClass('hgOver');
                });
            }
            $(g.gDiv).append(g.rDiv);
        }
        // add pager
        if (p.usepager) {
      
            g.pDiv.className = 'pDiv';
            g.pDiv.innerHTML = '<div class="pDiv2"></div>';
            $(g.bDiv).after(g.pDiv);
            var html = ' <div class="pGroup"> <div class="pFirst pButton"><span></span></div><div class="pPrev pButton"><span></span></div> </div> <div class="btnseparator"></div> <div class="pGroup"><span class="pcontrol">' + p.pagetext + ' <input type="text" size="4" value="1" /> ' + p.outof + ' <span> 1 </span></span></div> <div class="btnseparator"></div> <div class="pGroup"> <div class="pNext pButton"><span></span></div><div class="pLast pButton"><span></span></div> </div> <div class="btnseparator"></div> <div class="pGroup"> <div class="pReload pButton"><span></span></div> </div> <div class="btnseparator"></div> <div class="pGroup"><span class="pPageStat"></span></div><a class="adhoc-creator-link grid-link-button tip" abbr="Create an Ad-Hoc Assignment Group from the parcels listed below in the Parcel Search Results. You will be <br> re-directed to the Appraisal Task Control screen to complete the Ad-Hoc Assignment Group process.<br/><br/> You must have the Appraisal Task Control user role / link to use this feature." tip-margin-top="-23" tip-margin-left="-665"  tip-bottom="4" style="display:none;" onclick="return openAdhocCreator();">Create AdHoc</a><a class="grid-export-link grid-link-button tip" tip-margin-top="-23" tip-margin-left="-619" abbr="Export the Parcel Search Results to csv or xlsx file. The record limit to be exported is 30,000." style="display: none;" onclick="return showexportPopup();">Export</a>';
            $('div', g.pDiv).html(html);
            $('.pReload', g.pDiv).click(function () {
                if (__DTR && $('.app-dtr-selected-task option').length == 0)
                    doSearch();
                else {
                    let parcelResultsElement = $(".parcel-results")[0];
                    if (parcelResultsElement && parcelResultsElement.p && parcelResultsElement.p.total <= 1) {
                        return true;
                    }
                    g.populate()
                }
            });
            $('.pFirst', g.pDiv).click(function () {
                g.changePage('first')
            });
            $('.pPrev', g.pDiv).click(function () {
                g.changePage('prev')
            });
            $('.pNext', g.pDiv).click(function () {
                g.changePage('next')
            });
            $('.pLast', g.pDiv).click(function () {
                g.changePage('last')
            });
            $('.pcontrol input', g.pDiv).keydown(function (e) {
               // if (e.keyCode == 13) g.changePage('input')
                 if ((e.keyCode == 13)||(e.keyCode == 9)) {g.changePage('input'); e.preventDefault();}
            });
            if ($.browser.msie && $.browser.version < 7) $('.pButton', g.pDiv).hover(function () {
                $(this).addClass('pBtnOver');
            }, function () {
                $(this).removeClass('pBtnOver');
            });
            if (p.useRp) {
                var opt = '',
					sel = '';
                for (var nx = 0; nx < p.rpOptions.length; nx++) {
                    if (p.rp == p.rpOptions[nx]) sel = 'selected="selected"';
                    else sel = '';
                    opt += "<option value='" + p.rpOptions[nx] + "' " + sel + " >" + p.rpOptions[nx] + "&nbsp;&nbsp;</option>";
                }
                $('.pDiv2', g.pDiv).prepend("<div class='pGroup'><select name='rp'>" + opt + "</select></div> <div class='btnseparator'></div>");
                $('select', g.pDiv).change(function () {
                    if (p.onRpChange) {
                        p.onRpChange(+this.value);
                    } else {
                        p.newp = 1;
                        p.rp = +this.value;
                     if(__DTR && $('.app-dtr-selected-task option').length == 0)
                           doSearch();
                       else
                            g.populate(null, null, null, true);
                    }
                });
            }
            //add search button
            if (p.searchitems) {
                $('.pDiv2', g.pDiv).prepend("<div class='pGroup'> <div class='pSearch pButton'><span></span></div> </div>  <div class='btnseparator'></div>");
                $('.pSearch', g.pDiv).click(function () {
                    $(g.sDiv).slideToggle('fast', function () {
                        $('.sDiv:visible input:first', g.gDiv).trigger('focus');
                    });
                });
                //add search box
                g.sDiv.className = 'sDiv';
                var sitems = p.searchitems;
                var sopt = '', sel = '';
                for (var s = 0; s < sitems.length; s++) {
                    if (p.qtype == '' && sitems[s].isdefault == true) {
                        p.qtype = sitems[s].name;
                        sel = 'selected="selected"';
                    } else {
                        sel = '';
                    }
                    sopt += "<option value='" + sitems[s].name + "' " + sel + " >" + sitems[s].display + "&nbsp;&nbsp;</option>";
                }
                if (p.qtype == '') {
                    p.qtype = sitems[0].name;
                }
                //This is the search UI. Sarath Bookmatked this.
                $(g.sDiv).append("<div class='sDiv2'>" + p.findtext +
						" <input type='text' value='" + p.query + "' size='30' name='q' class='qsbox' /> " +
						" <select name='qtype'>" + sopt + "</select></div>");
                //Split into separate selectors because of bug in jQuery 1.3.2
                $('input[name=q]', g.sDiv).keydown(function (e) {
                    if (e.keyCode == 13) {
                        g.doSearch();
                    }
                });
                $('select[name=qtype]', g.sDiv).keydown(function (e) {
                    if (e.keyCode == 13) {
                        g.doSearch();
                    }
                });
                $('input[value=Clear]', g.sDiv).click(function () {
                    $('input[name=q]', g.sDiv).val('');
                    p.query = '';
                    g.doSearch();
                });
                $(g.bDiv).after(g.sDiv);
            }
        }
        $(g.pDiv, g.sDiv).append("<div style='clear:both'></div>");
        // add title
        if (p.title) {
            g.mDiv.className = 'mDiv';
            g.mDiv.innerHTML = '<div class="ftitle"><span class="app-search-results-title">' + p.title + '</span><span class="lastDownSync" abbr="This will display the time of the last DownSync.<br/> This will display in red if more than 24 hours ago." style="display:none;"><span>Last DownSync:</span>&nbsp;<span class="lastDownSyncDate"></span></span><span class="lastUpSync" abbr="This will display the time of the last UpSync.<br/> This will display in red if more than 24 hours ago." style="display:none;"><span>Last UpSync:</span>&nbsp;<span class="lastUpSyncDate"></span></span><a class="back-to-priority" style="display:none;" onclick="backToPriorityList();">&#10094; Back</a></a><a class="bulk-edit-link tip" abbr="Bulk Edit" style="display:none; float:right; margin-left: 5px" tip-margin-left= "-92"  onclick="return openBulkEditor();"/>Bulk Edit<a class="customize-grid tip" tip-margin-left= "-336" abbr="The Column Chooser will allow for fields  to be <br/> added / removed to the Parcel Search Results.<br/> This setup is system-wide, <b>not</b>, per user. Fields that <br/> can be displayed  here are parcel-level fields only <br/> i.e.fields that  have a  one-to-one  relationship<br/> with a parcel." style="display:inline-block; float:right" onclick="return showPopup();"></a> <div class="col-seperator-grid" style="display:none"></div></div>';
            $(g.gDiv).prepend(g.mDiv);
            if (p.showTableToggleBtn) {
                $(g.mDiv).append('<div class="ptogtitle" title="Minimize/Maximize Table"><span></span></div>');
                $('div.ptogtitle', g.mDiv).click(function () {
                    $(g.gDiv).toggleClass('hideBody');
                    $(this).toggleClass('vsble');
                });
            }
        }
        //setup cdrops
        g.cdropleft = document.createElement('span');
        g.cdropleft.className = 'cdropleft';
        g.cdropright = document.createElement('span');
        g.cdropright.className = 'cdropright';
        //add block
        g.block.className = 'gBlock';
        var gh = $(g.bDiv).height();
        var gtop = g.bDiv.offsetTop;
        $(g.block).css({
            width: g.bDiv.style.width,
            height: gh,
            background: 'white',
            position: 'relative',
            marginBottom: (gh * -1),
            zIndex: 1,
            top: gtop,
            left: '0px'
        });
        $(g.block).fadeTo(0, p.blockOpacity);
        // add column control
        if ($('th', g.hDiv).length) {
            g.nDiv.className = 'nDiv';
            g.nDiv.innerHTML = "<table cellpadding='0' cellspacing='0'><tbody></tbody></table>";
            $(g.nDiv).css({
                marginBottom: (gh * -1),
                display: 'none',
                top: gtop
            }).noSelect();
            var cn = 0;
            $('th div', g.hDiv).each(function () {
                var kcol = $("th[axis='col" + cn + "']", g.hDiv)[0];
                var chk = 'checked="checked"';
                if (kcol.style.display == 'none') {
                    chk = '';
                }
                $('tbody', g.nDiv).append('<tr><td class="ndcol1"><input type="checkbox" ' + chk + ' class="togCol" value="' + cn + '" /></td><td class="ndcol2">' + this.innerHTML + '</td></tr>');
                cn++;
            });
            if ($.browser.msie && $.browser.version < 7.0) $('tr', g.nDiv).hover(function () {
                $(this).addClass('ndcolover');
            }, function () {
                $(this).removeClass('ndcolover');
            });
            $('td.ndcol2', g.nDiv).click(function () {
                if ($('input:checked', g.nDiv).length <= p.minColToggle && $(this).prev().find('input')[0].checked) return false;
                return g.toggleCol($(this).prev().find('input').val());
            });
            $('input.togCol', g.nDiv).click(function () {
                if ($('input:checked', g.nDiv).length < p.minColToggle && this.checked == false) return false;
                $(this).parent().next().trigger('click');
            });
            $(g.gDiv).prepend(g.nDiv);
            $(g.nBtn).addClass('nBtn')
				.html('<div></div>')
				.attr('title', 'Hide/Show Columns')
				.click(function () {
				    $(g.nDiv).toggle();
				    return true;
				}
			);
            if (p.showToggleBtn) {
                $(g.gDiv).prepend(g.nBtn);
            }
        }
        // add date edit layer
        $(g.iDiv).addClass('iDiv').css({
            display: 'none'
        });
        $(g.bDiv).append(g.iDiv);
        // add flexigrid events
        $(g.bDiv).hover(function () {
            $(g.nDiv).hide();
            $(g.nBtn).hide();
        }, function () {
            if (g.multisel) {
                g.multisel = false;
            }
        });
        $(g.gDiv).hover(function () { }, function () {
            $(g.nDiv).hide();
            $(g.nBtn).hide();
        });
        //add document events
        $(document).mousemove(function (e) {
            g.dragMove(e)
        }).mouseup(function (e) {
            g.dragEnd()
        }).hover(function () { }, function () {
            g.dragEnd()
        });
        //browser adjustments
        if ($.browser.msie && $.browser.version < 7.0) {
            $('.hDiv,.bDiv,.mDiv,.pDiv,.vGrip,.tDiv, .sDiv', g.gDiv).css({
                width: '100%'
            });
            $(g.gDiv).addClass('ie6');
            if (p.width != 'auto') {
                $(g.gDiv).addClass('ie6fullwidthbug');
            }
        }
        g.rePosDrag();
        g.fixHeight();
        //make grid functions accessible
        t.p = p;
        t.grid = g;
        // load data
        if (p.url && p.autoload) {
            g.populate();
        }
        return t;
    };
    var docloaded = false;
    $(document).ready(function () {
        docloaded = true
    });
    $.fn.flexigrid = function (p) {
        return this.each(function () {
            if (!docloaded) {
                $(this).hide();
                var t = this;
                $(document).ready(function () {
                    $.addFlex(t, p);
                });
            } else {
                $.addFlex(this, p);
            }
        });
    }; //end flexigrid
    $.fn.flexReload = function (p) { // function to reload grid
        return this.each(function () {
            if (this.grid && this.p.url) this.grid.populate();
        });
    }; //end flexReload
    $.fn.flexOptions = function (p) { //function to update general options
        if(typeof p == 'string'){
            //if (this.grid) return this.p[p];
            var value;
            this.each(function () {
                if (this.grid) value= this.p[p];
                return;
            });
            return value;
        } else {
            return this.each(function () {
                if (this.grid) $.extend(this.p, p);
            });
        };
    }; //end flexOptions
    $.fn.flexToggleCol = function (cid, visible) { // function to reload grid
        return this.each(function () {
            if (this.grid) this.grid.toggleCol(cid, visible);
        });
    }; //end flexToggleCol
    $.fn.flexAddData = function (data) { // function to add data to grid
        return this.each(function () {
            if (this.grid) this.grid.addData(data);
        });
    };
    $.fn.noSelect = function (p) { //no select plugin by me :-)
        var prevent = (p == null) ? true : p;
        if (prevent) {
            return this.each(function () {
                if ($.browser.msie || $.browser.safari) $(this).bind('selectstart', function () {
                    return false;
                });
                else if ($.browser.mozilla) {
                    $(this).css('MozUserSelect', 'none');
                    $('body').trigger('focus');
                } else if ($.browser.opera) $(this).bind('mousedown', function () {
                    return false;
                });
                else $(this).attr('unselectable', 'on');
            });
        } else {
            return this.each(function () {
                if ($.browser.msie || $.browser.safari) $(this).unbind('selectstart');
                else if ($.browser.mozilla) $(this).css('MozUserSelect', 'inherit');
                else if ($.browser.opera) $(this).unbind('mousedown');
                else $(this).removeAttr('unselectable', 'on');
            });
        }
    }; //end noSelect

//    $.fn.Search = function (query) {
//        return this.each(function () {
//            if (this.grid) this.grid.customSearch(query);
//        });
//    };

//    $.fn.PowerSearch = function (query) {
//        return this.each(function () {
//            if (this.grid) this.grid.powerSearch(query);
//        });
//    };

    $.fn.ResetSearch = function () {
        return this.each(function () {
            if (this.grid) this.grid.resetSearch();
        });
    };

    $.fn.SetHeight = function (h) {
        return this.each(function () {
            //if (this.grid) this.grid.fixHeight(h);
            if (this.grid) this.grid.setHeight(h);
        });
    };
    $.fn.SetWidth = function (w) {
        return this.each(function () {
            if (this.grid) this.grid.setWidth(w);
        });
    };

    $.fn.ChangePage = function (pg, callback) {
        return this.each(function () {
            //if (this.grid) this.grid.fixHeight(h);
            if (this.grid) this.grid.changePage(pg, callback);
        });
    };


    $.fn.Refresh = function () {
        return this.each(function () {
            if (this.grid) this.grid.refresh();
        });
    };

    $.fn.Search = function (params) {
        return this.each(function () {
            if (this.grid)   this.grid.populate(null, params);
          
        });
      
    };

})(jQuery);


				
function appendSearchQuery(query,field)
{
	if($(field).val() != "")
	{
		if(query != "")
			query += "|";
		query += ($(field).attr('field')) + " LIKE:%" + ($(field).val()) + "%";
	}
	return query;
}

function appendValueQuery(query,field,operator, suffix)
{
	if(suffix == null)
		suffix = '';
	if(operator == null)
		operator = "=";
	if($(field).val() != "")
	{
		if(query != "")
			query += "|";
		query += ($(field).attr('field')) + " " + operator + ":" + ($(field).val() + suffix) + "";
	}
	return query;
}

