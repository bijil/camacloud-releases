﻿
CAMACloud.Sketching.Formatters.WinGap = {
    name: "WinGap for Georgia",
    saveAllSegments: false,
    allowSegmentAddition: false,
    allowSegmentDeletion: false,
    arcMode: 'ARC',
    preventSavingOpenSegments: false,
    allowSegmentAdditionOnMidsketch: true,
    originPosition: "topRight",
    nodeToString: function (n, e) {
            var str = "";
            var nn = n.nextNode;
            if(nn != n.endNode){
                if(nn && nn.isArc){
                    str = "A" + (nn.arcLength > 0 ? 'CW' : 'CC') + sRound(n.p.x) + ',' + sRound(-n.p.y) + ',' + sRound(nn.arcLength);
                } else {
                    str = sRound(n.p.x) + ',' + sRound(-n.p.y);
                }
            }
            return str;
    },
    vectorToString: function (v) {
        var str = "";
        if (v.startNode != null) {
            str += v.startNode.vectorString();
            var nn = v.startNode.nextNode;
            while (nn != null) {
                str += ";" + nn.vectorString();
                nn = nn.nextNode;
            }
            //str += ';'
        }
        return str;
    },
    vectorToLabelCommand: function (v) {
        //var str = "";
        //var sx = 0, sy = 0, n = 0;
        //if (v.startNode != null) {
        //    var nn = v.startNode.nextNode;
        //    while (nn != null) {
        //        sx += sRound(nn.p.x);
        //        sy += sRound(nn.p.y);
        //        n++;
        //        nn = nn.nextNode;
        //    }
        //    str += ';'
        //}
        //if (n == 0) {
        //    if (v.startNode) {
        //        str = v.startNode.p.x + ',' + (-v.startNode.p.y) + ',' + v.name.trim() + ',0;'
        //    } else {
        //        str = "";
        //    }
        //    return str;
        //}
        if (!v.labelPosition)
            return "";
        var cx = v.labelPosition;
        //var cy = sRound(sy / n);
        str = cx.x + ',' + (-cx.y) + ',' + (v.name ? v.name.trim() : "") + ',0;'
        return str;
    },
    vectorToDimensionCommand: function (v) {
        var str = "";
        var ln;
        if (v.startNode != null) {
            ln = v.startNode;
            var nn = v.startNode.nextNode;
            while (nn != null) {
                var mx = sRound(ln.p.x + nn.p.x) / 2;
                var my = -sRound(ln.p.y + nn.p.y) / 2;
                var l = nn.length;
                var dim = mx + ',' + my + ',' + parseFloat(l.toFixed(0)) + ',0;'
                str += dim;
                ln = nn;
                nn = nn.nextNode;
            }
        }
        return str;
    },
    labelPositionFromString: function (editor, l) {
        if (!l)
            return null
        var parts = l.split(',');
        return new PointX(parseFloat(parts[0]), -(parseFloat(parts[1])))
    },
    vectorFromString: function (editor, s) {
      if (s == null || s == '') {
           var nv = new Vector(editor);
           nv.isClosed = false;
           nv.label = "Blank"
           nv.uid = -1
           return nv;
       }

       var o = editor.origin;
       var p = o.copy();

       var v = new Vector(editor);

       var ins = s.split(';');
       var isStart = true;
       var hasStarted = false;
       var lastIns = null, lastArcLength = 0;
       for (var i in ins) {
			var vstr = ins[i]
	   		if (!vstr) continue;
			var isArc = false, arcLength = 0;
			if(vstr[0] == 'A'){
			isArc = true;
			var arcType = vstr.substr(1,2);
			vstr = vstr.substr(3);
			var vstr_parts = vstr.split(',')
			arcLength = parseFloat(vstr_parts[2]);
			vstr_parts.pop();
			vstr = vstr_parts.join(',')
	  } 
           
           var pxy = vstr.split(",");
           if (lastIns == vstr) continue;
           if (pxy.length != 2) continue;

           var px = parseFloat(pxy[0]);
           var py = parseFloat(pxy[1]);

           var p = new PointX(px, -py);

           if (isStart) {
               v.start(p.copy(), true);
               hasStarted = true;
               isStart = false;
           } else if (hasStarted) {
               var q = p.copy(); //.alignToGrid(editor);
               if (!v.startNode.overlaps(q))
                   v.connect(p.copy(), true);
               else
                   v.terminateNode(v.startNode);
           }

			if(v.endNode && lastArcLength != 0) { v.endNode.arcLength = lastArcLength;  v.endNode.isArc = true;};

           	lastIns = vstr;
			lastArcLength = arcLength;
      }
       //WinGap: Allow auto-closing of vectors, if they aren't closed by default.
       if (v.startNode)
           if (!v.startNode.overlaps(v.endNode.p.copy())) {
               v.terminateNode(v.startNode);
           }
           
           if(v.endNode && lastArcLength != 0) { v.endNode.arcLength = lastArcLength;  v.endNode.isArc = true;};
       v.isClosed = true;
       return v;
    },
    open: function (editor, data) {
        editor.vectors = [];
        editor.sketches = [];
        let isError = false;

        for (var x in data) {
            var sketch = new Sketch(editor);
            var s = data[x];
            sketch.parentRow = s.parentRow;
            sketch.uid = s.uid;
            sketch.label = s.label;
            sketch.vectors = [];
            sketch.config = s.config || {};
            sketch.sid = s.sid;
            sketch.lookUp = s.lookUp;
            sketch.maxNotelen = s.maxNotelen;
            var counter = 0;
            try {
                for (var i in s.sketches) {
                    counter += 1;
                    var sk = s.sketches[i];
                    var v = this.vectorFromString(editor, sk.vector);
                    v.sketch = sketch;
                    v.uid = sk.uid;
                    v.index = counter;
                    v.name = sk.label[0].Value;
                    if (sk.label[0] && sk.label[0].colorCode) {
                        var clrcode = sk.label[0].colorCode.split('~');
                        var isdtr = window.opener ? window.opener.__DTR : (typeof (__DTR) !== 'undefined' ? __DTR : false);
                        let isSvApp = window.opener && window.opener.appType == "sv" ? true : (typeof (appType) !== 'undefined' && appType == "sv" ? true : false);
                        var DQC = isdtr ? 'DTR' : (isSvApp ? 'SV' : 'QC');
                        v.colorCode = clrcode[0] ? clrcode[0] : null;
                        v.newLineColor = (clrcode[1] && sketchSettings['SketchLineColorCustomizations'] && sketchSettings['SketchLineColorCustomizations'].contains(DQC)) ? clrcode[1] : null;
                        v.newLabelColorCode = (clrcode[2] && sketchSettings['SketchTextColorCustomizations'] && sketchSettings['SketchTextColorCustomizations'].contains(DQC)) ? clrcode[2] : null;
                    }
                    v.label = '[' + counter + '] ' + sk.label.map(function (a) { return ((a.Description && sketchSettings["DoNotShowLabelDescriptionSketch"] != '1' && clientSettings["DoNotShowLabelDescriptionSketch"] != '1' && a.Description.Name) ? ((sketchSettings["DoNotShowLabelCode"] == '1' || a.showLabelDescriptionOnly) ? a.Description.Name : a.Value + '-' + a.Description.Name) : a.Value) }).filter(function (a) { return a }).join('/');
                    v.labelFields = sk.label;
                    v.vectorString = sk.vector;
                    v.vectorConfig = sk.vectorConfig;
                    v.isChanged = sk.isChanged;
                    v.labelPosition = this.labelPositionFromString(editor, sk.labelPosition);
                    editor.vectors.pop();
                    sketch.vectors.push(v);
                }
            }
            catch (e) {
                editor.vectors = []; sketch.vectors = []; isError = true;
            }
            sketch.isModified = false;
            editor.loadNotesForSketch(sketch, s.notes);
            editor.sketches.push(sketch);
        }

        if (isError) {
            editor.sketches.forEach((sk) => { sk.vectors = []; });
            throw "Sketch cannot be rendered due to the wrong sketch data.";
        }
    } 
}
