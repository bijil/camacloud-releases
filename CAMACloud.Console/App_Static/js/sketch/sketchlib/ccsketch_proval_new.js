﻿                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          //Rinsheed - 28-07-2018
/*var Proval_Lookup = [];
var Proval_Default_Exterior_Cover = [];
var Proval_Outbuliding_Lookup = [];
var Proval_sketch_codes = [];*/
var extension_Recd_Array = [];
var lookup;
var saveData = '';
var parcel;

function convertPointToStr(p) {
    if ( !p ) return '';
    p = p.toString();
    var d = p.split( '/' ),arclength;
    if (p.length > 1) {
        p = d[0];
        arclength = d[1];
    }
    var x = p << 16 >> 16;
    var y = p >> 16;
    var str = '';
    if (x != 0) {
        var d = x < 0 ? 'L' : 'R';
        str += d + Math.abs( x / 10 );
    }
    if (y != 0) {
        if (str != '') str += '/';
        var d = y < 0 ? 'U' : 'D';
        str += d + Math.abs( y / 10 );
    }
    if (arclength > 0)
        str = 'A' + (arclength/10) + '/' + str;
    else if ( arclength < 0 )
        str = 'B' + ( arclength / 10 ) + '/' + str;
    return str;
}

function convertPointsArrayToString(p) {
    var str = '';
	p.forEach(function (x) {
		let xp = x.split('('), lt = xp[1];
		str += convertPointToStr(xp[0]) + '(' + lt + ' ';
	});
    return str;
}

function convertStrToPoint(str, getLength) {
	if (str.trim() == '') return 0;
	var arc = '';
	if (str.startsWith('A') || str.startsWith('B')) { 
		arc = str.split('/')[0]; 
		arc = '/' + (arc.startsWith('B')? (parseFloat(arc.split('B')[1]) * -10): (parseFloat(arc.split('A')[1]) * 10));
		str = str.split('/')[1] + (str.split('/')[2]? ('/' + str.split('/')[2]): ''); 
	}
	var coordinates = str.split('/');
	var x = null, y = null, y1 = '0', x1 = '0', point = null, x2 = '0', y2 = '0';

	coordinates.forEach(function(c){
		if (c.startsWith('U') || c.startsWith('D')) y = c;
		else x = c;
	})

	if (y) {
		y1 = parseFloat(y.substring(1, y.length)) * 10;
		y2 = ((y.startsWith('U') ? -1 * y1: y1) << 16).toString(2);
	}
	if (x) {
		x1 = parseFloat(x.substring(1, x.length)) * 10;
		x2 = ((x.startsWith('L'))? ((-1 * x1) >>> 0).toString(2).substring(16,32): x1.toString(2)); // if L, take 2's complement.
	}	
	x1 = (x1 != "") ? x1: '0'; x2 = (x2 != "") ? x2: '0'; y1 = (y1 != "") ? y2: '0'; y1 = (y2 != "") ? y2: '0';
	if(getLength)
		return (Math.sqrt(Math.pow(parseInt(x1)- 0 , 2) +Math.pow(parseInt(y1)- 0 , 2)) )/10
	else
		return parseInt(y2,2) + parseInt(x2,2) + arc;
}
function ProvalLabelPositionToStringNew(data, vectorSource, showPrevious) {
	if(showPrevious)
		data = data.Original?data.Original:"";
	if (!data || !data.label_position) return null;
	return convertPointToStr(data.label_position.toString()).replace('/', ' ');
}

function ProvalLabelPositionFromString(position) {
	var y = (position.y < 0)? 'D' + position.y * -1: 'U' + position.y;
	var x = (position.x < 0)? 'L' + position.x * -1: 'R' + position.x;
	return convertStrToPoint(x + '/' + y);
}

function OutBuildingDefinitionNew( data, config, options, sketch,showPrevious ) {
	parcel = window.opener? window.opener.activeParcel: activeParcel;
	var ConfigTables = window.opener? window.opener.ccma.Sketching.Config.ConfigTables : ccma.Sketching.Config.ConfigTables;
	var OutB_Lookups=lookup?lookup:(window.opener?window.opener.lookup:[]);
    var sketches = [], outB = [], val;    
    if ( ( options && options.type == 'outBuilding' ) ) {
        
        $( '.Current_vector_details .head' ).html( 'Outbuilding' )
        $( '.mask' ).show();
        $( '.Current_vector_details' ).css( 'width', 480 );
        $( '.Current_vector_details' )
        var validation = true;
        $( '.skNote' ).hide(); $( '.Current_vector_details .vectorPage , .Current_vector_details .footradio,.Current_vector_details .obsketch' ).parent().remove()
        $( '.assocValidation,.UnSketched' ).remove();
        $( '.dynamic_prop div' ).remove();
        $('.dynamic_prop').html('')
        $( '.Current_vector_details' ).show()
        $( '.Current_vector_details #Btn_cancel_vector_properties' ).unbind( touchClickEvent )
        $( '.Current_vector_details #Btn_cancel_vector_properties' ).bind( touchClickEvent, function () {
            $( '.Current_vector_details' ).hide();
            $( '.Current_vector_details .outbdiv' ).remove();
            if ( !sketchApp.isScreenLocked ) $( '.mask' ).hide();
            sketchApp.currentSketch.isNewSketch = false;
            sketchApp.currentSketch.isAfterSaveNewSketch = false;
           // sketchApp.sketches = sketchApp.sketches.filter( a => a.isNewSketch != true || a.vectors.length > 0 )
          
            return false;
        } )
        $( '.Current_vector_details .head' ).after( '<div class ="outbdiv"> <div class="obsketch"><label class ="checkboxlabel" style="float: left;"><input class="obcheckbox" type="checkbox"  style="width: 20px;margin-right:7px;margin-top: -3px;"/>Draw Sketch</label><label style="float: left; color: #a2a2a2;"><input class="yicheckbox" Disabled = "Disabled" type="radio" name="OB" style="width: 20px;margin-right:7px;margin-top: -3px;"/>Yard Items</label><label style="float: left; color: #a2a2a2;"><input class="sfcheckbox" Disabled = "Disabled" type="radio" name="OB" style="width: 20px;margin-right:7px;margin-top: -3px;"/>Special Features</label><label style="float: left"><input class="obcheckbox1" type="radio" name = "OB"style="width: 20px;margin-right:7px;margin-top: -3px;"/>OB</label></div><div><label style="float: left;width: 98px;"> Label</label><select class="obddl"></select><div class = "outType" style = "display: none; padding: 10px 0px"><label style="float: left; width: 98px;">Type : </label><input class = "outTypeText" type = "textbox" style="display: none; width: 267px"/></div><div><span class = "identM" style = "display : none; color: #e00909; min-width: 12px; margin: 4px;">Please Check all Values</span></div></div></div>' );
       	
       	if(sketchApp.currentSketch.parentRow.RorC == 'C'){
       		$('.yicheckbox').removeAttr( 'disabled' );
       		$('.sfcheckbox').removeAttr( 'disabled' );
       		$('.yicheckbox').parent().css('color','#000');
       		$('.sfcheckbox').parent().css('color','#000');
       		if(options.isEdit != true){
       			$( ".Current_vector_details .yicheckbox" ).prop( 'checked',true);
       			appenlookup('O[5-9]');
       		}       		
       	}
		else{
			if(options.isEdit != true){
       			$( ".Current_vector_details .obcheckbox1" ).prop( 'checked',true);
       			appenlookup('O[0-4]');
       		}
		}       	
        $('.yicheckbox').click(function(){
        	pattern=/O[5-9]/;
        	if($('.obddl').val().search(pattern)<0){
        		appenlookup(pattern);
        	}        	       	
        })
        $('.sfcheckbox').click(function(){
        	pattern=/S/;
        	if($('.obddl').val().search(pattern)<0){
        		appenlookup(pattern);
        	} 
        })
        $('.obcheckbox1').click(function(){
        	pattern=/O[0-4]/;
        	if($('.obddl').val().search(pattern)<0){
        		appenlookup(pattern);
        	} 
        })
        
        $('.obddl').change(function(){
        	if($( '.obddl' ).val() == 'O054' || $( '.obddl' ).val() == 'O603'){
            	$('.outType').css('display','block');
				$('.outTypeText').css('display','block');
				val = $( '.obddl' ).val();				
            }
            else{
            	$('.outType').css('display','none');
				$('.outTypeText').css('display','none');
				val = $( '.obddl' ).val();
			}
        })
        
        function appenlookup(pattern,callback){
        	$( '.obddl' ).empty();
        	Object.keys(OutB_Lookups["ob_codes_imp_type"]).map(function(x){ return OutB_Lookups["ob_codes_imp_type"][x]}).sort(function(a,b){return a.Ordinal - b.Ordinal}).forEach(function(x){
        		if(x.Id.match(pattern)){
            		$( '.obddl' ).append( '<option value="' + x.Id + '">' + x.Description + '</option>' );
        		}
        	});
        	$('.outType').css('display','none');
			$('.outTypeText').css('display','none');
        	$('.identM').css('display','none');
        	if(callback) callback();
        }
        
        function edit_appenlookup(){
        	
        	if(sketchApp.currentVector.labelFields[0].label_code.search('O[5-9]')>-1){
            	$('.Current_vector_details .yicheckbox').prop("checked", true);
				pattern=/O[5-9]/;           	
            }
            if(sketchApp.currentVector.labelFields[0].label_code.search('S')>-1){
            	$('.Current_vector_details .sfcheckbox').prop("checked", true);
				pattern=/S/;           	
            }
            if(sketchApp.currentVector.labelFields[0].label_code.search('O[0-4]')>-1){
            	$('.Current_vector_details .obcheckbox1').prop("checked", true);
				pattern=/O[0-4]/;           	
            }
            appenlookup(pattern,function (){
            	var OI_code = sketchApp.currentVector.labelFields[0].label_code;
            	var value = Object.keys(OutB_Lookups['ob_codes_imp_type']).map(function(x) { return OutB_Lookups['ob_codes_imp_type'][x]}).filter(function (y) {return y.Id == OI_code})[0];
            	if (!value){
            		$( '.obddl' ).append( '<option value="' + OI_code + '">' + OI_code + '</option>' );
            	}
            	if(sketchApp.currentVector.labelFields[0].label_code == 'O054' || sketchApp.currentVector.labelFields[0].label_code == 'O603'){
            		$('.outType').css('display','block');
					$('.outTypeText').css('display','block');
					$( '.Current_vector_details .obddl' ).val( sketchApp.currentVector.labelFields[0].label_code );
					$('.outTypeText').val(sketchApp.currentVector.labelFields[0].miscValue);						
            	}
            	else
            		$( '.Current_vector_details .obddl' ).val( sketchApp.currentVector.labelFields[0].label_code );
            });
       	}        
        
        if ( options.isEdit == true && !sketchApp.currentVector.isUnSketchedArea ){
            $( ".Current_vector_details .obcheckbox" ).prop( 'checked',true);
            $( ".Current_vector_details .checkboxlabel" ).css('color','#a2a2a2')
            $( ".Current_vector_details .obcheckbox" ).prop( 'Disabled','Disabled');
			edit_appenlookup();            
        }
        if ( options.isEdit == true && sketchApp.currentVector.isUnSketchedArea && sketchApp.currentVector.name ){
            $('.Current_vector_details .obcheckbox1').prop("checked", true);
            edit_appenlookup();
        }
        
        $( '.Current_vector_details #Btn_Save_vector_properties' ).unbind( touchClickEvent )
        $( '.Current_vector_details #Btn_Save_vector_properties' ).bind( touchClickEvent, function () {            
            val = $( '.obddl' ).val();
            var ob_imptype = OutB_Lookups["ob_codes_imp_type"][val]? OutB_Lookups["ob_codes_imp_type"][val].Name: '';
            if(ob_imptype == 'MISC' || ob_imptype == 'Misc' || val == 'O054' || val =='O603')
            	ob_imptype = $('.outTypeText').val()
            var draw_outbuilding = $( ".obcheckbox" ).prop( 'checked' );
            if(val == '' || ((val == 'O054' || val =='O603') && $('.outTypeText').val() == '')){
            	$('.identM').css('display','block');
            	return false;
            }            
            if ( options.callback ){
            	if (options.isEdit == true) sketchApp.currentVector.labelFields = [{ Value: ob_imptype, label_code: val, miscValue: $('.outTypeText').val() || '' }]
            	options.callback( [{ Value: ob_imptype, label_code: val, miscValue: $('.outTypeText').val() || '' }], { 'drawSketch': draw_outbuilding, 'OBlabelValue': val } )
            }
            if ( !sketchApp.isScreenLocked ) $( '.mask' ).hide();
            $( '.Current_vector_details' ).hide();
            
            if (options.isEdit && !sketchApp.currentVector.newRecord) {
				var seg_table = ConfigTables.SktOutbuilding;
				var seg_data = parcel[seg_table].filter(function(r){ return r.ROWUID == sketchApp.currentVector.uid })[0];
				if (seg_data.CC_RecordStatus != "I") {
					sketchApp.deletedVectors.push({ uid : sketchApp.currentVector.uid, eEid: sketchApp.currentVector.sketch.uid, sketchType: 'outBuilding', islabelEditedTrue: true });
    				sketchApp.currentVector.newRecord = true;
    				sketchApp.currentVector.obSketchEdited = { uid: sketchApp.currentVector.uid, isEdited: true };
				}	
            }
            
            if ( options.isEdit == true && draw_outbuilding && !sketchApp.currentVector.isUnSketchedArea) {
                sketchApp.currentVector.name = ob_imptype;
                sketchApp.currentVector.labelEdited = true;
                sketchApp.currentVector.label = '[' + sketchApp.currentVector.index + '] ' + sketchApp.currentVector.name;
            }
            else if ( options.isEdit == true && draw_outbuilding && sketchApp.currentVector.isUnSketchedArea) {
            	sketchApp.currentVector.vectorString = null;
                sketchApp.currentVector.startNode = null;
                sketchApp.currentVector.endNode = null;
                sketchApp.currentVector.commands = [];
                sketchApp.currentVector.endNode = null;
                sketchApp.currentVector.placeHolder = false;
                sketchApp.render()
                sketchApp.startNewVector( true );
                sketchApp.currentVector.isUnSketchedArea = false;
                sketchApp.currentVector.hideAreaValue = false;
                sketchApp.currentVector.name = ob_imptype;
                sketchApp.currentVector.labelEdited = true;
                sketchApp.currentVector.isClosed = false;
                sketchApp.currentVector.label = '[' + sketchApp.currentVector.index + '] ' + sketchApp.currentVector.name;
           }
           else if( options.isEdit == true && !draw_outbuilding && sketchApp.currentVector.isUnSketchedArea){
           		sketchApp.currentVector.name = ob_imptype;
                sketchApp.currentVector.labelEdited = true;
                sketchApp.currentVector.label = '[' + sketchApp.currentVector.index + '] ' + sketchApp.currentVector.name;
           }
		  $( '.Current_vector_details .outbdiv' ).remove();
		  return false;
        } )
    }
    else{
    	var sk;
    	if( options && options.type != 'outBuilding' ){ 		
            var vector_table = ConfigTables.SktVector_outbuilding, labelposition;    		
    		var vector_data = [];
    		if(showPrevious){
    			data[vector_table].forEach(function(vData){ vector_data.push(vData.Original); });
    			data = data.Original? data.Original: '';
    		}
			else{
				vector_data = data[vector_table] ? data[vector_table].filter(function(vec){return vec.CC_Deleted != true}) : null;
			}    			
    		var ob_imptype = (data.label_code == 'O054' || data.label_code =='O603')? 'MISC': (OutB_Lookups["ob_codes_imp_type"] && OutB_Lookups["ob_codes_imp_type"][data.label_code])? OutB_Lookups["ob_codes_imp_type"][data.label_code].Name: '';
			var imp_id = data.improvement_id;
			if(imp_id && imp_id.split('{').length > 1)
				imp_id = imp_id.split('{')[1].split('}')[0];
			var imp_IdOB = (imp_id && !(isNaN(imp_id)))? parseInt(imp_id): imp_id;
        	var Imp_Rec = parcel.ccv_improvements.filter(function(imp){ return (imp.ROWUID == imp_IdOB || (imp.improvement_id == imp_IdOB)) && (imp.ParentROWUID == data.ParentROWUID) && imp.CC_Deleted != true})[0];
            var imp_ID = (Imp_Rec && Imp_Rec.CC_RecordStatus != "I")? Imp_Rec.improvement_id: '';
    		var startValue = data['vector_start'];
    		if(ob_imptype == 'MISC' || ob_imptype == 'Misc'|| data.label_code == 'O054' || data.label_code =='O603')
    			ob_imptype = Imp_Rec? Imp_Rec.misc_imp_type:'MISC';
			var areaValue = data['area'], original_vector = [], original_start_value = data.Original? data.Original['vector_start']: '';
			var perimeterValue = data['perimeter'];
			var Org_label_code = data.Original? data.Original['label_code']: '';
			var Org_label_position = data.Original? data.Original['label_position']: '';			
			if(data.area == 0 || data.area == 12 || data['label_position'] == '655370000'){
				data['label_position'] = data['vector_start'];
				labelposition = ProvalLabelPositionToStringNew(data);
			}
			else
				labelposition = ProvalLabelPositionToStringNew(data);
			var vector_string = '', original_vect_str = '', isChanged = false;
			if(vector_data && !showPrevious)
				vector_data.forEach(function(vData){ original_vector.push(vData.Original); });			
			if (!vector_data || (!startValue && !original_start_value)) return '';

			vector_data = vector_data.sort(function (a, b) { return a.seq_number - b.seq_number }).map(function (a) { return a.run_rise + ((a.curve_height && a.curve_height != '' && a.curve_height != 0) ? '/' + a.curve_height : '') + '(' + (a.line_type ? a.line_type : '0') + '~' + (a.floor_level ? a.floor_level : '0') + '~' + (a.number_floors ? a.number_floors : '0') + ')' });
			original_vector = original_vector.sort(function (a, b) { return a.seq_number - b.seq_number }).map(function (a) { return a.run_rise + ((a.curve_height && a.curve_height != '' && a.curve_height != 0) ? '/' + a.curve_height : '') + '(' + (a.line_type ? a.line_type : '0') + '~' + (a.floor_level ? a.floor_level : '0') + '~' + (a.number_floors ? a.number_floors : '0') + ')' });
    		if (!vector_data) return '';
    		var desc;
	        var lookDesc = ( !options.sksource.DoNotShowLabelDescription && sketchSettings["DoNotShowLabelDescriptionSketch"] != '1' && clientSettings["DoNotShowLabelDescriptionSketch"] != "1") ? desc : null
	        var label = [];
	        label.push({Field: config.LabelField, Value: imp_ID +'-'+ ob_imptype, label_code: data.label_code, miscValue: Imp_Rec? Imp_Rec.misc_imp_type:null, Description: lookDesc})
	        if (vector_data == '' || vector_data.length == 0){ 
    			var ischange = (((startValue != original_start_value) || (data.label_code != Org_label_code)) && !showPrevious) ? true: false;
    			 sk = {
                		uid: data.ROWUID.toString(),
                		label: label,
                		vector: ':' +convertPointToStr(startValue).replace('/',' ') + ' S P2',
                		labelPosition: labelposition,
                		isChanged: ischange,
	            		vectorConfig: config, 
                		hideAreaValue: true,
                		noVectorSegment: true,
                		areaFieldValue: '',
                		perimeterFieldValue: perimeterValue,
                		sketchType: 'outBuilding',
                		isUnSketchedArea: true,
                		areaUnit: ' ' ,
                		imp_Type: Imp_Rec ? Imp_Rec.imp_type : null,
                		vectorStart: startValue
            		};
    			 sketches.push(sk);
    		}
			else{
    			vector_string = convertPointToStr(startValue).replace('/',' ') + ' S ' + convertPointsArrayToString(vector_data);
    			original_vect_str = convertPointToStr(original_start_value).replace('/',' ') + ' S ' + convertPointsArrayToString(original_vector);
    			if (((startValue != original_start_value) || (data.label_code != Org_label_code) || (data['label_position'] != Org_label_position)) && !showPrevious) isChanged = true;
            		sk = {
                			uid: data.ROWUID.toString(),
                			label: label,
                			vector: ':' + vector_string,
                			labelPosition: labelposition,
                			isChanged: isChanged,
	            			vectorConfig: config, 
                			//hideAreaValue: true,
                			noVectorSegment: false,
                			sketchType: 'outBuilding',
                			isUnSketchedArea: false,
                			imp_Type: Imp_Rec? Imp_Rec.imp_type : null,
                			vectorStart: startValue
            			}
            		sketches.push( sk );
            }        		          	
       	}
        return sketches;
    }
}

function ProvalVectorDefinitionNew(data, config, processor,showPrevious) {
	var ConfigTables = window.opener? window.opener.ccma.Sketching.Config.ConfigTables : ccma.Sketching.Config.ConfigTables;
	var sketchType = (config.Table == 'ccv_sktsegment_dwellings')? '_dwellings': (config.Table == 'ccv_sktsegment_comm_bldgs'? '_comm_bldgs': '_imps');
    var vector_table = ConfigTables.SktVector;
    var label_table = ConfigTables.SktSegmentLabel;
    var vector_data = [];
    if(showPrevious){
    	data[vector_table].forEach(function(vData){ vector_data.push(vData.Original); });
    	data = data.Original?data.Original:"";
    }
	else{
		vector_data = data[vector_table] ? data[vector_table].filter(function(vec){return vec.CC_Deleted != true}) : null;
	}    
    var startValue = data['vector_start'];
	var areaValue = data['area'], original_vector = [], original_start_value = data.Original?data.Original['vector_start']:"";
	var perimeterValue = data['perimeter'];
	var vector_string = '', original_vect_str = '', isChanged = false;
	if(vector_data && !showPrevious)
		vector_data.forEach(function(vData){ original_vector.push(vData.Original); });
	if (!vector_data) return '';

	vector_data = vector_data.sort(function (a, b) { return a.seq_number - b.seq_number }).map(function (a) { return a.run_rise + ((a.curve_height && a.curve_height != '' && a.curve_height != 0) ? '/' + a.curve_height : '') + '(' + (a.line_type ? a.line_type : '0') + '~' + (a.floor_level ? a.floor_level : '0') + '~' + (a.number_floors ? a.number_floors : '0') + ')' });
	original_vector = original_vector.sort(function (a, b) { return a.seq_number - b.seq_number }).map(function (a) { return a.run_rise + ((a.curve_height && a.curve_height != '' && a.curve_height != 0) ? '/' + a.curve_height : '') + '(' + (a.line_type ? a.line_type : '0') + '~' + (a.floor_level ? a.floor_level : '0') + '~' + (a.number_floors ? a.number_floors : '0') + ')' });

	if (!vector_data) return '';
    if (vector_data == '' || vector_data.length == 0){
		var ischange = (startValue != original_start_value) ? true: false;    
    	return { isChanged: ischange, vector_string: ':' + convertPointToStr(startValue).replace('/',' ') + ' S P2', noVectorSegment: true, areaFieldValue: areaValue, areaUnit: ' ' ,Line_Type: true,isUnSketchedTrueArea: true, perimeterFieldValue: perimeterValue};
	}
	
    vector_string = convertPointToStr(startValue).replace('/',' ') + ' S ' + convertPointsArrayToString(vector_data);
    original_vect_str = convertPointToStr(original_start_value).replace('/',' ') + ' S ' + convertPointsArrayToString(original_vector);
    if (vector_string != original_vect_str) isChanged = true;
    return { isChanged: isChanged, vector_string: ':' + vector_string, noVectorSegment: false };
}


function ProvalNoteDefinitionNew(sketchsource, showPrevious) {
	var ConfigTables = window.opener? window.opener.ccma.Sketching.Config.ConfigTables : ccma.Sketching.Config.ConfigTables;
    var notearray = [], note_y_positions = [], note_data = [];        	
    data = activeParcel[ConfigTables.SktNote] && activeParcel[ConfigTables.SktNote].filter(function (d) { return ( ( d.ParentROWUID && d.ParentROWUID == sketchsource.ROWUID)) });
    if(showPrevious){
    	data.forEach(function(nData){ note_data.push(nData.Original); });
    	data = note_data;
    }
	else{
		data = data.filter(function(d){return d.CC_Deleted != true});		
	}    
    var mod = function(val) {
    	if (val < 0) return val * -1;
    	else return val;
    }
    for (var x in data) {
        var note = data[x], mod_diff;
        var x = parseInt(note['note_position']) << 16 >> 16;
        var y = -1 *parseInt(note['note_position']) >> 16;
        if(y == 0 || y == '0' || y == NaN){
        	note_y_positions.forEach(function(y_pos){
        		mod_diff = mod(mod(y_pos) - mod(y));
        		if (mod_diff < 40) {
        			y = y - (40 - mod_diff);
        			return;
    			}
        	});
        }
        note_y_positions.push(y);
        var n = {
            uid: note[ 'ROWUID'],
            text: (note['note_text'] || '').replace( /&lt;/g, '<' ).replace( /&gt;/g, '>' ),
            x: x/10,
            y: y/10
        }
        notearray.push(n)
    }
    return notearray;
}

function ProvalLabelDefinitionNew(data, config, showPrevious) {
	var ConfigTables = window.opener? window.opener.ccma.Sketching.Config.ConfigTables : ccma.Sketching.Config.ConfigTables;
	var Proval_Lookup= ( ( parent && parent.Proval_Lookup )? parent.Proval_Lookup: ( ( parent && parent.window && parent.window.opener && parent.window.opener.Proval_Lookup ) ? parent.window.opener.Proval_Lookup : ( Proval_Lookup? Proval_Lookup: (window.opener ? window.opener.Proval_Lookup: []) ) ) );
	var Proval_default_exterior_Cover = ( ( parent && parent.Proval_Default_Exterior_Cover )? parent.Proval_Default_Exterior_Cover: ( ( parent && parent.window && parent.window.opener && parent.window.opener.Proval_Default_Exterior_Cover ) ? parent.window.opener.Proval_Default_Exterior_Cover :  ( Proval_Default_Exterior_Cover ? Proval_Default_Exterior_Cover: ( window.opener ? window.opener.Proval_Default_Exterior_Cover : [] ) ) ) );

	var labelTable = ConfigTables.SktSegmentLabel;
	var labelValue = '', label_val_orig = '', lblData, segmentLbl = {}, isChanged = false, base_array = [],count = 0, basementfract = false;
	var extRuid = data.ParentROWUID;
	var segment_data_label = [];
	if(showPrevious){	
		data[labelTable].forEach(function(segData){ segment_data_label.push(segData.Original); });	
		data = data.Original?data.Original:"";
	}
	else{
		segment_data_label = data[labelTable] ? data[labelTable].filter(function(vec){return vec.CC_Deleted != true}) : [];
	}
	var seg_area = parseInt(data.area);
    
    var getDescription = function(lbl_data){
    	return (lbl_data? (lbl_data.field_1 && lbl_data.field_1.trim() != '')? lbl_data.field_1: lbl_data.tbl_element_desc: '');
    }
    
    
    
	if (segment_data_label) {
		var labelData = _.sortBy(segment_data_label, 'lbl_level_code');
		
		var getBasementDetails = function(lbl_value, labelData, isOriginal,seg_type, ImpIdMethod, ImpType, bsmtCallback) {
			if(lbl_value.length > 0)
				lbl_value = lbl_value + '|';
			basementfract = true;
			var isorginall = true, slabLabelExists = false, bsmtGarageExists = false, bsmtGarageFinExists = false, updatedAreaNumber = 2, bsmtGarageArea = 0, bsmtModifierExists = false;
			var lblData = labelData;
			if (isOriginal) {
				lblData = lblData.Original
				isorginall = false;
			}
			else
				segmentLbl['1'] = {comp1: 0, comp2: 0, comp3: 0,comp4: 0,modifier1: "",modifier2: "",modifier3: "",modifier4: "",modifierSign1: "",modifierSign2: "",modifierSign3: "",modifierSign4: "",prefix1: 0,prefix2: 0,prefix3: 0,prefix4: 0,walkout1: "",walkout2: "",walkout3: "",walkout4: "", parent_type: seg_type , parent_imp_id: lblData.improvement_id, ImpIdMethod: ImpIdMethod, ImpType: ImpType}
			var frac1 = parseInt(lblData.lbl_frac1);
			var frac2 = parseInt(lblData.lbl_frac2);
			var frac3 = parseInt(lblData.lbl_frac3);
			var frac4 = parseInt(lblData.lbl_frac4);
			var finish_area = parseInt(lblData.finish_area);
			var unfin_area = parseInt(lblData.unfin_area);
			var area2 = parseInt(lblData.area2);
			var area3 = parseInt(lblData.area3);
			var calculateBelowGradeAreas = function(frac, blevel, belowGradecallback){
				if(!(frac != 0 && frac > 0)){
					if(belowGradecallback) belowGradecallback();
					return;
				}
				var prefix = frac<<16>>16;
				var pref = (prefix == 1) ? '1/4' : (prefix == 2) ? '1/2' : '3/4';
				var comp = frac>>16;
				var farct= frac.toString();
				var fin = '', finPlus = '', finMin = '', uf = '', walk = '', walkValue = '';
				if(farct.substring(0,4)=="1297")
					fin='Fin';
				else if(farct.substring(0,4)=="9887"){
					fin='Fin';
					finPlus='+';
				}
				else if(farct.substring(0,4)=="5592"){
					fin='Fin';
					finMin='-';
				}
				if(comp>1000){
					comp = comp%1000;
					walk = 'Y';
					walkValue = '-wo';
				}					
				var tmp_comp = Proval_Lookup.filter(function(pro){ return pro.tbl_type_code == 'Component' && pro.tbl_element == comp; });
				var comps = tmp_comp.length > 0? getDescription(tmp_comp[0]): comp;
				var lblvalue = (pref + ' ') + (comps + ' ')+ (walkValue != '' ? (walkValue + ' '): '' )+ (fin != '' ? (fin) : '' )+ (finPlus != '' ? (finPlus) :'') + (finMin != '' ? (finMin):'');
				if(isorginall){
					segmentLbl['1']['comp'+ blevel] = comp; segmentLbl['1']['prefix'+ blevel] = prefix; segmentLbl['1']['modifier'+ blevel] = fin;
					segmentLbl['1']['modifierSign' + blevel] = (finPlus != '')? finPlus : (finMin !='') ? finMin : '';
					segmentLbl['1']['walkout' + blevel] = walk;
					if(comp == 202 && fin != '')
					{
						bsmtModifierExists = true;
						base_array.push({Id: comp.toString(), area: finish_area, modifier: 'Fin'})
						base_array.push( {Id: comp.toString(), area: unfin_area, modifier: "UnFin" })
					}
                    else if(comp == 202 && fin == '')
                        base_array.push({Id: comp.toString(), area: unfin_area})
					else{
						if(comp == 205){
						    base_array.push({Id: comp.toString(), area: 0});
						    slabLabelExists = true;
						}
						else{
							if(comp == 206){
								bsmtGarageExists = true;
								bsmtGarageFinExists = (fin == 'Fin') ? 'Fin' : 'unFin';
							}
							if(updatedAreaNumber == 2){
						        base_array.push({Id:comp.toString(),area: area2});
						        updatedAreaNumber++;
						        if(bsmtGarageExists)
						            bsmtGarageArea = area2;
							}
							else if (updatedAreaNumber == 3){
								base_array.push({Id:comp.toString(),area: area3});
								updatedAreaNumber++;
						        if(bsmtGarageExists)
						            bsmtGarageArea = area3;
							}
						}
					}
				}
				if(blevel == '1')
				    lbl_value = lbl_value + lblvalue;
				else
				    lbl_value = lbl_value + ' + ' + lblvalue; 
				if(belowGradecallback) belowGradecallback();
			}
			calculateBelowGradeAreas(frac1, '1' ,function(){
				calculateBelowGradeAreas(frac2, '2' ,function(){
				    calculateBelowGradeAreas(frac3, '3' ,function(){
				        calculateBelowGradeAreas(frac4, '4' ,function(){
                            if(bsmtGarageExists){
                            	if(bsmtModifierExists){
                            		if(bsmtGarageFinExists == 'Fin'){
                            			var bsmt_base_arr = base_array.filter(function(x){return x.Id == '202' && x.modifier == 'Fin'})[0];
                            			if(bsmt_base_arr)
                            			    bsmt_base_arr.area = bsmt_base_arr.area - bsmtGarageArea;
                            		}
                            		else if(bsmtGarageFinExists == 'unFin'){
                            			var bsmt_base_arr = base_array.filter(function(x){return x.Id == '202' && x.modifier == 'UnFin'})[0];
                            			if(bsmt_base_arr)
                            			    bsmt_base_arr.area = bsmt_base_arr.area - bsmtGarageArea;
                            		}
                            	}
                            	else {
                            		if(bsmtGarageFinExists == 'unFin'){
                            			var bsmt_base_arr = base_array.filter(function(x){return x.Id == '202'})[0];
                            			if(bsmt_base_arr)
                            			    bsmt_base_arr.area = (bsmt_base_arr.area - bsmtGarageArea > 0) ? (bsmt_base_arr.area - bsmtGarageArea) : bsmt_base_arr.area;
                            		}
                            	}
                            }
                            if(slabLabelExists){
                            	var sl_base_arr = base_array.filter(function(x){return x.Id == '205'})[0];
                            	var b_tot_array = 0;
                                base_array.forEach(function(b_array){
                                	b_tot_array = b_tot_array + b_array.area;
                                });
                                sl_base_arr.area = seg_area - b_tot_array;
                            }
                            base_array.forEach(function(b_array){
                            	b_array.area = b_array.area.toString();
                            });
                            if(bsmtCallback) bsmtCallback(lbl_value);
			            });
			        });
			    });
			});
		}
		
		var getLabelDetails = function(labelData, isOriginal) {
			var lbl_value = '';
			for (idx = labelData.length - 1; idx != -1; idx--) {
				var lblData = labelData[idx];
				var seg_type = '', ImpIdMethods = '', ImpTypes = '';
				if(lblData.improvement_id){
				if(lblData.improvement_id.split('{').length > 1){
					var stbl = lblData.improvement_id.split('{')[0].split('&%')[1];
					var srowuid = lblData.improvement_id.split('{')[1].split('}')[0]; 
					seg_type = (stbl == 'ccv_improvements_dwellings')? 'dwellings': ((stbl == 'ccv_improvements_comm_bldgs')? 'comm_bldgs': 'imps');
					var segParentData = stbl ? activeParcel[stbl].filter(function(s){return s.ROWUID == srowuid})[0] : [];
					segParentData = segParentData ? segParentData : [];
					ImpIdMethods = segParentData.improvement_id;
					ImpTypes = segParentData.imp_type;
				}
				else{	
					seg_type = (lblData.improvement_id == 'D' || lblData.improvement_id == 'M' || lblData.improvement_id == 'A')? 'dwellings': ((lblData.improvement_id == 'C')? 'comm_bldgs': 'imps');
					var stbl = ((seg_type == 'dwellings') ? 'ccv_improvements_dwellings': ((seg_type == 'comm_bldgs') ? 'ccv_improvements_comm_bldgs' : 'ccv_improvements'));
					var segParentData = activeParcel[stbl].filter(function(s){return (s.improvement_id == lblData.improvement_id)  && s.ParentROWUID == extRuid})[0];
					if(!segParentData){
						var stab = 'ccv_improvements';
						segParentData = activeParcel[stab].filter(function(s){return (s.improvement_id == lblData.improvement_id)  && s.ParentROWUID == extRuid})[0];
						if(segParentData){
							seg_type = 'imps';
							stbl = 'ccv_improvements';
						}
					}
					segParentData = segParentData ? segParentData : [];
					ImpIdMethods = segParentData.improvement_id;
					ImpTypes = segParentData.imp_type;				
				}
				if((lblData.lbl_frac2 != '0' && lblData.lbl_frac2 != 0) && (lblData.lbl_level_code == '1' || lblData.lbl_level_code == 1))
					getBasementDetails(lbl_value, lblData, isOriginal, seg_type, ImpIdMethods, ImpTypes, function(res_label){
						lbl_value = res_label;
					});
				else{
					if (isOriginal) lblData = lblData.Original;
					var flr_records;					
					var frac1 = parseInt(lblData.lbl_frac1), const_code = parseInt(lblData.lbl_const_code), ext_feat_code = parseInt(lblData.lbl_ext_feat_code);
					var prefix = frac1<<16>>16;
					var ns='', mat_Ext_Feat = '';
                	if(prefix == '10' || prefix == '15'){
                		ns=(lblData.lbl_n && lblData.lbl_n != '0')?lblData.lbl_n: 0;
                   		ns=parseInt(ns.toString().split(".")[0]);
         			}
					var comp = frac1>>16;
					var walk=false,fin=false,finPlus=false,finMin=false,uf=false;
					var farct=frac1.toString();
					var walkout = '', Modifier='', modifierSign='', ext_cover = '';
					if(farct.substring(0,4)=="1297"){
						fin=true;
						Modifier='Fin';
					}
					else if(farct.substring(0,4)=="9887"){
						fin=true;
						finPlus=true;
						modifierSign='+';
					}
					else if(farct.substring(0,4)=="5592"){
						fin=true;
						finMin=true;
						modifierSign='-';
					}
					else if(farct.substring(0,4)=="1292"){
						uf=true;
						Modifier='UF';
					}
					if(comp>1000){
						comp=comp%1000;
						walk=true;
					}
					prefix = (prefix == 0 && comp == 0 && lblData.lbl_frac && lblData.lbl_frac1.toString() != '0')? frac1: prefix;
					if (lblData.lbl_level_code && ['1','2','3'].indexOf((lblData.lbl_level_code).toString()) > -1 && !isOriginal) {
						segmentLbl[lblData.lbl_level_code] = { ext_feat_code: ext_feat_code, comp: comp, prefix: prefix, constr: const_code, ns: ns, walkout: '', ext_cover: lblData.default_ext_cover, modifier: '',modifierSign:'', parent_type:seg_type , parent_imp_id: lblData.improvement_id, ImpIdMethod: ImpIdMethods, ImpType: ImpTypes}
						if(uf==true){
							segmentLbl[lblData.lbl_level_code].modifier = 'UF';							
						}
						if(fin==true){
							segmentLbl[lblData.lbl_level_code].modifier = 'Fin';							
							if(finPlus==true){
								segmentLbl[lblData.lbl_level_code].modifierSign = '+';								
							}
							else if(finMin==true){
								segmentLbl[lblData.lbl_level_code].modifierSign = '-';								
							}
						}
					}
					if (prefix && prefix.toString() != '0') {
						var tmp_prefix = Proval_Lookup.filter(function(pro){ return pro.tbl_type_code == 'Prefix' && pro.tbl_element == prefix; });
						prefix = tmp_prefix.length > 0? getDescription(tmp_prefix[0]): prefix;
						if(prefix == 'N s' && ns != '')	
                    		prefix =ns.toString() + ' s'	
                    	else if (prefix == 'N c' && ns != '')
                    		prefix =ns.toString() + ' c';
                    	if((!isOriginal) && tmp_prefix.length > 0 && tmp_prefix[0].AdditionalScreen && tmp_prefix[0].AdditionalScreen != ''){
							var addScreen = ( ( tmp_prefix[0].AdditionalScreen.split("/").length > 1 )?  tmp_prefix[0].AdditionalScreen.split("/")[1]: tmp_prefix[0].AdditionalScreen );
							var areaEnt = ( (addScreen == 'AreaEntry')? true: false );
							if(areaEnt)	{
								segmentLbl[lblData.lbl_level_code].fin_area = (lblData.finish_area).toString();
								segmentLbl[lblData.lbl_level_code].unfin_fin_area = (lblData.unfin_area).toString();
							}
						}
					}
				
					if (comp && comp.toString() != '0') {
						var tmp_comp = Proval_Lookup.filter(function(pro){ return pro.tbl_type_code == 'Component' && pro.tbl_element == comp; });
						comp = tmp_comp.length > 0? getDescription(tmp_comp[0]): comp;
						if (comp.toString() == 'B') {
							if(walk==true){
								walkout='Y';
								segmentLbl[lblData.lbl_level_code].walkout = walkout;
							}
						}
						if((!isOriginal) && tmp_comp.length > 0 && tmp_comp[0].AdditionalScreen && tmp_comp[0].AdditionalScreen != ''){
							var addScreen = ( ( tmp_comp[0].AdditionalScreen.split("/").length > 1 )?  tmp_comp[0].AdditionalScreen.split("/")[1]: tmp_comp[0].AdditionalScreen );
							var areaEnt = ( (addScreen == 'AreaEntry')? true: false );
							if(areaEnt && tmp_comp[0].tbl_type_code == "Component")
								areaEnt = (segmentLbl[lblData.lbl_level_code].modifier == "Fin" || segmentLbl[lblData.lbl_level_code].modifier == "UF") ? true: false;
							if(areaEnt)	{
								segmentLbl[lblData.lbl_level_code].fin_area = (lblData.finish_area).toString();
								segmentLbl[lblData.lbl_level_code].unfin_fin_area = (lblData.unfin_area).toString();
							}
						}
					}
					ext_cover = ((lblData.default_ext_cover && (lblData.default_ext_cover != '0' && lblData.default_ext_cover != 'null')) ? lblData.default_ext_cover : '');
					if (const_code && const_code.toString() != '0') {
						var tmp_const_code = Proval_default_exterior_Cover.filter(function(pro){ return pro.tbl_type_code == 'const' && pro.tbl_element == const_code }); 
						const_code = tmp_const_code.length > 0? getDescription(tmp_const_code[0]): const_code;
					}
				
					if (ext_feat_code && ext_feat_code.toString() != '0') {
						var ext_split_code = parseInt(ext_feat_code);
						var ext_temp = ext_split_code % 1000;
						var maatValuess = parseInt((ext_split_code/1000));
						if(maatValuess != '0' || maatValuess != 0){
							maatValuess = parseInt(maatValuess);
							var baseValue = parseInt(parseInt(maatValuess,10).toString(2));
							var j = 0;
							while(baseValue > 0){
								j = j + 1;
								var rem = baseValue % 10;
								if(rem > 0){
									switch(j){
										case 1: mat_Ext_Feat = mat_Ext_Feat + 'T'; break;
										case 2: mat_Ext_Feat = mat_Ext_Feat + 'R'; break;
										case 3: mat_Ext_Feat = mat_Ext_Feat + 'S'; break;
										case 4: mat_Ext_Feat = mat_Ext_Feat + 'W'; break;
									}
								}
								baseValue = parseInt(baseValue / 10);
							}
						}
						var tmp_ext_feat_code = Proval_Lookup.filter(function(pro){ return pro.tbl_type_code == 'Exterior Feature' && pro.tbl_element == ext_temp; });
						ext_feat_code = tmp_ext_feat_code.length > 0? getDescription(tmp_ext_feat_code[0]): ext_temp;
					}
					if (lblData.lbl_level_code && ['1','2','3'].indexOf((lblData.lbl_level_code).toString()) > -1) {
						lbl_value += (lbl_value.length > 0? '|': '') + ((prefix && prefix.toString() != '0' && prefix != '' )? (prefix + ' ') : '') + ((const_code && const_code.toString() != '0' && const_code != '' )? (const_code + ' ') : '') + ((comp && comp.toString() != '0' && comp != '' )? (comp + ' ') : '') + ((walkout == 'Y')? '-wo ' : '') + ((ext_cover && ext_cover.toString() != '0' && ext_cover != '' )? (ext_cover + ' ') : '') + ((ext_feat_code && ext_feat_code.toString() != '0' && ext_feat_code != '' )? (ext_feat_code + ' ') : '') + ((mat_Ext_Feat && mat_Ext_Feat.toString() != '0' && mat_Ext_Feat != '' )? ('-' + mat_Ext_Feat + ' ') : '') + ((Modifier && Modifier.toString() != '0' && Modifier != '' )? (Modifier) : '') + ((modifierSign && modifierSign.toString() != '0' && modifierSign != '' )? (modifierSign) : '');
					}
				}
			}
			}
			return lbl_value;
		}
		
		labelValue = getLabelDetails(labelData, false);
		if (!showPrevious && getLabelDetails(labelData, true) != labelValue) isChanged = true;
		if(labelData && labelData.length == 1 && (labelData[0].lbl_level_code == '3' || labelData[0].lbl_level_code == 3 ))
			labelValue = labelValue + ' (Upper)';		
	}
	[1,2,3].forEach(function(lvl_code) {
		if(!(segmentLbl[lvl_code] || segmentLbl[lvl_code.toString()])) segmentLbl[lvl_code] = { ext_feat_code: 0, comp: 0, prefix: 0, constr: 0 }
	})
	if(basementfract)
		var retVal = { labelValue: labelValue, isChanged: isChanged, base_array: base_array, BasementFunctionality:true };
	else
		var retVal = { labelValue: labelValue, isChanged: isChanged };
	retVal.details = segmentLbl;
	return retVal;
}

function ProvalLabelConfigNew(head, items, lookups, callback, editor, type) {
	var Proval_Sketch_codes = window.opener ? window.opener.Proval_sketch_codes : [];
	var Proval_Lookup=window.opener?window.opener.Proval_Lookup:[];
	var Proval_Default_Exterior_Cover = window.opener ? window.opener.Proval_Default_Exterior_Cover : [];
	if(head == "New Vector - Labels")
		head = "New Sketch - Labels";
	var backFromBasementFunctionalityLabelDaetails;
	function BasementFunctionality(isChangSketchLabel, basecallback){
		var labelValues = '',BasementFunctionalityDetail = [], BFIndex =  0 ,ValidateLabelsString = '',sketch_type = '', CurrentLabelValue = '', editLabelChanged = false;
		sketch_type = ((editor.currentSketch.parentRow.RorC)=='R'?'Res':'Comm')
		var segment_Parent_Type = (sketch_type == 'Res' ? 'dwellings' : 'comm_bldgs');
		var grade_col = sketch_type + 'GradeLevel';	
		var prefix_filter_col = sketch_type + 'Prefix';		
		var constr_filter_col = sketch_type + 'ConstrOption';
		var material_filter_col = sketch_type + 'Material';

		var option = '', headers = [{name: 'Prefix', value: 'prefix', filter: prefix_filter_col, grade_filter: grade_col},{name: 'Component', value: 'comp'}];
		var lbl_level = [{name: 'Below Grade', value: 'Below_Grade', value1: 1, value2: 'Below'}];
		var html = '<table style="width: 100%;font-size: 18px;"><tbody><tr><th>Fractional Below Grade Allocation</th></tr></tbody></table>';
	
		$('.Current_vector_details .head').html(head);
		$( '.dynamic_prop div,.outbdiv' ).remove();
		$('.mask').show(); $('.dynamic_prop').html('');
   		$('.dynamic_prop').append('<div class="divvectors"></div>');
    
    	var getDescription = function(lbl_data, isField_1){
    		if (isField_1) return (lbl_data? (lbl_data.field_1 && lbl_data.field_1.trim() != '')? lbl_data.field_1: lbl_data.tbl_element_desc: '');
    		return (lbl_data? (lbl_data.tbl_element_desc && lbl_data.tbl_element_desc.trim() != '')? lbl_data.tbl_element_desc: lbl_data.field_1: '');
    	}
		headers.forEach(function(hdr) {
    		if(hdr.name == 'Prefix')
    			hdr.lookup = Proval_Lookup.filter(function(pro){ return (pro.tbl_type_code == hdr.name) && (pro.tbl_element_desc == '1/4' || pro.tbl_element_desc == '1/2' || pro.tbl_element_desc == '3/4') });
    		else
    			hdr.lookup = Proval_Lookup.filter(function(pro){ return pro.tbl_type_code == hdr.name &&  pro.AdditionalScreen && (pro.AdditionalScreen == "BGAreaEntry" || (pro.AdditionalScreen.split("/")[0] == "BGAreaEntry"))});
    	});
    
    	lbl_level.forEach(function(lbl, index) {
    		headers.forEach(function(hdr, hdr_index) {
    			html += '<div class="Below'+ hdr.name +'" style="width: 30%;border-right: 1px solid #c7cdcf;height:175px;"><span style="float:left;width:200px;justify-content: center;font-weight: bold;font-size: 14px;margin: 5px 0;padding-left: 60px;">'+hdr.name+'</span>';
    			if (hdr_index >= 0)
					hdr.lookup.filter(function(lk){ return (lk[grade_col] && lk[grade_col].indexOf(lbl.value2) > -1); }).forEach(function(lk) {
						if(lk.tbl_element_desc == "Bsmt")
							html += '<span style="float:left;width:300px;min-width: 300px;margin: 5px 0;";><label style="float: left; font-size: 16px;"><input type="radio" id="'+lk.tbl_element+'" name="'+hdr.name+'" value="' + getDescription(lk, true) + '" style="float:left;width:50px;height:20px" checked> ' + getDescription(lk) + '</label><label style="float: left; font-size: 16px;"><input id="walkout" type="checkbox" style="width:50px;height:20px;" >walkout</label></span>'
						else
	    					html += '<span style="float:left;width:300px;min-width: 300px;margin: 5px 0;";><label style="float: left; font-size: 16px;"><input type="radio" id="'+lk.tbl_element+'" name="'+hdr.name+'" value="' + getDescription(lk, true) + '" style="float:left;width:50px;height:20px"> ' + getDescription(lk) + '</label></span>';

	    			});
	    			html+='</div>'
    		});
    		html += '<div class="Belowmodifier" style="width: 30%;"><span style="float:left;width:200px;justify-content: center;font-weight: bold;font-size: 14px;margin: 5px 0;padding-left: 50px;">Modifier</span>';
    		html += '<span style="float:left;min-width: 250px;margin: 5px 0;"><label style="float: left; font-size: 16px;"><input type="radio" id="302" name="modifer" value="Fin" style="float:left;width:50px;height:20px;">Fin</label>';
    		html += '<label style="float: left; font-size: 16px;"><input type="radio" id="M303" name="modifierSign" value="+" style="float:left;width:50px;height:20px;" disabled>+</label></span>';
    		html += '<span style="float:left;min-width: 250px;margin: 5px 0;"><label style="float: left; font-size: 16px;"><input type="radio" id="301" name="modifer" value="UF" style="float:left;width:50px;height:20px;"disabled>UF</label>';
    		html += '<label style="float: left; font-size: 16px;"><input type="radio" id="M304" name="modifierSign" value="-" style="float:left;width:56px;height:20px;" disabled>-</label></span></div>';
    	});
    	var previousLabelValue = items[0].Newlabel;
    	var notBelowLabel = '';
    	var previousLabelLength = previousLabelValue.split("|").length;
    	if(previousLabelLength > 1){
    		for(var t =0; t< previousLabelLength-1; t++)
    			notBelowLabel = notBelowLabel + previousLabelValue.split("|")[t] + '|';
    		labelValues = labelValues + notBelowLabel + previousLabelValue.split("|")[previousLabelLength - 1];
    	}
    	else
    		labelValues = labelValues + previousLabelValue;
		 
    	$('.divvectors').append(html);
    	$('.dynamic_prop').append('<span class="ValidateLabels" style="display: none; width:450px; color:Red;margin-left: 150px;">'+ValidateLabelsString+'</span><span class="CurrentLabel" style="display: none; max-width:550px;color:#a01e1e;font-size: 14px;margin-left: 26px;">'+CurrentLabelValue+'</span>');
    	$('.Current_vector_details').show(); 
		$('.Current_vector_details').css('left','20%');    	
 		$('.Current_vector_details').width(785);
 		$('.divvectors').height(300);
 		$('.dynamic_prop').append('<div class="skNote" style="width: 92%; margin: 0px 15px;"></div>');
 		$('.skNote').show();
    	$('.skNote').html('<span><b>Sketch Label Preview:</b>&nbsp;</span><span class="label_preview">'+labelValues+'</span>');
		//if(type == 'edit')
			///$('#Btn_cancel_vector_properties').attr('disabled','disabled');
		var labelDetailsval1 = items[0].labelDetails.details[1];
		var cid = (labelDetailsval1.comp != 0 && labelDetailsval1.comp != '0') ? ($("#"+labelDetailsval1.comp+"")[0].checked = true) : 0;
		$("#"+labelDetailsval1.prefix+"")[0].checked = true;
		var mid = labelDetailsval1.modifier != ''? ((labelDetailsval1.modifier == 'Fin')? ($("#302")[0].checked = true): ($('#301')[0].checked= true)): '';
		var midsign = labelDetailsval1.modifierSign != ''? ((labelDetailsval1.modifierSign == '+' ) ? ($("#M303")[0].checked = true) : ($("#M304")[0].checked = true)): '';
		var walko = labelDetailsval1.walkout != '' ?  ((labelDetailsval1.walkout == "Y" ? ($("#walkout")[0].checked = true) :'' )) : '';
		if(labelDetailsval1.comp != '202'){
			$('#walkout')[0].checked=false;
    		$('#walkout').attr('disabled','disabled');
		}
		if(labelDetailsval1.modifier == "Fin"){
        	$('#M303').removeAttr("disabled");
            $('#M304').removeAttr("disabled");
        }
        
        var lookupValss = Proval_Lookup.filter(function(lkup){ return lkup.tbl_element == labelDetailsval1.comp })[0];
    	lookupValss = lookupValss ? lookupValss: {};
    	if(!(_.isEmpty(lookupValss)) && lookupValss.tbl_type_code == "Component" && lookupValss.tbl_element != "302"){
    		if(sketch_type ==  'Res' && lookupValss.ResModifier && lookupValss.ResModifier != '')
    			$('#M302').removeAttr("disabled");
    		else if(sketch_type == 'Comm'  && lookupValss.CommMaterial && lookupValss.CommMaterial != '' )
    			$('#M302').removeAttr("disabled");
    		else
    			$('#M302').attr('disabled','disabled');
    	}
        
		var imp_Tpe =  labelDetailsval1.ImpType;
		var imp_tpe_method = labelDetailsval1.ImpIdMethod;
    	$("input[type='radio'],input[type='checkbox']").change(function(){
    		if(type == 'edit')
    			editLabelChanged = false;
    		$('.ValidateLabels').hide();
			var lbl_val = $(this).val();
			var lbl_Id = $(this).attr('id'); 
			var comp_current_select_val = '',pre_current_select_val= '' ,modi_current_select_val = '', modifierSign_current_select_val = '', walk_value = '';
			var lookupVal = Proval_Lookup.filter(function(lkup){ return lkup.tbl_element == lbl_Id; })[0];
    		lookupVal = lookupVal? lookupVal: {};
    		var bel_comp = $('.BelowComponent span input[type=radio]:checked');
    		var bel_pre = $('.BelowPrefix span input[type=radio]:checked');
    		var bel_modi = $('.Belowmodifier span input[type=radio]:checked');
    		comp_current_select_val =bel_comp[0]? bel_comp[0].value:''; 
    		var comp_current_select_Id =bel_comp[0]? bel_comp[0].id:'';
    		pre_current_select_val = bel_pre[0]?bel_pre[0].value:''; 
    		var pre_current_select_Id = bel_pre[0]?bel_pre[0].id:'';
    		if(bel_modi.length > 1){
    			modi_current_select_val = bel_modi[0]?bel_modi[0].value:''; 
    			var modi_current_select_Id =bel_modi[0]? bel_modi[0].id:'';
    			modifierSign_current_select_val =bel_modi[1]? bel_modi[1].value:''; 
    			var modifierSign_current_select_Id =bel_modi[1]? bel_modi[1].id:'';
    		}
    		else{
    			modi_current_select_val =bel_modi[0]? bel_modi[0].value:''; 
    			var modi_current_select_Id =bel_modi[0]? bel_modi[0].id:'';
    		}
    		
    		if(!(_.isEmpty(lookupVal)) && lookupVal.tbl_type_code == "Component" && lookupVal.tbl_element != "302"){
    			if(sketch_type ==  'Res' && lookupVal.ResModifier && lookupVal.ResModifier != '')
    				$('#M302').removeAttr("disabled");
    			else if(sketch_type == 'Comm'  && lookupVal.CommMaterial && lookupVal.CommMaterial != '' )
    				$('#M302').removeAttr("disabled");
    			else
    				$('#M302').attr('disabled','disabled');
    		}
    		
    		if(lbl_Id == 202 || comp_current_select_Id == 202){
    			$('#walkout').removeAttr("disabled");
    		}
    		else{
    			$('#walkout')[0].checked=false;
    			$('#walkout').attr('disabled','disabled');
    		}
    		if(lbl_Id == 302 || modi_current_select_Id == 302){
				$('#M303').removeAttr("disabled");
				$('#M304').removeAttr("disabled");
    		}
    		else{
    			$('#M303').attr('disabled','disabled');
				$('#M304').attr('disabled','disabled');
    		}
    		if($('#walkout').prop("checked") == true){
				walk_value = '-wo'
    		}
    		var new_Label = pre_current_select_val + ' ' + comp_current_select_val + ' ' + (walk_value!= '' ? walk_value : '') + (modi_current_select_val != '' ? modi_current_select_val: '') + modifierSign_current_select_val;
    		var gradelabel = labelValues;
    		var gradeLength = labelValues.split('|').length;
			if(gradeLength > 1) 
				gradelabel = labelValues.split('|')[gradeLength-1];			
			var LabelSplit = gradelabel.split(" + ");
			if(LabelSplit.length > 1) {
				switch(LabelSplit.length)
				{
					case 2: labelValues = notBelowLabel + LabelSplit[0] + ' + ' + new_Label ; break;
					case 3: labelValues = notBelowLabel + LabelSplit[0] + ' + ' + LabelSplit[1] + ' + ' + new_Label ; break;
					case 4: labelValues = notBelowLabel + LabelSplit[0] + ' + ' + LabelSplit[1] + ' + ' + LabelSplit[2] + ' + ' + new_Label ; break;
				}
			}
			else
				labelValues = notBelowLabel + new_Label;
			$('.label_preview').html(labelValues);
		 });
		 $('.Current_vector_details #Btn_Save_vector_properties').unbind(touchClickEvent);
    	 $('.Current_vector_details #Btn_Save_vector_properties').bind(touchClickEvent, function () {
    	 	valid = true;
    	 	if(!editLabelChanged){
    			var nsVal, BasementLabel = '', total_value = 0, rem_comp = [];
    			var lokValue = Proval_Lookup.filter(function (c) {return c[grade_col] && c.AdditionalScreen && (c.AdditionalScreen == "BGAreaEntry" || (c.AdditionalScreen.split("/")[0] == "BGAreaEntry"))})
    			lokValue.forEach(function (lval){
    				rem_comp.push(lval.field_1);
    			});
				var prefixVal, prefixId, compVal, compId, modiferVal, modifierSignVal, walkval = '';
				items[0].Value = $('.label_preview').html();
    			items[0].Newlabel = $('.label_preview').html();
    			BasementLabel = $('.label_preview').html();
    			if(BasementLabel.split("|").length > 1)
    				BasementLabel = BasementLabel.split("|")[BasementLabel.split("|").length -1];
				prefixVal = $("input[name='Prefix']:checked").val();
				compVal = $("input[name='Component']:checked").val();
				modiferVal = $("input[name='modifer']:checked").val() ? $("input[name='modifer']:checked").val() : '' ;
				modifierSignVal = $("input[name='modifierSign']:checked").val() ? $("input[name='modifierSign']:checked").val() : '' ;
				if(!prefixVal || !compVal){
					ValidateLabelsString = "Please select label from both Prefix and Component."
					$('.ValidateLabels').html(ValidateLabelsString);
					$('.ValidateLabels').show();
					return false;
				}
				prefixId = $("input[name='Prefix']:checked")[0].id;
				compId = $("input[name='Component']:checked")[0].id;
				if(compId == 202 ){
					if($('#walkout').prop("checked") == true)
						walkval = 'Y';
				}
				BasementFunctionalityDetail[BFIndex] = _.clone({comp: compId, prefix: prefixId, walkout: walkval, modifier: modiferVal, modifierSign: modifierSignVal});
				var BasementLabelSplit = BasementLabel.split(" + ");
				var BasementLabelSplitLength = BasementLabelSplit.length, k=0, PrefixSplit = [], compSplit = [];
				for(k =0; k<BasementLabelSplitLength ; k++){
					var FinSplit = BasementLabelSplit[k].split("Fin");
					PrefixSplit[k] = FinSplit[0].match('[0-9]/[0-9]');
					switch(PrefixSplit[k][0]){
						case "1/4": total_value = total_value + 1/4;break;
						case "1/2": total_value = total_value + 1/2;break;
						case "3/4": total_value = total_value + 3/4;break;
					}
					compSplit[k] = FinSplit[0].split(PrefixSplit[k][0])[1];
					compSplit[k] = compSplit[k].replace(/ /g,'');
					if(compSplit[k].indexOf("B-wo")>-1)
						compSplit[k] = compSplit[k].split("-wo")[0];
				}
				if(total_value > 1){
					ValidateLabelsString = "Total prefix sum greater than 1.Please choose correct prefix value";
					$('.ValidateLabels').html(ValidateLabelsString);
					$('.ValidateLabels').show();
					return false;
				}
				else if(total_value < 1){
					var remaingvalue = 1- total_value;
					var rem_prefix = '';
					switch(remaingvalue){
						case .25:rem_prefix = '1/4';break;
						case .5:rem_prefix = '1/2';break;
						case .75: rem_prefix = '3/4';break;
					}
					for(k =0; k<BasementLabelSplitLength ; k++){
						var r_index = _.indexOf(rem_comp,rem_comp.filter(function(t){return t == compSplit[k]})[0])
						rem_comp.splice(r_index,1);
					}
					if(rem_comp.length <= 0){
						ValidateLabelsString = "No New Label Available.Please choose correct prefix value";
						$('.ValidateLabels').html(ValidateLabelsString);
						$('.ValidateLabels').show();
						return false;
					}
					BFIndex = BFIndex + 1;
					var new_comp = rem_comp.shift();
					var new_Labels = BasementLabel + ' + ' + rem_prefix + ' ' + new_comp;	
					labelValues = notBelowLabel + new_Labels;
					$('.label_preview').html(labelValues);
					$("input[name='Prefix'][value='"+rem_prefix+"']")[0].checked = true;
					$("input[name='Component'][value='"+new_comp+"']")[0].checked = true;
					if($("input[name='modifer']:checked").length>0)
						$("input[name='modifer']:checked")[0].checked = false;
					if($('input[name=modifierSign]:checked').length>0)
						$('input[name=modifierSign]:checked')[0].checked = false;
					if($('#walkout')[0].checked){
						$('#walkout')[0].checked = false;
					}
					
					var compIddd = $("input[name='Component'][value='"+new_comp+"']")[0].id;
					var lookupVals = Proval_Lookup.filter(function(lkup){ return lkup.tbl_element == compIddd })[0];
    				lookupVals = lookupVals ? lookupVals: {};
    				if(!(_.isEmpty(lookupVals)) && lookupVals.tbl_type_code == "Component" && lookupVals.tbl_element != "302"){
    					if(sketch_type ==  'Res' && lookupVals.ResModifier && lookupVals.ResModifier != '')
    						$('#M302').removeAttr("disabled");
    					else if(sketch_type == 'Comm'  && lookupVals.CommMaterial && lookupVals.CommMaterial != '' )
    						$('#M302').removeAttr("disabled");
    					else
    						$('#M302').attr('disabled','disabled');
    				}
					
					$('#'+compId+'').attr('disabled','disabled')
					if(new_comp =='B')
						$('#walkout').removeAttr('disabled','disabled');
					return false;
				}
				else{
					var BFDCount = 0;
					items[0].labelDetails.details[1] = ( _.clone({comp1: 0, prefix1: 0, walkout1: '', modifier1: '', modifierSign1: '', comp2: 0, prefix2: 0, walkout2: '', modifier2: '', modifierSign2: '', comp3: 0, prefix3: 0, walkout3: '', modifier3: '', modifierSign3: '',comp4: 0, prefix4: 0, walkout4: '', modifier4: '', modifierSign4: '', parent_type: segment_Parent_Type, parent_imp_id: '', ImpType: imp_Tpe, ImpIdMethod: imp_tpe_method}));
					items[0].labelDetails.labelValue = $('.label_preview').html()
					BasementFunctionalityDetail.forEach(function(BFD){
						BFDCount = BFDCount + 1;
						if(BFDCount == 1){
							items[0].labelDetails.details[1].comp1 = BFD.comp;
							items[0].labelDetails.details[1].prefix1 = BFD.prefix;
							items[0].labelDetails.details[1].walkout1 = BFD.walkout;
							items[0].labelDetails.details[1].modifier1 = BFD.modifier;
							items[0].labelDetails.details[1].modifierSign1 = BFD.modifierSign;
						}
						else if(BFDCount == 2){
							items[0].labelDetails.details[1].comp2 = BFD.comp;
							items[0].labelDetails.details[1].prefix2 = BFD.prefix;
							items[0].labelDetails.details[1].walkout2 = BFD.walkout;
							items[0].labelDetails.details[1].modifier2 = BFD.modifier;
							items[0].labelDetails.details[1].modifierSign2 = BFD.modifierSign;
						}
						else if(BFDCount == 3){
							items[0].labelDetails.details[1].comp3 = BFD.comp;
							items[0].labelDetails.details[1].prefix3 = BFD.prefix;
							items[0].labelDetails.details[1].walkout3 = BFD.walkout;
							items[0].labelDetails.details[1].modifier3 = BFD.modifier;
							items[0].labelDetails.details[1].modifierSign3 = BFD.modifierSign;
						}
						else if(BFDCount == 4){
							items[0].labelDetails.details[1].comp4 = BFD.comp;
							items[0].labelDetails.details[1].prefix4 = BFD.prefix;
							items[0].labelDetails.details[1].walkout4 = BFD.walkout;
							items[0].labelDetails.details[1].modifier4 = BFD.modifier;
							items[0].labelDetails.details[1].modifierSign4 = BFD.modifierSign;
						}
					})
				}
			}
    		var validateLabel = true, i = 1, ValidateArea = false, CheckUnsketch = false;
    		if (valid) {
    			if(type == 'edit')
    				$('#Btn_cancel_vector_properties').removeAttr('disabled')
				if(basecallback) basecallback();
				return false;
        	}
        	else messageBox('Please check the values.')
    	});
    
    	$('.Current_vector_details #Btn_cancel_vector_properties').unbind(touchClickEvent)
    	$('.Current_vector_details #Btn_cancel_vector_properties').bind(touchClickEvent, function() {
			$('.Current_vector_details').css('top','25%');
			$('.Current_vector_details').css('left','35%');
        	$('.Current_vector_details').hide();
        	if (!sketchApp.isScreenLocked) $('.mask').hide();
        	NoNBasementFunctionality(true);
       		return false;
    	});  
  }

  function NoNBasementFunctionality(backFromBasementFunctionality){
  	var deleteVectorInEdit = [];
	var changeVar = 0, labelValueSelect, ImpIdMethod, headers = [], changeBase = 0, checkedTrue = true, checkedArea = '', checkedPerimeter ='', ImpType = '',sketch_Type ='', changeFinandExtcover = false;
	var CurrentLabelValue ='', clval ='', lblCurrentField = '', partialLabelss = '', DrawSketchTrue = false;
	var sketch_type=((editor.currentSketch.parentRow.RorC)=='R'?'Res':'Comm')
	sketch_Type = sketch_type;
	var grade_col = sketch_type + 'GradeLevel';	
	var prefix_filter_col = sketch_type + 'Prefix';		
	var constr_filter_col = sketch_type + 'ConstrOption';
	var material_filter_col = sketch_type + 'Material';
	
	var impGrade_col='ImpGradeLevel';
	var impGrade_colprefix_filter_col ='ImpPrefix';		
	var impConstr_filter_col = 'ImpConstrOption';
	var impMaterial_filter_col = 'ImpMaterial';
    
    lblCurrentField = (items[0] &&  items[0].Value) ? items[0].Value : '';
	if(type == 'edit' && sketchApp.currentVector && sketchApp.currentVector.BasementFunctionality){
		partialLabelss =  items[0] &&  items[0].Value;
		if(partialLabelss.split('|').length > 0)
			partialLabelss = partialLabelss.split('|')[partialLabelss.split('|').length - 1];
		lblCurrentField = lblCurrentField.split(' + ')[0];
	}
    
    var getDescription = function(lbl_data, isField_1){
    	if (isField_1) return (lbl_data? (lbl_data.field_1 && lbl_data.field_1.trim() != '')? lbl_data.field_1: lbl_data.tbl_element_desc: '');
    	return (lbl_data? (lbl_data.tbl_element_desc && lbl_data.tbl_element_desc.trim() != '')? lbl_data.tbl_element_desc: lbl_data.field_1: '');
    }
    function changesegment(){
    	var html = "", isEditTrue = true;
		var option = ''; 
		headers = [{name: 'Prefix', value: 'prefix', filter: prefix_filter_col, grade_filter: grade_col}, {name: 'Construction', value: 'constr', filter: constr_filter_col, grade_filter: grade_col}, {name: 'Component', value: 'comp'}, {name: 'Exterior Feature', value: 'ext_feat_code'}];
		var lbl_level = [{name: 'Below Grade', value: 'Below_Grade', value1: 1, value2: 'Below'}, {name: 'At Grade', value: 'At_Grade', value1: 2, value2: 'At'}, {name: 'Above Grade', value: 'Above_Grade', value1: 3, value2: 'Above'}];
	 	//html = '<table style="width: 100% ;"><tr style="background: #e4e4e4;height: 22px;"><th>Sketch Label </th>';
		html = '<table style="width: 100% ;"><tr style="background: #e4e4e4;height: 22px;"><th></th>';
		
		headers.forEach(function(hdr) {
    		html += ('<th>' + hdr.name + '</th>');
    		if(hdr.name == 'Construction')
    			hdr.lookup = Proval_Default_Exterior_Cover.filter(function(pro){ return pro.tbl_type_code == 'const' })
    		else
    			hdr.lookup = Proval_Lookup.filter(function(pro){ return pro.tbl_type_code == hdr.name });
    	});
		$('.Current_vector_details .head').html(head);
		$( '.dynamic_prop div,.outbdiv,.CurrentLabel' ).remove();
		$('.mask').show(); $('.dynamic_prop').html('');
		if(type == 'new' || (type == 'edit' && sketchApp.currentVector.isUnSketchedTrueArea)){
			DrawSketchTrue = true;
			$('.dynamic_prop').append('<div class="UnSketched"> <div class="UnSketchedarea" style="height:30px;padding: 0px;"><label style="float: left;width: 98px;"> Draw Sketch</label><label style="float: left"><input class="checkbox" type="checkbox" checked="checked" style="width: 20px;margin-right:7px;margin-top: -3px;">Yes</label></div><div class="Unarea" style="height:30px;display: none;padding: 0px;padding-left: 15px;"><span class="ValidateArea" style="display: none; font-weight: bold; min-width: 5px; color:Red;">*</span><label style="float: left;width: 50px;"> Area</label><input class="sqft" type="number" style="width: 100px;margin-right:7px;margin-top: -3px;"></div><div class="Unperimeter"  style="height:30px;display: none;padding: 0px;padding-left: 15px;"><label style="float: left;width: 80px;"> Perimeter</label><input class="perimeter" type="number" style="width: 100px;margin-right:7px;margin-top: -3px;"></div></div>');
		}
    	$('.dynamic_prop').append('<div class="divvectors"></div>');

    	lbl_level.forEach(function(lbl, index) {	
    		html += ('<tr class="lbl_level" name="' + lbl.value2 + '"><td style="font-weight:500;">' + lbl.name + '</td>');
    		option = '';
    		headers.forEach(function(hdr, hdr_index) {
    			html += '<td><select  style="width:110px;" type="' + hdr.value + '" grade="' + lbl.value + '" class="segmentLbl ' + hdr.value + '">';
    			html += '<option value="0"></option>';
    			if (hdr_index >= 0){
    				if(hdr.name == 'Construction' && lbl.value1 != 1)
    					hdr.lookup.forEach(function(lk) {
	    					html += '<option value="' + lk.tbl_element + '" value1="' + getDescription(lk, true) + '">' + getDescription(lk) + '</option>';
	    				});
    				else
						hdr.lookup.filter(function(lk){ return ((lk[grade_col] && lk[grade_col].indexOf(lbl.value2) > -1) ||lk[impGrade_col] && lk[impGrade_col].indexOf(lbl.value2) > -1 ); }).forEach(function(lk) {
	    					html += '<option value="' + lk.tbl_element + '" value1="' + getDescription(lk, true) + '">' + getDescription(lk) + '</option>';
	    				});
	    		}
    			html += '</select></td>';
    		});

    		var materialLookup = Proval_Sketch_codes.filter(function(t){return t.tbl_type_code== "Material"});
    		html += '<tr class="extraval" style="display: none;"><td></td><td><input style="height: 20px; width: 96px;" onkeypress="if(this.value.length==20) return false;" placeholder="N-" type="number" class="' + lbl.value + ' ns" readonly="readonly"></td><td style="float: right; font-style: italic;">Walkout:</td><td><select grade="' + lbl.value + '" class="' + lbl.value + ' walkout" style="height: 24px; width: 100px;" disabled><option value=" "></option><option value="Y">Y</option><option value="N">N</option></select></td></tr>';
    	
    		html += '<tr class="add_vals"><td style="float: right; font-style: italic;">Default Exterior Cover:</td><td><select class="' + lbl.value + ' ext_cover" style="width: 110px;">';
    	
    		if(sketch_Type =='Res') {
    			let NoneOption = Proval_Default_Exterior_Cover.filter(function(pro){ return ( pro.tbl_type_code == 'extcover' && pro.tbl_element_desc == "None" ) })[0];
    			if (!NoneOption)
                    html +='<option value="0" value1="None">None</option>';
    			else if (NoneOption)
                    html += '<option value="' + NoneOption.tbl_element + '" value1="' + getDescription(NoneOption, true) + '">' + getDescription(NoneOption) + '</option>';
    			Proval_Default_Exterior_Cover.filter(function(pro){ return ( pro.tbl_type_code == 'extcover' && pro.tbl_element_desc != "None" ) }).forEach(function(extCover){
    				html += '<option value="' + extCover.tbl_element + '" value1="' + getDescription(extCover, true) + '">' + getDescription(extCover) + '</option>';
    			});
    		}
    		else if(sketch_Type =='Comm') {
    			let NoneOption = Proval_Default_Exterior_Cover.filter(function(pro){ return ( pro.tbl_type_code == 'MSExtWall' && pro.tbl_element_desc == "None" ) })[0];
    			if (!NoneOption)
    			    html +='<option value="0" value1="None">None</option>';
    			else if (NoneOption)
                    html += '<option value="' + NoneOption.tbl_element + '" value1="' + getDescription(NoneOption, true) + '">' + getDescription(NoneOption) + '</option>';
    			Proval_Default_Exterior_Cover.filter(function(pro){ return ( pro.tbl_type_code == 'MSExtWall' && pro.tbl_element_desc != "None" ) }).forEach(function(extCover) {
    				html += '<option value="' + extCover.tbl_element + '" value1="' + getDescription(extCover, true) + '">' + getDescription(extCover) + '</option>';
    			});
    		}
    	
    		html += '</select></td><td style="padding-left: 10px; font-style: italic;text-align: right;" >Modifier:</td><td><select style="width: 65px; float: inherit;" class="' + lbl.value + ' modifier"><option value=""></option>'; 
    	
    		Proval_Lookup.filter(function(pro){ return (pro.tbl_type_code == 'Modifier' && getDescription(pro) &&(( pro[grade_col] && pro[grade_col].indexOf(lbl.value2) > -1) ||(pro[impGrade_col] && pro[impGrade_col].indexOf(lbl.value2) > -1))); }).forEach(function(mod){
    			if (getDescription(mod) != '+' && getDescription(mod) != '-')
    				html += '<option value="' + getDescription(mod) + '" value1="' + getDescription(mod, true) + '">' + getDescription(mod) + '</option>';
    		});
    	
    		html += '</select><select style="width: 44px;margin-left: 2px;float: inherit;" class="' + lbl.value + ' modifierSign" disabled="disabled"><option value=""></option><option value="+">+</option><option value="-">-</option></select></td>';
    	
    		//Proval_Lookup.filter(function(pro){ return (pro.tbl_type_code == 'Material' &&(( pro[grade_col] && pro[grade_col].indexOf(lbl.value2) > -1) ||(pro[impGrade_col] && pro[impGrade_col].indexOf(lbl.value2) > -1))); }).forEach(function(mat){
    			//html += '<option value="' + getDescription(mat) + '" value1="' + getDescription(mat, true) + '">' + getDescription(mat) + '</option>';
    		//});
    		if(lbl.value1 != 1)
    			html += '</tr><tr><td style="float:right;padding-top: 8px;" >Material:</td><td colspan="4" style="font-style: italic;padding-top: 5px;">';
    		var matAdd = 1;
    		for(i = 0; i < 4;i++  ){
    			if(lbl.value1 != 1)
					html += '<span style="height: 20px; width: 104px; padding: 4px 2px;font-size: 14px;"><label style="float: left"><input class="ext_Materials" type="checkbox" value = "' + materialLookup[i].field_1 + '" value1= "' + matAdd + '" name="' + lbl.value + 'ext_Materials" style="width: 15px;margin-right: 6px;margin-top: -3px;margin-bottom: -3px;"  Disabled = "Disabled">'+materialLookup[i].tbl_element_desc+'</label></span>';
    			//else
    				//html += '<span style="height: 20px; width: 104px; padding: 4px 2px;font-size: 14px;"><label style="float: left"><input class="ext_Materials" type="checkbox" value = "'+materialLookup[i].field_1+'" value1= "'+matAdd+'" name="' + lbl.value + 'ext_Materials" style="width: 15px;margin-right: 6px;margin-top: -3px;margin-bottom: -3px;" Disabled = "Disabled">'+materialLookup[i].tbl_element_desc+'</label></span>';
    			matAdd = matAdd * 2;
    		}
    		
			html += '</td></tr>'
			html += (index != lbl_level.length -1)? '<tr ><td colspan="5" style="Height:0px;border-bottom: 1.5px solid #b9b9b9;"></td></tr>': '';
    	});
    	$('.divvectors').append(html + '</table>');
    	$('.divvectors').append('<span class="ValidateLabel" style="height: 12px; display: none; width:450px; color:Red;margin-left: 100px;">Please choose any label from Prefix, Components or Exterior Feature.</span>');
    	$('.Current_vector_details').show();   
 		$('.Current_vector_details').width(690);
 		$('.divvectors').width(655);
 		$('.divvectors').css('max-height','370px')
 		$('.divvectors').height(362);
 		$('.Current_vector_details').css('top','15%');
    	$('.lbl_level td').css('padding','3px 0px');
    	$('.divvectors').css('padding-bottom','0px');
    	$('.dynamic_prop').css('padding-bottom','0px');
    	$('.Current_vector_details').css('left','25%');
    	$('.dynamic_prop').append('<div class="skNote" style="width: 92%; margin: 0px 15px;"></div>');
    	$('.skNote').show();
    	$('.skNote').html('<span><b>Sketch Label Preview:</b>&nbsp;</span><span class="label_preview">' + (lblCurrentField || '') + '</span>'); 
		if(type == 'edit' && sketchApp.currentVector){
    		$('.dynamic_prop').after('<span class="CurrentLabel" style="display: none; max-width:550px; color:#a01e1e;margin-left: 4px;font-size: 14px; margin-left: 35px;">'+CurrentLabelValue+'</span>');
    		var extlabel = "Current Sketch Label : ";
    		clval = (backFromBasementFunctionality && backFromBasementFunctionalityLabelDaetails )? backFromBasementFunctionalityLabelDaetails[0].Value: items[0].Value;;
			CurrentLabelValue = extlabel + clval;
			$('.CurrentLabel').html(CurrentLabelValue);
			$('.CurrentLabel').show();
		}
		if(type == 'edit'){
			var vectorscopy;
			if(!backFromBasementFunctionality)
				backFromBasementFunctionalityLabelDaetails = JSON.parse(JSON.stringify(sketchApp.currentVector.labelFields));
			if(backFromBasementFunctionalityLabelDaetails)
				vectorscopy = backFromBasementFunctionalityLabelDaetails;
			var vUid = sketchApp.currentVector.uid;
			var eVuid = sketchApp.currentVector.sketch.uid;
			deleteVectorInEdit.push({uid : vUid.toString(), labelFields: vectorscopy, islabelEditedTrue: true, eEid: eVuid.toString()})
			isEditTrue = false;
		}
 		var isWalkout = false;
 		$('.lbl_level[name=Below] .segmentLbl.ext_feat_code').attr('disabled','true');
 		$('.segmentLbl').unbind();
    	$('.segmentLbl,.modifier,.modifierSign,.ns,.ext_cover,.ext_Materials').change(function(index){
    		$('.skNote').show();
    		$('.ValidateLabel').hide();
    		$('.At_Grade.modifier').removeAttr('disabled');
    		isEditTrue = true;
    		var lbls = [], codes = [], lbl = '', code = '', that = this, lbl_value = null, extraValues = '', FirstSelectFin = false;
			var tr = $(that).parents('.lbl_level'), thisValue = $(that).val();
			if(thisValue == '209' ||thisValue == '207' || thisValue == '208')
				$('.At_Grade.modifier').attr('disabled','disabled');
			if($(that).attr('class').search('modifier')>-1){
				if(thisValue == 'Fin' || thisValue == 'UF' ||thisValue ==""){
					FirstSelectFin = true;
					var next_select = $(this).next('select');
    				if ($(this).val() == 'Fin') $(next_select).removeAttr('disabled');
    				else {
    					$(next_select).val('');
    					$(next_select).attr('disabled', 'disabled');
    				}
				}
			}
			var extra_tr = $(tr).next('tr'), extraVal = false;
			var lookupVal = {};
			var labelLevel=$(tr).attr('name');
    		var tttype=$(that).attr('type');
			if(thisValue != '212')
				lookupVal = Proval_Lookup.filter(function(lkup){ return lkup.tbl_element == thisValue; })[0];
			else
				lookupVal = Proval_Lookup.filter(function(lkup){ return lkup.tbl_element == thisValue && lkup.CommGradeLevel == labelLevel ; })[0];
    		lookupVal = lookupVal? lookupVal: {};
    		var chanvalue=thisValue;
    		var sketch_type=((sketchApp.currentSketch.parentRow.RorC)=='R'?'Res':'Comm')
		if((chanvalue=="202" || chanvalue==202) && labelLevel=="Below" && changeBase==0){
			var rd=$('.extraval')[0];
			$(rd).attr('style','inline');
			$('.Below_Grade.walkout').removeAttr('disabled')
		}
		if((chanvalue == "15" || chanvalue == "10") && changeBase==0 ){
			var nsIndex = 0;
			if(labelLevel=="Below")
				nsIndex = 0
			else if(labelLevel=="At")
				nsIndex = 1;
			else if(labelLevel == "Above")
				nsIndex = 2;
			var kr=$('.extraval')[nsIndex];
			$(kr).show();
			var rx=$('.ns')[nsIndex];
			$(rx).removeAttr('readonly');

		}
		changeFinandExtcover = false; FirstSelectFin = false;
			changeBase=1;
    		if (!isWalkout) {
	    		if (lookupVal && lookupVal[material_filter_col] && lookupVal[material_filter_col] == 'Y') 
					$('.material', $(extra_tr).next('tr')).removeAttr('disabled');
				else $('.material', $(extra_tr).next('tr')).attr('disabled', 'disabled');
	    		if ($(that).hasClass('prefix')) {
	    			if (thisValue == '10' ||thisValue == '15' ) {
	    				$(extra_tr).show();
	    				$('.ns', extra_tr).val('N-')
	    				$('.ns', extra_tr).removeAttr('readonly');
					}
	    			else {
	    				if ($('.walkout', extra_tr).attr('disabled')) $(extra_tr).hide();
	    				$('.ns', extra_tr).attr('readonly', 'readonly');
					}
	    		}
	    		if ($(that).hasClass('comp')) {
	    			if (thisValue == '202' && sketch_type == 'Res') { 
	    				$(extra_tr).show(); 
	    				$('.walkout', extra_tr).removeAttr('disabled'); 
					}
	    			else {
	    				$('.walkout', extra_tr).attr('disabled', '');
	    				$('.walkout', extra_tr).val('');    		
	    				if ($('.walkout', extra_tr).attr('disabled')) $(extra_tr).hide();
					}
					var html = '<option value="0"></option>';
					if (thisValue == '0') {
						$('.prefix', tr).html(html);
						Proval_Lookup.filter(function(lk){ return (lk[grade_col] && lk.tbl_type_code =='Prefix'  && lk[grade_col].indexOf($(tr).attr('name')) > -1); }).forEach(function(lk) {
	    					html += '<option value="' + lk.tbl_element + '" value1="' + getDescription(lk, true) + '">' + getDescription(lk) + '</option>';
	    				});
	    				$('.prefix', tr).html(html);
	    				var html = '<option value="0"></option>';
						$('.constr', tr).html(html);
						Proval_Lookup.filter(function(lk){ return (lk[grade_col] && lk.tbl_type_code =='Construction'  && lk[grade_col].indexOf($(tr).attr('name')) > -1); }).forEach(function(lk) {
	    					html += '<option value="' + lk.tbl_element + '" value1="' + getDescription(lk, true) + '">' + getDescription(lk) + '</option>';
	    				});
	    				$('.constr', tr).html(html);
					}
					else {
						var array = [0,1];
						array.forEach(function(idx){
							var hdr = headers[idx];
							var currentLookupFilValue = $('.' + hdr.value, tr).val();
							var currentlooFilValTrue = false;
							var filter_filed = (idx == 0) ? impGrade_colprefix_filter_col : impConstr_filter_col ;
							lookupVal = Proval_Lookup.filter(function(lkup){ return lkup.tbl_element == thisValue && ((lkup[hdr.grade_filter]  && lkup[hdr.grade_filter].indexOf($(tr).attr('name')) > -1) || (lkup[impGrade_col]  && lkup[impGrade_col].indexOf($(tr).attr('name')) > -1) ) ; })[0];
							lookupVal = lookupVal? lookupVal: {};
							var valid_fields = lookupVal[hdr.filter]? lookupVal[hdr.filter].split(','): (lookupVal[filter_filed]? lookupVal[filter_filed].split(',') :[]);
							valid_fields = valid_fields.map(s => (typeof(s) == 'string'? s.trim(): s));
							hdr.lookup.filter(function(lkp){ return (valid_fields.indexOf(lkp.tbl_element_desc) > -1); }).forEach(function(lkup){
								if(lkup.tbl_element == currentLookupFilValue)
									currentlooFilValTrue = true;
								html += '<option value="' + lkup.tbl_element +'" value1="' + getDescription(lkup, true) + '">' + getDescription(lkup) + '</option>';
							});
							$('.' + hdr.value, tr).html(html);
							if(currentlooFilValTrue)
								$('.' + hdr.value, tr).val(currentLookupFilValue);
							html = '<option value="0"></option>';
						});
					}
	    		}
    		}
    		$('.lbl_level').each(function(index, lvl){
    			var ext_feature_selected = false;
    			var mValue=$('.add_vals').eq(index).find('.modifier').val();
    			var modSignValue=$('.add_vals').eq(index).find('.modifierSign').val();
    			var ext_covers='';
    			ext_covers = $('.add_vals').eq(index).find('.ext_cover').val();
    			lbl = ''; code = ''; lbl_value = null;
				extraValues = $('.extraval').eq(index).find('.ns').val();
				let mcObj = index == 0 ? {} : { comp: { v: '', lkp: null }, prefix: { v: '', lkp: null }, constr: { v: '', lkp: null }, ext_feat_code: { v: '', lkp: null } };


    			$('select', lvl).each(function(index, select){
    				lbl_value = $('option[value="'+ $(select).val() +'"]', select).attr('value1');
    				var level = $(select).attr('grade');
    				if(index == '3' && lbl_value)
    					ext_feature_selected = true;
    				if($(select).val() == '209' || $(select).val() == '207' || $(select).val() == '208')
						$('.At_Grade.modifier').attr('disabled','disabled');
    				else if( $(select).val() == '201' ){
    					$('.'+level+'.modifier').removeAttr('disabled');
						$('.'+level+'.modifier').attr('enabled', 'enabled');
    				}    					
    				else if( $(select).attr('type') == 'comp' ){
						
						if(lbl_value != '' && lbl_value != ' ' && lbl_value != 0 && lbl_value != null){
							var lbllookup = Proval_Lookup.filter(function(x){ return x.tbl_element == $(select).val()})[0];
							var modif = sketch_Type =='Res'? lbllookup.ResModifier: (sketch_Type =='Comm')? lbllookup.CommModifier:'';
							if(modif != '' && modif != ' ' && modif != null){
								$('.'+level+'.modifier').removeAttr('disabled');
								$('.'+level+'.modifier').attr('enabled', 'enabled');	
							}
							else{
								if(mValue != ''){
									$('.'+level+'.modifier').val('');
									$('.'+level+'.modifierSign').val('');
								}								
								$('.'+level+'.modifier').removeAttr('enabled');
								$('.'+level+'.modifierSign').removeAttr('enabled');
								$('.'+level+'.modifierSign').attr('disabled', 'disabled');
								$('.'+level+'.modifier').attr('disabled', 'disabled');
							}
						}
						else{
							$('.'+level+'.modifier').removeAttr('disabled');					
							$('.'+level+'.modifier').attr('enabled', 'enabled');
						}
					}
    				if((lbl_value == 'N s' || lbl_value == 'N c') && index == 0 ){
    					if(extraValues && extraValues != '' && lbl_value == 'N s')
    						lbl_value = extraValues + ' s';
    						else if(extraValues && extraValues != '' && lbl_value == 'N c')
    						lbl_value = extraValues + ' c';
    				}
    				lbl += lbl_value? (lbl_value + ' '): '';
    				if ($(select).hasClass('comp') && $(select).val() == '202') {
    					if ($('.walkout[grade="' + $(select).attr('grade') + '"]').val() == 'Y')
    						lbl += '-wo ';
					}
					code += $(select).val() + ',';

					if (mcObj[$(select).attr('type')] && $(select).val() && $(select).val() != '0') {
						mcObj[$(select).attr('type')]['v'] = $(select).val();
						let lkp = Proval_Lookup.filter((x) => { return x.tbl_element == $(select).val(); })[0];
						if (lkp) mcObj[$(select).attr('type')]['lkp'] = lkp;
					}

    			});

				let materialArray = [], mCheck = false, cm = false, im = false, sktp = sketch_type, iin = false;

				if (!_.isEmpty(mcObj)) {
					let mt = sketch_type == 'Comm' ? 'CommMaterial' : 'ResMaterial';
					if (mcObj['comp']?.v && mcObj['comp']?.lkp) {
						if (mcObj['comp']['lkp'].ImpGradeLevel != null && mcObj['comp']['lkp'].ImpGradeLevel != '' && mcObj['comp']['lkp'].ImpGradeLevel != ' ') sktp = 'imps';
						if (mcObj['comp']['lkp'][mt] == 'Y') cm = true;
						if (mcObj['comp']['lkp']['ImpMaterial'] == 'Y') im = true; iin = true;
					}

					if (mcObj['prefix']?.v && mcObj['prefix']?.lkp) {
						if (mcObj['prefix']['lkp'].ImpGradeLevel != null && mcObj['prefix']['lkp'].ImpGradeLevel != '' && mcObj['prefix']['lkp'].ImpGradeLevel != ' ' && !iin) sktp = 'imps';
						if (mcObj['prefix']['lkp'][mt] == 'Y') cm = true;
						if (mcObj['prefix']['lkp']['ImpMaterial'] == 'Y') im = true; iin = true;
					}

					if (mcObj['constr']?.v && mcObj['constr']?.lkp) {
						if (mcObj['constr']['lkp'].ImpGradeLevel != null && mcObj['constr']['lkp'].ImpGradeLevel != '' && mcObj['constr']['lkp'].ImpGradeLevel != ' ' && !iin) sktp = 'imps';
						if (mcObj['constr']['lkp'][mt] == 'Y') cm = true;
						if (mcObj['constr']['lkp']['ImpMaterial'] == 'Y') im = true; iin = true;
					}

					if (mcObj['ext_feat_code']?.v && mcObj['ext_feat_code']?.lkp) {
						if (mcObj['ext_feat_code']['lkp'].ImpGradeLevel != null && mcObj['ext_feat_code']['lkp'].ImpGradeLevel != '' && mcObj['ext_feat_code']['lkp'].ImpGradeLevel != ' ' && !iin) sktp = 'imps';
						if (mcObj['ext_feat_code']['lkp'][mt] == 'Y') cm = true;
						if (mcObj['ext_feat_code']['lkp']['ImpMaterial'] == 'Y') im = true; iin = true;
					}

				}


				if ((sktp == 'imps' && im) || (sktp != 'imps' && cm)) {
					if (index == '1') {
						$("input[name='At_Gradeext_Materials']").removeAttr('disabled');
						$.each($("input[name='At_Gradeext_Materials']:checked"), function () {
							materialArray.push($(this).val());
						});
					}
					else if (index == '2') {
						$("input[name='Above_Gradeext_Materials']").removeAttr('disabled');
						$.each($("input[name='Above_Gradeext_Materials']:checked"), function () {
							materialArray.push($(this).val());
						});
					}

					var fselect = 0;
					if (ext_feature_selected && materialArray.length > 0) {
						materialArray.forEach(function (matr) {
							matr = matr.split("-")[1];
							lbl += (fselect == 0 ? "-" + matr : matr);
							fselect++;
						});
						lbl = lbl + ' ';
					}
					else if (materialArray.length > 0) {
						materialArray.forEach(function (matr) {
							matr = matr.split("-")[1];
							lbl += (fselect == 0 ? "155-" + matr : matr);
							fselect++;
						});
						lbl = lbl + ' ';
					}
				}
				else {
					if (index == '1') {
						$.each($("input[name='At_Gradeext_Materials']:checked"), function () { $(this)[0].checked = false; });
						$("input[name='At_Gradeext_Materials']").attr('disabled', true);
					}
					else if (index == '2') {
						$.each($("input[name='Above_Gradeext_Materials']:checked"), function () { $(this)[0].checked = false; });
						$("input[name='Above_Gradeext_Materials']").attr('disabled', true);
					}
				}

    			if(ext_covers && ext_covers != '' && ext_covers != '0' )
    		    	lbl=lbl+ext_covers + ' ';
    			if ((lbl && lbl != '') ||( mValue && mValue != '')){
    				if(mValue && mValue != '' || modSignValue && modSignValue !='')
    				 	lbl=lbl+(mValue ? mValue: '')+(modSignValue ? modSignValue: ''); 
    				 lbls.push(lbl);
    			}
    			if (code && code != '') codes.push(code.substring(0, code.length - 1));
    		});
    	$('.label_preview').html(lbls.reverse().join('|'));
    });
    $('.walkout').unbind();
    $('.walkout').bind('change', function() { 
		$('.skNote').show();    
    	isWalkout = true;
		$(this).parents('.extraval').prev().find('.comp').trigger('change');
		isWalkout = false;
    })
    $('.ns').keyup(function(){
    	$('.skNote').show();
        $('.ns').change();
    });
 	$("input[type='checkbox']").change(function(){
    	if(!$('.checkbox').prop("checked")){
    			//$('.checkbox').prop('checked', false);
			$('.Unarea').show();
			$('.Unperimeter').show();
		}
		else
		{
				//$('.checkbox').prop('checked', 'checked');	
			$('.Unarea').hide();
			$('.ValidateArea').hide();
			$('.Unperimeter').hide();
			$('.perimeter').val("");
			$('.sqft').val("");
		}
    });
    $('.sqft,.perimeter').keypress(function(event) {
    	if (event.keyCode == 13) {
   	     	event.preventDefault();
   		}
	});
    $('.sqft').keyup(function(t){
		$('.ValidateArea').hide();
		var sqfttt = $('.sqft').val();
		if(sqfttt && sqfttt.length > 10){
			sqfttt = sqfttt.slice(0,-1);
			$('.sqft').val(sqfttt);
		}
	});
	$('.perimeter').keyup(function(t){
		var peri = $('.perimeter').val();
		if(peri && peri.length > 10){
			peri = peri.slice(0,-1);
			$('.perimeter').val(peri);
		}
	});
    $('.Current_vector_details #Btn_Save_vector_properties').unbind(touchClickEvent);
    $('.Current_vector_details #Btn_Save_vector_properties').bind(touchClickEvent, function () {
		if(isEditTrue){
			var valid = true, nsVal;
    		var detailObj = {ext_feat_code: 0, comp: 0, prefix: 0, constr: 0, ns: 0, walkout: '', ext_cover: '', modifier: '', modifierSign:'', parent_type:'' , parent_imp_id: '', ImpType: '', ImpIdMethod: ''};
    		items[0].Value = $('.label_preview').html();
    		items[0].Newlabel = $('.label_preview').html();
    		if ((!items[0].labelDetails) || (type == 'edit' && sketchApp.currentVector.BasementFunctionality)) 
    			items[0].labelDetails = { 
    				details: {
    					1: _.clone(detailObj),
						2: _.clone(detailObj),
						3: _.clone(detailObj)
			 		},
    				labelValue: $('.label_preview').html()
			};
    		$('.lbl_level').each(function(lvl_idx, lvl){
    			$('select', lvl).each(function(lkp_idx, select){
    				items[0].labelDetails.details[lvl_idx + 1][$(select).attr('type')] = $(select).val();
    			});
    			items[0].labelDetails.details[lvl_idx + 1].ext_cover = $('.ext_cover', $(lvl).next().next()).val();
    			items[0].labelDetails.details[lvl_idx + 1].modifier = $('.modifier', $(lvl).next().next()).val();
    			items[0].labelDetails.details[lvl_idx + 1].modifierSign = $('.modifierSign', $(lvl).next().next()).val();
    			if ($('.prefix', lvl).val() == '10' || $('.prefix', lvl).val() == '15') {
	    			nsVal = $('.ns', $(lvl).next()).val();
	    			valid = valid && !isNaN(nsVal);
    				items[0].labelDetails.details[lvl_idx + 1].ns = (isNaN(nsVal)|| nsVal == "")? 0: parseInt(nsVal);
				}
    			if ($('.comp', lvl).val() == '202')
    				items[0].labelDetails.details[lvl_idx + 1].walkout = $('.walkout', $(lvl).next()).val();
    			else
    				items[0].labelDetails.details[lvl_idx + 1].walkout = "";
    			var materialArray = [];
 				if(lvl_idx == '1'){
    				$.each($("input[name='At_Gradeext_Materials']:checked"), function(){
						materialArray.push($(this).attr('value1'));
					});
    			}
    			else if(lvl_idx == '2'){
    				$.each($("input[name='Above_Gradeext_Materials']:checked"), function(){
						materialArray.push($(this).attr('value1'));
					});
    			}
    			
    			if(items[0].labelDetails.details[lvl_idx + 1].unfin_fin_area || items[0].labelDetails.details[lvl_idx + 1].fin_area) {
    				items[0].labelDetails.details[lvl_idx + 1].fin_area = 0;
    				items[0].labelDetails.details[lvl_idx + 1].unfin_fin_area = 0;
    			}
    			
    			var exter_feat_pre = items[0].labelDetails.details[lvl_idx + 1].ext_feat_code;
    			if(materialArray.length > 0){
    				var matSum = 0;
    				materialArray.forEach(function(matr){
    					matSum = matSum + parseInt(matr);
    				});
    				var ext_de = items[0].labelDetails.details[lvl_idx + 1].ext_feat_code;
    				if((ext_de != 0 && ext_de != '0')){
    					ext_de = matSum + ext_de;
    					items[0].labelDetails.details[lvl_idx + 1].ext_feat_code = ext_de;
    				}
    				else{
    					ext_de = matSum + '155';
    					items[0].labelDetails.details[lvl_idx + 1].ext_feat_code = ext_de;
    				}
    			}
    			var segment_Parent_Type ='',imptypeParent = '', impIdMethodParent ='', imptype = '';
    			var commpId = items[0].labelDetails.details[lvl_idx + 1].comp;
				var preefixId = items[0].labelDetails.details[lvl_idx + 1].prefix;
				var ext_fet_id = items[0].labelDetails.details[lvl_idx + 1].ext_feat_code;
				var constr_id = items[0].labelDetails.details[lvl_idx + 1].constr;
				var ext_cover_id = items[0].labelDetails.details[lvl_idx + 1].ext_cover;
				var mod_id = items[0].labelDetails.details[lvl_idx + 1].modifier;
				if (type == 'edit' && items[0].labelDetails.details[lvl_idx + 1].parent_imp_id)  //clearing parent_imp_id for edited records. when edit label, create new sketch and creates new record for that sketch. so new Id updated
                     items[0].labelDetails.details[lvl_idx + 1].parent_imp_id = '';  
				if((commpId != 0 && commpId != '0') || (preefixId != 0 && preefixId != '0') || (ext_fet_id != 0 && ext_fet_id != '0') || (constr_id != 0 && constr_id != '0') || (ext_cover_id != '' && ext_cover_id !='0') ||(mod_id != '')){
    				if(sketch_type == 'Res'){
    					segment_Parent_Type = 'dwellings';
    					imptypeParent = 'DWELL';
    					imptype = 'D'
    				}
    				else{
    					segment_Parent_Type = 'comm_bldgs';
    					imptypeParent = 'COMBLDG';
    					imptype = 'C'
    				}			
    				if(commpId != 0 && commpId != '0'){
    					var lookupValss = Proval_Lookup.filter(function(lkup){ return lkup.tbl_element == commpId; })[0];
    					if(lookupValss.ImpGradeLevel != null && lookupValss.ImpGradeLevel != '' &&  lookupValss.ImpGradeLevel != ' ')
    						segment_Parent_Type = 'imps';
    					if(segment_Parent_Type == 'dwellings'){
    						imptypeParent = lookupValss.ImpType ? lookupValss.ImpType : imptypeParent;
    						impIdMethodParent = lookupValss.ImpIDMethod ? lookupValss.ImpIDMethod : imptype;
    					}
    					else if(segment_Parent_Type == 'comm_bldgs'){
    						imptypeParent = lookupValss.CommImpType ? lookupValss.CommImpType : imptypeParent;
    						impIdMethodParent = lookupValss.CommImpIDMethod ? lookupValss.CommImpIDMethod : imptype;
    					}
    					else{
    						imptypeParent = lookupValss.ImpType1 ? lookupValss.ImpType1 : 'G';
    						impIdMethodParent = lookupValss.ImpIDMethod1 ? lookupValss.ImpIDMethod1 : "ATTGAR";
    					}
    				}
    				else if(preefixId != 0 && preefixId != '0'){
    					var lookupValss = Proval_Lookup.filter(function(lkup){ return lkup.tbl_element == preefixId; })[0];
    					if(lookupValss.ImpGradeLevel != null && lookupValss.ImpGradeLevel != '' &&  lookupValss.ImpGradeLevel != ' ')
    						segment_Parent_Type = 'imps';
    					if(segment_Parent_Type == 'dwellings'){
    						imptypeParent = lookupValss.ImpType ? lookupValss.ImpType : imptypeParent;
    						impIdMethodParent = lookupValss.ImpIDMethod ? lookupValss.ImpIDMethod : imptype;
    					}
    					else if(segment_Parent_Type == 'comm_bldgs'){
    						imptypeParent = lookupValss.CommImpType ? lookupValss.CommImpType : imptypeParent;
    						impIdMethodParent = lookupValss.CommImpIDMethod ? lookupValss.CommImpIDMethod : imptype;
    					}
    					else{
    						imptypeParent = lookupValss.ImpType1 ? lookupValss.ImpType1 : 'G';
    						impIdMethodParent = lookupValss.ImpIDMethod1 ? lookupValss.ImpIDMethod1 : "ATTGAR";
    					}
    				}
    				else if(ext_fet_id != 0 && ext_fet_id != '0'){
    					var lookupValss = Proval_Lookup.filter(function(lkup){ return lkup.tbl_element == exter_feat_pre; })[0];
    					lookupValss = lookupValss ? lookupValss : {};
    					if(lookupValss.ImpGradeLevel != null && lookupValss.ImpGradeLevel != '' &&  lookupValss.ImpGradeLevel != ' ')
    						segment_Parent_Type = 'imps';
    					if(segment_Parent_Type == 'dwellings'){
    						imptypeParent = lookupValss.ImpType ? lookupValss.ImpType : imptypeParent;
    						impIdMethodParent = lookupValss.ImpIDMethod ? lookupValss.ImpIDMethod : imptype;
    					}
    					else if(segment_Parent_Type == 'comm_bldgs'){
    						imptypeParent = lookupValss.CommImpType ? lookupValss.CommImpType : imptypeParent;
    						impIdMethodParent = lookupValss.CommImpIDMethod ? lookupValss.CommImpIDMethod : imptype;
    					}
    					else{
    						imptypeParent = lookupValss.ImpType1 ? lookupValss.ImpType1 : 'G';
    						impIdMethodParent = lookupValss.ImpIDMethod1 ? lookupValss.ImpIDMethod1 : "ATTGAR";
    					}
    				}
    			}
    			items[0].labelDetails.details[lvl_idx + 1].parent_type = segment_Parent_Type;
    			items[0].labelDetails.details[lvl_idx + 1].ImpType = imptypeParent;
    			items[0].labelDetails.details[lvl_idx + 1].ImpIdMethod = impIdMethodParent ? impIdMethodParent : imptype;
    		});
    		var validateLabel = false, i = 1, ValidateArea = false, CheckUnsketch = false;
    		for(i=1; i<=3;i++){
    			var Details = items[0].labelDetails.details[i];
    			if(Details.prefix != '0'  || Details.comp != '0' || Details.ext_feat_code != '0'){
    				validateLabel = true;
    				break;
    			}
    		}
    		if(!validateLabel){
    			$('.ValidateArea').hide();
				$('.ValidateLabel').show();
				return false;
    		}
    		if($( '.UnSketchedarea .checkbox' )[0] && !$( '.UnSketchedarea .checkbox' )[0].checked){
				var areaa = $('.sqft').val();
				CheckUnsketch = true;
				if(areaa != '')
					ValidateArea = true;
			}
			if(validateLabel && CheckUnsketch && !ValidateArea){
				$('.ValidateArea').show();
				return false;
			}
			
			var labelDatailsFeat = items[0].labelDetails.details;
			var extfeatAlone = false, MapExteriorFeature = false, parentTypeArrayss = [], showUpperInLabel = false;
			$.each(labelDatailsFeat,function(det){
				var lbfeat = labelDatailsFeat[det];
				parentTypeArrayss.push({partType: lbfeat.parent_type});
				if(((lbfeat.comp != 0 && lbfeat.comp != '0') || (lbfeat.prefix != 0 && lbfeat.prefix != '0') || (lbfeat.constr != 0 && lbfeat.constr != '0') || (lbfeat.modifier != '')))
					extfeatAlone = true;
				if(det == 1 || det == 2 || det == '1' || det == '2'){
				    if((lbfeat.comp != 0 && lbfeat.comp != '0') || (lbfeat.prefix != 0 && lbfeat.prefix != '0') || (lbfeat.constr != 0 && lbfeat.constr != '0') || (lbfeat.ext_cover != '' && lbfeat.ext_cover !='0') || (lbfeat.modifier != '') || (lbfeat.ext_feat_code != 0 && lbfeat.ext_feat_code != '0'))
				        showUpperInLabel = true;
				}
			});
			
			if(!showUpperInLabel){ // show upper in sketch if select only above grade labels
            	items[0].Value = items[0].Value + ' (Upper)';
    		    items[0].Newlabel = items[0].Newlabel + ' (Upper)';
            }
			
			if(!extfeatAlone){
				MapExteriorFeature = true;
			}
			else{
				$.each(labelDatailsFeat,function(det){
					if(det != 1 && det != '1'){
						var lbfeat = labelDatailsFeat[det];
						if(lbfeat.ext_feat_code != 0 && lbfeat.ext_feat_code != '0'){
							if((lbfeat.comp != 0 && lbfeat.comp != '0') || (lbfeat.prefix != 0 && lbfeat.prefix != '0') || (lbfeat.constr != 0 && lbfeat.constr != '0') || (lbfeat.ext_cover != '' && lbfeat.ext_cover !='0') || (lbfeat.modifier != '')){
							}
							else{
								if(det == 2 || det == '2'){
									if(parentTypeArrayss[0].partType)
										lbfeat.parent_type = parentTypeArrayss[0].partType;
									else if(parentTypeArrayss[2].partType)
										lbfeat.parent_type = parentTypeArrayss[2].partType;
								}
								else if(det == 3 || det == '3'){
									if(parentTypeArrayss[1].partType)
										lbfeat.parent_type = parentTypeArrayss[1].partType;
									else if(parentTypeArrayss[0].partType)
										lbfeat.parent_type = parentTypeArrayss[0].partType;
								}
							}
						}
					}
				});
			}
			var areaSketch = false, sqftvalue = 0, perivalue = 0;
			var unsketchedAreaStatus = ($( '.UnSketchedarea .checkbox' ).length > 0 && $( '.UnSketchedarea .checkbox' )[0].checked)
			if( $( '.UnSketchedarea .checkbox' ).length > 0 && !$( '.UnSketchedarea .checkbox' )[0].checked){
				areaSketch = true;
				sqftvalue = $('.sqft').val() ? $('.sqft').val(): 0;
				perivalue = $('.perimeter').val() ? $('.perimeter').val() : 0;
			}
    		if (valid) {
    			var basefunctionTrue = false;
    			function sktsave(){
    	    		if(type == 'edit' && sketchApp.currentVector && sketchApp.currentVector.isUnSketchedTrueArea && unsketchedAreaStatus){
    					sketchApp.currentVector.vectorString = null;
                		sketchApp.currentVector.startNode = null;
                		sketchApp.currentVector.endNode = null;
                		sketchApp.currentVector.commands = [];
                		sketchApp.currentVector.endNode = null;
                		sketchApp.currentVector.placeHolder = false;
                		sketchApp.currentVector.Line_Type = false;
                		sketchApp.render()
                		sketchApp.startNewVector( true );
                		sketchApp.currentVector.isUnSketchedTrueArea = false;
                		sketchApp.currentVector.fixedAreaTrue = null;
                		sketchApp.currentVector.fixedPerimeterTrue = null;
                		sketchApp.currentVector.isClosed = false;
    				}
    				else if(type == 'edit' && sketchApp.currentVector && sketchApp.currentVector.isUnSketchedTrueArea && !unsketchedAreaStatus){
    					sketchApp.currentVector.fixedAreaTrue = $('.sqft').val() != '' ? $('.sqft').val() : sketchApp.currentVector.areaFieldValue;
    					sketchApp.currentVector.fixedPerimeterTrue = $('.perimeter').val() != '' ? $('.perimeter').val() : sketchApp.currentVector.perimeterFieldValue; 
    				}
    				if(type == 'edit'){
						sketchApp.deletedVectors.push(_.clone(deleteVectorInEdit[0]));
    					sketchApp.currentVector.newRecord = true;
    				}
	    			if (callback) callback(items, {'unSketchedTrue': ( areaSketch ? areaSketch : false), 'Sqft': (sqftvalue ? sqftvalue: 0), 'perimeter':(perivalue ? perivalue : 0), 'BasementFunctionality':basefunctionTrue, 'MapExteriorFeature': MapExteriorFeature});
	    			if (!editor.isScreenLocked) $('.mask').hide();
					$('.CurrentLabel').remove();
					$('.Current_vector_details').css('top','25%');
					$('.Current_vector_details').css('left','35%');
	        		$('.Current_vector_details').hide();
	        		return false;
	        	}
    			if(((items[0].labelDetails.details[1].prefix == '1' || items[0].labelDetails.details[1].prefix == 1) || (items[0].labelDetails.details[1].prefix == '2' || items[0].labelDetails.details[1].prefix == 2) || (items[0].labelDetails.details[1].prefix == '3' || items[0].labelDetails.details[1].prefix == 3)) && (items[0].labelDetails.details[1].ext_feat_code == 0 ||items[0].labelDetails.details[1].ext_feat_code == '0') && (items[0].labelDetails.details[1].ext_cover == 0 || items[0].labelDetails.details[1].ext_cover == '0') && (items[0].labelDetails.details[1].constr == 0 || items[0].labelDetails.details[1].constr == '0')){
    				basefunctionTrue = true;
    				var txtcomp = false, compval = false;
    				if(items[0].labelDetails.details[1].comp != '0' && items[0].labelDetails.details[1].comp != '0'){
    					var fiid = items[0].labelDetails.details[1].comp;
    					var plookup = Proval_Lookup.filter(function(x){return x.tbl_element == fiid})[0];
    					if(plookup.AdditionalScreen && (plookup.AdditionalScreen == "BGAreaEntry" || (plookup.AdditionalScreen.split("/")[0] == "BGAreaEntry")))
    						compval = true;
    				}
    				if(!compval){
    					basefunctionTrue = false;
						sktsave();
						return false;
    				}
    				else{
    					if(type == 'edit'){
    						if(clval != ($('.label_preview').html()))
    							txtcomp = true;
    					}	
    					BasementFunctionality(txtcomp, function(){
    						sktsave();
    						return false;
    					});
    				}
    				return false;
    			}
    			else{
    				if(type == 'edit'){
    					if(sketchApp.currentVector.BasementFunctionality)
    						sketchApp.currentVector.BasementFunctionality = false;
    				}
    				sktsave();
    				return false;
    			}
        	}
        	else messageBox('Please check the values.')
      	}
		else{
			$('.CurrentLabel').remove();
			$('.Current_vector_details').css('top','25%');
			$('.Current_vector_details').css('left','35%');
        	$('.Current_vector_details').hide();
        	if (!sketchApp.isScreenLocked) $('.mask').hide();
        	return false;
		}      
    });
    
    $('.Current_vector_details #Btn_cancel_vector_properties').unbind(touchClickEvent)
    $('.Current_vector_details #Btn_cancel_vector_properties').bind(touchClickEvent, function() {
		if(type == 'edit' && backFromBasementFunctionality)
			items[0] = backFromBasementFunctionalityLabelDaetails[0];
		$('.CurrentLabel').remove();
		$('.Current_vector_details').css('top','25%');
		$('.Current_vector_details').css('left','35%');
        $('.Current_vector_details').hide();
        if (!sketchApp.isScreenLocked) $('.mask').hide();
        return false;
    }); 
	if (type == 'edit' || backFromBasementFunctionality) {
	    [1,2,3].forEach(function(i) {
	    	if (!items[0].labelDetails.details) return;
	    	var element = items[0].labelDetails.details[i];
	    	var extraVal = false;
	    	if(element && sketchApp.currentVector && sketchApp.currentVector.BasementFunctionality && i == 1 && !backFromBasementFunctionality){
	    			var com = items[0].labelDetails.details[i].comp1;
	    			var pre = items[0].labelDetails.details[i].prefix1;
	    			var modi = items[0].labelDetails.details[i].modifier1;
	    			var msig = items[0].labelDetails.details[i].modifierSign1;
	    			var walkout = items[0].labelDetails.details[i].walkout1;
	    			var className = '.' + lbl_level.filter(function(l){ return l.value1 == i; })[0].value2;
	    			$(className + '_Grade.walkout' + '.ns').attr('readonly', 'readonly');
					
					$('.' + 'comp', $('.lbl_level').eq(i-1)).val(com);
					if (com && com.toString() == '202') {
						$('.segmentLbl').eq(((i - 1) * 4) + 2).trigger('change');
						$(className + '_Grade.walkout').removeAttr('readonly');
						$(className + '_Grade.walkout').val(walkout);
						extraVal = true;
					}
					else $(className + '.walkout').attr('readonly', 'readonly');
					$('.' + 'prefix', $('.lbl_level').eq(i-1)).val(pre);
					$(className + '_Grade.' + 'modifier').val(modi);
					$(className + '_Grade.' + 'modifierSign').val(msig);
	    	}
			else if (element) {
				let mtEnable = false, cm = false, im = false, cNames = (i == 2) ? 'At_Gradeext_Materials' : 'Above_Gradeext_Materials';
				$.each(element, function (type, value) {
					let lkp = null;
					if(type == 'ext_feat_code' && i != 1){
						var ext_split_code = parseInt(value);
						var ext_temp = ext_split_code % 1000;
						lkp = Proval_Lookup.filter((x) => { return x.tbl_element == ext_temp })[0];
						$('.' + type, $('.lbl_level').eq(i-1)).val(ext_temp);
						var mtValues = parseInt((ext_split_code/1000));
						if(mtValues != '0' || mtValues != 0){
							mtValues = parseInt(mtValues);
							var baseValue = parseInt(parseInt(mtValues,10).toString(2));
							var j = 0;
							while(baseValue > 0){
								j = j + 1;
								var rem = baseValue % 10;
								if(rem > 0){
									switch(j){
										case 1: $(".ext_Materials[name='" + cNames + "'][value1='1']").prop('checked','checked'); break;
										case 2: $(".ext_Materials[name='" + cNames + "'][value1='2']").prop('checked','checked'); break;
										case 3: $(".ext_Materials[name='" + cNames + "'][value1='4']").prop('checked','checked'); break;
										case 4: $(".ext_Materials[name='" + cNames + "'][value1='8']").prop('checked','checked'); break;
									}
									mtEnable = true;
								}
								baseValue = parseInt(baseValue / 10);
							}
						}
					}
					else
						$('.' + type, $('.lbl_level').eq(i-1)).val(value);
					var className = '.' + lbl_level.filter(function(l){ return l.value1 == i; })[0].value2;
					if (type == 'prefix') {
						if (value && (value.toString() == '10') || value.toString() == '15') { 
							$(className + '_Grade' + '.ns').removeAttr('readonly');
							$(className + '.' + type).val(value);
							extraVal = true;
						}
						else{
							$(className + '_Grade.walkout' + '.ns').attr('readonly', 'readonly');
							$(className + '.' + type).val(value);
						}
						lkp = Proval_Lookup.filter((x) => { return x.tbl_element == value })[0];
					}
					if(type=='constr'){
						if (value > 0) {
							$(className + '.' + type).val(value);
							lkp = Proval_Lookup.filter((x) => { return x.tbl_element == value })[0];
						}
					}
					if (type == 'comp') {
						if(value > 0) {
	    					$('.segmentLbl').eq(((i - 1) * 4) + 2).trigger('change');
	    					if (value && value.toString() == '202') {
								$(className + '_Grade.walkout').removeAttr('readonly');
								$(className + '_Grade.walkout').val(element['walkout']);
								extraVal = true;
							}
							else $(className + '.walkout').attr('readonly', 'readonly');
							lkp = Proval_Lookup.filter((x) => { return x.tbl_element == value })[0];
						}
					}
					if (type == 'modifier' || type == 'ext_cover' || type == 'modifierSign' || type == 'ns') $(className + '_Grade.' + type).val(value);

					let mt = sketch_type == 'Comm' ? 'CommMaterial' : 'ResMaterial';
					if (lkp && lkp[mt] == 'Y') cm = true;
					if (lkp && lkp['ImpMaterial'] == 'Y') im = true;
				});

				if (mtEnable || (!mtEnable && ((element.parent_type == "imps" && im) || (element.parent_type != "imps" && cm)))) {
					$("input[name='" + cNames + "']").removeAttr('disabled');
				}
				else {
					$.each($("input[name='" + cNames + "']:checked"), function () { $(this)[0].checked = false; });
					$("input[name='" + cNames + "']").attr('disabled', true);
				}
			}
			if (!extraVal) $('.lbl_level').eq(i-1).next('tr').hide();
			else $('.lbl_level').eq(i-1).next('tr').show();
	    });
	    $('.modifier').trigger('change');
	    $('.skNote').html('<span><b>Sketch Label Preview:</b>&nbsp;</span><span class="label_preview">' + (lblCurrentField || '') + '</span>');
    }
    $('.skNote').hide();
}
changesegment();
 }
	$('.mask').hide();
	NoNBasementFunctionality();
	return false;
}

function updateDefaultValues(parcelId, aRowid,pROwid, tbleName, man_housing){
	var getDataField = window.opener? window.opener.getDataField: getDataField;
	var statusField = getDataField('status', tbleName);
	var ShowFutureData = (window.opener? window.opener.showFutureData : showFutureData);
	var statusVal = (man_housing ? 'A' : (ShowFutureData ? 'F' : 'A'));
	var FYvalue = '0';
	if (clientSettings.FutureYearValue) {
		FYvalue = clientSettings.FutureYearValue;
	}
	if(clientSettings['FutureYearEnabled'] == '1') {
		var YearField = getDataField('Year', tbleName);
		var CC_YearStatusVal = statusVal;
		var YearFieldVal = (ShowFutureData ? FYvalue : clientSettings["CurrentYearValue"] );
		if(statusField)
			saveData += '^^^' + parcelId + '|' + aRowid + '|' + statusField.Id + '||' + statusVal + '|edit|' + pROwid + '\n';
		if(YearField)
			saveData += '^^^' + parcelId + '|' + aRowid + '|' + YearField.Id + '||' + YearFieldVal + '|edit|' + pROwid + '\n';
	}
	else {
		if(statusField)
			saveData += '^^^' + parcelId + '|' + aRowid + '|' + statusField.Id + '||' + statusVal + '|edit|' + pROwid + '\n';
	}
}

function NumericScaleHandle(dFields,NewValue,callback){
	var NAME = dFields.Name ;
	var NameCheck = false ;
	if(NAME){
		if(NAME.indexOf('area') > -1 || NAME.indexOf('perimeter') > -1)
			NameCheck = true;
	}
	if(NameCheck && (dFields.InputType == '8' || dFields.InputType == '2') ){
		var Numscale =   dFields.NumericScale;
		if(Numscale == 0 || Numscale == '0')
			NewValue = parseInt(NewValue);
		if(callback) callback(NewValue);
    }
	else
		if(callback) callback(NewValue);
}

function dimensionConversion(p1, p2, p3, area, exthead_scale){
	var RVal = 0, LVal = 0, DVal = 0, UVal = 0, retVal = 0;
	if(area < 300){
		if(exthead_scale < 150)
			RVal = "12", LVal = "65524", DVal = "786432", UVal = "-786432";
		else
			RVal = "16", LVal = "65520", DVal = "1048576", UVal = "-1048576";
	}
	else{
		if(exthead_scale < 150)
			RVal = "18", LVal = "65518", DVal = "1310720", UVal = "-1310720";
		else
			RVal = "30", LVal = "65506", DVal = "1835008", UVal = "-1966080";	
	}
	if(p1.match('[\A]') || p1.match('[\B]'))
		retVal = 0;
	else if(!(p1.split("/").length > 1 || p2.split("/").length > 1)){
		if(p1.match('[\L]') || p1.match('[\R]')){
			if(!(p2.match('[\A]') || p2.match('[\B]')) && (p2.match('[\U]') || p2.match('[\D]')))
				retVal = (p2.match('[\U]')) ? UVal: DVal;	
			else if(!(p3.match('[\A]') || p3.match('[\B]')) && (p3.match('[\U]') || p3.match('[\D]')))
				retVal = (p3.match('[\U]')) ? UVal: DVal;
			else	
				retVal = 0;
		}
		else{
			if(!(p2.match('[\A]') || p2.match('[\B]')) && (p2.match('[\L]') || p2.match('[\R]')))
				retVal = (p2.match('[\L]')) ? LVal: RVal ;	
			else if(!(p3.match('[\A]') || p3.match('[\B]')) && (p3.match('[\U]') || p3.match('[\D]')))
				retVal = (p3.match('[\L]')) ? LVal : RVal;	
			else	
				retVal = 0;
		}
	}
	else{
		var len1 = 0 , len2 = 0, pointVal = '';
		var p1Split = p1.split("/");
		if(p1Split.length > 1 ) {
			var len1 = parseFloat((p1Split[0].match(/[a-zA-Z]+|[0-9]+(?:\.[0-9]+|)/g))[1]);
			var len2 = parseFloat((p1Split[1].match(/[a-zA-Z]+|[0-9]+(?:\.[0-9]+|)/g))[1]);
		}
		else
			var len1 = parseFloat((p1Split[0].match(/[a-zA-Z]+|[0-9]+(?:\.[0-9]+|)/g))[1]);
		if(len1 > len2 )
			pointVal = p1Split[0];
		else
			pointVal = p1Split[1];
		if(pointVal.match('[\L]') || pointVal.match('[\R]')){
			if(pointVal.match('[\L]'))
				retVal = (p2.match('[\U]') || (!(p2.match('[\D]')) && (p2.match('[\R]')))) ? UVal:  DVal;
			else
				retVal = (p2.match('[\U]') || (!(p2.match('[\D]')) && (p2.match('[\L]')))) ? UVal: DVal;
		}
		else {
			if(pointVal.match('[\U]'))
				retVal = ((!(p2.match('[\U]')) && (p2.match('[\R]'))) || p2.match('[\D]')) ? RVal: LVal;
			else
				retVal = ((!(p2.match('[\U]')) && (p2.match('[\L]'))) || p2.match('[\D]')) ? LVal: RVal;
		}
	}
	return retVal;
}

function ProvalSketchSaveNew(sketchDataArray, noteData, sketchSaveCallBack, onError) {
	var Proval_Lookup = window.opener ? window.opener.Proval_Lookup : [];
	var OutB_Lookups=lookup?lookup:(window.opener?window.opener.lookup:[]);
	var Proval_Default_Exterior_Cover = window.opener ? window.opener.Proval_Default_Exterior_Cover : [];
	var ConfigTables = window.opener? window.opener.ccma.Sketching.Config.ConfigTables : ccma.Sketching.Config.ConfigTables;
	var CC_YearStatus = ( (clientSettings['FutureYearEnabled'] == '1') ? ( (window.opener? window.opener.showFutureData : showFutureData) ? 'F' : 'A'  ) : "" );
	var totalLength = 0,savedataLength = 0,TotalValueLength = 0,ValueLength = 0, deleteMHRecords = [], parentRecordProperties = [], dwellingRecordArray = [], commRecordArray = [], MHRecordArray = [], finalFloorRecordsArray = [], extensionRecordsArray = [], ProvalExteriorFeatureCounter = [], exteriorFeatureCreationArray = [];
	var otherValuesImpUpdates = [];
	if (sketchDataArray.length == 0 && noteData.length == 0) { 
		if (sketchSaveCallBack) sketchSaveCallBack(); return; 
	}
	
	//var parcel = window.opener? window.opener.activeParcel: activeParcel;
    var getDataField = window.opener? window.opener.getDataField: getDataField;
    var getCategoryFromSourceTable = window.opener? window.opener.getCategoryFromSourceTable: getCategoryFromSourceTable;
	
	var floor_records = [], copy_parentData = [], deletedSketchFloorRecords = [];
	var savedata = [], deleteData = [], saveNoteData = [], newRecords = [], deleteNoteData = [], parentData = []; 
	var noteTxtField = getDataField('note_text', ConfigTables.SktNote), notePosField = getDataField('note_position', ConfigTables.SktNote);
	var last_update_field = getDataField('last_update', ConfigTables.SktHeader), extensions = [], seg_id = 0;
	
	function fun() {
		var isdtr = window.opener ? window.opener.__DTR : (typeof (__DTR) !== 'undefined' ? __DTR : false);
		let isSvApp = window.opener && window.opener.appType == "sv" ? true : (typeof (appType) !== 'undefined' && appType == "sv" ? true : false);
		var _appType = isdtr ? 'DTR' : (isSvApp ? 'SV' : 'QC');
		var saveRequestData = {
        	ParcelId: parcel.Id,
        	Priority: '',
        	AlertMessage: '',
        	Data: saveData,
        	recoveryData: '',
        	AppraisalType: '',
        	activeParcelPhotoEditsdata: '',
        	appType: _appType
    	};
    
    	if (saveData != '') {
    		console.log(saveData);
        	$.ajax({
            	url: '/quality/saveparcelchanges.jrq',
            	type: "POST",
            	dataType: 'json',
            	data: saveRequestData,
            	success: function (resp) {
                	if (resp.status == "OK"){
                		console.log('AfterSaveSucces');
                    	if (sketchSaveCallBack) sketchSaveCallBack(afterSketchProval);
                    }
                    else{
                    	console.log('AfterSavefailed' + resp.message);
                    }
            	}
        	});
    	}
    	else {
        	if (sketchSaveCallBack) sketchSaveCallBack(afterSketchProval);
        	return;
    	}
	}
	
	var processNoteData = function() {
		var delCount = 0, noteCatId = getCategoryFromSourceTable(ConfigTables.SktNote).Id;
		var deleteNote = function(callback) {
			if (deleteNoteData.length == 0) callback();
			else
				deleteNoteData.forEach(function(delNote){
					saveData += '^^^' + parcel.Id + '|' + delNote + '|||' + ConfigTables.SktNote + '|delete|\n';
					delCount++; 
					if (delCount == deleteNoteData.length) callback(); 
				});
		}
		deleteNote(function() {
			var saveNoteDataLength = saveNoteData.length, i = 0, SaveNotePosition = 0;
			for(i = 0;i < saveNoteDataLength;i++){
				var noteData = saveNoteData.pop();
				var note_record;
				if(noteData && (!noteData.parentData || noteData.parentData.lengh == 0))
				 	note_record = parcel[ConfigTables.SktNote].filter(function(ntr){ return ntr.ROWUID == noteData.rowid })[0];	
				if(noteData && noteData.rowid){
					if(isNaN(noteData.position))
						SaveNotePosition = 0;
					else
						SaveNotePosition = noteData.position;
					var ParetRowuid=(note_record)? note_record.ParentROWUID : noteData.parentData.ROWUID;
					saveData += '^^^' + parcel.Id + '|' + noteData.rowid + '|' + noteTxtField.Id + '||' + noteData.text + '|edit|' + ParetRowuid + '\n';
					saveData += '^^^' + parcel.Id + '|' + noteData.rowid + '|' + notePosField.Id + '||' + SaveNotePosition + '|edit|' + ParetRowuid + '\n';
				}
			}
			if (saveNoteData.length == 0) {
				/*var hdr_data;
				if (parcel[ConfigTables.SktHeader])
					extensions.forEach(function(ext) {
						if(ext && (ext!=" ")){
							hdr_data = parcel[ConfigTables.SktHeader].filter(function(hdr){ return hdr.extension == ext })[0];
							if (hdr_data)
								saveData += '^^^' + parcel.Id + '|' + hdr_data.ROWUID + '|' + last_update_field.Id + '||' + ext + '|edit|' + hdr_data.ParentROWUID + '\n'
						}
					})*/
				if((totalLength == savedataLength) && (TotalValueLength == ValueLength))	
					fun();
			}
		});
	}
	
	var saveParentData = function() {	
		var res_fields = ['attic_area', 'attic_fin_area', 'bsmnt_area', 'bsmnt_fin_area', 'bgar_area', 'crawl_area', 'crawl_code', 'basement_code', 'bgar_capacity'], fields = [], values = [], fnames = [];
		var comm_fields = ['total_area', 'number_floors'];
		var val = {}, fin_area, unfin_area, floor_recordd = [], flr_val = {}, deleteFloorRecordArray = [];
		extensionRecordsArray.forEach(function(ext_row){
			var ext_rowuid = ext_row.ext_rowuid;
			var deRecord = parcel['ccv_extensions'].filter(function(rfrecord){return (rfrecord.ROWUID == ext_rowuid)})[0];
			var patable = (deRecord.RorC == 'R')? 'ccv_improvements_dwellings': 'ccv_improvements_comm_bldgs';
			var fl_table = (deRecord.RorC == 'R')? ConfigTables.res_floor: ConfigTables.comm_floors;
			var floor_key_col = (deRecord.RorC == 'R')? 'floor_key': 'floor_number';
			var parrec = deRecord[patable].filter(function(ret){return ret.CC_Deleted != true})[0];
			//var extERC = extension_Recd_Array.filter(function(ere){return ere.eRowuid == ext_rowuid})[0];
			//parrec = parrec ? parrec: extERC.CdRecord[0];
			if(parrec){
				var flr_records = parrec[fl_table].filter(function(sk){ return sk.CC_Deleted != true});
				var rlrRowuidArray = [];
				flr_records.forEach(function(fre){
					rlrRowuidArray.push(fre.ROWUID);
				});
				finalFloorRecordsArray.forEach(function(frecc){
					var frecrec = frecc.frecRec;
					var exte = frecrec.extensionRowuid;
					if(exte == ext_rowuid){
						var fl_key = frecrec.floorKey;
						var fl_key1 = fl_key;
						var fl_key2 = fl_key + '.0';
						for(i in flr_records){
							if(flr_records[i][floor_key_col] == fl_key1 || flr_records[i][floor_key_col] == fl_key2){
								var indc = rlrRowuidArray.indexOf(flr_records[i].ROWUID)
								if(indc != '-1' && indc != -1)
									var rrcr = rlrRowuidArray.splice(indc, 1);
								break;
							}
						}
					}	
				});
				rlrRowuidArray.forEach(function(fcc){
					deleteFloorRecordArray.push(_.clone({delRowuid: fcc, deleTable: fl_table}))
				});
			}
		});
		if(parentData.length == 0){
			if(copy_parentData.length > 0){
				parentData.push(copy_parentData[0]);
			}
		}
		parentData.forEach(function(pdata) {
			var flr_rec_num = 0;
			var parentRecordsources = parentRecordProperties;
			var updatetotalarea = [];
			var pRowu = pdata.rowid;
			//var kk = finalFloorRecordsArray;
			if(parentRecordsources.length > 0){
				parentRecordsources.forEach(function(preco){
					var extension_rowuid = preco.extRowuid ;
					if(pRowu == extension_rowuid){
						var improve_id_row = preco.improve_id;
						var recor = preco.record;
						var sou_table  = preco.sour_table;
						if(recor){
							var rec = parcel[sou_table].filter(function(sk){ return sk.ROWUID == recor.ROWUID && sk.CC_Deleted != true})[0];
							//var extERC = extension_Recd_Array.filter(function(ere){return ere.eRowuid == extension_rowuid})[0];
							//rec = rec ? rec : (extERC ? extERC.CdRecord.filter(function(flr_dt){ return flr_dt.ROWUID == recor.ROWUID })[0] : []);
							var level_cod = preco.level_code;
							var fin_ar = preco.fin_area ? Math.round(preco.fin_area) : 0;
							var comp_idsss = preco.comp_idss, totareaB = 0, attf = 0 ,basefin = 0, totareaA = 0, bgar_area = 0, crawl_area = 0, bgar_capacity = 0, basement_areas = 0;
							if(comp_idsss == '202'){
								totareaB = preco.Tarea ? Math.round(preco.Tarea) : 0;
								basefin = Math.round(fin_ar);
								basement_areas = totareaB;
							}
							else if(comp_idsss == '201'){
								totareaA = preco.Tarea ? Math.round(preco.Tarea) : 0;
								attf = Math.round(fin_ar);
							}	
							if(preco.bgar_label){
								totareaB = totareaB + Math.round(preco.bgar_area);
	                        	bgar_area = Math.round(preco.bgar_area);
	                    	}
	                    	if(preco.crawl_label)
	                    		crawl_area = Math.round(preco.crawl_area)
	                    	if(preco.bgar_capacity)
	                    		bgar_capacity = parseFloat(preco.bgar_capacity);
							var un_ar = preco.unfin_area ? Math.round(preco.unfin_area) : 0;
							var tt_ar = preco.Tarea ? Math.round(preco.Tarea) : 0;
							var eff_peri = preco.effperimeter ? Math.round(preco.effperimeter) : 0;
							var ctrue = false;
							if(sou_table == "ccv_improvements_dwellings"){
								updatetotalarea.forEach(function(ttarea){
									if(ttarea.ext_rowuid == extension_rowuid && ttarea.improve_id == improve_id_row && ttarea.stable == sou_table){
										ctrue = true;
										ttarea.attic_area = Math.round(ttarea.attic_area + totareaA);
										ttarea.attic_fin_area = Math.round(ttarea.attic_fin_area + attf);
										if(level_cod.toString() == '1'){
											ttarea.bsmnt_area = Math.round(ttarea.bsmnt_area + totareaB);
											ttarea.bsmnt_fin_area = Math.round(ttarea.bsmnt_fin_area + basefin);
											ttarea.bgar_area = Math.round(ttarea.bgar_area + bgar_area);
											ttarea.crawl_area = Math.round(ttarea.crawl_area + crawl_area);
											ttarea.basement_areas = Math.round(ttarea.basement_areas + basement_areas);
											if(bgar_capacity != 0)
												ttarea.bgar_capacity = parseFloat(ttarea.bgar_capacity + parseFloat(bgar_capacity));
										}	
									}
								});
							}
							else if(sou_table == "ccv_improvements_comm_bldgs"){
								updatetotalarea.forEach(function(ttarea){
									if(ttarea.ext_rowuid == extension_rowuid && ttarea.improve_id == improve_id_row && ttarea.stable == sou_table){
										ctrue = true;
										ttarea.total_area = Math.round(ttarea.total_area + tt_ar);
										ttarea.eff_perimeter = Math.round(ttarea.eff_perimeter + eff_peri);
									}
								});
							}
							if(!ctrue){
								if(sou_table == "ccv_improvements_dwellings"){
									if(level_cod.toString() == '1')
										updatetotalarea.push({ext_rowuid: extension_rowuid, stable: sou_table, improve_id: improve_id_row, record : rec, attic_area: totareaA, attic_fin_area: parseInt(attf), bsmnt_area: totareaB, bsmnt_fin_area: parseInt(basefin), bgar_area: parseInt(bgar_area), crawl_area: crawl_area, bgar_capacity: bgar_capacity, basement_areas: basement_areas});
									else
										updatetotalarea.push({ext_rowuid: extension_rowuid, stable: sou_table, improve_id: improve_id_row, record : rec,  attic_area: totareaA , attic_fin_area: parseInt(attf), bsmnt_area: 0, bsmnt_fin_area: 0, bgar_area: 0, crawl_area: 0, bgar_capacity: 0, basement_areas: 0});
								}
								else if(sou_table == "ccv_improvements_comm_bldgs")
									updatetotalarea.push({ext_rowuid: extension_rowuid, stable: sou_table, improve_id: improve_id_row, record : rec, total_area: tt_ar, eff_perimeter: eff_peri });
							}
						}
					}
				});
			}
			else{
				var dcRec;
				var eextt_rec = parcel['ccv_extensions'].filter(function(sk){ return (sk.ROWUID == pRowu); })[0];
				if(eextt_rec)
					dcRec = (eextt_rec.RorC == 'R') ?  eextt_rec['ccv_improvements_dwellings'].filter(function(dd){return (!dd.CC_Deleted)})[0]: eextt_rec['ccv_improvements_comm_bldgs'].filter(function(dd){return (!dd.CC_Deleted)})[0];
				//var extERC = extension_Recd_Array.filter(function(ere){return ere.eRowuid == pRowu})[0];
				//dcRec = dcRec ? dcRec : (extERC ? extERC.CdRecord[0] : null);
				if(eextt_rec && dcRec){
					var dcRecTable = (eextt_rec.RorC == 'R') ?  'ccv_improvements_dwellings': 'ccv_improvements_comm_bldgs';
					if(dcRecTable == "ccv_improvements_dwellings")
						updatetotalarea.push({ext_rowuid: pRowu, stable: dcRecTable, record : dcRec,  attic_area: 0 , attic_fin_area: 0, bsmnt_area: 0, bsmnt_fin_area: 0, bgar_area: 0, crawl_area: 0, bgar_capacity:0, basement_code: 0, crawl_code: 0, basement_areas: 0});
				}
			}
			updatetotalarea.forEach(function(trec){
				if(trec.stable == "ccv_improvements_dwellings" ){
					var reccd = trec.record;
					fnames = res_fields;
					val.attic_area = trec.attic_area;
					val.attic_fin_area = trec.attic_fin_area;
					val.bsmnt_area = trec.bsmnt_area;
					val.bsmnt_fin_area = trec.bsmnt_fin_area;
					val.bgar_area = trec.bgar_area;
					val.crawl_area = trec.crawl_area;
					var bsmt_bgar_Area = trec.basement_areas + trec.bgar_area, bgar_capacity = 0, basement_code = 0, crawl_code = 0;
					if(trec.bgar_capacity && trec.bgar_capacity != 0)
						bgar_capacity = trec.bgar_capacity;
					var fl_rec_one = reccd[ConfigTables.res_floor].filter(function(flre){return flre.floor_key == "1.0" && flre.CC_Deleted != true})[0];
					var fl_rec_b = reccd[ConfigTables.res_floor].filter(function(flre){return flre.floor_key == "B" && flre.CC_Deleted != true})[0];
					deleteFloorRecordArray.forEach(function(delFlRec){
						if(fl_rec_one && fl_rec_one.ROWUID == delFlRec.delRowuid)
							fl_rec_one = null;
						else if(fl_rec_b && fl_rec_b.ROWUID == delFlRec.delRowuid)
							fl_rec_one = null;
					});
					
					if(fl_rec_one && fl_rec_one.base_area && fl_rec_one.base_area != 0 && fl_rec_one.base_area !='0'){
						if(fl_rec_b && fl_rec_b.base_area)
							basement_code = (parseFloat(fl_rec_b.base_area)) / (parseFloat(fl_rec_one.base_area));
						crawl_code = (trec.crawl_area) / (parseFloat(fl_rec_one.base_area));
						basement_code = ( (basement_code <.125)? 0: ( (basement_code >= .125 && basement_code < .375)? 1: ( (basement_code >= .375 && basement_code < .625)? 2: ( (basement_code >= .625 && basement_code < .875)? 3: 4 ) ) ) );
						crawl_code = ( (crawl_code <.125)? 0: ( (crawl_code >= .125 && crawl_code < .375)? 1: ( (crawl_code >= .375 && crawl_code < .625)? 2: ( (crawl_code >= .625 && crawl_code < .875)? 3: 4 ) ) ) );
					}
					if(bgar_capacity )
						bgar_capacity = (parseFloat(bgar_capacity)).toFixed(1);
					val.basement_code = basement_code;
					val.crawl_code = crawl_code;
					val.bgar_capacity = bgar_capacity;
					fnames.forEach(function(fname){ fields.push(getDataField(fname, 'ccv_improvements_dwellings'))});
					values.push({ tbl: 'ccv_improvements_dwellings', rowid: reccd.ROWUID, val: val, record: reccd, fields: _.clone(fields) });
				}
				else if(trec.stable == "ccv_improvements_comm_bldgs"){
					var reccd = trec.record;
					val.total_area = trec.total_area;
					fnames = (pdata.tbl.indexOf('comm') > -1)? comm_fields: res_fields;
					fnames.forEach(function(fname){ fields.push(getDataField(fname, pdata.tbl))});
					if(reccd && reccd[ConfigTables.comm_floors]){
						reccd[ConfigTables.comm_floors].forEach(function(flr){
							flr_rec_num = flr_rec_num + 1;
							flr_val = {flr_recorded: flr, flr_rec_num: flr_rec_num }
							floor_recordd.push(_.clone(flr_val));
							if (!isNaN(flr.floor_number) && val.number_floors < parseInt(flr.floor_number)) 
								val.number_floors = parseInt(flr.floor_number);
						});
					}
					values.push({ tbl: 'ccv_improvements_comm_bldgs', rowid: reccd.ROWUID, val: val, record: reccd, fields: _.clone(fields) });
				}
			});
			if(pdata && pdata.otherfields) {
				let uniqueArr = [], diffArr = [];
				otherValuesImpUpdates.forEach(function(otherValuesImp) {
					if (uniqueArr.filter(function(x) { return (x.rowuid == otherValuesImp.rowuid && x.fieldName == otherValuesImp.fieldName)})[0]) {
						if(diffArr.filter(function(x) { return (x.rowuid == otherValuesImp.rowuid && x.fieldName == otherValuesImp.fieldName)}).length == 0)
							diffArr.push(_.clone({rowuid: otherValuesImp.rowuid, fieldName: otherValuesImp.fieldName}));
					}
					else
						uniqueArr.push(_.clone({rowuid: otherValuesImp.rowuid, fieldName: otherValuesImp.fieldName}));
				});
			
				pdata.otherfields.forEach(function(other_field){
					if(other_field.table == 'ccv_improvements' &&  other_field["tbl_type_code"] == "Exterior Feature" && diffArr.filter(function(x) { return (x.rowuid == (other_field.record? other_field.record.ROWUID: null) && x.fieldName == other_field.fieldName) })[0]) {
						return;
					}
					val = {};
					var table_sourece = other_field.table;
					var record = other_field.record;
					var flKey = other_field.FloorKey;
					if(record){
						var rowId = record.ROWUID;
						if(table_sourece == 'ccv_res_floor' || table_sourece == 'ccv_dwellings_ext_features'){
							if(table_sourece == 'ccv_res_floor' && flKey && rowId){
								var fl_rexcord = parcel.res_floor.filter(function(fl){return ( (fl.floor_key == flKey) && (fl.ParentROWUID == rowId) && (fl.CC_Deleted != true) ) })[0];
								if(fl_rexcord){
									val[other_field.fieldName] = other_field.value;
									values.push({ tbl: other_field.table, rowid: fl_rexcord.ROWUID, val: val, record: fl_rexcord, fields: [getDataField(other_field.fieldName, other_field.table)] });
								}
							}
						}
						else if (rowId && other_field.table && record) {
							if( !(other_field.table == 'ccv_improvements_dwellings' && other_field.fieldName == "basement_code") ){
								val[other_field.fieldName] = other_field.value;
								values.push({ tbl: other_field.table, rowid: rowId, val: val, record: record, fields: [getDataField(other_field.fieldName, other_field.table)] });
							}
						}
					}
				});
			}
		});
		var length = 0, count = 0;
		TotalValueLength = values.length;
		var saveValueData = function() {	
			if (values.length == 0) processNoteData();
			else
			values.forEach(function(value) {	
				if(value.fields[0]){
					ValueLength = ValueLength + 1;
					value.fields = value.fields.filter(function( element ) { return (element !== undefined) && (element !== null); });
					length += value.fields.length;
					value.fields.forEach(function(field) {
						NumericScaleHandle(field,value.val[field.Name],function(NewValuess){
							value.val[field.Name] = NewValuess;
						});
						saveData += '^^^' + parcel.Id + '|' + value.record.ROWUID + '|' + field.Id + '||' + value.val[field.Name] + '|edit|' + value.record.ParentROWUID + '\n';
						count = count + 1;
						if (count == length) processNoteData();
					});
				}
				else{
					ValueLength = ValueLength + 1;
					if(TotalValueLength == ValueLength)
						processNoteData();
				}

			});
		}
		
		var createExteriorRecord = function ( newAuxRowuid ){
			if(exteriorFeatureCreationArray.length == 0){
				saveValueData();
				return;
			}
			var ext_Rec_Feat = exteriorFeatureCreationArray.pop();
			var tstable = ext_Rec_Feat.ExtsourceTable;
			insertNewAuxRecordSketch(parcel.Id, ext_Rec_Feat.ppRecord, getCategoryFromSourceTable(tstable).Id, newAuxRowuid, { ext_feat_code: ext_Rec_Feat.ext_feat_code, area: ext_Rec_Feat.area}, function (saveDataExtFeat) {
				if(saveDataExtFeat)
					saveData += saveDataExtFeat;
				var extFeatAuxRowuid = newAuxRowuid + 8;
				updateDefaultValues(parcel.Id, newAuxRowuid, ext_Rec_Feat.ppRecord.ROWUID, tstable);
				createExteriorRecord( extFeatAuxRowuid );
			});
			/*var ext_feat_Field = getDataField('ext_feat_code',tstable);
			var area_Field = getDataField('area',tstable);
			var CC_YearStatus = ( ( (clientSettings['FutureYearEnabled'] == '1') && getCategoryFromSourceTable(tstable).YearPartitionField && (getCategoryFromSourceTable(tstable).YearPartitionField != '0') )? ( (window.opener? window.opener.showFutureData : showFutureData) ? 'F' : 'A'  ) : "" );
			saveData += '^^^' + parcel.Id + '|' + newAuxRowuid + '|' + ext_Rec_Feat.ppRecord.ROWUID + '||' + tstable + '|new|' + CC_YearStatus + '\n';
			updateDefaultValues(parcel.Id, newAuxRowuid, ext_Rec_Feat.ppRecord.ROWUID, tstable);
			saveData += '^^^' + parcel.Id + '|' + newAuxRowuid + '|' + ext_feat_Field.Id + '||' + ext_Rec_Feat.ext_feat_code + '|edit|' + ext_Rec_Feat.ppRecord.ROWUID + '\n';
			saveData += '^^^' + parcel.Id + '|' + newAuxRowuid + '|' + area_Field.Id + '||' + ext_Rec_Feat.area + '|edit|' + ext_Rec_Feat.ppRecord.ROWUID + '\n';
			createExteriorRecord();*/
		}
		
		var delefloorRecFun = function() {
			if(deleteFloorRecordArray.length == 0){
				var extFeatAuxRowuid = ccTicks();
				createExteriorRecord( extFeatAuxRowuid );
				return;
			}
			var deleflrRec = deleteFloorRecordArray.pop();
			var cattId = getCategoryFromSourceTable(deleflrRec.deleTable).Id;
			saveData += '^^^' + parcel.Id + '|' + deleflrRec.delRowuid + '|||' + deleflrRec.deleTable + '|delete|\n';
			delefloorRecFun();
		}
		if(floor_recordd.length > 0){
			var flr_id_field = getDataField('floor_id', ConfigTables.comm_floors);
			var saveFlrData = function() {
				var f_record = floor_recordd.pop();
				var f_rec = f_record.flr_recorded;
				saveData += '^^^' + parcel.Id + '|' + f_rec.ROWUID + '|' + flr_id_field.Id + '||' + f_record.flr_rec_num + '|edit|' + f_rec.ParentROWUID + '\n';
				if(floor_recordd.length > 0)
					saveFlrData();
				else 
					delefloorRecFun();
			}
			saveFlrData();
		}
		else
			delefloorRecFun();
	}
	
	var process_floorRecs = function(uniq_flr_records) {
		if (uniq_flr_records.length == 0) { saveParentData(); return; }
		
		var res_flr_names = ['base_area', 'finish_living_area', 'floor_key', 'walkout_bsmt', 'ext_cover1'];
		var comm_flr_name = ['base_area', 'eff_perimeter', 'floor_number', 'par']
		var flr_key, old_rec, record_count = 0, flrFields = [];
		
		var update = function(rec, field_idx, field_count){
			var extt = false;
			NumericScaleHandle(flrFields[field_idx],rec[flrFields[field_idx].Name],function(NewValuess){
					rec[flrFields[field_idx].Name] = NewValuess;
			});
			if(rec.table == ConfigTables.res_floor)
    			rec.perimeter = rec.eff_perimeter ? rec.eff_perimeter:0;
			if(flrFields[field_idx].Name == 'floor_number'){ 	
				if(rec[flrFields[field_idx].Name] == undefined || rec[flrFields[field_idx].Name] == null)
					rec[flrFields[field_idx].Name] = '' ;
			}
			if((rec.table == ConfigTables.res_floor && flrFields[field_idx].Name == 'ext_cover1') || (rec.table == ConfigTables.res_floor && flrFields[field_idx].Name == 'extension')){
				if(rec[flrFields[field_idx].Name] == undefined || rec[flrFields[field_idx].Name] == null)
					extt = true;
				else if (rec.record && flrFields[field_idx].Name == 'ext_cover1'){
					var dVal = getDataField('ext_cover1', ConfigTables.res_floor).DefaultValue;
					if(rec.ext_cover1 == dVal && rec.record.CC_RecordStatus != "I")
						extt = true;	
				}					
			}
			if(rec.table == ConfigTables.res_floor && flrFields[field_idx].Name == 'walkout_bsmt'){
				if(rec[flrFields[field_idx].Name] == undefined || rec[flrFields[field_idx].Name] == null || rec[flrFields[field_idx].Name] == '' || rec[flrFields[field_idx].Name] == " ")
					extt = true;	
			}
			if(rec.table == ConfigTables.res_floor && flrFields[field_idx].Name == 'finish_living_area'){
				if(rec[flrFields[field_idx].Name] == undefined || rec[flrFields[field_idx].Name] == null || rec[flrFields[field_idx].Name] === '')
					extt = true;
				if(!extt && rec['floor_key'] == 'B' && (!rec.floorBFinExists) )
					extt = true;
			}
			if(extt){
				if ((field_idx + 1) < field_count)
					update(rec, ++field_idx, field_count); 
				else process_records();
			}
			else{
				saveData += '^^^' + parcel.Id + '|' + rec.floorRowuid + '|' + flrFields[field_idx].Id + '||' + rec[flrFields[field_idx].Name] + '|edit|' + rec.segmentData.ROWUID  + '\n';
				var pflRecrord = parcel[rec.table].filter(function(r){ return r.ROWUID == rec.floorRowuid })[0];
				if(pflRecrord)
					pflRecrord[flrFields[field_idx].Name] = rec[flrFields[field_idx].Name];
				if ((field_idx + 1) < field_count)
					update(rec, ++field_idx, field_count); 
				else process_records();
			}
		}
		
		var process_records = function() {
			if (uniq_flr_records.length == 0) { saveParentData(); return; }
			var flr_rec = uniq_flr_records.pop();
			flrFields = [];
			if( flr_rec.isSketchModified ) {
				var flr_names = ((flr_rec.table.indexOf('res') > -1)? res_flr_names: comm_flr_name)
				var fin_qual=getDataField('floor_fin_qual',ConfigTables.res_floor)
				flr_names.forEach(function(fname){ flrFields.push(getDataField(fname, flr_rec.table)); });
				if (flr_rec.new_rec){
					var newFloorRowuid = ccTicks();
					insertNewAuxRecordSketch(null, flr_rec.segmentData, getCategoryFromSourceTable(flr_rec.table).Id, newFloorRowuid, null, function (saveDataFloor) {
						if(saveDataFloor)
							saveData += saveDataFloor;
						flr_rec.record = parcel[flr_rec.table].filter(function(r){ return r.ROWUID == newFloorRowuid })[0];
						flr_rec.floorRowuid = newFloorRowuid;
						update(flr_rec, 0, flr_names.length);
						if(flr_rec.floor_fin_qual>0)
							saveData += '^^^' + parcel.Id + '|' + flr_rec.floorRowuid + '|' + fin_qual.Id + '||' + flr_rec.floor_fin_qual + '|edit|' + flr_rec.segmentData.ROWUID + '\n';
					});
					
					/*
					var CC_YearStatus = ( ( (clientSettings['FutureYearEnabled'] == '1') && getCategoryFromSourceTable(flr_rec.table).YearPartitionField && (getCategoryFromSourceTable(flr_rec.table).YearPartitionField != '0') )? ( (window.opener? window.opener.showFutureData : showFutureData) ? 'F' : 'A'  ) : "" );
					saveData += '^^^' + parcel.Id + '|' + newFloorRowuid + '|' + flr_rec.segmentData.ROWUID + '||' + flr_rec.table + '|new|' + CC_YearStatus + '\n';
					updateDefaultValues(parcel.Id, newFloorRowuid, flr_rec.segmentData.ROWUID, flr_rec.table);
					flr_rec.floorRowuid=newFloorRowuid;
					update(flr_rec, 0, flr_names.length);
					if(flr_rec.floor_fin_qual>0)
						saveData += '^^^' + parcel.Id + '|' + flr_rec.floorRowuid + '|' + fin_qual.Id + '||' + flr_rec.floor_fin_qual + '|edit|' + flr_rec.segmentData.ROWUID + '\n';
					*/
				}
				else {
					flr_rec.floorRowuid = flr_rec.record.ROWUID;
					update(flr_rec, 0, flr_names.length);
					if(flr_rec.floor_fin_qual>0)
						saveData += '^^^' + parcel.Id + '|' + flr_rec.floorRowuid + '|' + fin_qual.Id + '||' + flr_rec.floor_fin_qual + '|edit|' + flr_rec.segmentData.ROWUID + '\n';
				}
			}
			else
				process_records();
		}
		if(deletedSketchFloorRecords.length > 0) {
			uniq_flr_records.forEach(function(u_f_rec){
				if(!u_f_rec['isSketchModified']) {
					var floor_key_col = (u_f_rec.table.indexOf('res') > -1)? 'floor_key': 'floor_number';
					var fl_key = u_f_rec[floor_key_col];
					var del_sk_rec = deletedSketchFloorRecords.filter(function(del_sk_fl_rec){return del_sk_fl_rec.floorKey == fl_key})[0];
					if(del_sk_rec) {
						u_f_rec['isSketchModified'] = true;
						if(!u_f_rec.floorBFinExists)
							u_f_rec.floorBFinExists = del_sk_rec.floorBFinExists;
					}
				}
			});
		}
		process_records();
	}
	
	var processor = function() {
		if (savedata.length == 0) {
			var uniq_flr_records = [], uniq_rec = [], floor_key_col;
			floor_records.forEach(function(floor){
				floor_key_col = (floor.tblName.indexOf('res') > -1)? 'floor_key': 'floor_number';
				floor.record.forEach(function(dt, i){
					if (!isNaN(dt.floorKey) && !dt.isTwoSFloor) {
						var startVal = (dt.lbl_lvl_code == "3") ? 2: 1;
						for (i = 1; i < parseFloat(dt.floorKey); i++) {
							let cdup = true;
							if (dt.lbl_lvl_code == "3" || dt.lbl_lvl_code == "2") {
								let lvcode = dt.lbl_lvl_code == "3" ? '2' : '3';
								let flRecExistsInAt = floor.record.filter((x) => { return x.floorKey == (i).toFixed(1) })[0];
								if (flRecExistsInAt) cdup = false;
							}
							if (cdup) {
								var duplicate_dt = _.clone(dt);
								duplicate_dt.floorKey = (((i).toFixed(1)).toString());
								duplicate_dt.finish_living_areas = duplicate_dt.area;
								floor.record.push(duplicate_dt);
							}
						}
					}
				});
			});
			
			var any_floor_record_modified = false;
			floor_records.forEach(function(floor){
				floor.record.forEach(function(dt, i){
					if(dt.floor_recorddd[0]) 
						finalFloorRecordsArray.push(_.clone({flr_table: floor.tblName , frecRec : dt}));
					if(dt.isSketchModified && !any_floor_record_modified)
						any_floor_record_modified = true;
				});
			});
			
			if(!any_floor_record_modified && deletedSketchFloorRecords.length > 0)
				any_floor_record_modified = true;
			
			if(any_floor_record_modified) {
				floor_records.forEach(function(floor){
					floor.record.forEach(function(dt, i){
						if(dt.floor_recorddd[0]) {
							//finalFloorRecordsArray.push(_.clone({flr_table: floor.tblName , frecRec :dt}));
							var impDwellCommRec;
							if(floor.tblName == ConfigTables.res_floor)
								impDwellCommRec = parcel['ccv_improvements_dwellings'].filter(function(flr_dt){ return flr_dt.ROWUID == dt.floor_recorddd[0].ROWUID })[0];
							else
								impDwellCommRec = parcel['ccv_improvements_comm_bldgs'].filter(function(flr_dt){ return flr_dt.ROWUID == dt.floor_recorddd[0].ROWUID })[0];
							//var extERC = extension_Recd_Array.filter(function(ere){return ere.eRowuid == dt.extensionRowuid})[0];
							//impDwellCommRec = impDwellCommRec ? impDwellCommRec: (extERC ? extERC.CdRecord.filter(function(flr_dt){ return flr_dt.ROWUID == dt.floor_recorddd[0].ROWUID })[0] : []);
							var rec = { extension: dt.extension, base_area: parseFloat(dt.area), eff_perimeter: parseFloat(dt.perimeter), par: parseFloat(dt.PAR), finish_living_area: parseFloat(dt.finish_living_areas), walkout_bsmt: dt.walkout_bsmt, table: floor.tblName, segmentData: impDwellCommRec, floor_number: dt.floor_no, perimeter:0, ext_cover1:dt.ext_cover1, isSketchModified: dt.isSketchModified, floorBFinExists: dt.floorBFinExists }
							rec[floor_key_col] = dt.floorKey;
							var fl_key1 = dt.floorKey;
							var fl_key2 = dt.floorKey + '.0';
							uniq_rec = uniq_flr_records.filter(function (uniq) { return ((uniq[floor_key_col] == fl_key1 || uniq[floor_key_col] == fl_key2) && uniq.table == floor.tblName && uniq.extension == rec.extension) })
							dt.otherValues.forEach(function(otherVal){
								if (otherVal.filter && uniq_rec[0]) {
									if (otherVal.filter.value == uniq_rec[0][floor_key_col]) 
										rec[otherVal.fieldName] = otherVal.value;
								}
								else rec[otherVal.fieldName] = otherVal.value;
							});
							if (uniq_rec.length > 0) {
								uniq_rec[0].base_area += Math.round(dt.area);
								uniq_rec[0].eff_perimeter += Math.round(dt.perimeter);
								uniq_rec[0].par += parseFloat(dt.PAR);
								uniq_rec[0].finish_living_area += Math.round(dt.finish_living_areas);
								if (dt.walkout_bsmt != '')
									uniq_rec[0].walkout_bsmt = dt.walkout_bsmt;
								if(dt.ext_cover1)
									uniq_rec[0].ext_cover1 = dt.ext_cover1;
								if( dt.isSketchModified )
									uniq_rec[0].isSketchModified = true;
								if( dt.floorBFinExists )
									uniq_rec[0].floorBFinExists = true;
							}
							else {						
								if(dt.floor_recorddd[0] && dt.floor_recorddd[0].ROWUID)
									old_rec = parcel[floor.tblName].filter(function(flr_dt){ return ((flr_dt.ParentROWUID == impDwellCommRec.ROWUID && ( flr_dt[floor_key_col] && (((flr_dt[floor_key_col]).toString() == fl_key1) || ((flr_dt[floor_key_col]).toString() == fl_key2 )))) && flr_dt.CC_Deleted != true) })
								else
									old_rec = parcel[floor.tblName].filter(function(flr_dt){ return ((flr_dt.extension == dt.extension && ( flr_dt[floor_key_col] && (((flr_dt[floor_key_col]).toString() == fl_key1) || ((flr_dt[floor_key_col]).toString() == fl_key2 )))) && flr_dt.CC_Deleted != true) })
								if (old_rec.length > 0){
									rec.new_rec = false;
									rec.record = old_rec[0];
								}
								else rec.new_rec = true;
								uniq_flr_records.push(rec);
							}
						}
					});
				});
			}
			process_floorRecs(uniq_flr_records); 
			return;			
		}
		
		var data = savedata.pop();
		if (!data.isSegmentIdOnly && data.segment_tbl != ConfigTables.SktOutbuilding) {
			savedataLength = savedataLength + 1; 
			var _newRowuidVectLabel = ccTicks(); 
			var vectFnames = ['CC_segment_id', 'curve_height', 'run_rise', 'seq_number', 'line_type', 'vector', 'dimension_font_size', 'dimension_position', 'improvement_id', 'floor_level', 'number_floors'];
			var lblFnames = [ 'CC_segment_id', 'lbl_level_code', 'lbl_frac1', 'lbl_const_code', 'lbl_ext_feat_code', 'finish_area', 'unfin_area', 'default_ext_cover' , 'lbl_n', 'improvement_id', 'lbl_frac2', 'lbl_frac3', 'lbl_frac4','area2','area3'];
			var seg_fnames = [ 'CC_segment_id', 'vector_start', 'area', 'perimeter', 'label_position', 'displayed_area_position', 'improvement_id'];
			var imp_fnames = ['improvement_id', 'imp_type', 'imp_width', 'imp_length', 'mh_length', 'mh_width'];

			var vectFields = [], lblFields = [], segFields = [], imp_dwell_field = [];
			var par_data = parentData.filter(function(pd){ return pd.tbl == data.parentTbl && pd.rowid == data.parentRowid })
			if (par_data.length == 0)
				parentData.push({ tbl: data.parentTbl, rowid: data.parentRowid, seg_tbl: data.segment_tbl, lbl_tbl: data.lbl_table, flr_tbl: data.floor_table, ext_feature_tbl: data.extFeatTbl, otherfields: [], dataValuess:[] })
			par_data = parentData.filter(function(pd){ return pd.tbl == data.parentTbl && pd.rowid == data.parentRowid })[0];
			par_data.otherfields = _.union(par_data.otherfields, data.otherValues);
			var paIndex = parentData.indexOf(parentData.filter(function(pd){ return pd.tbl == data.parentTbl && pd.rowid == data.parentRowid })[0])
			var DataValuess = _.clone(data);
			if(paIndex > -1)
				parentData[paIndex].dataValuess.push(_.clone(DataValuess));
			seg_fnames.forEach(function(fname){ segFields.push(getDataField(fname, data.segment_tbl)); });
			vectFnames.forEach(function(fname){ vectFields.push(getDataField(fname, data.vect_tbl)); });
			lblFnames.forEach(function(fname){ lblFields.push(getDataField(fname, data.lbl_table)); });
			imp_fnames.forEach(function(fname){ imp_dwell_field.push(getDataField(fname, 'ccv_improvements_dwellings')); });

			var segment_data = parcel[data.segment_tbl].filter(function(d){ return d.ROWUID == data.rowId })[0];
			
			var update = function(ins_data, dt, tbl_name, fields, rowid, count, parentRowuidd, ins_callback, extraValue) {
				if (count == fields.length) { 
					if (tbl_name == ConfigTables.SktSegment) ins_callback();
					else insert(ins_data, tbl_name, fields, parentRowuidd, ins_callback, extraValue); 
					return; 
				}
				var f = fields[count++], valid = true;
				var value = dt[f.Name];
				if(value == undefined && f.Name == 'lbl_n')
					value = f.DefaultValue;
				//if (f.Name == 'default_ext_cover' && tbl_name == ConfigTables.SktSegmentLabel && !value)
                //    value = '0';
				NumericScaleHandle(f,value,function(NewValuess){
					value = NewValuess;
				});
				if (!valid) { update(ins_data, dt, tbl_name, fields, rowid, count, ins_callback, extraValue); return; }
				var record = parcel[tbl_name].filter(function(ac){ return ac.ROWUID == rowid })[0];
				if(record){
					saveData += '^^^' + parcel.Id + '|' + record.ROWUID + '|' + f.Id + '||' + value + '|edit|' + record.ParentROWUID + '\n';
					isUpdated=false;
				}
				else{
					var parentrowidd;
					if (tbl_name.split(/_/g)[0] == ConfigTables.SktSegment)
						parentrowidd = dt.parentRowid;
					else
					 parentrowidd = parentRowuidd;
					 saveData += '^^^' + parcel.Id + '|' + rowid + '|' + f.Id + '||' + value + '|edit|' + parentrowidd + '\n';
					 isUpdated=false;
				}
				if (isUpdated && tbl_name.indexOf('label') > -1 && f.Name == 'lbl_ext_feat_code') {
					var feat_table = tbl_name.indexOf('label');
					var rowId =parcel[extraValue]?parcel[extraValue].filter(function(ext_ft){ return ext_ft.improvement_id == dt.improvement_id })[0]:null;
					rowId = rowId? rowId.ROWUID: null;
						if (rowId) {
							var pData = parentData.filter(function(prec){ return prec.tbl == extraValue && prec.rowid == rowId })
							if (pData.length > 0)
								pData[0].area += parseFloat(dt.area);
							else 
								parentData.push({ tbl: extraValue, rowid: rowId, area: parseFloat(dt.area) });
						}
					//}
				}
				update(ins_data, dt, tbl_name, fields, rowid, count, parentRowuidd, ins_callback, extraValue); 
			}
			var insert = function(ins_data, tbl_name, fields, parentRowuidd, ins_callback, extraValue) {
				if (ins_data.length == 0) { ins_callback(); return; }
				var dt = ins_data.pop(), count = 0;
				if(segment_data)
					var existing_rec = segment_data[tbl_name].filter(function(r){ return r.CC_segment_id == dt.CC_segment_id && r.lbl_level_code == dt.lbl_level_code });
				
				if (tbl_name.indexOf('ccv_sktsegmentlabelDwe') > -1 && existing_rec.length > 0)
					update(ins_data, dt, tbl_name, fields, existing_rec[0].ROWUID, 0, existing_rec[0].ParentROWUID, ins_callback, extraValue);
				else{
					_newRowuidVectLabel = _newRowuidVectLabel + 108;
					insertNewAuxRecordSketch(parcel.Id, segment_data, getCategoryFromSourceTable(tbl_name).Id, _newRowuidVectLabel, null, function (sktVectLabelData, rowid) {
						if(sktVectLabelData)
							saveData +=	sktVectLabelData;
						updateDefaultValues(parcel.Id, rowid, parentRowuidd, tbl_name);		
						update(ins_data, dt, tbl_name, fields, rowid, 0, parentRowuidd, ins_callback, extraValue);
					});
					/*
					setTimeout(function() {
						var newAuxRowuidd = ccTicks();
						var CC_YearStatus = ( ( (clientSettings['FutureYearEnabled'] == '1') && getCategoryFromSourceTable(tbl_name).YearPartitionField && (getCategoryFromSourceTable(tbl_name).YearPartitionField != '0') )? ( (window.opener? window.opener.showFutureData : showFutureData) ? 'F' : 'A'  ) : "" );
						saveData += '^^^' + parcel.Id + '|' + newAuxRowuidd + '|' + parentRowuidd + '||' + tbl_name + '|new|' + CC_YearStatus + '\n';
										
						update(ins_data, dt, tbl_name, fields, newAuxRowuidd, 0,parentRowuidd, ins_callback, extraValue);
					}, 100);*/
				}
			}
			var updated = function(tbl_name, fields,dt, rowid, count,recordd, parentrowidd, Mcallback) {
				if (count == fields.length) {
					if(Mcallback) Mcallback(); 
					return
				}
				var f = fields[count++], valid = true;
				var value = dt[f.Name];
				saveData += '^^^' + parcel.Id + '|' + rowid + '|' + f.Id + '||' + value + '|edit|' + parentrowidd + '\n';
				updated(tbl_name, fields, dt,rowid, count,recordd,parentrowidd ,Mcallback);
			}
			update(null, data, data.segment_tbl, segFields, data.rowId, 0, null, function() {
				insert(data.vects, data.vect_tbl, vectFields, data.rowId, function(){
					insert(_.clone(data.labels), data.lbl_table, lblFields, data.rowId,function(){
						if (data.floor_record.length > 0)
							floor_records.push({ record: _.clone(data.floor_record), tblName: data.floor_table, segmentRowuid: data.rowId, segmentParentRowuid: data.parentRowid  });
							if(data.MHOMEValuesTrue){
								var mRecord = [];
								var iMDrecords = data.MHOMEValues[0].iMDrecord;
								var mTablee= data.MHOMEValues[0].mTable;
								updated(mTablee, imp_dwell_field, data.MHOMEValues[0],iMDrecords.ROWUID, 0, iMDrecords, iMDrecords.ParentROWUID ,function() {
									mRecord = parcel[ConfigTables.manuf_housing] && parcel[ConfigTables.manuf_housing].filter(function(ac){ return ac.ParentROWUID == iMDrecords.ParentROWUID });
									var PRecord = parcel['ccv_extensions'] && parcel['ccv_extensions'].filter(function(pc){ return pc.ROWUID == iMDrecords.ParentROWUID })[0];
									if(mRecord && mRecord.length < 0){
										var newAuxRowuidd = ccTicks();
										insertNewAuxRecordSketch(parcel.Id, PRecord, getCategoryFromSourceTable(ConfigTables.manuf_housing).Id, newAuxRowuidd, null, function (saveDataMH) {
											if(saveDataMH)
												saveData +=	saveDataMH;
											updateDefaultValues(parcel.Id, newAuxRowuidd, PRecord.parentRowid, ConfigTables.manuf_housing, true);
											processor();					
										});
										/*
										
										var CC_YearStatus = ( ( (clientSettings['FutureYearEnabled'] == '1') && getCategoryFromSourceTable(ConfigTables.manuf_housing).YearPartitionField && (getCategoryFromSourceTable(ConfigTables.manuf_housing).YearPartitionField != '0') )? ( (window.opener? window.opener.showFutureData : showFutureData) ? 'F' : 'A'  ) : "" );
										saveData += '^^^' + parcel.Id + '|' + newAuxRowuidd + '|' + PRecord.parentRowid + '||' + ConfigTables.manuf_housing + '|new|' + CC_YearStatus + '\n'; 
										updateDefaultValues(parcel.Id, newAuxRowuidd, PRecord.parentRowid, ConfigTables.manuf_housing, true);	
										processor();*/
									}
									else
										processor();
								});	
							}
							else
								processor();
						//process_floorRecs(data.floor_record, data.floor_table, flrFields, segment_data, processor);
					}, data.extFeatTbl);
				});
			});
		}
		else if (!data.isSegmentIdOnly && data.segment_tbl == ConfigTables.SktOutbuilding) {
			savedataLength = savedataLength + 1;
			var _newOutBuildingRowuid = ccTicks(); 
			var Out_vectFnames = ['curve_height', 'run_rise', 'seq_number', 'line_type', 'vector', 'dimension_font_size', 'dimension_position', 'floor_level', 'number_floors'];
			var Out_seg_fnames = ['label_code', 'vector_start', 'area', 'perimeter', 'label_position', 'displayed_area_position', 'improvement_id'];
			var imp_fnames = ['imp_width', 'imp_length', 'imp_size', 'imp_type', 'misc_imp_type'];
			var Out_vectFields = [], Out_segFields = [], imp_field = [];
			
			Out_seg_fnames.forEach(function(fname){ Out_segFields.push(getDataField(fname, data.segment_tbl)); });
			Out_vectFnames.forEach(function(fname){ Out_vectFields.push(getDataField(fname, data.vect_tbl)); });			
			imp_fnames.forEach(function(fname){ imp_field.push(getDataField(fname, 'ccv_improvements')); });
			var segment_data = parcel[data.segment_tbl].filter(function(d){ return d.ROWUID == data.rowId && d.CC_Deleted != true})[0];
			var imp_id = data.improvement_id;
			if(imp_id && imp_id.split('{').length > 1)
				imp_id = imp_id.split('{')[1].split('}')[0];
			var imp_IdOB = (imp_id && !(isNaN(imp_id)))? parseInt(imp_id): imp_id;
			var parentROEW=parcel?parcel['ccv_improvements'].filter(function(s){return ((s.ROWUID == imp_IdOB) || (s.improvement_id == imp_IdOB)) && (s.ParentROWUID == segment_data.ParentROWUID)&& s.CC_Deleted != true})[0]:[];
			var update_Out = function(ins_data, dt, tbl_name, fields, rowid, count, parentRowuidd, ins_callback, extraValue) {
				if (count == fields.length) { 
					if (tbl_name == ConfigTables.SktOutbuilding) ins_callback();
					else insert_Out(ins_data, tbl_name, fields, parentRowuidd, ins_callback, extraValue); 
					return; 
				}
				var f = fields[count++], valid = true;
				var value = dt[f.Name];
				if(value == undefined && f.Name == 'lbl_n')
					value = f.DefaultValue;
				NumericScaleHandle(f,value,function(NewValuess){
					value = NewValuess;
				});
				if (!valid) { update_Out(ins_data, dt, tbl_name, fields, rowid, count, ins_callback, extraValue); return; }
				var record = parcel[tbl_name].filter(function(ac){ return ac.ROWUID == rowid })[0];
				if(record){
					saveData += '^^^' + parcel.Id + '|' + record.ROWUID + '|' + f.Id + '||' + value + '|edit|' + record. ParentROWUID + '\n';
					isUpdated=false;
				}
				else{
					var parentrowidd;
					if (tbl_name == ConfigTables.SktOutbuilding)
						parentrowidd=dt.parentRowid;
					else
					 parentrowidd=parentRowuidd;
					 saveData += '^^^' + parcel.Id + '|' + rowid + '|' + f.Id + '||' + value + '|edit|' + parentrowidd + '\n';
					 isUpdated=false;
				}				
				update_Out(ins_data, dt, tbl_name, fields, rowid, count, parentRowuidd, ins_callback, extraValue); 
			}			
			var insert_Out = function(ins_data, tbl_name, fields, parentRowuidd, ins_callback, extraValue) {
				if (ins_data.length == 0) { ins_callback(); return; }
				var dt = ins_data.pop(), count = 0;
				if(segment_data)
					var existing_rec = segment_data[tbl_name].filter(function(r){ return r.CC_segment_id == dt.CC_segment_id && r.lbl_level_code == dt.lbl_level_code });
				_newOutBuildingRowuid = _newOutBuildingRowuid + 108;
				insertNewAuxRecordSketch(parcel.Id, segment_data, getCategoryFromSourceTable(tbl_name).Id, _newOutBuildingRowuid, null, function (sktOUTBVectLabelData, rowid) {
						if(sktOUTBVectLabelData)
							saveData +=	sktOUTBVectLabelData;
						updateDefaultValues(parcel.Id, rowid, parentRowuidd, tbl_name);		
						update_Out(ins_data, dt, tbl_name, fields, rowid, 0, parentRowuidd, ins_callback, extraValue);
					});				
				/*setTimeout(function() {
					var newAuxRowuidd = ccTicks();
					var CC_YearStatus = ( ( (clientSettings['FutureYearEnabled'] == '1') && getCategoryFromSourceTable(tbl_name).YearPartitionField && (getCategoryFromSourceTable(tbl_name).YearPartitionField != '0') )? ( (window.opener? window.opener.showFutureData : showFutureData) ? 'F' : 'A'  ) : "" );
					saveData += '^^^' + parcel.Id + '|' + newAuxRowuidd + '|' + parentRowuidd + '||' + tbl_name + '|new|' + CC_YearStatus + '\n';
					updateDefaultValues(parcel.Id, newAuxRowuidd, parentRowuidd, tbl_name);						
					update_Out(ins_data, dt, tbl_name, fields, newAuxRowuidd, 0,parentRowuidd, ins_callback, extraValue);
				}, 100);*/				
			}			
			var updated_Out = function(tbl_name, fields,dt, rowid, count,recordd, parentrowidd, IMP_callback) {
				if (count == fields.length) {
					if(IMP_callback) IMP_callback(); 
					return
				}
				var f = fields[count++], valid = true;
				var value = dt[f.Name];
				if(value != undefined)
					saveData += '^^^' + parcel.Id + '|' + rowid + '|' + f.Id + '||' + value + '|edit|' + parentrowidd + '\n';
				updated_Out(tbl_name, fields, dt,rowid, count,recordd,parentrowidd ,IMP_callback);
			} 
			update_Out(null, data, data.segment_tbl, Out_segFields, data.rowId, 0, null, function() {
				insert_Out(data.vects, data.vect_tbl, Out_vectFields, data.rowId, function(){
					if(data.OBimp_ValuesTrue){
						var iMP_records = data.OBimp_Values[0].iMP_Record;
						updated_Out('ccv_improvements', imp_field, data.OBimp_Values[0],iMP_records.ROWUID, 0, iMP_records, iMP_records.ParentROWUID, function() {							
							if(parentROEW && data.sketchedOB)
								parentROEW['imp_size'] = data.OBimp_Values[0]['imp_size'].toString(), parentROEW['imp_width'] = data.OBimp_Values[0]['imp_width'].toString(), parentROEW['imp_length'] = data.OBimp_Values[0]['imp_length'].toString();
							processor();
						});	
					}
					else
					processor();						
				});
			});
		}
		else {			
			savedataLength = savedataLength + 1;	
			if(data.seg_tbl != ConfigTables.SktOutbuilding){						
				var seg_data = data.segmentRec;
				var seg_segId_field = getDataField('CC_segment_id', data.seg_tbl);
				var vect_segId_field = getDataField('CC_segment_id', data.vect_tbl);
				var lbl_segId_field = getDataField('CC_segment_id', data.lbl_tbl);
				if (seg_data) {
					if (data.floor_Records && data.floor_Records.length > 0)
						floor_records.push({ record: _.clone(data.floor_Records), tblName: data.floor_Table, segmentRowuid: seg_data.ROWUID, segmentParentRowuid: seg_data.ParentROWUID});
					saveData += '^^^' + parcel.Id + '|' + seg_data.ROWUID + '|' + seg_segId_field.Id + '||' +  data.CC_segment_id + '|edit|' + seg_data.ParentROWUID + '\n';
					var rec_count = seg_data[data.vect_tbl].length;
					var lbl_count = seg_data[data.lbl_tbl].length;
					if (rec_count == 0 && lbl_count == 0) processor();
					else {
						seg_data[data.vect_tbl].forEach(function(vect_data){
							saveData += '^^^' + parcel.Id + '|' + vect_data.ROWUID + '|' + vect_segId_field.Id + '||' +  data.CC_segment_id + '|edit|' + vect_data.ParentROWUID + '\n';
						});
						seg_data[data.lbl_tbl].forEach(function(lbl_data){
							saveData += '^^^' + parcel.Id + '|' + lbl_data.ROWUID + '|' + lbl_segId_field.Id + '||' +  data.CC_segment_id + '|edit|' + lbl_data.ParentROWUID + '\n';
						});
						processor();
					}
				}
				else processor();
			}
			else{
				var seg_data = parcel[data.seg_tbl].filter(function(sg_dt){ return sg_dt.ROWUID == data.rowId })[0];
				var seg_labelcode_field = getDataField('label_code', data.seg_tbl);
				var vect_impId_field = getDataField('improvement_id', data.vect_tbl);
				var ob_imptype = OutB_Lookups["ob_codes_imp_type"][data.label_code]? OutB_Lookups["ob_codes_imp_type"][data.label_code].AdditionalValue1: '';
				var ImpIdMethoddd = (data.label_code == 'O054' || data.label_code == 'O603')? 'MISC' : ob_imptype;
				var ImpIdMethod_Fieldd = getDataField('imp_type','ccv_improvements');
				var ImpIdMethod_Misc = getDataField('misc_imp_type','ccv_improvements');
				var imp_id = seg_data.improvement_id;
				if(imp_id && imp_id.split('{').length > 1)
					imp_id = imp_id.split('{')[1].split('}')[0];
				var imp_IdOB = (imp_id && !(isNaN(imp_id)))? parseInt(imp_id): imp_id;
				var parentROEW=parcel?parcel['ccv_improvements'].filter(function(s){return ((s.ROWUID == imp_IdOB) || (s.improvement_id == imp_IdOB)) && (s.ParentROWUID == seg_data.ParentROWUID) && s.CC_Deleted != true})[0]:[];
				var impFieldArrays = [];
				var impFieldss = ['imp_width', 'imp_length', 'imp_size'];
				impFieldss.forEach(function(fname){ impFieldArrays.push(getDataField(fname, 'ccv_improvements')); });
				var impTpye_Update = function (){
					if(parentROEW){
						if(data.OBimp_ValuesTrue){
							var iMP_records = data.OBimp_Values[0].iMP_Record;
							var dt = data.OBimp_Values[0];
							for(var i = 0; i < 3; i++){
								if(impFieldArrays[i])
									saveData += '^^^' + parcel.Id + '|' + iMP_records.ROWUID + '|' + impFieldArrays[i].Id + '||' + dt.impFieldss[i] + '|edit|' + iMP_records.parentRecord.ROWUID + '\n';
							}	
						}					
						if(data.label_code == 'O054' || data.label_code == 'O603'){
        					saveData += '^^^' + parcel.Id + '|' + parentROEW.ROWUID + '|' + ImpIdMethod_Fieldd.Id + '||' + ImpIdMethoddd + '|edit|' + parentROEW.parentRecord.ROWUID + '\n';
							saveData += '^^^' + parcel.Id + '|' + parentROEW.ROWUID + '|' + ImpIdMethod_Misc.Id + '||' + data.miscValue + '|edit|' + parentROEW.parentRecord.ROWUID + '\n';
							processor();
        				}
        				else{
        					saveData += '^^^' + parcel.Id + '|' + parentROEW.ROWUID + '|' + ImpIdMethod_Fieldd.Id + '||' + ImpIdMethoddd + '|edit|' + parentROEW.parentRecord.ROWUID + '\n';
							processor();
        				}
        			}
        			else processor();
				}				
				if (seg_data) {
					saveData += '^^^' + parcel.Id + '|' + seg_data.ROWUID + '|' + seg_labelcode_field.Id + '||' +  data.label_code + '|edit|' + seg_data.ParentROWUID + '\n';
					var rec_count = seg_data[data.vect_tbl].length, count = 0;
					if (rec_count == 0) impTpye_Update();
					else 
						seg_data[data.vect_tbl].forEach(function(vect_data){
							saveData += '^^^' + parcel.Id + '|' + vect_data.ROWUID + '|' + vect_impId_field.Id + '||' +  seg_data.improvement_id + '|edit|' + vect_data.ParentROWUID + '\n';
								if (rec_count == ++count) impTpye_Update();							
						})				
				}
				else processor();
			}
		}
	}
	
	var calculateCC_sizemethod = function() {
	    var copy_save_data = _.clone(savedata);
	    var CC_sizemethod_field = getDataField('CC_sizemethod', 'ccv_improvements');
	    if ( CC_sizemethod_field ) {
	        copy_save_data.forEach(function (copy_data) {
	            if (!copy_data.isSegmentIdOnly) {
	                var CC_sizemethod_value = ( copy_data.vects && copy_data.vects.length > 0 ) ? 'M': 'C';
	                var extn_rowuid = copy_data.parentRowid, copy_improvement_id = copy_data.improvement_id, is_improvement_mapped = false;;
	                if ( extn_rowuid && copy_improvement_id) {
						if ( copy_improvement_id.split('{').length > 1 ) {
							is_improvement_mapped = copy_improvement_id.split('{')[0].split('&%')[1] == 'ccv_improvements' ? true: false;
							copy_improvement_id = copy_improvement_id.split('{')[1].split('}')[0];
	                    }
	                    else 
	                        is_improvement_mapped =  ( copy_improvement_id != 'D' && copy_improvement_id != 'M' && copy_improvement_id != 'A' && copy_improvement_id != 'C' ) ? true: false;      
						var copy_improvement_idOB = ( copy_improvement_id && !( isNaN(copy_improvement_id) ) )? parseInt(copy_improvement_id): copy_improvement_id;
	                    if ( copy_data.segment_tbl == ConfigTables.SktOutbuilding || is_improvement_mapped ) {
	                        var copy_improvement_Rec = parcel.ccv_improvements.filter(function(imp){ return ( ( imp.improvement_id == copy_improvement_idOB ) || ( imp.ROWUID == copy_improvement_idOB ) ) && ( imp.ParentROWUID == extn_rowuid ) })[0];
	                        if (copy_improvement_Rec)
                                saveData += '^^^' + parcel.Id + '|' + copy_improvement_Rec.ROWUID + '|' + CC_sizemethod_field.Id + '||' +  CC_sizemethod_value + '|edit|' + copy_improvement_Rec.ParentROWUID + '\n';
	                    }
	        			
	                }  
	            } 
	        });
	    }
	    processor(); 
	}
	
	var createNew = function(segtNewROWUID ) {
		if (newRecords.length == 0) { 
			totalLength = savedata.length;
			calculateCC_sizemethod(); 
			return;
		}
		var new_rec = newRecords.pop();		
		var newAuxRowuid = new_rec.newAuxRowuid? new_rec.newAuxRowuid: segtNewROWUID;
		//var CC_YearStatus = ( ( (clientSettings['FutureYearEnabled'] == '1') && getCategoryFromSourceTable(new_rec.sourceTable).YearPartitionField && (getCategoryFromSourceTable(new_rec.sourceTable).YearPartitionField != '0') )? ( (window.opener? window.opener.showFutureData : showFutureData) ? 'F' : 'A'  ) : "" );
		//saveData += '^^^' + parcel.Id + '|' + newAuxRowuid + '|' + new_rec.parentData.ROWUID + '||' + new_rec.sourceTable + '|new|' + CC_YearStatus + '\n'; 
		insertNewAuxRecordSketch(parcel.Id, new_rec.parentData, new_rec.catId, newAuxRowuid, null, function (saveDataSeg, rowid) {
			if(saveDataSeg)
				saveData += saveDataSeg;
			updateDefaultValues(parcel.Id, rowid, new_rec.parentData.ROWUID, new_rec.sourceTable);
			if (!new_rec.doNotUpdate) {
				new_rec.saveObj.parentRowid = new_rec.parentData.ROWUID;
				new_rec.saveObj.rowId = rowid;
				new_rec.saveObj.status = ((window.opener ? window.opener.showFutureData : showFutureData) ? 'F' : 'A');
				new_rec.saveObj.extension = new_rec.extension;
				savedata.push(_.clone(new_rec.saveObj));
			}
			var segNewRowUid = newAuxRowuid + 8;
			createNew( segNewRowUid ); 
		});
			
	}
	
	var deleteMHFun = function() {
		if(deleteMHRecords.length == 0){
			var segNewRowUid = ccTicks()
			createNew( segNewRowUid ); 
			return;
		}
		var MHRec = deleteMHRecords.pop();
		var cattId = getCategoryFromSourceTable(ConfigTables.manuf_housing).Id;
		var ParRecord = MHRec.parent;
		if(ParRecord.length == 0)
			deleteMHFun();
		else {
			saveData += '^^^' + parcel.Id + '|' + ParRecord.ROWUID + '|||' + ConfigTables.manuf_housing + '|delete|' + ParRecord.ParentROWUID + '\n';
			deleteMHFun();
		}
	}
	
	var deleteRecs = function() {
		if (deleteData.length == 0) { 
			if(deleteMHRecords.length > 0)
				deleteMHFun();
			else {	
				var segNewRowUid = ccTicks()
				createNew( segNewRowUid ); 
			}
			return; 
		}			
		var rec = deleteData.pop();
		if(rec.isMHRecord){
			deleteMHRecords.push(_.clone(rec));
			deleteRecs(deleteData);
			return;
		}
		var del_Recs = ((rec.isCurrentRecord || rec.isICDRecord || rec.isCurrentOutBD || rec.isCurrentOutBD_par || rec.isextRecord)? [rec.parent]: rec.parent[rec.source]);
		var recCount = del_Recs.length, count = 0;
		if (rec.isCurrentRecord) {
			var _segId = getDataField( 'CC_segment_id', rec.source ).Id, _vId = getDataField( 'CC_segment_id', rec.vector_tbl ).Id, _lId = getDataField( 'CC_segment_id', rec.lbl_tbl ).Id;
			var _cSegId = del_Recs[0]['CC_segment_id'].toString();
			var _randomSegId = ( -( Number( _cSegId + ( Math.floor(Math.random() * (100000 -0 + 1) ) + 0) ) ) );
			saveData += '^^^' + parcel.Id + '|' + del_Recs[0].ROWUID + '|' + _segId + '||' + _randomSegId + '|edit|' + del_Recs[0].ParentROWUID + '\n';
			del_Recs[0][rec.vector_tbl].forEach(function(vect) {
				saveData += '^^^' + parcel.Id + '|' + vect.ROWUID + '|' + _vId + '||' + _randomSegId + '|edit|' + vect.ParentROWUID + '\n';
				deleteData.push({ source: rec.vector_tbl, parent: del_Recs[0] });
			});
			del_Recs[0][rec.lbl_tbl].forEach(function(vect) {
				saveData += '^^^' + parcel.Id + '|' + vect.ROWUID + '|' + _lId + '||' + _randomSegId + '|edit|' + vect.ParentROWUID + '\n';
				deleteData.push({ source: rec.lbl_tbl, parent: del_Recs[0] });
			});
		}
		if (rec.isCurrentOutBD) {
			del_Recs[0][rec.vector_tbl].forEach(function(vect) {
				deleteData.push({ source: rec.vector_tbl, parent: del_Recs[0] });
			});
		}
		var catId = getCategoryFromSourceTable(rec.source).Id;
		function callback() { if (++count == recCount) deleteRecs(); }	
		if (rec.source.indexOf(ConfigTables.SktVector) > -1 || rec.source.indexOf('improvement') > -1) {
			var sourceTTable = rec.source;
			if (recCount > 0)
				del_Recs.forEach(function(rec) {	
					saveData += '^^^' + parcel.Id + '|' + rec.ROWUID + '|||' + sourceTTable + '|delete|' + rec.ParentROWUID + '\n';
					callback();
				});
			else deleteRecs(deleteData);
		}
		else if (rec.source.indexOf(ConfigTables.SktVector_outbuilding) > -1) {
			var sourceTTable = rec.source;
			if (recCount > 0){
				if(deleteData.length == 0 && rec.source.indexOf('improvement') > -1 && !(_.isEmpty(cachedAuxOptions)))
					del_Recs.forEach(function(rec) {
						saveData += '^^^' + parcel.Id + '|' + rec.ROWUID + '|||' + sourceTTable + '|delete|' + rec.ParentROWUID + '\n';
						callback();
					});
				else
					del_Recs.forEach(function(rec) {
						saveData += '^^^' + parcel.Id + '|' + rec.ROWUID + '|||' + sourceTTable + '|delete|' + rec.ParentROWUID + '\n';
						callback();
					});
			}
			else deleteRecs(deleteData);
		}
		else {
			var sourceTTable = rec.source;
			if (recCount == 0) deleteRecs();
			else
				del_Recs.forEach(function(del_rec) {
					saveData += '^^^' + parcel.Id + '|' + del_rec.ROWUID + '|||' + sourceTTable + '|delete|' + del_rec.ParentROWUID + '\n';
					//saveData += '^^^' + parcel.Id + '|' + del_rec.ROWUID + '|' + status_field.Id + '||' + 'H' + '|edit|' + del_rec.ParentROWUID + '\n';
					callback();
				});
		}
	}
	
	var evaluate_otherColumns = function(sketch_codes, sketch_type, area, floor_no, nValue, recordit) {
		if (sketch_type == 'I') return [];
		var otherValues = [];
		area = area ? Math.abs(area) : area;
		var other_column = 'OtherColumns';
		other_column = (sketch_type == 'Comm'? 'Comm': (sketch_type == 'Imp'? 'Imp': '')) + other_column;
		var other_col_data = sketch_codes[other_column];
		
		try {
			other_col_data = JSON.parse(other_col_data);
			other_col_data.forEach(function(other_val){
				if (other_val.value == '{area}') other_val.value = area;
				if (other_val.value == '{field_1}') other_val.value = (sketch_codes.field_1 ? sketch_codes.field_1 : '');
				if (other_val.value == '{field_2}') other_val.value = (sketch_codes.field_2 ? sketch_codes.field_2 : '');
				if (other_val.value == '{startingfloor}') other_val.value = floor_no;
				if (other_val.value == '{n}') other_val.value = nValue;	
				other_val.record = recordit;
				if(other_val.table == "ccv_res_floor" && sketch_codes.FloorKey) {
					other_val.FloorKey = sketch_codes.FloorKey;
				}				
			});
			return other_col_data;
		} catch(ex) { return []; }
	}
	
	var sketchRowuids = _.uniq(sketchDataArray.map(function(skda){ return skda.parentRow.ROWUID }));
	
	sketchRowuids.forEach(function(skrowid){
		var ext_rec_record = parcel['ccv_extensions'].filter(function(sk){ return (sk.ROWUID == skrowid); })[0];
		var ResorComm = ext_rec_record.RorC;
		var scaleVal = '80';
		//var extERC = extension_Recd_Array.filter(function(ere){return ere.eRowuid == skrowid})[0];
		extensionRecordsArray.push(_.clone({ ext_rowuid: skrowid, ReComm: ResorComm}));
		if(ResorComm == 'R'){
			var ImpRECOD = [], ResRECOD = [];
			ext_rec_record['ccv_improvements'].forEach(function(impRe){
				if(impRe.CC_Deleted != true)
					ImpRECOD.push(_.clone({ICDwellsRecords: impRe , extCounter: 0, impRouidss: impRe.ROWUID}));
			});
			ext_rec_record['ccv_improvements_dwellings'].forEach(function(Re){
				if(Re.CC_Deleted != true)
					ResRECOD.push(_.clone({ICDwellsRecords: Re , extCounter: 0, Rouidss: Re.ROWUID}));
			});
		//	extension_Recd_Array.push(_.clone({eRowuid: extension_rowuid, iRecord: [], CdRecord: []}));
			/* if(extERC){
				extERC.iRecord.forEach(function(impRe){
					ImpRECOD.push(_.clone({ICDwellsRecords: impRe , extCounter: 0, impRouidss: impRe.ROWUID}));
				});
				extERC.CdRecord.forEach(function(Re){
					ResRECOD.push(_.clone({ICDwellsRecords: Re , extCounter: 0, Rouidss: Re.ROWUID}));
				});
			}*/
			ProvalExteriorFeatureCounter.push(_.clone({ ext_rowuid: skrowid, RORC: ResorComm, ImPRecords: ImpRECOD, CDwellsRecords: ResRECOD}));
		}
		else if(ResorComm == 'C'){
			var ImpRECOD = [], commRECOD = [];
			ext_rec_record['ccv_improvements'].forEach(function(impRe){
				if(impRe.CC_Deleted != true)
					ImpRECOD.push(_.clone({ICDwellsRecords: impRe , extCounter: 0, impRouidss: impRe.ROWUID}));
			});
			ext_rec_record['ccv_improvements_comm_bldgs'].forEach(function(comRe){
				if(comRe.CC_Deleted != true)
					commRECOD.push(_.clone({ICDwellsRecords: comRe , extCounter: 0, Rouidss: comRe.ROWUID}));
			});
			//var extERC = extension_Recd_Array.filter(function(ere){return ere.eRowuid == skrowid});
			/*if(extERC){
				extERC.iRecord.forEach(function(impRe){
					ImpRECOD.push(_.clone({ICDwellsRecords: impRe , extCounter: 0, impRouidss: impRe.ROWUID}));
				});
				extERC.CdRecord.forEach(function(Re){
					commRECOD.push(_.clone({ICDwellsRecords: Re , extCounter: 0, Rouidss: Re.ROWUID}));
				});
			}*/
			ProvalExteriorFeatureCounter.push(_.clone({ ext_rowuid: skrowid, RORC: ResorComm, ImPRecords: ImpRECOD, CDwellsRecords: commRECOD}));
		}
		seg_id = 0;
		var sketchOutBuilding = sketchApp.sketches.filter( function ( sBuild ) { return sBuild.uid == skrowid } )[0];		
		var sketchOutB = sketchDataArray.filter( function ( a ) { return (a.type == 'outBuilding') } );
		var extRocord = sketchOutBuilding.parentRow;
		var scaleField = getDataField('scale',ConfigTables.SktHeader);
		if(extRocord[ConfigTables.SktHeader][0]){
			scaleVal =  ( sketchOutBuilding.boundaryScale? sketchOutBuilding.boundaryScale: ( ( extRocord[ConfigTables.SktHeader][0]['scale'] && extRocord[ConfigTables.SktHeader][0]['scale'] != '0' ) ? extRocord[ConfigTables.SktHeader][0]['scale'] : ( ( extRocord.RorC == 'R' ) ? '80' : '200' ) ) );
			saveData += '^^^' + parcel.Id + '|' + extRocord[ConfigTables.SktHeader][0].ROWUID + '|' + scaleField.Id + '||' + scaleVal + '|edit|' + extRocord[ConfigTables.SktHeader][0].ParentROWUID + '\n';
		}
		sketchOutB.filter(function(skda){ return skda.parentRow.ROWUID == skrowid }).forEach(function(vectOB) {
			var saveObj = { parentRowid: skrowid, parentTbl: 'ccv_extensions', rowId: null, vector_start: null, oldStartVal: null, vects: [], labels: [], OBimp_Values: [], OBimp_ValuesTrue: false, segment_tbl: ConfigTables.SktOutbuilding, vect_tbl: ConfigTables.SktVector_outbuilding, status: ((window.opener ? window.opener.showFutureData : showFutureData) ? 'F' : 'A')}
			var parentTbl = 'ccv_extensions';
			if(vectOB.vectorString != '' && vectOB.vectorString != null && vectOB.vectorString != ' '){
				var extenRecord = parcel['ccv_extensions'].filter(function(sk){ return ((sk.ROWUID == skrowid) && sk.CC_Deleted != true); })[0];
				extenRecord = extenRecord ? extenRecord: extRocord;
				var sketch_source_name = ConfigTables.SktOutbuilding;			
				var segment_data;
				var parent_sketch_table = '', outBd_sketch_imp_id = '';
				vectOB.vectorString.split(';').forEach(function(vectorString, index) {
					var sid = (vectOB.newRecord && index >= 0) ? (ccTicks() - 10 * index):vectOB.sid;
					saveObj.rowId = sid;
					saveObj.vects=[],saveObj.label=[];
					var points = vectorString.split(':')[1].split(' S ');
					saveObj.vector_start = convertStrToPoint(points[0].replace(' ', '/'));
					if (vectOB.newRecord)
						segment_data = extenRecord;
					else segment_data = parcel[sketch_source_name].filter(function(sk){ return sk.ROWUID == sid })[0];
					var imp_id = (vectOB.newRecord)? vectOB.outBd_imp_id: segment_data['improvement_id'];
					if(imp_id && imp_id.split('{').length > 1)
						imp_id = imp_id.split('{')[1].split('}')[0];
					var imp_OB =  (imp_id && !(isNaN(imp_id)))? parseInt(imp_id): imp_id;
					parent_sketch_table = parcel['ccv_improvements'].filter(function(pdatas){ return (pdatas.ROWUID == imp_OB) || (pdatas.improvement_id == imp_OB) && (pdatas.ParentROWUID == extenRecord.ROWUID) && pdatas.CC_Deleted != true})[0];
					saveObj.label_code = vectOB.outBFields[0].label_code? vectOB.outBFields[0].label_code: segment_data['label_code'];
					saveObj.miscValue = vectOB.outBFields[0].miscValue? vectOB.outBFields[0].miscValue: parent_sketch_table? parent_sketch_table['misc_imp_type']: null;
					outBd_sketch_imp_id = vectOB.outBd_imp_id				
					var ob_imptype = OutB_Lookups["ob_codes_imp_type"][saveObj.label_code]? OutB_Lookups["ob_codes_imp_type"][saveObj.label_code].AdditionalValue1: '';
					if(vectOB.newRecord || (segment_data && segment_data.label_code != saveObj.label_code)){
						saveObj.imp_type = (saveObj.label_code == 'O054' || saveObj.label_code == 'O603')? 'MISC' : ob_imptype;
						saveObj.misc_imp_type = saveObj.miscValue;
					}

					let ob_vector = sketchOutBuilding.vectors.filter((v) => { return v.uid == vectOB.sid })[0];

					if (vectOB.isModified) {
						let nn = ob_vector?.startNode ? ob_vector.startNode.nextNode : null;

				    	points.length > 1 && points[1].split(/\s/).forEach(function(p, i) {
					    	if ( p.trim() != '' && p.trim() != 'P2' ){
					        	var rr = convertStrToPoint( p );
					        	var ch = 0;
					        	if ( rr.indexOf( '/' ) > -1)  {
					        		var st = rr;
					           		rr = st.split( '/' )[0];
					           		ch = st.split( '/' )[1];
					        	}

					        	var imptid = vectOB.newRecord ? outBd_sketch_imp_id : segment_data['improvement_id'];
					        	var exten = (segment_data['extension'] != ' ' && segment_data['extension'] != null ) ? segment_data['extension'] : skrowid;
								var area = vectOB.isUnSketchedArea ? 0 : vectOB.area;
								let pl_type = nn?.lineStrokePattern ? nn?.lineStrokePattern : '0', pf_level = '0', pn_floors = '0';
								if (nn?.proval_line_type) {
									let pl_ts = nn.proval_line_type.split('~');
									pf_level = pl_ts[1] ? pl_ts[1] : '0';
									pn_floors = pl_ts[2] ? pl_ts[2] : '0';
								}
								nn = nn && nn.nextNode;
								saveObj.vects.push({ curve_height: ch, run_rise: rr, seq_number: (i + 1), extension: exten, improvement_id: imptid, status: ((window.opener ? window.opener.showFutureData : showFutureData) ? 'F' : 'A'), eff_year: '0', line_type: pl_type, area: parseInt(area), perimeter: parseInt(vectOB.perimeter), vector: ((convertStrToPoint(p, true) * 10) << 16), dimension_font_size: '8', dimension_position: '0', floor_level: pf_level, number_floors: pn_floors });
					    	}
						});

						if(parent_sketch_table && !vectOB.isUnSketchedArea){
							var imp_width = 0 ,imp_length = 0,hLength = 0,RLength = 0, imp_size = 0, lT=0, uT=0; 							
							 points.length > 1 && points[1].split(/\s/).forEach(function(p, i) {
							 	if ( p.trim() != '' && p.trim() != 'P2' ){
									if(p.split("/").length > 1)
										return;
									else{
										if((p.split("L").length > 1 || p.split("R").length > 1) && lT == 0 ){
											hLength = p.split("L").length > 1 ? parseFloat(p.split("L")[1]) : parseFloat(p.split("R")[1]);
											lT = 1;
										}
										else if((p.split("U").length > 1 || p.split("D").length > 1) && uT == 0 ){
											RLength = p.split("U").length > 1 ? parseFloat(p.split("U")[1]) : parseFloat(p.split("D")[1]);
											uT =1;
										}	
									}
							 	}
							});							 	
							if(hLength > RLength) {
								imp_length = hLength;
								imp_width = RLength;
							}
							else{
								imp_length = RLength;
								imp_width = hLength;
							}
							imp_size = vectOB.isUnSketchedArea? 0: Math.round(vectOB.area);
							saveObj.OBimp_Values.push(  {iMP_Record: parent_sketch_table, imp_width :imp_width, imp_length: imp_length, imp_size: imp_size, imp_type: saveObj.imp_type, misc_imp_type: saveObj.misc_imp_type});
							saveObj.sketchedOB = true;
							saveObj.OBimp_ValuesTrue = true;
						}
						if(parent_sketch_table && vectOB.isUnSketchedArea){
							saveObj.OBimp_Values.push(  {iMP_Record: parent_sketch_table, imp_type: saveObj.imp_type, misc_imp_type: saveObj.misc_imp_type});
							saveObj.sketchedOB = false;
							saveObj.OBimp_ValuesTrue = true;
						}
						if (extensions.indexOf(segment_data['extension']) == -1) extensions.push(segment_data['extension'])
						saveObj.extension = (segment_data['extension'] != ' ' && segment_data['extension'] != null ) ? segment_data['extension'] : skrowid;
						saveObj.improvement_id = vectOB.newRecord ? outBd_sketch_imp_id : segment_data['improvement_id'];
						saveObj.label_position = (vectOB.isUnSketchedArea)? '655370000': ProvalLabelPositionFromString(vectOB.labelPositions[index]);
						var pos = vectOB.labelPositions[index];
						pos.y=pos.y - (((vectOB.label.split('|').length)==1)?4: (vectOB.label.split('|').length* 3));
						saveObj.displayed_area_position = ProvalLabelPositionFromString(pos);
						if (vectorString && vectorString.trim().length > 0) {
							saveObj.parentRowid = segment_data['ParentROWUID'] == null? segment_data['ROWUID']: segment_data['ParentROWUID'];
							saveObj.area = vectOB.fixedAreaTrue ? vectOB.fixedAreaTrue : parseInt(vectOB.area);
							saveObj.perimeter = vectOB.fixedPerimeterTrue ? vectOB.fixedPerimeterTrue: parseInt(vectOB.perimeter);
							saveObj.eff_year = '0';
						}
						if (!vectOB.newRecord) {
							deleteData.push({ source: saveObj.vect_tbl, parent: segment_data });
							if (vectorString && vectorString.trim().length > 0) 
								savedata.push(_.clone(saveObj));
						}
						else 
							newRecords.push({ catId: getCategoryFromSourceTable(ConfigTables.SktOutbuilding).Id, parentData: segment_data, saveObj: _.clone(saveObj), extension: saveObj.extension, improvement_id: saveObj.improvement_id, sourceTable: sketch_source_name});
					}
					/*else 
						savedata.push({ isSegmentIdOnly: true, miscValue: saveObj.miscValue, improvement_id: segment_data['improvement_id'], label_code: saveObj.label_code, rowId: saveObj.rowId, seg_tbl: sketch_source_name, vect_tbl: saveObj.vect_tbl});*/
				});
			}
		});	
		
		let obImprovementIds = [];
		let obVectors = sketchApp.sketches.filter((ss) => { return (ss.uid == skrowid) })[0].vectors.filter((v) => { return (v.sketchType == 'outBuilding') });
		obVectors.forEach((ov) => {
			let out_imprId = null;
			if (ov.newRecord) out_imprId = ov.outBd_imp_id? ov.outBd_imp_id: null;
			else {
				let outRecord = parcel[ConfigTables.SktOutbuilding].filter((out) => { return (out.ROWUID == ov.uid) })[0];
		 	 	out_imprId = outRecord && outRecord.improvement_id && outRecord.improvement_id != ''? outRecord.improvement_id: null;
	 	 	}
	 	 	if (out_imprId)	{ 
	 	 		if (out_imprId.split('{').length > 1) out_imprId = out_imprId.split('{')[1].split('}')[0];
	 	 		obImprovementIds.push(out_imprId);	 
			}	 	 	
		});
		
		let fsketchDataArray = sketchDataArray.filter( function ( a ) { return (a.type != 'outBuilding')} );
		fsketchDataArray.filter(function(skda){ return skda.parentRow.ROWUID == skrowid }).forEach(function(skd){
			var saveObj = { parentRowid: skrowid, parentTbl: 'ccv_extensions', rowId: null, vector_start: null, oldStartVal: null, vects: [], labels: [], floor_record: [], segment_tbl: ConfigTables.SktSegment, vect_tbl: ConfigTables.SktVector, lbl_table: ConfigTables.SktSegmentLabel, CC_segment_id: seg_id, status: ((window.opener ? window.opener.showFutureData : showFutureData) ? 'F' : 'A'), floor_table: null, otherValues: [], MHOMEValues: [], MHOMEValuesTrue:false}
			var parentTbl = 'ccv_extensions', floor_col = 'FloorRecord', floor_key_col = 'FloorKey', grade_col = 'GradeLevel';
			if (skd.vectorString != '' && skd.vectorString != null && skd.vectorString != ' ') {
				let sk_vector = sketchOutBuilding.vectors.filter((v) => { return v.uid == skd.sid })[0];
				var skt_type = 'I';
				var extenRecord = parcel['ccv_extensions'].filter(function(sk){ return ((sk.ROWUID == skrowid) && sk.CC_Deleted != true); })[0];
				extenRecord = extenRecord ? extenRecord: extRocord;
				extRuid = extenRecord.ROWUID;
				if(extenRecord.RorC == 'R'){
					saveObj.floor_table = ConfigTables.res_floor;
				}
				else if(extenRecord.RorC == 'C'){
					saveObj.floor_table = ConfigTables.comm_floors;
					floor_key_col='FloorID';
				}
				var sketch_source_name = ConfigTables.SktSegment;
				var llablel_details = skd.labelDetails[0];
				var parent_sketch_table = '', parent_sketch_imp_id = '';			
				if(llablel_details[2].parent_type != ''){
					if(llablel_details[2].parent_type == 'dwellings'){
						parent_sketch_table = 'ccv_improvements_dwellings';
						parent_sketch_imp_id = llablel_details[2].parent_imp_id;
					}
					else if(llablel_details[2].parent_type == 'comm_bldgs'){
						parent_sketch_table = 'ccv_improvements_comm_bldgs';
						parent_sketch_imp_id = llablel_details[2].parent_imp_id;
					}
					else if(llablel_details[2].parent_type == 'imps'){
						parent_sketch_table = 'ccv_improvements';
						parent_sketch_imp_id = llablel_details[2].parent_imp_id;
					}
				}
				else if(llablel_details[1].parent_type != ''){
					if(llablel_details[1].parent_type == 'dwellings'){
						parent_sketch_table = 'ccv_improvements_dwellings';
						parent_sketch_imp_id = llablel_details[1].parent_imp_id;
					}	
					else if(llablel_details[1].parent_type == 'comm_bldgs'){
						parent_sketch_table = 'ccv_improvements_comm_bldgs';
						parent_sketch_imp_id = llablel_details[1].parent_imp_id;
					}
					else if(llablel_details[1].parent_type == 'imps'){
						parent_sketch_table = 'ccv_improvements';
						parent_sketch_imp_id = llablel_details[1].parent_imp_id;
					}
				}
				else if(llablel_details[3].parent_type != ''){
					if(llablel_details[3].parent_type == 'dwellings'){
						parent_sketch_table = 'ccv_improvements_dwellings';
						parent_sketch_imp_id = llablel_details[3].parent_imp_id;
					}	
					else if(llablel_details[3].parent_type == 'comm_bldgs'){
						parent_sketch_table = 'ccv_improvements_comm_bldgs';
						parent_sketch_imp_id = llablel_details[3].parent_imp_id;
					}
					else if(llablel_details[3].parent_type == 'imps'){
						parent_sketch_table = 'ccv_improvements';
						parent_sketch_imp_id = llablel_details[3].parent_imp_id;
					}
				}
				var segvectImprovementId = parent_sketch_imp_id;
				if(parent_sketch_imp_id.split('{').length > 1)
					parent_sketch_imp_id = parent_sketch_imp_id.split('{')[1].split('}')[0];
				skd.vectorString.split(';').forEach(function(vectorString, index) {
					var sid = (skd.newRecord ) ? (ccTicks() - 10 ): skd.sid;
					saveObj.rowId = sid;
					saveObj.floor_record=[], saveObj.vects=[], saveObj.labels=[], saveObj.otherValues=[];
					if (vectorString == '') return;
					++seg_id;
				
					var points = vectorString.split(':')[1].split(' S ');
					saveObj.vector_start = convertStrToPoint(points[0].replace(' ', '/'));
					saveObj.CC_segment_id = seg_id;
					var segment_data, Mtype = false, mTable = 'ccv_improvements_dwellings';
					if (skd.newRecord)
						segment_data = extenRecord;
					else segment_data = parcel[sketch_source_name].filter(function(sk){ return sk.ROWUID == sid })[0];
					var lvl_code_values = {1: 'Below', 2: 'At', 3: 'Above'};
					var MH_dwellRecord = [], floor_key_cols = '';
					function basecalculate(det){
						var lbl = { lbl_level_code: 1, lbl_const_code: 0, lbl_ext_feat_code: '0', CC_segment_id: seg_id, status: ((window.opener ? window.opener.showFutureData : showFutureData) ? 'F' : 'A'), modifier: " ", area: parseInt(skd.areas[index]), perimeter: parseInt(skd.perimeters[index]), improvement_id: '', extension: ((segment_data['extension'] != ' ' && segment_data['extension'] != null ) ? segment_data['extension'] : skrowid),  default_ext_cover: '0', lbl_frac1: 0, lbl_frac2: 0,lbl_frac3: 0, lbl_frac4: 0, finish_area: 0, unfin_area: 0, area2: 0,area3: 0, extfeat_id: 0}; 
				 		var sTable = ((det.parent_type == 'dwellings') ? 'ccv_improvements_dwellings' : (det.parent_type == 'comm_bldgs') ? 'ccv_improvements_comm_bldgs' :'ccv_improvements');	
				 		var skt_type = ((det.parent_type == 'dwellings') ? 'Res' : (det.parent_type == 'comm_bldgs') ? 'Comm' :'Imp'); 
						var grade_col = 'GradeLevel';
						var grade_col = skt_type + grade_col;
				 		if(skt_type == 'comm'){
				 			floor_col = skt_type + floor_col;
							floor_key_col='FloorID';
							floor_key_col = skt_type + floor_key_col;
							floor_key_cols = skt_type + 'FloorNumber';
				 		}
				 		var flr = {floor_recs: 0, floor_recorddd:[], otherValues: []};
				 		var tArea = skd.fixedAreaTrue ? parseInt(skd.fixedAreaTrue) : parseInt(skd.areas[index]);
				 		var tperimeter = skd.fixedPerimeterTrue ? Math.round(parseFloat(skd.fixedPerimeterTrue)): Math.round(skd.perimeters[index]);
				 		var lab_details = skd.labelDetails[0][1];
				 		var Ba_details = skd.basementDetails[0];
				 		lbl.improvement_id = det.parent_imp_id;
				 		var impIDD = '', impdwellcommRecord =[];
				 		if(det.parent_imp_id.split('{').length > 1)
							impIDD = det.parent_imp_id.split('{')[1].split('}')[0];
						else
							impIDD = det.parent_imp_id;
						impdwellcommRecord = parcel[sTable].filter(function(sk){ return ((sk.ROWUID == impIDD || sk.improvement_id == impIDD) && sk.ParentROWUID == extRuid && sk.CC_Deleted != true); })[0];	
						impdwellcommRecord = impdwellcommRecord? impdwellcommRecord: [];
				 		//impdwellcommRecord = impdwellcommRecord ? impdwellcommRecord : (extERC ? ((sTable == 'ccv_improvements') ? (extERC.iRecord.filter(function(sk){ return (sk.ROWUID == impIDD || sk.improvement_id == impIDD); })[0]) :(extERC.CdRecord.filter(function(sk){ return (sk.ROWUID == impIDD || sk.improvement_id == impIDD); })[0])): []);
				 		var bind = 0;
				 		var fin_area1=0,unfin1=0,area2=0,area3=0;
				 		function flrcalculate(comp , pre, modi, modisign,walkout , fin_area, unfin, baArea, type){
				 			var fins_area = det.fin_area ? det.fin_area : 0;
				 			var lbl_frac = 0,fin_areas=0, floorBFinExists = false;
				 			var prefix_lkup = Proval_Lookup.filter(function(lkp){ return lkp.tbl_element == pre && lkp.tbl_type_code == 'Prefix' && lkp[grade_col] && lkp[grade_col].indexOf(lvl_code_values[1]) > -1 })[0];
				 			prefix_lkup = prefix_lkup? prefix_lkup: {}
				 			var comp_lkup = Proval_Lookup.filter(function(lkp){ return lkp.tbl_element == comp && lkp.tbl_type_code == 'Component' && lkp[grade_col] && lkp[grade_col].indexOf(lvl_code_values[1]) > -1 })[0];
				 			comp_lkup = comp_lkup? comp_lkup: {}
				 			if (pre.toString() != 0) {
				 				flr.floor_recs +=  ((prefix_lkup[floor_col] && !isNaN(prefix_lkup[floor_col]))? parseInt(prefix_lkup[floor_col]): 0);
				 			}
				 			if (comp.toString() != 0) 
				 				flr.floor_recs +=  ((comp_lkup[floor_col] && !isNaN(comp_lkup[floor_col]))? parseInt(comp_lkup[floor_col]): 0);
				 			var comp1 = '', prefix = '', flr_area = 0;
				 			if(walkout=="Y")
				 				comp = 1202;
				 			if (comp.toString() == '0' && pre.toString() != '0') 
				 				lbl_frac = pre.toString();
				 			else {
					 			if (comp.toString() != '0')  comp1 = (parseInt(comp) << 16).toString(2);
					 			if (pre.toString() != '0') prefix = ((parseInt(pre)) >>> 0).toString(2);
					 			lbl_frac = ((comp1 == ''? 0: parseInt(comp1, 2)) + (prefix == ''? 0: parseInt(prefix, 2))).toString();
							}
							flr.finish_living_areas = 0
							if(modi =="Fin"){
								var lbl_update=0;
								fin_areas = fin_area?fin_area : 0;
						 		flr.finish_living_areas = ((fin_area != 0) ? fin_area: ((fins_area == 0) ? tArea : 0));
						 		if(modisign=="+"){
						 			flr.floor_fin_qual=2;
						 			lbl_frac=parseInt(lbl_frac)+9887014715392;
						 			lbl_update=1;
						 		}
						 		else if(modisign=="-"){
						 			flr.floor_fin_qual=1;
						 			lbl_frac=parseInt(lbl_frac)+5592047419392;
						 			lbl_update=1;
						 		}
						 		if(lbl_update==0){
						 			lbl_frac=parseInt(lbl_frac)+1297080123392;
						 		}
							}
							else if(modi==""){
								fin_areas = '0';
								flr.finish_living_areas =  0;
							}
							if(type == 1)
								lbl.lbl_frac1 = lbl_frac;
							else if(type == 2)
								lbl.lbl_frac2 = lbl_frac;
							else if(type == 3)
								lbl.lbl_frac3 = lbl_frac;
							else
								lbl.lbl_frac4 = lbl_frac;
								
							if(comp.toString() == '206' || comp.toString() == '202' || comp.toString() == '1202'){
								if( modi == "Fin" )
									floorBFinExists = true;
							}
							
							flr.ext_cover1 = '';
							[prefix_lkup, comp_lkup].forEach(function(lkup){
								var other_val = evaluate_otherColumns(lkup, skt_type, tArea, impdwellcommRecord);
								other_val.forEach(function(ov){
									if (ov.table == saveObj.floor_table) 
										flr.otherValues.push(ov);
									else saveObj.otherValues = _.union(_.clone(saveObj.otherValues), ov);
								});
							});
						 
							if ((comp_lkup[floor_key_col]) && skt_type != 'Imp') {
								/*
								flr.fin_area = fin_areas && !isNaN(fin_areas)? Math.round(parseFloat(fin_areas)): 0;
								if (pre == 1 || pre == 2 || pre == 3) 
									flr_area =skd.fixedAreaTrue ? (parseFloat(prefix_lkup[floor_key_col]) * parseFloat(skd.fixedAreaTrue)): (parseFloat(prefix_lkup[floor_key_col]) * parseFloat(skd.areas[index]));
								else flr_area =skd.fixedAreaTrue ? parseFloat(skd.fixedAreaTrue) : parseFloat(skd.areas[index]);
								flr.area = Math.round(flr_area);
								*/
								flr.fin_area = Math.round(fin_area);
								flr.area = Math.round(baArea);
								flr.perimeter =skd.fixedPerimeterTrue ? Math.round(parseFloat(skd.fixedPerimeterTrue)): Math.round(skd.perimeters[index]);
								flr.PAR = Math.round(parseFloat((flr.perimeter / flr.area) * 100));
								flr.extension = (segment_data['extension'] != ' ' && segment_data['extension'] != null ) ? segment_data['extension'] : skrowid;
								flr.extensionRowuid = skrowid;
								flr.dwelling_number = (skd.newRecord && segment_data[sTable][0])? segment_data[sTable][0].dwelling_number: (segment_data.dwelling_number ?segment_data.dwelling_number:'1' );
								flr.walkout_bsmt = walkout;
								flr.floor_recorddd.push(impdwellcommRecord);
								flr.lbl_lvl_code = "1";
								flr.isSketchModified = skd.isModified ? skd.isModified : false;
								var f_key_col = (skt_type == 'Comm'? floor_key_cols: floor_key_col);
								/*
								if ((prefix_lkup && prefix_lkup[f_key_col]) ) {
									flr.floorKey = ((prefix_lkup[f_key_col])).toString();;
						 			saveObj.floor_record.push(_.clone(flr));
					 			}
					 			*/
								if (comp_lkup && comp_lkup[f_key_col]) {
									flr.floorKey = (comp_lkup[f_key_col]).toString();
									if (floorBFinExists && flr.isSketchModified)
						 				flr.floorBFinExists = floorBFinExists;
						 			saveObj.floor_record.push(_.clone(flr));
					 			}
							}
				 		}
				 		var areaCounter = 2;
				 		function belowAreaCalculate(comps, modifis){
				 			var baAreas = 0, fin_areass = 0, unfin_areass = 0;
				 			var Ba_detailss = Ba_details.filter(function (x) {return x.Id == comps});
				 			if(comps == '202' && Ba_detailss && Ba_detailss.length > 1){
				 				baAreas = Math.round(Ba_detailss[0].area) + Math.round(Ba_detailss[1].area);
				 				fin_areass = Math.round(Ba_detailss[0].area);
				 				fin_area1 = Math.round(fin_area1) + Math.round(fin_areass);
				 				unfin_areass = Math.round(Ba_detailss[1].area);
				 				unfin1 = Math.round(unfin1) + Math.round(unfin_areass);
				 			}
				 			else {
				 				baAreas = Math.round(Ba_detailss[0].area);
				 				if(comps == '206' && comps == 'Fin'){
				 					fin_areass = Math.round(Ba_detailss[0].area);
				 					fin_area1 = Math.round(fin_area1) + Math.round(fin_areass);
				 				}
				 				else if(comps == '206' || modifis == '202'){
				 					unfin_areass = Math.round(Ba_detailss[0].area);
				 					unfin1 = Math.round(unfin1) + Math.round(unfin_areass);
				 				}
				 				if(comps != '206' && comps != '202' && modifis == 'Fin')
				 					fin_areass = Math.round(Ba_detailss[0].area);
				 				if(comps != "205" && comps != '202'){
				 					if(areaCounter == 2)
				 						area2 = Math.round(Ba_detailss[0].area);
				 					else if(areaCounter == 3)
				 						area3 = Math.round(Ba_detailss[0].area);
				 					areaCounter = areaCounter + 1;
				 				}
				 				
				 			}
				 			return ({baAreas: baAreas, fin_areass: fin_areass, unfin_areass: unfin_areass})
				 		}
				 		
				 		
				 		if(lab_details.comp1 != 0){
				 			var bfuAreas = belowAreaCalculate(lab_details.comp1, lab_details.modifier1);
				 			flrcalculate(lab_details.comp1, lab_details.prefix1, lab_details.modifier1, lab_details.modifierSign1, lab_details.walkout1 , bfuAreas.fin_areass, bfuAreas.unfin_areass, bfuAreas.baAreas, 1)
				 		}
				 		if(lab_details.comp2 != 0){
				 			var bfuAreas = belowAreaCalculate(lab_details.comp2, lab_details.modifier2);
				 			flrcalculate(lab_details.comp2 , lab_details.prefix2, lab_details.modifier2, lab_details.modifierSign2, lab_details.walkout2, bfuAreas.fin_areass, bfuAreas.unfin_areass, bfuAreas.baAreas, 2)
				 		}
				 		if(lab_details.comp3 != 0){
				 			var bfuAreas = belowAreaCalculate(lab_details.comp3, lab_details.modifier3);
				 			flrcalculate(lab_details.comp3 , lab_details.prefix3, lab_details.modifier3, lab_details.modifierSign3, lab_details.walkout3, bfuAreas.fin_areass, bfuAreas.unfin_areass, bfuAreas.baAreas,3)
				 		}
				 		if(lab_details.comp4 != 0){
				 			var bfuAreas = belowAreaCalculate(lab_details.comp4, lab_details.modifier4);
				 			flrcalculate(lab_details.comp4 , lab_details.prefix4, lab_details.modifier4, lab_details.modifierSign4, lab_details.walkout4, bfuAreas.fin_areass, bfuAreas.unfin_areass, bfuAreas.baAreas,4)
				 		}
				 		
				 		lbl.finish_area = Math.round(fin_area1);
				 		lbl.unfin_area = Math.round(unfin1);
				 		lbl.area2 = Math.round(area2);
				 		lbl.area3 = Math.round(area3);
				 		var baset = false;
				 		
				 		var bgar_area = 0, bgar_label = false;
                        var bgar_details = Ba_details.filter(function (x) {return x.Id == 206})[0];
                        if(bgar_details){
                        	bgar_label = true;
					        bgar_area = bgar_details.area;
                        }
                        
                        var crawl_area = 0, crawl_label = false;
                        var crawl_details = Ba_details.filter(function (x) {return (x.Id == 203 || x.Id == '203')})[0];
                        if(crawl_details){
                        	crawl_label = true;
					        crawl_area = Math.round(crawl_details.area);
                        }
                        
                        var bfin_areasss = 0, bUnfin_areasss = 0, bTot_areasss = 0;
                        var Basment_fin_detailss = Ba_details.filter(function (x) {return (x.Id == '202' || x.Id == 202)});
                        if(Basment_fin_detailss && Basment_fin_detailss.length > 0){
                        	if(Basment_fin_detailss.length > 1){
                        		bfin_areasss = Math.round(Basment_fin_detailss[0].area);
                        		bUnfin_areasss = Math.round(Basment_fin_detailss[1].area);
                        		bTot_areasss = bfin_areasss + bUnfin_areasss;
                        	}
                        	else{
                        		bfin_areasss = 0;
                        		bUnfin_areasss = Math.round(Basment_fin_detailss[0].area);
                        		bTot_areasss = bfin_areasss + bUnfin_areasss;
                        	}
                        		
                        }
				 		
				 		if(lab_details.comp1 == '202' || lab_details.comp2 == '202' || lab_details.comp3 == '202' || lab_details.comp4 == '202' || lab_details.comp1 == '1202' || lab_details.comp2 == '1202' || lab_details.comp3 == '1202' || lab_details.comp4 == '1202' )
				 			baset = true;
				 		if(sTable == 'ccv_improvements_dwellings' || sTable == 'ccv_improvements_comm_bldgs'){
							if(sTable == 'ccv_improvements_dwellings'){
								if(baset){
									parentRecordProperties.push(_.clone({extRowuid: skrowid, improve_id: det.parent_imp_id, sour_table: sTable,record: impdwellcommRecord, level_code:'1' , fin_area: bfin_areasss, unfin_area: bUnfin_areasss,Tarea: bTot_areasss, effperimeter: tperimeter , comp_idss : '202', bgar_area: bgar_area, bgar_label: bgar_label, crawl_area: crawl_area, crawl_label: crawl_label}))
							    }
							    else if(bgar_label)
							        parentRecordProperties.push(_.clone({extRowuid: skrowid, improve_id: det.parent_imp_id, sour_table: sTable,record: impdwellcommRecord, level_code:'1' , fin_area: bfin_areasss, unfin_area: bUnfin_areasss,Tarea: bTot_areasss, effperimeter: tperimeter , comp_idss : '206', bgar_area: bgar_area, bgar_label: bgar_label, crawl_area: crawl_area, crawl_label: crawl_label}))
								else if(crawl_label)
							    	parentRecordProperties.push(_.clone({extRowuid: skrowid, improve_id: det.parent_imp_id, sour_table: sTable,record: impdwellcommRecord, level_code:'1' , fin_area: bfin_areasss, unfin_area: bUnfin_areasss,Tarea: bTot_areasss, effperimeter: tperimeter , comp_idss : '203', bgar_area: bgar_area, bgar_label: bgar_label, crawl_area: crawl_area, crawl_label: crawl_label}))
								var dwellinsert = false;
								for(k in dwellingRecordArray){
									if(dwellingRecordArray[k].extension_row_id == skrowid && dwellingRecordArray[k].dc_table == sTable){
										dwellinsert = true;
												break;
									}
								}
								if(!dwellinsert)
									dwellingRecordArray.push(_.clone({extension_row_id: skrowid, dc_table:sTable}))
											
							}
							else{
								parentRecordProperties.push(_.clone({extRowuid: skrowid, improve_id: det.parent_imp_id, sour_table: sTable,record: impdwellcommRecord, level_code:'1' , fin_area: lbl.finish_area, unfin_area: lbl.unfin_area,Tarea: tArea, effperimeter: tperimeter , comp_idss : ''}))
								var comminsert = false;
								for(k in commRecordArray){
									if(commRecordArray[k].extension_row_id == skrowid && commRecordArray[k].dc_table == sTable){
										comminsert = true;
												break;
									}
								}
								if(!comminsert)
									commRecordArray.push(_.clone({extension_row_id: skrowid, dc_table:sTable}))
							}
						}
						if (skd.isModified) saveObj.labels.push(_.clone(lbl));				 	
					}
					var twosFloorTrue = false, onesFrtrue = false, frGtrue = false, countflg = 0, atflg = 0, abvflg = 0, nsflag = 0, compflg = 0, ignoreAbove = false
					
					$.each(skd.labelDetails[index], function(lvl_code, det) {
						var valid = false;
						$.each(det, function(idx, k) {
							if(idx != 'ImpIdMethod' && idx != 'ImpType' && idx != 'parent_imp_id' && idx != 'parent_type'){
								if (k && k.toString() != '0') valid = valid || true;
							}
						});
						if (valid) {
							if(skd.BasementFunctionality && (lvl_code == '1' || lvl_code == 1))
								basecalculate(det);
							else{
								var fins_area = det.fin_area ? det.fin_area : 0;
								var floorBFinExists = false;
								var unfins_fin_area = det.unfin_fin_area ? det.unfin_fin_area: 0;
								if(det.ext_cover == 0 || det.ext_cover == 'null' || det.ext_cover == null)
									det.ext_cover = '0';
								var sTable = ((det.parent_type == 'dwellings') ? 'ccv_improvements_dwellings' : (det.parent_type == 'comm_bldgs') ? 'ccv_improvements_comm_bldgs' :'ccv_improvements');	
								var impdwellcommRecord = [], impIDD='';
								if(det.parent_imp_id.split('{').length > 1)
									impIDD = det.parent_imp_id.split('{')[1].split('}')[0];
								else
									impIDD = det.parent_imp_id;
								impdwellcommRecord = parcel[sTable].filter(function(sk){ return ((sk.ROWUID == impIDD || sk.improvement_id == impIDD) && sk.ParentROWUID == extRuid && sk.CC_Deleted != true); })[0];	
				 				impdwellcommRecord = impdwellcommRecord ? impdwellcommRecord: [];
				 				//impdwellcommRecord = impdwellcommRecord ? impdwellcommRecord : (extERC ? ((sTable == 'ccv_improvements') ? (extERC.iRecord.filter(function(sk){ return (sk.ROWUID == impIDD || sk.improvement_id == impIDD); })[0]) :(extERC.CdRecord.filter(function(sk){ return (sk.ROWUID == impIDD || sk.improvement_id == impIDD); })[0])): []);
				 				
				 				var skt_type = ((det.parent_type == 'dwellings') ? 'Res' : (det.parent_type == 'comm_bldgs') ? 'Comm' :'Imp');
				 				var grade_col = 'GradeLevel';
				 				if(skt_type == 'Comm'){
				 					floor_col = skt_type + floor_col;
									floor_key_col='FloorID';
									floor_key_col = skt_type + floor_key_col;
									floor_key_cols = skt_type + 'FloorNumber';
				 				}
								var grade_col = skt_type + grade_col;
				 				var lbl = { lbl_level_code: lvl_code, lbl_const_code: det.constr.toString(), lbl_ext_feat_code: det.ext_feat_code.toString(), CC_segment_id: seg_id, status: ((window.opener ? window.opener.showFutureData : showFutureData) ? 'F' : 'A'), modifier: det.modifier, area: parseInt(skd.areas[index]), perimeter: parseInt(skd.perimeters[index]), improvement_id: det.parent_imp_id, extension: ((segment_data['extension'] != ' ' && segment_data['extension'] != null ) ? segment_data['extension'] : skrowid),  default_ext_cover: det.ext_cover, lbl_frac2: 0,lbl_frac3: 0, lbl_frac4: 0,area2: 0,area3: 0, finish_area: 0, unfin_area: 0, extfeat_id: 0}; 
				 				var flr = {floor_recs: 0, floor_recorddd:[], otherValues: []};
				 				var tArea = skd.fixedAreaTrue ? parseInt(skd.fixedAreaTrue) : parseInt(skd.areas[index]);
				 				var tperimeter = skd.fixedPerimeterTrue ? Math.round(parseFloat(skd.fixedPerimeterTrue)): Math.round(skd.perimeters[index]);
				 				var ext_code_mat = '', matValuess = '';
								if(det.ext_feat_code && det.ext_feat_code != '0' && det.ext_feat_code != 0 && det.ext_feat_code != ''){
									var ext_splt_code = parseInt(det.ext_feat_code);
									ext_code_mat = ext_splt_code % 1000;
									matValuess = parseInt((ext_splt_code/1000));								
								}
		
				 				var prefix_lkup = Proval_Lookup.filter(function(lkp){ return lkp.tbl_element == det.prefix && lkp.tbl_type_code == 'Prefix' && lkp[grade_col] && lkp[grade_col].indexOf(lvl_code_values[lvl_code]) > -1 })[0];
				 				prefix_lkup = prefix_lkup? prefix_lkup: {}
				 				var _comp = ( det.comp && det.comp.toString() == '1202' ) ? 202: det.comp;
				 				var comp_lkup = Proval_Lookup.filter(function(lkp){ return lkp.tbl_element == _comp && lkp.tbl_type_code == 'Component' && lkp[grade_col] && lkp[grade_col].indexOf(lvl_code_values[lvl_code]) > -1 })[0];
				 				comp_lkup = comp_lkup? comp_lkup: {}
				 		
				 				var const_lkup = Proval_Default_Exterior_Cover.filter(function(pro){ return pro.tbl_type_code == 'const' && pro.tbl_element == det.constr })[0];
				 				const_lkup = const_lkup? const_lkup: {}
				 		
				 				var ext_feat_lkup = Proval_Lookup.filter(function(lkp){ return lkp.tbl_element == ext_code_mat && lkp.tbl_type_code == 'Exterior Feature' })[0];
				 				ext_feat_lkup = ext_feat_lkup? ext_feat_lkup: {}
				 		
								if (det.prefix.toString() != 0) {
									flr.floor_recs += ((prefix_lkup[floor_col] && !isNaN(prefix_lkup[floor_col])) ? parseInt(prefix_lkup[floor_col]) : (prefix_lkup[floor_col] == 'n' && !isNaN(det.ns) ? det.ns : 0));
									if ((det.prefix.toString() == '10' || det.prefix.toString() == '15') && !isNaN(det.ns) && det.ns != 0)
										lbl.lbl_n = det.ns;
								}

								if (det.comp.toString() != 0) {
									flr.floor_recs += ((comp_lkup[floor_col] && !isNaN(comp_lkup[floor_col])) ? parseInt(comp_lkup[floor_col]) : (comp_lkup[floor_col] == 'n' && !isNaN(det.ns) ? det.ns : 0));

									if (lvl_code == '1' && det.comp.toString() != '202') {
										lbl.area2 = tArea;
									}
								}

								if (det.constr.toString() != 0)
									flr.floor_recs += ((const_lkup[floor_col] && !isNaN(const_lkup[floor_col])) ? parseInt(const_lkup[floor_col]) : (const_lkup[floor_col] == 'n' && !isNaN(det.ns) ? det.ns : 0));

				 				var comp = '', prefix = '', flr_area = 0;
				 				if(det.walkout=="Y")
				 					det.comp=1202;
				 				if (det.comp.toString() == '0' && det.prefix.toString() != '0') 
				 					lbl.lbl_frac1 = det.prefix.toString();
				 				else {
					 				if (det.comp.toString() != '0')  comp = (parseInt(det.comp) << 16).toString(2);
					 				if (det.prefix.toString() != '0') prefix = ((parseInt(det.prefix)) >>> 0).toString(2);
					 				lbl.lbl_frac1 = ((comp == ''? 0: parseInt(comp, 2)) + (prefix == ''? 0: parseInt(prefix, 2))).toString();
								}
								if(det.comp.toString() == '216' || det.comp.toString() == '217'){
									Mtype = true;
									MH_dwellRecord.push(impdwellcommRecord);
								}
								// lblfract of fin selected labels
								if(det.modifier=="Fin"){
									var lbl_update=0;
						 			if(det.modifierSign == "+"){
						 				flr.floor_fin_qual = 2;
						 				lbl.lbl_frac1 = parseInt(lbl.lbl_frac1) + 9887014715392;
						 				lbl_update = 1;
						 			}
						 			else if(det.modifierSign == "-"){
						 				flr.floor_fin_qual = 1;
						 				lbl.lbl_frac1 = parseInt(lbl.lbl_frac1) + 5592047419392;
						 				lbl_update = 1;
						 			}
						 			if(lbl_update == 0){
						 				lbl.lbl_frac1 = parseInt(lbl.lbl_frac1) + 1297080123392;
						 			}
								}
					    		else if(det.modifier == "UF"){
									lbl.lbl_frac1 = parseInt(lbl.lbl_frac1) + 1292785156096;
								}
								var extfeatsAlone = true;// to check label is exterior feature only
								if(((det.comp != 0 && det.comp != '0') || (det.prefix != 0 && det.prefix != '0') || (det.constr != 0 && det.constr != '0') || (det.ext_cover != '' && det.ext_cover !='0') || (det.modifier != '')))
									extfeatsAlone = false;
								// area updating floor and skt_segment_label records
								flr.finish_living_areas = 0;	
								var tote_area = skd.fixedAreaTrue ? Math.round(skd.fixedAreaTrue) : Math.round(skd.areas[index]);
								if(fins_area && unfins_fin_area){
									lbl.finish_area = Math.round(fins_area);
									lbl.unfin_area = Math.round(unfins_fin_area);
									flr.finish_living_areas = Math.round(fins_area);
								}
								else if(extfeatsAlone){
									if(lvl_code == 2 || lvl_code == '2'){
										lbl.unfin_area = tote_area;
										lbl.finish_area = 0;
									}
									else if(lvl_code == 3 || lvl_code == '3'){
										lbl.unfin_area = 0;
										lbl.finish_area = tote_area;
									}
								}
								else {
									if (lvl_code != 1 && lvl_code != '1') {
										if (det.modifier == "Fin" || det.modifier == "") {
											lbl.finish_area = Math.round(tote_area); lbl.unfin_area = 0; flr.finish_living_areas = Math.round(tote_area);
											if (det.modifier == "Fin" && (det.modifierSign == '+' || det.modifierSign == '-') && det.comp.toString() == '220') {
												flr.finish_living_areas = 0;
											}
										}
										else if (det.modifier == "UF") {
											lbl.finish_area = 0; lbl.unfin_area = Math.round(tote_area); flr.finish_living_areas = 0;
										}
									}
									else if (det.comp.toString() == '1202' || det.comp.toString() == '202') {
										if (det.modifier == "Fin") {
											lbl.finish_area = tote_area; lbl.unfin_area = 0; flr.finish_living_areas = tote_area;
										}
										else if (det.modifier == "UF" || det.modifier == "") {
											lbl.finish_area = 0; lbl.unfin_area = tote_area; flr.finish_living_areas = 0;
										}
									}
									else if (det.comp.toString() == '220' && det.modifier == "Fin" && det.modifierSign == "" ) {
										flr.finish_living_areas = tote_area;
									}
									else if (det.comp.toString() == '206' && det.modifier != "Fin") {
										lbl.unfin_area = tote_area;
									}
								}
								
								var bgar_area = 0, bgar_label= false;
								if(det.comp.toString() == '206'){
								    bgar_area = tArea;
								    bgar_label = true;
								}
								
								var crawl_area = 0, crawl_label = false;
								if(det.comp.toString() == '203'){
								    crawl_area = Math.round(tArea);
								    crawl_label = true;
								}
								
								var bgar_capacity = null;
								if((lvl_code == '1' || lvl_code == 1) && (det.comp.toString() == '206') && (det.prefix.toString() == '11' || det.prefix.toString() == '12' || det.prefix.toString() == '13' || det.prefix.toString() == '14'))
								    bgar_capacity = ( det.prefix.toString() == '11'? '1.0' : ( det.prefix.toString() == '12'? '1.5': ( det.prefix.toString() == '13'? '2.0' : '2.5' ) ) );
								
								if((lvl_code == '1' || lvl_code == 1) && (det.comp.toString() == '206' || det.comp.toString() == '202' || det.comp.toString() == '1202')){
									if( det.modifier == "Fin" )
										floorBFinExists = true;
								}
								
								if(sTable == 'ccv_improvements_dwellings' || sTable == 'ccv_improvements_comm_bldgs'){
									if(sTable == 'ccv_improvements_dwellings'){
										if(det.comp.toString() == '202' || det.comp.toString() == '201' || det.comp.toString() == '206' || det.comp.toString() == '203' || det.comp.toString() == '1202'){
											var l_code = det.comp.toString() == '1202' ? '202': det.comp.toString();
											parentRecordProperties.push(_.clone({extRowuid: skrowid, improve_id: det.parent_imp_id, sour_table:sTable, record: impdwellcommRecord, level_code:lvl_code , fin_area: lbl.finish_area, unfin_area: lbl.unfin_area,Tarea: tArea, effperimeter: tperimeter, comp_idss : l_code, bgar_area: bgar_area, bgar_label: bgar_label, crawl_area: crawl_area, crawl_label: crawl_label, bgar_capacity: bgar_capacity}))
										}
										var dwellinsert = false;
										for(k in dwellingRecordArray){
											if(dwellingRecordArray[k].extension_row_id == skrowid && dwellingRecordArray[k].dc_table == sTable){
												dwellinsert = true;
												break;
											}
										}
										if(!dwellinsert)
											dwellingRecordArray.push(_.clone({extension_row_id: skrowid, dc_table:sTable}))
										var mInsert = false;
										if(Mtype){
											for(k in MHRecordArray){
												if(MHRecordArray[k].extension_rowu_id == skrowid && MHRecordArray[k].d_table == sTable){
													mInsert = true;
													break;
												}
											}
											if(!mInsert)
												MHRecordArray.push(_.clone({extension_rowu_id: skrowid, d_table:sTable}))
										}	
									}
									else{
										parentRecordProperties.push(_.clone({extRowuid: skrowid, improve_id: det.parent_imp_id, sour_table:sTable, record: impdwellcommRecord, level_code:lvl_code , fin_area: lbl.finish_area, unfin_area: lbl.unfin_area,Tarea: tArea, effperimeter: tperimeter, comp_idss : ''}))
										var comminsert = false;
										for(k in commRecordArray){
											if(commRecordArray[k].extension_row_id == skrowid && commRecordArray[k].dc_table == sTable){
												comminsert = true;
												break;
											}
										}
										if(!comminsert)
											commRecordArray.push(_.clone({extension_row_id: skrowid, dc_table:sTable}))
									}
								}
								
								var ext_featur_lkup = Proval_Lookup.filter(function(lkp){ return lkp.tbl_element == ext_code_mat && lkp.tbl_type_code == 'Exterior Feature'})[0];
				 				ext_featur_lkup = ext_featur_lkup? ext_featur_lkup: {};
								
								if(det.ext_feat_code && det.ext_feat_code != '0' && det.ext_feat_code != 0 && det.ext_feat_code != '' && ext_featur_lkup){
									var ext_table = ((sTable == 'ccv_improvements') ? 'ccv_improvements_ext_features': ((sTable == 'ccv_improvements_dwellings') ?'ccv_dwellings_ext_features': 'ccv_comm_bldgs_ext_features'));
									var mateValExt = '';
									var ext_field_value = ext_featur_lkup.field_2 ?ext_featur_lkup.field_2: '155';
									if(matValuess != '0' || matValuess != 0){
										matValuess = parseInt(matValuess);
										var baseValue = parseInt(parseInt(matValuess,10).toString(2));
										var j = 0;
										while(baseValue > 0){
											j = j + 1;
											var rem = baseValue % 10;
											if(rem > 0){
												switch(j){
													case 1: mateValExt = mateValExt + 'T'; break;
													case 2: mateValExt = mateValExt + 'R'; break;
													case 3: mateValExt = mateValExt + 'S'; break;
													case 4: mateValExt = mateValExt + 'W'; break;
												}
											}
											baseValue = parseInt(baseValue / 10);
										}
										ext_field_value = ext_field_value + '-' + mateValExt;
									}
									if(lvl_code == '3' || lvl_code == 3)
										ext_field_value = ext_field_value + '/';
									var pExtFeat = ProvalExteriorFeatureCounter.filter(function(es){return es.ext_rowuid == skrowid})[0];
									var recordICD = [];
									if(pExtFeat){
										if(sTable == 'ccv_improvements')
											recordICD = pExtFeat.ImPRecords.filter(function(es){return es.impRouidss == impdwellcommRecord.ROWUID})[0];		
										else
											recordICD = pExtFeat.CDwellsRecords.filter(function(es){return es.Rouidss == impdwellcommRecord.ROWUID})[0];
										if(recordICD){
											recordICD.extCounter = recordICD.extCounter + 1;
											lbl.extfeat_id = recordICD.extCounter;
											exteriorFeatureCreationArray.push(_.clone({extfeat_id: recordICD.extCounter, ext_feat_code: ext_field_value, area: tArea, ppRecord: impdwellcommRecord, ExtsourceTable: ext_table}));
										}
									}
								}
								
								if(det.ext_cover != '' && det.ext_cover != '0')
									flr.ext_cover1 = det.ext_cover;
								[prefix_lkup, comp_lkup, const_lkup, ext_feat_lkup].forEach(function(lkup){
									var other_val = evaluate_otherColumns(lkup, skt_type, tArea, det.floor_no, det.ns, impdwellcommRecord);
									other_val.forEach(function(ov) {
										if (lvl_code == '2' && ov.fieldName == "misc_imp_type" && ov.table == "ccv_improvements" && ov.value && ov.value != '') {
                                             let lbdetother = skd.labelDetails[index][3];
                                             if (lbdetother && lbdetother.ext_feat_code && lbdetother.ext_feat_code != '0')
                                                return;   
                                                        
                                        }
										if (lvl_code == '3' && ov.fieldName == "misc_imp_type" && ov.table == "ccv_improvements" && ov.value && ov.value != '')
                                            ov.value = ov.value + '/';
										if (ov.table == saveObj.floor_table) 
											flr.otherValues.push(ov);
										else {
											let inOtherval =  true; ov["tbl_type_code"] = lkup["tbl_type_code"];
											if (ov.table == "ccv_improvements" && lkup["tbl_type_code"] == "Exterior Feature" && otherValuesImpUpdates.filter(function(x) { return (x.fieldName == ov.fieldName && x.rowuid == ov.record.ROWUID) })[0]) inOtherval = false;
											let imp_id_other =  det.parent_imp_id && det.parent_imp_id.split('{').length > 1? det.parent_imp_id.split('{')[1].split('}')[0]: det.parent_imp_id; 
											if (inOtherval && ov.table == "ccv_improvements" && lkup["tbl_type_code"] == "Exterior Feature" && det.parent_type == 'imps' && obImprovementIds.filter((x) => { return x == imp_id_other })[0]) inOtherval = false;	
											
											if (inOtherval) {
												saveObj.otherValues = _.union(_.clone(saveObj.otherValues), ov);
												if (ov.table == "ccv_improvements")
													otherValuesImpUpdates.push(_.clone({ table: ov.table, fieldName: ov.fieldName, rowuid: (ov.record? ov.record.ROWUID: null) }));
											}
										}
									});
								});
						 
								if ((det.fin_area || det.ns || prefix_lkup[floor_key_col] || comp_lkup[floor_key_col] || const_lkup[floor_key_col]) && skt_type != 'Imp') {
									flr.fin_area = fins_area ? parseInt(fins_area) :(det.fin_area && !isNaN(det.fin_area)? Math.round(parseFloat(det.fin_area)): 0);
									if (det.prefix == 1 || det.prefix == 2 || det.prefix == 3) 
										flr_area =skd.fixedAreaTrue ? (parseFloat(prefix_lkup[floor_key_col]) * parseFloat(skd.fixedAreaTrue)): (parseFloat(prefix_lkup[floor_key_col]) * parseFloat(skd.areas[index]));
									else flr_area =skd.fixedAreaTrue ? parseFloat(skd.fixedAreaTrue) : parseFloat(skd.areas[index]);
									flr.area = Math.round(flr_area);
									flr.finish_living_areas = Math.round(flr.finish_living_areas);
									flr.perimeter =skd.fixedPerimeterTrue ? Math.round(parseFloat(skd.fixedPerimeterTrue)): Math.round(skd.perimeters[index]);
									flr.PAR = Math.round(parseFloat((flr.perimeter / flr.area) * 100));
									flr.extension = (segment_data['extension'] != ' ' && segment_data['extension'] != null )? segment_data['extension'] : skrowid;
									flr.extensionRowuid = skrowid;
									flr.dwelling_number = (skd.newRecord && segment_data[sTable][0])? segment_data[sTable][0].dwelling_number: (segment_data.dwelling_number ? segment_data.dwelling_number:'1' );
									flr.walkout_bsmt = det.walkout;
									flr.floor_recorddd.push(impdwellcommRecord);
									flr.lbl_lvl_code = lvl_code;
									flr.isSketchModified = skd.isModified ? skd.isModified : false;
									var f_key_col = (skt_type == 'Comm'? floor_key_cols: floor_key_col);
									if ((prefix_lkup && (prefix_lkup[f_key_col]) )|| det.ns) {
										flr.floorKey = ((prefix_lkup[f_key_col] || det.ns)).toString();
										if(skt_type =="Res"){
											if(lvl_code_values[lvl_code] == 'At' && det.prefix == '8' && (skd.labelDetails[index][3].ImpIdMethod != undefined && skd.labelDetails[index][3].ImpIdMethod != ''))
										    countflg = countflg+1;
										if(countflg>0 && lvl_code_values[lvl_code] == 'Above' && (det.prefix =='16' || det.prefix == '4')){
										    flr.floorKey = parseFloat(flr.floorKey) + 1;
									    }
										if(lvl_code_values[lvl_code] == 'At' && det.prefix == '10' && (skd.labelDetails[index][3].ImpIdMethod != undefined && skd.labelDetails[index][3].ImpIdMethod != '')){
											//if((det.prefix == '10' && flr.floorKey == 3) && (skd.labelDetails[index][3].prefix == 8 || skd.labelDetails[index][3].prefix == 9 || skd.labelDetails[index][3].prefix == 10)) 
											//flr.isTwoSFloor = true;
											flr.floorKey = skd.labelDetails[index][3].prefix == "6" ? flr.floorKey = parseFloat(flr.floorKey) + 1.5 :(skd.labelDetails[index][3].prefix == "16" ? flr.floorKey =parseFloat(flr.floorKey)+ .75 :(skd.labelDetails[index][3].prefix == "4" ? flr.floorKey =parseFloat(flr.floorKey)+ .5 : flr.floorKey =parseFloat(flr.floorKey)+ 1));
											nsflag =1;
										}
										if(lvl_code_values[lvl_code] == 'Above' && nsflag >0) flr.floorKey = '2.0';
										if(lvl_code_values[lvl_code] == 'Above' && (det.prefix == '6' || det.prefix == '7' || det.prefix == '8' || det.prefix == '9')){
										    flr.floorKey = parseFloat(flr.floorKey) + countflg +1;
											//if((skd.labelDetails[index][2].prefix == 10 && skd.labelDetails[index][2].ns == 3) && (det.prefix =='8' || det.prefix == '9' || det.prefix == '10'))
											//flr.isTwoSFloor = true;
									    }
									    if(countflg > 0 && lvl_code_values[lvl_code] == 'Above' && det.prefix == '5'){
										    flr.floorKey = parseFloat(flr.floorKey) + countflg +1;
										}

										if (lvl_code_values[lvl_code] == 'Above' && det.prefix == '10' && det.ns == '3' && (skd.labelDetails[index][2].prefix == '10' || skd.labelDetails[index][2].prefix == '5' || skd.labelDetails[index][2].prefix == '8' || skd.labelDetails[index][2].prefix == '' || skd.labelDetails[index][2].prefix == 0)) flr.floorKey = parseFloat(flr.floorKey) + 1;

										if (lvl_code_values[lvl_code] == 'At' && (det.prefix == '6' || det.prefix == '7' || det.prefix == '9') && skd.labelDetails[index][3].prefix == '10') {
											ignoreAbove = true;
											flr.floorKey = parseFloat(flr.floorKey);
											flr.isTwoSFloor = false;
										}

										if (lvl_code_values[lvl_code] == 'At' && (skd.labelDetails[index][3].prefix == '6' || skd.labelDetails[index][3].prefix == '7' || skd.labelDetails[index][3].prefix == '9') && det.prefix == '10') {
											ignoreAbove = true;
											let kblp = Proval_Lookup.filter(function (lkp) { return lkp.tbl_element == skd.labelDetails[index][3].prefix && lkp.tbl_type_code == 'Prefix' && lkp[grade_col] && lkp[grade_col].indexOf('Above') > -1 })[0];
											if (kblp && kblp[f_key_col]) {
												flr.floorKey = parseFloat(kblp[f_key_col]);
												flr.floorKey = parseFloat(flr.floorKey) + parseInt(det.ns);
												flr.fin_area = skd.labelDetails[index][3].fin_area && !isNaN(skd.labelDetails[index][3].fin_area) ? Math.round(parseFloat(skd.labelDetails[index][3].fin_area)) : 0;
												flr.finish_living_areas = Math.round(flr.fin_area);
												flr.isTwoSFloor = false;
											}
										}


									    if(lvl_code_values[lvl_code] == 'At' && (det.prefix == '6' || det.prefix == '9' || det.prefix == '7') && (skd.labelDetails[index][3].ImpIdMethod != undefined && skd.labelDetails[index][3].ImpIdMethod != '') && !ignoreAbove){
										    atflg = atflg+1;
										    if(det.prefix == '9') abvflg = 1;
										    flr.area = 2 * flr.area;
										    flr.isTwoSFloor = true;
									    }
									    if(lvl_code_values[lvl_code] == 'Above' && abvflg == 1)flr.floorKey = '2.0';
									    if(atflg >0 && abvflg ==0 && lvl_code_values[lvl_code] == 'Above')
									        flr.floorKey = '1.0';
										if(Number.isInteger(parseFloat(flr.floorKey))) flr.floorKey = parseFloat(flr.floorKey) + ".0";
										}
										if(skt_type == 'Comm'){
										    if(lvl_code_values[lvl_code] == 'At' && det.prefix == '8' && (skd.labelDetails[index][3].ImpIdMethod != undefined && skd.labelDetails[index][3].ImpIdMethod != ''))
										        countflg = countflg+1;
											if(countflg > 0 && lvl_code_values[lvl_code] == 'Above' && det.prefix == '5'){
										        flr.floorKey = parseFloat(flr.floorKey) + countflg +1;
									        }
									        if(lvl_code_values[lvl_code] == 'Above' && det.prefix == '8' && (skd.labelDetails[index][2].ImpIdMethod != undefined && skd.labelDetails[index][2].ImpIdMethod != '')){
										        flr.floorKey = parseFloat(flr.floorKey) + countflg +1;
									        }
									        if(lvl_code_values[lvl_code] == 'Above' && det.prefix == '5' && !(skd.labelDetails[index][2].ImpIdMethod != undefined && skd.labelDetails[index][2].ImpIdMethod != '')){
									        	flr.floorKey = '1.0';
									        	compflg = 1;
									        }
									            
											if(Number.isInteger(parseFloat(flr.floorKey))) flr.floorKey = parseFloat(flr.floorKey) + ".0";
										}
						 				var floorKeyExists = saveObj.floor_record ? saveObj.floor_record.filter(function(flrr){return ((flrr.floorKey == flr.floorKey) && (flrr.lbl_lvl_code == lvl_code))})[0]: null;
						 				if(lvl_code_values[lvl_code] == 'At' && skt_type == 'Res' && det.comp == 218) floorKeyExists =true;
						 				if(!floorKeyExists){
						 					if((flr.floorKey == '1.0' || flr.floorKey == '1') && (lvl_code == '3' || lvl_code == 3)){
						 						twosFloorTrue = true;
						 					}
											else if (!(ignoreAbove && lvl_code_values[lvl_code] == 'Above'))
						 						saveObj.floor_record.push(_.clone(flr));
						 				}
					 				}
					 				/*if(skt_type == 'Res' && lvl_code_values[lvl_code] == 'At' && (det.prefix == '10' && skd.labelDetails[index][2].ns == 3) && (skd.labelDetails[index][3].prefix == 8 || skd.labelDetails[index][3].prefix == 9 || skd.labelDetails[index][3].prefix == 10)){
					 					for(var i=1;i<3;i++){
					 						flr.floorKey = i+'.0';
					 						flr.isTwoSFloor = true;
					 						saveObj.floor_record.push(_.clone(flr));
					 					}
					 				}*/
					 				if(skt_type == 'Comm' && lvl_code_values[lvl_code] == 'Above' && det.prefix == '8' && !(skd.labelDetails[index][2].ImpIdMethod != undefined && skd.labelDetails[index][2].ImpIdMethod != '')){
									        	flr.floorKey = '1.0';
									        	saveObj.floor_record.push(_.clone(flr));
									        }
					 				if(skt_type == 'Res' && !comp_lkup[f_key_col] && abvflg == 1 && lvl_code_values[lvl_code] == 'Above'){
					 					    flr.floorKey = '1.0';
					 					    flr.area = Math.round(flr_area);
					 					    comp_lkup = undefined;
					 					    if(twosFloorTrue == false && !const_lkup[f_key_col]) twosFloorTrue =true;
					 				    }
									if (comp_lkup && comp_lkup[f_key_col]) {
										flr.floorKey = (comp_lkup[f_key_col]).toString();
										var floorKeyExists = saveObj.floor_record ? saveObj.floor_record.filter(function(flrr){return ((flrr.floorKey == flr.floorKey) && (flrr.lbl_lvl_code == lvl_code))})[0]: null;
						 				if (floorBFinExists && flr.isSketchModified)
						 					flr.floorBFinExists = floorBFinExists;
						 				if(!floorKeyExists){
						 					var inFloorRec = false, twoFloor = false;
						 					if(flr.floorKey == "1.0" || flr.floorKey == "1"){
						 						var floorKeyExistsLevel = saveObj.floor_record ? saveObj.floor_record.filter(function(flrr){return (flrr.lbl_lvl_code == lvl_code)})[0]: null;
						 						if(floorKeyExistsLevel){
						 							if (!isNaN(floorKeyExistsLevel.floorKey)) {
														if(parseFloat(floorKeyExistsLevel.floorKey) >= parseFloat(flr.floorKey)){
															inFloorRec = true;
														}
													}
						 						}
						 						if((lvl_code == '3' || lvl_code == 3)){
						 							twosFloorTrue = true; twoFloor = true;
						 						}
						 					}
											if (!inFloorRec && !twoFloor && !(ignoreAbove && lvl_code_values[lvl_code] == 'Above'))
						 						saveObj.floor_record.push(_.clone(flr));
						 				}
					 				}
									if (const_lkup && const_lkup[f_key_col]) {
										flr.floorKey = (const_lkup[f_key_col]).toString();
										var floorKeyExists = saveObj.floor_record ? saveObj.floor_record.filter(function(flrr){return ((flrr.floorKey == flr.floorKey) && (flrr.lbl_lvl_code == lvl_code))})[0]: null;
						 				if(!floorKeyExists){
						 					var inFloorRec = false, twoFloor = false;
						 					if(flr.floorKey == "1.0" || flr.floorKey == "1"){
						 						var floorKeyExistsLevel = saveObj.floor_record ? saveObj.floor_record.filter(function(flrr){return (flrr.lbl_lvl_code == lvl_code)})[0]: null;
						 						if(floorKeyExistsLevel){
						 							if (!isNaN(floorKeyExistsLevel.floorKey)) {
														if(parseFloat(floorKeyExistsLevel.floorKey) >= parseFloat(flr.floorKey)){
															inFloorRec = true;
														}
													}
						 						}
						 						if((lvl_code == '3' || lvl_code == 3)){
						 							twosFloorTrue = true; twoFloor = true;
						 						}
						 					}
											if (!inFloorRec && !twoFloor && !(ignoreAbove && lvl_code_values[lvl_code] == 'Above'))
						 						saveObj.floor_record.push(_.clone(flr));
						 				}
					 				}	
								}
								if(det.floor_no) {
									saveObj.floor_record.forEach(function(flr){
										flr.floor_no = det.floor_no;
									});
								}
								
								if(twosFloorTrue){
									if(atflg == 0 && compflg == 0)flr.floorKey = '2.0';
									flr.isTwoSFloor = true;
									saveObj.floor_record.push(_.clone(flr));
								}
				 				if (skd.isModified) saveObj.labels.push(_.clone(lbl));
				 			}
				 		}
					});

					if (skd.isModified) {	
						var modiArea = skd.fixedAreaTrue ? Math.round(skd.fixedAreaTrue) : Math.round(skd.areas[index]);
						let nn = sk_vector?.startNode ? sk_vector.startNode.nextNode : null;

				  		if(points.length > 1) {
				  			var splitPoints = points[1].split(/\s/);
				  			splitPoints.forEach(function(p, i) {
					    		if ( p.trim() != '' && p.trim() != 'P2' ){
					       			var rr = convertStrToPoint( p );
					        		var ch = 0, dimPosition = 0;;
					        		if ( rr.indexOf( '/' ) > -1)  {
					        			var st = rr;
					            		rr = st.split( '/' )[0];
					            		ch = st.split( '/' )[1];
									}

					        		var imptid = skd.newRecord ? segvectImprovementId : segment_data['improvement_id'];
					        		var p2 = splitPoints[i + 1] ? splitPoints[i + 1] : splitPoints[0];
					        		var p3 = splitPoints[i + 2] ? splitPoints[i + 2] : (splitPoints[i + 1]) ? splitPoints[1] : splitPoints[2];
									dimPosition = dimensionConversion(p, p2, p3, modiArea, parseFloat(scaleVal));
									let pl_type = nn?.lineStrokePattern ? nn?.lineStrokePattern : '0', pf_level = '0', pn_floors = '0';
									if (nn?.proval_line_type) {
										let pl_ts = nn.proval_line_type.split('~');
										pf_level = pl_ts[1] ? pl_ts[1] : '0';
										pn_floors = pl_ts[2] ? pl_ts[2] : '0';
									}
									nn = nn && nn.nextNode;
									saveObj.vects.push({ curve_height: ch, run_rise: rr, seq_number: (i + 1), CC_segment_id: seg_id, extension: (segment_data['extension'] != ' ' && segment_data['extension'] != null) ? segment_data['extension'] : skrowid, improvement_id: imptid, status: ((window.opener ? window.opener.showFutureData : showFutureData) ? 'F' : 'A'), eff_year: '0', line_type: pl_type, area: Math.round(skd.areas[index]), perimeter: Math.round(skd.perimeters[index]), vector: ((convertStrToPoint(p, true) * 10) << 16), dimension_font_size: '8', dimension_position: dimPosition, floor_level: pf_level, number_floors: pn_floors });
					    		}
							});
						}

						if(Mtype && (MH_dwellRecord.length > 0) && MH_dwellRecord[0]){
							var imp_width = 0 ,imp_length = 0,hLength = 0,RLength = 0, improvement_ide = 'M' , imp_typed = 'MHOME', lT=0,uT=0; 
							points.length > 1 && points[1].split(/\s/).forEach(function(p, i) {
							 	if ( p.trim() != '' ){
									if(p.split("/").length > 1)
										return;
									else{
										if((p.split("L").length > 1 || p.split("R").length > 1) && lT == 0 ){
											hLength = p.split("L").length > 1 ? parseFloat(p.split("L")[1]) : parseFloat(p.split("R")[1]);
											lT = 1;
										}
										else if((p.split("U").length > 1 || p.split("D").length > 1) && uT == 0 ){
											RLength = p.split("U").length > 1 ? parseFloat(p.split("U")[1]) : parseFloat(p.split("D")[1]);
											uT =1;
										}	
									}

							 	}
							});
							if(hLength > RLength) {
								imp_length = hLength;
								imp_width = RLength;
							}
							else{
								imp_length = RLength;
								imp_width = hLength;
							}
							imp_length = Math.round(imp_length);
							imp_width = Math.round(imp_width);
							saveObj.MHOMEValues.push(  {iMDrecord: MH_dwellRecord[0], improvement_id: improvement_ide, imp_type: imp_typed ,imp_width :imp_width, imp_length: imp_length, mh_length:imp_length, 
mh_width: imp_width, mTable: mTable});
							saveObj.MHOMEValuesTrue = true;
						}
						if (extensions.indexOf(segment_data['extension']) == -1) extensions.push(segment_data['extension'])
						saveObj.extension = (segment_data['extension'] != ' ' && segment_data['extension'] != null ) ? segment_data['extension'] : skrowid;
						saveObj.improvement_id = skd.newRecord ? segvectImprovementId : segment_data['improvement_id'];
						saveObj.label_position = ProvalLabelPositionFromString(skd.labelPositions[index])
						var pos = skd.labelPositions[index];
						pos.y=( pos.y - ( (parseFloat(scaleVal) > 150) ? (((skd.label.split('|').length)==1)? 6: (skd.label.split('|').length* 4.5 ))  : (((skd.label.split('|').length)==1)? 4: (skd.label.split('|').length* 3)) ) );
						saveObj.displayed_area_position = ProvalLabelPositionFromString(pos);
						if (vectorString && vectorString.trim().length > 0) {
							saveObj.parentRowid = segment_data['ParentROWUID'];
							saveObj.area = skd.fixedAreaTrue ? Math.round(skd.fixedAreaTrue) : Math.round(skd.areas[index]);
							saveObj.perimeter = skd.fixedPerimeterTrue ? Math.round(skd.fixedPerimeterTrue): Math.round(skd.perimeters[index]);
							saveObj.eff_year = '0';
							saveObj.CC_segment_id = seg_id;
						}
						if (!skd.newRecord) {
							deleteData.push({ source: saveObj.vect_tbl, parent: segment_data });
							deleteData.push({ source: saveObj.lbl_table, parent: segment_data });
							if (vectorString && vectorString.trim().length > 0) 
								savedata.push(_.clone(saveObj));
						}
						else 
							newRecords.push({ catId: getCategoryFromSourceTable(sketch_source_name).Id, parentData: segment_data, saveObj: _.clone(saveObj), CC_segment_id: seg_id, extension: saveObj.extension, improvement_id: saveObj.improvement_id, sourceTable: sketch_source_name });
					}
					else {
						var seg_datat = parcel[sketch_source_name].filter(function(sg_dt){ return sg_dt.ROWUID == saveObj.rowId })[0];
						savedata.push({ isSegmentIdOnly: true, CC_segment_id: seg_id.toString(), rowId: saveObj.rowId, seg_tbl: sketch_source_name, vect_tbl: saveObj.vect_tbl, lbl_tbl: saveObj.lbl_table,floor_Records:saveObj.floor_record,floor_Table:saveObj.floor_table, segmentRec: seg_datat });
					}
				});
			}
		});		
	});
	
	var _dnp = _.uniq( noteData.map(function(nd){ return nd.parentId }) );
	var _dnpArray = _dnp.filter(function(dp) {
  		return sketchRowuids.indexOf( dp.toString() ) == -1;
	});	
	
	noteData.forEach(function(nt) {
		var note_rec = parcel[ConfigTables.SktNote].filter(function(nt_rec) { return nt_rec.ROWUID == nt.noteid })[0];
		if (nt.noteText == '*DEL*') 
			deleteNoteData.push(nt.noteid);
		else {
			if (nt.newRecord) {
				var parentData = sketchApp.sketches.filter(function(sk){ return sk.sid == nt.parentId })[0].parentRow;
				newRecords.push({ newAuxRowuid: nt.clientId, doNotUpdate: true, catId: getCategoryFromSourceTable(ConfigTables.SktNote).Id, parentData: parentData, sourceTable: ConfigTables.SktNote})
			}
			var xPos = nt.xPosition, yPos = nt.yPosition, pos = 0;
			pos = parseFloat((xPos > 0)? convertStrToPoint('R' + xPos): convertStrToPoint('L' +(parseFloat(xPos.toString().split("-")[1]))));
			pos += parseFloat((yPos < 0)? convertStrToPoint('D' + (parseFloat(yPos.toString().split("-")[1]))): convertStrToPoint('U' + yPos));
			saveNoteData.push({ position: pos, text: nt.noteText, rowid: (nt.newRecord? nt.clientId: nt.noteid), parentData: parentData });
		}
	});
	
	_dnpArray.forEach(function(dnp) {
		var _sktRec = sketchApp.sketches.filter( function ( sBuild ) { return sBuild.uid == dnp } )[0];		
		var _exRec = _sktRec.parentRow;
		var _sf = getDataField('scale', ConfigTables.SktHeader);
		if ( _exRec[ConfigTables.SktHeader][0] ) {
			scaleVal =  ( _sktRec.boundaryScale? _sktRec.boundaryScale: ( ( _exRec[ConfigTables.SktHeader][0]['scale'] && _exRec[ConfigTables.SktHeader][0]['scale'] != '0' ) ? _exRec[ConfigTables.SktHeader][0]['scale'] : ( ( _exRec.RorC == 'R' ) ? '80' : '200' ) ) );
			saveData += '^^^' + parcel.Id + '|' + _exRec[ConfigTables.SktHeader][0].ROWUID + '|' + _sf.Id + '||' + scaleVal + '|edit|' + _exRec[ConfigTables.SktHeader][0].ParentROWUID + '\n';
		}
	});
	
	while(sketchApp.deletedVectors.length > 0) {
		var vector = sketchApp.deletedVectors.pop();
		var extension_rowid = vector.uid;
		var ext_length = extension_rowid.split('/').length;
		if(ext_length <= 1 && vector.sketchType != 'outBuilding'){
			var seg_table = ConfigTables.SktSegment;
			var seg_data = parcel[seg_table].filter(function(r){ return r.ROWUID == extension_rowid })[0];
			var extension_id = seg_data.ParentROWUID;
			var par_data = parcel['ccv_extensions'].filter(function(pdata){ return pdata.ROWUID == extension_id })[0];
			var fl_tab = (par_data && par_data.RorC == "R") ? (ConfigTables.res_floor) : (ConfigTables.comm_floors);
			var copy_par_data = copy_parentData.filter(function(pd){ return pd.tbl == 'ccv_extensions' && pd.rowid == extension_id })
			if (copy_par_data.length == 0){
				copy_parentData.push({ tbl: 'ccv_extensions', rowid: extension_id, seg_tbl: seg_table, lbl_tbl: ConfigTables.SktSegmentLabel, flr_tbl: fl_tab, ext_feature_tbl: '', otherfields: [] })
			}
			
			if(vector.labelFields[0] && vector.labelFields[0].labelDetails){
				if(vector.labelFields[0].labelDetails.details){
					details = vector.labelFields[0].labelDetails.details;
					var extfeatAlone = false;
					var lvl_code_values = {1: 'Below', 2: 'At', 3: 'Above'};
					var f_key_col = fl_tab.indexOf('res') > -1 ? 'FloorKey': 'CommFloorNumber';
					var skt_type = fl_tab.indexOf('res') > -1 ? 'Res' : 'Comm';
					var grade_col = skt_type + 'GradeLevel';
					
					$.each(details,function(det){
						var lbfeat = details[det];
						
						if( (det == 1 || det == '1') && lbfeat.comp1 &&  lbfeat.prefix1){
							var comp_Array = [{comp: lbfeat.comp1, mod: lbfeat.modifier1}, {comp: lbfeat.comp2, mod: lbfeat.modifier2}, {comp: lbfeat.comp3, mod: lbfeat.modifier3}, {comp: lbfeat.comp4, mod: lbfeat.modifier4}];
							comp_Array.forEach(function(com_arr){
								var comp_lkup = Proval_Lookup.filter(function(lkp){ return lkp.tbl_element == lbfeat.comp && lkp.tbl_type_code == 'Component' && lkp[grade_col] && lkp[grade_col].indexOf(lvl_code_values[det]) > -1 })[0];
				 				comp_lkup = comp_lkup? comp_lkup: {}
				 				
				 				if (comp_lkup && comp_lkup[f_key_col]) {
				 					var floorBFinExists = false;
									if((com_arr.comp.toString() == '206' || com_arr.comp.toString() == '202' || com_arr.comp.toString() == '1202')){
										if( com_arr.mod == "Fin" )
											floorBFinExists = true;
									}
									var floorKey = (comp_lkup[f_key_col]).toString();
									deletedSketchFloorRecords.push({floorKey: floorKey, floorBFinExists: floorBFinExists});
								}
							});
						}
						else {
							var prefix_lkup = Proval_Lookup.filter(function(lkp){ return lkp.tbl_element == lbfeat.prefix && lkp.tbl_type_code == 'Prefix' && lkp[grade_col] && lkp[grade_col].indexOf(lvl_code_values[det]) > -1 })[0];
				 				prefix_lkup = prefix_lkup? prefix_lkup: {}
				 		
				 			var comp_lkup = Proval_Lookup.filter(function(lkp){ return lkp.tbl_element == lbfeat.comp && lkp.tbl_type_code == 'Component' && lkp[grade_col] && lkp[grade_col].indexOf(lvl_code_values[det]) > -1 })[0];
				 				comp_lkup = comp_lkup? comp_lkup: {}
				 				
				 			if ((prefix_lkup && (prefix_lkup[f_key_col]) )|| lbfeat.ns) {
								var floorKey = ((prefix_lkup[f_key_col] || lbfeat.ns)).toString();
								if(floorKey == '1.0' && (det == 3 || det == '3'))
									floorKey = '2.0';
								if (!isNaN(floorKey) && parseFloat(floorKey) > 1) {
									var st_val = (det == 2 || det == '2') ? 1: 2;
									for(i = st_val; i < parseFloat(floorKey); i++) {
										var flo_Key = (fl_tab.indexOf('res') > -1) ? (((i).toFixed(1)).toString()) : ((i).toString());
										deletedSketchFloorRecords.push({floorKey: flo_Key});
									}
								}
								floorKey = floorKey.toString();
								deletedSketchFloorRecords.push({floorKey: floorKey});
							}
							if (comp_lkup && comp_lkup[f_key_col]) {
								var floorKey = (comp_lkup[f_key_col]).toString();
								var floorBFinExists = false;
								if((det == '1' || det == 1) && (lbfeat.comp.toString() == '206' || lbfeat.comp.toString() == '202'|| lbfeat.comp.toString() == '1202')){
									if( lbfeat.modifier == "Fin" )
										floorBFinExists = true;
								}
								if(floorKey == '1.0' && (det == 3 || det == '3'))
									floorKey = '2.0';
								if (!isNaN(floorKey) && parseFloat(floorKey) > 1) {
									var st_val = (det == 2 || det == '2') ? 1: 2;
									for(i = st_val; i < parseFloat(floorKey); i++) {
										var flo_Key = (fl_tab.indexOf('res') > -1) ? (((i).toFixed(1)).toString()) : ((i).toString());
										deletedSketchFloorRecords.push({floorKey: flo_Key});
									}
								}
								floorKey = floorKey.toString();
								deletedSketchFloorRecords.push({floorKey: floorKey, floorBFinExists: floorBFinExists});
							}
						}
						
						if(((lbfeat.comp != 0 && lbfeat.comp != '0') || (lbfeat.prefix != 0 && lbfeat.prefix != '0') || (lbfeat.constr && lbfeat.constr != 0 && lbfeat.constr != '0') || (lbfeat.modifier && lbfeat.modifier != '')))
							extfeatAlone = true;
					});	
					if(!extfeatAlone){
						var imprr_id = '';
						if(!vector.doNotDeleteImp){
							$.each(details,function(det){
								var lbfeat = details[det];
								if(lbfeat.parent_type != '' && lbfeat.parent_type != null && lbfeat.parent_type != ' '){
									if(imprr_id == '' && lbfeat.parent_type == "imps"){
										imprr_id = lbfeat.parent_imp_id;
										if(imprr_id){
											if(imprr_id.split("{").length > 1)
												imprr_id = imprr_id.split("{")[1].split("}")[0];
											var ICDRecord = parcel['ccv_improvements'].filter(function(pdatas){ return ((pdatas.ROWUID == imprr_id || pdatas.improvement_id == imprr_id) && pdatas.ParentROWUID == extension_id)})[0];
											if(ICDRecord && ICDRecord.attached_flag == 'N')
												deleteData.push({ source: 'ccv_improvements', parent: ICDRecord, isICDRecord: true});
										}
									}
									else{
										var ptabel, sk_len = true;
										imprr_id = lbfeat.parent_imp_id;
										if( lbfeat.parent_type == 'dwellings' ) {
											ptabel = 'ccv_improvements_dwellings';
											if(dwellingRecordArray.length == 0)
												sk_len = false;
										}
										else if(lbfeat.parent_type == 'comm_bldgs') {
											ptabel = 'ccv_improvements_comm_bldgs';
											if(commRecordArray.length == 0)
												sk_len = false;
										}
										if(imprr_id && !sk_len){
											if(imprr_id.split("{").length > 1)
												imprr_id = imprr_id.split("{")[1].split("}")[0];
											var ICDRecord = parcel[ptabel].filter(function(pdatas){ return ((pdatas.ROWUID == imprr_id || pdatas.improvement_id == imprr_id) && pdatas.ParentROWUID == extension_id)})[0];
											if(ICDRecord)
												deleteData.push({ source: ptabel, parent: ICDRecord, isICDRecord: true});
										}  
									}
								}
							});
						}						
					}
					else{					
						$.each(details,function(det){
							var ptabel = '', imp_id , insertDeleteData = false, mhdeletedata = false, mhenter = false, mhRecodd = [];
							if(details[det].parent_type != '' && details[det].parent_type != null && details[det].parent_type != ' '){
								if(details[det].parent_type == 'dwellings'){
									ptabel = 'ccv_improvements_dwellings';
									dwellingRecordArray.forEach(function(dw){
										if(dw.extension_row_id == extension_id &&  dw.dc_table == ptabel)
											insertDeleteData = true;
									});
									if(details[det].comp == "217" || details[det].comp == "216" ){
										mhenter = true;
										MHRecordArray.forEach(function(mw){
											if(mw.extension_rowu_id == extension_id &&  mw.d_table == ptabel)
												mhdeletedata = true;
										});
									}
						
								}
								else if(details[det].parent_type == 'comm_bldgs'){
									ptabel = 'ccv_improvements_comm_bldgs';
									commRecordArray.forEach(function(cw){
										if(cw.extension_row_id == extension_id &&  cw.dc_table == ptabel)
											insertDeleteData = true;
									});
								}
								else
									ptabel = 'ccv_improvements';
								imp_id = details[det].parent_imp_id;
								if(imp_id){
									if(imp_id.split("{").length > 1)
										imp_id = imp_id.split("{")[1].split("}")[0];
									var ICDRecord = parcel[ptabel].filter(function(pdatas){ return ((pdatas.ROWUID == imp_id || pdatas.improvement_id == imp_id) && pdatas.ParentROWUID == extension_id)})[0];
								}
								if(!insertDeleteData && ICDRecord)
									deleteData.push({ source: ptabel, parent: ICDRecord, isICDRecord: true});
								if(mhenter && !mhdeletedata && mhRecodd) {
									var mhImpRecord = parcel['ccv_improvements'].filter(function(mhImp) { return ( mhImp.ParentROWUID == extension_id && mhImp.improvement_id == 'M' && mhImp.imp_type == 'MHOME' ) })[0];
									if (mhImpRecord)
                                        deleteData.push({ source: 'ccv_improvements', parent: mhImpRecord, isICDRecord: true});
                                        
									mhRecodd = (par_data[ConfigTables.manuf_housing].length > 0) ? par_data[ConfigTables.manuf_housing][0] : [];
									deleteData.push({ source: ConfigTables.manuf_housing, parent: mhRecodd, isMHRecord: true});
								}
							}						
						});
					}
				}
			}
			deleteData.push({ source: seg_table, parent: seg_data, isCurrentRecord: true, vector_tbl: ConfigTables.SktVector, lbl_tbl: ConfigTables.SktSegmentLabel });
		}
		if(ext_length <= 1 && vector.sketchType == 'outBuilding'){
			var seg_table = ConfigTables.SktOutbuilding;
			var ptabel = 'ccv_improvements';
			var seg_data = parcel[seg_table].filter(function(r){ return r.ROWUID == vector.uid })[0];
			var imp_id = seg_data.improvement_id;
			var extension_id = seg_data.ParentROWUID;
			if(imp_id && imp_id.split('{').length > 1)
				imp_id = imp_id.split('{')[1].split('}')[0];
			var imp_IdOB = (imp_id && !(isNaN(imp_id)))? parseInt(imp_id): imp_id;
			var ICDRecord = parcel[ptabel].filter(function(pdatas){ return ((pdatas.ROWUID == imp_IdOB)|| (pdatas.improvement_id == imp_IdOB))  && (pdatas.ParentROWUID == extension_id) && pdatas.CC_Deleted != true})[0];
			if (ICDRecord) deleteData.push({source: ptabel, parent: ICDRecord, isCurrentOutBD_par: true});
			deleteData.push({source: seg_table, parent: seg_data, isCurrentOutBD: true, vector_tbl: ConfigTables.SktVector_outbuilding});
		}
	}
	extensionRecordsArray.forEach(function(extRe){
		var ext_ID = extRe.ext_rowuid;
		var RRorCC = extRe.ReComm;
		var extension_RRecord = parcel['ccv_extensions'].filter(function(pdata){ return pdata.ROWUID == ext_ID })[0];
		extension_RRecord['ccv_improvements'].forEach(function(imm){
			if(imm.CC_Deleted != true)
				imm['ccv_improvements_ext_features'].forEach(function(imExt){
					if(imExt.CC_Deleted != true)
						deleteData.push({source: 'ccv_improvements_ext_features', parent: imExt, isextRecord: true});
				});
		});
		if(RRorCC == 'R'){
			extension_RRecord['ccv_improvements_dwellings'].forEach(function(dwe){
				if(dwe.CC_Deleted != true)
					dwe['ccv_dwellings_ext_features'].forEach(function(deExt){
						if(deExt.CC_Deleted != true)
							deleteData.push({source: 'ccv_dwellings_ext_features', parent: deExt, isextRecord: true});
					});
			});
		}
		else{
			extension_RRecord['ccv_improvements_comm_bldgs'].forEach(function(comd){
				if(comd.CC_Deleted != true)
					comd['ccv_comm_bldgs_ext_features'].forEach(function(comExt){
						if(comExt.CC_Deleted != true)
							deleteData.push({source: 'ccv_comm_bldgs_ext_features', parent: comExt, isextRecord: true});
				    });
			});
		}
	});
	console.log('savedata: ', savedata);
	console.log('deleteData: ', deleteData);
	deleteRecs(deleteData);
}

function ProvalSketchBeforeSaveNew(sketches, callback) {
	var OutB_Lookups=lookup?lookup:(window.opener?window.opener.lookup:[]);
	parcel = window.opener? window.opener.activeParcel: activeParcel;
	var ConfigTables = window.opener? window.opener.ccma.Sketching.Config.ConfigTables : ccma.Sketching.Config.ConfigTables;
	extension_Recd_Array = []; saveData = '';
	var Proval_sketch_codes = window.opener ? window.opener.Proval_sketch_codes : [];
	var Proval_Lookup=window.opener?window.opener.Proval_Lookup:[];
	var Proval_Default_Exterior_Cover = window.opener ? window.opener.Proval_Default_Exterior_Cover : [];
	var getDataField = window.opener? window.opener.getDataField: getDataField;
    var getCategoryFromSourceTable = window.opener? window.opener.getCategoryFromSourceTable: getCategoryFromSourceTable;
	var labels = [], lookupVal,BasementFunctionalitys = [], level_code = {'1': 'Below', '2': 'At', '3': 'Above'}, skt_type = '', var_Check = false, sketch_test = [], deleteVect_Ext_Feat_Array = [], delete_Ext_Feat_window_show = [];
	function sketchsavee(){
		$('.ccse-canvas').removeClass('dimmer');
        $('.dimmer').hide();
		sketches.filter(function(skda){ return skda.isModified }).forEach(function(skd){
			skd.vectors.filter(function(vect){ return (vect.isModified && vect.sketchType != 'outBuilding'); }).forEach(function(vector){
				if( vector.vectorString != '' && vector.vectorString != null && vector.vectorString != ' ' && ( vector.newRecord || ( parseFloat(vector.sktSegmentArea) != vector.area() ) ) ){
					if(vector.BasementFunctionality){
						if(vector.labelFields[0] && vector.labelFields[0].labelDetails){
							var B_area = vector.fixedAreaTrue ? parseInt(vector.fixedAreaTrue) : parseInt(vector.area())
							var sskt_type = ((vector.labelFields[0].labelDetails.details[1].parent_type == 'dwellings') ? 'Res' : (vector.labelFields[0].labelDetails.details[1].parent_type == 'comm_bldgs') ? 'Comm' : 'Imp');
							BasementFunctionalitys.push({ labelValue: vector.labelFields[0].labelDetails.labelValue, sid: skd.sid, sketch: vector, vect_uid: vector.uid, screen: 'BGAreaEntry', lvl_code: '1', labelDetails: vector.labelFields[0].labelDetails, Area: parseInt(B_area),skt_type: sskt_type});
							$.each(vector.labelFields[0].labelDetails.details, function(lvl_code, details) {
								if(lvl_code != '1' && lvl_code != 1){
									$.each(details, function(type, value) {
										var skt_type = ((details.parent_type == 'dwellings') ? 'Res' : (details.parent_type == 'comm_bldgs') ? 'Comm' : 'Imp');
										if (value != 0) {
											lookupVal = Proval_Lookup.filter(function(lk){ return lk.tbl_element == value && lk.AdditionalScreen && lk[skt_type + 'GradeLevel'] && lk[skt_type + 'GradeLevel'].indexOf(level_code[lvl_code]) > -1; });
											if (lookupVal.length > 0){
												var addScreen = '';
												if(lookupVal[0].AdditionalScreen && lookupVal[0].AdditionalScreen.split("/").length > 1)
													addScreen = lookupVal[0].AdditionalScreen.split("/")[0];
												else
													addScreen = lookupVal[0].AdditionalScreen;
												var areaEnt = true;
												if(addScreen == 'AreaEntry' &&  lookupVal[0].tbl_type_code =="Component")
													areaEnt = (details['modifier'] == "Fin" || details['modifier'] == "UF") ? true: false;
												if(areaEnt)
													labels.push({ lookupDetail: lookupVal[0], sid: skd.sid, sketch: vector, screen: addScreen, lvl_code: lvl_code });
											}
										}
									});
								}
							});
						}	
					}
					else{
						if(vector.labelFields[0] && vector.labelFields[0].labelDetails){
							$.each(vector.labelFields[0].labelDetails.details, function(lvl_code, details) {
								$.each(details, function(type, value) {
									var skt_type = ((details.parent_type == 'dwellings') ? 'Res' : (details.parent_type == 'comm_bldgs') ? 'Comm' : 'Imp');
									if (value != 0) {
										lookupVal = Proval_Lookup.filter(function(lk){ return lk.tbl_element == value && lk.AdditionalScreen && lk[skt_type + 'GradeLevel'] && lk[skt_type + 'GradeLevel'].indexOf(level_code[lvl_code]) > -1; });
										if (lookupVal.length > 0){
											var addScreens = '';
											if(lookupVal[0].AdditionalScreen && lookupVal[0].AdditionalScreen.split("/").length > 1)
												addScreens = ((lookupVal[0].AdditionalScreen.split("/")[0] != "BGAreaEntry") ? lookupVal[0].AdditionalScreen.split("/")[0] : lookupVal[0].AdditionalScreen.split("/")[1]);
											else
												addScreens = lookupVal[0].AdditionalScreen;
											var areaEnt = true;
											if(addScreens == 'AreaEntry' &&  lookupVal[0].tbl_type_code =="Component")
												areaEnt = (details['modifier'] == "Fin" || details['modifier'] == "UF") ? true: false;
											if(areaEnt)
												labels.push({ lookupDetail: lookupVal[0], sid: skd.sid, sketch: vector, screen: addScreens, lvl_code: lvl_code });
										}
									}
								});
							});
						}
					}
				}
			});
		});
	function nBasementFunctionality(){
		if (labels.length > 0) {
	 		$('.mask').show();	
	 		$('.Current_vector_details').show();
	 		$('.outbdiv').hide();
	 		$('.skNote').html('');
	 		var cancelCallback = function(isChecked) {
	 			isChecked = !isChecked? false: true;
	       		$('.Current_vector_details').hide();
	        	$('.mask').hide();
	        	//hideKeyboard();
	 			callback(isChecked, isChecked);
	        	return false;
	 		}
	 	
	 		var checkAreaEntry = function(){
		 		var area_entry_lbl = labels.filter(function(lbl){ return lbl.screen == 'AreaEntry' });
		 		if (area_entry_lbl.length > 0) {
		 			$('.Current_vector_details .head').html('Area Entry');
		 			var table = '<table class="area_entry"><tr>';
		 			table += '<th rowspan="2">Label</th><th rowspan="2">Area</th><th colspan="2">Finished Area</th><th  rowspan="2">Unfinished Area</th></tr>';
		 			table += '<tr><th>Value</th><th>%</th></tr>';
					area_entry_lbl.forEach(function (label) {
						let ska = label.sketch.fixedAreaTrue ? parseInt(label.sketch.fixedAreaTrue) : label.sketch.area();
						table += '<tr class="trow" sid="' + label.sid + '" vid="' + label.sketch.uid + '" lbl="' + label.lookupDetail.tbl_element_desc + '" lvl_code="' + label.lvl_code + '" type="' + label.lookupDetail.tbl_type_code + '" area = "' + ska + '">'
						table += '<td>' + label.lookupDetail.tbl_element_desc + '</td><td>' + ska + '</td>';
						table += '<td><input type="number" onkeydown="return (event.keyCode == 69 || event.keyCode == 107 || event.keyCode == 109 || event.keyCode == 187 || event.keyCode == 189) ? false : true" class="fin_area fin_area_value" /></td>';
						table += '<td><input type="number" class="fin_area fin_area_perc" /></td>';
						table += '<td style="text-align: center;"><span class="unfin_area">' + ska + '</spin></td></tr>';
			 		});
			 		$('.Current_vector_details .dynamic_prop').html(table + '</table>');
			 		$('.fin_area').css({'width': '90px', 'height': '20px'})
			 		$('.fin_area').unbind();
			 		$('.fin_area').bind('keyup', function(){
			 			var that = this;
			 			var thisVal = $(that).val();
		 				thisVal = thisVal && !isNaN(thisVal)? parseInt(thisVal): 0;
		 				if(isNaN(thisVal))
		 					thisVal=0;
		 				var trow = $(that).parents('.trow')
		 				var area = $(trow).attr('area')
		 				area = area && !isNaN(area)? parseInt(area): 0;
			 			if ($(that).hasClass('fin_area_value')) {
			 				if (thisVal > area) {
			 					$(that).val($(that).val().substring(0,$(that).val().length-1))
			 					return false;
		 					}
		 					else {
		 						var perc = Math.round((thisVal/area) * 100);
		 						$('.fin_area_perc', trow).val(perc);
		 						$('.unfin_area', trow).html(Math.round(area - thisVal));
		 					}
			 			}
							if ($(that).hasClass('fin_area_perc')) {
								if (thisVal > 100) {
									$(that).val($(that).val().substring(0, $(that).val().length - 1))
									return false;
								}
								else {
									var value = (area * thisVal) / 100;
									$('.fin_area_value', trow).val(Math.round(value));
									$('.unfin_area', trow).html(Math.round(area - value));
								}
							}
						});


			   		$('#Btn_Save_vector_properties').unbind(touchClickEvent);
			    	$('#Btn_Save_vector_properties').bind(touchClickEvent, function () {
			        	var sid, vid, lvl_code, type, lookupVal;
			        	$('.trow').each(function(index, trow){
			        		sid = $(trow).attr('sid');
			        		vid = $(trow).attr('vid');
			        		lvl_code = $(trow).attr('lvl_code');
			        		type = $(trow).attr('type');
			        		var t_areas = $(trow).attr('area');
			        		var f_areas = $('.fin_area_value', $(trow)).val();
			        		f_areas = (f_areas && f_areas != "") ? f_areas : '0';
			        		var u_areas = (parseFloat(t_areas) - parseFloat(f_areas)).toString();
			        		sketchApp.sketches.filter(function(sk){ return sk.sid == sid })[0].vectors.filter(function(vect){ return vect.uid == vid })[0].labelFields[0].labelDetails.details[lvl_code].fin_area = f_areas;
			        		sketchApp.sketches.filter(function(sk){ return sk.sid == sid })[0].vectors.filter(function(vect){ return vect.uid == vid })[0].labelFields[0].labelDetails.details[lvl_code].unfin_fin_area = u_areas;
			        	})
			        	$('.Current_vector_details').hide();
			        	$('.mask').hide();
			    		if (callback) callback(true, true);
			    		return false;
			    	});	
		 		}
		 		else{ cancelCallback(true); return false;}
		 	
		 		$('#Btn_cancel_vector_properties').unbind(touchClickEvent);
				$('#Btn_cancel_vector_properties').bind(touchClickEvent, function(){ cancelCallback(false); return false;});	    
		 		$('.Current_vector_details').width(440)
			}
		
	 		var starting_flr_lbl = labels.filter(function(lbl){ return lbl.screen == 'Starting Floor' });
	 		if (starting_flr_lbl.length > 0) {
	 			$('.Current_vector_details .head').html(starting_flr_lbl[0].screen);
	 			var table = '<table class="starting_floor"><tr>';
	 			table += '<th style="width: 125px;">Label</th><th style="width: 75px;">Area</th><th>Starting Floor No</th></tr>'
				starting_flr_lbl.forEach(function (label) {
					let ska = label.sketch.fixedAreaTrue ? parseInt(label.sketch.fixedAreaTrue) : label.sketch.area();
					table += '<tr><td>' + label.lookupDetail.tbl_element_desc + '</td><td style="text-align: center;">' + ska + '</td>';
		 			table += '<td><input style="width: 100px; height: 20px;" type="number" class="floorNo lbl_" oninput="if(value.length>4)value=value.slice(0,4)" uid="' + label.sketch.uid + '" sid="' + label.sid + '" lvl_code="' + label.lvl_code + '" ' + label.lookupDetail.tbl_type_code + '" /></td></tr>';
		 		});
		 		$('.Current_vector_details .dynamic_prop').html(table + '</table>');
	    	    
		    	$('#Btn_Save_vector_properties').unbind(touchClickEvent);
		    	$('#Btn_Save_vector_properties').bind(touchClickEvent, function () {
		    		var isValid = true;
		    		$('.floorNo').each(function(){
		    			if ($(this).val() == '') isValid = false;
		    		});
		    		if (isValid) {
		    			$('.floorNo').each(function(i, txt_box){
		    				sketchApp.sketches.filter(function(sk){ return sk.sid == $(txt_box).attr('sid') })[0].vectors.filter(function(v){ return v.uid == $(txt_box).attr('uid') })[0].labelFields[0].labelDetails.details[$(txt_box).attr('lvl_code')]['floor_no'] = $(txt_box).val();
		    			});
		    			checkAreaEntry();
	    			}
		    		else return false;
		   		});	
		 	
		 		$('#Btn_cancel_vector_properties').unbind(touchClickEvent);
				$('#Btn_cancel_vector_properties').bind(touchClickEvent, function(){ cancelCallback(false); });	 
		    	$('.Current_vector_details').width(365)
	 		}
	 		else checkAreaEntry();
 		}
 		else if (callback) callback(true, true);	
	} 	

	if (BasementFunctionalitys.length > 0) {
		function BasementFunctionalityss(){
			var basementFunctionalitys = BasementFunctionalitys.pop();
			var html = '',components = [],default_area=[];
			var bfun = basementFunctionalitys.labelDetails.details[1];
			var to_Area = basementFunctionalitys.Area;
			var Vect_uid = basementFunctionalitys.vect_uid;
			var Sid = basementFunctionalitys.sid;
			var skt_type = basementFunctionalitys.skt_type;
			$('.Current_vector_details .head').html('Area Entry');
			$('.dynamic_prop').html('');
			$('.dynamic_prop').append('<div class="divvectors"></div>');
	 		html += '<span style="width: 315px;justify-content: center;font-weight: bold;font-size: 18px;margin-left: 75px;">Enter Below Ground Areas</span>';
	 		if(bfun.comp2 != 0 && bfun.comp2 == '202' && bfun.modifier2 != '')
	 			components.push({comp:bfun.comp2, prefix: bfun.prefix2, modifier:bfun.modifier2});
	 		else if(bfun.comp3 != 0 && bfun.comp3 == '202' && bfun.modifier3 != '')
	 			components.push({comp:bfun.comp3, prefix: bfun.prefix3, modifier:bfun.modifier3});
	 		else if(bfun.comp4 !=0 && bfun.comp4 == '202' && bfun.modifier4 != '')
	 			components.push({comp:bfun.comp4, prefix: bfun.prefix4, modifier:bfun.modifier4});

	 		if(bfun.comp1!=0)
	 			components.push({comp:bfun.comp1, prefix: bfun.prefix1, modifier:bfun.modifier1});
	 		if(bfun.comp2 !=0 && !( bfun.comp2 == '202' && bfun.modifier2 != ''))
	 			components.push({comp:bfun.comp2, prefix: bfun.prefix2, modifier:bfun.modifier2});
	 		if(bfun.comp3!=0 && !( bfun.comp3 == '202' && bfun.modifier3 != ''))
	 			components.push({comp:bfun.comp3, prefix: bfun.prefix3, modifier:bfun.modifier3});
	 		if(bfun.comp4!=0 && !( bfun.comp4 == '202' && bfun.modifier4 != ''))
	 			components.push({comp:bfun.comp4, prefix: bfun.prefix4, modifier:bfun.modifier4});
	 			
	 		var i = 0;
	 		html += '<div class="BfunBeforeSave"><table style="border-spacing: 6px;">';
	 		components.forEach(function(com){
	 			i= i+1;
	 			var lookupVal = Proval_Lookup.filter(function(lk){ return lk.tbl_element == com.comp && lk.AdditionalScreen && (lk.AdditionalScreen == "BGAreaEntry" || (lk.AdditionalScreen.split("/")[0] == "BGAreaEntry")) && lk[skt_type + 'GradeLevel'] })[0];
	 			var t_area = com.prefix == '1' ? parseInt( 0.25 * basementFunctionalitys.Area) : (com.prefix == '2' ? parseInt( 0.5 * basementFunctionalitys.Area) : parseInt( 0.75 * basementFunctionalitys.Area))
	 			default_area.push(t_area);
	 			if(i == components.length){
	 				if(com.comp == '202' && com.modifier != ''){
	 					html += '<tr><td style="padding-right: 75px;font-weight: bold;font-size: 15px;">Finished Basement</td>';
		 				html += '<td><input style="width: 100px; height: 20px;" type="number" id="Fin" uid="' + basementFunctionalitys.sketch.uid + '" sid="' + basementFunctionalitys.sid + '" value="'+t_area+'" value2="'+com.comp+'" /></td></tr>';
		 				html += '<tr><td style="padding-right: 75px;font-weight: bold;font-size: 15px;">Unfinished Basement</td>';
		 				html += '<td><input style="width: 100px; height: 20px;" type="number" id="UnFin" uid="' + basementFunctionalitys.sketch.uid + '" sid="' + basementFunctionalitys.sid + '" value="'+0+'" value2="'+com.comp+'" disabled/></td></tr>';
	 				}
	 				else {
	 					html += '<tr><td style="padding-right: 75px;font-weight: bold;font-size: 15px;">'+ lookupVal.tbl_element_desc +'</td>';
		 				html += '<td><input style="width: 100px; height: 20px;" type="number" id='+com.comp+' uid="' + basementFunctionalitys.sketch.uid + '" sid="' + basementFunctionalitys.sid + '" value="'+t_area+'" disabled /></td></tr>';
	 				}
	 			}
	 			else{
	 				if(com.comp == '202' && com.modifier != ''){
	 					html += '<tr><td style="padding-right: 75px;font-weight: bold;font-size: 15px;">Finished Basement</td>';
		 				html += '<td><input style="width: 100px; height: 20px;" type="number" id="Fin" uid="' + basementFunctionalitys.sketch.uid + '" sid="' + basementFunctionalitys.sid + '" value="'+t_area+'" value2="'+com.comp+'" /></td></tr>';
		 				html += '<tr><td style="padding-right: 75px;font-weight: bold;font-size: 15px;">Unfinished Basement</td>';
		 				html += '<td><input style="width: 100px; height: 20px;" type="number" id="UnFin" uid="' + basementFunctionalitys.sketch.uid + '" sid="' + basementFunctionalitys.sid + '" value="'+0+'" value2="'+com.comp+'"/></td></tr>';
	 				}
	 				else{
	 					html += '<tr ><td style="padding-right: 75px;font-weight: bold;font-size: 15px;">'+ lookupVal.tbl_element_desc +'</td>';
		 				html += '<td><input style="width: 100px; height: 20px;" type="number" id='+com.comp+' uid="' + basementFunctionalitys.sketch.uid + '" sid="' + basementFunctionalitys.sid + '" value="'+t_area+'" /></td></tr>';
	 				}
	 			}
	 		});
	 		html += '</table></div>'
			$('.divvectors').append(html);
			$('.skNote').hide();
			$('#Btn_cancel_vector_properties').html('Defaults ');
			$('.Current_vector_details').show()
			$('.Current_vector_details').width(425)
			$('.BfunBeforeSave input').unbind();
			$('.BfunBeforeSave input').bind('keyup', function(){
  				var input_length = $(".BfunBeforeSave input").length;
  				var area_array = [], tot_area=0, tt_area = 0, last_area = 0;
  				for(var k=0 ; k <input_length ; k++ ){
                    area_array.push($('.BfunBeforeSave input')[k].value);
             	}
  				for(var i=0; i<area_array.length-1; i++){
  					if(area_array[i] == '')
  						area_array[i] = '0';
  					tot_area = tot_area + parseInt(area_array[i]);
  				}
  				tt_area = to_Area - tot_area;
  				if(tt_area >= 0){
  					last_area = tt_area;
  					$('.BfunBeforeSave input')[input_length-1].value = last_area;
  				}
  				else
  				{	
  					this.value = parseInt(this.value) + tt_area;
  					last_area = 0;
  					$('.BfunBeforeSave input')[input_length-1].value = last_area;
  				}
			});
			$('#Btn_Save_vector_properties').unbind(touchClickEvent);
			$('#Btn_Save_vector_properties').bind(touchClickEvent, function () {
				var sdetails = {}; 
				var vect = sketchApp.sketches.filter(function(sk){ return sk.sid == Sid })[0].vectors.filter(function(vect){ return vect.uid == Vect_uid })[0];
				vect.labelFields[0].labelDetails.base_array = [];
				for(var k=0 ; k <$('.BfunBeforeSave input').length ; k++ ){
                    var sav = $('.BfunBeforeSave input')[k];
                    var areaas = (sav.value && sav.value != '') ? sav.value : '0';
                    if(sav.id == "Fin" || sav.id == "UnFin")
						sdetails = {Id: '202', area: areaas, modifier: sav.id};
					else
						sdetails = {Id: sav.id, area: areaas};
					vect.labelFields[0].labelDetails.base_array.push(_.clone(sdetails));
				}
				$('.Current_vector_details').hide();
				$('#Btn_cancel_vector_properties').html('Cancel ');
				if(BasementFunctionalitys.length > 0){
					BasementFunctionalityss();
					return false;
				}
				else{
					$('.mask').hide();
					nBasementFunctionality();
					return false;
				}
			});
			$('#Btn_cancel_vector_properties').unbind(touchClickEvent);
			$('#Btn_cancel_vector_properties').bind(touchClickEvent, function(){ 
				var k = 0;
				for(var i=0 ; i <$('.BfunBeforeSave input').length ; i++ ){
                    var Inp = $('.BfunBeforeSave input')[k];
					if(Inp.id == 'UnFin')
						Inp.value = '0';
					else{
						Inp.value = default_area[k];
						k = k + 1;
					}
				}
				return false;
			});	 
		}
		$('.mask').show();
		BasementFunctionalityss();
		return false;
	}
	 else{
		nBasementFunctionality();
		return false;
	 }
	}
	var imp_width = 0 ,imp_length = 0;
	function calculateImpwidth(souTable, impcallback){
		if(souTable != 'ccv_improvements'){
			if(impcallback) impcallback();
		}
		else{
			var vectors = sketchApp.currentSketch && sketchApp.currentSketch.vectors[0];

		
			if(impcallback)
				impcallback();
		}
	}
  function RecCretation(){
	    var modified_sketches = sketchApp.sketches.filter(function(skda){ return skda.isModified })
		var new_vectors = [];
		modified_sketches.forEach(function(Md_sketches){
			Md_sketches.vectors.filter(function(vect) { return vect.newRecord && !(vect.exterior_new_reocrd); }).forEach(function(vt) {
                if (vt.isPasteVector && vt.sketchType != "outBuilding") {
                    var labelDatailsFeat = vt.labelFields[0].labelDetails.details, extfeatAlone = false;
					$.each(labelDatailsFeat,function(det) {
						var lbfeat = labelDatailsFeat[det];
						if((lbfeat.comp != 0 && lbfeat.comp != '0') || (lbfeat.prefix != 0 && lbfeat.prefix != '0') || (lbfeat.constr != 0 && lbfeat.constr != '0'))
							extfeatAlone = true;
					});
					if(!extfeatAlone)
						vt.MapExteriorFeature = true;
                }        
                new_vectors.push(vt);
            });
		});
		new_vectors = new_vectors.reverse();
		function new_vector_record_creation(new_vectors_rec){
        	if(new_vectors_rec.length == 0){
				sketchsavee();
				return false;
            }
			var n_vect_rec = new_vectors_rec.pop();
			var extension_rowuid = n_vect_rec.sketch.uid;
			var ParentRowExt, cSketchTrue = false;	

			ParentRowExt = parcel["ccv_extensions"].filter(function(ext){return ext.ROWUID == extension_rowuid})[0];	
			/*
			extension_Recd_Array.forEach(function(exRe){
				if(!cSketchTrue && exRe.eRowuid == extension_rowuid)
					cSketchTrue = true;
			});
			if(!cSketchTrue)
				extension_Recd_Array.push(_.clone({eRowuid: extension_rowuid, iRecord: [], CdRecord: []}));
			*/
			var specialCatId = getCategoryFromSourceTable(ConfigTables.comm_special_use).Id;
			var ImpIdMethod_Field, ImpTypee_Field, resCommRecordsArr = [];
			var details = {}; 

			var imp_length = 0, imp_width = 0;
			var vectorString = n_vect_rec.vectorString? n_vect_rec.vectorString: n_vect_rec.toString();
			if(vectorString == '' || vectorString == null || vectorString == ' '){
				new_vector_record_creation(new_vectors_rec);
				return false;
			}
			
			function MaptoExteriorFeatureRecordCreation(n_vect_recsss, extCheckedValues, extension_recordd, imlen, imwid){
				var extRowuid, ext_featRecord = [];
				var ImpTys = 'MISC', impiddd = '1', inim = false, misc_Im_Type = '155', _impMethod;
				var labelDatailsFeat = n_vect_recsss.labelFields[0].labelDetails.details, upl = false;
				$.each(labelDatailsFeat,function(det){
					//if(det != 1 && det != '1'){
						var lbfeat = labelDatailsFeat[det];
						if(lbfeat.ext_feat_code != 0 && lbfeat.ext_feat_code != '0'){
							var ext_split_code = parseInt(lbfeat.ext_feat_code);
							var ext_temp = ext_split_code % 1000;
							var pro_look = Proval_Lookup.filter(function(lkp){ return lkp.tbl_element == ext_temp && lkp.tbl_type_code == 'Exterior Feature'})[0];
							misc_Im_Type = (pro_look && pro_look.field_2 && pro_look.field_2 != '') ? pro_look.field_2 : misc_Im_Type;
							ext_featRecord.push({lb_detail: lbfeat, dett: det});
							if (det == 3) upl = true;
							if(!inim && pro_look){
								ImpTys = lbfeat.ImpType;
								impiddd = lbfeat.ImpIdMethod;
								_impMethod = impiddd;
								inim = true;
							}
						}
					//}
				});
				if(extCheckedValues == 'StandAlone'){
					/*
					var impFieldArray = [];
					var impFields = ['imp_width', 'imp_length', 'imp_type', 'improvement_id', 'misc_imp_type', 'attached_flag'];
					var impFieldValue = [imwid, imlen, ImpTys, impiddd, misc_Im_Type, 'N'];
					impFields.forEach(function(fname){ impFieldArray.push(getDataField(fname, 'ccv_improvements')); });
					var CC_YearStatus = ( ( (clientSettings['FutureYearEnabled'] == '1') && getCategoryFromSourceTable('ccv_improvements').YearPartitionField && (getCategoryFromSourceTable('ccv_improvements').YearPartitionField != '0') )? ( (window.opener? window.opener.showFutureData : showFutureData) ? 'F' : 'A'  ) : "" );
					saveData += '^^^' + parcel.Id + '|' + rowidImp + '|' + extension_recordd.ROWUID + '||' + 'ccv_improvements' + '|new|' + CC_YearStatus + '\n';
					updateDefaultValues(parcel.Id, rowidImp, extension_recordd.ROWUID, 'ccv_improvements');
					var extttRecs = extension_Recd_Array.filter(function(rt){return rt.eRowuid == extension_recordd.ROWUID})[0];
					if(extttRecs)
						extttRecs.iRecord.push(_.clone({ROWUID: rowidImp, ParentROWUID: extension_recordd.ROWUID, CC_RecordStatus: 'I', imp_type: ImpTys, improvement_id: impiddd, misc_imp_type: misc_Im_Type, attached_flag: 'N'}))
					for(var i = 0; i < 6; i++){
						saveData += '^^^' + parcel.Id + '|' + rowidImp + '|' + impFieldArray[i].Id + '||' + impFieldValue[i] + '|edit|' + extension_recordd.ROWUID + '\n';
					}
					ext_featRecord.forEach(function(lbt){
						var levelt = lbt.dett;
						n_vect_rec.labelFields[0].labelDetails.details[levelt].parent_imp_id = "&%ccv_improvements{" + rowidImp + "}&%";
						n_vect_rec.labelFields[0].labelDetails.details[levelt].parent_type =  "imps";
					});
					new_vector_record_creation(new_vectors_rec);
					
					*/
					var caeId = getCategoryFromSourceTable('ccv_improvements').Id;
					var rowidImp = ccTicks();
					misc_Im_Type = ((upl && misc_Im_Type && misc_Im_Type != '')? misc_Im_Type + '/': misc_Im_Type);
					insertNewAuxRecordSketch(parcel.Id, extension_recordd, caeId, rowidImp,{imp_width: imwid, imp_length: imlen, imp_type: ImpTys, improvement_id: impiddd , misc_imp_type: misc_Im_Type, attached_flag:'N'},function (saveNewRecordDataI) {
						if(saveNewRecordDataI)
							saveData += saveNewRecordDataI;
						updateDefaultValues(parcel.Id, rowidImp, extension_recordd.ROWUID, 'ccv_improvements');
						ext_featRecord.forEach(function(lbt){
							var levelt = lbt.dett;
							n_vect_rec.labelFields[0].labelDetails.details[levelt].parent_imp_id = "&%ccv_improvements{" + rowidImp + "}&%";
							n_vect_rec.labelFields[0].labelDetails.details[levelt].parent_type =  "imps";
							if(levelt == '1')
								n_vect_rec.labelFields[0].labelDetails.details[levelt].ImpIdMethod = _impMethod; 
						});
						new_vector_record_creation(new_vectors_rec);
					});
					
				}
				else{
					var chRwid , stablee, impTrue = false;
					if( extCheckedValues.split('{').length > 1){
						 chRwid = extCheckedValues.split('{')[1].split('}')[0];
						 stablee = extCheckedValues.split('{')[0];
						 impTrue = true;
					}
					else if(extCheckedValues.split('%').length > 1){
						chRwid = extCheckedValues.split('%')[1].split('$')[0];
						stablee = extCheckedValues.split('%')[0];
						impTrue = false;
					}
					var seg_typef = (stablee == 'ccv_improvements_dwellings')? 'dwellings': ((stablee == 'ccv_improvements_comm_bldgs')? 'comm_bldgs': 'imps');
					ext_featRecord.forEach(function(lbt){
						var levelt = lbt.dett;
						if(impTrue)
							n_vect_rec.labelFields[0].labelDetails.details[levelt].parent_imp_id = "&%" + stablee+ "{" + chRwid + "}&%";
						else
							n_vect_rec.labelFields[0].labelDetails.details[levelt].parent_imp_id = chRwid;
						n_vect_rec.labelFields[0].labelDetails.details[levelt].parent_type = seg_typef;
						if(levelt == '1')
								n_vect_rec.labelFields[0].labelDetails.details[levelt].ImpIdMethod = _impMethod; 
					});
					new_vector_record_creation(new_vectors_rec);
				}
			}
			
			function MaptoExteriorFeature(n_vect_recss, extension_rowuid, imlen, imwid){
				var html = '', ext_change_var = 'StandAlone', mis_Im_Type = '155';
				var labelDatailsFeat = n_vect_recss.labelFields[0].labelDetails.details;
				$.each(labelDatailsFeat,function(det){
					if(det != 1 && det != '1'){
						var lbfeat = labelDatailsFeat[det];
						if(lbfeat.ext_feat_code != 0 && lbfeat.ext_feat_code != '0'){
							var ext_split_codes = parseInt(lbfeat.ext_feat_code);
							var ext_temps = ext_split_codes % 1000;
							var pro_look = Proval_Lookup.filter(function(lkp){ return lkp.tbl_element == ext_temps && lkp.tbl_type_code == 'Exterior Feature'})[0];
							mis_Im_Type = (pro_look && pro_look.field_1 && pro_look.field_1 != '') ? pro_look.field_1 : mis_Im_Type;
						}
					}
				});
				$('.Current_vector_details .head').html('Exterior Feature Improvement');
				$('.dynamic_prop').html('');
				$('.Current_vector_details div').css('padding','0px');
				$('.dynamic_prop').append('<div class="divvectors"></div>');
	 			html += '<span style="width: 460px;justify-content:font-weight: bold;justify-content: center;font-size: 16px; padding-bottom: 15px;">Please select the improvement for the exterior feature "'+ mis_Im_Type +'":</span>';
				html += '<span class ="ext_feature_value" style="width: 300px;justify-content:font-weight: bold; justify-content: center; font-size: 16px;padding-bottom: 15px;margin-left: 180px;">'+ ext_change_var+'</span>';
				html += '<div class="Ext_features">';
				html += '<span style="float:left;width:300px;font-size: 16px;padding-bottom: 15px;"><input type="radio" id="standAlone" name="Exterior_Features" value="StandAlone" value1="StandAlone" checked="checked" style="float:left;width:50px;height:20px"><label for="standAlone" style="font-size: 16px;">[StandAlone]</label></span></br>';
				var extension_recordd = ParentRowExt;
				var ext_typee = extension_recordd.RorC;
				var DorCTable = (ext_typee == 'R') ? 'ccv_improvements_dwellings': 'ccv_improvements_comm_bldgs';
				var ICDwellRecords = [];
				extension_recordd['ccv_improvements'].forEach(function(im){
					if ( ((im.improvement_id != null && im.improvement_id != 'null' && im.improvement_id != '' && im.improvement_id != ' ') || ((im.improvement_id == null || im.improvement_id == '') && im.imp_type && im.CC_RecordStatus == 'I')) && im.CC_Deleted != true )
						ICDwellRecords.push(_.clone({ICDFrecords: im, soTable:'ccv_improvements'}));
				});
				/*
				var extttRecs = extension_Recd_Array.filter(function(rt){return rt.eRowuid == extension_rowuid})[0];
				if(extttRecs)
					extttRecs.iRecord.forEach(function(im){
						ICDwellRecords.push(_.clone({ICDFrecords: im, soTable:'ccv_improvements'}));
					});
				*/
				extension_recordd[DorCTable].forEach(function(im){
					if(im.CC_Deleted != true)
						ICDwellRecords.push(_.clone({ICDFrecords: im, soTable: DorCTable}));
				});
				/*
				if(extttRecs)
					extttRecs.CdRecord.forEach(function(im){
						ICDwellRecords.push(_.clone({ICDFrecords: im, soTable:DorCTable}));
					});
				*/
				ICDwellRecords.forEach(function(ICDW){
					var icRecord = ICDW.ICDFrecords;
					var tabl = ICDW.soTable;
					var delte_rec = deleteVect_Ext_Feat_Array.filter(function(del_v){return ((del_v.ext_rowid == icRecord.ParentROWUID ) &&( del_v.souTable == tabl) && (del_v.pa_imp_id == icRecord.ROWUID || del_v.pa_imp_id == icRecord.improvement_id))})[0];
					if(!delte_rec){
						if(icRecord.CC_RecordStatus == "I") {
							let ic_imp_id = icRecord.improvement_id? icRecord.improvement_id: '';
							html += '<span style="float:left;width:300px;font-size: 16px;padding-bottom: 15px;"><input type="radio" id="'+ icRecord.ROWUID +'" name="Exterior_Features" value="'+ tabl + '{' + icRecord.ROWUID + '}'+'" value1="'+ ic_imp_id + ':' + icRecord.imp_type +'" style="float:left;width:50px;height:20px;"><label for="'+ icRecord.ROWUID +'" style="font-size: 16px;">'+ ic_imp_id + ':' + icRecord.imp_type +'</label></span></br>';
						}
						else
							html += '<span style="float:left;width:300px;font-size: 16px;padding-bottom: 15px;"><input type="radio" id="'+icRecord.ROWUID+'" name="Exterior_Features" value="' + tabl + '%' +icRecord.improvement_id+ '$' +'" value1="'+ icRecord.improvement_id + ':' + icRecord.imp_type +'" style="float:left;width:50px;height:20px;"><label for="'+icRecord.ROWUID+'" style="font-size: 16px;">'+ icRecord.improvement_id + ':' + icRecord.imp_type +'</label></span></br>';
					}
				});
				html += '</div>'
				$('.divvectors').append(html);
				$('.skNote').hide();
				$('#Btn_cancel_vector_properties').hide();
				$('#Btn_Save_vector_properties').html('OK ');
				$('#Btn_Save_vector_properties').css('margin-right','200px');
				$('.Current_vector_details').show()
				$('.Current_vector_details').width(520);
				$("input[type='radio']").change(function(){
					var chtm = $(this).attr('value1'); 
					if (chtm && chtm.split(':').length > 1) {
						let nchtm = chtm.split(':')[0];						 
						if (nchtm == '' && chtm.split(':')[1] && chtm.split(':')[1] != '')
							nchtm = chtm.split(':')[1];
						chtm = nchtm;
					}
					ext_change_var = chtm;
					$('.ext_feature_value').html(ext_change_var);
					$('.ext_feature_value').show();
				});
				$('#Btn_Save_vector_properties').unbind(touchClickEvent);
				$('#Btn_Save_vector_properties').bind(touchClickEvent, function () {
					var extCheckedValue = $("input[name='Exterior_Features']:checked").val();
					$('#Btn_Save_vector_properties').html('Save ');
					$('#Btn_Save_vector_properties').css('margin-right','4px');
					$('#Btn_cancel_vector_properties').show();
					$('.Current_vector_details').hide();
					$('.mask').hide();
					MaptoExteriorFeatureRecordCreation(n_vect_recss, extCheckedValue, extension_recordd, imlen, imwid);
					return false;
				});
			$('.mask').show();
			}
			
			var spArea = n_vect_rec.area();
			var points = vectorString.split(':')[1].split(' S ');
			let lenWidth = widthLengthCalculator(points);
			imp_length = Math.round(lenWidth.imp_length);
			imp_width = Math.round(lenWidth.imp_width);
			if(n_vect_rec.MapExteriorFeature){
				MaptoExteriorFeature(n_vect_rec, extension_rowuid, imp_length, imp_width);
				return false;
			}
			
			var imp_detail_array = [];
			if(n_vect_rec.labelFields[0].labelDetails){
				if(n_vect_rec.labelFields[0].labelDetails.details){
					details = n_vect_rec.labelFields[0].labelDetails.details;			
					$.each(details,function(det){
						imp_detail_array.push( _.clone(details[det]));
					});
				}
			}
			imp_detail_array = imp_detail_array.reverse();
			var j = 0;
			
			function new_vector_record(imp_detail_arrays){ 
				if(imp_detail_arrays == 0){
					new_vector_record_creation(new_vectors_rec);
					return;
				}
				var imp_detail_arr = imp_detail_arrays.pop();
				j++;
				var ImpIdMethodd = (imp_detail_arr && imp_detail_arr.ImpIdMethod) ? imp_detail_arr.ImpIdMethod : '';
				var ImpTypee = (imp_detail_arr && imp_detail_arr.ImpType) ? imp_detail_arr.ImpType : '';
				if(ImpIdMethodd == '' && ImpTypee == ''){
					new_vector_record(imp_detail_arrays);
					return;
				}
				var sTable = ((imp_detail_arr.parent_type == 'dwellings') ? 'ccv_improvements_dwellings' : (imp_detail_arr.parent_type == 'comm_bldgs') ? 'ccv_improvements_comm_bldgs' :'ccv_improvements');
				var dRecord = [];
				//var extttRec = extension_Recd_Array.filter(function(rt){return rt.eRowuid == ParentRowExt.ROWUID})[0];
				if(sTable == "ccv_improvements_comm_bldgs" || sTable == "ccv_improvements_dwellings"){
					//var comDERE = ParentRowExt[sTable].filter(function(del){return (del.CC_Deleted != true)});
					//dRecord = comDERE.length > 0 ? comDERE : extttRec.CdRecord;
					dRecord = parcel[sTable].filter(function(rrrecord){return ((rrrecord.ParentROWUID == extension_rowuid) && (rrrecord.CC_Deleted != true)) });
				}
				var imp_width_imp = 0, imp_length_imp = 0;
				var cateeId = getCategoryFromSourceTable(sTable).Id;
				if(sTable=='ccv_improvements') {
					imp_width_imp = imp_width;
					imp_length_imp = imp_length;
					if(!((imp_detail_arr.comp != 0 && imp_detail_arr.comp != '0') || (imp_detail_arr.prefix != 0 && imp_detail_arr.prefix != '0') || (imp_detail_arr.constr != 0 && imp_detail_arr.constr != '0') || (imp_detail_arr.ext_cover != '' && imp_detail_arr.ext_cover !='0') || (imp_detail_arr.modifier != '')) && (imp_detail_arr.ext_feat_code != 0 && imp_detail_arr.ext_feat_code != '0')){
						for(i = 1;i <= 3;i++){
							if( j == i )
								continue;
							else{
								var impp_p_type = n_vect_rec.labelFields[0].labelDetails.details[i].parent_type;
								if(impp_p_type == 'imps'){
									var p_im_id = n_vect_rec.labelFields[0].labelDetails.details[i].parent_imp_id;
									if(p_im_id && p_im_id != ''){
										if(p_im_id.split('{').length > 1){
											p_im_id = p_im_id.split('{')[1].split('}')[0];
										}
										//var iRecs = ParentRowExt[sTable].filter(function(rrrecord){return ((rrrecord.ROWUID == p_im_id || rrrecord.improvement_id == p_im_id) && rrrecord.CC_Deleted != true)});
										//iRecs = iRecs.length > 0 ? iRecs: extttRec.iRecord.filter(function(rrrecord){return (rrrecord.ROWUID == p_im_id || rrrecord.improvement_id == p_im_id)});
										//dRecord =  iRecs;
										dRecord = parcel['ccv_improvements'].filter(function(rrrecord){return ((rrrecord.ROWUID == p_im_id || rrrecord.improvement_id == p_im_id) && (rrrecord.ParentROWUID == extension_rowuid) && (rrrecord.CC_Deleted != true) )});
										if(dRecord)
											break;
									}
								}
							}
						}
					}
				}
				dRecord = dRecord ? dRecord :[];
				if(dRecord.length > 0){
					var imppp_id
					if(dRecord[0].CC_RecordStatus == 'I')
						imppp_id = '&%' + sTable +'{' + dRecord[0].ROWUID + '}&%';
					else
						imppp_id = dRecord[0].improvement_id;
					n_vect_rec.labelFields[0].labelDetails.details[j].parent_imp_id = imppp_id;
					new_vector_record(imp_detail_arrays);
				}
				else{
						var icdRowuidd = ccTicks();
						insertNewAuxRecordSketch( parcel.Id, ParentRowExt, cateeId, icdRowuidd, {imp_width: imp_width_imp, imp_length: imp_length_imp, imp_type: ImpTypee, improvement_id: ImpIdMethodd }, function (saveNewRecordDataICD) {
							if(saveNewRecordDataICD)
								saveData += saveNewRecordDataICD;
							updateDefaultValues(parcel.Id, icdRowuidd, ParentRowExt.ROWUID, sTable);
							var parentROEW = parcel? parcel[sTable].filter(function(rrrecord){return rrrecord.ROWUID == icdRowuidd})[0]: [];
							var imppp_id = '&%' + sTable +'{' + icdRowuidd +'}&%';
							n_vect_rec.labelFields[0].labelDetails.details[j].parent_imp_id = imppp_id;
	        				if(sTable=="ccv_improvements_comm_bldgs"){
	        					var Area_special = parseInt(spArea);
	        					var icdRowuiddr = icdRowuidd + 50 ; 
								insertNewAuxRecordSketch(parcel.Id, parentROEW, specialCatId, icdRowuiddr, {use_code: 'COMBLDG', area: Area_special},function (saveNewRecordDataSP) {
									if(saveNewRecordDataSP)
										saveData += saveNewRecordDataSP;
									updateDefaultValues(parcel.Id, icdRowuiddr, icdRowuidd, ConfigTables.comm_special_use);
									new_vector_record(imp_detail_arrays);
	                        	});	
	        				}
	        				else
	        					new_vector_record(imp_detail_arrays);
						});
						
						/*
						var CC_YearStatus = ( ( (clientSettings['FutureYearEnabled'] == '1') && getCategoryFromSourceTable(sTable).YearPartitionField && (getCategoryFromSourceTable(sTable).YearPartitionField != '0') )? ( (window.opener? window.opener.showFutureData : showFutureData) ? 'F' : 'A'  ) : "" );
						saveData += '^^^' + parcel.Id + '|' + icdRowuidd + '|' + ParentRowExt.ROWUID + '||' + sTable + '|new|' + CC_YearStatus + '\n';
						updateDefaultValues(parcel.Id, icdRowuidd, ParentRowExt.ROWUID, sTable);
						if(sTable == 'ccv_improvements')
							extttRec.iRecord.push(_.clone({ROWUID: icdRowuidd, ParentROWUID: ParentRowExt.ROWUID, CC_RecordStatus: 'I', imp_type: ImpTypee, improvement_id: ImpIdMethodd}))
						else
							extttRec.CdRecord.push(_.clone({ROWUID: icdRowuidd, ParentROWUID: ParentRowExt.ROWUID, CC_RecordStatus: 'I', imp_type: ImpTypee, improvement_id: ImpIdMethodd}))
						/*
						var impFieldArrays = [];
						var impFieldss = ['imp_width', 'imp_length', 'imp_type', 'improvement_id'];
						var impFieldValues = [imp_width_imp, imp_length_imp, ImpTypee, ImpIdMethodd];
						impFieldss.forEach(function(fname){ impFieldArrays.push(getDataField(fname, sTable)); });
						for(var i = 0; i < 4; i++){
							if(impFieldArrays[i])
								saveData += '^^^' + parcel.Id + '|' + icdRowuidd + '|' + impFieldArrays[i].Id + '||' + impFieldValues[i] + '|edit|' + ParentRowExt.ROWUID + '\n';
						}
						
        				if(sTable == "ccv_improvements_comm_bldgs"){
        					var Area_special = parseInt(spArea);
        					var icdRowuiddr = icdRowuidd + 50 ; 
        					var use_code_field = getDataField('use_code', ConfigTables.comm_special_use);
        					var area_field = getDataField('area', ConfigTables.comm_special_use);
        					var CC_YearStatus = ( ( (clientSettings['FutureYearEnabled'] == '1') && getCategoryFromSourceTable(ConfigTables.comm_special_use).YearPartitionField && (getCategoryFromSourceTable(ConfigTables.comm_special_use).YearPartitionField != '0') )? ( (window.opener? window.opener.showFutureData : showFutureData) ? 'F' : 'A'  ) : "" );
        					saveData += '^^^' + parcel.Id + '|' + icdRowuiddr + '|' + icdRowuidd + '||' + ConfigTables.comm_special_use + '|new|' + CC_YearStatus + '\n';
        					updateDefaultValues(parcel.Id, icdRowuiddr, icdRowuidd, ConfigTables.comm_special_use);
							saveData += '^^^' + parcel.Id + '|' + icdRowuiddr + '|' + use_code_field.Id + '||' + 'COMBLDG' + '|edit|' + icdRowuidd + '\n';
							saveData += '^^^' + parcel.Id + '|' + icdRowuiddr + '|' + area_field.Id + '||' + Area_special + '|edit|' + icdRowuidd + '\n';
							new_vector_record(imp_detail_arrays);	
        				}
        				else
        					new_vector_record(imp_detail_arrays);
        				*/
				}
        }
        if(n_vect_rec.sketchType == 'outBuilding'){
			var labelcode = n_vect_rec.labelFields[0].label_code;
			var ob_imptype = OutB_Lookups["ob_codes_imp_type"][labelcode]? OutB_Lookups["ob_codes_imp_type"][labelcode].AdditionalValue1: '';
			var ImpIdMethoddd = (labelcode == 'O054' || labelcode == 'O603')? 'MISC' : ob_imptype;
			var ImpIdMethod_Fieldd = getDataField('imp_type','ccv_improvements');
			var ImpIdMethod_Misc = getDataField('misc_imp_type','ccv_improvements');
			var cateId = getCategoryFromSourceTable('ccv_improvements').Id;
			var OutB_impRowid = ccTicks();
			var impSkipFIelds = ['imp_type', 'imp_width', 'imp_length', 'imp_size'];	
			var obSketchEdited = n_vect_rec.obSketchEdited; 
			var vectorString = n_vect_rec.vectorString;
			var points = vectorString.split(':')[1].split(' S ');
			var hLength = 0, RLength = 0, lT=0, uT=0, imp_width = 0, imp_length = 0, imp_size = 0; 
			points.length > 1 && points[1].split(/\s/).forEach(function(p, i) {
				if ( p.trim() != '' ){
					if(p.split("/").length > 1)
						return;
					else{
						if((p.split("L").length > 1 || p.split("R").length > 1) && lT == 0 ){
							hLength = p.split("L").length > 1 ? parseFloat(p.split("L")[1]) : parseFloat(p.split("R")[1]);
							lT = 1;
						}
						else if((p.split("U").length > 1 || p.split("D").length > 1) && uT == 0 ){
							RLength = p.split("U").length > 1 ? parseFloat(p.split("U")[1]) : parseFloat(p.split("D")[1]);
							uT =1;
						}	
					}

				}
			});
			if(hLength > RLength) {
				imp_length = hLength;
				imp_width = RLength;
				imp_size = imp_width * imp_length;
			}
			else{
				imp_length = RLength;
				imp_width = hLength;
				imp_size = imp_width * imp_length;
			}
			imp_size = Math.round(imp_size);
			imp_length = Math.round(imp_length);
			imp_width = Math.round(imp_width);
			var impFieldArrays = [];
			var impFieldss = ['imp_width', 'imp_length', 'imp_size'];
			var impFieldValues = [imp_width, imp_length, imp_size];
			impFieldss.forEach(function(fname){ impFieldArrays.push(getDataField(fname, 'ccv_improvements')); });
			
			function labelEditedObSketchFieldUpdation(newImpRowId, obsketchEdited) {
				if (obsketchEdited && obsketchEdited.uid) {
					impSkipFIelds = impSkipFIelds.concat(tableKeys.filter(function(k) { return k.SourceTable == 'ccv_improvements' }).map(function(keyf){ return keyf.Name }));
					var obSketchRec = parcel[ConfigTables.SktOutbuilding].filter(function(r){ return r.ROWUID == obsketchEdited.uid })[0];
					var ob_imp_id = obSketchRec.improvement_id;
					var ob_parent_id = obSketchRec.ParentROWUID;
					var delImpRec = parcel['ccv_improvements'].filter(function(m){ return (m.improvement_id == ob_imp_id && m.ParentROWUID == ob_parent_id) })[0];
					if (delImpRec) {
						for (key in delImpRec) {
	                        if (key != 'Original' && key != 'ParentRecord' && (!delImpRec[key] || delImpRec[key] === 0 || delImpRec[key] == '' || typeof delImpRec[key] != 'object') && typeof delImpRec[key] != 'function' && impSkipFIelds.indexOf(key) == -1) {
	                            var cfield = getDataField(key,'ccv_improvements');
	                            if (cfield)
	                            	saveData += '^^^' + parcel.Id + '|' + newImpRowId + '|' + cfield.Id + '||' + delImpRec[key] + '|edit|' + ParentRowExt.ROWUID + '\n';
	                        }
	                    }
                    }
				}
				new_vector_record_creation(new_vectors_rec);
				return;
			}
			
			insertNewAuxRecordSketch(parcel.Id, ParentRowExt, cateId, OutB_impRowid, { imp_width: imp_width, imp_length: imp_length, imp_size: imp_size, imp_type: ImpIdMethoddd }, function(saveNewRecordDataIMP) {
				if (saveNewRecordDataIMP)
					saveData += saveNewRecordDataIMP;
				updateDefaultValues(parcel.Id, OutB_impRowid, ParentRowExt.ROWUID, 'ccv_improvements');
				n_vect_rec.outBd_imp_id = '&%ccv_improvements{'+ OutB_impRowid +'}&%';
				if (labelcode == 'O054' || labelcode == 'O603') {
					saveData += '^^^' + parcel.Id + '|' + OutB_impRowid + '|' + ImpIdMethod_Misc.Id + '||' + n_vect_rec.labelFields[0].miscValue + '|edit|' + ParentRowExt.ROWUID + '\n';
					impSkipFIelds.push('misc_imp_type');     			
        		}
        		labelEditedObSketchFieldUpdation(OutB_impRowid, obSketchEdited);
	       	});
	       	
			/*var CC_YearStatus = ( ( (clientSettings['FutureYearEnabled'] == '1') && getCategoryFromSourceTable('ccv_improvements').YearPartitionField && (getCategoryFromSourceTable('ccv_improvements').YearPartitionField != '0') )? ( (window.opener? window.opener.showFutureData : showFutureData) ? 'F' : 'A'  ) : "" );
			saveData += '^^^' + parcel.Id + '|' + OutB_impRowid + '|' + ParentRowExt.ROWUID + '||' + 'ccv_improvements' + '|new|' + CC_YearStatus + '\n';
			updateDefaultValues(parcel.Id, OutB_impRowid, ParentRowExt.ROWUID, 'ccv_improvements');
			n_vect_rec.outBd_imp_id = '&%ccv_improvements{'+OutB_impRowid+'}&%';
			for(var i = 0; i < 3; i++){
				if(impFieldArrays[i])
					saveData += '^^^' + parcel.Id + '|' + OutB_impRowid + '|' + impFieldArrays[i].Id + '||' + impFieldValues[i] + '|edit|' + ParentRowExt.ROWUID + '\n';
			}
			if(labelcode == 'O054' || labelcode == 'O603'){
        		saveData += '^^^' + parcel.Id + '|' + OutB_impRowid + '|' + ImpIdMethod_Fieldd.Id + '||' + ImpIdMethoddd + '|edit|' + ParentRowExt.ROWUID + '\n';
				saveData += '^^^' + parcel.Id + '|' + OutB_impRowid + '|' + ImpIdMethod_Misc.Id + '||' + n_vect_rec.labelFields[0].miscValue + '|edit|' + ParentRowExt.ROWUID + '\n';
				new_vector_record_creation(new_vectors_rec);        			
        	}
        	else{
        		saveData += '^^^' + parcel.Id + '|' + OutB_impRowid + '|' + ImpIdMethod_Fieldd.Id + '||' + ImpIdMethoddd + '|edit|' + ParentRowExt.ROWUID + '\n';
				new_vector_record_creation(new_vectors_rec);
        	}*/	        	
		}
		else
			new_vector_record(imp_detail_array);
	}
	new_vector_record_creation(new_vectors);			
	   
  }
  	$('.ccse-canvas').addClass('dimmer');
  	$('.dimmer').show();  
  	
  	sketchApp.deletedVectors.forEach(function(delexvect){
  		var exten_rowide = (delexvect.islabelEditedTrue) ? delexvect.eEid: delexvect.sketch.uid;
  		var cutSketch = sketchApp.sketches.filter(function(dk){return dk.uid == exten_rowide})[0];
  		var cutVectors = cutSketch.vectors.filter(function(vk){return vk.sketchType != 'outBuilding'});
  		let outVectors = cutSketch.vectors.filter(function(vk){ return vk.sketchType == 'outBuilding' });
  		var extension_rowid = delexvect.uid;
		var ext_length = extension_rowid.split('/').length;
		if(ext_length <= 1 && delexvect.sketchType != 'outBuilding'){
  			var ext_details = delexvect.labelFields[0].labelDetails.details;
  			let impsMatched = false, standImpId = null, nextStep = true, extfeatAlone = false, notstandalone = false;
  			$.each(ext_details,function(det) {
				var lbfeat = ext_details[det];
				if(((lbfeat.comp != 0 && lbfeat.comp != '0') || (lbfeat.prefix != 0 && lbfeat.prefix != '0') || (lbfeat.constr != 0 && lbfeat.constr != '0') || (lbfeat.ext_cover != '' && lbfeat.ext_cover !='0') || (lbfeat.modifier != ''))) extfeatAlone = true;
				if( (lbfeat.comp != 0 && lbfeat.comp != '0') || (lbfeat.prefix != 0 && lbfeat.prefix != '0') || (lbfeat.constr != 0 && lbfeat.constr != '0') ) notstandalone = true; 
                if (lbfeat.parent_type == "imps") { 
                	impsMatched = true;
                	standImpId = lbfeat.parent_imp_id;
                }
			});
			
			if (!notstandalone && impsMatched && standImpId) {
				if (standImpId.split('{').length > 1) standImpId = standImpId.split('{')[1].split('}')[0];
				let cd = sketchApp.deletedVectors.filter((vk) => { return (vk.sketchType == 'outBuilding' && !vk.newRecord && vk.sketch && (vk.sketch.uid == exten_rowide)) });
				let combined = outVectors.concat(cd), outMatch = false;
				combined.forEach((ob) => {
					let outRecord = parcel[ConfigTables.SktOutbuilding].filter((out) => { return (out.ROWUID == ob.uid) })[0];
					let out_imprId = outRecord && outRecord.improvement_id && outRecord.improvement_id != ''? outRecord.improvement_id: null;
					if (out_imprId && out_imprId.split('{').length > 1) out_imprId = out_imprId.split('{')[1].split('}')[0];
					if (out_imprId == standImpId) outMatch = true;
				});
				
				if (outMatch) {	nextStep =  false; delexvect.doNotDeleteImp =  true; }
			}
			
			if (extfeatAlone && nextStep) {
		  		$.each(ext_details,function(detext){
					var ext_det = ext_details[detext];
					var p_ext_type = ext_det.parent_type;
					var imp_Id_Icd = ext_det.parent_imp_id;
					if((p_ext_type && p_ext_type !='') && (imp_Id_Icd && imp_Id_Icd != '')){
						var imppdid = imp_Id_Icd;
						if(imp_Id_Icd && imp_Id_Icd.split('{').length > 1)
							imp_Id_Icd = imp_Id_Icd.split('{')[1].split('}')[0];	
						var souTable = (p_ext_type == 'dwellings')? 'ccv_improvements_dwellings' : ((p_ext_type == "imps") ? 'ccv_improvements' :'ccv_improvements_comm_bldgs');
						var ext_feat_table = ((souTable == 'ccv_improvements') ? 'ccv_improvements_ext_features': ((souTable == 'ccv_improvements_dwellings') ?'ccv_dwellings_ext_features': 'ccv_comm_bldgs_ext_features'));
						var noExists = true;
						if(p_ext_type != '' && p_ext_type != null && p_ext_type != ' '){
							if(p_ext_type == "imps"){
								var noImpExists = false;
								cutVectors.forEach(function(cVect){
									if(!(noImpExists)){
										var ext_detas = cVect.labelFields[0].labelDetails.details;
										$.each(ext_detas,function(exde){
											var esttde = ext_detas[exde];
											if(!((esttde.comp != 0 && esttde.comp != '0') || (esttde.prefix != 0 && esttde.prefix != '0') || (esttde.constr != 0 && esttde.constr != '0') ||  ( esttde.modifier && esttde.modifier != '')) && (esttde.ext_feat_code != 0 && esttde.ext_feat_code != '0')){
											}
											else{
												if(esttde.parent_type && esttde.parent_type == "imps" && esttde.parent_imp_id == imppdid){
													noImpExists = true;
													noExists = false;
												}
											}
										});
									}
								});
								if(notstandalone)
                                    noExists = true;
								if(!noImpExists || notstandalone)
									deleteVect_Ext_Feat_Array.push(_.clone({ext_rowid: exten_rowide, souTable: souTable , pa_imp_id: imp_Id_Icd}))	
								else
									delexvect.doNotDeleteImp =  true;
							}
							else{
								var noDweorCommExists = false;
								cutVectors.forEach(function(cVect){
									if(!(noDweorCommExists)){
										var ext_detas = cVect.labelFields[0].labelDetails.details;
										$.each(ext_detas,function(exde){
											var esttde = ext_detas[exde];
											if(!((esttde.comp != 0 && esttde.comp != '0' && esttde.comp != null && esttde.comp != undefined) || (esttde.prefix != 0 && esttde.prefix != '0' && esttde.prefix != null && esttde.prefix != undefined) || (esttde.constr != 0 && esttde.constr != '0' && esttde.constr != null && esttde.constr != undefined) || (esttde.modifier != '' && esttde.modifier != null && esttde.modifier != undefined)) && (esttde.ext_feat_code != 0 && esttde.ext_feat_code != '0')){
											}
											else{
												if(esttde.parent_type && esttde.parent_type != "imps" && esttde.parent_type != ''){
													noDweorCommExists = true;
													noExists = false;
												}
											}
										});
									}
								});
								if(!(noDweorCommExists)){
									deleteVect_Ext_Feat_Array.push(_.clone({ext_rowid: exten_rowide, souTable: souTable, pa_imp_id: imp_Id_Icd}))
								}						
							}
				
							var p_ICD_Record = parcel[souTable].filter(function(sk){ return ((sk.ROWUID == imp_Id_Icd || sk.improvement_id == imp_Id_Icd) && sk.ParentROWUID == exten_rowide); })[0];
							var ex_ICD_Record = [];
							if(p_ICD_Record)
								ex_ICD_Record = p_ICD_Record[ext_feat_table];
							if(noExists && ex_ICD_Record && ex_ICD_Record.length > 0){
								cutVectors.forEach(function(cect){
									if(!cect.newRecord){
										var ext_detass = cect.labelFields[0].labelDetails.details;
										var extfeatAlones = false;
  										$.each(ext_detass,function(det){
											var lbfeat = ext_detass[det];
											if(((lbfeat.comp != 0 && lbfeat.comp != '0') || (lbfeat.prefix != 0 && lbfeat.prefix != '0') || (lbfeat.constr != 0 && lbfeat.constr != '0')  || (lbfeat.modifier && lbfeat.modifier != '')))
												extfeatAlones = true;
										});
										if(!extfeatAlones){
											$.each(ext_detass,function(exdes){
												var esttde = ext_detass[exdes];
												if(!((esttde.comp != 0 && esttde.comp != '0' && esttde.comp != null && esttde.comp != undefined) || (esttde.prefix != 0 && esttde.prefix != '0' && esttde.prefix != null && esttde.prefix != undefined) || (esttde.constr != 0 && esttde.constr != '0' && esttde.constr != null && esttde.constr != undefined) || (esttde.ext_cover != '' && esttde.ext_cover !='0' && esttde.ext_cover != null && esttde.ext_cover != undefined) || (esttde.modifier != '' && esttde.modifier != null && esttde.modifier != undefined)) && (esttde.ext_feat_code != 0 && esttde.ext_feat_code != '0')){
													if(esttde.parent_imp_id == imppdid && esttde.parent_type == p_ext_type){
														if(delete_Ext_Feat_window_show && delete_Ext_Feat_window_show.length > 0){
															var delRow = delete_Ext_Feat_window_show.filter(function(x) {return x.cuUid == cect.uid})[0];
															if(!delRow)
																delete_Ext_Feat_window_show.push(_.clone({ext_rowidr: exten_rowide, cuUid: cect.uid, impro_id: p_ICD_Record.improvement_id, impro_type: p_ICD_Record.imp_type}));
														}
														else
															delete_Ext_Feat_window_show.push(_.clone({ext_rowidr: exten_rowide, cuUid: cect.uid, impro_id: p_ICD_Record.improvement_id, impro_type: p_ICD_Record.imp_type}));
													}
												}
											});
										}
									}
								});
							}	
						}
					}
				});
	  		}
	 	 }
	 	 else if (ext_length <= 1 && delexvect.sketchType == 'outBuilding' && !delexvect.newRecord) {
	 	 	let outRecord = parcel[ConfigTables.SktOutbuilding].filter((out) => { return (out.ROWUID == extension_rowid) })[0];
	 	 	let out_imprId = outRecord && outRecord.improvement_id && outRecord.improvement_id != ''? outRecord.improvement_id: null;
	 	 	let imp_Id_rowuid = out_imprId && out_imprId.split('{').length > 1?  out_imprId.split('{')[1].split('}')[0]: out_imprId;
	 	 	let imp_record = parcel['ccv_improvements'].filter((i) => { return (i.improvement_id == imp_Id_rowuid || i.ROWUID == imp_Id_rowuid) })[0];
	 	 	if (imp_Id_rowuid && imp_record) {
	 	 		cutVectors.forEach((vt) => {
					if (!vt.newRecord) {
						let lb_details = vt.labelFields[0].labelDetails.details, matched = false;						
						$.each(lb_details, (lb_det) => {
							let det = lb_details[lb_det];
							let imp_Id_in = det.parent_imp_id && det.parent_imp_id.split('{').length > 1?  det.parent_imp_id.split('{')[1].split('}')[0]: det.parent_imp_id;
							if (imp_Id_in == imp_Id_rowuid && det.parent_type == "imps")
								matched = true;
						});
						
						if (matched) {
							if (delete_Ext_Feat_window_show && delete_Ext_Feat_window_show.length > 0) {
								let delRow = delete_Ext_Feat_window_show.filter((x) => { return x.cuUid == vt.uid })[0];
								if (!delRow)
									delete_Ext_Feat_window_show.push(_.clone({ ext_rowidr: exten_rowide, cuUid: vt.uid, impro_id: outRecord.improvement_id, impro_type: outRecord.imp_type }));
							}
							else
								delete_Ext_Feat_window_show.push(_.clone({ ext_rowidr: exten_rowide, cuUid: vt.uid, impro_id: (imp_record.improvement_id? imp_record.improvement_id: ''), impro_type: imp_record.imp_type }));																	
						}
					}
				});
				
				deleteVect_Ext_Feat_Array.push(_.clone({ ext_rowid: exten_rowide, souTable: 'ccv_improvements' , pa_imp_id: imp_Id_rowuid }));
	 	 	}
	 	 }
  	});
   	function delete_exterior_window(delete_Ext_Feat_window){
   		if(delete_Ext_Feat_window.length == 0){
   			RecCretation();
   			return false;
   		}
   		var delete_ext_re = delete_Ext_Feat_window.pop();
   		var cutSketch = sketchApp.sketches.filter(function(dk){return dk.uid == delete_ext_re.ext_rowidr})[0];
  		var cutVector = cutSketch.vectors.filter(function(vk){return vk.uid == delete_ext_re.cuUid})[0];
  		function changeExterior(n_vect_recss, extCheckedValue, extension_recordd, value_select){
  			var extRowuid, ext_featRecord = [];
			var ImpTys = '', impiddd = '', inim = false, misc_Im_Type = '155';
			var labelDatailsFeat = n_vect_recss.labelFields[0].labelDetails.details, upl = false;
			$.each(labelDatailsFeat,function(det){
				if(det != 1 && det != '1'){
					var lbfeat = labelDatailsFeat[det];
					if(lbfeat.ext_feat_code != 0 && lbfeat.ext_feat_code != '0'){
						var ext_split_codess = parseInt(lbfeat.ext_feat_code);
						var ext_tempss = ext_split_codess % 1000;
						var pro_look = Proval_Lookup.filter(function(lkp){ return lkp.tbl_element == ext_tempss && lkp.tbl_type_code == 'Exterior Feature'})[0];
						misc_Im_Type = (pro_look && pro_look.field_2 && pro_look.field_2 != '') ? pro_look.field_2 : misc_Im_Type;
						if (det == 3) upl = true;
						ext_featRecord.push({lb_detail: lbfeat, dett: det});
					}
				}
			});
			var deleteVectorInEdit = [];
			var vectorscopy= JSON.parse(JSON.stringify(n_vect_recss.labelFields));
			var vUid = n_vect_recss.uid;
			deleteVectorInEdit.push({uid : vUid.toString(), labelFields: vectorscopy})
			if(extCheckedValue == 'StandAlone'){
				var caeId = getCategoryFromSourceTable('ccv_improvements').Id;
				let vtString = n_vect_recss.vectorString? n_vect_recss.vectorString: n_vect_recss.toString();
				let points = vtString.split(':')[1].split(' S ');
				let lenWidth = widthLengthCalculator(points);
				/*
				var impFieldArray = [];
				var impFields = ['imp_type', 'improvement_id', 'misc_imp_type', 'attached_flag'];
				var impFieldValue = ["MISC", '1', misc_Im_Type, 'N'];
				impFields.forEach(function(fname){ impFieldArray.push(getDataField(fname, 'ccv_improvements')); });
				var CC_YearStatus = ( ( (clientSettings['FutureYearEnabled'] == '1') && getCategoryFromSourceTable('ccv_improvements').YearPartitionField && (getCategoryFromSourceTable('ccv_improvements').YearPartitionField != '0') )? ( (window.opener? window.opener.showFutureData : showFutureData) ? 'F' : 'A'  ) : "" );
				*/
				var rowidImp = ccTicks();
				misc_Im_Type = ((upl && misc_Im_Type && misc_Im_Type != '')? misc_Im_Type + '/': misc_Im_Type);
				insertNewAuxRecordSketch(parcel.Id, extension_recordd, caeId, rowidImp, {imp_type: "MISC", improvement_id: '1' , misc_imp_type: misc_Im_Type, attached_flag:'N', imp_width: Math.round(lenWidth.imp_width), imp_length:  Math.round(lenWidth.imp_length)}, function (saveNewRecordData) {
					if(saveNewRecordData)
						saveData += saveNewRecordData;
					updateDefaultValues(parcel.Id, rowidImp, extension_recordd.ROWUID, 'ccv_improvements');
                    ext_featRecord.forEach(function(lbt){
                        var levelt = lbt.dett;
                        n_vect_recss.labelFields[0].labelDetails.details[levelt].parent_imp_id = "&%ccv_improvements{" + rowidImp + "}&%";
                        n_vect_recss.labelFields[0].labelDetails.details[levelt].parent_type =  "imps";
				    });
                    sketchApp.deletedVectors.push(_.clone(deleteVectorInEdit[0]));
                    n_vect_recss.newRecord = true;
                    n_vect_recss.exterior_new_reocrd = true;
                    n_vect_recss.isModified = true;
                    delete_exterior_window(delete_Ext_Feat_window);
				});
			}
			else if(extCheckedValue == 'Select_improvements'){
				var chRwid , stablee, impTrue = false;
				if( value_select.split('{').length > 1){
					 chRwid = value_select.split('{')[1].split('}')[0];
					 stablee = value_select.split('{')[0];
						impTrue = true;
				}
				else if(value_select.split('%').length > 1){
					chRwid = value_select.split('%')[1].split('$')[0];
					stablee = value_select.split('%')[0];
					impTrue = false;
				}
				var seg_typef = (stablee == 'ccv_improvements_dwellings')? 'dwellings': ((stablee == 'ccv_improvements_comm_bldgs')? 'comm_bldgs': 'imps');
				ext_featRecord.forEach(function(lbt){
					var levelt = lbt.dett;
					if(impTrue)
						n_vect_recss.labelFields[0].labelDetails.details[levelt].parent_imp_id = "&%" + stablee+ "{" + chRwid + "}&%";
					else
						n_vect_recss.labelFields[0].labelDetails.details[levelt].parent_imp_id = chRwid;
					n_vect_recss.labelFields[0].labelDetails.details[levelt].parent_type = seg_typef;
				});
				sketchApp.deletedVectors.push(_.clone(deleteVectorInEdit[0]));
    			n_vect_recss.newRecord = true;
    			n_vect_recss.exterior_new_reocrd = true;
    			n_vect_recss.isModified = true;
				delete_exterior_window(delete_Ext_Feat_window);
			}
			else if(extCheckedValue == 'DeleteExterior'){
				var cvi = cutSketch.vectors.indexOf(n_vect_recss);
            	sketchApp.deletedVectors.push(n_vect_recss);
            	sketchApp.sketches.filter(function(dk){return dk.uid == delete_ext_re.ext_rowidr})[0].vectors.splice(cvi, 1);
            	delete_exterior_window(delete_Ext_Feat_window)
			}
  		}
  		function deleteExteriorFeature(n_vect, exten_uid, improv_id,improv_type){
			var html = '', ext_change_var = 'StandAlone', valCondition = '';
			$('.Current_vector_details .head').html('Exterior Feature(S) On Deleted Improvement');
			$('.dynamic_prop').html('');
			$('.dynamic_prop').append('<div class="divvectors"></div>');
	 		html += '<span style="width: 500px;justify-content:font-weight: bold;justify-content: center;font-size: 15px;">The deleted Improvement '+improv_id + ':' + improv_type +' has Exterior Feature(S) attached to it.</span>';
			html += '<span style="width: 500px;justify-content:font-weight: bold; justify-content: center; font-size: 15px;">How do you want handle the attached Exterior Feature(s)</span>';
			html += '<div class="Ext_features_delete">';
			html += '<div style="float:left;width:400px;font-size: 15px"><table><tr><td><input type="radio" id="Select_improvements" name="Exterior_Features_delete" value="Select_improvements" style="float:left;width:50px;height:20px"><label for="Select_improvements" style="font-size: 16px;">Select improvements</label></td><td><select style="width: 160px;" type="improvements" class="class_select_improvement" disabled><option value="0">Select improvements</option>';
    		var extension_recordd = parcel['ccv_extensions'].filter(function(sk){ return (sk.ROWUID == exten_uid); })[0];
			var ext_typee = extension_recordd.RorC;
			var DorCTable = (ext_typee == 'R') ? 'ccv_improvements_dwellings': 'ccv_improvements_comm_bldgs';
			var ICDwellRecords = [];
			extension_recordd['ccv_improvements'].forEach(function(im){
				if( ((im.improvement_id != null && im.improvement_id != 'null' && im.improvement_id != '' && im.improvement_id != ' ') || ((im.improvement_id == null || im.improvement_id == '') && im.imp_type && im.CC_RecordStatus == 'I')) && im.CC_Deleted != true)
					ICDwellRecords.push(_.clone({ICDFrecords: im, soTable:'ccv_improvements'}));
			});
			extension_recordd[DorCTable].forEach(function(im){
				if(im.CC_Deleted != true)
					ICDwellRecords.push(_.clone({ICDFrecords: im, soTable: DorCTable}));
			});
			ICDwellRecords.forEach(function(ICDW){
				var icRecord = ICDW.ICDFrecords;
				var tabl = ICDW.soTable;
				var delte_rec = deleteVect_Ext_Feat_Array.filter(function(del_v){return ((del_v.ext_rowid == icRecord.ParentROWUID ) &&( del_v.souTable == tabl) && (del_v.pa_imp_id == icRecord.ROWUID || del_v.pa_imp_id == icRecord.improvement_id))})[0];
				if(!delte_rec){
					if(icRecord.CC_RecordStatus == "I") {
						let ic_imp_id = icRecord.improvement_id? icRecord.improvement_id: '';
						html += '<option value="' + tabl + '{' +icRecord.ROWUID+ '}' + '" value1="' + ic_imp_id + ':' + icRecord.imp_type + '">' + ic_imp_id + ':' + icRecord.imp_type + '</option>';
					}
					else
						html += '<option value="' + tabl + '%' +icRecord.improvement_id+ '$' + '" value1="' +icRecord.improvement_id + ':' + icRecord.imp_type + '">' + icRecord.improvement_id + ':' + icRecord.imp_type + '</option>';
				}
			});
    		html += '</selected></td></tr></table></div>';
			html += '<div style="float:left;width:300px;font-size: 15px"><input type="radio" id="Stand_Alone" name="Exterior_Features_delete" value="StandAlone"  style="float:left;width:50px;height:20px"><label for="Stand_Alone" style="font-size: 16px;">Stand Alone</label></div>';
			html += '<div style="float:left;width:300px;font-size: 15px"><input type="radio" id="DeleteExterior" name="Exterior_Features_delete" value="DeleteExterior"  style="float:left;width:50px;height:20px"><label for="DeleteExterior" style="font-size: 16px;">Delete</label></div>';
			html += '<span class="ext_validation" style="width: 400px;font-weight: bold;margin-left: 68px;display: none; font-size: 15px;">'+ valCondition +'</span>';
			html += '</div>'
			$('.divvectors').append(html);
			$('.skNote').hide();
			$('#Btn_cancel_vector_properties').hide();
			$('#Btn_Save_vector_properties').html('OK ');
			$('#Btn_Save_vector_properties').css('margin-right','180px');
			$('.Current_vector_details').show()
			$('.Current_vector_details').width(550);
			if(ICDwellRecords && ICDwellRecords.length <=0)
				$('#Select_improvements').attr('disabled','disabled');	
			$("input[type='radio'], .class_select_improvement").change(function(){
				$('.ext_validation').hide();
				var chtm = $("input[name='Exterior_Features_delete']:checked").val(); 
				if(chtm == 'Select_improvements')
					$('.class_select_improvement').removeAttr( 'disabled' );
				else
					$('.class_select_improvement').attr('disabled','disabled');
			});
			$('#Btn_Save_vector_properties').unbind(touchClickEvent);
			$('#Btn_Save_vector_properties').bind(touchClickEvent, function () {
				var extCheckedValue = $("input[name='Exterior_Features_delete']:checked").val();
				var select_val, selct_val1;
				if(extCheckedValue){
					if(extCheckedValue == 'Select_improvements'){
						select_val = $('.class_select_improvement').val();
						if(select_val == '0'){
							valCondition = 'Please Choose Improvements From DropDown';
							$('.ext_validation').html(valCondition);
							$('.ext_validation').show();
							return;
						}	
					}
				}
				else{
					valCondition = 'Please Choose Any Option';
					$('.ext_validation').html(valCondition);
					$('.ext_validation').show();
					return;
				}
				$('#Btn_Save_vector_properties').html('Save ');
				$('#Btn_Save_vector_properties').css('margin-right','4px');
				$('#Btn_cancel_vector_properties').show();
				$('.Current_vector_details').hide();
				$('.mask').hide();
				changeExterior(n_vect, extCheckedValue, extension_recordd, select_val);
				return false;
			});
			$('.mask').show();
		}
		deleteExteriorFeature(cutVector, delete_ext_re.ext_rowidr, delete_ext_re.impro_id, delete_ext_re.impro_type);	
   	}
   	delete_exterior_window(delete_Ext_Feat_window_show);
}

function widthLengthCalculator (points) {
	var hLength = 0, RLength = 0, lT = 0, uT = 0, imp_length = 0, imp_width = 0; 
	points.length > 1 && points[1].split(/\s/).forEach(function(p, i) {
		if ( p.trim() != '' ) {
			if(p.split("/").length > 1)
				return;
			else {
				if((p.split("L").length > 1 || p.split("R").length > 1) && lT == 0 ) {
					hLength = p.split("L").length > 1 ? parseFloat(p.split("L")[1]): parseFloat(p.split("R")[1]);
					lT = 1;
				}
				else if((p.split("U").length > 1 || p.split("D").length > 1) && uT == 0 ){
					RLength = p.split("U").length > 1 ? parseFloat(p.split("U")[1]): parseFloat(p.split("D")[1]);
					uT = 1;
				}	
			}

		}
	});
	if (hLength > RLength) {
		imp_length = hLength; imp_width = RLength;
	}
	else {
		imp_length = RLength; imp_width = hLength;
	}
	return ({imp_length: imp_length, imp_width: imp_width});
}
