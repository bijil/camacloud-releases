﻿/*var FTC_Detail_Lookup = [];
var FTC_AddOns_Lookup = [];
var FTC_Floor_Lookup = [];
var FTC_BltasFloor_Lookup = [];
//var Proval_Lookup = [];*/
var bldgSqft = [];
var oldVectorString = [];
var parcel, datafields;

function ApexCartSketchAfterSave(sketchData, callback) {
    if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }
    parcel = window.opener.activeParcel;
    datafields = window.opener.datafields;
    var getDataField = window.opener.getDataField;
    var sketchRowIds = [];
    sketchData.forEach(function (sk) {
        sketchRowIds.push(sk.rowid)
    })
    var vectorIndex = 0, line;
    var sourceFields = [{ source: 'CAMBT', fields: ['BTTRAV', 'ITEM', 'BTLINE'] }, { source: 'CAMBS', fields: ['BSSUB', 'BSYR', 'BSACTL'] }];
    var cambtFieldIds = [];
    var cambsFieldIds = [];
    sourceFields.filter(function (sf) { return sf.source == 'CAMBT' })[0].fields.forEach(function (f) {
        cambtFieldIds.push(getDataField(f, 'CAMBT').Id);
    });
    sourceFields.filter(function (sf) { return sf.source == 'CAMBS' })[0].fields.forEach(function (f) {
        cambsFieldIds.push(getDataField(f, 'CAMBS').Id);
    })
    var isdtr = window.opener ? window.opener.__DTR : (typeof (__DTR) !== 'undefined' ? __DTR : false);
    let isSvApp = window.opener && window.opener.appType == "sv" ? true : (typeof (appType) !== 'undefined' && appType == "sv" ? true : false);
    var _appType = isdtr ? 'DTR' : (isSvApp ? 'SV' : 'QC');
    var saveRequestData = {
        ParcelId: parcel.Id,
        Priority: '',
        AlertMessage: '',
        Data: '',
        recoveryData: '',
        AppraisalType: '',
        activeParcelPhotoEditsdata: '',
        appType: _appType
    };
    var saveDataArray = [];
    var saveData = '';
    var sourceData = [], area, label, year, vectors, newObject = {}, recordValues;
    //deleteing records
    sourceFields.forEach(function (sf) {
        sketchRowIds.forEach(function (rowId) {
            sourceData = parcel.CAMBL.filter(function (x) { return x.ROWUID == rowId })[0][sf.source] ? parcel.CAMBL.filter(function (x) { return x.ROWUID == rowId })[0][sf.source].filter(function (x) { return x.CC_Deleted != true }) : [];
            for (x in sourceData) {
                saveData += '^^^' + parcel.Id + '|' + sourceData[x].ROWUID + '|||' + sf.source + '|delete|' + rowId + '\n';
            }
        });
    });

    //creating new records and updating with new value
    sourceFields.forEach(function (sf) {
        sketchRowIds.forEach(function (rowID) {
            sourceData = parcel.CAMBL.filter(function (x) { return x.ROWUID == rowID })[0];
            vectors = sketchApp.sketches.filter(function (s) { return s.sid == rowID })[0].vectors;
            vectors = vectors.filter(function (x) { return !(x.isFreeFormLineEntries) });
            recordValues = [];
            for (v = 0; vectors[v] != null; v++) {
                area = Math.round(vectors[v].area());
                label = (vectors[v].name).toString().trim();
                year = label.slice(-4)
                if (!isNaN(year))
                    label = label.replace(year, '')
                var pushed = false;
                if (sf.source == 'CAMBS')
                    for (var r in recordValues) {
                        if (recordValues[r].Label == label && recordValues[r].Year == year) {
                            recordValues[r].Area += area;
                            pushed = true;
                        }
                    }
                if (!pushed)
                    recordValues.push({ Label: label, Year: year, Area: area });
            }
            if (sf.source == 'CAMBT') {
                var value = '', recIndex = 0;
                var fields = Object.keys(datafields).map(function (x) { return datafields[x] }).filter(function (x) { return x.SourceTable == 'CAMBT' })
                for (r = 0; r < recordValues.length; r++) {
                    value += recordValues[vectorIndex + r].Label + recordValues[vectorIndex + r].Year + '=' + Math.round(recordValues[vectorIndex + r].Area).toString() + '$ ';
                    if (r % 2 == 1 || r == recordValues.length - 1) {
                        var auxRowuid = ccTicks();
                        value = value.slice(0, -1);
                        if (r == recordValues.length - 1) value = value + '.';
                        ++recIndex;
                        saveData += '^^^' + parcel.Id + '|' + auxRowuid + '|' + rowID + '||CAMBT|new|' + rowID + '\n';
                        fields.forEach(function (item) {
                            var newValue = null;
                            if (item.Id == cambtFieldIds[0])
                                newValue = value;
                            else if (item.Id == cambtFieldIds[1])
                                newValue = sourceData.ITEM;
                            else if (item.Id == cambtFieldIds[2])
                                newValue = recIndex;
                            else if (item.DefaultValue != null && item.DefaultValue != "")
                                newValue = item.DefaultValue;
                            else
                                return;
                            saveData += '^^^' + parcel.Id + '|' + auxRowuid + '|' + item.Id + '||' + newValue + '|edit|' + rowID + '\n';
                        });
                        value = ''
                    }
                }
            }
            else {
                var fields = Object.keys(datafields).map(function (x) { return datafields[x] }).filter(function (x) { return x.SourceTable == 'CAMBS' })
                for (r = 0; r < recordValues.length; r++) {
                    var auxRowuid = ccTicks();
                    saveData += '^^^' + parcel.Id + '|' + auxRowuid + '|' + rowID + '||CAMBS|new|' + rowID + '\n';
                    fields.forEach(function (item) {
                        var newValue = null;
                        if (item.Id == cambsFieldIds[0])
                            newValue = recordValues[r].Label;
                        else if (item.Id == cambsFieldIds[1])
                            newValue = recordValues[r].Year;
                        else if (item.Id == cambsFieldIds[2])
                            newValue = recordValues[r].Area;
                        //else if (item.DefaultValue != null && item.DefaultValue != "")
                        //    newValue = item.DefaultValue;
                        else return;
                        saveData += '^^^' + parcel.Id + '|' + auxRowuid + '|' + item.Id + '||' + newValue + '|edit|' + rowID + '\n';
                    });
                }
            }
        });
    });
    var blFieldId = getDataField('BLSFAC', 'CAMBL').Id
    sketchRowIds.forEach(function (rowId) {
        var area = 0;
        sketchApp.sketches.filter(function (s) { return s.sid == rowId })[0].vectors.filter(function (x) { return !(x.isFreeFormLineEntries) }).forEach(function (v) {
            area += parseFloat(v.area());
        });
        saveData += '^^^' + parcel.Id + '|' + rowId + '|' + blFieldId + '||' + Math.round(area) + '|edit|\n';
    });
    if (saveData != '') {
        saveRequestData.Data = saveData;
        $.ajax({
            url: '/quality/saveparcelchanges.jrq',
            type: "POST",
            dataType: 'json',
            data: saveRequestData,
            success: function (resp) {
                if (resp.status != "OK")
                    console.error(resp);
                if (callback) callback();
            }
        });
    }
    else {
        if (callback) callback();
        return;
    }
}

function HennepinSketchAfterSave(sketchData, callback) {
    if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }
    var isBristol = ((clientSettings.SketchConfig == 'Bristol' || sketchSettings.SketchConfig == 'Bristol') ? true : false);
    var resTable = (isBristol ? 'CCV_CONSTR_RESDEP' : 'CCV_CONSTRSECTION_RES');
    var comTable = (isBristol ? 'CCV_CONSTR_COMDEP' : 'CCV_CONSTRSECTION_COM');
    var changefields = ['SUB_CODE', 'SUB_AREA_GROSS', 'BID', 'PID', 'MNC'];
    parcel = window.opener.activeParcel;
    datafields = window.opener.datafields;
    var getDataField = window.opener.getDataField;
    sketchData.forEach(function (sk) {
        var sketchSourceData = parcel['CCV_BLDG_CONSTR'].filter(function (p) { return p.ROWUID == sk.rowid && p.CC_Deleted != true })[0];
        sk.BID = sketchSourceData.BID;
        if (parcel[resTable] && parcel[resTable].filter(function (res) { return res.BID == sketchSourceData.BID && res.CC_Deleted != true }).length > 0)
            sk.childSource = [resTable, 'CCV_SUBAREA_RES'];
        else if (parcel[comTable] && parcel[comTable].filter(function (com) { return com.BID == sketchSourceData.BID && com.CC_Deleted != true }).length > 0)
            sk.childSource = [comTable, 'CCV_SUBAREA_COM'];
        sk.CNS_USE_APEX = sketchSourceData.CNS_USE_APEX;
    });
    var isdtr = window.opener ? window.opener.__DTR : (typeof (__DTR) !== 'undefined' ? __DTR : false);
    let isSvApp = window.opener && window.opener.appType == "sv" ? true : (typeof (appType) !== 'undefined' && appType == "sv" ? true : false);
    var _appType = isdtr ? 'DTR' : (isSvApp ? 'SV' : 'QC');
    var saveRequestData = {
        ParcelId: parcel.Id,
        Priority: '',
        AlertMessage: '',
        Data: '',
        recoveryData: '',
        AppraisalType: '',
        activeParcelPhotoEditsdata: '',
        appType: _appType
    };
    var saveData = '';
    var parentSourceData = [], area, label, year, vectors, newObject = {}, recordValues;
    //deleteing records
    sketchData.forEach(function (sk) {
        if (sk.childSource && ((sk.CNS_USE_APEX && sk.CNS_USE_APEX.toString() == '1') || (clientSettings.SketchConfig == 'Bristol' || sketchSettings.SketchConfig == 'Bristol'))) {
            var sourceData = parcel[sk.childSource[0]].filter(function (x) { return x.BID == sk.BID })[0][sk.childSource[1]];
            sourceData = sourceData ? sourceData.filter(function (x) { return (x.CC_Deleted.toString() != 'true') }) : [];
            for (x in sourceData) {
                saveData += '^^^' + parcel.Id + '|' + sourceData[x].ROWUID + '|||' + sk.childSource[1] + '|delete|' + sk.rowid + '\n';
            }
        }
    });

    //creating new records and updating with new value
    sketchData.forEach(function (sk) {
        if (sk.childSource && ((sk.CNS_USE_APEX && sk.CNS_USE_APEX.toString() == '1') || (clientSettings.SketchConfig == 'Bristol' || sketchSettings.SketchConfig == 'Bristol'))) {
            parentSourceData = parcel[sk.childSource[0]].filter(function (x) { return x.BID == sk.BID })[0];
            vectors = sketchApp.sketches.filter(function (s) { return s.sid == sk.rowid })[0].vectors;
            vectors = vectors.filter(function (svs) { return !(svs.isFreeFormLineEntries) });
            recordValues = [];
            var sprocessor = function (lbl, area) {
                for (var r in recordValues) {
                    if (recordValues[r][0] == lbl) {
                        recordValues[r][1] += area;
                        pushed = true;
                    }
                }
                if (!pushed)
                    recordValues.push([lbl, area, parentSourceData.BID, parentSourceData.PID, parentSourceData.MNC]);
            }
            for (v = 0; vectors[v] != null; v++) {
                area = (isBristol ? Math.round(parseFloat(vectors[v].area())) : parseFloat(vectors[v].area()));
                label = (vectors[v].code || vectors[v].name).toString().trim();
                var pushed = false;
                if (isBristol) {
                    label.split('/').forEach(function (lbl) {
                        pushed = false;
                        sprocessor(lbl, area);
                    });
                }
                else {
                    pushed = false;
                    sprocessor(label, area);
                }
            }

            var fields = Object.keys(datafields).map(function (x) { return datafields[x] }).filter(function (x) { return x.SourceTable == sk.childSource[1] })
            var fieldIds = [];
            changefields.forEach(function (cf) {
                fieldIds.push(getDataField(cf, sk.childSource[1]).Id);
            })
            var auxRowuid = ccTicks();
            recordValues.forEach(function (rec) {
                auxRowuid += 10;
                saveData += '^^^' + parcel.Id + '|' + auxRowuid + '|' + parentSourceData.ROWUID + '||' + sk.childSource[1] + '|new|' + parentSourceData.ROWUID + '\n';
                fieldIds.forEach(function (fId, index) {
                    if (rec[index] != null)
                        saveData += '^^^' + parcel.Id + '|' + auxRowuid + '|' + fId + '||' + rec[index] + '|edit|' + parentSourceData.ROWUID + '\n';
                });
            });
        }
    });
    if (saveData != '') {
        console.log(saveData);
        saveRequestData.Data = saveData;
        $.ajax({
            url: '/quality/saveparcelchanges.jrq',
            type: "POST",
            dataType: 'json',
            data: saveRequestData,
            success: function (resp) {
                if (resp.status == "OK")
                    if (callback) callback();
            }
        });
    }
    else {
        if (callback) callback();
        return;
    }
}

function Vision6_5SketchAfterSave(sketchData, callback) {
    if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }
    var isBristol = ((clientSettings.SketchConfig == 'Vision6_5' || sketchSettings.SketchConfig == 'Vision6_5') ? true : false);
    var resTable = 'CCV_CONSTR_RESDEP';
    var comTable = 'CCV_CONSTR_COMDEP';
    var condoTable = 'CCV_CONSTR_CDMDEP';
    var condoUnitTable = 'CCV_CONSTR_CDUDEP';
    var sketchSectNumData = _.clone(sketchData);
    var changefields = ['SUB_CODE', 'SUB_AREA_GROSS', 'BID', 'PID', 'MNC', 'SECT_NUM'];
    parcel = window.opener.activeParcel;
    datafields = window.opener.datafields;
    var getDataField = window.opener ? window.opener.getDataField : getDataField;
    var getCategoryFromSourceTable = window.opener ? window.opener.getCategoryFromSourceTable : getCategoryFromSourceTable;
    sketchData.forEach(function (sk) {
        var sketchSourceData = parcel['CCV_BLDG_CONSTR'].filter(function (p) { return p.ROWUID == sk.rowid && p.CC_Deleted != true })[0];
        sk.BID = sketchSourceData.BID;
        if (parcel[resTable] && parcel[resTable].filter(function (res) { return (res.ParentROWUID == sketchSourceData.ROWUID) && res.CC_Deleted != true }).length > 0)
            sk.childSource = [resTable, 'CCV_SUBAREA_RES'];
        else if (parcel[comTable] && parcel[comTable].filter(function (com) { return (com.ParentROWUID == sketchSourceData.ROWUID) && com.CC_Deleted != true }).length > 0)
            sk.childSource = [comTable, 'CCV_SUBAREA_COM'];
        else if (parcel[condoTable] && parcel[condoTable].filter(function (condo) { return (condo.ParentROWUID == sketchSourceData.ROWUID) && condo.CC_Deleted != true }).length > 0)
            sk.childSource = [condoTable, 'CCV_SUBAREA_CDM'];
        else if (parcel[condoUnitTable] && parcel[condoUnitTable].filter(function (conunit) { return (conunit.ParentROWUID == sketchSourceData.ROWUID) && conunit.CC_Deleted != true }).length > 0)
            sk.childSource = [condoUnitTable, 'CCV_SUBAREA_CDU'];
    });
    var isdtr = window.opener ? window.opener.__DTR : (typeof (__DTR) !== 'undefined' ? __DTR : false);
    let isSvApp = window.opener && window.opener.appType == "sv" ? true : (typeof (appType) !== 'undefined' && appType == "sv" ? true : false);
    var _appType = isdtr ? 'DTR' : (isSvApp ? 'SV' : 'QC');
    var saveRequestData = {
        ParcelId: parcel.Id,
        Priority: '',
        AlertMessage: '',
        Data: '',
        recoveryData: '',
        AppraisalType: '',
        activeParcelPhotoEditsdata: '',
        appType: _appType
    };
    var saveData = '';
    var parentSourceData = [], area, label, year, vectors, newObject = {}, recordValues;
    //updating CCSKETCH
    sketchSectNumData.forEach(function (sksectData) {
        var sksectDa = '';
        var CCField = getDataField('CC_SKETCH', 'CCV_BLDG_CONSTR');
        var sktch = sketchApp.sketches.filter(function (tes) { return tes.uid == sksectData.rowid })[0];
        var BldRecord = parcel['CCV_BLDG_CONSTR'].filter(function (skd) { return skd.ROWUID == sksectData.rowid && skd.CC_Deleted != true; })[0];
        sktch.vectors.forEach(function (vect) {
            var tvectString = vect.vectorString;
            if (tvectString) {
                if (vect.wasModified) {
                    var sectn = vect.sectionNum;
                    var temp_str = tvectString.split('[')[1].split(']')[0].split(/\,/);
                    temp_str[1] = sectn;
                    sksectDa = sksectDa + tvectString.split('[')[0] + '[' + temp_str + ']' + tvectString.split(']')[1] + ';';
                }
                else
                    sksectDa = sksectDa + tvectString + ';';
            }
        });
        saveData += '^^^' + parcel.Id + '|' + sksectData.rowid + '|' + CCField.Id + '||' + sksectDa + '|edit|' + '\n';
    });
    //deleteing records
    sketchData.forEach(function (sk) {
        if (sk.childSource && (clientSettings.SketchConfig == 'Vision6_5' || sketchSettings.SketchConfig == 'Vision6_5')) {
            var childRecordData = [];
            var sourceData = parcel[sk.childSource[0]].filter(function (x) { return (x.ParentROWUID == sk.rowid) });
            sourceData = sourceData ? sourceData.filter(function (x) { return (x.CC_Deleted.toString() != 'true') }) : [];
            sourceData.forEach(function (sdata) {
                var chData = sdata[sk.childSource[1]] ? sdata[sk.childSource[1]].filter(function (x) { return (x.CC_Deleted.toString() != 'true') }) : [];
                chData.forEach(function (cdata) {
                    childRecordData.push(cdata);
                });
            });
            for (x in childRecordData) {
                saveData += '^^^' + parcel.Id + '|' + childRecordData[x].ROWUID + '|||' + sk.childSource[1] + '|delete|\n';
            }
        }
    });

    //creating new records and updating with new value
    sketchData.forEach(function (sk) {
        if (sk.childSource && (clientSettings.SketchConfig == 'Vision6_5' || sketchSettings.SketchConfig == 'Vision6_5')) {
            // parentSourceData = parcel[sk.childSource[0]].filter(function (x) { return (x.ParentROWUID == sk.ROWUID || x.BID == sk.BID )})[0];
            vectors = sketchApp.sketches.filter(function (s) { return s.sid == sk.rowid })[0].vectors;
            recordValues = [];
            var secNums;
            var sprocessor = function (lbl, area, secNums) {
                for (var r in recordValues) {
                    if (recordValues[r][0] == lbl && recordValues[r][5] == secNums) {
                        recordValues[r][1] += area;
                        pushed = true;
                    }
                }
                if (!pushed)
                    recordValues.push([lbl, area, parentSourceData.BID, parentSourceData.PID, parentSourceData.MNC, secNums, parentSourceData]);
            }
            for (v = 0; vectors[v] != null; v++) {
                secNums = vectors[v].sectionNum;
                if (secNums.split("{").length > 1) {
                    if (secNums.split("#").length > 1)
                        secNums = secNums.split("#")[1].split("$")[0];
                    else
                        secNums = secNums.split("{")[1].split("}")[0];
                }
                parentSourceData = parcel[sk.childSource[0]].filter(function (spt) { return ((spt.ParentROWUID == sk.rowid) && (spt.SECT_NUM == secNums || spt.ROWUID == secNums)) })[0];
                area = (isBristol ? Math.round(parseFloat(vectors[v].area())) : parseFloat(vectors[v].area()));
                label = (vectors[v].code || vectors[v].name).toString().trim();
                var pushed = false;
                if (isBristol) {
                    label.split('/').forEach(function (lbl) {
                        pushed = false;
                        sprocessor(lbl, area, secNums);
                    });
                }
            }

            var fields = Object.keys(datafields).map(function (x) { return datafields[x] }).filter(function (x) { return x.SourceTable == sk.childSource[1] })
            var fieldIds = [];
            changefields.forEach(function (cf) {
                fieldIds.push(getDataField(cf, sk.childSource[1]).Id);
            })
            var auxRowuid = ccTicks();
            recordValues.forEach(function (rec) {
                auxRowuid += 10;
                var parentSource = rec[6];
                saveData += '^^^' + parcel.Id + '|' + auxRowuid + '|' + parentSource.ROWUID + '||' + sk.childSource[1] + '|new|' + parentSource.ROWUID + '\n';
                fieldIds.forEach(function (fId, index) {
                    if (rec[index] != null)
                        saveData += '^^^' + parcel.Id + '|' + auxRowuid + '|' + fId + '||' + rec[index] + '|edit|' + parentSource.ROWUID + '\n';
                });
            });
        }
    });
    if (saveData != '') {
        console.log(saveData);
        saveRequestData.Data = saveData;
        $.ajax({
            url: '/quality/saveparcelchanges.jrq',
            type: "POST",
            dataType: 'json',
            data: saveRequestData,
            success: function (resp) {
                if (resp.status == "OK")
                    if (callback) callback();
            }
        });
    }
    else {
        if (callback) callback();
        return;
    }
}

function Vision75ApexSketchAfterSaveNew(sketchData, callback) {
    if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }

    var isBristol = ((clientSettings.SketchConfig == 'Bristol' || sketchSettings.SketchConfig == 'Bristol') ? true : false);
    var resTable = (isBristol ? 'CCV_CONSTR_RESDEP' : 'CCV_CONSTRSECTION_RES');
    var comTable = (isBristol ? 'CCV_CONSTR_COMDEP' : 'CCV_CONSTRSECTION_COM');
    var condoTable = 'CCV_CONSTRSECTION_CDM';
    var condoUnitTable = 'CCV_CONSTRSECTION_CDU';
    var sketchSectNumData = _.clone(sketchData);
    var changefields = ['SUB_CODE', 'SUB_AREA_GROSS', 'BID', 'PID', 'MNC', 'SECT_NUM'];
    parcel = window.opener.activeParcel;
    datafields = window.opener.datafields;
    var getDataField = window.opener.getDataField;
    var getCategoryFromSourceTable = window.opener ? window.opener.getCategoryFromSourceTable : getCategoryFromSourceTable;
    sketchData.forEach(function (sk) {
        var sketchSourceData = parcel['CCV_BLDG_CONSTR'].filter(function (p) { return p.ROWUID == sk.rowid && p.CC_Deleted != true })[0];
        sk.BID = sketchSourceData.BID;

        if (parcel[resTable] && parcel[resTable].filter(function (res) { return (res.ParentROWUID == sketchSourceData.ROWUID) && res.CC_Deleted != true }).length > 0)
            sk.childSource = [resTable, 'CCV_SUBAREA_RES'];
        else if (parcel[comTable] && parcel[comTable].filter(function (com) { return (com.ParentROWUID == sketchSourceData.ROWUID) && com.CC_Deleted != true }).length > 0)
            sk.childSource = [comTable, 'CCV_SUBAREA_COM'];
        else if (parcel[condoTable] && parcel[condoTable].filter(function (condo) { return (condo.ParentROWUID == sketchSourceData.ROWUID) && condo.CC_Deleted != true }).length > 0)
            sk.childSource = [condoTable, 'CCV_SUBAREA_CDM'];
        else if (parcel[condoUnitTable] && parcel[condoUnitTable].filter(function (conunit) { return (conunit.ParentROWUID == sketchSourceData.ROWUID) && conunit.CC_Deleted != true }).length > 0)
            sk.childSource = [condoUnitTable, 'CCV_SUBAREA_CDU'];

        sk.CNS_USE_APEX = sketchSourceData.CNS_USE_APEX;
    });

    var isdtr = window.opener ? window.opener.__DTR : (typeof (__DTR) !== 'undefined' ? __DTR : false);
    let isSvApp = window.opener && window.opener.appType == "sv" ? true : (typeof (appType) !== 'undefined' && appType == "sv" ? true : false);
    var _appType = isdtr ? 'DTR' : (isSvApp ? 'SV' : 'QC');
    var saveRequestData = {
        ParcelId: parcel.Id,
        Priority: '',
        AlertMessage: '',
        Data: '',
        recoveryData: '',
        AppraisalType: '',
        activeParcelPhotoEditsdata: '',
        appType: _appType
    };

    var saveData = '';
    var parentSourceData = [], area, label, year, vectors, newObject = {}, recordValues;
    //updating CCSKETCH
    let processor = function () {
        sketchSectNumData.forEach(function (sksectData) {
            var sksectDa = '';
            var CCField = getDataField('CC_SKETCH', 'CCV_BLDG_CONSTR');
            var sktch = sketchApp.sketches.filter(function (tes) { return tes.uid == sksectData.rowid })[0];
            var BldRecord = parcel['CCV_BLDG_CONSTR'].filter(function (skd) { return skd.ROWUID == sksectData.rowid && skd.CC_Deleted != true; })[0];
            sktch.vectors.forEach(function (vect) {
                var tvectString = vect.vectorString;
                if (tvectString) {
                    if (vect.wasModified) {
                        var sectn = vect.sectionNum;
                        var temp_str = tvectString.split('[')[1].split(']')[0].split(/\,/);
                        temp_str[1] = sectn;
                        sksectDa = sksectDa + tvectString.split('[')[0] + '[' + temp_str + ']' + tvectString.split(']')[1] + ';';
                    }
                    else
                        sksectDa = sksectDa + tvectString + ';';
                }
            });
            saveData += '^^^' + parcel.Id + '|' + sksectData.rowid + '|' + CCField.Id + '||' + sksectDa + '|edit|' + '\n';
        });
        //deleteing records
        sketchData.forEach(function (sk) {
            if (sk.childSource && ((sk.CNS_USE_APEX != null ? sk.CNS_USE_APEX.toString() : true) || (clientSettings.SketchConfig == 'Bristol' || sketchSettings.SketchConfig == 'Bristol'))) {
                var childRecordData = [];
                var sourceData = parcel[sk.childSource[0]].filter(function (x) { return (x.ParentROWUID == sk.rowid) });
                sourceData = sourceData ? sourceData.filter(function (x) { return (x.CC_Deleted.toString() != 'true') }) : [];
                sourceData.forEach(function (sdata) {
                    var chData = sdata[sk.childSource[1]] ? sdata[sk.childSource[1]].filter(function (x) { return (x.CC_Deleted.toString() != 'true') }) : [];
                    chData.forEach(function (cdata) {
                        childRecordData.push(cdata);
                    });
                });
                for (x in childRecordData) {
                    saveData += '^^^' + parcel.Id + '|' + childRecordData[x].ROWUID + '|||' + sk.childSource[1] + '|delete|\n';
                }
            }
        });

        //creating new records and updating with new value
        sketchData.forEach(function (sk) {
            if (sk.childSource && ((sk.CNS_USE_APEX != null ? sk.CNS_USE_APEX.toString() : true) || (clientSettings.SketchConfig == 'Bristol' || sketchSettings.SketchConfig == 'Bristol'))) {
                //parentSourceData = parcel[sk.childSource[0]].filter( function ( x ) { return x.BID == sk.BID } )[0];
                vectors = sketchApp.sketches.filter(function (s) { return s.sid == sk.rowid })[0].vectors;
                recordValues = [];
                var secNums;
                var sprocessor = function (lbl, area, secNums) {
                    for (var r in recordValues) {
                        if (recordValues[r][0] == lbl && recordValues[r][5] == secNums) {
                            recordValues[r][1] += area;
                            pushed = true;
                        }
                    }
                    if (!pushed)
                        recordValues.push([lbl, area, parentSourceData.BID, parentSourceData.PID, parentSourceData.MNC, secNums, parentSourceData]);
                }
                for (v = 0; vectors[v] != null; v++) {
                    secNums = vectors[v].sectionNum;
                    if (secNums.split("{").length > 1) {
                        if (secNums.split("#").length > 1)
                            secNums = secNums.split("#")[1].split("$")[0];
                        else
                            secNums = secNums.split("{")[1].split("}")[0];
                    }
                    parentSourceData = parcel[sk.childSource[0]].filter(function (spt) { return ((spt.ParentROWUID == sk.rowid) && (spt.SECT_NUM == secNums || spt.ROWUID == secNums)) })[0];
                    area = vectors[v].fixedArea && vectors[v].fixedArea != 0 && vectors[v].fixedArea != -1 ? parseFloat(vectors[v].fixedArea) : (isBristol ? Math.round(parseFloat(vectors[v].area())) : parseFloat(vectors[v].area()));
                    label = (vectors[v].code || vectors[v].name).toString().trim();
                    var pushed = false;
                    label.split('/').forEach(function (lbl) {
                        pushed = false;
                        sprocessor(lbl, area, secNums);
                    });
                }

                var fields = Object.keys(datafields).map(function (x) { return datafields[x] }).filter(function (x) { return x.SourceTable == sk.childSource[1] })
                var fieldIds = [];
                changefields.forEach(function (cf) {
                    fieldIds.push(getDataField(cf, sk.childSource[1]).Id);
                })
                var auxRowuid = ccTicks();
                recordValues.forEach(function (rec) {
                    auxRowuid += 10;
                    var parentSource = rec[6];
                    saveData += '^^^' + parcel.Id + '|' + auxRowuid + '|' + parentSource.ROWUID + '||' + sk.childSource[1] + '|new|' + parentSource.ROWUID + '\n';
                    fieldIds.forEach(function (fId, index) {
                        if (rec[index] != null)
                            saveData += '^^^' + parcel.Id + '|' + auxRowuid + '|' + fId + '||' + rec[index] + '|edit|' + parentSource.ROWUID + '\n';
                    });
                });
            }
        });
        if (saveData != '') {
            console.log(saveData);
            saveRequestData.Data = saveData;
            $.ajax({
                url: '/quality/saveparcelchanges.jrq',
                type: "POST",
                dataType: 'json',
                data: saveRequestData,
                success: function (resp) {
                    if (resp.status == "OK")
                        if (callback) callback();
                }
            });
        }
        else {
            if (callback) callback();
            return;
        }
    }

    if (visionPCIs.length > 0) {
        visionCappingRecordCreation((sd) => {
            saveData += sd;
            processor();
        });
    }
    else processor();

}

function Vision75ApexSketchAfterSave(sketchData, callback) {
    if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }
    var isBristol = ((clientSettings.SketchConfig == 'Bristol' || sketchSettings.SketchConfig == 'Bristol') ? true : false);
    var resTable = (isBristol ? 'CCV_CONSTR_RESDEP' : 'CCV_CONSTRSECTION_RES');
    var comTable = (isBristol ? 'CCV_CONSTR_COMDEP' : 'CCV_CONSTRSECTION_COM');
    var changefields = ['SUB_CODE', 'SUB_AREA_GROSS', 'BID', 'PID', 'MNC'];
    parcel = window.opener.activeParcel;
    datafields = window.opener.datafields;
    var getDataField = window.opener.getDataField;
    sketchData.forEach(function (sk) {
        var sketchSourceData = parcel['CCV_BLDG_CONSTR'].filter(function (p) { return p.ROWUID == sk.rowid && p.CC_Deleted != true })[0];
        sk.BID = sketchSourceData.BID;
        if (parcel[resTable] && parcel[resTable].filter(function (res) { return res.BID == sketchSourceData.BID && res.CC_Deleted != true }).length > 0)
            sk.childSource = [resTable, 'CCV_SUBAREA_RES'];
        else if (parcel[comTable] && parcel[comTable].filter(function (com) { return com.BID == sketchSourceData.BID && com.CC_Deleted != true }).length > 0)
            sk.childSource = [comTable, 'CCV_SUBAREA_COM'];
        sk.CNS_USE_APEX = sketchSourceData.CNS_USE_APEX;
    });
    var isdtr = window.opener ? window.opener.__DTR : (typeof (__DTR) !== 'undefined' ? __DTR : false);
    let isSvApp = window.opener && window.opener.appType == "sv" ? true : (typeof (appType) !== 'undefined' && appType == "sv" ? true : false);
    var _appType = isdtr ? 'DTR' : (isSvApp ? 'SV' : 'QC');
    var saveRequestData = {
        ParcelId: parcel.Id,
        Priority: '',
        AlertMessage: '',
        Data: '',
        recoveryData: '',
        AppraisalType: '',
        activeParcelPhotoEditsdata: '',
        appType: _appType
    };
    var saveData = '';
    var parentSourceData = [], area, label, year, vectors, newObject = {}, recordValues;
    //deleteing records
    sketchData.forEach(function (sk) {
        if (sk.childSource && ((sk.CNS_USE_APEX != null ? sk.CNS_USE_APEX.toString() : true) || (clientSettings.SketchConfig == 'Bristol' || sketchSettings.SketchConfig == 'Bristol'))) {
            var sourceData = parcel[sk.childSource[0]].filter(function (x) { return x.BID == sk.BID })[0][sk.childSource[1]];
            sourceData = sourceData ? sourceData.filter(function (x) { return (x.CC_Deleted.toString() != 'true') }) : [];
            for (x in sourceData) {
                saveData += '^^^' + parcel.Id + '|' + sourceData[x].ROWUID + '|||' + sk.childSource[1] + '|delete|' + sk.rowid + '\n';
            }
        }
    });

    //creating new records and updating with new value
    sketchData.forEach(function (sk) {
        if (sk.childSource && ((sk.CNS_USE_APEX != null ? sk.CNS_USE_APEX.toString() : true) || (clientSettings.SketchConfig == 'Bristol' || sketchSettings.SketchConfig == 'Bristol'))) {
            parentSourceData = parcel[sk.childSource[0]].filter(function (x) { return x.BID == sk.BID })[0];
            vectors = sketchApp.sketches.filter(function (s) { return s.sid == sk.rowid })[0].vectors;
            recordValues = [];
            var sprocessor = function (lbl, area) {
                for (var r in recordValues) {
                    if (recordValues[r][0] == lbl) {
                        recordValues[r][1] += area;
                        pushed = true;
                    }
                }
                if (!pushed)
                    recordValues.push([lbl, area, parentSourceData.BID, parentSourceData.PID, parentSourceData.MNC]);
            }
            for (v = 0; vectors[v] != null; v++) {
                area = vectors[v].fixedArea && vectors[v].fixedArea != 0 && vectors[v].fixedArea != -1 ? parseInt(vectors[v].fixedArea) : (isBristol ? Math.round(parseFloat(vectors[v].area())) : parseFloat(vectors[v].area()));
                label = (vectors[v].code || vectors[v].name).toString().trim();
                var pushed = false;
                //if ( isBristol )               {
                label.split('/').forEach(function (lbl) {
                    pushed = false;
                    sprocessor(lbl, area);
                });
                //}
                /*
                else                {
                    pushed = false;
                    sprocessor( label, area );
                }
                */
            }

            var fields = Object.keys(datafields).map(function (x) { return datafields[x] }).filter(function (x) { return x.SourceTable == sk.childSource[1] })
            var fieldIds = [];
            changefields.forEach(function (cf) {
                fieldIds.push(getDataField(cf, sk.childSource[1]).Id);
            })
            var auxRowuid = ccTicks();
            recordValues.forEach(function (rec) {
                auxRowuid += 10;
                saveData += '^^^' + parcel.Id + '|' + auxRowuid + '|' + parentSourceData.ROWUID + '||' + sk.childSource[1] + '|new|' + parentSourceData.ROWUID + '\n';
                fieldIds.forEach(function (fId, index) {
                    if (rec[index] != null)
                        saveData += '^^^' + parcel.Id + '|' + auxRowuid + '|' + fId + '||' + rec[index] + '|edit|' + parentSourceData.ROWUID + '\n';
                });
            });
        }
    });
    if (saveData != '') {
        console.log(saveData);
        saveRequestData.Data = saveData;
        $.ajax({
            url: '/quality/saveparcelchanges.jrq',
            type: "POST",
            dataType: 'json',
            data: saveRequestData,
            success: function (resp) {
                if (resp.status == "OK")
                    if (callback) callback();
            }
        });
    }
    else {
        if (callback) callback();
        return;
    }
}

function Vision8ApexSketchAfterSave(sketchData, callback) {
    if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }

    var resTable = 'CCV_CONSTR_RESDEP';
    var comTable = 'CCV_CONSTR_COMDEP';
    var changefields = ['SUB_CODE', 'SUB_AREA_GROSS', 'BID', 'PID', 'MNC'];
    parcel = window.opener.activeParcel;
    datafields = window.opener.datafields;
    var getDataField = window.opener.getDataField;
    sketchData.forEach(function (sk) {
        var sketchSourceData = parcel['CCV_BLDG_CONSTR'].filter(function (p) { return p.ROWUID == sk.rowid && p.CC_Deleted != true })[0];
        sk.BID = sketchSourceData.BID;
        if (parcel[resTable] && parcel[resTable].filter(function (res) { return res.BID == sketchSourceData.BID && res.CC_Deleted != true }).length > 0)
            sk.childSource = [resTable, 'CCV_SUBAREA_RES'];
        else if (parcel[comTable] && parcel[comTable].filter(function (com) { return com.BID == sketchSourceData.BID && com.CC_Deleted != true }).length > 0)
            sk.childSource = [comTable, 'CCV_SUBAREA_COM'];
        sk.CNS_USE_APEX = sketchSourceData.CNS_USE_APEX;
    });
    var isdtr = window.opener ? window.opener.__DTR : (typeof (__DTR) !== 'undefined' ? __DTR : false);
    let isSvApp = window.opener && window.opener.appType == "sv" ? true : (typeof (appType) !== 'undefined' && appType == "sv" ? true : false);
    var _appType = isdtr ? 'DTR' : (isSvApp ? 'SV' : 'QC');
    var saveRequestData = {
        ParcelId: parcel.Id,
        Priority: '',
        AlertMessage: '',
        Data: '',
        recoveryData: '',
        AppraisalType: '',
        activeParcelPhotoEditsdata: '',
        appType: _appType
    };
    var saveData = '';
    var parentSourceData = [], area, label, year, vectors, newObject = {}, recordValues;
    //deleteing records
    sketchData.forEach(function (sk) {
        if (sk.childSource && ((sk.CNS_USE_APEX != null ? sk.CNS_USE_APEX.toString() : true) || (clientSettings.SketchConfig == 'Bristol' || sketchSettings.SketchConfig == 'Bristol'))) {
            var sourceData = parcel[sk.childSource[0]].filter(function (x) { return x.BID == sk.BID })[0][sk.childSource[1]];
            sourceData = sourceData ? sourceData.filter(function (x) { return (x.CC_Deleted.toString() != 'true') }) : [];
            for (x in sourceData) {
                saveData += '^^^' + parcel.Id + '|' + sourceData[x].ROWUID + '|||' + sk.childSource[1] + '|delete|' + sk.rowid + '\n';
            }
        }
    });

    //creating new records and updating with new value
    sketchData.forEach(function (sk) {
        if (sk.childSource && ((sk.CNS_USE_APEX != null ? sk.CNS_USE_APEX.toString() : true) || (clientSettings.SketchConfig == 'Bristol' || sketchSettings.SketchConfig == 'Bristol'))) {
            parentSourceData = parcel[sk.childSource[0]].filter(function (x) { return x.BID == sk.BID })[0];
            vectors = sketchApp.sketches.filter(function (s) { return s.sid == sk.rowid })[0].vectors;
            recordValues = [];
            var sprocessor = function (lbl, area) {
                for (var r in recordValues) {
                    if (recordValues[r][0] == lbl) {
                        recordValues[r][1] += area;
                        pushed = true;
                    }
                }
                if (!pushed)
                    recordValues.push([lbl, area, parentSourceData.BID, parentSourceData.PID, parentSourceData.MNC]);
            }
            for (v = 0; vectors[v] != null; v++) {
                area = vectors[v].fixedArea && vectors[v].fixedArea != 0 && vectors[v].fixedArea != -1 ? parseInt(vectors[v].fixedArea) : (parseFloat(vectors[v].area()));
                label = (vectors[v].code || vectors[v].name).toString().trim();
                var pushed = false;
                label.split('/').forEach(function (lbl) {
                    pushed = false;
                    sprocessor(lbl, area);
                });
            }

            var fields = Object.keys(datafields).map(function (x) { return datafields[x] }).filter(function (x) { return x.SourceTable == sk.childSource[1] })
            var fieldIds = [];
            changefields.forEach(function (cf) {
                fieldIds.push(getDataField(cf, sk.childSource[1]).Id);
            })
            var auxRowuid = ccTicks();
            recordValues.forEach(function (rec) {
                auxRowuid += 10;
                saveData += '^^^' + parcel.Id + '|' + auxRowuid + '|' + parentSourceData.ROWUID + '||' + sk.childSource[1] + '|new|' + parentSourceData.ROWUID + '\n';
                fieldIds.forEach(function (fId, index) {
                    if (rec[index] != null)
                        saveData += '^^^' + parcel.Id + '|' + auxRowuid + '|' + fId + '||' + rec[index] + '|edit|' + parentSourceData.ROWUID + '\n';
                });
            });
        }
    });
    if (saveData != '') {
        console.log(saveData);
        saveRequestData.Data = saveData;
        $.ajax({
            url: '/quality/saveparcelchanges.jrq',
            type: "POST",
            dataType: 'json',
            data: saveRequestData,
            success: function (resp) {
                if (resp.status == "OK")
                    if (callback) callback();
            }
        });
    }
    else {
        if (callback) callback();
        return;
    }
}

function Vision8ApexSketchAfterSaveNew(sketchData, callback) {
    if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }

    var resTable = 'CCV_CONSTR_RESDEP';
    var comTable = 'CCV_CONSTR_COMDEP';
    var condoTable = 'CCV_CONSTR_CDMDEP';
    var condoUnitTable = 'CCV_CONSTR_CDUDEP';
    var sketchSectNumData = _.clone(sketchData);
    var changefields = ['SUB_CODE', 'SUB_AREA_GROSS', 'BID', 'PID', 'MNC', 'SECT_NUM'];
    parcel = window.opener.activeParcel;
    datafields = window.opener.datafields;
    var getDataField = window.opener.getDataField;
    var getCategoryFromSourceTable = window.opener ? window.opener.getCategoryFromSourceTable : getCategoryFromSourceTable;
    sketchData.forEach(function (sk) {
        var sketchSourceData = parcel['CCV_BLDG_CONSTR'].filter(function (p) { return p.ROWUID == sk.rowid && p.CC_Deleted != true })[0];
        sk.BID = sketchSourceData.BID;
        if (parcel[resTable] && parcel[resTable].filter(function (res) { return (res.ParentROWUID == sketchSourceData.ROWUID) && res.CC_Deleted != true }).length > 0)
            sk.childSource = [resTable, 'CCV_SUBAREA_RES'];
        else if (parcel[comTable] && parcel[comTable].filter(function (com) { return (com.ParentROWUID == sketchSourceData.ROWUID) && com.CC_Deleted != true }).length > 0)
            sk.childSource = [comTable, 'CCV_SUBAREA_COM'];
        else if (parcel[condoTable] && parcel[condoTable].filter(function (condo) { return (condo.ParentROWUID == sketchSourceData.ROWUID) && condo.CC_Deleted != true }).length > 0)
            sk.childSource = [condoTable, 'CCV_SUBAREA_CDM'];
        else if (parcel[condoUnitTable] && parcel[condoUnitTable].filter(function (conunit) { return (conunit.ParentROWUID == sketchSourceData.ROWUID) && conunit.CC_Deleted != true }).length > 0)
            sk.childSource = [condoUnitTable, 'CCV_SUBAREA_CDU'];
        sk.CNS_USE_APEX = sketchSourceData.CNS_USE_APEX;
    });
    var isdtr = window.opener ? window.opener.__DTR : (typeof (__DTR) !== 'undefined' ? __DTR : false);
    let isSvApp = window.opener && window.opener.appType == "sv" ? true : (typeof (appType) !== 'undefined' && appType == "sv" ? true : false);
    var _appType = isdtr ? 'DTR' : (isSvApp ? 'SV' : 'QC');
    var saveRequestData = {
        ParcelId: parcel.Id,
        Priority: '',
        AlertMessage: '',
        Data: '',
        recoveryData: '',
        AppraisalType: '',
        activeParcelPhotoEditsdata: '',
        appType: _appType
    };
    var saveData = '';
    var parentSourceData = [], area, label, year, vectors, newObject = {}, recordValues;
    //updating CCSKETCH
    sketchSectNumData.forEach(function (sksectData) {
        var sksectDa = '';
        var CCField = getDataField('CC_SKETCH', 'CCV_BLDG_CONSTR');
        var sktch = sketchApp.sketches.filter(function (tes) { return tes.uid == sksectData.rowid })[0];
        var BldRecord = parcel['CCV_BLDG_CONSTR'].filter(function (skd) { return skd.ROWUID == sksectData.rowid && skd.CC_Deleted != true; })[0];
        sktch.vectors.forEach(function (vect) {
            var tvectString = vect.vectorString;
            if (tvectString) {
                if (vect.wasModified) {
                    var sectn = vect.sectionNum;
                    var temp_str = tvectString.split('[')[1].split(']')[0].split(/\,/);
                    temp_str[1] = sectn;
                    sksectDa = sksectDa + tvectString.split('[')[0] + '[' + temp_str + ']' + tvectString.split(']')[1] + ';';
                }
                else
                    sksectDa = sksectDa + tvectString + ';';
            }
        });

        let nts = sktch.notes.filter((vn) => { return !vn.isDeleted });
        nts.forEach((nt) => {
            if (nt.noteData) sksectDa += nt.noteData;
        });

        saveData += '^^^' + parcel.Id + '|' + sksectData.rowid + '|' + CCField.Id + '||' + sksectDa + '|edit|' + '\n';
    });
    //deleteing records
    sketchData.forEach(function (sk) {
        if (sk.childSource && ((sk.CNS_USE_APEX != null ? sk.CNS_USE_APEX.toString() : true) || (clientSettings.SketchConfig == 'Bristol' || sketchSettings.SketchConfig == 'Bristol'))) {
            var childRecordData = [];
            var sourceData = parcel[sk.childSource[0]].filter(function (x) { return (x.ParentROWUID == sk.rowid) });
            sourceData = sourceData ? sourceData.filter(function (x) { return (x.CC_Deleted.toString() != 'true') }) : [];
            sourceData.forEach(function (sdata) {
                var chData = sdata[sk.childSource[1]] ? sdata[sk.childSource[1]].filter(function (x) { return (x.CC_Deleted.toString() != 'true') }) : [];
                chData.forEach(function (cdata) {
                    childRecordData.push(cdata);
                });
            });
            for (x in childRecordData) {
                saveData += '^^^' + parcel.Id + '|' + childRecordData[x].ROWUID + '|||' + sk.childSource[1] + '|delete|\n';
            }
        }
    });

    //creating new records and updating with new value
    sketchData.forEach(function (sk) {
        if (sk.childSource && ((sk.CNS_USE_APEX != null ? sk.CNS_USE_APEX.toString() : true) || (clientSettings.SketchConfig == 'Bristol' || sketchSettings.SketchConfig == 'Bristol'))) {
            vectors = sketchApp.sketches.filter(function (s) { return s.sid == sk.rowid })[0].vectors;
            recordValues = [];
            var secNums;
            var sprocessor = function (lbl, area, secNums) {
                for (var r in recordValues) {
                    if (recordValues[r][0] == lbl && recordValues[r][5] == secNums) {
                        recordValues[r][1] += area;
                        pushed = true;
                    }
                }
                if (!pushed)
                    recordValues.push([lbl, area, parentSourceData.BID, parentSourceData.PID, parentSourceData.MNC, secNums, parentSourceData]);
            }
            for (v = 0; vectors[v] != null; v++) {
                secNums = vectors[v].sectionNum;
                if (secNums.split("{").length > 1) {
                    if (secNums.split("#").length > 1)
                        secNums = secNums.split("#")[1].split("$")[0];
                    else
                        secNums = secNums.split("{")[1].split("}")[0];
                }
                parentSourceData = parcel[sk.childSource[0]].filter(function (spt) { return ((spt.ParentROWUID == sk.rowid) && (spt.SECT_NUM == secNums || spt.ROWUID == secNums)) })[0];
                area = vectors[v].fixedArea && vectors[v].fixedArea != 0 && vectors[v].fixedArea != -1? parseInt(vectors[v].fixedArea) : (parseFloat(vectors[v].area()));
                label = (vectors[v].code || vectors[v].name).toString().trim();
                var pushed = false;
                label.split('/').forEach(function (lbl) {
                    pushed = false;
                    sprocessor(lbl, area, secNums);
                });
            }

            var fields = Object.keys(datafields).map(function (x) { return datafields[x] }).filter(function (x) { return x.SourceTable == sk.childSource[1] })
            var fieldIds = [];
            changefields.forEach(function (cf) {
                fieldIds.push(getDataField(cf, sk.childSource[1]).Id);
            })
            var auxRowuid = ccTicks();
            recordValues.forEach(function (rec) {
                auxRowuid += 10;
                var parentSource = rec[6];
                saveData += '^^^' + parcel.Id + '|' + auxRowuid + '|' + parentSource.ROWUID + '||' + sk.childSource[1] + '|new|' + parentSource.ROWUID + '\n';
                fieldIds.forEach(function (fId, index) {
                    if (rec[index] != null)
                        saveData += '^^^' + parcel.Id + '|' + auxRowuid + '|' + fId + '||' + rec[index] + '|edit|' + parentSource.ROWUID + '\n';
                });
            });
        }
    });
    if (saveData != '') {
        console.log(saveData);
        saveRequestData.Data = saveData;
        $.ajax({
            url: '/quality/saveparcelchanges.jrq',
            type: "POST",
            dataType: 'json',
            data: saveRequestData,
            success: function (resp) {
                if (resp.status == "OK")
                    if (callback) callback();
            }
        });
    }
    else {
        if (callback) callback();
        return;
    }
}

function HennepinSketchBeforeSave(sketches, callback) {
    var valid = { isValid: true, message: 'The following label(s) are <b>incorrect</b>: ', auditTrailEntry: 'The following label(s) are incorrect: ' };
    var LabelLookup = []
    var invalid_label_error = 'Sketch labels are invalid, sketch changes were saved but the SUBAREA information was not updated.';
    var multiple_sectnum_error = 'There are multiple Sections for this Building. The sketch changes were saved but the SubArea information was not updated.';
    var multiple_sectnum_msg = 'There are multiple Sections for this Building. The sketch changes will be saved but the SubArea information will not be updated.';
    var parcel = window.opener.activeParcel;
    if (sketches === undefined || sketches == null) {
        if (callback) callback(true);
        return;
    }

    sketches.forEach(function (sk) {
        var svectors = sk.vectors.filter(function (x) { return !(x.isFreeFormLineEntries) });
        svectors.forEach(function (v) {
            LabelLookup = sketchApp.external.lookup[v.vectorConfig.LabelLookup]
            if (Object.keys(LabelLookup).filter(function (label) { return label == v.code; }).length == 0) {
                valid.isValid = valid.isValid && false;
                valid.message += (v.name + ', ');
                valid.auditTrailEntry += (v.name + ', ');
            }
        });
    });
    valid.message = valid.message.substring(0, valid.message.length - 2) + '. Tap "OK" and select the sketch segment, press "Edit Label" and select a sketch label from one in the list.';
    valid.message += '<br/><br/><span style="color:red">If you do not correct these labels your Subarea information will not be updated.</span>';
    valid.auditTrailEntry = valid.auditTrailEntry.substring(0, valid.auditTrailEntry.length - 2) + '. If you do not correct these labels your Subarea information will not be updated.'
    var multipleSectnum_validator = function () {
        var hasDuplicate = false;
        sketches.forEach(function (sk) {
            var resData = parcel['CCV_BLDG_CONSTR'].filter(function (bldg) { return bldg.ROWUID == sk.sid; })[0]['CCV_CONSTRSECTION_RES'];
            var comData = parcel['CCV_BLDG_CONSTR'].filter(function (bldg) { return bldg.ROWUID == sk.sid; })[0]['CCV_CONSTRSECTION_COM'];
            var sectNum = [];
            if (resData && resData.length > 0)
                sectNum = resData.map(function (res) { return res.SECT_NUM });
            else if (comData && comData.length > 0)
                sectNum = comData.map(function (com) { return com.SECT_NUM });

            hasDuplicate = (new Set(sectNum).size !== sectNum.length);
            //There are multiple Sections for this Building. The sketch changes will be saved but the SubArea information will not be updated.
        });
        return hasDuplicate;
    }
    if (multipleSectnum_validator()) {
        messageBox(multiple_sectnum_msg, ['OK'], function () {
            if (callback) callback(false, true, multiple_sectnum_error);
        });
    }
    else if (!valid.isValid) {
        messageBox(valid.message, ['OK'], function () {
            if (callback) callback(false, true, null, valid.auditTrailEntry);
        });

        //else {
        //    if (multipleSectnum_validator()) {
        //        if (alert(valid.message))
        //            if (callback) callback(true, multiple_sectnum_error);
        //    }
        //    else if (callback) callback(false);
        //}
    }
    else {
        if (callback) callback(true, true);
    }
}

function BrillionSketchBeforeSave(sketches, callback) {
    var valid = { isValid: true, message: 'The following label(s) are <b>incorrect</b>: ', auditTrailEntry: 'The following label(s) are incorrect: ' };
    var LabelLookup = [];
    var parcel = window.opener.activeParcel;
    if (sketches === undefined || sketches == null) {
        if (callback) callback(true);
        return;
    }

    sketches.forEach(function (sk) {
        sk.vectors.forEach(function (v) {
            LabelLookup = sketchApp.external.lookup[v.vectorConfig.LabelLookup]
            if (Object.keys(LabelLookup).filter(function (label) { return label == v.code; }).length == 0) {
                valid.isValid = valid.isValid && false;
                valid.message += (v.name + ', ');
                valid.auditTrailEntry += (v.name + ', ');
            }
        });
    });
    valid.message = valid.message.substring(0, valid.message.length - 2) + '. Tap "OK" and select the sketch segment, press "Edit Label" and select a sketch label from one in the list.';

    if (!valid.isValid) {
        messageBox(valid.message, ['OK'], function () {
            if (callback) callback(false, true);
        });
    }
    else {
        if (callback) callback(true, true);
    }
}

function ClarkKySketchAfterSave(sketchData, callback) {
    if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }

    var areaField = null, skSource = { tblResidential: { vectors: [] }, tblCommercial: { vectors: [] }, tblMobileHome: { vectors: [] }, tblFarm: { vectors: [] } }, areaFactor = null;

    parcel = window.opener.activeParcel;
    sketchData.forEach(function (sk) {
        var currSketch = sketchApp.sketches.filter(function (s) { return s.sid == sk.rowid })[0];
        var vectors = currSketch.vectors;
        sourceTable = currSketch.config.SketchSource.Table;
        vectors.forEach(function (v) {
            var label = v.name ? (v.name).toString().trim() : '';
            skSource[sourceTable].vectors.push({ label: label, area: parseFloat(v.area()), FieldName: null, Rowuid: sk.rowid });
        });
    });

    Object.keys(skSource).forEach(function (source) {
        var areaSummaryFields = ccma.Sketching.Config.sources.filter(function (con) { return con.SketchSource.Table == source })[0].AreaSumFields;
        skSource[source].vectors.forEach(function (v) {
            for (field in areaSummaryFields) {
                if (_.find(areaSummaryFields[field], function (f) { return f == v.label })) {
                    v.FieldName = field;
                    return;
                }
            }
        });
    });

    var sketchSourceData = {};

    Object.keys(skSource).forEach(function (sourceTable) {
        sourceData = skSource[sourceTable];
        sourceData.vectors.forEach(function (vect) {
            if (vect.FieldName == null) return;
            if (!sketchSourceData[sourceTable]) sketchSourceData[sourceTable] = [];

            vect.area = vect.area * (lookup['FeatureCodes_DCS'][vect.label] ? parseFloat(lookup['FeatureCodes_DCS'][vect.label].NumericValue) : 1);

            if (sketchSourceData[sourceTable].length == 0 || sketchSourceData[sourceTable].filter(function (skd) { return skd.FieldName == vect.FieldName }).length == 0)
                sketchSourceData[sourceTable].push({ FieldName: vect.FieldName, area: vect.area, Rowuid: vect.Rowuid });

            else
                sketchSourceData[sourceTable].filter(function (skd) { return skd.FieldName == vect.FieldName })[0].area += vect.area;

        });
    });
    var preCallback = function () {
        var updateAndReplace = false;
        $('.ccse-canvas').removeClass('dimmer');
        $('.dimmer').hide();
        var preProcessor = function () {
            $('.ccse-canvas').addClass('dimmer');
            $('.dimmer').show();
            ClarkKyAfterSaveConfirm(sketchSourceData, sketchData, updateAndReplace, callback);
        }
        messageBox('Replace and Update ALL SqFt fields whether sketched or not', ['OK', 'Cancel'], function () { updateAndReplace = true; preProcessor(); }, function () { updateAndReplace = false; preProcessor(); });
    }
    var updateAndReplace = false;
    if (sketchData.length > 0) preCallback()

}

function ClarkKyAfterSaveConfirm(sketchSourceData, sketchData, updateAndReplace, callback) {
    var saveData = '';
    parcel = window.opener.activeParcel;
    var getDataField = window.opener.getDataField;
    if (updateAndReplace) {
        sketchData.forEach(function (skd) {
            var areaSummaryFields = ccma.Sketching.Config.sources.filter(function (con) { return con.SketchSource.Table == skd.source })[0].AreaSumFields;
            var fieldId;
            var skData = sketchSourceData[skd.source];
            for (field in areaSummaryFields) {
                fieldId = getDataField(field, skd.source).Id;
                if (_.where(parcel[skd.source], { ROWUID: parseInt(skd.rowid) })[0][field] != null || (skData && _.where(skData, { Rowuid: skd.rowid, FieldName: field }).length == 0))
                    saveData += '^^^' + parcel.Id + '|' + skd.rowid + '|' + fieldId + '|||edit|\n';
            }
        });
    }
    for (source in sketchSourceData) {
        var rowid = sketchData.filter(function (sk) { return sk.source == source; })[0].rowid;
        var skData = sketchSourceData[source];
        skData.forEach(function (sk) {
            if (!sk || sk.FieldName == null) {
                if (skData.length > 0) enqueuePCI(skData, source, callback)
                else if (callback) callback();
                return;
            }
            var fieldId = getDataField(sk.FieldName, source).Id;
            saveData += '^^^' + parcel.Id + '|' + rowid + '|' + fieldId + '||' + sk.area + '|edit|\n';
        });
    }
    var isdtr = window.opener ? window.opener.__DTR : (typeof (__DTR) !== 'undefined' ? __DTR : false);
    let isSvApp = window.opener && window.opener.appType == "sv" ? true : (typeof (appType) !== 'undefined' && appType == "sv" ? true : false);
    var _appType = isdtr ? 'DTR' : (isSvApp ? 'SV' : 'QC');
    var saveRequestData = {
        ParcelId: parcel.Id,
        Priority: '',
        AlertMessage: '',
        Data: '',
        recoveryData: '',
        AppraisalType: '',
        activeParcelPhotoEditsdata: '',
        appType: _appType
    };

    if (saveData != '') {
        console.log(saveData);
        saveRequestData.Data = saveData;
        $.ajax({
            url: '/quality/saveparcelchanges.jrq',
            type: "POST",
            dataType: 'json',
            data: saveRequestData,
            success: function (resp) {
                if (resp.status == "OK")
                    if (callback) callback();
            }
        });
    }
    else {
        if (callback) callback();
        return;
    }
}

function BaldwinSketchAfterSave(sketchData, callback) {
    if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }
    var parcel = window.opener ? window.opener.activeParcel : activeParcel;

    var skSource = [];
    var existingRowuids = [];
    var fieldDetails = [], bldFieldDetails = [], bldDetails = {};
    var apexLookup = window.opener ? window.opener.lookup['Apex_Area_Codes'] : lookup['Apex_Area_Codes'];
    var bldUpdated = true, bldROWUID, bldParentRowid;
    var getDataFields = window.opener ? window.opener.getDataField : getDataField;
    var cc_assocField = getDataFields('CC_AssociateValues', 'CCV_BLDG_MASTER');
    var bld_master = [];
    var fields = ['AREA_CODE_REST', 'GROSS_AREA', 'MULTIPLIER', 'BUILDING_CODE', 'FLOOR_CODE', 'HEAT_CODE', 'ADJUSTED_AREA', 'AREA_NAME'];
    var field;
    var bldFields = ['BASIC_AREA', 'ADJUSTED_AREA', 'SECOND_FLOOR_AREA'];
    var assocVals = [];
    var saveData = '';

    fields.forEach(function (fname) {
        var field = getDataFields(fname, 'APDSKA01');
        fieldDetails.push({ name: fname, assignedName: field.AssignedName, Id: field.Id, value: null })
    });

    bldFields.forEach(function (fname) {
        var field = getDataFields(fname, 'APDIM301');
        bldFieldDetails.push({ name: fname, assignedName: field.AssignedName, Id: field.Id, value: 0 })
    });

    sketchData.forEach(function (sk) {
        var currSketch = sketchApp.sketches.filter(function (s) { return s.sid == sk.rowid })[0];
        var vectors = currSketch.vectors.filter(function (sv) { return !(sv.isFreeFormLineEntries) });
        var ch_record = parcel['APDIM301'].filter(function (p) { return p.ParentROWUID == sk.rowid; });
        ch_record.forEach(function (ch) {
            if (ch['APDSKA01']) ch['APDSKA01'].filter(function (chk) { return chk.CC_Deleted == false }).forEach(function (ap) { existingRowuids.push(ap.ROWUID); });
        })
        var vects = [], cc_assocVal = '';
        vectors.forEach(function (v) {
            var label = v.name ? v.name.toString().trim() : '';
            var code = v.code ? v.code.toString().trim() : '';
            var line = (v.associateValues.APDIM301.CARD_LINE == 'null' ? null : v.associateValues.APDIM301.CARD_LINE);
            vects.push({ code: v.code, label: label, area: Math.round(parseFloat(v.area())), LINE: line, Rowuid: sk.rowid });
            cc_assocVal += line + ',';
        });
        skSource.push({ Rowuid: sk.rowid, vectors: vects });

        parcel['APDIM301'].filter(function (x) { return x.ParentROWUID == sk.rowid }).forEach(function (bld) {
            if (!bldDetails[sk.rowid]) bldDetails[sk.rowid] = {};
            if (!bldDetails[sk.rowid][bld.CARD_LINE]) bldDetails[sk.rowid][bld.CARD_LINE] = {};
            if (!bldDetails[sk.rowid][bld.CARD_LINE]['rowid']) {
                bldDetails[sk.rowid][bld.CARD_LINE]['rowid'] = bld.ROWUID;
                bldDetails[sk.rowid][bld.CARD_LINE]['fieldDetails'] = JSON.parse(JSON.stringify(bldFieldDetails));
                bldDetails[sk.rowid][bld.CARD_LINE]['parentRowid'] = bld.ParentROWUID;
            }
        })
    });

    sketchApp.sketches.forEach(function (sk) {
        var assocVal = '';
        var svectors = sk.vectors.filter(function (sv) { return !(sv.isFreeFormLineEntries) });
        svectors.forEach(function (v) {
            assocVal += (v.associateValues.APDIM301.CARD_LINE == 'null' ? null : v.associateValues.APDIM301.CARD_LINE) + ',';
        });
        var rec = parcel.CCV_BLDG_MASTER.filter(function (ac) { return ac.ROWUID == sk.sid })[0];
        assocVal = assocVal.substring(0, assocVal.length - 1);
        if (rec.CC_AssociateValues != assocVal)
            saveData += '^^^' + parcel.Id + '|' + sk.sid + '|' + cc_assocField.Id + '||' + assocVal + '|edit|\n';
    });

    existingRowuids.forEach(function (rowid) {
        saveData += '^^^' + parcel.Id + '|' + rowid + '|||APDSKA01|delete|\n';
    })

    var auxRowuid = ccTicks();
    skSource.forEach(function (sk) {
        var sVectors = sk.vectors.filter(function (sv) { return !(sv.isFreeFormLineEntries) });
        sVectors.forEach(function (vect) {
            fieldDetails.forEach(function (fd) { fd.value = null; })
            var rowId = (auxRowuid += 10)
            saveData += '^^^' + parcel.Id + '|' + rowId + '|' + bldDetails[sk.Rowuid][vect.LINE].rowid + '||APDSKA01|new|\n';
            var multiplier = null, desc = null, no_code = false;
            if (!apexLookup[vect.code])
                no_code = true
            var b_code = vect.code.substring(0, 2)
            var F_code = vect.code.substring(2, 3)
            var H_code = vect.code.substring(3, 5)
            var A_code = vect.code.substring(6, 10);
            if (no_code)
                fieldDetails.filter(function (fd) { return fd.name == 'AREA_CODE_REST'; })[0].value = 'nocal'
            else {
                fieldDetails.filter(function (fd) { return fd.name == 'AREA_CODE_REST'; })[0].value = A_code;
                multiplier = parseFloat(apexLookup[vect.code].NumericValue);
                desc = apexLookup[vect.code].Name;
            }
            var adj_area = Math.round(vect.area * ((multiplier != null) ? multiplier : 1))

            fieldDetails.filter(function (fd) { return fd.name == 'GROSS_AREA'; })[0].value = vect.area;
            fieldDetails.filter(function (fd) { return fd.name == 'MULTIPLIER'; })[0].value = multiplier
            fieldDetails.filter(function (fd) { return fd.name == 'BUILDING_CODE'; })[0].value = (no_code ? '0' : (isNaN(b_code) == true || b_code == '' ? '99' : b_code));
            fieldDetails.filter(function (fd) { return fd.name == 'FLOOR_CODE'; })[0].value = (isNaN(F_code) == true || F_code == '' ? '1' : F_code);
            fieldDetails.filter(function (fd) { return fd.name == 'HEAT_CODE'; })[0].value = ((isNaN(H_code) == true || no_code || H_code == '') ? '0' : H_code);
            fieldDetails.filter(function (fd) { return fd.name == 'ADJUSTED_AREA'; })[0].value = adj_area;
            fieldDetails.filter(function (fd) { return fd.name == 'AREA_NAME'; })[0].value = (desc ? desc : vect.label)

            if (apexLookup[vect.code] && apexLookup[vect.code].AdditionalValue1 && (apexLookup[vect.code].AdditionalValue2 != 0 || apexLookup[vect.code].AdditionalValue2 != '0') && b_code != '00') {
                if (apexLookup[vect.code].AdditionalValue1 == 'Base')
                    bldDetails[sk.Rowuid][vect.LINE].fieldDetails.filter(function (bfd) { return bfd.name == 'BASIC_AREA'; })[0].value += vect.area;
                else if (apexLookup[vect.code].AdditionalValue1 == 'OtherFloor' && vect.code.substring(2, 3) != '1')
                    bldDetails[sk.Rowuid][vect.LINE].fieldDetails.filter(function (bfd) { return bfd.name == 'SECOND_FLOOR_AREA'; })[0].value += adj_area;
                bldDetails[sk.Rowuid][vect.LINE].fieldDetails.filter(function (bfd) { return bfd.name == 'ADJUSTED_AREA'; })[0].value += adj_area;
            }
            fieldDetails.forEach(function (tfd) {
                saveData += '^^^' + parcel.Id + '|' + rowId + '|' + tfd.Id + '||' + tfd.value + '|edit|' + bldDetails[sk.Rowuid][vect.LINE].rowid + '\n';
            });
        });
        var bldDetail = bldDetails[sk.Rowuid];
        for (line in bldDetail) {
            var OVER_AREA_INPUT = false; // If the user has selected 'Y' for the Override Area Input, the sketchsave logic should *not* update or calculate the SQFT Fd-7772
            if (bldDetail[line].rowid != null) {
                var rec_bld = parcel.APDIM301.filter(function (bld) { return bld.ROWUID == bldDetail[line].rowid })[0];
                OVER_AREA_INPUT = (rec_bld && rec_bld.OVERRIDE_AREA_INPUT && rec_bld.OVERRIDE_AREA_INPUT == "Y") ? true : false;
            }
            if (!OVER_AREA_INPUT) {
                bldDetail[line].fieldDetails.forEach(function (bfd) {
                    saveData += '^^^' + parcel.Id + '|' + bldDetail[line].rowid + '|' + bfd.Id + '||' + bfd.value + '|edit|' + bldDetail[line].parentRowid + '\n';
                });
            }
        }
    })
    var isdtr = window.opener ? window.opener.__DTR : (typeof (__DTR) !== 'undefined' ? __DTR : false);
    let isSvApp = window.opener && window.opener.appType == "sv" ? true : (typeof (appType) !== 'undefined' && appType == "sv" ? true : false);
    var _appType = isdtr ? 'DTR' : (isSvApp ? 'SV' : 'QC');
    var saveRequestData = {
        ParcelId: parcel.Id,
        Priority: '',
        AlertMessage: '',
        Data: '',
        recoveryData: '',
        AppraisalType: '',
        activeParcelPhotoEditsdata: '',
        appType: _appType
    };

    if (saveData != '') {
        console.log(saveData);
        saveRequestData.Data = saveData;
        $.ajax({
            url: '/quality/saveparcelchanges.jrq',
            type: "POST",
            dataType: 'json',
            data: saveRequestData,
            success: function (resp) {
                if (resp.status == "OK")
                    if (callback) callback();
            }
        });
    }
    else {
        if (callback) callback();
        return;
    }
}

function LafayetteSketchAfterSave(sketchData, callback) {
    if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }
    var Lafayette_Lookup = Lafayette_Lookup ? Lafayette_Lookup : (window.opener ? window.opener.Lafayette_Lookup : []);
    var parcel = window.opener ? window.opener.activeParcel : activeParcel;
    var getDataField = window.opener ? window.opener.getDataField : getDataField;
    var getCategoryFromSourceTable = window.opener ? window.opener.getCategoryFromSourceTable : getCategoryFromSourceTable;
    var TotalPerimeter = 0, actual_Ar = 0, gross_Ar = 0, heat_Ar = 0, eff_Ar = 0;

    var trav_field = getDataField('trav', 'ccv_bld_mstr_bld');
    // var perimeter_Field=getDataField('perimeter', 'ccv_bld_mstr_bld');
    //  var actual_Ar_Field=getDataField('act_ar', 'ccv_bld_mstr_bld');
    // var gross_Ar_Field=getDataField('gross_ar', 'ccv_bld_mstr_bld');
    // var heat_Ar_Field=getDataField('heat_ar', 'ccv_bld_mstr_bld');
    var apx_flg_Field = getDataField('apx_flg', 'ccv_bld_mstr_bld');
    var apx_pgs_Field = getDataField('apx_pgs', 'ccv_bld_mstr_bld');
    var trav_val = '', perimeter = '', lkVal = null, savedatas = {};
    var saveData = '';

    //var fields = ['sar_cd', 'act_ar', 'gross_ar', 'eff_ar', 'heat_ar', 'perimeter'];
    var fields = ['sar_cd'];
    var Bld_Table = "ccv_bld_mstr_bld", SubArea_Table = "bld_sar";
    var subAreaRecord = [], recRowIds = [], skSourceData = [], recordValues = [], activeParcelData = [], recordDataValues = [], BldRec = [];
    var cat = getCategoryFromSourceTable(SubArea_Table);
    sketchData.forEach(function (skd) {
        trav_val = '';
        sketchApp.sketches.filter(function (sk) { return sk.uid == skd.rowid }).forEach(function (sks) {
            var sksid = sks.sid;
            var vectors = sks.vectors.filter(function (svs) { return !(svs.isFreeFormLineEntries) });
            vectors.forEach(function (v) {
                var multi = 1;
                lkVal = Lafayette_Lookup.filter(function (lk) { return lk.sar_cd == v.code })[0]
                var sketchPerimeter = parseInt((v.perimeter()).toFixed(0));
                var sketchArea = parseInt((v.area()).toFixed(0));
                perimeter = ((lkVal && lkVal.perimeter_flg == 'Y') ? ' P' + sketchPerimeter : '')
                actual_Ar = actual_Ar + sketchArea;
                switch (v.code) {
                    case "TWO":
                        multi = 2;
                        break;
                    case "THR":
                        multi = 3;
                        break;
                    case "FOR":
                        multi = 4;
                        break;
                    case "FIV":
                        multi = 5;
                        break;
                    case "SIX":
                        multi = 6;
                        break;
                    default:
                        multi = 1;
                }
                gross_Ar = gross_Ar + (sketchArea * multi);
                if (perimeter != '') {
                    heat_Ar = heat_Ar + (sketchArea * multi);
                    TotalPerimeter = TotalPerimeter + (perimeter != '' ? sketchPerimeter : 0);
                }
                trav_val += v.code + '(' + sketchArea + perimeter + ')';
                recordValues.push({ Label: v.code, Label_Name: v.name, gross_Ar: (sketchArea * multi), actual_Ar: sketchArea, eff_Ar: (sketchArea * multi), heat_Ar: perimeter != '' ? (sketchArea * multi) : 0, Total_Perimeter: perimeter != '' ? sketchPerimeter : 0, sketchSid: sksid });
            });
        });
        trav_val = trav_val + '.';
        savedatas[skd.rowid] = { trav_val: trav_val, TotalPerimeter: TotalPerimeter, actual_Ar: actual_Ar, gross_Ar: gross_Ar, heat_Ar: heat_Ar, bld_sar: {} }
        skSourceData.push(parcel[Bld_Table].filter(function (s) { return s.ROWUID == skd.rowid })[0]);
    });

    activeParcelData = parcel.ccv_bld_mstr_bld;
    var saveCount = Object.keys(savedatas).length;
    $.each(savedatas, function (rowid, sd) {
        saveData += '^^^' + parcel.Id + '|' + rowid + '|' + trav_field.Id + '||' + sd.trav_val + '|edit|\n';
        //saveData += '^^^' + parcel.Id + '|' + rowid + '|' + perimeter_Field.Id + '||' + sd.TotalPerimeter + '|edit|\n';
        //saveData += '^^^' + parcel.Id + '|' + rowid + '|' + actual_Ar_Field.Id + '||' + sd.actual_Ar + '|edit|\n';
        //saveData += '^^^' + parcel.Id + '|' + rowid + '|' + heat_Ar_Field.Id + '||' + sd.heat_Ar + '|edit|\n';
        //saveData += '^^^' + parcel.Id + '|' + rowid + '|' + gross_Ar_Field.Id + '||' + sd.gross_Ar + '|edit|\n';
        saveData += '^^^' + parcel.Id + '|' + rowid + '|' + apx_flg_Field.Id + '||' + 'Y' + '|edit|\n';
        saveData += '^^^' + parcel.Id + '|' + rowid + '|' + apx_pgs_Field.Id + '||' + 1 + '|edit|\n';
    })

    skSourceData.forEach(function (skv) {
        BldRec = parcel[Bld_Table].filter(function (s) { return s.ROWUID == skv.ROWUID })[0];
        subAreaRecord = BldRec.bld_sar.filter(function (skd) { return (skd.CC_Deleted != true || skd.CC_Deleted != "true") })
        for (r in subAreaRecord)
            recRowIds.push(subAreaRecord[r].ROWUID);
    });

    if (recRowIds.length > 0) {
        recRowIds.forEach(function (rowuid) {
            saveData += '^^^' + parcel.Id + '|' + rowuid + '|||' + SubArea_Table + '|delete|\n';
        });
    }
    var field = [];
    fields.forEach(function (f) {
        var fd = getDataField(f, SubArea_Table);
        field.push({ Id: fd.Id, AssignedName: fd.AssignedName, Name: fd.Name, Value: null });
    });

    skSourceData.forEach(function (sk) {
        var vectors = sketchApp.sketches.filter(function (s) { return s.sid == sk.ROWUID })[0].vectors;
        vectors = vectors.filter(function (svs) { return !(svs.isFreeFormLineEntries) });
        recordDataValues = recordValues.filter(function (recval) { return recval.sketchSid == sk.ROWUID })
        var rowId = ccTicks();
        recordDataValues.forEach(function (recValue) {
            rowId = rowId + 50;
            saveData += '^^^' + parcel.Id + '|' + rowId + '|' + sk.ROWUID + '||' + SubArea_Table + '|new|\n';
            saveData += '^^^' + parcel.Id + '|' + rowId + '|' + field[0].Id + '||' + recValue.Label_Name + '|edit|' + sk.ROWUID + '\n';
            //saveData += '^^^' + parcel.Id + '|' + rowId + '|' + field[1].Id + '||' + recValue.actual_Ar + '|edit|' + sk.ROWUID + '\n';
            //saveData += '^^^' + parcel.Id + '|' + rowId + '|' + field[2].Id + '||' + recValue.actual_Ar + '|edit|' + sk.ROWUID + '\n';
            //saveData += '^^^' + parcel.Id + '|' + rowId + '|' + field[3].Id + '||' + recValue.eff_Ar + '|edit|' + sk.ROWUID + '\n';
            //saveData += '^^^' + parcel.Id + '|' + rowId + '|' + field[4].Id + '||' + recValue.heat_Ar + '|edit|' + sk.ROWUID + '\n';
            //saveData += '^^^' + parcel.Id + '|' + rowId + '|' + field[5].Id + '||' + recValue.Total_Perimeter + '|edit|' + sk.ROWUID + '\n';
        });
    });


    var isdtr = window.opener ? window.opener.__DTR : (typeof (__DTR) !== 'undefined' ? __DTR : false);
    let isSvApp = window.opener && window.opener.appType == "sv" ? true : (typeof (appType) !== 'undefined' && appType == "sv" ? true : false);
    var _appType = isdtr ? 'DTR' : (isSvApp ? 'SV' : 'QC');
    var saveRequestData = {
        ParcelId: parcel.Id,
        Priority: '',
        AlertMessage: '',
        Data: '',
        recoveryData: '',
        AppraisalType: '',
        activeParcelPhotoEditsdata: '',
        appType: _appType
    };

    if (saveData != '') {
        console.log(saveData);
        saveRequestData.Data = saveData;
        $.ajax({
            url: '/quality/saveparcelchanges.jrq',
            type: "POST",
            dataType: 'json',
            data: saveRequestData,
            success: function (resp) {
                if (resp.status == "OK")
                    if (callback) callback();
            }
        });
    }
    else {
        if (callback) callback();
        return;
    }
}

function PatriotSketchAfterSave(sketchData, callback) {
    if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }

    var Patriot_Lookup = Patriot_Lookup ? Patriot_Lookup : (window.opener ? window.opener.Patriot_Lookup : []);
    var parcel = window.opener ? window.opener.activeParcel : activeParcel;
    var getDataField = window.opener ? window.opener.getDataField : getDataField;
    var getCategoryFromSourceTable = window.opener ? window.opener.getCategoryFromSourceTable : getCategoryFromSourceTable;
    var area_Table = "CCV_SketchedAreas", area_AlternateTypes_Table = "CCV_SketchedAreaAlternateTypes", buiding_Table = "CCV_Buildings_BuildingDepreciaton";
    var saveData = '', sketchDataArea_TableRowuids = [], fieldDetails = [];
    var skt_field = getDataField('CC_SKETCH', 'CCV_Sketch'); var sketchDataCopy = _.clone(sketchData);
    var fields = ['xrSubAreaID', 'Area', 'SizedAdjustedArea', 'AdjustedArea', 'Perimeter', 'IsUnsketchedFlag', 'FinishedArea'];

    sketchData.forEach(function (skt) {
        var sk = sketchApp.sketches.filter(function (x) { return (x.uid == skt.rowid) })[0];
        var pRowuid = sk.parentRow.ParentROWUID;
        var pRecord = sk.parentRow.parentRecord;
        var sketched_Areas_Records = [];
        if (pRecord && pRecord.CCV_SketchedAreas)
            sketched_Areas_Records = pRecord.CCV_SketchedAreas.filter(function (skd) { return (skd.CC_Deleted != true || skd.CC_Deleted != "true") });
        for (r in sketched_Areas_Records)
            sketchDataArea_TableRowuids.push(sketched_Areas_Records[r].ROWUID)
    });

    fields.forEach(function (f) {
        fieldDetails.push(getDataField(f, area_Table));
    });

    var processor = function (sketchDatas, processorCallback) {
        var skt = sketchDatas.pop();
        var sketchess = sketchApp.sketches.filter(function (x) { return x.uid == skt.rowid })[0];
        var vectors = sketchess.vectors;
        var buiding_Table_Record = sketchess.parentRow.parentRecord;
        var area = 0, label = '', recordValues = [], SubAreaId, patriot_lookup = [], FinishedArea, SizeAdjustedArea, AdjustedArea, Perimeter, pushed = false, IsUnsketchedFlag = 0, labelSketch;
        var newAuxRowuid = ccTicks();

        for (v = 0; vectors[v] != null; v++) {
            var vector = vectors[v], newSketchedAreaId = "", vectorSketchIds = (vector._SketchedAreaID ? vector._SketchedAreaID.split(" ") : []), isNew = vector.clientId ? true : false;
            area = vector.fixedArea == '' ? null : parseFloat(vector.fixedArea ? vector.fixedArea : vector.area());
            area = (!isNaN(area)) ? area : 0;
            IsUnsketchedFlag = vector.isUnSketchedArea ? 0 : 1;
            labelSketch = (vectors[v].code || vectors[v].name).toString().trim();
            vectorSketchIds = vectorSketchIds.length ? vectorSketchIds : [];
            labelSketch.split('/').forEach(function (label) {
                patriot_lookup = Patriot_Lookup.filter(function (pat) { return ((pat.xrSubAreaID == label) || (pat.SubArea == label)) })[0];
                var _sketchAreaId, sourceRecord;
                newAuxRowuid = newAuxRowuid + 58;
                if (isNew) {
                    _sketchAreaId = newAuxRowuid;
                    newSketchedAreaId += '{$' + area_Table + '#' + _sketchAreaId + '$} ';
                }
                else {
                    _sketchAreaId = vectorSketchIds.shift(); _sketchAreaId = _sketchAreaId ? _sketchAreaId : 'new';
                    var _skAreaId = ((_sketchAreaId.indexOf("#") < 0) ? _sketchAreaId.replace(/[${}]/g, '') : _sketchAreaId.replace(/[${}]/g, '').split("#")[1]);
                    var sk_area_record = buiding_Table_Record.CCV_SketchedAreas.filter(function (skd) { return ((skd.SketchedAreaID == _skAreaId || skd.ROWUID == _skAreaId) && (skd.CC_Deleted != true || skd.CC_Deleted != "true")) })[0];
                    if (sk_area_record) {
                        newSketchedAreaId += _sketchAreaId + ' ';
                        _sketchAreaId = sk_area_record.ROWUID;
                        sourceRecord = sk_area_record;
                        sketchDataArea_TableRowuids = sketchDataArea_TableRowuids.filter(function (rowId) { return (rowId != _sketchAreaId) });
                    }
                    else {
                        _sketchAreaId = newAuxRowuid;
                        newSketchedAreaId += '{$' + area_Table + '#' + _sketchAreaId + '$} ';
                    }
                }

                Perimeter = vectors[v].perimeter();
                if (patriot_lookup) {
                    SubAreaId = patriot_lookup.xrSubAreaID;
                    SizeAdjustedArea = ((!area && area != 0) ? null : ((patriot_lookup.UseInSizeAdjustmentFlag == 'true') ? (area * patriot_lookup.AdjustSketchFactor) : area));
                    FinishedArea = ((!area && area != 0) ? null : (patriot_lookup.IsFinishedFlag == 'true' ? area : null));
                    var deci_limit = parseInt(fieldDetails[v].NumericScale);
                    AdjustedArea = parseFloat(((!area && area != 0) ? null : (patriot_lookup.AdjustSketchFactor ? (area * patriot_lookup.AdjustSketchFactor) : area)).toFixed(scale));
                    recordValues.push({ xrSubAreaID: SubAreaId, Area: area, SizedAdjustedArea: SizeAdjustedArea, FinishedArea: FinishedArea, AdjustedArea: AdjustedArea, Perimeter: Perimeter, IsUnsketchedFlag: IsUnsketchedFlag, sourceRecord: sourceRecord, rowuid: _sketchAreaId })
                }
            });
            vector._SketchedAreaID = newSketchedAreaId.trim();
        }

        recordValues = recordValues.reverse();

        function insertFunction() {
            var recValue = recordValues.pop();
            function _patriotRecordUpdate(recRowuid) {
                fieldDetails.forEach(function (fld) {
                    if (recValue[fld.Name] || recValue[fld.Name] == 0 || recValue[fld.Name] == "")
                        saveData += '^^^' + parcel.Id + '|' + recRowuid + '|' + fld.Id + '||' + recValue[fld.Name] + '|edit|' + buiding_Table_Record.ROWUID + '\n';
                });

                if (recordValues.length > 0) insertFunction();
                else if (sketchDatas.length > 0) processor(sketchDatas, processorCallback);
                else if (processorCallback) processorCallback();
            }

            if (recValue.sourceRecord)
                _patriotRecordUpdate(recValue.rowuid);
            else {
                insertNewAuxRecordSketch(parcel.Id, buiding_Table_Record, getCategoryFromSourceTable(area_Table).Id, recValue.rowuid, null, function (in_sketchAreaData, rowid) {
                    if (in_sketchAreaData)
                        saveData += in_sketchAreaData;
                    _patriotRecordUpdate(rowid);
                });
            }
        }

        if (recordValues.length > 0) insertFunction();
        else if (processorCallback) processorCallback();
    }

    function serverUpdate() {
        var isdtr = window.opener ? window.opener.__DTR : (typeof (__DTR) !== 'undefined' ? __DTR : false);
        let isSvApp = window.opener && window.opener.appType == "sv" ? true : (typeof (appType) !== 'undefined' && appType == "sv" ? true : false);
        var _appType = isdtr ? 'DTR' : (isSvApp ? 'SV' : 'QC');

        var saveRequestData = {
            ParcelId: parcel.Id,
            Priority: '',
            AlertMessage: '',
            Data: '',
            recoveryData: '',
            AppraisalType: '',
            activeParcelPhotoEditsdata: '',
            appType: _appType
        };

        if (saveData != '') {
            console.log(saveData);
            saveRequestData.Data = saveData;
            $.ajax({
                url: '/quality/saveparcelchanges.jrq',
                type: "POST",
                dataType: 'json',
                data: saveRequestData,
                success: function (resp) {
                    if (resp.status == "OK")
                        if (callback) callback();
                }
            });
        }
        else {
            if (callback) callback();
            return;
        }
    }

    var _PCIUpdate = function () {
        sketchDataArea_TableRowuids.forEach(function (rowuid) {
            saveData += '^^^' + parcel.Id + '|' + rowuid + '|||' + area_Table + '|delete|\n';
        });

        sketchDataCopy.forEach(function (skt) {
            var sk_string = '';
            var skt = sketchApp.sketches.filter(function (x) { return x.uid == skt.rowid })[0];
            var vect = skt.vectors;
            var skRecord = skt.parentRow;
            vect.forEach(function (vt) {
                var _skSreaId = vt._SketchedAreaID;
                var vect_string = vt.vectorString;
                if (vect_string) {
                    var squarePart = vect_string.split("[")[1].split("]")[0].split(",");
                    squarePart[5] = _skSreaId; squarePart = squarePart.toString();
                    sk_string += vect_string.split("[")[0] + '[' + squarePart + ']' + vect_string.split("]")[1] + ';';
                }
            });
            saveData += '^^^' + parcel.Id + '|' + skRecord.ROWUID + '|' + skt_field.Id + '||' + sk_string + '|edit|' + skRecord.ParentROWUID + '\n';
        });
        serverUpdate();
    }

    processor(sketchData, function () {
        _PCIUpdate();
    });
}

var FTC_associate_vector_config = function (sketchApp, sourceTable, SketchAssociateFields) {
    var FTC_Detail_Lookup = (parent && parent.FTC_Detail_Lookup) ? parent.FTC_Detail_Lookup : FTC_Detail_Lookup ? FTC_Detail_Lookup : (window.opener ? window.opener.FTC_Detail_Lookup : []),
        FTC_AddOns_Lookup = (parent && parent.FTC_AddOns_Lookup) ? parent.FTC_AddOns_Lookup : FTC_AddOns_Lookup ? FTC_AddOns_Lookup : (window.opener ? window.opener.FTC_AddOns_Lookup : []),
        FTC_Floor_Lookup = (parent && parent.FTC_Floor_Lookup) ? parent.FTC_Floor_Lookup : FTC_Floor_Lookup ? FTC_Floor_Lookup : (window.opener ? window.opener.FTC_Floor_Lookup : []),
        parcel = window.opener ? window.opener.activeParcel : activeParcel, lookupdata = window.opener ? window.opener.lookup : lookup,
        valid = true, html = '', fieldHtml = '', toHideDropDown = 0;

    sketchApp.sketches.filter(function (sk) { return (sk.isModified || (sk.sid == sketchApp.currentSketch.sid)); }).forEach((sketch, i) => {
        fieldHtml = ''; let rec = [];

        sketch.vectors.filter(function (v) { return (!v.isFreeFormLineEntries) }).filter(function (vect) { return (vect.isModified || (sketch.sid == sketchApp.currentSketch.sid)); }).forEach(function (vectort) {
            let code = (vectort.code && vectort.code != '') ? vectort.code : vectort.name,
                vector_descrp = (lookupdata['AREA_CODES'] && lookupdata['AREA_CODES'][code]?.['Name']) ? lookupdata['AREA_CODES'][code]['Name'] : (vectort.name ? vectort.name : ''),
                apexLink, recd = [], activeFlag, save_dtl_lookup = FTC_Detail_Lookup.filter(function (lkup) { return (lkup.IMPDETAILDESCRIPTION == vector_descrp && lkup.APEXAREACODE == vectort.code) })[0];

            if (!save_dtl_lookup) save_dtl_lookup = FTC_Detail_Lookup.filter(function (lkup) { return (lkup.APEXAREACODE == null && lkup.IMPDETAILDESCRIPTION == vector_descrp) })[0];

            let save_add_on_lookup = FTC_AddOns_Lookup.filter(function (lkup) { return lkup.ADDONCODE == code; })[0],
                save_floor_lookup = FTC_Floor_Lookup.filter(function (lkup) { return lkup.IMPSFLOORDESCRIPTION == vector_descrp; })[0];

            if (sourceTable == 'CCV_IMPDETAIL_ATTACHED_DETACHED') {
                if (save_dtl_lookup) {
                    apexLink = save_dtl_lookup.APEXLINKFLAG;
                    activeFlag = save_dtl_lookup.ACTIVEFLAG;
                }
            }
            else if (sourceTable == 'CCV_IMPDETAIL_ADD_ONS') {
                if (save_add_on_lookup) {
                    apexLink = save_add_on_lookup.APEXLINKFLAG;
                    activeFlag = save_add_on_lookup.ACTIVEFLAG;
                }
            }
            else if (sourceTable == 'IMPBUILTASFLOOR') {
                if (save_floor_lookup) {
                    apexLink = save_floor_lookup.APEXLINKFLAG;
                    activeFlag = save_floor_lookup.ACTIVEFLAG;
                }
            }

            if (apexLink == 1 && activeFlag == 1) {
                let blt_rec = parcel.IMPBUILTAS.filter(function (b_rec) { return b_rec.ParentROWUID == sketch.parentRow.ParentROWUID })[0];
                if (sourceTable == 'IMPBUILTASFLOOR' && blt_rec) {
                    recd = blt_rec[sourceTable].filter(function (blt_r) { return blt_r.IMPSFLOORDESCRIPTION == vector_descrp && !blt_r.CC_Deleted });
                    recd.forEach(function (recort) {
                        let sameRowuid = 0;
                        rec.forEach(function (ree) {
                            if (ree.ROWUID == recort.ROWUID) sameRowuid = 1;
                        });

                        if (sameRowuid == 0) rec.push(recort)
                    });
                }
                else if (sourceTable == 'CCV_IMPDETAIL_ATTACHED_DETACHED' || sourceTable == 'CCV_IMPDETAIL_ADD_ONS') {
                    recd = parcel[sourceTable].filter(function (src_rec) { return src_rec.ParentROWUID == sketch.parentRow.ParentROWUID && !src_rec.CC_Deleted && (src_rec.ADDONCODE == code || src_rec.IMPDETAILDESCRIPTION == vector_descrp); });
                    recd.forEach(function (recort) {
                        let sameRowuid = 0;
                        rec.forEach(function (ree) {
                            if (ree.ROWUID == recort.ROWUID) sameRowuid = 1;
                        });

                        if (sameRowuid == 0) rec.push(recort)
                    });
                }
            }
        });


        sketch.vectors.filter(function (vect) { return (vect.isModified || (sketch.sid == sketchApp.currentSketch.sid)); }).forEach(function (vector, j) {
            let opt = '', code = (vector.code && vector.code != '') ? vector.code : vector.name;
            valid = false
            //if(vector.code=="NCA") return;

            if ((!vector.isFreeFormLineEntries) && (lookupdata['AREA_CODES'][code] || vector.name)) {
                let addVal = lookupdata['AREA_CODES'][code]?.AdditionalValue1 ? lookupdata['AREA_CODES'][code].AdditionalValue1 : "",
                    vector_descrp = lookupdata['AREA_CODES'][code]?.['Name'] ? lookupdata['AREA_CODES'][code]['Name'] : (vector.name ? vector.name : ""),
                    apexLink, identifyApex = 0, activeFlag, save_dtl_lookup = FTC_Detail_Lookup.filter((lkup) => { return (lkup.IMPDETAILDESCRIPTION == vector_descrp && lkup.APEXAREACODE == vector.code) })[0];

                if (vector.associateValues === undefined) vector.associateValues = {};
                if (!save_dtl_lookup) save_dtl_lookup = FTC_Detail_Lookup.filter((lkup) => { return (lkup.APEXAREACODE == null && lkup.IMPDETAILDESCRIPTION == vector_descrp) })[0];

                let save_add_on_lookup = FTC_AddOns_Lookup.filter((lkup) => { return lkup.ADDONCODE == code; })[0],
                    save_floor_lookup = FTC_Floor_Lookup.filter((lkup) => { return lkup.IMPSFLOORDESCRIPTION == vector_descrp; })[0];

                if (sourceTable == 'CCV_IMPDETAIL_ATTACHED_DETACHED') {
                    if (save_dtl_lookup) {
                        apexLink = save_dtl_lookup.APEXLINKFLAG;
                        activeFlag = save_dtl_lookup.ACTIVEFLAG;
                    }
                }
                else if (sourceTable == 'CCV_IMPDETAIL_ADD_ONS') {
                    if (save_add_on_lookup) {
                        apexLink = save_add_on_lookup.APEXLINKFLAG;
                        activeFlag = save_add_on_lookup.ACTIVEFLAG;
                    }
                }
                else if (sourceTable == 'IMPBUILTASFLOOR') {
                    if (save_floor_lookup) {
                        apexLink = save_floor_lookup.APEXLINKFLAG;
                        activeFlag = save_floor_lookup.ACTIVEFLAG;
                    }
                }

                if (save_dtl_lookup == undefined && save_add_on_lookup == undefined && save_floor_lookup == undefined) return;
                if (sourceTable == 'CCV_IMPDETAIL_ATTACHED_DETACHED' && save_dtl_lookup == undefined) return;
                if (apexLink == '0' || activeFlag == '0') identifyApex = 1;

                if (sourceTable == 'CCV_IMPDETAIL_ATTACHED_DETACHED' && ["GLA", "GBA", "Add On"].indexOf(addVal) == -1)
                    valid = true;
                else if (sourceTable == 'CCV_IMPDETAIL_ADD_ONS' && addVal == "Add On")
                    valid = true;
                else if (sourceTable == 'IMPBUILTASFLOOR' && ["GLA", "GBA"].indexOf(addVal) > -1)
                    valid = true;

                if (valid) {
                    /*var blt_rec = parcel.IMPBUILTAS.filter(function(b_rec){ return b_rec.ParentROWUID == sketch.parentRow.ParentROWUID })[0];
                    if (sourceTable == 'IMPBUILTASFLOOR' && blt_rec) 
                        rec = blt_rec[sourceTable].filter(function(blt_r){ return blt_r.APEXLINKFLAG != '0'; });
                    else 
                        rec = parcel[sourceTable].filter(function(src_rec){ return src_rec.ParentROWUID == sketch.parentRow.ParentROWUID && src_rec.APEXLINKFLAG != '0'; })
                   */
                    if (identifyApex == 1) {
                        opt += '<tr sketch="' + i + '" vector="' + j + '" apexlink="' + 0 + '" style="display:none;">';
                        identifyApex = 0;
                        if (toHideDropDown != 1) toHideDropDown = 2;
                    }
                    else {
                        opt += '<tr sketch="' + i + '" vector="' + j + '">';
                        toHideDropDown = 1;
                    }

                    if (fieldHtml == '') {
                        $.each(SketchAssociateFields, (key, table) => {
                            fieldHtml += '<td style="text-align: center;"><select class="popupselect1" sourceTable="' + sourceTable + '" fieldName="' + key + '" style="width: ' + ((key.indexOf('-') > -1) ? '200' : '175') + 'px;float: none;">';
                            fieldHtml += '<option value="0" value1=""></option>';

                            if (table == 'IMPBUILTASFLOOR' && key == "IMPSFLOORDESCRIPTION-BLTASSF") {
                                key = "IMPSFLOORDESCRIPTION-BLTASFLOORSF";
                            }

                            let field = key.split('/')[0], isRowuid = key.split('/')[1] ? true : false;

                            let value = rec.map((r) => {
                                let val = field.split('-').map((fld) => { return (r[fld] ? r[fld] : (r[fld] === null ? 'null' : '')) }).join(' - ');
                                return { [field]: val, rowid: r.ROWUID }
                            });

                            if (table == 'IMPBUILTAS' && blt_rec) {
                                blt_rec.IMPBUILTASFLOOR.forEach((flr, idx) => { fieldHtml += '<option rowid="' + flr.ROWUID + '" value="' + (idx + 1) + '" value1="' + blt_rec[field] + '">' + blt_rec[field] + (isRowuid ? '/' + flr.ROWUID : '') + '</option>'; })
                            }
                            else
                                value.forEach(function (a, idx) { fieldHtml += '<option rowid="' + a.rowid + '" value="' + (idx + 1) + '" value1="' + a[field] + '">' + a[field] + (isRowuid ? '/' + a.rowid : '') + '</option>' });
                            fieldHtml += '</select></td>';
                        });
                    }
                    opt += ' <td><span>' + vector.code + " - " + (vector.label.split("]").length > 1 ? vector.label.split("]")[1] : vector.label) + '</span></td><td style= "text-align: center;"><span class="assoc_area">' + vector.area() + '</span></td>' + fieldHtml;
                    opt += '</tr>';
                }
                else {
                    if ((apexLink || apexLink == 0) && sourceTable == 'CCV_IMPDETAIL_ATTACHED_DETACHED' && save_dtl_lookup)
                        vector.isKeepAfterSave = true;
                    else if ((apexLink || apexLink == 0) && sourceTable == 'CCV_IMPDETAIL_ADD_ONS' && save_add_on_lookup)
                        vector.isKeepAfterSave = true;
                    else if ((apexLink || apexLink == 0) && sourceTable == 'IMPBUILTASFLOOR' && save_floor_lookup)
                        vector.isKeepAfterSave = true;
                }
                html += opt;
            }
        });
    });

    let tHid = '';
    if (toHideDropDown == 1) {
        tHid = '<tr toHide="' + 0 + '"></tr>'
        html += tHid;
    }
    else if (toHideDropDown == 2) {
        tHid = '<tr toHide="' + 1 + '"></tr>'
        html += tHid;
    }

    return html;
}

var FTC_associate_autoSelect = function (sketchApp) {
    $('.divvectors table tr[tohide="' + 1 + '"]').closest('table').closest('div').css("display", "none")

    sketchApp.sketches.filter((sk) => { return (sk.isModified || (sk.sid == sketchApp.currentSketch.sid)); }).forEach((sketch, sk_i) => {
        sketch.vectors.filter((v) => { return (!v.isFreeFormLineEntries) }).filter(function (vect) { return (vect.isModified || (sketch.sid == sketchApp.currentSketch.sid)); }).forEach((vector, vct_i) => {
            if (!vector.vectorString) return;

            let apex_id = vector.vectorString.split('[')[1].split(']')[0].split(/\,/g)[4],
                row = $('.divvectors tr[sketch="' + sk_i + '"][vector="' + vct_i + '"]').eq(0);

            if (apex_id != -1) {
                let apex = $('select', $(row)).eq(0), value = $('option[value1="' + apex_id + '"]', apex).val();
                $(apex).val(value);
                $(apex).trigger('change');
            }
            else {
                if (vector.tableRowId || vector.header.split('{')[1]) {
                    let rowwwid;

                    if (vector.tableRowId) {
                        rowwwid = vector.tableRowId.indexOf("$") == -1 ? vector.tableRowId.split("{")[1].split("}")[0] : vector.tableRowId.split("#")[1].split("$")[0];
                    }
                    else {
                        if (!vector.isPasteVector) {
                            let headeridd = vector.header.split('{')[1].split(']')[0].split(/\,/g)[0];
                            if (headeridd) rowwwid = headeridd.indexOf("$") == -1 ? headeridd.split("}")[0] : headeridd.split("#")[1].split("$")[0];
                        }
                    }

                    let apex = $('select', $(row)).eq(0), value = $('option[rowid="' + rowwwid + '"]', apex).val();

                    if (!vector.labelValueEdited) {
                        $(apex).val(value);
                        $(apex).trigger('change');
                    }
                }
            }
        });
    });
}

function FTCSketchAfterSave(sketchData, callback) {
    if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }
    var FTC_Detail_Lookup = (parent && parent.FTC_Detail_Lookup) ? parent.FTC_Detail_Lookup : FTC_Detail_Lookup ? FTC_Detail_Lookup : (window.opener ? window.opener.FTC_Detail_Lookup : []);
    var FTC_AddOns_Lookup = (parent && parent.FTC_AddOns_Lookup) ? parent.FTC_AddOns_Lookup : FTC_AddOns_Lookup ? FTC_AddOns_Lookup : (window.opener ? window.opener.FTC_AddOns_Lookup : []);
    var FTC_Floor_Lookup = (parent && parent.FTC_Floor_Lookup) ? parent.FTC_Floor_Lookup : FTC_Floor_Lookup ? FTC_Floor_Lookup : (window.opener ? window.opener.FTC_Floor_Lookup : []);
    var imp_dtl_tbl = 'CCV_IMPDETAIL_ATTACHED_DETACHED', add_on_tbl = 'CCV_IMPDETAIL_ADD_ONS', blt_as_tbl = 'IMPBUILTAS', bltas_flr_tbl = 'IMPBUILTASFLOOR', sketch_tbl = 'IMPAPEXOBJECT';
    var parcel = window.opener ? window.opener.activeParcel : activeParcel;
    var getDataField = window.opener ? window.opener.getDataField : getDataField;
    var getCategoryFromSourceTable = window.opener ? window.opener.getCategoryFromSourceTable : getCategoryFromSourceTable;
    var labelValidations = {
        imp: ['GBA', 'GLA'],
        det_val: ['BSMT', 'Porch', 'GAR', 'OTH', 'CARPT', 'COM SUB', 'Balcony', 'Storage', 'Add On']
    }
    var saveData = '', sketchSaveData = [], deleted_rec = [], perimeter = 0, area = 0, new_recs = [], refreshParcel = false, sketchSave = [], addonDescr, cc_sketch_save_data = [];
    var sketch_field = getDataField('CC_SKETCH', sketch_tbl);
    var perimeter_field = getDataField('IMPPERIMETER', 'IMP');
    var det_cat = getCategoryFromSourceTable(imp_dtl_tbl);
    var add_on_cat = getCategoryFromSourceTable(add_on_tbl);
    var det_apexid_field = getDataField('APEXID', imp_dtl_tbl);
    var addon_apexid_field = getDataField('APEXID', add_on_tbl);
    var det_area_field = getDataField('DETAILUNITCOUNT', imp_dtl_tbl);
    var det_descr = getDataField('IMPDETAILDESCRIPTION', imp_dtl_tbl);
    var det_calc_type_field = getDataField('DETAILCALCULATIONTYPE', imp_dtl_tbl);
    var det_type_id_field = getDataField('IMPDETAILTYPEID', imp_dtl_tbl);

    var addOn_descr = getDataField('IMPDETAILDESCRIPTION', add_on_tbl);
    var detail_type = getDataField('IMPDETAILTYPE', imp_dtl_tbl);
    var addOn_type = getDataField('ADDONCODE', add_on_tbl);
    var addOn_area_field = getDataField('DETAILUNITCOUNT', add_on_tbl);
    var ADDIMPDETAILTYPE = getDataField('IMPDETAILTYPE', 'CCV_IMPDETAIL_ADD_ONS');
    var addOn_type_id_field = getDataField('IMPDETAILTYPEID', add_on_tbl);
    var addOn_filter_type_field = getDataField('ADDONFILTERTYPE', add_on_tbl);

    var floor_cat = getCategoryFromSourceTable(bltas_flr_tbl);
    var bltas_area_field = getDataField('BLTASSF', blt_as_tbl);
    var floor_descr = getDataField('IMPSFLOORDESCRIPTION', bltas_flr_tbl);
    var floor_story_height = getDataField('STORYHEIGHT', bltas_flr_tbl);
    var floor_area = getDataField('BLTASFLOORSF', bltas_flr_tbl);
    var floor_apex_id = getDataField('APEXID', bltas_flr_tbl);
    var lookupdata = window.opener ? window.opener.lookup : lookup;
    sketchApp.sketches.filter(function (sk) { return (sk.wasModified || (sk.sid == sketchApp.currentSketch.sid)); }).forEach(function (sketch) {
        var skRowid = sketch.parentRow.ParentROWUID;
        var imp_data = sketch.parentRow.parentRecord;
        var bltas_record = imp_data[blt_as_tbl][0] || { ROWUID: 'sk-' + skRowid, BLTASSTORYHEIGHT: null, [bltas_flr_tbl]: [] };
        area = 0, perimeter = 0;
        sketch.vectors.filter(function (vect) { return (vect.wasModified || (sketch.sid == sketchApp.currentSketch.sid)); }).forEach(function (vector, vect_index) {
            var code = ((vector.code && vector.code != '') ? vector.code : vector.name)
            if (!lookupdata['AREA_CODES'] || !lookupdata['AREA_CODES'][code]) return;
            var condition = false;
            var apex_id = vector.vectorString.split('[')[1].split(']')[0].split(/\,/g)[4];
            var assocVal = vector.associateValues;
            var addVal = lookupdata['AREA_CODES'][code]['AdditionalValue1'];
            let vectuid = vector.clientId;
            var vector_descr = lookupdata['AREA_CODES'][code]['Name'];
            var save_dtl_lookup = FTC_Detail_Lookup.filter(function (lkup) { return (lkup.IMPDETAILDESCRIPTION == vector_descr && lkup.APEXAREACODE == vector.code) })[0];
            if (!save_dtl_lookup) {
                save_dtl_lookup = FTC_Detail_Lookup.filter(function (lkup) { return (lkup.APEXAREACODE == null && lkup.IMPDETAILDESCRIPTION == vector_descr) })[0];
            }
            var save_add_on_lookup = FTC_AddOns_Lookup.filter(function (lkup) { return lkup.ADDONCODE == code; })[0];
            var save_floor_lookup = FTC_Floor_Lookup.filter(function (lkup) { return lkup.IMPSFLOORDESCRIPTION == vector_descr; })[0];
            var det_type = code, det_type_id = null, det_field_1 = null, apexLink = null, activeFlag = null;
            if (save_dtl_lookup) {
                det_type = save_dtl_lookup.IMPDETAILTYPE;
                det_type_id = save_dtl_lookup.IMPDETAILTYPEID;
                det_field_1 = save_dtl_lookup.DETAILCALCULATIONTYPE;
                apexLink = save_dtl_lookup.APEXLINKFLAG;
                activeFlag = save_dtl_lookup.ACTIVEFLAG;
                addonDescr = save_dtl_lookup.IMPDETAILDESCRIPTION;
            }
            else if (save_add_on_lookup) {
                det_type = save_add_on_lookup.ADDONCODE;
                det_type_id = save_add_on_lookup.ADDONCODE;
                det_field_1 = save_add_on_lookup.ADDONFILTERTYPE;
                apexLink = save_add_on_lookup.APEXLINKFLAG;
                activeFlag = save_add_on_lookup.ACTIVEFLAG;
                addonDescr = save_add_on_lookup.ADDONDESCRIPTION;
            }
            else if (save_floor_lookup) {
                apexLink = save_floor_lookup.APEXLINKFLAG
                activeFlag = save_floor_lookup.ACTIVEFLAG;
            };
            var GLAIdLabel = (code.indexOf("GLA") > -1 || code.indexOf("GBA") > -1) ? true : false;
            area += ((labelValidations.imp.indexOf(addVal) > -1 || GLAIdLabel) ? vector.area() : 0);
            perimeter += ((labelValidations.imp.indexOf(addVal) > -1 || GLAIdLabel) ? vector.perimeter() : 0);
            if (save_dtl_lookup == undefined && save_add_on_lookup == undefined && save_floor_lookup == undefined) {
                cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: null, vect_index: vect_index, tbl: null });
                return;
            }
            if (assocVal[imp_dtl_tbl]) {
                if (apexLink == "0" || activeFlag == "0") {
                    if (vectuid) {
                        cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: null, vect_index: vect_index, tbl: imp_dtl_tbl });
                        return;
                    }
                    else {
                        let rowwwId = null;
                        if (vector.header.split('{')[1]) {
                            let headeridd = vector.header.split('{')[1].split(']')[0].split(/\,/g)[0];
                            if (headeridd) rowwwId = headeridd.indexOf("$") == -1 ? headeridd.split("}")[0] : headeridd.split("#")[1].split("$")[0];
                        }
                        else {
                            if (vector.vectorString) {
                                let apex_id = vector.vectorString.split('[')[1].split(']')[0].split(/\,/g)[4];
                                if (apex_id > -1) {
                                    let recordAdd = parcel[imp_dtl_tbl].filter(function (r) { return r.APEXID == apex_id })[0];
                                    if (recordAdd) rowwwId = recordAdd.ROWUID;
                                }
                            }
                        }
                        sketchSaveData.push({ sourceTable: imp_dtl_tbl, rowid: rowwwId ? rowwwId : null, apexId: apex_id, area: vector.area(), parentrowid: skRowid, story_ht: null, description: addonDescr, type: (assocVal[imp_dtl_tbl] && assocVal[imp_dtl_tbl].IMPDETAILTYPE) ? assocVal[imp_dtl_tbl].IMPDETAILTYPE : det_type, field_1: det_field_1, type_id: det_type_id });
                        cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: rowwwId ? rowwwId : null, vect_index: vect_index, tbl: imp_dtl_tbl });
                    }
                }
                else {
                    if (!assocVal[imp_dtl_tbl].rowuid) {
                        //var exist_new = new_recs.filter(function(nw){ return nw.apexId == apex_id && nw.parentrowid == skRowid && nw.sourceTable == imp_dtl_tbl && nw.description == vector_descr; });
                        //if (exist_new.length == 0)
                        new_recs.push({ area: vector.area(), apexId: apex_id, parentrowid: skRowid, sourceTable: imp_dtl_tbl, story_ht: null, description: vector_descr, type: det_type, type_id: det_type_id, field_1: det_field_1, vect_string: vector.vectorString, sid: sketch.sid, vect_index: vect_index, });
                        //else
                        //exist_new[0].area += vector.area();
                    }
                    else {
                        if (assocVal[imp_dtl_tbl]["IMPDETAILTYPE-IMPDETAILDESCRIPTION-DETAILUNITCOUNT"]) {
                            var Typee = assocVal[imp_dtl_tbl]["IMPDETAILTYPE-IMPDETAILDESCRIPTION-DETAILUNITCOUNT"].split('-')[0];
                            var Descr = assocVal[imp_dtl_tbl]["IMPDETAILTYPE-IMPDETAILDESCRIPTION-DETAILUNITCOUNT"].split('-')[1];
                        }
                        sketchSaveData.push({ sourceTable: imp_dtl_tbl, rowid: assocVal[imp_dtl_tbl].rowuid, apexId: apex_id, area: vector.area(), parentrowid: skRowid, story_ht: null, description: assocVal[imp_dtl_tbl].IMPDETAILDESCRIPTION ? assocVal[imp_dtl_tbl].IMPDETAILDESCRIPTION : $.trim(Descr), type: assocVal[imp_dtl_tbl].IMPDETAILTYPE ? assocVal[imp_dtl_tbl].IMPDETAILTYPE : $.trim(Typee), type_id: det_type_id, field_1: det_field_1 });
                        cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: assocVal[imp_dtl_tbl].rowuid, vect_index: vect_index, tbl: imp_dtl_tbl });
                    }
                }
            }

            if (assocVal[add_on_tbl]) {
                if (apexLink == "0" || activeFlag == "0") {
                    if (vectuid) {
                        cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: null, vect_index: vect_index, tbl: add_on_tbl });
                        return;
                    }
                    else {
                        let rowwwId;
                        if (vector.header.split('{')[1]) {
                            let headeridd = vector.header.split('{')[1].split(']')[0].split(/\,/g)[0];
                            if (headeridd) rowwwId = headeridd.indexOf("$") == -1 ? headeridd.split("}")[0] : headeridd.split("#")[1].split("$")[0];
                        }
                        else {
                            if (vector.vectorString) {
                                let apex_id = vector.vectorString.split('[')[1].split(']')[0].split(/\,/g)[4];
                                if (apex_id > -1) {
                                    let recordAdd = parcel[add_on_tbl].filter(function (r) { return r.APEXID == apex_id })[0];
                                    if (recordAdd) rowwwId = recordAdd.ROWUID;
                                }
                            }
                        }
                        if (rowwwId)
                            sketchSaveData.push({ sourceTable: add_on_tbl, rowid: rowwwId ? rowwwId : null, apexId: apex_id, area: ((labelValidations.det_val.indexOf(addVal) > -1) ? vector.area() : 0), parentrowid: skRowid, story_ht: null, description: addonDescr, type: (assocVal[add_on_tbl] && assocVal[add_on_tbl].IMPDETAILTYPE) ? assocVal[add_on_tbl].IMPDETAILTYPE : det_type, field_1: det_field_1 });
                        cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: rowwwId ? rowwwId : null, vect_index: vect_index, tbl: add_on_tbl });
                    }
                }
                else {
                    if (!assocVal[add_on_tbl].rowuid) {
                        //var exist_new = new_recs.filter(function(nw){ return nw.apexId == apex_id && nw.parentrowid == skRowid && nw.sourceTable == add_on_tbl && nw.description == vector_descr; });
                        //if (exist_new.length == 0)
                        new_recs.push({ area: ((labelValidations.det_val.indexOf(addVal) > -1) ? vector.area() : 0), apexId: apex_id, parentrowid: skRowid, sourceTable: add_on_tbl, story_ht: null, story_ht: null, description: vector_descr, type: det_type, vect_string: vector.vectorString, sid: sketch.sid, vect_index: vect_index, field_1: det_field_1 });
                        //else
                        //exist_new[0].area += vector.area();
                    }
                    else {
                        if (assocVal[add_on_tbl]["IMPDETAILTYPE-IMPDETAILDESCRIPTION-DETAILUNITCOUNT"]) {
                            var Typee = assocVal[add_on_tbl]["IMPDETAILTYPE-IMPDETAILDESCRIPTION-DETAILUNITCOUNT"].split('-')[0];
                            var Descr = assocVal[add_on_tbl]["IMPDETAILTYPE-IMPDETAILDESCRIPTION-DETAILUNITCOUNT"].split('-')[1];
                        }
                        if ($.trim(Descr))
                            var save_add_on_lookup = FTC_AddOns_Lookup.filter(function (lkup) { return lkup.ADDONDESCRIPTION == $.trim(Descr); })[0];
                        sketchSaveData.push({ sourceTable: add_on_tbl, rowid: assocVal[add_on_tbl].rowuid, apexId: apex_id, area: ((labelValidations.det_val.indexOf(addVal) > -1) ? vector.area() : 0), parentrowid: skRowid, story_ht: null, description: assocVal[add_on_tbl].IMPDETAILDESCRIPTION ? assocVal[add_on_tbl].IMPDETAILDESCRIPTION : $.trim(Descr), type: assocVal[add_on_tbl].IMPDETAILTYPE ? assocVal[add_on_tbl].IMPDETAILTYPE : save_add_on_lookup ? save_add_on_lookup.ADDONCODE : det_type, field_1: det_field_1 });
                        cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: assocVal[add_on_tbl].rowuid, vect_index: vect_index, tbl: add_on_tbl });
                    }
                }
            }

            if (assocVal[bltas_flr_tbl] && labelValidations.imp.indexOf(addVal) > -1) {
                if (apexLink == "0" || activeFlag == "0") {
                    if (vectuid) {
                        cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: null, vect_index: vect_index, tbl: bltas_flr_tbl });
                        return;
                    }
                    else {
                        let rowwwId = null;
                        if (vector.header.split('{')[1]) {
                            let headeridd = vector.header.split('{')[1].split(']')[0].split(/\,/g)[0];
                            if (headeridd) rowwwId = headeridd.indexOf("$") == -1 ? headeridd.split("}")[0] : headeridd.split("#")[1].split("$")[0];
                        }
                        else {
                            if (vector.vectorString) {
                                let apex_id = vector.vectorString.split('[')[1].split(']')[0].split(/\,/g)[4];
                                if (apex_id > -1) {
                                    let recordAdd = activeParcel[bltas_flr_tbl].filter(function (r) { return r.APEXID == apex_id })[0];
                                    if (recordAdd) rowwwId = recordAdd.ROWUID;
                                }
                            }
                        }
                        if (rowwwId)
                            sketchSaveData.push({ sourceTable: bltas_flr_tbl, rowid: rowwwId ? rowwwId : null, apexId: apex_id, area: vector.area(), parentrowid: bltas_record.ROWUID, story_ht: bltas_record.BLTASSTORYHEIGHT, description: impFlrDesc, type: assocVal[bltas_flr_tbl].IMPBUILTASFLOORID, apexLink: apexLink, activeFlag: activeFlag });
                        cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: rowwwId ? rowwwId : null, vect_index: vect_index, tbl: bltas_flr_tbl });

                    }
                }
                else {
                    if (!assocVal[bltas_flr_tbl].rowuid) {
                        //var exist_new = new_recs.filter(function(nw){ return nw.apexId == apex_id && nw.parentrowid == skRowid && nw.sourceTable == bltas_flr_tbl && nw.description == vector_descr; });
                        //if (exist_new.length == 0)
                        new_recs.push({ area: vector.area(), apexId: apex_id, parentrowid: bltas_record.ROWUID, sourceTable: bltas_flr_tbl, story_ht: bltas_record.BLTASSTORYHEIGHT, description: vector_descr, vect_string: vector.vectorString, sid: sketch.sid, vect_index: vect_index, });
                        //else
                        //exist_new[0].area += vector.area();
                    }
                    else {
                        if (assocVal[bltas_flr_tbl]["IMPSFLOORDESCRIPTION-BLTASSF"])
                            var descr = assocVal[bltas_flr_tbl]["IMPSFLOORDESCRIPTION-BLTASSF"].split('-')[0]
                        sketchSaveData.push({ sourceTable: bltas_flr_tbl, rowid: assocVal[bltas_flr_tbl].rowuid, apexId: apex_id, area: vector.area(), parentrowid: bltas_record.ROWUID, story_ht: bltas_record.BLTASSTORYHEIGHT, description: assocVal[bltas_flr_tbl].IMPSFLOORDESCRIPTION ? assocVal[bltas_flr_tbl].IMPSFLOORDESCRIPTION : $.trim(descr), type: assocVal[bltas_flr_tbl].IMPBUILTASFLOORID });
                        cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: assocVal[bltas_flr_tbl].rowuid, vect_index: vect_index, tbl: bltas_flr_tbl });
                    }
                }
            }
        });

        var getDeleteRec = function (tbl) {
            var records = (tbl == bltas_flr_tbl) ? bltas_record : imp_data
            records[tbl].forEach(function (rec) {
                if (!sketchSaveData.some(function (skd) { return skd.sourceTable == tbl && skd.rowid == rec.ROWUID; }) && rec.APEXLINKFLAG != '0')
                    saveData += '^^^' + parcel.Id + '|' + rec.ROWUID + '|||' + tbl + '|delete|\n';
            });
        }
        getDeleteRec(imp_dtl_tbl);
        getDeleteRec(add_on_tbl);
        getDeleteRec(bltas_flr_tbl);
        sketchSave.push({ rowid: skRowid, area: Math.round(area), perimeter: Math.round(perimeter) });
    });

    sketchSave.forEach(function (sk) {
        saveData += '^^^' + parcel.Id + '|' + sk.rowid + '|' + perimeter_field.Id + '||' + sk.perimeter + '|edit|\n';
        var bltasRec = parcel.IMPBUILTAS.filter(function (bltas) { return bltas.ParentROWUID == sk.rowid; })[0];
        var bltasNewRecs = new_recs.filter(function (nw_rec) { return nw_rec.parentrowid == 'sk-' + sk.rowid });
        if (bltasRec)
            saveData += '^^^' + parcel.Id + '|' + bltasRec.ROWUID + '|' + bltas_area_field.Id + '||' + sk.area + '|edit|' + sk.rowid + '\n';
        else if (bltasNewRecs.length > 0) {
            var rowId = ccTicks();
            saveData += '^^^' + parcel.Id + '|' + rowId + '|' + sk.rowid + '||IMPBUILTAS|new|\n';
            saveData += '^^^' + parcel.Id + '|' + rowId + '|' + bltas_area_field.Id + '||' + sk.area + '|edit|' + sk.rowid + '\n';
            bltasNewRecs.forEach(function (new_rec) { new_rec.parentrowid = rowId; });
        }
    });
    var rowId = ccTicks();
    new_recs.forEach(function (rec) {
        rowId += 10;
        saveData += '^^^' + parcel.Id + '|' + rowId + '|' + rec.parentrowid + '||' + rec.sourceTable + '|new|\n';
        sketchSaveData.push({ rowid: rowId, apexId: rec.apexId, area: rec.area, parentrowid: rec.parentrowid, sourceTable: rec.sourceTable, story_ht: rec.story_ht, description: rec.description, type: rec.type, type_id: rec.type_id ? rec.type_id : null, field_1: rec.field_1 ? rec.field_1 : null });
        cc_sketch_save_data.push({ vect_string: rec.vect_string, sid: rec.sid, rowid: rowId, vect_index: rec.vect_index, tbl: rec.sourceTable });
    });

    sketchSaveData.forEach(function (rec) {
        var apex_field = (rec.sourceTable == add_on_tbl ? addon_apexid_field : (rec.sourceTable == imp_dtl_tbl ? det_apexid_field : floor_apex_id));
        var area_field = (rec.sourceTable == add_on_tbl ? addOn_area_field : (rec.sourceTable == imp_dtl_tbl ? det_area_field : floor_area));
        var descr_field = (rec.sourceTable == add_on_tbl ? addOn_descr : (rec.sourceTable == imp_dtl_tbl ? det_descr : floor_descr));
        var type_field = (rec.sourceTable == add_on_tbl ? addOn_type : (rec.sourceTable == imp_dtl_tbl ? detail_type : null));
        let type_id_field = (rec.sourceTable == add_on_tbl ? addOn_type_id_field : (rec.sourceTable == imp_dtl_tbl ? det_type_id_field : null));
        let field1 = (rec.sourceTable == add_on_tbl ? addOn_filter_type_field : (rec.sourceTable == imp_dtl_tbl ? det_calc_type_field : null));

        var record = parcel[rec.sourceTable].filter(function (r) { return r.ROWUID == rec.rowid })[0];
        //if (record && record.APEXLINKFLAG == '0') return;

        saveData += '^^^' + parcel.Id + '|' + rec.rowid + '|' + apex_field.Id + '||' + rec.apexId + '|edit|' + rec.parentrowid + '\n';
        saveData += '^^^' + parcel.Id + '|' + rec.rowid + '|' + descr_field.Id + '||' + rec.description + '|edit|' + rec.parentrowid + '\n';
        if (type_field) saveData += '^^^' + parcel.Id + '|' + rec.rowid + '|' + type_field.Id + '||' + rec.type + '|edit|' + rec.parentrowid + '\n';
        if (type_id_field && rec.type_id) saveData += '^^^' + parcel.Id + '|' + rec.rowid + '|' + type_id_field.Id + '||' + rec.type_id + '|edit|' + rec.parentrowid + '\n';
        if (field1 && rec.field_1) saveData += '^^^' + parcel.Id + '|' + rec.rowid + '|' + field1.Id + '||' + rec.field_1 + '|edit|' + rec.parentrowid + '\n';

        //var parentRowId = rec.sourceTable == bltas_flr_tbl? activeParcel[blt_as_tbl].filter(function(blt){ return blt.ParentROWUID == rec.parentrowid })[0].ParentROWUID: rec.parentrowid;
        if (rec.sourceTable == add_on_tbl)
            saveData += '^^^' + parcel.Id + '|' + rec.rowid + '|' + ADDIMPDETAILTYPE.Id + '||' + ADDIMPDETAILTYPE.DefaultValue + '|edit|' + rec.parentrowid + '\n';
        //var skRowuids = parcel.IMPAPEXOBJECT.filter(function(imp_apex){ return sketchData.map(function(sk){ return sk.rowid; }).indexOf(imp_apex.ROWUID.toString()) > -1; }).map(function(imp){ return imp.ParentROWUID; });
        //if (skRowuids.indexOf(rec.parentrowid) > -1) {
        if (rec.sourceTable == bltas_flr_tbl && rec.story_ht)
            saveData += '^^^' + parcel.Id + '|' + rec.rowid + '|' + floor_story_height.Id + '||' + rec.story_ht + '|edit|' + rec.parentrowid + '\n';
        saveData += '^^^' + parcel.Id + '|' + rec.rowid + '|' + area_field.Id + '||' + Math.round(rec.area) + '|edit|' + rec.parentrowid + '\n';
    });

    function finalSave() {
        var isdtr = window.opener ? window.opener.__DTR : (typeof (__DTR) !== 'undefined' ? __DTR : false);
        let isSvApp = window.opener && window.opener.appType == "sv" ? true : (typeof (appType) !== 'undefined' && appType == "sv" ? true : false);
        var _appType = isdtr ? 'DTR' : (isSvApp ? 'SV' : 'QC');
        var saveRequestData = {
            ParcelId: parcel.Id,
            Priority: '',
            AlertMessage: '',
            Data: saveData,
            recoveryData: '',
            AppraisalType: '',
            activeParcelPhotoEditsdata: '',
            appType: _appType
        };

        if (saveData != '') {
            console.log(saveData);
            $.ajax({
                url: '/quality/saveparcelchanges.jrq',
                type: "POST",
                dataType: 'json',
                data: saveRequestData,
                success: function (resp) {
                    if (resp.status == "OK")
                        if (callback) callback();
                }
            });
        }
        else {
            if (callback) callback();
            return;
        }
    }
    var update_CC_Sketch = function (skSaveData, index) {
        var save_count = Object.keys(skSaveData).length;
        if (index == save_count) {
            //window.opener.saveSketchChanges(sketchDataArray,null,null,null,function () {
            finalSave();
            return;
            //});
        }
        var sid = Object.keys(skSaveData)[index];
        var sourceRecord = parcel.IMPAPEXOBJECT.filter(function (obj) { return obj.ROWUID == sid; })[0];
        //sketchDataArray.filter(function (f){return f.sid==sourceRecord.ROWUID})[0].vectorString=skSaveData[sid];
        saveData += '^^^' + parcel.Id + '|' + sourceRecord.ROWUID + '|' + sketch_field.Id + '||' + skSaveData[sid] + '|edit|' + sourceRecord.ParentROWUID + '\n';
        update_CC_Sketch(skSaveData, ++index);

    }

    var skSaveData = {}
    sketchApp.sketches.forEach(function (sketch) {
        _.sortBy(cc_sketch_save_data.filter(function (sk) { return sk.sid == sketch.sid }), 'vect_index').forEach(function (skSave) {
            var temp_str = skSave.vect_string.split('[')[1].split(']')[0].split(/\,/);
            var vect_str = '';
            if (skSave.rowid == null) {
                if (skSave.vect_string.indexOf("{") > -1) {
                    var t = skSave.vect_string.split('{')[0];
                    vect_str = t.substr(0, t.length - 1) + ']' + skSave.vect_string.split(']')[1] + ';';
                }
                else {
                    vect_str = skSave.vect_string + ';';
                }
            }
            else if (skSave.vect_string.indexOf("{") > -1)
                vect_str = skSave.vect_string.split('{')[0] + '{$' + skSave.tbl + '#' + skSave.rowid + '$}]' + skSave.vect_string.split(']')[1] + ';';
            else {
                vect_str = skSave.vect_string.split(']')[0] + ',{$' + skSave.tbl + '#' + skSave.rowid + '$}]' + skSave.vect_string.split(']')[1] + ';';
            }
            if (skSaveData[skSave.sid]) skSaveData[skSave.sid] += vect_str;
            else skSaveData[skSave.sid] = vect_str;
        });
    });
    update_CC_Sketch(skSaveData, 0);

}

function WinGapRSSketchAfterSave(sketchData, callback) {
    if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }
    var parcel = window.opener ? window.opener.activeParcel : activeParcel;
    var getDataField = window.opener ? window.opener.getDataField : getDataField;
    var getCategoryFromSourceTable = window.opener ? window.opener.getCategoryFromSourceTable : getCategoryFromSourceTable;
    var saveData = '';
    var sketch_Modified = [];
    sketch_Modified = sketchApp.sketches.filter(function (sk) { return (sk.wasModified) });
    sketch_Modified.forEach(function (skDatas) {
        var Vectors = skDatas.vectors;
        var RowuiD, parRowuid;
        var SketchTable = skDatas.config.VectorSource[0].Table;
        var existsField = getDataField("EXISTS", SketchTable);
        Vectors.forEach(function (VectorData) {
            var VectDataRowuid = VectorData.uid.split("/");
            if (VectDataRowuid.length > 1) {
                RowuiD = VectorData.clientId;
                parRowuid = skDatas.uid;
            }
            else {
                RowuiD = VectorData.uid;
                parRowuid = parcel[SketchTable].filter(function (re) { return re.ROWUID == RowuiD })[0].ParentROWUID;
            }
            saveData += '^^^' + parcel.Id + '|' + RowuiD + '|' + existsField.Id + '||' + 1 + '|edit|' + parRowuid + '\n';
        });
    });
    var isdtr = window.opener ? window.opener.__DTR : (typeof (__DTR) !== 'undefined' ? __DTR : false);
    let isSvApp = window.opener && window.opener.appType == "sv" ? true : (typeof (appType) !== 'undefined' && appType == "sv" ? true : false);
    var _appType = isdtr ? 'DTR' : (isSvApp ? 'SV' : 'QC');
    var saveRequestData = {
        ParcelId: parcel.Id,
        Priority: '',
        AlertMessage: '',
        Data: '',
        recoveryData: '',
        AppraisalType: '',
        activeParcelPhotoEditsdata: '',
        appType: _appType
    };

    if (saveData != '') {
        console.log(saveData);
        saveRequestData.Data = saveData;
        $.ajax({
            url: '/quality/saveparcelchanges.jrq',
            type: "POST",
            dataType: 'json',
            data: saveRequestData,
            success: function (resp) {
                if (resp.status == "OK")
                    if (callback) callback();
            }
        });
    }
    else {
        if (callback) callback();
        return;
    }
}

function WyandottSketchDefinition(data, isChanged) {
    var vector = '', vectorStart = true, sketches = [], vectorSource = ccma.Sketching.Config.sources[0].VectorSource[0], vectorCounter = 0, startPos = '';

    var convertToSketch = function (d, type, isAngled, isInverted) {
        var updownData = isInverted ? parseInt(d['CASKLDU' + type]) * -1 : parseInt(d['CASKLDU' + type]);
        var leftrightData = isInverted ? parseInt(d['CASKLRL' + type]) * -1 : parseInt(d['CASKLRL' + type]);

        return ' ' + (leftrightData > 0 ? 'R' + leftrightData : 'L' + leftrightData * -1) + (isAngled ? '/' : ' ') + (updownData > 0 ? 'D' + updownData : 'U' + updownData * -1);
    }

    data.map(function (d) { if (d.CASKLRECNUM) d.CASKLRECNUM = parseInt(d.CASKLRECNUM); })

    data = _.sortBy(data, 'CASKLRECNUM')
    data.forEach(function (d) {
        ++vectorCounter;
        if (d.CASKLDIAG1 != '+' && d.CASKLDIAG1 != '-') {
            if (vectorStart) {
                startPos = convertToSketch(d, 1);
                vector += ':' + startPos + ' S '
                vectorStart = false;
            }
            else vector += convertToSketch(d, 1);
            vector += convertToSketch(d, 2);
        }
        else {
            vector += convertToSketch(d, 1, true);
            vector += convertToSketch(d, 2);
        }
        if (d.CASKLSQFT != '0') {

            if (vectorCounter == 1) vector += convertToSketch(d, 2, false, true);

            sketches.push({
                uid: (d.CASKLPARCEL ? d.CASKLPARCEL.toString() : ''),
                label: [{ Field: 'CASKLCONS', Value: d.CASKLCONS, Description: null, IsEdited: isChanged, hiddenFromEdit: vectorSource.false, hiddenFromShow: false, lookup: null, Caption: vectorSource.labelCaption, ShowCurrentValueOnly: vectorSource.ShowCurrentValueOnly, lookUpQuery: null, splitByDelimiter: null, Target: vectorSource.LabelTarget, UseLookUpNameAsValue: null, IsLargeLookup: false }],
                vector: d.CASKLCONS + '[-1,-1,' + startPos + ']' + vector,
                labelPosition: null,
                isChanged: isChanged,
                vectorConfig: vectorSource
            });
            vectorStart = true;
            vector = '';
            startPos = '';
            vectorCounter = 0;
        }
    });
    console.log(sketches);
    return sketches;
}

function PnASketchBeforeSave(sketches, sqcallback) {
    var sketchData = [];
    sketches[0].vectors.filter(function (x) { return (x.isModified && !x.isDeletedVector) }).forEach(function (v) {
        sketchData.push({ 'source': v.sourceName, 'rowid': v.uid })
    })
    var parcel = window.opener ? window.opener.activeParcel : activeParcel;
    bldgSqft = [];
    $('#maskLayer').hide();
    $('.Current_vector_details .head').html('Sketch Sqft')
    $('.mask').show();
    $('.Current_vector_details').css('left', '');
    $('.Current_vector_details').css('width', 700);
    $('.Current_vector_details')
    var validation = true;
    $('.skNote').hide(); $('.Current_vector_details .vectorPage , .Current_vector_details .footradio,.Current_vector_details .obsketch').parent().remove()
    $('.assocValidation,.UnSketched').remove();
    $('.dynamic_prop div').remove();
    $('.dynamic_prop').html('')
    $('.Current_vector_details .PnANewValue ').html('');
    $('.Current_vector_details').css('left', '25%');
    $('.Current_vector_details').css('max-width', 700);
    $('.Current_vector_details').show()
    //$( '.Current_vector_details #Btn_cancel_vector_properties' ).hide();
    $('.Current_vector_details #Btn_cancel_vector_properties').css('margin-right', '229px');
    $('.Current_vector_details #Btn_cancel_vector_properties').bind(touchClickEvent, function () {
        $('.Current_vector_details #Btn_cancel_vector_properties').css('margin-right', '');
        $('.Current_vector_details').css('left', '35%');
        $('.Current_vector_details').hide();
        $('.Current_vector_details .sktsqft').remove();
        if (!sketchApp.isScreenLocked) $('.mask').hide();
        if (sqcallback) sqcallback(true, false);
        return false;
    })
    $('.Current_vector_details .head').after('<div class = "sktsqft"><p>The calculated sqft from the drawing does not match the total sqft on the building. Would you like to update the building sqft to match the sketch area?</p><br><div class = "pnaSqft" style = "max-height: 200px;overflow-y: auto"></div></div>');
    var html = '<table style="width: 100%;"><tr><th style="background: #d6d6d6;height:35px;width:200px">Building Type/Sequence</th><th style="background: #d6d6d6;height:35px;width:150px">Building Total Sqft</th><th style="background: #d6d6d6;height:35px;width:150px">Sketch Sqft</th><th style="background: #d6d6d6;height:35px;width:150px">Update Building Sqft</th></tr>';
    var value = false;
    sketchData.forEach(function (skDatas, i) {
        var cVector, newSketch = false, NewBldg;
        cVector = sketchApp.sketches[0].vectors.filter(function (vect) { return (vect.uid == skDatas.rowid) })[0];
        if (cVector.uid.split("/").length > 1) {
            BldRowuid = cVector.clientId;
            newSketch = true;
        }
        else
            BldRowuid = cVector.uid;
        NewBldg = cVector.BldgIsNew ? cVector.BldgIsNew : false;
        BldRec = parcel['bldg'] ? parcel['bldg'].filter(function (bl) { return bl.ROWUID == BldRowuid })[0] : [];
        var blgtotsqft, bldgId;
        if (BldRec) {
            blgtotsqft = BldRec['Total_Sqft'] ? BldRec['Total_Sqft'] : 0;
            bldgId = BldRec['Bldg_Id'];
        }
        var area = parseInt(cVector.area());
        if (!newSketch) {
            if (!NewBldg && blgtotsqft != area) {
                value = true;
                html += '<tr><td><span style="word-break: break-all;text-align: left">' + cVector.labelFields[0].Description.Name + '&nbsp;/&nbsp;' + BldRec.Sequence + '</span</td><td><span style="word-break: break-all;text-align: center">' + blgtotsqft + '</span></td><td><span style="word-break: break-all;text-align: center">' + area + '</span></td><td><select class="sqftddl" style="width: 150px;float: right;height: 20px;"><option value="1" rowid = ' + skDatas.rowid + '>Yes</option><option value="0" rowid = ' + skDatas.rowid + '>No</option></select></td></tr>'
            }
            else
                bldgSqft.push({ Rec_rowid: skDatas.rowid, sqftUpdate: '2' });
        }
    })
    html += '</table>';
    $('.pnaSqft').append(html);
    if (!value) {
        $('#maskLayer').show();
        $('.Current_vector_details .sktsqft').html('');
        $('.Current_vector_details #Btn_cancel_vector_properties').css('margin-right', '');
        $('.Current_vector_details').css('left', '35%');
        $('.Current_vector_details').hide();
        if (sqcallback) sqcallback(true, true);
    }
    $('.Current_vector_details #Btn_Save_vector_properties').unbind(touchClickEvent)
    $('.Current_vector_details #Btn_Save_vector_properties').bind(touchClickEvent, function () {
        $('#maskLayer').show();
        $('.pnaSqft .sqftddl').each(function (index, el) {
            var rowid = $('option[value="' + $(el).val() + '"]', $(el)).attr('rowid');
            var val_update = $(el).val();
            bldgSqft.push({ Rec_rowid: rowid, sqftUpdate: val_update });
        });
        if (!sketchApp.isScreenLocked) $('.mask').hide();
        $('.Current_vector_details .sktsqft').html('');
        $('.Current_vector_details #Btn_cancel_vector_properties').css('margin-right', '');
        $('.Current_vector_details').css('left', '35%');
        $('.Current_vector_details').hide();
        if (sqcallback) sqcallback(true, true);
        return false;
    })
}

function PnASketchAfterSave(sketchData, callback) {
    if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }
    var parcel = window.opener ? window.opener.activeParcel : activeParcel;
    var getDataField = window.opener ? window.opener.getDataField : getDataField;
    var getCategoryFromSourceTable = window.opener ? window.opener.getCategoryFromSourceTable : getCategoryFromSourceTable;
    var saveData = '';

    sketchData.forEach(function (sketchDatas) {
        var cVector = sketchApp.sketches[0].vectors.filter(function (vect) { return (vect.uid == sketchDatas.rowid) })[0];
        var isNewField = getDataField('Is_New', 'bldg');
        var totSqftField = getDataField('Total_Sqft', 'bldg');
        var newSqftField = getDataField('New_Sqft', 'bldg');
        var sqftField = getDataField('SqFt', 'bldg');
        var sketch_sqftField = getDataField('Sketch_Sqft', 'bldg');
        if (cVector && cVector.vectorString != '' && cVector.vectorString != null) {
            var newSketch = false, BldRowuid, BldRec = [];
            if (cVector.uid.split("/").length > 1) {
                BldRowuid = cVector.clientId;
                newSketch = true;
            }
            else
                BldRowuid = cVector.uid;
            BldRec = parcel['bldg'] ? parcel['bldg'].filter(function (bl) { return bl.ROWUID == BldRowuid })[0] : [];
            var blgtotsqft, areaUpdate, sqftRec;
            if (BldRec && bldgSqft) {
                sqftRec = bldgSqft.filter(function (x) { return x.Rec_rowid == BldRec.ROWUID })[0];
                areaUpdate = sqftRec ? sqftRec.sqftUpdate : 0;
                blgtotsqft = BldRec['Total_Sqft'];
            }
            var area = parseInt(cVector.area());
            if (newSketch) {
                var isNewPnASk = cVector.isPNANewValue;
                var isnewUpdate = (isNewPnASk == "1") ? true : false;
                saveData += '^^^' + parcel.Id + '|' + BldRowuid + '|' + isNewField.Id + '||' + isnewUpdate + '|edit|' + '\n';
                if (isNewPnASk == '0' || !isnewUpdate) {
                    saveData += '^^^' + parcel.Id + '|' + BldRowuid + '|' + sqftField.Id + '||' + area + '|edit|' + '\n';
                }
                else {
                    saveData += '^^^' + parcel.Id + '|' + BldRowuid + '|' + newSqftField.Id + '||' + area + '|edit|' + '\n';
                }
            }
            else {
                var isNewPnASk = BldRec.Is_New;
                if (isNewPnASk == 'false' || isNewPnASk == false) {
                    if (['1', '2', 1, 2].indexOf(areaUpdate) > -1) {
                        saveData += '^^^' + parcel.Id + '|' + BldRowuid + '|' + sqftField.Id + '||' + area + '|edit|' + '\n';
                    }
                }
                else {
                    if (['1', '2', 1, 2].indexOf(areaUpdate) > -1) {
                        saveData += '^^^' + parcel.Id + '|' + BldRowuid + '|' + newSqftField.Id + '||' + 0 + '|edit|' + '\n';
                    }
                }
            }
            if (sketch_sqftField)
                saveData += '^^^' + parcel.Id + '|' + BldRowuid + '|' + sketch_sqftField.Id + '||' + area + '|edit|' + '\n';
        }
    });
    var isdtr = window.opener ? window.opener.__DTR : (typeof (__DTR) !== 'undefined' ? __DTR : false);
    let isSvApp = window.opener && window.opener.appType == "sv" ? true : (typeof (appType) !== 'undefined' && appType == "sv" ? true : false);
    var _appType = isdtr ? 'DTR' : (isSvApp ? 'SV' : 'QC');
    var saveRequestData = {
        ParcelId: parcel.Id,
        Priority: '',
        AlertMessage: '',
        Data: '',
        recoveryData: '',
        AppraisalType: '',
        activeParcelPhotoEditsdata: '',
        appType: _appType
    };

    if (saveData != '') {
        console.log(saveData);
        saveRequestData.Data = saveData;
        $.ajax({
            url: '/quality/saveparcelchanges.jrq',
            type: "POST",
            dataType: 'json',
            data: saveRequestData,
            success: function (resp) {
                if (resp.status == "OK")
                    if (callback) callback();
            }
        });
    }
    else {
        if (callback) callback();
        return;
    }
}


function FTCJsonSketchAfterSave(sketchData, callback) {
    if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }

    var FTC_Detail_Lookup = FTC_Detail_Lookup ? FTC_Detail_Lookup : (window.opener ? window.opener.FTC_Detail_Lookup : []),
        FTC_AddOns_Lookup = FTC_AddOns_Lookup ? FTC_AddOns_Lookup : (window.opener ? window.opener.FTC_AddOns_Lookup : []),
        FTC_Floor_Lookup = FTC_Floor_Lookup ? FTC_Floor_Lookup : (window.opener ? window.opener.FTC_Floor_Lookup : []),
        imp_dtl_tbl = 'CCV_IMPDETAIL_ATTACHED_DETACHED', add_on_tbl = 'CCV_IMPDETAIL_ADD_ONS', blt_as_tbl = 'IMPBUILTAS', bltas_flr_tbl = 'IMPBUILTASFLOOR', sketch_tbl = 'IMPAPEXOBJECT',
        parcel = window.opener ? window.opener.activeParcel : activeParcel, getDataField = window.opener ? window.opener.getDataField : getDataField,
        getCategoryFromSourceTable = window.opener ? window.opener.getCategoryFromSourceTable : getCategoryFromSourceTable, sketchParentRowid = [],
        getCategory = window.opener ? window.opener.getCategory : getCategory;

    let labelValidations = {
        imp: ['GBA', 'GLA'],
        det_val: ['BSMT', 'Porch', 'GAR', 'OTH', 'CARPT', 'COM SUB', 'Balcony', 'Storage', 'Add On']
    }

    let saveData = '', sketchSaveData = [], deleted_rec = [], perimeter = 0, area = 0, new_recs = [], sketchSave = [], addonDescr, cc_sketch_save_data = [],
        sketch_field = getDataField('CC_SKETCH', sketch_tbl), perimeter_field = getDataField('IMPPERIMETER', 'IMP'), det_cat = getCategoryFromSourceTable(imp_dtl_tbl),
        add_on_cat = getCategoryFromSourceTable(add_on_tbl), det_apexid_field = getDataField('APEXID', imp_dtl_tbl), addon_apexid_field = getDataField('APEXID', add_on_tbl),
        det_area_field = getDataField('DETAILUNITCOUNT', imp_dtl_tbl), det_descr = getDataField('IMPDETAILDESCRIPTION', imp_dtl_tbl), det_apx_lnk_field = getDataField('APEXLINKFLAG', imp_dtl_tbl),
        det_calc_type_field = getDataField('DETAILCALCULATIONTYPE', imp_dtl_tbl), det_type_id_field = getDataField('IMPDETAILTYPEID', imp_dtl_tbl);

    let addOn_descr = getDataField('IMPDETAILDESCRIPTION', add_on_tbl), detail_type = getDataField('IMPDETAILTYPE', imp_dtl_tbl), addOn_type = getDataField('ADDONCODE', add_on_tbl),
        addOn_area_field = getDataField('DETAILUNITCOUNT', add_on_tbl), ADDIMPDETAILTYPE = getDataField('IMPDETAILTYPE', 'CCV_IMPDETAIL_ADD_ONS'),
        addOn_type_id_field = getDataField('IMPDETAILTYPEID', add_on_tbl), addOn_filter_type_field = getDataField('ADDONFILTERTYPE', add_on_tbl), addon_apx_lnk_field = getDataField('APEXLINKFLAG', add_on_tbl);

    let floor_cat = getCategoryFromSourceTable(bltas_flr_tbl), bltas_area_field = getDataField('BLTASSF', blt_as_tbl), floor_descr = getDataField('IMPSFLOORDESCRIPTION', bltas_flr_tbl),
        floor_story_height = getDataField('STORYHEIGHT', bltas_flr_tbl), floor_area = getDataField('BLTASFLOORSF', bltas_flr_tbl), floor_apex_id = getDataField('APEXID', bltas_flr_tbl),
        floor_apx_lnk_field = getDataField('APEXLINKFLAG', bltas_flr_tbl), lookupdata = window.opener ? window.opener.lookup : lookup, rowidd = [], rowiddAddon = [], rowiddFloor = [],
        cateIdd = getCategoryFromSourceTable(imp_dtl_tbl).Id, addOnCatId = getCategoryFromSourceTable(add_on_tbl).Id, floorCatId = getCategoryFromSourceTable(bltas_flr_tbl).Id;

    sketchApp.sketches.filter((sk) => { return (sk.wasModified || (sk.sid == sketchApp.currentSketch.sid)); }).forEach((sketch) => {
        let skRowid = sketch.parentRow.ParentROWUID, imp_data = sketch.parentRow.parentRecord, DetailrecRecord = [], AddOnsrecRecord = [], FloorrecRecord = [], _cnBuiltasRec = false;
        sketchParentRowid.push(skRowid);

        if (sketch.sid == sketchApp.currentSketch.sid) {
            let DetailRecord = [], AddOnsRecord = [], FloorRecord = [];
            DetailRecord = parcel["CCV_IMPDETAIL_ATTACHED_DETACHED"].filter((src_rec) => { return src_rec.ParentROWUID == skRowid && src_rec.CC_Deleted != true });

            DetailRecord.forEach((recort) => {
                let sameRowuid = 0, IMPDETAILDESCRIPTION, IMPDETAILTYPEID, apexLink, activeFlag;
                IMPDETAILDESCRIPTION = recort.IMPDETAILDESCRIPTION; IMPDETAILTYPEID = recort.IMPDETAILTYPEID;

                let save_dtl_lookup = FTC_Detail_Lookup.filter((lkup) => { return (lkup.IMPDETAILDESCRIPTION == IMPDETAILDESCRIPTION && lkup.IMPDETAILTYPEID == IMPDETAILTYPEID) })[0]
                if (!save_dtl_lookup) save_dtl_lookup = FTC_Detail_Lookup.filter((lkup) => { return (lkup.APEXAREACODE == null && lkup.IMPDETAILDESCRIPTION == IMPDETAILDESCRIPTION) })[0];

                if (save_dtl_lookup) {
                    apexLink = save_dtl_lookup.APEXLINKFLAG;
                    activeFlag = save_dtl_lookup.ACTIVEFLAG;
                }

                if (apexLink == 1 && activeFlag == 1) {
                    DetailrecRecord.forEach(function (ree) {
                        if (ree.ROWUID == recort.ROWUID) sameRowuid = 1;
                    });

                    if (sameRowuid == 0) DetailrecRecord.push(recort);
                }
            });

            AddOnsRecord = parcel["CCV_IMPDETAIL_ADD_ONS"].filter((src_rec) => { return src_rec.ParentROWUID == skRowid && src_rec.CC_Deleted != true });
            AddOnsRecord.forEach((recort) => {
                let sameRowuid = 0, IMPDETAILDESCRIPTION, apexLink, ADDONCODE, activeFlag;

                IMPDETAILDESCRIPTION = recort.IMPDETAILDESCRIPTION; ADDONCODE = recort.ADDONCODE;

                let save_add_on_lookup = FTC_AddOns_Lookup.filter((lkup) => { return (lkup.ADDONCODE == ADDONCODE || lkup.ADDONDESCRIPTION == IMPDETAILDESCRIPTION); })[0];

                if (save_add_on_lookup) {
                    apexLink = save_add_on_lookup.APEXLINKFLAG;
                    activeFlag = save_add_on_lookup.ACTIVEFLAG;
                }

                if (apexLink == 1 && activeFlag == 1) {
                    AddOnsrecRecord.forEach(function (ree) {
                        if (ree.ROWUID == recort.ROWUID) sameRowuid = 1;
                    });

                    if (sameRowuid == 0) AddOnsrecRecord.push(recort);
                }
            });

            let IMPBUILTASRecord = [];

            IMPBUILTASRecord = parcel["IMPBUILTAS"].filter((src_rec) => { return src_rec.ParentROWUID == skRowid && src_rec.CC_Deleted != true })[0];
            if (IMPBUILTASRecord) FloorRecord = IMPBUILTASRecord.IMPBUILTASFLOOR;

            FloorRecord.forEach((recort) => {
                let sameRowuid = 0, IMPSFLOORDESCRIPTION, apexLink, activeFlag; IMPSFLOORDESCRIPTION = recort.IMPSFLOORDESCRIPTION;
                let save_floor_lookup = FTC_Floor_Lookup.filter((lkup) => { return lkup.IMPSFLOORDESCRIPTION == IMPSFLOORDESCRIPTION; })[0];

                if (save_floor_lookup) {
                    apexLink = save_floor_lookup.APEXLINKFLAG;
                    activeFlag = save_floor_lookup.ACTIVEFLAG;
                }

                if (apexLink == 1 && activeFlag == 1) {
                    FloorrecRecord.forEach((ree) => {
                        if (ree.ROWUID == recort.ROWUID) sameRowuid = 1;
                    });

                    if (sameRowuid == 0) FloorrecRecord.push(recort);
                }
            });

        }
        else {
            let recdDetail = [], recdFloor = [], recdAddon = [];

            sketch.vectors.filter((vect) => { return (vect.wasModified); }).forEach((vectort) => {
                let code = (vectort.code && vectort.code != '') ? vectort.code : vectort.name,
                    vector_descrp = lookupdata['AREA_CODES'][code]?.['Name'] ? lookupdata['AREA_CODES'][code]['Name'] : (vectort.name ? vectort.name : ''),
                    apexLink, recd = [], activeFlag,
                    save_dtl_lookup = FTC_Detail_Lookup.filter((lkup) => { return (lkup.IMPDETAILDESCRIPTION == vector_descrp && lkup.APEXAREACODE == vectort.code) })[0];

                if (!save_dtl_lookup) save_dtl_lookup = FTC_Detail_Lookup.filter((lkup) => { return (lkup.APEXAREACODE == null && lkup.IMPDETAILDESCRIPTION == vector_descrp) })[0];
                let save_add_on_lookup = FTC_AddOns_Lookup.filter((lkup) => { return lkup.ADDONCODE == code; })[0];
                save_floor_lookup = FTC_Floor_Lookup.filter((lkup) => { return lkup.IMPSFLOORDESCRIPTION == vector_descrp; })[0];

                if (save_dtl_lookup) {
                    apexLink = save_dtl_lookup.APEXLINKFLAG;
                    activeFlag = save_dtl_lookup.ACTIVEFLAG;
                }
                else if (save_add_on_lookup) {
                    apexLink = save_add_on_lookup.APEXLINKFLAG;
                    activeFlag = save_add_on_lookup.ACTIVEFLAG;
                }
                else if (save_floor_lookup) {
                    apexLink = save_floor_lookup.APEXLINKFLAG;
                    activeFlag = save_floor_lookup.ACTIVEFLAG;
                }

                if (apexLink == 1 && activeFlag == 1 && save_dtl_lookup) {
                    recdDetail = parcel["CCV_IMPDETAIL_ATTACHED_DETACHED"].filter((src_rec) => { return src_rec.ParentROWUID == skRowid && src_rec.IMPDETAILDESCRIPTION == vector_descrp && src_rec.CC_Deleted != true });
                    recdDetail.forEach((recort) => {
                        let sameRowuid = 0;
                        DetailrecRecord.forEach((ree) => {
                            if (ree.ROWUID == recort.ROWUID) sameRowuid = 1;
                        });

                        if (sameRowuid == 0) DetailrecRecord.push(recort);
                    });
                }
                else if (apexLink == 1 && activeFlag == 1 && save_add_on_lookup) {
                    recdAddon = parcel["CCV_IMPDETAIL_ADD_ONS"].filter((src_rec) => { return src_rec.ParentROWUID == skRowid && src_rec.CC_Deleted != true && (src_rec.ADDONCODE == code || src_rec.IMPDETAILDESCRIPTION == vector_descrp); });
                    recdAddon.forEach((recort) => {
                        let sameRowuid = 0;
                        AddOnsrecRecord.forEach((ree) => {
                            if (ree.ROWUID == recort.ROWUID) sameRowuid = 1;
                        });

                        if (sameRowuid == 0) AddOnsrecRecord.push(recort);
                    });
                }
                else if (apexLink == 1 && activeFlag == 1 && save_floor_lookup) {
                    let IMPBUILTASRecord = [];
                    IMPBUILTASRecord = parcel["IMPBUILTAS"].filter((src_rec) => { return src_rec.ParentROWUID == skRowid && src_rec.CC_Deleted != true && src_rec.IMPSFLOORDESCRIPTION == vector_descrp; })[0];

                    if (IMPBUILTASRecord) recdFloor = IMPBUILTASRecord.IMPBUILTASFLOOR;

                    recdFloor.forEach((recort) => {
                        let sameRowuid = 0;
                        FloorrecRecord.forEach((ree) => {
                            if (ree.ROWUID == recort.ROWUID) sameRowuid = 1;
                        });

                        if (sameRowuid == 0) FloorrecRecord.push(recort);
                    });
                }

            });
        }

        for (i = 0; i < DetailrecRecord.length; i++) {
            rowidd.push(DetailrecRecord[i].ROWUID);
        }

        for (i = 0; i < AddOnsrecRecord.length; i++) {
            rowiddAddon.push(AddOnsrecRecord[i].ROWUID);
        }

        for (i = 0; i < FloorrecRecord.length; i++) {
            rowiddFloor.push(FloorrecRecord[i].ROWUID);
        }

        let bltas_record = imp_data[blt_as_tbl][0] || { ROWUID: 'sk-' + skRowid, BLTASSTORYHEIGHT: null, [bltas_flr_tbl]: [] };
        area = 0, perimeter = 0;
        bltas_record.BLTASSTORYHEIGHT = bltas_record.BLTASSTORYHEIGHT == null ? '' : bltas_record.BLTASSTORYHEIGHT;

        sketch.vectors.filter((v) => { return (!v.isFreeFormLineEntries) }).filter((vect) => { return ((vect.wasModified || (sketch.sid == sketchApp.currentSketch.sid)) && vect.vectorString != ''); }).forEach((vector, vect_index) => {
            let code = ((vector.code && vector.code != '') ? vector.code : vector.name);
            if (!lookupdata['AREA_CODES'][code] && !vector.name) return;

            let condition = false, apex_id = vector.vectorString.split('[')[1].split(']')[0].split(/\,/g)[4], assocVal = vector.associateValues,
                vectuid = vector.clientId,
                addVal = lookupdata['AREA_CODES'][code] ? lookupdata['AREA_CODES'][code]['AdditionalValue1'] : "",
                vector_descr = lookupdata['AREA_CODES'][code]?.['Name'] ? lookupdata['AREA_CODES'][code]['Name'] : (vector.name ? vector.name : ""),
                save_dtl_lookup = FTC_Detail_Lookup.filter((lkup) => { return (lkup.IMPDETAILDESCRIPTION == vector_descr && lkup.APEXAREACODE == vector.code) })[0];

            if (!save_dtl_lookup) save_dtl_lookup = FTC_Detail_Lookup.filter((lkup) => { return (lkup.APEXAREACODE == null && lkup.IMPDETAILDESCRIPTION == vector_descr) })[0];

            let save_add_on_lookup = FTC_AddOns_Lookup.filter((lkup) => { return lkup.ADDONCODE == code; })[0],
                save_floor_lookup = FTC_Floor_Lookup.filter((lkup) => { return lkup.IMPSFLOORDESCRIPTION == vector_descr; })[0],
                det_type = code, det_type_id = null, det_field_1 = null, apexLink = null, activeFlag = null;

            if (save_dtl_lookup) {
                det_type = save_dtl_lookup.IMPDETAILTYPE;
                det_type_id = save_dtl_lookup.IMPDETAILTYPEID;
                det_field_1 = save_dtl_lookup.DETAILCALCULATIONTYPE;
                apexLink = save_dtl_lookup.APEXLINKFLAG;
                activeFlag = save_dtl_lookup.ACTIVEFLAG;
                addonDescr = save_dtl_lookup.IMPDETAILDESCRIPTION;
            }
            else if (save_add_on_lookup) {
                det_type = save_add_on_lookup.ADDONCODE;
                det_type_id = save_add_on_lookup.ADDONCODE;
                det_field_1 = save_add_on_lookup.ADDONFILTERTYPE;
                apexLink = save_add_on_lookup.APEXLINKFLAG;
                activeFlag = save_add_on_lookup.ACTIVEFLAG;
                addonDescr = save_add_on_lookup.ADDONDESCRIPTION;
            }
            else if (save_floor_lookup) {
                apexLink = save_floor_lookup.APEXLINKFLAG;
                activeFlag = save_floor_lookup.ACTIVEFLAG;
            }

            let GLAIdLabel = (code.indexOf("GLA") > -1 || code.indexOf("GBA") > -1) ? true : false;
            _cnBuiltasRec = (!_cnBuiltasRec && (GLAIdLabel || (save_dtl_lookup == undefined && save_add_on_lookup == undefined && save_floor_lookup == undefined && ((labelValidations.imp.indexOf(addVal) > -1))))) ? true : false;
            area += ((labelValidations.imp.indexOf(addVal) > -1 || GLAIdLabel) ? Math.round(vector.area()) : 0);
            perimeter += ((labelValidations.imp.indexOf(addVal) > -1 || GLAIdLabel) ? vector.perimeter() : 0);

            if ((save_dtl_lookup == undefined && save_add_on_lookup == undefined && save_floor_lookup == undefined) || vector.isKeepAfterSave) {
                cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: null, vect_index: vect_index, tbl: null });
                return;
            }

            if (assocVal[imp_dtl_tbl]) {
                if (apexLink == "0" || activeFlag == "0") {
                    if (vectuid) {
                        cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: null, vect_index: vect_index, tbl: imp_dtl_tbl });
                        return;
                    }
                    else {
                        let rowwwId = null;
                        if (vector.header.split('{')[1]) {
                            let headeridd = vector.header.split('{')[1].split(']')[0].split(/\,/g)[0];
                            if (headeridd) rowwwId = headeridd.indexOf("$") == -1 ? headeridd.split("}")[0] : headeridd.split("#")[1].split("$")[0];
                        }
                        else {
                            if (vector.vectorString) {
                                let apex_id = vector.vectorString.split('[')[1].split(']')[0].split(/\,/g)[4];
                                if (apex_id > -1) {
                                    let recordAdd = parcel[imp_dtl_tbl].filter(function (r) { return r.APEXID == apex_id })[0];
                                    if (recordAdd) rowwwId = recordAdd.ROWUID;
                                }
                            }
                        }
                        sketchSaveData.push({ sourceTable: imp_dtl_tbl, rowid: rowwwId ? rowwwId : null, apexId: apex_id, area: vector.area(), parentrowid: skRowid, story_ht: null, description: addonDescr, type: (assocVal[imp_dtl_tbl] && assocVal[imp_dtl_tbl].IMPDETAILTYPE) ? assocVal[imp_dtl_tbl].IMPDETAILTYPE : det_type, field_1: det_field_1, type_id: det_type_id, apexLink: apexLink, activeFlag: activeFlag });
                        cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: rowwwId ? rowwwId : null, vect_index: vect_index, tbl: imp_dtl_tbl });
                    }
                }
                else {
                    if (!assocVal[imp_dtl_tbl].rowuid) {
                        new_recs.push({ area: vector.area(), apexId: apex_id, parentrowid: skRowid, sourceTable: imp_dtl_tbl, story_ht: null, description: vector_descr, type: det_type, vect_string: vector.vectorString, sid: sketch.sid, vect_index: vect_index, type_id: det_type_id, field_1: det_field_1, apexLink: apexLink, activeFlag: activeFlag });
                    }
                    else {
                        if (assocVal[imp_dtl_tbl]["IMPDETAILTYPE-IMPDETAILDESCRIPTION-DETAILUNITCOUNT"]) {
                            var Typee = assocVal[imp_dtl_tbl]["IMPDETAILTYPE-IMPDETAILDESCRIPTION-DETAILUNITCOUNT"].split('-')[0];
                            var Descr = assocVal[imp_dtl_tbl]["IMPDETAILTYPE-IMPDETAILDESCRIPTION-DETAILUNITCOUNT"].split('-')[1];
                        }

                        if (rowidd) {
                            rowidd.forEach((ridd) => {
                                if (ridd == assocVal[imp_dtl_tbl].rowuid) rowidd.splice(rowidd.indexOf(ridd), 1);
                            });
                        }

                        sketchSaveData.push({ sourceTable: imp_dtl_tbl, rowid: assocVal[imp_dtl_tbl].rowuid, apexId: apex_id, area: vector.area(), parentrowid: skRowid, story_ht: null, description: assocVal[imp_dtl_tbl].IMPDETAILDESCRIPTION ? assocVal[imp_dtl_tbl].IMPDETAILDESCRIPTION : $.trim(Descr), type: assocVal[imp_dtl_tbl].IMPDETAILTYPE ? assocVal[imp_dtl_tbl].IMPDETAILTYPE : $.trim(Typee), type_id: det_type_id, field_1: det_field_1, apexLink: apexLink, activeFlag: activeFlag });
                        cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: assocVal[imp_dtl_tbl].rowuid, vect_index: vect_index, tbl: imp_dtl_tbl });
                    }
                }
            }

            if (assocVal[add_on_tbl]) {
                if (apexLink == "0" || activeFlag == "0") {
                    if (vectuid) {
                        cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: null, vect_index: vect_index, tbl: add_on_tbl });
                        return;
                    }
                    else {
                        let rowwwId;
                        if (vector.header.split('{')[1]) {
                            let headeridd = vector.header.split('{')[1].split(']')[0].split(/\,/g)[0];
                            if (headeridd) rowwwId = headeridd.indexOf("$") == -1 ? headeridd.split("}")[0] : headeridd.split("#")[1].split("$")[0];
                        }
                        else {
                            if (vector.vectorString) {
                                let apex_id = vector.vectorString.split('[')[1].split(']')[0].split(/\,/g)[4];
                                if (apex_id > -1) {
                                    let recordAdd = parcel[add_on_tbl].filter(function (r) { return r.APEXID == apex_id })[0];
                                    if (recordAdd) rowwwId = recordAdd.ROWUID;
                                }
                            }
                        }
                        if (rowwwId)
                            sketchSaveData.push({ sourceTable: add_on_tbl, rowid: rowwwId ? rowwwId : null, apexId: apex_id, area: ((labelValidations.det_val.indexOf(addVal) > -1) ? vector.area() : 0), parentrowid: skRowid, story_ht: null, description: addonDescr, type: (assocVal[add_on_tbl] && assocVal[add_on_tbl].IMPDETAILTYPE) ? assocVal[add_on_tbl].IMPDETAILTYPE : det_type, field_1: det_field_1, apexLink: apexLink, activeFlag: activeFlag });
                        cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: rowwwId ? rowwwId : null, vect_index: vect_index, tbl: add_on_tbl });
                    }
                }
                else {
                    if (!assocVal[add_on_tbl].rowuid) {
                        new_recs.push({ area: ((labelValidations.det_val.indexOf(addVal) > -1) ? vector.area() : 0), apexId: apex_id, parentrowid: skRowid, sourceTable: add_on_tbl, story_ht: null, story_ht: null, description: vector_descr, type: det_type, vect_string: vector.vectorString, sid: sketch.sid, vect_index: vect_index, field_1: det_field_1, apexLink: apexLink, activeFlag: activeFlag });
                    }
                    else {
                        if (assocVal[add_on_tbl]["IMPDETAILTYPE-IMPDETAILDESCRIPTION-DETAILUNITCOUNT"]) {
                            var Typee = assocVal[add_on_tbl]["IMPDETAILTYPE-IMPDETAILDESCRIPTION-DETAILUNITCOUNT"].split('-')[0];
                            var Descr = assocVal[add_on_tbl]["IMPDETAILTYPE-IMPDETAILDESCRIPTION-DETAILUNITCOUNT"].split('-')[1];
                        }

                        let save_add_on_lookup;
                        if ($.trim(Descr)) save_add_on_lookup = FTC_AddOns_Lookup.filter((lkup) => { return lkup.ADDONDESCRIPTION == $.trim(Descr); })[0];

                        if (rowiddAddon) {
                            rowiddAddon.forEach((ridd) => {
                                if (ridd == assocVal[add_on_tbl].rowuid) rowiddAddon.splice(rowiddAddon.indexOf(ridd), 1);
                            });
                        }

                        sketchSaveData.push({ sourceTable: add_on_tbl, rowid: assocVal[add_on_tbl].rowuid, apexId: apex_id, area: ((labelValidations.det_val.indexOf(addVal) > -1) ? vector.area() : 0), parentrowid: skRowid, story_ht: null, description: assocVal[add_on_tbl].IMPDETAILDESCRIPTION ? assocVal[add_on_tbl].IMPDETAILDESCRIPTION : $.trim(Descr), type: assocVal[add_on_tbl].IMPDETAILTYPE ? assocVal[add_on_tbl].IMPDETAILTYPE : save_add_on_lookup ? save_add_on_lookup.ADDONCODE : det_type, field_1: det_field_1, apexLink: apexLink, activeFlag: activeFlag });
                        cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: assocVal[add_on_tbl].rowuid, vect_index: vect_index, tbl: add_on_tbl });
                    }
                }
            }

            if (assocVal[bltas_flr_tbl] && labelValidations.imp.indexOf(addVal) > -1) {
                if (apexLink == "0" || activeFlag == "0") {
                    if (vectuid) {
                        cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: null, vect_index: vect_index, tbl: bltas_flr_tbl });
                        return;
                    }
                    else {
                        let rowwwId = null;
                        if (vector.header.split('{')[1]) {
                            let headeridd = vector.header.split('{')[1].split(']')[0].split(/\,/g)[0];
                            if (headeridd) rowwwId = headeridd.indexOf("$") == -1 ? headeridd.split("}")[0] : headeridd.split("#")[1].split("$")[0];
                        }
                        else {
                            if (vector.vectorString) {
                                let apex_id = vector.vectorString.split('[')[1].split(']')[0].split(/\,/g)[4];
                                if (apex_id > -1) {
                                    let recordAdd = activeParcel[bltas_flr_tbl].filter(function (r) { return r.APEXID == apex_id })[0];
                                    if (recordAdd) rowwwId = recordAdd.ROWUID;
                                }
                            }
                        }

                        if (rowwwId)
                            sketchSaveData.push({ sourceTable: bltas_flr_tbl, rowid: rowwwId ? rowwwId : null, apexId: apex_id, area: vector.area(), parentrowid: bltas_record.ROWUID, story_ht: bltas_record.BLTASSTORYHEIGHT, description: impFlrDesc, type: assocVal[bltas_flr_tbl].IMPBUILTASFLOORID, apexLink: apexLink, activeFlag: activeFlag });
                        cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: rowwwId ? rowwwId : null, vect_index: vect_index, tbl: bltas_flr_tbl });
                    }
                }
                else {
                    if (!assocVal[bltas_flr_tbl].rowuid) {
                        new_recs.push({ area: vector.area(), apexId: apex_id, parentrowid: bltas_record.ROWUID, sourceTable: bltas_flr_tbl, story_ht: bltas_record.BLTASSTORYHEIGHT, description: vector_descr, vect_string: vector.vectorString, sid: sketch.sid, vect_index: vect_index, apexLink: apexLink, activeFlag: activeFlag });
                    }
                    else {
                        let descr = '';
                        if (assocVal[bltas_flr_tbl]["IMPSFLOORDESCRIPTION-BLTASSF"]) descr = assocVal[bltas_flr_tbl]["IMPSFLOORDESCRIPTION-BLTASSF"].split('-')[0];

                        if (rowiddFloor) {
                            rowiddFloor.forEach(function (ridd) {
                                if (ridd == assocVal[bltas_flr_tbl].rowuid) rowiddFloor.splice(rowiddFloor.indexOf(ridd), 1);
                            });
                        }

                        sketchSaveData.push({ sourceTable: bltas_flr_tbl, rowid: assocVal[bltas_flr_tbl].rowuid, apexId: apex_id, area: vector.area(), parentrowid: bltas_record.ROWUID, story_ht: bltas_record.BLTASSTORYHEIGHT, description: assocVal[bltas_flr_tbl].IMPSFLOORDESCRIPTION ? assocVal[bltas_flr_tbl].IMPSFLOORDESCRIPTION : $.trim(descr), type: assocVal[bltas_flr_tbl].IMPBUILTASFLOORID, apexLink: apexLink, activeFlag: activeFlag });
                        cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: assocVal[bltas_flr_tbl].rowuid, vect_index: vect_index, tbl: bltas_flr_tbl });
                    }
                }
            }
        });
        /*
        var getDeleteRec = function (tbl) {
            var records = (tbl == bltas_flr_tbl)? bltas_record: imp_data
            records[tbl].forEach(function(rec) {
                if (!sketchSaveData.some(function(skd) { return skd.sourceTable == tbl && skd.rowid == rec.ROWUID; }) && rec.APEXLINKFLAG != '0')
                    saveData += '^^^' + parcel.Id + '|' + rec.ROWUID + '|||' + tbl + '|delete|\n';
            });
        }
        getDeleteRec(imp_dtl_tbl);
        getDeleteRec(add_on_tbl);
        getDeleteRec(bltas_flr_tbl);*/
        sketchSave.push({ rowid: skRowid, area: Math.round(area), perimeter: Math.round(perimeter), _cnBuiltasRec: _cnBuiltasRec });
    });

    let currRowuid = [], forDelete = [], currentUid = sketchApp.currentSketch.uid, sketchee = sketchApp.sketches.filter((x) => { return x.uid != currentUid });

    sketchee.forEach((sketchees) => {
        let vectorrs = sketchees.vectors.filter((y) => { return y.isModified != true });
        vectorrs.filter((v) => { return (!v.isFreeFormLineEntries) }).forEach((vectorrrs) => {
            let tRowuid;

            if (vectorrrs.tableRowId) {
                if (vectorrrs.tableRowId.split('#')[1]) tRowuid = vectorrrs.tableRowId.split('#')[1].split('$')[0];
                else if (vectorrrs.tableRowId.split('{')[1]) tRowuid = vectorrrs.tableRowId.split('{')[1].split('}')[0];
                currRowuid.push(tRowuid); forDelete.push(tRowuid);
            }
            else if (vectorrrs.vectorString) {
                let apex_id = vectorrrs.vectorString.split('[')[1].split(']')[0].split(/\,/g)[4],
                    code = ((vectorrrs.code && vectorrrs.code != '') ? vectorrrs.code : vectorrrs.name)
                    vector_descrp = lookupdata['AREA_CODES'][vectorrrs.code]?.['Name'] ? lookupdata['AREA_CODES'][vectorrrs.code]['Name'] : (vectorrrs.name ? vectorrrs.name : ""),
                    save_dtl_lookup = FTC_Detail_Lookup.filter((lkup) => { return (lkup.IMPDETAILDESCRIPTION == vector_descrp && lkup.APEXAREACODE == vectorrs.code) })[0];

                if (!save_dtl_lookup) save_dtl_lookup = FTC_Detail_Lookup.filter((lkup) => { return (lkup.APEXAREACODE == null && lkup.IMPDETAILDESCRIPTION == vector_descrp) })[0];

                let save_add_on_lookup = FTC_AddOns_Lookup.filter((lkup) => { return lkup.ADDONCODE == code; })[0];
                save_floor_lookup = FTC_Floor_Lookup.filter((lkup) => { return lkup.IMPSFLOORDESCRIPTION == vector_descrp; })[0];

                if (apex_id > -1 && save_dtl_lookup) {
                    let recordAdd = parcel["CCV_IMPDETAIL_ATTACHED_DETACHED"].filter((r) => { return r.APEXID == apex_id && r.CC_Deleted != true })[0];
                    if (recordAdd) {
                        tRowuid = recordAdd.ROWUID;
                        currRowuid.push(tRowuid); forDelete.push(tRowuid);
                    }
                }
                else if (apex_id > -1 && save_add_on_lookup) {
                    let recordAdd = parcel["CCV_IMPDETAIL_ADD_ONS"].filter((r) => { return r.APEXID == apex_id && r.CC_Deleted != true })[0];
                    if (recordAdd) {
                        tRowuid = recordAdd.ROWUID;
                        forDelete.push(tRowuid);
                    }
                }
                else if (apex_id > -1 && save_floor_lookup) {
                    let recordAdd = parcel["IMPBUILTASFLOOR"].filter((r) => { return r.APEXID == apex_id && r.CC_Deleted != true })[0];
                    if (recordAdd) {
                        tRowuid = recordAdd.ROWUID;
                        forDelete.push(tRowuid);
                    }
                }
            }
        });
    });

    sketchApp.sketches.filter((sk) => { return (sk.wasModified && (sk.sid != sketchApp.currentSketch.sid)); }).forEach((sketchin) => {
        sketchin.vectors.filter((v) => { return (!v.isFreeFormLineEntries) }).filter((vect) => { return (vect.wasModified); }).forEach((vecteri, vect_inde) => {
            let assocValu = vecteri.associateValues, rowwwwwId, noRecord = false,
                code = ((vecteri.code && vecteri.code != '') ? vecteri.code : vecteri.name),
                vector_descr = lookupdata['AREA_CODES'][code]?.['Name'] ? lookupdata['AREA_CODES'][code]['Name'] : (vecteri.name ? vecteri.name : ""),
                save_dtl_lookup = FTC_Detail_Lookup.filter((lkup) => { return (lkup.IMPDETAILDESCRIPTION == vector_descr && lkup.APEXAREACODE == vecteri.code) })[0];

            if (!save_dtl_lookup) save_dtl_lookup = FTC_Detail_Lookup.filter((lkup) => { return (lkup.APEXAREACODE == null && lkup.IMPDETAILDESCRIPTION == vector_descr) })[0];

            let save_add_on_lookup = FTC_AddOns_Lookup.filter((lkup) => { return lkup.ADDONCODE == code; })[0],
                save_floor_lookup = FTC_Floor_Lookup.filter((lkup) => { return lkup.IMPSFLOORDESCRIPTION == vector_descr; })[0];

            if (save_dtl_lookup == undefined && save_add_on_lookup == undefined && save_floor_lookup == undefined) noRecord = true;

            if (!noRecord) {
                if (_.isEmpty(assocValu)) {
                    let tblname = save_dtl_lookup ? "CCV_IMPDETAIL_ATTACHED_DETACHED" : (save_add_on_lookup ? "CCV_IMPDETAIL_ADD_ONS" : (save_floor_lookup ? "IMPBUILTASFLOOR" : null));
                    if (vecteri.vectorString.split('#')[1] && vecterin.vectorString.split('#')[1].split('$')[0]) rowwwwwId = vecteri.vectorString.split('#')[1].split('$')[0];
                    cc_sketch_save_data.push({ vect_string: vecteri.vectorString, sid: sketchin.sid, rowid: rowwwwwId ? rowwwwwId : null, vect_index: vect_inde, tbl: tblname });
                }
            }
        });

        sketchin.vectors.filter((v) => { return (!v.isFreeFormLineEntries) }).filter((vect) => { return (!vect.wasModified); }).forEach((vecterin, vect_index) => {
            let rowwwwId, code = ((vecterin.code && vecterin.code != '') ? vecterin.code : vecterin.name),
                vector_descr = lookupdata['AREA_CODES'][code]?.['Name'] ? lookupdata['AREA_CODES'][code]['Name'] : (vecterin.name ? vecterin.name : ""),
                save_dtl_lookup = FTC_Detail_Lookup.filter((lkup) => { return (lkup.IMPDETAILDESCRIPTION == vector_descr && lkup.APEXAREACODE == vecterin.code) })[0];

            if (!save_dtl_lookup) save_dtl_lookup = FTC_Detail_Lookup.filter((lkup) => { return (lkup.APEXAREACODE == null && lkup.IMPDETAILDESCRIPTION == vector_descr) })[0];

            let save_add_on_lookup = FTC_AddOns_Lookup.filter((lkup) => { return lkup.ADDONCODE == code; })[0],
                save_floor_lookup = FTC_Floor_Lookup.filter((lkup) => { return lkup.IMPSFLOORDESCRIPTION == vector_descr; })[0],
                tblname = save_dtl_lookup ? "CCV_IMPDETAIL_ATTACHED_DETACHED" : (save_add_on_lookup ? "CCV_IMPDETAIL_ADD_ONS" : (save_floor_lookup ? "IMPBUILTASFLOOR" : null));

            if (vecterin.vectorString.split('#')[1] && vecterin.vectorString.split('#')[1].split('$')[0]) rowwwwId = vecterin.vectorString.split('#')[1].split('$')[0];
            cc_sketch_save_data.push({ vect_string: vecterin.vectorString, sid: sketchin.sid, rowid: rowwwwId ? rowwwwId : null, vect_index: vect_index, tbl: tblname });
        });
    });


    let rRowuid = _.clone(rowidd);
    if (rRowuid && currRowuid) {
        rRowuid.forEach((rowiddd) => {
            for (let i = 0; i < currRowuid.length; i++) {
                if (rowiddd == currRowuid[i]) rowidd.splice(rowidd.indexOf(currRowuid[i]), 1);
            }
        });
    }

    if (rowidd) {
        rowidd.forEach((rowiddd) => {
            deleted_rec.push({ rowid: rowiddd, catid: cateIdd });
        });
    }

    if (rowiddAddon) {
        rowiddAddon.forEach((rowiddd) => {
            deleted_rec.push({ rowid: rowiddd, catid: addOnCatId });
        });
    }

    if (rowiddFloor) {
        rowiddFloor.forEach((rowiddd) => {
            deleted_rec.push({ rowid: rowiddd, catid: floorCatId });
        });
    }

    deleted_rec.forEach((del) => {
        let cat = getCategory(del.catid);
        saveData += '^^^' + parcel.Id + '|' + del.rowid + '|||' + cat.SourceTable + '|delete|\n';
    });

    sketchSave.forEach(function (sk) {
        saveData += '^^^' + parcel.Id + '|' + sk.rowid + '|' + perimeter_field.Id + '||' + sk.perimeter + '|edit|\n';
        let bltasRec = parcel.IMPBUILTAS.filter(function (bltas) { return bltas.ParentROWUID == sk.rowid; })[0],
            bltasNewRecs = new_recs.filter(function (nw_rec) { return nw_rec.parentrowid == 'sk-' + sk.rowid });

        if (bltasRec)
            saveData += '^^^' + parcel.Id + '|' + bltasRec.ROWUID + '|' + bltas_area_field.Id + '||' + sk.area + '|edit|' + sk.rowid + '\n';
        else if (bltasNewRecs.length > 0) {
            let rowId = ccTicks();
            saveData += '^^^' + parcel.Id + '|' + rowId + '|' + sk.rowid + '||IMPBUILTAS|new|\n';
            saveData += '^^^' + parcel.Id + '|' + rowId + '|' + bltas_area_field.Id + '||' + sk.area + '|edit|' + sk.rowid + '\n';
            bltasNewRecs.forEach(function (new_rec) { new_rec.parentrowid = rowId; });
        }
    });

    var rowId = ccTicks();
    new_recs.forEach(function (rec) {
        rowId += 10;
        saveData += '^^^' + parcel.Id + '|' + rowId + '|' + rec.parentrowid + '||' + rec.sourceTable + '|new|\n';
        sketchSaveData.push({ rowid: rowId, apexId: rec.apexId, area: rec.area, parentrowid: rec.parentrowid, sourceTable: rec.sourceTable, story_ht: rec.story_ht, description: rec.description, type: rec.type, type_id: rec.type_id ? rec.type_id : null, field_1: rec.field_1 ? rec.field_1 : null, apexLink: rec.apexLink ? rec.apexLink : null });
        cc_sketch_save_data.push({ vect_string: rec.vect_string, sid: rec.sid, rowid: rowId, vect_index: rec.vect_index, tbl: rec.sourceTable });
    });

    sketchSaveData.forEach(function (rec) {
        let apex_field = (rec.sourceTable == add_on_tbl ? addon_apexid_field : (rec.sourceTable == imp_dtl_tbl ? det_apexid_field : floor_apex_id)),
            area_field = (rec.sourceTable == add_on_tbl ? addOn_area_field : (rec.sourceTable == imp_dtl_tbl ? det_area_field : floor_area)),
            descr_field = (rec.sourceTable == add_on_tbl ? addOn_descr : (rec.sourceTable == imp_dtl_tbl ? det_descr : floor_descr)),
            type_field = (rec.sourceTable == add_on_tbl ? addOn_type : (rec.sourceTable == imp_dtl_tbl ? detail_type : null)),
            type_id_field = (rec.sourceTable == add_on_tbl ? addOn_type_id_field : (rec.sourceTable == imp_dtl_tbl ? det_type_id_field : null)),
            field1 = (rec.sourceTable == add_on_tbl ? addOn_filter_type_field : (rec.sourceTable == imp_dtl_tbl ? det_calc_type_field : null)),
            apex_link_field = (rec.sourceTable == add_on_tbl ? addon_apx_lnk_field : (rec.sourceTable == imp_dtl_tbl ? det_apx_lnk_field : floor_apx_lnk_field));

        saveData += '^^^' + parcel.Id + '|' + rec.rowid + '|' + apex_field.Id + '||' + rec.apexId + '|edit|' + rec.parentrowid + '\n';
        saveData += '^^^' + parcel.Id + '|' + rec.rowid + '|' + descr_field.Id + '||' + rec.description + '|edit|' + rec.parentrowid + '\n';
        if (type_field) saveData += '^^^' + parcel.Id + '|' + rec.rowid + '|' + type_field.Id + '||' + rec.type + '|edit|' + rec.parentrowid + '\n';
        if (type_id_field && rec.type_id) saveData += '^^^' + parcel.Id + '|' + rec.rowid + '|' + type_id_field.Id + '||' + rec.type_id + '|edit|' + rec.parentrowid + '\n';
        if (field1 && rec.field_1) saveData += '^^^' + parcel.Id + '|' + rec.rowid + '|' + field1.Id + '||' + rec.field_1 + '|edit|' + rec.parentrowid + '\n';
        if (apex_link_field && rec.apexLink) saveData += '^^^' + parcel.Id + '|' + rec.rowid + '|' + apex_link_field.Id + '||' + rec.apexLink + '|edit|' + rec.parentrowid + '\n';
        if (rec.sourceTable == add_on_tbl) saveData += '^^^' + parcel.Id + '|' + rec.rowid + '|' + ADDIMPDETAILTYPE.Id + '||' + ADDIMPDETAILTYPE.DefaultValue + '|edit|' + rec.parentrowid + '\n';
        if (rec.sourceTable == bltas_flr_tbl && rec.story_ht) saveData += '^^^' + parcel.Id + '|' + rec.rowid + '|' + floor_story_height.Id + '||' + rec.story_ht + '|edit|' + rec.parentrowid + '\n';
        saveData += '^^^' + parcel.Id + '|' + rec.rowid + '|' + area_field.Id + '||' + Math.round(rec.area) + '|edit|' + rec.parentrowid + '\n';
    });

    function finalSave() {
        var isdtr = window.opener ? window.opener.__DTR : (typeof (__DTR) !== 'undefined' ? __DTR : false);
        let isSvApp = window.opener && window.opener.appType == "sv" ? true : (typeof (appType) !== 'undefined' && appType == "sv" ? true : false);
        var _appType = isdtr ? 'DTR' : (isSvApp ? 'SV' : 'QC');
        var saveRequestData = {
            ParcelId: parcel.Id,
            Priority: '',
            AlertMessage: '',
            Data: saveData,
            recoveryData: '',
            AppraisalType: '',
            activeParcelPhotoEditsdata: '',
            appType: _appType
        };

        if (saveData != '') {
            console.log(saveData);
            $.ajax({
                url: '/quality/saveparcelchanges.jrq',
                type: "POST",
                dataType: 'json',
                data: saveRequestData,
                success: function (resp) {
                    if (resp.status == "OK")
                        if (callback) callback();
                }
            });
        }
        else {
            if (callback) callback();
            return;
        }
    }

    var update_CC_Sketch = function (skSaveData, index) {
        let save_count = Object.keys(skSaveData).length;
        if (index == save_count) {
            finalSave(); return;
        }

        let sid = Object.keys(skSaveData)[index], skSketch = sketchApp.sketches.filter((sk) => { return (sk.sid == sid) })[0], stringVal = '';
        if (skSketch) {
            var jstring = skSketch.jsonString; jstring.DCS = skSaveData[sid]; stringVal = JSON.stringify(jstring);
        }

        let sourceRecord = parcel.IMPAPEXOBJECT.filter((obj) => { return obj.ROWUID == sid; })[0];
        saveData += '^^^' + parcel.Id + '|' + sourceRecord.ROWUID + '|' + sketch_field.Id + '||' + stringVal + '|edit|' + sourceRecord.ParentROWUID + '\n';
        update_CC_Sketch(skSaveData, ++index);
    }

    let skSaveData = {}
    sketchApp.sketches.forEach((sketch) => {
        _.sortBy(cc_sketch_save_data.filter((sk) => { return sk.sid == sketch.sid }), 'vect_index').forEach((skSave) => {
            let temp_str = skSave.vect_string.split('[')[1].split(']')[0].split(/\,/), vect_str = '';

            if (skSave.rowid == null) {
                if (skSave.vect_string.indexOf("{") > -1) {
                    let t = skSave.vect_string.split('{')[0];
                    vect_str = t.substr(0, t.length - 1) + ']' + skSave.vect_string.split(']')[1] + ';';
                }
                else {
                    vect_str = skSave.vect_string + ';';
                }
            }
            else if (skSave.vect_string.indexOf("{") > -1) {
                vect_str = skSave.vect_string.split('{')[0] + '{$' + skSave.tbl + '#' + skSave.rowid + '$}]' + skSave.vect_string.split(']')[1] + ';';
            }
            else {
                vect_str = skSave.vect_string.split(']')[0] + ',{$' + skSave.tbl + '#' + skSave.rowid + '$}]' + skSave.vect_string.split(']')[1] + ';';
            }

            if (skSaveData[skSave.sid]) skSaveData[skSave.sid] += vect_str;
            else skSaveData[skSave.sid] = vect_str;
        });
    });

    update_CC_Sketch(skSaveData, 0);
}

function FarragutSketchAfterSave(sketchData, callback) {
    if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }

    var Farragut_Lookup = window.opener ? window.opener.Farragut_Lookup : Farragut_Lookup;
    var parcel = window.opener ? window.opener.activeParcel : activeParcel;
    var getDataField = window.opener ? window.opener.getDataField : getDataField;
    var getCategoryFromSourceTable = window.opener ? window.opener.getCategoryFromSourceTable : getCategoryFromSourceTable;
    var lookupdata = window.opener ? window.opener.lookup : lookup;

    var copySketchData = [], deletedRecords = [], resAddnTable = 'CCV_RES_ADDITION', comAddnTable = 'CCV_COM_ADDITION', secAddnTable = 'CCV_COM_SECTION_FEATURE', saveData = '', newRecordRowuid = ccTicks();
    var resAddnCatId = getCategoryFromSourceTable(resAddnTable).Id, comAddnCatId = getCategoryFromSourceTable(comAddnTable).Id, secCatId = getCategoryFromSourceTable(secAddnTable).Id;
    var resSketchFields = ['TOTAL_LIVING_AREA', 'MB_FOOTPRINT_AREA', 'MB_IS_SKETCHED', 'MB_SKETCH_VECTOR', 'MB_PERIMETER'], resFields = [];
    var comSketchFields = ['SECTION_GROSS_SQFT', 'ADDITION_GROSS_SQFT', 'BUILDING_GROSS_SQFT'], comFields = [];
    var resAddnFields = ['FOOTPRINT_AREA', 'LIVING_AREA_PCT', 'SIZE_FACTOR_FLAG', 'SIZE_FACTOR', 'NON_SKETCHED_FLAG', 'MAIN_LOOKUP'], rAddnFields = [];
    var comAddnFields = ['FOOTPRINT_AREA', 'LIVING_AREA_PCT', 'SIZE_FACTOR_FLAG', 'SIZE_FACTOR', 'NON_SKETCHED_FLAG', 'MAIN_LOOKUP'], cAddnFields = [];
    var sectFields = ['SQFT', 'SQFT_REFERENCE', 'IS_SKETCHED', 'OCCUPANCY', 'PERIMITER_INPUT'], snFields = [];

    resSketchFields.forEach(function (field) {
        resFields.push(getDataField(field, 'CCV_RES_BUILDING'));
    });
    comSketchFields.forEach(function (field) {
        rAddnFields.push(getDataField(field, 'CCV_COM_BUILDING'));
    });
    resAddnFields.forEach(function (field) {
        rAddnFields.push(getDataField(field, resAddnTable));
    });
    comAddnFields.forEach(function (field) {
        cAddnFields.push(getDataField(field, comAddnTable));
    });
    sectFields.forEach(function (field) {
        snFields.push(getDataField(field, secAddnTable));
    });

    sketchData.forEach(function (sdata) {
        var skt = sketchApp.sketches.filter(function (s) { return s.sid == sdata.rowid })[0];
        var sktTable = skt.config.SketchSource.Table;
        var sktRecord = parcel[sktTable].filter(function (s) { return s.ROWUID == sdata.rowid && s.CC_Deleted != true })[0];

        copySketchData.push({ sid: sdata.rowid, sketchTable: sktTable, parentRecord: sktRecord });
        if (sktTable == 'CCV_RES_BUILDING') {
            sktRecord[resAddnTable].filter(function (addn) { return addn.CC_Deleted != true && addn.NON_SKETCHED_FLAG == 'N' }).forEach(function (resAddn) {
                deletedRecords.push(_.clone({ rowuid: resAddn.ROWUID, sourceTable: resAddnTable }));
            });
        }
        else if (sktTable == 'CCV_COM_BUILDING') {
            sktRecord[comAddnTable].filter(function (addn) { return addn.CC_Deleted != true && addn.NON_SKETCHED_FLAG == 'N' }).forEach(function (comAddn) {
                deletedRecords.push(_.clone({ rowuid: comAddn.ROWUID, sourceTable: comAddnTable }));
            });
            sktRecord[secAddnTable].filter(function (sec) { return sec.CC_Deleted != true && sec.IS_SKETCHED == 'Y' }).forEach(function (resSec) {
                deletedRecords.push(_.clone({ rowuid: resSec.ROWUID, sourceTable: secAddnTable }));
            });
        }
    });

    function serverUpdate() {
        var isdtr = window.opener ? window.opener.__DTR : (typeof (__DTR) !== 'undefined' ? __DTR : false);
        let isSvApp = window.opener && window.opener.appType == "sv" ? true : (typeof (appType) !== 'undefined' && appType == "sv" ? true : false);
        var _appType = isdtr ? 'DTR' : (isSvApp ? 'SV' : 'QC');

        var saveRequestData = {
            ParcelId: parcel.Id,
            Priority: '',
            AlertMessage: '',
            Data: '',
            recoveryData: '',
            AppraisalType: '',
            activeParcelPhotoEditsdata: '',
            appType: _appType
        };

        if (saveData != '') {
            console.log(saveData);
            saveRequestData.Data = saveData;
            $.ajax({
                url: '/quality/saveparcelchanges.jrq',
                type: "POST",
                dataType: 'json',
                data: saveRequestData,
                success: function (resp) {
                    if (resp.status == "OK") {
                        console.log('sketch successfully saved');
                        if (callback) callback();
                        return;
                    }
                }
            });
        }
        else {
            if (callback) callback();
            return;
        }
    }

    var deleteRecord = function (delRecords) {
        delRecords.forEach(function (delRec) {
            saveData += '^^^' + parcel.Id + '|' + delRec.rowuid + '|||' + delRec.sourceTable + '|delete|\n';
        });
        serverUpdate(); return;
    }

    var processor = function (sketchRecords) {
        if (sketchRecords.length == 0) {
            deleteRecord(deletedRecords); return;
        }

        var sketchRecord = sketchRecords.shift();
        var sketh = sketchApp.sketches.filter(function (s) { return s.sid == sketchRecord.sid })[0];
        var vectors = sketh.vectors.filter(function (svs) { return !(svs.isFreeFormLineEntries) });
        var sketchTable = sketchRecord.sketchTable;
        var parentRecord = parcel[sketchTable].filter(function (s) { return s.ROWUID == sketchRecord.sid && s.CC_Deleted != true })[0];
        var sketchFields = [], newRecordList = [], isSketch = 'N', totLivingArea = 0, footPrintArea = 0, sectGrossSqft = 0, addnGrossSqft = 0, buidGrossSqft = 0, vectorString = '', basUpdated = false, mb_footPrintArea = 0, mb_perimeter = '';

        vectors.forEach(function (vector) {
            var area = vector.area(), perimeter = vector.perimeter();
            var code = vector.code;
            var fLookup = Farragut_Lookup.filter(function (f) { return f.ATTRIBUTE_VALUE == code })[0];
            var apexLookup = lookupdata['Apex_Area_Codes'][code];

            if (fLookup) {
                var lookupCode = fLookup.LOOKUP_VALUE_PK;
                var addnOrSect = fLookup.LOOKUP_TYPE_AdditionalValue1;
                var associateRowuid = vector.associateRowuid;
                isSketch = 'Y';
                var isNewRecord = false;
                if (vector.newRecord || !associateRowuid || associateRowuid == '0') {
                    newRecordRowuid = newRecordRowuid + 50;
                    associateRowuid = newRecordRowuid;
                    isNewRecord = true;
                }
                var newRecordTable = null;
                if (addnOrSect && addnOrSect == 'ADDN') {
                    if (sketchTable == 'CCV_RES_BUILDING') {
                        totLivingArea = fLookup.LIVING_AREA_FLAG == 'Y' ? (totLivingArea + ((apexLookup.NumericValue && apexLookup.NumericValue != '' && !isNaN(apexLookup.NumericValue)) ? (area * parseFloat(apexLookup.NumericValue)) : area)) : totLivingArea;
                        footPrintArea = footPrintArea + area;
                        mb_footPrintArea += ((code == 'BAS') ? mb_footPrintArea : 0);
                        newRecordList.push(_.clone({ SOURCE_TABLE: resAddnTable, CATEGORY_ID: resAddnCatId, PARENT_RECORD: parentRecord, FOOTPRINT_AREA: Math.round(area), LIVING_AREA_PCT: (fLookup.LIVING_AREA_FLAG == 'Y' ? 100 : 0), SIZE_FACTOR_FLAG: 'N', SIZE_FACTOR: 1, NON_SKETCHED_FLAG: 'N', MAIN_LOOKUP: fLookup.LOOKUP_VALUE_PK, isNewSketch: isNewRecord, rowuid: associateRowuid }));
                        newRecordTable = resAddnTable;
                        deletedRecords = deletedRecords.filter(function (d) { return !(d.rowuid == associateRowuid && d.sourceTable == resAddnTable) });
                    }
                    else if (sketchTable == 'CCV_COM_BUILDING') {
                        addnGrossSqft = (fLookup.LIVING_AREA_FLAG == 'Y' ? (addnGrossSqft + area) : addnGrossSqft);
                        newRecordList.push(_.clone({ SOURCE_TABLE: comAddnTable, CATEGORY_ID: comAddnCatId, PARENT_RECORD: parentRecord, FOOTPRINT_AREA: Math.round(area), LIVING_AREA_PCT: (fLookup.LIVING_AREA_FLAG == 'Y' ? 100 : 0), SIZE_FACTOR_FLAG: 'N', SIZE_FACTOR: 1, NON_SKETCHED_FLAG: 'N', MAIN_LOOKUP: fLookup.LOOKUP_VALUE_PK, isNewSketch: isNewRecord, rowuid: associateRowuid }));
                        newRecordTable = comAddnTable;
                        deletedRecords = deletedRecords.filter(function (d) { return !(d.rowuid == associateRowuid && d.sourceTable == comAddnTable) });
                    }
                }
                else if (addnOrSect && addnOrSect == 'SOCC' && sketchTable == 'CCV_COM_BUILDING') {
                    sectGrossSqft = sectGrossSqft + ((apexLookup.NumericValue && apexLookup.NumericValue != '' && !isNaN(apexLookup.NumericValue)) ? (area * parseFloat(apexLookup.NumericValue)) : area);
                    newRecordList.push(_.clone({ SOURCE_TABLE: secAddnTable, CATEGORY_ID: secCatId, PARENT_RECORD: parentRecord, SQFT: Math.round(area), SQFT_REFERENCE: 0, IS_SKETCHED: 'Y', OCCUPANCY: fLookup.LOOKUP_VALUE_PK, PERIMITER_INPUT: perimeter, isNewSketch: isNewRecord, rowuid: associateRowuid }));
                    newRecordTable = secAddnTable;
                    deletedRecords = deletedRecords.filter(function (d) { return !(d.rowuid == associateRowuid && d.sourceTable == secAddnTable) });
                }

                if (parseFloat(associateRowuid) < 0)
                    associateRowuid = '{$' + newRecordTable + '#' + parseFloat(associateRowuid) + '$}';
                else
                    associateRowuid = '{' + associateRowuid + '}';
                var tempstr = vector.vectorString.split('[')[1].split(']')[0].split(/\,/);
                if (tempstr[tempstr.length - 1].indexOf('{') > -1 && tempstr[tempstr.length - 1].indexOf('}') > -1)
                    tempstr.pop();
                tempstr[tempstr.length] = associateRowuid;
                vectorString += vector.vectorString.split('[')[0] + '[' + tempstr.toString() + ']' + vector.vectorString.split(']')[1] + ';';
            }
            else {
                if (code == 'BAS') {
                    if (sketchTable == 'CCV_RES_BUILDING') {
                        totLivingArea = totLivingArea + area;
                        mb_footPrintArea = mb_footPrintArea + area;
                        mb_perimeter = ((mb_perimeter == '') ? perimeter : (mb_perimeter + perimeter));
                    }
                    else if (sketchTable == 'CCV_COM_BUILDING')
                        addnGrossSqft = addnGrossSqft + area;
                    if (!basUpdated) basUpdated = true;
                }
                vectorString += vector.vectorString + ';';
            }
        });

        var jsonString = sketh.jsonString;
        if (jsonString) {
            jsonString.DCS = vectorString;
            vectorString = JSON.stringify(jsonString);
        }

        var sketchRecordResult = { TOTAL_LIVING_AREA: totLivingArea, MB_FOOTPRINT_AREA: Math.round(mb_footPrintArea), BUILDING_GROSS_SQFT: 0, ADDITION_GROSS_SQFT: addnGrossSqft, SECTION_GROSS_SQFT: sectGrossSqft, MB_IS_SKETCHED: isSketch, MB_SKETCH_VECTOR: '' };

        if (sketchTable == 'CCV_RES_BUILDING') {
            var nonSketchedRecords = parentRecord[resAddnTable].filter(function (rd) { return rd.CC_Deleted != true && rd.NON_SKETCHED_FLAG != 'N' });
            nonSketchedRecords.forEach(function (nonSketch) {
                var lkCode = nonSketch['MAIN_LOOKUP'];
                if (basUpdated && lkCode == "ADDNBSMTUF")
                    return;
                if (lkCode) {
                    var sklook = Farragut_Lookup.filter(function (f) { return f.LOOKUP_VALUE_PK == lkCode })[0];
                    var aplook = lookupdata['Apex_Area_Codes'][sklook.ATTRIBUTE_VALUE];
                    sketchRecordResult.TOTAL_LIVING_AREA = sketchRecordResult.TOTAL_LIVING_AREA + (sklook.LIVING_AREA_FLAG == 'Y' && nonSketch.FOOTPRINT_AREA ? ((aplook && aplook.NumericValue && aplook.NumericValue != '' && !isNaN(aplook.NumericValue)) ? (parseFloat(nonSketch.FOOTPRINT_AREA) * parseFloat(aplook.NumericValue)) : parseFloat(nonSketch.FOOTPRINT_AREA)) : 0);
                }
            });
            sketchRecordResult.TOTAL_LIVING_AREA = Math.round(sketchRecordResult.TOTAL_LIVING_AREA);
            sketchRecordResult.MB_PERIMETER = ((mb_perimeter == '') ? mb_perimeter : Math.round(mb_perimeter));

            resFields.forEach(function (rField) {
                if (rField) saveData += '^^^' + parcel.Id + '|' + parentRecord.ROWUID + '|' + rField.Id + '||' + sketchRecordResult[rField.Name] + '|edit|\n';
            });
        }
        else if (sketchTable == 'CCV_COM_BUILDING') {
            var nonAddSketchedRecords = parentRecord[comAddnTable].filter(function (rd) { return rd.CC_Deleted != true && rd.NON_SKETCHED_FLAG != 'N' });
            nonAddSketchedRecords.forEach(function (nonAddSketch) {
                var lkCode = nonAddSketch['MAIN_LOOKUP'];
                if (basUpdated && lkCode == "ADDNBSMTUF")
                    return;
                sketchRecordResult.ADDITION_GROSS_SQFT = sketchRecordResult.ADDITION_GROSS_SQFT + (nonAddSketch.LIVING_AREA_PCT == '100' && nonAddSketch.FOOTPRINT_AREA ? parseFloat(nonAddSketch.FOOTPRINT_AREA) : 0);
            });

            var nonSectSketchedRecords = parentRecord[secAddnTable].filter(function (rd) { return rd.CC_Deleted != true && rd.IS_SKETCHED != 'Y' });
            nonSectSketchedRecords.forEach(function (nonSectSketch) {
                sketchRecordResult.SECTION_GROSS_SQFT = sketchRecordResult.SECTION_GROSS_SQFT + (nonSectSketch.SQFT ? ((nonSectSketch.AREA_MODIFIER && nonSectSketch.AREA_MODIFIER != '' && !isNaN(nonSectSketch.AREA_MODIFIER)) ? (parseFloat(nonSectSketch.AREA_MODIFIER) * parseFloat(nonSectSketch.SQFT)) : parseFloat(nonSectSketch.SQFT)) : 0);
            });

            sketchRecordResult.BUILDING_GROSS_SQFT = Math.round(sketchRecordResult.ADDITION_GROSS_SQFT + sketchRecordResult.SECTION_GROSS_SQFT);
            sketchRecordResult.ADDITION_GROSS_SQFT = Math.round(sketchRecordResult.ADDITION_GROSS_SQFT);
            sketchRecordResult.SECTION_GROSS_SQFT = Math.round(sketchRecordResult.SECTION_GROSS_SQFT);

            comFields.forEach(function (cField) {
                if (cField) saveData += '^^^' + parcel.Id + '|' + parentRecord.ROWUID + '|' + cField.Id + '||' + sketchRecordResult[cField.Name] + '|edit|\n';
            });
        }

        var insertFunction = function (newRecordLists) {
            if (newRecordList.length == 0) {
                var sketchField = getDataField('CC_SKETCH', sketchTable);
                if (sketchField)
                    saveData += '^^^' + parcel.Id + '|' + parentRecord.ROWUID + '|' + sketchField.Id + '||' + vectorString + '|edit|\n';
                processor(copySketchData); return;
            }

            var newRecList = newRecordLists.shift();
            var recSourceTable = newRecList.SOURCE_TABLE, recCatId = newRecList.CATEGORY_ID, pRecord = newRecList.PARENT_RECORD, rowuid = newRecList.rowuid, newSketch = newRecList.isNewSketch;

            var recordUpdate = function () {
                if (recSourceTable == resAddnTable) {
                    rAddnFields.forEach(function (rfield) {
                        if (rfield && (newRecList[rfield.Name] || newRecList[rfield.Name] == 0)) saveData += '^^^' + parcel.Id + '|' + rowuid + '|' + rfield.Id + '||' + newRecList[rfield.Name] + '|edit|' + pRecord.ROWUID + '\n';
                    });
                }
                else if (recSourceTable == comAddnTable) {
                    cAddnFields.forEach(function (cfield) {
                        if (cfield && (newRecList[cfield.Name] || newRecList[cfield.Name] == 0)) saveData += '^^^' + parcel.Id + '|' + rowuid + '|' + cfield.Id + '||' + newRecList[cfield.Name] + '|edit|' + pRecord.ROWUID + '\n';
                    });
                }
                else if (recSourceTable == secAddnTable) {
                    snFields.forEach(function (sfield) {
                        if (sfield && (newRecList[sfield.Name] || newRecList[sfield.Name] == 0)) saveData += '^^^' + parcel.Id + '|' + rowuid + '|' + sfield.Id + '||' + newRecList[sfield.Name] + '|edit|' + pRecord.ROWUID + '\n';
                    });
                }
                insertFunction(newRecordLists);
            }
            if (newSketch) {
                insertNewAuxRecordSketch(parcel.Id, pRecord, recCatId, rowuid, null, function (saveNewRecordData) {
                    if (saveNewRecordData) saveData += saveNewRecordData;
                    recordUpdate();
                });
            }
            else
                recordUpdate();
        }

        insertFunction(newRecordList);
    }

    processor(copySketchData);
}

function T2SketchAfterSave(sketchData, callback) {
    if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }

    let parcel = window.opener ? window.opener.activeParcel : activeParcel,
        getDataField = window.opener ? window.opener.getDataField : getDataField,
        getCategoryFromSourceTable = window.opener ? window.opener.getCategoryFromSourceTable : getCategoryFromSourceTable,
        lookup = window.opener ? window.opener.lookup : lookup;

    let sketch = sketchApp.sketches.filter((s) => { return s.sid == sketchData[0].rowid })[0], saveData = '', newRecordData = '',
        resTable = 'ccv_tAA_VSTerraGon_Res', comTable = 'ccv_tAA_VSTerraGon_Com', miscTable = 'ccv_tAA_Misc', apextable = 'tAA_ApexPolygon', newRecordRowuid = ccTicks(),
        resCat = getCategoryFromSourceTable(resTable), comCat = getCategoryFromSourceTable(comTable), miscCat = getCategoryFromSourceTable(miscTable), apexCat = getCategoryFromSourceTable(apextable),
        resFields = ['PolygonID', 'SketchBaseArea', 'SketchMultiplier', 'SketchTotalArea'], resDataFields = {},
        comFields = ['PolygonID', 'SketchBaseArea', 'SketchMultiplier', 'SketchTotalArea'], comDataFields = {},
        apexFields = ['AreaCode', 'AreaSize', 'Multiplier', 'Description', 'Page', 'Perimeter'], apexDataFields = {}, updatedRecordList = [],
        miscFields = ['PolygonID'], miscDataFields = [], vectorString = '', newRecords = [], isAssociatedField = getDataField('IsAssociated', apextable),
        vsField = getDataField('CC_SKETCH'), associateSource = sketchApp.config.AssociateVector.source, IsDeletedField = getDataField('IsDeleted', apextable);

    _PCIBatchArray = [];

    resFields.forEach((f) => { resDataFields[f] = getDataField(f, resTable); });
    comFields.forEach((f) => { comDataFields[f] = getDataField(f, comTable); });
    miscFields.forEach((f) => { miscDataFields[f] = getDataField(f, miscTable); });
    apexFields.forEach((f) => { apexDataFields[f] = getDataField(f, apextable); });

    function serverUpdate() {
        var isdtr = window.opener ? window.opener.__DTR : (typeof (__DTR) !== 'undefined' ? __DTR : false);
        let isSvApp = window.opener && window.opener.appType == "sv" ? true : (typeof (appType) !== 'undefined' && appType == "sv" ? true : false);
        var _appType = isdtr ? 'DTR' : (isSvApp ? 'SV' : 'QC');

        var saveRequestData = {
            ParcelId: parcel.Id,
            Priority: '',
            AlertMessage: '',
            Data: '',
            recoveryData: '',
            AppraisalType: '',
            activeParcelPhotoEditsdata: '',
            appType: _appType
        };

        if (saveData != '') {
            console.log(saveData);
            saveRequestData.Data = newRecordData + saveData;
            $.ajax({
                url: '/quality/saveparcelchanges.jrq',
                type: "POST",
                dataType: 'json',
                data: saveRequestData,
                success: function (resp) {
                    if (resp.status == "OK") {
                        console.log('sketch successfully saved');
                        if (callback) callback();
                        return;
                    }
                }
            });
        }
        else {
            if (callback) callback();
            return;
        }
    }

    var widthLengthCalculator = function (points) {
        var hLength = 0, RLength = 0, lT = 0, uT = 0, imp_length = 0, imp_width = 0;
        points.length > 1 && points[1].split(/\s/).forEach(function (p, i) {
            if (p.trim() != '') {
                if (p.split("/").length > 1)
                    return;
                else {
                    if ((p.split("L").length > 1 || p.split("R").length > 1) && lT == 0) {
                        hLength = p.split("L").length > 1 ? parseFloat(p.split("L")[1]) : parseFloat(p.split("R")[1]);
                        lT = 1;
                    }
                    else if ((p.split("U").length > 1 || p.split("D").length > 1) && uT == 0) {
                        RLength = p.split("U").length > 1 ? parseFloat(p.split("U")[1]) : parseFloat(p.split("D")[1]);
                        uT = 1;
                    }
                }
            }
        });

        if (hLength > RLength) {
            imp_length = hLength; imp_width = RLength;
        }
        else {
            imp_length = RLength; imp_width = hLength;
        }
        return ({ imp_length: imp_length, imp_width: imp_width });
    }

    var createNewApexRecords = function (nRecords) {
        if (nRecords.length == 0) {
            serverUpdate(); return;
        }

        let nr = nRecords.pop();
        insertNewAuxRecordSketch(parcel.Id, null, nr.Category.Id, nr.Rowuid, null, function (saveNewRecordData) {
            if (saveNewRecordData) newRecordData += saveNewRecordData;
            createNewApexRecords(nRecords);
        });
    }

    var findAssociatedRecord = function (sk) {
        let splitVectorString = sk.vectorString.split('[')[1].split(']')[0], rowuid = -1, apex_id = parseInt(splitVectorString.split(/\,/g)[4]), polyRec = null, polyId = null, srecord = null, type = null;
        if (apex_id < 0) {
            if (splitVectorString.indexOf('{') > -1 && splitVectorString.indexOf('}') > -1)
                rowuid = splitVectorString.indexOf('#') > -1 ? (splitVectorString.split('#')[1].split('$')[0]) : splitVectorString.split('{')[1].split('}')[0];
            else if (sk.tableRowId)
                rowuid = sk.tableRowId.indexOf('#') > -1 ? (sk.tableRowId.split('#')[1].split('$')[0]) : sk.tableRowId.split('{')[1].split('}')[0];
        }

        polyRec = parcel['tAA_ApexPolygon'].filter((x) => {
            return ((x.UniqueID == (apex_id > -1 ? apex_id : -99999)) || (x.ROWUID == (apex_id < 0 && x.CC_RecordStatus == 'I' ? rowuid : -99999))) && x.CC_Deleted != true && x.IsDeleted != '1'
        })[0];
        if (polyRec) {
            polyId = polyRec.CC_RecordStatus != 'I' ? polyRec.PolygonID : polyRec.ROWUID;
            if (polyId) {
                for (i in associateSource) {
                    let src = associateSource[i], tbl = src['SourceTable'], srec = parcel[tbl].filter((rec) => { return (rec.CC_Deleted != true && rec.IsDeleted != '1' && rec.PolygonID == polyId) })[0];
                    if (srec) {
                        srecord = srec; type = src.Type;
                        break;
                    }
                }
            }
        }

        return { PolygonRecord: polyRec, SourceRecord: srecord, Type: type };
    }

    sketchApp.deletedVectors.forEach((v) => {
        let rec = findAssociatedRecord(v);
        if (rec) {
            if (rec.PolygonRecord) {
                saveData += '^^^' + parcel.Id + '|' + rec.PolygonRecord.ROWUID + '|||' + apexCat.SourceTable + '|delete|\n';
                saveData += '^^^' + parcel.Id + '|' + rec.PolygonRecord.ROWUID + '|' + IsDeletedField.Id + '||' + '1' + '|edit|\n';
            }

            if (rec.SourceRecord) {
                let datFields = rec.Type == 'Res' ? resDataFields : (rec.Type == 'Com' ? comDataFields : ((rec.Type == 'Imp' || rec.Type == 'Mrs' || rec.Type == 'Mcm') ? miscDataFields : null));
                for (k in datFields) {
                    let fl = datFields[k]; saveData += '^^^' + parcel.Id + '|' + rec.SourceRecord.ROWUID + '|' + fl.Id + '||' + '' + '|edit|' + rec.SourceRecord.ParentROWUID + '\n';
                }
            }
        }
    });

    sketch.vectors.forEach((vector) => {
        if (vector.isFreeFormLineEntries) {
            vectorString += vector.vectorString + ';';
            return;
        }

        let area = parseFloat(vector.area()), perimeter = vector.perimeter(), code = vector.code, apexLookup = lookup['Apex_Area_Codes'][code],
            associateRowuid = vector.associateRowuid, polyRowuId = null, polyRecord = null, oldSrcRecord = null, oldType = null, len = 0, wid = 0;

        if (vector.vectorString && vector.vectorString != '') {
            let pts = vector.vectorString.split(':')[1].split(' S '), lenWidth = widthLengthCalculator(pts);
            len = Math.round(lenWidth.imp_length); wid = Math.round(lenWidth.imp_width);
        }

        if (vector.newRecord) {
            newRecordRowuid = polyRowuId = newRecordRowuid + 10;
            newRecords.push({ Category: apexCat, Rowuid: newRecordRowuid, Parent: null });
            let apRec = {'AreaCode': code, 'AreaSize': area, 'Multiplier': (apexLookup?.NumericValue ? parseFloat(apexLookup.NumericValue) : 1), 'Description': (apexLookup?.Description ? apexLookup.Description : vector.name), 'Perimeter': perimeter, 'Page': vector.pageNum };
            for (k in apRec) {
                let fl = apexDataFields[k];
                saveData += '^^^' + parcel.Id + '|' + newRecordRowuid + '|' + fl.Id + '||' + apRec[k] + '|edit|\n';
            }
            saveData += '^^^' + parcel.Id + '|' + newRecordRowuid + '|' + isAssociatedField.Id + '||' + (associateRowuid ? '1' : '0') + '|edit|\n';
            
            let tempstr = vector.vectorString.split('[')[1].split(']')[0].split(/\,/);
            tempstr[tempstr.length] = '{$' + apextable + '#' + parseFloat(newRecordRowuid) + '$}';
            vectorString += vector.vectorString.split('[')[0] + '[' + tempstr.toString() + ']' + vector.vectorString.split(']')[1] + ';';
        }
        else {
            let asrec = findAssociatedRecord(vector);
            if (asrec?.PolygonRecord) {
                polyRecord = asrec.PolygonRecord; polyRowuId = asrec.PolygonRecord.ROWUID;
                oldSrcRecord = asrec.SourceRecord; oldType = asrec.Type;
            }
        }

        if (polyRowuId) {
            let tp = associateRowuid?.substring(0, 3), roId = associateRowuid?.substring(3), mul = (apexLookup?.NumericValue ? parseFloat(apexLookup.NumericValue) : 1), desc = (apexLookup?.Description ? apexLookup.Description : vector.name),
                ntbl = tp == 'Res' ? resTable : (tp == 'Com' ? comTable : (tp == 'Imp' ? miscTable : (tp == 'Mrs' ? 'ccv_tAA_VS_Misc_Res' : 'ccv_tAA_VS_Misc_Com'))),
                recs = ntbl ? parcel[ntbl].filter((x) => { return x.ROWUID == roId })[0] : null, apRec = null;

            if (polyRecord?.CC_RecordStatus == 'I') {
                let nassociateRowuid = null;
                if (parseFloat(polyRowuId) < 0) nassociateRowuid = '{$' + apextable + '#' + parseFloat(polyRowuId) + '$}';
                else nassociateRowuid = '{' + polyRowuId + '}';

                let tempstr = vector.vectorString.split('[')[1].split(']')[0].split(/\,/);
                if (tempstr[tempstr.length - 1].indexOf('{') > -1 && tempstr[tempstr.length - 1].indexOf('}') > -1) tempstr.pop();
                tempstr[tempstr.length] = nassociateRowuid;
                vectorString += vector.vectorString.split('[')[0] + '[' + tempstr.toString() + ']' + vector.vectorString.split(']')[1] + ';';
            }
            else if (!vector.newRecord) {
                vectorString += vector.vectorString + ';';
            }

            if (vector.isModified && polyRecord) {
                let apexRec = { 'AreaCode': code, 'AreaSize': area, 'Multiplier': mul, 'Description': desc, 'Perimeter': perimeter, 'Page': vector.pageNum };
                for (k in apexRec) {
                    let fl = apexDataFields[k];
                    saveData += '^^^' + parcel.Id + '|' + polyRecord.ROWUID + '|' + fl.Id + '||' + apexRec[k] + '|edit|\n';
                }
            }

            if (vector.newRecord && associateRowuid && recs) {
                oldType = tp; apRec = { 'PolygonID': polyRowuId, 'SketchBaseArea': area, 'SketchMultiplier': mul, 'SketchTotalArea': Math.round(area * mul) };
            }
            else if (oldSrcRecord && !associateRowuid) {
                let apexRec = { 'PolygonID': '', 'SketchBaseArea': '', 'SketchMultiplier': '', 'SketchTotalArea': '' };

                for (k in apexRec) {
                    let fl = null;
                    if (oldType == 'Res') fl = resDataFields[k];
                    else if (oldType == 'Com') fl = comDataFields[k];
                    else if (oldType == 'Imp' || oldType == 'Mrs' || oldType == 'Mcm') fl = getDataField(k, ntbl);
                    if (fl) saveData += '^^^' + parcel.Id + '|' + oldSrcRecord.ROWUID + '|' + fl.Id + '||' + apexRec[k] + '|edit|' + oldSrcRecord.ParentROWUID + '\n';
                }
                saveData += '^^^' + parcel.Id + '|' + polyRecord.ROWUID + '|' + isAssociatedField.Id + '||' + '0' + '|edit|\n';
            }
            else if (recs && polyRecord && ((polyRecord.CC_RecordStatus == 'I' && polyRecord.ROWUID != recs.PolygonID) || (polyRecord.CC_RecordStatus != 'I' && polyRecord.PolygonID != recs.PolygonID))) {
                apRec = { 'PolygonID': polyRecord.CC_RecordStatus == 'I' ? polyRecord.ROWUID : polyRecord.PolygonID, 'SketchBaseArea': area, 'SketchMultiplier': mul, 'SketchTotalArea': Math.round(area * mul) };
                saveData += '^^^' + parcel.Id + '|' + polyRecord.ROWUID + '|' + isAssociatedField.Id + '||' + '1' + '|edit|\n';
                let modRec = updatedRecordList.filter((x) => { return x.rowuid == oldSrcRecord?.ROWUID && x.type == oldType })[0];
                if (!modRec && oldSrcRecord) {
                    let apexRec = { 'PolygonID': '', 'SketchBaseArea': '', 'SketchMultiplier': '', 'SketchTotalArea': '' };

                    for (k in apexRec) {
                        let fl = null;
                        if (oldType == 'Res') fl = resDataFields[k];
                        else if (oldType == 'Com') fl = comDataFields[k];
                        else if (oldType == 'Imp' || oldType == 'Mrs' || oldType == 'Mcm') fl = getDataField(k, ntbl);
                        if (fl) _PCIBatchArray.push({ parcelId: activeParcel.Id, auxRowId: oldSrcRecord.ROWUID, parentAuxRowId: oldSrcRecord.ParentROWUID, action: 'E', field: fl, oldValue: oldSrcRecord.Original[fl.Name] ? oldSrcRecord.Original[fl.Name] : null, newValue: apexRec[k] });
                    }
                }
                oldType = tp;
                updatedRecordList.push({ rowuid: recs.ROWUID, type: tp });
            }
            else if (recs && polyRecord && ((polyRecord.CC_RecordStatus == 'I' && polyRecord.ROWUID == recs.PolygonID) || (polyRecord.CC_RecordStatus != 'I' && polyRecord.PolygonID == recs.PolygonID)) && vector.isModified) {
                oldType = tp; apRec = { 'PolygonID': polyRecord.CC_RecordStatus == 'I' ? polyRecord.ROWUID : polyRecord.PolygonID, 'SketchBaseArea': area, 'SketchMultiplier': mul, 'SketchTotalArea': Math.round(area * mul) };
            }

            if (apRec) {
                apRec['V04'] = len, apRec['V05'] = wid; apRec['V01'] = area;
                for (k in apRec) {
                    let fl = null;
                    if (oldType == 'Res') fl = resDataFields[k];
                    else if (oldType == 'Com') fl = comDataFields[k];
                    else if (oldType == 'Imp' || oldType == 'Mrs' || oldType == 'Mcm') fl = getDataField(k, ntbl);
                    if (fl) saveData += '^^^' + parcel.Id + '|' + recs.ROWUID + '|' + fl.Id + '||' + apRec[k] + '|edit|' + recs.ParentROWUID + '\n';
                }
            }
        }
        else if (!vector.newRecord) { vectorString += vector.vectorString + ';'; }
    });

    let jsonString = sketch.jsonString;
    if (jsonString) { jsonString.DCS = vectorString; vectorString = JSON.stringify(jsonString); }
    if (vsField) saveData += '^^^' + parcel.Id + '|' + 0 + '|' + vsField.Id + '||' + vectorString + '|edit|\n';
    createNewApexRecords(newRecords);
}


function LucasDTRAfterSave(sketchData, callback) {
    if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }

    var parcel = window.opener ? window.opener.activeParcel : activeParcel;
    var getDataField = window.opener ? window.opener.getDataField : getDataField;
    var storyField = getDataField('STORIES', 'DWELDAT'), cduField = getDataField('CDU', 'DWELDAT'),
        yearField = getDataField('YRBLT', 'DWELDAT'), gradeField = getDataField('GRADE', 'DWELDAT'), saveData = "";

    sketchApp.sketches.forEach(function (skt) {
        if (skt.config.DoNotAddOrEditFirstRecordlabel) {
            var sktRecord = skt.parentRow;
            var firstRecordVector = skt.vectors.filter(function (vect) { return vect.firstLabelValueModified })[0];
            if (firstRecordVector && sktRecord && firstRecordVector.label) {
                var valMod = firstRecordVector.firstLabelValueModified;
                var spLabel = firstRecordVector.label.split(']')[1];
                if (spLabel) {
                    spLabel = spLabel.trim();
                    var stories = (spLabel.split('/')[0] && spLabel.split('/')[0] != '---' && spLabel.split('/')[0] != '???') ? spLabel.split('/')[0] : '';
                    if (storyField && stories)
                        saveData += '^^^' + parcel.Id + '|' + sktRecord.ROWUID + '|' + storyField.Id + '||' + stories + '|edit|\n';
                }

                if (cduField && valMod.cdu) saveData += '^^^' + parcel.Id + '|' + sktRecord.ROWUID + '|' + cduField.Id + '||' + valMod.cdu + '|edit|\n';
                if (yearField && valMod.ybuilt) saveData += '^^^' + parcel.Id + '|' + sktRecord.ROWUID + '|' + yearField.Id + '||' + valMod.ybuilt + '|edit|\n';
                if (gradeField && valMod.grade) saveData += '^^^' + parcel.Id + '|' + sktRecord.ROWUID + '|' + gradeField.Id + '||' + valMod.grade + '|edit|\n';
            }
        }
    });

    var isdtr = window.opener ? window.opener.__DTR : (typeof (__DTR) !== 'undefined' ? __DTR : false);
    let isSvApp = window.opener && window.opener.appType == "sv" ? true : (typeof (appType) !== 'undefined' && appType == "sv" ? true : false);
    var _appType = isdtr ? 'DTR' : (isSvApp ? 'SV' : 'QC');

    var saveRequestData = {
        ParcelId: parcel.Id,
        Priority: '',
        AlertMessage: '',
        Data: '',
        recoveryData: '',
        AppraisalType: '',
        activeParcelPhotoEditsdata: '',
        appType: _appType
    };

    if (saveData != '') {
        console.log(saveData);
        saveRequestData.Data = saveData;
        $.ajax({
            url: '/quality/saveparcelchanges.jrq',
            type: "POST",
            dataType: 'json',
            data: saveRequestData,
            success: function (resp) {
                if (resp.status == "OK") {
                    console.log('sketch successfully saved');
                    if (callback) callback();
                    return;
                }
            }
        });
    }
    else {
        if (callback) callback();
        return;
    }
}

function AACAfterSave(sketchData, callback) {
    if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }

    let parcel = window.opener ? window.opener.activeParcel : activeParcel,
        getDataField = window.opener ? window.opener.getDataField : getDataField,
        getCategoryFromSourceTable = window.opener ? window.opener.getCategoryFromSourceTable : getCategoryFromSourceTable,
        lookupdata = window.opener ? window.opener.lookup : lookup;

    let res_tble = 'res_building_tb', res_att_tble = 'res_attachments_sup', saveData = '',
        copySketchData = [], res_lookup = lookupdata.res_sketch_lookup, res_building_fields = {}, deletedRecords = [],
        res_att_catId = getCategoryFromSourceTable(res_att_tble).Id, newRowuid = null, newRecords = [],
        areaField = getDataField('area', res_att_tble), typeField = getDataField('attachment_type_cd', res_att_tble);

    for (l in res_lookup) {
        let lk = res_lookup[l];
        if (!res_building_fields[lk.Object.column_name])
            res_building_fields[lk.Object.column_name] = { value: 0, field: getDataField(lk.Object.column_name, res_tble) };
    }
    res_building_fields['sfla'] = { value: 0, field: getDataField('sfla', res_tble) };

    var updateRecordPCI = function () {
        var isdtr = window.opener ? window.opener.__DTR : (typeof (__DTR) !== 'undefined' ? __DTR : false);
        let isSvApp = window.opener && window.opener.appType == "sv" ? true : (typeof (appType) !== 'undefined' && appType == "sv" ? true : false);
        var _appType = isdtr ? 'DTR' : (isSvApp ? 'SV' : 'QC');

        var saveRequestData = {
            ParcelId: parcel.Id,
            Priority: '',
            AlertMessage: '',
            Data: '',
            recoveryData: '',
            AppraisalType: '',
            activeParcelPhotoEditsdata: '',
            appType: _appType
        };

        if (saveData != '') {
            console.log(saveData);
            saveRequestData.Data = saveData;
            $.ajax({
                url: '/quality/saveparcelchanges.jrq',
                type: "POST",
                dataType: 'json',
                data: saveRequestData,
                success: function (resp) {
                    if (resp.status == "OK") {
                        console.log('sketch successfully saved');
                        if (callback) callback();
                        return;
                    }
                }
            });
        }
        else {
            if (callback) callback();
            return;
        }
    }

    var createNewRecord = function (newRecds) {
        if (newRecds.length == 0) {
            updateRecordPCI();
            return;
        }

        let newRec = newRecds.shift();
        newRowuid += 12;
        insertNewAuxRecordSketch(parcel.Id, newRec.parentRecord, newRec.catId, newRowuid, { area: newRec.area, attachment_type_cd: newRec.attachment_type_cd }, (saveNewRecordData) => {
            if (saveNewRecordData) saveData += saveNewRecordData;
            createNewRecord(newRecds);
        });
    }

    var deleteRecord = function (delRecords, delCallback) {
        delRecords.forEach((delRec) => {
            saveData += '^^^' + parcel.Id + '|' + delRec.rowuid + '|||' + delRec.sourceTable + '|delete|\n';
        });

        deletedRecords = [];
        if (delCallback) delCallback();
        else {
            newRowuid = ccTicks();
            createNewRecord(newRecords);
        }
        return;
    }

    var processor = function (sketchRecords) {
        if (sketchRecords.length == 0) {
            deleteRecord(deletedRecords);
            return;
        }

        let sketchRecord = sketchRecords.shift(),
            sketch = sketchApp.sketches.filter(function (s) { return s.sid == sketchRecord.sid })[0],
            vectors = sketch.vectors,
            sketchTable = sketchRecord.sketchTable,
            parentRecord = parcel[sketchTable].filter(function (s) { return s.ROWUID == sketchRecord.sid && s.CC_Deleted != true })[0],
            res_building_fields_clone = _.clone(res_building_fields);

        vectors.forEach((v) => {
            let vname = v.name, area = Math.round(v.area()), currentVectorArea = parseFloat(v.currentVectorArea);
            vname.split('/').forEach((vn) => {
                let res_lk = res_lookup[vn];
                if (res_lk && res_lk['Object']) {
                    let res_lk_ob = res_lk['Object'];
                    if (res_lk_ob['table_name'] == 'res_building_tb') {
                        let ml = parseFloat(res_lk_ob['multiplier']), is_sfla = parseFloat(res_lk_ob['is_sfla']);
                        if (res_lk_ob['column_name'] && ml > 0) {
                            res_building_fields_clone[res_lk_ob['column_name']].value = (res_building_fields_clone[res_lk_ob['column_name']].value + (area * ml));
                            res_building_fields_clone['sfla'].value = (res_building_fields_clone['sfla'].value + (area * ml * is_sfla));
                        }
                    }
                    else if (res_lk_ob['table_name'] == 'res_attachments_sup') {
                        if (v.newRecord) newRecords.push({ catId: res_att_catId, parentRecord: parentRecord, area: area, attachment_type_cd: vn });
                        else {
                            if (v.isModified) {
                                let exeRec = parcel[res_att_tble].filter((r) => { return (!r.CC_Deleted && (r.ParentROWUID == parentRecord.ROWUID) && (r.attachment_type_cd == vn) && ((currentVectorArea - 1) <= (Math.round(r.area)) && (Math.round(r.area)) <= (currentVectorArea + 1))) })[0];
                                if (exeRec) {
                                    if (areaField) saveData += '^^^' + parcel.Id + '|' + exeRec.ROWUID + '|' + areaField.Id + '||' + area + '|edit|' + exeRec.ParentROWUID + '\n';
                                    if (typeField) saveData += '^^^' + parcel.Id + '|' + exeRec.ROWUID + '|' + typeField.Id + '||' + vn + '|edit|' + exeRec.ParentROWUID + '\n';
                                }
                                else newRecords.push({ catId: res_att_catId, parentRecord: parentRecord, area: area, attachment_type_cd: vn });
                            }
                        }
                    }
                }
            });
        });

        for (x in res_building_fields_clone) {
            let res_field = res_building_fields_clone[x];
            if (res_field.field) saveData += '^^^' + parcel.Id + '|' + parentRecord.ROWUID + '|' + res_field.field.Id + '||' + Math.round(res_field.value) + '|edit|\n';
        }

        processor(sketchRecords);
    }

    sketchApp.deletedVectors.filter((d) => { return !d.newRecord }).forEach((del) => {
        let vname = del.name, area = Math.round(del.area()), skt = del.sketch,
            type = skt && skt.config ? skt.config.SketchLabelPrefix : null,
            prec = parcel[res_tble].filter(function (s) { return s.ROWUID == skt.sid && s.CC_Deleted != true })[0],
            currentVectorArea = parseFloat(del.currentVectorArea);

        if (type && type.indexOf('RES -') > -1) {
            vname.split('/').forEach((vn) => {
                let delRec = parcel[res_att_tble].filter((r) => { return (!r.CC_Deleted && (r.ParentROWUID == prec.ROWUID) && (r.attachment_type_cd == vn) && ((currentVectorArea - 1) <= (Math.round(r.area)) && (Math.round(r.area)) <= (currentVectorArea + 1))) })[0];
                if (delRec) deletedRecords.push({ rowuid: delRec.ROWUID, categoryId: res_att_catId, sourceTable: res_att_tble });
            });
        }
    });

    deleteRecord(deletedRecords, () => {
        sketchData.forEach((sdata) => {
            let skt = sketchApp.sketches.filter(function (s) { return s.sid == sdata.rowid })[0];
            let sktTable = skt.config.SketchSource.Table;
            let sktRecord = parcel[sktTable].filter(function (s) { return s.ROWUID == sdata.rowid && s.CC_Deleted != true })[0];

            if (sktTable == res_tble)
                copySketchData.push({ sid: sdata.rowid, sketchTable: sktTable, parentRecord: sktRecord });
        });

        processor(copySketchData);
    });
}

function SigmaCuyahogaAfterSave(sketchData, callback) {
    if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }

    let parcel = window.opener ? window.opener.activeParcel : activeParcel,
        getDataField = window.opener ? window.opener.getDataField : getDataField,
        getCategoryFromSourceTable = window.opener ? window.opener.getCategoryFromSourceTable : getCategoryFromSourceTable,
        SigmaCuyahoga_Lookup = window.opener ? window.opener.SigmaCuyahoga_Lookup : SigmaCuyahoga_Lookup,
        lookupdata = window.opener ? window.opener.lookup : lookup;

    let amenityTbl = 'RES_AMENITY', measField = getDataField('AMENITY_MEAS', amenityTbl), typeField = getDataField('AMENITY_TYPE', amenityTbl),
        newRecords = [], amenityCat = getCategoryFromSourceTable(amenityTbl), saveData = '', newData = '';

    function serverUpdate() {
        var isdtr = window.opener ? window.opener.__DTR : (typeof (__DTR) !== 'undefined' ? __DTR : false);
        let isSvApp = window.opener && window.opener.appType == "sv" ? true : (typeof (appType) !== 'undefined' && appType == "sv" ? true : false);
        var _appType = isdtr ? 'DTR' : (isSvApp ? 'SV' : 'QC');

        var saveRequestData = {
            ParcelId: parcel.Id,
            Priority: '',
            AlertMessage: '',
            Data: '',
            recoveryData: '',
            AppraisalType: '',
            activeParcelPhotoEditsdata: '',
            appType: _appType
        };

        if (saveData != '') {
            saveRequestData.Data = newData + saveData;
            console.log(saveRequestData.Data);
            $.ajax({
                url: '/quality/saveparcelchanges.jrq',
                type: "POST",
                dataType: 'json',
                data: saveRequestData,
                success: function (resp) {
                    if (resp.status == "OK") {
                        console.log('sketch successfully saved');
                        if (callback) callback();
                        return;
                    }
                }
            });
        }
        else {
            if (callback) callback();
            return;
        }
    }

    var createNewRecord = function (newRecds) {
        if (newRecds.length == 0) {
            serverUpdate();
            return;
        }

        let newRec = newRecds.shift();
        insertNewAuxRecordSketch(parcel.Id, newRec.parentRecord, newRec.catId, newRec.rowuid, {}, (saveNewRecordData) => {
            if (saveNewRecordData) newData += saveNewRecordData;
            createNewRecord(newRecds);
        });
    }

    sketchData.forEach((sketchRecord) => {
        let sketch = sketchApp.sketches.filter(function (s) { return s.sid == sketchRecord.rowid })[0], vectors = sketch?.vectors ? sketch.vectors : [],
            parentRecord = parcel['RESIDENTIAL_BUILDING'].filter(function (s) { return s.ROWUID == sketchRecord.rowid && s.CC_Deleted != true })[0],
            amenityRecords = parentRecord[amenityTbl].filter((x) => { return x.CC_Deleted != true }), amenityProcessingRecords = [], newRowuid = ccTicks();

        amenityRecords.forEach((r) => { amenityProcessingRecords.push({ Rowuid: r.ROWUID, Type: r.AMENITY_TYPE, Area: 0, isProcessed: false, record: r }); });
        sketch.MSKetchArray.forEach((x) => {
            vectors.push({ name: x.msketchLabel, area: x.mSketchArea, marea: true });
        });
        let delVect = sketchApp.deletedVectors.filter((x) => { return x.sketch?.sid == sketchRecord.rowid });
        delVect.forEach((x) => { x.IsSkDeleted = true; vectors.push(x); });

        vectors.forEach((vect) => {
            let vname = vect.name, area = vect.marea ? Math.round(vect.area) : Math.round(vect.area());
            if (vect.IsSkDeleted) area = 0;
            vname.split('/').forEach((vn) => {
                let slk = _.filter(lookupdata["sketch_label_lookup"], (obj) => { return obj.Id == vn })[0],
                    lk = SigmaCuyahoga_Lookup.filter((x) => { return slk ? caseInsensitiveCompare(x.Desc, slk.Name) : x.CODE == vn })[0];

                if (lk) {
                    let rec = amenityProcessingRecords.filter((x) => { return x.Type == lk.CODE })[0];
                    if (rec) {
                        rec.Area = rec.Area + area; rec.isProcessed = true;
                    }
                   /* else {
                        newRowuid = newRowuid + 10;
                        newRecords.push({ rowuid: newRowuid, catId: amenityCat.Id, parentRecord: parentRecord });
                        amenityProcessingRecords.push({ Rowuid: newRowuid, Type: lk.CODE, Area: area, isProcessed: true });
                    }*/
                }
            });
        });

        amenityProcessingRecords.forEach((r) => {
            if (r.isProcessed) {
                if (measField) saveData += '^^^' + parcel.Id + '|' + r.Rowuid + '|' + measField.Id + '||' + r.Area + '|edit|' + parentRecord.ROWUID + '\n';
                if (typeField) saveData += '^^^' + parcel.Id + '|' + r.Rowuid + '|' + typeField.Id + '||' + r.Type + '|edit|' + parentRecord.ROWUID + '\n';
            }
            /*else {
                saveData += '^^^' + parcel.Id + '|' + r.Rowuid + '|||' + amenityCat.SourceTable + '|delete|\n';
            }*/
        });

        sketch.vectors = sketch.vectors.filter((x) => { return !x.marea });
    });

    createNewRecord(newRecords);
}

function LucasDTRSketchBeforeSave(sketches, callback) {
    let msg = "", parcel = window.opener ? window.opener.activeParcel : activeParcel;
    if (sketches === undefined || sketches == null) {
        if (callback) callback(true);
        return;
    }

    sketches.filter((x) => { return x.label.contains("COM") }).forEach((sk) => {
        sk.vectors.filter((m) => { return m.isModified }).forEach((v) => {
            if (v.vectorConfig?.Table == "COMINTEXT" && !v.newRecord) {
                let uId = v.uid, rec = parcel['COMINTEXT'].filter((r) => { return r.ROWUID == uId })[0];
                if (rec) {
                    let area = v.fixedArea ? parseFloat(v.fixedArea) : parseFloat(v.area()), peri = v.perimeter(), rarea = rec.AREA, rperi = rec.PERIM, areac = Math.ceil(area), areaf = Math.floor(area);
                    if (!(area == 0 && peri == 0)) {
                        if (!(rarea >= areaf && rarea <= areac) && peri != rperi) {
                            msg += "The " + v.label + " sketch Area/SQFT has changed from " + rarea + " to " + area + ", and the Perimeter has changed from " + rperi + " to " + peri + ". <br>";
                        }
                        else if (!(rarea >= areaf && rarea <= areac)) {
                            msg += "The " + v.label + " sketch Area/SQFT has changed from " + rarea + " to " + area + ". <br>";
                        }
                        else if (peri != rperi) {
                            msg += "The " + v.label + " sketch Perimeter has changed from " + rperi + " to " + peri + ". <br>";
                        }
                    }
                }
            }
        });
    });

    if (msg != "") {
        msg += 'Pressing "Okay" will proceed with the sketch Save.'
        messageBox(msg, ['OK', 'Cancel'], () => {
            if (callback) callback(true, true);
        }, () => {
            if (callback) callback(false, false);
        });
    }
    else if (callback) callback(true, true);
}


function HelionRSBeforeSave(sketches, callback) {
    let parcel = window.opener ? window.opener.activeParcel : activeParcel,
        getDataField = window.opener ? window.opener.getDataField : getDataField,
        getCategoryFromSourceTable = window.opener ? window.opener.getCategoryFromSourceTable : getCategoryFromSourceTable;

    let newSketches = sketchApp.sketches.filter((x) => { return x.rsNewPage }), delSketches = sketchApp.rsDeletedPages ? sketchApp.rsDeletedPages : [],
        catId = getCategoryFromSourceTable('CC_APPRAISAL_DRAWING').Id, cField = getDataField('CHANGE_TYPE', 'CC_APPRAISAL_DRAWING'), onlyDelete = true,
        insertData = { YEAR: window.opener.ccma.CurrentYear, SOURCE: 'Account', ACCOUNT_ID: parcel.ACCOUNT_ID, TRANSACTION_ID: parcel.TRANSACTION_ID, CHANGE_TYPE: 'Insert' }, saveData = '';
 
    function serverUpdate() {
        var isdtr = window.opener ? window.opener.__DTR : (typeof (__DTR) !== 'undefined' ? __DTR : false);
        let isSvApp = window.opener && window.opener.appType == "sv" ? true : (typeof (appType) !== 'undefined' && appType == "sv" ? true : false);
        var _appType = isdtr ? 'DTR' : (isSvApp ? 'SV' : 'QC');

        var saveRequestData = {
            ParcelId: parcel.Id,
            Priority: '',
            AlertMessage: '',
            Data: '',
            recoveryData: '',
            AppraisalType: '',
            activeParcelPhotoEditsdata: '',
            appType: _appType
        };

        if (saveData != '') {
            saveRequestData.Data = saveData;
            console.log(saveRequestData.Data);
            $.ajax({
                url: '/quality/saveparcelchanges.jrq',
                type: "POST",
                dataType: 'json',
                data: saveRequestData,
                success: function (resp) {
                    if (resp.status == "OK") {
                        console.log('sketch successfully saved');
                        if (onlyDelete && window.opener && window.opener.getParcel) {
                            $('.ccse-canvas').addClass('dimmer');
                            $('.dimmer').show();
                            window.opener.getParcel(window.opener.activeParcel.Id, function () {
                                window.onbeforeunload = null;
                                $('.ccse-canvas').removeClass('dimmer');
                                $('.dimmer').hide();
                                if (callback) callback(true, true);
                                return;
                            }, true);
                        }
                        else {
                            if (callback) callback(true, true);
                            return;
                        }
                    }
                }
            });
        }
        else {
            if (callback) callback(true, true);
            return;
        }
    }

    sketchApp.sketches.filter((x) => { return x.isModified && !x.rsNewPage }).forEach((sk) => {
        onlyDelete = false;
        let rec = parcel['CC_APPRAISAL_DRAWING'].filter((r) => { return r.ROWUID == sk.uid })[0];
        if (rec && rec.CC_RecordStatus != 'I' && cField) saveData += '^^^' + parcel.Id + '|' + rec.ROWUID + '|' + cField.Id + '||' + "Update" + '|edit|\n';
    });

    if (newSketches.length == 0 && delSketches.length == 0) {
        serverUpdate();
        return;
    }

    let deletePages = function (dsketches) {
        if (dsketches.length == 0) {
            serverUpdate();
            return;
        }

        let dsketch = dsketches.shift();
        saveData += '^^^' + parcel.Id + '|' + dsketch.uid + '|||' + "CC_APPRAISAL_DRAWING" + '|delete|\n';
        deletePages(dsketches);
    }

    let createNewPage = function (nsketches) {
        if (nsketches.length == 0) {
            deletePages(delSketches);
            return;
        }
        
        let nsketch = nsketches.shift(); onlyDelete = false;
        insertNewAuxRecordSketch(parcel.Id, null, catId, nsketch.uid, insertData, (saveNewRecordData) => {
            if (saveNewRecordData) saveData += saveNewRecordData;
            createNewPage(nsketches);
        });
    }

    createNewPage(newSketches);
}

function IASUSSketchBeforeSave(sketches, callback) {
    let html = "";
    if (sketches === undefined || sketches == null) {
        if (callback) callback(true);
        return;
    }

    let parcel = window.opener ? window.opener.activeParcel : activeParcel;

    sketches.filter((x) => { return x.label.contains("COM") }).forEach((sk) => {
        sk.vectors.filter((m) => { return m.isModified }).forEach((v) => {
            if (v.vectorConfig?.Table == "COMINTEXT" && !v.newRecord) {
                let uId = v.uid, rec = parcel['COMINTEXT'].filter((r) => { return r.ROWUID == uId })[0];
                if (rec) {
                    let area = v.fixedArea ? parseFloat(v.fixedArea) : parseFloat(v.area()), peri = v.perimeter(), rarea = rec.AREA, rperi = rec.PERIM, areac = Math.ceil(area), areaf = Math.floor(area);
                    if (!(area == 0 && peri == 0)) {
                        if (!(rarea >= areaf && rarea <= areac) && peri != rperi) {
                            html += "<span style='display: block; width: 90% !important; font-size: 14px !important; margin-bottom: 10px;'><input type='checkbox' class='com-diff-area' suid='" + sk.uid + "' uid='" + v.uid + "' rarea='" + rarea + "' rperi='" + rperi + "' style='width: 22px; margin-right: 20px;'/> <label>The " + v.label + " sketch Area/SQFT has changed from " + rarea + " to " + area + ",<br> and the Perimeter has changed from " + rperi + " to " + peri + ". </label></span><br>";
                        }
                        else if (!(rarea >= areaf && rarea <= areac)) {
                            html += "<span style='display: block; width: 90% !important; font-size: 14px !important; margin-bottom: 10px;'><input type='checkbox' class='com-diff-area' suid='" + sk.uid + "' uid='" + v.uid + "' rarea='" + rarea + "' style='width: 22px; margin-right: 20px;'/> <label>The " + v.label + " sketch Area/SQFT has changed from " + rarea + " to " + area + ". </label></span><br>";
                        }
                        else if (peri != rperi) {
                            html += "<span style='display: block; width: 90% !important; font-size: 14px !important; margin-bottom: 10px;'><input type='checkbox' class='com-diff-area' suid='" + sk.uid + "' uid='" + v.uid + "' rperi='" + rperi + "' style='width: 22px; margin-right: 20px;'/><label>The " + v.label + " sketch Perimeter has changed from " + rperi + " to " + peri + ". </label></span><br>";
                        }
                    }
                }
            }
        });
    });

    if (html != "") {
        $('.control_div div, .UnSketched').remove();
        $('.Current_vector_details .head').html("Sketch Area Updation");
        $('.mask').show(); $('.dynamic_prop').html('');
        $('.dynamic_prop').append('<div class="divvectors"></div>');
        $('.divvectors').append(html);
        $('.dynamic_prop').append("<span class ='skNote' style='color: #fb2300;'><b>Note:</b> Enable the checkbox to keep the COMINTEXT area different than the sketch's area. </br> (Sketch area will not update in COMINTEXT)</span>");
        $('.skNote').css('color', '#fb2300')
        $('.Current_vector_details').show();
        $('.divvectors').css('width', 710);
        $('.skNote').show();
        $('.divvectors').css('max-height', 400);
        $('.Current_vector_details').css('width', 750);

        $('.Current_vector_details #Btn_cancel_vector_properties').unbind(touchClickEvent)
        $('.Current_vector_details #Btn_cancel_vector_properties').bind(touchClickEvent, function () {
            $('.skNote').html('');
            $('.skNote').remove();
            $('.Current_vector_details').hide();
            if (!sketchApp.isScreenLocked) $('.mask').hide();
            return false;
        });

        $('.Current_vector_details #Btn_Save_vector_properties').unbind(touchClickEvent)
        $('.Current_vector_details #Btn_Save_vector_properties').bind(touchClickEvent, function () {
            sketchApp.EAConfigComTextKeepAreaVects = [];
            $('.com-diff-area').each(function () {
                let sk = sketchApp.sketches.filter((x) => { return x.uid == $(this).attr('suid') })[0];
                if (sk) {
                    let vk = sk.vectors.filter((x) => { return x.uid == $(this).attr('uid') })[0];
                    if (vk) {
                        let ra = $(this).attr('rarea'), pe = $(this).attr('rperi');
                        if ($(this)[0].checked) {
                            if (ra) vk.comTextKeepArea = parseFloat(ra);
                            if (pe) vk.comTextKeepPeri = parseFloat(pe);
                            sketchApp.EAConfigComTextKeepAreaVects.push({ uid: $(this).attr('uid'), keepArea: true });
                        }
                        else {
                            sketchApp.EAConfigComTextKeepAreaVects.push({ uid: $(this).attr('uid'), keepArea: false });
                        }
                    }
                }
            });

            $('.skNote').html('');
            $('.skNote').remove();
            $('.Current_vector_details').hide();
            if (!sketchApp.isScreenLocked) $('.mask').hide();
            if (callback) callback(true, true);
            return false;
        });
    }
    else if (callback) callback(true, true);
}

function IASUSSketchAfterSave(sketchData, callback) {
    if (!(sketchApp?.EAConfigComTextKeepAreaVects)) sketchApp.EAConfigComTextKeepAreaVects = [];
    if (sketchData == null || sketchData == undefined || sketchData.length == 0 || sketchApp.EAConfigComTextKeepAreaVects.length == 0) {
        if (callback) callback();
        return;
    }

    var parcel = window.opener ? window.opener.activeParcel : activeParcel, saveData = '';

    function serverUpdate() {
        var isdtr = window.opener ? window.opener.__DTR : (typeof (__DTR) !== 'undefined' ? __DTR : false);
        let isSvApp = window.opener && window.opener.appType == "sv" ? true : (typeof (appType) !== 'undefined' && appType == "sv" ? true : false);
        var _appType = isdtr ? 'DTR' : (isSvApp ? 'SV' : 'QC');

        var saveRequestData = {
            ParcelId: parcel.Id,
            Priority: '',
            AlertMessage: '',
            Data: '',
            recoveryData: '',
            AppraisalType: '',
            activeParcelPhotoEditsdata: '',
            appType: _appType
        };

        if (saveData != '') {
            saveRequestData.Data = saveData;
            console.log(saveRequestData.Data);
            $.ajax({
                url: '/quality/saveparcelchanges.jrq',
                type: "POST",
                dataType: 'json',
                data: saveRequestData,
                success: function (resp) {
                    if (resp.status == "OK") {
                        console.log('sketch successfully saved');
                        if (callback) callback();
                        return;
                    }
                }
            });
        }
        else {
            if (callback) callback(true, true);
            return;
        }
    }

    var getDataField = window.opener ? window.opener.getDataField : getDataField;
    var storyField = getDataField('STORIES', 'DWELDAT'), cduField = getDataField('CDU', 'DWELDAT'),
        yearField = getDataField('YRBLT', 'DWELDAT'), gradeField = getDataField('GRADE', 'DWELDAT'), saveData = "";

    sketchApp.sketches.forEach(function (skt) {
        if (skt.config.DoNotAddOrEditFirstRecordlabel) {
            var sktRecord = skt.parentRow;
            var firstRecordVector = skt.vectors.filter(function (vect) { return vect.firstLabelValueModified })[0];
            if (firstRecordVector && sktRecord && firstRecordVector.label) {
                var valMod = firstRecordVector.firstLabelValueModified;
                var spLabel = firstRecordVector.label.split(']')[1];
                if (spLabel) {
                    spLabel = spLabel.trim();
                    var stories = (spLabel.split('/')[0] && spLabel.split('/')[0] != '---' && spLabel.split('/')[0] != '???') ? spLabel.split('/')[0] : '';
                    if (storyField && stories)
                        saveData += '^^^' + parcel.Id + '|' + sktRecord.ROWUID + '|' + storyField.Id + '||' + stories + '|edit|\n';
                }

                if (cduField && valMod.cdu) saveData += '^^^' + parcel.Id + '|' + sktRecord.ROWUID + '|' + cduField.Id + '||' + valMod.cdu + '|edit|\n';
                if (yearField && valMod.ybuilt) saveData += '^^^' + parcel.Id + '|' + sktRecord.ROWUID + '|' + yearField.Id + '||' + valMod.ybuilt + '|edit|\n';
                if (gradeField && valMod.grade) saveData += '^^^' + parcel.Id + '|' + sktRecord.ROWUID + '|' + gradeField.Id + '||' + valMod.grade + '|edit|\n';
            }
        }
    });

    sketchApp.EAConfigComTextKeepAreaVects.forEach((vc) => {
        let rec = parcel['COMINTEXT'].filter((r) => { return r.ROWUID == vc.uid })[0];
        if (rec) {
            let msg = vc.keepArea ? "Keep the COMINTEXT record #" + vc.uid + " area different than the sketch's area" : "Update the COMINTEXT record #" + vc.uid + " to match the sketch's area";
            saveData += '^^^' + parcel.Id + '||||' + msg + '|audit|\n';
        }
    });
    sketchApp.EAConfigComTextKeepAreaVects = [];
    serverUpdate();
}

function MSGovernAfterSave(sketchData, callback) {
    if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }

    let parcel = window.opener ? window.opener.activeParcel : activeParcel,
        getDataField = window.opener ? window.opener.getDataField : getDataField,
        getCategoryFromSourceTable = window.opener ? window.opener.getCategoryFromSourceTable : getCategoryFromSourceTable;

    let bldgTbl = 'CCV_MA_Buildings_2_3_BLDG_INFO', areaTbl = 'MA_BLDG_AREA', deleteRecords = [], areaCat = getCategoryFromSourceTable(areaTbl), newRecords = [],
        bldgFields = ['Total_Base', 'Total_Actual', 'Total_Effective', 'Total_Heated'], bldgDataFields = {}, pciChanges = [],
        areaFields = ['AREA_CODE', 'BASE_AREA', 'ACTUAL_AREA', 'EFFECTIVE_AREA', 'LIVING_AREA', 'HEATED_AREA', 'PERCENT_HEATED', 'PERIMETER'];


    function serverUpdate() {
        var isdtr = window.opener ? window.opener.__DTR : (typeof (__DTR) !== 'undefined' ? __DTR : false);
        let isSvApp = window.opener && window.opener.appType == "sv" ? true : (typeof (appType) !== 'undefined' && appType == "sv" ? true : false);
        var _appType = isdtr ? 'DTR' : (isSvApp ? 'SV' : 'QC');

        var saveRequestData = {
            ParcelId: parcel.Id,
            Priority: '',
            AlertMessage: '',
            Data: '',
            recoveryData: '',
            AppraisalType: '',
            activeParcelPhotoEditsdata: '',
            appType: _appType
        };

        if (saveData != '') {
            saveRequestData.Data = saveData;
            console.log(saveRequestData.Data);
            $.ajax({
                url: '/quality/saveparcelchanges.jrq',
                type: "POST",
                dataType: 'json',
                data: saveRequestData,
                success: function (resp) {
                    if (resp.status == "OK") {
                        console.log('sketch successfully saved');
                        if (callback) callback();
                        return;
                    }
                }
            });
        }
        else {
            if (callback) callback(true, true);
            return;
        }
    }

    bldgFields.forEach((f) => { bldgDataFields[f] = getDataField(f, bldgTbl); });

    var updateRecordPCI = function () {
        pciChanges.forEach((x) => {
            saveData += '^^^' + parcel.Id + '|' + x.auxRowId + '|' + x.fieldId + '||' + x.newValue + '|edit|' + x.parentAuxRowId +'\n';
        });
        serverUpdate();
    }

    var createNewRecord = function (newRecds) {
        if (newRecds.length == 0) {
            updateRecordPCI();
            return;
        }

        let newRec = newRecds.shift();
        insertNewAuxRecordSketch(parcel.Id, newRec.parentRecord, newRec.catId, null, newRec.arObj, (saveNewRecordData) => {
            if (saveNewRecordData) saveData += saveNewRecordData;
            createNewRecord(newRecds);
        });
    }

    sketchData.forEach((sketchRecord) => {
        let sketch = sketchApp.sketches.filter((s) => { return s.sid == sketchRecord.rowid })[0],
            parentRecord = parcel[bldgTbl].filter((s) => { return s.ROWUID == sketchRecord.rowid && s.CC_Deleted != true && s.OVERRIDE_AREA != '1' })[0];
        if (parentRecord?.MA_BLDG_AREA) {
            parentRecord.MA_BLDG_AREA.filter((x) => { return x.CC_Deleted != true }).forEach((ba) => { saveData += '^^^' + parcel.Id + '|' + ba.ROWUID + '|||' + areaTbl + '|delete|\n'; });
            let totalArea = 0;

            sketch.vectors.forEach((vect) => {
                let ar = Math.round(vect.area()), peri = Math.round(vect.perimeter()), arObj = {};
                totalArea = totalArea + vect.area();
                areaFields.forEach((f) => {
                    if (f == 'AREA_CODE') { arObj[f] = vect.code ? vect.code : vect.name; }
                    else if (f == 'PERIMETER') { arObj[f] = peri; }
                    else { arObj[f] = ar; }
                });

                newRecords.push({ arObj: arObj, parentRecord: parentRecord, catId: areaCat.Id });
            });

            totalArea = Math.round(totalArea);
            for (f in bldgDataFields) {
                if (bldgDataFields[f]) pciChanges.push({ parcelId: parcel.Id, auxRowId: parentRecord.ROWUID, parentAuxRowId: parentRecord.ParentROWUID, action: 'E', fieldId: bldgDataFields[f].Id, oldValue: null, newValue: totalArea });
            }
        }
    });

    createNewRecord(newRecords);
}