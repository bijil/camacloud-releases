﻿function wrightAfterSave(sketchData, callback) {
    if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }

    let parcel = window.opener ? window.opener.activeParcel : activeParcel,
        getDataField = window.opener ? window.opener.getDataField : getDataField,
        getCategoryFromSourceTable = window.opener ? window.opener.getCategoryFromSourceTable : getCategoryFromSourceTable,
        WrightAddn_Lookup = window.opener ? window.opener.WrightAddn_Lookup : WrightAddn_Lookup,
        WrightCom_Lookup = window.opener ? window.opener.WrightCom_Lookup : WrightCom_Lookup,
        WrightComType_Lookup = window.opener ? window.opener.WrightComType_Lookup : WrightComType_Lookup,
        lookupdata = window.opener ? window.opener.lookup : lookup, saveData = '', newData = '';

    let newRecords = [], deletedRecords = [], newRecId = ccTicks(), addCatId = getCategoryFromSourceTable('ADDN').Id, ctextCatId = getCategoryFromSourceTable('COMINTEXT').Id, cfeatCatId = getCategoryFromSourceTable('COMFEAT').Id;

    var serverUpdate = function () {
        let isdtr = window.opener ? window.opener.__DTR : (typeof (__DTR) !== 'undefined' ? __DTR : false);
        let isSvApp = window.opener && window.opener.appType == "sv" ? true : (typeof (appType) !== 'undefined' && appType == "sv" ? true : false);
        let _appType = isdtr ? 'DTR' : (isSvApp ? 'SV' : 'QC');

        var saveRequestData = {
            ParcelId: parcel.Id,
            Priority: '',
            AlertMessage: '',
            Data: '',
            recoveryData: '',
            AppraisalType: '',
            activeParcelPhotoEditsdata: '',
            appType: _appType
        };

        if (saveData != '') {
            saveRequestData.Data = newData + saveData;
            console.log(saveRequestData.Data);
            $.ajax({
                url: '/quality/saveparcelchanges.jrq',
                type: "POST",
                dataType: 'json',
                data: saveRequestData,
                success: function (resp) {
                    if (resp.status == "OK") {
                        console.log('sketch successfully saved');
                        if (callback) callback();
                        return;
                    }
                }
            });
        }
        else {
            if (callback) callback();
            return;
        }
    }

    var createNewRecord = function (newRecds) {
        if (newRecds.length == 0) {
            serverUpdate(); return;
        }

        let newRec = newRecds.shift();
        insertNewAuxRecordSketch(parcel.Id, newRec.parentRecord, newRec.catId, newRec.rowId, newRec.AdditionalFields, (saveNewRecordData) => {
            if (saveNewRecordData) newData += saveNewRecordData;
            createNewRecord(newRecds);
        });
    }

    var deleteRecord = function (delRecords, delCallback) {
        delRecords.forEach((delRec) => {
            newData += '^^^' + parcel.Id + '|' + delRec.rowuid + '|||' + delRec.sourceTable + '|delete|\n';
        });
        delRecords = [];
        createNewRecord(newRecords);
        return;
    }

    var findAssociatedRecord = function (sk, tbl, prowId) {
        let svs = sk.vectorString.split('[')[1].split(']')[0], rowuid = null, apex_id = -1; apex_id = parseInt(svs.split(/\,/g)[4]);
        if (apex_id < 0) {
            if (svs.indexOf('{') > -1 && svs.indexOf('}') > -1) {
                rowuid = svs.indexOf('#') > -1 ? (svs.split('#')[1].split('$')[0]) : svs.split('{')[1].split('}')[0];
            }
            else if (sk.tableRowId) {
                rowuid = sk.tableRowId.indexOf('#') > -1 ? (sk.tableRowId.split('#')[1].split('$')[0]) : sk.tableRowId.split('{')[1].split('}')[0];
            }
        }

        let rec = parcel[tbl].filter((x) => { return x.ParentROWUID == prowId && x.CC_Deleted != true && ((apex_id > -1 && x.AREAID == (parseInt(apex_id) + 1)) || (rowuid && x.ROWUID == rowuid)) })[0];
        return { record: rec, apex_id: apex_id, rowuid: rowuid };
    }

    var createString = function (vs, table, rowId, sp) {
        let tempstr = vs.split('[')[1].split(']')[0].split(/\,/);
        if (sp && tempstr[tempstr.length - 1].indexOf('{') > -1 && tempstr[tempstr.length - 1].indexOf('}') > -1) tempstr.pop();
        tempstr[tempstr.length] = sp ? rowId : ('{$' + table + '#' + parseFloat(rowId) + '$}');
        return vs.split('[')[0] + '[' + tempstr.toString() + ']' + vs.split(']')[1] + ';';
    }

    var processSketchData = function (sketchRecords) {
        if (sketchRecords.length == 0) {
            deleteRecord(deletedRecords); return;
        }

        let sketchRecord = sketchRecords.shift(), sketch = sketchApp.sketches.filter((s) => { return s.sid == sketchRecord.rowid })[0],
            vectors = sketch.vectors.filter((x) => { return !x.isFreeFormLineEntries }), vs = '', addnAreaField = getDataField('AREA', 'ADDN'), addnEffField = getDataField('EFF_AREA', 'ADDN'),
            pages = {}, skField = getDataField('CC_SKETCH'), edRecs = (sketchApp.createdWrightSketchCards ? sketchApp.createdWrightSketchCards : []),
            drec = parcel['CCV_DWELDAT_APEXSKETCH'].concat(edRecs.filter((x) => { return x.SoureTable == 'CCV_DWELDAT_APEXSKETCH' })),
            crec = parcel['CCV_COMDAT_APEXSKETCH'].concat(edRecs.filter((x) => { return x.SoureTable == 'CCV_COMDAT_APEXSKETCH' }));

        for (var x in vectors) {
            let v = vectors[x];
            if (v.pageNum && !pages[v.pageNum]) {
                let parentRecord = drec.filter((x) => { return x.CARD == v.pageNum && x.CC_Deleted != true })[0];
                if (parentRecord)
                    pages[v.pageNum] = { sketchTable: 'CCV_DWELDAT_APEXSKETCH', parentRecord: parentRecord, baseArea: 0, totalArea: 0, lzArea: 0 }
                else {
                    parentRecord = crec.filter((x) => { return x.CARD == v.pageNum && x.CC_Deleted != true })[0];
                    if (parentRecord) pages[v.pageNum] = { sketchTable: 'CCV_COMDAT_APEXSKETCH', parentRecord: parentRecord, comtextTotalArea: 0 }
                }
                if (pages[v.pageNum]) pages[v.pageNum]['lzFlag'] = false;
            }
        }

        vectors.forEach((v, i) => {
            let pg = v.pageNum, parentRecord = pages[pg]?.parentRecord, sketchTable = pages[pg]?.sketchTable, code = v.code, area = Math.round(v.area()), peri = Math.round(v.perimeter());
            if (sketchTable == 'CCV_DWELDAT_APEXSKETCH') {
                let lk = WrightAddn_Lookup.filter((x) => { return x.Id == code })[0];

                if (!pages[pg]['lzFlag']) {
                    pages[v.pageNum]['lzFlag'] = true; pages[v.pageNum].lzArea = area;
                }
                if (v.newRecord) {
                    newRecId = newRecId + 5; pages[v.pageNum].totalArea = pages[v.pageNum].totalArea + area;
                    let nrec = { rowId: newRecId, catId: addCatId, parentRecord: parentRecord, AdditionalFields: { AREAID: newRecId, AREA: area, EFF_AREA: 0 } }

                    if (lk) {
                        ['FIRST', 'LOWER', 'SECOND', 'THIRD'].forEach((x) => { if (lk[x] && lk[x] != '') nrec.AdditionalFields[x] = lk[x]; });
                        if (lk.BASEAREA == 'Y') { pages[v.pageNum].baseArea = pages[v.pageNum].baseArea + area; nrec.AdditionalFields.EFF_AREA = area; }
                    }
                    newRecords.push(nrec); vs += createString(v.vectorString, 'ADDN', newRecId);
                }
                else if (v.isModified) {
                    let rec = findAssociatedRecord(v, 'ADDN', parentRecord.ROWUID);

                    if (rec.record) {
                        if (rec.record.LLINE == 0) {
                            pages[v.pageNum]['lzFlag'] = true; pages[v.pageNum].lzArea = area;
                        }
                        pages[v.pageNum].totalArea = pages[v.pageNum].totalArea + area;
                        if (addnAreaField)
                            saveData += '^^^' + parcel.Id + '|' + rec.record.ROWUID + '|' + addnAreaField.Id + '||' + area + '|edit|' + rec.record.ParentROWUID + '\n';
                        if (lk) {
                            ['FIRST', 'LOWER', 'SECOND', 'THIRD'].forEach((x) => {
                                let fld = getDataField(x, 'ADDN');
                                if (lk[x] && lk[x] != '' && fld) saveData += '^^^' + parcel.Id + '|' + rec.record.ROWUID + '|' + fld.Id + '||' + lk[x] + '|edit|' + rec.record.ParentROWUID + '\n';
                            });
                            if (lk.BASEAREA == 'Y') {
                                pages[v.pageNum].baseArea = pages[v.pageNum].baseArea + area;
                                if (addnEffField) saveData += '^^^' + parcel.Id + '|' + rec.record.ROWUID + '|' + addnEffField.Id + '||' + area + '|edit|' + rec.record.ParentROWUID + '\n';
                            }
                        }

                        if (rec.record.CC_RecordStatus == 'I') {
                            let nassociateRowuid = parseFloat(rec.record.ROWUID) < 0 ? ('{$ADDN#' + parseFloat(rec.record.ROWUID) + '$}') : '{' + rec.record.ROWUID + '}';
                            vs += createString(v.vectorString, null, nassociateRowuid, true);
                        }
                        else if (!v.newRecord) { vs += v.vectorString + ';'; }
                    }
                    else {
                        pages[v.pageNum].baseArea = lk?.BASEAREA == 'Y' ? (pages[v.pageNum].baseArea + area) : pages[v.pageNum].baseArea; vs += v.vectorString + ';';
                    }
                }
                else {
                    let rec = findAssociatedRecord(v, 'ADDN', parentRecord.ROWUID);
                    if (rec?.record?.LLINE == 0) {
                        pages[v.pageNum]['lzFlag'] = true; pages[v.pageNum].lzArea = area;
                    }
                    pages[v.pageNum].baseArea = lk?.BASEAREA == 'Y' ? (pages[v.pageNum].baseArea + area) : pages[v.pageNum].baseArea;
                    vs += v.vectorString + ';';
                }
            }
            else if (sketchTable == 'CCV_COMDAT_APEXSKETCH') {
                let lk = WrightCom_Lookup.filter((x) => { return x.Id == code })[0];
                if (v.newRecord) {
                    newRecId = newRecId + 5, nrec = null;

                    if (lk?.TBLE == 'COMINTEXT') {
                        pages[v.pageNum].comtextTotalArea = pages[v.pageNum].comtextTotalArea + area;
                        nrec = { rowId: newRecId, catId: ctextCatId, parentRecord: parentRecord, AdditionalFields: { AREAID: newRecId, AREA: area, CUBICFT: (area * 10), PERIM: peri, SF: area } };
                        if (v.vectorExtraParameters?.wrightParams) {
                            let pm = v.vectorExtraParameters.wrightParams;
                            if (pm.FLRFROM) nrec.AdditionalFields['FLRFROM'] = pm.FLRFROM;
                            if (pm.FLRTO) nrec.AdditionalFields['FLRTO'] = pm.FLRTO;
                        }

                        let ulk = WrightComType_Lookup.filter((x) => { return x.Id == code })[0];
                        if (ulk?.USETYPE && ulk?.USETYPE != '') nrec.AdditionalFields['USETYPE'] = ulk.USETYPE;
                        vs += createString(v.vectorString, 'COMINTEXT', newRecId);
                    }
                    else if (lk?.TBLE == 'COMFEAT') {
                        nrec = { rowId: newRecId, catId: cfeatCatId, parentRecord: parentRecord, AdditionalFields: { AREAID: newRecId, AREA: area, STRUCT: code } }

                        /*if (v.vectorExtraParameters?.wrightParams) {
                            let pm = v.vectorExtraParameters.wrightParams;
                            if (pm.MEAS1) nrec.AdditionalFields['MEAS1'] = pm.MEAS1;
                            if (pm.MEAS2) nrec.AdditionalFields['MEAS2'] = pm.MEAS2;
                        }*/

                        vs += createString(v.vectorString, 'COMFEAT', newRecId);
                    }

                    if (nrec) newRecords.push(nrec);
                }
                else if (v.isModified) {
                    if (lk?.TBLE == 'COMINTEXT') {
                        let rec = findAssociatedRecord(v, 'COMINTEXT', parentRecord.ROWUID); pages[v.pageNum].comtextTotalArea = pages[v.pageNum].comtextTotalArea + area;
                        if (rec.record) {
                            ['AREA', 'SF', 'CUBICFT', 'PERIM'].forEach((x) => {
                                let fld = getDataField(x, 'COMINTEXT');
                                if (fld) {
                                    if (x == 'PERIM')
                                        saveData += '^^^' + parcel.Id + '|' + rec.record.ROWUID + '|' + fld.Id + '||' + peri + '|edit|' + rec.record.ParentROWUID + '\n';
                                    else if (x == 'CUBICFT')
                                        saveData += '^^^' + parcel.Id + '|' + rec.record.ROWUID + '|' + fld.Id + '||' + (area * 10) + '|edit|' + rec.record.ParentROWUID + '\n';
                                    else
                                        saveData += '^^^' + parcel.Id + '|' + rec.record.ROWUID + '|' + fld.Id + '||' + area + '|edit|' + rec.record.ParentROWUID + '\n';
                                }
                            });

                            if (v.vectorExtraParameters?.wrightParams) {
                                let pm = v.vectorExtraParameters.wrightParams, flField = getDataField('FLRFROM', 'COMINTEXT'), toField = getDataField('FLRTO', 'COMINTEXT');
                                if (pm.FLRFROM && flField) saveData += '^^^' + parcel.Id + '|' + rec.record.ROWUID + '|' + flField.Id + '||' + pm.FLRFROM + '|edit|' + rec.record.ParentROWUID + '\n';
                                if (pm.FLRTO) saveData += '^^^' + parcel.Id + '|' + rec.record.ROWUID + '|' + toField.Id + '||' + pm.FLRTO + '|edit|' + rec.record.ParentROWUID + '\n';
                            }

                            if (rec.record.CC_RecordStatus == 'I') {
                                let nassociateRowuid = parseFloat(rec.record.ROWUID) < 0 ? ('{$COMINTEXT#' + parseFloat(rec.record.ROWUID) + '$}') : '{' + rec.record.ROWUID + '}';
                                vs += createString(v.vectorString, null, nassociateRowuid, true);
                            }
                            else if (!v.newRecord) vs += v.vectorString + ';';
                        }
                        else if (!v.newRecord) vs += v.vectorString + ';';
                    }
                    else if (lk?.TBLE == 'COMFEAT') {
                        let rec = findAssociatedRecord(v, 'COMFEAT', parentRecord.ROWUID);

                        if (rec.record) {
                            let aField = getDataField('AREA', 'COMFEAT'), sField = getDataField('STRUCT', 'COMFEAT'), m1Field = getDataField('MEAS1', 'COMFEAT'), m2Field = getDataField('MEAS2', 'COMFEAT');
                            if (aField) saveData += '^^^' + parcel.Id + '|' + rec.record.ROWUID + '|' + aField.Id + '||' + area + '|edit|' + rec.record.ParentROWUID + '\n';
                            if (sField) saveData += '^^^' + parcel.Id + '|' + rec.record.ROWUID + '|' + sField.Id + '||' + code + '|edit|' + rec.record.ParentROWUID + '\n';

                            /*if (v.vectorExtraParameters?.wrightParams) {
                                let pm = v.vectorExtraParameters.wrightParams;
                                if (pm.MEAS1 && m1Field) saveData += '^^^' + parcel.Id + '|' + rec.record.ROWUID + '|' + m1Field.Id + '||' + pm.MEAS1 + '|edit|' + rec.record.ParentROWUID + '\n';
                                if (pm.MEAS2 && m2Field) saveData += '^^^' + parcel.Id + '|' + rec.record.ROWUID + '|' + m2Field.Id + '||' + pm.MEAS2 + '|edit|' + rec.record.ParentROWUID + '\n';
                            }*/

                            if (rec.record.CC_RecordStatus == 'I') {
                                let nassociateRowuid = parseFloat(rec.record.ROWUID) < 0 ? ('{$COMINTEXT#' + parseFloat(rec.record.ROWUID) + '$}') : ('{' + rec.record.ROWUID + '}');
                                vs += createString(v.vectorString, null, nassociateRowuid, true);
                            }
                            else if (!v.newRecord) vs += v.vectorString + ';';
                        }
                        else if (!v.newRecord) vs += v.vectorString + ';';
                    }
                    else {
                        //if lookup doesn't matched.
                        vs += v.vectorString + ';';
                    }
                }
                else {
                    pages[v.pageNum].comtextTotalArea = lk?.TBLE == 'COMINTEXT' ? (pages[v.pageNum].comtextTotalArea + area) : pages[v.pageNum].comtextTotalArea; vs += v.vectorString + ';';
                }
            }
            else vs += v.vectorString + ';';
        });

        for (k in pages) {
            let pg = pages[k];
            if (pg.sketchTable == 'CCV_DWELDAT_APEXSKETCH') {
                ['ACAREA', 'ADDNAREA', 'HEATAREA', 'SFLA', 'ADJAREA', 'AREASUM', 'EFF_AREA', 'FLR1AREA', 'MGFA'].forEach((x) => {
                    let fld = getDataField(x, 'CCV_DWELDAT_APEXSKETCH');
                    if (fld) {
                        if (x == 'ADJAREA') {
                            saveData += '^^^' + parcel.Id + '|' + pg.parentRecord.ROWUID + '|' + fld.Id + '||' + (Math.round(pg.lzArea / 10) * 10) + '|edit|\n';
                        }
                        else if (['AREASUM', 'EFF_AREA', 'FLR1AREA', 'MGFA'].includes(x)) {
                            saveData += '^^^' + parcel.Id + '|' + pg.parentRecord.ROWUID + '|' + fld.Id + '||' + pg.lzArea + '|edit|\n';
                        }
                        else {
                            saveData += '^^^' + parcel.Id + '|' + pg.parentRecord.ROWUID + '|' + fld.Id + '||' + pg.baseArea + '|edit|\n';
                        }
                    }
                });
            }
            else if (pg.sketchTable == 'CCV_COMDAT_APEXSKETCH') {
                let bField = getDataField('BUSLA', 'CCV_COMDAT_APEXSKETCH'), cuField = getDataField('CUBICFT', 'CCV_COMDAT_APEXSKETCH');
                if (bField) saveData += '^^^' + parcel.Id + '|' + pg.parentRecord.ROWUID + '|' + bField.Id + '||' + pg.comtextTotalArea + '|edit|\n';
                if (cuField) saveData += '^^^' + parcel.Id + '|' + pg.parentRecord.ROWUID + '|' + cuField.Id + '||' + (pg.comtextTotalArea * 10) + '|edit|\n';
            }
        }

        if (skField) {
            let jsonString = sketch.jsonString; jsonString.DCS = vs; vs = JSON.stringify(jsonString);
            saveData += '^^^' + parcel.Id + '|0|' + skField.Id + '||' + vs + '|edit|\n';
        }

        processSketchData(sketchRecords);
    }

    sketchApp.deletedVectors.filter((x) => { return !x.newRecord && !x.isFreeFormLineEntries }).forEach((v) => {
        let vTable = '', parentRecord = parcel['CCV_DWELDAT_APEXSKETCH'].filter((x) => { return x.CARD == v.pageNum && x.CC_Deleted != true })[0];

        if (parentRecord) vTable = 'CCV_DWELDAT_APEXSKETCH';
        else {
            parentRecord = parcel['CCV_COMDAT_APEXSKETCH'].filter((x) => { return x.CARD == v.pageNum && x.CC_Deleted != true })[0];
            vTable = parentRecord ? 'CCV_COMDAT_APEXSKETCH' : '';
        }

        if (vTable == 'CCV_DWELDAT_APEXSKETCH') {
            let rec = findAssociatedRecord(v, 'ADDN', parentRecord.ROWUID);
            if (rec.record) deletedRecords.push({ sourceTable: 'ADDN', rowuid: rec.record.ROWUID });
        }
        else if (vTable == 'CCV_COMDAT_APEXSKETCH') {
            let lk = WrightCom_Lookup.filter((x) => { return x.Id == v.code })[0];

            if (lk?.TBLE == 'COMINTEXT') {
                let rec = findAssociatedRecord(v, 'COMINTEXT', parentRecord.ROWUID);
                if (rec.record) deletedRecords.push({ sourceTable: 'COMINTEXT', rowuid: rec.record.ROWUID });
            }
            else if (lk?.TBLE == 'COMFEAT') {
                let rec = findAssociatedRecord(v, 'COMFEAT', parentRecord.ROWUID);
                if (rec.record) deletedRecords.push({ sourceTable: 'COMFEAT', rowuid: rec.record.ROWUID });
            }
        }
    });

    let delcards = sketchApp.deletedWrightSketchCards ? sketchApp.deletedWrightSketchCards : [];
    delcards.forEach((dr) => {
        let parentRecord = parcel['CCV_DWELDAT_APEXSKETCH'].filter((x) => { return x.CARD == dr.Card && x.CC_Deleted != true })[0], cat = null;

        if (parentRecord) {
            cat = getCategoryFromSourceTable('CCV_DWELDAT_APEXSKETCH');
        }
        else {
            parentRecord = parcel['CCV_COMDAT_APEXSKETCH'].filter((x) => { return x.CARD == dr.Card && x.CC_Deleted != true })[0];
            cat = getCategoryFromSourceTable('CCV_COMDAT_APEXSKETCH');
        }
        if (cat && parentRecord)
            newData += '^^^' + parcel.Id + '|' + parentRecord.ROWUID + '|||' + cat.SourceTable + '|delete|\n';
    });

    var createCardRecords = function (newCards) {
        if (newCards.length == 0) {
            delete sketchApp.createdWrightSketchCards;
            delete sketchApp.deletedWrightSketchCards;
            processSketchData(sketchData);
            return;
        }

        let nr = newCards.pop(), cat = getCategoryFromSourceTable(nr.SoureTable);
        insertNewAuxRecordSketch(parcel.Id, null, cat.Id, nr.ROWUID, { CARD: nr.CARD }, (saveNewRecordData) => {
            if (saveNewRecordData) newData += saveNewRecordData;
            createCardRecords(newCards);
        });
    }

    let newcards = sketchApp.createdWrightSketchCards ? sketchApp.createdWrightSketchCards : [];
    createCardRecords(newcards);    
}