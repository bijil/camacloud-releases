﻿
CAMACloud.Sketching.Formatters.WarrenOH = {
    name: "Warren OH",
    saveAllSegments: false,
    allowSegmentAddition: false,
    allowSegmentDeletion: false,
    arcMode: "disable", //to disable Ellipse 
    preventSavingOpenSegments: false,
    allowSegmentAdditionOnMidsketch: true,
    nodeToString: function (n) {
        var str = "";
        if (!n.isStart && (n.dy > 0) && (n.dx > 0)) {
            str += "A";
        }
        if (n.dy > 0) {
            str += n.sdy + sRound(n.dy);
        }
        if (n.dx > 0) {
            str += n.sdx + sRound(n.dx);
        }
        if (n.isStart) {
            str += "C";
        }
        return str;
    },
    vectorToString: function (v) {
        var str = "";
        if (v.startNode != null) {
            str += v.startNode.vectorString();
            var nn = v.startNode.nextNode;
            while (nn != null) {
                str += nn.vectorString();
                nn = nn.nextNode;
            }
            str += "H"
        }
        return str;
    },
    labelPositionFromString: function (editor, l) {
        if (!l)
            return null
        var parts = l.split(',');
        return new PointX(parseFloat(parts[0]), -(parseFloat(parts[1])))
    },
    vectorFromString: function (editor, s) {
        if (s == null) {
            var nv = new Vector(editor);
            nv.isClosed = false;
            nv.label = "Blank"
            nv.uid = -1
            return nv;
        }

        var o = editor.origin;
        var p = o.copy();

        var v = new Vector(editor);

        var hi = s.split(':');
        var head = hi[0];
        var lineString = s

        v.header = "";

        var isStart = false;
        var hasStarted = false;
        var isVeer = false;
        var veerSteps = 0;
        var ins = lineString.match(/[C]|[UDLRA*][0-9]*/g);
        var oppositAction = false;
        for (i = 0; i < ins.length; i++) {
            var cmd = ins[i];
            var distance = 0;
            if (cmd == "*") {
                if (!oppositAction) {
                    var g = i - 2;
                    cmd = ins[g];
                    i = g;
                    oppositAction = true;
                } else continue;
            }
            if (cmd == "C") {
                isStart = true;
            }

            else if (cmd == "A") {
                isVeer = true;
                veerSteps = 2;
                continue;
            }
            else {
                var cp = cmd.match(/[UDLR]|[0-9]*/g);
                var dir = cp[0];
                distance = cp[1];
                var pix = parseFloat(distance) * DEFAULT_PPF;
                switch (dir) {
                    case "U": oppositAction ? p.moveBy(0, -pix) : p.moveBy(0, pix); break;
                    case "D": oppositAction ? p.moveBy(0, pix) : p.moveBy(0, -pix); break;
                    case "L": oppositAction ? p.moveBy(pix, 0) : p.moveBy(-pix, 0); break;
                    case "R": oppositAction ? p.moveBy(-pix, 0) : p.moveBy(pix, 0); break;
                }
                if (!isVeer)
                    p = p.copy();
            }

            if (isVeer) {
                veerSteps--;
                if (veerSteps == 0) {
                    isVeer = false;
                } else
                    continue;
            }

            if (isStart) {
                v.start(p.copy());
                hasStarted = true;
                isStart = false;
            } else if (hasStarted) {
                var q = p.copy().alignToGrid(editor);
                if (!v.startNode.overlaps(q))
                    v.connect(p.copy());
                else {
                    v.terminateNode(v.startNode);
                    if (oppositAction)
                    { i = ins.length }
                }
            }
            if (v.endNode)
                if (cmd) v.endNode.commands.push(cmd);


        }

        if (v.startNode)
            if (v.startNode.overlaps(v.endNode.p.copy()))
                v.isClosed = true;
        return v;

    },
    open: function (editor, data) {
        editor.vectors = [];
        editor.sketches = [];
        let isError = false;

        for (var x in data) {
            var sketch = new Sketch(editor);
            var s = data[x];
            sketch.parentRow = s.parentRow;
            sketch.uid = s.uid;
            sketch.label = s.label;
            sketch.vectors = [];
            sketch.lookUp = s.lookUp;
            sketch.config = s.config || {};
            sketch.sid = s.sid;
            sketch.maxNotelen = s.maxNotelen;
            var counter = 0;
            try {
                for (var i in s.sketches) {
                    counter += 1;
                    var sk = s.sketches[i];
                    var v = this.vectorFromString(editor, sk.vector);
                    v.sketch = sketch;
                    v.uid = sk.uid;
                    v.index = counter;
                    v.name = sk.label[0].Value;
                    if (sk.label[0] && sk.label[0].colorCode) {
                        var clrcode = sk.label[0].colorCode.split('~');
                        var isdtr = window.opener? window.opener.__DTR:  (typeof(__DTR) !== 'undefined'? __DTR: false);
            		    let isSvApp = window.opener && window.opener.appType == "sv"? true: (typeof(appType) !== 'undefined' && appType == "sv"? true: false);
                	    var DQC = isdtr? 'DTR':(isSvApp? 'SV': 'QC');
                        v.colorCode = clrcode[0]? clrcode[0]: null;
                        v.newLineColor = (clrcode[1] && sketchSettings['SketchLineColorCustomizations'] && sketchSettings['SketchLineColorCustomizations'].contains(DQC))? clrcode[1]: null;
                        v.newLabelColorCode = (clrcode[2] && sketchSettings['SketchTextColorCustomizations'] && sketchSettings['SketchTextColorCustomizations'].contains(DQC))? clrcode[2]: null;
                    }
                    v.label = '[' + counter + '] ' + sk.label.map(function (a) { return ( ( a.Description && sketchSettings["DoNotShowLabelDescriptionSketch"] != '1' && clientSettings["DoNotShowLabelDescriptionSketch"] != '1' && a.Description.Name ) ? ( ( sketchSettings["DoNotShowLabelCode"] == '1' || a.showLabelDescriptionOnly ) ? a.Description.Name : a.Value + '-' + a.Description.Name ) : a.Value ) }).filter(function (a) { return a }).join('/');
                    v.labelFields = sk.label;
                    v.vectorString = sk.vector;
                    v.isChanged = sk.isChanged;
                    v.vectorConfig = sk.vectorConfig;
                    v.labelPosition = this.labelPositionFromString(editor, sk.labelPosition);
                    editor.vectors.pop();
                    sketch.vectors.push(v);
                }
            }
            catch (e) {
                editor.vectors = []; sketch.vectors = []; isError = true;
            }
            sketch.isModified = false;
            editor.loadNotesForSketch(sketch, s.notes);
            editor.sketches.push(sketch);
        }

        if (isError) {
            editor.sketches.forEach((sk) => { sk.vectors = []; });
            throw "Sketch cannot be rendered due to the wrong sketch data.";
        }
    } 
}
