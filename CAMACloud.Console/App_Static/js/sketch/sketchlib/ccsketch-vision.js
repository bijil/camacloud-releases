﻿
CAMACloud.Sketching.Formatters.Vision = {
    name: "CAMACloud.SketchFormat",
    saveAllSegments: true,
    allowSegmentAddition: false,
    allowSegmentDeletion: false,
    arcMode: 'ARC',
    vectorSeperator: ";",
    originPosition: "topLeft",
    newVectorHeaderSuffix: '[-1,-1]:',
    nodeToString: function (n) {
        var str = "";
        if (!n.isStart && (n.isEllipse || n.isEllipseEndNode)) {
            if (n.dy > 0) { str += "A" + sRound(n.dy); }

            if (n.dx > 0) { str += (str != '') ? ("/" + n.sdx + sRound(n.dx * 2)) : (n.sdx + sRound(n.dx * 2)); }
        }
        else {
            if (n.arcLength > 0) {
                str += "A" + sRound(n.arcLength);
            }
            if (n.arcLength < 0) {
                str += "B" + Math.abs(sRound(n.arcLength));
            }
            if (n.dy > 0) {
                if (n.isStart) {
                    if (str != "") str += " ";
                } else {
                    if (str != "") str += "/";
                }
                str += n.sdy + sRound(n.dy);
            }
            if (n.dx > 0) {
                if (n.isStart) {
                    if (str != "") str += " ";
                } else {
                    if (str != "") str += "/";
                }
                str += n.sdx + sRound(n.dx);
            }
            if (n.hideDimensions) {
                if (str != "") str += "/";
                str += "#";
            }
            if (n.isStart) {
                str += " S";
            }
            if (n.vector.placeHolder && n.isStart) {
                str += " P" + n.vector.placeHolderSize.toString();
            }
        }
        return str;
    },
    vectorToString: function (v, fromSketchSave) {
        var str = "";
        var label = v.name;
        if (v.startNode != null) {
            if ( !v.labelEdited && !v.newRecord ){
                if (v.header) {
                    var r1 = /(.*?)\[(.*?)\]/;
                    var headpart = r1.exec(v.header);
                    var labels = "", refString = "[-1,-1]";
                    if (headpart != null) {
                        labels = headpart[1];
                        refString = "[" + headpart[2] + "]"
                    }
                    var headerValue = labels + refString + ":";
                    str += headerValue;
                } else {
                    if (v.referenceIds) {
                        str += label + "[" + v.referenceIds + "]:";
                    } else {
                        if (v.rowId)
                            str += label + "[" + v.rowId + ",-1]:";
                        else
                            str += label + "[-1,-1]:";
                    }

                };
            }
            else {
                str += label + "[-1," + (v.sectionNum ? v.sectionNum : '-1') + "," + this.labelPositionToString( v.labelPosition ) + "," + (v.fixedArea? v.fixedArea.trim(): '-1') + "," + (v.visketchType? v.visketchType: 0) + "]:";
            }
            str += v.startNode.vectorString();
            if (v.fixedArea && fromSketchSave) {
                let spt = str.split(":"), lstr = "S U10 R10 D10 L10", ssstr = spt[1].split('S')[0];
                if (v.unSketchedString) {
                    ssplit = v.unSketchedString.split("S");
                    if (ssplit[1]) {
                        lstr = "S " + ssplit[1].trim();
                        if (!v.isModified) ssstr = ssplit[0].trim() + " ";
                    }
                }

                str = spt[0] + ':' + ssstr + lstr;
            }
            else {
                var nn = v.startNode.nextNode;
                while (nn != null) {
                    str += " ";
                    str += nn.vectorString();
                    nn = nn.nextNode;
                }
            }
        }
        return str;
    },
    vectorFromString: function (editor, s) {
        if ((s || '').trim() == '') {
            var nv = new Vector(editor);
            nv.isClosed = false;
            nv.label = "Blank"
            nv.uid = -1;
            return nv;
        }

        var o = editor.origin;
        var p = o.copy();

        var v = new Vector(editor);
        this.updateVectorFromString(editor, v, s);
        v.isModified = false;
        return v;
    },
    updateVectorFromString: function (editor, v, s) {
        v.startNode = null;
        v.endNode = null;
        var o = editor.origin;
        var p = o.copy();

        var hi = s.split(':');
        var head = hi[0];
        var lineString = hi[1] || '';
        var r1 = /(.*?)\[(.*?)\]/;
        var headpart = r1.exec(head);
        var labels = "", refString = "[-1,-1]";
        if (headpart != null) {
            labels = headpart[1];
            refString = "[" + headpart[2] + "]"
        }
        v.header = labels + refString + ":";
        var isStart = false;
        var hasStarted = false;
        var isVeer = false;
        var veerSteps = 0;
        var nodeStrings = lineString.split(' ');
        let isOval = headpart[2].split(',')[4] && headpart[2].split(',')[4] == '1' ? true : false, ovalStart = false;
        for (var i in nodeStrings) {
            var arcLength = 0;
            var hideDimensions = false;
            var isPlaceholder = false;
            var ns = nodeStrings[i];
            if (ns.trim() != '') {
                var cmds = ns.split('/');
                if (isOval && ovalStart) {
                    let vl = lineString.split('S')[1].trim().split(' ');
                    if (vl.length > 2) throw "Invalid sketch string format";
                    let cps = vl[0].match(/[ABUDLR#PC]|[0-9]+(.[0-9]+)?/g), h = (parseFloat(cps[1])) * DEFAULT_PPF, w = parseFloat(cps[3] / 2) * DEFAULT_PPF;
                    p = p.copy(); p.moveBy(0, h);
                    p = p.copy(); p.moveBy(w, 0);
                    v.connect(p.copy()); v.endNode.isEllipse = true; if (cmd) v.endNode.commands.push(cmd);
                    p = p.copy(); p.moveBy(0, -h);
                    p = p.copy(); p.moveBy(-w, 0);
                    v.connect(p.copy()); v.endNode.isEllipseEndNode = true;
                    break;
                }

                for (var c in cmds) {
                    var cmd = cmds[c];
                    if (cmd == '') continue;
                    if (cmd == 'S') {
                        isStart = true;
                        ovalStart = true;
                    }
                    else {
                        var cp = cmd.match(/[ABUDLR#PC]|[0-9]+(.[0-9]+)?/g);
                        var dir = cp[0];
                        distance = cp[1] || 0;
                        var pix = CAMACloud.Sketching.Utilities.toPixel(distance);
                        switch (dir) {
                            case "A": arcLength = Number(distance); break;
                            case "B": arcLength = -Number(distance); break;
                            case "U": p.moveBy(0, pix); break;
                            case "D": p.moveBy(0, -pix); break;
                            case "L": p.moveBy(-pix, 0); break;
                            case "R": p.moveBy(pix, 0); break;
                            case "#": hideDimensions = true; break;
                            case "P": isPlaceholder = true; break;
                        }
                    }
                }

                p = p.copy();

                if (isPlaceholder) {
                    v.placeHolder = v.isUnSketchedArea = v.CheckFixedArea = true;
                    v.placeHolderSize = Number(distance);
                    v.terminateNode(v.startNode);
                    // v.radiusNode = new Node(v.startNode.p.copy().moveBy(v.placeHolderSize, 0), v)   //for circle radiusNode
                }
                else if (isStart) {
                    v.start(p.copy());
                    hasStarted = true;
                    isStart = false;
                } else if (hasStarted) {
                    var q = p.copy().alignToGrid(editor);
                    if (!v.startNode.overlaps(q))
                        v.connect(p.copy());
                    else
                        v.terminateNode(v.startNode);
                }

                if (v.endNode) {
                    v.endNode.hideDimensions = hideDimensions;
                    v.endNode.arcLength = arcLength;
                    if (arcLength != 0)
                        v.endNode.isArc = true;
                }

            }
        }

        if (v.startNode)
            if (v.startNode.overlaps(v.endNode.p.copy()))
                v.isClosed = true;
        return v;
    },
    labelPositionFromString: function (editor, l) {
        if (!l)
            return null
        l = l.replace(' ', '');
        var ins = l.match(/[UDLR]|[0-9.]*/g);
        if (ins.length > 3) {
            var x = ins[0] == "R" ? ins[1] : -ins[1];
            var y = ins[2] == "U" ? ins[3] : -ins[3];
            return new PointX(parseFloat(x), (parseFloat(y)))
        }

    },
    labelPositionToString: function (p) {
        var x = p.x > 0 ? "R" + p.x : "L" + -p.x;
        var y = p.y > 0 ? "U" + p.y : "D" + -p.y;
        return x + " " + y
    },
    open: function (editor, data) {
        editor.vectors = [];
        editor.sketches = [];
        let isError = false;

        for (var x in data) {
            var sketch = new Sketch(editor);
            var s = data[x];
            sketch.parentRow = s.parentRow;
            sketch.uid = s.uid;
            sketch.label = s.label;
            sketch.vectors = [];
            sketch.config = s.config || {};
            sketch.sid = s.sid;
            sketch.lookUp = s.lookUp;
            sketch.isShowSectNum = s.isShowSectNum;
            sketch.maxNotelen = s.maxNotelen;
            var counter = 0;
            try {
                for (var i in s.sketches) {
                    var sk = s.sketches[i];
                    var segments = (sk.vector || '').split(';');
                    var scount = 0;
                    if (segments.length < 2) scount = -1;
                    if (!sk.otherValues) sk.OtherValues = {};
                    for (var si in segments) {
                        counter += 1;
                        scount += 1;
                        var sv = segments[si];
                        var v = this.vectorFromString(editor, sv);
                        v.sketch = sketch;
                        v.uid = sk.uid + (scount ? '/' + scount : '');
                        v.index = counter;
                        v.name = sk.label[0].Value;
                        v.label = '[' + counter + '] ' + sk.label.map(function (a) { return ((a.Description && sketchSettings["DoNotShowLabelDescriptionSketch"] != '1' && clientSettings["DoNotShowLabelDescriptionSketch"] != '1' && a.Description.Name) ? ((sketchSettings["DoNotShowLabelCode"] == '1' || a.showLabelDescriptionOnly) ? a.Description.Name : a.Value + '-' + a.Description.Name) : a.Value) }).filter(function (a) { return a }).join('/');
                        v.labelFields = sk.label;
                        if (sk.label[0] && sk.label[0].colorCode) {
                            var clrcode = sk.label[0].colorCode.split('~');
                            var isdtr = window.opener ? window.opener.__DTR : (typeof (__DTR) !== 'undefined' ? __DTR : false);
                            let isSvApp = window.opener && window.opener.appType == "sv" ? true : (typeof (appType) !== 'undefined' && appType == "sv" ? true : false);
                            var DQC = isdtr ? 'DTR' : (isSvApp ? 'SV' : 'QC');
                            v.colorCode = clrcode[0] ? clrcode[0] : null;
                            v.newLineColor = (clrcode[1] && sketchSettings['SketchLineColorCustomizations'] && sketchSettings['SketchLineColorCustomizations'].contains(DQC)) ? clrcode[1] : null;
                            v.newLabelColorCode = (clrcode[2] && sketchSettings['SketchTextColorCustomizations'] && sketchSettings['SketchTextColorCustomizations'].contains(DQC)) ? clrcode[2] : null;
                        }
                        v.vectorString = sv;
                        v.referenceIds = sk.referenceIds;
                        var ridParts = (sk.referenceIds || '').split(",");
                        if (ridParts.length > 3) {
                            v.areaValue = ridParts[3];
                        }
                        v.rowId = sk.rowId;
                        v.isChanged = sk.isChanged;
                        v.vectorConfig = sk.vectorConfig;
                        v.noVectorSegment = sk.noVectorSegment;

                        if (sk.otherValues) {
                            for (var keys in sk.otherValues) {
                                v[keys] = sk.otherValues[keys];
                            }
                        }

                        if (sketch.isShowSectNum && v.sectionNum) {
                            if (v.sectionNum.split('{').length < 2)
                                v.label = v.label + ' (S:' + v.sectionNum + ')';
                        }
                        this.AreaUnit = sk.areaUnit;
                        v.labelPosition = this.labelPositionFromString(editor, sk.labelPosition);
                        editor.vectors.pop();
                        sketch.vectors.push(v);
                    }
                }
            }
            catch (e) {
                editor.vectors = []; sketch.vectors = []; isError = true;
            }
            sketch.isModified = false;
            editor.loadNotesForSketch(sketch, s.notes, null, ['noteData', 'ww', 'hh']);
            editor.sketches.push(sketch);
        }

        if (isError) {
            editor.sketches.forEach((sk) => { sk.vectors = []; });
            throw "Sketch cannot be rendered due to the wrong sketch data.";
        }
    },
    getParts: function (vectorString, decoder, sections) {
        var p = [], nc = 0, noteList = [];
        var parts = (vectorString || '').split(";");
        for (var i in parts) {
            var part = parts[i];
            if (/(.*?):(.*?)/.test(part)) {
                var hi = part.split(':');
                var head = hi[0];
                var info = hi[1];
                var labels, refString, labelPosition = null, fixedArea = null, visketchType = 0, visionDeleted = false, unSketchedString = "";

                var r1 = /(.*?)\[(.*?)\]/;
                var hparts = r1.exec(head);
                if (hparts == null) {
                    labels = "";
                    refString = "-1,-1";
                } 
                else {
                    labels = hparts[1].replace(/\{(.*?)\}/g, '');
                    refString = hparts[2];
                    var t = refString.split(',');

                    if (t.length > 3 && t[4] == '3' && t[5]) {
                        let sp = info.split(' S '), sl = sp[0]?.contains('/') ? sp[0].trim().split('/') : sp[0].trim().split(' '), sll = sp[1].split(' '), w = 0, h = 0, sla = [sll[0], sll[1]], ww = 0, hh = 0;
                        for (s in sl) {
                            if (sl[s]?.trim() != '') {
                                let cp = sl[s].trim().match(/[UDLR#]|[0-9]+(.[0-9]+)?/g), dir = cp[0], distance = cp[1] || 0;
                                switch (dir) {
                                    case "U": h = parseFloat(distance); break;
                                    case "D": h = parseFloat(-distance); break;
                                    case "L": w = parseFloat(-distance); break;
                                    case "R": w = parseFloat(distance); break;
                                }
                            }
                        }

                        if (sla[0].contains('/') || sla[1].contains('/')) {
                            sla[0].split('/').forEach((a) => {
                                if (a.contains('R') || a.contains('L')) {
                                    let cp = a.trim().match(/[LR]|[0-9]+(.[0-9]+)?/g), dir = cp[0], distance = cp[1] || 0;
                                    switch (dir) {
                                        case "L": w += parseFloat(-distance / 2); ww = parseFloat(-distance / 2); break;
                                        case "R": w += parseFloat(distance / 2); ww = parseFloat(distance / 2); break;
                                    }
                                }
                            });
                            sla[1].split('/').forEach((a) => {
                                if (a.contains('U') || a.contains('D')) {
                                    let cp = a.trim().match(/[UD]|[0-9]+(.[0-9]+)?/g), dir = cp[0], distance = cp[1] || 0;
                                    switch (dir) {
                                        case "U": h += parseFloat(distance / 2); hh = parseFloat(distance / 2); break;
                                        case "D": h += parseFloat(-distance / 2); hh = parseFloat(-distance / 2); break;
                                    }
                                }
                            });
                        }
                        else {
                            for (s in sla) {
                                if (sla[s]?.trim() != '') {
                                    let cp = sla[s].trim().match(/[UDLR#]|[0-9]+(.[0-9]+)?/g), dir = cp[0], distance = cp[1] || 0;
                                    switch (dir) {
                                        case "U": h += parseFloat(distance / 2); hh = parseFloat(distance / 2); break;
                                        case "D": h += parseFloat(-distance / 2); hh = parseFloat(-distance / 2); break;
                                        case "L": w += parseFloat(-distance / 2); ww = parseFloat(-distance / 2); break;
                                        case "R": w += parseFloat(distance / 2); ww = parseFloat(distance / 2); break;
                                    }
                                }
                            }
                        }
                        noteList.push({
                            isVisionNote: true,
                            note: { uid: nc, text: Base64.decode(t[5]), x: w, y: h, noteData: part, ww: ww, hh: hh }
                        });
                        nc = nc + 1;
                        continue;
                    }

                    refString = t[0] + ',' + t[1];
                    var sectionNum = refString && refString.split( ',' )[1] ? refString.split( ',' )[1] : null;
                    if ( t.length > 2 ) { labelPosition = t[2]; }
                    if ( t.length > 3 ) {
                        fixedArea = t[3] && parseFloat(t[3]) != -1 && parseFloat(t[3]) != 0 ? t[3] : null;
                        visketchType = t[4] ? t[4] : 0;
                        if (fixedArea) {
                            part = head + ':' + info.split("S")[0] + 'S P2';
                            unSketchedString = info;
                        }
                    }

                    if (sectionNum && sections && sections.sectParentTable) {
                        let sn = (sectionNum.contains('#') ? sectionNum.match(/#(.*)\$/)[1] : (sectionNum.contains('{') ? sectionNum.match(/{(.*)}/)[1] : sectionNum)), rec = sections.skRecord,
                            parentRec = rec[sections.sectParentTable].filter((s) => { return (s.SECT_NUM == sn || s.ROWUID == sn) })[0];
                        if (parentRec && parentRec['CC_IS_DELETED_SECTION'] == 'D') visionDeleted = true;
                    }
                }

                p.push({
                    label: labels,
                    vector: part,
                    referenceIds: refString,
                    labelPosition: labelPosition,
                    otherValues: { fixedArea: fixedArea, sectionNum: sectionNum, visketchType: visketchType, visionDeleted: visionDeleted, unSketchedString: unSketchedString }
                });
            }
        }
        p = p.concat(noteList);
        return p;
    } 
}