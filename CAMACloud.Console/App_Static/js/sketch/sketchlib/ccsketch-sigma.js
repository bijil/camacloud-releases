﻿
CAMACloud.Sketching.Formatters.Sigma = {
    name: "SIGMA Sketch Format",
    saveAllSegments: true,
    allowSegmentAddition: true,
    allowSegmentDeletion: true,
    nodeToString: function (n) {
        var str = "";
        if ((n.dy > 0) && (n.dx > 0)) {
            str += "V";
        }
        if (n.dy > 0) {
            str += n.sdy + sRound(n.dy);
        }
        if (n.dx > 0) {
            str += n.sdx + sRound(n.dx);
        }
        if (n.isStart) {
            str += "S";
        }
        return str;
    },
    vectorToString: function (v) {
        var str = "";
        if (v.startNode != null) {
            str += v.header;
            str += v.startNode.vectorString();
            var nn = v.startNode.nextNode;
            while (nn != null) {
                str += nn.vectorString();
                nn = nn.nextNode;
            }
        }
        return str;
    },
    vectorFromString: function (editor, s) {
        if (s == null) {
            var nv = new Vector(editor);
            nv.isClosed = false;
            nv.label = "Blank"
            nv.uid = -1
            return nv;
        }

        var o = editor.origin;
        var p = o.copy();

        var v = new Vector(editor);

        var hi = s.split(':');
        var head = hi[0];
        var lineString = hi[1];

        v.header = head + ":";

        var isStart = false;
        var hasStarted = false;
        var isVeer = false;
        var veerSteps = 0;
        var ins = lineString.match(/[S]|[V]|[UDLR][0-9]*/g);
        for (var i in ins) {
            var cmd = ins[i];
            var distance = 0;
            if (cmd == "S") {
                isStart = true;
            } else if (cmd == "V") {
                isVeer = true;
                veerSteps = 2;
                continue;
            }
            else {
                var cp = cmd.match(/[UDLR]|[0-9]*/g);
                var dir = cp[0];
                distance = cp[1];
                var pix = parseFloat(distance) * DEFAULT_PPF;
                switch (dir) {
                    case "U": p.moveBy(0, pix); break;
                    case "D": p.moveBy(0, -pix); break;
                    case "L": p.moveBy(-pix, 0); break;
                    case "R": p.moveBy(pix, 0); break;
                }
                if (!isVeer)
                    p = p.copy();
            }

            if (isVeer) {
                veerSteps--;
                if (veerSteps == 0) {
                    isVeer = false;
                } else
                    continue;
            }

            if (isStart) {
                v.start(p.copy());
                hasStarted = true;
                isStart = false;
            } else if (hasStarted) {
                var q = p.copy().alignToGrid(editor);
                if (!v.startNode.overlaps(q))
                    v.connect(p.copy());
                else
                    v.terminateNode(v.startNode);
            }
            if (v.endNode)
                if (cmd) v.endNode.commands.push(cmd);


        }

        if (v.startNode)
            if (v.startNode.overlaps(v.endNode.p.copy()))
                v.isClosed = true;
        return v;

    },
    open: function (editor, data) {
        editor.vectors = [];
        editor.sketches = [];
        let isError = false;

        for (var x in data) {
            var sketch = new Sketch(editor);
            var s = data[x];
            sketch.parentRow = s.parentRow;
            sketch.uid = s.uid;
            sketch.label = s.label;
            sketch.vectors = [];
            sketch.config = s.config || {};
            sketch.sid = s.sid;
            sketch.lookUp = s.lookUp;
            sketch.maxNotelen = s.maxNotelen;
            var counter = 0;
            try {
                for (var i in s.sketches) {
                    counter += 1;
                    var sk = s.sketches[i];
                    var v = this.vectorFromString(editor, sk.vector);
                    v.sketch = sketch;
                    v.uid = sk.uid;
                    v.index = counter;
                    v.name = sk.label[0].Value;
                    if (sk.label[0] && sk.label[0].colorCode) {
                        var clrcode = sk.label[0].colorCode.split('~');
                        var isdtr = window.opener ? window.opener.__DTR : (typeof (__DTR) !== 'undefined' ? __DTR : false);
                        let isSvApp = window.opener && window.opener.appType == "sv" ? true : (typeof (appType) !== 'undefined' && appType == "sv" ? true : false);
                        var DQC = isdtr ? 'DTR' : (isSvApp ? 'SV' : 'QC');
                        v.colorCode = clrcode[0] ? clrcode[0] : null;
                        v.newLineColor = (clrcode[1] && sketchSettings['SketchLineColorCustomizations'] && sketchSettings['SketchLineColorCustomizations'].contains(DQC)) ? clrcode[1] : null;
                        v.newLabelColorCode = (clrcode[2] && sketchSettings['SketchTextColorCustomizations'] && sketchSettings['SketchTextColorCustomizations'].contains(DQC)) ? clrcode[2] : null;
                    }
                    v.label = '[' + counter + '] ' + sk.label.map(function (a) { return ((a.Description && sketchSettings["DoNotShowLabelDescriptionSketch"] != '1' && clientSettings["DoNotShowLabelDescriptionSketch"] != '1' && a.Description.Name) ? ((sketchSettings["DoNotShowLabelCode"] == '1' || a.showLabelDescriptionOnly) ? a.Description.Name : a.Value + '-' + a.Description.Name) : a.Value) }).filter(function (a) { return a }).join('/');
                    v.labelFields = sk.label;
                    v.vectorString = sk.vector;
                    v.vectorConfig = sk.vectorConfig;
                    editor.vectors.pop();
                    sketch.vectors.push(v);
                }
            }
            catch (e) {
                editor.vectors = []; sketch.vectors = []; isError = true;
            }
            sketch.isModified = false;
            editor.loadNotesForSketch(sketch, s.notes);
            editor.sketches.push(sketch);
        }

        if (isError) {
            editor.sketches.forEach((sk) => { sk.vectors = []; });
            throw "Sketch cannot be rendered due to the wrong sketch data.";
        }
    },
    getParts: function (vectorString) {
        if (vectorString == null) vectorString = '';
        var p = [];
        var parts = vectorString.split(",");
        for (var i in parts) {
            var part = parts[i];
            if (/(.*?):(.*?)/.test(part)) {
                var hi = part.split(':');
                var head = hi[0];
                var info = hi[1];

                var r1 = /(.*?)\[(.*?)\]/;
                var hparts = r1.exec(head);
                if (hparts == null) {
                    labels = "";
                    posString = "R0U0";
                } else {
                    labels = hparts[1].replace(/\{(.*?)\}/g, '');
                    posString = hparts[2];
                }

                p.push({
                    label: labels,
                    vector: part
                })

            }
        }

        return p;
    },
    addNewVector: function () {

    } 
}
