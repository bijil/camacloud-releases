﻿CAMACloud.Sketching.Configs = {};
/*
	New variables in config
	----------------------------------------------------
	SkipIsApprovedChanges: In some config there is no labelField and command field , so no data push to approvedChanges or sketchchanges.
	ParcelValidationInBeforeSave: if new record created in before save validation occurs if required fields in category, validation function calls before beforesave actions.
	LabelFieldDelimter: it's used to show - between label fields, default / showing. Apply to sketch source labelField.
	SkipRowuidInChange: In farragut config, we are appending rowuid in sketch string after save. It causes chnage indication without modification.Property added to skip rowuid part in sketch string.
	LabelAreaPosition: true , it's used to calculate area label position of vector
	AddNewNote : to show notes button
	SketchLabelPrefixInSv : Property should added to sketch source , if config contain more than one sketch sourceTable. Otherwise we can't find mapped sketch data in sv.
*/

CAMACloud.Sketching.Configs.GetConfigFromSettings = function () {
    if ( clientSettings["SketchConfig"] != null || sketchSettings["SketchConfig"] != null ) {
        var cfg = eval( 'CAMACloud.Sketching.Configs.' + ( clientSettings["SketchConfig"] || sketchSettings["SketchConfig"] ) );
        if (cfg) return cfg;
    }
    var config = { sources: [] };
    var source = {
        SketchSource: { Table: null, LabelField: null, CommandField: null, KeyFields: [] },
        VectorSource: [{ Table: null, LabelField: null, CommandField: null, AreaField: null, PerimeterField: null, ConnectingFields: [] }]
    }
	var _cslen = 0;
	
    for (var x in clientSettings) {
        switch (x) {
            case "SketchLabel": source.SketchSource.LabelField = clientSettings[x]; _cslen++; break;
            case "SketchCommandField": source.SketchSource.CommandField = clientSettings[x]; _cslen++; break;
            case "SketchKeyField": source.SketchSource.KeyFields = clientSettings[x] ? clientSettings[x].split(',') : []; _cslen++; break;
            case "SketchTable": source.SketchSource.Table = clientSettings[x]; _cslen++; break;
            case "SketchVectorTable": source.VectorSource[0].Table = clientSettings[x]; _cslen++; break;
            case "SketchVectorLabelField": source.VectorSource[0].LabelField = clientSettings[x]; _cslen++; break;
            case "SketchVectorCommandField": source.VectorSource[0].CommandField = clientSettings[x]; _cslen++; break;
            case "SketchPerimeter": source.VectorSource[0].PerimeterField = clientSettings[x]; _cslen++; break;
            case "SketchAreaField": source.VectorSource[0].AreaField = clientSettings[x]; _cslen++; break;
            case "SketchVectorConnectingField": source.VectorSource[0].ConnectingFields = clientSettings[x] ? clientSettings[x].split(',') : []; _cslen++; break;
        }
    }
    
    if( _cslen == 0) {
	    for (var x in sketchSettings) {
	        switch (x) {
	            case "SketchLabel": source.SketchSource.LabelField = sketchSettings[x]; break;
	            case "SketchCommandField": source.SketchSource.CommandField = sketchSettings[x]; break;
	            case "SketchKeyField": source.SketchSource.KeyFields = sketchSettings[x] ? sketchSettings[x].split(',') : []; break;
	            case "SketchTable": source.SketchSource.Table = sketchSettings[x]; break;
	            case "SketchVectorTable": source.VectorSource[0].Table = sketchSettings[x]; break;
	            case "SketchVectorLabelField": source.VectorSource[0].LabelField = sketchSettings[x]; break;
	            case "SketchVectorCommandField": source.VectorSource[0].CommandField = sketchSettings[x]; break;
	            case "SketchPerimeter": source.VectorSource[0].PerimeterField = sketchSettings[x]; break;
	            case "SketchAreaField": source.VectorSource[0].AreaField = sketchSettings[x]; break;
	            case "SketchVectorConnectingField": source.VectorSource[0].ConnectingFields = sketchSettings[x] ? sketchSettings[x].split(',') : []; break;
	        }
	    }
    }

    config.sources.push(source);

    return config;

}

CAMACloud.Sketching.Configs.PACS80 = {
    formatter: 'TASketch',
    sources: [
        {
            InsertDataOnAddition: true,
            //AllowMultiSegmentAddDelete: true,
            AllowSegmentAddition: true,
            IsLargeLookup: true,
            AllowLabelEdit: true,
            // SketchLabelLookup: 'imprv_det_type',
            SketchSource: { Table: "imprv", LabelField: "imprv_state_cd", CommandField: null, KeyFields: ["imprv_id"] },
            VectorSource: [{ Table: "imprv_detail", LabelField: "imprv_det_type_cd", CommandField: "sketch_cmds", ConnectingFields: ["imprv_id"], AreaField: "sketch_area", PerimeterField: null, LabelLookup: 'imprv_det_type', labelCaption: 'Type' }],
            NotesSource: { Table: "imprv_sketch_note", TextField: "NoteText", PositionXField: "xLocation", PositionYField: "yLocation", LineXField: "xLine", LineYField: "yLine", ConnectingFields: ["imprv_id"], ScaleFactor: 0.01 }
        }
    ]
}

CAMACloud.Sketching.Configs.PACS802 = {
    formatter: 'TASketch',
    sources: [
        {
            InsertDataOnAddition: true,
            //AllowMultiSegmentAddDelete: true,
            AllowSegmentAddition: true,
            IsLargeLookup: true,
            AllowLabelEdit: true,
            //SketchLabelLookup: 'imprv_det_type',
            SketchSource: { Table: "imprv", LabelField: "imprv_state_cd", CommandField: null, KeyFields: ["imprv_id"] },
            VectorSource: [{ Table: "imprv_detail", LabelField: "imprv_det_type_cd", CommandField: "CC_SKETCH", ConnectingFields: ["imprv_id"], AreaField: "sketch_area", PerimeterField: null, LabelLookup: 'imprv_det_type_cd', labelCaption: 'Type' }],
            NotesSource: { Table: "imprv_sketch_note", TextField: "NoteText", PositionXField: "xLocation", PositionYField: "yLocation", LineXField: "xLine", LineYField: "yLine", ConnectingFields: ["imprv_id"], ScaleFactor: 0.01 }
        }
    ]
}

CAMACloud.Sketching.Configs.RapidSketch = {
    formatter: 'RapidSketch',
    AppendSketchPrefixInSVData: true,
    sources: [
        {
            Name: "imprv_detail_sketch",
            Key: "PID",
            SketchLabelPrefix: "Det",
        	SketchLabelPrefixInSv: "Det",
            AllowTransferIn: true,
            InsertDataOnAddition: true,
            SketchLabelLookup: 'imprv_det_type_cd',
            AllowMultiSegmentAddDelete: true,
            AllowSegmentAddition: true,
            AllowLabelEdit: true,
            SketchSource: { Table: "imprv", LabelField: "imprv_state_cd", CommandField: null, KeyFields: ["imprv_id"] },
            VectorSource: [{ Table: "imprv_detail", IdField: "imprv_det_id", LabelField: "imprv_det_type_cd", CommandField: "custom_imprv_detail_sketch", ConnectingFields: ["imprv_id"], AreaField: 'sketch_area', PerimeterField: 'perimeter', LabelLookup: 'imprv_det_type', labelCaption: 'Type' }],
            NotesSource: { Table: "imprv_sketch_note", TextField: "NoteText", PositionXField: "xLocation", PositionYField: "yLocation", ConnectingFields: ["imprv_id"], ScaleFactor: 0.01 }
        },
        {
            Name: "imprv_sketch",
            Key: "PI",
            SketchLabelPrefix: "Imp",
            SketchLabelPrefixInSv: "Imp",
            HideNullSketches: true,
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            AllowLabelEdit: true,
            AllowTransferOut: true,
            SaveAllSegments: true,
            SketchLabelLookup: null,
            SketchLabelLookupX: 'imprv_det_type',
            SketchSource: { Table: "imprv", LabelField: "imprv_state_cd", CommandField: "custom_imprv_sketch", KeyFields: ["imprv_id"],labelCaption: 'Type'  },
            VectorSource: [{ Table: "imprv", IdField: null, LabelField: null, CommandField: "custom_imprv_sketch", ConnectingFields: ["imprv_id"], AreaField: null, PerimeterField: null }]
        },
        {
            Name: "property_sketch",
            Key: "P",
            SketchLabelPrefix: "Prop",
            SketchLabelPrefixInSv: "Prop",
            HideNullSketches: true,
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            AllowLabelEdit: true,
            AllowTransferOut: true,
            SaveAllSegments: true,
            SketchLabelLookup: null,
            SketchLabelLookupX: 'imprv_det_type',
            SketchSource: { Table: null, LabelField: "prop_id", CommandField: "custom_property_sketch", KeyFields: ["prop_id"] },
            VectorSource: [{ Table: null, IdField: null, LabelField: null, CommandField: "custom_property_sketch", ConnectingFields: ["prop_id"], AreaField: null, PerimeterField: null }]
        }
    ]
}
CAMACloud.Sketching.Configs.RapidSketch_TA = {
    formatter: 'RapidSketch',
    AppendSketchPrefixInSVData: true,
    sources: [
        {
            Name: "imprv_detail_sketch",
            InsertDataOnAddition: true,
            //AllowMultiSegmentAddDelete: true,
            AllowSegmentAddition: true,
            Key: "PID",
            SketchLabelPrefix: "Det",
            SketchLabelPrefixInSv: "Det",
            AllowTransferIn: true,
            IsLargeLookup: true,
            AllowLabelEdit: true,
            AllowMultiSegmentAddDelete: true,
            SketchSource: { Table: "imprv", LabelField: "imprv_state_cd", CommandField: null, KeyFields: ["imprv_id"] },
            VectorSource: [{ Table: "imprv_detail", IdField: "imprv_det_id", LabelField: "imprv_det_type_cd", CommandField: "custom_imprv_detail_sketch", ConnectingFields: ["imprv_id"], AreaField: 'sketch_area', PerimeterField: 'perimeter', LabelLookup: 'imprv_det_type', labelCaption: 'Type' }],
            NotesSource: { Table: "imprv_sketch_note", TextField: "NoteText", PositionXField: "xLocation", PositionYField: "yLocation", ConnectingFields: ["imprv_id"], ScaleFactor: 0.01 }
        },
        {
            Name: "imprv_sketch",
            Key: "PI",
            SketchLabelPrefix: "Imp",
            SketchLabelPrefixInSv: "Imp",
            HideNullSketches: true,
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            AllowLabelEdit: true,
            AllowTransferOut: true,
            //IsLargeLookup: true,
            SaveAllSegments: true,
            SketchSource: { Table: "imprv", LabelField: "imprv_state_cd", CommandField: "custom_imprv_sketch", KeyFields: ["imprv_id"] },
            VectorSource: [{ Table: "imprv", IdField: null, LabelField: null, CommandField: "custom_imprv_sketch", ConnectingFields: ["imprv_id"], AreaField: null, PerimeterField: null }]
        },
        {
            Name: "property_sketch",
            Key: "P",
            SketchLabelPrefix: "Prop",
            SketchLabelPrefixInSv: "Prop",
            HideNullSketches: true,
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            AllowLabelEdit: true,
            AllowTransferOut: true,
            //IsLargeLookup: true,
            SaveAllSegments: true,
            SketchSource: { Table: null, LabelField: "prop_id", CommandField: "custom_property_sketch", KeyFields: ["prop_id"] },
            VectorSource: [{ Table: null, IdField: null, LabelField: null, CommandField: "custom_property_sketch", ConnectingFields: ["prop_id"], AreaField: null, PerimeterField: null }]
        }
    ]
}
CAMACloud.Sketching.Configs.RapidSketch2 = {
    formatter: 'RapidSketch2',
    AppendSketchPrefixInSVData: true,
    sources: [
      	{
          	Name: "imprv_detail_sketch",
          	Key: "PID",
			SketchLabelPrefix: "Det",
			SketchLabelPrefixInSv: "Det",
			AllowTransferIn: true,
			//AllowMultiSegmentAddDelete: true,
    		AllowSegmentAddition: true,
			DoNotAllowLabelMove: true,
			InsertDataOnAddition: true,
			DoNotHeaderAddition: true,
			IsLargeLookup: true,
			ShowYearBuilt: true,
			AllowLabelEdit: true,
			SketchLabelLookup: 'imprv_det_type_cd',
			SketchSource: { Table: "imprv", LabelField: "imprv_type_cd/primary_use_cd", CommandField: null, KeyFields: ["imprv_id"] },
			VectorSource: [{ Table: "imprv_detail", IdField: "imprv_det_id", LabelField: "imprv_det_type_cd", CommandField: "CC_SKETCH", ConnectingFields: ["imprv_id"], AreaField: 'sketch_area', PerimeterField: 'perimeter', LabelLookup: 'imprv_det_type_cd', labelCaption: 'Type' }],
			NotesSource: { Table: "imprv_sketch_note", TextField: "NoteText", PositionXField: "xLocation", PositionYField: "yLocation", ConnectingFields: ["imprv_id"], ScaleFactor: 0.01 }
		},
        {
            Name: "imprv_sketch",
            Key: "PI",
            SketchLabelPrefix: "Imp",
            SketchLabelPrefixInSv: "Imp",
            HideNullSketches: true,
            InsertDataOnAddition: true,
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            AllowLabelEdit: true,
            AllowTransferOut: true,
            DoNotAllowLabelMove: true,
            SaveAllSegments: true,
           	SketchLabelLookup: null,
            IsLargeLookup: true,
            SketchLabelLookup: 'imprv_det_type_cd',
            SketchSource: { Table: "imprv", LabelField: "imprv_type_cd/primary_use_cd", CommandField: "CC_SKETCH", KeyFields: ["imprv_id"] },
            VectorSource: [{ Table: "imprv", IdField: null, LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["imprv_id"], AreaField: null, PerimeterField: null, LabelLookup: 'imprv_det_type_cd' }]
        },
        {
            Name: "property_sketch",
            Key: "P",
            SketchLabelPrefix: "Prop",
            SketchLabelPrefixInSv: "Prop",
            HideNullSketches: true,
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            AllowLabelEdit: true,
            AllowTransferOut: true,
            DoNotAllowLabelMove: true,
            SaveAllSegments: true,
            IsLargeLookup: true,
            SketchLabelLookup: 'imprv_det_type_cd',
            SketchSource: { Table: "ccv_property_val_profile", LabelField: "prop_id", CommandField: "CC_SKETCH", KeyFields: ["prop_id"] },
            VectorSource: [{ Table: "ccv_property_val_profile", IdField: null, LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["prop_id"], AreaField: null, PerimeterField: null, LabelLookup: 'imprv_det_type_cd' }]
        }
    ]
}

CAMACloud.Sketching.Configs.RapidSketch2_NFYL = {
    formatter: 'RapidSketch2',
    AppendSketchPrefixInSVData: true,
    sources: [
      	{
          	Name: "imprv_detail_sketch",
          	Key: "PID",
			SketchLabelPrefix: "Det",
			SketchLabelPrefixInSv: "Det",
			AllowTransferIn: true,
			//AllowMultiSegmentAddDelete: true,
    		AllowSegmentAddition: true,
			DoNotAllowLabelMove: true,
			InsertDataOnAddition: true,
			DoNotHeaderAddition: true,
			IsLargeLookup: true,
			ShowYearBuilt: true,
			AllowLabelEdit: true,
			SketchLabelLookup: 'imprv_det_type_cd',
			SketchSource: { Table: "imprv", LabelField: "imprv_type_cd/primary_use_cd", CommandField: null, KeyFields: ["imprv_id"] },
			VectorSource: [{ Table: "imprv_detail", IdField: "imprv_det_id", LabelField: "imprv_det_type_cd", CommandField: "CC_SKETCH", ConnectingFields: ["imprv_id"], AreaField: 'sketch_area', PerimeterField: 'perimeter', LabelLookup: 'imprv_det_type_cd', labelCaption: 'Type' }],
			NotesSource: { Table: "imprv_sketch_note", TextField: "NoteText", PositionXField: "xLocation", PositionYField: "yLocation", ConnectingFields: ["imprv_id"], ScaleFactor: 0.01 }
		},
        {
            Name: "imprv_sketch",
            Key: "PI",
            SketchLabelPrefix: "Imp",
            SketchLabelPrefixInSv: "Imp",
            HideNullSketches: true,
            InsertDataOnAddition: true,
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            AllowLabelEdit: true,
            AllowTransferOut: true,
            DoNotAllowLabelMove: true,
            SaveAllSegments: true,
           	SketchLabelLookup: null,
            IsLargeLookup: true,
            SketchLabelLookup: 'imprv_det_type_cd',
            SketchSource: { Table: "imprv", LabelField: "imprv_type_cd/primary_use_cd", CommandField: "CC_SKETCH", KeyFields: ["imprv_id"] },
            VectorSource: [{ Table: "imprv", IdField: null, LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["imprv_id"], AreaField: null, PerimeterField: null, LabelLookup: 'imprv_det_type_cd' }]
        },
        {
            Name: "property_sketch",
            Key: "P",
            SketchLabelPrefix: "Prop",
            SketchLabelPrefixInSv: "Prop",
            HideNullSketches: true,
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            AllowLabelEdit: true,
            AllowTransferOut: true,
            DoNotAllowLabelMove: true,
            SaveAllSegments: true,
            IsLargeLookup: true,
            SketchLabelLookup: 'imprv_det_type_cd',
            SketchSource: { Table: null, LabelField: "prop_id", CommandField: "CC_SKETCH", KeyFields: ["prop_id"] },
            VectorSource: [{ Table: null, IdField: null, LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["prop_id"], AreaField: null, PerimeterField: null, LabelLookup: 'imprv_det_type_cd' }]
        }
    ]
}

CAMACloud.Sketching.Configs.WinGap = {
    formatter: 'WinGap',
    isShowBoundary: true,
    sources: [
        {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            ClearSegmentOnDeletion: true,
            DeleteBlankSegmentsOnSave: true,
            SketchLabelPrefix: "RES ",
            SketchLabelPrefixInSv: "RES",
            AllowLabelEdit: true,
            InsertDataOnAddition: true,
           // DoNotShowLabelDescription: true,
         //   SketchLabelLookup: 'IMPLABEL_RES',
            SketchSource: { Table: "REPROP", LabelField: "REPROPKEY", CommandField: null, KeyFields: ["REPROPKEY"] },
            VectorSource: [{ Table: "SKETCH_RES", LabelField: "IMPKEY", CommandField: "VERTICES", ConnectingFields: ["REPROPKEY"], AreaField: "AREA", PerimeterField: "PERIMETER", LabelCommandField: "IMPLABEL", DimensionCommandField: "LABELS", LabelLookup: 'IMPLABEL_RES' }]
        },
        {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            ClearSegmentOnDeletion: true,
            DeleteBlankSegmentsOnSave: true,
            SketchLabelPrefix: "COM ",
            SketchLabelPrefixInSv: "COM",
            AllowLabelEdit: true,
            InsertDataOnAddition: true,
           // DoNotShowLabelDescription: true,
          //  SketchLabelLookup: 'IMPLABEL_COMM',
            SketchSource: { Table: "COMMIMP", LabelField: "IMPROV_NO/SECTION_NO", CommandField: null, KeyFields: ["COMMKEY"] },
            VectorSource: [{ Table: "SKETCH_COMM", LabelField: "IMPKEY", CommandField: "VERTICES", ConnectingFields: ["COMMKEY"], AreaField: "AREA", PerimeterField: "PERIMETER", LabelCommandField: "IMPLABEL", DimensionCommandField: "LABELS", LabelLookup: 'IMPLABEL_COMM'  }]
        },
       {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            ClearSegmentOnDeletion: true,
            DeleteBlankSegmentsOnSave: true,
            SketchLabelPrefix: "MH ",
            SketchLabelPrefixInSv: "MH",
            AllowLabelEdit: true,
            InsertDataOnAddition: true,
           // DoNotShowLabelDescription: true,
           // SketchLabelLookup: 'IMPLABEL_MH',
            SketchSource: { Table: "CCV_MOBILE", LabelField: "MOBILEKEY/RECID", CommandField: null, KeyFields: ["MOBILEKEY"] },
            VectorSource: [{ Table: "SKETCH_MH", LabelField: "IMPKEY", CommandField: "VERTICES", ConnectingFields: ["MOBILEKEY"], AreaField: "AREA", PerimeterField: "PERIMETER", LabelCommandField: "IMPLABEL", DimensionCommandField: "LABELS", LabelLookup: 'IMPLABEL_MH' }]
        },
        {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            ClearSegmentOnDeletion: true,
            DeleteBlankSegmentsOnSave: true,
            SketchLabelPrefix: "MHP ",
            SketchLabelPrefixInSv: "MHP",
            AllowLabelEdit: true,
            InsertDataOnAddition: true,
           // DoNotShowLabelDescription: true,
            SketchSource: { Table: "CCV_MOBILE_PREBILL", LabelField: "MOBILEKEY/RECID", CommandField: null, KeyFields: ["MOBILEKEY"] },
            VectorSource: [{ Table: "SKETCH_MH_PREBILL", LabelField: "IMPKEY", CommandField: "VERTICES", ConnectingFields: ["MOBILEKEY"], AreaField: "AREA", PerimeterField: "PERIMETER", LabelCommandField: "IMPLABEL", DimensionCommandField: "LABELS", LabelLookup: 'IMPLABEL_MH' }]
        }
    ]
}

CAMACloud.Sketching.Configs.Sigma = {
    formatter: "Sigma2",
    sources: [
        {
            AllowSegmentAddition: true,
            SketchSource: { Table: "residence", LabelField: "bldg_style", CommandField: "bldg_style", KeyFields: ["recid1"] },
            VectorSource: [{ Table: "residence", LabelField: "bldg_style", CommandField: "sketch", AreaField: null, PerimeterField: null, ConnectingFields: ["recid1"] }],
            LabelAreaFields: {
                "LM": "main_fn_area",
                "LU": "uppr_fn_area",
                "FA": "addl_fn_area",
                "UNF": "unfin_area",
                "B": "bsmt_area",
                "BF": "fn_bsmt_area",
                "S": "att_storg_sf",
                "AC": "carport_sqft",
                "AG": "att_gar_sqft",
                "BIG": "bltin_garage",
                "BZ": "breezeway_sf",
                "CP": "cov_porch_sf",
                "EP": "enc_porch_sf",
                "OP": "opn_porch_sf",
                "SP": "scr_porch_sf",
                "WD": "wood_deck_sf",
                "GP": "gls_porch_sf",
                "X": "attic_area",
                "PAT": "patio_sf",
                "COV": "cover_sf",
                "FAT": "fin_attic_ar"


            },
            AreaSummaryFields: {
                "tot_sqf_l_area": ["LM", "LU"]
            }
        }
    ]
}

CAMACloud.Sketching.Configs.SigmaCuyahoga = {
    formatter: "Sigma2",
    splitLabelAreaFields: true,
    enableManualAreaEntry: true,
    clearModifiedAfterSave: true,
    sources: [
        {
            AllowSegmentAddition: true,
            splitByDelimiter: '/',
            SketchSource: { Table: "RESIDENTIAL_BUILDING", LabelField: "STYLE", CommandField: null, KeyFields: ["recid1"] },
            VectorSource: [{ Table: "RESIDENTIAL_BUILDING", LabelField: "STYLE", CommandField: "SKETCH", AreaField: null, PerimeterField: null, ConnectingFields: ["recid1"], LabelLookup: 'sketch_label_lookup' }],
            LabelAreaFields: {
                "L1": "LIV_AREA_1",
                "L2": "LIV_AREA_2",
                "LU": "LIV_AREA_UP",
                "V": "LIV_AREA_1",
                "V1": "LIV_AREA_1",
                "W1": "LIV_AREA_1",
                "W2": "LIV_AREA_2",
                "WU": "LIV_AREA_UP",
                "V2": "LIV_AREA_2",
                "VU": "LIV_AREA_UP",
                "B": "BSMT_SQ_FT",
                "R": "BSMT_FINISHD",
                "Q": "LIV_AR_BSMT",
                /*"G": "GARAGE_SIZE",
                "N": "GARAGE_SIZE",
                "D": "GARAGE_SIZE",
                "I": "GARAGE_SIZE",
                "T": "TER_DECKING",
                "P": "OPEN_PORCH",
                "E": "ENCLOSD_PRCH"*/
            },
            LabelAreaGrades: {
                "L1": "A", "L2": "A", "LU": "A", "V": "A", "V1": "A", "W1": "A", "W2": "A", "WU": "A", "V2": "A", "VU": "A",
                "B": "B", "R": "B", "Q": "B", "G": "A", "N": "B", "D": "A", "I": "A", "T": "A", "P": "A", "E": "A"
            },
            AreaSummaryFields: {
                "LIV_AREA_TOT": ["L1", "L2", "LU", "Q", "V", "V1", "W1", "W2", "WU", "V2", "VU"]
            }
        }
    ],
    AfterSave: SigmaCuyahogaAfterSave
}

CAMACloud.Sketching.Configs.Stark = {
    formatter: 'Apex',
    ApexAutoSubtract: true,
    sources: [
        {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            AllowLabelEdit: true,
           // SketchLabelLookup: 'BLDG_TYPE_and0Val',
            DoNotShowLabelDescription: true,
            LabelDelimiter: '',
            addCalculatedType: true,
            SketchSource: { Table: 'BUILDING', LabelField: "BUILDING_ID/BUILDING_TYPE", CommandField: null, KeyFields: ["BUILDING_ID"] },
            VectorSource: [{ Table: "BUILDING", LabelField: "BUILDING_TYPE", CommandField: "CC_SKETCH", ConnectingFields: ["BUILDING_ID"], AreaField: null, PerimeterField: null, labelCaption: 'Building Type', IsLabelRequired: false, GetLabelValueFromField: true, HideLabelFromShow: true, ShowCurrentValueOnly: true, LabelLookup: 'BLDG_TYPE_and0Val', ExtraLabelFields: [{ Name: null, LookUp: "SKETCH_CODES_RES", Target: 'name', Caption: 'Addition Type', requiredAny: true, DoNotShowLabelDescription: true }] }]
        }
    ]
}
CAMACloud.Sketching.Configs.FTC = {
    formatter: 'Apex',
    DimensionFormatter : function(ln) {return Math.round(ln);},
    ApexEditSketchLabel: true,  //only enable if no other extrafield or splitbydelimter property .
	EnableSpecialRounding: true,    
    sources: [
        {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            AllowLabelEdit: true,
        	AssignLookUpNameAsLabel: true,
            //SketchLabelLookup: 'AREA_CODES',
        	IsLargeLookup: true,
        	addCalculatedType:true,
            SketchSource: { Table: 'IMPAPEXOBJECT', LabelField: "IMPID/IMPNO", CommandField: null, KeyFields: ["IMPID"], sketchSourceSorting: "IMPNO ASC" },
            VectorSource: [{ Table: "IMPAPEXOBJECT", LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["IMPID"], AreaField: null, PerimeterField: null, LabelTarget: "code", IsLabelRequired: true, LabelLookup: 'AREA_CODES', ExtraLabelFields: [{ Name: null, Target: 'name', Caption: 'Label'}] }]
        }
    ],
    AssociateVector: { 
    	source: [
    		{ sourceTable: 'IMPBUILTASFLOOR', fields: 'IMPBUILTASFLOOR.APEXID/IMPBUILTASFLOOR.ROWUID, IMPBUILTASFLOOR.IMPSFLOORDESCRIPTION-IMPBUILTAS.BLTASSF', headers: 'ApexId/ROWUID,Description-Floor SF'}, 
    		{ sourceTable: 'CCV_IMPDETAIL_ATTACHED_DETACHED', fields: 'CCV_IMPDETAIL_ATTACHED_DETACHED.APEXID/CCV_IMPDETAIL_ATTACHED_DETACHED.ROWUID, CCV_IMPDETAIL_ATTACHED_DETACHED.IMPDETAILTYPE-CCV_IMPDETAIL_ATTACHED_DETACHED.IMPDETAILDESCRIPTION-CCV_IMPDETAIL_ATTACHED_DETACHED.DETAILUNITCOUNT', headers: 'ApexId/ROWUID,Detail Type-Description-Units'}, 
    		{ sourceTable: 'CCV_IMPDETAIL_ADD_ONS', fields: 'CCV_IMPDETAIL_ADD_ONS.APEXID/CCV_IMPDETAIL_ADD_ONS.ROWUID, CCV_IMPDETAIL_ADD_ONS.IMPDETAILTYPE-CCV_IMPDETAIL_ADD_ONS.IMPDETAILDESCRIPTION-CCV_IMPDETAIL_ADD_ONS.DETAILUNITCOUNT', headers: 'ApexId/ROWUID,Detail Type-Detail Description-Units'}
		], 
		isConnected: true,
		requiredFieldValidation: false,
		autoSelectIfSingle: false,
		labelNoReplace: true,
		isEditedAndCurrentSketchOnly: true,
		autoSelectFunction: FTC_associate_autoSelect,
		vectorListConfig: FTC_associate_vector_config,
		note: "Associate each sketch to an existing CAMA record below. If no CAMA record is chosen, one will be created based on the sketch details."
	},
    AfterSave: FTCSketchAfterSave
}
CAMACloud.Sketching.Configs.Apex = {
    formatter: 'Apex',
    ApexAutoSubtract: true,
	ApexEditSketchLabel: true,
    sources: [
        {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            AllowLabelEdit: true,
            SketchSource: { Table: 'Building_Sketch', LabelField: "BUILDING_NUMBER", CommandField: null, KeyFields: ["BUILDING_NUMBER"] },
            VectorSource: [{ Table: "Building_Sketch", LabelField: "BUILDING_NUMBER", CommandField: "SKETCH", ConnectingFields: ["BUILDING_NUMBER"], AreaField: null, PerimeterField: null, ExtraLabelFields: [{ Name: null, Target: 'name', Caption: 'Label'}] }]
        }
    ]
}

CAMACloud.Sketching.Configs.Bristol = {
    formatter: 'Apex',
    ApexAutoSubtract: true,
    sources: [
        {
            LookUpQuery: "SELECT AREA_TYPE Id, AREA_TYPE || ' - ' || DSC_DESC Name FROM AREACODE INNER JOIN DESCRIPT ON AREA_TYPE = DSC_CODE WHERE MDL= '{CNS_MDL}' AND DSC_TYPE = 'sat'",
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            AllowLabelEdit: true,
			splitByDelimiter:'/',
			addCalculatedType: true,
            SketchSource: { Table: 'CCV_BLDG_CONSTR', LabelField: "BID/BLDG_NUM", CommandField: null, KeyFields: ["BID"] },
            VectorSource: [{ Table: "CCV_BLDG_CONSTR", LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["BID"], AreaField: null, PerimeterField: null }]
        }
    ],
    AfterSave: HennepinSketchAfterSave
}

CAMACloud.Sketching.Configs.Vision6_5 = {
    formatter: 'Apex',
    ShowSectNum: true,
    isBristolTable: true,
    ApexAutoSubtract: true,
    sources: [
        {
            LookUpQuery: "SELECT AREA_TYPE Id, AREA_TYPE || ' - ' || DSC_DESC Name FROM AREACODE INNER JOIN DESCRIPT ON AREA_TYPE = DSC_CODE WHERE MDL= '{CNS_MDL}' AND DSC_TYPE = 'sat'",
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            AllowLabelEdit: true,
			splitByDelimiter:'/',
			AllowSectNum:true,
			addCalculatedType: true,
            SketchSource: { Table: 'CCV_BLDG_CONSTR', LabelField: "BID/BLDG_NUM", CommandField: null, KeyFields: ["BID"] },
            VectorSource: [{ Table: "CCV_BLDG_CONSTR", LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["BID"], AreaField: null, PerimeterField: null }]
        }
    ],
    AfterSave: Vision6_5SketchAfterSave
}

CAMACloud.Sketching.Configs.Hennepin = {
    formatter: 'Apex',
    ApexAutoSubtract: true,
	ApexEditSketchLabel: true,
    sources: [
        {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            AllowLabelEdit: true,
            addCalculatedType: true,
         	//SketchLabelLookup: 'Apex_Area_Codes',
            SketchSource: { Table: 'CCV_BLDG_CONSTR', LabelField: "BID/BLD_BLDG_NAME", CommandField: null, KeyFields: ["BID"] },
            VectorSource: [{ Table: "CCV_BLDG_CONSTR", LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["BID"], AreaField: null, PerimeterField: null, LabelTarget: "code", LabelLookup: 'Apex_Area_Codes', ExtraLabelFields: [{ Name: null, Target: 'name', Caption: 'Label'}] }]
        }
    ],
    AfterSave: HennepinSketchAfterSave,
    BeforeSave: HennepinSketchBeforeSave
}

CAMACloud.Sketching.Configs.Vision7_5XML = {
    formatter: 'Vision',
    sources: [
        {
        	//LookUpQuery: "SELECT AREA_TYPE Id, AREA_TYPE || ' - ' || DSC_DESC Name FROM AREACODE INNER JOIN DESCRIPT ON AREA_TYPE = DSC_CODE WHERE MDL= '{CNS_MDL}' AND DSC_TYPE = 'sat'",
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            AllowLabelEdit: true,
            splitByDelimiter: '/',
            DrawPlaceHolder: true,
            CustomToolForVector: "SQFT,fixedArea",
            SketchSource: { Table: 'CCV_BLDG_CONSTR', LabelField: "BID/BLD_BLDG_NAME", CommandField: null, KeyFields: ["BID"] },
            VectorSource: [{ Table: "CCV_BLDG_CONSTR", LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["BID"], AreaField: null, PerimeterField: null, LabelTarget: "code", LabelLookup: 'SUB_CODE'}]
        }
    ],
    AfterSave: Vision75ApexSketchAfterSave
}

CAMACloud.Sketching.Configs.Vision7_5XMLNew = {
    formatter: 'Vision',
    ShowSectNum: true,
    isShowBoundary: true,
    CreateAssociateWindow: visionDeleteWindowDefinition,
    DeleteAssociateWindow: visionDeleteWindowDefinition,
    ManualSection: true,
    sources: [
        {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            AllowLabelEdit: true,
            splitByDelimiter:'/',
            AllowSectNum:true,
            AllowSectNum:true,
            CustomToolForVector: "SQFT,fixedArea",
            DrawPlaceHolder: true,
            SketchSource: { Table: 'CCV_BLDG_CONSTR', LabelField: "BID/BLD_BLDG_NAME", CommandField: null, KeyFields: ["BID"] },
            VectorSource: [{ Table: "CCV_BLDG_CONSTR", LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["BID"], AreaField: null, PerimeterField: null, LabelTarget: "code", LabelLookup: 'SUB_CODE'}]
        }
    ],
    AfterSave: Vision75ApexSketchAfterSaveNew
}

CAMACloud.Sketching.Configs.VisionXML = {
    formatter: 'Vision',
    ShowSectNum: true,
    isShowBoundary: true,
    CreateAssociateWindow: visionDeleteWindowDefinition,
    DeleteAssociateWindow: visionDeleteWindowDefinition,
    ManualSection: true,
    sources: [
        {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            AllowLabelEdit: true,
            splitByDelimiter: '/',
            AllowSectNum: true,
            AllowSectNum: true,
            CustomToolForVector: "SQFT,fixedArea",
            DrawPlaceHolder: true,
            SketchSource: { Table: 'CCV_BLDG_CONSTR', LabelField: "BID/BLD_BLDG_NAME", CommandField: null, KeyFields: ["BID"] },
            VectorSource: [{ Table: "CCV_BLDG_CONSTR", LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["BID"], AreaField: null, PerimeterField: null, LabelTarget: "code", LabelLookup: 'SUB_CODE' }]
        }
    ],
    AfterSave: Vision75ApexSketchAfterSaveNew
}

CAMACloud.Sketching.Configs.Vision8XML = { 
    formatter: 'Vision',
    EnableVisionNotes: true,
    AddNewNote: true,
    CustomNotePopup: true,
    sources: [
        {
        	//LookUpQuery: "SELECT AREA_TYPE Id, AREA_TYPE || ' - ' || DSC_DESC Name FROM AREACODE INNER JOIN DESCRIPT ON AREA_TYPE = DSC_CODE WHERE MDL= '{CNS_MDL}' AND DSC_TYPE = 'sat'",
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            AllowLabelEdit: true,
            splitByDelimiter: '/',
            DrawPlaceHolder: true,
            CustomToolForVector: "SQFT,fixedArea",
            SketchSource: { Table: 'CCV_BLDG_CONSTR', LabelField: "BID/CNS_OCC/CNS_STYLE", CommandField: null, KeyFields: ["BID"] },
            VectorSource: [{ Table: "CCV_BLDG_CONSTR", LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["BID"], AreaField: null, PerimeterField: null, LabelTarget: "code", LabelLookup: 'SUB_CODE'}]
        }
    ],
    AfterSave: Vision8ApexSketchAfterSave
}

CAMACloud.Sketching.Configs.Vision8XMLNew = { 
    formatter: 'Vision',
    ShowSectNum: true,
    isBristolTable: true,
    isShowBoundary: true,
    EnableVisionNotes: true,
    AddNewNote: true,
    CustomNotePopup: true,
    sources: [
        {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            AllowLabelEdit: true,
            splitByDelimiter:'/',
            AllowSectNum: true,
            DrawPlaceHolder: true,
            CustomToolForVector: "SQFT,fixedArea",
            SketchSource: { Table: 'CCV_BLDG_CONSTR', LabelField: "BID/CNS_OCC/CNS_STYLE", CommandField: null, KeyFields: ["BID"] },
            VectorSource: [{ Table: "CCV_BLDG_CONSTR", LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["BID"], AreaField: null, PerimeterField: null, LabelTarget: "code", LabelLookup: 'SUB_CODE'}]
        }
    ],
    AfterSave: Vision8ApexSketchAfterSaveNew
}

CAMACloud.Sketching.Configs.ApexCart = {
    formatter: 'Apex',
    ApexAutoSubtract: true,
    sources: [
        {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            InsertDataOnAddition: true,
            AllowLabelEdit: true,
            DoNotAllowLabelMove: false,
            LabelDelimiter: '',
            addCalculatedType: true,
            SketchSource: { Table: 'CAMBL', LabelField: "BLBVAL", CommandField: null, KeyFields: ["BTLINE"] },
            VectorSource: [{ Table: "CAMBL", LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["BTLINE"], AreaField: null, HideLabelFromEdit: true, HideLabelFromShow: true, PerimeterField: null, ExtraLabelFields: [{ Name: null, LookUp: "AREA_CODE", Target: 'code', Caption: 'Area Code', HideLabelFromShow: true, IsRequired: true }, { Name: null, LookUp: "CAMSUBA_distinct", Target: 'name', ValueRegx: '^.{0,3}', Caption: 'Sub-Area', DoNotShowLabelDescription: true, IsLargeLookup: true, IsRequired: true }, { Name: null, LookUp: null, Target: 'name', ValueRegx: '....$', Caption: 'Year', ValidationRegx: /^[12][0-9]{3}$/ }] }]
        }
    ],
    AfterSave: ApexCartSketchAfterSave
}

CAMACloud.Sketching.Configs.CustomApex = {
    formatter: 'Apex',
    ApexAutoSubtract: true,
	ApexEditSketchLabel: true,
    sources: [
    {
        AllowSegmentAddition: true,
        AllowSegmentDeletion: true,
        InsertDataOnAddition: true,
        AllowLabelEdit: true,
        addCalculatedType: true,
        SketchSource: { Table: 'prs_residential', LabelField: "property_key", CommandField: null, KeyFields: ["property_key"] },
        VectorSource: [{ Table: "prs_residential", LabelField: "structure_id", CommandField: "sketch", ConnectingFields: ["property_key"], AreaField: null, PerimeterField: null, ExtraLabelFields: [{ Name: null, Target: 'name', Caption: 'Label'}]  }]
    }
    ]
}
CAMACloud.Sketching.Configs.Baldwin = {
    formatter: 'Apex',
    //ApexAutoSubtract: true,
    sources: [{
        AllowSegmentAddition: true,
        AllowSegmentDeletion: true,
        InsertDataOnAddition: true,
        AllowLabelEdit: true,
      //  SketchLabelLookup: 'Apex_Area_Codes',
        AssignLookUpNameAsLabel: true,
        IsLargeLookup: true,
        //addCalculatedType: true,
        SketchSource: { Table: 'CCV_BLDG_MASTER', LabelField: "PPIN/NUMBER", CommandField: null, KeyFields: ["PPIN"] },
        VectorSource: [{ Table: "CCV_BLDG_MASTER", LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["PPIN"], AreaField: null, PerimeterField: null, LabelTarget: "code" , labelCaption:"Code" ,HideLabelFromShow: true, LabelLookup: 'Apex_Area_Codes' , ExtraLabelFields: [{ Name: null, Target: 'name', Caption: 'Label'}] }]
    }],
    AssociateVector: { source: [{ sourceTable: 'APDIM301', category: 'Building', fields: 'APDIM301.CARD_LINE,APDIM301.STRUCTURE_DESCRIPTION', headers: '# on Card,Structure Description'}], isConnected: true, requiredFieldValidation: true, autoSelectIfSingle: true },
    AfterSave: BaldwinSketchAfterSave
}

CAMACloud.Sketching.Configs.Delta = {
    formatter: 'Apex',
    //ApexAutoSubtract: true,
    sources: [{
        AllowSegmentAddition: true,
        AllowSegmentDeletion: true,
        InsertDataOnAddition: true,
        AllowLabelEdit: true,
        Specific_Identifier_00_Code: true,
      //  SketchLabelLookup: 'Apex_Area_Codes',
        AssignLookUpNameAsLabel: true,
        IsLargeLookup: true,
        //addCalculatedType: true,
        SketchSource: { Table: 'CCV_BLDG_MASTER', LabelField: "PPIN/NUMBER", CommandField: null, KeyFields: ["PPIN"] },
        VectorSource: [{ Table: "CCV_BLDG_MASTER", LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["PPIN"], AreaField: null, PerimeterField: null, LabelTarget: "code" , labelCaption:"Code" ,HideLabelFromShow: true, LabelLookup: 'Apex_Area_Codes' , ExtraLabelFields: [{ Name: null, Target: 'name', Caption: 'Label'}] }]
    }],
    AssociateVector: { source: [{ sourceTable: 'APDIM301', category: 'Building', fields: 'APDIM301.CARD_LINE,APDIM301.STRUCTURE_DESCRIPTION', headers: '# on Card,Structure Description'}], isConnected: true, requiredFieldValidation: true, autoSelectIfSingle: true },
    AfterSave: BaldwinSketchAfterSave
}

CAMACloud.Sketching.Configs.StLouis = {
    formatter: 'Apex',
    DimensionFormatter : function(ln) {return Math.round(ln);},
    ApexAutoSubtract: true,
	ApexEditSketchLabel: true,
    sources: [
    {
        AllowSegmentAddition: true,
        AllowSegmentDeletion: true,
        InsertDataOnAddition: true,
        AllowLabelEdit: true,
        IsLargeLookup: true,
        addCalculatedType: true,
         AssignLookUpNameAsLabel: true,
       // SketchLabelLookup: 'Apex_Area_Codes',
        SketchSource: { Table: null, LabelField: "PARCELNBR", CommandField: null, KeyFields: ["PARCELNBR"] },
        VectorSource: [{ Table: null, LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["PARCELNBR"], AreaField: null, PerimeterField: null, LabelTarget: "code", LabelLookup: 'Apex_Area_Codes', ExtraLabelFields: [{ Name: null, Target: 'name', Caption: 'Label'}] }]
    }
    ]
}

CAMACloud.Sketching.Configs.LexurSVG = {
    formatter: 'CCSketch',
    sources: [
    {
        AllowSegmentAddition: true,
        AllowSegmentDeletion: true,
        InsertDataOnAddition: true,
        DoNotAllowOpenSegments: false,
        AllowLabelEdit: true,
        SketchLabelPrefix: "COM ",
        SketchLabelPrefixInSv: "COM",
        SketchSource: { Table: 'BuildingSection', LabelField: "CardNumber", CommandField: null, KeyFields: ["ROWUID"] },
        VectorSource: [{ Table: "BuildingSection", LabelField: null, CommandField: "SKETCH", ConnectingFields: ["ROWUID"], AreaField: null, PerimeterField: null }],
        NotesSource: { Table: "Building_SketchNotes", TextField: "SKETCHNOTE", PositionXField: "SKETCHNOTE_X", PositionYField: "SKETCHNOTE_Y", ConnectingFields: ["PropertyNumber", "CardNumber", "BuildingSectionId"], ScaleFactor: 0.01 }
    },
    {
        AllowSegmentAddition: true,
        AllowSegmentDeletion: true,
        InsertDataOnAddition: true,
        DoNotAllowOpenSegments: false,
        AllowLabelEdit: true,
        SketchLabelPrefix: "RES ",
        SketchLabelPrefixInSv: "RES",
        SketchSource: { Table: 'Dwelling', LabelField: "CardNumber", CommandField: null, KeyFields: ["ROWUID"] },
        VectorSource: [{ Table: "Dwelling", LabelField: null, CommandField: "SKETCH", ConnectingFields: ["ROWUID"], AreaField: null, PerimeterField: null }],
        NotesSource: { Table: "Dwelling_SketchNotes", TextField: "SKETCHNOTE", PositionXField: "SKETCHNOTE_X", PositionYField: "SKETCHNOTE_Y", ConnectingFields: ["PropertyNumber", "CardNumber"], ScaleFactor: 0.01 }
    }
    ]
}
CAMACloud.Sketching.Configs.LexurSVGWilliams = {
    formatter: 'CCSketch',
    sources: [
    {
        AllowSegmentAddition: true,
        AllowSegmentDeletion: true,
        InsertDataOnAddition: true,
        DoNotAllowOpenSegments: false,
        AllowLabelEdit: true,
        SketchLabelPrefix: "COM ",
        SketchLabelPrefixInSv: "COM",
        SketchSource: { Table: 'Building', LabelField: "CardNumber", CommandField: null, KeyFields: ["ROWUID"] },
        VectorSource: [{ Table: "Building", LabelField: null, CommandField: "SKETCH", ConnectingFields: ["ROWUID"], AreaField: null, PerimeterField: null }],
        NotesSource: { Table: "Building_SketchNotes", TextField: "SKETCHNOTE", PositionXField: "SKETCHNOTE_X", PositionYField: "SKETCHNOTE_Y", ConnectingFields: ["PropertyNumber", "CardNumber", "BuildingSectionId"], ScaleFactor: 0.01 }
    },
    {
        AllowSegmentAddition: true,
        AllowSegmentDeletion: true,
        InsertDataOnAddition: true,
        DoNotAllowOpenSegments: false,
        AllowLabelEdit: true,
        SketchLabelPrefix: "RES ",
        SketchLabelPrefixInSv: "RES",
        SketchSource: { Table: 'Dwelling', LabelField: "CardNumber", CommandField: null, KeyFields: ["ROWUID"] },
        VectorSource: [{ Table: "Dwelling", LabelField: null, CommandField: "SKETCH", ConnectingFields: ["ROWUID"], AreaField: null, PerimeterField: null }],
        NotesSource: { Table: "Dwelling_SketchNotes", TextField: "SKETCHNOTE", PositionXField: "SKETCHNOTE_X", PositionYField: "SKETCHNOTE_Y", ConnectingFields: ["PropertyNumber", "CardNumber"], ScaleFactor: 0.01 }
    }
    ]
}
CAMACloud.Sketching.Configs.WarrenOH = {
    formatter: 'WarrenOH',
    sources: [
        {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            ClearSegmentOnDeletion: true,
            DeleteBlankSegmentsOnSave: true,
            SketchLabelPrefix: "COM ",
            SketchLabelPrefixInSv: "COM",
            AllowLabelEdit: true,
            InsertDataOnAddition: true,
           // SketchLabelLookup: 'Sketch_VL',
            SketchSource: { Table: "MAF42", LabelField: "BLDGID", CommandField: null, KeyFields: ["ACCT"] },
            VectorSource: [{ Table: "CCV_56SKCM", LabelField: "SKLABL", CommandField: "SKVCTR", ConnectingFields: ["ACCT"], AreaField: "SKSQFT", PerimeterField: null, LabelCommandField: null, DimensionCommandField: null, LabelLookup: 'Sketch_VL', ExtraLabelFields: [{ Name: "SKCNST", LookUp: "SKCNST", IsRequired: true }] }]
        },
        {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            ClearSegmentOnDeletion: true,
            DeleteBlankSegmentsOnSave: true,
            SketchLabelPrefix: "RES ",
            SketchLabelPrefixInSv: "RES",
            AllowLabelEdit: true,
            InsertDataOnAddition: true,
          //  SketchLabelLookup: 'Sketch_VL',
            SketchSource: { Table: "MAF43", LabelField: "ACCT", CommandField: null, KeyFields: ["ACCT"] },
            VectorSource: [{ Table: "CCV_56SKRS", LabelField: "SKLABL", CommandField: "SKVCTR", ConnectingFields: ["ACCT"], AreaField: "SKSQFT", PerimeterField: null, LabelCommandField: null, DimensionCommandField: null, LabelLookup: 'Sketch_VL', ExtraLabelFields: [{ Name: "SKCNST", LookUp: "SKCNST", IsRequired: true }] }]
        }
    ]
}
CAMACloud.Sketching.Configs.Fulton = {
    formatter: 'Lucas',
    sources: [
        {
            Key: "RES",
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            ClearSegmentOnDeletion: true,
            DeleteBlankSegmentsOnSave: true,
            SketchLabelPrefix: "RES ",
            SketchLabelPrefixInSv: "RES",
            AllowLabelEdit: true,
            InsertDataOnAddition: true,
            ShowVectorDetails: true,
           // SketchLabelLookup: 'OKLOW',
            sectionFilterField: 'CARD',
            SketchSource: { Table: "DWELDAT", LabelField: "CARD/YRBLT", CommandField: null, KeyFields: ["PARID"] },
            VectorSource: [{ Table: "ADDN", LabelField: "LOWER", CommandField: "VECT", ConnectingFields: ["PARID"], AreaField: "AREA", PerimeterField: null, LabelCommandField: null, DimensionCommandField: null,LabelLookup: 'OKLOW', ExtraLabelFields: [{ Name: "FIRST", LookUp: "OK1" }, { Name: "SECOND", LookUp: "OK2" }, { Name: "THIRD", LookUp: "OK3" }] }]
        },
        {
            Key: "COM",
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            ClearSegmentOnDeletion: true,
            DeleteBlankSegmentsOnSave: true,
            SketchLabelPrefix: "COM ",
            SketchLabelPrefixInSv: "COM",
            AllowLabelEdit: true,
            InsertDataOnAddition: true,
            // SketchLabelLookup: 'Sketch_VL',
            sectionFilterField: 'CARD',
            SketchSource: { Table: "COMDAT", LabelField: "PARID", CommandField: null, KeyFields: ["PARID"] },
            VectorSource: [{ Table: "COMINTEXT", LabelField: "SF", CommandField: "VECT", ConnectingFields: ["PARID"], AreaField: "AREA", PerimeterField: null, LabelCommandField: null, DimensionCommandField: null }]
        }
    ]
}

CAMACloud.Sketching.Configs.LucasDTR = {
    formatter: 'Lucas',
    LabelAssociateWindow: LucasLabelWindowDefinition,
    sources: [
        {
            Key: "RES",
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            ClearSegmentOnDeletion: true,
            DeleteBlankSegmentsOnSave: true,
            SketchLabelPrefix: "RES -",
            SketchLabelPrefixInSv: "RES",
            AllowLabelEdit: true,
            InsertDataOnAddition: true,
            ShowVectorDetails: true,
          //  SketchLabelLookup: 'OKLOW',
            DoNotAllowLabelMove: true,
            sectionFilterField: 'CARD',
			EnableMarkedAreaDrawing: true,            
            DoNotAddOrEditFirstRecordlabel: true,
            FirstRecordlabelValue:"STORIES/BSMT",
            SaveExtraLabelFields: true,
            SketchSource: { Table: "DWELDAT", LabelField: "CARD/STORIES/YRBLT", CommandField: null, KeyFields: ["CARD"] },
            VectorSource: [{ Table: "ADDN", LabelField: "LOWER", labelCaption: "Lower", CommandField: "VECT", ConnectingFields: ["CARD"], AreaField: "AREA", PerimeterField: null, LabelCommandField: null, IsLabelRequired: false, DimensionCommandField: null, LabelLookup: 'OKLOW/LOWER', ExtraLabelFields: [{ Name: "FIRST", LookUp: "OK1/FIRST", Caption: "First", IsRequired: false }, { Name: "SECOND", Caption: "Second", LookUp: "OK2/SECOND", IsRequired: false }, { Name: "THIRD", Caption: "Third", LookUp: "OK3/THIRD", IsRequired: false }] }]
        },
        {
            Key: "COM",
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            ClearSegmentOnDeletion: true,
            DeleteBlankSegmentsOnSave: true,
            SketchLabelPrefix: "COM -",
            SketchLabelPrefixInSv: "COM",
            AllowLabelEdit: true,
            InsertDataOnAddition: true,
            DoNotAllowLabelMove: true,
            EnableMarkedAreaDrawing: true,
            // SketchLabelLookup: 'Sketch_VL',
            sectionFilterField: 'CARD',
            SaveExtraLabelFields: true,
            SketchSource: { Table: "COMDAT", LabelField: "CARD/BLDNUM/YRBLT", CommandField: null, KeyFields: ["CARD"] },
            VectorSource: [{ Table: "COMINTEXT", LabelField: "SECT", labelCaption: "Sect", CommandField: "VECT", ConnectingFields: ["CARD"], AreaField: "AREA", PerimeterField: 'PERIM', LabelCommandField: null, IsLabelRequired: false, DimensionCommandField: null, EnableFieldValidation: { sourceTable: 'COMINTEXT' }, ExtraLabelFields: [{ Name: "USETYPE", Caption: "UseType", LookUp: "USETYPE", IsRequired: false }, { Name: "FLRFROM", LookUp: null, IsRequired: false,Caption: 'From', EnableFieldValidation: { sourceTable: 'COMINTEXT' } }, { Name: "FLRTO", LookUp: null, IsRequired: false,Caption: 'To', EnableFieldValidation: { sourceTable: 'COMINTEXT' }  }] },
                { Table: "COMFEAT", LabelField: "STRUCT", labelCaption: "Struct", CommandField: "VECT", ConnectingFields: ["CARD"], AreaField: "AREA", PerimeterField: null, LabelCommandField: null, IsLabelRequired: false, DimensionCommandField: null, LabelLookup : "STRUCT" }
            ]
        }, {
            Key: "OBY",
            SketchLabelPrefix: "OBY -",
            SketchLabelPrefixInSv: "OBY",
            AllowLabelEdit: true,
            InsertDataOnAddition: true,
            SketchLabelLookup: 'CODE',
            DoNotAllowLabelMove: true,
            EnableMarkedAreaDrawing: true,
            EnableOBYEditlabel: true,
            SketchSource: { Table: null, LabelField: "PARID", CommandField: null, KeyFields: ["PARID"] },
            VectorSource: [{ Table: "OBY", LabelField: "CODE", labelCaption: "Code", CommandField: "VECT", ConnectingFields: ["PARID"], AreaField: "AREA", PerimeterField: null, LabelCommandField: null, IsLabelRequired: false, DimensionCommandField: null, LabelLookup: 'CODE', LabelLookupSelector: { Table: 'OBY', FieldName: 'CODE' } }]
        }

    ],
    BeforeSave: LucasDTRSketchBeforeSave,
    AfterSave: LucasDTRAfterSave
}

CAMACloud.Sketching.Configs.ClarkKY = {
    formatter: 'Apex',
    ApexAutoSubtract: true,
	ApexEditSketchLabel: true,
    sources: [
        {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            AllowLabelEdit: true,
           	// SketchLabelLookup: 'FeatureCodes_DCS',
           	addCalculatedType: true,
           	SketchLabelPrefixInSv: "RES",
           	AreaSumFields: {
				'BASEMENT_SQFT': ['B'], 
				'GARAGE_SQFT': ['BG', 'G', 'CP', 'Storage', 'Wrhs', 'Barn'], 
				'PATIO_DECK_SQFT': ['Deck', 'Patio'],
				'LIVING_SQFT': ['GF', '(1/2 S)', '(3/4 S)', '(1 S)', '(1 1/2 S)', '(1 3/4 S)', '(2 S)', '(2 1/2 S)', '(3 S)'],
				'PORCH_SQFT': ['Porch'],
				'POOL_SIZE': ['Pool']
			},
            SketchSource: { Table: 'tblResidential', LabelField: "YR_BUILT/STRUCT_TYPE", CommandField: null, KeyFields: ["RES_ID"] },
            VectorSource: [{ Table: "tblResidential", LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: [], AreaField: null, PerimeterField: null, LabelLookup: 'FeatureCodes_DCS', ExtraLabelFields: [{ Name: null, Target: 'name', Caption: 'Label'}] }]
        },
        {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            AllowLabelEdit: true,
        	//SketchLabelLookup: 'FeatureCodes_DCS',
        	addCalculatedType: true,
        	SketchLabelPrefixInSv: "COM",
            AreaSumFields: {
				'BASEMENT_SQFT': ['B'], 
				'OFFICE_SQFT': ['Office'], 
				'MANUF_SQFT': ['Manuf'],
				'AREA': ['GF', 'Com', 'Storage', 'Wrhs', '(1/2 S)', '(3/4 S)', '(1 S)', '(1 1/2 S)', '(1 3/4 S)', '(2 S)', '(2 1/2 S)', '(3 S)', 'Public']
			},
            SketchSource: { Table: 'tblCommercial', LabelField: "YR_BUILT/C_TYPE", CommandField: null, KeyFields: ["COM_ID"] },
            VectorSource: [{ Table: "tblCommercial", LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: [], AreaField: null, PerimeterField: null, LabelLookup: 'FeatureCodes_DCS', ExtraLabelFields: [{ Name: null, Target: 'name', Caption: 'Label'}] }]
        },
        {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            AllowLabelEdit: true,
           	//SketchLabelLookup: 'FeatureCodes_DCS',
           	addCalculatedType: true,
           	SketchLabelPrefixInSv: "MOB",
            AreaSumFields: {
				'BSMT_SQFT': ['B'], 
				'GRGE_SQFT': ['BG', 'G', 'CP', 'Storage', 'Wrhs', 'Barn'], 
				'PTDK_SQFT': ['Deck', 'Patio'],
				'LVNG_SQFT': ['GF', '(1/2 S)', '(3/4 S)', '(1 S)', '(1 1/2 S)', '(1 3/4 S)', '(2 S)', '(2 1/2 S)', '(3 S)'],
				'PRCH_SQFT': ['Porch'],
				'POOL_SIZE': ['Pool']
			},
            SketchSource: { Table: 'tblMobileHome', LabelField: "YR_BUILT/MANUFACT", CommandField: null, KeyFields: ["MH_ID"] },
            VectorSource: [{ Table: "tblMobileHome", LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: [], AreaField: null, PerimeterField: null, LabelLookup: 'FeatureCodes_DCS', ExtraLabelFields: [{ Name: null, Target: 'name', Caption: 'Label'}] }]
        },
        {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            AllowLabelEdit: true,
            addCalculatedType: true,
            SketchLabelPrefixInSv: "FARM",
            // SketchLabelLookup: 'FeatureCodes_DCS',
            AreaSumFields: {
				'AREA': ['CP', 'Com', 'Deck', 'G', 'Office', 'Patio', 'Porch', 'Manuf', 'Storage', 'Wrhs', 'Public', 'Barn']
			},
            SketchSource: { Table: 'tblFarm', LabelField: "YR_BUILT/FC_TYPE", CommandField: null, KeyFields: ["FARM_ID"] },
            VectorSource: [{ Table: "tblFarm", LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: [], AreaField: null, PerimeterField: null, LabelLookup: 'FeatureCodes_DCS', ExtraLabelFields: [{ Name: null, Target: 'name', Caption: 'Label'}] }]
        }
    ],
	AfterSave: ClarkKySketchAfterSave
}


CAMACloud.Sketching.Configs.Brillion = {
    formatter: 'Apex',
    ApexAutoSubtract: true,
	ApexEditSketchLabel: true,
    sources: [
    {
        AllowSegmentAddition: true,
        AllowSegmentDeletion: true,
        InsertDataOnAddition: true,
        DoNotAllowOpenSegments: false,
        AllowLabelEdit: true,
        SketchLabelPrefix: "COM ",
        SketchLabelPrefixInSv: "COM",
        addCalculatedType: true,
        //SketchLabelLookup: 'AreaCode_AXWDDA1',
        SketchSource: { Table: 'CommercialBuildings', LabelField: "YearBuilt", CommandField: null, KeyFields: ["ROWUID"] },
        VectorSource: [{ Table: "CommercialBuildings", LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["ROWUID"], AreaField: null, PerimeterField: null, LabelTarget: "code", LabelLookup: 'AreaCode_AXWDDA1', ExtraLabelFields: [{ Name: null, Target: 'name', Caption: 'Label'}] }]
    },
    {
        AllowSegmentAddition: true,
        AllowSegmentDeletion: true,
        InsertDataOnAddition: true,
        DoNotAllowOpenSegments: false,
        AllowLabelEdit: true,
        SketchLabelPrefix: "MOB ",
        SketchLabelPrefixInSv: "MOB",
        addCalculatedType: true,
        //SketchLabelLookup: 'AreaCode_AXWDDA1',
        SketchSource: { Table: 'MobileHomes', LabelField: "YearBuilt", CommandField: null, KeyFields: ["ROWUID"] },
        VectorSource: [{ Table: "MobileHomes", LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["ROWUID"], AreaField: null, PerimeterField: null, LabelTarget: "code", LabelLookup: 'AreaCode_AXWDDA1', ExtraLabelFields: [{ Name: null, Target: 'name', Caption: 'Label'}] }]
    },
    {
        AllowSegmentAddition: true,
        AllowSegmentDeletion: true,
        InsertDataOnAddition: true,
        DoNotAllowOpenSegments: false,
        AllowLabelEdit: true,
        SketchLabelPrefix: "RES ",
        SketchLabelPrefixInSv: "RES",
        addCalculatedType: true,
        //SketchLabelLookup: 'AreaCode_AXWDDA1',
        SketchSource: { Table: 'ResidentialBuildings', LabelField: "Story", CommandField: null, KeyFields: ["ROWUID"] },
        VectorSource: [{ Table: "ResidentialBuildings", LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["ROWUID"], AreaField: null, PerimeterField: null, LabelTarget: "code", LabelLookup: 'AreaCode_AXWDDA1', ExtraLabelFields: [{ Name: null, Target: 'name', Caption: 'Label'}] }]
    }
    ],
    BeforeSave: BrillionSketchBeforeSave
}
CAMACloud.Sketching.Configs.Wyandott = {
    formatter: 'CCSketch',
    SketchDefinition: WyandottSketchDefinition,
    sources: [{
        AllowSegmentAddition: true,
        AllowSegmentDeletion: true,
        InsertDataOnAddition: true,
        AllowLabelEdit: true,
        SketchSource: { Table: null, LabelField: 'CASKLTYPE', CommandField: null, KeyFields: ["CASKLPARCEL"] },
        VectorSource: [{ Table: 'Sketches', LabelField: "CASKLCONS", CommandField: null, ConnectingFields: ["CASKLPARCEL"], AreaField: null, PerimeterField: null, LabelTarget: "code" }]
    }]
}

CAMACloud.Sketching.Configs.PnA = {
    formatter: 'pna',
    IsVectorStartCommandField: true,
    IsBldgNew: true,
    FullViewEnabled: true,
    sources: [{
        LookUpQuery: "SELECT Id Id, Class Name FROM tab_bldgtype",
        AllowSegmentAddition: true,
        DeleteBlankSegmentsOnSave: true,
        ClearSegmentOnDeletion: true,
        AllowSegmentDeletion: true,
        AllowLabelEdit: true,
        IsLargeLookup: true,
        AddFootMode: true,
        InsertDataOnAddition: true,
        DoNotAllowLabelMove: true,
        IsPnANewValue: true,
        GroupingField: 'ParcelId',
        SketchSource: { Table: null, LabelField: "Parcel_Id", CommandField: null, KeyFields: ["Parcel_Id"] },
        VectorSource: [{ Table: "bldg", LabelField: 'Type_Id', CommandField: "CC_SKETCH", ConnectingFields: ["Parcel_Id"], AreaField: null, PerimeterField: 'Perimeter', VectorStartCommandField: "Adjustments", IsLabelRequired: true, LabelLookup: 'Type_Id', vectorPagingField: 'Is_Start_New_Card', labelCaption: 'Building Type', FootModeField: 'Is_Drawing_By_Tenth' }]
    }
    ],
    AfterSave: PnASketchAfterSave,
    BeforeSave: PnASketchBeforeSave
}

CAMACloud.Sketching.Configs.ProVal = {
    formatter: 'CCSketch',
    VectorDefinition: ProvalVectorDefinition,
    NoteDefinition: ProvalNoteDefinition,
    LabelDefinition: ProvalLabelDefinition,
    LabelConfig: ProvalLabelConfig,
    LabelPositionDefinition: ProvalLabelPositionToString,
    onSketchSave: ProvalSketchSave,
    BeforeSave: ProvalSketchBeforeSave,
    sources: [ {
		AllowSegmentAddition: true,
		AllowSegmentDeletion: true,
		SketchLabelPrefix: "Imp - ",
        InsertDataOnAddition: true,
        SketchOutBuilding:true,
		AllowLabelEdit: true,
		GroupingField: 'extension',
		IgnoreUnsketchedAreaFromSave: true,
		SketchSource: { Table: 'ccv_improvements', LabelField: "extension/improvement_id/imp_type", CommandField: null, KeyFields: ["extension"] },
		VectorSource: [{ Table: 'ccv_sktsegment_imps', LabelField: null, CommandField: null, ConnectingFields: ["extension"], AreaField: null, PerimeterField: null, filter: 'ivx.status == "A"' }],
		NotesSource: { Table: "SktNote", TextField: "note_text", ConnectingFields: ["extension"] }
    }, {
		AllowSegmentAddition: true,
		AllowSegmentDeletion: true,
		SketchLabelPrefix: "Dwe - ",
		InsertDataOnAddition: true,
		AllowLabelEdit: true,
        //SketchLabelLookup: 'Apex_Area_Codes',
		GroupingField: 'extension',
		SketchSource: { Table: 'ccv_improvements_dwellings', LabelField: "extension/mkt_house_type/year_built", CommandField: null, KeyFields: ["extension"] },
		VectorSource: [{ Table: 'ccv_sktsegment_dwellings', LabelField: null, CommandField: null, ConnectingFields: ["extension"], AreaField: null, PerimeterField: null, filter: 'ivx.status == "A"' }],
		NotesSource: { Table: "SktNote", TextField: "note_text", ConnectingFields: ["extension"] }
    }, {
		AllowSegmentAddition: true,
		AllowSegmentDeletion: true,
		SketchLabelPrefix: "Com - ",
		InsertDataOnAddition: true,
		AllowLabelEdit: true,
		GroupingField: 'extension',
		//SketchLabelLookup: 'Apex_Area_Codes',
		SketchSource: { Table: 'ccv_improvements_comm_bldgs', LabelField: "extension/pricing_key/year_built", CommandField: null, KeyFields: ["extension"] },
		VectorSource: [{ Table: 'ccv_sktsegment_comm_bldgs', LabelField: null, CommandField: null, ConnectingFields: ["extension"], AreaField: null, PerimeterField: null, filter: 'ivx.status == "A"' }],
		NotesSource: { Table: "SktNote", TextField: "note_text", ConnectingFields: ["extension"] }
     }]
}

CAMACloud.Sketching.Configs.ProValNew = {
    formatter: 'CCSketch',
	VectorDefinition: ProvalVectorDefinitionNew,
    NoteDefinition: ProvalNoteDefinitionNew,
    LabelDefinition: ProvalLabelDefinitionNew,
    LabelConfig: ProvalLabelConfigNew,
    LabelPositionDefinition: ProvalLabelPositionToStringNew,
    onSketchSave:ProvalSketchSaveNew,
    BeforeSave: ProvalSketchBeforeSaveNew,
    EnableBoundary: true,
    IsProvalConfig: true,
    IsNoteMaxLength: true,
    //AllowSketchAddition: true,
    //DoNotSelectFirstFromDCC: true,
    //SketchParentTable:"ccv_extensions",
    OutBuildingDefinition: OutBuildingDefinitionNew,
    SkipIsApprovedChanges: true,
    ParcelValidationInBeforeSave: true,
    ConfigTables: {comm_floors: 'comm_floors', comm_special_use: 'comm_special_use', res_floor: 'res_floor', manuf_housing: 'manuf_housing', SktHeader: 'SktHeader', SktNote : 'SktNote', SktOutbuilding: 'SktOutbuilding', SktVector_outbuilding: 'ccv_sktvector_outbuilding', SktSegment: 'sktsegment', SktSegmentLabel: 'SktSegmentLabel', SktVector: 'SktVector'}, 
    //DimensionFormatter : function(ln) {return Math.round(ln);},
    sources: [{
        ConfigName: 'extension',
        AllowSegmentAddition: false,
        AllowSegmentAdditionByCustomButtons: true,
		AllowSegmentDeletion: true,
        InsertDataOnAddition: true,
		AllowLabelEdit: true,
		IgnoreUnsketchedAreaFromSave: true,
		SketchSource: { Table: 'ccv_extensions', LabelField: "RorC/prop_subclass/ext_description", CommandField: null, KeyFields: ["extension"], sketchSourceFilter: 'st.extension ? st.extension.indexOf("L") != 0 : true', sketchSourceSorting: "extension ASC" },
        VectorSource: [{ Table: 'sktsegment', LabelField: null, CommandField: null, ConnectingFields: ["extension"], AreaField: null, PerimeterField: null, filter: 'ivx.status == "A"', EnableLineSelection: true  },
            { Table: 'SktOutbuilding', LabelField: null, CommandField: null, ConnectingFields: ["extension"], AreaField: null, PerimeterField: null, filter: 'ivx.status == "A"', type: 'outBuilding' }
			],
        NotesSource: { Table: "SktNote", TextField: "note_text", ConnectingFields: ["extension"] },
        Buttons: [
            {
                Text: 'Add Segment',
                Action: 'createNewVector',
                Data: { vectorSourceTable: 'sktsegment'},
                Modes: ['MODE_DEFAULT'],
                Requires: 'currentSketch'
            },
            {
                Text: 'Add Outbuilding',
                Action: 'createNewVector',
                Data: { vectorSourceTable: 'SktOutbuilding', type: 'outBuilding' },
                Modes: ['MODE_DEFAULT'],
                Requires: 'currentSketch'
            }
        ]
    }]
}
	
CAMACloud.Sketching.Configs.ProValNewFYL = {
    formatter: 'CCSketch',
	VectorDefinition: ProvalVectorDefinitionNew,
    NoteDefinition: ProvalNoteDefinitionNew,
    LabelDefinition: ProvalLabelDefinitionNew,
    LabelConfig: ProvalLabelConfigNew,
    LabelPositionDefinition: ProvalLabelPositionToStringNew,
    onSketchSave:ProvalSketchSaveNew,
    BeforeSave: ProvalSketchBeforeSaveNew,
    //AllowSketchAddition: true,
    //DoNotSelectFirstFromDCC: true,
    //SketchParentTable:"ccv_extensions",
    OutBuildingDefinition: OutBuildingDefinitionNew,
    SkipIsApprovedChanges: true,
    EnableBoundary: true,
    IsProvalConfig: true,
    IsNoteMaxLength: true, // set note maxLength
    ParcelValidationInBeforeSave: true,
    ConfigTables: {comm_floors: 'ccv_comm_floors', comm_special_use: 'ccv_comm_special_use', res_floor: 'ccv_res_floor', manuf_housing: 'ccv_manuf_housing', SktHeader: 'ccv_SktHeader', SktNote : 'ccv_SktNote', SktOutbuilding: 'ccv_SktOutbuilding', SktVector_outbuilding: 'ccv_SktVector_outbuilding', SktSegment: 'ccv_SktSegment', SktSegmentLabel: 'ccv_SktSegmentLabel', SktVector: 'ccv_SktVector'}, 
    //DimensionFormatter : function(ln) {return Math.round(ln);},
    sources: [{
        ConfigName: 'extension',
        AllowSegmentAddition: false,
        AllowSegmentAdditionByCustomButtons: true,
		AllowSegmentDeletion: true,
        InsertDataOnAddition: true,
		AllowLabelEdit: true,
		IgnoreUnsketchedAreaFromSave: true,
		SketchSource: { Table: 'ccv_extensions', LabelField: "RorC/prop_subclass/ext_description", CommandField: null, KeyFields: ["extension"], sketchSourceFilter: 'st.extension ? st.extension.indexOf("L") != 0 : true', sketchSourceSorting: "extension ASC" },
        VectorSource: [{ Table: 'ccv_SktSegment', LabelField: null, CommandField: null, ConnectingFields: ["extension"], AreaField: null, PerimeterField: null, EnableLineSelection: true },
			{ Table: 'ccv_SktOutbuilding', LabelField: null, CommandField: null, ConnectingFields: ["extension"], AreaField: null, PerimeterField: null, type: 'outBuilding' }
			],
        NotesSource: { Table: "ccv_SktNote", TextField: "note_text", ConnectingFields: ["extension"] },
        Buttons: [
            {
                Text: 'Add Segment',
                Action: 'createNewVector',
                Data: { vectorSourceTable: 'ccv_SktSegment'},
                Modes: ['MODE_DEFAULT'],
                Requires: 'currentSketch'
            },
            {
                Text: 'Add Outbuilding',
                Action: 'createNewVector',
                Data: { vectorSourceTable: 'ccv_SktOutbuilding', type: 'outBuilding' },
                Modes: ['MODE_DEFAULT'],
                Requires: 'currentSketch'
            }
        ]
    }]
}

//window.__defineGetter__("a", function () { sketchApp.render(); });

CAMACloud.Sketching.Configs.CustomCAMA = {
    formatter: 'Apex',
    ApexAutoSubtract: true,
	ApexEditSketchLabel: true,
    sources: [
    {
    	LookUpQuery: "SELECT sar_cd Id, dscr Name FROM lu_sar",
        AllowSegmentAddition: true,
        AllowSegmentDeletion: true,
        InsertDataOnAddition: true,
        DoNotAllowOpenSegments: false,
        AllowLabelEdit: true,
        addCalculatedType: true,
        SketchSource: { Table: 'ccv_bld_mstr_bld', LabelField: 'trav', CommandField: null, KeyFields: ["ROWUID"] },
        VectorSource: [{ Table: "ccv_bld_mstr_bld", LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["ROWUID"], AreaField: null, PerimeterField: null, LabelTarget: "code", ExtraLabelFields: [{ Name: null, Target: 'name', Caption: 'Label'}] }]
    }]
}

CAMACloud.Sketching.Configs.Lafayette = {
    formatter: 'Apex',
    ApexAutoSubtract: true,
	ApexEditSketchLabel: true,
    sources: [
    {
        //LookUpQuery: "SELECT sar_cd Id, dscr Name FROM lu_sar",
        AllowSegmentAddition: true,
        AllowSegmentDeletion: true,
        InsertDataOnAddition: true,
        DoNotAllowOpenSegments: false,
        AllowLabelEdit: true,
        addCalculatedType: true,
        SketchSource: { Table: 'ccv_bld_mstr_bld', LabelField: 'bld_num/impr_tp/impr_mdl_cd/class/act', CommandField: null, KeyFields: ["ROWUID"] },
        VectorSource: [{ Table: "ccv_bld_mstr_bld", LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["ROWUID"], AreaField: null, PerimeterField: null, LabelTarget: "code", LabelLookup: 'Apex_Area_Codes', ExtraLabelFields: [{ Name: null, Target: 'name', Caption: 'Label'}] }]
    }],
    AfterSave: LafayetteSketchAfterSave
}

CAMACloud.Sketching.Configs.Patriot= {
    formatter: 'Patriot',
    PlaceUnsketchedSegment: true,
    IsPatriot: true,
    sources: [{     
        AllowSegmentAddition: true,
        AllowSegmentDeletion: true,
        InsertDataOnAddition: true,
        DoNotAllowOpenSegments: false,
        EnableUnSketchedAreaDrawing : true,
        AllowLabelEdit: true,
        splitByDelimiter:'/',
        IsLargeLookup: true,
        SketchSource: { Table: 'CCV_Sketch', LabelField: 'parentRecord.BuildingID/parentRecord.xrBuildingTypeID/parentRecord.YearBuilt', CommandField: null, KeyFields: ["ROWUID"] },
        VectorSource: [{ Table: "CCV_Sketch", LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["ROWUID"], AreaField: null, PerimeterField: null, LabelLookup: 'Sketch_lookup' }]
    }
    ],
    AfterSave: PatriotSketchAfterSave
}

CAMACloud.Sketching.Configs.LucasSV = {
    formatter: 'Lucas',
    AppendSketchPrefixInSVData: true,
    sources: [
        {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            ClearSegmentOnDeletion: true,
            DeleteBlankSegmentsOnSave: true,
            SketchLabelPrefix: "COM ",
            AllowLabelEdit: true,
            InsertDataOnAddition: true,
            SketchLabelLookup: 'Sketch_VL',
            SketchLabelPrefixInSv: "COM",
            sectionFilterField: 'CARD',
            SketchSource: { Table: null, LabelField: "IASW_ID", CommandField: null, KeyFields: ["PARID"] },
            VectorSource: [{ Table: "ADDN_SV", LabelField: "IASW_ID", CommandField: "VECT", ConnectingFields: ["PARID"], AreaField: "AREA", PerimeterField: null, LabelCommandField: null, DimensionCommandField: null }]
        },
        {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            ClearSegmentOnDeletion: true,
            DeleteBlankSegmentsOnSave: true,
            SketchLabelPrefix: "RES ",
            SketchLabelPrefixInSv: "RES",
            AllowLabelEdit: true,
            InsertDataOnAddition: true,
            SketchLabelLookup: 'Sketch_VL',
            sectionFilterField: 'CARD',
            SketchSource: { Table: null, LabelField: "PARID", CommandField: null, KeyFields: ["PARID"] },
            VectorSource: [{ Table: "COMINTEXTFEAT_SV", LabelField: "SF", CommandField: "VECT", ConnectingFields: ["PARID"], AreaField: "AREA", PerimeterField: null, LabelCommandField: null, DimensionCommandField: null }]
        }
    ]
}

CAMACloud.Sketching.Configs.CustomCAMATraverse = {
    encoder: 'CustomCAMATraverse',
	formatter: 'CCSketch',
    sources: [{        
        AllowSegmentAddition: true,
        AllowSegmentDeletion: true,
        InsertDataOnAddition: true,
        DoNotAllowOpenSegments: false,
        AllowLabelEdit: true,
        SketchSource: { Table: 'ccv_bld_mstr_bld', LabelField: 'trav', CommandField: null, KeyFields: ["ROWUID"] },
        VectorSource: [{ Table: "ccv_bld_mstr_bld", LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["ROWUID"], AreaField: null, PerimeterField: null, LabelTarget: "code", LabelLookup: 'Apex_Area_Codes' }]
    }],
     //AfterSave: HillsBoroughSketchAfterSave
}    

CAMACloud.Sketching.Configs.WinGapRS = {
    formatter: 'WinGap',
    sources: [
        {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            ClearSegmentOnDeletion: true,
            DeleteBlankSegmentsOnSave: true,
            SketchLabelPrefix: "RES ",
            SketchLabelPrefixInSv: "RES",
            AllowLabelEdit: true,
            InsertDataOnAddition: true,
            SketchSource: { Table: "REPROP", LabelField: "REPROPKEY", CommandField: null, KeyFields: ["REPROPKEY"] },
            VectorSource: [{ Table: "SKETCH_RES", LabelField: "IMPKEY", CommandField: "VERTICES", ConnectingFields: ["REPROPKEY"], AreaField: "AREA", PerimeterField: "PERIMETER", LabelCommandField: "IMPLABEL", DimensionCommandField: "LABELS", LabelLookup: 'IMPLABEL_RES' }]
        },
        {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            ClearSegmentOnDeletion: true,
            DeleteBlankSegmentsOnSave: true,
            SketchLabelPrefix: "COM ",
            SketchLabelPrefixInSv: "COM",
            AllowLabelEdit: true,
            InsertDataOnAddition: true,
            SketchSource: { Table: "COMMIMP", LabelField: "IMPROV_NO/SECTION_NO", CommandField: null, KeyFields: ["COMMKEY"] },
            VectorSource: [{ Table: "SKETCH_COMM", LabelField: "IMPKEY", CommandField: "VERTICES", ConnectingFields: ["COMMKEY"], AreaField: "AREA", PerimeterField: "PERIMETER", LabelCommandField: "IMPLABEL", DimensionCommandField: "LABELS", LabelLookup: 'IMPLABEL_COMM' }]
        },
        {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            ClearSegmentOnDeletion: true,
            DeleteBlankSegmentsOnSave: true,
            SketchLabelPrefix: "MH ",
            SketchLabelPrefixInSv: "MH",
            AllowLabelEdit: true,
            InsertDataOnAddition: true,
            SketchSource: { Table: "CCV_MOBILE", LabelField: "MOBILEKEY/RECID", CommandField: null, KeyFields: ["MOBILEKEY"] },
            VectorSource: [{ Table: "SKETCH_MH", LabelField: "IMPKEY", CommandField: "VERTICES", ConnectingFields: ["MOBILEKEY"], AreaField: "AREA", PerimeterField: "PERIMETER", LabelCommandField: "IMPLABEL", DimensionCommandField: "LABELS", LabelLookup: 'IMPLABEL_MH' }]
        },
        {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            ClearSegmentOnDeletion: true,
            DeleteBlankSegmentsOnSave: true,
            SketchLabelPrefix: "MHP ",
            SketchLabelPrefixInSv: "MHP",
            AllowLabelEdit: true,
            InsertDataOnAddition: true,
            SketchSource: { Table: "CCV_MOBILE_PREBILL", LabelField: "MOBILEKEY/RECID", CommandField: null, KeyFields: ["MOBILEKEY"] },
            VectorSource: [{ Table: "SKETCH_MH_PREBILL", LabelField: "IMPKEY", CommandField: "VERTICES", ConnectingFields: ["MOBILEKEY"], AreaField: "AREA", PerimeterField: "PERIMETER", LabelCommandField: "IMPLABEL", DimensionCommandField: "LABELS", LabelLookup: 'IMPLABEL_MH' }]
        }
    ],
    AfterSave: WinGapRSSketchAfterSave
}

CAMACloud.Sketching.Configs.MFCD = {
    formatter: 'CCSketch',
    LabelConfig: MFCDLabelConfig,
    SketchDefinition: MFCDSketchDefinition,
    onSketchSave: MFCDonSketchSave,
    FullViewEnabled: true,
    sources: [
    	{
        	AllowSegmentAddition: true,
        	AllowSegmentDeletion: true,
        	InsertDataOnAddition: true,
        	AllowLabelEdit: true,
        	DoNotAllowLabelMove: true,
        	SketchLabelPrefixInSv: "DWELL",
        	GroupingField: 'DWELRECNUM',
			DGroupingField: true,
        	SketchSource: { Table: "CAM_DWEL", LabelField: 'DWELRECNUM/CADWOCCUPANCYTYPE', CommandField: null, KeyFields: ["CASKLPARCEL"] },
        	VectorSource: [{ Table: "CAM_SKETCH", LabelField: "CASKLCONS", CommandField: null, ConnectingFields: ["CASKLPARCEL"], AreaField: null, PerimeterField: null, LabelTarget: "code" }]
    	},
    	{
        	AllowSegmentAddition: true,
        	AllowSegmentDeletion: true,
        	InsertDataOnAddition: true,
        	AllowLabelEdit: true,
        	DoNotAllowLabelMove: true,
        	GroupingField: 'DWELRECNUM',
        	SketchLabelPrefixInSv: "OUTB",
			OGroupingField: true,
        	SketchSource: { Table: "CAM_OUTB", LabelField: 'OBLRECNUM/DescriptionForSketch/CAOBLYEARB', CommandField: null, KeyFields: ["CAOSLPARCEL"] },
        	VectorSource: [{ Table: "CAM_OUTSK", LabelField: "CAOSLCONS", CommandField: null, ConnectingFields: ["CAOSLPARCEL"], AreaField: null, PerimeterField: null, LabelTarget: "code" }]
    	}
    ]
}

CAMACloud.Sketching.Configs.MVP= {
    formatter: 'mvp',
    LabelConfig: mvpLabelConfig,
    OutBuildingDefinition: MvPOtherImpDefinition,
    BeforeSave: MVPSketchBeforeSaveNew,
    onSketchSave: MVPSketchSave,
    SkipIsApprovedChanges: true,
    //isShowBoundary: true,
    isOIunsketched: true,
    EnableBoundary: true,
    AddNewNote: true,
    NoteMaxLength: 34,
    DisableNoteCharacters: [188, 186, 52, 219, 221, 51, 190, 220],	
    ParcelValidationInBeforeSave: true,
    isMVP: true,
    sources: [{     
        AllowSegmentAddition: false,
        AllowSegmentAdditionByCustomButtons: true,
        AllowSegmentDeletion: true,
        InsertDataOnAddition: true,
        DoNotAllowOpenSegments: false,
    	moveLabelcenter: true,
        AllowLabelEdit: true,
        SketchSource: { Table: 'CC_Card', LabelField: 'CardNumber/MainBuildingType', CommandField: null, KeyFields: ["ROWUID"] },
        VectorSource: [{ Table: "CC_Card", LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["ROWUID"], AreaField: null, PerimeterField: null, LabelLookup: null, LabelAreaPosition: true }],
    	Buttons: [
            {
                Text: 'Add Segment',
                Action: 'createNewVector',
                Data: { vectorSourceTable: 'CC_Card'},
                Modes: ['MODE_DEFAULT'],
                Requires: 'currentSketch'
            },
            {
                Text: 'Add Other Imp',
                Action: 'createNewVector',
                Data: { vectorSourceTable: 'CC_Card', type: 'outBuilding' },
                Modes: ['MODE_DEFAULT'],
                Requires: 'currentSketch'
            },
            {
                Text: 'Show OI Labels',
                Action: 'showOtherImpDescription',
                Data: { vectorSourceTable: 'CC_Card', type: 'outBuilding' },
                Modes: ['MODE_DEFAULT'],
                Requires: 'currentSketch',
                DisplayCondition: 'sketchSettings.ShowOtherImprovementDescLabel == 1',
                Class: 'showOtherImpDescription'
            }
        ]
    }],
   // AfterSave: PatriotSketchAfterSave
}

CAMACloud.Sketching.Configs.ApexJson = {
    formatter: 'ApexJson',
    //DimensionFormatter : function(ln) {return Math.round(ln);},
   	ApexFreeFormTextLabels: true,
   	IsJsonFormat: true,
   	ApexEditSketchLabel: true,
   	EnableSpecialRounding: true,
    sources: [
        {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            AllowLabelEdit: true,
        	AssignLookUpNameAsLabel: true,
        	IsLargeLookup: true,
        	addCalculatedType:true,
            SketchSource: { Table: 'IMPAPEXOBJECT', LabelField: "IMPID/IMPNO", CommandField: null, KeyFields: ["IMPID"], sketchSourceSorting: "IMPNO ASC" },
            VectorSource: [{ Table: "IMPAPEXOBJECT", LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["IMPID"], AreaField: null, PerimeterField: null, LabelTarget: "code", LabelLookup: 'AREA_CODES', IsLabelRequired: true, LabelAreaPosition: true, ExtraLabelFields: [{ Name: null, Target: 'name', Caption: 'Label'}] }]
        }
    ],
    AssociateVector: { 
    	source: [
    		{ sourceTable: 'IMPBUILTASFLOOR', fields: 'IMPBUILTASFLOOR.APEXID/IMPBUILTASFLOOR.ROWUID, IMPBUILTASFLOOR.IMPSFLOORDESCRIPTION-IMPBUILTAS.BLTASSF', headers: 'ApexId/ROWUID,Description-Floor SF'}, 
    		{ sourceTable: 'CCV_IMPDETAIL_ATTACHED_DETACHED', fields: 'CCV_IMPDETAIL_ATTACHED_DETACHED.APEXID/CCV_IMPDETAIL_ATTACHED_DETACHED.ROWUID, CCV_IMPDETAIL_ATTACHED_DETACHED.IMPDETAILTYPE-CCV_IMPDETAIL_ATTACHED_DETACHED.IMPDETAILDESCRIPTION-CCV_IMPDETAIL_ATTACHED_DETACHED.DETAILUNITCOUNT', headers: 'ApexId/ROWUID,Detail Type-Description-Units'}, 
    		{ sourceTable: 'CCV_IMPDETAIL_ADD_ONS', fields: 'CCV_IMPDETAIL_ADD_ONS.APEXID/CCV_IMPDETAIL_ADD_ONS.ROWUID, CCV_IMPDETAIL_ADD_ONS.IMPDETAILTYPE-CCV_IMPDETAIL_ADD_ONS.IMPDETAILDESCRIPTION-CCV_IMPDETAIL_ADD_ONS.DETAILUNITCOUNT', headers: 'ApexId/ROWUID,Detail Type-Detail Description-Units'}
		], 
		isConnected: true,
		requiredFieldValidation: false,
		autoSelectIfSingle: false,
		labelNoReplace: true,
		isEditedAndCurrentSketchOnly: true,
		autoSelectFunction: FTC_associate_autoSelect,
		vectorListConfig: FTC_associate_vector_config,
		note: "Associate each sketch to an existing CAMA record below. If no CAMA record is chosen, one will be created based on the sketch details."
	},
	
	isHideAssociateVector:true,
   	AfterSave: FTCJsonSketchAfterSave
}

CAMACloud.Sketching.Configs.ApexJsonARP = {
   formatter: 'ApexJson',
   ApexFreeFormTextLabels: true,
   IsJsonFormat: true,
   ApexAutoSubtract: true,
   ApexEditSketchLabel: true,
   sources: [
      {
       AllowSegmentAddition: true,
       AllowSegmentDeletion: true,
       InsertDataOnAddition: true,
       AllowLabelEdit: true,
       addCalculatedType: true,
       //AssignLookUpNameAsLabel: true,
       SketchSource: { Table: null, LabelField: "property_id", CommandField: null, KeyFields: ["property_id"] },
       VectorSource: [{ Table: null, LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["property_id"], AreaField: null, PerimeterField: null, LabelLookup: 'AREA_CODES', LabelAreaPosition: true, ExtraLabelFields: [{ Name: null, Target: 'name', Caption: 'Label'}] }]
      }
    ] 
}

CAMACloud.Sketching.Configs.MA_Lite = {
    formatter: 'ApexJson',
    ApexFreeFormTextLabels: true,
   	IsJsonFormat: true,
   	ApexAutoSubtract: true,
    ApexEditSketchLabel: true,
    sources: [
    {
        AllowSegmentAddition: true,
        AllowSegmentDeletion: true,
        InsertDataOnAddition: true,
        AllowLabelEdit: true,
        IsLargeLookup: true,
        AssignLookUpNameAsLabel: true,
        EnableSwapLookupIdName: true,
        addCalculatedType: true,
        SketchSource: { Table: null, LabelField: "AccountId/PropertyClass", CommandField: null, KeyFields: ["ROWUID"] },
        VectorSource: [{ Table: null, LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["ROWUID"], AreaField: null, PerimeterField: null, LabelTarget: "code", LabelLookup: 'Apex_Area_Codes', LabelAreaPosition: true, ExtraLabelFields: [{ Name: null, Target: 'name', Caption: 'Label'}] }]
    }
    ]
}

CAMACloud.Sketching.Configs.Helion = {
    formatter: 'ApexJson',
    ApexFreeFormTextLabels: true,
    IsJsonFormat: true,
    ApexAutoSubtract: true,
    ApexEditSketchLabel: true,
    sources: [
        {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            InsertDataOnAddition: true,
            AllowLabelEdit: true,
            IsLargeLookup: true,
            AssignLookUpNameAsLabel: true,
            EnableSwapLookupIdName: true,
            addCalculatedType: true,
            SketchSource: { Table: null, LabelField: "{KeyValue1} Sketch", CommandField: null, KeyFields: ["ROWUID"] },
            VectorSource: [{ Table: null, LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["ROWUID"], AreaField: null, PerimeterField: null, LabelTarget: "code", LabelLookup: 'Apex_Area_Codes', LabelAreaPosition: true, ExtraLabelFields: [{ Name: null, Target: 'name', Caption: 'Label' }] }]
        }
    ]
}

CAMACloud.Sketching.Configs.HennepinJson = {
    formatter: 'ApexJson',
    ApexFreeFormTextLabels: true,
   	IsJsonFormat: true,
   	ApexAutoSubtract: true,
	ApexEditSketchLabel: true,
    sources: [
        {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            AllowLabelEdit: true,
            addCalculatedType: true,
         	//SketchLabelLookup: 'Apex_Area_Codes',
            SketchSource: { Table: 'CCV_BLDG_CONSTR', LabelField: "BID/BLD_BLDG_NAME", CommandField: null, KeyFields: ["BID"] },
            VectorSource: [{ Table: "CCV_BLDG_CONSTR", LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["BID"], AreaField: null, PerimeterField: null, LabelAreaPosition: true, LabelTarget: "code", LabelLookup: 'Apex_Area_Codes', ExtraLabelFields: [{ Name: null, Target: 'name', Caption: 'Label'}] }]
        }
    ],
    AfterSave: HennepinSketchAfterSave,
    BeforeSave: HennepinSketchBeforeSave
}

CAMACloud.Sketching.Configs.DeltaJson = {
    formatter: 'ApexJson',
    ApexFreeFormTextLabels: true,
   	IsJsonFormat: true,
   	//ApexAutoSubtract: true,
    sources: [{
        AllowSegmentAddition: true,
        AllowSegmentDeletion: true,
        InsertDataOnAddition: true,
        AllowLabelEdit: true,
        //addCalculatedType: true,
        Specific_Identifier_00_Code: true,
      //  SketchLabelLookup: 'Apex_Area_Codes',
        AssignLookUpNameAsLabel: true,
        IsLargeLookup: true,
        SketchSource: { Table: 'CCV_BLDG_MASTER', LabelField: "PPIN/NUMBER", CommandField: null, KeyFields: ["PPIN"] },
        VectorSource: [{ Table: "CCV_BLDG_MASTER", LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["PPIN"], AreaField: null, PerimeterField: null, LabelTarget: "code" , labelCaption:"Code" ,HideLabelFromShow: true, LabelAreaPosition: true, LabelLookup: 'Apex_Area_Codes' , ExtraLabelFields: [{ Name: null, Target: 'name', Caption: 'Label'}] }]
    }],
    AssociateVector: { source: [{ sourceTable: 'APDIM301', category: 'Building', fields: 'APDIM301.CARD_LINE,APDIM301.STRUCTURE_DESCRIPTION', headers: '# on Card,Structure Description'}], isConnected: true, requiredFieldValidation: true, autoSelectIfSingle: true },
    AfterSave: BaldwinSketchAfterSave
}

CAMACloud.Sketching.Configs.MCISJson  = {
    formatter: 'ApexJson', //StLouis
    ApexFreeFormTextLabels: true,
   	IsJsonFormat: true,
    DimensionFormatter : function(ln) {return Math.round(ln);},
    ApexAutoSubtract: true,
	ApexEditSketchLabel: true,
    sources: [
    {
        AllowSegmentAddition: true,
        AllowSegmentDeletion: true,
        InsertDataOnAddition: true,
        AllowLabelEdit: true,
        IsLargeLookup: true,
        addCalculatedType: true,
        AssignLookUpNameAsLabel: true,
        //SketchLabelLookup: 'Apex_Area_Codes',
        SketchSource: { Table: null, LabelField: "PARCELNBR", CommandField: null, KeyFields: ["PARCELNBR"] },
        VectorSource: [{ Table: null, LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["PARCELNBR"], AreaField: null, PerimeterField: null, LabelAreaPosition: true, LabelTarget: "code", LabelLookup: 'Apex_Area_Codes', ExtraLabelFields: [{ Name: null, Target: 'name', Caption: 'Label'}] }]
    }
    ]
}

CAMACloud.Sketching.Configs.CustomCAMAJson = {
    formatter: 'ApexJson', //'Apex',
    ApexFreeFormTextLabels: true,
   	IsJsonFormat: true,
    DimensionFormatter : function(ln) {return Math.round(ln);},
    ApexAutoSubtract: true,
	ApexEditSketchLabel: true,
    sources: [
    {
    	LookUpQuery: "SELECT sar_cd Id, dscr Name FROM lu_sar",
        AllowSegmentAddition: true,
        AllowSegmentDeletion: true,
        InsertDataOnAddition: true,
        DoNotAllowOpenSegments: false,
        AllowLabelEdit: true,
        addCalculatedType: true,
        SketchSource: { Table: 'ccv_bld_mstr_bld', LabelField: 'trav', CommandField: null, KeyFields: ["ROWUID"] },
        VectorSource: [{ Table: "ccv_bld_mstr_bld", LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["ROWUID"], AreaField: null, PerimeterField: null, LabelAreaPosition: true, LabelTarget: "code", ExtraLabelFields: [{ Name: null, Target: 'name', Caption: 'Label'}] }]
    }]
}

//CAMA USA Apex Sketch Config
CAMACloud.Sketching.Configs.ApexCartJson = {
    formatter: 'ApexJson', //'Apex',
    ApexFreeFormTextLabels: true,
   	IsJsonFormat: true,
    DimensionFormatter : function(ln) {return Math.round(ln);},
    ApexAutoSubtract: true,
    sources: [
        {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            InsertDataOnAddition: true,
            AllowLabelEdit: true,
            DoNotAllowLabelMove: false,
            LabelDelimiter: '',
            addCalculatedType: true,
            SketchSource: { Table: 'CAMBL', LabelField: "BLBVAL", CommandField: null, KeyFields: ["BTLINE"] },
            VectorSource: [{ Table: "CAMBL", LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["BTLINE"], AreaField: null, HideLabelFromEdit: true, LabelAreaPosition: true, HideLabelFromShow: true, PerimeterField: null, ExtraLabelFields: [{ Name: null, LookUp: "AREA_CODE", Target: 'code', Caption: 'Area Code', HideLabelFromShow: true, IsRequired: true }, { Name: null, LookUp: "CAMSUBA_distinct", Target: 'name', ValueRegx: '^.{0,3}', Caption: 'Sub-Area', DoNotShowLabelDescription: true, IsLargeLookup: true, IsRequired: true }, { Name: null, LookUp: null, Target: 'name', ValueRegx: '....$', Caption: 'Year', ValidationRegx: /^[12][0-9]{3}$/ }] }]
        }
    ],
    AfterSave: ApexCartSketchAfterSave
}

//CustomCAMA config
CAMACloud.Sketching.Configs.LafayetteJson = {
    formatter: 'ApexJson', //'Apex',
    ApexFreeFormTextLabels: true,
   	IsJsonFormat: true,
    DimensionFormatter : function(ln) {return Math.round(ln);},
    ApexAutoSubtract: true,
	ApexEditSketchLabel: true,
    sources: [
    {
        //LookUpQuery: "SELECT sar_cd Id, dscr Name FROM lu_sar",
        AllowSegmentAddition: true,
        AllowSegmentDeletion: true,
        InsertDataOnAddition: true,
        DoNotAllowOpenSegments: false,
        AllowLabelEdit: true,
        addCalculatedType: true,
        SketchSource: { Table: 'ccv_bld_mstr_bld', LabelField: 'bld_num/impr_tp/impr_mdl_cd/class/act', CommandField: null, KeyFields: ["ROWUID"] },
        VectorSource: [{ Table: "ccv_bld_mstr_bld", LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["ROWUID"], AreaField: null, PerimeterField: null, LabelTarget: "code", LabelLookup: 'Apex_Area_Codes', LabelAreaPosition: true, ExtraLabelFields: [{ Name: null, Target: 'name', Caption: 'Label'}] }]
    }],
    AfterSave: LafayetteSketchAfterSave
}


CAMACloud.Sketching.Configs.PVD_Manage = {
    formatter: 'RapidSketch',
    isPVDConfig: true,
    doNotDecodeEncode: true,
    sources: [
    {
        //HideNullSketches: true,
        AllowSegmentAddition: true,
        AllowSegmentDeletion: true,
        AllowLabelEdit: true,
        //AllowTransferOut: true,
        SaveAllSegments: true,
        SketchSource: { Table: "Buildings", LabelField: "Building/Description/ResidenceType %# CommercialType2/YearConstrcuted", CommandField: null, KeyFields: ["ROWUID"]  },
        VectorSource: [{ Table: "Buildings", LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["ROWUID"], AreaField: null, PerimeterField: null, LabelLookup: 'Sketch_Lookup' }]
    }]
}

CAMACloud.Sketching.Configs.ARPJson = {
   formatter: 'ApexJson',
   ApexFreeFormTextLabels: true,
   IsJsonFormat: true,
   FullViewEnabled: true,
   sources: [
    	{
        	AllowSegmentAddition: true,
        	AllowSegmentDeletion: true,
        	InsertDataOnAddition: true,
        	AllowLabelEdit: true,
        	GroupingField: 'ParcelId',
        	AssignLookUpNameAsLabel: true,
        	SketchLabelPrefixInSv: "CHAR",
        	SketchSource: { Table: "ccv_pvadata_characteristics", LabelField: 'imp_no/Structure_Type/built', CommandField: null, KeyFields: ["ROWUID"] },
        	VectorSource: [{ Table: "ccv_pvadata_characteristics", LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["ROWUID"], AreaField: null, PerimeterField: null, LabelLookup: 'Apex_Area_Codes', LabelTarget: "code", LabelAreaPosition: true }]
    	},
    	{
        	AllowSegmentAddition: true,
        	AllowSegmentDeletion: true,
        	InsertDataOnAddition: true,
        	AllowLabelEdit: true,
        	DoNotAllowLabelMove: true,
        	GroupingField: 'ParcelId',
        	SketchLabelPrefixInSv: "OI",
        	AssignLookUpNameAsLabel: true,
        	SketchSource: { Table: "CCV_pvadata_OI", LabelField: 'imp_no/Structure_Type/built/Condition', CommandField: null, KeyFields: ["ROWUID"] },
        	VectorSource: [{ Table: "CCV_pvadata_OI", LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["ROWUID"], AreaField: null, PerimeterField: null, LabelLookup: 'Apex_Area_Codes', LabelTarget: "code", LabelAreaPosition: true }]
    	}
   ]
    
}

CAMACloud.Sketching.Configs.FarragutJson = {
   formatter: 'ApexJson',
   ApexFreeFormTextLabels: true,
   IsJsonFormat: true, 
   SkipRowuidInChange: true,
   sources: [
    	{
        	AllowSegmentAddition: true,
        	AllowSegmentDeletion: true,
        	InsertDataOnAddition: true,
        	AllowLabelEdit: true,
        	AssignLookUpNameAsLabel: true,
        	SketchLabelPrefixInSv: "RES",
           SketchSource: { Table: "CCV_RES_BUILDING", LabelField: 'Card # {CARD_NUM}/Yr Blt: {YEAR_BUILT}', CommandField: null, KeyFields: ["ROWUID"], sketchSourceSorting: "CARD_NUM ASC, ROWUID ASC", LabelFieldDelimter: ' - ' },
           VectorSource: [{ Table: "CCV_RES_BUILDING", LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["ROWUID"], AreaField: null, PerimeterField: null, LabelLookup: 'Res_Sketch_Lookup', LabelTarget: "code", LabelAreaPosition: true }]
    	},
    	{
        	AllowSegmentAddition: true,
        	AllowSegmentDeletion: true,
        	InsertDataOnAddition: true,
        	AllowLabelEdit: true,
        	DoNotAllowLabelMove: true,
            AssignLookUpNameAsLabel: true, 
        	SketchLabelPrefixInSv: "COM",
            SketchSource: { Table: "CCV_COM_BUILDING", LabelField: 'Card # {CARD_NUM}/Yr Blt: {YEAR_BUILT}/Eff Yr: {EFFECTIVE_YEAR}/Depr: {DEPRECIATION}', CommandField: null, KeyFields: ["ROWUID"], sketchSourceSorting: "CARD_NUM ASC, ROWUID ASC", LabelFieldDelimter: ' - ' },
            VectorSource: [{ Table: "CCV_COM_BUILDING", LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["ROWUID"], AreaField: null, PerimeterField: null, LabelLookup: 'Com_Sketch_Lookup', LabelTarget: "code", LabelAreaPosition: true }]
    	}
   ],
   AssociateVector: { 
    	source: [
    		{ Type: 'MainBuilding', ResSourceTable: 'CCV_RES_ADDITION', ComSourceTable: 'CCV_COM_ADDITION', ResFields: 'MAIN_LOOKUP/FOOTPRINT_AREA/VALUE', ComFields: 'MAIN_LOOKUP/FOOTPRINT_AREA/VALUE', ResHeaders: 'Residential Building', ComHeaders: 'Commercial Building' }, 
    		{ Type: 'Addition', ResSourceTable: 'CCV_RES_ADDITION', ComSourceTable: 'CCV_COM_ADDITION', ResFields: 'MAIN_LOOKUP/FOOTPRINT_AREA/VALUE', ComFields: 'MAIN_LOOKUP/FOOTPRINT_AREA/VALUE', ResHeaders: 'Additions', ComHeaders: 'Additions' }, 
           { Type: 'Section', hidden: 'sketchSourceTable == "CCV_RES_BUILDING"', ComSourceTable: 'CCV_COM_SECTION_FEATURE', ComFields: 'OCCUPANCY/SQFT/VALUE', ComHeaders: 'Sections' }
		], 
		associateVectorDefinition: FarragutAssociateVectorDefinition,
        note: "Associate each sketch to an existing CAMA record. If no CAMA record is chosen,<b> one will be created based on the sketch details</b>.<br><br>If sketch is new, a new record will be created based on the sketch details.<br><br>[M]: Modified Sketch  [N]: New sketch"
	},
    AfterSave: FarragutSketchAfterSave    
}

CAMACloud.Sketching.Configs.T2Json = {
    formatter: 'ApexJson',
   	ApexFreeFormTextLabels: true,
    IsJsonFormat: true,
    clearModifiedAfterSave: true,
    sources: [
        {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            AllowLabelEdit: true,
        	AssignLookUpNameAsLabel: true,
        	IsLargeLookup: true,
            SketchSource: { Table: null, LabelField: "ParcelNumber", CommandField: null, KeyFields: ["ParcelNumber"] },
            VectorSource: [{ Table: null, LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["ParcelNumber"], AreaField: null, PerimeterField: null, LabelTarget: "code", LabelAreaPosition: true, LabelLookup: 'Apex_Area_Codes' }]
        }
    ],
    AssociateVector: {
        source: [
            { Type: 'Res', SourceTable: 'ccv_tAA_VSTerraGon_Res', Fields: 'SliceTypeID/KeyID/SketchBaseArea' },
            { Type: 'Com', SourceTable: 'ccv_tAA_VSTerraGon_Com', Fields: 'SliceTypeID/KeyID/SketchBaseArea' },
            { Type: 'Imp', SourceTable: 'ccv_tAA_Misc', Fields: 'SliceTypeID/MICodeID/V01' },
            { Type: 'Mrs', SourceTable: 'ccv_tAA_VS_Misc_Res', Fields: 'SliceTypeID/MICodeID/V01' },
            { Type: 'Mcm', SourceTable: 'ccv_tAA_VS_Misc_Com', Fields: 'SliceTypeID/MICodeID/V01' }
        ],
        associateVectorDefinition: T2AssociateVectorDefinition,
        note: "[M]: Modified Sketch  [N]: New sketch"
    },
    AfterSave: T2SketchAfterSave
}

CAMACloud.Sketching.Configs.IASW_US = {
    formatter: 'Lucas',
    LabelAssociateWindow: IASW_USLabelWindowDefinition,
    sources: [
        {
            Key: "RES",
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            ClearSegmentOnDeletion: true,
            DeleteBlankSegmentsOnSave: true,
            SketchLabelPrefix: "RES -",
            SketchLabelPrefixInSv: "RES",
            AllowLabelEdit: true,
            InsertDataOnAddition: true,
            ShowVectorDetails: true,
          //  SketchLabelLookup: 'OKLOW',
            DoNotAllowLabelMove: true,
            sectionFilterField: 'CARD',
			EnableMarkedAreaDrawing: true,            
            DoNotAddOrEditFirstRecordlabel: true,
            FirstRecordlabelValue:"STORIES/BSMT",
            SaveExtraLabelFields: true,
            SketchSource: { Table: "DWELDAT", LabelField: "CARD/STYLE/YRBLT", CommandField: null, KeyFields: ["CARD"] },
            VectorSource: [{ Table: "ADDN", LabelField: "LOWER", labelCaption: "Lower", CommandField: "VECT", ConnectingFields: ["CARD"], AreaField: "AREA", PerimeterField: null, LabelCommandField: null, IsLabelRequired: false, DimensionCommandField: null, LabelLookup: 'OKLOW/LOWER', ExtraLabelFields: [{ Name: "FIRST", LookUp: "OK1/FIRST", Caption: "First", IsRequired: false }, { Name: "SECOND", Caption: "Second", LookUp: "OK2/SECOND", IsRequired: false }, { Name: "THIRD", Caption: "Third", LookUp: "OK3/THIRD", IsRequired: false }] }]
        },
        {
            Key: "COM",
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            ClearSegmentOnDeletion: true,
            DeleteBlankSegmentsOnSave: true,
            SketchLabelPrefix: "COM -",
            SketchLabelPrefixInSv: "COM",
            AllowLabelEdit: true,
            InsertDataOnAddition: true,
            DoNotAllowLabelMove: true,
            EnableMarkedAreaDrawing: true,
            // SketchLabelLookup: 'Sketch_VL',
            sectionFilterField: 'CARD',
            SaveExtraLabelFields: true,
            SketchSource: { Table: "COMDAT", LabelField: "CARD/BLDNUM/YRBLT", CommandField: null, KeyFields: ["CARD"] },
            VectorSource: [{ Table: "COMINTEXT", LabelField: "SECT", labelCaption: "Sect", CommandField: "VECT", ConnectingFields: ["CARD"], AreaField: "AREA", PerimeterField: 'PERIM', LabelCommandField: null, IsLabelRequired: false, DimensionCommandField: null, EnableFieldValidation: { sourceTable: 'COMINTEXT' }, ExtraLabelFields: [{ Name: "USETYPE", Caption: "UseType", LookUp: "USETYPE", IsRequired: false }, { Name: "FLRFROM", LookUp: null, IsRequired: false,Caption: 'From', EnableFieldValidation: { sourceTable: 'COMINTEXT' } }, { Name: "FLRTO", LookUp: null, IsRequired: false,Caption: 'To', EnableFieldValidation: { sourceTable: 'COMINTEXT' }  }] },
                { Table: "COMFEAT", LabelField: "STRUCT", labelCaption: "Struct", CommandField: "VECT", ConnectingFields: ["CARD"], AreaField: "AREA", PerimeterField: null, LabelCommandField: null, IsLabelRequired: false, DimensionCommandField: null, LabelLookup : "STRUCT" }
            ]
        }, {
            Key: "OBY",
            SketchLabelPrefix: "OBY -",
            SketchLabelPrefixInSv: "OBY",
            AllowLabelEdit: true,
            InsertDataOnAddition: true,
            SketchLabelLookup: 'CODE',
            DoNotAllowLabelMove: true,
            EnableMarkedAreaDrawing: true,
            EnableOBYEditlabel: true,
            SketchSource: { Table: null, LabelField: "PARID", CommandField: null, KeyFields: ["PARID"] },
            VectorSource: [{ Table: "OBY", LabelField: "CODE", labelCaption: "Code", CommandField: "VECT", ConnectingFields: ["PARID"], AreaField: "AREA", PerimeterField: null, LabelCommandField: null, IsLabelRequired: false, DimensionCommandField: null, LabelLookup: 'CODE', LabelLookupSelector: { Table: 'OBY', FieldName: 'CODE' } }]
        }

    ],
    BeforeSave: IASUSSketchBeforeSave,
    AfterSave: IASUSSketchAfterSave
}

CAMACloud.Sketching.Configs.IASW_CA = {
    formatter: 'Lucas',
    //LabelAssociateWindow: LucasLabelWindowDefinition,
    sources: [
        {
            Key: "RES",
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            ClearSegmentOnDeletion: true,
            DeleteBlankSegmentsOnSave: true,
            SketchLabelPrefix: "RES -",
            SketchLabelPrefixInSv: "RES",
            AllowLabelEdit: true,
            InsertDataOnAddition: true,
            ShowVectorDetails: true,
          //  SketchLabelLookup: 'OKLOW',
            DoNotAllowLabelMove: true,
            sectionFilterField: 'CARD', 
            DoNotAddOrEditFirstRecordlabel: true,
            EnableMarkedAreaDrawing: true,   
            FirstRecordlabelValue:"STORIES/BSMT",
            SaveExtraLabelFields: true,
            SketchSource: { Table: "DWELDAT", LabelField: "CARD/STYLE/YRBLT", CommandField: null, KeyFields: ["CARD"] },
            VectorSource: [{ Table: "ADDN", LabelField: "LOWER", labelCaption: "Lower", CommandField: "VECT", ConnectingFields: ["CARD"], AreaField: "AREA", PerimeterField: null, LabelCommandField: null, IsLabelRequired: false, DimensionCommandField: null, LabelLookup: 'OKLOW', ExtraLabelFields: [{ Name: "FIRST", LookUp: "OK1", Caption: "First", IsRequired: false }, { Name: "SECOND", Caption: "Second", LookUp: "OK2", IsRequired: false }, { Name: "THIRD", Caption: "Third", LookUp: "OK3", IsRequired: false }] }]
        },
        {
            Key: "COM",
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            ClearSegmentOnDeletion: true,
            DeleteBlankSegmentsOnSave: true,
            SketchLabelPrefix: "COM -",
            SketchLabelPrefixInSv: "COM",
            AllowLabelEdit: true,
            InsertDataOnAddition: true,
            DoNotAllowLabelMove: true,
            EnableMarkedAreaDrawing: true,   
            // SketchLabelLookup: 'Sketch_VL',
            sectionFilterField: 'CARD',
            SaveExtraLabelFields: true,
            SketchSource: { Table: "COMDAT", LabelField: "CARD/BLDNUM/YRBLT", CommandField: null, KeyFields: ["CARD"] },
            VectorSource: [{ Table: "COMINTEXT", LabelField: "SECT", labelCaption: "Sect", CommandField: "VECT", ConnectingFields: ["CARD"], AreaField: "AREA", PerimeterField: 'PERIM', LabelCommandField: null, IsLabelRequired: false, DimensionCommandField: null, EnableFieldValidation: { sourceTable: 'COMINTEXT' }, ExtraLabelFields: [{ Name: "OCCUPANCY", LookUp: 'OCCUPANCY', IsRequired: false, Caption: 'MS Occupancy1' }, { Name: "OCCUPANCY1", LookUp: 'OCCUPANCY1', IsRequired: false, Caption: 'MS Occupancy2' }, { Name: "FLRFROM", LookUp: null, IsRequired: false,Caption: 'From', EnableFieldValidation: { sourceTable: 'COMINTEXT' } }, { Name: "FLRTO", LookUp: null, IsRequired: false,Caption: 'To', EnableFieldValidation: { sourceTable: 'COMINTEXT' }  }] },
                { Table: "COMFEAT", LabelField: "STRUCT", labelCaption: "Struct", CommandField: "VECT", ConnectingFields: ["CARD"], AreaField: "AREA", PerimeterField: null, LabelCommandField: null, IsLabelRequired: false, DimensionCommandField: null, LabelLookup : "STRUCT" }
            ]
        }, {
            Key: "OBY",
            SketchLabelPrefix: "OBY -",
            SketchLabelPrefixInSv: "OBY",
            AllowLabelEdit: true,
            InsertDataOnAddition: true,
            SketchLabelLookup: 'CODE',
            DoNotAllowLabelMove: true,
            EnableMarkedAreaDrawing: true,   
            SketchSource: { Table: null, LabelField: "PARID", CommandField: null, KeyFields: ["PARID"] },
            VectorSource: [{ Table: "OBY", LabelField: "CODE", labelCaption: "Code", CommandField: "VECT", ConnectingFields: ["PARID"], AreaField: "AREA", PerimeterField: null, LabelCommandField: null, IsLabelRequired: false, DimensionCommandField: null, LabelLookup: 'CODE', LabelLookupSelector: { Table: 'OBY', FieldName: 'CODE' } }]
        }

    ],
    BeforeSave: LucasDTRSketchBeforeSave
    /*
    AfterSave: LucasDTRAfterSave*/
}

CAMACloud.Sketching.Configs.BrightCama = {
   formatter: 'BrightCama',
   onSketchSave: brightSketchSave,
   warnings: "If there are non attached sketches, it will not be saved",
   sources: [
    	{
        	AllowSegmentAddition: true,
        	AllowSegmentDeletion: true,
        	InsertDataOnAddition: true,
        	AllowLabelEdit: true,
        	SketchLabelPrefix: "RES -",
            SketchLabelPrefixInSv: "RES",
        	DoNotAllowLabelMove: true,
        	SketchSource: { Table: "ccv_ResSketches", LabelField: 'Account/Sequence', CommandField: null, KeyFields: ["ROWUID"] },
        	VectorSource: [{ Table: "ccv_ResSketches", LabelField: null, CommandField: "cc_sketch", ConnectingFields: ["ROWUID"], AreaField: null, PerimeterField: null, LabelLookup: 'ResSketches' }]
    	},
    	{
        	AllowSegmentAddition: true,
        	AllowSegmentDeletion: true,
        	InsertDataOnAddition: true,
        	AllowLabelEdit: true,
        	DoNotAllowLabelMove: true,
        	SketchLabelPrefix: "COM -",
            SketchLabelPrefixInSv: "COM",
        	SketchSource: { Table: "ccv_ComSketches", LabelField: 'Account/Sequence', CommandField: null, KeyFields: ["ROWUID"] },
        	VectorSource: [{ Table: "ccv_ComSketches", LabelField: null, CommandField: "cc_sketch", ConnectingFields: ["ROWUID"], AreaField: null, PerimeterField: null, LabelLookup: 'ComSketches' }]
    	}
   ]   
}

CAMACloud.Sketching.Configs.AAC = {
    formatter: 'AAC',
    clearModifiedAfterSave: true,
    sources: [
        {
            Key: "RES",
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            SketchLabelPrefix: "RES -",
            SketchLabelPrefixInSv: "RES",
            AllowLabelEdit: true,
            InsertDataOnAddition: true,
            splitByDelimiter:'/',
            SketchSource: { Table: "res_building_tb", LabelField: "bld_id/occupancy_cd/story_cd", CommandField: null, KeyFields: ["ROWUID"] },
            VectorSource: [{ Table: "res_building_tb", LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["ROWUID"], LabelLookup: 'res_sketch_lookup' }],
        	NotesSource: { Table: "Residential_Sketch_Notes", TextField: "Note", PositionXField: "xposition", PositionYField: "yposition", ConnectingFields: ["bld_id"], ScaleFactor: 0.01 }
        },
        {
            Key: "COM",
            AllowSegmentAddition: false,
            AllowSegmentDeletion: true,
            AllowSegmentAdditionByCustomButtons: true,
            SketchLabelPrefix: "COM -",
            SketchLabelPrefixInSv: "COM",
            AllowLabelEdit: true,
            InsertDataOnAddition: true,
            splitByDelimiter:'/',
            SketchSource: { Table: "comm_bldg_tb", LabelField: "bldg_name/predomiant_use_cd", CommandField: null, KeyFields: ["ROWUID"] },
            VectorSource: [{ Table: "comm_bldg_tb", LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["ROWUID"], LabelLookup: 'com_sketch_lookup' }],
            NotesSource: { Table: "Combldg_Sketch_Notes", TextField: "Note", PositionXField: "xposition", PositionYField: "yposition", ConnectingFields: ["bldg_id"], ScaleFactor: 0.01 },
            Buttons: [
	            {
	                Text: 'Add Segment',
	                Action: 'createNewVector',
	                Data: { vectorSourceTable: 'comm_bldg_tb'},
	                Modes: ['MODE_DEFAULT'],
	                Requires: 'currentSketch',
	                DisplayCondition: 'tb.editor.currentSketch.config.SketchSource.Table == "comm_bldg_tb"'
	            }
        	]
        }
    ], 
    AfterSave: AACAfterSave
}

CAMACloud.Sketching.Configs.PVD_Manage_CC = {
    formatter: 'RapidSketch',
    isPVDConfig: true,
    doNotDecodeEncode: true,
    sources: [
        {
            //HideNullSketches: true,
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            AllowLabelEdit: true,
            //AllowTransferOut: true,
            SaveAllSegments: true,
            SketchSource: { Table: "Buildings", LabelField: "Building/Description/ResidenceType %# CommercialType2/YearConstrcuted", CommandField: null, KeyFields: ["ROWUID"] },
            VectorSource: [{ Table: "Buildings", LabelField: null, CommandField: "UDF_CC_SKETCH", ConnectingFields: ["ROWUID"], AreaField: null, PerimeterField: null, LabelLookup: 'Sketch_Lookup' }]
        }]
}

CAMACloud.Sketching.Configs.HelionRS = {
    formatter: 'RapidSketch',
    doNotDecodeEncode: true,
    isHelionRS: true,
    AddNewNote: true,
    ModifyIfNoteChanged: true,
    sources: [
        {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            InsertDataOnAddition: true,
            SaveAllSegments: true,
            AllowLabelEdit: true,
            SketchSource: { Table: 'CC_APPRAISAL_DRAWING', LabelField: "SOURCE/ACCOUNT_ID/NAME", CommandField: null, KeyFields: ["ROWUID"] },
            VectorSource: [{ Table: 'CC_APPRAISAL_DRAWING', LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["ROWUID"], AreaField: null, PerimeterField: null, LabelLookup: 'SKETCH_CODES', SketchProField: 'SketchProData' }]
        }
    ],
    BeforeSave: HelionRSBeforeSave
}

CAMACloud.Sketching.Configs.MSGovern = {
    formatter: 'ApexJson',
    ApexFreeFormTextLabels: true,
    IsJsonFormat: true,
    sources: [
        {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            AllowLabelEdit: true,
            AssignLookUpNameAsLabel: true,
            IsLargeLookup: true,
            SketchSource: { Table: "CCV_MA_Buildings_2_3_BLDG_INFO", LabelField: "BLDG_ID/BLDG_SEQ/BLDG_MODEL_CODE/BLDG_USE_CODE/STRDESIGN", CommandField: null, KeyFields: ["BLDG_ID"] },
            VectorSource: [{ Table: "CCV_MA_Buildings_2_3_BLDG_INFO", LabelField: null, CommandField: "APEX_SKETCH", ConnectingFields: ["BLDG_ID"], AreaField: null, PerimeterField: null, LabelAreaPosition: true, LabelTarget: "code", LabelLookup: 'Apex_Area_Codes' }]
        }
    ],
    AfterSave: MSGovernAfterSave
}

CAMACloud.Sketching.Configs.RapidSketchPACSWA = {
    formatter: 'RapidSketch2',
    AppendSketchPrefixInSVData: true,
    sources: [
        {
            Name: "imprv_detail_sketch",
            Key: "PID",
            SketchLabelPrefix: "Det",
            SketchLabelPrefixInSv: "Det",
            AllowTransferIn: true,
            //AllowMultiSegmentAddDelete: true,
            AllowSegmentAddition: true,
            DoNotAllowLabelMove: true,
            InsertDataOnAddition: true,
            DoNotHeaderAddition: true,
            IsLargeLookup: true,
            ShowYearBuilt: true,
            AllowLabelEdit: true,
            SketchLabelLookup: 'imprv_det_type_cd',
            SketchSource: { Table: "imprv", LabelField: "imprv_type_cd/primary_use_cd", CommandField: null, KeyFields: ["imprv_id"] },
            VectorSource: [{ Table: "imprv_detail", IdField: "imprv_det_id", LabelField: "imprv_det_type_cd", CommandField: "CC_SKETCH", ConnectingFields: ["imprv_id"], AreaField: 'sketch_area', PerimeterField: 'perimeter', LabelLookup: 'imprv_det_type_cd', labelCaption: 'Type' }],
            NotesSource: { Table: "imprv_sketch_note", TextField: "NoteText", PositionXField: "xLocation", PositionYField: "yLocation", ConnectingFields: ["imprv_id"], ScaleFactor: 0.01 }
        },
        {
            Name: "imprv_sketch",
            Key: "PI",
            SketchLabelPrefix: "Imp",
            SketchLabelPrefixInSv: "Imp",
            HideNullSketches: true,
            InsertDataOnAddition: true,
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            AllowLabelEdit: true,
            AllowTransferOut: true,
            DoNotAllowLabelMove: true,
            SaveAllSegments: true,
            SketchLabelLookup: null,
            IsLargeLookup: true,
            SketchLabelLookup: 'imprv_det_type_cd',
            SketchSource: { Table: "imprv", LabelField: "imprv_type_cd/primary_use_cd", CommandField: "CC_SKETCH", KeyFields: ["imprv_id"] },
            VectorSource: [{ Table: "imprv", IdField: null, LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["imprv_id"], AreaField: null, PerimeterField: null, LabelLookup: 'imprv_det_type_cd' }]
        },
        {
            Name: "property_sketch",
            Key: "P",
            SketchLabelPrefix: "Prop",
            SketchLabelPrefixInSv: "Prop",
            HideNullSketches: true,
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            AllowLabelEdit: true,
            AllowTransferOut: true,
            DoNotAllowLabelMove: true,
            SaveAllSegments: true,
            IsLargeLookup: true,
            SketchLabelLookup: 'imprv_det_type_cd',
            SketchSource: { Table: null, LabelField: "prop_id", CommandField: "CC_SKETCH", KeyFields: ["prop_id"] },
            VectorSource: [{ Table: null, IdField: null, LabelField: null, CommandField: "CC_SKETCH", ConnectingFields: ["prop_id"], AreaField: null, PerimeterField: null, LabelLookup: 'imprv_det_type_cd' }]
        }
    ]
}

CAMACloud.Sketching.Configs.WrightJson = {
    formatter: 'ApexJson',
    ApexFreeFormTextLabels: true,
    IsJsonFormat: true,
    IsLabelAssociate: true,
    clearModifiedAfterSave: true,
    IsWrightSketch: true,
    LabelAssociateWindow: WrightLabelWindowDefinition,
    sources: [
        {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            AllowLabelEdit: true,
            InsertDataOnAddition: true,
            AssignLookUpNameAsLabel: true,
            SketchSource: { Table: null, LabelField: "PARID", CommandField: null, KeyFields: ["PARID"] },
            VectorSource: [{ Table: null, LabelField: null, labelCaption: "code", CommandField: "CC_SKETCH", ConnectingFields: ["PARID"], LabelAreaPosition: true, LabelTarget: "code", LabelLookup: 'Wright_Sketch_Lookup' }]
        }
    ],
    AfterSave: wrightAfterSave
}
