﻿//    DATA CLOUD SOLUTIONS, LLC ("COMPANY") CONFIDENTIAL 
//    Unpublished Copyright (c) 2010-2013 
//    DATA CLOUD SOLUTIONS, an Ohio Limited Liability Company, All Rights Reserved.
//    NOTICE: All information contained herein is, and remains the property of COMPANY.
//    The intellectual and technical concepts contained herein are proprietary to COMPANY
//    and may be covered by U.S. and Foreign Patents, patents in process, and are protected
//    by trade secret or copyright law. Dissemination of this information or reproduction
//    of this material is strictly forbidden unless prior written permission is obtained
//    from COMPANY. Access to the source code contained herein is hereby forbidden to
//    anyone except current COMPANY employees, managers or contractors who have executed
//    Confidentiality and Non-disclosure agreements explicitly covering such access.</p>
//    The copyright notice above does not evidence any actual or intended publication
//    or disclosure of this source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
//    DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
//    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
//    IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
//    OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
//    TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
//    ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
/*
 ** CAMA Cloud Sketch Editor - Version 1.5
 ** HTML5 Sketch Editor for CAMA Cloud Sketches - Desktop & Mobile Version
 ** Developer: Sarath
 */
var CAMACloud = {}
if (CAMACloud === undefined) CAMACloud = {};
var sketchCache = {};
var sketchopened = false;
var DEFAULT_PPF = 10;
var ARC_SIGN = 1;
var DISTANCE_UNIT = ''
var AREA_UNIT = 'sq. ft'
var vectorLabelPositions = [];
var _moving = false;
var undoProcessArray = [];
var nodeNumber = null;
var fvEnabled;
var colorCodevalidationAlert = false;
var _sdp = 2; //clientsettings sketch decimal value, used to changed toFixed case.

//Quickbutton Variables
var _drawPoly = false;
var shape_coordinates = {};
var _polycoorinates = [];
var _arcCoorinates = [];
var _quickShape = {};
var _drawshape = false;
var _arclen = 10;

CAMACloud.Sketching = {
    MODE_DEFAULT: 0x0000,
    MODE_NEW: 0x0001,
    MODE_EDIT: 0x0002,
    MODE_MOVE: 0x0003,
    MODE_ERASER: 0x0100,
    MODE_SPLITTER: 0x0110,
    MODE_SCROLL: 0x9001,
    MODE_ZOOM: 0x9010,
    MODE_SCRIBBLE: 0x8001,
    MODE_ERASESCRIBBLE: 0x8010
};
var windowsTouch = /Win/.test(window.navigator.userAgent) ? (window.navigator.msMaxTouchPoints || ("ontouchstart" in window)) ? true : false : false;
CAMACloud.Sketching.Formatters = {};
CAMACloud.Sketching.Encoders = {};
CAMACloud.Sketching.roundFactor = null;

function CCSketchEditor(canvas, formatter) {

    console.log('Initializing Sketch Editor');

    if (!formatter) formatter = CAMACloud.Sketching.Formatters.CCSketch;
    this.showOrigin = true;
    this.formatter = formatter;
    this.encoder = null;
    var _mode = CAMACloud.Sketching.MODE_DEFAULT;
    var editor = this;
    this.scale = 1;
    this.origin = new PointX(0, 0);
    this.offset = new PointX(0, 0);
    this.showStatus = function (message) {
        return false;
    }
    this.setZoomValue = function (z) {
        return false;
    }
    var ccse;
    this.drawing = null;
    this.brush = null;
    this.width = 500;
    this.height = 400;
    this.origin = RoundX(0, 0);
    this.actualOrigin = RoundX(0, 0);
    this.refOrigin = new PointX(this.origin.x, this.origin.y);
    this.keypadActive = false;
    this.vectorSelector = null;
    this.sketchSelector = null;
    this.pageSelector = null;
    this.viewOnly = false;
    this.saving = false;
    this.sketchMovable = false;
    this.displayNotes = true;
    this.isPreview = false;
    this.undoChanges = [];
    this.external = {
        datafields: {},
        lookup: {},
        fieldCategories: {}
    }

    this.setFormatter = function (ftr) {
        formatter = ftr;
        this.formatter = ftr;
    }

    this.minZoom = 0.05;
    this.maxZoom = 2
    this.__defineGetter__("zoomRange", function () {
        return this.maxZoom - this.minZoom;
    });

    this.pen = false;
    this.ink = false;
    this.currentSketch = null;
    this.currentVector = null;
    this.currentNode = null;
    this.currentLine = null;
    this.currentSketchNote = null;
    this.vectors = [];
    this.deletedVectors = [];
    this.sketches = [];
    this.angleLock = false;
    this.keypad = null;
    this.toolbar = null;
    this.isRefreshAfterSave = false;

    this.__defineGetter__("mode", function () {
        return _mode;
    });
    this.__defineSetter__("mode", function (val) {
        _mode = val;
    });

    this.onModeChange = function (mode, currentVector, currentNode, currentSketchNote) { }
    this.onDrawLine = function (p1, p2, v) { }
    this.onClose = function () { }
    this.onSave = function (data) { }

    var _config;

    this.__defineGetter__("config", function () {
        return _config;
    });
    this.__defineSetter__("config", function (v) {
        _config = v; editor.toolbar && editor.toolbar.applyConfig(v);
    });

    // Initialize Canvas

    function init() {
        if (typeof canvas == "string") {
            ccse = $(canvas)[0];
        } else {
            ccse = canvas;
        }
        if (!ccse) {
            return;
        }
        editor.drawing = ccse.getContext("2d");
        editor.width = $(ccse).width();
        editor.height = $(ccse).height();
        editor.origin = GetOrigin(editor, editor.formatter.originPosition)
        editor.refOrigin = new PointX(editor.origin.x, editor.origin.y);
        editor.actualOrigin = new PointX(editor.origin.x, editor.origin.y);
        editor.drawing.imageSmoothingEnabled = true;
        editor.brush = new Brush(editor.drawing, editor);

        ccse.editor = editor;
        setCanvasEvents(ccse, editor);
    }

    this.refresh = function (_scale) {
        editor.scale = _scale;
        editor.width = $(ccse).width();
        editor.height = $(ccse).height();
        editor.origin = GetOrigin(editor, editor.formatter.originPosition)
        editor.refOrigin = new PointX(editor.origin.x, editor.origin.y);
        editor.actualOrigin = new PointX(editor.origin.x, editor.origin.y);
        editor.brush = new Brush(editor.drawing, editor);
        editor.zoom(_scale);
    }

    this.startNewVector = function (forceStart) {
        if (_mode == CAMACloud.Sketching.MODE_DEFAULT || forceStart) {
            _mode = CAMACloud.Sketching.MODE_NEW;
            this.isClosed = false;
            this.raiseModeChange();
        }
    }

    this.raiseModeChange = function () {
        if (this.onModeChange)
            this.onModeChange(_mode, this.currentVector, this.currentNode, this.currentSketchNote);
    }

    // Editor Functions
    this.clearCanvas = function () {
        this.drawing.clearRect(0, 0, ccse.width, ccse.height);
    }

    this.zoomPercent = function (zp) {
        var z = (zp / 100 * editor.zoomRange) + editor.minZoom;
        editor.zoom(z);
    }

    this.zoom = function (z, p) {
        z = editor.normalizeScale(z);
        editor.scale = z;
        editor.setZoomValue(Math.round(z * 100));
        czoom.updateRange((Math.round(z * 100)) / 2)
        editor.brush.setScale(this.scale);
        editor.render();
    }

    this.setZoom = function (z) {

    }

    this.toDataURL = function () {
        return ccse.toDataURL();
    }
    this.redo = function () {
        var p = this.undoChanges.pop();
        if (p) {
            if (p && this.currentNode) {
                sketchApp.currentVector.connect(p, true);
                $('.ccse-meter-length').val(sRound(this.currentVector.endNode.length || 0))
                $('.ccse-meter-angle').val('');
            }
            else
                sketchApp.currentVector.start(p, true)
            this.render()
        }
    }
    this.undo = function () {
        if (this.currentVector != null)
            if (this.currentVector.endNode) {
                if (this.currentVector.startNode && (this.currentVector.endNode == this.currentVector.startNode)) {
                    var sn = this.currentVector.startNode;
                    this.currentVector.endNode = null;
                    this.currentVector.startNode = null;
                    delete sn;
                    this.undoChanges.push(sn.p);
                } else {
                    var nn = this.currentVector.endNode;
                    var pp = nn.prevNode;
                    this.currentVector.endNode = pp;
                    pp.nextNode = null;
                    delete nn;
                    $('.ccse-meter-length').val(sRound(this.currentVector.endNode.length || 0))
                    $('.ccse-meter-angle').val('');
                    this.undoChanges.push(nn.p);
                }
                this.currentNode = this.currentVector.endNode;
                this.render();
            }
    }

    this.normalizeScale = function (z) {
        if (z > editor.maxZoom) {
            return editor.maxZoom;
        } else if (z < editor.minZoom) {
            return editor.minZoom;
        } else {
            return z;
        }
    }

    this.resetOrigin = function (changeOriginPosition, beforeAfterView) {
        if (!beforeAfterView && !changeOriginPosition && this.isOpenSketchEditorTrue)
            return;
        this.origin = this.actualOrigin.copy();
        this.refOrigin = this.origin.copy();
        //this.render();
    }

    this.panOrigin = function (sb, originPosition, scale, changeOriginPositions, beforeAfterViews) {
        if (!beforeAfterViews && !changeOriginPositions && this.isOpenSketchEditorTrue)
            return;
        if (!scale)
            scale = this.scale;
        if (originPosition == "topRight") {
            x = -sb.xmax * DEFAULT_PPF * scale;
            y = -sb.ymax * DEFAULT_PPF * scale;
        }
        else if (originPosition == "topLeft") {
            x = -sb.xmin * DEFAULT_PPF * scale;
            y = -sb.ymax * DEFAULT_PPF * scale;
        }
        else {
            x = -sb.xmin * DEFAULT_PPF * scale;
            y = -sb.ymin * DEFAULT_PPF * scale
        }
        this.offset = new PointX(x, y);
        this.offset = RoundX(this.offset.x, this.offset.y, scale);
        this.origin.setOffset(this.offset);
        this.refOrigin = this.origin.copy();
        this.render();
    }

    this.pan = function (x, y) {
        this.offset = new PointX(x, y);
        this.origin.setOffset(this.offset);
        this.render();
    }

    this.resetPan = function (x, y) {
        this.origin = this.refOrigin.copy();
        this.render();
    }

    this.toggleSketchMovable = function () {
        this.sketchMovable = !this.sketchMovable;
        this.render();
    }

    this.resetSketchMovable = function () {
        if (this.sketchMovable) {
            this.sketchMovable = false;
            $('.ccse-sketch-move').html('Move');
            this.render();
        }
    }

    this.panUp = function () {
        editor.pan(0, 50);
    }
    this.panDown = function () {
        editor.pan(0, -50);
    }
    this.panLeft = function () {
        editor.pan(-50, 0);
    }
    this.panRight = function () {
        editor.pan(50, 0)
    }
    this.panReset = function () {
        editor.resetPan();
    }

    this.attachPanControl = function (cpan) {
        cpan.panLeft = editor.panLeft;
        cpan.panRight = editor.panRight;
        cpan.panUp = editor.panUp;
        cpan.panDown = editor.panDown;
        cpan.panReset = editor.panReset;
    }

    this.attachZoomControl = function (czoom) {
        czoom.zoom = editor.zoomPercent;
        editor.setZoom = czoom.setZoom;
    }

    this.attachKeypad = function (ckeypad) {
        editor.keypad = ckeypad;
    }

    this.refreshControlList = function () {
        if (!this.sketchSelector || !this.vectorSelector) return false;

        if (this.sketchSelector) {
            this.sketchSelector.html('');
            var nsel = document.createElement('option');
            nsel.innerHTML = '-- Select --';
            nsel.setAttribute('value', '');
            this.sketchSelector[0].appendChild(nsel);
        }

        if (this.pageSelector) {
            this.pageSelector.html('');
            var nsel = document.createElement('option');
            nsel.innerHTML = '--';
            nsel.setAttribute('value', '');
            this.pageSelector[0].appendChild(nsel);
        }

        if (this.vectorSelector) {
            this.vectorSelector.html('');
            var nsel = document.createElement('option');
            nsel.innerHTML = '-- Select Vector --';
            nsel.setAttribute('value', '');
            this.vectorSelector[0].appendChild(nsel);
        }
        for (var s in this.sketches) {
            var sk = this.sketches[s];

            if (this.sketchSelector) {
                if (this.sketchSelector.length > 0) {
                    var opt = document.createElement('option');
                    opt.innerHTML = sk.label || ('&lt;No Label&gt;' + sk.uid);
                    opt.setAttribute('value', sk.uid);
                    if (this.currentSketch != null) {
                        if (sk.uid == this.currentSketch.uid) {
                            opt.setAttribute('selected', 'selected');
                        }
                    }
                    this.sketchSelector[0].appendChild(opt);
                }
            }
        }

        $(this.sketchSelector).unbind('change');
        $(this.sketchSelector).bind('change', function (event) {
            var changeOrigin = false;
            undoProcessArray = [];
            if (editor.ischangeSketchOriginPosition)
                changeOrigin = true;
            editor.zoom(1);
            event.preventDefault();
            if (editor.config && editor.config.IsJsonFormat)
                editor.loadPageSelector(this)
            else
                editor.loadVectorSelector(this);
            if (!editor.isRefreshAfterSave) {
                editor.panSketchPosition(!changeOrigin);
                editor.mode = CAMACloud.Sketching.MODE_DEFAULT;
            } else
                editor.isRefreshAfterSave = false;
            editor.raiseModeChange();
            editor.render();
            if (editor.config?.isHelionRS && editor.currentSketch) $('.ccse-delete-page').css('display', 'inline-block');
            else $('.ccse-delete-page').hide();
        });

        $(this.pageSelector).unbind('change');
        $(this.pageSelector).bind('change', function (event) {
            var changeOrigin = false;
            undoProcessArray = [];
            if (editor.ischangeSketchOriginPosition)
                changeOrigin = true;
            let pageNm = this.value ? parseInt(this.value) : this.selectedIndex + 1;
            editor.currentSketch.currentPage = pageNm;
            editor.zoom(1);
            event.preventDefault();
            editor.loadVectorSelector($('.ccse-sketch-select'), null, pageNm);
            if (!editor.isRefreshAfterSave) {
                editor.panSketchPosition(!changeOrigin);
                editor.mode = CAMACloud.Sketching.MODE_DEFAULT;
            } else
                editor.isRefreshAfterSave = false;
            editor.raiseModeChange();
            editor.render();
        });

        $(this.vectorSelector).unbind('change');
        $(this.vectorSelector).bind('change', function (event) {
            event.preventDefault();
            var oneSelected = false;
            undoProcessArray = [];
            if (!editor.currentSketch) return;
            if ($(this).val() == "") {
                sketchApp.currentNode = null;
                sketchApp.currentLine = null;
            }
            for (var x in editor.currentSketch.vectors) {
                var v = editor.currentSketch.vectors[x];
                if ($(this).val() == v.uid) {
                    editor.currentVector = v;
                    editor.currentNode = null;
                    editor.currentLine = null;
                    if (sketchApp.CC_beforeSketch) {
                        if (editor.currentVector.isClosed || editor.currentVector.isFreeFormLineEntries) {
                            editor.mode = CAMACloud.Sketching.MODE_EDIT;
                        }
                        oneSelected = true;
                    }
                    else {
                        if ((v.toString() == "")) {
                            editor.promptAndCreateVector();
                        } else if (editor.currentVector.isClosed || editor.currentVector.isFreeFormLineEntries) {
                            editor.mode = CAMACloud.Sketching.MODE_EDIT;
                        } else {
                            editor.promptAndContinueVector();
                        }
                        oneSelected = true;
                    }
                }
            }
            if (!oneSelected) {
                editor.currentVector = null;
                editor.mode = CAMACloud.Sketching.MODE_DEFAULT;
            }
            editor.raiseModeChange();
            editor.render();
        });

    }
    this.panSketchPosition = function (DLLChange) {
        var sb = editor.sketchBounds();
        var ss = editor.screenBounds();
        var gg, hh = 1,
            pp, xx = 1;
        var ysketch = sb.ymax + Math.abs(sb.ymin);
        var yscreen = ss.uy + Math.abs(ss.ly);
        var xsketch = sb.xmax + Math.abs(sb.xmin);
        var xscreen = ss.ux + Math.abs(ss.lx);
        if (ysketch > yscreen) {
            gg = (yscreen * editor.scale) / ysketch;
            hh = Math.ceil(gg / .05) * .05;
        }
        if (xsketch > xscreen) {
            pp = (xscreen * editor.scale) / xsketch;
            xx = Math.ceil(pp / .05) * .05;
        }
        if (xx > hh) editor.zoom((hh - .1))
        else if (xx < hh) editor.zoom((xx - .1))
        else if (xx == hh && xx < 1) editor.zoom((xx - .1))
        if (ysketch == 0 && xsketch == 0)
            editor.zoom(1);
        editor.resetOrigin(DLLChange);
        editor.panOrigin(sb, editor.formatter.originPosition, null, DLLChange);
    }
    this.loadVectorSelector = function (ss, cv, curPage) {
        var oneSelected = false;

        if (editor.vectorSelector) {
            editor.vectorSelector.html('');
            var nsel = document.createElement('option');
            nsel.innerHTML = '-- Select --';
            nsel.setAttribute('value', '');
            editor.vectorSelector[0].appendChild(nsel);
        }
        clearVectorlabelPosition();
        var index = ss && ss.selectedIndex
        if (curPage) index = $('.ccse-sketch-select')[0].selectedIndex;
        if (editor.config?.IsWrightSketch && !index && curPage == 0) {
            oneSelected = true;
        }
        if (index) {
            index -= 1;
            var s = editor.sketches[index];
            editor.currentSketch = s;
            editor.currentSketch.currentPage = curPage;
            editor.currentVector = cv;
            editor.currentNode = null;
            oneSelected = true;
            for (var x in s.vectors) {
                /*MA_2479*/
               /* if (s.vectors[x].marea != true) {*/
                    if (editor.config && editor.config.IsJsonFormat) {
                        if (s.vectors[x].pageNum && curPage && s.vectors[x].pageNum == curPage)
                            s.vectors[x].render();
                        else if (!curPage)
                            s.vectors[x].render();
                    }
                    else
                        s.vectors[x].render();
                    if (editor.vectorSelector) {
                        if (editor.vectorSelector.length > 0) {
                            var v = null;
                            if (editor.config && editor.config.IsJsonFormat) {
                                if (s.vectors[x].pageNum && curPage && s.vectors[x].pageNum == curPage)
                                    v = s.vectors[x];
                                else if (!curPage)
                                    v = s.vectors[x];
                                if (!v)
                                    continue;
                            }
                            else
                                v = s.vectors[x];
                            //if(v.isFreeFormLineEntries)
                            //continue;
                            var opt = document.createElement('option');
                            var label;
                            if (sketchSettings['ShowOtherImprovementDescLabel'] == '1' && v.sketchType == 'outBuilding' && editor.mvpDisplayLabel)
                                label = v.mvpDisplayLabel;
                            else if ((sketchSettings['ShowOI_IDandLabel'] == 1 || sketchSettings['ShowOI_IDandLabel'] == 0) && v.sketchType == 'outBuilding') {
                                var count = v.label.split(' ')[0];
                                if (v.mvpData && v.mvpData['Improvement_ID'] && v.mvpData['Improvement_ID'].split('{').length < 2)
                                    label = count + ' ' + v.mvpData['Improvement_ID'] + '-' + v.name;
                                else
                                    label = v.label
                            }
                            else
                                label = v.label
                            opt.innerHTML = label || ('&lt;No Label&gt;' + v.uid);
                            if (v && v.isFreeFormLineEntries) {
                                opt.innerHTML = opt.innerHTML + 'Line';
                            }
                            opt.setAttribute('value', v.uid);
                            if (editor.currentVector != null) {
                                if (v.uid == editor.currentVector.uid) {
                                    opt.setAttribute('selected', 'selected');
                                }
                            }
                            editor.vectorSelector[0].appendChild(opt);
                        }
                    }
               /* }*/
            }


        }

        if (!oneSelected) {
            editor.currentSketch = null;
        }
    }

    this.loadPageSelector = function (ss, cv) {
        var oneSelected = false;
        if (editor.config && editor.config.IsJsonFormat) {
            if (editor.pageSelector) {
                editor.pageSelector.html('');
                $('.ccse-page-select').css('display', 'inline-block');
                $('.ccse-create-newpage').css('display', 'inline-block');
                $('.ccse-delete-newpage').css('display', 'inline-block');
            }
            var index = ss && ss.selectedIndex
            if (index && index > 0) {
                var pageNum = index
                index -= 1;
                var s = editor.sketches[index];
                editor.currentSketch = s;
                editor.currentVector = cv;
                editor.currentNode = null;
                oneSelected = true;
                var page = [];
                for (var x in s.vectors) {
                    if (editor.vectorSelector && editor.pageSelector) {
                        if (editor.vectorSelector.length > 0 && editor.pageSelector.length > 0) {
                            var v = null;
                            v = s.vectors[x];
                            if (v.pageNum && (page.indexOf((v.pageNum).toString()) == -1)) {
                                page.push((v.pageNum).toString());
                            }
                        }
                    }
                }
                for (var x in s.notes) {
                    if (editor.vectorSelector && editor.pageSelector) {
                        if (editor.vectorSelector.length > 0 && editor.pageSelector.length > 0) {
                            var n = null;
                            n = s.notes[x];
                            if (n.pageNum && (page.indexOf((n.pageNum).toString()) == -1)) {
                                page.push((n.pageNum).toString());
                            }
                        }
                    }
                }
                var pageLength = Math.max.apply(null, page);
                let wp = true, fp = 1;
                if (editor.config?.IsWrightSketch) {
                    let parcel = window.opener ? window.opener.activeParcel : activeParcel, edRecs = (editor.createdWrightSketchCards ? editor.createdWrightSketchCards : []),
                        recs = parcel.CCV_DWELDAT_APEXSKETCH.filter((x) => { return x.CC_Deleted != true }).concat(parcel.CCV_COMDAT_APEXSKETCH.filter((x) => { return x.CC_Deleted != true }), edRecs), delRec = editor.deletedWrightSketchCards ? editor.deletedWrightSketchCards : [];

                    recs.forEach((x) => {
                        if (x.CARD && (page.indexOf((x.CARD).toString()) == -1) && !delRec.filter((y) => { return y.Card == x.CARD })[0]) page.push(x.CARD);
                    });
                    page = page.sort((a, b) => a - b);
                    pageLength = Math.max.apply(null, page);
                    if (pageLength >= 1) {
                        page.forEach((i) => {
                            let pVal = ((i).toString()), tp = '', drec = parcel['CCV_DWELDAT_APEXSKETCH'].concat(edRecs.filter((x) => { return x.SoureTable == 'CCV_DWELDAT_APEXSKETCH' })),
                                crec = parcel['CCV_COMDAT_APEXSKETCH'].concat(edRecs.filter((x) => { return x.SoureTable == 'CCV_COMDAT_APEXSKETCH' }));

                            let rr = drec.filter((x) => { return x.CARD == pVal && x.CC_Deleted != true })[0];
                            if (rr) tp = 'Dwell';
                            else {
                                rr = crec.filter((x) => { return x.CARD == pVal && x.CC_Deleted != true })[0];
                                tp = rr ? 'Com  ' : '';
                            }

                            let ht = tp ? tp + ' - ' + pVal : pVal;
                            let opt = document.createElement('option');
                            opt.innerHTML = ht;
                            opt.setAttribute('value', pVal);
                            editor.pageSelector[0].appendChild(opt);
                        });
                        fp = editor.pageSelector[0].value;
                    }
                    else {
                        wp = false;
                        let opt = document.createElement('option');
                        opt.innerHTML = '-- None --';
                        opt.setAttribute('value', 0);
                        editor.pageSelector[0].appendChild(opt);
                    }
                    $('.ccse-page-select').css({ "width": "80px", "height": "24px"});
                }
                else {
                    if (pageLength >= 1) {
                        var opt;
                        for (i = 1; i <= pageLength; i++) {
                            var pVal = ((i).toString());
                            opt = document.createElement('option');
                            opt.innerHTML = pVal;
                            opt.setAttribute('value', pVal);
                            editor.pageSelector[0].appendChild(opt);
                        }
                    }
                    else {
                        let iopt;
                        let i = 1;
                        let ipVal = ((i).toString());
                        iopt = document.createElement('option');
                        iopt.innerHTML = ipVal;
                        iopt.setAttribute('value', ipVal);
                        editor.pageSelector[0].appendChild(iopt);
                    }
                }
                wp = wp ? fp : null;
                editor.loadVectorSelector(ss, null, wp);
            }
            else {
               // $('.ccse-page-select').hide();
                editor.loadVectorSelector(ss, null);
            }
            if (!oneSelected) {
                editor.currentSketch = null;
            }
        }
        else
            editor.loadVectorSelector(ss);
    }

    this.promptAndCreateVector = function () {
        if (!(window.opener && window.opener.appType == "sv" && (window.opener.lightWeight || (clientSettings && clientSettings["SketchValidationEditSketch"] != "1")))) {
            messageBox('The selected segment does not have a sketch definition. Proceed to draw a new one? ', ["Yes", "No"], function () {
                messageBox('Tap on any point to start a new sketch segment.', function () {
                    editor.mode = CAMACloud.Sketching.MODE_DEFAULT;
                    $('.ccse-meter-length').val('');
                    $('.ccse-meter-angle').val('');
                    /*if ( editor.config && editor.config.EnableBoundary && editor.config.IsProvalConfig && editor.currentSketch ) {
                        var _scale = editor.currentSketch.boundaryScale? editor.currentSketch.boundaryScale: 100;
                        $('.ccse-scale').val(_scale);
                    }
                    else 
                        $('.ccse-scale').val('');*/

                    editor.drawlabelOnly = false;
                    let cv = editor.currentVector;
                    if (cv) {
                        cv.fixedArea = cv.areaFieldValue = null;
                        cv.isUnSketchedArea = cv.hideAreaValue = cv.isClosed = cv.isMarkedArea = cv.noVectorSegment = cv.CheckFixedArea = false;
                    }

                    editor.startNewVector();
                    if (editor.currentVector) {
                        if (editor.config && editor.config.IsBldgNew) editor.currentVector.BldgIsNew = true;
                        editor.currentVector.startNode = null;
                    }
                    return;
                })
            }, function () {
                if (editor.currentSketch && editor.currentSketch.config && editor.currentSketch.config.EnableOBYEditlabel) {
                    editor.mode = CAMACloud.Sketching.MODE_EDIT;
                    editor.raiseModeChange();
                    $('.ccse-mode-select-vector, .ccse-mode-select-node').css({ 'display': "none" });
                }
                else {
                    editor.mode = CAMACloud.Sketching.MODE_DEFAULT;
                    editor.raiseModeChange();
                }
            });
        }
    }

    //this.promptAndContinueVector = function (callback) {
    //    messageBox('The selected segment is not a closed shape. Do you want to continue drawing? ', ["Yes", "No"], function () {
    //        messageBox('Tap on any point to continue drawing.', function () {
    //            console.log("sadf");
    //            editor.currentNode = editor.currentVector.endNode  ;
    //            editor.mode = CAMACloud.Sketching.MODE_NEW;
    //            editor.raiseModeChange();
    //            if (callback) callback();
    //        })
    //    }, function () {
    //        editor.mode = CAMACloud.Sketching.MODE_EDIT;
    //        editor.raiseModeChange();
    //    });
    //}

    this.promptAndContinueVector = function (callback) {
        if (!(window.opener && window.opener.appType == "sv" && (window.opener.lightWeight || (clientSettings && clientSettings["SketchValidationEditSketch"] != "1")))) {
            messageBox('The selected segment is not a closed shape. Do you want to continue drawing? ', ["Yes", "No"], function () {
                messageBox('Tap on any point to continue drawing.', function () {
                    editor.currentNode = editor.currentVector.endNode;
                    editor.mode = CAMACloud.Sketching.MODE_NEW;
                    editor.raiseModeChange();
                    if (callback) callback();
                })
            }, function () {
                if (editor.currentVector) {
                    editor.mode = CAMACloud.Sketching.MODE_EDIT;
                    editor.raiseModeChange();
                    editor.render();
                }
                else {
                    editor.mode = CAMACloud.Sketching.MODE_DEFAULT;
                }
           });
        }
    }

    this.createNewSketchNote = function () {
        var noteMaxLength = editor.currentSketch.maxNotelen;
        var disableNoteCharacters = editor.config.DisableNoteCharacters ? editor.config.DisableNoteCharacters : [];
        disableNoteCharacters.push(220);

        var newNotecreate = function (newnote, customParameters) {
            var vnum = 0;
            if (editor.currentSketch.notes.length > 0) {
                vnum = editor.currentSketch.notes.length;
            }
            var id = editor.currentSketch.uid + "/" + vnum;
            var note = new Annotation(editor, editor.currentSketch, newnote, new PointX(0, 0), new PointX(0, 0), new PointX(100, 100), id.toString());
            note.isModified = true;
            if (sketchApp && sketchApp.config && (sketchApp.config.IsJsonFormat || sketchApp.config.EnableVisionNotes || sketchApp.config.ModifyIfNoteChanged))
                sketchApp.currentSketch.isModified = true;
            note.newRecord = true;
            note.clientId = ccTicks().toString();
            if (editor.config && editor.config.IsJsonFormat && editor.currentSketch && editor.currentSketch.currentPage)
                note.pageNum = editor.currentSketch.currentPage;

            for (k in customParameters) {
                note[k] = customParameters[k];
            }
            editor.currentSketch.notes.push(note);
            editor.render();
            if ($('.ccse-mode-notes').css('display') == 'none') {
                $('.ccse-mode-notes').css({
                    'display': "inline-block"
                });

                if (editor.displayNotes) {
                    $('.ccse-notes-toggle').html('Hide Notes');
                }
                else {
                    $('.ccse-notes-toggle').html('Show Notes');
                }
            }
        }

        if (editor.config.CustomNotePopup) {
            let customHtml = '<div class="wh-div" style="margin-bottom: 10px;height: 30px;"><label>Width: <input type="number" min="1" max="100" style="height: 25px; width: 60px; margin-left: 39px;"></label><label style="margin-left: 40px;">Height: <input type="number" style="height: 25px; width: 60px; margin-left: 39px;" min="1" max="100"></label></div>',
                sectionList = getSectionNumHtml(editor);
            customHtml += '<div class="sec-div" style="margin-bottom: 10px;height: 30px;">' + sectionList.secthtml + '</div>';

            createCustomNote(noteMaxLength, customHtml, { specialKeys: disableNoteCharacters, visionNote: true, secthtml: sectionList.secthtml }, (newnote, customParameters) => {
                newNotecreate(newnote, customParameters);
            });
        }
        else {
            input('', "New Sketch Note:", function (newnote) {
                newNotecreate(newnote);
            }, function () { }, '', 'textarea', true, noteMaxLength, { specialKeys: disableNoteCharacters });
        }
    };

    this.createNewVector = function (processCallback, skipTapMessage, options) {
        var processor = function (callback) {
            if (callback) callback();
        }
        var vs = editor.currentSketch.config.VectorSource
        if (vs.length > 1) {
            var selectedVS = null;
            if (options && options.vectorSourceTable) {
                var vr = vs.filter(function (a) { return a.Table == options.vectorSourceTable; });
                if (vr.length > 0) { selectedVS = vr[0]; }
            }
            if (selectedVS) {
                processor = function (callback) {
                    if (callback) callback(selectedVS);
                };
            } else {
                var vectConfig = vs.map(function (a) {
                    return {
                        Id: a.Table,
                        Name: a.Table
                    }
                });
                processor = function (callback) {
                    input('Select Sketch Type:', vectConfig, function (table) {
                        var vr = vs.filter(function (a) {
                            return a.Table == table;
                        })
                        if (vr) vr = vr[0];
                        if (callback) callback(vr);
                    }, function () { });
                }
            }
        }
        processor(function (vr) {
            if (vs.length == 1)
                vr = editor.currentSketch.config.VectorSource[0];
            var lblReqd = vr.IsLabelRequired == undefined ? true : vr.IsLabelRequired;
            var value = null;
            if (vr.GetLabelValueFromField)
                value = editor.currentSketch.parentRow[vr.LabelField]
            var labelFields = [];
            let lknm = vr.LabelLookup;
            if (lknm?.contains("/")) {
                let slashlist = lknm.split("/");
                for (slt in slashlist) {
                    if (editor.external.lookup?.[slashlist[slt]]) {
                        lknm = slashlist[slt]; break;
                    }
                }
            }
            labelFields.push({
                Field: vr.LabelField,
                Value: value,
                lookup: clientSettings['SketchLabelLookup'] || sketchSettings['SketchLabelLookup'] || lknm,
                hiddenFromEdit: vr.HideLabelFromEdit,
                IsRequired: lblReqd,
                Caption: vr.labelCaption,
                DoNotShowLabelDescription: editor.currentSketch.config.DoNotShowLabelDescription,
                ShowCurrentValueOnly: vr.ShowCurrentValueOnly,
                lookUpQuery: editor.currentSketch.config.LookUpQuery,
                splitByDelimiter: editor.currentSketch.config.splitByDelimiter,
                Target: vr.LabelTarget,
                UseLookUpNameAsValue: (editor.currentSketch.config.AssignLookUpNameAsLabel ? editor.currentSketch.config.AssignLookUpNameAsLabel : ((sketchApp.config.ApexEditSketchLabel && sketchSettings['ApexEditSketchLabel'] == '1') ? true : false)),
                IsLargeLookup: editor.currentSketch.config.IsLargeLookup,
                hiddenFromShow: (vr.HideLabelFromShow ? vr.HideLabelFromShow : ((sketchApp.config.ApexEditSketchLabel && sketchSettings['ApexEditSketchLabel'] == '1') ? true : false)),
                IsSwapLookupIdName: editor.currentSketch.config.EnableSwapLookupIdName,
                EnableFieldValidation: vr.EnableFieldValidation
            })
            if (vr.ExtraLabelFields && (!sketchApp.config.ApexEditSketchLabel || (sketchApp.config.ApexEditSketchLabel && sketchSettings['ApexEditSketchLabel'] == '1'))) {
                vr.ExtraLabelFields.forEach(function (item) {
                    let lknm = item.LookUp;
                    if (lknm?.contains("/")) {
                        let slashlist = lknm.split("/");
                        for (slt in slashlist) {
                            if (editor.external.lookup?.[slashlist[slt]]) {
                                lknm = slashlist[slt]; break;
                            }
                        }
                    }
                    labelFields.push({
                        Field: item.Name,
                        lookup: lknm,
                        Target: item.Target,
                        Caption: item.Caption,
                        hiddenFromShow: item.HideLabelFromShow,
                        DoNotShowLabelDescription: item.DoNotShowLabelDescription,
                        ValidationRegx: item.ValidationRegx,
                        IsLargeLookup: item.IsLargeLookup,
                        IsRequired: item.IsRequired,
                        requiredAny: item.requiredAny,
                        EnableFieldValidation: item.EnableFieldValidation
                    })
                })
            }
            var type;
            if (options && options.type)
                type = options.type;
            multipleInput('New Vector - Labels', labelFields, editor.external.lookup, function (values, optionResults) {
                var isnewPage = false;
                if (editor.currentSketch.config.VectorSource[0].vectorPagingField && optionResults.page && (sketchSettings.EnableSketchPaging == '1' || clientSettings.EnableSketchPaging == '1' || clientSettings.EnableSketchPaging == 'true')) {
                    var cr = _.clone(editor.currentSketch)
                    cr.vectors = [];
                    cr.label = cr.label.split('-')[0] + ' - ' + (editor.sketches.length + 1);
                    editor.sketches.push(cr)
                    editor.currentSketch = cr;
                    editor.refreshControlList()
                    $(editor.sketchSelector).prop("selectedIndex", (editor.sketches.length))
                    $(editor.sketchSelector).change();
                    isnewPage = true;
                    editor.currentSketch.isNewPage = true;
                }
                else if (editor.currentSketch.config.VectorSource[0].vectorPagingField)
                    editor.currentSketch.vectorPage = 'false';
                labelFields = values
                newlabel = values.filter(function (a) {
                    return a.Target == 'name'
                }).map(function (a) {
                    return a.Value
                }).join('') || ((editor.currentSketch.config.AssignLookUpNameAsLabel || (sketchApp.config.ApexEditSketchLabel && sketchSettings['ApexEditSketchLabel'] == '1')) ? values[0].NameValue : values[0].Value);
                if (!newlabel) newlabel = '';
                var directCreate = false;
                if (optionResults.unSketch && sketchApp.config.PlaceUnsketchedSegment)
                    directCreate = true;

                if (optionResults.sqft && optionResults.unSketch && sketchApp.config.enableManualAreaEntry) {
                    sketchApp.currentSketch.MSKetchArray.push({ msketchLabel: newlabel, mSketchArea: optionResults.sqft, modified: true });
                    sketchApp.currentSketch.isModified = true;
                    return;
                }

                var add = function (newSection) {
                    var vnum = 0;
                    if (editor.currentSketch.vectors.length > 0)
                        vnum = editor.currentSketch.vectors[editor.currentSketch.vectors.length - 1].index + 1;
                    var newv = new Vector(editor, editor.currentSketch);
                    newv.vectorConfig = vr;
                    newv.uid = editor.currentSketch.uid + "/" + vnum;
                    newv.index = vnum;
                    newv.label = '[' + vnum + '] ' + labelFields.filter(function (a) {
                        return (!a.hiddenFromShow || a.requiredAny)
                    }).map(function (a) {
                        return ((sketchSettings["DoNotShowLabelCode"] == '1' && a.Description && a.Description.Name && a.Value && !a.Value.contains('/')) ? a.Description.Name : ((a.Description && !a.DoNotShowLabelDescription && clientSettings["DoNotShowLabelDescriptionSketch"] != '1' && sketchSettings["DoNotShowLabelDescriptionSketch"] != '1' && a.Description.Name) ? (editor.formatter.showLabelDescriptionOnly || a.showLabelDescriptionOnly || clientSettings.DoNotShowLabelCodeSketch == '1' ? (editor.currentSketch.config.EnableSwapLookupIdName ? a.Description.Id : a.Description.Name) : (editor.currentSketch.config.EnableSwapLookupIdName ? a.Value + '-' + a.Description.Id : a.Value + '-' + a.Description.Name)) : a.Value))
                    }).join(editor.currentSketch.config.LabelDelimiter === undefined ? '/' : editor.currentSketch.config.LabelDelimiter)
                    if (editor.currentSketch.config.FirstRecordlabelValue && vnum == 0) {
                        var labelField = editor.currentSketch.config.FirstRecordlabelValue.split('/');
                        var labelFieldValue = labelField[1] ? (editor.currentSketch.parentRow[labelField[0]] || '---') + '/' + (editor.currentSketch.parentRow[labelField[1]] || '---') : s.parentRow[labelField[0]] || '---';
                        newv.label = '[' + vnum + '] ' + labelFieldValue;
                    }
                    newv.labelFields = labelFields
                    newv.name = newlabel;
                    var colorCodes = (values[0].Description && values[0].Description.color) ? values[0].Description.color : ((values[0].colorDescription && values[0].colorDescription.colorcode) ? values[0].colorDescription.colorcode : '');
                    if (colorCodes) {
                        var clrcode = colorCodes.split('~');
                        var isdtr = window.opener ? window.opener.__DTR : (typeof (__DTR) !== 'undefined' ? __DTR : false);
                        let isSvApp = window.opener && window.opener.appType == "sv" ? true : (typeof (appType) !== 'undefined' && appType == "sv" ? true : false);
                        var DQC = isdtr ? 'DTR' : (isSvApp ? 'SV' : 'QC');
                        newv.colorCode = clrcode[0] ? clrcode[0] : null;
                        newv.newLineColor = (clrcode[1] && sketchSettings['SketchLineColorCustomizations'] && sketchSettings['SketchLineColorCustomizations'].contains(DQC)) ? clrcode[1] : null;
                        newv.newLabelColorCode = (clrcode[2] && sketchSettings['SketchTextColorCustomizations'] && sketchSettings['SketchTextColorCustomizations'].contains(DQC)) ? clrcode[2] : null;
                    }
                    newv.clientId = ccTicks().toString();
                    newv.newRecord = true;
                    newv.header = ((editor.currentSketch.config && editor.currentSketch.config.DoNotHeaderAddition) ? null : (newlabel + (editor.currentSketch.config.NewVectorHeaderSuffix || editor.formatter.newVectorHeaderSuffix || ':')));
                    newv.code = values.filter(function (a) {
                        return a.Target == 'code'
                    }).map(function (a) {
                        return a.Value
                    }).join('')
                    if (editor.config && editor.config.IsJsonFormat && editor.currentSketch.currentPage)
                        newv.pageNum = editor.currentSketch.currentPage;
                    if (editor.config && editor.config.ShowSectNum && editor.currentSketch.isShowSectNum && optionResults.SECTNUM) {
                        var sNo = optionResults.SECTNUM;
                        if (sNo.split('{').length < 2)
                            newv.label = newv.label + ' (S:' + sNo + ')';
                    }
                    if (optionResults.mvpDisplayLabel) {
                        newv.mvpDisplayLabel = '[' + vnum + '] ' + optionResults.mvpDisplayLabel;
                        newv.sketchType = 'outBuilding';
                    }
                    if (optionResults.firstLabelValue?.lblValue) {
                        newv.label = '[' + vnum + '] ' + optionResults.firstLabelValue.lblValue;
                        newv.firstLabelValueModified = optionResults.firstLabelValue;
                    }

                    editor.currentSketch.vectors.push(newv);
                    editor.currentVector = newv;
                    editor.loadVectorSelector(editor.sketchSelector[0], newv, newv.pageNum);
                    if (directCreate) {
                        var _vects = editor.currentSketch.vectors.filter(function (x) { return x.isUnSketchedArea })
                        var ylen = 45;
                        _vects.forEach(function (ve) {
                            if (ve.startNode && ve.startNode.p.y < ylen)
                                ylen = ve.startNode.p.y;
                        });
                        if (_vects.length > 0)
                            ylen = ylen - 5;
                        newv.fixedArea = optionResults.sqft;
                        directlyPlaceUnsketchedSegments(editor, newv, 80, ylen);
                    }
                    else {
                        if (!skipTapMessage)
                            editor.mode = CAMACloud.Sketching.MODE_DEFAULT;
                        editor.startNewVector();
                        if (editor.currentVector)
                            editor.currentVector.startNode = null;
                    }
                    editor.currentVector.isModified = true;
                    $('.ccse-meter-length').val('')
                    $('.ccse-meter-angle').val('');
                    /*if ( editor.config && editor.config.EnableBoundary && editor.config.IsProvalConfig && editor.currentSketch ) {
                            var _scale = editor.currentSketch.boundaryScale? editor.currentSketch.boundaryScale: 100;
                            $('.ccse-scale').val(_scale);
                        }
                        else 
                            $('.ccse-scale').val('');*/
                    editor.drawlabelOnly = false;
                    if (isnewPage) newv.vectorPage = 'true';
                    if (editor.currentSketch.config.addCalculatedType && (!sketchApp.config.ApexAutoSubtract || (sketchApp.config.ApexAutoSubtract && sketchSettings['ApexAutoSubtract'] == '1'))) {
                        newv.calculationType = optionResults.calcType;
                        if (newv.calculationType == 'negative')
                            newv.areaMultiplier = -1;
                        else if (newv.calculationType == 'auto')
                            newv.linkedUniqueId = optionResults.linkedUniqueId;
                    }
                    if (optionResults.footMode == 'tenth') {
                        newv.footMode = 'true';
                        editor.scale = .3;
                        sketchApp.render();
                        sketchApp.setZoomValue('30')
                    }
                    else if (vr.FootModeField) {
                        var t = sketchApp.currentSketch.vectors.filter(function (a) { return a.footMode == 'true' })
                        if (t && t.length > 0)
                            newv.footMode = 'true';
                        else
                            newv.footMode = 'false';
                    }
                    else if (optionResults.OBlabelValue && editor.config && editor.config.isOIunsketched) {
                        if (optionResults.drawSketch != true) {
                            editor.drawlabelOnly4x4 = true;
                            newv.hideAreaValue = true;
                            newv.fixedArea = optionResults.sqft;
                            newv.isUnSketchedArea = true;
                        }
                        else {
                            newv.isUnSketchedArea = false;
                            editor.drawlabelOnly4x4 = false;
                        }
                        newv.sketchType = 'outBuilding';
                    }
                    else if (optionResults.OBlabelValue) {
                        if (optionResults.drawSketch != true) {
                            editor.drawlabelOnly = true;
                            newv.hideAreaValue = true;
                            newv.fixedArea = optionResults.sqft;
                            newv.isUnSketchedArea = true;
                        }
                        else {
                            editor.drawlabelOnly = false;
                        }
                        newv.sketchType = 'outBuilding';
                    }
                    else if (optionResults.IsOutbuilding) {
                        newv.sketchType = 'outBuilding';
                        if (optionResults.NewOBdrawSketch) {
                            editor.drawlabelOnly = true;
                            newv.fixedAreaTrue = optionResults.outArea;
                            newv.fixedPerimeterTrue = optionResults.outPerimeter;
                            newv.isUnSketchedArea = true;
                        }
                    }
                    if (optionResults.unSketch && !directCreate) {
                        if (editor.currentSketch.config.CustomToolForVector) {
                            newv.fixedArea = optionResults.sqft;
                            if (editor.currentSketch.config.DrawPlaceHolder) {
                                editor.drawlabelOnly = newv.isUnSketchedArea = newv.CheckFixedArea = true;
                            }
                            else editor.drawfixedArea = true;
                        } else {
                            editor.drawlabelOnly = true;
                            newv.hideAreaValue = true;
                            newv.isUnSketchedArea = true;
                            if (newv.sketch.config.EnableMarkedAreaDrawing) {
                                newv.isMarkedArea = newv.CheckFixedArea = newv.noVectorSegment = true;
                                newv.fixedArea = newv.areaFieldValue = optionResults.sqft;
                                newv.hideAreaValue = false;
                            }
                        }
                    }
                    if (optionResults.unSketchedTrue) {
                        editor.drawlabelOnly = true;
                        newv.hideAreaValue = false;
                        newv.isUnSketchedTrueArea = true;
                        newv.fixedAreaTrue = optionResults.Sqft;
                        newv.fixedPerimeterTrue = optionResults.perimeter;
                        newv.CheckFixedArea = true;
                        newv.Line_Type = true;
                    }
                    if (optionResults.SECTNUM)
                        newv.sectionNum = optionResults.SECTNUM;
                    if (optionResults.BasementFunctionality)
                        newv.BasementFunctionality = true;
                    if (optionResults.MapExteriorFeature)
                        newv.MapExteriorFeature = true;
                    if (optionResults.isNewPNAValue)
                        newv.isPNANewValue = optionResults.isNewPNAValue;
                    if (optionResults.isAreaMultiplier)
                        newv.areaMultiplier = optionResults.isAreaMultiplier;
                    if (optionResults.mvpData)
                        newv.mvpData = optionResults.mvpData;
                    if (optionResults.vectorExtraParameters)
                        newv.vectorExtraParameters = optionResults.vectorExtraParameters;
                    if (optionResults.visionClientId) newv.visionClientId = optionResults.visionClientId;
                    if (newSection) newv.newSection = true;
                    if (processCallback) processCallback()
                    return;
                }

                var addFun = function (newSection) {
                    if (skipTapMessage || directCreate)
                        add(newSection);
                    else
                        messageBox('Tap on any point to start the new sketch segment.', function () {
                            add(newSection);
                        });
                }

                if (editor?.config?.CreateAssociateWindow && optionResults.SECTNUM && optionResults.autoSection) {
                    $('.mask').show();
                    editor.config.CreateAssociateWindow(editor, { sectionNum: optionResults.SECTNUM, name: newlabel }, 'New', (rObj) => {
                        if (rObj?.sectNum) optionResults.SECTNUM = rObj.sectNum;
                        if (rObj?.visionClientId) optionResults.visionClientId = rObj.visionClientId;
                        $('.mask').hide();
                        addFun(true);
                    });
                }
                else addFun();

            }, editor, 'new', {
                'addpagingcontrol': ((vr.vectorPagingField && (sketchSettings.EnableSketchPaging == '1' || clientSettings.EnableSketchPaging == '1' || clientSettings.EnableSketchPaging == 'true')) ? true : false), 'addFootMode': editor.currentSketch.config.AddFootMode,
                'addCalculatedType': editor.currentSketch.config.addCalculatedType, 'type': type
            });

            var selectedCode = null;
        })
    }


    this.createNewVectorSegment = function (callback, skipTapMessage) {
        var add = function () {
            var vnum = 0;
            var counter = 0;
            var newlabel = editor.currentVector.label.substr(editor.currentVector.label.indexOf(']') + 1);
            var vuid = editor.currentVector.uid;
            if (vuid.indexOf("/") > -1) vuid = vuid.substr(0, vuid.indexOf("/"));

            for (var vi in editor.currentSketch.vectors) {
                var v = editor.currentSketch.vectors[vi];
                var vnum2 = v.uid.indexOf('/') > -1 ? parseInt(v.uid.substr(v.uid.indexOf('/') + 1)) : 0;
                var vnum1 = parseInt(vi || 0) + 1;
                if (vnum1 > vnum) vnum = vnum1;
                if (vnum2 > vnum) vnum = vnum2;
            }
            if (editor.currentSketch.vectors.length > 0) {
                counter = editor.currentSketch.vectors[editor.currentSketch.vectors.length - 1].index + 1;
            }

            var newv = new Vector(editor, editor.currentSketch);
            newv.uid = vuid + "/" + vnum;
            newv.index = counter;
            newv.label = '[' + counter + '] ' + newlabel
            newv.name = newlabel
            newv.clientId = ccTicks().toString();
            newv.labelFields = editor.currentVector.labelFields;
            newv.vectorConfig = editor.currentVector.vectorConfig
            newv.header = ((editor.currentSketch.config && editor.currentSketch.config.DoNotHeaderAddition) ? null : (newlabel + (editor.currentSketch.config.NewVectorHeaderSuffix || editor.formatter.newVectorHeaderSuffix || ':')));
            editor.currentSketch.vectors.push(newv);
            editor.currentVector = newv;
            editor.loadVectorSelector(editor.sketchSelector[0], newv);
            editor.mode = CAMACloud.Sketching.MODE_DEFAULT;
            editor.startNewVector();
            if (editor.currentVector)
                editor.currentVector.startNode = null;
            editor.currentVector.isModified = true;
            if (callback) callback();
            return;
        }
        if (skipTapMessage)
            add()
        else
            messageBox('Tap on any point to start the new sketch segment.', function () {
                add()
            })
    }


    this.transferVector = function () {
        if (!_) {
            alert('Requires underscore.js');
            return;
        }
        //        var configSources = ccma.Sketching.Config.sources;
        //        var targets = _.where(configSources, { AllowTransferIn: true });
        //        if (targets.length == 0) {
        //            messageBox('No transfer targets selected in the configuration. Use AllowTransferIn in source config.');
        //            return;
        //        }
        //        var c = targets[0];
        //        var targetElems = activeParcel[c.SketchSource.Table];
        var data = [];
        for (var x in editor.sketches) {
            var sx = editor.sketches[x];
            var cx = sx.config;
            if (cx.AllowTransferIn) {
                data.push({
                    Id: sx.uid,
                    Name: sx.label
                });
            }
            //            var t = targetElems[x];
            //            data.push({ Id: (c.Key ? (c.Key + '-') : '') + t[c.SketchSource.KeyField || 'ROWUID'], Name: t[c.SketchSource.LabelField] });
        }


        if (data.length == 0) {
            messageBox('There is no record to transfer this sketch to. Please verify that at least one other improvement record exists and then transfer the sketch as needed. (AllowTransferIn)');
            return;
        }

        input('Select Sketch:', data, function (targetRowUID) {
            if (targetRowUID != "") {
                var v = editor.currentVector;
                var targetSketch = null;
                var vi = 0,
                    ri = -1;
                for (var x in editor.currentSketch.vectors) {
                    if (v == editor.currentSketch.vectors[x]) {
                        ri = vi;
                    }
                    vi++;
                }

                for (var x in editor.sketches) {
                    var sk = editor.sketches[x];
                    if (sk.uid == targetRowUID) {
                        targetSketch = sk;
                    }
                }

                if (ri > -1 && targetSketch) {
                    var vectorList = [];
                    for (var x in targetSketch.vectors) {
                        var vx = targetSketch.vectors[x];
                        vectorList.push({
                            Id: vx.uid,
                            Name: vx.label
                        });
                    }

                    if (vectorList.length == 0) {
                        if (data.length == 0) {
                            messageBox('The selected sketch does not contain segment to copy the data into.');
                            return;
                        }
                    }

                    input('Select Segment:', vectorList, function (targetVectorID) {
                        var targetVector = null;
                        for (var x in targetSketch.vectors) {
                            var tk = targetSketch.vectors[x];
                            if (tk.uid == targetVectorID) {
                                targetVector = tk;
                            }
                        }

                        if (targetVector) {
                            var copyVectorData = function () {
                                var zm = editor.scale;
                                editor.zoom(1);
                                v = editor.currentSketch.vectors.splice(ri, 1)[0];
                                targetVector.vectorString = v.toString();
                                editor.formatter.updateVectorFromString(editor, targetVector, v.vectorString);
                                targetVector.labelPosition = new PointX(v.labelPosition.x, v.labelPosition.y);
                                targetVector.isModified = true;
                                targetVector.isCopied = true;
                                editor.currentSketch.isModified = true;
                                editor.currentVector.isModified = true;
                                editor.currentVector = null;
                                editor.loadVectorSelector(editor.sketchSelector[0]);
                                editor.mode = CAMACloud.Sketching.MODE_DEFAULT;
                                editor.renderAll();
                                editor.zoom(zm);
                                editor.onModeChange(0);
                            }
                            if (targetVector.vectorString != null && targetVector.vectorString != '') {
                                messageBox('The selected segment contains data already. Are you sure you want to overwrite?', ["OK", "Cancel"], function () {
                                    copyVectorData();
                                }, function () {
                                    return;
                                })
                            } else {
                                copyVectorData();
                            }
                        }
                    }, function () {
                        //Do Nothing - Cancel Button
                    });

                }


            }
        }, function () {
            //Do Nothing - Cancel Button
        });

    }

    this.setControlList = function (sketchSel, vectorSel, pageSel) {
        this.vectorSelector = $(vectorSel);
        this.sketchSelector = $(sketchSel);
        if (pageSel)
            this.pageSelector = $(pageSel);

        this.refreshControlList();
    }


    this.resizeCanvas = function (w, h) {
        ccse.width = w;
        ccse.height = h;
        $(ccse).width(w);
        $(ccse).height(h);
        editor.width = $(ccse).width();
        editor.height = $(ccse).height();
        if (editor.isOpenSketchEditorTrue)
            editor.origin = editor.origin;
        else
            editor.origin = GetOrigin(editor, editor.formatter.originPosition)
        editor.refOrigin = new PointX(editor.origin.x, editor.origin.y);
        editor.brush = new Brush(editor.drawing, editor);
    }

    this.renderAll = function () {
        this.clearCanvas();
        this.brush.setScale(this.scale);
        this.brush.moveTo(this.origin);

        if (this.currentSketch) {
            clearVectorlabelPosition();
            var curPage = null;
            if (this.config && this.config.IsJsonFormat && this.currentSketch.currentPage)
                curPage = this.currentSketch.currentPage;
            for (var x in this.currentSketch.vectors) {
                this.currentSketch.vectors[x].render();
            }
            if (this.displayNotes) this.currentSketch.renderNotes(curPage);

        } else {
            for (var i in this.sketches) {
                clearVectorlabelPosition();
                for (var x in this.sketches[i].vectors) {
                    this.sketches[i].vectors[x].render();
                }
                if (this.displayNotes) this.sketches[i].renderNotes();
            }
        }
    }

    this.render = function (currentPoint, options) {
        var moving = false;
        this.clearCanvas();

        if (this.showOrigin) this.drawOrigin();
        if (options) {
            moving = options.moving;
            if (options.renderMode == "fit") {

            }
            if (options.cursor)
                this.drawCursor(currentPoint);
        }

        this.brush.setScale(this.scale);
        this.brush.moveTo(this.origin);
        for (var i in this.sketches) {
            if (this.sketches[i] == this.currentSketch || !this.currentSketch || fvEnabled == "NV" || !this.currentSketch.GroupingValue || !this.sketches[i].GroupingValue || this.currentSketch.GroupingValue != this.sketches[i].GroupingValue)
                continue
            clearVectorlabelPosition();
            for (var x in this.sketches[i].vectors) {
                this.sketches[i].vectors[x].render();
            }
            //if ( this.displayNotes ) this.sketches[i].renderNotes();
        }
        clearVectorlabelPosition();
        if (this.currentSketch) {
            sketchCache[this.currentSketch.uid] = '';
            var curPage = null;
            if (this.config && this.config.IsJsonFormat && this.currentSketch.currentPage)
                curPage = this.currentSketch.currentPage;
            for (var x in this.currentSketch.vectors) {
                var v = null;
                if (this.config && this.config.IsJsonFormat && curPage) {
                    if (this.currentSketch.vectors[x].pageNum == curPage)
                        v = this.currentSketch.vectors[x];
                    if (!v)
                        continue;
                }
                /*MA_2479*/
                //else if (!this.currentSketch.vectors[x].marea) {
                v = this.currentSketch.vectors[x];
                v.render(currentPoint, moving);
                //}
                if (v && v.isModified && sketchopened) {
                    sketchCache[this.currentSketch.uid] += this.currentSketch.uid + '#' + v.uid + '#' + v.labelFields.map(function (a) { return a.Value }).join('|') + '#' + v.toString() + '#0#' + (v.clientId ? v.clientId : '') + (v.fixedArea ? '#' + v.fixedArea : '') + '%%';
                }
            }

            if (!moving && this.displayNotes) {
                this.currentSketch.renderNotes(curPage);
            } else if (this.currentSketchNote && moving) {
                this.currentSketch.renderNotes(curPage);
            }
        }
        var t = Object.keys(sketchCache).map(function (x) { return sketchCache[x] }).join('')
        if (activeParcel && t.trim() != '')
            t = activeParcel.Id + '$' + t;
        if (sketchopened) localStorage.setItem("sketchCache", t);

    }

    this.drawOrigin = function (o) {
        o = o || this.origin;
        var tempScale = this.scale;
        this.brush.setScale(1);

        this.brush.setColor('#999', 'Red'); // origin colors
        this.brush.begin();
        this.brush.moveTo(o);
        if (this.formatter && this.formatter.originPosition == "topRight") {
            this.brush.down(50, true, true);
            this.brush.left(50, true, true);
        }
        else if (this.formatter && this.formatter.originPosition == "topLeft") {
            this.brush.down(50, true, true);
            this.brush.right(50, true, true);

        } else {
            this.brush.up(50, true, true);
            this.brush.right(50, true, true);
        }
        this.brush.stroke();
        this.brush.end()

        if (this.config && this.config.EnableBoundary) { // MVP config Rectangle boundary showing
            this.brush.begin();
            this.drawing.setLineDash([5, 3])
            this.brush.moveTo(o);
            var bScale = 100;
            if (this.currentSketch && this.currentSketch.boundaryScale)
                bScale = this.currentSketch.boundaryScale;
            bScale = parseInt(bScale) * 10;
            if (this.formatter && this.formatter.originPosition == "topRight") {
                this.brush.down((bScale * this.scale), true, true);
                this.brush.left((bScale * this.scale), true, true);
                this.brush.moveTo({ x: (-(bScale * this.scale) + o.x), y: o.y });
                this.brush.down((bScale * this.scale), true, true);
                this.brush.moveTo({ x: o.x, y: ((o.y) + (bScale * this.scale)) });
                this.brush.left((bScale * this.scale), true, true)
            }
            else if (this.formatter && this.formatter.originPosition == "topLeft") {
                this.brush.down((bScale * this.scale), true, true);
                this.brush.right((bScale * this.scale), true, true);
                this.brush.moveTo({ x: (bScale * this.scale) + o.x, y: o.y });
                this.brush.down((bScale * this.scale), true, true);
                this.brush.moveTo({ x: o.x, y: ((o.y) + (bScale * this.scale)) });
                this.brush.right((bScale * this.scale), true, true)
            } else {
                this.brush.up((bScale * this.scale), true, true);
                this.brush.right((bScale * this.scale), true, true);
                this.brush.moveTo({ x: (bScale * this.scale) + o.x, y: o.y });
                this.brush.up((bScale * this.scale), true, true);
                this.brush.moveTo({ x: o.x, y: ((o.y) - (bScale * this.scale)) });
                this.brush.right((bScale * this.scale), true, true)
            }
            this.brush.stroke();
            this.brush.end()
            this.drawing.setLineDash([]);
        } else if (this.config && this.config.isShowBoundary) {
            this.brush.begin();
            this.drawing.setLineDash([5, 3])
            this.brush.moveTo(o);
            var winHeight = window.innerHeight;
            var winWidth = window.innerWidth;
            if (this.formatter && this.formatter.originPosition == "topRight") {
                this.brush.down((Math.abs(o.y) + winHeight), true, true);
                this.brush.left((Math.abs(o.x) + winWidth), true, true);
            }
            else if (this.formatter && this.formatter.originPosition == "topLeft") {
                this.brush.down((Math.abs(o.y) + winHeight), true, true);
                this.brush.right((Math.abs(o.x) + winWidth), true, true);

            } else {
                this.brush.up((Math.abs(o.y) + winHeight), true, true);
                this.brush.right((Math.abs(o.x) + winWidth), true, true);
            }
            this.brush.stroke();
            this.brush.end()
            this.drawing.setLineDash([]);
        }

        this.brush.begin();
        this.brush.moveTo(o);
        this.brush.rectX(9, 9);
        this.brush.strokeAndFill();
        this.brush.end();

        this.brush.setScale(tempScale);
    }

    this.drawNode = function (current, s, e, ol, angle) {
        var nodeColor = '#CFCFCF';
        if (s) nodeColor = '#00FF00';
        if (e) nodeColor = 'Black';
        if (current) nodeColor = 'Blue';
        if (ol) nodeColor = 'Magenta';

        this.drawing.lineWidth = 1;
        var nz = 7;
        var tempScale = this.scale;
        this.brush.setScale(1);

        if (s) nz = 11;
        var p = this.brush.begin();

        s && (angle != null) ? this.brush.pointer(nz, nz, 360 - angle) : this.brush.rectX(nz, nz);
        this.brush.stroke();
        this.brush.setColor('Black', nodeColor); //node - black + choice
        this.brush.fill();
        this.brush.end();
        this.brush.moveTo(p);

        this.brush.setScale(this.scale);
    }

    this.drawCursor = function (p) {
        try {
            var crosshair = 2400;
            var cp = p.copy();
            var hx = false,
                hy = false;
            if (this.currentVector != null) {
                var t = p.copy().alignToGrid(this);
                var sn = this.currentVector.startNode;
                while (sn != null) {
                    if ((this.mode == CAMACloud.Sketching.MODE_NEW) || ((this.mode == CAMACloud.Sketching.MODE_EDIT) && (sn != this.currentNode) && (sn != this.currentVector.endNode))) {
                        if (sn.p.x == t.x) hy = true;
                        if (sn.p.y == t.y) hx = true;
                    }
                    sn = sn.nextNode;
                }
            }

            this.drawing.lineWidth = 1;
            var tempScale = this.scale;
            this.brush.setScale(1);
            this.brush.begin();
            this.brush.setColor(hx ? '#F09' : '#BBB', 'Red'); //guide cursors
            this.brush.moveTo(cp);
            this.brush.left(crosshair, true, true);
            this.brush.right(crosshair, true, true);
            this.brush.stroke();
            this.brush.end()

            this.brush.setColor(hy ? '#F09' : '#BBB', 'Red'); //guide cursors
            this.brush.begin();
            this.brush.moveTo(cp);
            this.brush.up(crosshair, true, true);
            this.brush.down(crosshair, true, true);
            this.brush.stroke();
            this.brush.end()

            this.brush.setScale(tempScale);
        } catch (e) {
            alert(e);
        }

        //        var img = new Image();
        //        img.onload = function () {
        //            editor.drawing.drawImage(img, p.x - 13, p.y - 13);
        //        }
        //        img.src = 'static/images/sketch-cursor.png';
    }

    this.screenBounds = function () {
        var lxy = (new PointX(0, 0)).alignToGrid(this);
        var uxy = (new PointX(this.width, this.height)).alignToGrid(this);
        return {
            lx: lxy.x,
            ly: uxy.y,
            ux: uxy.x,
            uy: lxy.y
        };
    }

    this.getCoords = function (e) {
        var p = new PointX(0, 0);
        if (event.touches) {
            var t = event.touches[0];
            if (!t)
                t = event.changedTouches[0];
            p = new PointX(t.clientX - t.target.offsetLeft, t.clientY - t.target.offsetTop);
        } else {
            p = new PointX(event.clientX - event.target.offsetLeft, event.clientY - event.target.offsetTop);
        }
        return p;
    }

    this.registerNode = function (p) {
        if (!p)
            p = this.origin.copy();
        var ap = p.copy();
        if (this.currentVector == null) {
            this.currentVector = new Vector(this, this.currentSketch, p);
        } else {
            if (this.currentVector.startNode)
                this.currentVector.connect(p);
            else
                this.currentVector.start(p);
        }
        this.render();
    }

    this.open = function (data, defaultVectorUID, isPreview, afterSave, beforeSave, CC_beforeSketch, lineColorCode, labelColorCode, changeSketchOrigin, currentPage) {
        //        if (!this.formatter) throw "A Sketch formatter is required to open the vector.";
        //        this.formatter = formatter;
        this.labelColorCode = labelColorCode
        this.lineColor = lineColorCode
        this.isPreview = isPreview;
        this.afterSave = afterSave;
        this.beforeSave = beforeSave;
        this.currentVector = null;
        this.currentSketchNote = null;
        this.currentSketch = null;
        this.currentNode = null;
        this.currentLine = null;
        this.displayNotes = true;
        this.deletedVectors = [];
        this.rsDeletedPages = [];
        if (this.config?.IsWrightSketch) {
            delete this.deletedWrightSketchCards;
            delete this.createdWrightSketchCards;
        }
        this.formatter.open(this, data);
        this.mode = CAMACloud.Sketching.MODE_DEFAULT;
        this.hideKeypad();
        this.raiseModeChange();
        this.refreshControlList();
        if (CC_beforeSketch) {
            this.CC_beforeSketch = true;
            $('.ccse-currentsketch').hide();
            $('.ccse-beforesketch').show();
            $('.ccse-save').hide();
        }
        else {
            this.CC_beforeSketch = false;
            $('.ccse-currentsketch').show();
            $('.ccse-beforesketch').hide();
        }
        if (window.opener && window.opener.appType == "sv" && (window.opener.lightWeight || (clientSettings && clientSettings["SketchValidationEditSketch"] != "1"))) {
            $('.ccse-preview-sketch').hide();
        }

        if (editor.config?.isHelionRS) {
            $('.ccse-add-page').css('display', 'inline-block');
            $('.ccse-add-page').removeClass('ccse-add-page-highlight');
        }

        var skIndex, sameSketches = []; sketchCache = {}; _moving = false; undoProcessArray = []; nodeNumber = null;
        var _sdpVal = (clientSettings && clientSettings['SketchDecimals']) || (sketchSettings && sketchSettings['SketchDecimals']);
        _sdp = (_sdpVal && _sdpVal > 2 ? (_sdpVal > 4 ? 4 : parseInt(_sdpVal)) : 2);
        if (changeSketchOrigin)
            this.ischangeSketchOriginPosition = true;
        else {
            if (this.config && this.config.FullViewEnabled && sketchSettings.SketchFullViewDefault && $('.fv-switch-input')[0]) {
                $('.fvSwitch').show();
                fvEnabled = (sketchSettings.SketchFullViewDefault == 'FV' ? 'FV' : (sketchSettings.SketchFullViewDefault == 'NV' ? 'NV' : null));
                if (fvEnabled == 'FV')
                    $('.fv-switch-input')[0].checked = true;
                else if (fvEnabled == 'NV')
                    $('.fv-switch-input')[0].checked = false;
            }
        }
        if (defaultVectorUID && defaultVectorUID.indexOf && defaultVectorUID.indexOf('/')) {
            skIndex = defaultVectorUID.split('/')[1]
            defaultVectorUID = defaultVectorUID.split('/')[0];
            sameSketches = this.sketches.filter(function (sk) { return sk.uid == defaultVectorUID })
        }
        if (this.sketches.length == 0) {
            $('.ccse-preview-sketch ').hide()
        }
        else if (editor.sketchSelector && this.sketches.length > 0 && (this.sketches[0].config.AutoSelectFirstItem != false || this.sketches[0].config.AutoSelectFirstItem == 'undefined') && !isPreview) {
            $(editor.sketchSelector)[0].selectedIndex = 1
            $(editor.sketchSelector).change();
        }
        var sketch_selected = false;
        if (currentPage)
            this.currentPageNum = currentPage;
        this.sketches.forEach(function (sk, index) {
            //if (sk.config.AutoSelectFistItem == true && index == 0 && !isPreview) {
            //if ((sk.config.AutoSelectFirstItem != false || sk.config.AutoSelectFirstItem == 'undefined') && (index == 0 && !isPreview)) {
            //    $(editor.sketchSelector).val(sk.uid);
            //    $(editor.sketchSelector).change();
            //}
            if (defaultVectorUID == sk.uid) { // if sketchId given as default one
                $(editor.sketchSelector).val(sk.uid);
                if (skIndex && sameSketches.length > 1)
                    $(editor.sketchSelector)[0].selectedIndex = skIndex
                $(editor.sketchSelector).change();
                if (currentPage > 1) {
                    var pageChange = false;
                    if (editor.config?.IsWrightSketch) {
                        if ($('option[value="' + currentPage + '"]', $(editor.pageSelector)[0])[0]) {
                            $(editor.pageSelector)[0].value = currentPage;
                        }
                        else {
                            if (($(editor.pageSelector)[0].length) > 1)
                                $(editor.pageSelector)[0].selectedIndex = 0;
                            else
                                pageChange = true;
                        }
                    }
                    else {
                        if (($(editor.pageSelector)[0].length) > (currentPage - 1))
                            $(editor.pageSelector)[0].selectedIndex = currentPage - 1;
                        else {
                            if (($(editor.pageSelector)[0].length) > 1)
                                $(editor.pageSelector)[0].selectedIndex = 0;
                            else
                                pageChange = true;
                        }
                    }
                    
                    if (!pageChange)
                        $(editor.pageSelector).change();
                }
                //   editor.currentSketch = sk;
                editor.currentNode = null;
                sketch_selected = true;
            }
            sk.vectors.forEach(function (v) {
                sectionValue = v.section || 1;
                if (!sk.sections[sectionValue]) sk.sections[sectionValue] = [];
                sk.sections[sectionValue].push(v);
                if (v.uid == defaultVectorUID && !sketch_selected) {
                    $(editor.sketchSelector).val(sk.uid);
                    editor.loadVectorSelector(editor.sketchSelector ? editor.sketchSelector[0] : editor.sketchSelector);
                    $(editor.vectorSelector).val(v.uid);
                    editor.currentVector = v;
                    editor.currentSketch = sk;
                    editor.currentNode = null;
                    if (v.toString() == "") {
                        if (!editor.viewOnly)
                            editor.promptAndCreateVector();
                    } else {
                        editor.mode = CAMACloud.Sketching.MODE_EDIT;
                        editor.raiseModeChange();
                    }
                }
            });
        });

        if (changeSketchOrigin)
            this.ischangeSketchOriginPosition = false;

        if (currentPage)
            this.currentPageNum = null;

        if (editor.config?.isHelionRS && this.sketches.length == 0 && !isPreview && typeof (messageBox) == 'function') {
            messageBox('There are no sketch records available. Would you like to create a new one?', ['OK', 'Cancel'], function () {
                rsSketchAddPage(editor);
            }, function () {
                $('.ccse-add-page').addClass('ccse-add-page-highlight');
            });
        }
        //        for (var x in this.vectors) {
        //            if (this.vectors[x].uid == defaultVectorUID) {
        //                this.currentVector = this.vectors[x];
        //            }
        //        }
    }

    this.deleteCurrentNode = function () {
        var c = this.currentNode;
        if (c.isEllipse)
            if (c.nextNode && c.nextNode.isEllipseEndNode) {
                nodeNumber++;
                this.deleteNode(c.nextNode);
            }
        this.deleteNode(c);
        if (this.currentVector)
            this.currentVector.isModified = true;
    }
    this.deleteCurrentSketchNote = function () {
        var note = this.currentSketchNote;
        note.isDeleted = true;
        this.currentSketchNote.isModified = true;
        if (sketchApp && sketchApp.config && (sketchApp.config.IsJsonFormat || sketchApp.config.EnableVisionNotes || sketchApp.config.ModifyIfNoteChanged))
            sketchApp.currentSketch.isModified = true;
        this.currentSketchNote = null;
        editor.mode = CAMACloud.Sketching.MODE_DEFAULT;
        editor.raiseModeChange();
        this.render();
    }

    this.copyCurrentVector = function () {
        var _getCurrentVector = function (callback) {
            for (key in editor.currentVector) {
                if (key == 'labelPosition') {
                    copiedVector[key] = _.clone(editor.currentVector[key]);
                }
                else if (key == 'labelFields' && editor.currentVector[key] && editor.currentVector[key][0] && editor.currentVector[key][0].labelDetails) {
                    let lfObj = JSON.parse(JSON.stringify(editor.currentVector[key]));
                    copiedVector[key] = lfObj;
                }
                else
                    copiedVector[key] = editor.currentVector[key];
            };
            if (window.opener)
                window.opener.copyVector = copiedVector;
            else
                copyVector = copiedVector;
            if (callback) callback();
        }
        if (this.currentVector) {
            if (this.currentVector.isModified == true)
                messageBox('Please save the changes before you copy the vector');
            else {
                if (window.opener)
                    window.opener.copyVector = null;
                var copiedVector = {};
                _getCurrentVector(function () {
                    messageBox('Tap on the white space to enable the Paste Segment button');
                });
            }
        }
    }

    //keyevent//ctrl+x//
    this.cutCurrentVector = function () {
        var _getCurrentVector = function (callback) {
            for (key in editor.currentVector) {
                if (key == 'labelPosition')
                    copiedVector[key] = _.clone(editor.currentVector[key]);
                else
                    copiedVector[key] = editor.currentVector[key];
            };
            if (window.opener)
                window.opener.copyVector = copiedVector;
            else
                copyVector = copiedVector;

            editor.deleteCurrentVector();
            if (callback) callback();
        }
        if (this.currentVector) {
            if (this.currentVector.isModified == true)
                messageBox('Please save the changes before you cut the vector');
            else {
                if (window.opener)
                    window.opener.copyVector = null;
                var copiedVector = {};
                _getCurrentVector(function () {
                    messageBox('Tap on the white space to enable the Paste Segment button');
                });
            }
        }
    }

    /////keyevent ends
    this.pasteVector = function () {
        // var cv = editor.copiedVector;
        if ((!editor.currentSketch.config.InsertDataOnAddition && editor.currentSketch.config.SketchSource.Table != editor.currentSketch.config.VectorSource[0].Table) || (editor.currentSketch.isNewPage)) {
            messageBox('You are not allowed to add new record to the selected source')
            return;
        }
        var t = editor.scale;
        editor.zoom(1);
        if (window.opener)
            copyVector = window.opener.copyVector;
        var cv = editor.formatter.vectorFromString(editor, copyVector.vectorString);
        cv.sketch = editor.currentSketch;
        if (copyVector.sketchType != 'outBuilding') {

            cv.vectorConfig = copyVector.vectorConfig;
            cv.labelFields = copyVector.labelFields;
            cv.Line_Type = copyVector.Line_Type;
            cv.isUnSketchedTrueArea = copyVector.isUnSketchedTrueArea;
            cv.BasementFunctionality = copyVector.BasementFunctionality;
            cv.areaFieldValue = copyVector.areaFieldValue;
            cv.sectionNum = copyVector.sectionNum;
            cv.MapExteriorFeature = copyVector.MapExteriorFeature;
            cv.isPNANewValue = copyVector.isPNANewValue;
            cv.areaMultiplier = copyVector.areaMultiplier;
            if (copyVector.mvpData) {
                var mvData = copyVector.mvpData;
                cv.mvpData = _.clone(mvData);
            }
            if (copyVector.calculationType == 'negative')
                cv.calculationType = copyVector.calculationType;
            if (copyVector.calculationType == 'auto') {
                cv.calculationType = copyVector.calculationType;
                cv.linkedUniqueId = copyVector.linkedUniqueId;
            }
            if (editor.config && editor.config.IsJsonFormat && ((editor.currentSketch && editor.currentSketch.currentPage) || copyVector.pageNum))
                cv.pageNum = (editor.currentSketch && editor.currentSketch.currentPage) ? editor.currentSketch.currentPage : copyVector.pageNum;
            if (cv.isUnSketchedTrueArea) {
                cv.fixedAreaTrue = cv.areaFieldValue;
                if (copyVector.jsonSegment) {
                    cv.fixedAreaTrue = copyVector.fixedAreaTrue;
                    cv.fixedPerimeterTrue = copyVector.fixedPerimeterTrue;
                }
            }
            else if (copyVector.isUnSketchedArea) {
                cv.isUnSketchedArea = copyVector.isUnSketchedArea;
                cv.fixedArea = copyVector.areaFieldValue;
                if (cv.sketch?.config?.DrawPlaceHolder && copyVector.fixedArea) cv.fixedArea = copyVector.fixedArea;
            }

            if (copyVector.vectorExtraParameters) cv.vectorExtraParameters = copyVector.vectorExtraParameters;
            if (copyVector.isMarkedArea) cv.isMarkedArea = copyVector.isMarkedArea;
            if (copyVector.noVectorSegment) cv.noVectorSegment = copyVector.noVectorSegment;
            if (copyVector.CheckFixedArea) cv.CheckFixedArea = copyVector.CheckFixedArea;
            if (copyVector.visketchType) cv.visketchType = copyVector.visketchType;
            if (copyVector.newSection) cv.newSection = copyVector.newSection;
        }
        else {
            cv.vectorConfig = copyVector.vectorConfig;
            cv.labelFields = copyVector.labelFields;
            cv.isUnSketchedArea = copyVector.isUnSketchedArea;
            cv.sketchType = copyVector.sketchType;
            cv.areaFieldValue = copyVector.areaFieldValue;
            cv.sectionNum = copyVector.sectionNum;
            if (copyVector.mvpData) {
                var mvData = copyVector.mvpData;
                cv.mvpData = _.clone(mvData);
            }
            if (cv.isUnSketchedArea) {
                cv.fixedAreaTrue = cv.areaFieldValue;
                if (cv.fixedPerimeterTrue)
                    cv.fixedPerimeterTrue = cv.fixedPerimeterTrue;
            }
        }
        var vnum = 0;
        if (editor.currentSketch.vectors.length > 0) {
            vnum = editor.currentSketch.vectors[editor.currentSketch.vectors.length - 1].index + 1;
        }
        //if(editor.config && editor.config.IsJsonFormat)
        cv.isPasteVector = true;

        cv.uid = editor.currentSketch.uid + "/" + vnum;
        cv.index = vnum;
        cv.label = '[' + vnum + '] ' + copyVector.labelFields.filter(function (a) {
            return !a.hiddenFromShow
        }).map(function (a) {
            return ((sketchSettings["DoNotShowLabelCode"] == '1' && a.Description && a.Description.Name && a.Value && !a.Value.contains('/')) ? a.Description.Name : ((a.Description && !a.DoNotShowLabelDescription && clientSettings["DoNotShowLabelDescriptionSketch"] != '1' && sketchSettings["DoNotShowLabelDescriptionSketch"] != '1' && a.Description.Name) ? (editor.formatter.showLabelDescriptionOnly || a.showLabelDescriptionOnly || clientSettings.DoNotShowLabelCodeSketch == '1' ? (editor.currentSketch.config.EnableSwapLookupIdName ? a.Description.Id : a.Description.Name) : (editor.currentSketch.config.EnableSwapLookupIdName ? a.Value + '-' + a.Description.Id : a.Value + '-' + a.Description.Name)) : a.Value))
        }).join(editor.currentSketch.config.LabelDelimiter === undefined ? '/' : editor.currentSketch.config.LabelDelimiter)
        if (copyVector.mvpDisplayLabel && copyVector.sketchType == 'outBuilding') {
            var mvpdisText = copyVector.mvpDisplayLabel.split(']')[1];
            cv.mvpDisplayLabel = '[' + vnum + ']' + mvpdisText;
        }

        if (editor.config && editor.config.ShowSectNum && editor.currentSketch.isShowSectNum && cv.sectionNum) {
            var sNo = cv.sectionNum;
            if (sNo.split('{').length < 2)
                cv.label = cv.label + ' (S:' + sNo + ')';
        }
        cv.name = copyVector.name;
        cv.clientId = ccTicks().toString();
        cv.newRecord = true;
        cv.isChanged = true;
        cv.code = copyVector.code;
        cv.colorCode = copyVector.colorCode ? copyVector.colorCode : null;
        cv.newLineColor = copyVector.newLineColor ? copyVector.newLineColor : null;
        cv.newLabelColorCode = copyVector.newLabelColorCode ? copyVector.newLabelColorCode : null;
        if (!copyVector.disableMove)
            cv.labelPosition = new PointX(copyVector.labelPosition.x, copyVector.labelPosition.y);
        editor.currentSketch.vectors.push(cv);
        editor.currentVector = cv;
        editor.loadVectorSelector(editor.sketchSelector[0], cv, cv.pageNum);
        editor.mode = CAMACloud.Sketching.MODE_EDIT;
        if (copyVector.disableMove) {
            var _vects = editor.currentSketch.vectors.filter(function (x) { return x.isUnSketchedArea })
            var ylen = 45;
            _vects.forEach(function (ve) {
                if (ve.startNode && ve.startNode.p.y < ylen)
                    ylen = ve.startNode.p.y;
            });
            if (_vects.length > 0)
                ylen = ylen - 5;
            cv.fixedArea = copyVector.fixedArea;
            directlyPlaceUnsketchedSegments(editor, cv, 80, ylen);
        }
        else {
            editor.raiseModeChange();
            editor.render();
        }
        editor.currentSketch.isModified = true;
        editor.currentVector.isModified = true;
        copyVector = null;
        if (window.opener)
            window.opener.copyVector = null;
        $('.ccse-meter-length').val('');
        $('.ccse-meter-angle').val('');
        //$('.ccse-scale').val('');
        editor.zoom(t);
        return;

    }
    this.deleteCurrentVector = function () {
        var _removeMultipleVectors = function (v, callBack) {
            v.forEach(function (vt) {
                var cvi = editor.currentSketch.vectors.indexOf(v);
                editor.deletedVectors.push(v);
                editor.currentSketch.vectors.splice(cvi, 1);
            });
            callBack && callBack();
        }
        var _removeCurrentVector = function () {
            var cvi = editor.currentSketch.vectors.indexOf(editor.currentVector);
            editor.deletedVectors.push(editor.currentVector);
            editor.currentSketch.vectors.splice(cvi, 1);
            if (typeof (editor.config.CreateAssociateWindow) == 'function' && editor.currentVector.newRecord && editor.currentVector.visionClientId && visionPCIs?.length > 0) {
                visionPCIs = visionPCIs.filter((x) => { return x.clientId != editor.currentVector.visionClientId });
            }
            editor.currentSketch.isModified = true;
            editor.currentVector = null;
            editor.currentNode = null;
            editor.render();
            editor.loadVectorSelector(editor.sketchSelector[0], null, editor.currentSketch.currentPage);
        }

        var _clearCurrentVector = function () {
            editor.currentVector.vectorString = null;
            editor.currentVector.startNode = null;
            editor.currentVector.endNode = null;
            editor.currentVector.commands = [];
            editor.currentVector.header = null;
            editor.currentVector.isDeletedVector = true;
            editor.currentVector.isClosed = false;
            editor.currentVector.isModified = true;
            editor.currentVector.labelPosition = null;
            editor.render();
            editor.currentNode = null;
            editor.currentVector = null;
        }

        if (this.currentVector) {
            if (this.currentSketch.config.ClearSegmentOnDeletion) {
                _clearCurrentVector();
            } else if (this.formatter.allowSegmentDeletion || this.currentSketch.config.AllowSegmentDeletion) {
                var linkedVectors = this.currentSketch.vectors.filter(function (a) { return this.uniqueId && a.linkedUniqueId == v.currentVector.uniqueId });
                _removeMultipleVectors(linkedVectors, function () { _removeCurrentVector(); })
            } else if (this.currentSketch.config.AllowMultiSegmentAddDelete) {
                var testuid = this.currentVector.uid;
                if (testuid.indexOf("/") > -1) testuid = testuid.substr(0, testuid.indexOf("/"));
                var scount = 0;
                for (var vi in this.currentSketch.vectors) {
                    var v = this.currentSketch.vectors[vi];
                    var vuid = v.uid;
                    if (vuid.indexOf("/") > -1) vuid = vuid.substr(0, vuid.indexOf("/"));
                    if (vuid == testuid) {
                        scount += 1;
                        v.isModified = true;
                    }
                }
                if (scount > 1) {
                    _removeCurrentVector()
                } else {
                    _clearCurrentVector();
                }
            } else {
                _clearCurrentVector();
            }

            $(editor.vectorSelector).val('');
            editor.mode = CAMACloud.Sketching.MODE_DEFAULT;
            editor.raiseModeChange();
        }
    }

    this.deleteNode = function (c) {
        if (c == this.currentVector.startNode) {
            if (c.nextNode) {
                if (c.nextNode.overlaps(c.p)) {
                    this.deleteNode(c.nextNode);
                }
            }
            if (c.nextNode) {
                messageBox('You are not allowed to delete the anchor node of the segment until all other nodes are deleted.');
                return;
                //                var n = c.nextNode;
                //                this.currentVector.startNode = n;
                //                delete c;
                //                this.currentVector.startNode.recalculateAll();

            } else {
                this.currentVector.endNode = null;
                this.currentVector.startNode = null;
                //                var delIndex = this.vectors.indexOf(this.currentVector);
                //                this.vectors.splice(delIndex, 1);
                //                this.currentVector = null;
                this.mode = CAMACloud.Sketching.MODE_NEW;
                this.raiseModeChange();
                this.currentNode = null;
                this.currentVector.isClosed = false;

            }
        } else {
            var p = c.prevNode;
            var n = c.nextNode;
            if (p == this.currentVector.startNode && n == null) {
                p.nextNode = null;
                this.currentVector.isClosed = false;
                this.currentVector.endNode = p;
                this.mode = CAMACloud.Sketching.MODE_NEW;
                this.raiseModeChange();
            }
            else if (!this.currentVector.isClosed && n == null) {
                p.nextNode = null;
                this.currentVector.isClosed = false;
                this.currentVector.endNode = p;
                this.mode = CAMACloud.Sketching.MODE_NEW;
                this.raiseModeChange();
            }
            else if (p == this.currentVector.startNode && p.overlaps(n.p)) {
                p.nextNode = null;
                this.currentVector.isClosed = false;
                this.currentVector.endNode = p;
                this.mode = CAMACloud.Sketching.MODE_NEW;
                this.raiseModeChange();
            } else {
                if (this.currentVector.isClosed) {
                    undoProcessArray.push({ Action: 'DeleteNode', NodeNumber: nodeNumber, NodePoint: _.clone(c.p), Node: _.clone(c) });
                    if (undoProcessArray.length > 20)
                        undoProcessArray.shift();
                }
                nodeNumber = nodeNumber - 1;
                p.nextNode = n;
                if (n) n.prevNode = p;
            }

            delete c;
            p.adjustEllipse();
            this.currentNode = p;
            this.currentVector.startNode.recalculateAll();
        }
        this.currentVector.isModified = true;
        this.render();
    }

    this.addNodeBeforeCurrent = function () {
        var c = this.currentNode;
        if (c == this.currentVector.startNode) {
            messageBox('New node cannot be added before the start node.');
            return false;
        } else if (c.isEllipse || c.isEllipseEndNode) {
            messageBox('You have selected an ellipse resizer node. Add Node cannot be applied here.')
            return false;
        } else {
            var p = c.prevNode;
            if (this.currentVector && this.currentVector.isClosed) {
                undoProcessArray.push({ Action: 'AddNodeBefore', NodeNumber: nodeNumber, NodePoint: _.clone(c.p), Node: _.clone(c) });
                if (undoProcessArray.length > 20)
                    undoProcessArray.shift();
            }
            nodeNumber = nodeNumber;
            var rFunc = (this.currentVector && this.currentVector.isClosed) ? sRound : Math.round;
            var np = new PointX(rFunc((c.p.x + p.p.x) / 2), rFunc((c.p.y + p.p.y) / 2));
            var n = new Node(np, this.currentVector);
            p.nextNode = n;
            n.nextNode = c;
            c.prevNode = n;
            n.prevNode = p;
            p.recalculateAll();
            this.currentNode = n;
            this.render();
            this.currentVector.isModified = true;
        }
    }

    this.addNodeAfterCurrent = function (overlay, ELL) {
        var c = this.currentNode;
        if (!c) return false;
        var p = c.nextNode;
        var np;
        var rFunc = (this.currentVector && this.currentVector.isClosed) ? sRound : Math.round;
        if (p && !overlay)
            np = new PointX(rFunc((c.p.x + p.p.x) / 2), rFunc((c.p.y + p.p.y) / 2));
        else
            np = c.p.copy();
        var n = new Node(np, this.currentVector);
        if (p) {
            if (!ELL) {
                undoProcessArray.push({ Action: 'AddNodeAfter', NodeNumber: nodeNumber, NodePoint: _.clone(c.p), Node: _.clone(c) });
                if (undoProcessArray.length > 20)
                    undoProcessArray.shift();
            }
            nodeNumber = nodeNumber + 1;
            c.nextNode = n;
            n.nextNode = p;
            n.prevNode = c;
            p.prevNode = n;
            c.recalculateAll();
        } else {
            c.nextNode = n;
            n.prevNode = c;
            n.nextNode = null;
            this.currentVector.endNode = n;
        }
        this.currentNode = n;
        this.render();
        this.currentVector.isModified = true;
        return n;
    }

    this.editCurrentVectorLabel = function () {
        var vr = { type: 'null', isEdit: false };
        if (editor.currentVector.sketchType == 'outBuilding') {
            vr = { type: 'outBuilding', isEdit: true };
        }

        multipleInput('Edit Labels', editor.currentVector.labelFields, editor.external.lookup, function (values, optionResults) {
            newlabel = (values.length > 0 && values[0].NewLabel) ? values[0].NewLabel : values.filter(function (a) {
                return a.Target == 'name'
            }).map(function (a) {
                return a.Value
            }).join('') || ((editor.currentSketch.config.AssignLookUpNameAsLabel || (sketchApp.config.ApexEditSketchLabel && sketchSettings['ApexEditSketchLabel'] == '1')) ? values[0].NameValue : values[0].Value);
            if (!newlabel) newlabel = '';

            editor.currentVector.label = '[' + editor.currentVector.index + '] ' + editor.currentVector.labelFields.filter(function (a) {
                return !a.hiddenFromShow
            }).map(function (a) {
                return ((sketchSettings["DoNotShowLabelCode"] == '1' && a.Description && a.Description.Name && a.Value && !a.Value.contains('/')) ? a.Description.Name : ((a.Description && !a.DoNotShowLabelDescription && clientSettings["DoNotShowLabelDescriptionSketch"] != '1' && sketchSettings["DoNotShowLabelDescriptionSketch"] != '1' && a.Description.Name) ? (editor.formatter.showLabelDescriptionOnly || a.showLabelDescriptionOnly || clientSettings.DoNotShowLabelCodeSketch == '1' ? (editor.currentSketch.config.EnableSwapLookupIdName ? a.Description.Id : a.Description.Name) : (editor.currentSketch.config.EnableSwapLookupIdName ? a.Value + '-' + a.Description.Id : a.Value + '-' + a.Description.Name)) : a.Value))
            }).join(editor.currentSketch.config.LabelDelimiter === undefined ? '/' : editor.currentSketch.config.LabelDelimiter)

            if (editor.currentSketch.config.FirstRecordlabelValue && (editor.currentVector.index == 0 || (editor.currentSketch && editor.currentSketch.vectors[0] && editor.currentSketch.vectors[0].index == 1 && editor.currentVector.index == 1))) {
                var labelField = editor.currentSketch.config.FirstRecordlabelValue.split('/');
                var labelFieldValue = labelField[1] ? (editor.currentSketch.parentRow[labelField[0]] || '---') + '/' + (editor.currentSketch.parentRow[labelField[1]] || '---') : s.parentRow[labelField[0]] || '---';
                editor.currentVector.label = '[' + editor.currentVector.index + '] ' + labelFieldValue;
            }
            if (optionResults.mvpDisplayLabel)
                editor.currentVector.mvpDisplayLabel = '[' + editor.currentVector.index + '] ' + optionResults.mvpDisplayLabel;
            if (optionResults.firstLabelValue?.lblValue) {
                editor.currentVector.label = '[' + editor.currentVector.index + '] ' + optionResults.firstLabelValue.lblValue;
                editor.currentVector.firstLabelValueModified = optionResults.firstLabelValue;;
            }

            var cPage = null;
            var colorCodes = (values[0].Description && values[0].Description.color) ? values[0].Description.color : ((values[0].colorDescription && values[0].colorDescription.colorcode) ? values[0].colorDescription.colorcode : '');
            if (colorCodes) {
                var clrcode = colorCodes.split('~');
                var isdtr = window.opener ? window.opener.__DTR : (typeof (__DTR) !== 'undefined' ? __DTR : false);
                let isSvApp = window.opener && window.opener.appType == "sv" ? true : (typeof (appType) !== 'undefined' && appType == "sv" ? true : false);
                var DQC = isdtr ? 'DTR' : (isSvApp ? 'SV' : 'QC');
                editor.currentVector.colorCode = clrcode[0] ? clrcode[0] : null;
                editor.currentVector.newLineColor = (clrcode[1] && sketchSettings['SketchLineColorCustomizations'] && sketchSettings['SketchLineColorCustomizations'].contains(DQC)) ? clrcode[1] : null;
                editor.currentVector.newLabelColorCode = (clrcode[2] && sketchSettings['SketchTextColorCustomizations'] && sketchSettings['SketchTextColorCustomizations'].contains(DQC)) ? clrcode[2] : null;
            }
            if (editor.config && editor.config.IsJsonFormat && editor.currentSketch.currentPage)
                cPage = editor.currentSketch.currentPage;
            editor.currentVector.name = newlabel;
            editor.loadVectorSelector(editor.sketchSelector[0], editor.currentVector, cPage);
            editor.currentVector.labelEdited = true;
            editor.currentVector.labelValueEdited = true;
            editor.currentVector.isModified = true;
            if (editor.config && editor.config.ShowSectNum && editor.currentSketch.isShowSectNum && optionResults.SECTNUM) {
                var sNo = optionResults.SECTNUM;
                if (sNo.split('{').length < 2)
                    editor.currentVector.label = editor.currentVector.label + ' (S:' + sNo + ')';
            }
            if (optionResults.SECTNUM)
                editor.currentVector.sectionNum = optionResults.SECTNUM;
            if (optionResults.BasementFunctionality)
                editor.currentVector.BasementFunctionality = true;
            if (optionResults.MapExteriorFeature)
                editor.currentVector.MapExteriorFeature = true;
            if (optionResults.isAreaMultiplier)
                editor.currentVector.areaMultiplier = optionResults.isAreaMultiplier;
            if (optionResults.vectorExtraParameters)
                editor.currentVector.vectorExtraParameters = optionResults.vectorExtraParameters;
            if (optionResults.mvpData)
                editor.currentVector.mvpData = optionResults.mvpData;
            editor.currentVector.code = values.filter(function (a) {
                return a.Target == 'code'
            }).map(function (a) {
                return a.Value
            }).join('')
            let vsk = editor.currentVector.sketch, vt = editor.currentVector;
            if (vsk.config && vsk.config.EnableMarkedAreaDrawing) {
                if (optionResults.unSketch)
                    vt.fixedArea = vt.areaFieldValue = optionResults.sqft;
                if (vsk.config.EnableOBYEditlabel && vt.vectorString == "" && vt.MarkedAreaValue) {
                    if (!optionResults.unSketch)
                        drawOBYSketch(editor, vt, true);
                    else
                        drawOBYSketch(editor, vt);
                }
                else {
                    if (!optionResults.unSketch && vt.isUnSketchedArea)
                        switchToUnsketch(editor, vt, true);
                    else if (optionResults.unSketch && !vt.isUnSketchedArea)
                        switchToUnsketch(editor, vt);
                }
            }
            editor.render();
        }, editor, 'edit', { 'type': vr.type, 'isEdit': vr.isEdit })
    }

    this.save = function (afterSaveAction, beforeSaveAction, ignoreMsgBox) {
        var onError = function (e) {
            $('.ccse-canvas').removeClass('dimmer');
            $('.dimmer').hide();
            messageBox('Sketch changes are not saved - ' + e);
            editor.saving = false;
        }
        var deletedPageCount = 0;
        //Save sketch changes
        function proceedWithSave(isValidated, invalidMessage, auditTrailEntry) {
            $('.ccse-canvas').addClass('dimmer');
            $('.dimmer').show();
            editor.saving = true;
            var data = [];
            var noteData = [];
            var sketchDataArray = [];
            var changeCount = 0;
            editor.sketches.forEach(function (sk) {
                if (sk.isModified) {
                    sk.wasModified = true;
                    var saveAll = (editor.formatter.saveAllSegments || sk.config.SaveAllSegments), _fCount = 0;
                    if (sk.vectors.length == 0 && saveAll) {
                        var vx = {
                            sketchid: sk.uid,
                            sid: sk.sid,
                            uid: null,
                            label: "",
                            vector: "",
                            area: 0,
                            config: sk.config,
                            parentRow: sk.parentRow,
                            vectorConfig: sk.config.VectorSource[0]
                        }
                        data.push(vx);
                    }

                    if (editor.config.enableManualAreaEntry && sk.MSKetchArray.length > 0) {
                        let mdMsSketch = sk.MSKetchArray;
                        mdMsSketch.forEach((ms) => {
                            let lblGrades = sk.config.LabelAreaGrades, c = 0, lblName = '', proc = false;
                            ms.msketchLabel.split("/").reverse().forEach((slbl) => {
                                let mul = 1;
                                if (slbl?.split('*').length > 1) {
                                    let splbl = slbl.split('*'); slbl = splbl[0].trim();
                                    mul = parseFloat(splbl[1].trim()); if (isNaN(mul)) mul = 1;
                                }

                                if (lblGrades[slbl] != 'B' && !proc) c = 1; proc = true;
                                lblName = slbl + (mul != 1 ? '*' + mul : '') + "{" + c + "}" + (lblName != '' ? '/' : '') + lblName;
                                c = c + 1;
                            });
                            let lbsnm = lblName.replace(/{.*?}/g, '');
                            sk.vectors.push({ name: lbsnm, vectorString: (lblName + ':' + 'A' + ms.mSketchArea), isMsSKetchLabel: true, fixedArea: ms.mSketchArea, vectorConfig: sketchApp.config.sources[0].VectorSource[0] });
                        });
                    }

                    sk.vectors.filter(function (x) {
                        return ((x.isModified || saveAll) && !x.isFreeFormLineEntries)
                    }).forEach(function (v) {
                        var vx = {
                            sketchid: sk.uid,
                            sid: sk.sid,
                            uid: v.uid,
                            label: v.name,
                            vector: (v.isModified || (sk.config?.DrawPlaceHolder && v.isUnSketchedArea)) ? v.toString(true) : v.vectorString,
                            area: v.isMsSKetchLabel ? Number(v.fixedArea) : (v.comTextKeepArea ? v.comTextKeepArea : v.area()),
                            perimeter: v.isMsSKetchLabel ? 0 : (v.comTextKeepPeri ? v.comTextKeepPeri : v.perimeter()),
                            labelCommand: v.isMsSKetchLabel ? null : v.labelCommandString(),
                            dimensionCommand: v.isMsSKetchLabel ? null : v.dimensionCommandString(),
                            config: sk.config,
                            newRecord: v.newRecord,
                            clientId: v.clientId,
                            parentRow: sk.parentRow,
                            outBFields: v.labelFields || [],
                            extraLabels: v.labelFields || [],
                            vectorConfig: v.vectorConfig,
                            isModified: v.isModified,
                            labelDetails: ((v.labelFields && v.labelFields[0].labelDetails && v.labelFields[0].labelDetails.details) ? v.labelFields[0].labelDetails.details : {}),
                            basementDetails: ((v.labelFields && v.labelFields[0].labelDetails && v.labelFields[0].labelDetails.base_array) ? v.labelFields[0].labelDetails.base_array : []),
                            vectorStart: v.isModified ? v.toVectorStartString() : v.vectorStartString,
                            vectorPage: v.vectorPage,
                            footMode: v.footMode,
                            labelPosition: v.labelPosition,
                            type: v.sketchType,
                            outBd_imp_id: v.outBd_imp_id,
                            isUnSketchedArea: v.isUnSketchedArea,
                            isUnSketchedTrueArea: v.isUnSketchedTrueArea,
                            BasementFunctionality: v.BasementFunctionality,
                            MapExteriorFeature: v.MapExteriorFeature,
                            mvpData: v.mvpData,
                            vectorExtraParameters: v.vectorExtraParameters,
                            isPNANewValue: v.isPNANewValue,
                            fixedAreaTrue: v.fixedAreaTrue,
                            sectionNum: v.sectionNum,
                            fixedPerimeterTrue: v.fixedPerimeterTrue,
                            startNode: v.startNode
                        }
                        if (v.isModified) v.wasModified = true;
                        data.push(vx);
                    });

                    if (editor.config.enableManualAreaEntry && sk.MSKetchArray.length > 0) {
                        sk.vectors = sk.vectors.filter((mv) => { return !mv.isMsSKetchLabel });
                    }

                    if (window.opener.ccma.Sketching.Config.IsJsonFormat) {
                        if (sk.jsonString && sk.jsonString.JSON) {
                            sk.jsonString.JSON.LineEntries = [];
                            sk.vectors.filter(function (x) {
                                return (x.isFreeFormLineEntries)
                            }).forEach(function (ve) {
                                var path = '';
                                _fCount = _fCount + 1;
                                var veString = ve.isModified ? ve.toString() : ve.vectorString;
                                if (veString && veString != '')
                                    path = (veString.split(':').length > 1) ? veString.split(':')[1] : '';
                                var le = {
                                    Color: ve.LineEntryColor ? ve.LineEntryColor : '#004088',
                                    KeyCode: ve.KeyCode ? ve.KeyCode : -1,
                                    LinePattern: ve.LinePattern ? ve.LinePattern : 'Continuous',
                                    PageNum: ve.pageNum ? ve.pageNum : 1,
                                    Path: path,
                                    Width: ve.LineEntryWidth ? ve.LineEntryWidth : 3
                                }
                                sk.jsonString.JSON.LineEntries.push(le);
                            });
                        }
                    }

                }
                if ((sk.config.NotesSource && sk.config.NotesSource.Table) || window.opener.ccma.Sketching.Config.NoteDefinition || window.opener.ccma.Sketching.Config.AddNewNote)
                    sk.notes.filter(function (x) {
                        return x.isModified
                    }).forEach(function (n) {
                        var nt = {};
                        if (window.opener.ccma.Sketching.Config.NoteDefinition) {
                            nt = {
                                noteid: n.noteid,
                                noteText: n.isDeleted == true ? '*DEL*' : n.noteText,
                                xPosition: sRound(n.notePosition.x),
                                yPosition: sRound(n.notePosition.y),
                                clientId: n.clientId,
                                parentId: sk.sid,
                                newRecord: n.newRecord,
                                config: sk.config
                            }
                        }
                        else if (window.opener.ccma.Sketching.Config.AddNewNote) {
                            nt = {
                                noteid: n.noteid,
                                noteText: n.isDeleted == true ? '*DEL*' : n.noteText,
                                xPosition: sRound(n.notePosition.x),
                                yPosition: sRound(n.notePosition.y),
                                config: sk.config,
                                parentId: sk.sid,
                                fontSize: n.fontSize ? n.fontSize : 12
                            }
                        }
                        else {
                            var table = sk.config.NotesSource.Table
                            nt = {
                                noteid: n.noteid,
                                noteText: n.isDeleted == true ? '*DEL*' : n.noteText,
                                noteField: sk.config.NotesSource.TextField,
                                noteFieldID: editor.getDataField(table, sk.config.NotesSource.TextField),
                                xPosition: sRound(n.notePosition.x / (sk.config.NotesSource.ScaleFactor || 1)),
                                yPosition: sRound(n.notePosition.y / (sk.config.NotesSource.ScaleFactor || 1)),
                                xPositionField: sk.config.NotesSource.PositionXField,
                                yPositionField: sk.config.NotesSource.PositionYField,
                                xPositionFieldId: editor.getDataField(table, sk.config.NotesSource.PositionXField),
                                yPositionFieldId: editor.getDataField(table, sk.config.NotesSource.PositionYField),
                                sourceName: table,
                                clientId: n.clientId,
                                parentId: sk.sid,
                                newRecord: n.newRecord,
                                config: sk.config
                            }
                        }

                        noteData.push(nt);
                    });
                else if (window.opener.ccma.Sketching.Config.ApexFreeFormTextLabels) {
                    if (sk.isModified && sk.jsonString && sk.jsonString.JSON) {
                        sk.jsonString.JSON.LabelEntries = [];
                        let sdf = sketchSettings?.ApexJsonDefaultFontSize, dfFontSize = (sdf && !isNaN(sdf) && sdf > 0 && sdf <= 20) ? sdf : 10;
                        sk.notes.filter(function (x) {
                            return (!x.isDeleted)
                        }).forEach(function (n) {
                            var xPosition = sRound(n.notePosition.x), yPosition = sRound(n.notePosition.y);
                            var notePosition = xPosition + "," + yPosition;
                            nt = {
                                Bold: n.bold ? n.bold : false,
                                Color: n.color ? n.color : "#000000",
                                FontFace: n.fontFace ? n.fontFace : "Arial",
                                FontSize: n.fontSize ? n.fontSize : dfFontSize,
                                Italic: n.italic ? n.italic : false,
                                KeyCode: n.KeyCode ? n.KeyCode : -1,
                                PageNum: n.pageNum ? n.pageNum : 1,
                                Position: notePosition,
                                Rotation: n.rotation ? n.rotation : 0,
                                Text: (n.noteText)
                            }
                            sk.jsonString.JSON.LabelEntries.push(nt);
                        });
                    }
                }

                if (data.length == 0 && window.opener.ccma.Sketching.Config.IsJsonFormat && sk.vectors.length == _fCount && saveAll) {
                    var vx = { sketchid: sk.uid, sid: sk.sid, uid: "", label: "", vector: "", area: 0, config: sk.config, parentRow: sk.parentRow, vectorConfig: sk.config.VectorSource[0] }
                    data.push(vx);
                }

            });
            var sketchData = {};
            for (var x in data) {
                var d = data[x];
                var c = d.config;
                var sourceName = d.vectorConfig.Table;
                var sourceField = d.vectorConfig.CommandField;
                var areaField = d.vectorConfig.AreaField;
                var perimeterField = d.vectorConfig.PerimeterField;
                var sketchProField = d.vectorConfig.SketchProField;
                var labelCommandField = d.vectorConfig.LabelCommandField;
                var dimensionCommandField = d.vectorConfig.DimensionCommandField;
                var LabelField = d.vectorConfig.LabelField;
                var vectorStartField = d.vectorConfig.VectorStartCommandField;
                var vectorPageField = d.vectorConfig.vectorPagingField;
                var footModeField = d.vectorConfig.FootModeField;
                var sourceFieldId = -1;
                var areaFieldId = -1;
                var perimeterFieldId = -1;
                var sketchProFieldId = -1;
                var labelCommandFieldId = -1;
                var dimensionCommandFieldId = -1;
                var labelFieldId = -1;
                var vectorStartFieldId = -1;
                var vectorPageFieldId = -1;
                var footModeFieldId = -1;
                var extraLabels = []
                try {
                    if (d.extraLabels)
                        d.extraLabels.forEach(function (item, i) {
                            if (i == 0 || (!item.IsEdited && !d.newRecord && !d.isModified) || !item.Field) return;
                            var id = editor.getDataField(sourceName, item.Field);
                            extraLabels.push({
                                FieldId: id,
                                FieldName: item.Field,
                                Value: item.Value
                            })
                        })
                    sourceFieldId = editor.getDataField(sourceName, sourceField);
                    areaFieldId = areaField ? editor.getDataField(sourceName, areaField) : -1;
                    perimeterFieldId = perimeterField ? editor.getDataField(sourceName, perimeterField) : -1;
                    sketchProFieldId = sketchProField ? editor.getDataField(sourceName, sketchProField) : -1;
                    labelCommandFieldId = labelCommandField ? editor.getDataField(sourceName, labelCommandField) : -1;
                    dimensionCommandFieldId = dimensionCommandField ? editor.getDataField(sourceName, dimensionCommandField) : -1;
                    labelFieldId = LabelField && (c.AllowLabelEdit || d.newRecord) ? editor.getDataField(sourceName, LabelField) : -1;
                    vectorStartFieldId = vectorStartField ? editor.getDataField(sourceName, vectorStartField) : -1;
                    vectorPageFieldId = vectorPageField ? editor.getDataField(sourceName, vectorPageField) : -1;
                    footModeFieldId = footModeField ? editor.getDataField(sourceName, footModeField) : -1;
                } catch (e) {
                    if (onError) onError(e.message || e);
                    editor.saving = false;
                    return;
                }

                var insertRecord = false;
                var keyValue = (c.SketchSource.Table == d.vectorConfig.Table ? d.sid : (d.uid ? d.uid : '')).toString();
                if ((c.InsertDataOnAddition || clientSettings['EnableDisto'] == '1') && c.SketchSource.Table != d.vectorConfig.Table && keyValue.indexOf('/') > -1 && d.newRecord)
                    insertRecord = true;

                if (!insertRecord)
                    if (keyValue.indexOf('/') > -1) keyValue = keyValue.substr(0, keyValue.indexOf('/'));
                var itemKey = sourceName + '~' + keyValue;

                if (sketchData[itemKey] == undefined) {
                    sketchData[itemKey] = {
                        sid: keyValue,
                        vectorString: "",
                        sourceName: sourceName,
                        sourceFieldId: sourceFieldId,
                        sourceField: sourceField,
                        areaFieldId: areaFieldId,
                        areaField: areaField,
                        perimeterFieldId: perimeterFieldId,
                        perimeterField: perimeterField,
                        sketchProFieldId: sketchProFieldId,
                        sketchProField: sketchProField,
                        labelCommandField: labelCommandField,
                        labelCommandFieldId: labelCommandFieldId,
                        dimensionCommandField: dimensionCommandField,
                        dimensionCommandFieldId: dimensionCommandFieldId,
                        newRecord: d.newRecord,
                        insertRecord: insertRecord,
                        isDeleted: false,
                        label: d.label,
                        labelField: LabelField,
                        labelFieldId: labelFieldId,
                        area: 0,
                        perimeter: 0,
                        labelCommand: d.labelCommand,
                        clientId: c.SketchSource?.Table == d.vectorConfig?.Table ? keyValue : d.clientId,
                        dimensionCommand: d.dimensionCommand,
                        segments: {}, //Segments for Split-Area calculation,
                        parentRow: d.parentRow,
                        extraLabels: extraLabels,
                        isModified: d.isModified,
                        outBFields: d.outBFields,
                        labelDetails: [],
                        areas: [],
                        perimeters: [],
                        labelPositions: [],
                        basementDetails: [],
                        vectorStartFieldId: vectorStartFieldId,
                        vectorStart: d.vectorStart,
                        vectorStartField: vectorStartField,
                        vectorPageFieldId: vectorPageFieldId,
                        vectorPageField: vectorPageField,
                        vectorPage: d.vectorPage,
                        footModeFieldId: footModeFieldId,
                        footModeField: footModeField,
                        footMode: d.footMode,
                        type: d.type,
                        outBd_imp_id: d.outBd_imp_id,
                        isUnSketchedArea: d.isUnSketchedArea,
                        isUnSketchedTrueArea: d.isUnSketchedTrueArea,
                        BasementFunctionality: d.BasementFunctionality,
                        MapExteriorFeature: d.MapExteriorFeature,
                        mvpData: d.mvpData,
                        vectorExtraParameters: d.vectorExtraParameters,
                        isPNANewValue: d.isPNANewValue,
                        fixedAreaTrue: d.fixedAreaTrue,
                        sectionNum: d.sectionNum,
                        fixedPerimeterTrue: d.fixedPerimeterTrue,
                        startNode: d.startNode
                    };

                    if (c.LabelAreaFields) {
                        for (var lbl in c.LabelAreaFields) {
                            var lf = c.LabelAreaFields[lbl];
                            var lfid = lf ? editor.getDataField(sourceName, lf, (editor?.config?.splitLabelAreaFields ? true : false)) : -1;
                            var key = 'a+' + (lf || '*');
                            sketchData[itemKey].segments[key] = {
                                type: 'Area',
                                label: lbl,
                                field: lf,
                                fieldId: lfid,
                                area: 0.0
                            }
                        }
                    }
                }
                if (d.vector != null && d.vector != '') {
                    if (sketchData[itemKey].vectorString != "")
                        sketchData[itemKey].vectorString += (editor.formatter.vectorSeperator || ",");
                    sketchData[itemKey].vectorString += d.vector;
                    if (sketchData[itemKey].labelDetails)
                        sketchData[itemKey].labelDetails.push(d.labelDetails);
                    if (sketchData[itemKey].basementDetails)
                        sketchData[itemKey].basementDetails.push(d.basementDetails);
                    if (sketchData[itemKey].areas)
                        sketchData[itemKey].areas.push(d.area);
                    if (sketchData[itemKey].perimeters)
                        sketchData[itemKey].perimeters.push(d.perimeter);
                    if (sketchData[itemKey].labelPositions && d.labelPosition)
                        sketchData[itemKey].labelPositions.push({ x: d.labelPosition.x, y: d.labelPosition.y });
                }

                //Split-Area calculation - populate the segments
                try {
                    let splitlbl = editor?.config?.splitLabelAreaFields ? d.label.split("/") : [d.label];
                    splitlbl.forEach((slbl) => {
                        let mul = 1;
                        if (slbl?.split('*').length > 1 && editor?.config?.splitLabelAreaFields) {
                            let splbl = slbl.split('*'); slbl = splbl[0].trim();
                            mul = parseFloat(splbl[1].trim()); if (isNaN(mul)) mul = 1;
                        }
                        var af = c.LabelAreaFields ? c.LabelAreaFields[slbl] || areaField : areaField; //Split-Area calculation - picking the label/area field from sketch config
                        var areaSegmentLabel = 'a+' + (af || '*');
                        if (!sketchData[itemKey].segments[areaSegmentLabel]) {
                            var afId = af ? editor.getDataField(sourceName, af, (editor?.config?.splitLabelAreaFields ? true : false)) : -1;
                            sketchData[itemKey].segments[areaSegmentLabel] = {
                                type: 'Area',
                                label: slbl,
                                field: af,
                                fieldId: afId,
                                area: 0.0
                            }
                        }

                        //Split-Area calculation - accumulate area in case of multiple segments with same label.
                        sketchData[itemKey].segments[areaSegmentLabel].area += (d.area || 0) * mul;
                    });

                } catch (ex) {
                    if (onError) onError(ex.message || ex);
                    editor.saving = false;
                    return;
                }

                //Split-Area calculation - Compute and append the summary fields.
                //Provision added for multiple summary fields, if required in future
                for (var sumField in c.AreaSummaryFields) {
                    var sfId = sumField ? editor.getDataField(sourceName, sumField) : -1;
                    var sumLabel = "as+" + sumField;
                    if (!sketchData[itemKey].segments[sumLabel]) {
                        sketchData[itemKey].segments[sumLabel] = {
                            type: 'AreaSummary',
                            label: sumLabel,
                            field: sumField,
                            fieldId: sfId,
                            area: 0.0
                        }
                    }
                    let splitlbl = editor?.config?.splitLabelAreaFields ? d.label.split("/") : [d.label];
                    splitlbl.forEach((slbl) => {
                        let mul = 1;
                        if (slbl?.split('*').length > 1 && editor?.config?.splitLabelAreaFields) {
                            let splbl = slbl.split('*'); slbl = splbl[0].trim();
                            mul = parseFloat(splbl[1].trim()); if (isNaN(mul)) mul = 1;
                        }

                        if (c.AreaSummaryFields[sumField].indexOf(slbl) > -1) {
                            sketchData[itemKey].segments[sumLabel].area += d.area * mul;
                        }
                    });
                }
                //Split-Area calculation - end of processing

                sketchData[itemKey].area += d.area || 0;
                sketchData[itemKey].perimeter += d.perimeter || 0;
            }


            for (var x in sketchData) {
                var skd = sketchData[x];
                if (c.DeleteBlankSegmentsOnSave) {
                    if (skd.vectorString == "") {
                        skd.isDeleted = true;
                        skd.vectorString = "*DEL*";
                    }
                }
                if (skd.vectorString != "*DEL*") {
                    if (editor.formatter.encodeSketch && !window.opener.ccma.Sketching.Config.doNotDecodeEncode) {
                        skd.vectorString = editor.formatter.encodeSketch(skd.vectorString);
                    }
                    else if (editor?.config?.splitLabelAreaFields && skd.vectorString != '' && skd.vectorString[skd.vectorString.length - 1] != ',') {
                        skd.vectorString = skd.vectorString + ',';
                    }
                }
                if (window.opener.ccma.Sketching.Config.IsJsonFormat && skd.vectorString != "*DEL*") {
                    var vectString = skd.vectorString;
                    var skdSid = skd.sid;
                    var sket = editor.sketches.filter(function (skt) { return skt.sid == skdSid })[0];
                    if (sket) {
                        var jstring = sket.jsonString;
                        jstring.DCS = vectString;
                        skd.vectorString = JSON.stringify(jstring);
                    }
                }

                if (window.opener.ccma.Sketching.Config.isHelionRS) {
                    let sket = editor.sketches.filter(function (skt) { return skt.sid == skd.sid })[0],
                        nts = sket.notes.filter((vn) => { return !vn.isDeleted }), vs = skd.vectorString.trim();

                    if (vs == '*DEL*') vs = '';
                    else if (vs[vs.length - 1] != ';') vs += ';';

                    nts.forEach((vn) => {
                        if (vn.isModified) {
                            let ntxt = vn.noteText, x = vn.notePosition.x, y = vn.notePosition.y, noteData = '';
                            if (vn.newRecord || !vn.noteData) {
                                noteData = {
                                    Font: { Bold: false, Italic: false, Name: "Times New Roman", Size: 10, Strikeout: false, Underline: false },
                                    FontColor: { A: 255, R: 0, G: 0, B: 0 },
                                    Rotation: 0,
                                    Text: ntxt
                                };
                            }
                            else {
                                noteData = { Font: vn.noteData.Font, FontColor: vn.noteData.FontColor, FontColor: vn.noteData.FontColor, Text: ntxt }
                            }

                            if (noteData != '') {
                                noteData = Base64.encode(JSON.stringify(noteData));
                                vs += 'Text[' + noteData + ']:' + (x > 0 ? ('R' + x) : ('L' + (-x))) + ' ' + (y > 0 ? ('U' + y) : ('D' + (-y))) + ';';
                            }
                        }
                        else {
                            let nd = vn.noteData.encodedText.trim(), hs = nd[nd.length - 1] == ';' ? nd : (nd + ';');
                            vs += hs;
                        }
                    });

                    skd.vectorString = vs;
                }

                if (skd.sketchProFieldId != -1) {
                    let skLbls = editor?.sketches.filter((sxx) => { return sxx.sid == skd.sid })[0];
                    if (skLbls) {
                        let skLblsSplit = skLbls.label.split("[")[0].trim().split("/");
                        skLbls = "";
                        skLblsSplit.forEach((sls) => {
                            skLbls = skLbls + (sls != '' ? (sls + "/") : "");
                        });
                        skLbls = skLbls.slice(0, -1);
                    }
                    else skLbls = '';
                    let sketchProStr = skd.vectorString != '' ? convertHelionToSketchPro(skd.vectorString, skLbls, skd.labelPositions) : '';
                    skd.sketchProData = sketchProStr;
                }

                if (window.opener.ccma.Sketching.Config.EnableVisionNotes) {
                    let sket = editor.sketches.filter((skt) => { return skt.sid == skd.sid })[0],
                        nts = sket.notes.filter((vn) => { return !vn.isDeleted }), vs = skd.vectorString.trim();

                    if (vs == '*DEL*') vs = '';
                    else if (vs[vs.length - 1] != ';') vs += ';';

                    nts.forEach((vn) => {
                        if (vn.isModified) {
                            let ntxt = Base64.encode(vn.noteText), xp = vn.notePosition.x, yp = vn.notePosition.y;
                            if (vn.newRecord) {
                                let sx = xp - (vn.width/2), sy = yp + (vn.height/2),
                                    hs = '[-1,' + vn.sectNum + ',,,3,' + ntxt + ']:' + ((sx > 0) ? ('R' + sx) : ('L' + (-sx))) + '/' + ((sy > 0) ? ('U' + sy) : ('D' + (-sy))) + ' S R' + vn.width + ' D' + vn.height + ' L' + vn.width + ' U' + vn.height + ';';
                                vs += hs; vn.noteData = hs;
                            }
                            else {
                                let nds = vn.noteData.split(']:'), hs = nds[0].split(','), ins = nds[1].replace(';', '').split(' S '), sll = ins[1].split(' '), sla = [sll[0], sll[1]], vw = 0, vh = 0;
                                hs[hs.length - 1] = ntxt; hs = hs.toString() + ']:';

                                /*for (s in sla) {
                                    let cp = sla[s].trim().match(/[UDLR#]|[0-9]+(.[0-9]+)?/g), dir = cp[0], dis = cp[1] || 0;
                                    switch (dir) {
                                        case "U": vh += parseFloat(-dis); break;
                                        case "D": vh += parseFloat(dis); break;
                                        case "L": vw += parseFloat(dis); break;
                                        case "R": vw += parseFloat(-dis); break;
                                    }
                                }*/

                                let sx = xp - vn.ww, sy = yp - vn.hh; //vw = Math.abs(vw); vh = Math.abs(vh)
                                hs = hs + ((sx > 0) ? ('R' + sx) : ('L' + (-sx))) + '/' + ((sy > 0) ? ('U' + sy) : ('D' + (-sy))) + ' S ' + ins[1] + ';';
                                vs += hs; vn.noteData = hs;
                            }
                        }
                        else {
                            let nd = vn.noteData.trim(), hs = nd[nd.length - 1] == ';' ? nd : (nd + ';');
                            vs += hs; vn.noteData = hs;
                        }
                    });

                    skd.vectorString = vs;
                }

                sketchDataArray.push(skd);
                if (skd.segments)
                    for (var aField in skd.segments) {
                        var item = skd.segments[aField]
                        if (item && item.fieldId > -1 && item.area > 0) {
                            var f = sketchApp.external.datafields[item.fieldId]
                            if (f && f.NumericScale == "0" || f.NumericScale === 0)
                                item.area = Math.round(item.area)
                            else if (f && f.NumericScale > 0)
                                item.area = item.area.toFixed(f.NumericScale)
                        }
                    }
            }

            if (window.opener.ccma.Sketching.Config.EnableVisionNotes || window.opener.ccma.Sketching.Config.isHelionRS) noteData = [];

            var sketchDataArrayRowIds = [];
            sketchDataArray.forEach(function (sk) {
                sketchDataArrayRowIds.push({
                    'source': sk.sourceName,
                    'rowid': sk.sid
                });
            });
            //sketchDataArrayRowIds = (sketchDataArrayRowIds.length > 0) ? sketchDataArrayRowIds.slice(0, -1) : '';
            changeCount = sketchDataArray.length;
            NoteChangeCount = noteData.length;
            var preProcessor = function (callback) {
                if (callback) callback();
            }
            if (changeCount > 0 || NoteChangeCount > 0 || deletedPageCount > 0) preProcessor = saveParcelChange;
            preProcessor(function (afterLoad) {
                var sketchSaveCallBack = function (callback) {
                    editor.saving = false;
                    var clearModified = function () {
                        editor.deletedVectors = [];
                        editor.sketches.forEach(function (sk) {
                            sk.isModified = false;
                            sk.vectors.forEach(function (v) {
                                v.isModified = false;
                                v.newRecord = false;
                            });
                            sk.notes.forEach(function (n) {
                                n.isModified = false;
                                n.newRecord = false;
                            });
                        });
                    }

                    if (!(editor.config && editor.config.clearModifiedAfterSave))
                        clearModified();
                    var sb = editor.sketchBounds();
                    editor.resetOrigin();
                    editor.panOrigin(sb, editor.formatter.originPosition);
                    var processor = function (callback, preCallback) {
                        var callbackProcessor = function () {
                            $('.ccse-canvas').removeClass('dimmer');
                            $('.dimmer').hide();
                            if (afterLoad) afterLoad();
                            if (changeCount > 0 || NoteChangeCount > 0 || deletedPageCount > 0) {
                                var _sketchReload = function () {
                                    var sketches = loadSketchSegments(null, true);
                                    if (ignoreMsgBox) {
                                        editor.refreshSketchesAfterSave(sketches);
                                        if (preCallback) preCallback();
                                        return true;
                                    }
                                    else
                                        messageBox('All changes to the sketch have been saved.', ['Close', 'Continue'], function () {
                                            editor.close();
                                            sketchopened = false;
                                            _enableWarehouse && CCWarehouse.SketchClose();
                                        }, function () {
                                            editor.refreshSketchesAfterSave(sketches);
                                            return true;
                                        });
                                }
                                if (window.opener && window.opener.calculationExpressionSketch) {
                                    $('.ccse-canvas').addClass('dimmer');
                                    $('.app-mask-layer-info').html("Processing Calculation Expression...");
                                    $('.dimmer').show();
                                    window.opener.calculationExpressionSketch(function () {
                                        $('.app-mask-layer-info').html("Please wait...");
                                        $('.ccse-canvas').removeClass('dimmer');
                                        $('.dimmer').hide();
                                        _sketchReload();
                                    });
                                } else
                                    _sketchReload();
                            } else if (ignoreMsgBox) {
                                if (preCallback) preCallback();
                                return true;
                            }
                            else messageBox('No changes found!');
                        }
                        if (callback) callback(function () {
                            callbackProcessor();
                        });
                        else callbackProcessor();
                    }
                    if (ignoreMsgBox) processor(callback, function () { afterSaveAction(sketchDataArrayRowIds); if (editor.config && editor.config.clearModifiedAfterSave) clearModified(); });
                    else if (afterSaveAction && isValidated) {
                        afterSaveAction(sketchDataArrayRowIds, function () {
                            if (editor.config && editor.config.clearModifiedAfterSave)
                                clearModified();
                            processor(callback);
                        });
                    } else
                        processor(callback);
                }
                if (sketchApp.config.onSketchSave)
                    sketchApp.config.onSketchSave(sketchDataArray, noteData, sketchSaveCallBack, onError, invalidMessage, auditTrailEntry)
                else if (editor.onSave)
                    editor.onSave(sketchDataArray, noteData, function () { }, sketchSaveCallBack, onError, invalidMessage, auditTrailEntry);
                return sketchDataArray;
            });
        }

        var isSNOutside = false;
        var originPos = editor.formatter.originPosition;
        if (window.opener.ccma.Sketching.Config.isShowBoundary || window.opener.ccma.Sketching.Config.EnableBoundary) {
            editor.sketches.forEach(function (sk) {
                var enByTrue = false, bnScale = 0;
                if (window.opener.ccma.Sketching.Config.EnableBoundary) {
                    enByTrue = true;
                    var bnScale = sk.boundaryScale ? sk.boundaryScale : 100;
                }

                var sModified = sk.isModified;
                if (!sModified) {
                    var nmodified = sk.notes.filter(function (n) { return n.isModified });
                    if (nmodified.length > 0)
                        sModified = true;
                }

                if (sModified && !isSNOutside) {
                    for (var i in sk.vectors) {
                        var vect = sk.vectors[i];
                        /*if(enByTrue && vect.sketchType == 'outBuilding' && vect.isUnSketchedArea)
                            continue;*/
                        if (isSNOutside)
                            return;
                        var vPoints = vect.toPoints();
                        for (var j in vPoints) {
                            if (originPos == "topRight") {
                                if (vPoints[j].x > 0 || vPoints[j].y > 0 || (enByTrue && ((vPoints[j].x < (-bnScale)) || (vPoints[j].y < (-bnScale))))) {
                                    isSNOutside = true;
                                    return;
                                }
                            }
                            else if (originPos == "topLeft") {
                                if (vPoints[j].x < 0 || vPoints[j].y > 0 || (enByTrue && ((vPoints[j].x > (bnScale)) || (vPoints[j].y < (-bnScale))))) {
                                    isSNOutside = true;
                                    return;
                                }
                            }
                            else {
                                if (vPoints[j].x < 0 || vPoints[j].y < 0 || (enByTrue && ((vPoints[j].x > (bnScale)) || (vPoints[j].y > (bnScale))))) {
                                    isSNOutside = true;
                                    return;
                                }
                            }
                        }
                        if ((!isSNOutside) && (vect.labelPosition) && (vect.sketchType != 'outBuilding' || (vect.sketchType == 'outBuilding' && !vect.isUnSketchedArea))) {
                            var lPos = vect.labelPosition;
                            if (originPos == "topRight") {
                                if (lPos.x > 0 || lPos.y > 0 || (enByTrue && ((lPos.x < (-bnScale)) || (lPos.y < (-bnScale))))) {
                                    isSNOutside = true;
                                    return;
                                }
                            }
                            else if (originPos == "topLeft") {
                                if (lPos.x < 0 || lPos.y > 0 || (enByTrue && ((lPos.x > (bnScale)) || (lPos.y < (-bnScale))))) {
                                    isSNOutside = true;
                                    return;
                                }
                            }
                            else {
                                if (lPos.x < 0 || lPos.y < 0 || (enByTrue && ((lPos.x > (bnScale)) || (lPos.y > (bnScale))))) {
                                    isSNOutside = true;
                                    return;
                                }
                            }
                        }
                    }
                    if (!isSNOutside) {
                        var sknotes = sk.notes.filter(function (n) { return !n.isDeleted });
                        for (var i in sknotes) {
                            var nt = sknotes[i];
                            if (isSNOutside)
                                return;
                            var nPos = nt.notePosition;
                            if (originPos == "topRight") {
                                if (nPos.x > 0 || nPos.y > 0 || (enByTrue && ((nPos.x < (-bnScale)) || (nPos.y < (-bnScale))))) {
                                    isSNOutside = true;
                                    return;
                                }
                            }
                            else if (originPos == "topLeft") {
                                if (nPos.x < 0 || nPos.y > 0 || (enByTrue && ((nPos.x > (bnScale)) || (nPos.y < (-bnScale))))) {
                                    isSNOutside = true;
                                    return;
                                }
                            }
                            else {
                                if (nPos.x < 0 || nPos.y < 0 || (enByTrue && ((nPos.x > (bnScale)) || (nPos.y > (bnScale))))) {
                                    isSNOutside = true;
                                    return;
                                }
                            }
                        }
                    }
                }
            });
        }
        if (isSNOutside) {
            messageBox('"There are sketches or notes that cross the dashed boundary lines. These sketch segments or notes must be moved within the dashed lines in order to save the sketch."');
            return false;
        }

        var warning = 'There is an incomplete sketch segment. Please finalize (close) the sketch, or delete the segment.';
        var valid = true;
        var continueWithWarning = false;
        this.sketches.forEach(function (sk) {
            var res = sk.validateVectors();
            if (!res.valid) {
                valid = false;
                warning = res.error;
                if (!res.preventSave && sketchSettings["DoNotAllowOpenSegments"] == "1")
                    warning = "Cannot save changes!<br>&nbsp; There are unclosed segments that must be closed<br> before the sketch can be saved.<br>";
                else if (!(sk.config.DoNotAllowOpenSegments || editor.formatter.preventSavingOpenSegments)) {
                    warning += " Are you sure you want to continue saving this?";
                    continueWithWarning = true;
                }
            }
        });

        if (window.opener.ccma.Sketching.Config.warnings) {
            if (valid) {
                warning = window.opener.ccma.Sketching.Config.warnings;
                continueWithWarning = true;
            }
            else if (continueWithWarning)
                warning = window.opener.ccma.Sketching.Config.warnings + '<br>' + warning;
        }

        this.sketches.forEach(function (sk) {
            if (sk.vectors.length > 0 && sk.vectors[0].doNotAllowDeleteFirstVector == true && sk.vectors[0].isModified && sk.vectors[0].toString() == '' && (sk.vectors[0].vectorString == null || sk.vectors[0].vectorString == '')) {
                valid = false;
                warning = 'Delete process denied as you cannot delete the first vector.';
            }
        });
        /* this.sketches.forEach(function (sk) {
              if (sk.vectors.length > 0 && sk.vectors[0].maxVectorLimit > 0 && sk.vectors.length > sk.vectors[0].maxVectorLimit){
                    valid = false;
                    warning = 'The vector count has exceeded the maximum vector limit for the source ' + sk.vectors[0].vectorConfig.Table + '(' + sk.vectors[0].maxVectorLimit + ').'
              }
         }); */
        deletedPageCount = editor.rsDeletedPages ? editor.rsDeletedPages.length: 0;

        var fprocessor = function (sketches) {
            $('.Current_vector_details #Btn_Save_vector_properties').removeAttr('disabled');
            if (beforeSaveAction) {
                validateParcelChange(function () {
                    beforeSaveAction(sketches, function (isValid, proceedToSave, invalidMessage, auditTrailEntry) {
                        if (proceedToSave)
                            proceedWithSave(isValid, invalidMessage, auditTrailEntry);
                        else return;
                    });
                });
            }
            else associateVector(function () { proceedWithSave(true); })
        }
        var sketches = this.sketches;
        if ((valid || (ignoreMsgBox && continueWithWarning)) && !window.opener.ccma.Sketching.Config.warnings)
            return fprocessor(sketches);
        else {
            if (continueWithWarning) {
                messageBox(warning, ["OK", "Cancel"], function () {
                    return fprocessor(sketches);
                }, function () {

                });
            } else {
                messageBox(warning);
            }
        }
    }

    this.getDataField = function (sourceName, fieldName, caseInsensitive) {
        if (fieldName == null) return null;
        var checkField = _.filter(editor.external.datafields, (obj) => {
            if (caseInsensitive) return caseInsensitiveCompare(obj.SourceTable, sourceName) && caseInsensitiveCompare(obj.Name, fieldName);
            else return obj.SourceTable == sourceName && obj.Name == fieldName;
        });

        /*
            _.where(editor.external.datafields, {
            "SourceTable": sourceName,
            "Name": fieldName
        });
        */
        if (checkField.length == 0) {
            throw {
                message: ('Field not found - ' + fieldName + ' in ' + (sourceName || 'Parcel Data'))
            };
            return;
        }
        return checkField[0].Id;
    }

    this.close = function () {
        if (editor.saving) {
            messageBox('Please wait.. Sketch changes being saved.');
            return;
        }

        var isModified = false;
        if (editor.isOpenSketchEditorTrue)
            editor.isOpenSketchEditorTrue = false;
        editor.sketches.forEach(function (sk) {
            isModified = isModified || sk.isModified;
            sk.vectors.forEach(function (v) {
                isModified = isModified || v.isModified;
            });
            sk.notes.forEach(function (n) {
                isModified = isModified || n.isModified;
            });
            if (editor.rsDeletedPages?.length > 0 && !isModified) isModified = true;
        });

        var t = this;

        if (isModified && !t.CC_beforeSketch) {
            //            messageBox('There are unsaved changes in the sketch. Tap OK to continue on this editor or Cancel to lose changes and return to last screen.', ["OK", "Cancel"], function () {
            //                return;
            //            }, function () {
            //                t.currentVector = null;
            //                t.currentSketch = null;
            //                t.currentNode = null;
            //                t.onClose();
            //            });
            messageBox(msg_sketcheditor_close, ["OK", "Cancel"], function () {
                t.currentVector = null;
                t.currentSketch = null;
                t.currentNode = null;
                t.onClose();
                sketchopened = false;
                _enableWarehouse && CCWarehouse.SketchClose();
                if (t.onUnSave)
                    t.onUnSave();
                localStorage.setItem("sketchCache", '')
            }, function () {
                return;
            });
            return;
        }

        this.currentVector = null;
        this.currentSketch = null;
        this.currentNode = null;
        this.onClose();
        sketchopened = false;
        _enableWarehouse && CCWarehouse.SketchClose();
    }

    this.bounds = function () {
        var bb = new PointX(0, 0);
        this.sketches.forEach(function (sk) {
            for (x in sk.vectors) {
                var b = new PointX(0, 0);
                var v = sk.vectors[x];
                var sn = v.startNode;
                while (sn != null) {
                    if (sn.sdx == "R") {
                        b.x += sn.dx;
                    }
                    if (sn.sdy == "U") {
                        b.y += sn.dy;
                    }

                    sn = sn.nextNode;
                }

                if (b.x > bb.x) {
                    bb.x = b.x;
                }
                if (b.y > bb.y) {
                    bb.y = b.y;
                }
            }
        });


        return bb;
    }

    this.lbounds = function () {
        var bb = new PointX(0, 0);
        this.sketches.forEach(function (sk) {
            for (x in sk.vectors) {
                var b = new PointX(0, 0);
                var v = sk.vectors[x];
                var sn = v.startNode;
                while (sn != null) {
                    if (sn.sdx == "L") {
                        b.x -= sn.dx;
                    }
                    if (sn.sdy == "D") {
                        b.y -= sn.dy;
                    }

                    sn = sn.nextNode;
                }

                if (b.x < bb.x) {
                    bb.x = b.x;
                }
                if (b.y < bb.y) {
                    bb.y = b.y;
                }
            }
        });

        return bb;
    }

    this.allbounds = function (isPView) {
        var x1 = 0,
            x2 = 0,
            y1 = 0,
            y2 = 0;
        this.sketches.forEach(function (sk) {
            for (x in sk.vectors) {
                var b = new PointX(0, 0);
                var v = sk.vectors[x];
                var sn = v.startNode;
                while (sn != null) {
                    x1 = Math.min(x1, isNaN(sn.p.x) ? 0 : sn.p.x);
                    x2 = Math.max(x2, isNaN(sn.p.x) ? 0 : sn.p.x);
                    y1 = Math.min(y1, isNaN(sn.p.y) ? 0 : sn.p.y);
                    y2 = Math.max(y2, isNaN(sn.p.y) ? 0 : sn.p.y);

                    if (sn.isArc) {
                        var mp = sn.midpoint;
                        x1 = Math.min(x1, isNaN(mp.x) ? 0 : mp.x);
                        x2 = Math.max(x2, isNaN(mp.x) ? 0 : mp.x);
                        y1 = Math.min(y1, isNaN(mp.y) ? 0 : mp.y);
                        y2 = Math.max(y2, isNaN(mp.y) ? 0 : mp.y);
                    }

                    sn = sn.nextNode;
                }

                if (isPView && v.labelPosition && !(v.sketchType == 'outBuilding' && v.isUnSketchedArea)) {
                    var lb = v.labelPosition;
                    x1 = Math.min(x1, isNaN(lb.x) ? 0 : lb.x - 10);
                    x2 = Math.max(x2, isNaN(lb.x) ? 0 : lb.x + 10);
                    y1 = Math.min(y1, isNaN(lb.y) ? 0 : lb.y - 10);
                    y2 = Math.max(y2, isNaN(lb.y) ? 0 : lb.y + 10);
                }
            }

            for (x in sk.notes) {
                var n = sk.notes[x];
                var np = n.notePosition;
                var nx1 = np.x, nx2 = np.x, ny1 = np.y, ny2 = np.y;

                if (isPView) {
                    var ntText = (n.noteText || ''), ntlen = 0;
                    ntText.split(/\s+/).forEach(function (tp) {
                        if (tp.length > ntlen) ntlen = tp.length;
                    });
                    nx1 = nx1 - ntlen; nx2 = nx2 + ntlen; ny1 = ny1 - 15; ny2 = ny2 + 15;
                }

                x1 = Math.min(x1, isNaN(nx1) ? 0 : nx1);
                x2 = Math.max(x2, isNaN(nx2) ? 0 : nx2);
                y1 = Math.min(y1, isNaN(ny1) ? 0 : ny1);
                y2 = Math.max(y2, isNaN(ny2) ? 0 : ny2);
            }
        });
        return {
            xmax: x2,
            ymax: y2,
            xmin: x1,
            ymin: y1
        };
    }

    this.sketchBounds = function (isPView) {
        var x1 = 0,
            x2 = 0,
            y1 = 0,
            y2 = 0;
        if (this.currentSketch) {
            var sk = this.currentSketch;
            for (x in sk.vectors) {
                var b = new PointX(0, 0);
                var v = sk.vectors[x];
                var sn = v.startNode;
                while (sn != null) {
                    x1 = Math.min(x1, isNaN(sn.p.x) ? 0 : sn.p.x);
                    x2 = Math.max(x2, isNaN(sn.p.x) ? 0 : sn.p.x);
                    y1 = Math.min(y1, isNaN(sn.p.y) ? 0 : sn.p.y);
                    y2 = Math.max(y2, isNaN(sn.p.y) ? 0 : sn.p.y);

                    if (sn.isArc) {
                        var mp = sn.midpoint;
                        x1 = Math.min(x1, isNaN(mp.x) ? 0 : mp.x);
                        x2 = Math.max(x2, isNaN(mp.x) ? 0 : mp.x);
                        y1 = Math.min(y1, isNaN(mp.y) ? 0 : mp.y);
                        y2 = Math.max(y2, isNaN(mp.y) ? 0 : mp.y);
                    }

                    sn = sn.nextNode;
                }

                if (isPView && v.labelPosition && !(v.sketchType == 'outBuilding' && v.isUnSketchedArea)) {
                    var lb = v.labelPosition;
                    x1 = Math.min(x1, isNaN(lb.x) ? 0 : lb.x - 10);
                    x2 = Math.max(x2, isNaN(lb.x) ? 0 : lb.x + 10);
                    y1 = Math.min(y1, isNaN(lb.y) ? 0 : lb.y - 10);
                    y2 = Math.max(y2, isNaN(lb.y) ? 0 : lb.y + 10);
                }
            }
            for (x in sk.notes) {
                var n = sk.notes[x];
                var np = n.notePosition;
                var nx1 = np.x, nx2 = np.x, ny1 = np.y, ny2 = np.y;

                if (isPView) {
                    var ntText = (n.noteText || ''), ntlen = 0;
                    ntText.split(/\s+/).forEach(function (tp) {
                        if (tp.length > ntlen) ntlen = tp.length;
                    });
                    nx1 = nx1 - ntlen; nx2 = nx2 + ntlen; ny1 = ny1 - 15; ny2 = ny2 + 15;
                }

                x1 = Math.min(x1, isNaN(nx1) ? 0 : nx1);
                x2 = Math.max(x2, isNaN(nx2) ? 0 : nx2);
                y1 = Math.min(y1, isNaN(ny1) ? 0 : ny1);
                y2 = Math.max(y2, isNaN(ny2) ? 0 : ny2);
            }
        }
        return {
            xmax: x2,
            ymax: y2,
            xmin: x1,
            ymin: y1
        };
    }

    this.processVectorCmd = function (cmd, dist, angle) {
        var d = {};
        if (cmd != 'U' && cmd != 'D' && cmd != 'R' && cmd != 'L' && cmd != 'OK')
            return false;
        else if (sketchApp && sketchApp.rotationButtonEnabled) {
            if (cmd == 'U' || cmd == 'D' || angle == 0)
                return false;
            if ((cmd == 'OK' || cmd == 'R' || cmd == 'L') && angle != 0) {
                if ((sketchSettings && sketchSettings["EnableRotation"] == "1") || (clientSettings && clientSettings["EnableRotation"] == "1")) {
                    if (cmd == 'L')
                        angle = -(angle);
                    var points = this.currentVector.toPoints();
                    undoProcessArray.push({ Action: 'RotateSketch', NodeNumber: null, NodePoint: _.clone(this.currentVector.startNode.p), Node: _.clone(this.currentVector.startNode), Points: points });
                    if (undoProcessArray.length > 20)
                        undoProcessArray.shift();
                    var lx = 0, ly = 0, ux = 0, uy = 0;
                    for (var x in points) {
                        var pt = points[x];
                        if (x == 0) {
                            lx = ux = pt.x;
                            ly = uy = pt.y;
                        }
                        else {
                            (pt.x < lx) ? lx = pt.x : ((pt.x > ux) ? ux = pt.x : null);
                            (ly > pt.y) ? ly = pt.y : ((uy < pt.y) ? uy = pt.y : null);
                        }
                    }
                    var cen = {};
                    cen.x = ((lx + ux) / 2);
                    cen.y = ((ly + uy) / 2);
                    var sn = this.currentVector.startNode;
                    var R = (angle * Math.PI / 180);
                    if (!sn) return;
                    while (sn != null) {
                        if ((sn == this.currentVector.endNode) && (sn.overlaps(this.currentVector.startNode.p))) {

                        }
                        else {
                            var x1 = Number((((sn.p.x - cen.x) * (Math.cos(R))) + (((sn.p.y - cen.y) * (Math.sin(R))) + cen.x)));
                            var y1 = Number(-((((sn.p.x - cen.x) * (Math.sin(R)))) - (((sn.p.y - cen.y) * (Math.cos(R))) + cen.y)));
                            sn.p.x = parseInt(x1 * 10) / 10
                            sn.p.y = parseInt(y1 * 10) / 10;
                        }
                        sn = sn.nextNode;
                    }
                    if (this.currentVector.startNode) {
                        this.currentVector.startNode.recalculateAll();
                    }
                    this.currentVector.isModified = true;
                    this.render();
                }
                else if ((sketchSettings && sketchSettings["EnableRotation"] == "1") || (clientSettings && clientSettings["EnableRotation"] != "1")) {
                    messageBox("Sketch Rotation is not Enabled.");
                }
            }
            $('.sketch-pad .sp-left span').css("background-image", "url(/App_Static/css/sketchpad/sp-left.png)");
            $('.sketch-pad .sp-right span').css("background-image", "url(/App_Static/css/sketchpad/sp-right.png)");
            this.hideKeypad();
            sketchApp.rotationButtonEnabled = false;
            return false;
        }
        else if (sketchApp && !sketchApp.rotationButtonEnabled && cmd == 'OK') {
            return false;
        }
        if (dist == 0) {
            var cvDistance = [], ovDistance = [];
            var sn = this.currentVector.startNode;
            if (!sn) return false;
            while (sn != null) {
                var nn = sn.nextNode;
                if (!nn) break;
                if (cmd == 'R' || cmd == 'L') {
                    if (sn.p.x == nn.p.x)
                        cvDistance.push(sn.p.x);
                }
                else {
                    if (sn.p.y == nn.p.y)
                        cvDistance.push(sn.p.y);
                }
                sn = sn.nextNode;
            }
            for (var x in this.currentSketch.vectors) {
                if (this.currentSketch.vectors[x] != this.currentVector) {
                    var vsn = this.currentSketch.vectors[x].startNode;
                    if (!vsn) continue;
                    while (vsn != null) {
                        var vnn = vsn.nextNode;
                        if (!vnn) break;
                        if (cmd == 'R' || cmd == 'L') {
                            if (vsn.p.x == vsn.p.x)
                                ovDistance.push(vsn.p.x);
                        }
                        else {
                            if (vsn.p.y == vsn.p.y)
                                ovDistance.push(vsn.p.y);
                        }
                        vsn = vsn.nextNode;
                    }
                }
            }
            var minX = null, maxX = null, minY = null, maxY = null;
            if (cmd == 'L') {
                var ovMaxX;
                if (cvDistance.length > 0) {
                    cvDistance.forEach(function (cxVal) {
                        if (!minX) minX = cxVal;
                        minX = Math.min(minX, cxVal);
                    });
                }
                if (minX) {
                    ovDistance.forEach(function (ovxVal) {
                        if (ovxVal < minX) {
                            if (!ovMaxX) ovMaxX = ovxVal;
                            ovMaxX = Math.max(ovMaxX, ovxVal);
                        }
                    });
                    if (ovMaxX)
                        dist = Math.abs(minX - ovMaxX);
                }
                if (dist == 0 || dist > 5)
                    dist = 1;
            }
            else if (cmd == 'R') {
                var ovMinX;
                if (cvDistance.length > 0) {
                    cvDistance.forEach(function (cxVal) {
                        if (!maxX) maxX = cxVal;
                        maxX = Math.max(maxX, cxVal);
                    });
                }
                if (maxX) {
                    ovDistance.forEach(function (ovxVal) {
                        if (ovxVal > maxX) {
                            if (!ovMinX) ovMinX = ovxVal;
                            ovMinX = Math.min(ovMinX, ovxVal);
                        }
                    });
                    if (ovMinX)
                        dist = Math.abs(ovMinX - maxX);
                }
                if (dist == 0 || dist > 5)
                    dist = 1;

            }
            else if (cmd == 'U') {
                var ovMinY;
                if (cvDistance.length > 0) {
                    cvDistance.forEach(function (cxVal) {
                        if (!maxY) maxY = cxVal;
                        maxY = Math.max(maxY, cxVal);
                    });
                }
                if (maxY) {
                    ovDistance.forEach(function (ovyVal) {
                        if (ovyVal > maxY) {
                            if (!ovMinY) ovMinY = ovyVal;
                            ovMinY = Math.min(ovMinY, ovyVal);
                        }
                    });
                    if (ovMinY)
                        dist = Math.abs(ovMinY - maxY);
                }
                if (dist == 0 || dist > 5)
                    dist = 1;
            }
            else if (cmd == 'D') {
                var ovMaxY;
                if (cvDistance.length > 0) {
                    cvDistance.forEach(function (cxVal) {
                        if (!minY) minY = cxVal;
                        minY = Math.min(minY, cxVal);
                    });
                }
                if (minY) {
                    ovDistance.forEach(function (ovyVal) {
                        if (ovyVal < minY) {
                            if (!ovMaxY) ovMaxY = ovyVal;
                            ovMaxY = Math.max(ovMaxY, ovyVal);
                        }
                    });
                    if (ovMaxY)
                        dist = Math.abs(minY - ovMaxY);
                }
                if (dist == 0 || dist > 5)
                    dist = 1;
            }
        }
        switch (cmd) {
            case "U":
                d.x = 0; d.y = (-1 * dist);
                break;
            case "D":
                d.x = 0; d.y = (1 * dist);
                break;
            case "L":
                d.x = (1 * dist); d.y = 0;
                break;
            case "R":
                d.x = (-1 * dist); d.y = 0;
                break;
        }

        if (this.currentVector.isClosed || this.currentVector.isFreeFormLineEntries) {
            undoProcessArray.push({ Action: 'SketchMove', NodeNumber: null, NodePoint: _.clone(this.currentVector.startNode.p), Node: _.clone(this.currentVector.startNode), LabelPosition: _.clone(editor.currentVector.labelPosition), AreaLabelPosition: _.clone(editor.currentVector.AreaLabelPosition) });
            if (undoProcessArray.length > 20)
                undoProcessArray.shift();
        }

        this.currentVector.moveBy(d, null, true);
        if (this.currentVector) this.currentVector.isModified = true;
        this.currentVector.drawnUsingKeys = true;
        this.render();
    }

    this.processCmd = function (cmd, dist, angle) {
        if (!this.currentVector) return false;
        if (!this.currentNode) {
            if (cmd != "+")
                if (this.currentVector) {
                    this.processVectorCmd(cmd, dist, angle);
                    return false;
                }
                else
                    return false;
        };

        if (cmd == 'U' || cmd == 'D' || cmd == 'R' || cmd == 'L') {
            if (this.currentNode && this.currentVector && (this.currentVector.isClosed || this.currentVector.isFreeFormLineEntries))
                undoProcessArray.push({ Action: 'NodeMove', NodeNumber: nodeNumber, NodePoint: _.clone(this.currentNode.p), Node: _.clone(this.currentNode) });
            if (undoProcessArray.length > 20)
                undoProcessArray.shift();
        }

        switch (cmd) {
            case "U":
                if (dist == 0) dist = 1;
                var p = new PointX(this.currentNode.p.x, this.currentNode.p.y)
                p.moveBy(0, -1 * dist);
                if (!this.checkOverLapAndClose(p)) {
                    this.currentNode.p.moveBy(0, -1 * dist);
                    this.currentNode.recalculateAll();
                }
                break;
            case "D":
                if (dist == 0) dist = 1;
                var p = new PointX(this.currentNode.p.x, this.currentNode.p.y)
                p.moveBy(0, 1 * dist);
                if (!this.checkOverLapAndClose(p)) {
                    this.currentNode.p.moveBy(0, 1 * dist);
                    this.currentNode.recalculateAll();
                }
                break;
            case "L":
                if (dist == 0) dist = 1;
                var p = new PointX(this.currentNode.p.x, this.currentNode.p.y)
                p.moveBy(-1 * dist, 0);
                if (!this.checkOverLapAndClose(p)) {
                    this.currentNode.p.moveBy(-1 * dist, 0);
                    this.currentNode.recalculateAll();
                }
                break;
            case "R":
                if (dist == 0) dist = 1;
                var p = new PointX(this.currentNode.p.x, this.currentNode.p.y)
                p.moveBy(1 * dist, 0);
                if (!this.checkOverLapAndClose(p)) {
                    this.currentNode.p.moveBy(1 * dist, 0);
                    this.currentNode.recalculateAll();
                }
                break;
            case "A":
                if (!(this.currentVector && this.currentVector.isFreeFormLineEntries)) {
                    this.processCmd("+", true);
                    if (!this.currentNode)
                        break;
                    var sgn = 1;
                    if (angle != 0)
                        sgn = angle / Math.abs(angle);
                    angle = sgn * (180 - Math.abs(angle));

                    var rAngle = 90;
                    var p1 = this.currentNode.p;
                    var p2 = p1.copy();
                    var a = angle;

                    if (this.currentNode.prevNode) {
                        if (this.currentNode.prevNode.prevNode) {
                            p2 = this.currentNode.prevNode.prevNode.p;
                            rAngle = Math.atan((p1.y - p2.y) / (p1.x - p2.x)) * (180 / Math.PI);
                            if ((p2.y < p1.y) && (p2.x > p1.x)) rAngle = 180 + rAngle;
                            else if ((p2.y > p1.y) && (p2.x > p1.x)) rAngle = rAngle - 180;
                            else if ((p2.y == p1.y) && (p2.x > p1.x)) rAngle = 180 - rAngle;
                            if (isNaN(rAngle)) rAngle = 0;
                        }
                    }

                    a = rAngle + angle;
                    //console.log("Effective angle: ", a);
                    var dx = Math.cos(a * Math.PI / 180) * dist;
                    var dy = Math.sin(a * Math.PI / 180) * dist;
                    //console.log(rAngle, a, dx, dy);
                    this.currentNode.p.moveBy(0, -1 * dy);
                    this.currentNode.p.moveBy(1 * dx, 0);
                    this.currentNode.recalculateAll();
                }
                break;
            case "+":
                if (this.currentNode && this.currentVector.continuedrawing)
                    this.currentVector.continueVector(this.currentNode.p.copy())
                else if (this.mode == CAMACloud.Sketching.MODE_NEW) {
                    if (!this.currentNode && !this.currentVector.startNode) {
                        this.registerNode();
                        return false;
                    }
                    if (this.currentNode.prevNode)
                        if (this.currentNode.overlaps(this.currentNode.prevNode.p)) {
                            if (!dist)
                                messageBox('Node created already. Please move it to a different location before creating a new one.');
                            return false;
                        }
                    this.currentVector.connect(this.currentNode.p.copy(), true, false, true);
                } else if (this.mode == CAMACloud.Sketching.MODE_EDIT) {
                    if (this.currentNode && !this.currentVector.isFreeFormLineEntries) {
                        var cv = this.currentVector.isClosed ? false : true;
                        this.addNodeAfterCurrent(null, cv);
                    }
                }

                break;
            case "OK":
                if (sketchApp.currentNode && sketchApp.currentVector.continuedrawing) {
                    sketchApp.currentNode.nextNode.noStroke = false;
                    sketchApp.currentVector.continuedrawing = false;
                }
                else if (this.currentVector.isFreeFormLineEntries) {
                }
                else if (this.currentVector.startNode && this.currentVector.startNode.nextNode == this.currentVector.endNode) {
                    messageBox('Can not close a shape with less than 3 points');
                    return;
                }
                else if (!this.currentVector.isClosed) {
                    this.currentVector.terminateNode(this.currentVector.startNode);
                    this.currentVector.isModified = true;
                    this.currentVector.close();
                }
                this.hideKeypad();
                break;
            case "BOX":
                if ((this.currentNode == this.currentVector.startNode) && (this.currentNode.nextNode == null)) {
                    if (dist == 0) {
                        messageBox('Enter a valid length.');
                        return;
                    }
                    this.processCmd('+');
                    this.processCmd("U", dist);
                    this.processCmd('+');
                    this.processCmd("R", dist);
                    this.processCmd('+');
                    this.processCmd("D", dist);
                    this.processCmd("OK")
                    this.hideKeypad();
                } else {
                    messageBox('You cannot create box from this point.');
                    return;
                }
                break;
            case "RECT":
                if ((this.currentNode == this.currentVector.startNode) && (this.currentNode.nextNode == null)) {
                    var distance = dist.split('/');
                    if (distance[0] == 0) {
                        messageBox('Enter a valid length.');
                        return;
                    }
                    if (distance[1] == 0) {
                        messageBox('Enter a valid width.');
                        return;
                    }
                    this.processCmd('+');
                    this.processCmd("U", distance[0]);
                    this.processCmd('+');
                    this.processCmd("R", distance[1]);
                    this.processCmd('+');
                    this.processCmd("D", distance[0]);
                    this.processCmd("OK")
                    this.hideKeypad();
                } else {
                    messageBox('You cannot create rectangle from this point.');
                    return;
                }
                break;
            case "ARC":
                if (this.currentNode && this.currentNode.prevNode) {
                    if (this.currentNode.overlaps(this.currentNode.prevNode.p)) {
                        return;
                    }
                    var cn = this.currentNode;
                    if (this.currentVector.isClosed || this.currentVector.isFreeFormLineEntries) {
                        undoProcessArray.push({ Action: 'ARC', NodeNumber: nodeNumber, NodePoint: _.clone(cn.p), Node: _.clone(cn) });
                        if (undoProcessArray.length > 20)
                            undoProcessArray.shift();
                    }
                    this.currentNode.arcLength = dist;
                    this.currentNode.isArc = (dist != 0);

                    cn.recalculateAll();
                    this.currentVector.isModified = true;

                }
                break;
            case "ARX":
                if (this.currentNode && this.currentNode.prevNode) {
                    if (this.currentNode.overlaps(this.currentNode.prevNode.p)) {
                        return;
                    }
                    var cn = this.currentNode;
                    if (this.currentVector.isClosed || this.currentVector.isFreeFormLineEntries) {
                        undoProcessArray.push({ Action: 'ARC', NodeNumber: nodeNumber, NodePoint: _.clone(cn.p), Node: _.clone(cn) });
                        if (undoProcessArray.length > 20)
                            undoProcessArray.shift();
                    }

                    this.currentNode.arcLength = -dist;
                    this.currentNode.isArc = (dist != 0);

                    cn.recalculateAll();
                    this.currentVector.isModified = true;

                }
                break;
            case "ELL":
                if (this.currentNode && (!(this.currentVector && this.currentVector.isFreeFormLineEntries))) {
                    if (this.currentNode.nextNode)
                        if (this.currentNode.nextNode.isEllipse) {
                            messageBox('Another ellipse exists on the same point.');
                            return;
                        }
                    if (this.currentNode.isEllipse) {
                        messageBox('You cannot create ellipse from this point.');
                        return;
                    }
                    var cn = this.currentNode;
                    if (this.currentVector.isClosed) {
                        undoProcessArray.push({ Action: 'ELL', NodeNumber: nodeNumber, NodePoint: _.clone(cn.p), Node: _.clone(cn) });
                        if (undoProcessArray.length > 20)
                            undoProcessArray.shift();
                    }
                    var a = this.addNodeAfterCurrent(true, true);
                    a.p.moveBy(0, -1 * dist);
                    a.p.moveBy(1 * dist, 0);
                    a.recalculateAll();
                    a.isEllipse = true;

                    var b = this.addNodeAfterCurrent(true, true);
                    b.p.moveBy(0, 1 * dist);
                    b.p.moveBy(-1 * dist, 0);
                    b.recalculateAll();
                    b.isEllipseEndNode = true;

                    cn.recalculateAll();
                    this.currentVector.isModified = true;
                }
                break;
            case "POLY":
                if (this.currentNode) {
                    if (this.currentNode.prevNode == this.currentVector.startNode && this.currentNode.nextNode == null) {
                        var p1 = this.currentVector.startNode.p;
                        var p2 = this.currentNode.p;
                        console.log(p1.distanceFrom(p2), DEFAULT_PPF, this.scale);
                        var dist = Math.round(p1.distanceFrom(p2) * 100) / 100;
                        if (dist == 0) {
                            messageBox('Draw a line of non-zero length.');
                        } else {
                            var numNodes = parseInt($('.sketch-poly-sides').val());
                            var sn = this.currentNode;
                            var ang = (360 / numNodes) - 180;
                            for (var ni = 0; ni < numNodes - 2; ni++) {
                                this.processCmd("A", dist, ang);
                            }
                            this.processCmd("OK");
                        }
                    } else {
                        messageBox("Polygons can be created only from a single line drawn from a start node. The polygon will be drawn clockwise from the line you have created.")
                    }
                } else {
                    messageBox("Invalid state to generate polygon.")
                }
                break;
        }
        if (this.currentVector) this.currentVector.isModified = true;
        if (this.currentNode) {
            var sb = this.screenBounds();
            var p = this.currentNode.p;
            if (p.x < sb.lx) {
                this.pan(((sb.lx - p.x) * DEFAULT_PPF) * this.scale + 25, 0)
            }
            if (p.x > sb.ux) {
                this.pan(((sb.ux - p.x) * DEFAULT_PPF) * this.scale - 25, 0)
            }
            if (p.y < sb.ly) {
                this.pan(0, ((sb.ly - p.y) * DEFAULT_PPF) * this.scale + 25)
            }
            if (p.y > sb.uy) {
                this.pan(((sb.uy - p.y) * DEFAULT_PPF) * this.scale - 25, 0)
            }
        }

        this.currentVector.drawnUsingKeys = true;
        this.render();
    }
    this.checkOverLapAndClose = function (p) {
        if (this.currentVector.startNode.overlaps(p)) {
            var nn = this.currentVector.endNode;
            var pp = nn.prevNode;
            this.currentVector.endNode = pp;
            pp.nextNode = null;
            delete nn;
            this.currentVector.terminateNode(this.currentVector.startNode);
            this.currentVector.isModified = true;
            this.currentVector.close();
            return true
        }
        if (this.currentVector.continuedrawing && this.currentNode.nextNode.overlaps(p)) {
            this.currentNode.nextNode.noStroke = false;
            this.deleteCurrentNode();
            this.currentVector.continuedrawing = false;
            return true;
        }
        return false;
    }
    var pz = {};
    this.autoPan = function (dir, clear, dp) {
        if (dp) dp = Math.abs(dp);

        if (!clear) {
            if (!pz[dir]) {
                var cmd = 'sketchApp.panCanvas("' + dir + '")';
                pz[dir] = window.setTimeout(cmd, 1500);
            }
        } else {
            if (pz[dir]) {
                window.clearTimeout(pz[dir]);
                pz[dir] = null;
            }
        }
    }

    this.clearAutoPanning = function () {
        for (var x in pz) {
            if (pz[x]) {
                window.clearTimeout(pz[x]);
                pz[x] = null;
            }
        }
    }

    this.panCanvas = function (dir) {
        var pl = 120;
        if (pz[dir]) pz[dir] = null;
        switch (dir) {
            case "U":
                editor.pan(0, pl);
                break;
            case "D":
                editor.pan(0, -pl);
                break;
            case "L":
                editor.pan(-pl, 0);
                break;
            case "R":
                editor.pan(pl, 0);
                break;
        }
    }


    this.CVProfile = function (a) {
        if (!this.currentVector) return false;
        if (!a) a = 0;
        var sn = this.currentVector.startNode;
        while (sn != null) {
            var p1 = sn.p;
            var p2 = p1.copy();
            if (sn.nextNode) {
                var n = sn.nextNode;
                p2 = n.p;
                var dsq = 0;
                for (var x in n.path) {
                    dsq += n.path[x].dist * n.path[x].dist;
                }
                var dist = Math.round(Math.sqrt(dsq));
                var rad = Math.abs(Math.atan((p2.y - p1.y) / (p2.x - p1.x)));
                var sgn = 1;
                if ((p2.y < p1.y) && (p2.x < p1.x)) {
                    sgn = -1;
                } else if ((p2.y == p1.y) && (p2.x < p1.x)) {
                    sgn = -1;
                } else if ((p2.y < p1.y) && (p2.x == p1.x)) {
                    sgn = -1;
                }

                rAngle = (rad + a) * (180 / Math.PI);
                if (isNaN(rAngle)) rAngle = 0;
                console.log(dist + 'ft ' + Math.round(sgn * rAngle) + '°');
            }

            sn = sn.nextNode;
        }

        return "";
    }

    this.showKeypad = function () {
        if (this.keypad) {
            $(this.keypad.selector).show();
            this.keypadActive = true;
            this.keypad.resetText();
            var cposKeypad = $(this.keypad.selector).position(), docWidth = $(document.body).width(), docHeight = $(document.body).height();
            if (cposKeypad.left < 0 || cposKeypad.left > (docWidth - 20) || cposKeypad.top > (docHeight - 20)) { $(this.keypad.selector).css('left', '0px'); $(this.keypad.selector).css('top', '0px'); }
        }
    }

    this.hideKeypad = function () {
        if (this.keypad) $(this.keypad.selector).hide();
        this.keypadActive = false;
    }

    this.loadNotesForSketch = function (sketch, notes, apexFreeFormLabels, extraFields) {
        for (var i in notes) {
            var extraJsonNoteFields = null, extraNoteFields = null;
            var n = notes[i];
            if (apexFreeFormLabels) {
                extraJsonNoteFields = { bold: n['Bold'], color: n['Color'], fontFace: n['FontFace'], fontSize: n['FontSize'], italic: n['Italic'], pageNum: n['PageNum'], rotation: n['Rotation'], KeyCode: n['KeyCode'] }
            }
            else if (extraFields) {
                extraNoteFields = {};
                for (var nx in extraFields) {
                    extraNoteFields[extraFields[nx]] = n[extraFields[nx]];
                }
            }
            var note = new Annotation(this, sketch, n.text, new PointX(n.x, n.y), new PointX(n.lx, n.ly), new PointX(n.x, n.y), n.uid, extraJsonNoteFields, extraNoteFields);
            sketch.notes.push(note);
        }
    }

    this.$ = function () {
        if (this.currentVector) {
            return this.currentVector.toString();
        }
    }
    this.openDisto = function (callBack) {
        if (!this.currentSketch) {
            messageBox('Please select a sketch to continue.');
            return;
        } else if (!this.currentVector && this.currentSketch.config.AllowMultiSegmentAddDelete) {
            messageBox('Please select a vector to continue.');
            return;
        } else if (this.currentVector && this.currentVector.isClosed && !this.currentSketch.config.AllowMultiSegmentAddDelete) {
            messageBox('Cannot add a new segment to selected vector.');
            return;
        }
        if (callBack) callBack()
    }
    this.refreshSketchesAfterSave = function (sketches, beForeAfterView) {
        editor.isRefreshAfterSave = true;
        var scale = editor.scale;
        editor.zoom(1);
        var index = (sketchApp.sketches.indexOf(sketchApp.currentSketch) + 1);
        var current_Page = null;
        if (editor.config && editor.config.IsJsonFormat && editor.currentSketch && editor.currentSketch.currentPage)
            current_Page = editor.currentSketch.currentPage;
        editor.open(sketches, editor.currentSketch ? editor.currentSketch.uid + '/' + index : null, null, editor.afterSave, editor.beforeSave, editor.CC_beforeSketch, null, null, true, current_Page);
        editor.resetOrigin(false, beForeAfterView);
        editor.panOrigin(editor.sketchBounds(), editor.formatter.originPosition, null, false, beForeAfterView);
        editor.zoom(scale);
    }
    this.addVectorFromCache = function (s) {
        //  var s = localStorage.getItem( "sketchCache" )
        if (s && s != '' && editor.formatter.addSketchFromCache == true) {
            var vs = s.split('$');
            var pid = vs[0];
            if (pid == activeParcel.Id)
                messageBox('There are some unsaved sketches from the last session. Do you want to recover? ', ["Recover", "Discard"], function () {
                    sketchApp.zoom(1)
                    var vectors = vs[1].split('%%');
                    vectors.forEach(function (item, no) {
                        var values = item.split('#')
                        var sid = values[0];
                        var sk = editor.sketches.filter(function (a) { return a.uid == sid })
                        if (sk && sk.length > 0) sk = sk[0]
                        else return
                        var vid = values[1];
                        var vt = sk.vectors.filter(function (a) { return a.uid == vid })
                        if (vt.length > 0) {
                            if (vt && vt.length > 0) vt = vt[0]
                            else return
                            if (editor.formatter.updateVectorFromString) {
                                editor.formatter.updateVectorFromString(editor, vt, values[3]);
                            }
                            else {
                                editor.formatter.vectorFromString(editor, values[3]);
                            }
                            vt.isChanged = true;
                            vt.isModified = true;
                            var savedLabels = values[2].split('|')
                            vt.labelFields.forEach(function (a, i) {
                                a.Value = savedLabels[i]
                            })
                            if (values.length > 6)
                                vt.fixedArea = values[6]
                        }
                        else {
                            var v = editor.formatter.vectorFromString(editor, values[3]);
                            sk.vectors.push(v);
                            var cindex = values[4];
                            var savedLabels = values[2].split('|')
                            var vr = sk.config.VectorSource[parseInt(cindex) || 0]
                            var labelFields = loadFieldProperties(sk, vr);
                            labelFields.forEach(function (a, i) {
                                a.Value = savedLabels[i]
                            })
                            v.sketch = sk;
                            v.uid = values[1];
                            v.index = sk.vectors.length + 1;
                            v.vectorString = values[3];
                            v.labelFields = labelFields;
                            v.pageNum = sk.currentPage;
                            v.code = labelFields[0].Value;
                            v.name = labelFields[0].Value;
                            v.label = '[' + sk.vectors.length + '] ' + labelFields.map(function (a) { return a.Description && a.Description.Name ? a.Value + '-' + a.Description.Name : a.Value }).filter(function (a) { return a }).join('/');
                            v.isChanged = true;
                            v.isModified = true;
                            v.newRecord = true;
                            v.vectorConfig = vr;
                            v.clientId = values[5]
                            if (values.length > 6)
                                v.fixedArea = values[6]

                        }
                        if (vectors.length - 2 == no) {
                            var i = editor.sketches.indexOf(sk);
                            $(editor.sketchSelector)[0].selectedIndex = (i + 1);
                            $(editor.sketchSelector).change();
                        }
                    })
                    sketchApp.panSketchPosition();
                })
        }
    }
    this.addVectorFromString = function (vectorString, doNotClose, distoCallback) {
        var pass = editor.createNewVector;
        if (this.currentVector && !sketchApp.currentVector.isClosed)
            pass = function (callBack) {
                if (callBack) callBack()
            }
        else if (this.currentVector && this.currentSketch.config.AllowMultiSegmentAddDelete)
            pass = editor.createNewVectorSegment;
        pass(function () {
            if (!(editor.currentVector && editor.currentVector.startNode))
                editor.processCmd('+');
            var parts = vectorString.split(' ');
            var first = true,
                points = 0;
            for (i in parts) {
                var p = parts[i];
                var sparts = p.split('/');
                editor.processCmd('+');
                for (j in sparts) {
                    var cmd = sparts[j].toString();
                    if (cmd != "") {
                        var dir = cmd.charAt(0);
                        var dist = parseFloat(cmd.replace(dir, ''));
                        editor.processCmd(dir, dist);
                        points += 1;
                    }
                }
            }
            if (points > 0 && !doNotClose)
                editor.processCmd("OK");
            if (distoCallback) distoCallback()
        }, true)
    }

    this.processCmdString = function (vectorString, moveOnly) {
        if (!(editor.currentVector && editor.currentVector.startNode) && !moveOnly)
            editor.processCmd('+');
        var parts = vectorString.split(' ');
        var first = true,
            points = 0;
        for (i in parts) {
            var p = parts[i];
            var sparts = p.split('/');
            if (!moveOnly)
                editor.processCmd('+');
            for (j in sparts) {
                var cmd = sparts[j].toString();
                if (cmd != "") {
                    var dir = cmd.charAt(0);
                    var dist = parseFloat(cmd.replace(dir, ''));
                    editor.processCmd(dir, dist);
                    points += 1;
                }
            }
        }
    }

    init();
}


function Node(p, v) {
    this.vector = null;
    this.p = new PointX();
    this.dx = 0;
    this.dy = 0;
    this.sdx = "";
    this.sdy = "";
    this.path = [];

    this.isVeer = false;
    this.isEllipse = false;
    this.isEllipseEndNode = false;
    this.isStart = false;
    this.commands = [];
    this.isArc = false;
    this.arcLength = 0;
    this.hideDimensions = false;

    this.nextNode = null;
    this.prevNode = null;

    this.__defineGetter__("length", function () {
        if (this.isStart) return NaN;
        var p1 = this.prevNode.p;;
        var p2 = this.p;
        return p1.getDistanceTo(p2);
    });

    this.__defineGetter__("direction", function () {
        if (this.isStart) return NaN;
        var p1 = this.prevNode.p;;
        var p2 = this.p;
        return p1.getDirectionInDegrees(p2);
    });

    this.__defineGetter__("midpoint", function () {
        if (this.isStart) return null;
        var p1 = this.prevNode.p;;
        var p2 = this.p;
        var mp = p1.midpointTo(p2);
        if (this.arcLength == 0) {
            return mp;
        }
        var angle = p1.getDirectionInDegrees(p2);
        var pangle = angle - ARC_SIGN * sign(this.arcLength) * 90; //Reverse ARC_SIGN here, since theoretical graph and computer graph differs
        if (pangle > 360) pangle = pangle - 360;
        mp = mp.getPointAt(pangle, this.arcLength);
        return mp;
    });

    this.__defineGetter__("radius", function () {
        if (this.isStart) return NaN;
        if (this.arcLength == 0) return Infinity;
        var p1 = this.prevNode.p;
        var p2 = this.p;
        var angle = this.direction;
        var p3 = this.midpoint;
        var ap1p3 = p1.getDirectionInDegrees(p3);
        var leftInclusive = Math.abs(ap1p3 - angle);
        if (leftInclusive > 90) leftInclusive = 360 - leftInclusive;
        var topInclusive = 180 - 2 * leftInclusive;
        var dp1p3 = p1.getDistanceTo(p3);
        return radius = Math.abs((dp1p3 / 2) / Math.cos(topInclusive / 2 * Math.PI / 180));
    });

    this.__defineGetter__("arcCenter", function () {
        if (this.isStart) return null;
        if (this.arcLength == 0) return null;

        var pangle = this.direction - ARC_SIGN * sign(this.arcLength) * 90; //Reverse ARC_SIGN here, since theoretical graph and computer graph differs
        if (pangle >= 360) pangle -= 360;
        var rpangle = pangle + 180;
        if (rpangle >= 360) rpangle -= 360;
        return this.midpoint.getPointAt(rpangle, this.radius);
    });

    this.arcArea = function () {
        var area = 0;
        if (this.isStart) return 0;
        if (this.arcLength == 0) return 0;
        var c = this.arcCenter;
        var r = this.radius;
        var pi = Math.PI;

        var p1 = this.prevNode.p;
        var p2 = this.p;
        var mp = this.midpoint;

        var halfAngle = c.getDirectionInDegrees(mp) - c.getDirectionInDegrees(p1);
        if (halfAngle > 180) halfAngle = halfAngle - 360;
        if (halfAngle < -180) halfAngle = 360 + halfAngle;
        halfAngle = Math.abs(halfAngle);

        var fullAngle = halfAngle * 2;
        var intersect = p1.getDistanceTo(p2);
        var angleHeight = Math.sqrt(r * r - intersect * intersect / 4)

        if (halfAngle == 90) {
            area = pi * r * r / 2;
        } else if (halfAngle < 90) {
            area = pi * r * r * fullAngle / 360 - angleHeight * (intersect / 2);
        } else {
            area = pi * r * r * fullAngle / 360 + angleHeight * (intersect / 2);
        }

        return area;
    }

    this.arcPerimeter = function () {
        var perimeter = 0;
        if (this.isStart) return 0;
        if (this.arcLength == 0) return this.length;
        var c = this.arcCenter;
        var r = this.radius;
        var pi = Math.PI;

        var p1 = this.prevNode.p;
        var p2 = this.p;
        var mp = this.midpoint;

        var halfAngle = c.getDirectionInDegrees(mp) - c.getDirectionInDegrees(p1);
        if (halfAngle > 180) halfAngle = halfAngle - 360;
        if (halfAngle < -180) halfAngle = 360 + halfAngle;
        halfAngle = Math.abs(halfAngle);

        var fullAngle = halfAngle * 2;

        perimeter = 2 * pi * r * fullAngle / 360;

        return perimeter
    }

    this.ellipseArea = function (dxv, dyv) {
        if (this.isEllipse) {
            var xv = (dxv || dxv == 0) ? dxv : this.dx;
            var yv = (dyv || dyv == 0) ? dyv : this.dy;
            return Math.PI * xv * yv;
        }
        return 0;
    }

    this.connect = function (p, pn) {
        var n = new Node(p, this.vector);
        this.nextNode = n;
        n.prevNode = this;
        n.calculateVector(this.p);
        if (pn) {
            n.isArc = pn.Arc;
            n.isEllipse = pn.Ellipse;
            n.arcLength = pn.arcLength;
            n.isEllipseEndNode = pn.EllipseEndNode;
        }
        //        this.calculateVector(p);
        this.vector.endNode = n;
        return n;
    }

    this.overlaps = function (p, tol) {
        if (tol) {
            if ((p.x >= this.p.x - tol) && (p.x <= this.p.x + tol) && (p.y >= this.p.y - tol) && (p.y <= this.p.y + tol)) {
                return true;
            }
        }
        if ((p.x == this.p.x) && (p.y == this.p.y)) {
            return true;
        } else {
            return false;
        }
    }

    this.moveTo = function (p) {
        this.p.x = p.x;
        this.p.y = p.y;
        this.recalculateAll();
    }
    this.recalculateAll = function () {
        if (this.vector.editor.mode == CAMACloud.Sketching.MODE_NEW && this.prevNode) {
            var p1 = this.p;
            var editor = this.vector.editor;
            var p2 = this.prevNode.p;
            var p3, p4
            var d = p1.distanceFrom(p2);
            d = sRound(Math.round(d * (DEFAULT_PPF * 10)) / 100);
            var angle = 360 - (p2.getDirectionInDegrees(p1));
            $('.ccse-meter-length').val(d + (editor.formatter.LengthUnit || DISTANCE_UNIT || ''));
            if (editor.currentNode) {
                p3 = editor.currentNode.prevNode.p;
                prevnode = editor.currentNode.prevNode
            }
            if (prevnode && prevnode.prevNode)
                p4 = editor.currentNode.prevNode.prevNode.p;
            if (p3 && p4) {
                var angle2 = p3.getDirectionInDegrees(p4);
                var angle = (360 - angle2) - angle;
                if (angle < 0) {
                    angle = 360 + angle
                }
                if (isNaN(angle))
                    $('.ccse-meter-angle').val('0°');
                else
                    $('.ccse-meter-angle').val(parseInt(angle) + ' °');
            }
        }
        this.adjustEllipse();
        var nn = this;
        var ref;
        while (nn != null) {
            if (nn.prevNode)
                ref = nn.prevNode.p;
            else
                ref = nn.vector.editor.origin.copy().alignToGrid(nn.vector.editor); // Math.ceil
            nn.calculateVector(ref);
            nn = nn.nextNode;
        }
        if (editor && editor.currentNode) {
            var cn = editor.currentNode.p
            var sc = editor.screenBounds()
            if (sc.uy < cn.y || sc.ly > cn.y || sc.ux < cn.x || sc.lx > cn.x)
                editor.panSketchPosition();
        }
    }

    //node-calculate-vector
    this.calculateVector = function (ref) {
        var ed = this.vector.editor;
        var p = this.p;

        var scale = ed.scale;
        //Decimation
        //        var pdx = Math.round((p.x - ref.x));
        //        var pdy = Math.round((p.y - ref.y));
        var pdx = Number((p.x - ref.x).toFixed(2));
        var pdy = Number((p.y - ref.y).toFixed(2));
        this.sdx = pdx >= 0 ? "R" : "L";
        this.sdy = pdy >= 0 ? "U" : "D";
        this.dx = Math.abs(pdx);
        this.dy = Math.abs(pdy);

        this.isVeer = (this.dx > 0) && (this.dy > 0) && (this.isStart == false);
        this.path = [];

        if (this.dy > 0) {
            this.path.push({
                "dir": this.sdy,
                "dist": this.dy
            });
        }

        if (this.dx > 0) {
            this.path.push({
                "dir": this.sdx,
                "dist": this.dx
            });
        }

    }

    this.adjustEllipse = function () {
        var n = this.nextNode;
        if (n) {
            var q = this.p;
            if (n.isEllipse) {
                var t = n.p;
                if (n.nextNode) {
                    var a = n.nextNode.p;
                    var r = new PointX(t.x + q.x - a.x, t.y + q.y - a.y);
                    n.nextNode.moveTo(q);
                    n.moveTo(r);
                }
            }
        }

    }

    this.repos = function () {
        var sf = DEFAULT_PPF * this.vector.editor.scale;
        var n = this;
        if (this.prevNode) {
            var r = this.prevNode.p;
            var dx = n.dx * ((n.sdx == "L") ? -1 : 1);
            var dy = n.dy * ((n.sdy == "U") ? -1 : 1);
            console.log(dx, dy);
        }
    }

    this.vectorString = function () {
        return this.vector.editor.formatter.nodeToString(this, this.vector.editor);
    }

    this.p = p;
    this.vector = v;
}

function Vector(editor, sketch, startPoint) {
    if (!editor)
        throw "Cannot create a vector without editor parameter.";
    this.editor = editor;
    this.sketch = sketch;
    var v = this;
    this.index = 0;
    this.uid = "";
    this.code = "";
    this.label = "vector";
    this.name = null;
    this.vectorString = null;
    this.startNode = null;
    this.endNode = null;
    this.commands = [];
    this.header = null;
    this.labelPosition = null;
    this.AreaLabelPosition = null;
    this.level = 1;
    this.labelEdited = false;
    this.isClosed = false;
    this.isChanged = false;
    this.newRecord = false;
    var _isModified = false;
    //this.radiusNode = null;
    this.__defineGetter__("isModified", function () {
        return _isModified;
    });
    this.__defineSetter__("isModified", function (value) {
        _isModified = value;
        var cv = this;
        var cvuid = cv.uid;
        if (cvuid.indexOf("/") > -1) cvuid = cvuid.substr(0, cvuid.indexOf("/"));
        if (value && this.sketch) {
            this.isChanged = true;
            this.sketch.isModified = value;
            if (this.sketch.config.AllowMultiSegmentAddDelete) {
                this.sketch.vectors.forEach(function (v) {
                    var vuid = v.uid;
                    if (vuid.indexOf("/") > -1) vuid = vuid.substr(0, vuid.indexOf("/"));
                    if (vuid == cvuid && v.uid != cv.uid && !v.isModified) {
                        v.isModified = true;
                    }
                });
            }
        }
    });

    this.isIncomplete = function () {
        return this.startNode != null && this.isClosed == false;
    }

    this.connect = function (p, noalign, doNotTerminate, doNotSetOverlap) {
        if (!noalign) p = p.alignToGrid(this.editor, function (x) {
            return x;
        });

        if (this.endNode.prevNode && (this.endNode.prevNode != this.startNode)) {
            var overlaplimit = 0.9;
            if (editor.mode == CAMACloud.Sketching.MODE_NEW && !doNotSetOverlap) overlaplimit = 1.5 / (Math.max(sketchApp.scale, .2))
            if (this.startNode.overlaps(p, overlaplimit) && !doNotTerminate) {
                this.terminateNode(this.startNode);
                this.isModified = true;
                this.close();
            } else {
                this.endNode = this.endNode.connect(p);
                this.editor.currentNode = this.endNode;
            }
        } else {
            this.endNode = this.endNode.connect(p);
            this.editor.currentNode = this.endNode;
        }
        return this;
    }

    this.terminateNode = function (n) {
        this.endNode = this.endNode.connect(n.p);
        this.editor.undoChanges = [];
        return this;
    }
    this.continueVector = function (p) {
        var sn = this.editor.currentNode.nextNode
        var t = [], d = [], close = false;
        while (sn != null) {
            if (sn.overlaps(p, 0.8)) { p = sn.p.copy(); close = true; }
            else {
                d.push({ Start: _.clone(sn.isStart), Arc: _.clone(sn.isArc), Ellipse: _.clone(sn.isEllipse), EllipseEndNode: _.clone(sn.isEllipseEndNode), arcLength: _.clone(sn.arcLength) });
                t.push(sn.p.copy());
            }
            sn = sn.nextNode;
        }
        var x = sketchApp.currentNode.connect(p);
        sketchApp.currentNode = x;
        var v = this;
        t.forEach(function (a, i) {
            if (v.startNode.overlaps(a)) {
                //   close = true
                v.terminateNode(v.startNode);
            }
            else x = x.connect(a, d[i]);
            if (t.length == 1)
                x = v.endNode
            if (i == 0 && !close) x.noStroke = true
        })
        if (!close) { this.editor.currentVector.continuedrawing = true; }
        else {
            this.editor.currentVector.continuedrawing = false;
        }
        v.isModified = true;
    }
    this.start = function (p, noAlign) {
        var ref = this.editor.origin.copy().alignToGrid(this.editor, Math.round); //Origin is rounded
        if (!noAlign) p = p.alignToGrid(this.editor);
        this.startNode = new Node(p, this);
        this.startNode.isStart = true;
        this.startNode.calculateVector(ref);
        this.endNode = this.startNode;
        this.editor.currentNode = this.startNode;
    }

    this.close = function () {
        this.isClosed = true;
        // NEW FUNCTIONALITY BEGINS
        this.editor.currentNode = null;
        editor.mode = CAMACloud.Sketching.MODE_EDIT;
        editor.raiseModeChange();
        //        this.editor.currentVector = null;
        //        this.editor.currentNode = null;
        //        $(editor.vectorSelector).val('');
        //        editor.mode = CAMACloud.Sketching.MODE_DEFAULT;
        //        editor.raiseModeChange();
    }

    this.render = function (currentPoint, onMove) {
        this.renderPart(currentPoint, null, null, true);
        this.renderPart(currentPoint);
        if (!onMove) this.renderPart(currentPoint, true); //Render Nodes
        if (!onMove) this.renderPart(currentPoint, false, true); //Render Labels
    }

    this.renderPart = function (currentPoint, nodesOnly, labelsOnly, fillColor) {
        try {
            var ed = this.editor;
            var brush = ed.brush;
            var v = this;
            var current = ed.currentVector == this;
            let linePatternObject = { 1: [20, 5], 2: [15, 3, 3, 3], 3: [1, 1], 4: [20, 3, 3, 3, 3, 3, 3, 3], 5: [10, 10] };

            if (nodesOnly && !current) return false;
            if (ed.sketchMovable)
                brush.setColor('Blue');
            else if (v.newLineColor)
                brush.setColor(v.newLineColor);
            else if (v.visionDeleted)
                brush.setColor('#FF3333');
            else if (ed.lineColor)
                brush.setColor(ed.lineColor)
            else if (this.isChanged && (!current || (current && ed.mode == CAMACloud.Sketching.MODE_NEW)))
                brush.setColor((ed.sketchMovable ? 'Blue' : (v.LineEntryColor ? v.LineEntryColor : ' #F87217')), '#FBB917');
            else
                brush.setColor((ed.sketchMovable ? 'Blue' : (v.LineEntryColor ? v.LineEntryColor : 'Black')), '#444');
            if (!this.editor.isPreview && this.editor.currentSketch != this.sketch) {
                this.showInLowOpacity = true
                brush.setColor('#b7b7b7', '#b7b7b7');
            } else this.showInLowOpacity = false;
            ed.drawing.lineWidth = (v.isFreeFormLineEntries ? 4 : (this.editor.isPreview ? 2 : (current && !nodesOnly && !labelsOnly) ? 4 : 1));
            let copyStrokeStyle = ed.drawing.strokeStyle, copyfillStyle = ed.drawing.fillStyle, copylineWidth = ed.drawing.lineWidth;
            var top = new PointX(10000, 10000);
            brush.begin();
            let lnDash = [];
            if (this.calculationType == 'negative' || this.calculationType == 'auto' || this.Line_Type == true) lnDash = [5, 3];
            editor.drawing.setLineDash(lnDash);
            brush.moveTo(ed.origin);
            var sn = this.startNode, currentdeleteNodePoint = null;
            while (sn != null) {
                if (labelsOnly && sn.hideDimensions) {
                    sn = sn.nextNode;
                    continue;
                }

                let lineStrokeProperties = false, cline = false;
                if (!sn.isStart && !fillColor && !labelsOnly && !nodesOnly) {
                    ed.drawing.beginPath();
                    cline = (current && ed.currentLine && (sn == ed.currentLine.eNode)) ? true : false;

                    if (sn.lineStrokePattern) {
                        lineStrokeProperties = true;
                        if (sn.lineStrokePattern) editor.drawing.setLineDash(linePatternObject[sn.lineStrokePattern]);
                    }
                    if (cline) { brush.setColor('Red', 'Red'); ed.drawing.lineWidth = 6; }
                }

                if (this.placeHolder) {
                    if (nodesOnly) {
                        brush.drawPath(v, sn.path, sn.isVeer, false);
                        editor.drawNode((currentPoint == null) && (sn == this.editor.currentNode))
                        // brush.moveBy(this.placeHolderSize * DEFAULT_PPF, 0)
                        // editor.drawNode((currentPoint == null) && (this.radiusNode == this.editor.currentNode))
                    } else if (labelsOnly) { } else if ((!this.disableMove) || (this.disableMove && current)) {
                        //this.radiusNode = new Node(sn.p.copy().moveBy(this.placeHolderSize, 0), this)
                        // this.radiusNode.p = sn.p.copy().moveBy(this.placeHolderSize, 0);
                        brush.drawPath(v, sn.path, sn.isVeer, false);
                        //if (!v.noVectorSegment) 
                        brush.circle(this.placeHolderSize);
                    }
                    break;
                } else if (sn.isEllipse) {
                    if (nodesOnly) {
                        brush.drawPath(v, sn.path, sn.isVeer, false);
                        if ((!this.editor.currentVector.isClosed) || (sn != this.editor.currentVector.endNode))
                            ed.drawNode((currentPoint == null) && (sn == this.editor.currentNode));
                    } else if (labelsOnly) {

                    } else {
                        brush.drawEllipse(sn.path, ((!fillColor && !sn.noStroke) ? true : false));
                    }
                } else if (sn.isArc) {
                    if (nodesOnly) {
                        brush.drawPath(v, sn.path, sn.isVeer, false);
                        if ((!this.editor.currentVector.isClosed) || (sn != this.editor.currentVector.endNode))
                            ed.drawNode((currentPoint == null) && (sn == this.editor.currentNode));
                    } else if (labelsOnly) {

                    } else {
                        brush.drawArc(sn.path, sn.arcLength, sn, fillColor, false, sn.noStroke);
                    }
                } else if (sn.isEllipseEndNode) {
                    if (nodesOnly) {
                        brush.drawPath(v, sn.path, sn.isVeer, false);
                    } else if (labelsOnly) {

                    } else {
                        //brush.drawPath(sn.path, sn.isVeer, false);
                    }
                } else {
                    if (nodesOnly) {
                        brush.drawPath(v, sn.path, sn.isVeer, false);
                        if ((!this.editor.currentVector.isClosed) || (sn != this.editor.currentVector.endNode)) {
                            ed.drawNode((currentPoint == null) && (sn == this.editor.currentNode), sn == this.editor.currentVector.startNode, sn == this.editor.currentVector.endNode, sn.prevNode ? sn.overlaps(sn.prevNode.p) : false, (sn == this.editor.currentVector.startNode) && sn.nextNode ? sn.p.getDirectionInDegrees(sn.nextNode.p) : null);
                        }
                    } else if (labelsOnly) {

                    } else {
                        brush.drawPath(v, sn.path, sn.isVeer, !(sn.isStart || sn.noStroke), null, null, fillColor);
                    }
                }

                if (cline || lineStrokeProperties) { brush.setColor(copyStrokeStyle, copyfillStyle); ed.drawing.lineWidth = copylineWidth; editor.drawing.setLineDash(lnDash); }
                if (brush.currentPoint.x < top.x) top.x = brush.currentPoint.x;
                if (brush.currentPoint.y < top.y) top.y = brush.currentPoint.y;
                if (current && ed.mode == CAMACloud.Sketching.MODE_NEW && ed.currentNode && ed.currentVector.continuedrawing && sn == ed.currentNode)
                    currentdeleteNodePoint = { x: brush.currentPoint.x, y: brush.currentPoint.y };
                sn = sn.nextNode;
            }



            if (fillColor && (this.isClosed) && !this.showInLowOpacity) {
                // add false condition to prevent vector fill with blue shade.. FD 800
                var clr = this.colorCode ? this.colorCode : "#d4f4f6";
                var grd = editor.drawing.createLinearGradient(top.x, top.y, top.x + 500, top.y + 500);
                var convertedClr = ((clr && clr.indexOf('rgba') > -1) ? clr : (current ? ('rgba(' + CovertHexToRGB(clr) + ',0.6)') : ('rgba(' + CovertHexToRGB(clr) + ',0.4)')));
                if (current) {
                    grd.addColorStop(0, convertedClr)
                    // grd.addColorStop( 1, 'rgba(197,255,255,0.2)' )
                    //brush.setColor('Black', '#C5E7FF');
                    $('#color_picker').val(clr);
                    $('.colorPicker-picker').css("background-color", clr)
                    if (v.newLineColor)
                        brush.setColor(v.newLineColor, grd);
                    else if (ed.lineColor)
                        brush.setColor(ed.lineColor, grd);
                    else
                        brush.setColor('Black', grd);
                    brush.fill();
                } else {
                    grd.addColorStop(0, convertedClr)
                    //grd.addColorStop( 1, 'rgba(227, 245, 245, 0.1)' )
                    if (v.newLineColor)
                        brush.setColor(v.newLineColor, grd);
                    else if (this.isChanged)
                        brush.setColor('#FFA62F', grd);
                    else
                        brush.setColor('Black', grd);
                    brush.fill();
                }
            }

            if (labelsOnly) {
                sn = this.startNode;
                while (sn != null) {
                    if (sn.isEllipse) {

                    } else if (sn.isEllipseEndNode) { } else if (v.placeHolder) {
                        !(sn.isStart || sn.noStroke)
                        if (!v.noVectorSegment)                            
                            brush.drawCircleRadiuslabel(v,v.placeHolderSize,sn.p)
                    } else if (sn.isArc) {
                        var p1 = sn.prevNode.p;
                        var mp = sn.midpoint;
                        brush.drawArcLabel(v, mp.x - p1.x, mp.y - p1.y, sn.p.x - p1.x, sn.p.y - p1.y, sn.arcPerimeter(), Math.abs(sn.arcLength), sn.hideDimensions);
                    } else {
                        brush.drawPath(v, sn.path, sn.isVeer, false, !sn.isStart, sn.hideDimensions);
                    }
                    sn = sn.nextNode;
                }
                var cp;
                var boundary = (4 * editor.scale * (editor.scale > .70 ? 1 : 1.5)) - ((8 - brush.getFontSize(null, this)) / 2)
                if (!this.segmentareaValue && this.sketch.config && this.sketch.config.moveLabelcenter)
                    this.segmentareaValue = this.area();
                if (this.labelPosition && this.isClosed && !this.drawnUsingKeys && !this.sketch.config.DoNotAllowLabelMove && !editor.formatter.DoNotAllowLabelMove)
                    if (this.sketch.config && this.sketch.config.moveLabelcenter) {
                        if (this.segmentareaValue && this.segmentareaValue != this.area()) {
                            cp = this.getCenter();
                            if (cp && cp.x != undefined) this.labelPosition = new PointX(sRound(cp.x / editor.scale), sRound(cp.y / editor.scale));
                            this.segmentareaValue = this.area();
                        }
                        else
                            cp = new PointX(this.labelPosition.x * editor.scale, this.labelPosition.y * editor.scale);
                    }
                    else
                        cp = new PointX(this.labelPosition.x * editor.scale, this.labelPosition.y * editor.scale);
                else {
                    cp = this.getCenter();
                    if (cp && cp.x != undefined) this.labelPosition = new PointX(sRound(cp.x / editor.scale), sRound(cp.y / editor.scale));
                    this.drawnUsingKeys = false;
                }
                if (this.labelPosition && this.isClosed && (clientSettings.SketchConfig == 'MVP' || sketchSettings.SketchConfig == 'MVP') && this.isUnSketchedArea) {
                    cp = this.getCenter();
                    if (cp && cp.x != undefined) this.labelPosition = new PointX(sRound(cp.x / editor.scale), sRound(cp.y / editor.scale));
                }
                if (this.disableMove && (clientSettings.SketchConfig == 'Patriot' || sketchSettings.SketchConfig == 'Patriot')) {
                    cp = this.getCenter();
                    if (cp && cp.x != undefined) this.labelPosition = new PointX(sRound(cp.x / editor.scale), sRound(cp.y / editor.scale));
                }
                var lbls = vectorLabelPositions.filter(function (a) {
                    return ((a.x + (boundary + 2) >= cp.x && a.x - (boundary + 2) <= cp.x) && a.y + boundary >= cp.y && a.y - boundary <= cp.y)
                })
                if (lbls.length > 0)
                    cp.y = lbls[lbls.length - 1].y - boundary;
                vectorLabelPositions.push(cp)
                if (this.isClosed || ed.mode != CAMACloud.Sketching.MODE_NEW) {
                    var areaColor = (editor.labelColorCode ? editor.labelColorCode : (this.isChanged ? '#ECA114' : 'Black'));
                    var lblColor = this.newLabelColorCode ? this.newLabelColorCode : areaColor;
                    if (this.showInLowOpacity)
                        lblColor = areaColor = '#b7b7b7';
                    editor.brush.setColor(lblColor, lblColor);
                    editor.drawing.font = brush.getFont(this, "label");
                    var labelText = this.label;
                    if (sketchSettings && sketchSettings['ShowOtherImprovementDescLabel'] == '1' && this.sketchType == 'outBuilding' && editor.mvpDisplayLabel)
                        labelText = this.mvpDisplayLabel;
                    var tw = editor.drawing.measureText(labelText).width / DEFAULT_PPF;
                    var cx = cp.copy();
                    cx.x -= tw * editor.scale / 2;
                    brush.write(cx, labelText || '---', brush.getFont(this, "label"));
                    var th = brush.getFontSize(editor.scale, this) * 1.5;
                    var dx = cp.copy();;
                    editor.drawing.font = brush.getFont(this, "area");
                    editor.brush.setColor(areaColor, areaColor);
                    //MA_2442
                    var areaText;
                    if (this.hideAreaValue)
                        areaText = '';
                    else if (this.fixedArea == '' && this.disableMove)
                        areaText = (this.fixedArea + AREA_UNIT);
                    else if (this.fixedArea == 0 && this.disableMove)
                        areaText = this.fixedArea + AREA_UNIT;
                    else
                        areaText = (this.fixedArea || this.fixedAreaTrue || this.area()) + AREA_UNIT;
                    //var areaText = this.hideAreaValue ? '' : ((this.fixedArea == '' && this.disableMove) ? '' : ((this.fixedArea == 0 && this.disableMove) ? (this.fixedArea + (editor.formatter.AreaUnit || AREA_UNIT || '')) : ((this.fixedArea || this.fixedAreaTrue || this.area()) + (editor.formatter.AreaUnit || AREA_UNIT || ''))));
                    var atw = editor.drawing.measureText(areaText).width;
                    dx.x -= atw / DEFAULT_PPF * editor.scale / 2;
                    dx.y -= th / DEFAULT_PPF * 1.6;
                    if (this.editor && this.editor.config && this.editor.config.IsProvalConfig && !this.sktSegmentArea) {
                        var _provalArea = (this.fixedArea || this.fixedAreaTrue || this.area());
                        this.sktSegmentArea = _provalArea ? _provalArea.toString() : null;
                    }
                    if (this.isClosed && this.labelPosition && this.vectorConfig && this.vectorConfig.LabelAreaPosition && this.isModified) {
                        var alp = new PointX(this.labelPosition.x, this.labelPosition.y)
                        var ah = brush.getFontSize(1, this) * 1.5;
                        if (clientSettings.SketchConfig == 'MVP' || sketchSettings.SketchConfig == 'MVP') {
                            alp.x -= atw / DEFAULT_PPF * 0.25;
                            alp.y -= ah / DEFAULT_PPF * 4.37;
                        }
                        else {
                            alp.x -= atw / DEFAULT_PPF * 0.25;
                            alp.y -= ah / DEFAULT_PPF * 2.6;
                        }
                        alp.x = sRound(alp.x); alp.y = sRound(alp.y);
                        this.AreaLabelPosition = alp;
                    }
                    brush.write(dx, areaText, brush.getFont(this, "area"));
                }
            }



            if (current && currentPoint != null) {
                if (nodesOnly) {
                    var lp = brush.currentPoint.copy();
                    brush.moveTo(currentPoint);
                    this.editor.onDrawLine(lp, currentPoint.copy(), ed.currentVector);
                    ed.drawNode(true);
                } else if (labelsOnly) {

                } else {
                    if (ed.mode == CAMACloud.Sketching.MODE_NEW) {
                        if (!this.isClosed) {
                            brush.lineTo(currentPoint);
                        }
                        else if (currentdeleteNodePoint) {
                            currentdeleteNodePoint = new PointX(currentdeleteNodePoint.x, currentdeleteNodePoint.y);
                            brush.moveTo(currentdeleteNodePoint);
                            brush.lineTo(currentPoint);
                            brush.stroke();
                            brush.setColor('black', 'black');
                            var p1 = currentdeleteNodePoint, p2 = currentPoint.copy();
                            var d = sRound((p1.distanceFrom(p2) / (DEFAULT_PPF * ed.scale)));
                            var dy = p2.y - p1.y;
                            var dx = p2.x - p1.x;
                            var measurement_coordinate = { x: p1.x + (dx / 2), y: p1.y + (dy / 2) }
                            ed.drawing.fillText(d, measurement_coordinate.x, measurement_coordinate.y);
                        }
                    }
                }
            }

            if (nodesOnly) {
                brush.fill();
            }
            else if (labelsOnly) {

            }
            else if (this.placeHolder || (ed.mode == CAMACloud.Sketching.MODE_NEW && !currentdeleteNodePoint)) {
                brush.stroke();
            }
            brush.end();

            brush.moveTo(ed.origin);
        } catch (e) {
            alert(e);
        }

    }

    this.toPoints = function () {
        var ps = [];
        var sn = this.startNode;
        while (sn != null) {
            if (!(sn != this.startNode && sn.overlaps(this.startNode.p)))
                ps.push(sn.p.copy());
            sn = sn.nextNode;
        }
        return ps;
    }

    this.getCenter = function () {
        var points = this.toPoints();
        var sx = 0,
            sy = 0;
        for (x in points) {
            var pt = points[x];
            sx += pt.x;
            sy += pt.y;
        }

        var cx = new PointX(sRound((sx / points.length) * this.editor.scale), sRound((sy / points.length) * this.editor.scale));
        return cx;
    }

    this.area = function () {
        var points = this.toPoints();
        if (this.placeHolder) {
            var r = this.placeHolderSize;
            return this.noVectorSegment ? this.areaFieldValue : Number((Math.PI * r * r).toFixed(2))
        }
        var ellipseArea = 0, n = this.startNode, ax = [], ay = [];
        if (sketchSettings.SegmentRoundingFactor && n != null) {
            var nx = n.p.x, ny = n.p.y;
            ax.push((nx)); ay.push((ny)); // ax.push(sRound(nx)); ay.push(sRound(ny));
            while (n != null) {
                var nn = n.nextNode;
                if (nn != null) {
                    var ncp = { x: nn.p.x, y: nn.p.y }, npp = { x: n.p.x, y: n.p.y }, pp = { x: nx, y: ny };
                    var R = Math.atan2((nn.p.y - n.p.y), (nn.p.x - n.p.x)), dis = 0;
                    if (nn.isVeer) {
                        var dsq = 0;
                        for (var x in nn.path) {
                            var p = nn.path[x];
                            dsq += (p ? (p.dist * p.dist) : 0);
                        }
                        dis = Number(Math.sqrt(dsq));
                    }
                    else
                        dis = nn.path[0] ? nn.path[0].dist : 0;

                    dis = Math.round(dis * sketchSettings.SegmentRoundingFactor) / sketchSettings.SegmentRoundingFactor;
                    nx = nx + (dis * Math.cos(R)); ny = ny + (dis * Math.sin(R));

                    if (n.isEllipse) {
                        var dxv = nx - pp.x, dyv = ny - pp.y;
                        ellipseArea += nn.ellipseArea(dxv, dyv);
                    }
                    if (nn.isArc) {
                        n.p.x = pp.x; n.p.y = pp.y; nn.p.x = nx; nn.p.p = ny;
                        if (this.containsInFrame(nn.midpoint))
                            ellipseArea += -nn.arcArea();
                        else
                            ellipseArea += nn.arcArea();
                        n.p.x = npp.x; n.p.y = npp.y; nn.p.x = ncp.x; nn.p.p = ncp.y;
                    }
                    ax.push((nx)); ay.push((ny)); //ax.push(sRound(nx)); ay.push(sRound(ny)); calculation accuracy issue occur when round 
                }
                n = n.nextNode;
            }
        }
        else {
            while (n != null) {
                if (n.isEllipse) {
                    ellipseArea += n.ellipseArea();
                }
                if (n.isArc) {
                    if (this.containsInFrame(n.midpoint)) {
                        ellipseArea += -n.arcArea();
                    } else {
                        ellipseArea += n.arcArea();
                    }
                }
                ax.push(sRound(n.p.x));
                ay.push(sRound(n.p.y));
                n = n.nextNode;
            }
        }
        var v = this;
        var len = ax.length;
        var polyArea = polygonArea(ax, ay, len);
        //return Number(( ellipseArea + polyArea ).toFixed( 2 ) * ( this.calculationType == 'negative' ? -1 : 1 ) );
        var totArea = (ellipseArea + polyArea);
        var linkedNegativeVector = this.sketch.vectors.filter(function (a) { return a.linkedUniqueId == v.uniqueId })
        if (this.uniqueId && this.uniqueId > -1 && linkedNegativeVector && linkedNegativeVector.length > 0)
            totArea -= linkedNegativeVector[0].area();

        var _sConfig = (clientSettings && clientSettings.SketchConfig) || (sketchSettings && sketchSettings.SketchConfig), _dSQFTonUI = sketchSettings && sketchSettings["DisplaySQFTOnUI"];
        if (_dSQFTonUI && _dSQFTonUI != "Whole Foot") {
            if (_sConfig == "Baldwin" || _sConfig == "Delta")
                return Number(totArea.toFixed(_dSQFTonUI));
            else
                return Number((totArea * ((this.areaMultiplier && !isNaN(this.areaMultiplier)) ? this.areaMultiplier : 1)).toFixed(_dSQFTonUI));
        }
        else {
            totArea = totArea.toFixed(2);
            if (_dSQFTonUI && _dSQFTonUI == "Whole Foot")
                return Math.round(totArea * (this.areaMultiplier && !isNaN(this.areaMultiplier) ? this.areaMultiplier : 1));
            else if (_sConfig == "Baldwin" || _sConfig == "Delta")
                return sRound(totArea);
            else if (this.areaMultiplier && !isNaN(this.areaMultiplier))
                return Number((sRound(totArea * (this.areaMultiplier && !isNaN(this.areaMultiplier) ? this.areaMultiplier : 1))).toFixed(2));
            else
                return Number(totArea * (this.areaMultiplier && !isNaN(this.areaMultiplier) ? this.areaMultiplier : 1));
        }
    }

    this.perimeter = function () {
        var p = 0, n = this.startNode, nf = { 1: 0.4, 2: 0.5, 3: 0 };
        while (n != null) {
            if (isFinite(n.length)) {
                let f = ((n?.proval_line_type && n?.lineStrokePattern && (n?.lineStrokePattern != '0')) ? nf[parseInt(n.lineStrokePattern)] : 1);
                p = p + (n.isArc ? sRound(n.arcPerimeter()) : sRound(n.length) * f);

              //  p = p + (sRound(n.length) * f);
            }
            n = n.nextNode;
        }
      
        return p;
    }

    function polygonArea(X, Y, numPoints) {
        area = 0; // Accumulates area in the loop
        j = numPoints - 1; // The last vertex is the 'previous' one to the first
        for (i = 0; i < numPoints; i++) {
            area = area + (X[j] + X[i]) * (Y[j] - Y[i]);
            j = i; //j is previous vertex to i
        }
        return Math.abs(area / 2);
    }

    this.containsInFrame = function (pt) {
        var poly = this.toPoints();
        for (var c = false, i = -1, l = poly.length, j = l - 1; ++i < l; j = i)
            ((poly[i].y <= pt.y && pt.y < poly[j].y) || (poly[j].y <= pt.y && pt.y < poly[i].y)) && (pt.x < (poly[j].x - poly[i].x) * (pt.y - poly[i].y) / (poly[j].y - poly[i].y) + poly[i].x) && (c = !c);
        if (c) return c;

        var sn = this.startNode;
        while (sn != null) {
            if (sn.isEllipse) {
                var ex = sn.p.x;
                var ey = sn.prevNode.p.y;
                var ea = sn.dx;
                var eb = sn.dy;
                var px = pt.x;
                var py = pt.y;
                var dx = px - ex;
                var dy = py - ey;
                c = ((dx * dx) / (ea * ea) + (dy * dy) / (eb * eb) <= 1);
                if (c) return c;
            }
            sn = sn.nextNode;
        }

        return c;
    }

    this.containsOn = function (pt) {
        var poly = [];
        var sn = this.startNode;
        while (sn != null) {
            if (!sn.isStart) {
                poly.push(sn.midpoint.copy());
            }
            poly.push(sn.p.copy());
            sn = sn.nextNode;
        }

        for (var c = false, i = -1, l = poly.length, j = l - 1; ++i < l; j = i)
            ((poly[i].y <= pt.y && pt.y < poly[j].y) || (poly[j].y <= pt.y && pt.y < poly[i].y)) && (pt.x < (poly[j].x - poly[i].x) * (pt.y - poly[i].y) / (poly[j].y - poly[i].y) + poly[i].x) && (c = !c);
        if (c) return c;

        var sn = this.startNode;
        while (sn != null) {
            if (sn.isEllipse) {
                var ex = sn.p.x;
                var ey = sn.prevNode.p.y;
                var ea = sn.dx;
                var eb = sn.dy;
                var px = pt.x;
                var py = pt.y;
                var dx = px - ex;
                var dy = py - ey;
                c = ((dx * dx) / (ea * ea) + (dy * dy) / (eb * eb) <= 1);
                if (c) return c;
            }
            sn = sn.nextNode;
        }

        return c;
    }

    this.contains = function (pt) {
        if (this.isOnPerimeter(pt)) {
            return true;
        }

        var poly = [];
        var sn = this.startNode;
        if (this.placeHolder && sn) {
            if (this.disableMove)
                return ((pt.x < (sn.p.x + 6)) && (pt.x > (sn.p.x - 6)) && (pt.y < (sn.p.y + 2.5)) && (pt.y > (sn.p.y - 2.5)));
            else
                return Math.pow(pt.x - sn.p.x, 2) + Math.pow(pt.y - sn.p.y, 2) < Math.pow(this.placeHolderSize, 2)
        }
        while (sn != null) {
            if (!sn.isStart) {
                poly.push(sn.midpoint.copy());
            }
            poly.push(sn.p.copy());
            sn = sn.nextNode;
        }

        for (var c = false, i = -1, l = poly.length, j = l - 1; ++i < l; j = i)
            ((poly[i].y <= pt.y && pt.y < poly[j].y) || (poly[j].y <= pt.y && pt.y < poly[i].y)) && (pt.x < (poly[j].x - poly[i].x) * (pt.y - poly[i].y) / (poly[j].y - poly[i].y) + poly[i].x) && (c = !c);
        if (c) return c;

        var sn = this.startNode;
        while (sn != null) {
            if (sn.isEllipse) {
                var ex = sn.p.x;
                var ey = sn.prevNode.p.y;
                var ea = sn.dx;
                var eb = sn.dy;
                var px = pt.x;
                var py = pt.y;
                var dx = px - ex;
                var dy = py - ey;
                c = ((dx * dx) / (ea * ea) + (dy * dy) / (eb * eb) <= 1);
                if (c) return c;
            }
            sn = sn.nextNode;
        }

        return c;
    }

    this.isOnPerimeter = function (pt) {
        var poly = this.toPoints();
        var vect = this;
        for (var x in poly) {
            var cp = poly[x];
            if (cp.overlaps(pt, vect)) {
                return true;
            }
        }

        for (var i = 0; i <= poly.length - 2; i += 2) {
            var p1 = poly[i];
            var p2 = poly[i + 1];
            if (p1.getDistanceTo(p2) == p1.getDistanceTo(pt) + p2.getDistanceTo(pt)) {
                return true;
            }
        }

        return false;
    }

    this.lineSelection = function (event, editor) {
        var ecx = event.x, ecy = event.y, onLine = false;
        var sn = this.startNode, paths = editor.drawing, brush = editor.brush, cpoint = brush.currentPoint, currentLineWidth = paths.lineWidth;
        paths.lineWidth = 12; brush.begin(); brush.moveTo(editor.origin);
        while (sn != null) {
            var path = sn.path, dy = 0, dx = 0, onLine = false;
            for (var pi in path) {
                var p = path[pi];
                var sd = CAMACloud.Sketching.Utilities.toPixel(p.dist);
                switch (p.dir) {
                    case "U":
                        dy = -sd;
                        break;
                    case "D":
                        dy = sd;
                        break;
                    case "L":
                        dx = -sd;
                        break;
                    case "R":
                        dx = sd;
                        break;
                }
            }
            var nextPoint = new PointX(cpoint.x + dx * editor.scale, cpoint.y + dy * editor.scale);
            if (sn.isStart)
                paths.moveTo(nextPoint.x, nextPoint.y);
            else {
                if (sn.isEllipse) {
                    var newLinePoint = new PointX(cpoint.x, cpoint.y);
                    onLine = brush.drawEllipse(sn.path, false, null, { ecx: ecx, ecy: ecy, currentPoint: newLinePoint });
                }
                else if (sn.isArc) {
                    var newLinePoint = new PointX(cpoint.x, cpoint.y);
                    var nPoint = brush.drawArc(sn.path, sn.arcLength, sn, true, { dx: dx, dy: dy, currentPoint: newLinePoint, ecx: ecx, ecy: ecy });
                    nextPoint = nPoint.p2; onLine = nPoint.isLine;
                }
                else
                    paths.lineTo(nextPoint.x, nextPoint.y);
                onLine = onLine ? onLine : paths.isPointInStroke(ecx, ecy);
                if (onLine == true) {
                    editor.currentLine = {};
                    editor.currentLine.sNode = sn.prevNode;
                    editor.currentLine.eNode = sn;
                    break;
                }
            }
            cpoint = nextPoint;
            sn = sn.nextNode;
        }
        paths.lineWidth = currentLineWidth;
        return true;
    }

    this.moveBy = function (d, micromove, excludeFloor) {
        var sn = this.startNode;
        if (!sn) return;
        var p2 = excludeFloor ? new PointX((sn.p.x), (sn.p.y)) : new PointX(Math.floor(sn.p.x), Math.floor(sn.p.y));
        var diff = new PointX(sn.p.x - p2.x, sn.p.y - p2.y);
        var moveIndex = 0;
        if (micromove) diff = new PointX(0, 0);
        while (sn != null) {
            if ((sn == this.endNode) && (sn.overlaps(this.startNode.p))) {

            } else {
                sn.p.x = Number((sn.p.x - d.x - diff.x).toFixed(_sdp));
                sn.p.y = Number((sn.p.y - d.y - diff.y).toFixed(_sdp));
            }
            sn = sn.nextNode;
            moveIndex++;
        }
        this.labelPosition.x -= d.x;
        this.labelPosition.y -= d.y;
        this.labelEdited = true;
        if (this.startNode) {
            this.startNode.recalculateAll();
        }

        this.isModified = true;
    }

    this.analyze = function (d) {
        var sn = this.startNode;
        while (sn != null) {
            sn = sn.nextNode;
        }
    }

    this.reconstruct = function () {
        var o = this.editor.origin.copy();
        var sn = this.startNode;
        while (sn != null) {
            var ref = sn.prevNode ? sn.prevNode.p : o;
            var dx = sn.dx * ((sn.sdx == "L") ? -1 : 1);
            var dy = sn.dy * ((sn.sdy == "U") ? -1 : 1);
            sn.x = ref.x - dx;
            sn.y = ref.y - dy;
            sn = sn.nextNode;
        }
        if (this.startNode) this.startNode.recalculateAll();
    }

    this.toString = function (fromSketchSave) {
        this.vectorString = this.editor.formatter.vectorToString(this, fromSketchSave);
        return this.vectorString;
    }
    this.toVectorStartString = function () {
        if (this.editor.formatter.vectorToStartString)
            return this.editor.formatter.vectorToStartString(this);
        else
            return null;
    }

    this.labelCommandString = function () {
        if (this.editor.formatter.vectorToLabelCommand)
            return this.editor.formatter.vectorToLabelCommand(this);
        else
            return null;
    }

    this.dimensionCommandString = function () {
        if (this.editor.formatter.vectorToDimensionCommand)
            return this.editor.formatter.vectorToDimensionCommand(this);
        else
            return null;
    }

    this.toPointString = function () {
        var points = this.toPoints();
        var str = "";
        for (var x in points) {
            if (str != "") str += ", ";
            str += points[x].toString();
        }
        return str;
    }

    if (startPoint != null)
        this.start(startPoint);

    editor.vectors.push(this);
}

function Sketch(editor) {
    this.uid = null;
    this.label = null;
    this.vectors = [];
    this.notes = [];
    this.editor = editor;
    this.isModified = false;
    this.config = {};
    var s = this;
    this.sections = {};
    this.reconstruct = function (actionCallback, callback, FlipVH) {
        for (var x in s.vectors) {
            var o = this.editor.origin.copy().alignToGrid(this.editor, Math.round); //origin is rounded
            var cv = s.vectors[x];
            if (FlipVH && cv.disableMove)
                continue;
            var nn = cv.startNode;
            while (nn != null) {
                if (actionCallback) actionCallback(nn, cv);
                //if (nn.sdx == "L") nn.sdx = "R"; else nn.sdx = "L";
                var dx = nn.dx;
                var dy = nn.dy;

                if (dx > 0) {
                    if (nn.sdx == "L") {
                        o.moveBy(-dx, 0)
                    } else {
                        o.moveBy(dx, 0);
                    }
                }
                if (dy > 0) {
                    if (nn.sdy == "D") {
                        o.moveBy(0, dy);
                    } else {
                        o.moveBy(0, -dy);
                    }
                }

                nn.p.x = o.x;
                nn.p.y = o.y;
                nn = nn.nextNode;
            }

            if (cv.startNode) {
                cv.startNode.recalculateAll();
            }
            if (callback) callback(cv);
            cv.isModified = true;
            //cv.reconstruct();
        }

        var sb = this.editor.sketchBounds();
        this.editor.resetOrigin(true);
        editor.panOrigin(sb, editor.formatter.originPosition, null, true);
        //this.editor.render();
    }

    this.flipHorizontal = function (skApp) {
        var sktchApp = sketchApp || skApp;
        if (!sktchApp.sketchMovable && this.notes) {
            this.notes.forEach(function (i) {
                i.notePosition.x = -i.notePosition.x;
                i.isModified = true;
            });
            if (sketchApp && sketchApp.config && sketchApp.config.IsJsonFormat)
                sketchApp.currentSketch.isModified = true;
        }
        this.reconstruct(function (nn) {
            if (nn.sdx == "L") nn.sdx = "R";
            else nn.sdx = "L";
            nn.arcLength = -nn.arcLength;
        }, function (cv) {
            if (!cv.labelPosition) return
            if (cv.labelPosition.x > 0) {
                cv.labelPosition.x = -cv.labelPosition.x;
            } else {
                cv.labelPosition.x = -cv.labelPosition.x;
            }
            cv.labelEdited = true;
        }, true);
    }

    this.flipVertical = function (skApp) {
        var sktchApp = sketchApp || skApp;
        if (!sktchApp.sketchMovable && this.notes) {
            this.notes.forEach(function (i) {
                i.notePosition.y = -i.notePosition.y;
                i.isModified = true
            });
            if (sketchApp && sketchApp.config && sketchApp.config.IsJsonFormat)
                sketchApp.currentSketch.isModified = true;
        }
        this.reconstruct(function (nn) {

            if (nn.sdy == "U") nn.sdy = "D";
            else nn.sdy = "U";
            nn.arcLength = -nn.arcLength;
        }, function (cv) {
            if (!cv.labelPosition) return
            if (cv.labelPosition.y > 0) {
                cv.labelPosition.y = -cv.labelPosition.y;
            } else {
                cv.labelPosition.y = -cv.labelPosition.y;
            }
            cv.labelEdited = true;
        }, true);
    }

    this.renderNotes = function (CP) {
        if (CP) {
            this.notes.filter(function (y) { return y.pageNum == CP }).forEach(function (x) {
                x.render()
            });
        }
        else {
            this.notes.forEach(function (x) {
                x.render()
            });
        }
    }

    this.validateVectors = function () {
        var errorMessage = "";
        for (var x in this.vectors) {
            var v = this.vectors[x];
            if ((v.isIncomplete() && !v.isUnSketchedArea && !v.isUnSketchedTrueArea && (v.area() == 0 || v.area() > 1)) && !v.isFreeFormLineEntries) {
                return {
                    valid: false,
                    error: 'One or more segments are not closed. '
                };
            }
        }

        return {
            valid: true
        };
    }

    //    this.notes.push(new Annotation(editor, "YES!", new PointX(30, 30)));
    //    this.notes.push(new Annotation(editor, "This is GOOD", new PointX(40, 40)));
}

function Annotation(editor, sketch, text, p, lp, bo, uid, extraNoteField, extraNoteFields) {
    this.noteText = text;
    this.notePosition = p;
    this.lineStart = lp;
    this.editor = editor;
    this.sketch = sketch;
    this.boxOrgin = bo;
    this.boxWidth = 300;
    this.boxHeight = 100;
    this.isModified = false;
    this.noteid = uid;
    var n = this;
    this.isDeleted = false;
    this.newRecord = false;
    if (extraNoteField) {
        this.bold = extraNoteField.bold;
        this.color = extraNoteField.color;
        this.fontFace = extraNoteField.fontFace;
        this.fontSize = extraNoteField.fontSize;
        this.italic = extraNoteField.italic;
        this.pageNum = extraNoteField.pageNum;
        this.rotation = extraNoteField.rotation;
        this.KeyCode = extraNoteField.KeyCode;
    }

    if (extraNoteFields) {
        for (var nx in extraNoteFields) {
            this[nx] = extraNoteFields[nx];
        }
    }

    this.render = function () {
        if (this.isDeleted == true)
            return false;
        var ed = this.editor;
        var brush = ed.brush;
        var drawing = ed.drawing;

        var o = ed.origin;
        var sp = new PointX(o.x + this.notePosition.x * DEFAULT_PPF * ed.scale, o.y - this.notePosition.y * DEFAULT_PPF * ed.scale);
        var ls;
        if (this.lineStart) {
            if (this.lineStart.x != 0 && this.lineStart.y != 0) {
                ls = new PointX(o.x + this.lineStart.x * DEFAULT_PPF * ed.scale, o.y - this.lineStart.y * DEFAULT_PPF * ed.scale);
            }
        }


        brush.drawNotes(sp.x, sp.y, this, ls);

    }

    this.contains = function (pt) {
        var noteBox = [];
        var bo = this.boxOrgin;
        noteBox.push(bo);
        var so = new PointX(bo.x + this.boxWidth, bo.y)
        noteBox.push(so);
        noteBox.push(new PointX(so.x, so.y - this.boxHeight));
        noteBox.push(new PointX(bo.x, bo.y - this.boxHeight));
        for (var c = false, i = -1, l = noteBox.length, j = l - 1; ++i < l; j = i)
            ((noteBox[i].y <= pt.y && pt.y < noteBox[j].y) || (noteBox[j].y <= pt.y && pt.y < noteBox[i].y)) && (pt.x < (noteBox[j].x - noteBox[i].x) * (pt.y - noteBox[i].y) / (noteBox[j].y - noteBox[i].y) + noteBox[i].x) && (c = !c);
        if (c) return c;
        return c;
    }
    this.moveBy = function (d) {
        var sn = this.notePosition;
        var p2 = new PointX(Math.floor(sn.x), Math.floor(sn.y));
        sn.x -= d.x;
        sn.y -= d.y;
        this.isModified = true;
        if (sketchApp && sketchApp.config && (sketchApp.config.IsJsonFormat || sketchApp.config.EnableVisionNotes || sketchApp.config.ModifyIfNoteChanged))
            sketchApp.currentSketch.isModified = true;
    }
}

function GetOrigin(editor, originPosition, scale) {
    var o;
    if (originPosition == "topRight")
        o = RoundX(editor.width - 80, 60, scale);
    else if (originPosition == "topLeft")
        o = RoundX(40, 60);
    else
        o = RoundX(40, editor.height - 40, scale);
    return o;
}

function PointX(x, y) {
    this.x = x;
    this.y = y;

    this.scale = function (s) {
        this.x *= Math.round(this.x * s);
        this.y *= Math.round(this.y * s);
        return this;
    }

    this.toUnits = function (editor) {
        var up = new PointX();
        up.x = (this.x - editor.origin.x) / DEFAULT_PPF / editor.scale;
        up.y = (editor.origin.y - this.y) / DEFAULT_PPF / editor.scale;
        return up;
    }

    this.moveBy = function (x, y) {
        this.x += x;
        this.y -= y;
        return this;
    }

    this.shift = function (x, y) {
        var np = new PointX(this.x, this.y);
        np.x += x;
        np.y -= y;
        return np;
    }

    this.setOffset = function (p) {
        this.x += p.x;
        this.y -= p.y;
        return this;
    }

    this.getOffset = function (dx, dy, scale) {
        if (!scale) scale = 1;
        return new PointX(this.x + dx * scale, this.y + dy * scale);
    }

    this.scale = function (s) {
        this.x *= s;
        this.y *= s;
        return this;
    }

    this.getDirectionInDegrees = function (p2) {
        var p1 = this;
        var angle = (Math.atan((p1.y - p2.y) / (p2.x - p1.x)) * 180 / Math.PI);

        if (p1.y == p2.y && p1.x > p2.x) {
            angle = 180;
        } else if (p1.x > p2.x && p1.y > p2.y) {
            angle = 180 + angle;
        } else if (p1.x > p2.x && p1.y < p2.y) {
            angle = 180 + angle;
        } else if (p1.x < p2.x && p1.y < p2.y) {
            angle = 360 + angle;
        } else if (p1.x == p2.x && p1.y < p2.y) {
            angle = 270;
        }

        return angle;
    }

    this.getDistanceTo = function (p2) {
        var p1 = this;
        return Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));
    }

    this.getPointAt = function (degree, distance) {
        distance = Math.abs(distance);
        if (degree >= 360) degree = degree - 360;
        var dH = distance * Math.cos(degree * Math.PI / 180);
        var dV = -distance * Math.sin(degree * Math.PI / 180);

        return new PointX(this.x + dH, this.y + dV);
    }

    this.midpointTo = function (p2) {
        var p1 = this;
        return new PointX((p1.x + p2.x) / 2, (p1.y + p2.y) / 2);
    }

    this.copy = function () {
        return new PointX(this.x, this.y);
    }

    this.overlaps = function (p, vect) {
        var overlaplimit = (typeof (sketchApp) !== 'undefined' && sketchApp && sketchApp.scale) ? (1.5 / (Math.max(sketchApp.scale, .2))) : 3;
        overlaplimit = overlaplimit > 3 ? overlaplimit : 3;
        if (((p.x == this.x) && (p.y == this.y)) || (vect && vect.isFreeFormLineEntries && (((p.x == (Math.round(this.x))) && (p.y == (Math.round(this.y)))) || (((p.x <= (Math.round(this.x)) + overlaplimit) && (p.x >= (Math.round(this.x)) - overlaplimit)) && ((p.y <= (Math.round(this.y)) + overlaplimit) && (p.y >= (Math.round(this.y)) - overlaplimit)))))) {
            return true;
        } else {
            return false;
        }
    }

    this.mCopy = function (dx, dy) {
        return new PointX(this.x + dx, this.y + dy);
    }

    this.toString = function () {
        return "{" + Math.round(this.x) + "," + Math.round(this.y) + "}";
    }

    this.toYX = function () {
        return "{" + this.y + "," + this.x + "}";
    }

    this.alignToGrid = function (editor, mathOp) {
        if (mathOp == null)
            mathOp = sRound;

        var scale = editor.scale;
        var tx = 0, ty = 0;
        /*if( editor.currentVector && editor.currentNode && editor.currentVector.startNode ) {
            var p1 = { x: 0, y: 0 }, p2 = { x: 0, y: 0 };
            if( !editor.currentNode.isStart) {
                p1 = editor.currentVector.startNode.p;
                p2 = new PointX( mathOp(p1.x), mathOp(p1.y) )
            }
            else{
                p1 = editor.currentNode.p;
                p2 = new PointX(mathOp(p1.x), mathOp(p1.y))
            }
            tx = p1.x - p2.x;
            ty = p1.y - p2.y;
        }*/
        if (editor.currentVector && editor.currentVector.isClosed && editor.currentNode && editor.config && editor.config.EnableSpecialRounding) {
            var p1 = editor.currentNode.p;
            var p2 = new PointX(mathOp(p1.x), mathOp(p1.y));
            tx = p1.x - p2.x;
            ty = p1.y - p2.y;
        }
        var ppf = DEFAULT_PPF * scale;
        var o = this.copy();
        this.x = mathOp((this.x - editor.origin.x) / ppf) + tx;
        this.y = mathOp((editor.origin.y - this.y) / ppf) + ty;
        return this;
    }


    this.alignUnit = function (editor, mathOp) {
        if (mathOp == null)
            mathOp = sRound;

        this.alignToGrid(editor, mathOp);

        var scale = editor.scale;
        var ppf = DEFAULT_PPF * scale;
        var o = this.copy();
        this.x = CAMACloud.Sketching.Utilities.toPixel(this.x) * scale + editor.origin.x;
        this.y = editor.origin.y - CAMACloud.Sketching.Utilities.toPixel(this.y) * scale;
        return this;
    }

    this.distanceFrom = function (p) {
        return Math.sqrt(Math.pow(this.x - p.x, 2) + Math.pow(this.y - p.y, 2))
    }
}

function RoundX(x, y, scale) {
    var xx, yy;
    if (!scale) scale = 1;
    var sc = scale * 10;
    xx = Math.round(Math.round(x / sc) * sc);
    yy = Math.round(Math.round(y / sc) * sc);
    return new PointX(xx, yy);
}

function Brush(dwg, editor) {
    var brush = this;
    var drawing = dwg;
    var scale = 1;
    var offsetX = 0;
    var offsetY = 0;
    var currentPoint;
    this.editor = editor;

    drawing.lineWidth = 1;
    drawing.strokeStyle = 'Black';
    drawing.fillStyle = '#333';
    // this.lengthLabelFont = 'italic 8pt Arial';
    //this.labelFont = 'bold 10pt Arial';
    //this.areaFont = 'bold 9pt Arial';
    this.notesFont = '10pt Arial';

    this.__defineGetter__("currentPoint", function () {
        return currentPoint;
    });

    this.getFontSize = function (scale, vector) {
        var size = 0;
        if (!scale) scale = editor.scale;
        if (vector && vector.area() > 20000)
            size = 5;
        else if (vector && vector.area() > 5000)
            size = 4;
        else if (vector && vector.area() > 3000)
            size = 3;
        else if (vector && vector.area() > 1000 && scale < 0.6)
            size = 1;
        else if (vector && vector.area() > 500 && scale < 0.4)
            size = 2;
        if (scale == 1)
            return 8 + size;
        else if (scale < 0.1) {
            return 1;
        } else if ((scale >= 0.1) && (scale < 0.2)) {
            return 2 + size;
        } else if ((scale >= 0.2) && (scale < 0.4)) {
            return 4 + size;
        } else if ((scale >= 0.4) && (scale < 0.7)) {
            return 6 + size;
        } else if ((scale >= 0.7) && (scale < 1.2)) {
            return 8 + size;
        } else if ((scale >= 1.2) && (scale < 1.6)) {
            return 11 + size;
        } else if ((scale >= 1.6)) {
            return 13 + size;
        }
    }
    this.getFont = function (vector, type) {
        var fs = this.getFontSize(null, vector);
        if (type == "length")
            return 'italic ' + fs + 'pt Arial';
        else if (type == "label")
            return 'bold ' + Math.ceil(fs * 10 / 8) + 'pt Arial';
        else if (type == "area")
            return 'bold ' + Math.ceil(fs * 9 / 8) + 'pt Arial';
    }
    this.setScale = function (s) {
        scale = s;
        var fs = this.getFontSize(s);
        ////  this.lengthLabelFont = 'italic ' + fs + 'pt Arial';
        //this.labelFont = 'bold ' + Math.ceil(fs * 10 / 8) + 'pt Arial';
        //this.areaFont = 'bold ' + Math.ceil(fs * 9 / 8) + 'pt Arial';
        this.notesFont = '' + Math.ceil(fs * 10 / 8) + 'pt Arial';
    }

    this.setColor = function (stroke, fill) {
        if (stroke)
            drawing.strokeStyle = stroke;
        if (fill)
            drawing.fillStyle = fill;
    }

    this.setFillColor = function (c) {
        drawing.fillStyle = c;
    }

    this.setStrokeColor = function (c) {
        drawing.strokeStyle = c;
    }


    this.moveTo = function (p) {
        currentPoint = p;
        drawing.moveTo(currentPoint.x, currentPoint.y);
    }


    this.lineTo = function (p) {
        var o = currentPoint;
        currentPoint = p;
        drawing.lineTo(currentPoint.x, currentPoint.y);
    }

    this.moveBy = function (dx, dy) {
        currentPoint = new PointX(currentPoint.x + dx * scale, currentPoint.y - dy * scale);
        drawing.moveTo(currentPoint.x, currentPoint.y);
    }

    this.begin = function () {
        drawing.beginPath();
        return currentPoint;
    }

    this.end = function () {
        drawing.closePath();
        return currentPoint;
    }

    this.up = function (dy, stroke, stayAtStart) {
        this.any(0, -dy, stroke, stayAtStart);
    }

    this.down = function (dy, stroke, stayAtStart) {
        this.any(0, dy, stroke, stayAtStart);
    }

    this.left = function (dx, stroke, stayAtStart) {
        this.any(-dx, 0, stroke, stayAtStart);
    }

    this.right = function (dx, stroke, stayAtStart) {
        this.any(dx, 0, stroke, stayAtStart);
    }

    this.drawPath = function (vector, path, veer, stroke, labels, hideDimensions, fillColor) {
        if (veer) return this.veer(vector, path, stroke, labels, hideDimensions, fillColor);
        for (var pi in path) {
            var p = path[pi];
            brush.go(vector, p.dir, p.dist, stroke, false, labels, hideDimensions, fillColor);
        }
        return this;
    }

    this.go = function (vector, dir, dist, stroke, stayAtStart, labelsOnly, hideDimensions, fillColor) {
        var dx = 0,
            dy = 0;
        var sd = CAMACloud.Sketching.Utilities.toPixel(dist); // * scale;
        switch (dir) {
            case "U":
                dy = -sd;
                break;
            case "D":
                dy = sd;
                break;
            case "L":
                dx = -sd;
                break;
            case "R":
                dx = sd;
                break;
        }
        if ((dx != 0) || (dy != 0)) {
            if (labelsOnly) {
                dist = (sketchSettings.SegmentRoundingFactor ? (Math.round(dist * sketchSettings.SegmentRoundingFactor) / sketchSettings.SegmentRoundingFactor) : sRound(dist));
                this.drawLabels(vector, dx, dy, dist, hideDimensions);
            }
            else
                this.any(dx, dy, stroke, stayAtStart, dist, fillColor);
        }
    }

    this.veer = function (vector, path, stroke, labelsOnly, hideDimensions, fillColor) {
        var dx = 0,
            dy = 0;
        var dsq = 0;
        for (var x in path) {
            var p = path[x];
            var sd = CAMACloud.Sketching.Utilities.toPixel(p.dist); // * scale;
            dsq += p.dist * p.dist;
            switch (p.dir) {
                case "U":
                    dy = -sd;
                    break;
                case "D":
                    dy = sd;
                    break;
                case "L":
                    dx = -sd;
                    break;
                case "R":
                    dx = sd;
                    break;
            }
        }
        var val = Number(Math.sqrt(dsq));
        if (Math.abs(Math.round(val) - val) <= .009)
            val = Math.round(val)
        var adst = sRound(Math.round(val * (DEFAULT_PPF * 10)) / 100);
        adst = Number(adst.toFixed(2)); // convert to two decimal, if sketch decimal is 4 then decimal comes more than 2 because of javascript rounding issue
        if (labelsOnly) {
            adst = (sketchSettings.SegmentRoundingFactor ? (Math.round(val * sketchSettings.SegmentRoundingFactor) / sketchSettings.SegmentRoundingFactor) : adst);
            this.drawLabels(vector, dx, dy, adst, hideDimensions);
        }
        else
            this.any(dx, dy, stroke, false, adst, fillColor);
        return this;
    }

    this.any = function (dx, dy, stroke, stayAtStart, dist, fillColor) {
        var nextPoint = new PointX(currentPoint.x + dx * scale, currentPoint.y + dy * scale);
        if (stroke) {
            if (!fillColor) drawing.moveTo(currentPoint.x, currentPoint.y);
            drawing.lineTo(nextPoint.x, nextPoint.y);
            if (!fillColor) drawing.stroke();
            var o = currentPoint;
            var p = nextPoint;
        }
        else {
            drawing.moveTo(nextPoint.x, nextPoint.y);
        }
        if (!stayAtStart) {
            currentPoint = nextPoint
        }
        else drawing.moveTo(currentPoint.x, currentPoint.y);

        return this;
    }

    this.drawNotes = function (x, y, note, ls) {
        var text = note.noteText;
        if (!text)
            return true;

        function wrapText(context, text, x, y, maxWidth, lineHeight, measureOnly) {
            var textlines = text.split("\n");
            var textHeight = lineHeight;
            var line = '';
            for (var i in textlines) {
                var tline = textlines[i];
                var words = tline.split(' ');
                for (var n = 0; n < words.length; n++) {
                    var testLine = line + words[n] + ' ';
                    var metrics = context.measureText(testLine);
                    var testWidth = metrics.width;
                    if (testWidth > maxWidth - 10 && n > 0) {
                        if (!measureOnly) context.fillText(line, x, y);
                        line = words[n] + ' ';
                        y += lineHeight;
                        textHeight += lineHeight
                    } else {
                        line = testLine;
                    }
                }
                if (!measureOnly) context.fillText(line, x, y);
                y += lineHeight;
                textHeight += lineHeight
                line = ''
            }
            // if (!measureOnly) context.fillText(line, x, y);
            return textHeight - lineHeight;
        }

        drawing.font = this.notesFont;
        brush.setColor('black', 'black');

        var lineHeight = brush.getFontSize(editor.scale) * 10 / 8 * 1.5;
        var metrics = drawing.measureText(text);
        var boxHeight = wrapText(drawing, text, x, y, 300 * editor.scale, lineHeight);
        var boxWidth = metrics.width; //metrics.width > 300 * editor.scale ? 300 * editor.scale : metrics.width;

        var margin = 10 * editor.scale;
        drawing.beginPath();
        drawing.shadowBlur = 10;
        drawing.shadowColor = 'rgba(140,140,140,1)';

        //box Points
        var x1 = x - margin,
            y1 = y - lineHeight - margin,
            x2 = x + boxWidth + margin,
            y2 = y - lineHeight + boxHeight + margin;

        var bw = x2 - x1;
        var bh = y2 - y1;
        drawing.rect(x1, y1, bw, bh);
        note.boxWidth = Math.round((bw / editor.scale) / DEFAULT_PPF);
        note.boxHeight = Math.round((bh / editor.scale) / DEFAULT_PPF);
        note.boxOrgin = new PointX(x1, y1).alignToGrid(editor, Math.round);
        var grd = editor.drawing.createLinearGradient(x1, y1, x2, y2);
        grd.addColorStop(0, 'rgba(248,237,98,1)')
        grd.addColorStop(1, 'rgba(248,237,98,1)')
        brush.setColor('#dab600', 'rgba(248,237,98,1)')
        if (note == editor.currentSketchNote) {
            drawing.fillStyle = '#FFBB00';
        }
        drawing.stroke();
        drawing.fill()
        drawing.shadowBlur = 0;
        drawing.shadowColor = null;
        brush.setColor('Black', 'Black');
        wrapText(drawing, text, x, y, 300 * editor.scale, lineHeight);

        if (ls) {
            var cp = (new PointX(x1, y1)).midpointTo(new PointX(x2, y1));
            if (ls.getDistanceTo(new PointX(x2, y1)) < ls.getDistanceTo(cp)) cp = (new PointX(x2, y1)).midpointTo(new PointX(x1, y1));
            if (ls.getDistanceTo(new PointX(x1, y2)) < ls.getDistanceTo(cp)) cp = (new PointX(x1, y2)).midpointTo(new PointX(x2, y2));
            if (ls.getDistanceTo(new PointX(x2, y2)) < ls.getDistanceTo(cp)) cp = (new PointX(x2, y2)).midpointTo(new PointX(x1, y2));

            drawing.beginPath();
            drawing.moveTo(ls.x, ls.y);
            drawing.lineWidth = 5;
            drawing.lineTo(cp.x, cp.y);
            brush.setColor('#777', 'rgba(248,237,98,1)');
            drawing.stroke();
            drawing.beginPath();
            drawing.lineWidth = 1;
            brush.setColor('Black', 'rgba(248,237,98,1)');
            drawing.arc(ls.x, ls.y, 3, 0, 2 * Math.PI, 0);
            drawing.fill();
            drawing.stroke();
        }
        return this;
    }

    this.drawLabels = function (vector, dx, dy, dist, hideDimensions) {
        //dist = sRound(dist); rounded parameter passing
        if (vector.showInLowOpacity)
            editor.brush.setColor('#b7b7b7', '#b7b7b7');
        else if (vector.newLabelColorCode)
            editor.brush.setColor(vector.newLabelColorCode, vector.newLabelColorCode);
        else if (vector == editor.currentVector) {
            editor.brush.setColor('Black', 'Blue');
        } else {
            editor.brush.setColor('Black', 'Black');
        }
        var nextPoint = new PointX(currentPoint.x + dx * scale, currentPoint.y + dy * scale);

        var fs = brush.getFontSize(null, vector);
        if (fs > 5 || (vector.area() > 1000 && fs > 3)) {
            if (dist) {
                var mp = new PointX((currentPoint.x + nextPoint.x) / 2, (currentPoint.y + nextPoint.y) / 2);
                var dimensionValue = ((this.editor.config && this.editor.config.DimensionFormatter) ? this.editor.config.DimensionFormatter(dist) : ((typeof (ccma) !== 'undefined' && ccma.Sketching && ccma.Sketching.Config && ccma.Sketching.Config.DimensionFormatter) ? ccma.Sketching.Config.DimensionFormatter(dist) : dist));
                var dimensionText = dimensionValue + (this.editor.formatter.LengthUnit || DISTANCE_UNIT || '');
                var distText = dist + (this.editor.formatter.LengthUnit || DISTANCE_UNIT || '');
                drawing.font = this.getFont(vector, "length");
                var margin = 2 * editor.scale;
                var th = brush.getFontSize(editor.scale, vector) * 1.5;
                var tw = drawing.measureText(distText).width + 2 * editor.scale;

                var dir = "*";
                if (dx == 0) dir = "V";
                if (dy == 0) dir = "H";
                var lp = mp.copy();
                var labelPosition = "*";
                if (dir != "*") {
                    if (dir == "H") {
                        if (vector.contains(lp.toUnits(editor).mCopy(0, 2))) {
                            lp = new PointX(mp.x, mp.y + margin - th);
                            labelPosition = "B";
                        } //BOTTOM
                        else if (vector.contains(lp.toUnits(editor).mCopy(0, -2))) {
                            lp = new PointX(mp.x, mp.y - margin + (2 * th));
                            labelPosition = "T";
                        } // TOP
                    }
                    if (dir == "V") {
                        if (vector.contains(lp.toUnits(editor).mCopy(2, 0))) {
                            lp = new PointX(mp.x - margin + tw, mp.y);
                            labelPosition = "L";
                        } //LEFT
                        else if (vector.contains(lp.toUnits(editor).mCopy(-2, 0))) {
                            lp = new PointX(mp.x + margin - (2 * th), mp.y);
                            labelPosition = "R";
                        } // RIGHT
                    }
                } else {
                    if (dx > 0 && dy < 0)
                        if (vector.contains(mp.copy().toUnits(editor).mCopy(-1, 1))) {
                            lp = new PointX(mp.x - (3 * th / 2), mp.y - th / 2 + margin);
                            labelPosition = "BR";
                        } else {
                            lp = new PointX(mp.x + th / 2, mp.y + (3 * th / 2));
                            labelPosition = "BR";
                        } // TOP-LEFT
                    if (dx < 0 && dy < 0)
                        if (vector.contains(mp.copy().toUnits(editor).mCopy(-1, -1))) {
                            lp = new PointX(mp.x - (3 * th / 2), mp.y + (3 * th / 2));
                            labelPosition = "TR";
                        } else {
                            lp = new PointX(mp.x + th / 2, mp.y - th / 2);
                            labelPosition = "TR";
                        } //LEFT
                    if (dx < 0 && dy > 0)
                        if (vector.contains(mp.copy().toUnits(editor).mCopy(1, -1))) {
                            lp = new PointX(mp.x + th, mp.y + th);
                            labelPosition = "TL";
                        } else {
                            lp = new PointX(mp.x - (2 * th), mp.y);
                            labelPosition = "TL";
                        } // TOP
                    if (dx > 0 && dy > 0)
                        if (vector.contains(mp.copy().toUnits(editor).mCopy(1, 1))) {
                            lp = new PointX(mp.x + th / 2 - margin, mp.y - th / 2);
                            labelPosition = "BL";
                        } else {
                            lp = new PointX(mp.x - (3 * th / 2) + margin, mp.y + (3 * th / 2));
                            labelPosition = "BL";
                        } //BOTTOM-LEFT
                }


                //if (vector == editor.currentVector) console.log(dx, dy, labelPosition, distText, tw, th, margin, mp.toString(), dir,  lp.toString())
                if (hideDimensions || (vector && (vector.isFreeFormLineEntries || ((clientSettings.SketchConfig == 'MVP' || sketchSettings.SketchConfig == 'MVP') && (vector.isUnSketchedArea || vector.isUnSketchedTrueArea))))) { //apexJson freeformLine do not show dimensions
                }
                else {
                    drawing.fillText(dimensionText, lp.x, lp.y);
                }
            }
        }

        currentPoint = nextPoint;
        return this;
    }
    this.drawCircleRadiuslabel = function (vector, radius, center) {
        radius = sRound(radius);
        if (vector.showInLowOpacity)
            editor.brush.setColor('#b7b7b7', '#b7b7b7');
        else if (vector.newLabelColorCode)
            editor.brush.setColor(vector.newLabelColorCode, vector.newLabelColorCode);
        else if (vector == editor.currentVector)
            editor.brush.setColor('Black', 'Blue');
        else
            editor.brush.setColor('Black', 'Black');
        var mpl = new PointX(currentPoint.x + CAMACloud.Sketching.Utilities.toPixel(center.x + (radius / 2)) * scale, currentPoint.y - CAMACloud.Sketching.Utilities.toPixel(center.y) * scale);
        drawing.font = this.getFont(vector, "length");
        //MA_2442
        //if (!vector.disablemove && !((clientsettings.sketchconfig == 'mvp' || sketchsettings.sketchconfig == 'mvp') && (vector.isunsketchedarea || vector.isunsketchedtruearea)))
            //drawing.fillText(radius, mpl.x, mpl.y - 5)
    }
    this.drawArcLabel = function (vector, mx, my, dx, dy, length, arcLength, hideDimensions) {
        var unit = (this.editor.formatter.LengthUnit || DISTANCE_UNIT || '');
        length = length = (sketchSettings.SegmentRoundingFactor ? (Math.round(length * sketchSettings.SegmentRoundingFactor) / sketchSettings.SegmentRoundingFactor) : sRound(length));
        arcLength = sRound(arcLength);
        if (vector.showInLowOpacity)
            editor.brush.setColor('#b7b7b7', '#b7b7b7');
        else if (vector.newLabelColorCode)
            editor.brush.setColor(vector.newLabelColorCode, vector.newLabelColorCode);
        else if (vector == editor.currentVector)
            editor.brush.setColor('Black', 'Blue');
        else
            editor.brush.setColor('Black', 'Black');
        var mpl = new PointX(currentPoint.x + CAMACloud.Sketching.Utilities.toPixel(mx) * scale, currentPoint.y - CAMACloud.Sketching.Utilities.toPixel(my) * scale);
        drawing.font = this.getFont(vector, "length");
        if (!hideDimensions)
            drawing.fillText(length + unit + " / " + arcLength + unit, mpl.x + 1, mpl.y - 2);

        var nextPoint = new PointX(currentPoint.x + CAMACloud.Sketching.Utilities.toPixel(dx) * scale, currentPoint.y - CAMACloud.Sketching.Utilities.toPixel(dy) * scale);
        currentPoint = nextPoint;
        return this;
    }

    this.rect = function (w, h) {
        drawing.rect(currentPoint.x, currentPoint.y, w * scale, h * scale);
    }

    this.rectX = function (w, h) {
        var xx = Math.floor((w * scale) / 2);
        var yy = Math.floor((h * scale) / 2);
        currentPoint = new PointX(currentPoint.x - xx, currentPoint.y - yy);
        drawing.rect(currentPoint.x, currentPoint.y, w * scale, h * scale);
    }

    this.pointer = function (w, h, angle) {
        var hs = h * scale;
        var ws = w * scale;
        var xx = Math.floor((ws) / 2);
        var yy = Math.floor((hs) / 2);
        var px = Math.ceil(ws / 2);
        var py = Math.ceil(hs / 2);
        var cx = currentPoint.x,
            cy = currentPoint.y;
        drawing.translate(cx, cy);
        drawing.rotate(-angle * Math.PI / 180);
        drawing.translate(-cx, -cy);

        currentPoint = new PointX(currentPoint.x - xx, currentPoint.y - yy);
        drawing.moveTo(currentPoint.x, currentPoint.y);
        drawing.lineTo(currentPoint.x + ws, currentPoint.y);
        drawing.lineTo(currentPoint.x + ws + px, currentPoint.y + py);
        drawing.lineTo(currentPoint.x + ws, currentPoint.y + hs);
        drawing.lineTo(currentPoint.x, currentPoint.y + hs);
        drawing.lineTo(currentPoint.x, currentPoint.y);

        drawing.translate(cx, cy);
        drawing.rotate(angle * Math.PI / 180);
        drawing.translate(-cx, -cy);

    }


    this.circle = function (r) {
        drawing.arc(currentPoint.x, currentPoint.y, r * scale * DEFAULT_PPF, 0, 2 * Math.PI);
    }

    this.drawEllipse = function (path, stroke, labels, lineSelection) {
        if (path.length < 2) return this;
        var a = CAMACloud.Sketching.Utilities.toPixel(path[0].dist);
        var b = CAMACloud.Sketching.Utilities.toPixel(path[1].dist);
        var isLine = this.ellipse(b, a, stroke, lineSelection);
        isLine = lineSelection ? isLine : this;
        return isLine
    }

    this.drawArc = function (path, arcLength, node, fillColor, lineSelection, noStroke) {
        var p1, dx = 0, dy = 0, dsq = 0;
        if (lineSelection) {
            p1 = lineSelection.currentPoint;
            dx = lineSelection.dx;
            dy = lineSelection.dy;
        }
        else {
            p1 = new PointX(currentPoint.x, currentPoint.y);
            var dx = 0,
                dy = 0;
            var dsq = 0;
            for (var x in path) {
                var p = path[x];
                var sd = CAMACloud.Sketching.Utilities.toPixel(p.dist); // * scale;
                dsq += p.dist * p.dist;
                switch (p.dir) {
                    case "U":
                        dy = -sd;
                        break;
                    case "D":
                        dy = sd;
                        break;
                    case "L":
                        dx = -sd;
                        break;
                    case "R":
                        dx = sd;
                        break;
                }
            }
            var diam = CAMACloud.Sketching.Utilities.toPixel(sRound(Math.sqrt(dsq)));
        }

        var p2 = p1.getOffset(dx, dy, scale);
        var c = p1.midpointTo(p2);

        var angle = p1.getDirectionInDegrees(p2);

        var pangle = angle + ARC_SIGN * sign(arcLength) * 90;
        if (pangle > 360) pangle = pangle - 360;

        var p3 = c.getPointAt(pangle, CAMACloud.Sketching.Utilities.toPixel(arcLength) * scale);

        var ap1p3 = p1.getDirectionInDegrees(p3);
        var leftInclusive = Math.abs(ap1p3 - angle);
        if (leftInclusive > 90) leftInclusive = 360 - leftInclusive;
        var topInclusive = 180 - 2 * leftInclusive;
        var dp1p3 = p1.getDistanceTo(p3);
        var radius = Math.abs((dp1p3 / 2) / Math.cos(topInclusive / 2 * Math.PI / 180));

        var rpangle = pangle + 180;
        if (rpangle > 360) rpangle = rpangle - 360;
        var cc = p3.getPointAt(rpangle, radius);

        var startAngle = sRound(360 - cc.getDirectionInDegrees(p1))
        var endAngle = sRound(360 - cc.getDirectionInDegrees(p2))
        var midAngle = (360 - cc.getDirectionInDegrees(p3));

        //console.log("*", startAngle - midAngle, midAngle - endAngle);

        var check1 = startAngle - midAngle;
        if (check1 > 180) check1 = check1 - 360;
        if (check1 < -180) check1 = 360 + check1;
        var check2 = midAngle - endAngle;
        if (check2 > 180) check2 = check2 - 360;
        if (check2 < -180) check2 = 360 + check2;
        //console.log(check1, check2);
        if (!fillColor && !lineSelection) drawing.moveTo(currentPoint.x, currentPoint.y);
        cc.x = sRound(cc.x), cc.y = sRound(cc.y);
        drawing.arc(cc.x, cc.y, radius, startAngle * Math.PI / 180, endAngle * Math.PI / 180, check1 > 0);

        //        this.moveTo(cc);
        //        this.circle(4)
        //        this.moveTo(p3)
        //        this.rectX(4, 4)
        if (lineSelection) {
            var isLine = drawing.isPointInStroke(lineSelection.ecx, lineSelection.ecy);
            return { p2: p2, isLine: isLine };
        };
        this.lineTo(p2);
        if (!fillColor && !noStroke) drawing.stroke();
        return this;
    }

    this.arc180 = function (rx, ry, angle, stroke) {
        var center = new PointX(currentPoint.x + rx * scale, currentPoint.y);

        rx = rx * scale;
        ry = ry * scale;
        var sx = 1,
            sy = 1;
        if (rx > ry)
            sy = ry / rx;
        if (ry > rx)
            sx = rx / ry;
        drawing.scale(sx, sy);
        drawing.arc(center.x / sx, center.y / sy, rx / sx, Math.PI, 0, 0);
        if (stroke)
            drawing.stroke();
        drawing.scale(1 / sx, 1 / sy);
    }

    this.ellipse = function (rx, ry, stroke, lineSelection) {
        var center = lineSelection ? (new PointX(lineSelection.currentPoint.x + rx * scale, lineSelection.currentPoint.y)) : (new PointX(currentPoint.x + rx * scale, currentPoint.y));
        var isLine = false;

        rx = rx * scale;
        ry = ry * scale;
        var sx = 1,
            sy = 1;
        if (rx > ry)
            sy = ry / rx;
        if (ry > rx)
            sx = rx / ry;
        drawing.scale(sx, sy);
        drawing.arc(center.x / sx, center.y / sy, rx / sx, -1 * Math.PI, 1 * Math.PI);
        if (stroke)
            drawing.stroke();
        drawing.scale(1 / sx, 1 / sy);
        if (lineSelection)
            isLine = drawing.isPointInStroke(lineSelection.ecx, lineSelection.ecy);
        return isLine;
    }

    this.write = function (p, text, style) {
        var o = editor.origin.copy();
        drawing.font = style;
        drawing.fillText(text, (p.x * DEFAULT_PPF + o.x), (o.y - p.y * DEFAULT_PPF));
    }

    this.stroke = function () {
        drawing.stroke();
    }

    this.fill = function () {
        drawing.fill();
    }

    this.strokeAndFill = function () {
        drawing.stroke();
        drawing.fill();
    }
}

function PanControl(selector, editor) {
    var control;
    var pc = this;
    this.panUp = function () { }
    this.panDown = function () { }
    this.panLeft = function () { }
    this.panRight = function () { }
    this.panReset = function () { }

    function init() {
        $(control).bind('touchstart mousedown', function () {
            event.preventDefault();
            var x, y, undoPushed = false;
            if (event.touches) {
                x = event.touches[0].clientX - event.srcElement.offsetLeft;
                y = event.touches[0].clientY - event.srcElement.offsetTop;
            } else {
                x = event.offsetX;
                y = event.offsetY;
            }
            if ((x >= 50) && (x <= 100) && (y >= 2) && (y <= 50)) {
                if (editor.currentNode) {
                    if (editor.currentVector && (editor.currentVector.isClosed || editor.currentVector.isFreeFormLineEntries)) {
                        undoProcessArray.push({ Action: 'NodeMove', NodeNumber: nodeNumber, NodePoint: _.clone(editor.currentNode.p), Node: _.clone(editor.currentNode) });
                        undoPushed = true;
                    }
                    var p = new PointX(editor.currentNode.p.x, editor.currentNode.p.y)
                    p.moveBy(0, -.1);
                    if (!editor.checkOverLapAndClose(p)) {
                        editor.currentNode.p.moveBy(0, -.1);
                        editor.currentNode.recalculateAll();
                    }
                    if (editor.currentVector) editor.currentVector.isModified = true;
                }
                else if (editor.currentVector) {
                    if (editor.currentVector.isClosed || editor.currentVector.isFreeFormLineEntries) {
                        undoProcessArray.push({ Action: 'PanSketchMove', NodeNumber: null, NodePoint: _.clone(editor.currentVector.startNode.p), Node: _.clone(editor.currentVector.startNode), LabelPosition: _.clone(editor.currentVector.labelPosition), AreaLabelPosition: _.clone(editor.currentVector.AreaLabelPosition) });
                        undoPushed = true;
                    }
                    editor.currentVector.moveBy(new PointX(0, -0.1), true);
                }
                else {
                    $(control).attr('class', 'control-window pan-control pan-up');
                    pc.panUp();
                }
            } else if ((x >= 50) && (x <= 100) && (y >= 100) && (y <= 148)) {
                if (editor.currentNode) {
                    if (editor.currentVector && (editor.currentVector.isClosed || editor.currentVector.isFreeFormLineEntries)) {
                        undoProcessArray.push({ Action: 'NodeMove', NodeNumber: nodeNumber, NodePoint: _.clone(editor.currentNode.p), Node: _.clone(editor.currentNode) });
                        undoPushed = true;
                    }
                    var p = new PointX(editor.currentNode.p.x, editor.currentNode.p.y)
                    p.moveBy(0, .1);
                    if (!editor.checkOverLapAndClose(p)) {
                        editor.currentNode.p.moveBy(0, .1);
                        editor.currentNode.recalculateAll();
                    }
                    if (editor.currentVector) editor.currentVector.isModified = true;
                }
                else if (editor.currentVector) {
                    if (editor.currentVector.isClosed || editor.currentVector.isFreeFormLineEntries) {
                        undoProcessArray.push({ Action: 'PanSketchMove', NodeNumber: null, NodePoint: _.clone(editor.currentVector.startNode.p), Node: _.clone(editor.currentVector.startNode), LabelPosition: _.clone(editor.currentVector.labelPosition), AreaLabelPosition: _.clone(editor.currentVector.AreaLabelPosition) });
                        undoPushed = true;
                    }
                    editor.currentVector.moveBy(new PointX(0, 0.1), true);

                }
                else {
                    $(control).attr('class', 'control-window pan-control pan-down');
                    pc.panDown();
                }
            } else if ((x >= 2) && (x <= 50) && (y >= 50) && (y <= 148)) {
                if (editor.currentNode) {
                    if (editor.currentVector && (editor.currentVector.isClosed || editor.currentVector.isFreeFormLineEntries)) {
                        undoProcessArray.push({ Action: 'NodeMove', NodeNumber: nodeNumber, NodePoint: _.clone(editor.currentNode.p), Node: _.clone(editor.currentNode) });
                        undoPushed = true;
                    }
                    var p = new PointX(editor.currentNode.p.x, editor.currentNode.p.y)
                    p.moveBy(-.1, 0);
                    if (!editor.checkOverLapAndClose(p)) {
                        editor.currentNode.p.moveBy(-.1, 0);
                        editor.currentNode.recalculateAll();
                    }
                    if (editor.currentVector) editor.currentVector.isModified = true;
                }
                else if (editor.currentVector) {
                    if (editor.currentVector.isClosed || editor.currentVector.isFreeFormLineEntries) {
                        undoProcessArray.push({ Action: 'PanSketchMove', NodeNumber: null, NodePoint: _.clone(editor.currentVector.startNode.p), Node: _.clone(editor.currentVector.startNode), LabelPosition: _.clone(editor.currentVector.labelPosition), AreaLabelPosition: _.clone(editor.currentVector.AreaLabelPosition) });
                        undoPushed = true;
                    }
                    editor.currentVector.moveBy(new PointX(0.1, 0), true);
                }
                else {
                    $(control).attr('class', 'control-window pan-control pan-left');
                    pc.panLeft();
                }
            } else if ((x >= 100) && (x <= 150) && (y >= 50) && (y <= 148)) {
                if (editor.currentNode) {
                    if (editor.currentVector && (editor.currentVector.isClosed || editor.currentVector.isFreeFormLineEntries)) {
                        undoProcessArray.push({ Action: 'NodeMove', NodeNumber: nodeNumber, NodePoint: _.clone(editor.currentNode.p), Node: _.clone(editor.currentNode) });
                        undoPushed = true;
                    }
                    var p = new PointX(editor.currentNode.p.x, editor.currentNode.p.y)
                    p.moveBy(.1, 0);
                    if (!editor.checkOverLapAndClose(p)) {
                        editor.currentNode.p.moveBy(.1, 0);
                        editor.currentNode.recalculateAll();
                    }
                    if (editor.currentVector) editor.currentVector.isModified = true;
                }
                else if (editor.currentVector) {
                    if (editor.currentVector.isClosed || editor.currentVector.isFreeFormLineEntries) {
                        undoProcessArray.push({ Action: 'PanSketchMove', NodeNumber: null, NodePoint: _.clone(editor.currentVector.startNode.p), Node: _.clone(editor.currentVector.startNode), LabelPosition: _.clone(editor.currentVector.labelPosition), AreaLabelPosition: _.clone(editor.currentVector.AreaLabelPosition) });
                        undoPushed = true;
                    }
                    editor.currentVector.moveBy(new PointX(-0.1, 0), true);
                }
                else {
                    $(control).attr('class', 'control-window pan-control pan-right');
                    pc.panRight();
                }
            } else if ((x > 50) && (x < 100) && (y > 50) && (y < 150)) {
                if (!editor.currentVector) {
                    $(control).attr('class', 'control-window pan-control pan-reset');
                    pc.panReset();
                }
            } else {
                $(control).attr('class', 'control-window pan-control pan-normal');
            }

            if (undoPushed && undoProcessArray.length > 20)
                undoProcessArray.shift();

            if (editor.currentVector) sketchApp.render();
        });
        $(control).bind('touchend mouseup', function () {
            $(control).attr('class', 'control-window pan-control pan-normal');
        });
        $(control).bind('touchleave touchcancel mouseleave', function () {
            $(control).attr('class', 'control-window pan-control pan-normal');
        });
    }


    control = $(selector);
    if (control.length == 0) {
        return;
    }
    init();
}

function ZoomControl(selector) {
    var control;
    var range, plus, minus;
    var c = this;
    var mouseLock;


    var zoomPercent = 49;
    var rangeStartValue;
    var rangeFinalValue;

    this.zoom = function (z) {

    }

    this.setZoom = function (z) {
        z = Math.round(z);
        c.updateRange(z);
    }

    function init() {
        range = $('.range', control);
        minus = $('.minus', control);
        plus = $('.plus', control);

        $(plus).unbind(touchClickEvent);
        $(plus).bind(touchClickEvent, function (e) {
            e.preventDefault();
            zoomPercent += 2.5;
            if (zoomPercent > 100) {
                zoomPercent = 100;
            }
            c.updateRange();
            c.zoom(zoomPercent);
        });

        $(minus).unbind(touchClickEvent);
        $(minus).bind(touchClickEvent, function (e) {
            e.preventDefault();
            zoomPercent -= 2.5;
            if (zoomPercent < 0) {
                zoomPercent = 0;
            }
            c.updateRange();
            c.zoom(zoomPercent);
        });

        $(range).bind('touchstart touchmove touchend touchleave touchcancel mousedown mousemove mouseup mouseleave', function (event) {
            event.preventDefault();
            if (event.touches) {
                switch (event.type) {
                    case 'touchstart':
                        rangeStartValue = Math.round((event.touches[0].clientX - event.srcElement.offsetLeft) / 302 * 100);
                        c.updateRange(rangeStartValue);
                        break;
                    case 'touchmove':
                        rangeFinalValue = Math.round((event.touches[0].clientX - event.srcElement.offsetLeft) / 302 * 100);
                        c.updateRange(rangeFinalValue);
                        c.zoom(zoomPercent);
                        break;
                    case 'touchend':
                        if (event.touches)
                            if (event.touches.length > 0) {
                                rangeFinalValue = Math.round((event.touches[0].clientX - event.srcElement.offsetLeft) / 302 * 100);
                                c.updateRange(rangeFinalValue);
                                c.zoom(zoomPercent);
                            }
                        break;
                    default:
                        break;
                }
            } else {
                switch (event.type) {
                    case 'mousedown':
                        mouseLock = true;
                        rangeStartValue = Math.round((event.offsetX) / 302 * 100);
                        c.updateRange(rangeStartValue);
                        break;
                    case 'mousemove':
                        if (mouseLock) {
                            rangeFinalValue = Math.round((event.offsetX) / 302 * 100);
                            c.updateRange(rangeFinalValue);
                            c.zoom(zoomPercent);
                        }
                        break;
                    case 'mouseup':
                        mouseLock = false;
                        rangeFinalValue = Math.round((event.offsetX) / 302 * 100);
                        c.updateRange(rangeFinalValue);
                        c.zoom(zoomPercent);
                        break;
                    default:
                        mouseLock = false;
                        break;
                }
            }

        });
    }


    this.updateRange = function (z) {
        if (z)
            zoomPercent = z;
        if (zoomPercent > 100) {
            zoomPercent = 100;
        }
        if (zoomPercent < 0) {
            zoomPercent = 0;
        }

        var rperc = Math.round(zoomPercent) + '% 100%'
        $(range).css({
            'background-size': rperc
        });
    }

    control = $(selector);
    if (control.length == 0) {
        return;
    }

    init();
}

function SketchEditorToolbar(selector, editor) {
    var control;
    this.editor = editor;
    var tb = this;

    if (editor)
        editor.toolbar = this;

    function activate(c) {
        $(c).removeClass('ccse-tool-inactive');
        $(c).addClass('ccse-tool-active');
    }

    function deactivate(c) {
        $(c).removeClass('ccse-tool-active');
        $(c).addClass('ccse-tool-inactive');
    }

    function isActive(c) {
        return $(c).hasClass('ccse-tool-active');
    }

    function init() {
        $('.control-window', control).hide();
        $('.ccse-toolbutton', control).removeClass('ccse-tool-active');
        $('.ccse-toolbutton', control).addClass('ccse-tool-inactive');

        $('.ccse-toolbutton', control).unbind();
        bindToolbarButton('.ccse-pan', '.pan-control');
        bindToolbarButton('.ccse-zoom', '.zoom-control');
        bindToolbarButton('.ccse-angle-lock');
        $('.ccse-undo').unbind(touchClickEvent);
        $('.ccse-undo').bind(touchClickEvent, function (e) {
            e.preventDefault();
            if (tb.editor && tb.editor.undo) tb.editor.undo();
            return false;
        });
        $('.ccse-redo').unbind(touchClickEvent);
        $('.ccse-redo').bind(touchClickEvent, function (e) {
            e.preventDefault();
            if (tb.editor && tb.editor.redo) tb.editor.redo();
            return false;
        });
        $('#color_picker').unbind('change');
        $('#color_picker').bind('change', function () {
            if (tb.editor.currentVector) {
                tb.editor.currentVector.colorCode = $(this).val();
                tb.editor.render()
            }
            $('.colorPicker-picker').css("background-color", $(this).val())
        })
        $('.ccse-show-pad').unbind(touchClickEvent);
        $('.ccse-show-pad').bind(touchClickEvent, function (e) {
            e.preventDefault();
            if (((sketchSettings && sketchSettings["EnableRotation"] == "1") || (clientSettings && clientSettings["EnableRotation"] == "1")) && sketchApp && sketchApp.rotationButtonEnabled) {
                $('.sketch-pad .sp-left span').css("background-image", "url(/App_Static/css/sketchpad/sp-left.png)");
                $('.sketch-pad .sp-right span').css("background-image", "url(/App_Static/css/sketchpad/sp-right.png)");
                sketchApp.rotationButtonEnabled = false;
            }
            tb.editor.showKeypad();
            var sp_Text = $('.sp-text-box');
            $(sp_Text[0]).trigger('click')
            $('.box-length span').text('0');
            $('.box-angle span').text('0');
            $('.box-lengthR span').text('0');
            $('.box-width span').text('0');
            return false;
        });

        $('.ccse-rotate-sketch').bind(touchClickEvent, function (e) {
            e.preventDefault();
            tb.editor.showKeypad();
            sketchApp.rotationButtonEnabled = true;
            if ((sketchSettings && sketchSettings["EnableRotation"] == "1") || (clientSettings && clientSettings["EnableRotation"] == "1")) {
                $('.sketch-pad .sp-left span').css("background-image", "url(/App_Static/css/sketchpad/rotate-anti.png)");
                $('.sketch-pad .sp-right span').css("background-image", "url(/App_Static/css/sketchpad/rotate-clock.png)");
            }
            var sp_Text = $('.sp-text-box');
            $(sp_Text[1]).trigger('click')
            $('.box-length span').text('0');
            $('.box-angle span').text('0');
            $('.box-lengthR span').text('0');
            $('.box-width span').text('0');
            return false;
        });

        $('.ccse-delete-node').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                if (tb.editor) {
                    tb.editor.deleteCurrentNode();
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });
        $('.ccse-beforesketch').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                $('.ccse-save').show();
                $('.ccse-vector-select').show();
                $('.ccse-pan').show();
                $('.ccse-angle-lock').show();
                sketchApp.CC_beforeSketch = false;
                CC_beforeSketch = false;
                var skQuery = (sketchSettings["sketchLookupQuery"] && sketchSettings["sketchLookupQuery"] != '') ? true : false;
                var sketches = loadSketchSegments(sketchApp.CC_beforeSketch, skQuery);
                editor.refreshSketchesAfterSave(sketches, true);
                $('.ccse-currentsketch').show();
                $('.ccse-beforesketch').hide();
                if ($('#Btn_Save_vector_properties')[0])
                    $('#Btn_Save_vector_properties').removeAttr('disabled');
                if (!(window.opener && window.opener.appType == "sv")) {
                    window.opener.showSketchesInQC(null, sketchApp.CC_beforeSketch);
                    window.opener.showPreviousSketch('0')
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });

        $('.ccse-currentsketch').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                if (tb.editor) {
                    sketchApp.CC_beforeSketch = true;
                    CC_beforeSketch = true;
                    //$('.ccse-vector-select').show();
                    var skQuery = (sketchSettings["sketchLookupQuery"] && sketchSettings["sketchLookupQuery"] != '') ? true : false;
                    var sketches = loadSketchSegments(sketchApp.CC_beforeSketch, skQuery);
                    editor.refreshSketchesAfterSave(sketches, true);
                    if ($('#Btn_Save_vector_properties')[0])
                        $('#Btn_Save_vector_properties').attr('disabled', 'disabled');
                    if (!(window.opener && window.opener.appType == "sv")) {
                        window.opener.showSketchesInQC(null, sketchApp.CC_beforeSketch);
                        window.opener.showPreviousSketch('1');
                    }
                    $('.ccse-currentsketch').hide();
                    $('.ccse-beforesketch').show();
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });
        $('.ccse-delete-vector').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                if (tb.editor) {
                    messageBox(' Are you sure you want to delete the selected segment ?', ["OK", "Cancel"], function () {
                        let cv = tb.editor.currentVector;
                        if (!cv.newRecord && tb.editor.config?.DeleteAssociateWindow) {
                            tb.editor.config.DeleteAssociateWindow(tb.editor, cv);
                        }
                        else {
                            tb.editor.deleteCurrentVector();
                        }
                    }, function () {
                        return;
                    });
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });

        $('.ccse-copy-vector').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                if (tb.editor) {
                    tb.editor.copyCurrentVector();
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });
        $('.ccse-paste-vector').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                e.preventDefault();
                if (tb.editor) {
                    if (editor.currentVector) {
                        $(sketchApp.vectorSelector).val(''); //   While mid-Sketching
                        sketchApp.currentVector = null;
                        sketchApp.raiseModeChange();
                        sketchApp.renderAll();
                        sketchApp.mode = CAMACloud.Sketching.MODE_DEFAULT;
                        sketchApp.currentNode = null;
                    }
                    tb.editor.pasteVector();
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });

        $('.ccse-add-vector').bind(touchClickEvent, function (e) {
            e.preventDefault();
            //hideOrShowManualBox();
            try {
                if (tb.editor) {
                    if (editor.currentVector) {
                        $(sketchApp.vectorSelector).val(''); //   While mid-Sketching
                        sketchApp.currentVector = null;
                        sketchApp.raiseModeChange();
                        sketchApp.renderAll();
                        sketchApp.mode = CAMACloud.Sketching.MODE_DEFAULT;
                        sketchApp.currentNode = null;
                        sketchApp.currentLine = null;
                    }
                    tb.editor.createNewVector();
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });

        $('.ccse-add-segment').bind(touchClickEvent, function (e) {
            e.preventDefault();
            hideOrShowManualBox();
            try {
                if (tb.editor) {
                    tb.editor.createNewVectorSegment();
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });
        $('.ccse-transfer-vector').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                if (tb.editor) {
                    tb.editor.transferVector();
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });
        $('.ccse-delete-line').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                if (tb.editor && !$('.ccse-canvas').hasClass('dimmer')) {
                    if (sketchApp.currentVector.continuedrawing) {
                        messageBox("You are not allowed to delete the multiple line at a time")
                        return;
                    }
                    messageBox('Are you sure you would like to delete the selected line?', ['OK', 'Cancel'], function () {
                        undoProcessArray = [];
                        sketchApp.currentNode.nextNode.noStroke = true;
                        sketchApp.currentVector.continuedrawing = true;
                        sketchApp.currentNode = null;
                        tb.editor.render();
                        tb.editor.raiseModeChange();
                    });
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });
        $('.ccse-add-node-before').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                if (tb.editor) {
                    tb.editor.addNodeBeforeCurrent();
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });
        

        $('.ccse-save').click(function () {
            if (window.opener.__DTR &&  window.opener.DTRRoles.ReadOnly) {
                alert("You dont have permission to do this operation");
                return false;
                }
            try {
                hideOrShowManualBox();
                if (tb.editor && !tb.editor.saving) {
                    tb.editor.hideKeypad();
                    tb.editor.save(tb.editor.afterSave, tb.editor.beforeSave);
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });


        $('.ccse-close').bind(touchClickEvent, function (e) {
            e.preventDefault();
            hideOrShowManualBox();
            try {
                if (tb.editor) {
                    tb.editor.close();

                }
            } catch (e) {
                alert(e);
            }
            return false;
        });

        $('.ccse-flip-v').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                if (tb.editor) {
                    tb.editor.resetSketchMovable();
                    if (tb.editor.currentSketch)
                        tb.editor.currentSketch.flipVertical(tb.editor);
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });


        $('.ccse-flip-h').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                if (tb.editor) {
                    tb.editor.resetSketchMovable();
                    if (tb.editor.currentSketch)
                        tb.editor.currentSketch.flipHorizontal(tb.editor);
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });

        $('.ccse-sketch-move').bind(touchClickEvent, function (e) {
            e.preventDefault();
            hideOrShowManualBox();
            try {
                if (tb.editor) {
                    tb.editor.toggleSketchMovable();
                    if (tb.editor.sketchMovable) {
                        $('.ccse-sketch-move').html('Release');
                    } else {
                        $('.ccse-sketch-move').html('Move');
                    }
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });

        $('.ccse-download-jpg').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                imageDownload('jpg');
            } catch (e) {
                alert(e);
            }
            return false;
        });

        $('.ccse-download-png').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                imageDownload('png');
            } catch (e) {
                alert(e);
            }
            return false;
        });

        $('.ccse-undo-all').bind(touchClickEvent, function (e) {
            messageBox('Are you sure you want to discard all the changes', ['OK', 'Cancel'], function () {
                sketches = loadSketchSegments();
                sketchApp.refreshSketchesAfterSave(sketches)
            })
        })
        $('.ccse-close-all').bind(touchClickEvent, function (e) {
            tb.editor.currentSketch.vectors.forEach(function (v) {
                if (v.startNode && !v.isClosed && v.startNode.nextNode != v.endNode) {
                    v.terminateNode(v.startNode);
                    v.isModified = true;
                    v.close();
                }
            })
            editor.currentVector = null;
            editor.currentNode = null;
            editor.mode = CAMACloud.Sketching.MODE_DEFAULT;
            tb.editor.render();
            $('.ccse-option-list').hide();
            hideOrShowManualBox();
        })
        $('.ccse-notes-toggle').bind(touchClickEvent, function (e) {
            e.preventDefault();
            hideOrShowManualBox();
            try {
                if (tb.editor) {
                    tb.editor.displayNotes = !tb.editor.displayNotes;
                    if (tb.editor.displayNotes) {
                        $('.ccse-notes-toggle').html('Hide Notes');
                    } else {
                        $('.ccse-notes-toggle').html('Show Notes');
                    }
                    tb.editor.render();
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });
        $('.ccse-notes-edit').bind(touchClickEvent, function (e) {
            e.preventDefault();
            hideOrShowManualBox();
            try {
                if (tb.editor) {
                    var note = tb.editor.currentSketchNote;
                    var noteMaxLength = tb.editor.currentSketch.maxNotelen;
                    var disableNoteCharacters = tb.editor.config.DisableNoteCharacters;
                    input('', "Edit Note:", function (newnote) {
                        note.noteText = newnote;
                        note.isModified = true;
                        if (sketchApp && sketchApp.config && (sketchApp.config.IsJsonFormat || sketchApp.config.EnableVisionNotes || sketchApp.config.ModifyIfNoteChanged))
                            sketchApp.currentSketch.isModified = true;
                        tb.editor.render();
                    }, function () { }, note.noteText, 'textarea', true, noteMaxLength, { specialKeys: disableNoteCharacters });
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });
        $('.ccse-notes-delete').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                if (tb.editor) {
                    tb.editor.deleteCurrentSketchNote();
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });
        $('.ccse-notes-new').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                if (tb.editor) {
                    tb.editor.createNewSketchNote();
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });
        $('.ccse-edit-label').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                if (tb.editor) {

                    tb.editor.editCurrentVectorLabel();
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });
        $('.ccse-sqft-tool').bind(touchClickEvent, function (e) {
            e.preventDefault();
            var lines = -1; var sn = tb.editor.currentVector.startNode;
            while (sn != null) {
                lines += 1;
                sn = sn.nextNode;
            }
            if ((!tb.editor.currentVector.isClosed || lines != 4) && !(tb.editor.currentSketch?.config?.DrawPlaceHolder && tb.editor.currentVector.isUnSketchedArea)) {
                messageBox("The selected segment must have 4 sides to enter SQFT value.");
                return;
            }
            if (tb.editor && tb.editor.currentSketch.config.CustomToolForVector) {
                var con = tb.editor.currentSketch.config.CustomToolForVector.split(',')
                var property = con[1];
                var currentVlaue = tb.editor.currentVector[property] || '';
                input('Enter SQFT Value', 'SQFT', function (newValue) {
                    tb.editor.currentVector[property] = newValue;
                    tb.editor.currentVector.labelEdited = true;
                    tb.editor.currentVector.isModified = true;
                    tb.editor.render();
                }, function () { }, currentVlaue, { isSqftBox: true, maxLength: 10, type: 'Number' })
                return false;
            }
        })
        $('.ccse-move-label').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                if (tb.editor) {
                    messageBox('Tap on any point to move the selected vector label.', function () {
                        tb.editor.onMoveLabel = true;
                    });
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });

        $('.ccse-sketch-details').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                let parcel = window.opener ? window.opener.activeParcel : activeParcel,
                    getDataField = window.opener ? window.opener.getDataField : getDataField,
                    evalLookup = window.opener ? window.opener.evalLookup : evalLookup;

                if (tb.editor.currentVector) {
                    let vectorTable = tb.editor.currentVector.vectorConfig?.Table, htm = '<label style="display: block; text-align: center; font-size: 14px; margin-top: 10px; font-weight: bold;">' + sketchApp.currentVector.label + '</label>', rec = parcel[vectorTable].filter((r) => { return r.ROWUID == tb.editor.currentVector.uid })[0],
                        ss = [sketchSettings.CAMAFieldToDisplayOnSketch1, sketchSettings.CAMAFieldToDisplayOnSketch2, sketchSettings.CAMAFieldToDisplayOnSketch3, sketchSettings.CAMAFieldToDisplayOnSketch4, sketchSettings.CAMAFieldToDisplayOnSketch5];

                    ss.forEach((x) => {
                        if (x && x != '') {
                            let s = x.split("."), tbl = s[0], fld = s[1];
                            if (tbl == vectorTable && fld) {
                                let f = getDataField(fld, tbl);
                                if (f && rec) {
                                    let v = rec[fld], desc = v;
                                    if (f.InputType == '5') {
                                        desc = evalLookup(fld, v, null, f);
                                        desc = ((desc == '???') || (desc == '') || (desc == null)) ? v : desc;
                                    }
                                    htm += "<label style='display: block; font-size: 14px; margin-top: 10px;'>" + f.Label + " :  " + (desc ? desc : '') + "</label>";
                                }
                            }
                        }
                    });

                    $('.manual-entery-popup-content').html('').html(htm);
                    $('.manual-popup-title').html('Sketch Segment Details');
                    $('.ccse-mode-sketch-details').hide();
                    $('.manual-entry-container').show();

                    $('.manual-hide-button').unbind(touchClickEvent);
                    $('.manual-hide-button').bind(touchClickEvent, function () {
                        $('.manual-entry-container').hide();
                        $('.ccse-mode-sketch-details').css({
                            'display': "inline-block"
                        });
                    });
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });

        $('.ccse-manual-box').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                if (tb.editor.currentSketch) {
                    let htm = '', mSketchs = tb.editor.currentSketch.MSKetchArray;
                    mSketchs.forEach((msk, i) => {
                        let lbl = msk.msketchLabel.replace(/\{(.*?)\}/g, '');
                        let lookupdata = window.opener ? window.opener.lookup : lookup;
                        if (lookupdata.sketch_label_lookup[lbl]) lbl = lbl + '-' + lookupdata.sketch_label_lookup[lbl].Name;
                        htm += '<span class="msk" style="display: block;padding: 5px;"><label style="display: inline-block; width: 150px;">' + lbl + '</label><input style="margin-left: 20px;width: 50px;height: 25px;" class="msketchAreaChange" row="' + i + '" type="number" value= "' + msk.mSketchArea + '"/></span>';
                    });
                    $('.ccse-mode-manual-sketch').css({
                        'display': "none"
                    });
                    $('.manual-entery-popup-content').html('').html(htm);
                    $('.manual-popup-title').html('Manually Entered Areas');
                    $('.manual-entry-container').show();
                    $('.msketchAreaChange').bind('change', function (el) {
                        let vl = $(this).val(), ro = $(this).attr('row');
                        tb.editor.currentSketch.MSKetchArray[ro].mSketchArea = vl;
                        tb.editor.currentSketch.MSKetchArray[ro].modified = true;
                        tb.editor.currentSketch.isModified = true;
                    });
                    $('.manual-hide-button').unbind(touchClickEvent);
                    $('.manual-hide-button').bind(touchClickEvent, function () {
                        $('.manual-entry-container').hide();
                        $('.ccse-mode-manual-sketch').css({
                            'display': "inline-block"
                        });
                        return false;
                    });
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });

        $('.ccse-undo-segment').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                if (tb.editor && !$('.ccse-canvas').hasClass('dimmer')) {
                    if (tb.editor.currentVector && undoProcessArray.length > 0) {
                        var _lc = undoProcessArray.pop();
                        undoSegmentChanges(tb.editor, _lc.Action, _lc.NodeNumber, _lc.NodePoint, _lc.Node, _lc.Points, _lc.LabelPosition, _lc.AreaLabelPosition)
                        tb.editor.render();
                    }
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });

        $('.ccse-scale').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                if (tb.editor.mode != CAMACloud.Sketching.MODE_NEW)
                    boundaryScaleCalculation(tb.editor);
            } catch (e) {
                alert(e);
            }
            return false;
        });

        $('.ccse-party-wall').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                createPartyWall(tb.editor);
            } catch (e) {
                alert(e);
            }
            return false;
        });

        $('.ccse-add-page').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                rsSketchAddPage(tb.editor);
            }
            catch (e) {
                alert(e);
            }
            return false;
        });

        $('.ccse-delete-page').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                if (tb.editor.currentSketch) {
                    let rsPageDel = function () {
                        tb.editor.sketches = tb.editor.sketches.filter((x) => { return x.uid != tb.editor.currentSketch.uid });
                        if (!tb.editor.rsDeletedPages) tb.editor.rsDeletedPages = [];
                        if (!tb.editor.currentSketch.rsNewPage) tb.editor.rsDeletedPages.push(tb.editor.currentSketch);
                        tb.editor.currentSketch = tb.editor.currentVector = null;
                        tb.editor.refreshControlList();
                        $(tb.editor.sketchSelector)[0].selectedIndex = tb.editor.sketches.length > 0 ? 1 : 0;
                        $(tb.editor.sketchSelector).trigger('change');
                        tb.editor.mode = CAMACloud.Sketching.MODE_DEFAULT;
                        tb.editor.raiseModeChange();
                        tb.editor.render();
                        if (tb.editor.sketches?.length == 0) $('.ccse-add-page').addClass('ccse-add-page-highlight');
                    }

                    if (tb.editor.currentSketch.vectors.length > 0) {
                        messageBox('Warning! There are sketch segments that will also be deleted! ', ["Yes", "No"], function () {
                            rsPageDel();
                        }, function () { });
                    }
                    else rsPageDel();
                }
            }
            catch (e) {
                alert(e);
            }
            return false;
        });

        $('.ccse-create-newpage').on('click', function (e) {
            e.preventDefault();
            try {
                let createpg = function (pn, tp) {
                    let opt = document.createElement('option');
                    opt.innerHTML = tp;
                    opt.setAttribute('value', pn);
                    opt.setAttribute('selected', true);
                    editor.pageSelector[0].appendChild(opt);
                    $(editor.pageSelector).trigger('change');
                }

                if (editor.config?.IsWrightSketch) {
                    input('Select Sketch Type:', [{ Id: 'Dwell', Name: 'Dwelling' }, { Id: 'Com  ', Name: 'Commercial' }], function (table) {
                        let parcel = window.opener ? window.opener.activeParcel : activeParcel,
                            recs = parcel['CCV_DWELDAT_APEXSKETCH'].concat(parcel['CCV_COMDAT_APEXSKETCH'], (editor.createdWrightSketchCards ? editor.createdWrightSketchCards : [])), c = 0,
                            tbl = table == 'Dwell' ? 'CCV_DWELDAT_APEXSKETCH' : 'CCV_COMDAT_APEXSKETCH', pArr = [];

                        if ($('.ccse-page-select')[0]?.options?.length > 0) {
                            for (x in $('.ccse-page-select')[0].options) {
                                let el = $('.ccse-page-select')[0].options[x];
                                if (el.value && el.value != '0' && el.value != '' && !isNaN(el.value)) {
                                    pArr.push({ CARD: el.value });
                                }
                            }
                        }
                        recs = recs.concat(pArr);

                        c = recs.reduce((max, v) => Math.max(max, v.CARD || 0), c); c = c ? (c + 1) : 1;
                        if (!editor.createdWrightSketchCards) editor.createdWrightSketchCards = [];
                        editor.createdWrightSketchCards.push({ ROWUID: ccTicks(), CARD: c, SoureTable: tbl, CC_Deleted: false });
                        if (editor.pageSelector[0].childElementCount == 1 && editor.pageSelector[0].options[0]?.innerHTML == '-- None--') editor.pageSelector[0].innerHTML = '';
                        createpg(c, (table + ' - ' + c));
                    }, function () { });
                }
                else {
                    var pagecount = editor.currentSketch && editor.currentSketch.vectors ? editor.currentSketch.vectors.reduce((max, vector) => Math.max(max, vector.pageNum || 0), 0) : 0;
                    var childCount = editor.pageSelector[0] ? editor.pageSelector[0].childElementCount : 0;
                    if (pagecount == childCount) {
                        let pVal = ((pagecount + 1).toString());
                        createpg(pVal, pVal);
                    }
                }
            }
            catch (e) {
                alert(e);
            }
        });

        $('.ccse-delete-newpage').on('click', function (e) {
            e.preventDefault();
            try {
                let pageDelete = function () {
                    var selectedValue = $('.ccse-page-select').val();
                    var parentElement = editor.pageSelector[0];
                    var optionToRemove = parentElement.querySelector('option[value="' + selectedValue + '"]');
                    editor.pageSelector[0].removeChild(optionToRemove);

                    if (editor) {
                        var vectorsArray = editor.currentSketch.vectors;
                        editor.currentSketch.vectors = vectorsArray.filter(function (vector) {
                            return vector.pageNum != selectedValue;
                        });
                        if (editor.currentSketch.notes.length > 0) {
                            var notesArray = editor.currentSketch.notes;
                            editor.currentSketch.notes = notesArray.filter(function (notes) {
                                return notes.pageNum != selectedValue;
                            });
                        }

                        if (editor.config?.IsWrightSketch) {
                            if (sketchApp.createdWrightSketchCards?.filter((x) => { return x.CARD == selectedValue })?.length > 0) {
                                sketchApp.createdWrightSketchCards = sketchApp.createdWrightSketchCards.filter((x) => { return x.CARD != selectedValue });
                            }
                            else {
                                if (!editor.deletedWrightSketchCards) editor.deletedWrightSketchCards = [];
                                editor.deletedWrightSketchCards.push({ Card: selectedValue });
                            }
                        }

                        var selValue = selectedValue - 1;
                        if (editor.config?.IsWrightSketch && editor.pageSelector[0].options?.length > 0) {
                            selValue = editor.pageSelector[0].options[editor.pageSelector[0].options.length - 1].value;
                        }
                        var afterremove = parentElement.querySelector('option[value="' + selValue + '"]');
                        if (afterremove) afterremove.setAttribute('selected', true);
                        else if (editor.pageSelector[0].options?.length > 0) {
                            afterremove = editor.pageSelector[0].options[editor.pageSelector[0].options.length - 1];
                            afterremove.setAttribute('selected', true);
                        }
                        else if (editor.config?.IsWrightSketch) {
                            let opt = document.createElement('option');
                            opt.innerHTML = '-- None --';
                            opt.setAttribute('value', 0);
                            opt.setAttribute('selected', true);
                            editor.pageSelector[0].appendChild(opt);
                        }
                        $(editor.pageSelector).trigger('change');
                        if (editor.currentSketch) editor.currentSketch.isModified = true;
                        editor.render();
                    }
                }
                if (editor.currentSketch.vectors.filter((x) => { return x.pageNum == $('.ccse-page-select').val() })?.length > 0) {
                    messageBox('Warning! There are sketch segments that will also be deleted! ', ["Yes", "No"], function () {
                        pageDelete();
                    }, function () { });
                }
                else pageDelete();

            }
            catch (e) {
                alert(e);
            }

        });

        if (tb.editor) {
            tb.editor.setControlList('.ccse-sketch-select', '.ccse-vector-select', '.ccse-page-select');
           // $('.ccse-page-select').show();
            tb.editor.onModeChange = function (mode, currentVector, currentNode, currentSketchNote) {
                var selectedValue = $('.ccse-page-select').val();
                var lastValue = $('.ccse-page-select option:last').val();
                var shouldGrayOut = selectedValue !== String(lastValue) || ((selectedValue === '1' || selectedValue === '') && !tb.editor.config?.IsWrightSketch);
                if (tb.editor.config?.IsWrightSketch && selectedValue == 0 && lastValue == 0) shouldGrayOut = true;
                $('.ccse-delete-newpage').toggleClass('grayed-out', shouldGrayOut);

                var newGrayOut = selectedValue === '' ;
                $('.ccse-create-newpage').toggleClass('grayed-out', newGrayOut);


                $('.ccse-mode').hide();
                $('.colorPicker-picker').hide();
                $('.ccse-sketch-move').html('Move');
                var wasMovable = false;
                if (tb.editor.sketchMovable) {
                    wasMovable = true
                }
                tb.editor.sketchMovable = false;
                if (wasMovable)
                    tb.editor.render();

                $('.ccse-custom-buttons button').hide();
                $('.ccse-custom-buttons button').each(function (i, btn) {
                    var modes = $(btn).attr('ccse-modes');
                    var requires = $(btn).attr('ccse-requires');
                    var displayCondition = $(btn).attr('ccse-displayCondition');
                    var show = true;
                    if (modes) {
                        show = false;
                        if (modes.split(',').indexOf(mode.toString()) > -1) {
                            show = true;
                        }
                    }
                    if (show && requires) {
                        show = false;
                        var checkStops = ['currentNode', 'currentVector', 'currentSketchNote', 'currentSketch'];
                        for (var i in checkStops) {
                            var r = checkStops[i];
                            if (tb.editor[r] != null) {
                                if (r == requires) {
                                    show = true;
                                }
                                break;
                            }
                        }
                    }

                    if (displayCondition && show) {
                        var isDisp = eval(displayCondition);
                        show = isDisp ? isDisp : false;
                    }

                    if (show) {
                        $(btn).show();
                    }

                })
                if ($('.ccse-sketch-select :selected').val() == "")
                    $('.ccse-preview-sketch').hide()
                if (sketchApp.CC_beforeSketch) {
                    $('.ccse-save').hide();
                    //$('.ccse-vector-select').hide();
                    $('.ccse-custom-buttons button').hide();
                    $('.ccse-pan').hide();
                    $('.ccse-angle-lock').hide();
                    $('.ccse-currentsketch').hide();
                    $('.ccse-beforesketch').show();
                    if (tb.editor.currentSketch && tb.editor.currentVector && ((tb.editor.formatter.allowLabelEdit || tb.editor.currentSketch.config.AllowLabelEdit) && !tb.editor.currentVector.isFreeFormLineEntries)) {
                        $('.ccse-mode-select-vector-plus').css({ 'display': "inline-block" });
                        $('.ccse-edit-label').html('Edit Label');
                    }
                    if (tb.editor.currentSketch) {
                        if (tb.editor.currentSketch.notes.length > 0) {
                            $('.ccse-mode-notes').css({
                                'display': "inline-block"
                            });
                            if (tb.editor.displayNotes) {
                                $('.ccse-notes-toggle').html('Hide Notes');
                            } else {
                                $('.ccse-notes-toggle').html('Show Notes');
                            }
                        }
                    }
                    if ($('.ccse-sketch-select :selected').val() == "")
                        $('.ccse-preview-sketch').hide()
                    return;
                }
                else {
                    $('.ccse-currentsketch').show();
                    $('.ccse-beforesketch').hide();
                    if ($('.ccse-sketch-select :selected').val() == "")
                        $('.ccse-preview-sketch').hide()
                }
                if (window.opener && window.opener.appType == "sv" && (window.opener.lightWeight || (clientSettings && clientSettings["SketchValidationEditSketch"] != "1"))) {
                    $('.ccse-preview-sketch').hide();
                }

                if (CAMACloud.Sketching.MODE_DEFAULT != mode) {
                    $('.manual-entry-container').hide();
                }

                switch (mode) {
                    case CAMACloud.Sketching.MODE_NEW:
                        $('.ccse-mode-add-segment').css({
                            'display': "inline-block"
                        });
                        $('.ccse-mode-line').css({
                            'display': "inline-block"
                        });
                        $('.ccse-mode-select-vector').css({
                            'display': "inline-block"
                        });
                        //quickbutton tool button
                        $('.toolbar-openbtn-container').css({
                            'display': "inline-block"
                        });

                        if ((tb.editor.formatter.allowSegmentAddition || tb.editor.currentSketch.config.AllowSegmentAddition) && tb.editor.formatter.allowSegmentAdditionOnMidsketch)
                            $('.ccse-mode-select-sketch-plus').css({
                                'display': "inline-block"
                            });
                        /* if( tb.editor.config && tb.editor.config.EnableBoundary && editor.config.IsProvalConfig  && tb.editor.currentSketch && tb.editor.currentSketch.boundaryScale ){
                             $('.ccse-scale').val(tb.editor.currentSketch.boundaryScale);
                             $('.ccse-mode-scale').css({
                                 'display': "inline-block"
                             });
                         }*/
                        /*if (!tb.editor.formatter.disableCopyVector && (tb.editor.formatter.allowSegmentAddition || tb.editor.currentSketch.config.AllowSegmentAddition || tb.editor.currentSketch.config.AllowMultiSegmentAddDelete || tb.editor.currentSketch.config.AllowSegmentAdditionByCustomButtons))
                            $('.ccse-mode-copy-vector').css({
                                'display': "inline-block"
                            });*/
                        quicktoolbarUpdate();
                        undoProcessArray = [];
                        break;
                    case CAMACloud.Sketching.MODE_EDIT:
                        if (tb.editor.currentNode) {
                            if (tb.editor.currentVector && tb.editor.currentVector.disableMove) {
                                $('.ccse-mode-select-node').css({
                                    'display': "none"
                                });
                            }
                            else {
                                $('.ccse-mode-select-node').css({
                                    'display': "inline-block"
                                });
                            }

                            if (((sketchSettings && sketchSettings["EnableRotation"] == "1") || (clientSettings && clientSettings["EnableRotation"] == "1")) && tb.editor.rotationButtonEnabled) {
                                $('.sketch-pad .sp-left span').css("background-image", "url(../static/css/sketchpad/sp-left.png)");
                                $('.sketch-pad .sp-right span').css("background-image", "url(../static/css/sketchpad/sp-right.png)");
                                tb.editor.rotationButtonEnabled = false;
                            }

                            var FreeFormLine = (tb.editor.currentVector && tb.editor.currentVector.isFreeFormLineEntries) ? true : false;  // to check segment is free form line or not. If free form line then hide three options
                            if (!FreeFormLine && !tb.editor.currentVector.placeHolder) {
                                $('.ccse-delete-node').show();
                                $('.ccse-add-node-before').show();
                                $('.ccse-delete-line').show();
                            }
                        }
                        else if (tb.editor.currentLine) {
                            if (!tb.editor.currentLine.eNode.isEllipse && !tb.editor.currentLine.eNode.isArc) {
                                $('.ccse-mode-select-line').css({
                                    'display': "inline-block"
                                });
                            }
                        }
                        else if (tb.editor.currentVector) {
                            $('.ccse-edit-label').html('Edit Label')

                            var FreeFormLineEntry = tb.editor.currentVector.isFreeFormLineEntries;
                            if (tb.editor.currentVector.disableMove) {
                                $('.ccse-mode-select-node').css({
                                    'display': "none"
                                });
                            }
                            else {
                                $('.ccse-mode-select-node').css({
                                    'display': "inline-block"
                                });
                            }
                            $('.ccse-delete-node').hide();
                            $('.ccse-add-node-before').hide();
                            $('.ccse-delete-line').hide();

                            let configName = ((clientSettings && clientSettings.SketchConfig) || (sketchSettings && sketchSettings.SketchConfig));

                            if ((tb.editor.currentVector.isUnSketchedTrueArea && configName != 'MVP') || (tb.editor.currentVector.isUnSketchedArea && tb.editor.currentVector.sketchType == 'outbuilding'))
                                $('.ccse-edit-label').html('Edit/Draw')
                            if (tb.editor.currentSketch.config.AllowSketchColoring || tb.editor.formatter.AllowSketchColoring)
                                $('.colorPicker-picker').show();
                            if ((tb.editor.formatter.allowLabelEdit || tb.editor.currentSketch.config.AllowLabelEdit) && !FreeFormLineEntry)
                                $('.ccse-mode-select-vector-plus').css({
                                    'display': "inline-block"
                                });
                            if (tb.editor.currentSketch.config.AllowMultiSegmentAddDelete)
                                $('.ccse-mode-multi-segment').css({
                                    'display': "inline-block"
                                });
                            if (!tb.editor.currentVector.visionDeleted) {
                                $('.ccse-mode-select-vector').css({
                                    'display': "inline-block"
                                });
                            }
                            if (tb.editor.currentSketch.config.AllowTransferOut)
                                $('.ccse-mode-transfer-segment').css({
                                    'display': "inline-block"
                                });
                            if ((!tb.editor.formatter.disableCopyVector && (tb.editor.formatter.allowSegmentAddition || tb.editor.currentSketch.config.AllowSegmentAddition || tb.editor.currentSketch.config.AllowMultiSegmentAddDelete || tb.editor.currentSketch.config.AllowSegmentAdditionByCustomButtons)) && !FreeFormLineEntry)
                                $('.ccse-mode-copy-vector').css({
                                    'display': "inline-block"
                                });
                            if ((!tb.editor.currentSketch.config.DoNotAllowLabelMove && !tb.editor.formatter.DoNotAllowLabelMove) && !FreeFormLineEntry && (tb.editor.currentVector && !tb.editor.currentVector.disableMove) && !(tb.editor.config && tb.editor.config.isMVP && tb.editor.currentVector && tb.editor.currentVector.isUnSketchedTrueArea))
                                $('.ccse-mode-select-label-move').css({
                                    'display': "inline-block"
                                });
                            if (tb.editor.currentSketch.config.CustomToolForVector) {
                                var con = tb.editor.currentSketch.config.CustomToolForVector.split(',')
                                $('.ccse-mode-sqft-tool button').text(con[0])
                                $('.ccse-mode-sqft-tool').css({
                                    'display': "inline-block"
                                });
                            }
                            if (((sketchSettings && sketchSettings["EnableRotation"] == "1") || clientSettings && clientSettings["EnableRotation"] == "1")) {
                                $('.ccse-mode-rotate-sketch').css({
                                    'display': "inline-block"
                                });

                                $('.ccse-mode-quick-rotate').css({
                                    'display': "inline-block"
                                });
                            }

                            if (checkSketchDetailView(tb.editor.currentVector)) {
                                $('.ccse-mode-sketch-details').css({
                                    'display': "inline-block"
                                });
                            }

                            tb.editor.hideKeypad();
                        } else if (tb.editor.currentSketchNote) {
                            $('.ccse-mode-notes-edit').css({
                                'display': "inline-block"
                            });
                            $('.ccse-mode-notes-delete').css({
                                'display': "inline-block"
                            });
                            $('.ccse-mode-notes-add').css({
                                'display': "inline-block"
                            });
                        } else if (tb.editor.currentSketch) {
                            if (tb.editor.formatter.allowSegmentAddition || tb.editor.currentSketch.config.AllowSegmentAddition)
                                $('.ccse-mode-select-sketch-plus').css({
                                    'display': "inline-block"
                                });

                            $('.ccse-mode-select-sketch').css({
                                'display': "inline-block"
                            });

                            tb.editor.hideKeypad();
                        } else if (copyVector) {
                            $('.ccse-mode-paste-vector').css({
                                'display': "inline-block"
                            });
                        } else if (window.opener) {
                            if (window.opener.copyVector)
                                $('.ccse-mode-paste-vector').css({
                                    'display': "inline-block"
                                });
                        } else {
                            tb.editor.hideKeypad();
                        }

                        if (tb.editor.currentVector && (tb.editor.currentVector.isClosed || tb.editor.currentVector.isFreeFormLineEntries)) {
                            $('.ccse-mode-undo-segment').css({
                                'display': "inline-block"
                            });
                        } else {
                            $('.ccse-mode-undo-segment').hide();
                            undoProcessArray = [];
                        }

                        break;
                    case CAMACloud.Sketching.MODE_DEFAULT:
                        if (tb.editor.currentSketch && (!tb.editor.config?.IsWrightSketch || (tb.editor.config?.IsWrightSketch && tb.editor.currentSketch?.currentPage))) {
                            if ((tb.editor.currentSketch.config.NotesSource && tb.editor.external.fieldCategories.filter(function (u) {
                                return u.SourceTableName == tb.editor.currentSketch.config.NotesSource.Table
                            }).length > 0) || (tb.editor.config && (tb.editor.config.ApexFreeFormTextLabels || tb.editor.config.AddNewNote)))
                                $('.ccse-mode-notes-new').css({
                                    'display': "inline-block"
                                });
                            if (tb.editor.formatter.allowSegmentAddition || tb.editor.currentSketch.config.AllowSegmentAddition)
                                $('.ccse-mode-select-sketch-plus').css({
                                    'display': "inline-block"
                                });
                            if (tb.editor.formatter.allowSegmentAddition || tb.editor.currentSketch.config.AllowSegmentAddition || tb.editor.currentSketch.config.AllowMultiSegmentAddDelete || tb.editor.currentSketch.config.AllowSegmentAdditionByCustomButtons) {
                                if (tb.editor.currentVector && tb.editor.currentVector.startNode && !tb.editor.formatter.disableCopyVector)
                                    $('.ccse-mode-copy-vector').css({
                                        'display': "inline-block"
                                    });
                                if (copyVector)
                                    $('.ccse-mode-paste-vector').css({
                                        'display': "inline-block"
                                    });
                                if (window.opener) {
                                    if (window.opener.copyVector)
                                        $('.ccse-mode-paste-vector').css({
                                            'display': "inline-block"
                                        });
                                }
                            }

                            if (tb.editor.config?.enableManualAreaEntry && tb.editor.currentSketch.MSKetchArray?.length > 0 && $('.manual-entry-container').css('display') != 'block') {
                                $('.ccse-mode-manual-sketch').css({
                                    'display': "inline-block"
                                });
                            }
                            else {
                                $('.manual-entry-container').hide();
                            }

                            if (sketchEditorOpenFirstTime && tb.editor.config?.enableManualAreaEntry && tb.editor.currentSketch.MSKetchArray?.length > 0 && $('.manual-entry-container').css('display') != 'block') {
                                $('.ccse-manual-box').trigger('click');
                                sketchEditorOpenFirstTime = false;
                            }
                            //MA_2466
                            if (tb.editor.currentSketch.MSKetchArray?.length <= 0) {
                                sketchEditorOpenFirstTime = false;
                            }

                            $('.ccse-mode-select-sketch').css({
                                'display': "inline-block"
                            });
                            tb.editor.hideKeypad();
                        } else {
                            tb.editor.hideKeypad();
                        }

                        break;
                }

                if (tb.editor?.isInvalidSketchString) $('.ccse-save').hide();

                if (tb.editor.config && tb.editor.config.EnableBoundary && (editor.config.IsProvalConfig || editor.config.isMVP) && tb.editor.currentSketch && tb.editor.currentSketch.boundaryScale) {
                    $('.ccse-scale').val(tb.editor.currentSketch.boundaryScale);
                    $('.ccse-mode-scale').css({
                        'display': "inline-block"
                    });
                }

                if (tb.editor.currentSketch) {
                    let ntc = tb.editor.currentSketch.notes.filter((x) => { return !x.isDeleted });
                    if (ntc.length > 0 && !tb.editor.currentSketchNote) {
                        $('.ccse-mode-notes').css({
                            'display': "inline-block"
                        });
                        if (tb.editor.displayNotes) {
                            $('.ccse-notes-toggle').html('Hide Notes');
                        } else {
                            $('.ccse-notes-toggle').html('Show Notes');
                        }
                    }
                    else if (ntc.length > 0 && tb.editor.currentSketchNote && !tb.editor.displayNotes) {
                        $('.ccse-mode-notes').css({
                            'display': "inline-block"
                        });
                        $('.ccse-notes-toggle').html('Show Notes');
                    } else if (ntc.length > 0 && tb.editor.currentSketchNote && tb.editor.currentSketchNote.isDeleted) {
                        $('.ccse-mode-notes').css({
                            'display': "inline-block"
                        });
                        $('.ccse-notes-toggle').html('Hide Notes');
                    }
                }
                else {
                    $('.ccse-preview-sketch ').hide()
                }

            }
            tb.editor.onDrawLine = function (p1, p2, v) {
                var p3, p4, prevnode;
                //var d = sRound(Math.round(p1.distanceFrom(p2) / (DEFAULT_PPF * tb.editor.scale)));  FD_7421 length field in editor increase by 1
                var d = sRound((p1.distanceFrom(p2) / (DEFAULT_PPF * tb.editor.scale)));
                var dy = p2.y - p1.y;
                var dx = p2.x - p1.x;
                var measurement_coordinate = { x: p1.x + (dx / 2), y: p1.y + (dy / 2) }
                var angle = p1.getDirectionInDegrees(p2);
                $('.ccse-meter-length').val(d + (tb.editor.formatter.LengthUnit || DISTANCE_UNIT || ''));
                if (!v.isClosed) tb.editor.drawing.fillText(d, measurement_coordinate.x, measurement_coordinate.y);
                if (tb.editor.currentNode) {
                    p3 = tb.editor.currentNode.p;
                    prevnode = tb.editor.currentNode.prevNode
                }
                if (prevnode)
                    p4 = tb.editor.currentNode.prevNode.p;
                if (p3 && p4) {
                    var angle2 = p3.getDirectionInDegrees(p4);
                    var angle = (360 - angle2) - angle;
                    if (angle < 0) {
                        angle = 360 + angle
                    }
                    if (isNaN(angle))
                        $('.ccse-meter-angle').val('0°');
                    else
                        $('.ccse-meter-angle').val(parseInt(angle) + ' °');
                }
                /*if ( tb.editor.config && tb.editor.config.EnableBoundary && editor.config.IsProvalConfig && tb.editor.currentSketch ) {
                    var _scale = tb.editor.currentSketch.boundaryScale? tb.editor.currentSketch.boundaryScale: 100;
                    $('.ccse-scale').val(_scale);
                }*/
            }

            tb.editor.showStatus = function (message) {
                $('.sketch-label').html(message);
            }
            tb.editor.setZoomValue = function (z) {
                $('.ccse-zoom-value span').html(z);
                //   tb.editor.setZoom( z )
            }
        }

    }

    this.applyConfig = function () {
        if (tb.editor.config) {
            $('.ccse-custom-buttons').html('');
            tb.editor.config.sources.forEach(function (s, i) {
                if (s.Buttons && s.Buttons.length > 0) {
                    s.Buttons.forEach(function (b, j) {
                        var btn = $('<button/>');
                        $(btn).html(b.Text);
                        $('.ccse-custom-buttons').append(btn);
                        $(btn).attr('style', "margin-left: 6px");
                        if (b.Modes && b.Modes.length > 0) $(btn).attr('ccse-modes', b.Modes.map(function (m, k) { return CAMACloud.Sketching[m] }).join());
                        if (b.Requires) $(btn).attr('ccse-requires', b.Requires);
                        if (b.DisplayCondition) $(btn).attr('ccse-displayCondition', b.DisplayCondition);
                        if (b.Class) $(btn).addClass(b.Class);

                        $(btn).bind(touchClickEvent, function (e) {
                            e.preventDefault();
                            $('.ccse-option-list').hide();
                            try {
                                switch (b.Action) {
                                    case 'createNewVector':
                                        tb.editor.createNewVector(null, null, b.Data);
                                        break;
                                    case 'showOtherImpDescription':
                                        showOtherImpDescription(this, tb.editor, b.Data);
                                        break;
                                }

                            } catch (e) {
                                alert(e);
                            }
                            return false;
                        })
                    });


                }
            });
        }
    }

    function bindToolbarButton(buttonSelector, controlSelector) {
        $(buttonSelector, control).bind('mouseup touchend', function (event) {
            event.preventDefault();
            $('.control-window', control).hide();
            if (isActive(this)) {
                deactivate(this);
                if (controlSelector) $(controlSelector, control).hide();
                if (buttonSelector == '.ccse-angle-lock') tb.editor.angleLock = false;
            } else {
                $('.ccse-toolbutton', control).removeClass('ccse-tool-active');
                $('.ccse-toolbutton', control).addClass('ccse-tool-inactive');
                activate(this);
                if (controlSelector) $(controlSelector, control).show();
                if (buttonSelector == '.ccse-angle-lock') tb.editor.angleLock = true
            }

        });
    }

    control = $(selector);

    if (control.length == 0) {
        throw 'No toolbar found';
        return;
    }

    init();
}

function CCSketchPad(s, e) {
    var p = this;
    this.selector = s;
    this.editor = e;
    this.activeText = null;
    this.activeTextValue = 0;
    this.decimal = false;
    this.minus = false;
    this.activeTextDecimal = '';
    this.activeDecimalPoints = 0;
    var pos, ref, mov;
    var locked = false;
    function lockKeys() {
        locked = true;
        window.setTimeout(function () {
            locked = false;
        }, 200);
    }

    this.reset = function () {
        this.activeTextValue = 0;
        this.activeTextDecimal = '';
        this.activeDecimalPoints = 0;
        this.decimal = false;
        this.minus = false;
        if (_keyElement)
            _keyElement.lenth = 0;
        $('.box-length span', this.selector).text('0');
        $('.box-angle span', this.selector).text('0');
        $('.box-lengthR span', this.selector).text('0');
        $('.box-width span', this.selector).text('0');
        $('.sp-text-box', this.selector).removeAttr('selected');
        $('.box-length', this.selector).attr('selected', 'true');
    }

    this.reset = function () {
        this.activeTextValue = 0;
        this.activeTextDecimal = '';
        this.activeDecimalPoints = 0;
        this.decimal = false;
        this.minus = false;
        if (_keyElement)
            _keyElement.lenth = 0;
        $('.box-length span', this.selector).text('0');
        $('.box-angle span', this.selector).text('0');
        $('.box-lengthR span', this.selector).text('0');
        $('.box-width span', this.selector).text('0');
        $('.sp-text-box', this.selector).removeAttr('selected');
        $('.box-length', this.selector).attr('selected', 'true');
        this.activeText = $('.sp-text-box[selected]', this.selector)[0];
    }

    this.resetText = function () {
        this.activeTextValue = 0;
        this.activeTextDecimal = '';
        this.activeDecimalPoints = 0;
        this.decimal = false;
        this.minus = false;
        $('.sp-text-box[selected] span', this.selector).text('0');
    }

    this.sketchPadNumber = function (key, keyCall) {
        var intval = null;
        var maxValue = parseInt($(p.activeText).attr('max'));
        var minValue = parseInt($(p.activeText).attr('min'));
        switch (key) {
            case "CLEAR":
                p.resetText();
                return;
                break;
            case ".":
                p.decimal = true;
                break;
            case "-":
                if (minValue < 0)
                    p.minus = !p.minus;
                break;
            default:
                if (isFinite(key)) {
                    var a = p.activeTextValue;
                    var b = p.activeTextDecimal;
                    intval = parseInt(key);
                    if (!p.decimal) {
                        p.activeTextValue = (p.activeTextValue * 10) + intval;
                    } else {
                        p.activeTextDecimal = p.activeTextDecimal + intval.toString();
                    }
                    if (parseFloat((p.minus ? '-' : '') + p.activeTextValue + (p.activeTextDecimal != '' ? '.' + p.activeTextDecimal : '')) > maxValue) {
                        p.activeTextValue = a;
                        p.activeTextDecimal = b;
                    }
                    if (parseFloat((p.minus ? '-' : '') + p.activeTextValue + (p.activeTextDecimal != '' ? '.' + p.activeTextDecimal : '')) < minValue) {
                        p.activeTextValue = a;
                        p.activeTextDecimal = b;
                        p.minus = false;
                    }
                }
                break;
        }

        if (parseFloat((p.minus ? '-' : '') + p.activeTextValue + (p.activeTextDecimal != '' ? '.' + p.activeTextDecimal : '')) < minValue) {
            p.minus = false;
        }

        setTextValue(p.activeText, keyCall);
    }

    function getTextValue(txt) {
        var v = $('span', txt).text();
        p.activeTextValue = parseInt(v);
        p.activeTextDecimal = parseInt(v) == v ? '' : v.toString().split(".")[1];
        if (p.activeTextDecimal == '0') p.activeTextDecimal = '';
    }

    function setTextValue(txt, keycall) {
        var num = (p.minus ? '-' : '') + p.activeTextValue + (p.activeTextDecimal != '' ? '.' + p.activeTextDecimal : '');
        $('span', txt).text(num);
        if (keycall && _keyElement)
            _keyElement.lenth = num;
    }

    function distance() {
        return parseFloat($('.box-length span', this.selector).text())
    }
    function distance_R() {
        return parseFloat($('.box-lengthR span', this.selector).text()) + '/' + parseFloat($('.box-width span', this.selector).text())
    }

    function angle() {
        return parseFloat($('.box-angle span', this.selector).text())
    }

    function initKeys() {
        $('.box-lengthR', this.selector).removeAttr('selected');
        $('.box-width', this.selector).removeAttr('selected');
        var sel = this.selector;


        p.activeText = $('.sp-text-box[selected]', sel)[0];
        getTextValue(p.activeText);


        $('.sp-text-box', sel).bind(touchClickEvent, function (e) {
            e.preventDefault();
            $('.sp-text-box', sel).removeAttr('selected');
            $(this).attr('selected', 'true');
            p.decimal = $(this).text() && $(this).text().indexOf('.') > 0 ? true : false
            p.activeText = this;
            getTextValue(p.activeText);
        });

        $('.sketch-pad-num', sel).bind('touchend mouseup', function (e) {
            e.preventDefault();
            if (locked)
                return;
            lockKeys();
            var key = $(this).text();
            var tbx = $('.sp-text-box[selected]', sketchPad.selector).hasClass('box-length') ? true : false;
            p.sketchPadNumber(key, tbx);

        });

        $('.sketch-pad-cmd').bind('touchend mouseup', function (e) {
            e.preventDefault();
            if (locked)
                return;
            lockKeys();
            var cmd = $(this).attr('cmd');
            if (p.editor && cmd == 'RECT') {
                p.editor.processCmd(cmd, distance_R(), angle());
            }
            else if (p.editor) {
                p.editor.processCmd(cmd, distance(), angle());
            }
            p.reset();
        });

        ////////Quickbtn Functionality
        $('.ccse-quick-rotate').bind('click', function (e) {
            e.preventDefault();
            if (locked)
                return;
            lockKeys();
            var cmd = $(this).attr('cmd');
            if (p.editor) {
                sketchApp.rotationButtonEnabled = true;
                p.editor.processCmd(cmd, distance(), 10);
            }
            p.reset();

        });

        $('.rectanglebtn ').bind('click', function (e) {
            e.preventDefault();
            if (sketchApp.currentNode) {
                quicktoolbarUpdate();
                messageBox('You are not allowed to draw rectangle after creating first node')
                return;
            }
            else {
                if (locked)
                    return;
                _quickShape = {}
                _quickShape.shape = "RECT";
                _drawshape = true;

                lockKeys();
            }
        });


        $('.tool_convexarc ').bind('click', function (e) {
            e.preventDefault();
            if (!sketchApp.currentNode) {
                quicktoolbarUpdate();
                messageBox('You are allowed to draw Arc only after creating first node')
                return;
            }
            else {
                if (locked)
                    return;
                _quickShape = {}
                _quickShape.shape = "ARC";
                _quickShape.sign = -1;
                _drawshape = true;
                lockKeys();
            }
        });


        $('.tool_concavearc ').bind('click', function (e) {
            e.preventDefault();
            if (!sketchApp.currentNode) {
                quicktoolbarUpdate();
                messageBox('You are allowed to draw Arc only after creating first node')
                return;
            }
            else {
                if (locked)
                    return;
                _quickShape = {}
                _quickShape.shape = "ARC";
                _quickShape.sign = 1;
                _drawshape = true;

                lockKeys();
            }
        });
        $('.toolbox-control-hexbtn').bind('click', function (e) {
            e.preventDefault();
        });
        $('.toolbox-control-hexbtn').unbind('click', function (e) {
            e.preventDefault();
            $(".toolbox-control").css("visibility", "hidden");
        });

        $('.squrebtn').bind('click', function (e) {
            e.preventDefault();
            if (sketchApp.currentNode) {
                quicktoolbarUpdate();
                messageBox('You are not allowed to draw squre after creating first node')
                return;
            }
            else {
                if (locked)
                    return;
                _quickShape = {};
                _quickShape.shape = "POLY";
                _quickShape.edges = 4;
                _drawshape = true;
                lockKeys();
            }
        });

        $('#polysides').on('change', function () {
            _quickShape.edges = this.value;
        });


        $('.hexbtn').bind('click', function (e) {
            e.preventDefault();
            if (sketchApp.currentNode) {
                quicktoolbarUpdate();
                messageBox('You are not allowed to draw polygon after creating first node')
                return;
            }
            else {
                if (locked)
                    return;
                _quickShape = {}
                _quickShape.shape = "POLY";
                _quickShape.edges = $('#polysides').find(":selected").text();
                _drawshape = true;
                $('#polysides').prop('disabled', false);
                lockKeys();
            }
        });

        ///////quickbtn ends


        $('.movable-title-bar').bind('touchstart mousedown', function (e) {
            ref = getCoords();
            pos = $('.sketch-pad').position();
        });
        $('.movable-title-bar').bind('touchmove mousemove', function (e) {
            e.preventDefault();
            if (ref == null) return false;
            mov = getCoords();
            var x = pos.left + (mov.x - ref.x);
            var y = pos.top + (mov.y - ref.y);
            if (y < 20 && ((mov.y - ref.y) < 0)) return false;
            $('.sketch-pad').css({
                'top': y + 'px',
                'left': x + 'px'
            });
        });
        $('.movable-title-bar').bind('touchend touchcancel mouseup mouseleave', function (e) {
            ref = null;
            pos = null;
            mov = null;
        });

        $('.hide-sketch-pad').bind(touchClickEvent, function (e) {
            e.preventDefault();
            if (((sketchSettings && sketchSettings["EnableRotation"] == "1") || (clientSettings && clientSettings["EnableRotation"] == "1")) && sketchApp.rotationButtonEnabled) {
                $('.sketch-pad .sp-left span').css("background-image", "url(/App_Static/css/sketchpad/sp-left.png)");
                $('.sketch-pad .sp-right span').css("background-image", "url(/App_Static/css/sketchpad/sp-right.png)");
                sketchApp.rotationButtonEnabled = false;
            }
            if (p.editor) p.editor.hideKeypad();
            else $('.sketch-pad').hide();
        });

        var getCoords = function () {
            var p = new PointX(0, 0);
            if (event.touches) {
                var t = event.touches[0];
                if (!t)
                    t = event.changedTouches[0];
                p = new PointX(t.clientX - t.target.offsetLeft, t.clientY - t.target.offsetTop);
            } else {
                p = new PointX(event.clientX - event.target.offsetLeft, event.clientY - event.target.offsetTop);
            }
            return p;
        }


    }


    initKeys();

    if (p.editor) {
        p.editor.attachKeypad(this);
    }
}

function SketchHistory() {
    /*
    1. Move segment
    2. Move Sketch
    3. Move Node
    4. Add Node
    5. Delete Segment
    6. FlipV
    7. FlipH
    8. Add Point
    9. Arc Function
    10. Delete Node
    11. Add Node Before
    */
}

function setCanvasEvents(ccse, editor) {

    var refPoint;

    var sbl = 30
    var sx1 = sbl,
        sx2 = ccse.width - sbl;
    var sy1 = sbl,
        sy2 = ccse.height - sbl;

    $(ccse).bind('touchstart mousedown', function (event) {
        $(".toolbox-control").css("visibility", "hidden");
        if (alertLock)//|| sketchApp.CC_beforeSketch == true)
            return

        //quickbtn functionality start

        if (_drawshape && editor.currentVector && (!editor.currentVector.isClosed) && ((!editor.currentNode) || ((editor.currentNode) && (_quickShape.shape == "ARC")))) {

            _drawPoly = true;
            _drawshape = false;
            shape_coordinates.startX = event.pageX - this.offsetLeft;
            shape_coordinates.startY = event.pageY - this.offsetTop;
        }
        else {

            //quickbtn functionality ends
            sx2 = ccse.width - sbl;
            sy2 = ccse.height - sbl;
            event.preventDefault();
            var p = editor.getCoords();
            // Pen Down - for any action of touch
            editor.pen = true;
            var q = p.copy().alignToGrid(editor, Math.round);
            if (editor.currentNode && editor.currentVector && editor.currentVector.continuedrawing) {
                sketchApp.mode = 1
            }
            else if (editor.currentVector && editor.onMoveLabel == true) {
                var lbl_pos = _.clone(editor.currentVector.labelPosition);
                editor.currentVector.labelPosition = q.copy();
                editor.currentVector.isModified = true;
                editor.onMoveLabel = false;
                undoProcessArray.push({ Action: 'MoveLabel', NodeNumber: null, NodePoint: lbl_pos, Node: null });
                if (undoProcessArray.length > 20)
                    undoProcessArray.shift();
                editor.currentVector.labelEdited = true;
                editor.mode = CAMACloud.Sketching.MODE_EDIT
            } else if ((editor.currentSketchNote || editor.currentVector) && (editor.mode == CAMACloud.Sketching.MODE_EDIT)) {
                if (editor.currentVector && !(editor.currentVector.visketchType == 1)) {
                    editor.currentNode = null;
                    editor.currentLine = null;
                    refPoint = null;
                    //                if (editor.currentVector.placeHolder) {
                    //                    var rn = editor.currentVector.radiusNode;
                    //                    if (rn.overlaps(q, 1)) {
                    //                        editor.currentNode = rn;
                    //                    }
                    //                }
                    var sn = null;
                    if (!(editor.currentVector.disableMove))
                        sn = editor.currentVector.startNode;
                    var nearest = null;
                    var nearestDistance = 9999;
                    var i = 0;
                    while (sn != null) {
                        var d = Math.sqrt(Math.pow(sn.p.x - q.x, 2) + Math.pow(sn.p.y - q.y, 2));
                        if (sn.overlaps(q)) {
                            editor.currentNode = sn;
                            editor.currentLine = null;
                            nodeNumber = i;
                            break;
                        } else if (sn.overlaps(q, 1)) {
                            if (d < nearestDistance) {
                                nearestDistance = d;
                                nearest = sn;
                                nodeNumber = i;
                            }
                        }
                        sn = sn.nextNode;
                        i++;
                    }
                    if ((editor.config?.EnableLineSelection || ((editor.currentVector?.vectorConfig?.EnableLineSelection) && (editor.currentSketch?.parentRow?.RorC == 'C'))) && !editor.currentNode && !nearest && !editor.currentVector.placeHolder) {
                        editor.currentVector.lineSelection(p, editor);
                    }
                }

                if (editor.currentLine && editor.currentVector && !editor.currentNode) {
                    refPoint = q;
                }
                else if (!editor.currentNode && nearest) {
                    editor.currentNode = nearest;
                } else if (editor.currentVector && !editor.currentNode && editor.currentVector.contains(q)) {
                    if (editor.config && editor.config.EnableSpecialRounding) // in EnableSpecialRounding property enabled config, if a node selected and move vector then q value is not in rounded foramt point in node keeps.So in move startnode rounded. 
                        q = p.copy().alignToGrid(editor, Math.round);
                    refPoint = q;
                } else if (editor.currentSketch) {
                    // NEW FUNCTIONALITY BEGIN
                    var current_Page = null;
                    if (editor.config && editor.config.IsJsonFormat && editor.currentSketch.currentPage)
                        current_Page = editor.currentSketch.currentPage;
                    var selectedNote = null, noteArray = [];
                    if (current_Page)
                        noteArray = editor.currentSketch.notes.filter((x) => { return x.pageNum == current_Page && !x.isDeleted });
                    else
                        noteArray = editor.currentSketch.notes.filter((x) => { return !x.isDeleted });
                    noteArray.forEach(function (nt) {
                        if (!selectedNote && nt.contains(q)) {
                            editor.currentSketchNote = nt;
                            selectedNote = nt;
                            refPoint = q;
                        }
                    });
                    if (!selectedNote) {
                        editor.currentSketchNote = null;
                        editor.raiseModeChange();
                        //refPoint = p;
                    }
                    var selectedVector = null, vectorArray = [];
                    if (current_Page)
                        vectorArray = editor.currentSketch.vectors.filter(function (x) { return x.pageNum == current_Page });
                    else
                        vectorArray = editor.currentSketch.vectors;
                    vectorArray.forEach(function (v) {
                        if (!selectedNote && !selectedVector && v.contains(q)) {
                            var _in1 = editor.currentVector ? editor.currentVector.index : null, _in2 = v.index;
                            editor.currentVector = v;
                            $(editor.vectorSelector).val(v.uid);
                            selectedVector = v;
                            if (_in1 != _in2)
                                undoProcessArray = [];

                            if (v.isClosed || v.isFreeFormLineEntries) {
                                refPoint = q;
                            } else {
                                editor.promptAndContinueVector(function () {
                                    editor.render();
                                });
                            }
                        }
                    });
                    if (!selectedVector) {
                        $(editor.vectorSelector).val('');
                        editor.currentVector = null;
                        editor.raiseModeChange();
                        //  refPoint = p;
                    }
                    if (!selectedVector && !selectedNote) {
                        refPoint = p;
                        editor.mode = CAMACloud.Sketching.MODE_DEFAULT;
                    }
                    // NEW FUNCTIONALITY ENDS
                }
            } else if (editor.mode == CAMACloud.Sketching.MODE_DEFAULT) {
                if (editor.sketchMovable) {
                    refPoint = q;
                } else {
                    // NEW FUNCTIONALITY BEGIN
                    if (editor.currentSketch) {
                        var current_Page = null;
                        if (editor.config && editor.config.IsJsonFormat && editor.currentSketch.currentPage)
                            current_Page = editor.currentSketch.currentPage;
                        var selectedNote = null, noteArray = [];
                        if (current_Page)
                            noteArray = editor.currentSketch.notes.filter((x) => { return x.pageNum == current_Page && !x.isDeleted });
                        else
                            noteArray = editor.currentSketch.notes.filter((x) => { return !x.isDeleted });
                        noteArray.forEach(function (nt) {
                            if (!selectedNote && nt.contains(q)) {
                                editor.currentSketchNote = nt;
                                selectedNote = nt;
                                refPoint = q;
                                editor.mode = CAMACloud.Sketching.MODE_EDIT;
                            }
                        });
                        if (!selectedNote) {
                            refPoint = p;
                            editor.currentSketchNote = null;
                            editor.raiseModeChange();
                        }
                        if (editor.currentSketchNote)
                            return;

                        var selectedVector = null, vectorArray = [];
                        if (current_Page)
                            vectorArray = editor.currentSketch.vectors.filter(function (x) { return x.pageNum == current_Page });
                        else
                            vectorArray = editor.currentSketch.vectors;
                        vectorArray.forEach(function (v) {
                            if (!selectedNote && !selectedVector && v.contains(q)) {
                                editor.currentVector = v;
                                $(editor.vectorSelector).val(v.uid);
                                selectedVector = v;
                                undoProcessArray = [];

                                if (v.isClosed || v.isFreeFormLineEntries) {
                                    refPoint = q;
                                    editor.render();
                                    editor.mode = CAMACloud.Sketching.MODE_EDIT;
                                    editor.raiseModeChange();
                                } else {
                                    editor.promptAndContinueVector(function () {
                                        editor.render();
                                    });
                                }
                            }
                        });
                        if (!selectedVector) {
                            refPoint = p;
                        }
                    }

                    // NEW FUNCTIONALITY ENDS
                    //refPoint = p;
                }

            } else if (editor.currentSketchNote && (editor.mode == CAMACloud.Sketching.MODE_EDIT)) { }
        }
    });

    if (windowsTouch == true && Hammer)
        enablePinch();
    $(ccse).bind('touchmove mousemove', function (event) {

        //Quickbtn Polygon dragging

        if (_drawPoly) {
            var ctx = editor.drawing
            ctx.setLineDash([6]);
            shape_coordinates.endX = event.pageX - this.offsetLeft;
            shape_coordinates.endY = event.pageY - this.offsetTop;
            shape_coordinates.midX = (shape_coordinates.endX + shape_coordinates.startX) / 2;
            shape_coordinates.midY = (shape_coordinates.endY + shape_coordinates.startY) / 2;
            var radius = shape_coordinates.radius = Math.sqrt(Math.pow((shape_coordinates.endX - shape_coordinates.startX), 2) + Math.pow((shape_coordinates.endY - shape_coordinates.startY), 2)) / 2;
            ctx.clearRect(0, 0, ccse.width, ccse.height);
            editor.render();
            if (!(editor.currentNode)) {
                if (_quickShape.shape == "RECT")
                    _rectangle_draw(editor, shape_coordinates);
                else if (_quickShape.shape == "POLY")
                    _polygon(editor, shape_coordinates, shape_coordinates.startX, shape_coordinates.startY, shape_coordinates.radius, _quickShape.edges, 0);
            }
            else if (editor.currentNode && _quickShape.shape == "ARC") {
                if (editor.currentNode && editor.currentVector) {
                    var endNodeCoords = getCanvasPoint(sketchApp.currentVector.endNode.p, sketchApp.scale);
                    drawArc(editor, _arclen * _quickShape.sign, endNodeCoords, { x: shape_coordinates.endX, y: shape_coordinates.endY });
                }
            }
            return false;
        }
        //Quickbtn functionality ends

        event.preventDefault();
        var p = editor.getCoords();

        if (editor.pen) {

            if (editor.mode != CAMACloud.Sketching.MODE_DEFAULT) {
                if (p.x < sx1) {
                    editor.autoPan("R", false, p.x - sx1);
                } else {
                    editor.autoPan("R", true);
                }
                if (p.x > sx2) {
                    editor.autoPan("L", false, p.x - sx2);
                } else {
                    editor.autoPan("L", true);
                }
                if (p.y < sy1) {
                    editor.autoPan("D", false, p.y - sy1);
                } else {
                    editor.autoPan("D", true);
                }
                if (p.y > sy2) {
                    editor.autoPan("U", false, p.y - sy2);
                } else {
                    editor.autoPan("U", true);
                }
            }
            if ((editor.mode == CAMACloud.Sketching.MODE_NEW)) {
                //if (!editor.keypadActive) {fd-4741
                var q = p.copy().alignUnit(editor, Math.round);
                editor.render(q, {
                    moving: false,
                    cursor: true
                });
                // }
            } else if ((editor.currentNode || refPoint) && (editor.mode == CAMACloud.Sketching.MODE_EDIT)) {
                if (!_moving) {
                    if (editor.currentNode && editor.currentVector && (editor.currentVector.isClosed || editor.currentVector.isFreeFormLineEntries)) {
                        if (editor.currentVector.isUnSketchedArea || editor.currentVector.isUnSketchedTrueArea)
                            undoProcessArray.push({ Action: 'NodeMove', NodeNumber: nodeNumber, NodePoint: _.clone(editor.currentNode.p), Node: _.clone(editor.currentNode), LabelPosition: _.clone(editor.currentVector.labelPosition), AreaLabelPosition: _.clone(editor.currentVector.AreaLabelPosition) });
                        else
                            undoProcessArray.push({ Action: 'NodeMove', NodeNumber: nodeNumber, NodePoint: _.clone(editor.currentNode.p), Node: _.clone(editor.currentNode) });
                    }
                    else if (editor.currentVector && (editor.currentVector.isClosed || editor.currentVector.isFreeFormLineEntries))
                        undoProcessArray.push({ Action: 'SketchMove', NodeNumber: null, NodePoint: _.clone(editor.currentVector.startNode.p), Node: _.clone(editor.currentVector.startNode.p), LabelPosition: _.clone(editor.currentVector.labelPosition), AreaLabelPosition: _.clone(editor.currentVector.AreaLabelPosition) });
                    if (undoProcessArray.length > 20)
                        undoProcessArray.shift();
                    _moving = true;
                }
                var moving = false;
                var showCursor = false;
                var q = p.copy().alignToGrid(editor, Math.round);
                if (editor.currentNode) {
                    var d = new PointX(editor.currentNode.p.x - q.x, editor.currentNode.p.y - q.y);
                    editor.currentNode.moveTo(q);
                    if (editor.currentVector && editor.currentVector.placeHolder) {
                        editor.currentVector.labelPosition.x -= d.x;
                        editor.currentVector.labelPosition.y -= d.y;
                    }
                    //                    if (editor.currentVector && editor.currentVector.placeHolder) {
                    //                        if (editor.currentVector.radiusNode == editor.currentNode) {
                    //                            editor.currentVector.placeHolderSize = sRound(editor.currentNode.p.getDistanceTo(editor.currentVector.startNode.p));
                    //                        }
                    //                    }
                    if (editor.currentNode.nextNode) {
                        if (editor.currentNode.nextNode.isEllipse) {
                            var t = editor.currentNode.nextNode.p;
                            var a = editor.currentNode.nextNode.nextNode.p;
                            var r = new PointX(t.x + q.x - a.x, t.y + q.y - a.y);
                            editor.currentNode.nextNode.nextNode.moveTo(q);
                            editor.currentNode.nextNode.moveTo(r);
                        }
                    }
                    //                    if (editor.currentNode == editor.currentVector.endNode) {
                    //                        if (!editor.currentNode.overlaps(editor.currentVector.startNode.p)) {
                    //                            editor.currentVector.terminateNode(editor.currentVector.startNode);
                    //                        }
                    //                    }
                    //                    if (editor.currentNode == editor.currentVector.startNode) {
                    //                        if (!editor.currentNode.overlaps(editor.currentVector.endNode.p)) {
                    //                            editor.currentVector.terminateNode(editor.currentVector.startNode);
                    //                        }
                    //                    }
                    showCursor = true;
                }
                else if (editor.currentLine && !editor.currentLine.eNode.isEllipse) {
                    return;
                    //var d = new PointX(refPoint.x - q.x, refPoint.y - q.y);
                    //var sn = editor.currentLine.sNode;
                    //sn.p.x = Number((sn.p.x - d.x).toFixed(2));
                    //sn.p.y = Number((sn.p.y - d.y).toFixed(2));
                    //var en = editor.currentLine.eNode;
                    //en.p.x = Number((en.p.x - d.x).toFixed(2));
                    //en.p.y = Number((en.p.y - d.y).toFixed(2));
                    //editor.currentVector.startNode.recalculateAll();
                    //refPoint = q;
                    //lineMove = true;
                }
                else if (refPoint) {
                    //Move the entire segment when no point is selected.
                    var d = new PointX(refPoint.x - q.x, refPoint.y - q.y);
                    if (editor.currentVector) {
                        if (!editor.currentVector.disableMove) {
                            if (editor.config && editor.config.EnableSpecialRounding)
                                editor.currentVector.moveBy(d, null, true);
                            else
                                editor.currentVector.moveBy(d);
                        }
                    } else if (editor.currentSketchNote) {
                        editor.currentSketchNote.moveBy(d);
                    }
                    refPoint = q;
                    moving = true;
                }
                editor.render(p, {
                    moving: moving,
                    cursor: showCursor
                });
                if (editor.currentVector && !editor.currentVector.disableMove) editor.currentVector.isModified = true;
            } else if (editor.mode == CAMACloud.Sketching.MODE_DEFAULT) {
                // Pan Canvas when no segment is selected.
                if (editor.sketchMovable) {
                    var q = p.copy().alignToGrid(editor, Math.round); //Rounding of point to nearest foot when Touch/Click is used.
                    var d = new PointX(refPoint.x - q.x, refPoint.y - q.y);
                    editor.currentSketch.vectors.forEach(function (v) {
                        if (!v.disableMove)
                            v.moveBy(d, null, true);
                    });

                    refPoint = q;
                } else {
                    if (refPoint) editor.pan(p.x - refPoint.x, refPoint.y - p.y);
                    refPoint = p;
                }

                editor.render(null, {
                    moving: true
                });
            }
        }
    });

    $(ccse).bind('touchend mouseup', function (event) {
        var raiseModeChange = true;
        event.preventDefault();
        editor.clearAutoPanning();
        var p = editor.getCoords();


        //QUICKBTN FUNCTIONALITY  START
        if (_drawPoly && ((!editor.currentNode) || ((editor.currentNode) && (_quickShape.shape == "ARC"))) && editor.currentVector && !(editor.currentVector.isClosed)) {
            _drawPoly = false;
            if (_quickShape.shape == 'RECT' || _quickShape.shape == 'POLY') {
                _polycoorinates.forEach(function (_plc) {
                    var _pc = new PointX(_plc.x, _plc.y);
                    var qc = _pc.copy().alignUnit(editor);
                    var cpc = qc.copy().alignToGrid(editor, function (x) {
                        return x;
                    });

                    editor.render(qc, {
                        moving: false,
                        cursor: true
                    });
                    editor.registerNode(qc);
                });
                _drawPoly = false
            }
            else if (_quickShape.shape == 'ARC') {
                if (_arcCoorinates[1]) {
                    _Arc = _arcCoorinates[1];
                    var _pca = new PointX(_Arc.x, _Arc.y);
                    var qca = _pca.copy().alignUnit(editor, Math.round);

                    editor.render(qca, {
                        moving: false,
                        cursor: true
                    });
                    editor.registerNode(qca);
                    if (editor.currentNode || editor.currentVector.isClosed) {
                        if (editor.currentVector.isClosed)
                            editor.currentNode = editor.currentVector.endNode;
                        editor.currentNode.isArc = (_arclen != 0);
                        editor.currentNode.arcLength = _arclen * _quickShape.sign;
                        editor.currentNode.recalculateAll();
                        _arcCoorinates = {}
                    }
                }
            }
            editor.render();
            if (raiseModeChange)
                editor.raiseModeChange();
        }

        ////quickbtn ends

        // Pen Down - for any action of touch
        if (editor.pen) {
            if (_moving)
                _moving = false;
            if (editor.mode == CAMACloud.Sketching.MODE_NEW) {
                // if (!editor.keypadActive) {
                //QuickButton
                var p = editor.getCoords();
                //
                var q = p.copy().alignUnit(editor, Math.round);
                var cp = q.copy().alignToGrid(editor, function (x) {
                    return x;
                });
                if (editor.currentNode && editor.currentVector.continuedrawing) {
                    var closeVector = editor.currentVector.continueVector(cp);
                    undoProcessArray = [];
                    refPoint = null;
                    sketchApp.render();
                    editor.pen = false;
                    sketchApp.mode = 2
                    return
                }
                editor.render(q, {
                    moving: false,
                    cursor: true
                });
                if (editor.currentNode && editor.angleLock) {
                    //var cp = q.copy().alignToGrid(editor, function(x) {
                    //     return x;
                    //});
                    var np = editor.currentNode.p;
                    var angle = np.getDirectionInDegrees(cp);
                    var d = "";
                    var length = np.getDistanceTo(cp);
                    length = Math.round(length)
                    var overlaplimit = 0.9;
                    if (editor.currentVector.startNode.overlaps(cp, overlaplimit) && angle % 90 == 0)
                        editor.processCmd("OK")
                    else {
                        editor.processCmd("+", null, null);
                        if (angle >= 45 && angle <= 135) editor.processCmd("D", length);
                        if (angle >= 135 && angle <= 225) editor.processCmd("L", length);
                        if (angle >= 225 && angle <= 315) editor.processCmd("U", length);
                        if (angle > 315 || angle <= 45) editor.processCmd("R", length);
                    }
                } else {
                    editor.registerNode(q);
                    if (editor.drawlabelOnly4x4) {
                        editor.processCmd('+');
                        editor.processCmd("U", 4);
                        editor.processCmd('+');
                        editor.processCmd("R", 4);
                        editor.processCmd('+');
                        editor.processCmd("D", 4);
                        editor.processCmd("OK")
                        editor.drawlabelOnly4x4 = false;
                    }
                    else if (editor.drawlabelOnly) {
                        editor.currentVector.placeHolder = true;
                        editor.currentVector.placeHolderSize = Number(2);
                        editor.currentVector.terminateNode(editor.currentVector.startNode);
                        editor.currentVector.isClosed = true;
                        editor.currentVector.ignoreFromSave = true;
                        editor.mode = 2;
                        editor.drawlabelOnly = false;
                        editor.currentNode = null;
                        editor.currentLine = null;
                        editor.render()
                    }
                    else if (editor.drawfixedArea) {
                        editor.processCmd('+');
                        editor.processCmd("U", 10);
                        editor.processCmd('+');
                        editor.processCmd("R", 10);
                        editor.processCmd('+');
                        editor.processCmd("D", 10);
                        editor.processCmd("OK")
                        editor.drawfixedArea = false;
                    }
                }
                if (editor.currentVector) { editor.currentVector.isModified = true; undoProcessArray = []; };
                //}
            } else if (editor.mode == CAMACloud.Sketching.MODE_EDIT) {
                //editor.currentNode = null;
                refPoint = null;
                editor.render();
                if ((!editor.displayNotes && !editor.currentVector && !editor.currentNode) || (editor.currentSketchNote && editor.currentSketchNote.isDeleted))
                    editor.mode = CAMACloud.Sketching.MODE_DEFAULT;

            } else if (editor.mode == CAMACloud.Sketching.MODE_DEFAULT) {
                if (editor.sketchMovable) {
                    raiseModeChange = false;
                } else {

                }
                refPoint = null;
                editor.render();
            }
        }
        if (raiseModeChange)
            editor.raiseModeChange();
        editor.pen = false;
    });

    $(ccse).bind('touchcancel mouseleave', function (event) {
        var p = editor.getCoords();
        event.preventDefault();
        // Pen Down - for any action of touch
        editor.pen = false;

    });

    $(ccse).bind('mousewheel', function (e, d) {
        var ev = e;
        if (e.originalEvent)
            ev = e.originalEvent;
        var delta = ev.wheelDeltaY;
        var dd = (delta < 0 ? -0.05 : delta > 0 ? 0.05 : 0);
        var tscale = isNaN(editor.scale) ? 1 : editor.scale;
        editor.scale = Math.round((tscale + dd) * 100) / 100;
        var px = new PointX(ev.offsetX, ev.offsetY)
        editor.zoom(editor.scale, px);
        var percent = Math.round((editor.scale - editor.minZoom) / editor.zoomRange * 100);
        editor.setZoom(percent);
    });

    var refScale = null;
    $(ccse).bind('gesturestart', function (event) {
        event.preventDefault();
        refScale = editor.scale;
    });

    $(ccse).bind('gestureend', function (event) {
        event.preventDefault();
        refScale = null;
    });

    $(ccse).bind('gesturechange', function (event) {
        try {
            event.preventDefault();
            editor.scale = editor.normalizeScale(Math.round((refScale * Math.round(Math.pow(event.scale, 2) * 100) / 100) * 100) / 100);
            editor.zoom(editor.scale, null);

            //Calculate Percentage for the slider control.
            var percent = Math.round((editor.scale - editor.minZoom) / editor.zoomRange * 100);
            editor.setZoom(percent);
        } catch (e) {
            editor.showStatus(e);
        }
    });

}

function enablePinch() {
    if (!sketchApp) return;
    editor = sketchApp;
    var mc = new Hammer(document.getElementById('sketch-canvas'));
    mc.get('pinch').set({
        enable: true
    });
    var scale, zoom;
    mc.on('pinchstart', function (ev) {
        ev.preventDefault();
        refScale = editor.scale;
    });
    mc.on('pinch', function (ev) {
        if (refScale) {
            try {
                editor.scale = editor.normalizeScale(Math.round((refScale * Math.round(Math.pow(ev.scale, 2) * 100) / 100) * 100) / 100);
                editor.zoom(editor.scale, null);

                //var delta = (scale - ev.scale);
                //map.setZoom(Math.round(zoom - delta));

                //Calculate Percentage for the slider control.
                var percent = Math.round((editor.scale - editor.minZoom) / editor.zoomRange * 100);
                editor.setZoom(percent);
            } catch (e) {
                editor.showStatus(e);
            }
        }
    });
}

/*

Utility Functions

*/

CAMACloud.Sketching.Utilities = {}

splitString = function (string, splitters) {
    var list = [string];
    for (var i = 0, len = splitters.length; i < len; i++) {
        traverseList(list, splitters[i], 0);
    }
    return flatten(list);
}

traverseList = function (list, splitter, index) {
    if (list[index]) {
        if ((list.constructor !== String) && (list[index].constructor === String))
            (list[index] != list[index].split(splitter)) ? list[index] = list[index].split(splitter) : null;
        (list[index].constructor === Array) ? traverseList(list[index], splitter, 0) : null;
        (list.constructor === Array) ? traverseList(list, splitter, index + 1) : null;
    }
}

flatten = function (arr) {
    return arr.reduce(function (acc, val) {
        return acc.concat(val.constructor === Array ? flatten(val) : val);
    }, []);
}

function sRound(v) {
    if (CAMACloud.Sketching.roundFactor == null) {
        var dp = clientSettings['SketchDecimals'] || sketchSettings['SketchDecimals'];
        if ((dp == null) || (dp === undefined)) {
            dp = 0;
        }
        if (isNaN(dp)) {
            dp = 0;
        } else if (dp > 4) {
            dp = 4;
        } else if (dp < 0) {
            dp = 0;
        }
        CAMACloud.Sketching.roundFactor = Math.pow(10, dp);
    }

    var rv
    if (CAMACloud.Sketching.roundFactor == 1)
        rv = Math.round(v * CAMACloud.Sketching.roundFactor) / CAMACloud.Sketching.roundFactor;
    else
        rv = parseInt(v * CAMACloud.Sketching.roundFactor) / CAMACloud.Sketching.roundFactor;
    return rv;


    //return Math.round(v * CAMACloud.Sketching.roundFactor) / CAMACloud.Sketching.roundFactor;

}

function sign(x) {
    return x < 0 ? -1 : 1;
}

CAMACloud.Sketching.Utilities.toPixel = function (x) {
    return isNaN(x) ? 0 : Math.round(Number(x) * (DEFAULT_PPF * 10000)) / 10000;
}

CAMACloud.Sketching.Utilities.fromPixel = function (x) {
    return x / DEFAULT_PPF;
}
/*function labeltextlength(labelValue){
    if(sketchSettings['LabelMaxLimit']){
        var labelLength = parseInt(sketchSettings['LabelMaxLimit']);
        if( labelValue.length <= labelLength-1)
            return true;
        else
            return false;			
    }
}*/

function multipleInput(head, items, lookups, callback, editor, type, option) {
    var labelConfig = sketchApp.config.LabelConfig || editor.currentSketch.config.LabelConfig;
    if (sketchApp.CC_beforeSketch)
        $('.Current_vector_details #Btn_Save_vector_properties').attr('disabled', 'disabled');
    else
        $('.Current_vector_details #Btn_Save_vector_properties').removeAttr('disabled');

    if (labelConfig && option.type != 'outBuilding') {
        labelConfig(head, items, lookups, callback, editor, type);
        return;
    }
    else if (option.type == 'outBuilding') {
        editor.config.OutBuildingDefinition(null, {}, { type: 'outBuilding', isEdit: option.isEdit, callback: callback, head: head, items: items, action: type });
        return;
    }

    var disableFirstRecordlabel = (editor.currentSketch.config.DoNotAddOrEditFirstRecordlabel && (editor.currentSketch.vectors.length == 0 || (editor.currentVector && editor.currentVector.index == 0) || (type == 'edit' && editor.currentVector && editor.currentVector.index == 1 && (editor.currentVector == editor.currentSketch.vectors[0])))) ? true : false;
    if ((disableFirstRecordlabel || editor.config.IsLabelAssociate) && sketchApp.config.LabelAssociateWindow) {
        sketchApp.config.LabelAssociateWindow(head, items, lookups, callback, editor, type);
        return;
    }

    if (editor.currentSketch.config && editor.currentSketch.config.EnableMarkedAreaDrawing && type != 'edit') head = 'New Sketch - Labels';

    $('.Current_vector_details .vectorPage , .Current_vector_details .footradio,.Current_vector_details .calculatedradio').parent().remove()
    $('.assocValidation,.UnSketched,.linked-div,.SectNumDropDown,.PnANewValue,.createNewSec').remove();
    $('.dynamic_prop div').remove();
    $('.Current_vector_details .head').html(head);
    $('.Current_vector_details').width(490);
    $('.mask').show();
    var selectedIndex = 0
    var getDataFieldValues = window.opener ? window.opener.getDataField : getDataField;
    var selectOne = items.find(function (a) {
        return a.requiredAny
    })
    //$( '.Current_vector_details .vectorPage , .Current_vector_details .footradio' ).parent().remove()
    items.forEach(function (item, i) {
        var val = item.Value || ''
        val = val.toString();
        var lookupValuesLength = 0;
        var swapNameValue = item.NameValue || ''
        swapNameValue = swapNameValue.toString();
        let addMultiplier = null;
        val.split(items[i].splitByDelimiter).forEach(function (itemValue, j) {
            var div = document.createElement('div');
            if (item.hiddenFromEdit) $(div).addClass('hidden');
            var sp = document.createElement('span');
            var t = document.createTextNode(item.Caption || item.Field || 'Label');
            sp.appendChild(t);
            $(div).addClass('control_div')
            div.appendChild(sp);
            var input = document.createElement((item.lookup || item.lookUpQuery || (sketchSettings["sketchLookupQuery"] && sketchSettings["sketchLookupQuery"] != '')) ? 'select' : 'input');
            input.setAttribute('FieldName', item.Field);
            if (items[i].splitByDelimiter)
                $(input).addClass('same_field')
            if (item.lookup || item.lookUpQuery || (sketchSettings["sketchLookupQuery"] && sketchSettings["sketchLookupQuery"] != '')) {
                var options = false;
                if ((!itemValue || itemValue == '') || selectOne) {
                    var opt = document.createElement('option');
                    input.appendChild(opt);
                    options = true;
                }
                var lookUpArray = (item.lookup && !(sketchSettings["sketchLookupQuery"] && sketchSettings["sketchLookupQuery"] != '' && editor.currentSketch.lookUp)) ? (lookups[item.lookup] ? lookups[item.lookup] : []) : (item.lookUpQuery || (sketchSettings["sketchLookupQuery"] && sketchSettings["sketchLookupQuery"] != '')) ? editor.currentSketch.lookUp : [];
                var values = item.ShowCurrentValueOnly ? (lookUpArray && lookUpArray[itemValue] ? [lookUpArray[itemValue]] : []) : lookUpArray;
                values = Object.keys(values).map(function (x) {
                    return values[x]
                }).sort(function (a, b) {
                    return a.Ordinal - b.Ordinal
                })
                if (item.LookUpFilter && !item.ShowCurrentValueOnly) values = eval(' values.filter(function(lk){ return ' + item.LookUpFilter + '})')
                if (options == false) {
                    var opt = document.createElement('option');
                    input.appendChild(opt);
                    opt.selected = true;
                }
                var fLookUp = editor.external.lookupMap[item.lookup]
                var isLookUpQueryField = fLookUp ? fLookUp.LookupTable == '$QUERY$' : false;
                item.showLabelDescriptionOnly = isLookUpQueryField
                lookupValuesLength = values ? values.length : 0;

                if (editor?.config.splitLabelAreaFields) {
                    addMultiplier = 1;
                    if (itemValue?.split('*').length > 1) {
                        let splbl = itemValue.split('*'); itemValue = splbl[0].trim();
                        addMultiplier = parseFloat(splbl[1].trim()); if (isNaN(addMultiplier)) addMultiplier = 1;
                    }
                }

                for (var x in values) {
                    var opt = document.createElement('option');
                    opt.setAttribute('value', values[x].Id);
                    opt.innerHTML = ((sketchSettings["DoNotShowLabelCodeInDropDown"] == '1') ? values[x].Name : ((clientSettings["DoNotShowLabelDescription"] == 'true' || clientSettings["DoNotShowLabelDescription"] == '1' || sketchSettings["DoNotShowLabelDescription"] == '1' || sketchSettings["DoNotShowLabelDescription"] == 'true') ? values[x].Id : (item.IsSwapLookupIdName ? (values[x].Name + '-' + values[x].Id) : (((editor.formatter.showLabelDescriptionOnly || clientSettings.DoNotShowLabelCodeSketch == '1' || isLookUpQueryField) ? '' : (values[x].Id + '-')) + values[x].Name))));
                    input.appendChild(opt);
                    if (item.IsSwapLookupIdName) {
                        if (swapNameValue == values[x].Id) {
                            opt.selected = true;
                            options = true;
                            selectedIndex = i;
                        }
                    }
                    else if (itemValue == values[x].Id || (item.UseLookUpNameAsValue && itemValue == values[x].Name)) {
                        opt.selected = true;
                        options = true;
                        selectedIndex = i;
                    }
                }
                if (options == false) {
                    var opt = document.createElement('option');
                    input.appendChild(opt);
                    opt.selected = true;
                }
            } else {
                if (itemValue) input.value = itemValue;
                input.setAttribute('maxlength', 50);
                if (item.EnableFieldValidation) {
                    var tField = getDataFieldValues(item.Field, item.EnableFieldValidation.sourceTable);
                    if (tField) {
                        var inputType = parseInt(tField.InputType);
                        var d = window.opener.QC.dataTypes[inputType];
                        if (tField.MaxLength) {
                            var ml = parseInt(tField.MaxLength);
                            if (ml > 0)
                                input.setAttribute('maxlength', ml);
                        }
                        input.setAttribute('field_id', tField.Id);
                        input.setAttribute('fieldValidate', '1');
                        if (d.cctype == 2 || d.cctype == 8 || d.cctype == 10)
                            input.setAttribute('onkeyPress', "return fieldValidationInSketch(event, this)");
                        if (inputType == 4)
                            input.type = 'date';
                    }
                }
                else
                    input.setAttribute('onkeydown', "return blockSpecialChars(event)");
            }

            var doNotShowIsLargeLookup = false;
            if (item.ReadOnly || disableFirstRecordlabel) {
                input.setAttribute('disabled', true);
                input.setAttribute("readonly", "readonly");
                doNotShowIsLargeLookup = true;
            }
            /*if (!(item.lookup) && !(item.lookUpQuery) && sketchSettings['LabelMaxLimit']){
                input.setAttribute("onkeypress", "return labeltextlength($(this).val())");
            }*/

            if (addMultiplier) $(div).append('<input style="width: 50px; height: 23px; margin-right: 10px;" type="number" step="0.1" class="sigmaMul" value="' + addMultiplier + '" min="0" max="1"/>');

            div.appendChild(input);

            var del = document.createElement('span');
            $(del).addClass('del_element')
            $(del).text('-')
            if (j == 0) $(del).hide()
            div.appendChild(del);
            $(del).attr('onclick', 'add_remove_button(null,this)');
            var add = document.createElement('span');
            $(add).addClass('add_element')
            $(add).text('+')
            if (j != val.split('/').length - 1 || !items[i].splitByDelimiter)
                $(add).hide()
            div.appendChild(add);
            $(add).attr('onclick', 'add_remove_button("add",this)');
            $('.dynamic_prop').append(div)

            if ((item.IsLargeLookup || lookupValuesLength > 20) && !doNotShowIsLargeLookup) {
                var search = document.createElement('span');
                $(search).attr({
                    'onclick': 'return lookupforSketch("' + item.lookup + '",this);'
                });
                $(search).attr({
                    'lookup': item.lookup
                });
                $(search).addClass('lookupSearchInSkecth')
                div.appendChild(search);
            }

            if (item.requiredAny) {
                item.IsRequired = true;
                $('.dynamic_prop input,.dynamic_prop select').unbind('change');
                $('.dynamic_prop input,.dynamic_prop select').bind('change', function () {
                    $('.dynamic_prop input,.dynamic_prop select').not($(this)).val('').attr('disabled', 'disabled');
                    if ($(this).val() != '') items.filter(function (q) {
                        return q.IsRequired = false
                    })
                    else {
                        items[0].IsRequired = true;
                        $('.dynamic_prop input,.dynamic_prop select').not(this).removeAttr('disabled');
                    }
                });
            }
            if (item.UseLookUpNameAsValue) {
                $('.dynamic_prop select').live('change', function () {
                    $('.dynamic_prop input').val($(this).find("option[value='" + $(this).val() + "']").text())
                })
            }
        })
    })

    if (sketchSettings["EnableSplitLabelMaximum"] == '1') {
        $('.dynamic_prop').css({ "max-height": "200px", "overflow-y": "auto" });
        $('.Current_vector_details').css('width', '510px');
    }
    else if (editor?.config.splitLabelAreaFields)
        $('.Current_vector_details').css('width', '600px');

    if (selectOne)
        $('.dynamic_prop input,.dynamic_prop select').eq(selectedIndex).change();
    $('.Current_vector_details #Btn_cancel_vector_properties').unbind(touchClickEvent)
    $('.Current_vector_details #Btn_cancel_vector_properties').bind(touchClickEvent, function () {
        $('.Current_vector_details').hide();
        if (sketchSettings["EnableSplitLabelMaximum"] == '1')
            $('.dynamic_prop').css({ "max-height": "", "overflow-y": "" });
        $('.createNewSec').remove();
        if (!editor.isScreenLocked) $('.mask').hide();
        return false;
    })
    var validation = true;
    $('.validationFailMsg').remove();
    $('.Current_vector_details #Btn_Save_vector_properties').unbind(touchClickEvent)
    $('.Current_vector_details #Btn_Save_vector_properties').bind(touchClickEvent, function () {        
        validation = true;
        var sectionValidation = true, sectionValuesTrue = false, sectionValues, areaMultiplierValue, isDeltaConfig = false;
        $('.validationFailMsg').remove();
        var t = '';
        $('.dynamic_prop input,.dynamic_prop select').each(function (i, element) {
            if ($(element).hasClass('sigmaMul')) return;
            var k = sketchApp?.config.splitLabelAreaFields && i == 1 ? 0 : i;
            if ($(element).hasClass('same_field')) {
                var j = i; i = 0;
                let elVal = $(element).val(), mlEl = $(element).siblings('.sigmaMul')[0], mlVal = null;
                if (mlEl) {
                    mlVal = $(mlEl).val();
                    if (mlVal == '' || !mlVal || mlVal >= 1 || mlVal <= 0 || isNaN(parseFloat(mlVal))) mlVal = 1;
                    else mlVal = parseFloat(mlVal);
                }

                t += elVal + ((!mlVal || mlVal == 1) ? '' : ('*' + mlVal)) + items[i].splitByDelimiter;

                if (j == $('.dynamic_prop input,.dynamic_prop select').length - 1) {
                    t = t.slice(0, -1)
                    if (t != items[i].Value) {
                        items[i].Value = t;
                        items[i].IsEdited = true;
                    }
                }
            }
            if (($(element).val() != items[i].Value) || items[i].IsEdited) {
                if (items[i].hiddenFromEdit) return;
                if (items[i].ValidationRegx) {
                    if (!items[i].ValidationRegx.test($(element).val()) && $('.dynamic_prop span.validationFailMsg').length == 0) {
                        $('.dynamic_prop').append('<span class="validationFailMsg">Check all the values</span>')
                        validation = false;
                    }
                }
                if (items[i].IsRequired && $(element).val().trim() == '' && validation && $('.dynamic_prop span.validationFailMsg').length == 0) {
                    if (sketchSettings["EnableSplitLabelMaximum"] == '1')
                        $('.dynamic_prop').after('<span class="validationFailMsg">Check all the values</span>');
                    else
                        $('.dynamic_prop').append('<span class="validationFailMsg">Check all the values</span>');
                    validation = false
                }
                /*MA_2479*/
                //if ($('.sigmaMul').val() > 1 || $('.sigmaMul').val() < 0) {
                //    $('.dynamic_prop').append('<span class="validationFailMsg">The value should be between 0 and 1 </span>');
                //    validation = false;
                //}
                if (validation) {
                    hideOrShowManualBox();
                }
                if (validation && $(element).attr('fieldValidate') == '1') {
                    var field = window.opener.datafields[$(element).attr('field_id')];
                    var inputType = parseInt(field.InputType);
                    var d = window.opener.QC.dataTypes[inputType];
                    var pattern = new RegExp(d.pattern);
                    if (field.MaxLength && $(element).val() != "") {
                        var ml = parseInt(field.MaxLength);
                        if (ml > 0 && $(element).val().length > ml) {
                            var msg = 'Maximum field length of ' + items[i].Field + ' is ' + ml;
                            $('.dynamic_prop').append('<span class="validationFailMsg">' + msg + '</span>');
                            validation = false;
                        }
                    }

                    if (validation && pattern != '' && $(element).val() != "") {
                        if (!pattern.test($(element).val())) {
                            var msg = '';
                            if (d.baseType == 'number') {
                                if (d.typename == 'Year')
                                    msg = 'Invalid input. Please enter a valid 4-digit year for the field.';
                                else
                                    msg = 'Invalid input. Please enter a valid Number for the field.';
                                $('.dynamic_prop').append('<span class="validationFailMsg">' + msg + '</span>');
                                validation = false;
                            }
                        }
                    }
                }
                if (validation && (!items[i].splitByDelimiter || (items[i].Value && (!items[i].Value.contains('/') && (!sketchApp?.config.splitLabelAreaFields || (sketchApp?.config.splitLabelAreaFields && !items[i].Value.contains('*'))))))) {
                    items[i].Value = $(element).val();
                    var desc = items[i].lookup ? lookups[items[i].lookup][$(element).val()] : null;
                    if (items[i].UseLookUpNameAsValue && desc)
                        items[i].NameValue = desc.Name;
                    if (items[i].IsSwapLookupIdName) {
                        items[i].Value = desc.Name;
                        items[i].NameValue = desc.Id;
                    }
                    items[i].IsEdited = true;
                    items[i].Description = desc
                }
                else if (type = 'edit' && items[i].splitByDelimiter && (items[i].Value && items[i].Value.contains('/')))
                    items[i].Description = null;
                if (k == 0 && items[i].splitByDelimiter) {
                    var desc = items[i].lookup ? lookups[items[i].lookup][$(element).val()] : null;
                    if (desc)
                        items[0].colorDescription = desc;
                }                
            }
        })
        if ((clientSettings && (clientSettings.SketchConfig == "Baldwin" || clientSettings.SketchConfig == "Delta")) || (sketchSettings && (sketchSettings.SketchConfig == "Baldwin" || sketchSettings.SketchConfig == "Delta"))) {
            isDeltaConfig = true;
            var labelSelect = $('.dynamic_prop select').val();
            var apexLookup = window.opener ? window.opener.lookup['Apex_Area_Codes'] : lookup['Apex_Area_Codes'];
            var deltaLookupValue = labelSelect ? (apexLookup ? (apexLookup[labelSelect]) : null) : null;
            areaMultiplierValue = deltaLookupValue ? (deltaLookupValue.NumericValue ? parseFloat(deltaLookupValue.NumericValue) : 1) : 1;
        }

        let autoSection = false;
        if (editor.currentSketch.config.AllowSectNum) {
            if ($('.SectNum')[0]) {
                if ($('.SectNum').val()) {
                    sectionValues = $('.SectNum').val();
                    sectionValuesTrue = true;
                    if ($('.createNewSecLbl .checkbox').length > 0 && $('.createNewSecLbl .checkbox')[0].checked) autoSection = true;
                }
                else {
                    sectionValidation = false;
                    $('.SectNumDropDown').append('<span class="SectionvalidationFailMsg">Please choose value from dropdown</span>')
                }
            }
        }

        if (validation && editor.config.IsPatriot && $('.UnSketchedarea .checkbox').length > 0 && !$('.UnSketchedarea .checkbox')[0].checked && $('.UnSketched .sqft').val() == '')
            validation = false;

        if (validation && editor.currentSketch.config.EnableMarkedAreaDrawing && ($('.UnSketched .sqft').val() == '' || $('.UnSketched .sqft').val() <= 0)) {
            validation = false; $('.UnSketched .sqft').css('border', '1px solid red');
        }

        if (((clientSettings && (clientSettings.SketchConfig == "IASW_US" || clientSettings.SketchConfig == "IASW_CA" || clientSettings.SketchConfig == "LucasDTR")) || (sketchSettings && (sketchSettings["SketchConfig"] == "IASW_US" || sketchSettings["SketchConfig"] == "IASW_CA" || sketchSettings["SketchConfig"] == "LucasDTR"))) && !disableFirstRecordlabel) {
            let vd = false;
            items.forEach((im) => {
                if (im.Value) vd = true;
            });
            if (!vd) {
                validation = false;
                $('.dynamic_prop').append('<span class="validationFailMsg">Please fill at least one dropdown</span>');
            }
        }

        if (validation && sectionValidation) {
            if (callback) callback(items, { 'page': ($('.vectorPage').attr('checked')), 'footMode': ($('input[name="footMode"]').filter(function () { return this.checked }).val()), 'calcType': ($('input[name="calc"]').filter(function () { return this.checked }).val()), linkedUniqueId: $('.Current_vector_details .linkedAreaList').val(), 'unSketch': ($('.UnSketchedarea .checkbox').length > 0 ? (editor.currentSketch.config.CustomToolForVector || editor.config.enableManualAreaEntry ? $('.UnSketchedarea .checkbox')[0].checked : !$('.UnSketchedarea .checkbox')[0].checked) : false), 'sqft': $('.UnSketched .sqft').val(), 'SECTNUM': (sectionValuesTrue ? sectionValues : false), 'isNewPNAValue': ($('.PnANewValueTrue .checkbox').length > 0 ? ($('.PnANewValueTrue .checkbox')[0].checked ? '1' : '0') : false), 'isAreaMultiplier': (isDeltaConfig ? areaMultiplierValue : false), 'autoSection': autoSection })
            if (sketchSettings["EnableSplitLabelMaximum"] == '1')
                $('.dynamic_prop').css({ "max-height": "", "overflow-y": "" });
            $('.createNewSec').remove();
            if (!editor.isScreenLocked) $('.mask').hide();
            $('.Current_vector_details').hide();
        }
        return false;
    });

    $('.Current_vector_details select').unbind('change');
    $('.Current_vector_details select').bind('change', function () {
        $('.validationFailMsg').remove();
    });
    // $(".control_div input").attr("maxlength", 50);
    $('.control_div input').blur(function () {
        if (($(this).val()) != "" && ($(this).val()) != null)
            $('.validationFailMsg').remove();
    });
    $('.control_div input').keypress(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
        }
    });
    if (option.addFootMode)
        $('.Current_vector_details .head').after('<div> <div class="footradio"><label style="float: left;width: 98px;"> Mode</label><label style="float: left"><input type="radio" checked="checked" name="footMode" style="width: 20px;margin-right:7px;margin-top: -3px;"/>By Foot</label><label style="float: left"><input type="radio" value="tenth" name="footMode" style="width: 20px;margin-right:7px;margin-top: -3px;"/>By Tenth Foot</label></div></div>')
    if (option.addpagingcontrol && $('.Current_vector_details .vectorPage').length == 0 && type != 'edit')
        $('.Current_vector_details .head').after('<div> <label style="float: left;margin-left: 11px;"> Is new page </label><input type="checkbox" class="vectorPage" style="height: 19px;margin-top: 2px;    width: 74px;"/></div>')
    if (option.addCalculatedType && type != 'edit' && (!sketchApp.config.ApexAutoSubtract || (sketchApp.config.ApexAutoSubtract && sketchSettings['ApexAutoSubtract'] == '1'))) {
        $('.Current_vector_details .head').after('<div> <div class="calculatedradio"><label style="float: left"> Calculated Type :</label><label style="float: left"><input value="positive" type="radio" checked="checked" name="calc" style="width: 20px;margin-right:7px;margin-top: -3px;"/>Positive</label><label style="float: left"><input type="radio" value="negative" name="calc" style="width: 20px;margin-right:7px;margin-top: -3px;"/>Negative</label><label style="float: left"><input type="radio" value="auto" name="calc" style="width: 20px;margin-right:7px;margin-top: -3px;"/>Auto-Subtract</label></div><div class="linked-div hidden" style="    margin-top: 7px;"><span>Linking Area </span><select class="linkedAreaList"></select></div></div>')
    }
    if ((editor.currentSketch.config.IsPnANewValue) && type != 'edit') {
        var caption = 'New Value'
        $('.Current_vector_details .head').after('<div class ="PnANewValue"> <div class="PnANewValueTrue"><label style="float: left;width: 98px;"> ' + caption + '</label><label style="float: left"><input class="checkbox" type="checkbox"  style="width: 20px;margin-right:7px;margin-top: -3px;"/>Yes</label></div></div>');
    }
    var parcel = window.opener.activeParcel;
    var lookupDropDown = window.opener ? window.opener.lookup : lookup;
    if (editor.currentSketch.config.AllowSectNum && !(editor.currentVector && (editor.currentVector.visionDeleted || editor.currentVector.newSection))) {
        let sectionList = getSectionNumHtml(editor);
        $('.Current_vector_details .dynamic_prop').after(sectionList.secthtml);

        if (sectionList.sectRecords > 0 && editor.config?.CreateAssociateWindow && type != 'edit') {
            let nbht = '<div class ="createNewSec" style="width: 90%;"> <div class="createNewSecLbl"><label style="float: left;width: 98px;"> New Section </label><label style="float: left"><input class="checkbox" type="checkbox" style="width: 20px;margin-right:7px;margin-top: -3px;"/>Yes</label></div></div>';
            $('.Current_vector_details .head').after(nbht);
        }

        if (type == 'edit') {
            var stNum = editor.currentVector.sectionNum;
            $('.SectNum').val(stNum);
        }
    }

    var uncheck = 'checked', cconfig = editor.currentSketch.config;
    if (((cconfig.EnableUnSketchedAreaDrawing || cconfig.CustomToolForVector || editor.config.enableManualAreaEntry) && type != 'edit') || cconfig.EnableMarkedAreaDrawing) {
        var caption = 'Draw Sketch'
        if (cconfig.CustomToolForVector) {
            uncheck = '';
            caption = 'Add Sq Feet'
        }
        else if (editor.config.enableManualAreaEntry) {
            uncheck = '';
            caption = 'Manual Area';
        }
        else if (cconfig.EnableMarkedAreaDrawing && type == 'edit' && (editor.currentVector.CheckFixedArea || (cconfig.EnableOBYEditlabel && editor.currentVector.MarkedAreaValue && editor.currentVector.vectorString == ""))) uncheck = '';
        $('.Current_vector_details .head').after('<div class ="UnSketched"> <div class="UnSketchedarea"><label style="float: left;width: 98px;"> ' + caption + '</label><label style="float: left"><input class="checkbox" type="checkbox"  ' + uncheck + ' style="width: 20px;margin-right:7px;margin-top: -3px;"/>Yes</label></div></div>');
    }
    $('.UnSketchedarea .checkbox').unbind('click')
    $('.UnSketchedarea .checkbox').bind('click', function () {
        let arlbl = cconfig.EnableMarkedAreaDrawing ? ' Area' : ' SQFT';
        if ($(this).attr('checked'))
            uncheck == '' ? $('.UnSketchedarea').after('<div class="fixedarea"> <label style="float: left;width: 100px;"> ' + arlbl + ' </label> <input type="number" class="sqft" /> </div>') : $('.UnSketched .fixedarea').remove();
        else {
            if (editor.config.IsPatriot)
                uncheck == 'checked' ? $('.UnSketchedarea').after('<div class="fixedarea"> <label style="float: left;width: 100px;"> SQFT <abbr style="color:red;padding-left: 3px;font-size: 15px;"> *</abbr> </label>  <input type="number" class="sqft" /> </div>') : $('.UnSketched .fixedarea').remove();
            else
                uncheck == 'checked' ? $('.UnSketchedarea').after('<div class="fixedarea"> <label style="float: left;width: 100px;"> ' + arlbl + ' </label> <input type="number" class="sqft" /> </div>') : $('.UnSketched .fixedarea').remove();
        }
        $('.fixedarea .sqft').bind('keydown', function () {
            $('.UnSketched .sqft').css('border', '');
            if ($('.sqft').val().length >= 10) {
                var sqfttt = $('.sqft').val().slice(0, -1);
                $('.sqft').val(sqfttt);
            }
        });
    })

    $('.calculatedradio input[type="radio"]').unbind('click')
    $('.calculatedradio input[type="radio"]').bind('click', function () {
        if ($(this).val() == 'auto') {
            var links = editor.currentSketch.vectors.filter(function (a) { return ((!a.linkedUniqueId || a.linkedUniqueId == '-1') && a.uniqueId && a.uniqueId != '-1') }).map(function (a) { return { id: a.uniqueId, text: a.uniqueId + " - " + a.name } })
            var output = [];
            $.each(links, function (key, value) {
                output.push('<option value="' + value.id + '">' + value.text + '</option>');
            });
            $('.Current_vector_details .linkedAreaList').html(output.join(''))[0].selectedIndex = 0;
            $('.linked-div ').show();
        }
        else
            $('.linked-div ').hide();
    })
    $('.vectorPage').unbind('click')
    $('.vectorPage').bind('click', function () {
        if ($(this).attr('checked') == true || $(this).attr('checked') == 'checked')
            $('.footradio').show();
        else if (sketchApp.currentSketch.vectors.length > 0)
            $('.footradio').hide();
    })
    if (sketchApp.currentSketch.vectors.length > 0)
        $('.footradio').hide();
    $('.Current_vector_details').show()
    if (cconfig.EnableMarkedAreaDrawing) {
        if (type == 'edit' && (editor.currentVector.CheckFixedArea || (cconfig.EnableOBYEditlabel && editor.currentVector.MarkedAreaValue && editor.currentVector.vectorString == ""))) {
            uncheck = 'checked';
            let va = editor.currentVector.CheckFixedArea ? editor.currentVector.fixedArea : editor.currentVector.MarkedAreaValue;
            $('.UnSketchedarea').after('<div class="fixedarea"> <label style="float: left;width: 100px;"> Area</label> <input type="number" class="sqft" /> </div>');
            $('.sqft').val(va);
        }

        $('.UnSketched').css({ "display": "flex" }); $('.UnSketched').addClass('markedArea');
    }
}
var SketchLookup, currentLookupValue
function add_remove_button(type, elem) {
    if (type == 'add') {
        var _maxlen = sketchSettings["EnableSplitLabelMaximum"] == '1' ? 10 : 5;
        if ($('.dynamic_prop .control_div').length < _maxlen) {
            let cloneElem = $(elem).parents('.control_div').clone(true);
            if (sketchApp?.config.splitLabelAreaFields) $(cloneElem).children('.sigmaMul').val('1');
            $('.dynamic_prop').append(cloneElem);
            if (_maxlen > 5)
                $('.dynamic_prop').scrollTop($('.dynamic_prop')[0].scrollHeight);
        }
        $('.validationFailMsg').remove();
    }
    else
        $(elem).parents('.control_div').first().remove();
    $('.validationFailMsg').remove();
    $('.dynamic_prop .del_element').show().first().hide()
    $('.dynamic_prop .add_element').hide().last().show()
    $('.Current_vector_details select').unbind('change');
    $('.Current_vector_details select').bind('change', function () {
        $('.validationFailMsg').remove();
    });
}
function lookupforSketch(SketchLookupval, source) {
    var input = $(source).siblings('select')
    currentLookupValue = input;
    $('.customddlbtnadd').hide();
    $('.customddl, .mask').show();
    $('.divCodeFile').show();
    $('.divin span[legend]').html($(source).siblings('span').html() + ": ");
    $('div[controls] input').focus();
    $('div[controls] input').val('');
    $('#loolupul').html('');
    if (window.opener && window.opener.appType != 'sv')
        window.opener.setScreenDimensions();
    SketchLookup = SketchLookupval;
    $('.customddl span[legend]').html($(source).siblings('span').html());
    if (input) {
        var input_elem = $('option[value="' + input.val() + '"]', $(input))
        var currentText = (input_elem.length > 0) ? input_elem[0].text : '';
        $('span[selectedcustomddl]').html(currentText);
    }
    $("#searchtxt").keypress(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
        }
    });
}
function sketchLookupSelected(source) {
    currentLookupValue = $(source).attr('value');
    $('.customddlbtnadd').show();
    $('span[selectedcustomddl]').html($(source).html());
    $('.customddl_desc ul li').removeAttr('selected');
    $(source).attr('selected', '');
}
function CovertHexToRGB(hex) {
    var c;
    if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
        c = hex.substring(1).split('');
        if (c.length == 3)
            c = [c[0], c[0], c[1], c[1], c[2], c[2]];
        c = '0x' + c.join('');
        return [(c >> 16) & 255, (c >> 8) & 255, c & 255].join(',');
    }
    else {
        if (!colorCodevalidationAlert)
            alert('Bad Hex in sketch lookup color code');
        colorCodevalidationAlert = true;
        return '212,244,246';
    }
}

function associateVector(callBack) {
    var config = sketchApp.config || (window.opener.ccma.Sketching && window.opener.ccma.Sketching.Config) || (sketchApp.currentSketch && sketchApp.currentSketch.config)
    if (config.AssociateVector && !config.AssociateVector.associateVectorDefinition) {
        var SketchAssociateFields = {};
        if (!sketchApp.sketches.some(function (sk) { return sk.isModified; })) { if (callBack) callBack(); return; }
        if (!sketchApp.sketches.some(function (sk) { return sk.vectors.length != 0; })) { if (callBack) callBack(); return; }
        var recordCount = {};
        if (!sketchApp.currentSketch) {
            alert("To save associateVectors,Please select one of the sketch segment!");
            return;
        }

        var associateVector = config.AssociateVector;
        var skSource = associateVector.source;
        var parcel = window.opener.activeParcel;

        skSource.forEach(function (src) {
            SketchAssociateFields[src.sourceTable] = {};
            src.fields.split(',').forEach(function (field) {
                var f = '', fields = field.split('/');;
                if (field.indexOf('-') > -1) {
                    var flds = []
                    field.split('-').forEach(function (fld) {
                        flds.push(fld.split('.')[1]);
                    })
                    f = flds.join('-');
                }
                else f = fields[0].split('.')[1] + (fields[1] ? ('/' + fields[1].split('.')[1].trim()) : '');

                var arr = parcel[fields[0].split('.')[0].trim()];
                SketchAssociateFields[src.sourceTable][f] = fields[0].split('.')[0].trim();
                recordCount[src.sourceTable] = arr.length;
            });
        });
        $('.dynamic_prop div').remove();
        $('.Current_vector_details .head').html('Associate Sketch to ' + (skSource.length == 1 ? skSource[0].category : 'CAMA Record'));
        $('.mask').show(); $('.dynamic_prop').html('');
        $('.dynamic_prop').append('<div class="divvectors"></div>');
        skSource.forEach(function (src) {
            var html = '<table style="width: 100%;"><tr><th style="background: #d6d6d6;word-break: break-all;width:200px">Sketch Label </th><th style="background: #d6d6d6;">Area</th>';
            html += '<th style="background: #d6d6d6;">' + src.headers.split(',').join('</th><th style="background: #d6d6d6;">') + '</th>';
            html += '</tr>';
            var fieldHtml = '';
            if (associateVector.vectorListConfig) {
                var vect_html = associateVector.vectorListConfig(sketchApp, src.sourceTable, SketchAssociateFields[src.sourceTable]);
                if (vect_html == '') html = '';
                else html += vect_html;
                $('.Current_vector_details').css('left', '30%');
                $('.Current_vector_details').css('top', '25%');
            }
            else {
                sketchApp.sketches.forEach(function (sketch, i) {
                    fieldHtml = '';
                    $('.Current_vector_details').css('left', '20%');
                    sketch.vectors.forEach(function (sk, j) {
                        if (sk.isFreeFormLineEntries)
                            return;
                        var opt = '', addVector = true;
                        if (sk.associateValues === undefined) sk.associateValues = {};
                        opt += '<tr sketch="' + i + '" vector="' + j + '">';
                        if (fieldHtml == '') {
                            $.each(SketchAssociateFields[src.sourceTable], function (key, table) {
                                fieldHtml += '<td><select class="popupselect1" sourceTable="' + src.sourceTable + '" fieldName="' + key + '">';
                                fieldHtml += '<option value="0" value1=""></option>';
                                var value = parcel[table].filter(function (rec) { return ((rec.ParentROWUID == sketch.parentRow.ROWUID) && rec.CC_Deleted != true) }).map(function (r) { return r[key] });
                                value.forEach(function (a, idx) { fieldHtml += '<option value="' + (idx + 1) + '" value1="' + a + '">' + a + '</option>' });
                                fieldHtml += '</select></td>';
                            });
                        }
                        opt += ' <td><span style="word-break: break-all;width:150px;">' + sk.label + '</span></td><td><span style="text-align:center;" class="assoc_area">' + sk.area() + '</span></td>' + fieldHtml;
                        opt += '</tr>';
                        if (addVector) html += opt;
                    });
                });
            }

            var _skcon = clientSettings.SketchConfig || sketchSettings.SketchConfig;
            if (sketchApp.config.isHideAssociateVector || _skcon == "FTC" || _skcon == "ApexJson") {
                var getCategoryFromSourceTable = window.opener ? window.opener.getCategoryFromSourceTable : getCategoryFromSourceTable;

                if (skSource.length > 1 && html != '') {
                    var category = getCategoryFromSourceTable(src.sourceTable);
                    html += '</table></div>';
                    $('.divvectors').append('<div><span style="font-size: 14px; font-weight: 700;">' + category.Name + '</span>' + html + '');
                }
            }
            else if (skSource.length > 1 && html != '') {
                var category = window.opener.getCategoryFromSourceTable(src.sourceTable);
                $('.divvectors').append('<span style="font-size: 14px; font-weight: 700;">' + category.Name + '</div>');
                html += '</table>';
                $('.divvectors').append(html);
            }
        });
        if (associateVector.note) {
            $('.skNote').html('<span><b>Note:</b> ' + associateVector.note + '</span>');
            $('.skNote').css('color', '#fb2300')
        }
        //var xScroll = new IScroll('.dynamic_prop', iScrollOptions);

        $('.calculatedradio').hide()
        $('.linked-div').hide();
        $('.Current_vector_details').show();
        $('.Current_vector_details').css('width', ($('.divvectors').width() + 50) + 'px');

        if ((clientSettings && (clientSettings.SketchConfig == "Baldwin" || clientSettings.SketchConfig == "Delta")) || (sketchSettings && (sketchSettings.SketchConfig == "Baldwin" || sketchSettings.SketchConfig == "Delta"))) {
            $('.Current_vector_details').css('width', ($('.divvectors').width() + 50) + 'px');
        }
        else {
            $('.divvectors').css('width', 701);
            $('.divvectors').css('max-height', 385);
            $('.Current_vector_details').css('width', 750);
            $('.Current_vector_details').css('left', '25%');
            $('.Current_vector_details').css('top', '20%');
        }

        let currentValueOption;
        $('.dynamic_prop .popupselect1').change(function () {
            if (associateVector.isConnected) {
                var _sconf = clientSettings.SketchConfig || sketchSettings.SketchConfig
                if (sketchApp.config.isHideAssociateVector || _sconf == "FTC" || _sconf == "ApexJson") {
                    if ($(this).val() != 0)
                        $(this).closest('div').find('tr').find('select').find('option[value="' + $(this).val() + '"]').attr('disabled', 'disabled');
                    $(this).closest('div').find('tr').find('select').find('option[value="' + currentValueOption + '"]').removeAttr('disabled');
                }
                $(this).closest('tr').find('select').not($(this)).val($(this).val());
                $('.assocValidation').remove();
            }
        });

        $('.dynamic_prop .popupselect1').mouseup(function (er) {
            var _sconf = clientSettings.SketchConfig || sketchSettings.SketchConfig
            if (sketchApp.config.isHideAssociateVector || _sconf == "FTC" || _sconf == "ApexJson")
                currentValueOption = $(this).val();
        });

        $('.Current_vector_details #Btn_cancel_vector_properties').unbind(touchClickEvent)
        $('.Current_vector_details #Btn_cancel_vector_properties').bind(touchClickEvent, function () {
            $('.skNote').html('');
            $('.assocValidation').hide();
            $('.Current_vector_details').css('left', '35%');
            $('.Current_vector_details').hide();
            if (!sketchApp.isScreenLocked) $('.mask').hide();
            return false;
        });

        if (associateVector.autoSelectIfSingle)
            skSource.forEach(function (src) {
                if (recordCount[src.sourceTable] == 1) $('.popupselect1[sourceTable="' + src.sourceTable + '"]').val(1);
            });
        if (associateVector.autoSelectFunction)
            associateVector.autoSelectFunction(sketchApp);
        else {
            var vectorCount = 0;
            sketchApp.sketches.forEach(function (sk, j) {
                var assocVal = sk.parentRow.CC_AssociateValues;
                if (assocVal && assocVal != '') {
                    assocVal.split(',').forEach(function (assoc, i) {
                        for (src in SketchAssociateFields) {
                            var fields = Object.keys(SketchAssociateFields[src])
                            var popuplk = $('.popupselect1[sourceTable="' + src + '"][fieldName="' + fields[0] + '"]').eq(vectorCount + i);
                            var val = $('option[value1="' + assoc + '"]', $(popuplk)).attr('value');
                            $(popuplk).val(val);
                            $(popuplk).trigger('change');
                        }
                    })
                }
                vectorCount += sk.vectors.length;
            });
        }
        $('.assocValidation').remove();
        $('.Current_vector_details #Btn_Save_vector_properties').unbind(touchClickEvent)
        $('.Current_vector_details #Btn_Save_vector_properties').bind(touchClickEvent, function () {
            var valid = true;
            $('.dynamic_prop .popupselect1').each(function (index, el) {
                var val = $('option[value="' + $(el).val() + '"]', $(el)).attr('value1');
                var rowid = $('option[value="' + $(el).val() + '"]', $(el)).attr('rowid');
                if (!val || val == '') valid = false;
                var row = $(el).parents('tr').first();
                var sketches = sketchApp.sketches
                if (associateVector.isEditedAndCurrentSketchOnly)
                    sketches = sketches.filter(function (sk) { return (sk.isModified || (sk.sid == sketchApp.currentSketch.sid)); })
                if (sketches[$(row).attr('sketch')])
                    var assoc = sketches[$(row).attr('sketch')].vectors[$(row).attr('vector')].associateValues
                var tbl = $(el).attr('sourceTable');;
                if (assoc) {
                    if (!assoc[tbl]) assoc[tbl] = {};
                    assoc[tbl][$(el).attr('fieldName')] = val;
                    assoc[tbl]['rowuid'] = rowid;
                }
            })
            if (valid || !associateVector.requiredFieldValidation) {
                $('.Current_vector_details').css('left', '35%');
                $('.Current_vector_details').hide();
                if (!sketchApp.isScreenLocked) $('.mask').hide();
                if (callBack) callBack();
            }
            else {
                $('.assocValidation').remove();
                $('.dynamic_prop').after('<span class="assocValidation" style="color: red; width: 100% !important; text-align: center;">Fill all inputs</span>');
            }
            return false;
        })
    }
    else if (config.AssociateVector && config.AssociateVector.associateVectorDefinition) config.AssociateVector.associateVectorDefinition(config.AssociateVector, callBack);
    else if (callBack) callBack();
}

function directlyPlaceUnsketchedSegments(skEditor, newVect, xp, yp, switchToUnsketch) {
    var p = new PointX(xp, yp)
    newVect.startNode = new Node(p, newVect);
    newVect.startNode.isStart = true;
    newVect.startNode.calculateVector({ x: 0, y: 0 });
    newVect.endNode = newVect.startNode;
    newVect.editor.currentNode = newVect.startNode;
    newVect.isUnSketchedArea = true;
    if (!switchToUnsketch) {
        newVect.disableMove = true;
        newVect.ignoreFromSave = true;
        skEditor.drawlabelOnly = false;
    }
    newVect.placeHolder = true;
    newVect.placeHolderSize = Number(2);
    newVect.terminateNode(newVect.startNode);
    newVect.isModified = true;
    skEditor.mode = 2;
    skEditor.currentNode = null;
    skEditor.render();
    newVect.isClosed = true;
    skEditor.toString();
    skEditor.raiseModeChange();
}

function undoSegmentChanges(editor, _action, _nodeNum, _nodePoint, _node, _allPoints, _lPos, _alPos) {
    var _cn;

    if (_action != 'SketchMove' && _action != 'MoveLabel' && _action != 'RotateSketch' && _action != 'PanSketchMove') {
        var sn = editor.currentVector.startNode;
        var i = 0;
        while (sn != null) {
            if (i == _nodeNum) {
                _cn = sn;
                break;
            }
            sn = sn.nextNode;
            i++;
        }
    }

    if (_action == 'SketchMove') {
        var d = new PointX(editor.currentVector.startNode.p.x - _nodePoint.x, editor.currentVector.startNode.p.y - _nodePoint.y);
        editor.currentVector.moveBy(d, null, true);
        if (_lPos && ((_lPos.x || _lPos.x == 0) && !isNaN(_lPos.x)) && ((_lPos.y || _lPos.y == 0) && !isNaN(_lPos.y))) {
            editor.currentVector.labelPosition.x = _lPos.x;
            editor.currentVector.labelPosition.y = _lPos.y;
        }
        if (_alPos && ((_alPos.x || _alPos.x == 0) && !isNaN(_alPos.x)) && ((_alPos.y || _alPos.y == 0) && !isNaN(_alPos.y))) {
            editor.currentVector.AreaLabelPosition.x = _alPos.x;
            editor.currentVector.AreaLabelPosition.y = _alPos.y;
        }
    }
    else if (_action == 'PanSketchMove') {
        var d = new PointX(editor.currentVector.startNode.p.x - _nodePoint.x, editor.currentVector.startNode.p.y - _nodePoint.y);
        editor.currentVector.moveBy(d, true, true);
        if (_lPos && ((_lPos.x || _lPos.x == 0) && !isNaN(_lPos.x)) && ((_lPos.y || _lPos.y == 0) && !isNaN(_lPos.y))) {
            editor.currentVector.labelPosition.x = _lPos.x;
            editor.currentVector.labelPosition.y = _lPos.y;
        }
        if (_alPos && ((_alPos.x || _alPos.x == 0) && !isNaN(_alPos.x)) && ((_alPos.y || _alPos.y == 0) && !isNaN(_alPos.y))) {
            editor.currentVector.AreaLabelPosition.x = _alPos.x;
            editor.currentVector.AreaLabelPosition.y = _alPos.y;
        }
    }
    else if (_action == 'NodeMove') {
        editor.currentNode = _cn;
        editor.currentNode.p.x = _nodePoint.x;
        editor.currentNode.p.y = _nodePoint.y;
        if (editor.currentVector && (editor.currentVector.isUnSketchedArea || editor.currentVector.isUnSketchedTrueArea)) {
            if (_lPos && ((_lPos.x || _lPos.x == 0) && !isNaN(_lPos.x)) && ((_lPos.y || _lPos.y == 0) && !isNaN(_lPos.y))) {
                editor.currentVector.labelPosition.x = _lPos.x;
                editor.currentVector.labelPosition.y = _lPos.y;
            }
            if (_alPos && ((_alPos.x || _alPos.x == 0) && !isNaN(_alPos.x)) && ((_alPos.y || _alPos.y == 0) && !isNaN(_alPos.y))) {
                editor.currentVector.AreaLabelPosition.x = _alPos.x;
                editor.currentVector.AreaLabelPosition.y = _alPos.y;
            }
        }
        editor.currentNode.recalculateAll();
    }
    else if (_action == 'DeleteNode') {
        editor.currentNode = _cn.prevNode;
        var cn = editor.currentNode;
        var np = new PointX(_nodePoint.x, _nodePoint.y);
        var n = new Node(np, editor.currentVector);
        n.hideDimensions = _node.hideDimensions; n.isArc = _node.isArc; n.isEllipse = _node.isEllipse;
        n.isEllipseEndNode = _node.isEllipseEndNode; n.arcLength = _node.arcLength;
        var nn = cn.nextNode;
        cn.nextNode = n;
        n.nextNode = nn;
        nn.prevNode = n;
        n.prevNode = cn;
        n.recalculateAll();
        editor.currentNode = n;
        nodeNumber = _nodeNum;
    }
    else if (_action == 'AddNodeBefore') {
        var p = _cn.prevNode;
        var n = _cn.nextNode;
        p.nextNode = n;
        n.prevNode = p;
        n.recalculateAll();
        editor.currentNode = n;
        nodeNumber = _nodeNum;
    }
    else if (_action == 'AddNodeAfter') {
        var p = _cn.nextNode;
        var n = _cn;
        var c = p.nextNode;
        n.nextNode = c;
        c.prevNode = n;
        c.recalculateAll();
        editor.currentNode = n;
        nodeNumber = _nodeNum;
    }
    else if (_action == 'MoveLabel') {
        editor.currentVector.labelPosition.x = _nodePoint.x;
        editor.currentVector.labelPosition.y = _nodePoint.y;
    }
    else if (_action == 'ARC') {
        editor.currentNode = _cn;
        editor.currentNode.p.x = _nodePoint.x;
        editor.currentNode.p.y = _nodePoint.y;
        editor.currentNode.hideDimensions = _node.hideDimensions;
        editor.currentNode.isArc = _node.isArc;
        editor.currentNode.isEllipse = _node.isEllipse;
        editor.currentNode.isEllipseEndNode = _node.isEllipseEndNode;
        editor.currentNode.arcLength = _node.arcLength;
        editor.currentNode.recalculateAll();
    }
    else if (_action == 'ELL') {
        editor.currentNode = _cn;
        var nn = _cn.nextNode.nextNode.nextNode;
        _cn.nextNode = nn;
        nn.prevNode = _cn;
        nodeNumber = _nodeNum;
        editor.currentNode.recalculateAll();
    }
    else if (_action == 'RotateSketch') {
        var sn = editor.currentVector.startNode;
        if (!sn) return;
        while (sn != null) {
            if (!((sn == editor.currentVector.endNode) && (sn.overlaps(editor.currentVector.startNode.p)))) {
                var p = _allPoints.shift();
                sn.p.x = p.x;
                sn.p.y = p.y;
            }
            sn = sn.nextNode;
        }
        if (editor.currentVector.startNode)
            editor.currentVector.startNode.recalculateAll();
    }
}

function boundaryScaleCalculation(editor) {
    $('.mask').show();
    var html = '';
    var _sval = editor.currentSketch.boundaryScale ? editor.currentSketch.boundaryScale : $('.ccse-scale').val();

    $('.Current_vector_details .head').html('Scale');
    $('.Current_vector_details .head').css("font-size", "19px")
    $('.Current_vector_details .vectorPage , .Current_vector_details .footradio,.Current_vector_details .calculatedradio').parent().remove();
    $('.assocValidation,.UnSketched,.linked-div,.SectNumDropDown,.PnANewValue,.dynamic_prop div, .outbdiv, .CurrentLabel').remove();
    $('.dynamic_prop').html(''); $('.dynamic_prop').append('<div class="divvectors"></div>');
    $('.divvectors').append('<div class="scale" ></div>');
    html += '<span class="scaleSpan" style = "width: 100%; font-size: 16px; margin-left: 10px;"><label style="float: left;margin-top: 3px;">Enter Scale (feet/side) : </label><input type="Number" class="scale_val" style="width: 75px; margin-left: 25px; font-size:16px; font-weight : bold;" /></span>';
    html += '<span class="defaultScale" style="width: 100%; margin-top: 15px"><label style="float: left; font-size: 16px;"><input class="default_Scale" type="checkbox" min="40" max="2500" style="width: 20px; margin-right: 15px; margin-left: 10px; font-weight: bold; margin-top: -2px;"/>Use sketch default scale</label></span>';
    $('.scale').append(html);
    $('.skNote').hide();
    $('.scale_val').val(_sval);
    $('.Current_vector_details').width(400);
    $('.Current_vector_details').show();

    $('#Btn_Save_vector_properties').unbind(touchClickEvent);
    $('#Btn_Save_vector_properties').bind(touchClickEvent, function () {
        var eRec = editor.currentSketch.parentRow, default_checked = $('.default_Scale')[0].checked, sval = parseInt($('.scale_val').val());
        if (default_checked) {
            var b_field = 'RorC', b_field_Val = 'R';
            if (editor.config.isMVP) {
                b_field = 'MainBuildingType';
                b_field_Val = 'D'
            }
            sval = (eRec && eRec[b_field] == b_field_Val) ? '80' : '200';
        }
        else if (isNaN(sval))
            sval = parseInt(editor.currentSketch.boundaryScale);
        else if (sval >= 2250)
            sval = 2500;
        else if (sval <= 49)
            sval = 40;
        else {
            var scaleArray = {
                R: [{ min: 0, max: 49, scale: 40 }, { min: 50, max: 69, scale: 60 }, { min: 70, max: 89, scale: 80 }, { min: 90, max: 109, scale: 100 }, { min: 110, max: 129, scale: 120 }, { min: 130, max: 149, scale: 140 }, { min: 150, max: 169, scale: 160 }, { min: 170, max: 189, scale: 180 }, { min: 190, max: 224, scale: 200 }, { min: 225, max: 274, scale: 250 }, { min: 275, max: 324, scale: 300 }, { min: 325, max: 374, scale: 350 }, { min: 375, max: 425, scale: 400 }, { min: 425, max: 474, scale: 450 }, { min: 475, max: 549, scale: 500 }, { min: 550, max: 649, scale: 600 }, { min: 650, max: 749, scale: 700 }, { min: 425, max: 474, scale: 500 }, { min: 750, max: 849, scale: 800 }, { min: 850, max: 949, scale: 900 }, { min: 950, max: 1249, scale: 1000 }, { min: 1250, max: 1749, scale: 1500 }, { min: 1750, max: 2249, scale: 2000 }, { min: 2250, max: 2499, scale: 2500 }],
                C: [{ min: 0, max: 49, scale: 40 }, { min: 50, max: 69, scale: 60 }, { min: 70, max: 89, scale: 80 }, { min: 90, max: 109, scale: 100 }, { min: 110, max: 129, scale: 120 }, { min: 130, max: 149, scale: 140 }, { min: 150, max: 169, scale: 160 }, { min: 170, max: 189, scale: 180 }, { min: 190, max: 224, scale: 200 }, { min: 225, max: 274, scale: 250 }, { min: 275, max: 324, scale: 300 }, { min: 325, max: 374, scale: 350 }, { min: 375, max: 425, scale: 400 }, { min: 425, max: 474, scale: 450 }, { min: 475, max: 549, scale: 500 }, { min: 550, max: 649, scale: 600 }, { min: 650, max: 749, scale: 700 }, { min: 425, max: 474, scale: 500 }, { min: 750, max: 849, scale: 800 }, { min: 850, max: 949, scale: 900 }, { min: 950, max: 1999, scale: 1000 }, { min: 2000, max: 2249, scale: 2000 }, { min: 2250, max: 2499, scale: 2500 }]
            };
            var _arr = (eRec && eRec.RorC == 'R') ? scaleArray.R : scaleArray.C;
            for (var i = 0; i < _arr.length; i++) {
                var _ar = _arr[i];
                if ((sval >= _ar.min) && (sval <= _ar.max)) {
                    sval = _ar.scale;
                    break;
                }
            }
        }
        editor.currentSketch.boundaryScale = sval;
        editor.currentSketch.isBoundaryScaleModified = true;
        $('.ccse-scale').val(sval);
        if (!sketchApp.isScreenLocked) $('.mask').hide();
        $('.Current_vector_details').hide();
        editor.render();
        return false;
    });

    $('.Current_vector_details #Btn_cancel_vector_properties').unbind(touchClickEvent)
    $('.Current_vector_details #Btn_cancel_vector_properties').bind(touchClickEvent, function () {
        if (!sketchApp.isScreenLocked) $('.mask').hide();
        $('.Current_vector_details').hide();
        return false;
    });
}

function createPartyWall(editor) {
    $('.mask').show();
    $('.Current_vector_details .head').html('Party Wall');
    $('.dynamic_prop').html(''); $('.UnSketched, .outbdiv, .CurrentLabel').remove();

    let wts = [{ Id: '0', Value: 'Not a partition wall' }, { Id: '1', Value: 'Finished' }, { Id: '2', Value: 'Unfinished' }, { Id: '3', Value: 'No Wall' }],
        enode = editor.currentLine?.eNode, lt = enode?.lineStrokePattern ? editor.currentLine.eNode.lineStrokePattern : '0', pr = enode?.proval_line_type;

    let container = $("<div />", {
        class: "partyClass",
        style: 'display:block;'
    });

    let wallType = $("<div />", {
        class: "wtClass",
        style: 'display: block; margin-bottom: 25px;'
    });

    let wth = $("<p />", { class: "wth", text: "Wall Type", style: "padding-bottom: 15px; font-size: 16px; font-weight: bold;" }), wtsp = $("<span />", { class: "wts", style: 'display: flex' });

    wts.forEach((wt) => {
        let rad = document.createElement("input");
        setMultipleAttributes(rad, { 'type': 'radio', 'name': 'walltype', 'value': wt.Id, 'id': wt.Id, 'class': 'wtRadio', 'style': "width: 20px; height: 20px; margin-right: 6px;" });

        let lbl = $("<label />", {
            text: wt.Value,
            style: "margin-right: 6px; font-size: 15px;"
        });

        appendMultiple(wtsp, [rad, lbl]);
    });

    appendMultiple(wallType, [wth, wtsp]);

    let flType = $("<div />", {
        class: "flClass",
        style: 'display: block;'
    });

    let html = '';
    html += '<span><input type="radio" name="floorType" value="1" id="af" class="flradio" style="width: 20px; height: 20px; margin-right: 6px; float: left;"><label style="float: left; font-size: 15px;">All Floors</label></span>';
    html += '<span style="margin-top: 20px;"><input type="radio" name="floorType" value="2" id="sf" class="flradio" style="width: 20px; height: 20px; margin-right: 6px; float: left;"><label style="margin-right: 6px; font-size: 15px; float: left;">Starting Floor</label><input type="number" class="flnumber nmtb" min="-10" max="100" style="margin-right: 6px; width:75px; height: 25px; float: left;"><label style="margin-right: 6px; font-size: 15px; float: left;">Number of Floors</label><input type="number" min="-10" max="100" class="nf nmtb" style="margin-right: 6px; width:75px; height: 25px; float: left;"></span>';

    flType.append(html);
    appendMultiple(container, [wallType, flType]);

    $('.dynamic_prop').append(container);
    $('.dynamic_prop').append('<span class="validateLabel" style="height: 12px; display: none; width:450px; color:Red; margin-left: 100px;">Please fill values.</span>');
    $('.Current_vector_details').css('width', '550px'); $('.skNote').hide();
    $('.Current_vector_details').show();

    if (lt != '0') {
        let prs = pr.split('~');
        $('.wtRadio[value="' + lt + '"]')[0].checked = true;

        if (!prs[1] || prs[1] == '0') {
            $('#af')[0].checked = true;
            $(".flClass input[type=number]").val('').attr('disabled', 'disabled');
        }
        else {
            $('#sf')[0].checked = true;
            $('.flnumber').val(parseInt(prs[1]));
            $('.nf').val((prs[2] ? parseInt(prs[2]) : 0));
        }
    }
    else {
        $('.wtRadio[value="0"]').attr('checked', true);
        $(".flClass").find('input').attr('disabled', 'disabled');
    }

    $('.wtRadio').bind('change', function () {
        let val = $(this).val(); $('.nf, .flnumber').css('border', '');
        if (val == '0') {
            if ($(".flClass input[type=radio]:checked")[0]) $(".flClass input[type=radio]:checked")[0].checked = false;
            $(".flClass input").attr('disabled', 'disabled');
            $(".flClass input[type=number]").val('');
        }
        else {
            $(".flClass input[type=radio]").removeAttr('disabled');
            $(".flClass input[type=number]").val('').attr('disabled', 'disabled');
            if ($(".flClass input[type=radio]:checked")[0]) $(".flClass input[type=radio]:checked")[0].checked = false;
            $('.flradio[value="1"]')[0].checked = true;
        }
    });

    $('.flradio').bind('change', function () {
        let val = $(this).val(); $('.nf, .flnumber').css('border', '');
        if (val == '1')
            $(".flClass input[type=number]").val('').attr('disabled', 'disabled');
        else
            $(".flClass input[type=number]").val('').removeAttr('disabled');
    });

    $('.nmtb').bind('keyup', function () {
        if (this.value != "") {
            $('.nf, .flnumber').css('border', '');
            if (parseInt(this.value) < -10) {
                this.value = -10;
            }
            if (parseInt(this.value) > 100) {
                this.value = 100;
            }
        }
    });

    $('#Btn_Save_vector_properties').unbind(touchClickEvent);
    $('#Btn_Save_vector_properties').bind(touchClickEvent, () => {
        if ($(".flClass input[type=radio]:checked")[0]?.value == '2') {
            let v = true;
            if ($('.flnumber').val() == '') { $('.flnumber').css('border', '1px solid red'); v = false; }
            if ($('.nf').val() == '') { $('.nf').css('border', '1px solid red'); v = false; }
            if (!v) return false;
        }

        let wt = parseInt($(".wts input[type=radio]:checked").val());
        if (editor.currentLine?.eNode) {
            editor.currentLine.eNode.lineStrokePattern = wt;
            let lt = editor.currentLine.eNode.proval_line_type, lts = lt ? lt.split('~') : [];
            lts[0] = wt;
            if (wt != '0') {
                if ($(".flClass input[type=radio]:checked")[0]?.value == '2') {
                    lts[1] = $('.flnumber').val();
                    lts[2] = $('.nf').val();
                }
                else {
                    lts[1] = lts[2] = '0';
                }
            }
            else {
                lts[1] = lts[2] = '0';
            }
            editor.currentLine.eNode.proval_line_type = lts.join('~');
        }

        editor.currentVector.isModified = true;
        if (!sketchApp.isScreenLocked) $('.mask').hide();
        $('.Current_vector_details').hide();
        return false;
    });


    $('.Current_vector_details #Btn_cancel_vector_properties').unbind(touchClickEvent)
    $('.Current_vector_details #Btn_cancel_vector_properties').bind(touchClickEvent, () => {
        if (!sketchApp.isScreenLocked) $('.mask').hide();
        $('.Current_vector_details').hide();
        return false;
    });
}

function setMultipleAttributes(element, attributes) {
    Object.keys(attributes).forEach(attr => {
        element.setAttribute(attr, attributes[attr]);
    });
}

function appendMultiple(element, attributes) {
    attributes.forEach(a => {
        if (a['$append']) element.append(a.el);
        else $(element).append(a);
    });
}

function enableFullView() {
    fvEnabled = $('.fv-switch-input')[0].checked ? 'FV' : 'NV';
    sketchApp && sketchApp.render();
}

function blockSpecialChars(e) {
    var e = e || window.event;
    var k = e.which || e.keyCode;
    if (k == 186 || k == 219 || k == 221)
        return false;
}

function showOtherImpDescription(btns, editor, btnData) {
    editor.mvpDisplayLabel = !editor.mvpDisplayLabel;
    if (editor.mvpDisplayLabel) {
        $(btns).html('Hide OI Labels');
        editor.loadVectorSelector($('.ccse-sketch-select')[0], null, null);
    }
    else {
        $(btns).html('Show OI Labels');
        editor.loadVectorSelector($('.ccse-sketch-select')[0], null, null);
    }
    editor.render();
}

function switchToUnsketch(skeditor, cv, switchToSketch) {
    if (switchToSketch) {
        cv.vectorString = cv.startNode = cv.endNode = null;
        cv.commands = []; cv.placeHolder = false;
        skeditor.render()
        skeditor.startNewVector(true);
        cv.fixedArea = cv.areaFieldValue = null;
        cv.isUnSketchedArea = cv.hideAreaValue = cv.isClosed = cv.isMarkedArea = cv.noVectorSegment = cv.CheckFixedArea = false;
    }
    else {
        let xp = cv.startNode.p.x, yp = cv.startNode.p.y;
        cv.isMarkedArea = cv.CheckFixedArea = cv.noVectorSegment = true;
        directlyPlaceUnsketchedSegments(skeditor, cv, xp, yp, true);
    }
}

function drawOBYSketch(skeditor, cv, switchToSketch) {
    messageBox('Tap on any point to start the new sketch segment.', function () {
        if (switchToSketch) {
            cv.vectorString = cv.startNode = cv.endNode = null;
            cv.commands = []; cv.placeHolder = false;
            skeditor.render()
            skeditor.startNewVector(true);
            cv.fixedArea = cv.areaFieldValue = null;
            cv.isUnSketchedArea = cv.hideAreaValue = cv.isClosed = cv.isMarkedArea = cv.noVectorSegment = cv.CheckFixedArea = false;
        }
        else {
            cv.isMarkedArea = cv.CheckFixedArea = cv.noVectorSegment = cv.isUnSketchedArea = skeditor.drawlabelOnly = true;
            cv.hideAreaValue = false;
            skeditor.render()
            skeditor.startNewVector(true);
        }
    });
}

//quickbtn functions start
function _polygon(editor, rect, x, y, radius, sides, rotateAngle) {
    var ctx = editor.drawing;
    _polycoorinates = []
    if (sides < 3) return;
    var a = (Math.PI * 2) / sides;
    var side_len = (2 * radius * Math.sin(Math.PI / sides));
    var angle = (2 / sides * Math.PI);
    ctx.moveTo(x, y);
    var sidesangle = 0;
    var x1;
    var flag = 0;
    var y1 = y;
    _polycoorinates.push({ x: x, y: y });
    if (x < rect.endX) {
        x1 = x + side_len;
        _polycoorinates.push({ x: x1, y: y1 });
        drawDistanceLabel(editor, { x: x1, y: y1 }, { x: x, y: y }, false);

    }
    else {
        x1 = x;
        flag = 1;
    }
    ctx.beginPath();
    ctx.lineTo(x1, y1);
    for (var i = 1; i < sides; i++) {
        if (y < rect.endY)
            sidesangle = sidesangle + angle;
        else
            sidesangle = sidesangle - angle;

        x1 = x1 + (side_len * Math.cos(sidesangle))
        y1 = y1 + (side_len * Math.sin(sidesangle))
        _polycoorinates.push({ x: x1, y: y1 });
        ctx.lineTo(x1, y1);
    }
    if (flag == 1) {
        ctx.lineTo(x, y);
        _polycoorinates.push({ x: x, y: y });

        drawDistanceLabel(editor, { x: x, y: y }, { x: x1, y: y1 }, false);
    }
    else {
        ctx.lineTo(x + side_len, y);
    }
    ctx.restore();
    ctx.setLineDash([6]);
    ctx.stroke();



}

function distanceBetween(p1, p2) {
    return Math.sqrt(Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2))
}

function drawDistanceLabel(editor, p1, p2, round) {

    var dy = p2.y - p1.y;
    var dx = p2.x - p1.x;
    var d = sRound((distanceBetween(p1, p2) / (DEFAULT_PPF * editor.scale)));
    var measurement_coordinate = { x: p1.x + (dx / 2), y: p1.y + (dy / 2) }
    if (round) d = Math.round(d);
    editor.drawing.fillText(d, measurement_coordinate.x, measurement_coordinate.y)
    return false;
}



function _rectangle_draw(editor, rect) {
    var ctx = editor.drawing;
    _polycoorinates = [];
    ctx.beginPath();
    var width = rect.endX - rect.startX;
    var height = rect.endY - rect.startY;
    _polycoorinates.push({ x: rect.startX, y: rect.startY });
    _polycoorinates.push({ x: rect.endX, y: rect.startY });
    _polycoorinates.push({ x: rect.endX, y: rect.endY });
    _polycoorinates.push({ x: rect.startX, y: rect.endY });
    _polycoorinates.push({ x: rect.startX, y: rect.startY });
    ctx.rect(rect.startX, rect.startY, width, height);
    ctx.setLineDash([6]);
    ctx.stroke();
    drawDistanceLabel(editor, { x: rect.startX, y: rect.startY }, { x: rect.startX + width, y: rect.startY }, false);
    drawDistanceLabel(editor, { x: rect.startX, y: rect.startY }, { x: rect.startX, y: rect.startY + height }, false);
}


function _getDirectionInDegrees(p1, p2) {
    /*  var p1 = this; */
    var angle = (Math.atan((p1.y - p2.y) / (p2.x - p1.x)) * 180 / Math.PI);

    if (p1.y == p2.y && p1.x > p2.x) {
        angle = 180;
    } else if (p1.x > p2.x && p1.y > p2.y) {
        angle = 180 + angle;
    } else if (p1.x > p2.x && p1.y < p2.y) {
        angle = 180 + angle;
    } else if (p1.x < p2.x && p1.y < p2.y) {
        angle = 360 + angle;
    } else if (p1.x == p2.x && p1.y < p2.y) {
        angle = 270;
    }
    return angle;
}

function _midpointTo(p1, p2) {
    return {
        x: (p1.x + p2.x) / 2,
        y: (p1.y + p2.y) / 2
    }
}



function _getPointAt(p, degree, distance) {
    distance = Math.abs(distance);
    if (degree >= 360) degree = degree - 360;
    var dH = distance * Math.cos(degree * Math.PI / 180);
    var dV = -distance * Math.sin(degree * Math.PI / 180);

    return {
        x: p.x + dH,
        y: p.y + dV
    };
}

function _getDistanceTo(p1, p2) {
    return Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));
}

function _arcCenter(arcLength, direction, midpoint, radius) {
    if (arcLength == 0) return null;
    var pangle = direction - ARC_SIGN * sign(arcLength) * 90; //Reverse ARC_SIGN here, since theoretical graph and computer graph differs
    if (pangle >= 360) pangle -= 360;
    var rpangle = pangle + 180;
    if (rpangle >= 360) rpangle -= 360;
    return _getPointAt(midpoint, rpangle, radius);
}


function _alignToGrid(point, scale, mathOp) {
    var ppf = DEFAULT_PPF * scale;
    var p = {};
    p.x = mathOp((point.x - sketchApp.origin.x) / ppf);
    p.y = mathOp((sketchApp.origin.y - point.y) / ppf);
    return p;

}

function getCanvasPoint(point, scale) {
    var ppf = DEFAULT_PPF * scale;
    var p = {};
    p.x = (point.x * ppf) + sketchApp.origin.x;
    p.y = (sketchApp.origin.y - (point.y * ppf));
    return p;
}


function _arcPerimeter(p1, p2, mp, arcLength, radius) {
    var perimeter = 0;
    if (arcLength == 0) return length;//
    var direction = _getDirectionInDegrees(p1, p2);
    var c = _arcCenter(arcLength, direction, mp, radius);
    var r = radius
    var pi = Math.PI;
    var halfAngle = _getDirectionInDegrees(c, mp) - _getDirectionInDegrees(c, p1);
    if (halfAngle > 180) halfAngle = halfAngle - 360;
    if (halfAngle < -180) halfAngle = 360 + halfAngle;
    halfAngle = Math.abs(halfAngle);
    var fullAngle = halfAngle * 2;
    perimeter = 2 * pi * r * fullAngle / 360;
    return perimeter
}

function _midPoint(p1, p2, arcLength) {
    var mp = _midpointTo(p1, p2);
    if (arcLength == 0) {
        return mp;
    }
    var angle = _getDirectionInDegrees(p1, p2);
    var pangle = angle - ARC_SIGN * sign(arcLength) * 90; //Reverse ARC_SIGN here, since theoretical graph and computer graph differs
    if (pangle > 360) pangle = pangle - 360;
    mp = _getPointAt(mp, pangle, arcLength);
    return mp;
}

function _newRadius(p1, p2, midpoint, arcLength) {
    if (arcLength == 0) return Infinity;
    var angle = _getDirectionInDegrees(p1, p2);
    var p3 = midpoint;
    var ap1p3 = _getDirectionInDegrees(p1, p3);
    var leftInclusive = Math.abs(ap1p3 - angle);
    if (leftInclusive > 90) leftInclusive = 360 - leftInclusive;
    var topInclusive = 180 - 2 * leftInclusive;
    var dp1p3 = _getDistanceTo(p1, p3);
    return radius = Math.abs((dp1p3 / 2) / Math.cos(topInclusive / 2 * Math.PI / 180));
}



function drawArc(editor, arcLength, p1, p2) {

    _arcCoorinates = [];
    var ctx = editor.drawing;
    var c = _midpointTo(p1, p2);
    var angle = _getDirectionInDegrees(p1, p2);
    var pangle = angle + ARC_SIGN * sign(arcLength) * 90;
    if (pangle > 360) pangle = pangle - 360;
    var p3 = _getPointAt(c, pangle, CAMACloud.Sketching.Utilities.toPixel(arcLength) * editor.scale);
    var ap1p3 = _getDirectionInDegrees(p1, p3);
    var leftInclusive = Math.abs(ap1p3 - angle);
    if (leftInclusive > 90) leftInclusive = 360 - leftInclusive;
    var topInclusive = 180 - 2 * leftInclusive;
    var dp1p3 = _getDistanceTo(p1, p3);
    var radius = Math.abs((dp1p3 / 2) / Math.cos(topInclusive / 2 * Math.PI / 180));

    var rpangle = pangle + 180;
    if (rpangle > 360) rpangle = rpangle - 360;
    var cc = _getPointAt(p3, rpangle, radius);

    var startAngle = (360 - _getDirectionInDegrees(cc, p1))
    var endAngle = (360 - _getDirectionInDegrees(cc, p2))
    var midAngle = (360 - _getDirectionInDegrees(cc, p3));


    var check1 = startAngle - midAngle;
    if (check1 > 180) check1 = check1 - 360;
    if (check1 < -180) check1 = 360 + check1;
    var check2 = midAngle - endAngle;
    if (check2 > 180) check2 = check2 - 360;
    if (check2 < -180) check2 = 360 + check2;
    /*    console.log(dx); */
    ctx.beginPath();
    ctx.arc(cc.x, cc.y, radius, startAngle * Math.PI / 180, endAngle * Math.PI / 180, check1 > 0);
    _arcCoorinates.push(p1)
    _arcCoorinates.push(p2)
    ctx.setLineDash([6]);
    ctx.stroke();

    var startP = _alignToGrid(p1, editor.scale, Math.round);
    var endP = _alignToGrid(p2, editor.scale, Math.round);
    var _p3 = _midPoint(startP, endP, arcLength);
    _drawArcLabelOnMove(editor, ctx, startP, endP, _p3, arcLength, p1, _p3.x - startP.x, _p3.y - startP.y);

}


function _drawArcLabelOnMove(editor, ctx, startP, endP, _p3, arcLength, _currentPoint, mx, my, dx, dy) {
    var unit = (editor.formatter.LengthUnit || DISTANCE_UNIT || '');
    var mpl = new PointX(_currentPoint.x + CAMACloud.Sketching.Utilities.toPixel(mx) * editor.scale, _currentPoint.y - CAMACloud.Sketching.Utilities.toPixel(my) * editor.scale);
    var _radius = _newRadius(startP, endP, _p3, arcLength)
    var Length = sRound(_arcPerimeter(startP, endP, _p3, arcLength, _radius))
    ctx.fillText(Length + unit + "/" + Math.abs(arcLength), mpl.x + 1, mpl.y - 2);

    return false;
}

//quickbtn functions ends

function fieldValidationInSketch(e, item) {
    var e = e || window.event;
    var k = e.which || e.keyCode;
    if (k == 186 || k == 219 || k == 221)
        return false;
    var datafieldss = window.opener.datafields;
    var fieldId = parseInt($(item).attr('field_id')), field = datafieldss[fieldId], itype = parseInt(field.InputType);
    var maxlength = parseInt(field.MaxLength), precision = parseInt(field.NumericPrecision), scale = parseInt(field.NumericScale);
    var str, scaleLength;
    if ([8, 10].indexOf(itype) > -1) {
        precision = precision || maxlength || 20;
        scale = 0;
        if (!(e.keyCode >= 48 && e.keyCode <= 57)) { e.preventDefault(); return false; }
        if (item.value.length >= precision) { e.preventDefault(); return false; };
    }
    else {
        var selLength = item.selectionEnd - item.selectionStart;
        precision = precision || (maxlength - scale - 1);
        maxlength = (precision + (scale || 0) + 1) || 20;
        if (item.value.indexOf('.') > -1 && item.selectionStart > item.value.indexOf('.')) {
            str = item.value.split('.');
            scaleLength = str[1].length;
            if (scaleLength >= scale) { e.preventDefault(); return false; };
        }
        if (!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode == 46) || (e.keyCode == 45))) { e.preventDefault(); return false; }
        if (((e.keyCode == 46)) && item.value && item.value.indexOf('.') > -1) { e.preventDefault(); return false; }
        if (((e.keyCode == 45)) && item.value) { e.preventDefault(); return false; }
        if (selLength == 0 && item.value.length >= maxlength) { e.preventDefault(); return false; };
        if (selLength == 0 && item.value.replace('.', '').length >= precision) { e.preventDefault(); return false; };
    }
}

function getTimeDate() {
    let t = new Date(), d = t.getFullYear() + '-' + (t.getMonth() + 1) + '-' + t.getDate(),
        te = t.getHours() + ":" + t.getMinutes() + ":" + t.getSeconds(), dt = d + '-' + te;
    return dt;
}

function reRenderImageDownload(cScale, currentSketchIndex) {
    sketchApp.showOrigin = true;
    $('.ccse-sketch-select')[0].selectedIndex = currentSketchIndex;
    $('.ccse-sketch-select').trigger('change');
    sketchApp.zoom(cScale);
    setTimeout(() => { $('.ccse-canvas').removeClass('dimmer'); $('.dimmer').hide(); }, 400);
}

function sketchPreviewGeneration(imgType, index) {
    let url = "data:image/png;base64,R0lGODlhFAAUAIAAAP///wAAACH5BAEAAAAALAAAAAAUABQAAAIRhI+py+0Po5y02ouz3rz7rxUAOw==",
        cScale = sketchApp.scale, type = (imgType == 'png') ? 'image/jpeg' : 'image/png';

    $('.ccse-canvas').addClass('dimmer'); $('.app-mask-layer-info').html("Please wait..."); $('.dimmer').show();
    sketchApp.showOrigin = false;
    $('.ccse-sketch-select')[0].selectedIndex = index; $('.ccse-sketch-select').trigger('change');
    url = sketchApp.toDataURL(type, 1.0);
    setTimeout(() => { reRenderImageDownload(cScale, index); }, 100);
    return url;
}

function imageDownload(imgType) {
    let index = $('.ccse-sketch-select')[0].selectedIndex;
    if (index > 0 && window?.opener) {
        $('.ccse-canvas').addClass('dimmer'); $('.app-mask-layer-info').html("Please wait..."); $('.dimmer').show();
        //let newUrl = window.opener.sketchPreviewGenerations(imgType, index - 1), a = document.createElement("a"), imgName = (sketchApp.currentSketch && sketchApp.currentSketch.label) ? (sketchApp.currentSketch.label + '-' + getTimeDate()) : getTimeDate();
        let newUrl = (window.opener && window.opener.appType != 'sv') ? window.opener.sketchPreviewGenerations(imgType, index - 1) : window.opener.mapFrame.contentWindow.sketchPreviewGenerations(imgType, index - 1);
        let a = document.createElement("a"), imgName = (sketchApp.currentSketch && sketchApp.currentSketch.label) ? (sketchApp.currentSketch.label + '-' + getTimeDate()) : getTimeDate();
        a.href = newUrl;
        a.download = imgName + "." + imgType; $(a).addClass('sketchImageDownload');
        document.body.appendChild(a);
        a.click(); document.body.removeChild(a);
        setTimeout(() => { $('.ccse-canvas').removeClass('dimmer'); $('.dimmer').hide(); }, 400);
    }
}

function caseInsensitiveCompare(value, searchValue) {
    if (typeof value === 'string' && typeof searchValue === 'string') {
        return value.toLowerCase() === searchValue.toLowerCase();
    }
    return false;
}

function hideOrShowManualBox() {
    $('.manual-entry-container').hide();
    if (sketchApp.mode == 0 && sketchApp.config?.enableManualAreaEntry) {
        $('.ccse-mode-manual-sketch').css({
            'display': "inline-block"
        });
    }
    else if (sketchApp.mode == 2 && sketchApp.currentVector && !sketchApp.currentSketchNote && !sketchApp.currentNode && !sketchApp.currentLine && checkSketchDetailView(sketchApp.currentVector)) {
        $('.ccse-mode-sketch-details').css({
            'display': "inline-block"
        });
    }
}

function convertHelionToSketchPro(sketchStr, sketchLbl, lblList) {
    let sktPro = {
        "$schema": "https://schemas.opencamadata.org/0.10/data.schema.json",
        "sketches": [
            {
                "id": 1,
                "label": sketchLbl,
                "segments": [],
                "notes": []
            }
        ],
        "lookupCollection": {
            "standard": {
                "unspecified": {

                }
            }
        }
    };

    let parts = (sketchStr || '').split(";"), k = 0, li = 1, lv = 0, nc = 1;

    for (var i in parts) {
        let part = parts[i]; k = k + 1;
        if (/(.*?):(.*?)/.test(part)) {
            let hi = part.split(':'), head = hi[0], info = hi[1], r1 = /(.*?)\[(.*?)\]/, hparts = r1.exec(head), labels = '', labelText = '', labelDesc = '', ad1 = '', ad2 = '';

            if (head.split('[')[0].trim() == 'Text') {
                let nd = null;
                try {
                    nd = JSON.parse(Base64.decode(hparts[2]));
                }
                catch (ex) { }

                if (nd && info) {
                    let sl = info.split(' '), x = 0, y = 0;
                    for (s in sl) {
                        if (sl[s]?.trim() != '') {
                            let cp = sl[s].trim().match(/[UDLR]|[0-9]+(.[0-9]+)?/g), dir = cp[0], dis = cp[1] || 0;
                            switch (dir) {
                                case "U": y = parseFloat(dis); break;
                                case "D": y = parseFloat(-dis); break;
                                case "L": x = parseFloat(-dis); break;
                                case "R": x = parseFloat(dis); break;
                            }
                        }
                    }

                    sktPro["sketches"][0]["notes"].push({ "keycode": nc.toString(), "text": nd.Text, "position": { "x": x, "y": y }, "page": 1 });
                    nc = nc + 1;
                }
                continue;
            }

            if (hparts != null) {
                labels = hparts[1].replace(/\{(.*?)\}/g, '');
            }

            if (!labels || labels == "") {
                labels = labelText = labelDesc = "MISSING";
            }
            else {
                let lookupdata = window.opener ? window.opener.lookup : lookup, lk = lookupdata['SKETCH_CODES']?.[labels];
                if (lk) {
                    labelText = lk.Name;
                    labelDesc = lk.Description;
                    ad1 = lk.AdditionalValue1;
                    ad2 = lk.AdditionalValue2;
                }
                else {
                    labelText = labels;
                    labelDesc = labels;
                }
            }

            if (info) {
                let ssp = info.split("S"), sps = ssp[0].trim().split(" "), lps = ssp[1].trim().split(" "), sx = 0, sy = 0, ring = { "commands": [] }, segment = {
                    "id": k,
                    "label": {
                        "text": labelText,
                        "values": [
                            {
                                "text": labelText,
                                "lookupCode": labels,
                                "lookupName": "standard"
                            }
                        ],
                        "position": lblList[lv]
                    },
                    "vectors": [{
                        "label": {
                            "text": labelText,
                            "values": [
                                {
                                    "text": labelText,
                                    "lookupCode": labels,
                                    "lookupName": "standard"
                                }
                            ],
                            "position": lblList[lv]
                        },
                        "areaType": "positive",
                        "drawingType": "sketched",
                        "area": {},
                        "perimeter": {},
                        "id": k
                    }
                    ],
                    "boundarySize": 100,
                    "page": 1,
                    "area": {},
                    "perimeter": {},
                    "domain": "unspecified"
                };

                lv = lv + 1;

                for (s in sps) {
                    if (sps[s]?.trim() != '') {
                        let cp = sps[s].trim().match(/[UDLR#]|[0-9]+(.[0-9]+)?/g);
                        let dir = cp[0], distance = cp[1] || 0;
                        switch (dir) {
                            case "U": sy = parseFloat(distance); break;
                            case "D": sy = parseFloat(-distance); break;
                            case "L": sx = parseFloat(-distance); break;
                            case "R": sx = parseFloat(distance); break;
                        }
                    }
                }
                ring["origin"] = { "x": sx, "y": sy };

                for (var j in lps) {
                    let ns = lps[j];
                    if (ns.trim() != '') {
                        let cmds = ns.split('/'), xx = 0, yy = 0;
                        for (var c in cmds) {
                            let cmd = cmds[c];
                            if (cmd == '') continue;
                            let cp = cmd.match(/[ABUDLR#]|[0-9]+(.[0-9]+)?/g);
                            let dir = cp[0], distance = cp[1] || 0;
                            switch (dir) {
                                case "U": yy = parseFloat(distance); break;
                                case "D": yy = parseFloat(-distance); break;
                                case "L": xx = parseFloat(-distance); break;
                                case "R": xx = parseFloat(distance); break;
                            }
                        }
                        ring["commands"].push({ "line": { "x": xx, "y": yy } });
                    }
                }
                segment["vectors"][0]["ring"] = ring;
                sktPro["sketches"][0]["segments"].push(segment);

                if (!sktPro["lookupCollection"]["standard"]["unspecified"][labels]) {
                    sktPro["lookupCollection"]["standard"]["unspecified"][labels] = {
                        "name": labelText,
                        "description": labelDesc,
                        "ordinal": li,
                        "attributes": [
                            {
                                "key": "AdditionalValue1",
                                "value": ad1
                            },
                            {
                                "key": "AdditionalValue2",
                                "value": ad2
                            }
                        ]
                    }
                    li = li + 1;
                }
            }
        }
    }

    return JSON.stringify(sktPro);
}

function checkSketchDetailView(cv) {
    let v = false;
    if ((sketchSettings?.CAMAFieldToDisplayOnSketch1 && sketchSettings?.CAMAFieldToDisplayOnSketch1 != "") || (sketchSettings?.CAMAFieldToDisplayOnSketch2 && sketchSettings?.CAMAFieldToDisplayOnSketch2 != "") || (sketchSettings?.CAMAFieldToDisplayOnSketch3 && sketchSettings?.CAMAFieldToDisplayOnSketch3 != "") || (sketchSettings?.CAMAFieldToDisplayOnSketch4 && sketchSettings?.CAMAFieldToDisplayOnSketch4 != "") || (sketchSettings?.CAMAFieldToDisplayOnSketch5 && sketchSettings?.CAMAFieldToDisplayOnSketch5 != "")) {
        let vtTbl = cv.vectorConfig?.Table, skTble = cv.sketch.config?.SketchSource?.Table;
        if (vtTbl && vtTbl != skTble) v = true;
    }

    return v;
}

function getSectionNumHtml(editor) {
    if (!(editor?.currentSketch?.config?.AllowSectNum)) {
        return { secthtml: "", sectRecords: 0 };
    }

    var parcel = window.opener.activeParcel;
    var lookupDropDown = window.opener ? window.opener.lookup : lookup;
    var getDataFieldValues = window.opener ? window.opener.getDataField : getDataField;
    var _ctsktsettconfig = clientSettings.SketchConfig || sketchSettings.SketchConfig;
    var isBristol = (((_ctsktsettconfig == 'Vision6_5') || (_ctsktsettconfig == 'Vision8XML') || (_ctsktsettconfig == 'Vision8XMLNew')) ? true : false);
    var sectTables = {}, parentTable = 'CCV_BLDG_CONSTR', sectTable = '', areaTable = '', sectRecords = [], secthtml = '';
    if (isBristol)
        sectTables = { Residential: 'CCV_CONSTR_RESDEP', Commercial: 'CCV_CONSTR_COMDEP', CondoMain: 'CCV_CONSTR_CDMDEP', CondoUnit: 'CCV_CONSTR_CDUDEP' }
    else
        sectTables = { Residential: 'CCV_CONSTRSECTION_RES', Commercial: 'CCV_CONSTRSECTION_COM', CondoMain: 'CCV_CONSTRSECTION_CDM', CondoUnit: 'CCV_CONSTRSECTION_CDU' }

    var currentSid = editor.currentSketch.sid;
    var parentRecord = parcel[parentTable].filter(function (par) { return par.ROWUID == currentSid && par.CC_Deleted != true })[0];
    if (parcel[sectTables.Residential].filter(function (res) { return (res.ParentROWUID == parentRecord.ROWUID) && res.CC_Deleted != true }).length > 0)
        sectTable = sectTables.Residential;
    else if (parcel[sectTables.Commercial].filter(function (comm) { return (comm.ParentROWUID == parentRecord.ROWUID) && comm.CC_Deleted != true }).length > 0)
        sectTable = sectTables.Commercial;
    else if (parcel[sectTables.CondoMain].filter(function (comn) { return (comn.ParentROWUID == parentRecord.ROWUID) && comn.CC_Deleted != true }).length > 0)
        sectTable = sectTables.CondoMain;
    else if (parcel[sectTables.CondoUnit].filter(function (conut) { return (conut.ParentROWUID == parentRecord.ROWUID) && conut.CC_Deleted != true }).length > 0)
        sectTable = sectTables.CondoUnit;
    if (sectTable != '') {
        sectRecords = parcel[sectTable].filter(function (st) { return (st.ParentROWUID == parentRecord.ROWUID) && st.CC_Deleted != true });
        secthtml += '<div class="SectNumDropDown" style="padding-left: 22px;"><span>Section ID</span>';
        secthtml += '<select class="SectNum">';
        var Style_Id = getDataFieldValues('CNS_STYLE', sectTable) ? getDataFieldValues('CNS_STYLE', sectTable).Id : '';
        var Style_field = '#FIELD-' + Style_Id;
        var Occ_Id = getDataFieldValues('CNS_OCC', sectTable) ? getDataFieldValues('CNS_OCC', sectTable).Id : '';
        var Occ_field = '#FIELD-' + Occ_Id;
        sectRecords.forEach(function (sRecod) {
            var Style_desc = '', Occ_desc = '', isNewRecord = false;
            if (sRecod.CC_RecordStatus == "I")
                isNewRecord = true;
            var SECT_NUMS = (sRecod.SECT_NUM && sRecod.SECT_NUM != '') ? sRecod.SECT_NUM : '---';
            var CNS_STYLES = sRecod.CNS_STYLE ? sRecod.CNS_STYLE : '---';
            Style_desc = (CNS_STYLES != '---') ? ((lookupDropDown[Style_field] && lookupDropDown[Style_field][CNS_STYLES]) ? lookupDropDown[Style_field][CNS_STYLES].Description : CNS_STYLES) : CNS_STYLES;
            var CNS_OCCS = sRecod.CNS_OCC ? sRecod.CNS_OCC : '---';
            Occ_desc = (CNS_OCCS != '---') ? ((lookupDropDown[Occ_field] && lookupDropDown[Occ_field][CNS_OCCS]) ? lookupDropDown[Occ_field][CNS_OCCS].Description : CNS_OCCS) : CNS_OCCS;
            var disValue = SECT_NUMS + '/' + Style_desc + '/' + Occ_desc;
            SECT_NUMS = ((!isNewRecord && SECT_NUMS != '---') || (editor?.config?.ManualSection && SECT_NUMS != '---')) ? SECT_NUMS : ('&%' + sectTable + '{' + sRecod.ROWUID + '}&%');
            secthtml += '<option value="' + SECT_NUMS + '" value1="' + sRecod.ROWUID + '" value2="' + sectTable + '">' + disValue + '</option>';
        });
        secthtml += '</select></div>';
    }

    return { secthtml, sectRecords: sectRecords.length };
}

function createCustomNote(noteMaxLen, customHtml, customParameters, callback) {
    if (noteMaxLen) $('.note-textarea').attr("maxlength", noteMaxLen);

    $('.custom-note-options').html(''); $('.note-textarea').val('');
    if (customHtml) $('.custom-note-options').html(customHtml);

    $('.note-textarea').unbind('keydown');
    $('.note-textarea').bind('keydown', function (e) {
        $('.note-textarea').css('border-color', '');
    });

    if (customParameters) {
        if (customParameters.specialKeys) {
            let noteSpecialKeys = customParameters.specialKeys;
            $('.note-textarea').unbind('keydown');
            $('.note-textarea').bind('keydown', function (e) {
                $('.note-textarea').css('border-color', '');
                var e = e || window.event;
                var k = e.which || e.keyCode;
                if (noteSpecialKeys) {
                    var ntspecial = noteSpecialKeys.filter(function (x) { return x == k })[0];
                    if (ntspecial && e.key != '3' && e.key != '.' && e.key != '4' && (ntspecial != 220 || (ntspecial == 220 && e.key == '|')))
                        return false;
                }
            });
        }

        if (customParameters.visionNote) {
            $('.wh-div input').unbind('keydown');
            $('.wh-div input').bind('keydown', function (event) {
                let key = event.key;
                if (/[0-9\.]|Backspace|Delete/.test(key)) {
                    return true;
                }
                event.preventDefault();
                return false;
            });
            if (customParameters.secthtml != '') {
                $('.custom-note-options .SectNumDropDown').css({ 'padding-left': '' });
                $('.custom-note-options .SectNum').unbind('change');
                $('.custom-note-options .SectNum').bind('change', () => {
                    $('.custom-note-options .SectNum').css('border-color', '');
                });
            }
        }
    }

    $('.customNote').show();

    $('.note-ok').unbind(touchClickEvent);
    $('.note-ok').bind(touchClickEvent, () => {
        let noteText = $('.note-textarea').val(), customValues = { width: 10, height: 10, sectNum: -1 };
        if (noteText == '') {
            $('.note-textarea').css('border-color', 'red'); return false;
        }

        if (customParameters.visionNote) {
            if (customParameters.secthtml != '') {
                if ($('.custom-note-options .SectNum').val() == '') {
                    $('.custom-note-options .SectNum').css('border-color', 'red'); return false;
                }
                customValues.sectNum = $('.custom-note-options .SectNum').val();
            }

            if ($('.wh-div input')[0].value != '' && parseInt($('.wh-div input')[0].value) > 0 && parseInt($('.wh-div input')[0].value) < 100) customValues.width = parseInt($('.wh-div input')[0].value);
            if ($('.wh-div input')[1].value != '' && parseInt($('.wh-div input')[1].value) > 0 && parseInt($('.wh-div input')[1].value) < 100) customValues.height = parseInt($('.wh-div input')[1].value);
        }

        $('.customNote').hide();
        if (callback) callback(noteText, customValues);
        return false;
    });

    $('.note-cancel').unbind(touchClickEvent);
    $('.note-cancel').bind(touchClickEvent, () => {
        $('.customNote').hide();
        return false;
    });
}

function rsSketchAddPage(editor) {
    let nsketch = new Sketch(editor), parcel = window.opener ? window.opener.activeParcel : activeParcel,
        getDataField = window.opener ? window.opener.getDataField : getDataField;

    nsketch.uid = ccTicks();
    nsketch.sid = nsketch.uid;
    nsketch.config = editor.config ? editor.config.sources[0] : {};
    nsketch.label = 'Account' + (getDataField('ACCOUNT_ID', 'CC_APPRAISAL_DRAWING') ? ('/' + parcel.ACCOUNT_ID) : '');
    nsketch.rsNewPage = nsketch.isModified = true;
    editor.sketches.push(nsketch);
    editor.refreshControlList();
    $(editor.sketchSelector)[0].selectedIndex = editor.sketches.length;
    $(editor.sketchSelector).trigger('change');
    editor.currentVector = null;
    editor.mode = CAMACloud.Sketching.MODE_DEFAULT;
    editor.raiseModeChange();
    editor.render();
    $('.ccse-add-page').removeClass('ccse-add-page-highlight');
}