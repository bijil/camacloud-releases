﻿var TableHierarchical = [];
var activeTab, scrolled = 0;
var activeParcel = {};
var sketchRenderer = {};
var sketchApp = {};
var clientSettings = {};
var sketchSettings = {};
var filterValues = {};
var filterfieldType;
var includeReviewedparcels = false;
var ccma = { Data: { Evaluable: {} } };
function initApplication() {
    showMask();
    loadClientSettings(function () {
        loadFilterValue(function () {
            initInputFields(function () {
                if (clientSettings.SketchConfig != null)
                    ccma.Sketching.Config = eval('window.opener.CAMACloud.Sketching.Configs.' + clientSettings.SketchConfig);
                else
                    ccma.Sketching.Config = CAMACloud.Sketching.Configs.GetConfigFromSettings();
                if (clientSettings.SketchFormat)
                    ccma.Sketching.SketchFormatter = eval('window.opener.CAMACloud.Sketching.Formatters.' + clientSettings.SketchFormat);
ccma.Sketching.Config = CAMACloud.Sketching.Configs.GetConfigFromSettings();                if (!ccma.Sketching.SketchFormatter)
                    if (ccma.Sketching.Config.formatter)
                        ccma.Sketching.SketchFormatter = eval('window.opener.CAMACloud.Sketching.Formatters.' + ccma.Sketching.Config.formatter);
				if (clientSettings && clientSettings["sketchOriginPosition"])
        			ccma.Sketching.SketchFormatter.originPosition = clientSettings["sketchOriginPosition"];
                sketchRenderer = new CCSketchEditor('.ccse-img', ccma.Sketching.Config.formatter);
                sketchApp = new CCSketchEditor('.ccse-canvas', ccma.Sketching.Config.formatter);

                $(window).bind('resize', setScreenDimensions);
                setScreenDimensions();

                hideMask();

            });
        });
    });
}

function setScreenDimensions() {
    $('.left-panel').height($(window).height() - 3);
    $('.sketch-panel').height($(window).height() - $('.control-panel').height())
    $('#sketch').css({ "height": $(window).height() - $('.control-panel').height() - 10 })
    $('.parcel-list').height($(window).height() - 3 - $('.filter-panel').height() - 60);
}

function Parcel(p) {
    var points = [];
    var bounds = new google.maps.LatLngBounds();
    this.Id = 0;
    this.KeyValue1 = "";
    this.KeyValue2 = "";
    this.KeyValue3 = "";
    this.StreetAddress = "";
    this.SketchUrl = "";
    this.Latitude = 0.0;
    this.Longitude = 0.0;
    this.ReviewedBy = "";
    this.ReviewedDate = "";
    this.ColorCode = "";
    this.OutBuildings = [];

    this.CopyFrom = function (obj) {
        for (key in obj) {

            eval('this["' + key + '"] = obj["' + key + '"];');

        }
    };

    this.Points = function () { return points; }
    this.Bounds = function () { return bounds; }
    this.Center = function () { return bounds.getCenter(); }

    this.ShowOnMap = function (locate) {
        if (locate == null)
            locate = true;
        if (map) {
            var polyOptions = {
                path: this.Points(),
                strokeColor: this.ColorCode,
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: this.ColorCode,
                fillOpacity: 0.1,
                visible: true,
                clickable: false
            }
            if (parcelPolygon == null) {
                //parcelPolygon = new google.maps.Polygon(polyOptions);
            }
            else {
                if (parcelPolygon.setOptions)
                    parcelPolygon.setOptions(polyOptions);
            }
            parcelPolygon.setMap(map);
            map.setMapTypeId('CountyImagery');
            if (locate == true)
                this.LocateOnMap();
        }
    }

    this.LocateOnMap = function () {
        map.setCenter(this.Center());
        map.fitBounds(this.Bounds());
    }

    this.CopyFrom(p);
    for (x in p._mapPoints) {
        var pt = p._mapPoints[x];
        latlng = new google.maps.LatLng(pt.Latitude, pt.Longitude);
        points.push(latlng);
        bounds.extend(latlng);
    }
}
$.extend(Parcel.prototype, ccma.Data.Evaluable);
$.extend(newParcel.prototype, ccma.Data.Evaluable);

function LoadAuxDataPRC(activeParcel, tableListGlobal, callback) {
    TableHierarchical = [];
    tableListGlobal.forEach(function (name) {
        var no = window.opener.LoadTableHierarchical(name);
        if (no > 1) {
            TableHierarchical.push({ 'tableName': name, 'no': no });
        }
    });
    TableHierarchical.sort(function (a, b) {
        return a.no > b.no;
    })
    if (callback) callback(activeParcel);
}

function loadPRCactiveParcel(tablelist, parcel, callback) {
    if (tablelist.length == 0) {
        if (callback) callback(activeParcel);
        return;
    }
    var sourceTable = tablelist.pop().tableName;
    if (sourceTable === undefined) {
        if (callback) callback(activeParcel);
        return;
    }
    var childvalue = eval('parcel.' + sourceTable);
    if (childvalue != undefined && childvalue.length == 0) {
        // eval('delete parcel[sourceTable]');
        loadPRCactiveParcel(tablelist, parcel, callback);
        return;
    }
    var parenttable = window.opener.parentChild.filter(function (s) { return s.ChildTable == sourceTable })[0].ParentTable;
    var Parentvalue = eval('parcel.' + parenttable);
    var filterfields = window.opener.fieldCategories.filter(function (x) { return x.SourceTable == sourceTable }).map(function (p) { return p.FilterFields })[0]// fieldCategories.filter(function (x) { return x.SourceTable == sourceTable });
    if (filterfields && filterfields != "") { filterfields = filterfields.split(','); } else { filterfields = [] }
    if (Parentvalue) {
        Parentvalue.forEach(function (x) {
            var filterCondition = '';
            var iter_value = 0;
            while (iter_value < filterfields.length) {
                var childFilter, parentFilter; parentFilter = childFilter = filterfields[iter_value];
                if (childFilter.indexOf('/') > -1) { parentFilter = childFilter.split('/')[0]; childFilter = childFilter.split('/')[1]; }
                var values = eval("x." + parentFilter);
                filterCondition += 'd.' + childFilter + ' == \'' + values + '\'';
                iter_value += 1;
                if (iter_value < filterfields.length) {
                    filterCondition += ' && ';
                }
            }
            if (filterCondition == '') filterCondition = '1==1'
            var parentROWUID = x.ROWUID;

            var fiteredSource = eval('childvalue.filter(function (d, i) { return ((!d.ParentROWUID &&(' + filterCondition + ')) || (d.ParentROWUID && d.ParentROWUID == ' + parentROWUID + '))})');
            fiteredSource.forEach(function (cc) {
                cc["parentRecord"] = x
                cc["ParentROWUID"] = parentROWUID;
            })
            x[sourceTable] = fiteredSource;
        });
    }
    //eval('delete parcel[sourceTable]');
    loadPRCactiveParcel(tablelist, parcel, callback);
}

function applyParcelChanges(callback) {
    for (x in activeParcel.ParcelChanges) {
        var a = activeParcel.ParcelChanges[x];
        var oldValue = a.OriginalValue;
        var value = a.NewValue ? a.NewValue : null;
        if (a.Action != 'delete') {
            if ((a.AuxROWUID == null || (activeParcel[a.FieldName] !== undefined && activeParcel['ROWUID'] == a.AuxROWUID)) && a.FieldName) {
                activeParcel[a.FieldName] = value;
                activeParcel.Original[a.FieldName] = oldValue;
                //            activeParcelData[a.FieldName].Value = value;
                //            activeParcelData[a.FieldName].Original = oldValue;
            } else {
                var source = a.SourceTable;
                for (ex in activeParcel[source]) {
                    var acx = activeParcel[source][ex];
                    if (acx.ROWUID == a.AuxROWUID && a.FieldName) {
                        // activeParcelData[source][ex][a.FieldName].Original = oldValue;
                        // activeParcelData[source][ex][a.FieldName].Value = value;
                        activeParcel[source][ex][a.FieldName] = value;
                        activeParcel.Original[source][ex][a.FieldName] = oldValue;
                    }
                }
            }
        }
    };
    if (callback) callback();
}

function searchParcels() {
    var parcelno = $('#parcelNo').val();
    var assnGroup = $('#assignmentGroup').val();
    var reviewedFrom = $('#reviewedFrom').val();
    var reviewedTo = $('#reviewedTo').val();
    var reviwedBy = $('#reviewedBy').val();
    var dtrStatus = $('#dtrStatus').val();
    var curDate = new Date();
    var isoCurDate = curDate.toISOString();
    var CurFormattedDate = isoCurDate.slice(0, 10);
    if ((reviewedFrom === '' && reviewedTo !== '') || (reviewedFrom !== '' && reviewedTo === '')) {
        alert("Please enter both 'Reviewed From' and 'Reviewed To' dates or leave both empty.");
        return false;
    }
    if ((reviewedFrom > reviewedTo) || (reviewedTo > CurFormattedDate)) {
        alert('Invalid date range, please correct the date range');
        return false;
    }
    includeReviewedparcels = $('#includeReviewed').attr('checked') == 'checked';
    var tempSketchFields = [];
    ccma.Sketching.Config.sources.forEach(function (s) {
        tempSketchFields.push(s.VectorSource.map(function (v) { return v.Table + ':' + v.CommandField }).reduce(function (x, y) { return x + ',' + y }))
    });
    var sketchFields = tempSketchFields.reduce(function (x, y) { return x + ',' + y });
        //.map(function (x) { return "'" + x.VectorSource.Table + ":" + x.VectorSource.CommandField + "'" }).reduce(function (x, y) { return x + ',' + y });

    $('.sketch-panel').hide();
    $('.select-sketch-segments').find('option').remove();
    showMask();
    activeParcel = {};
    $('.parcel-list').empty();
    $.ajax({
        url: '/sketchview/searchparcelids.jrq',
        data: { parcelno: parcelno, assnGroup: assnGroup, reviewedBy: reviwedBy, reviewedFrom: reviewedFrom, reviewedTo: reviewedTo, includeReviewedparcels: includeReviewedparcels, dtrStatus: dtrStatus, sketchFields: sketchFields },
        dataType: 'json',
        success: function (data, status) {
            if (data && data.length > 0) {
                var tb = document.createElement('Div')
                $(tb).addClass('parcel')
                $(tb).html('<div><table class="${SVRClass} sr" value="${Id}" onclick="return showSketch(${Id})"><tr><td>${KeyValue1}</td></tr></table></div>');
                $(tb).fillTemplate(data);
                $('.parcel-list').empty();
                $('.parcel-list').append(tb);
                //$('.parcel-list').css('height', '220px');
                $('.parcel-list').show();
                hideMask();
            }

            else {
                $('.parcel-list').empty();
                $('.parcel-list').html('<h3 style="text-align: center; color: black"><b>No Parcels found!</b></h3>');
                //$('.parcel-list').css('height', '60px');
                $('.parcel-list').show();
                hideMask();
            }

            $('.control-panel').hide();
            $('.sketch-panel').hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $('.parcel-list').hide();
            showMask('', 'Invalid details');
            console.error(xhr, ajaxOptions, thrownError);
        }
    });
}

function showSketch(parcelId) {
    $('.parcel-list table').removeClass('selected')
    $('.parcel-list table[value="' + parcelId + '"]').addClass('selected');
    $('.select-sketch-segments').find('option').remove();
    showMask();
    getParcel(parcelId, function () {
        var sketchUrl = '';
        if (activeParcel.SketchViewReviewed)
            $('.markAsReview').html('Unmark Completion Flag')
        else
            $('.markAsReview').html('Mark Sketch As Completed')

        $.extend(activeParcel.prototype, ccma.Data.Evaluable);
        $('.markAsReview').show();
        $('.navigation').show();
        previewSketch('black');
        setScreenDimensions();
        $('.control-panel').show();
        $('.sketch-panel').show();

        $('[field]').each(function () {
            var field = $(this).attr('field');
            $(this).html(activeParcel[field]);
        })

        //$('.parcel-key-value').html(activeParcel.KeyValue1);
        //$('.neighborhood').html(activeParcel.AssignmentGroup);
        //$('.streetAddress').html(activeParcel.StreetAddress);
        $('.parcel-details').show();
        hideMask();
    }, function () {
        showMask('', 'Error');
    });
}
function newParcel(data) {
    _.extend(this, data);
}

function bindSketchSegment(sk) {
    var sketches = [];
    var index = $(".select-sketch-segments option:selected").index();
    if (sk)
        sketches = sk
    else
        sketches = getSketchSegments();
    var formatter = ccma.Sketching.SketchFormatter;
    var config = ccma.Sketching.Config;
    var showAll = $('.select-sketch-segments').val();
    if (!formatter)
        formatter = CAMACloud.Sketching.Formatters.TASketch;

    sketchRenderer.formatter = formatter;
    sketchRenderer.showOrigin = false;
    sketchRenderer.open(sketches, null, false, 'Black', 'Black');
    var url = "";
    sketchRenderer.clearCanvas();
    if (index == 0 || index > 0 && showAll != 'all') {
        if (sketchRenderer.sketches[index] != undefined || '') {
            sketchRenderer.currentSketch = sketchRenderer.sketches[index];
            var ab = sketchRenderer.sketchBounds();
            sketchRenderer.resizeCanvas((ab.xmax - ab.xmin) * DEFAULT_PPF + 95, (ab.ymax - ab.ymin) * DEFAULT_PPF + 95);
            sketchRenderer.pan(sketchRenderer.formatter.originPosition == "topRight" ? -ab.xmax * DEFAULT_PPF + 15 : -ab.xmin * DEFAULT_PPF + 15, (sketchRenderer.formatter.originPosition == "topRight" || sketchRenderer.formatter.originPosition == "topLeft") ? -ab.ymax * DEFAULT_PPF + 15 : -ab.ymin * DEFAULT_PPF + 15);

            for (var x in sketchRenderer.sketches[index].vectors) {
                sketchRenderer.sketches[index].vectors[x].render();
            }
            if (sketchRenderer.displayNotes) sketchRenderer.sketches[index].renderNotes();
        }
    }
    activeParcel.SketchPreview = sketchRenderer.toDataURL();
    if (sketchRenderer.toDataURL())
        url = sketchRenderer.toDataURL();
    $('.sketch-panel').css({ 'background-image': 'url(' + url + ')' });
}

function initInputFields(callback) {
    $('.parcel-details').hide();
    $('.navigation.prev').unbind('click');
    $('.navigation.prev').bind('click', function (e) {
        e.preventDefault();
        var elem = $('table.selected').parent().prev().children()
        if (elem && elem.length) {
            $('table.selected').removeClass('selected')
            $(elem).addClass('selected');
            showSketch($(elem).attr('value'));
            scrolled = scrolled - 25;
            $(".parcel-list").animate({
                scrollTop: scrolled
            });
        }
    });

    $('.navigation.next').unbind('click');
    $('.navigation.next').bind('click', function (e) {
        e.preventDefault();
        var elem = $('table.selected').parent().next().children()
        if (elem && elem.length) {
            $('table.selected').removeClass('selected')
            $(elem).addClass('selected');
            showSketch($(elem).attr('value'));
            scrolled = scrolled + 25;
            $(".parcel-list").animate({
                scrollTop: scrolled
            });
        }
    });

    $(".markAsReview").unbind('click');
    $(".markAsReview").bind('click', function (e) {
        e.preventDefault();
        $.ajax({
            url: '/sketchview/reviewsketch.jrq',
            data: { parcelId: activeParcel.Id, sketchViewReviewed: activeParcel.SketchViewReviewed },
            dataType: 'json',
            success: function (resp) {
                if (activeParcel.SketchViewReviewed) {
                    $('.sr[value="' + activeParcel.Id + '"]').removeClass('b');
                }
                else
                    $('.sr[value="' + activeParcel.Id + '"]').addClass('b')
                showSketch(activeParcel.Id);
            },
            error: function (resp) {
                console.error(resp);
            }
        });
    });

    $('.markAsReview').hide();
    $('.navigation').hide();

    if (callback) callback();
}

function showReviewedParcels() {
    if ($('#includeReviewed').attr('checked') && $('#includeReviewed').attr('checked') == 'checked')
        includeReviewedparcels = true;
    else
        includeReviewedparcels = false;
    //searchParcels();
}

function loadClientSettings(callback) {
    $.ajax({
        url: '/sketchview/getclientsettings.jrq',
        data: {},
        dataType: 'json',
        success: function (resp) {
            for (x in resp)
                clientSettings[resp[x].Name] = resp[x].Value;
            if (callback) callback();
        },
        error: function (resp) {
            console.error(resp);
        }
    });
    return false;
}

function processFilterValueField() {
    bindFilterValue();
    var input = document.createElement('input');
    switch ($('.filter-field').val()) {
        case "0": break;
        case "1": $(input).attr('type', 'text');
            $($('.filter-value.value1')).append(input);
            $('.filter-value input').css('width', '245px');
            filterfieldType = 'text';
            break;
        case "2": break;
        case "3": $(input).attr('type', 'date');
            $($('.filter-value')).append(input);
            $('.filter-value input').css('width', '245px');
            filterfieldType = 'date';
            break;
        case "4": break;
    }

    $('.filter-value').show();
}

function bindFilterValue() {
    var fillData = {};
    var selNbhd = document.createElement('select');
    $(selNbhd).attr('id', 'assignmentGroup');
    var selRevBy = document.createElement('select');
    $(selRevBy).attr('id', 'reviewedBy');

    var selDTRStatus = document.createElement('select');
    $(selDTRStatus).attr('id', 'dtrStatus');


    if (!_.isEmpty(filterValues.Neighborhood)) {
        $(selNbhd).html('<option value = ${Id}>${Name}</option>');
        $(selNbhd).fillTemplate(filterValues.Neighborhood);
        $(selNbhd).prepend('<option value="all" selected="selected">--All--</option>');
        $($('.assignmentGroup')).append(selNbhd);
    }

    if (!_.isEmpty(filterValues.UserSettings)) {
        $(selRevBy).html('<option value = ${Id}>${Name}</option>');
        $(selRevBy).fillTemplate(filterValues.UserSettings);
        $(selRevBy).prepend('<option value="all" selected="selected">--All--</option>');
        $($('.reviewedBy')).append(selRevBy);
    }
    if (!_.isEmpty(filterValues.DTRStatusTypes)) {
        $(selDTRStatus).html('<option value = ${Id}>${Name}</option>');
        $(selDTRStatus).fillTemplate(filterValues.DTRStatusTypes);
        $(selDTRStatus).prepend('<option value="all" selected="selected">--All--</option>');
        $($('.dtrStatusType')).append(selDTRStatus);
    }
}

function loadFilterValue(callback) {
    $('.filter-value').hide();
    $.ajax({
        url: '/sketchview/loadfiltervalue.jrq',
        data: {},
        dataType: 'json',
        success: function (data, status) {
            if (data)
                filterValues = data;
            else
                filterValues = {};
            processFilterValueField();
            if (callback) callback();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            filterValues = {};
            console.error(xhr, ajaxOptions, thrownError);
        }
    });
}

function getParcel(pid, callback, errorCallback) {
    $.ajax({
        url: '/sketchview/getparcelfromid.jrq',
        data: { pid: pid },
        dataType: 'json',
        success: function (resp) {
            activeParcel = resp;
            activeParcel.Original = {}
            activeParcel.Original = JSON.parse(JSON.stringify(resp));
            activeParcel = new newParcel(activeParcel);
            window.opener.getAuxiliaryTableNames(function (tableList) {
                LoadAuxDataPRC(activeParcel, tableList, function () {
                    window.opener.loadAuxData(activeParcel, tableList, function (parcel) {
                        loadPRCactiveParcel(TableHierarchical, activeParcel, function (parcel) {
                            activeParcel = parcel;
                            applyParcelChanges(function () {
                                if (callback) callback();
                            });                            
                        });
                    });
                });
            });
        },
        error: function (resp) {
            console.error(resp);
            if (errorCallback) errorCallback;
        }
    });
    return false;
}

function getSketchSegments() {
    var sketchedited = false;
    var IsSketchApproved = false;
    var processor = ccma.Sketching.SketchFormatter;
    var config = ccma.Sketching.Config;

    if (!processor)
        processor = CAMACloud.Sketching.Formatters.TASketch;

    var sketches = [];
    for (var ski in config.sources) {
        var sksource = config.sources[ski];
        var keyPrefix = sksource.Key ? (sksource.Key + '--') : '';
        var sketchTable = sksource.SketchSource.Table ? eval('activeParcel.' + sksource.SketchSource.Table) : [activeParcel];
        sketchTable = sketchTable && sketchTable.filter ? sketchTable.filter(function (x) { return x.CC_Deleted != true }) : sketchTable;
        var sketchTableOriginal = sksource.SketchSource.Table ? eval('activeParcel.Original.' + sksource.SketchSource.Table) : [activeParcel.Original];
        var notesTable = sksource.NotesSource && sksource.NotesSource.Table ? eval('activeParcel.' + sksource.NotesSource.Table) : [];
        notesTable = notesTable && notesTable.filter ? notesTable.filter(function (x) { return x.CC_Deleted != true }) : notesTable;
        var sketchKeyField = sksource.SketchSource.KeyField || 'ROWUID';
        for (var i in sketchTable) {
            var pi = sketchTable[i];
            var orgTable = sketchTableOriginal[i];
            var sketchKey = keyPrefix + pi[sketchKeyField];
            var labelField = sksource.SketchSource.LabelField ? sksource.SketchSource.LabelField.split('/') : [];
            var labelFieldValue = labelField[1] ? (eval('pi.' + labelField[0]) || '???') + '/' + (eval('pi.' + labelField[1]) || '???') : eval('pi.' + labelField[0]) || '???';
            var sketch = {
                uid: sketchKey.toString(),
                sid: pi[sketchKeyField],
                label: (sksource.SketchLabelPrefix ? (sksource.SketchLabelPrefix + ' ') : '') + (labelFieldValue) + (eval('pi.' + sksource.SketchSource.KeyFields[0]) ? (' [' + eval('pi.' + sksource.SketchSource.KeyFields[0]) + ']') : ''),
                sketches: [],
                notes: [],
                config: sksource,
                parentRow: sksource.SketchSource.Table ? pi : null
            }
            for (var vs in sksource.VectorSource) {
                var vectorSource = sksource.VectorSource[vs];
                var sketchVectorTable = vectorSource.Table ? eval('activeParcel.' + vectorSource.Table) : [activeParcel];
                sketchVectorTable = sketchVectorTable && sketchVectorTable.filter ? sketchVectorTable.filter(function (x) { return x.CC_Deleted != true }) : sketchVectorTable;
                var sketchVectorTableOrginal = vectorSource.Table ? eval('activeParcel.Original.' + vectorSource.Table) : [activeParcel.Original];
                var sketchVectorKeyField = vectorSource.KeyField || 'ROWUID';
				var sketchChanges = activeParcel.ParcelChanges.filter(function(ch){ if(ch.FieldName == vectorSource.CommandField || ch.FieldName == vectorSource.LabelField)return ch })
                var approvedSketchChanges = activeParcel.ParcelChanges.filter(function(ch){ if((ch.QCChecked) && (ch.FieldName == vectorSource.CommandField || ch.FieldName == vectorSource.LabelField)) return ch })
			    if(sketchChanges && approvedSketchChanges && sketchChanges.length > 0 && (sketchChanges.length == approvedSketchChanges.length))
				 	IsSketchApproved = true;
                if (sksource.SketchSource.Table == vectorSource.Table) {
                    var fullVector = eval('pi.' + vectorSource.CommandField);
                    var fullVectorOrg = orgTable ? eval('orgTable.' + vectorSource.CommandField) : null;
                    if (!processor) {
                        processor = CAMACloud.Sketching.Formatters.Sigma;
                    }
                    var parts = processor.getParts(fullVector);
                    var orgParts = fullVectorOrg ? processor.getParts(fullVectorOrg) : null;
                    var pcount = 0;
                    if (orgParts && parts && orgParts.length > parts.length)
                        $('.deletedmsg').show();
                    else
                        $('.deletedmsg').hide();
                    for (var x in parts) {
                        pcount++;
                        var part = parts[x];
                        var orgPart = orgParts ? orgParts[x] : null;
                        var vectorDeleted = orgParts && parts ? orgParts.length > parts.length : false;
                        var isChanged = false;
                        if (orgPart)
                            isChanged = !vectorDeleted && part.vector.replace(/ /g, '') != orgPart.vector.replace(/ /g, '');
                        else isChanged = true
                        var label = [];
                        label.push({ Field: vectorSource.LabelField, Value: part.label, Description: null, IsEdited: isChanged, hiddenFromEdit: vectorSource.HideLabelFromEdit, hiddenFromShow: vectorSource.HideLabelFromShow, lookup: sksource.SketchLabelLookup })
                        if (vectorSource.ExtraLabelFields) {
                            vectorSource.ExtraLabelFields.forEach(function (item) {
                                var value = part[item.Target]
                                if (item.ValueRegx) {
                                    value = value.match(item.ValueRegx)
                                    if (value && value[0]) value = value[0]
                                }
                                label.push({ Field: item.Name, Value: value, lookup: item.LookUp, Target: item.Target, Caption: item.Caption, hiddenFromShow: item.HideLabelFromShow, DoNotShowLabelDescription: item.DoNotShowLabelDescription, IsLargeLookup: item.IsLargeLookup, ValidationRegx: item.ValidationRegx })
                            })
                        }
                        if (isChanged)
                            sketchedited = true;
                        var sk = {
                            uid: sketchKey.toString() + "/" + pcount,
                            label: label,
                            labelPosition: part.labelPosition,
                            referenceIds: part.referenceIds,
                            isChanged: isChanged,
                            vector: part.vector,
                            LabelDelimiter: sksource.LabelDelimiter === undefined ? '/' : '',
                            vectorConfig: vectorSource
                        }

                        var hideSketch = (sksource.HideNullSketches && !sk.vector) || false;
                        if (!hideSketch) sketch.sketches.push(sk);
                    }

                } else {

                    //var filteredDetails = sketchVectorTable.filter(function (ivx) { if (ivx.ParentROWUID) return (ivx.ParentROWUID == pi.ROWUID); else return (eval('ivx.' + sksource.VectorSource.ConnectingFields[0]) == eval('pi.' + sksource.SketchSource.KeyFields[0])); });
                    var filteredDetails = sketchVectorTable.filter(function (ivx) { if (ivx.ParentROWUID) return (ivx.ParentROWUID == pi.ROWUID); else if (pi && pi.constructor.name == "Parcel") { return (ivx.CC_ParcelId == pi.Id); } else return (eval('ivx.' + vectorSource.ConnectingFields[0]) == eval('pi.' + sksource.SketchSource.KeyFields[0])); });
                    //var filteredDetailsOriginal = sketchVectorTableOrginal.filter(function (ivx) { if (ivx.ParentROWUID) return (ivx.ParentROWUID == pi.ROWUID); else if (pi && pi.constructor.name == "Parcel") { return (ivx.CC_ParcelId == pi.Id); } else return (eval('ivx.' + sksource.VectorSource.ConnectingFields[0]) == eval('pi.' + sksource.SketchSource.KeyFields[0])); });
                    for (var x in filteredDetails) {
                        var id = filteredDetails[x];
                        var org = id && id.Original ? id.Original : [];
                        var readonly = false
                        var vectorKey = id[sketchVectorKeyField];
                        var isChanged = true;
                        if (!processor) {
                            processor = CAMACloud.Sketching.Formatters.Sigma;
                        }
                        if (processor.getParts) {
                            var fullVector = eval('id.' + vectorSource.CommandField);
                            var fullVectorOrg = orgTable ? eval('org.' + vectorSource.CommandField) : null;
                            var parts = processor.getParts(fullVector);
                            var orgParts = fullVectorOrg ? processor.getParts(fullVectorOrg) : null;
                            if (orgParts && parts && orgParts.length > parts.length)
                                $('.deletedmsg').show();
                            else
                                $('.deletedmsg').hide();
                        }
                        if (org)
                            isChanged = eval('id.' + vectorSource.CommandField) != eval('org.' + vectorSource.CommandField) || eval('id.' + vectorSource.LabelField) != eval('org.' + vectorSource.LabelField);
                        if (isChanged)
                            sketchedited = true;
                        var field = window.opener.getDataField(vectorSource.LabelField, vectorSource.Table)
                        var index = sketchVectorTable.indexOf(id)
                        //if (field && field.ReadOnlyExpression)
                        //    readonly = activeParcel.EvalValue(vectorSource.Table + '[' + index + ']', field.ReadOnlyExpression)
                        var labelValue = eval('id.' + vectorSource.LabelField)
                        var lookDesc = sksource.SketchLabelLookup && !sksource.DoNotShowLabelDescription && window.opener.lookup[sksource.SketchLabelLookup] ? window.opener.lookup[sksource.SketchLabelLookup][labelValue] : null
                        var label = []; label.push({ Field: vectorSource.LabelField, Value: labelValue, Description: lookDesc, IsEdited: false, lookup: sksource.SketchLabelLookup, ReadOnly: readonly })
                        if (vectorSource.ExtraLabelFields) {
                            vectorSource.ExtraLabelFields.forEach(function (item) {
                                field = window.opener.getDataField(item.Name, vectorSource.Table)
                                var val = eval('id.' + item.Name)
                                var t = item.LookUp && window.opener.lookup[item.LookUp] ? window.opener.lookup[item.LookUp][val] : val
                                //if (field && field.ReadOnlyExpression)
                                //    readonly = activeParcel.EvalValue(vectorSource.Table + '[' + index + ']', field.ReadOnlyExpression)
                                label.push({ Field: item.Name, Value: val, Description: t, lookup: item.LookUp, ReadOnly: readonly })
                            })
                        }
                        var sk = {
                            uid: vectorKey.toString(),
                            label: label,
                            labelPosition: eval('id.' + vectorSource.LabelCommandField),
                            vector: eval('id.' + vectorSource.CommandField),
                            isChanged: isChanged,
                            vectorConfig: vectorSource
                        }

                        if (vectorSource.IdField) {
                            sk.rowId = eval('id.' + vectorSource.IdField);
                        }

                        if (processor.decodeSketch) {
                            sk.vector = processor.decodeSketch(sk.vector);
                        }
                        var hideSketch = (sksource.HideNullSketches && !sk.vector) || false;
                        if (!hideSketch) sketch.sketches.push(sk);
                    }
                }
            }
            if (notesTable && notesTable.length > 0) {
                var cf = sksource.NotesSource.ConnectingFields;
                var filterCondition = "1==1";
                cf.forEach(function (f) {
                    filterCondition += " && ivx." + f + " == pi." + f
                });
                var filteredNotes = notesTable.filter(function (ivx) { if (ivx.ParentROWUID) return (ivx.ParentROWUID == pi.ROWUID); else return (eval(filterCondition)) });
                for (var x in filteredNotes) {
                    var note = filteredNotes[x];
                    var n = {
                        uid: note[sksource.NotesSource.KeyField || 'ROWUID'],
                        text: note[sksource.NotesSource.TextField],
                        x: parseInt(note[sksource.NotesSource.PositionXField]) * (sksource.NotesSource.ScaleFactor || 1),
                        y: parseInt(note[sksource.NotesSource.PositionYField]) * (sksource.NotesSource.ScaleFactor || 1),
                        lx: parseInt(note[sksource.NotesSource.LineXField]) * (sksource.NotesSource.ScaleFactor || 1),
                        ly: parseInt(note[sksource.NotesSource.LineYField]) * (sksource.NotesSource.ScaleFactor || 1)
                    }
                    sketch.notes.push(n);
                }
            }

            if (sketch.config.AllowSegmentAddition || sketch.sketches.length > 0) {
                sketches.push(sketch);
            }
        }
    }
    if (sketchedited){
   		 if(IsSketchApproved){
   		 	$('.tabs li a[category="sketch-manager"]').removeClass('edited-tab');
    		$('.tabs li a[category="sketch-manager"]').addClass('approved');
    		}
    	 else {
    	 	$('.tabs li a[category="sketch-manager"]').removeClass('approved');
         	$('.tabs li a[category="sketch-manager"]').addClass('edited-tab');
          }
        }
    else{
    	$('.tabs li a[category="sketch-manager"]').removeClass('approved');
        $('.tabs li a[category="sketch-manager"]').removeClass('edited-tab');
    }
    return sketches;
}

function previewSketch(colorCode) {
    sketchRenderer.scale = 1
    sketchRenderer.resetOrigin();
    var sketches = getSketchSegments();
    $('.select-sketch-segments').empty().append('<option></option>').empty()
    for (x in sketches) {
        var skt = sketches[x];
        $('.select-sketch-segments').append($('<option>', { value: skt.uid, text: skt.label }));
    }
    bindSketchSegment(sketches);
}

function showMask(info, error, errorActionName, callback) {
    $('.masklayer').show();
    if (!info)
        info = 'Please wait ...'
    var helplink = ''
    if (callback && errorActionName) {
        helplink = '<a href="#" class="app-mask-error-help-link">Click here<a/> to ' + errorActionName
    }
    if (error) {
        info = '<span style="color:Red;">' + info + '</span> ' + helplink
    }

    $('.app-mask-layer-info').html(info);
}


function hideMask() {
    $('.masklayer').hide();
}
var ApexCartSketchAfterSave = function (sketchData, callback) {
    window.opener.ApexCartSketchAfterSave(sketchData, sketchApp, callback);
}
var HennepinSketchAfterSave = function (sketchData, callback) {
    window.opener.HennepinSketchAfterSave(sketchData, sketchApp, callback);
}
var HennepinSketchBeforeSave = function (sketchData, callback) {
    window.opener.HennepinSketchBeforeSave(sketchData, callback);
}
var ClarkKySketchAfterSave = function (sketchData, callback) {
    window.opener.ClarkKySketchAfterSave(sketchData, sketchApp, callback);
}
var BrillionSketchBeforeSave = function (sketchData, callback) {
    window.opener.BrillionSketchBeforeSave(sketchData, callback);
}
var AmesVectorDefinition = function ( data, config ){
    window.opener.AmesVectorDefinition( data, config );
}
