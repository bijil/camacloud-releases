﻿var internalSupportUsers = ["admin", "dcs-support", "vendor-cds", "dcs-qa", "dcs-rd", "dcs-integration", "dcs-ps"];
var isInternalSupportUser = false;

function infoChange() {
    //setTimeout(function () {
        if (localStorage.getItem('toolTipEnabled') == 0) {
            $('#info').css('background-position-x', '-22px');
            localStorage.setItem('toolTipEnabled', 1)
            $('.tip').tipr();
            $('.tip', $("#mapFrame").contents()).tipr($("#mapFrame").contents());
            if ($('.select.field option[value="nbhd.AssignedTo"]')[0]) $('.select.field option[value="nbhd.AssignedTo"]').attr('title', 'Shows the appraiser that is assigned to the Assignment Group the parcel is in.');
        }
        else {
            localStorage.setItem('toolTipEnabled', 0);
            if ($('.select.field option[value="nbhd.AssignedTo"]')[0]) $('.select.field option[value="nbhd.AssignedTo"]').removeAttr('title');
            $('#info').css('background-position-x', '-3px');
        }
    //}, 100);

}
function initTooltip () {
    if (localStorage.getItem('toolTipEnabled') == 0) {
        $('#info').css('background-position-x','-3px');
    }
    else if (localStorage.getItem('toolTipEnabled') == null) {
        localStorage.setItem('toolTipEnabled', 0)
        $('#info').css('background-position-x', '-3px');
    }
    else {
        $('#info').css('background-position-x','-22px');
        $('.tip').tipr();
        $('.tip', $("#mapFrame").contents()).tipr($("#mapFrame").contents());
    }
};

     function hide(className) {
         $(className).hide(effect1);
     }

function show(className) {
    $(className).show(effect1);
}
function showMask(info, error, errorActionName, callback) {
	var heiht=$('body').height();
	if(heiht>$(document).height())
    $('.masklayer').height(heiht);
    else
    {
    //$('.masklayer').height(($(document).height()))
	$('.masklayer').css('height','100%');
	}		
    $('.masklayer').show();
    window.scrollTo(0, 0);
    $('body').css('overflow', 'hidden');
    if (!info)
        info = 'Please wait ...'
    var helplink = ''
    if (callback && errorActionName) {
        helplink = '<a href="#" class="app-mask-error-help-link">Click here<a/> to ' + errorActionName
    }
    if (error) {
        info = '<span style="color:Red;">' + info + '</span> ' + helplink
    }

    $('.app-mask-layer-info').html(info);
}

function hideMask() {
    $('.masklayer').hide();
    $('body').css('overflow-Y', 'auto'); 
}

function notification(message, title, callback) {
    $('#webmessage').remove();
    if (title == null)
        title = "CAMA Cloud Success notification";
    var wm = '<div id="webmessage" style="display: none;">' + "<h4>" + title + "<span style='float:right; margin-right:10px;color:gray;margin-top:-10px;cursor:pointer' onclick='closenotification()'>x</span></h4><p>" + message + "</p></div>"
    $('.mask').after(wm);
    $('#webmessage').fadeIn(1000, "swing", function () {
        $('#webmessage').delay(4000).fadeOut(2000, "swing", callback);
    });
}
function closenotification() {
    $('#webmessage').remove();
}

function initSupportView() {
    if (internalSupportUsers.indexOf(currentSupportLoginId) > -1 && enabledInternalview == 'True') {
		$('.main-support-toggle').show();
		isInternalSupportUser = true;
		if(localStorage.internalSupportView == '1') 
			$('.main-support-toggle-switch-input')[0].checked = true;
		else
			$('.main-support-toggle-switch-input')[0].checked = false;
	}
	else {
		$('.main-support-toggle').hide();
		localStorage.internalSupportView = '0';
	}
}

function internalSupportView() {
	localStorage.internalSupportView = $('.main-support-toggle-switch-input')[0].checked ? '1': '0';
	if (localStorage.internalSupportView == '1') 
		localStorage.parcelDataSupportView = '1';
	if (typeof(activeParcel) != 'undefined' && activeParcel && typeof(switchSupportView) == 'function' && typeof(__DTR) != 'undefined' && !__DTR && $('.parcel-details')[0] && $('.parcel-details').css('display') == 'block' && $('.tabs-main li.selected').attr('content') == 'parceldata' ) {
		switchSupportView(true);
	}	
}