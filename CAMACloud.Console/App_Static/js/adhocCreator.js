﻿var drawingManager; 
var selectedShape;
var marker;
var map;
var pagesize;
var colors = ['#1E90FF', '#FF1493', '#32CD32', '#FF8C00', '#4B0082'];
var selectedColor;
var colorButtons = {};
var NeighborhoodId = {};
var parselarry = {};
var parselStatus = ["Normal", "High", "Urgent"];
var t;
var shapeCount =0;
var shapes={};
function clearSelection() {
	if (selectedShape) {
        selectedShape.setEditable(false);   
        selectedShape = null;
        
    }
}

function deleteSelectedShape() {
	if (selectedShape) {
	     delete shapes[selectedShape.zIndex];
    	selectedShape.setMap(null);
    	if(Object.keys(shapes).length=== 0){
          $('#myBtn').hide();
          $('#delete-button').hide();
          $('.parcelcount').html('');
          }
    }
    return false;
}

function setSelection(shape) {
    clearSelection();
    selectedShape = shape;
    shape.setEditable(true);
}
        // indexedDB
var db = new Dexie("AdHocDB",{autoOpen: false});
db.version(1).stores({ Mappoints:"++MId,Latitude,Longitude,Number,ParcelId,Priority,KeyValue"});
db.open().catch (function (err) {console.error('Failed to open db: ' + (err.stack || err));});
google.maps.event.addDomListener(window, "load", initialize);

function initialize(){
 $('#myBtn').hide();
 $('.parcelcount').html('') ;
 shapes={};
	$.ajax({
        	type: "POST",
        	url: "/parcels/setmapboundary.jrq",
        	dataType:'json',
        	data:{},
    		success: function (response) {
    			loadmap(response);
    		},
    		error:function(response){
    		
    		}
    		});

	function loadmap(data){
   		var Neighborhood = {};
        var popup = [];
        var unsortedParcels = {};
       	var ParcelData =[];
       	var minZoomLevel = 10;
     	map = new google.maps.Map(document.getElementById('map'), {   mapTypeId: google.maps.MapTypeId.SATELLITE });
        var infowindow = new google.maps.InfoWindow();
        var bound = new google.maps.LatLngBounds();
       // var test = [{ lat: 40.3541929564396, lng: -84.4261068744721}, {lat: 39.9165279215648, lng: -84.8125131750277 }]
      //  Object {Maxlat: 40.3541929564396, Maxlng: -84.4261068744721, Minlat: 39.9165279215648, Minlng: -84.8125131750277}
     var test =[
                 { lat : data[0].Maxlat,lng : data[0].Maxlng},
                 { lat : data[0].Minlat,lng : data[0].Minlng}
                   ]
             bound.extend(new google.maps.LatLng(test[0],test[1]));
    map.fitBounds(bound.extend(new google.maps.LatLng(test[1]))); 
        if(document.getElementById('delete-button').style.display=='block') { 
            document.getElementById('delete-button').style.display='none'; 
        }
          // Bounds for North America
        var strictBounds = new google.maps.LatLngBounds(new google.maps.LatLng(data[0].Maxlat,data[0].Maxlng),
                    new google.maps.LatLng(data[0].Minlat,data[0].Minlng));
                   
 // Listen for the dragend event
 google.maps.event.addListener(map, 'idel', function () {
     if (strictBounds.contains(map.getCenter())) return;
     var c = map.getCenter(),
         x = c.lng(),
         y = c.lat(),
         maxX = strictBounds.getNorthEast().lng(),
         maxY = strictBounds.getNorthEast().lat(),
         minX = strictBounds.getSouthWest().lng(),
         minY = strictBounds.getSouthWest().lat();

     if (x < minX) x = minX;
     if (x > maxX) x = maxX;
     if (y < minY) y = minY;
     if (y > maxY) y = maxY;

    map.setCenter(new google.maps.LatLng(y, x));
 });

 // Limit the zoom level
 google.maps.event.addListener(map, 'zoom_changed', function () {
     if (map.getZoom() < minZoomLevel) map.setZoom(minZoomLevel);
 }); 
        
                                                 
        var polyOptions = {
        	strokeWeight: 0,
            fillOpacity: 0.45,
            editable: true,
            draggable: true,
            drawingControl: true
        };
        drawingManager = new google.maps.drawing.DrawingManager({
            drawingControlOptions: {
                drawingModes: [google.maps.drawing.OverlayType.RECTANGLE]
            },
            rectangleOptions: polyOptions,
        	map: map
		}); //overlaycomplete bounds_changed 
		
	  
       google.maps.event.addListener(drawingManager,'overlaycomplete', function (e) {
         if(document.getElementById('delete-button').style.display=='none') { 
            document.getElementById('delete-button').style.display='block'; 
            }
              $('#myBtn').show();
			if (e.type !== google.maps.drawing.OverlayType.MARKER) {                              
		    	drawingManager.setDrawingMode(null);                          
			    var newShape = e.overlay;
			    newShape.type = e.type;
			    shapes[shapeCount] = newShape ;
			    shapeCount++;
			    t = newShape;
			    google.maps.event.addListener(t, 'click', function (e) {
		        	if (e.vertex !== undefined) {
			            if (t.type === google.maps.drawing.OverlayType.POLYGON) {
			                var path = t.getPaths().getAt(e.path);
			                path.removeAt(e.vertex);
			                if (path.length < 3) {
			                    t.setMap(null);
			                }
			            }
			            if (t.type === google.maps.drawing.OverlayType.POLYLINE) {
			                var path = t.getPath();
			                path.removeAt(e.vertex);
			                if (path.length < 2) {
			                    t.setMap(null);
			                }
			            }
			        }
			          $('#myBtn').show();
			        setSelection(newShape);
			    });
			   /* shapes[shapeCount] = newShape ;
			    shapeCount++;
			    t = newShape; */
			     google.maps.event.addListener(t,'dragend',function(T){ 
			        var ne = t.getBounds().getNorthEast();
                    var sw = t.getBounds().getSouthWest();
                    var coordinates = sw.lat() + ',' + ne.lat() + ',' + sw.lng()+',' + ne.lng();
                    $.ajax({
        	                type: "POST",
                        	url: "/parcels/getparcelcount.jrq",
        	                dataType:'json',
        	                data:{bounds :coordinates},
    		                success: function (response) {
    			            	var j ={Normal:0,High:0,Urgent:0};
    			            	for(i in response){
    			            		j[response[i].Priority] = response[i].count;   
    			               	}
    			            	var totalparcel = parseInt(j.Normal)+parseInt(j.High)+parseInt(j.Urgent);
    			            	$('.parcelcount').html("TOTAL PARCEL :"+totalparcel) ;
    			            	$('.parcelcount').show();
    		                },
    		                error:function(response){
    		
    		                }
    	        	});
                    
                      console.log(t.shape,
               'New north-east corner: ' + ne.lat() + ', ' + ne.lng() + '<br>' +
               'New south-west corner: ' + sw.lat() + ', ' + sw.lng());
			       setSelection(t);
 	
			    })
			
			    if (e.type == google.maps.drawing.OverlayType.POLYLINE || e.type == google.maps.drawing.OverlayType.POLYGON) {
			        var locations = e.overlay.getPath().getArray()   
			        document.getElementById('output').innerHTML = locations.toString();
			    }
			    else {
			        var bounds = e.overlay.getBounds();
			        var start = bounds.getNorthEast();
			        var end = bounds.getSouthWest();
			        var center = bounds.getCenter();
			        var range = {
			            firstLatitude: bounds.f.f,
			            firstLongtitude: bounds.b.b,
			            secondLatitude:bounds.f.b,
			            secondLongtitude: bounds.b.f
			        }
			        var points =  bounds.f.b +","+ bounds.f.f + "," + bounds.b.b + "," + bounds.b.f;
			        viewParcelCount(points);        
			    }
			}
        }); 
	}
	return false;
}
function getParcels(NeighborhoodId) {
            $.ajax({
                type: "POST",
                url: "/parcels/getparcels.jrq",
                dataType: 'json',
                data:{ number:NeighborhoodId.id},
                success: function (response) {
                    var parcels = [];
                    var parcel;
                    var infowindow = new google.maps.InfoWindow();
                    for (r in response) {
                        var point = response[r];
                        var pid = point.KeyValue1;
                        if (parcels[pid] == null)
                            parcels[pid] = { KeyValue1: point.KeyValue1, Priority:point.Priority, Points: [] };
                        parcels[pid].Points.push(point.Latitude + ","+ point.Longitude);
                    }
                    var icon = ["/App_Themes/Cloud/images/parcel.png",
                          "/App_Themes/Cloud/images/parcel-priority.png",
                          "/App_Themes/Cloud/images/parcel-alert.png"]
                    for (x in parcels) {
                        parcel = parcels[x];
                        var bounds = new google.maps.LatLngBounds();
                        for (i in parcel.Points) {
                            pstr = parcel.Points[i].split(',');
                            var loc = new google.maps.LatLng(parseFloat(pstr[0]), parseFloat(pstr[1]));
                            bounds.extend(loc);
                        }
                        marker = new google.maps.Marker({
                            position: new google.maps.LatLng(bounds.getCenter().lat(), bounds.getCenter().lng()),
                            map: map,
                            icon: icon[parcel.Priority]
                        });
                        google.maps.event.addListener(marker, 'click', (function (marker, parcel) {
                            return function () {
                                infowindow.setContent("'" + parcel.KeyValue1 + "'");
                                infowindow.open(map, marker);
                            }
                        })(marker, parcel));
                    }
                },
                error: function (response) {
                    var test = response;
                }
            });
            return false
        }
        
        var uniqueNames = [];
        function AddAdHoc() {
            var AdHocData;
            var checkbox;
            var radio;
            var checkboxdiv;
            $('.ParcelKey').find('.selectedCheckbox').each(function () { debugger;
               // if ($(this).prop('checked') == true) {
                   	$(this).css('background-color','#dddddd');
                   	$(this).next('.addadhoc').addClass('disabled');
                    if (Object.keys(parselarry).length > 0) {
                        var t = $(this).next('.addadhoc').val();
                        var status = Object.keys(parselarry).filter(function (el) {
                            return (el === t);
                        })
                        if (status.length ==0) {
                            parselarry[$(this).next('.addadhoc').val()] = { KeyValue1: $(this).next('.addadhoc').val(), Priority: $(this).next('.addadhoc').attr('priority') };
                        }
                    }
                    else
                        parselarry[$(this).next('.addadhoc').val()] = { KeyValue1: $(this).next('.addadhoc').val(), Priority: $(this).next('.addadhoc').attr('priority') };
                    
               // }
               $('#selectedparcel').html("SELECTED PARCELS : "+Object.keys(parselarry).length);
            })
            $('.AdHoc').remove();
            var setscroll =0;
            for(p in parselarry ){
                var parsel = parselarry[p];
                
                AdHocData = $('<tr class="AdHoc"></tr>');
                   // AdHocData.css('float', 'left')
                    checkboxdiv =$('<td onclick="checkbokselect(this)"><label class="selectParcelCheckbox"></label></td>')
                    checkbox = $("<label  class='AdHocData'><font face='Times New Roman' size='3'> &nbsp" + parsel.KeyValue1 + "</font></label>");
                    //checkbox = $("<input type='checkbox' class='AdHocData' ><label ><font face='Times New Roman' size='3'>" + parsel.KeyValue1 + "</font></label>");
                    checkbox.attr('value', parsel.KeyValue1);
                    checkbox.appendTo(checkboxdiv);
                    checkboxdiv.appendTo(AdHocData)
                    var radiotag = $('<td class="prioritystatus" onclick="return changePriority(this)"></td>');
                    for (var j = 0; j < 3; j++) {
                        radio = $('<input type="radio" class='+ parselStatus[j] +' /><labe> ' + parselStatus[j] + '</label>');
                        radio.attr('name', parsel.KeyValue1);
                        radio.attr('value', j);
                        radio.attr('checked', (j == parsel.Priority) ? true : false)
                        radio.appendTo(radiotag);
                    }
                    if(setscroll == 24)
                    $('.scroller').addClass('scroll');
                    radiotag.appendTo(AdHocData);
                    AdHocData.appendTo($('#AdHocHolder'));
                    setscroll = setscroll + 1;
            }
          
            return false;
        }
        function RemoveAdHoc() { //AdHoc
         $('.AdHoc').find('.selectedCheckbox').each(function () {
               // if ($(this).prop('checked') == true) {
                     $('.addadhoc[value="'+$(this).next('.AdHocData').val()+'"]').prev('label').removeClass('selectedCheckbox')
                     $('.addadhoc[value="'+$(this).next('.AdHocData').val()+'"]').prev('label').css('background-color','');
                     $('.addadhoc[value="'+$(this).next('.AdHocData').val()+'"]').removeClass('disabled');
                   $(this).parents('.AdHoc').remove();
                    var s = $(this);
                    for (p in parselarry) {
                        var parsel = parselarry[p];
                        if($(this).next('.AdHocData').val() == parsel.KeyValue1){
                        delete parselarry[p];
                        }
                   // }
                }
            })
               $('#selectedparcel').html("SELECTED PARCELS : "+Object.keys(parselarry).length);
            return false;
        }
        function checkbokselect(checkbox){
            var checkstatus = $(checkbox).find("label.selectParcelCheckbox");
            if($(checkstatus).hasClass('selectedCheckbox'))
            {
                if($(checkbox).find('label.addadhoc').hasClass('disabled')) return;
            	$(checkstatus).removeClass('selectedCheckbox')
            	$('#selectedparcel').html("SELECTED PARCELS : "+$('.selectedCheckbox').length);
            	$(checkstatus).css('background-color','');
           	}
            else
            {
				$(checkstatus).addClass('selectedCheckbox')
				$('#selectedparcel').html("SELECTED PARCELS : "+$('.selectedCheckbox').length);
			}				
        }
       function adhocassignment(){
             if($('#adhocassignmentgroup').prop('checked')== true){
               $('#AdHocPanel').show();
             }
             else{
                $('#AdHocPanel').hide();
             }
        } 
         function viewPanel(){
           var p = $(':radio[name="adhocgroup"]:checked').val();
           if(p=="Create"){
               $('#create').show();
               $('#Exist').hide();
           }else if(p=='Exists'){
               $('#create').hide();
               $('#Exist').show();
               $.ajax({
                type: "POST",  
                url: "/parcels/getadhocneighborhood.jrq",
                success: function (response) {
                $("#loadAdhoc option").remove();
                 $.each(response,function(index,value){
                 $('#loadAdhoc').append('<option value=' +value.Id +'>'+ value.Number + '</option>');
                 });
                },
                error:function(){
                
                }
               })
           }
        }
function createadhoc(){
	var status = $(':radio[name="adhocgroup"]:checked').val();
    var adhoc= null;
	if(status=="Create")
  		adhoc = $('#newadhoc').val();
    else if(status =="Exists")
 		adhoc = $('#loadAdhoc option:selected').text();
	var parcelId =[];
	var priority =[];
	$('#AdHocHolder').find('.selectedCheckbox').each(function(){ 
		parcelId.push($(this).next().val())
		priority.push($(this).parent().parent().find('.prioritystatus input[type="radio"]:checked').val());			
	})
	var data ={
		ParcelId:parcelId.join(","),
		Priority:priority.join(","),
		AdHocNumber:adhoc,
		Status:status ,
		LoginId:$('#LoginId').val()
	};
	if(parcelId.length >0){
		if(adhoc != "" ){
		    showMask();
			$.ajax({
				type: "POST",  
				url: "/parcels/adhoccreation.jrq",
				data:data,
				success: function (response) {
						initialize()
						hideMask();
						$('.close').trigger('click');
						parselarry ={};
						$('.parcelcount').html('') ;
						$('#myBtn').hide();
						$('#newadhoc').val('');
				},
				error:function(){
				        
				}
			});
		}
		else
			alert("Please enter a valid Adhoc Name ");		
	}
	else
		alert("Please select atleast one Parcel");
}
	
	function selectall(){
 		$('.selectParcelCheckbox').addClass('selectedCheckbox');
 		$('#selectedparcel').html("SELECTED PARCELS : "+$('.selectedCheckbox').length);
 			return false;
    	}
	function deselectall(){
        $('.selectParcelCheckbox').removeClass('selectedCheckbox');
        $('#selectedparcel').html("SELECTED PARCELS : "+$('.selectedCheckbox').length);
         	return false;
        }
	function getAdhocParcels(){
	var str ="";
	for(i in shapes) {
		var t = shapes[i];
		var ne = t.getBounds().getNorthEast();
		var sw = t.getBounds().getSouthWest();
	  	str += sw.lat()+","+ne.lat()+"|"+sw.lng()+","+ne.lng()+"$$"; 
	}
	showMask(); 
	console.log(str.trim());
	$.ajax({
	    type: "POST",
	    url: "/parcels/getparcelid.jrq",
	    data: { bounds: str.trim()},
		success: function (response) {
	   		console.log(response.message);
	   		if(response.length > 0) {
            	var t;
            	var checkbox;
            	var parcels;
            	var setscroll =0;
            	$('#output').children(".ParcelKey").remove()
            	for (r in response) {
        			t = response[r];
        			AdHocData = $('<tr class="AdHoc"></tr>');
                    checkboxdiv =$('<td onclick="checkbokselect(this)"><label class="selectParcelCheckbox"></label></td>')
                    checkbox = $("<label  class='AdHocData'><font face='Times New Roman' size='3'> &nbsp" + t.KeyValue1 + "</font></label>");
                    checkbox.attr('value', t.KeyValue1);
                    checkbox.appendTo(checkboxdiv);
                    checkboxdiv.appendTo(AdHocData)
                    var radiotag = $('<td class="prioritystatus"></td>');
                    for (var j = 0; j < 3; j++) {
                        radio = $('<input type="radio" class='+ parselStatus[j] +' /><label> ' + parselStatus[j] + '</label>');
                        radio.attr('name', t.KeyValue1);
                        radio.attr('value', j);
                        radio.attr('checked', (j == t.Priority) ? true : false)
                        radio.appendTo(radiotag);
                    }
                    if(setscroll == 24)
                    $('.scroller').addClass('scroll');
                    radiotag.appendTo(AdHocData);
                    AdHocData.appendTo($('#AdHocHolder'));
                    setscroll = setscroll + 1;
        		}
            	hideMask();
            	modelPopup();
            } else {
         		hideMask();
             	alert("NO Parcels")
            }
	    },
	    error: function (response) {
	        console.error(response);
	    }
	})	
}
function bulkOperation(){
	var process = $('#bulkchange').val();
	if(process != ""){
		$('.prioritystatus input[type="radio"]').prop('checked',false)
        $("."+process+"").prop('checked','true');		
        }
}
function modelPopup(){
	var modal = document.getElementById('myModal');
 	var btn = document.getElementById("myBtn");
 	var span = document.getElementsByClassName("close")[0];
	$("#AdHocPanel").attr("disabled", "disabled");
	$('.parcelcount').html("TOTAL PARCEL :"+$('.AdHoc').length) ;
	$('#myModal').show();
	$('.close').unbind();
	$('.close').click(function () {
    	modal.style.display = "none";
    	parselarry ={};
    	$('.AdHoc').remove();
    	$('.ParcelKey').remove();
       	$('.scroller').removeClass('scroll');
       	$('#selectedparcel').html('SELECTED PARCELS :');
       	
    });
	$('window').unbind('click');
	$('window').click(function(event) {
		if (event.target == modal) {
        	$('#myModal').hide();
        	parselarry ={};
        	$('.AdHoc').remove();
    	    $('.ParcelKey').remove();
        }         
    });
}
function viewParcelCount(coordinates){
	$.ajax({
		type: "POST",
		url: "/parcels/getparcelcount.jrq",
		dataType:'json',
	    data:{bounds :coordinates},
	    success: function (response) {
			var j ={Normal:0,High:0,Urgent:0};
	    	for (i in response) {
				j[response[i].Priority] = response[i].count;   
	       	}
			var totalparcel = parseInt(j.Normal)+parseInt(j.High)+parseInt(j.Urgent);
	    	$('.parcelcount').html("TOTAL PARCEL :"+totalparcel) ;
			$('.parcelcount').show();
	    },
	    error:function(response){
	    		
		}
	});  
}