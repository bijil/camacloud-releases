﻿
function initDTRApplication(callback) {
    var storedTask = window.localStorage.getItem('app-dtr-selected-task');
    if (storedTask) {
        $('.app-dtr-selected-task').val(storedTask);
        $('.app-search-results-title').html('Desktop Review : ' + $('.app-dtr-selected-task option:selected').text());
    }
    $('.app-dtr-selected-task').change(function () {
        var selectedTask = $('.app-dtr-selected-task').val();
        window.localStorage.setItem('app-dtr-selected-task', selectedTask);
        $('.app-search-results-title').html('Desktop Review : ' + $('.app-dtr-selected-task option:selected').text());
        //doSearch();
    });
}
var sanbornWindow=null;
function loadSanborn(){
	var viewerName = clientSettings["SanbornViewerName"];
	var searchPart;
	if(viewerName && viewerName.trim() !=""){
		var url='https://oblique.sanborn.com/'+viewerName+'/?'
		if(activeParcel.mapCenter)
			url +='ll='+activeParcel.mapCenter[0]+','+activeParcel.mapCenter[1]
		else if (activeParcel.StreetAddress != null || activeParcel.StreetAddress != "")
	    	url += 'addr=' + activeParcel.StreetAddress
		else {
	    	var parcelField = activeParcel.KeyValue3 || activeParcel.KeyValue1;
	    	url += 'id=' + parcelField
			}
	//var address = activeParcel.mapCenter ? activeParcel.mapCenter : [-73.935242, 40.730610];
	//var url='https://oblique.sanborn.com/'+viewerName+'/?ll='+address[0]+','+address[1];
	if( sanbornWindow && sanbornWindow.closed != true )  sanbornWindow.location.href = url; 
	//sanbornWindow.close();
	else
 		sanbornWindow=openWindow(sanbornWindow, 'sanborn', url, { resizable: 'true', width: 1200, height: 700 });
 	if(sanbornWindow)
 		sanbornWindow.focus();
 	}
}

var pictometryPopup, nearmappopup, woolpertPopup, nearmapWMSpopup, evPopup;
function loadPictometry() {
    //openWindow(hwnd, name, url, params)
    pictometryPopup = openWindow(pictometryPopup, 'pictometry', '/protected/pictometry/', { resizable: 'true', width: 1200, height: 700 });
    if (!pictometryPopup) {
        alert('Popup blocker on your computer is preventing the opening of EagleView. Please disable it to view EagleView.');
    }
}

function loadEagleView() {
    evPopup = openWindow(evPopup, 'eagleView', '/protected/eagleview/Default.aspx', { resizable: 'true', width: 1200, height: 700 });
    if (!evPopup) {
        alert('Popup blocker on your computer is preventing the opening of EagleView. Please disable it to view EagleView.');
    }
}

function loadWoolpert() {
    //openWindow(hwnd, name, url, params)
    woolpertPopup = openWindow(woolpertPopup, 'woolpert', '/protected/woolpert/wmap.aspx', { resizable: 'true', width: 1200, height: 700 });
    if (!woolpertPopup) {
        alert('Popup blocker on your computer is preventing the opening of Woolpert Imagery. Please disable it to view EagleView.');
    }
}
function closeWoolpert() {
    if (woolpertPopup) {
        woolpertPopup.close();
    }
    else {
        // console.log('no window found');
    }
}

function loadNearmap() {
    if (activeParcel) {
        $('.Setting').hide();
        if (activeParcel.Latitude == null) {
            alert('No map coordinates available for this parcel.');
            return false;
        }
        if (OsMap) {
            OsMap.options.maxZoom = 19;
        }
    }
    nearmappopup = openWindow(nearmappopup, 'nearmap', '/protected/nearmap/', { resizable: 'true', width: 1200, height: 700 });
    if (!nearmappopup) {
        alert('Popup blocker on your computer is preventing the opening of Nearmap. Please disable it to view Nearmap.');
    }

}

function loadNearmapWMS() {
    nearmapWMSpopup = openWindow(nearmapWMSpopup, 'nearmapwms', '/protected/nearmapwms/', { resizable: 'true', width: 1200, height: 700 });
    if (!nearmapWMSpopup) {
        alert('Popup blocker on your computer is preventing the opening of WMS Nearmap. Please disable it to view WMS Nearmap.');
    }
}

function closePictometry() {
    if (pictometryPopup) {
        pictometryPopup.close();
    }
    else {
        // console.log('no window found');
    }
}
var cycloPopup;
function loadCyclomedia() {
    cycloPopup = openWindow(cycloPopup, 'Cyclomedia', '/protected/cyclomedia/', { resizable: 'true', width: 1200, height: 700 });
    //if (!cycloPopup) {
       // alert('Popup blocker on your computer is preventing the opening of Cyclomedia. Please disable it to view Cyclomedia.');
    //}
}
function closeCyclomedia() {
    if (cycloPopup) {
        cycloPopup.close();
    }
}
var parcelPhotosPopup;
function loadParcelPhotos() {
    localStorage.setItem('MaxPhotosPerParcel', parseInt(getClientSettingValue("MaxPhotosPerParcel")));
    parcelPhotosPopup = openWindow(parcelPhotosPopup, 'parcelPhotos', '/protected/dtr/images.aspx?parcelId=' + activeParcel.Id, { resizable: 'true', width: 1200, height: 700 });
    if (!parcelPhotosPopup) {
        alert('The problem with the opening of parcel photos popup.');
    }
}

var parcelPRCPopup;
function loadParcelPRC() {
    refreshDigitalPrc();
    parcelPRCPopup = openWindow(parcelPRCPopup, 'digitalPRC', '/protected/dtr/digitalPRC.aspx?parcelId=' + activeParcel.Id, { resizable: 'true', width: 1200, height: 700 });
    if (!parcelPRCPopup) {
        alert('The problem with the opening of parcel Digital PRC popup.');
    }
}
function hidePrc() {
    localStorage.setItem('checkdigitalPRC', true);
    $(".tabs li[category='digital-prc']").css("display", "none");
    openCategory('dashboard', 'Dashboard');
}
function onunloadPoupUp1() {
    localStorage.setItem('checkdigitalPRC', false);
    $(".tabs li[category='digital-prc']").css("display", "inline");
}


function highlightCatLink() {
    $('.field-category-link').removeClass('field-category-edited');
    $('div.sublevelDiv a').removeClass('field-category-edited');
    $('.field-category-link').removeClass('approved');
    $('div.sublevelDiv a').removeClass('approved');
    var doNotHighlightCustomFields = clientSettings["DoNotHighlightCustomFields"] && ((clientSettings["DoNotHighlightCustomFields"] == "1") || (clientSettings["DoNotHighlightCustomFields"].toLowerCase() == "true"));
    var clsFCE = 'field-category-edited';
    for (x in activeParcel.ParcelChanges) {
        var ch = activeParcel.ParcelChanges[x];
        var field = datafields[ch.FieldId];
        var catId = field ? field.CategoryId : null;
        var cat = $('.field-category-link[category="'+ catId +'"]');
        if (!field) {
            continue;
        }
        var fc = _.findWhere(fieldCategories, { Id: field.CategoryId });
        //console.log(doNotHighlightCustomFields, field.IsCustomField, ch.Action, 
        //      field.Name, fc.Name, field.CategoryId, field.Id, field.SourceTable, ch)
        if (!fc && !ch.SourceTable) {
            continue;
        }
        if (field && field.IsCustomField && doNotHighlightCustomFields) {
            continue;
        }
        if (((ch.Action == 'new') || (ch.Action == 'edit') || (ch.Action == 'delete')) && (field && field.ReadOnly != true && (getCategory(ch.CategoryId) && getCategory(ch.CategoryId).IsReadOnly != true) && field.DoNotShow != true)){
        	//if(ch.QCChecked && catId )
        	//	$(cat).addClass('approved');
        	//else
        	//{
        		//$(cat).removeClass('approved');
	            var parentCategory, tempParentCategory;
	            parentCategory = ch.CategoryId;
	            if (parentCategory == null) {
	                parentCategory = getCategory(ch.SourceTable);
	            }
	            do {
	                tempParentCategory = $('#AllCategories li[childid="' + parentCategory + '"]').attr('category');
	
	                if (undefined == tempParentCategory) {
	                    break;
	                }
	                else {
	                    parentCategory = tempParentCategory;
	                }
	
	            } while (true);
	
	            var cls1 = 'div.sublevelDiv  a[category="' + ch.CategoryId; + '"]';
	            var clsO = '.field-category-link[category="' + parentCategory + '"]';
	            var cls3 = '.parentCategoryTitle[category="' + parentCategory + '"]';
	            if (!$(clsO).hasClass(clsFCE))   // && !activeParcel.QC)
	                $(clsO).addClass(clsFCE);
	            $(cls1).addClass(clsFCE);
	            $(cls3).addClass(clsFCE);
	            if (activeParcel.QC && ch.QCChecked) {
					if (!$(clsO).hasClass('approved'))						           
		           		$(clsO).addClass('approved');
	            	$(cls1).addClass('approved');
	            	$(cls3).addClass('approved');
            	}
            	else{
            		$(clsO).removeClass('approved');
	            	$(cls1).removeClass('approved');
	            	$(cls3).removeClass('approved');
            }
        }
    }
}

function openSketchViewer() {
    //window.open('/protected/sketchview/', 'sketchview', 'toolbar=no,scrollbars=no,resizable=yes,top=0,left=0,width=' + (screen.availWidth || screen.width) + ',height=' + (screen.availHeight || screen.height))
    try {
        var sketchViewer;
        sketchViewer = openWindow(sketchViewer, 'sketchview', '/protected/sketchview/', { toolbar: 'no', scrollbars: 'no', top: 0, left: 0, resizable: 'true', width: (screen.availWidth || screen.width), height: (screen.availHeight || screen.height) });
        if($('.left-panel').is(":visible") == false)
       	expandScreen();
    }
    catch (e) {

    }
    return false;
}

function qcVisualStats() {
    //do nothng in dtr
}
function redirectedPriorityListParcels(callback)
{
    //do nothng in dtr
   RedirectedPriorityListFlag='';
    if(callback) callback();
}