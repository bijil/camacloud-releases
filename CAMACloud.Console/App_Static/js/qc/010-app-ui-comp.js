﻿var currentLookup = [];
var currentText = '';
var extendedLookup = []

function getLookupData(field, options, callback, isCustom) {
    var data = [];
    var cat = options.category;
    if (cat == null) 
        cat = field.Category ;
    if (field.IsLargeLookup == 'true' || field.IsLargeLookup == true) {
        if (callback) callback([]);
        return;
    }
    var specialType = false;
    if (options.dataSource == 'ccma.YesNo') {
        if (callback) callback(ccma.YesNo);
    } else {
        if (field.LookupTable == '$QUERY$') {
            var query = field.LookupQuery || '';
            if (lookup['#FIELD-' + field.Id] == undefined) {
                var fullQuery = query || '';
				if (query.search(/\{[a-zA-Z0-9_.]*\}/i) > 0){
                   if (query.search(/where/i) > 0){
						if(query.search(/union/i) > 0){
							var unionSplit = query.split(/union/i)
							unionSplit.forEach(function(each,index){ if( unionSplit[index].search(/where/i) > 0 ) unionSplit[index] = unionSplit[index].substr(0, unionSplit[index].search(/where/i))});
							fullQuery = unionSplit.join('union');
						}
						else
							fullQuery = query.substr(0, query.search(/where/i));
                   }
               	}
                if ( fullQuery && fullQuery.search( /\{.*?\}/g ) == -1 || (field.LookupQueryForCache && field.LookupQueryForCache != '')) {
                    if ( field.LookupQueryFilter ) extendedLookup['#FIELD-' + field.Id] = []
                    if(field.LookupQueryForCache && field.LookupQueryForCache != '') fullQuery = field.LookupQueryForCache;
                    getData(fullQuery, [], function (ld) {
                        if (ld.length > 0) {
                            var f = field.Name;
                            lookup[f] = {};
                            var props = Object.keys(ld[0]);
                            var IdField = props[0]; var NameField = props[1]; var DescField = props[2]
                            if (!NameField) NameField = IdField;
                            if (!DescField) DescField = NameField;
                            ld = convertToCommaSeparatedDropDown(ld, field, IdField);
                            for (var x in ld) {
                                var d = ld[x];
                                var v = d[IdField];
                                if (v) v = v.toString().trim();
                                var name = d[NameField];
                                if (name) name = name.toString().trim();
                                if (!name) name = v;
                                if (lookup[f][v] === undefined)
                                    lookup[f][v] = { Name: name, Description: d[DescField], Ordinal: x, SortType: null, Id: v, Object: d };
                                if ( field.LookupQueryFilter )
                                {
                                    d.Ordinal = x;
                                    d.Name = name;
                                    d.Description = d[DescField];
                                    d.Id = v;
                                    extendedLookup['#FIELD-' + field.Id].push( d )
                                }
                            }
                            lookup['#FIELD-' + field.Id] = lookup[f];
                        }

                    });
                } else specialType = true;

            }
            var o;
            if (cat && cat.TabularData == "true") {

            } else {
                o = activeParcel;
            }
            if (options.source)
                o = options.source;
            if (o) {
                if (query.search(/\{.*?\}/g) > -1) {
                    query.match(/\{.*?\}/g).forEach(function (fn) {
                        var testFieldName = fn.replace('{', '').replace('}', '');
                        if (testFieldName.indexOf('parent.') > -1) {
                            var fieldName = testFieldName.split('parent.');
                            lookupField = fieldName[fieldName.length - 1];
                            source = o;
                            while (testFieldName.search('parent.') > -1) {
                                source = source["parentRecord"] ? (source["parentRecord"].constructor == Array ? source["parentRecord"][0] : source["parentRecord"]) : []
                                testFieldName = testFieldName.replace('parent.' + lookupField, lookupField);
                            }
                            //source = o["parentRecord"] ? o["parentRecord"] : [];
                            //lookupField = testFieldName.split('.')[1];
                            //if (testFieldName.indexOf('parent.parent') > -1) {
                            //    source = source["parentRecord"] ? source["parentRecord"] : [];
                            //    lookupField = testFieldName.split('.')[2];
                            //}
                        }
                        else if (testFieldName.indexOf('parcel.') > -1) {
                            source = activeParcel;
                            lookupField = testFieldName.split('parcel.')[1];
                        }
                        else { lookupField = testFieldName.split('.')[0]; source = o };
                        value = (testFieldName == 'CURRENT_USER')? ccma.CurrentUser: source[lookupField];
                        if(value && typeof(value) == "string") value = value.replace(/'/g,"''");
                        if (value != 0 && !value && value != '') value = 'null';
                        if (('ImagePath' in source) && ('Id' in source)) {
                            var imgfield = getDataField(lookupField, '_photo_meta_data');
                            var valueField = '';
                            if (imgfield) {
                                //                                valueField = imgfield.AssignedName.replace('PhotoMetaField', 'MetaData');
                                //                                value = o[valueField];
                                //                                if ((value == null || value === undefined || value == '') && imgfield.DefaultValue) value = imgfield.DefaultValue;
                                valueField = imgfield.AssignedName.replace('PhotoMetaField', 'MetaData');
                                value = source[valueField];
                            }

                            if (testFieldName.split('.') > 0) {
                                var attrName = testFieldName.split('.')[1];
                                var lk = getFieldLookup(lookupField);
                                if (lk) {
                                    if (lk[value]) {
                                        if (lk[value].Object) {
                                            value = lk[value].Object[attrName];
                                        }
                                    }
                                }
                            }

                        }

                        query = query.replace(fn, value);

                    });
                }
            }
            if (query) {
                if (query.search(/\{.*?\}/g) == -1 || query.search(/}&%/i) > -1) {
                    getData(query, [], function (ld) {
                        if (ld.length > 0) {
                            var f = field.Name;
                            var props = Object.keys(ld[0]);
                            var IdField = props[0]; var NameField = props[1]; var DescField = props[2]
                            if (!NameField) NameField = IdField;
                            ld = convertToCommaSeparatedDropDown(ld, field, IdField);
                            for (var x in ld) {
                                if (specialType) lookup[f] = {};
                                var d = ld[x];
                                var v = d[IdField];
                                if(v === 'null'){v = ''; d[IdField] = '';}
                                var name = d[NameField];
                                if (name) name = name.toString().trim();
                                //if (v && v.trim) {
                                //    v = v.trim();
                                //}
                                if (!name) name = v;
                                data.push({ Name: name, Description: d[DescField], Ordinal: x, SortType: null, Id: v });
                                if (specialType) {
                                    if (lookup[f][v] === undefined)
                                        lookup[f][v] = { Name: name, Description: d[DescField], Ordinal: x, SortType: null, Id: v, Object: d };
                                }
                            }
                        }
                        if (options.addNoValue) {
                            data.unshift({ Name: '-- No Value --', Id: '' });
                        } 
                        else if (isCustom && isTrue(field.DoNotAllowSelectNull)) {
                        	data = data.filter(function(r) { return (r.Id != '' && r.Id != '<blank>') });
                        }
                        else if (isTrue(field.LookupShowNullValue)) {
                            data.unshift({ Id: '', Name: '-- No Value --' });
                        } else {
                            data.unshift({ Name: '', Id: '' });
                        }
                        if (callback) callback(data,options.source, options);

                    }, null, false, function () {
                        data.unshift({ Name: '--Error in Custom Query--', Id: '' });
                        if (callback) callback(data, options.source, options);
                    });
                } else {
                    if (callback) callback([]);
                }
            }
            else {
                if (callback) callback([]);
            }
        } else {
            var l = lookup[field.LookupTable]
            for (x in l) {
                var Idfield = x;
               // if (field.IsClassCalculatorAttribute == 'true')
                   // Idfield = l[x].NumericValue || 0
                data.push({
                    Id: x,
                    Name: l[x].Name,
                    Description: l[x].Description,
                    Ordinal: l[x].Ordinal,
                    SortType: (l[x].SortType && !isNaN(l[x].SortType)) ? parseInt(l[x].SortType) : null
                })
            }
            if (options.addNoValue) {
                data.unshift({ Name: 'NO VALUE', Id: '' });
            }
            if (isCustom && isTrue(field.DoNotAllowSelectNull)) {
            	data = data.filter(function(r) { return (r.Id != '' && r.Id != '<blank>') });
            	//if (data[0].Id == '' || data[0].Id == '<blank>')
                //	data.shift();
            }
            else if (isTrue(field.LookupShowNullValue)) {
                data.unshift({ Id: '', Name: '-- No Value --' }); //data.unshift({ Name: '', Value: '<blank>' });in VB side LookupDropdown_Load function fill -- No Value  -- 
            }
            if (callback) callback(data.sort(function (x, y) { return x.Ordinal ? x.Ordinal - y.Ordinal : x.Id > y.Id ? 1 : -1 }), options.source, options);
        }
    }
}

function getFieldLookup( fieldName, fieldId, options ){
    var selector = fieldName;
    if (fieldId) 
        selector = '#FIELD-' + fieldId;
    var lk = ccma.Data.LookupMap[selector];

    if (lk == undefined) {
        return null;
    } else if ( lk.LookupTable == "$QUERY$" ){
        var lkd;
   		if (!lookup[selector]) {
   		    lkd = lookup[fieldName];
        } else {
   		    lkd = lookup[selector]; 
   		}
   		if ( options && options.referenceObject && options.referenceFilter && extendedLookup[selector].length > 0 )
   		{
   		    var source = options.referenceObject
   		    var expression = options.referenceFilter
   		    expression.match( /\{.*?\}/g ).forEach(function ( fn ) {
                var testFieldName = fn.replace( '{', '' ).replace( '}', '' );
                var tempSource = source;
                while ( testFieldName.indexOf( 'parent.' ) > -1 ) {
                    tempSource = source['parentRecord'];
                    testFieldName = testFieldName.replace( 'parent.', '' );
                }
                if (tempSource) {
	                var value = tempSource[testFieldName]
	                expression = expression.replace( fn, value );
                }
            })
   		    for ( x in extendedLookup[selector][0] ) { var regex = new RegExp( x.replace(/\|/g, '\\|'), "g" ); expression = expression.replace( regex, 'thisObject.' + x ) };
   		    while(expression.indexOf('thisObject.thisObject.') > -1){
			   expression = expression.replaceAll('thisObject.thisObject.','thisObject.')
			}
   		    lkd = eval( 'extendedLookup["' + selector + '"].filter(function (thisObject) { return (' + expression + ') })' );
   		    var obj = {};
   		    lkd.forEach( function ( e )
   		    {
   		        obj[e.Id] = e
   		    } )
   		    return obj;
   		} else
   		    return lkd;
    } else {
        return lookup[lk.LookupTable];
    }
}

function getLookupArray(name, options, callback) {
    if (!options) options = {};
    var ld = getFieldLookup(name);
    var lx = [];
    for (x in ld) {
        lx.push({
            Id: x,
            Name: ld[x].Name,
            Description: ld[x].Description,
            Ordinal: ld[x].Ordinal,
            SortType: (ld[x].SortType && !isNaN(ld[x].SortType)) ? parseInt(ld[x].SortType) : null
        })
    }
    if (options.addNoValue) {
        lx.unshift({ Name: 'NO VALUE', Id: '' });
    }
    return lx.sort(function (x, y) { return x.Ordinal ? x.Ordinal - y.Ordinal : x.Id > y.Id ? 1 : -1 });
}

function openCustomddlList(source) {
    isLookupScreen = true;
    var lookupvalue;
    currentLookup = $(source).siblings('.newvalue')[0];
    lookupvalue = $(currentLookup).val();
    try {
        currentText = $('option[value="' + lookupvalue + '"]', $(currentLookup))[0].text;
    } catch (ex1) {
        currentText = "";
    }

    $('span[selectedcustomddl]').html(currentText);
    $('.customddlbtnadd').hide();
    $('.customddl, .mask').show();
    $('div[controls] input').focus();
    $('div[controls] input').val('');
    $('div[controls] input').attr('field', $(source).siblings('.newvalue').attr('field-name'));
    $('#loolupul').html('');
    $('.customddl span[legend]').html($(source).siblings('.legend').html());
    setScreenDimensions();

}

function FillLookupList(source) {
    var searchWord = $('div[controls] input').val().trim().toLowerCase();
    var result = [];
    $('.customddlbtnadd').hide();
    result = (searchWord != '') ? getLookupArray($(source).attr('field')).filter(function (d) { return ((d.Id) ? d.Id.toLowerCase() : d.Id) == searchWord || ((d.Description) ? d.Description.toLowerCase().indexOf(searchWord) != -1 : d.Description) || ((d.Name) ? d.Name.toLowerCase().indexOf(searchWord) != -1 : d.Name); }) : [];
    // while (result.length > 100) { result.pop(); }
    if (result.length > 0) {
        (!isNaN(result[0].SortType) && result[0].SortType) ? result = ccma.Data.Evaluable.sortArray(result, eval('lookup_' + parseInt(result[0].SortType)), true) : '';
        $('#loolupul').html('<li value="${Id}"  onclick="markLookupSelected(this);">${Name}</li>');
        $('#loolupul').fillTemplate(result);
        $('span[selectedcustomddl]').html(currentText);
        refreshScrollable('lookupout');
    }
    else {
        $('#loolupul').html('<li>No matches ...</li>');
        $('span[selectedcustomddl]').html(currentText);
    }
}

function markLookupSelected(source) {
    $('.customddlbtnadd').show();
    $('.customddl_desc ul li').removeAttr('selected');
    $('span[selectedcustomddl]').html($(source).html())
    $(source).attr('selected', '');
}

function setLookupValue(source) {
    (source) ? $(source).hide() : '';
    var Field = $('div[controls] input').attr('field');
    var pval = $('.customddl_desc ul li[selected]').attr('value');
    if (pval) {
        var selectedValue = getLookupArray(Field).filter(function (d) { return d.Id == decodeURI(pval); });
        (selectedValue.length > 0) ? $(currentLookup).val(encodeURI(selectedValue[0].Id)) : '';
        saveInputValue(currentLookup, function () {

        });

        $('.customddl, .mask').hide();
        isLookupScreen = false;
        currentLookup = [];
        currentText = '';
    }
    else {
        closeddlDefault();
    }
}

function closeddlDefault() {
    currentLookup = [];
    $('.customddl, .mask').hide();
    isLookupScreen = false;
}

function loadSketchExtraLookups(callback) {
	if ( clientSettings.SketchConfig || sketchSettings.SketchConfig ) {
		var _cssktConfig = clientSettings.SketchConfig || sketchSettings.SketchConfig;
		if ( _cssktConfig == 'FTC' || _cssktConfig == 'ApexJson' )
			getData('SELECT DETAILCALCULATIONTYPE, IMPDETAILTYPEID, IMPDETAILDESCRIPTION, IMPDETAILTYPE, APEXLINKFLAG, ACTIVEFLAG, APEXAREACODE FROM TLKPIMPSDETAILTYPE ORDER BY SORTORDER', [], function(d) {
				FTC_Detail_Lookup = JSON.parse(JSON.stringify(d));
				getData('SELECT ADDONFILTERTYPE, ADDONDESCRIPTION, IMPDETAILTYPE, JURISDICTIONID, APEXLINKFLAG, ACTIVEFLAG, ADDONCODE FROM TLKPIMPSADDONS ORDER BY SORTORDER', [], function(d) {
					FTC_AddOns_Lookup = JSON.parse(JSON.stringify(d));
					getData('SELECT IMPSFLOORDESCRIPTION, APEXLINKFLAG, ACTIVEFLAG FROM TLKPIMPSFLOOR ORDER BY SORTORDER', [], function(d) {
						FTC_Floor_Lookup = JSON.parse(JSON.stringify(d));
						if (callback) callback();
					});
				});
			});
		else if ( _cssktConfig == 'ProVal' || _cssktConfig == 'ProValNew' || _cssktConfig == 'ProValNewFYL' )
			getData("SELECT s.*,c.tbl_element,c.field_2 FROM sketch_codes s join codes_table c on s.tbl_element_desc=c.tbl_element_desc and s.field_1=c.field_1 ORDER BY s.Ordinal", [], function(d) {
				Proval_Lookup = JSON.parse(JSON.stringify(d));
				getData("SELECT (label_code) Code,label_description,UPPER(label_short) label_short FROM imp_labels WHERE (label_code like 'O%') OR (label_code like 'S%') ORDER BY label_description ASC", [], function(d) {
					Proval_Outbuliding_Lookup = JSON.parse(JSON.stringify(d));
					getData("select tbl_element, tbl_element_desc, tbl_type_code from codes_table where tbl_type_code = 'extcover' or tbl_type_code = 'MSExtWall' or tbl_type_code = 'extframe' or tbl_type_code = 'const'", [], function(d) {
						Proval_Default_Exterior_Cover = JSON.parse(JSON.stringify(d));
						getData("select * from sketch_codes", [], function(d) {
							Proval_sketch_codes = JSON.parse(JSON.stringify(d));
							if (callback) callback();
				 		});
					});
				});
			});
		else if ( _cssktConfig == 'MVP' )
			getData("SELECT s.*,c.tbl_element,c.field_2,c.CodesToSysType FROM sketch_codes s join codes_table c on s.tbl_element_desc != 'N/A' and s.tbl_element_desc=c.tbl_element_desc and s.field_1=c.field_1 ORDER BY s.Ordinal", [], function(d) {
				MVP_Lookup = JSON.parse(JSON.stringify(d));
				getData("SELECT label_code, label_short, mdescr, label_text FROM imp_labels i JOIN t_systype t ON i.label_text = t.msysTypeId ORDER BY label_description", [], function(d) {
					MVP_Outbuliding_Lookup = JSON.parse(JSON.stringify(d));
					getData("select * from codes_table where tbl_type_code = 'extcover'", [], function(d) {
						MVP_Default_Exterior_Cover = JSON.parse(JSON.stringify(d));
						getData("select * from sketch_codes", [], function(d) {
							MVP_sketch_codes = JSON.parse(JSON.stringify(d));
							getData("select * from sketch_labels", [], function(d) {
								MVP_sketch_labels = JSON.parse(JSON.stringify(d));
								getData("select * from preferences where PrefSection = 'Sketch' AND PrefEntry IN ('FinishPct1', 'FinishPct2', 'FinishPct3')", [], function(d) {
									MVP_preferences_lookup = JSON.parse(JSON.stringify(d));
									if (callback) callback();
					 			});
				 			});
				 		});
				 	});
				 });
			});
		else if ( _cssktConfig == 'Lafayette' )
			getData("SELECT * FROM lu_sar", [], function(d) {
				Lafayette_Lookup = JSON.parse(JSON.stringify(d));
				if (callback) callback();
			});
		else if ( _cssktConfig == 'Patriot' )
			getData("SELECT * FROM CCV_xrSubArea", [], function(d) {
				Patriot_Lookup = JSON.parse(JSON.stringify(d));
				if (callback) callback();
			});
		else if ( _cssktConfig == 'FarragutJson' )
			getData("SELECT * FROM SKETCH_CODES_LOOKUP", [], function(d) {
				Farragut_Lookup = JSON.parse(JSON.stringify(d));
				if (callback) callback();
            });
        else if (_cssktConfig == 'SigmaCuyahoga')
            getData("SELECT CODE, DESCRIPTION Desc FROM VALUE_LIST WHERE TABLE_NAME = 'RES_AMENITY' AND COLUMN_NAME = 'AMENITY_TYPE'", [], function (d) {
                SigmaCuyahoga_Lookup = JSON.parse(JSON.stringify(d));
                if (callback) callback();
            });
        else if (_cssktConfig == 'WrightJson') {
            getData("SELECT p.[CODE] Id, p.[CODE] || ' - ' || p.[NAME] Name, p.[DESCR], p.[NAME] LName, f.[BASEAREA], f.[FIRST], f.[LOWER], f.[SECOND], f.[THIRD], t.TBLE FROM CCV_APEX_CODES_APEX_CODES_AUD p JOIN CCV_APEX_IAS_TAB_REF_APEX_IAS_TAB_REF_AUD t ON p.CODE = t.CODE JOIN CCV_APEX_ADDN_APEX_ADDN_AUD f ON p.CODE = f.CODE WHERE t.TBLE = 'ADDN' AND p.AUD_OP='I' AND f.AUD_OP='I'", [], function (d) {
                WrightAddn_Lookup = JSON.parse(JSON.stringify(d));
                getData("SELECT p.[CODE] Id, p.[CODE] || ' - ' || p.[NAME] Name, p.[DESCR], p.[NAME] LName, t.TBLE FROM CCV_APEX_CODES_APEX_CODES_AUD p JOIN CCV_APEX_IAS_TAB_REF_APEX_IAS_TAB_REF_AUD t ON p.CODE = t.CODE WHERE t.TBLE IN ('COMINTEXT', 'COMFEAT') AND p.AUD_OP='I'", [], function (d) {
                    WrightCom_Lookup = JSON.parse(JSON.stringify(d));
                    getData("select [CODE] Id, [USETYPE] from CCV_APEX_COMINTEXT_APEX_COMINTEXT_AUD where USETYPE IS NOT NULL", [], function (d) {
                        WrightComType_Lookup = JSON.parse(JSON.stringify(d));
                        if (callback) callback();
                    });
                });
            });
        }
		else if (callback) callback();
		
	}
	else if (callback) callback();
}

function convertToCommaSeparatedDropDown(list, field, idField) {
    let lk = list;
    if (datafieldsettings && list.length == 1 && field && datafieldsettings.filter((x) => { return x.FieldId == field.Id && x.PropertyName == "CommaSeparatedDropDown" && x.Value == '1' })[0]) {
        try {
            let lst = list[0][idField].split(","); lk = [];
            lst.forEach((l) => {
                l = l ? l.trim() : l;
                lk.push({ [idField]: l });
            });
        }
        catch (ex) {
            return list;
        }
    }
    return lk;
}
