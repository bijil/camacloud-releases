﻿var childWindows = {};
var windowParams = {};
var childWindowParamStrings = {};

function openWindow(hwnd, name, url, params) {
    var lsKey = 'window-opener-params-' + name;
    var strParams = '';
    var storedParams = window.localStorage.getItem(lsKey);
    if (storedParams) {
        storedParams = JSON.parse(storedParams);
        for (var x in storedParams) {
            params[x] = storedParams[x];
        }
    }
    for (var x in params) {
        strParams += (strParams != '' ? ',' : '') + x + '=' + params[x];
    }

    hwnd = window.open(url, name, strParams);
    
    var savePosition = function () {
        var loc = getWindowParams(hwnd);
        windowParams[name] = loc;
        window.localStorage.setItem(lsKey, JSON.stringify(loc));
    }

    if (hwnd) {
        childWindows[name] = hwnd;
        childWindowParamStrings[name] = strParams;

        hwnd.onafterresize = savePosition;
        hwnd.onpositionchange = savePosition;

        hwnd.onclose = function () {
            delete childWindows[name];
            delete childWindowParamStrings[name];
        }
    }

    return hwnd;
}

function getWindowParams(hwnd) {
    var h = $(hwnd).height();
    var w = $(hwnd).width();
    var t = hwnd.screenTop;
    var l = hwnd.screenLeft;
    return { width: w, height: h, top: t, left: l };
}

function winTest(t, l, w, h, s) {
    pictometryPopup = openWindow(pictometryPopup, 'pictometry', '/protected/pictometry/', { width: w || 200, height: h || 200, top: t, left: l, resizable: 'true' });
    window.setTimeout(function () { pictometryPopup.close() }, s || 600);
}

window.onunload = function () {
    for (var x in childWindows) {
        childWindows[x].close();
    }
}

function clearChildWindows() {
    for (var x in childWindows) {
        childWindows[x].location.href = 'about:blank' ;
    }    
}