﻿


Number.prototype.formatout = Date.prototype.formatout = String.prototype.formatout = Boolean.prototype.formatout = function (f) {
    var typeName = this.__proto__.constructor.name;
    switch (f) {
        case "money":
        case "$":
            return this.toMoney();
        case "imoney":
        case "$$":
            return this.toMoney(true);
        case "$$$":
            return this.toMoney(true, true);
        case ",":
            return this.toCommaNumber(true);
        case ",.":
            return this.toCommaNumber();
        case "%":
            return toPercent(this);
        case "int":
        case "i":
            return safeParseInt(this).toFixed(0).toString();
        case "f2":
            return safeParseFloat(this).toFixed(2).toString();
        case "f4":
            return this.toFixed(4).toString();
        case "dl":
            return this.toLiteral();
        case "d":
            return toDateString(this.toString());
        case "m":
            return this.getMonth();
        case "y":
        case "yy":
            return this.getYear();
        case "yyyy":
            return this.getFullYear();
        case "ymd":
            return toSQLDateString(this);
        case "s":
            return safeParseString(this);
        case "f":
            return safeParseFloat(this);
        default:
            return this.toString();
    }
}

Number.prototype.toMoney = String.prototype.toMoney = function (round, value) {
    try {
        nStr = 0
        if (this == null || this == "")
            return "";
        if (round == true)
            nStr = safeParseInt(this).toString();
        else if (safeParseInt(this) > 100)
            nStr = safeParseInt(this).toString();
        else
            nStr = parseFloat(this).toFixed(2).toString();
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        var money = x1 + x2;

        if (money == NaN) {
            return "***";
        }

        if ((money == NaN) || (money == Infinity)) {
            return "~";
        }
        if (money != "" && money !="NaN") {
            if (value)
                return money;
            else
                return "$" + money;
        } else
            return "";
    }
    catch (e) {
        console.log(e.message);
    }
}

Number.prototype.toCommaNumber = String.prototype.toCommaNumber = function (round) {
    try {
        nStr = 0
        if (this == 0)
            return "0"
        if (this == null || this == "" || !isFinite(this))
            return "";
        if (round == true || this.toString().indexOf('.') == -1)
            nStr = parseInt(this).toString();
        else
            nStr = parseFloat(this).toString();
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
    catch (e) {
        console.log(e.message);
    }
}

Boolean.prototype.toCommaNumber = function () {
    try {
        if (this == null || this == "")
            return false;
        return this;
    }
    catch (e) {
        console.log(e.message);
    }
}

Date.prototype.toLiteral = function () {
    try {
        var now = new Date();
        var dticks = now.tickSeconds() - this.tickSeconds();
        var dsec = dticks / 1000;
        var dmin = Math.round(dsec / 60);
        var dhrs = Math.round(dmin / 60);
        var ddays = Math.round(dhrs / 24);
        if (dmin < 2) {
            return "just now";
        } else if ((dmin >= 2) && (dhrs < 1)) {
            return "within past hour";
        } else if ((dhrs >= 1) && (dhrs < 3)) {
            return "within past 3 hours";
        } else if ((dhrs >= 3) && (dhrs < 8)) {
            return "within past 8 hours";
        } else if ((dhrs >= 8) && (dhrs < 24)) {
            return "within past 24 hours";
        } else if ((ddays >= 1) && (ddays < 7)) {
            return "a day ago";
        } else if ((ddays >= 7) && (ddays < 30)) {
            return "a week ago";
        } else if ((ddays >= 30) && (ddays < 365)) {
            return "a month ago";
        } else if ((ddays > 365)) {
            return "an year ago";
        }
    }
    catch (e) {
        console.log(e.message);
    }
}

String.prototype.contains = function (infix) {
    return this.indexOf(infix) != -1;
}

function toDateLiteral(x) {
    try {
        if (x == null) {
            return "never";
        } else {
            return x.toLiteral();
        }
    }
    catch (e) {
        console.log(e.message);
    }
}


function safeParseString(x) {
    try {
        if ((x == null) || (x === undefined))
            return "";
        else
            return x.toString();
    }
    catch (e) {
        console.log(e.message);
    }
}

function safeParseInt(x) {
    try {
        if ((x == null) || (x === undefined))
            return 0;
        else {
            if (typeof x == "string") {
                x = x.replace(/,/g, '');
            }
            return parseInt(x);
        }
    }
    catch (e) {
        console.log(e.message);
    }
}

function safeParseFloat(x, allowblank,blankToZero, dvRules) {
    try {
    	if (blankToZero && (x == "" || x == null))
			return 0;
     	if (allowblank && (x !== 0) && ( x == "" || x == null))
        	return "";	
        if (x == null)
            return 0;
        if(dvRules && (x === 0 || x === '0'))
    		return '0';
        if (typeof x == "string")
            x = x.replace(/,/g, '');
        if (x.toString() == "NaN")
            return 0;
        if (isNaN(x))
            return 0;
        if (!isFinite(x))
            return 0;
        return parseFloat(x);
    }
    catch (e) {
        console.log(e.message);
    }
}

function toDateString(str) {
    try {
        var noDate = "";

        if (!str) return noDate;
        var typeName = str.constructor.name;

        if ((str == null) || (str == "")) {
            return noDate;
        }
        
        if (str.length >= 8){
	        if(str.length == 8)
	            str = str.substring(0,4) + '-' + str.substring(4,6) + '-' + str.substring(6,8);       
	        else
	            str = str.substring(0,4) + '-' + str.substring(5,7) + '-' + str.substring(8,10) + ' ' + str.substring(11,str.length);
	    }
        
        var d;
        if (str instanceof Date) {
            d = str;
        } else {
         	var noConvert = clientSettings['NoTimeConversion'] || '0';
        	str = noConvert == 1 ? str : dateTolocal(str);
            d = parseDate(str.substr(0, 10));
           // var dts = str.substr(0, 10)
            //var dts = str.split(" ")[0].split("-");
            //var dd = dts[2];
            //var mm = dts[1];
           // var yyyy = dts[0];
           // return mm + "/" + dd + "/" + yyyy;
            if (isNaN(d)) return str;
        }

        if (d.getMonth() == NaN) { return ""; }
        var ds = (d.getMonth() + 1).toString().padLeft(2, '0') + '-' + d.getDate().toString().padLeft(2, '0') + '-' + d.getFullYear();
        if (ds.startsWith('NaN')) {
            var dt = str.split(" ")[0].split("-");
            var dd = dt[2];
            var mm = dt[1];
            var yyyy = dt[0];
            return mm + "/" + dd + "/" + yyyy;
        }
        return ds;
    }
    catch (e) {
        console.log(e.message);
    }
}
function toCustomDateFormat(str) {
    var noDate = "";

    if (!str) return noDate;
    var typeName = str.constructor.name;

    if ((str == null) || (str == "")) {
        return noDate;
    }
    var d;
    if (str instanceof Date) {
        d = str;
    } else {
        var noConvert = clientSettings['NoTimeConversion'] || '0';
        str = customFormatValue(str);
        str = noConvert == 1 ? str : dateTolocal(str);
        d = parseDate(str);
        if (isNaN(d)) return str;
    }
    if (d.getMonth() == NaN) { return ""; }
    var ds = (d.getMonth() + 1).toString().padLeft(2, '0') + '/' + d.getDate().toString().padLeft(2, '0') + '/' + d.getFullYear();
    if (ds.startsWith('NaN')) {
        var dt = str.split(" ")[0].split("-");
        var dd = dt[2];
        var mm = dt[1];
        var yyyy = dt[0];
        return mm + "/" + dd + "/" + yyyy;
    }

    return ds;
}
function toSQLDateString(str) {
    var noDate = "";

    if (!str) return noDate;
    var typeName = str.constructor.name;

    if ((str == null) || (str == "")) {
        return noDate;
    }
    var d;
    if (str instanceof Date) {
        d = str;
    } else {
        d = parseDate(str.substr(0, 10));
        if (isNaN(d)) return str;
    }

    if (d.getMonth() == NaN) { return ""; }
    var ds = d.getFullYear() + '-' + (d.getMonth() + 1).toString().padLeft(2, '0') + '-' + d.getDate().toString().padLeft(2, '0');
    if (ds.startsWith('NaN')) {
        var dt = str.split(" ")[0].split("-");
        var dd = dt[2];
        var mm = dt[1];
        var yyyy = dt[0];
        return yyyy + "-" + mm + "-" + dd;
    }

    return ds;
}

function toPercent(f) {
    try {
        if (f == null) { return ""; }
        if (isNaN(f)) { return ""; }
        if (!isFinite(f)) { return ""; }
        return ((f * 100).toFixed(2)) + "%";
    }
    catch (e) {
        console.log(e.message);
    }
}

function toMoney(x) {
    try {
        if (x == null)
            return "";
        var ret = x.toMoney();
        return ret;
    }
    catch (e) {
        console.log(e.message);
    }
}

function toCommaFormat(x) {
    try {
        if (x == null)
            return "";
        return x.toCommaNumber(true);
    }
    catch (e) {
        console.log(e.message);
    }
}

function toIntMoney(x) {
    try {
        if (x == null)
            return "";
        return x.toMoney(true);
    }
    catch (e) {
        console.log(e.message);
    }
}

function toCostValue(x) {
    try {
        if (x == null)
            return "";
        return x.toMoney(true, true);
    }
    catch (e) {
        console.log(e.message);
    }
}

function isDecimal(o) {
    try {
        if (typeof o == "number") {
            if (o.toString().indexOf(".") > -1) {
                return true;
            }
        }
        return false;
    }
    catch (e) {
        console.log(e.message);
    }
}

Date.prototype.getTimeZone = function () {
    try {
        var ts = (new Date()).toTimeString();
        var m = ts.match(/GMT(.*?)\s/);
        if (m.length > 1) {
            return m[1];
        } else {
            return "+00:00"
        }
    }
    catch (e) {
        console.log(e.message);
    }
}

function parseDate(input, format) {
    try {
        format = format || 'yyyy-mm-dd'; // default format
        var parts = "";
        if(input!="")
    	parts = input.match(/(\d+)/g);
      	var i = 0, fmt = {};
        // extract date-part indexes from the format
        format.replace(/(yyyy|dd|mm)/g, function (part) { fmt[part] = i++; });
        return new Date(parts[fmt['yyyy']], parts[fmt['mm']] - 1, parts[fmt['dd']]);
    }
    catch (e) {
        console.log(e.message);
    }
}
function replaceLikeOperatorsOld(st) { // Delete, if below one is correct
    st = st.replace(/\s*(%=%)\s*/g, '%=%');
    st = st.replace(/\s*(%=)\s*/g, '%=');
    st = st.replace(/\s*(=%)\s*/g, '=%');
    var iter = 0, pArray = [], retVal = '';
    while (iter < st.length) {
        pArray.push(st[iter]);
        ++iter;
    }
    //  for (x in pArray) {
    iter = 0;
    while (iter < pArray.length) {
        if (pArray[iter] == '%') {
            if (pArray[parseInt(iter) + 1] == '=') {
                if (pArray[parseInt(iter) + 2] == '%') {//contains
                    retVal += '.contains("';
                    for (var k = parseInt(iter) + 4; pArray[k] != '"'; k++) {
                        retVal += pArray[k];
                    }
                    retVal += '")';
                    iter = k + 1;
                }
                else {//start with
                    retVal += '.startsWith("';
                    for (var k = parseInt(iter) + 3; pArray[k] != '"'; k++) {
                        retVal += pArray[k];
                    }
                    retVal += '")';
                    iter = k + 1;
                }
            }
            else if (pArray[parseInt(iter) - 1] == '=') {//ends with
                retVal = retVal.substring(0, retVal.length - 1);
                retVal += '.endsWith("';
                for (var k = parseInt(iter) + 2; pArray[k] != '"'; k++) {
                    retVal += pArray[k];
                }
                retVal += '")';
                iter = k + 1;
            }
        }
        else {
            retVal += pArray[iter];
            ++iter;
        }
    }
    //}
    return retVal;
}

function replaceLikeOperators(st, jsType) {
    var iter = 0, pArray = [], retVal = '', likeVal = '';
    var appendLikeVal = function (i) {
        likeVal = (pArray[parseInt(iter) + i])
        while (likeVal == " ") {
            iter++;
            likeVal = (pArray[parseInt(iter) + i])
        }
        if (likeVal == "'") {
            likeVal = "";
            while (pArray[parseInt(iter) + i + 1] != "'") {
                likeVal += pArray[parseInt(iter) + i + 1];
                ++iter;
            }
        }
        retVal += likeVal
        retVal += "')";
        iter = parseInt(iter) + i + 2;
    }
    while (iter < st.length) {
        pArray.push(st[iter]);
        ++iter;
    }
    iter = 0;
    while (iter < pArray.length) {
        if (pArray[iter] == '%') {
            if (pArray[parseInt(iter) + 1] == '=') {
                if (jsType == 'Number')
                    retVal += '.toString()';
                if (pArray[parseInt(iter) + 2] == '%') {//contains
                    retVal += ".contains('";
                    appendLikeVal(3)
                }
                else {//start with
                    retVal += ".startsWith('";
                    appendLikeVal(2)
                }
            }
            else if (pArray[parseInt(iter) - 1] == '=') {//ends with
                retVal = retVal.substring(0, retVal.length - 1);
                if (jsType == 'Number')
                    retVal += '.toString()';
                retVal += ".endsWith('";
                appendLikeVal(1);
            }
        }
        else {
            retVal += pArray[iter];
            ++iter;
        }
    }
    return retVal;
}


String.prototype.utcStringToLocalTime = function () {
    var udt = parseInt(Date.parse(this));
    var lt = udt - (new Date()).getTimezoneOffset() * 60 * 1000;
    return new Date(lt);
};

function Sortscript( fieldsitems, tableName )
{//valueObject for inner value object eg:activeparceldata
    if ( fieldsitems == null || fieldsitems == '' ) return ""
    var SortItems = fieldsitems.trim().split( ',' );
	if (SortItems.indexOf('ROWUID') > -1) {
		SortItems.splice(SortItems.indexOf('ROWUID'),1);
		if (SortItems.length == 0) return "";
	}
    var sortScript = ".sort(function(x,y){ return("
    SortItems.forEach( function ( Item ) {
        var dir = '>', dir1 = '==', dir2 = '<' , sortField = Item.trim().split( " " )[0];
        var sortFieldScript = sortField
        var dt = getDataField( sortField, tableName );
        if ( dt ) dt = dt.InputType;
        
        if ( dt == "2" || dt == "7" || dt == "8" || dt == "9" ) {
            sortFieldScript = 'parseFloat(' + sortFieldScript + ')';
            //dir = '-'
        }
        else if ( dt == "4" ){
            sortFieldScript = 'new Date(' + sortFieldScript + ')';
            //dir = '-'
        }
        else if (dt == "1" || dt == "5") {
            sortFieldScript = '(' + sortFieldScript + '!== null  && ' + sortFieldScript + '!== "" && !isNaN(' + sortFieldScript + ')? (' + sortFieldScript + '): ' + sortFieldScript + ')';
        }
        
        if (dt == "4") {
            var sortFieldRegx = new RegExp(sortField, 'g');
            if( Item.replace( sortField, '' ).trim().toLowerCase() == "desc" )
                sortScript += '(' + sortFieldScript.replace(sortFieldRegx, 'y.' + sortField) + ' ' + dir + sortFieldScript.replace(sortFieldRegx, 'x.' + sortField) + '?1:' + sortFieldScript.replace(sortFieldRegx, 'y.' + sortField) + ' ' + dir2 + sortFieldScript.replace(sortFieldRegx, 'x.' + sortField ) + '?-1:0) ||';
            else
                sortScript += '(' + sortFieldScript.replace(sortFieldRegx, 'x.' + sortField) + ' ' + dir + sortFieldScript.replace(sortFieldRegx, 'y.' + sortField) + '?1:' + sortFieldScript.replace(sortFieldRegx, 'x.' + sortField) + ' ' + dir2 + sortFieldScript.replace(sortFieldRegx, 'y.' + sortField ) + '?-1:0) ||';
        }
        else {
        	if ( Item.replace( sortField, '' ).trim().toLowerCase() == "desc" )
                sortScript += '(' + sortFieldScript.replaceAll(sortField, 'y.' + sortField) + ' ' + dir + sortFieldScript.replaceAll(sortField, 'x.' + sortField) + '?1:' + sortFieldScript.replaceAll(sortField, 'y.' + sortField) + ' ' + dir1 + sortFieldScript.replaceAll( sortField, 'x.' + sortField ) + '?0:-1) ||';
        	else
                sortScript += '(' + sortFieldScript.replaceAll(sortField, 'x.' + sortField) + ' ' + dir + sortFieldScript.replaceAll(sortField, 'y.' + sortField) + '?1:' + sortFieldScript.replaceAll(sortField, 'x.' + sortField) + ' ' + dir1 + sortFieldScript.replaceAll( sortField, 'y.' + sortField ) + '?0:-1) ||';
    	}
    });
    return sortScript.slice( 0, -2 ) + ")})"
}

function SortscriptCASE(fieldsitems, tableName) {//valueObject for inner value object eg:activeparceldata
    if (fieldsitems == null || fieldsitems == '') return ""
    fieldsitems=fieldsitems.replace(/\s\s+/g, ' ');
    fieldsitems=fieldsitems.replace(/WHEN/gi, "WHEN");
    fieldsitems=fieldsitems.replace(/CASE/gi, "CASE");
    fieldsitems=fieldsitems.replace(/ELSE/gi, "ELSE");
    fieldsitems=fieldsitems.replace(/ASC/gi, "ASC");
    fieldsitems=fieldsitems.replace(/DESC/gi, "DESC");
    var SortItems = fieldsitems.trim().split('WHEN');    
	if (SortItems.indexOf('ROWUID') > -1) {
		SortItems.splice(SortItems.indexOf('ROWUID'),1);
		if (SortItems.length == 0) return "";
	}
	var sortField = SortItems[0].replace('CASE','').trim();
    var sortScript = ".sort(function(a,b){"
    var i,item,con1="",con2="";
    for(i=1;i<SortItems.length;i++){
        item=SortItems[i].split(" ");
        con1+="x=="+item[1]+"? x="+item[3].replace('ELSE','')+": ";
        con2+="y=="+item[1]+"? y="+item[3].replace('ELSE','')+": ";
    }
    con1+="x=x;";
    con2+="y=y;";
    var order=fieldsitems.trim().split(" ");
    if ((order[order.length-1]) == "DESC"){
   		sortScript+='var x=a.'+ sortField +',y=b.'+ sortField +';' + con1 + con2 + 'return((y>x ? 1 : y == x ? 0 : -1) )});';
    }
    else{
    	sortScript+='var x=a.'+ sortField +',y=b.'+ sortField + ';' +con1 + con2 + 'return((x>y ? 1 : x == y ? 0 : -1) )});';
    }
          
    return sortScript;
}


function dateTolocal( dt )
{
    if ( dt === null || dt === undefined ) return;
    dt = dt.trim();
    var Notdt = "";
    if ( isNaN( Date.parse( dt ) ) == true )
    {
        return toSQLDateString( Notdt );
    }
    if ( dt.length > 12 )
    {
        dt = dt.replace( ' ', 'T' );
        dt = dt.replace( 'AM', '' ).replace( 'PM', '' );
    }
    else
    { return dt.trim(); }
    var d = new Date( dt );
    if ( d == 'Invalid Date' )
    {
        dt = dt.split( 'T' )[0];
        d = new Date( dt );
        return d.toISOString().substring( 0, 10 );
    }
    var timeZoneFromDB = parseInt(( clientSettings["TimeZone"] || '-05:00' ).replace( ":", "." ) ); //time zone value from database
    var dst = parseInt( clientSettings["DST"] ) || 0; //DST value from database
    if ( dst == '1' )
    {
        var startDate = getDSTStart( d.getFullYear() );
        var endDate = getDSTEnd( d.getFullYear() );
        if ( d >= startDate && d <= endDate )
        {
            timeZoneFromDB = timeZoneFromDB + dst;
        }
    }
    var tzDifference = timeZoneFromDB * 60 * 60 * 1000;
    var offsetTime = new Date( d.getTime() + tzDifference );
    return toSQLDateString( offsetTime );
}
Object.defineProperties( Array.prototype, { any: { enumerable: false, get: function () { return function ( v ) { return this.indexOf( v ) > -1; } } }, all: { enumerable: false, get: function () { return function ( v ) { return this.map( function ( x ) { return x == v } ).reduce( function ( x, y ) { return x && y }, this.length > 0 ); } } }, first: { enumerable: false, get: function () { return function ( v ) { return ( v === undefined && this[0] ) || this.indexOf( v ) == 0; } } }, last: { enumerable: false, get: function () { return function ( v ) { return ( v === undefined && this[this.length - 1] ) || this.indexOf( v ) == this.length - 1; } } }, sum: { enumerable: false, get: function () { return function ( v ) { var r = this.length > 0 && ( this.reduce( function ( a, b ) { return parseFloat( a ) + parseFloat( b ); } ) ); return ( v === undefined && r ) || r == v } } }, avg: { enumerable: false, get: function () { return function ( v ) { var r = this.length > 0 && ( this.reduce( function ( a, b ) { return parseFloat( a ) + parseFloat( b ); } ) ) / this.length; return ( v === undefined && r ) || r == v } } }, count: { enumerable: false, get: function () { return function ( v ) { return ( v === undefined ? this.length : this.length == v ) } } }, min: { enumerable: false, get: function () { return function ( v ) { return ( v === undefined && Math.min.apply( null, this ) ) || Math.min.apply( null, this ) == v } } }, max: { enumerable: false, get: function () { return function ( v ) { return ( v === undefined && Math.max.apply( null, this ) ) || Math.max.apply( null, this ) == v } } } } );