﻿var activeParcel;
var activeFieldChanges;
var activeTab;
var activeParcelEdits = [];
var activeParcelPhotoEdits = [];
var activeParcelAlert = [];
var activeParcelPriority = [];
var recoveredItems = [];
var activeParcelQC;
var priorityvalues = [];
var dashboardChanged = false;
var validations;
var dataSourceFields;
var Notificationmsg = "";
var clientSettings = {};
var sketchSettings = {};
var neighborhoods = {};
var listparcels = [];
var displayFields = [];
var relationcycle = [];
var TableHierarchical = [];
var FirstPhoto = "";
var SketchPreview = "";
var parentIndex;
var fIndx = [];
var classCalculatorValidationResult = [];
var childRecCalcPattern = /\\.|(sum|avg)|\(\)/i;
var showFutureData = false;
var isBPPParcel= false;
var showfrm = false;
var shwfrm = false;
var activeParcelAllData;
var tableListGlobal = [];
var disableSwitch = false;
var autoselectloop = [];
var LFields = [];
var recoveryDeletedArrays = [];
var AtcWorkflowrole;
var deletedWorkflow = [];
var classCalculationNewValue;
var calculatedClassDisplayValue;

function handleError() {
    try {
        if (activeParcel.status == 'Error') {
            console.log('Parcel not found');
            closeParcelView();
            hideMask();
            return false;
        }
    }
    catch (e) {
    }
}

//Parcel navigation

function openParcelWithHash(pid) {
	$('.category-page').removeAttr('findex index rowuid childrowuid prowuid');
    window.location.hash = "/parcel/" + pid;
    refreshSearch();
} 
function closeParcelView() {
    activeParcelAlert = [];
    activeParcelPriority = [];
    try {
    hide('.field-audit-qc');
    clearFindex();
    if( qPublicWindow)  qPublicWindow.close();
        checkIfSaved(null, null, function () {
            switchToSearchView();
            switchToActualYear();
            $(".parcel-results").Refresh();
        })
    } catch (e) { }
    return false;
}

function openNextParcel() {
    activeParcelAlert = [];
    activeParcelPriority = [];
    try {
        clearFindex();
    	if( qPublicWindow)  qPublicWindow.close();
    	if (!__DTR && sketchEditor) sketchEditor.close();
        checkIfSaved(null, null, function () {
            navigateParcelList(1, function (pid) {
                openParcelWithHash(pid);
                updateParcelDashboard();
            });
        })
    } catch (e) { }
    return false;
}

function openPreviousParcel() {
    activeParcelAlert = [];
    activeParcelPriority = [];
    try {
    	clearFindex();
    	if( qPublicWindow)  qPublicWindow.close();
    	if (!__DTR && sketchEditor) sketchEditor.close();
        checkIfSaved(null, null, function () {
            navigateParcelList(-1, function (pid) {
                openParcelWithHash(pid);
                updateParcelDashboard();
            });
        })
    } catch (e) { }
    return false;
}

function switchToActualYear(){
	if ($(document).find( '.onoffswitch-checkbox' ).is( ':checked')) $(document).find( '.onoffswitch-checkbox' ).removeAttr( 'checked' );
	localStorage.setItem( 'showFutureData', false );
	
}

function newParcel(data) {
    _.extend(this, data);
}
function Parcel(parcelId, x) {
    this.Original = {};
    this.SPACE = ' ';
    this.CopyFrom = function (obj) {
        for (key in obj) {
            key = key.split(':')[0];
            var js = 'this.' + key + ' = obj.' + key + ";"
            var js2 = 'this.Original.' + key + ' = obj.' + key + ";"
            try {
                eval(js);
                eval(js2);
            } catch (e) {
                console.error(e);
                console.error(js);
            }
        }
        eval('this.Id = "' + parcelId + '";');
        eval('this.__ID = "' + parcelId + '";');
    };
    this.that = this;
    if (x != null) {
        this.CopyFrom(x)
    }
}
$.extend(Parcel.prototype, ccma.Data.Evaluable);
$.extend(newParcel.prototype, ccma.Data.Evaluable);
function navigateParcelList(d, callback) {
    var ss = getSearchStatus({ ParcelId: activeParcel.Id });
    var newParcelIndex = ss.parcelIndex + d;
    var minIndex = 0;
    var maxIndex = ss.pageSize - 1;
    var maxPages = Math.ceil(ss.total / ss.pageSize);
    if (ss.page == maxPages)
        maxIndex = Math.min(ss.total % ss.pageSize, ss.pageSize) - 1;
    var pd = 0;
    var pagedir = "";
    if (newParcelIndex > ss.maxIndex) {
        if (ss.page == ss.maxPages) {
            alert('You have reached the end of the list.');
            return false;
        }
        pd = 1;
        pagedir = "next";
        newParcelIndex = 0;
    }
    else if (newParcelIndex < ss.minIndex) {
        if (ss.page == 1) {
            alert('You are now at the beginning of the list.');
            return false;
        }
        pd = -1;
        pagedir = "prev";
        newParcelIndex = ss.pageSize - 1;
    }

    var getNewPid = function () {
        var pid = $($('.parcel-results tr[parcelid]')[newParcelIndex]).attr("parcelId");
        if (callback)
            callback(pid);
        else
            console.log(pid);
    }
    if (pd == 0) {
        getNewPid();
    } else {
        $('.parcel-results').ChangePage(pagedir, getNewPid);
    }
}

function applyParcelChanges(callback) {
    for (x in activeParcel.ParcelChanges) {
        var a = activeParcel.ParcelChanges[x];
        var oldValue = a.OriginalValue;
        var value = a.NewValue ? a.NewValue : null;
        if (a.Action != 'delete') {
            if ((a.AuxROWUID == null || (activeParcel[a.FieldName] !== undefined && activeParcel['ROWUID'] == a.AuxROWUID)) && a.FieldName && !(a.SourceTable && activeParcel[a.SourceTable])) {
                activeParcel[a.FieldName] = value;
                activeParcelAllData[a.FieldName] = value;
                activeParcel.Original[a.FieldName] = oldValue;
                activeParcelAllData.Original[a.FieldName] = oldValue;
                //            activeParcelData[a.FieldName].Value = value;
                //            activeParcelData[a.FieldName].Original = oldValue;
            } else {
                var source = a.SourceTable;
                for (ex in activeParcel[source]) {
                    var acx = activeParcel[source][ex];
                    if (acx.ROWUID == a.AuxROWUID && a.FieldName) {
                        // activeParcelData[source][ex][a.FieldName].Original = oldValue;
                        // activeParcelData[source][ex][a.FieldName].Value = value;
                        activeParcel[source][ex][a.FieldName] = value;
                        activeParcelAllData[source].filter(function(f){return f.ROWUID == a.AuxROWUID})[0][a.FieldName] = value;
						activeParcel.Original[source].filter(function(f){return f.ROWUID == a.AuxROWUID})[0][a.FieldName] = oldValue;
                        activeParcelAllData.Original[source].filter(function(f){return f.ROWUID == a.AuxROWUID})[0][a.FieldName] = oldValue;
                    }
                }
            }
        }
    };
    if (callback) callback();
}

function getAuxiliaryTableNames(callback) {
    getData("SELECT DISTINCT SourceTable FROM FieldCategory", [], function (sources) {
        var tableList = [];
        tableListGlobal = [];
        for (var x in sources) {
            if(sources[x].SourceTable != null)
                tableList.push(sources[x].SourceTable);
        }

        function checkAndAddTable(tableName) {
            if (tableName && tableName != '')
                if (tableList.indexOf(tableName) == -1) {
                    tableList.push(tableName);
                }
        }

        checkAndAddTable( clientSettings['SketchTable'] || sketchSettings['SketchTable'] )
        checkAndAddTable( clientSettings['SketchVectorTable'] || sketchSettings['SketchVectorTable'] )

        if (ccma && ccma.Sketching && ccma.Sketching.Config) {
            var sources = ccma.Sketching.Config.sources;
            for (var y in sources) {
                var s = sources[y];
                if (s.VectorSource) checkAndAddTable(s.VectorSource.Table);
                if (s.SketchSource) checkAndAddTable(s.SketchSource.Table);
                if (s.NotesSource) checkAndAddTable(s.NotesSource.Table);
            }
        }

        if (callback){ tableListGlobal = _.clone(tableList);  callback(tableList);}
    });
}
// Open Parcel
//loadNeighborhoods();
function getParcel(parcelId, callback, noRefreshPictometry, refreshGrid, noRefreshData) {
    $('.recover-property').hide();
    $('.Commit_Change').hide();
	$('.delete-property').hide();
    $('.parcel-field-values .input').attr('i', 'i');
    $('.qc a').hide();
    hide('.field-audit-qc');
    dashboardChanged = false;
    $('.qc a').removeAttr('changeid');
    $('.parcel-field-values td.value').removeClass('unsaved');
    $('.field-category-link').removeClass('approved field-category-edited');
    $('.tabs li a[category="sketch-manager"]').removeClass('approved');
    relationcycle = [];
    recoveryDeletedArrays = [];
    deletedWorkflow = [];
    $qc('parcel', { ParcelId: parcelId }, function (allData) {
	    activeParcelAllData = _.clone(allData);
	    activeParcelAllData.Original = {}
	    activeParcelAllData.Original = JSON.parse(JSON.stringify(allData));
	    activeParcelAllData = new newParcel(activeParcelAllData);
		futureparcelData(allData,function(data){
	        activeParcel = data;
	        activeParcel.Original = {}
	        activeParcel.Original = JSON.parse(JSON.stringify(data));
	        activeParcel = new newParcel(activeParcel);
	        isBPPParcel = (activeParcel.IsPersonalProperty == '1' || activeParcel.IsPersonalProperty) ? true : false;
	        if (activeParcel.ShowKeyValue1 == '0' && activeParcel.ShowAlternateField == '1'){
	            $('.parcel-head-values .alternate').html('');
	            $('.parcel-head-values .value[fieldname="Alternatekeyfieldvalue"]').html('');
	            $('.parcel-head-values .value[id="altf"]').attr("fieldname", "");
	            $('.parcel-head-values .value[id="kyf"]').attr("fieldname", "Alternatekeyfieldvalue");
	            $('.hidedheader .value1[id="hidedKeyfield"]').hide();
	            $('.hidedheader .keyfield[id="hidedKeyname"]').hide();
	        }
	        else if (activeParcel.ShowKeyValue1 == '1' && activeParcel.ShowAlternateField == '0') {
	            if(isBPPParcel && activeParcel.KeyValue1 == '0')
	            	activeParcel.KeyValue1 = activeParcel.ClientROWUID;
	            $('.parcel-head-values .value[fieldname="Alternatekeyfieldvalue"]').html('');
	            $('.parcel-head-values .value[id="altf"]').attr("fieldname", "");
	            $('.hidedheader .value1[id="hidedAlt"]').hide();
	            $('.hidedheader .alternate[id="hidedAltname"]').hide();
	        }
	        else if (activeParcel.ShowKeyValue1 == '1' && activeParcel.ShowAlternateField == '1') {
		           if(isBPPParcel && activeParcel.KeyValue1 == '0')
		            activeParcel.KeyValue1 = activeParcel.ClientROWUID;
		               $('.parcel-head-values .value[fieldname="KeyValue2"]').show();
		               $('.parcel-head-values .value[fieldname="KeyValue3"]').show();
		               $('.parcel-head-values .value[fieldname="Alternatekeyfieldvalue"]').show();

		               
		       }
	         if(!activeParcel.StreetAddress){
	        	$('.hidedheader .streetHide').hide();
	        }
	        else {
	             $('.hidedheader .streetHide').show();
	             }
	        //else if (isBPPParcel && activeParcel.KeyValue1 == '0')
	        //    activeParcel.KeyValue1 = activeParcel.ClientROWUID;
	        //if (activeParcel.ShowAlternateField == '1' && activeParcel.ShowKeyValue1 == 1)
	        //    activeParcel.KeyValue3 = activeParcel.Alternatekeyfieldvalue;
	        //  activeParcelData = JSON.parse(JSON.stringify(data));
	        //  activeParcelData = new newParcel(activeParcelData);
	        activeFieldChanges = [];
            activeParcelEdits = [];
            activeParcelAlert = [];
            activeParcelPriority = [];
	        recoveredItems = [];
	        activeParcelPhotoEdits = [];
	        activeParcelQC = null;
	        setScreenDimensions();
	        updateParcelDashboard();
	        
	        var globalms1 = globalms;
	        $('td.value.medtext.rightpad').show();
	        if(__DTR){
	           var docHeight = $(window).height();
	           var cHeight = docHeight - $('#topNavBar').innerHeight() - $('#footer').innerHeight();
	           cHeight = cHeight + ($(document).height() - $(window).height());
	           setPageLayout($('#contentTable').width(), cHeight-3);
            }
	        $('#hdnKeyValue1').val(activeParcel.KeyValue1);
	        $('#hdnAlternate').val(activeParcel.Alternatekeyfieldvalue);
	        applyParcelChanges(function(){
			for (x in activeParcel) {
	            var field = activeParcel[x];
	            var block = getDataField(x, null) ? $('.category-page[categoryid="' + getDataField(x, null).CategoryId + '"]') : null;
	            setFieldValue(field, x, block,null,null,null,true);
	        }});
	        for (var x in activeParcel.ParcelChanges) {
	            var ch = activeParcel.ParcelChanges[x];
	            if (ch.AuxROWUID == null) {
	               var qc = $('.qc[fieldid="' + ch.FieldId + '"]');
	                $('a', qc).hide();
	                $((activeParcel.QC && ch.QCChecked) ? '.passed' : '.check', qc).show();
	                $((activeParcel.QC && ch.QCChecked) ? '.passed' : '.check', qc).attr('changeid', ch.Id);
	                setFieldValue(ch.NewValue, ch.FieldId,null,null,null,null,true);
	            }
	        }
	        for (ci in fieldCategories) {
	            var cat = fieldCategories[ci];
	            if (cat.SortExpression) {
	                $('.category-page[categoryid="' + cat.Id + '"]').attr('sort-exp', cat.SortExpression);
	            }
	        }
	        if (refreshGrid == true) {
	            $('.grid-view').html('');
	            gridView();
	            $('.category-page').attr('findex', 0);
	            $('.category-page').attr('index', 0)
	        }                    	
			$('option', '.select-sketch-segments').remove();
			$(".sketch-frame").css('background-image', 'none');
			activeParcel.Photos = [];
            activeParcel.sketchImages = [];
            activeParcel.Images = activeParcel.Images.filter((x) => { return !activeParcel.QC || (activeParcel.QC && !x.CC_Deleted) });
			for (x in activeParcel.Images) {
                var img = activeParcel.Images[x];
				if (img.IsSketch) {
					activeParcel.sketchImages.push(copyDataObject(img));
				} else {
					activeParcel.Photos.push(copyDataObject(img));
				}
			}
			removeExceededPhotos();
	        showPhotoThumbnail();
	        
	        if ((clientSettings['EnableMALite'] == '1' || clientSettings['EnableMALite'] == 'True' || clientSettings['EnableMALite'] == 'true') && (activeParcel.QC == 'true' || activeParcel.QC == "True" || activeParcel.QC == '1') )
			{
			    $('.Commit_Change').show()
			}
			else
			{
			    $('.Commit_Change').hide()
			}
	
	        getAuxiliaryTableNames(function (tableList) {
	        	insertDataIntoTables(_.clone(tableList.concat('Parcel')), function(){
	            	LoadAuxDataPRC(activeParcel, tableList, function () {
		                loadAuxData(activeParcel, tableList, function (parcel) {
		                    activeParcel = parcel;
		                    sortBySortExpression();
		                    loadPRCactiveParcel(TableHierarchical, activeParcel, function () {
		                    	//initLookupFields(function() {
							        var sketches = getSketchSegments();
							        for (x in sketches) {
							            var skt = sketches[x];
							            $('.select-sketch-segments').append($('<option>', { value: skt.uid, text: skt.label }));
							        }
			                    	if(!noRefreshData){
			                    		var Clist = Object.keys( datafields ).map( function ( x ) { return datafields[x] } ).filter(function (x) { return (x.CalculationExpression != null &&  x.CategoryId !=null) });
       									Clist = _.uniq(Clist.map(function(field){ return field.CategoryId }));
			                    		$('.category-page[auxdata]').each(function (a, b) {
			                    	    	//var findex = $(b).attr('findex');
			                    	    	//if (!findex) 
			                    	    	//findex = 0;
			                    	    	var ShowAuxCall;
			                    	      	var ids = getChildCategories(parseInt($(this).attr('categoryid')))
			                    	    	while(ids.length > 0) {
								    			var eachId = ids.pop();
								    			if(!Clist.includes(parseInt(eachId))){
								        			ShowAuxCall = true;
								        			break;
								    			}
											}
			                    	    	if(ShowAuxCall || !Clist.includes(parseInt($(this).attr('categoryid'))))
			                    	    		showAuxData(this, 0 ,null,null,null,null,true, true);
			                    		});
			                    	}
			                   		// applyParcelChanges(function () {
			                   		
			                   		if (!__DTR && localStorage.internalSupportView == '1' && !noRefreshData) {
			                   			switchSupportView();
			                   		}
			                   		
			                        if (map)
       									resetMap();
       								if (globalms1 && globalms1.on && map) {
								        var measureDiv = $('.measurement-tool')[0];
								        if (measureDiv) {
								            map.setOptions({
								                draggableCursor: null
								            });
								            measureDiv.className = 'measurement-tool measure-off tip';
								            $(measureDiv).attr({
								                "abbr": 'Click on the measuring tool to activate it and measure. Click again to deactivate the tool.',
								                "width": '400px;'
								            });
								            $('.overlay').removeClass('pointer')
								            globalms1.clear();
								            globalms1.on = !globalms1.on;
								        }
								    }
								    
		                            if (__DTR) {
		                                if (!noRefreshPictometry) {
		                                	if(clientSettings["EagleView.APIKey"] && clientSettings["EagleView.APIKey"].trim() !="")
                                                loadPictometry();
                                            if (clientSettings["NewEV.ClientID"] && clientSettings["NewEV.EVClientSecret"].trim() != "")
                                                loadEagleView();
		                                    if(clientSettings['EnableNearmap'] && clientSettings['EnableNearmap'].includes('DTR') && clientSettings["Nearmap.APIKey"] && clientSettings["Nearmap.APIKey"].trim() !="")
                                                loadNearmap();
                                            if (clientSettings['EnableNearmapWMS'] && clientSettings['EnableNearmapWMS'].includes('DTR') && clientSettings["NearmapWMS.APIKey"] && clientSettings["NearmapWMS.APIKey"].trim() != "")
                                                loadNearmapWMS();
		                                     if(clientSettings['woolpertApikey'] && clientSettings['woolpertApikey'].trim() !=""){
		                                    	$('.woolpert').show();
		                                    	loadWoolpert();
		                                    	}
		                                    //openSketchEditor();

		                                    if(clientSettings['EnableCyclomedia']== "1"){
		                                    	$('.cyclomedia').show();
		                                    	loadCyclomedia();
		                                    	}
		                                    if(clientSettings['EnableqPublic']== "1" && clientSettings['qPublicKeyField'] && clientSettings['qPublicKeyField'].trim() !=''){
		           	 							$('.qPublic').show();	
		           	 							loadQPublic();
		           	 							}
		           	 						if(clientSettings['EnableSanborn']== "1" ){
	           	 								$('.sanborn').show();	
	           	 								loadSanborn();
                                            }
                                            if (clientSettings['HideSketchTab'] !== "1") {
                                                openSketchEditor();
                                            }
                                        }

                                        let pwindowOpen = false;
		                                if (childWindows.parcelPhotos) {
		                                    if (childWindows.parcelPhotos.closed == false && childWindows.parcelPhotos.location.href == "about:blank") {
                                                localStorage.setItem('PhotosPopupSatus', true); pwindowOpen = true;
                                            }
                                            else if (!childWindows.parcelPhotos.closed) pwindowOpen = true;
                                        }

                                        if (localStorage.getItem('PhotosPopupSatus') == "true" && pwindowOpen) {
                                            loadParcelPhotos();
                                            $(".tabs li[category='photo-viewer']").css("display", "none");
                                        }
		                            }
		                            if (callback) callback();
		                            $.extend(activeParcel.prototype, ccma.Data.Evaluable);
		                            // $.extend(activeParcelData.prototype, ccma.Data.Evaluable);
		                            if (validate_QC_SketchConfig()) {
		                                showSketchesInQC(true);
		                                var hideSketchTab = clientSettings["HideSketchTab"];
		                                if (hideSketchTab == '1') {
		                                    $('.tabs-main li[sketch]').hide();
		                                } else {
		                                    $('ul.tabs li[sketch]').show();
		                                }
		                            }
		                            else {
		                                console.log('Please update sketch settings');
		                                $('ul.tabs li[sketch]').hide();
		                            }
			                    	PrintsketchView();
		                    	//});
	                        });
		                    //});
		                });
	            	});
	            });
	        });
	        updateRecentAuditTrail();
	        var clsFCE = 'field-category-edited';
	        $('.field-category-link').removeClass(clsFCE);
	        $('.field-category-link').removeClass('approved');
	        highlightCatLink();
	        ShowAssignmentGroup();
	        handleBPPParcelMenu();
	        hideTabsInQC();
			$('.field-category-link[showCategoryTrue="0"]').css('display', 'none');
	        var _panorama = map? map.getStreetView(): null; //clear streetview when opening a parcel
	        if(_panorama)
            	_panorama.setVisible(false);
	    });
    });
}
function handleBPPParcelMenu(){
	if(isBPPParcel && clientSettings["EnablePersonalProperty"] == '1'){
		$('.tabcontents #parceldata .category-menu a').each(function(){
			var cid = $(this).attr('category');
			var catType = getCategory(cid).Type;
			if (catType == '0' || catType == null)
				$(this).hide();
			else
				$(this).show();
		});
		bppParcelBanner();
	}
	else{
		$('.tabcontents #parceldata .category-menu a').each(function(){
			var cid = $(this).attr('category');
			var catType = (getCategory(cid) && getCategory(cid).Type)? getCategory(cid).Type: '';
			if ( clientSettings["EnablePersonalProperty"] == '1' && catType == '1')
				$(this).hide();
			else
				$(this).show();
		});	
		$('.bpp-property').hide();
		$('.bpp-parcel-info').hide();
	 }
	 if(activeParcel.ChildParcels && activeParcel.ChildParcels != [] && clientSettings["EnablePersonalProperty"] == '1') $('.tabs-main li[category="bpp-child-parcels"]').css({ "display": "" });
     else $('.tabs-main li[category="bpp-child-parcels"]').css({ "display": "none" });

}
function bppParcelBanner(action) {
		//var parentPropLink="<a value ="+activeParcel.ParentParcelID+" onclick=openBPPParcel(this) style='text-decoration: underline;'>"+activeParcel.ParentKeyValue+"</a>"
		//var dLink = "<a qc = '-2'class='bpplink' style='color:red;' onclick = doQCAction(this); >Delete</a>"
		//var rLink = "<a qc = '-3' class='bpplink' style='color:#0072ff' onclick = doQCAction(this); >Recover</a>"
		//var bannerTextDel = activeParcel.ClientROWUID  && activeParcel.ClientROWUID < 0 ? 'This parcel is created and then deleted by the appraiser under real property '+parentPropLink + rLink :'This parcel is deleted by the appraiser under real property '+parentPropLink+ rLink;
		//var bannerTextNew= 'This parcel is newly created by the appraiser under real property '+parentPropLink+ dLink
		var newBPP = "<span class='bpp-parcel-info new-bpp-icon' style='display:inline-block;'></span>"
		var deletedBPP=" <span class='bpp-parcel-info recover-bpp-icon' style='display:inline-block;'></span>"
		$('.bpp-property').hide();
		if((activeParcel.CC_Deleted== true || activeParcel.CC_Deleted=='true') ){
			//$('.bppBannerDiv').html(bannerTextDel);  
			//$('.bppBannerDiv').css('color','#e84b4b');
			$('.recover-property').show();
			$('.delete-property').hide();
			$('.new-bpp-icon').hide();
			$('.bpp-parcel-info.recover-bpp-icon').remove();
			$('.parcel-head-values .medtext[fieldname="KeyValue1"]').append(deletedBPP);
			$('.parcel-head-values .medtext[fieldname="KeyValue1"]').removeClass('rightpad')
			//$('.bpp-parcel-info').
			//$('.bppBannerDiv').show();
			}
		else if((activeParcel.ClientROWUID  && activeParcel.ClientROWUID < 0 && clientSettings["EnablePersonalProperty"] == '1' ) ) {
			//$('.bppBannerDiv').html(bannerTextNew);
			//$('.bppBannerDiv').css('color','#00a000');
			//$('.bppBannerDiv').show();
			$('.recover-property').hide();
			$('.bpp-parcel-info.new-bpp-icon').remove();
			$('.parcel-head-values .medtext[fieldname="KeyValue1"]').append(newBPP);
			$('.recover-bpp-icon').hide();
			$('.delete-property').show();
			$('.parcel-head-values .medtext[fieldname="KeyValue1"]').removeClass('rightpad')
			}
		if(action){
			if(action == -2){
				//$('.bppBannerDiv').html(bannerTextDel);  
				//$('.bppBannerDiv').css('color','#e84b4b');
				//$('.bppBannerDiv').show();
				$('.recover-property').show();
				$('.delete-property').hide();
				$('.new-bpp-icon').hide();
				$('.bpp-parcel-info.recover-bpp-icon').remove();
				$('.parcel-head-values .medtext[fieldname="KeyValue1"]').append(deletedBPP);
				$('.parcel-head-values .medtext[fieldname="KeyValue1"]').removeClass('rightpad')
				activeParcel.CC_Deleted = true
				}
			if (action ==-3){
				//$('.bppBannerDiv').html(bannerTextNew);  
				//$('.bppBannerDiv').css('color','#00a000');
				//$('.bppBannerDiv').show();
				$('.recover-property').hide();
				$('.delete-property').show();
				$('.bpp-parcel-info.new-bpp-icon').remove();
				$('.parcel-head-values .medtext[fieldname="KeyValue1"]').append(newBPP);
				$('.recover-bpp-icon').hide();
				$('.parcel-head-values .medtext[fieldname="KeyValue1"]').removeClass('rightpad')
				activeParcel.CC_Deleted = false;
			}			
		}
		
}
function showBPPPopup(that){
	var pid= $(that).attr('value')
	var elem=activeParcel.ChildParcels.filter(function(e){return e.Id==pid});
	console.log(elem[0].Id);
	$('#bpp-child-popup-template').removeClass('hidden')
	$('#bpp-child-popup').removeClass('hidden')
	$('.bpp-child-popup').html($('#bpp-child-popup-template').html())
	$('.bpp-child-popup').fillTemplate(elem[0])
	var position = popupPosition(that, $('.bpp-child-popup'))
	$('.bpp-child-popup').css({ 'top': position[0] , 'left': position[1] });
    $('.bpp-child-popup').show();
}
function closeBPPPopup(){
$('.bpp-child-popup').hide();
 return false;
}
function popupPosition(source, popupBox) {
    var top;
    var left;
    var pos = [];
    var sTop = $(source).offset().top;
    var sLeft = $(source).offset().left;
    var sWidth = $(source).outerWidth();
    var sHght = $(source).outerHeight();
    var windowWidth = $(window).width();
    var windowHght = $(window).height();
    var contentHeight;
    var contentWidth;
    if (popupBox == undefined)
    {
        contentHeight = $('.bpp-child-popup').outerHeight();
        contentWidth = $('.bpp-child-popup').outerWidth();
    }
    else
    {
        contentHeight = $(popupBox).outerHeight();
        contentWidth = $(popupBox).outerWidth();
    }
   
    var scrollPosition = $(window).scrollTop();
    var distanceBottom = windowHght - (sTop + sHght);
    if (distanceBottom < (contentHeight)) {
        top = sTop - contentHeight ;
    }
    else {
        top = (sTop + sHght - (sHght / 2) );
    }

    var distanceRight = windowWidth - (sLeft + sWidth);
    if (distanceRight < contentWidth) {
        left = sLeft - (sWidth + contentWidth);
    }
    else {
        left = (sLeft + sWidth);
    }

    pos[0] = top;
    pos[1] = left;
    return pos;
}
function futureparcelData(data, callback){
	if(localStorage.getItem('showFutureData')== true || localStorage.getItem('showFutureData')== "true"){
		$(document).find( '.onoffswitch-checkbox' ).attr( 'checked', true );
	}
	showFutureData = $(document).find( '.onoffswitch-checkbox' ).is(':checked') ? true :false
    var futureYearEnabled = clientSettings["FutureYearEnabled"];
    if(futureYearEnabled == '1' || futureYearEnabled == 'true'){
    	var tempParcelData = data;
    	getAuxiliaryTableNames(function(tablelist){
    		for(var x in tablelist){
    			var sourceTable = tablelist[x];
    			var cat = fieldCategories.filter( function ( d ) { return d.SourceTable == sourceTable } )
    			if ((sourceTable !== undefined || sourceTable != null) && cat.length > 0 && cat[0].YearPartitionField) {
                    data[sourceTable] = tempParcelData[sourceTable].filter(function (d) { if (showFutureData) { return d.CC_YearStatus == 'F' } else { return d.CC_YearStatus == null || d.CC_YearStatus == 'A' || d.CC_YearStatus == ' '} })
    			}
    		}
    	callback(data);
    	});
    }
    else
    	callback(data);
}

function loadPRCactiveParcel(tablelist, parcel, callback) {
    if (tablelist.length == 0) {
        if (callback) callback(activeParcel);
        return;
    }
    var sourceTable = tablelist.pop().tableName;
    if (sourceTable === undefined) {
        if (callback) callback(activeParcel);
        return;
    }
    var childvalue = eval('parcel.' + sourceTable);
    var parenttable = parentChild.filter(function (s) { return s.ChildTable == sourceTable })[0].ParentTable;
    var Parentvalue = eval('parcel.' + parenttable);
    var _skcsettSketchConf = ( clientSettings && clientSettings['SketchConfig'] ) || ( sketchSettings && sketchSettings['SketchConfig'] ) ;
    if(Parentvalue && (_skcsettSketchConf == "ProValNew" || _skcsettSketchConf == "ProValNewFYL" ) && (parenttable == "sktsegment" || parenttable == "ccv_SktSegment")){ // handle proval filter field issues in sktsegment 
    	Parentvalue.sort(function(a, b){return a.CC_Deleted - b.CC_Deleted});
    }
    if (childvalue != undefined && childvalue.length == 0) {
        if (Parentvalue) Parentvalue.forEach(function(pVal){ pVal[sourceTable] = []; })
        loadPRCactiveParcel(tablelist, parcel, callback);
        return;
    }
    var filterfieldsArray = fieldCategories.filter(function (x) { return x.SourceTable == sourceTable }).map(function (p) { return p.FilterFields })// fieldCategories.filter(function (x) { return x.SourceTable == sourceTable });
    filterfieldsArray.forEach(function(filterfields){
    	if (filterfields && filterfields != "") { filterfields = filterfields.split(','); } else { filterfields = [] }
    	if (Parentvalue) {
       		Parentvalue.forEach(function (x) {
            	var filterCondition = '';
            	var iter_value = 0;
            	while (iter_value < filterfields.length) {
                	var childFilter, parentFilter; parentFilter = childFilter = filterfields[iter_value];
                	if (childFilter.indexOf('/') > -1) { parentFilter = childFilter.split('/')[0]; childFilter = childFilter.split('/')[1]; }
                	var values = eval("x." + parentFilter);
                	filterCondition += 'd.' + childFilter + ' == \'' + values + '\'';
                	iter_value += 1;
                	if (iter_value < filterfields.length) {
                    	filterCondition += ' && ';
                	}
            	}
            	if (filterCondition == '') filterCondition = '1==1'
            	var parentROWUID = x.ROWUID;

            	var fiteredSource = eval('childvalue.filter(function (d, i) { return ((!d.ParentROWUID &&(' + filterCondition + ')) || (d.ParentROWUID && d.ParentROWUID == ' + parentROWUID + '))})');
            	fiteredSource.forEach( function ( cc, findex )
            	{
					if(cc.Original) cc.Original["ParentROWUID"] = parentROWUID;
               		cc["parentRecord"] = x
                	cc["ParentROWUID"] = parentROWUID;
                	cc["CC_FIndex"] = findex;
            	})
            	x[sourceTable] = fiteredSource;
        	});
    	}
    });
    //eval('delete parcel[sourceTable]');
    loadPRCactiveParcel(tablelist, parcel, callback);
}
function LoadTableHierarchical(TableName) {
    var t = 1;
    var ab = function (child) { var dd = parentChild.filter(function (f) { return f.ChildTable == child }); if (dd.length == 0) { return t } if (dd[0].ParentTable.toLowerCase() == 'parcel') { return t } else { t = t + 1; ab(dd[0].ParentTable) } };
    var dd = ab(TableName); return t;
}
function LoadAuxDataPRC(activeParcel, tableListGlobal, callback) {
    TableHierarchical = [];
    tableListGlobal.forEach(function (name) {
        //if (activeParcelData) {
        //    var data = activeParcelData[name];
        //    if (data) {
        //        data.forEach(function (ab) {
        //            Object.keys(ab).forEach(function (key, n) {
        //                var t = ab[key];
        //                ab[key] = {}; ab[key].Value = t; ab[key].Original = t;
        //            });
        //        });
        //    }
        //}
        var no = LoadTableHierarchical(name);
        if (no > 1) {
            TableHierarchical.push({ 'tableName': name, 'no': no });
        }
    });
    TableHierarchical.sort(function (a, b) {
        return a.no > b.no;
    })
    if (callback) callback(activeParcel);
}

function loadAuxData(parcel, tableList, callback) {
    var parcelId = parcel.Id;
    if (tableList.length == 0) {
        if (callback) callback(parcel);
        return;
    }
    var sourceTable = tableList.pop();
    if (sourceTable === undefined) {
        if (callback) callback(parcel);
        return;
    }
    var data = parcel[sourceTable];
    var originalData = parcel.Original[sourceTable];
    if (data) {
        var index, element, object = [];
        for (d in data) {
            data[d].Original = {};
            object = Object.keys(data[d]);
            for (var o in object) {
                index = object[o];
                element = data[d][index];
                if (typeof (element) != 'object' || element == null)
                    data[d].Original[index] = originalData[d][index];
            }
        }
    }
    parcel[sourceTable] = data;
    loadAuxData(parcel, tableList, callback);
}

function calculateClassRecommendation(catID, st, idx, auxform) {
    var category = getCategory(catID); //fieldCategories[parseInt(catID) - 1];
    var classCalculatorResultField = getDataField(category.ClassCalculatorParameters.split(',')[0], category.SourceTable);
    var flag = 0;
    var sum = 0;
    
    $('.classCalcAttributes').each(function (index, item) {
    	var _dis= $(this).parent().parent().css('display');
        if ($(item).val() == '' && _dis != 'none')
            flag = 1;
    });
    
    if (flag == 1){
        alert("All fields are required for class calculation");
        $(".calculatedClassValue").html('');
    	$(".updateClass").hide();
    }
    else {
        $('.updateClass').show();
        $('.classCalcAttributes').each(function () {
            var fId = $(this).parent().attr('fieldid');
            var fName = datafields[fId].Name;
            var sourceTable = datafields[fId].SourceTable;
            var field = getDataField(fName, sourceTable);
            var value = $(this).val();
            //value = (value && value.length) ? value.replace('&gt;', '>') : ""
            //value = (value && value.length) ? value.replace('&lt;', '<') : ""
            value = (value && value.length) ? value.replace('>', '&gt;') : ""
            value = (value && value.length) ? value.replace('<', '&lt;') : ""
            var val = evalLookup(fName, value, null, field, true)
            sum += isNaN( Number(val) )? 0: Number(val);
        });
        if (clientSettings?.ClassCalcDisplayShortDescAndOrLongDesc) {
            let ClassCalcDisplayShortDescAndOrLongDesc = clientSettings.ClassCalcDisplayShortDescAndOrLongDesc;
             classCalculationNewValue = (classCalculatorResultField.InputType == 5) ? evalLookup(classCalculatorResultField.Name, sum, ClassCalcDisplayShortDescAndOrLongDesc, classCalculatorResultField) : sum;
             calculatedClassDisplayValue = classCalculatorResultField.InputType == 5 ? evalLookup(classCalculatorResultField.Name, sum, null, classCalculatorResultField) : sum;
            $('.classRec').html('<b>Calculated Class:<span class="calculatedClassValue"style="color:red;">&nbsp' + classCalculationNewValue + '<span/></b>');
            $('.classRec').show();
        }
        else {
             calculatedClassDisplayValue = classCalculatorResultField.InputType == 5 ? evalLookup(classCalculatorResultField.Name, sum, null, classCalculatorResultField) : sum;
            $('.classRec').html('<b>Calculated Class:<span class="calculatedClassValue"style="color:red;">&nbsp' + calculatedClassDisplayValue + '<span/></b>');
            $('.classRec').show();
        }
    }
    
    $('.updateClass').unbind('click');
    $('.updateClass').bind('click', function () {
        alertConfirm(catID, st, idx, calculatedClassDisplayValue, sum);
    });
}

function alertConfirm(cId, s, i, su, cv) {
    var cat = getCategory(cId);
    childSourceTable = cat.ClassCalculatorParameters.split(',')[1].split('.')[0];
    classCalculatorResultField = getDataField(cat.ClassCalculatorParameters.split(',')[0], cat.SourceTable);
    if (confirm("Are you sure you want to update the Calculated Class ?")) {
        $('.updateClass').hide();
        $('#classCalculatorFrame').hide();
        $('.classRec').html('');
        var fieldId = classCalculatorResultField.Id;
        if (activeParcel[s][i][classCalculatorResultField.Name] != cv.toString() || tempactiveParcelEdits.length ) {
        	let cvValue = classCalculatorResultField.InputType == 5? cv.toString(): su.toString();
            activeParcel[s][i][classCalculatorResultField.Name] = cvValue;
            let cat = tempsRec[0]["cat"];
            let auxid = tempsRec[0]["auxrowuid"];
            if (cat && auxid) {
                var sRec = activeParcel[cat].filter(function (x) { return x.ROWUID == auxid; })[0];
                Object.keys(tempsRec[1]).forEach(function (key) {
                    sRec[key] = tempsRec[1][key];
                });
                tempactiveParcelEdits.forEach(function (x, i) {
                    activeParcelEdits.push(tempactiveParcelEdits[i]);
                });
            }
            tempactiveParcelEdits.length = 0;
            tempsRec.length = 0;
            tempsRec = [{}, {}];
            $(".currClass").html("<b>" + su.toString() + "</b>");
            if (classCalculatorResultField.InputType == 5) {
                //$('.value[fieldid="' + fieldId + '"] .trackchanges').find('option').remove().end().append('<option value="' + su + '">' + su + '</option>');
                $('.value[fieldid="' + fieldId + '"] .trackchanges').val(cvValue);
                //$('.value[fieldid="' + fieldId + '"] .trackchanges').html(su.toString());
            } else {
                $('.value[fieldid="' + fieldId + '"] .trackchanges').val(su.toString());
            }
            $('.value[fieldid="' + fieldId + '"] .trackchanges').trigger("blur");
        }
        if (clientSettings?.ClassCalcDisplayShortDescAndOrLongDesc) {
            var parameterField = (cat.ClassCalculatorParameters.split(',')[1] || '').split('.');
            if (s == parameterField[0]) {
                var classCalculatorField = getDataField(parameterField[1], cat.SourceTable);
                if (classCalculatorField != null) {
                    var cfieldId = classCalculatorField.Id;
                    if (classCalculationNewValue != calculatedClassDisplayValue) {
                        if (classCalculatorField.InputType == 5) {
                            $('.value[fieldid="' + cfieldId + '"] .trackchanges').attr('sel', su.toString());
                            $('.value[fieldid="' + cfieldId + '"] .trackchanges').trigger("blur");
                        }
                    }
                    else
                    {
                        
                        let warningMessage = document.getElementById('dynamicWarningMessage');

                        
                        if (!warningMessage) {
                            warningMessage = document.createElement('div');
                            warningMessage.id = 'dynamicWarningMessage';
                            warningMessage.style.color = 'black';
                            warningMessage.style.position = 'fixed';
                            warningMessage.style.top = '50%';
                            warningMessage.style.left = '50%';
                            warningMessage.style.transform = 'translate(-50%, -50%)';
                            warningMessage.style.backgroundColor = 'white';
                            warningMessage.style.padding = '20px';
                            warningMessage.style.border = '1px solid black';
                            warningMessage.style.borderRadius = '5px';
                            warningMessage.style.zIndex = '1000';
                            warningMessage.style.textAlign = 'center';
                            warningMessage.style.boxShadow = '0px 0px 10px rgba(0, 0, 0, 0.1)';

                           
                            const messageText = document.createElement('p');
                            messageText.innerText = "The Calculated Class does not have a matching \"" +  classCalculatorField.Name  + "\" value! Please manually change the \"" +  classCalculatorField.Name  + "\" value and contact Support to resolve this issue.";
                            warningMessage.appendChild(messageText);

                            
                            const cancelButton = document.createElement('button');
                            cancelButton.innerText = 'Cancel';
                            cancelButton.style.marginTop = '10px';
                            cancelButton.style.padding = '5px 10px';
                            cancelButton.style.border = 'none';
                            cancelButton.style.backgroundColor = 'black';
                            cancelButton.style.color = 'white';
                            cancelButton.style.borderRadius = '3px';
                            cancelButton.style.cursor = 'pointer';
                            cancelButton.onclick = () => {
                                warningMessage.remove();
                            };
                            warningMessage.appendChild(cancelButton);

                            document.body.appendChild(warningMessage);
                        }

                       // alert("Warning: Class points and grade lookups do not match. So grade is not changed.");
                    }
                }
            }
        }
        // $('.classCalcAttributes').each(function (index, item) {
        //     var fId = $($(item).parent()).attr('fieldid');
        //     var sourceTable = datafields[fId].SourceTable;
        //     activeParcelEdits.push({ AuxROWUID: activeParcel[sourceTable][i].ROWUID, ChangeId: null, FieldId: fId, ParcelId: activeParcel.Id, QCValue: $(item).val().toString(), action: "edit", parentROWUID: activeParcel[sourceTable][i].ParentROWUID })
        // });
        //activeParcelEdits.push({ AuxROWUID: activeParcel[s][i].ROWUID, ChangeId: 0, FieldId: classCalculatorResultField.Id, ParcelId: activeParcel.Id, QCValue: classCalculatorResultField.value.toString(), action: "edit", parentROWUID: activeParcel[s][i].ParentROWUID })
        var showAuxParams = getParamOfAuxData(cId)[0];
        showAuxData($('.category-page[categoryid="' + cId + '"]'), i, showAuxParams.filter_field, showAuxParams.filter_value, showAuxParams.parentROWUID);
        $(".calculatedClassValue").html('');
        $('.heightdiv').show();
    }
}

function cancelClassCalc(catid, i) {
    $('#classCalculatorFrame').hide();
    $('.category-page[categoryid="' + catid + '"] Div[class="sublevelDiv"]').show();
    $('.data-navigator').show();
    $('.heightdiv').show();
    $('.classRec').html('');
    tempactiveParcelEdits.length = 0;
    tempsRec.length = 0;
    tempsRec = [{},{}];
    var showAuxParams = getParamOfAuxData(catid)[0];
    showAuxData($('.category-page[categoryid="' + catid + '"]'), i, showAuxParams.filter_field, showAuxParams.filter_value, showAuxParams.parentROWUID);
}

function classValidation(r) {
    if (r.length > 0) {
        if (confirm("The current Class is different than the Calculated Class. Press OK to continue, or CANCEL to return and edit the data.")) {
            var fieldId, val;
            //classCalculatorValidationResult.forEach(function (recItem) {
            //    activeParcelEdits.push({ AuxROWUID: recItem.ROWUID, ChangeId: 0, FieldId: recItem.FieldID, ParcelId: recItem.ParcelId, QCValue: recItem.value.toString(), action: "edit", parentROWUID: recItem.ParentRowUid.toString() })
            //})
        }
    }
    classCalculatorValidationResult = [];
}

function classCalculatorValidation(parcel) {
    var calCategories = fieldCategories.filter(function (ab) { return ab.ClassCalculatorParameters != null && ab.ClassCalculatorParameters != '' });
    var element, table, field, valField, resultField, parentRowUid;
    calCategories.forEach(function (cat) {
        element = cat.ClassCalculatorParameters.includes(',') ? cat.ClassCalculatorParameters.split(',')[1] : '';
        table = element.split('.')[0]
        field = element.split('.')[1];
        valField = getDataField(field, table);
        resultField = getDataField(cat.ClassCalculatorParameters.split(',')[0], cat.SourceTable);
        parcel[cat.SourceTable].forEach(function (pt) {
            parentRowUid = pt.ROWUID;
            if(pt[table]){
            pt[table].forEach(function (ct) {
                if (ct[field] === undefined || !pt[resultField.Name]) return;
                if (!(ct.CC_Deleted) && (ct[field] != pt[resultField.Name])) {
                    classCalculatorValidationResult.push({ ParcelId: parcel.Id, Category: cat.Name, ROWUID: ct.ROWUID, FieldName: valField.Name, FieldID: valField.Id, value: pt[resultField.Name], Sourcetable: table, ParentRowUid: parentRowUid });
                }
            })
            }
        })
    })
    return classCalculatorValidationResult;
} 
/*
function showAuxData(block, index, filter_name, filterValue, parentROWUID, callback,doNotCalculateExpression) {

    index = parseInt(index);
    if ($(block).attr('parceldata') == '')  //bypass parceldata since it has no auxilaries
        return false;
    if ($(block).attr('auxdata') == null) 
        return false;
    $( '.heightdiv', block ).show();

    handleError();
    hide('.field-audit-qc');
    $('.qc a').removeAttr("clicked");
    var sourceTable = $(block).attr('auxdata');
    var sourceData = activeParcel[sourceTable];
    var categoryID = $(block).attr('categoryid');
    var auxForm = $('.category-page[categoryid="' + categoryID + '"]');
    var cat = getCategory(categoryID);
    if ( cat && cat.ClassCalculatorParameters && cat.ClassCalculatorParameters != '' && activeParcel[sourceTable] && activeParcel[sourceTable][index] ){
        var recDeleted = activeParcel[sourceTable][index].CC_Deleted;
        var calculatorCondition = cat.ClassCalculatorParameters.split(',').length == 3 ? cat.ClassCalculatorParameters.split(',')[2] : null;
        var ShowCalculator;
        if (calculatorCondition)
            ShowCalculator = activeParcel.EvalValue(sourceTable + '[' + index + ']', calculatorCondition)
        if ((ShowCalculator || !calculatorCondition) && !recDeleted)
            $('.classCalc', auxForm).show();
        else
            $('.classCalc', auxForm).hide();
        $('.classCalc').unbind('click');
        $('.classCalc').bind('click', function () {
       		if (source && (source.length > 0)) {
           		$('#classCalculatorFrame').find('.check,.passed').hide();
            	var fst = source[index];
            	var classCalcChanges = activeParcel.ParcelChanges.filter(function (d, i) { return datafields[d.FieldId].IsClassCalculatorAttribute == true && fst.ROWUID == d.AuxROWUID });
             	if (classCalcChanges.length > 0) {
            		for (x in classCalcChanges) {
            			var ch = classCalcChanges[x];
                		var qc = $('.qc[fieldid="' + ch.FieldId + '"]');
                		$('a', qc).hide();
                		$(ch.QCChecked ? '.passed' : '.check', qc).show();
                		$(ch.QCChecked ? '.passed' : '.check', qc).attr('changeid', ch.Id); 
            		}
            		$( '.classCalcAttributes' ).siblings( 'span.qc' ).attr( 'auxrowuid', fst.ROWUID );
            	}
   			}
          
            var resultFieldname = (cat.ClassCalculatorParameters && cat.ClassCalculatorParameters.length > 0) ? cat.ClassCalculatorParameters.split(',')[0] : null;
         	var currentClass = activeParcel[sourceTable][index][resultFieldname] == null ? '' : activeParcel[sourceTable][index][resultFieldname];
            $('.currClass').html('<b>' + currentClass + '</b>');
            $('.parcel-field-values', auxForm).hide();
            $('.category-page[categoryid="' + categoryID + '"] Div[class="sublevelDiv"]').hide();
            $('.data-navigator').hide();
            $('.heightdiv').hide();
            $('#classCalculatorFrame').show();
            $('.classCalcAttributes').each(function (i, item) {
                var fId = $(this).parent().attr('fieldid');
                var dField = datafields[fId]
                var fName = dField.Name;
                var sourceTable = dField.SourceTable;
                var infoContent = dField.InfoContent
                if (infoContent && infoContent.toString().trim() != '') {
            		var label = $(this).parent().siblings('.label');
                	$(label).find('.infoButton').show();
                	var w = $('span', $(label)).eq(0).width();
                	if (w > 155) $(label).width(w + 12);
            	}
                var value = activeParcel[sourceTable][index][fName];
                value = (value && value.length) ? value.replace('&gt;', '>') : ""
                value = (value && value.length) ? value.replace('&lt;', '<') : ""
                $(item).val(value)
            });
            $('.classCalcAttributes').unbind('change');
  		    $('.classCalcAttributes').change(function () {
	  		   // $(this).parent().addClass('unsaved');
	            var fId = $($(this).parent()).attr('fieldid');
	            var field_name = datafields[fId].Name;
	            var sourceTable = datafields[fId].SourceTable;
	         	if(index != -1) {
	            	activeParcelEdits.push({ AuxROWUID: activeParcel[sourceTable][index].ROWUID, ChangeId: null, FieldId: parseInt(fId), ParcelId: activeParcel.Id, QCValue: $(this).val().toString(), action: "edit", parentROWUID: activeParcel[sourceTable][index].ParentROWUID })
	            	activeParcel[sourceTable][index][field_name] = $(this).val().toString();
	            }
	        });
        });
        $('.btnCalculate').unbind('click');
        $('.btnCalculate').bind('click', function () {
            calculateClassRecommendation(categoryID, sourceTable, index, auxForm);
        });
        $('.btnCancel').unbind('click');
        $('.btnCancel').bind('click', function () {
            $('.updateClass').hide();
            cancelClassCalc(categoryID, index);
        });
    }
    else
        $('.classCalc', auxForm).hide();
    $('.parcel-field-values .input[doNotAllowEditExistingRecords]', block).removeAttr("disabled");
   // $('.customddlspan', block).show();
    var source;
    var count;
    var filteredIndexes;
    var curntCat = getCategory(categoryID);
    if ((filter_name != undefined) && (filterValue != undefined) && (filterValue.length > 0) && (filter_name.length > 0)) {
        source = showAuxDataWithFilter(sourceTable, filter_name, filterValue, parentROWUID,categoryID);
        filteredIndexes = getFilteredIndexes(sourceTable, filter_name, filterValue, parentROWUID,categoryID);
        fIndx[sourceTable] = filteredIndexes;
    }
    else {
        source = eval('activeParcel.' + sourceTable);
        if (!source)
            filteredIndexes = [];
        else if (fIndx[sourceTable] && curntCat.ParentCategoryId)
            filteredIndexes = fIndx[sourceTable];
        else
            filteredIndexes = eval('activeParcel.' + sourceTable + '.map(function(d,i){return i;})');
    }
    getGridViewData(categoryID, filteredIndexes);
    var cat = getCategory(categoryID);
    if (cat.MultiGridStub == 'true'||cat.MultiGridStub == true) {
        loadMultiGrid(block, index, filteredIndexes);
    }
    if (source != undefined) {
        count = source.length;
        if (fIndx[sourceTable] && curntCat.ParentCategoryId)
            count = fIndx[sourceTable].length;
    }
    else count = 0;
    var recoveryDivControl;
    var first;
    $( '.aux-index', block ).attr( 'findex', filteredIndexes[index] );
    $( auxForm ).attr( 'findex', filteredIndexes[index] );
    var parentSpec = getParentSpec(categoryID);
    $('.childCategoryTitle').nextAll('span').remove();
    HideCategoryByExpression( categoryID, sourceTable, filteredIndexes[index], parentSpec ? parentSpec.source + '[' + parentSpec.index + ']' : null )
    var childCategories = getChildCategories(categoryID);
    var childcat = [];
    $.each(childCategories, function (a, b) {
        if ($(".childCategoryTitle[category='" + childCategories[a] + "']").css('display') != 'none') {
            childcat.push(childCategories[a]);
        }
    });
    for (x in childcat) {
        if (x == (childcat.length - 1)) {
        }
        else {
            $(".childCategoryTitle[category='" + childcat[x] + "']").after('<span>,</span>');
        }
    }
     if ($('.aux-grid-view[catid="' + categoryID + '"]').css("display") == 'block') {
        $('.data-navigator', block).hide();
        $('.heightdiv', block).hide();
        $('.sublevelDiv', block).hide();
        $('.parcel-field-values', block).hide();
        $('.auxrecover', block).hide();
        $(block).attr('state', 'grid');
    } else {
        $('.data-navigator', block).show();
        if (count > 0) {
            $('.heightdiv', block).show();
            $('.sublevelDiv', block).show();
            $('.parcel-field-values', block).show();
            $(block).attr('state', 'form');
            $('.parcel-data-view').scrollTop(0);
        }
    }
    if (source && (source.length > 0)) {
        first = source[index];
        //for (var x in first) {
        //    var field = first[x];
        //    $('.parcel-field-values .value[fieldname="' + x + '"] .input', block).attr('auxrowuid', first.ROWUID);
        //    $('.parcel-field-values .value[fieldname="' + x + '"] .input', block).siblings('span.qc').attr('auxrowuid', first.ROWUID);
        //    setFieldValue(field, x, block);
        //}
        // No need to loop through all key fields so changed above commented statements  
        $('.sublevelDiv').attr('ParentROWUID', parentROWUID);
        $('.parcel-field-values .value', block).each(function (a, f) {
            var t = $(f).attr('fieldname');
            var field = first[t];
            $('.input', f).attr('auxrowuid', first.ROWUID);
            $('.input', f).siblings('span.qc').attr('auxrowuid', first.ROWUID);
            setFieldValue(field, t, block,null,null,null,doNotCalculateExpression);
        });
        var recoverSet = false;
        var deletePCI = [], cId = 0;
        var filteredCh = activeParcel.ParcelChanges.filter(function (d, i) { return d.AuxROWUID == first.ROWUID && d.SourceTable == sourceTable; });
        $('span.qc a', block).hide();
        if (first.CC_Deleted == true || first.CC_Deleted == "true") {
            deletePCI = activeParcel.ParcelChanges.filter(function (d, i) { return d.AuxROWUID == first.ROWUID && d.SourceTable == sourceTable && d.Action == 'delete'; });
            recoveryDivControl = $('.category-page[categoryid="' + categoryID + '"] .auxrecover');
            $(recoveryDivControl).show();
            $('.del-item', block).hide();
            cId = deletePCI.length > 0 ? deletePCI[0].Id : 0;
            $(recoveryDivControl).attr({ 'AuxROWUID': first.ROWUID, 'SourceTable': sourceTable, 'ChangeId': cId });
            $('.category-page[categoryid="' + categoryID + '"] .childDiv').hide();
            $('.customddlspan', block).hide();
            $('.category-page[categoryid="' + categoryID + '"] span.qc a').hide();
            //$('.category-page[categoryid="' + categoryID + '"] .input').attr({ 'recovery': 'recovery', 'disabled': 'disabled' });
            recoverSet = true;
        }
        if (filteredCh.length > 0) {
            for (x in filteredCh) {
                var ch = filteredCh[x];
                var qc = $('.qc[fieldid="' + ch.FieldId + '"]');
                $('a', qc).hide();
                $(ch.QCChecked ? '.passed' : '.check', qc).show();
                $(ch.QCChecked ? '.passed' : '.check', qc).attr('changeid', ch.Id);
                //if (ch.Action == 'delete' || first.CC_Deleted == true || first.CC_Deleted == "true") {
                //    recoveryDivControl = $('.category-page[categoryid="' + categoryID + '"] .auxrecover');
                //    $(recoveryDivControl).show();
                //    $('.del-item', block).hide();
                //    $(recoveryDivControl).attr({ 'AuxROWUID': ch.AuxROWUID, 'SourceTable': ch.SourceTable, 'ChangeId': ch.Id });
                //    $('.category-page[categoryid="' + categoryID + '"] .childDiv').hide();
                //    $('.customddlspan', block).hide();
                //    $('.category-page[categoryid="' + categoryID + '"] span.qc a').hide();
                //    $('.category-page[categoryid="' + categoryID + '"] .input').attr({ 'recovery': 'recovery', 'disabled': 'disabled' });
                //}
                //else {
                if (!recoverSet) {
                    $('.category-page[categoryid="' + categoryID + '"] .auxrecover').hide();
                    $('.category-page[categoryid="' + categoryID + '"] .auxrecover').removeAttr('AuxROWUID SourceTable ChangeId');
                    var inputControl = $('.category-page[categoryid="' + categoryID + '"] .input[recovery]');
                    $(inputControl).removeAttr('disabled recovery');
                    if (getDescendantCategoryIds(categoryID).length > 0) {
                        $('.category-page[categoryid="' + categoryID + '"] .childDiv').show();
                    }
                }
                setFieldValue(ch.NewValue, ch.FieldId, block,null,null,null,doNotCalculateExpression);
            }
        }
        else {
            if (!recoverSet) {
                $('.category-page[categoryid="' + categoryID + '"] .auxrecover').hide();
                var inputControl = $('.category-page[categoryid="' + categoryID + '"] .input[recovery]');
                $(inputControl).removeAttr('disabled recovery');
                $('.category-page[categoryid="' + categoryID + '"] .auxrecover').removeAttr('AuxROWUID SourceTable ChangeId');
                if (getDescendantCategoryIds(categoryID).length > 0 && getChildCategories(categoryID).length > 0) {
                    $('.category-page[categoryid="' + categoryID + '"] .childDiv').show();
                }
            }
        }
        $('.parcel-field-values td.value', block).removeClass('unsaved');
        for (x in activeParcelEdits) {
            var ed = activeParcelEdits[x];
            if (ed.AuxROWUID && ed.AuxROWUID.toString() == $('.parcel-field-values .value[fieldid="' + ed.FieldId + '"] .input', block).attr('auxrowuid')) {
                $('.parcel-field-values td.value[fieldid="' + ed.FieldId + '"]', block).addClass('unsaved');
                setFieldValue(ed.QCValue, ed.FieldId, block,null,null,null,doNotCalculateExpression);
            }
        }
        if ($('.aux-grid-view[catid="' + categoryID + '"]').css("display") == 'block' && $('.grid-view[categoryid="' + categoryID + '"]').css("display") == 'block')
        $('.auxrecover', block).hide();
        if ($('.aux-grid-view[catid="' + categoryID + '"]').css("display") != 'block')
        $('.parcel-field-values', block).show();
        $('.category-page[categoryid="' + categoryID + '"] .aux-ROWUID').text('AUXROWUID : ' + first.ROWUID);
    } else {
        index = -1;
        $('.category-page[categoryid="' + categoryID + '"] .auxrecover').hide();
        $('.parcel-field-values', block).hide();
        $('.parcel-field-values .input').removeAttr('i');
        $('.childDiv',auxForm).hide();
    }

    if (index < 1) {
        $('.move-first', block).attr('disabled', 'disabled');
        $('.move-prev', block).attr('disabled', 'disabled');
    } else {
        $('.move-first', block).removeAttr('disabled');
        $('.move-prev', block).removeAttr('disabled');
    }

    if (index == count - 1) {
        $('.move-last', block).attr('disabled', 'disabled');
        $('.move-next', block).attr('disabled', 'disabled');
    } else {
        $('.move-last', block).removeAttr('disabled');
        $('.move-next', block).removeAttr('disabled');
    }
    $('.new-item', block).show();
    if ($('button.new-item', block).attr('IsReadOnly') == "True" || $('button.new-item', block).attr('AllowAddDelete') == "False") {
        $('.new-item', block).hide();
    }
    if (count == 0) {
        $('.del-item', block).hide();
    } else {
        $('.del-item', block).unbind();
        if (!recoveryDivControl)
            $('.del-item', block).show();
        $('.del-item', block).bind('click', function (e) {
            e.preventDefault();
            //vineesh
            //                        $qc('deleteauxrecord', { ParcelId: activeParcel.Id, SourceTable: sourceTable, ROWUID: first.ROWUID }, function (data) {
            //                            if (data.status == "OK") {
            //                                getParcelDataUpdates();
            //                            }
            //                        });
            //vineesh

            var delRecord = { ParcelId: activeParcel.Id, SourceTable: sourceTable, ROWUID: first.ROWUID, action: "delete" };
            deleteCurrentRecord(delRecord, first, source, categoryID);
        });

        if ($('.del-item', block).attr('IsReadOnly') == "True" || $('.del-item', block).attr('AllowAddDelete') == "False") {
            $('.del-item', block).hide();
        }
        if ($('.del-item', block).attr('DoNotAllowDeleteFirstRecord') == "True" && $('.del-item', block).attr('AllowAddDelete') == "True") {
            if (index == 0 || (index > 0 && source[index].CC_Deleted == true))
                $('.del-item', block).hide();
            else
                $('.del-item', block).show();
        }
        var isExp = true
        if ( cat.EnableDeleteExpression && cat.EnableDeleteExpression != '' )
            isExp = activeParcel.EvalValue( sourceTable + '[' + filteredIndexes[index] + ']', cat.EnableDeleteExpression, null, parentSpec ? parentSpec.source + '[' + parentSpec.index + ']' : null );

        var doNotAllowEditExistingRecords = fieldCategories.filter( function ( x ) { return x.SourceTable == sourceTable } ).map( function ( p ) { return p.DoNotAllowEditExistingRecords } )[0]
        if ( ( doNotAllowEditExistingRecords == 'true' || doNotAllowEditExistingRecords == true ) && isExistingAuxDetail( activeParcel, sourceTable, index ) )
        {  //to check if it is the existing record
            $( '.parcel-field-values .input', block ).attr( { 'doNotAllowEditExistingRecords': 'doNotAllowEditExistingRecords', "disabled": "disabled" } );
            $( '.parcel-field-values input:radio', block ).attr( 'disabled', 'disabled' );
            $( '.del-item', block ).attr( 'disabled', 'disabled' );
            $( '.customddlspan', block ).hide();
        }
        else if ( !isExp )
            $( '.del-item', block ).attr( 'disabled', 'disabled' );
        else
            $( '.del-item', block ).removeAttr( 'disabled', 'disabled' );
    }


    $('.aux-records', block).html(count ? count : '0');
    $('.aux-index', block).html(index + 1);
    $('.aux-grid-switch', block).attr('index', index);
    highlightwithFilter(categoryID);
    //setTimeout('highlightwithFilter(' + categoryID + ');', 500);
    //}
    //    catch (e) {
    //        console.log("AuxDataError: " + e);
    ////    }
    //$('.parcel-field-values .value', block).each(function (a, f) {
    //    var x = $(f).parent().parent().parent().find('tr')[a];
    //    if ($(f).attr('fieldid') || $(f).attr('fieldid') != '') {
    //        var dfield = datafields[$(f).attr('fieldid')];
    //        if (dfield && (dfield.IsClassCalculatorAttribute == true || dfield.IsClassCalculatorAttribute == "true"))
    //            $(x).attr('style', 'display: none');
    //        else
    //            $(x).show();
    //    }
    //});
    checkRequiredIfEditsFieldsInForm(categoryID, index);
    refreshAuxCount(categoryID, block)
    initRadioButtonChangeTracker();
    if (callback) callback(filteredIndexes[index]);
}
*/
function showAuxData(block, index, filter_name, filterValue, parentROWUID, callback,doNotCalculateExpression, firstTimeQCLoad, CalcFieldsOnly) {

    index = parseInt(index);
    if ($(block).attr('parceldata') == '')  //bypass parceldata since it has no auxilaries
        return false;
    if ($(block).attr('auxdata') == null) 
        return false;
    $( '.heightdiv', block ).show();

    handleError();
    hide('.field-audit-qc');
    $('.qc a').removeAttr("clicked");
    var sourceTable = $(block).attr('auxdata');
    var sourceData = activeParcel[sourceTable];
    var categoryID = $(block).attr('categoryid');
    var auxForm = $('.category-page[categoryid="' + categoryID + '"]');
    var cat = getCategory(categoryID);
    if ( cat && cat.ClassCalculatorParameters && cat.ClassCalculatorParameters != '' && activeParcel[sourceTable] && activeParcel[sourceTable][index] ){
        var recDeleted = activeParcel[sourceTable][index].CC_Deleted;
        var calculatorCondition = cat.ClassCalculatorParameters.split(',').length == 3 ? cat.ClassCalculatorParameters.split(',')[2] : null;
        var ShowCalculator;
        if (calculatorCondition)
            ShowCalculator = activeParcel.EvalValue(sourceTable + '[' + index + ']', calculatorCondition)
        if ((ShowCalculator || !calculatorCondition) && !recDeleted)
            $('.classCalc', auxForm).show();
        else
            $('.classCalc', auxForm).hide();
        $('.classCalc').unbind('click');
        $('.classCalc').bind('click', function () {
       		if (source && (source.length > 0)) {
           		$('#classCalculatorFrame').find('.check,.passed').hide();
           		$('.classRec').hide();
           		tempactiveParcelEdits.length = 0;
    			tempsRec.length = 0;
    			tempsRec = [{},{}];
            	var fst = source[index];
            	var classCalcChanges = activeParcel.ParcelChanges.filter(function (d, i) { return datafields[d.FieldId]?.IsClassCalculatorAttribute == true && fst.ROWUID == d.AuxROWUID });
             	if (classCalcChanges.length > 0) {
            		for (x in classCalcChanges) {
            			var ch = classCalcChanges[x];
                		var qc = $('.qc[fieldid="' + ch.FieldId + '"]');
                		$('a', qc).hide();
                		$((activeParcel.QC && ch.QCChecked) ? '.passed' : '.check', qc).show();
                		$((activeParcel.QC && ch.QCChecked) ? '.passed' : '.check', qc).attr('changeid', ch.Id); 
            		}
            		$( '.classCalcAttributes' ).siblings( 'span.qc' ).attr( 'auxrowuid', fst.ROWUID );
            	}
   			}
          
            var resultFieldname = (cat.ClassCalculatorParameters && cat.ClassCalculatorParameters.length > 0) ? cat.ClassCalculatorParameters.split(',')[0] : null;
         	var currentClass = activeParcel[sourceTable][index][resultFieldname] == null ? '' : activeParcel[sourceTable][index][resultFieldname];
            let classDisplayValue = currentClass;
            let class_field = getDataField(resultFieldname, sourceTable);
            
            if (class_field && class_field.InputType == 5) {
                if (clientSettings?.ClassCalcDisplayShortDescAndOrLongDesc) {
                    let ClassCalcDisplayShortDescAndOrLongDesc = clientSettings.ClassCalcDisplayShortDescAndOrLongDesc;
                    let cv = evalLookup(resultFieldname, currentClass, ClassCalcDisplayShortDescAndOrLongDesc, class_field)
                    classDisplayValue = cv ? cv : currentClass;
                }
                else {
                    let cv = evalLookup(resultFieldname, currentClass, null, class_field)
                    classDisplayValue = cv ? cv : currentClass;
                }
            }
            
            $('.currClass').html('<b>' + classDisplayValue + '</b>');
            $('.parcel-field-values', auxForm).hide();
            $('.category-page[categoryid="' + categoryID + '"] Div[class="sublevelDiv"]').hide();
            $('.data-navigator').hide();
            $('.support__catHead', auxForm).hide();
            $('.heightdiv').hide();
            $('#classCalculatorFrame').show();
            
            $('.classCalcAttributes').each(function (i, item) {
                var fId = $(this).parent().attr('fieldid');
                var dField = datafields[fId];
                if( dField.DoNotShow || dField.DoNotShow == 'true' || ( __DTR &&  ( dField.DoNotShowOnDTR || dField.DoNotShowOnDTR == 'true') ) ) 
                	$(this).parent().parent().css('display', 'none');
                else if (parseInt(dField.InputType) == 5) {
                    $(item)[0]?.getLookup.setData(lookup[dField.LookupTable]);
                }
				$(this).parent().removeClass('unsaved');
                var fName = dField.Name;
                var sourceTable = dField.SourceTable;
                var infoContent = dField.InfoContent
                if (infoContent && infoContent.toString().trim() != '') {
            		var label = $(this).parent().siblings('.label');
                	$(label).find('.infoButton').show();
                	var w = $('span', $(label)).eq(0).width();
                	if (w > 155) $(label).width(w + 12);
            	}
                var value = activeParcel[sourceTable][index][fName];
                var rowId = activeParcel[sourceTable][index]['ROWUID'];
                value = (value && value.length) ? value.replace('&gt;', '>') : "";
                value = (value && value.length) ? value.replace('&lt;', '<') : "";
                if (dField.InputType == 5) {
	                var disText = evalLookup( fName, value, null, dField, null, null );
	                disText = disText && disText != ''? disText: value;
	                $(item).html(disText);
                }

                $(item).attr('calcField', sourceTable + ',' + rowId);
                $(item).val(value);
            });
            $('.classCalcAttributes').unbind('change');
  		    $('.classCalcAttributes').change(function () {
                // $(this).parent().addClass('unsaved');
                /* 
                var fId = $($(this).parent()).attr('fieldid');
                var field_name = datafields[fId].Name;
                var sourceTable = datafields[fId].SourceTable;
                if(index != -1) {
                    activeParcelEdits.push({ AuxROWUID: activeParcel[sourceTable][index].ROWUID, ChangeId: null, FieldId: parseInt(fId), ParcelId: activeParcel.Id, QCValue: $(this).val().toString(), action: "edit", parentROWUID: activeParcel[sourceTable][index].ParentROWUID })
                    activeParcel[sourceTable][index][field_name] = $(this).val().toString();
                }
                */
               
                var sd = $(this).attr('calcField');
				var flId = $(this).parent().attr("fieldid");
        	    var fld = datafields[flId];
				let splitsd = sd.split(',') ,
                    selValue = $(this).val();
				tempsRec[0] = {'cat':splitsd[0],'auxrowuid':splitsd[1]};
				if (splitsd[0] && splitsd[1]) {
					var sRec = activeParcel[splitsd[0]].filter(function(x) {return x.ROWUID == splitsd[1]})[0];
					tempactiveParcelEdits.push({ AuxROWUID: splitsd[1], ChangeId: null, FieldId: parseInt(flId), ParcelId: activeParcel.Id, QCValue: selValue, action: "edit", parentROWUID: (sRec? sRec.ParentROWUID: null) });
	            	tempsRec[1][fld.Name] = selValue;
	            	$(this).parent().addClass('unsaved');
       	 	    }


	        });
        });
        $('.btnCalculate').unbind('click');
        $('.btnCalculate').bind('click', function () {
            calculateClassRecommendation(categoryID, sourceTable, index, auxForm);
        });
        $('.btnCancel').unbind('click');
        $('.btnCancel').bind('click', function () {
            $('.updateClass').hide();
            $('.classRec').hide();
            cancelClassCalc(categoryID, index);
        });
    }
    else
        $('.classCalc', auxForm).hide();
    $('.parcel-field-values .input[doNotAllowEditExistingRecords]', block).removeAttr("disabled");
   // $('.customddlspan', block).show();
    var source;
    var count;
    var filteredIndexes;
    var curntCat = getCategory(categoryID);
    let indicesToRemove = [];
    var sourceDataAux;
    const filteredFieldData = fieldCategories.filter(item => item.Id == categoryID && item.HideRecordExpression !== null);
    if (filteredFieldData.length > 0) {
        filteredFieldData.forEach(function (categ) {
            for (let uindex = 0; uindex < activeParcel[sourceTable].length; uindex++) {
                var isExp = activeParcel.EvalValue(sourceTable + '[' + uindex + ']', categ.HideRecordExpression);
                if (isExp && isExp != "****") {
                    indicesToRemove.push(uindex);
                }
            }

            sourceDataAux = activeParcel[sourceTable].filter(function (_, uindex) {
                return !indicesToRemove.includes(uindex);
            });
        });
    }
    if ((filter_name != undefined) && (filterValue != undefined) && (filterValue.length > 0) && (filter_name.length > 0)) {
        source = showAuxDataWithFilter(sourceTable, filter_name, filterValue, parentROWUID,categoryID);
        filteredIndexes = getFilteredIndexes(sourceTable, filter_name, filterValue, parentROWUID,categoryID);
        fIndx[sourceTable] = filteredIndexes;
    }
    else {
        source = eval('activeParcel.' + sourceTable);
        if (!source)
            filteredIndexes = [];
        else if (fIndx[sourceTable] && curntCat.ParentCategoryId)
            filteredIndexes = fIndx[sourceTable];
        else
            filteredIndexes = eval('activeParcel.' + sourceTable + '.map(function(d,i){return i;})');
    }

    if (sourceDataAux && (sourceDataAux.length > 0 || indicesToRemove.length > 0) ) {
        source = source.filter(item => {
            return sourceDataAux.some(auxItem => auxItem.ROWUID === item.ROWUID);
        });
    }
    //getGridViewData(categoryID, filteredIndexes);
	getnewGridView(categoryID,sourceTable,index)
    var cat = getCategory(categoryID);
    if((cat.MultiGridStub == 'true'||cat.MultiGridStub == true) && !firstTimeQCLoad) {
        loadnewMultiGrid(block,index)
    }
    if (source != undefined) {
        count = source.length;
        //if (fIndx[sourceTable] && curntCat.ParentCategoryId)
            //count = fIndx[sourceTable].length;
        var rowid = $(block).attr('ROWUID');
        sourceDataAux = sourceDataAux ?? activeParcel[sourceTable];

        if (rowid && sourceDataAux.length > 0) {
            if (sourceDataAux.filter(function (x) { return x.ROWUID == rowid })[0] != undefined)
                var prowid = sourceDataAux.filter(function (x) { return x.ROWUID == rowid })[0].ParentROWUID;
            if (prowid)
                count = sourceDataAux.filter(function (x) { return x.ParentROWUID == prowid }).length;
        }	
    }
    else count = 0;
    var recoveryDivControl;
    var first;
    $( '.aux-index', block ).attr( 'findex', filteredIndexes[index]);
    $( auxForm ).attr( 'findex', filteredIndexes[index]);   // $( auxForm ).attr( 'findex', index); 10300 need to update findex not index to auxdata. 
    var parentSpec = getParentSpec(categoryID);
    $('.childCategoryTitle').nextAll('span').remove();
    HideCategoryByExpression( categoryID, sourceTable, filteredIndexes[index], parentSpec ? parentSpec.source + '[' + parentSpec.index + ']' : null )
    var childCategories = getChildCategories(categoryID);
    var childcat = [];
    $.each(childCategories, function (a, b) {
        if ($(".childCategoryTitle[category='" + childCategories[a] + "']").css('display') != 'none') {
        if(!isBPPParcel) {
             if(getCategory(childCategories[x]).Type == '1') return;
             }
          else{
             if(getCategory(childCategories[x]).Type != '1' || getCategory(childCategories[x]).Type != '2')  return;
             }
        // if(!isBPPParcel) if(getCategory(childCategories[a]).Type == '1') return;
          childcat.push(childCategories[a]);
        }
    });
    for (x in childcat) {
        if (x == (childcat.length - 1)) {
        }
        else {
            $(".childCategoryTitle[category='" + childcat[x] + "']").after('<span>,</span>');
        }
    }
     if ($('.aux-grid-view[catid="' + categoryID + '"]').css("display") == 'block') {
        $('.data-navigator', block).hide();
        $('.heightdiv', block).hide();
        $('.sublevelDiv', block).hide();
        $('.parcel-field-values', block).hide();
        $('.auxrecover', block).hide();
        $('.support__catHead', block).hide();
        $(block).attr('state', 'grid');
    } else {
        $('.data-navigator', block).show();
        if (count > 0) {
            $('.heightdiv', block).show();
            $('.sublevelDiv', block).show();
            $('.parcel-field-values', block).show();
            if (!__DTR && localStorage && localStorage.internalSupportView == '1' && localStorage.parcelDataSupportView == '1') 
    			$('.support__catHead', block).show();
            $(block).attr('state', 'form');
            if (shwfrm == false){
            $('.parcel-data-view').scrollTop(0);
            }
           else
             shwfrm = false;
        }
    }
    if (source && (source.length > 0)) {
        first = source[index];
        //for (var x in first) {
        //    var field = first[x];
        //    $('.parcel-field-values .value[fieldname="' + x + '"] .input', block).attr('auxrowuid', first.ROWUID);
        //    $('.parcel-field-values .value[fieldname="' + x + '"] .input', block).siblings('span.qc').attr('auxrowuid', first.ROWUID);
        //    setFieldValue(field, x, block);
        //}
        // No need to loop through all key fields so changed above commented statements  
        $('.sublevelDiv').attr('ParentROWUID', parentROWUID);
        var recoverSet = false;
        var deletePCI = [], cId = 0;
        var filteredCh = activeParcel.ParcelChanges.filter(function (d, i) { return d.AuxROWUID == first.ROWUID && d.SourceTable == sourceTable && !(d.NewValue == null && d.OriginalValue == null) });
        $('span.qc a', block).hide();
        if (first.CC_Deleted == true || first.CC_Deleted == "true") {
            deletePCI = activeParcel.ParcelChanges.filter(function (d, i) { return d.AuxROWUID == first.ROWUID && d.SourceTable == sourceTable && d.Action == 'delete'; });
            recoveryDivControl = $('.category-page[categoryid="' + categoryID + '"] .auxrecover');
            $(recoveryDivControl).show();
            $('.del-item', block).hide();
            cId = deletePCI.length > 0 ? deletePCI[0].Id : 0;
            $(recoveryDivControl).attr({ 'AuxROWUID': first.ROWUID, 'SourceTable': sourceTable, 'ChangeId': cId });
            $('.category-page[categoryid="' + categoryID + '"] .childDiv').hide();
            $('.customddlspan', block).hide();
            $('.category-page[categoryid="' + categoryID + '"] span.qc a').hide();
            //$('.category-page[categoryid="' + categoryID + '"] .input').attr({ 'recovery': 'recovery', 'disabled': 'disabled' });
            recoverSet = true;
        }
        if (filteredCh.length > 0) {
        	if(!firstTimeQCLoad && !CalcFieldsOnly){
	            for (x in filteredCh) {
	                var ch = filteredCh[x];
	                var qc = $('.qc[fieldid="' + ch.FieldId + '"]');
	                $('a', qc).hide();
	                $((activeParcel.QC && ch.QCChecked) ? '.passed' : '.check', qc).show();
	                $((activeParcel.QC && ch.QCChecked) ? '.passed' : '.check', qc).attr('changeid', ch.Id);
	                //if (ch.Action == 'delete' || first.CC_Deleted == true || first.CC_Deleted == "true") {
	                //    recoveryDivControl = $('.category-page[categoryid="' + categoryID + '"] .auxrecover');
	                //    $(recoveryDivControl).show();
	                //    $('.del-item', block).hide();
	                //    $(recoveryDivControl).attr({ 'AuxROWUID': ch.AuxROWUID, 'SourceTable': ch.SourceTable, 'ChangeId': ch.Id });
	                //    $('.category-page[categoryid="' + categoryID + '"] .childDiv').hide();
	                //    $('.customddlspan', block).hide();
	                //    $('.category-page[categoryid="' + categoryID + '"] span.qc a').hide();
	                //    $('.category-page[categoryid="' + categoryID + '"] .input').attr({ 'recovery': 'recovery', 'disabled': 'disabled' });
	                //}
	                //else {
	                if (!recoverSet) {
	                    $('.category-page[categoryid="' + categoryID + '"] .auxrecover').hide();
	                    $('.category-page[categoryid="' + categoryID + '"] .auxrecover').removeAttr('AuxROWUID SourceTable ChangeId');
	                    var inputControl = $('.category-page[categoryid="' + categoryID + '"] .input[recovery]');
	                    $(inputControl).removeAttr('disabled recovery');
	                    if (getDescendantCategoryIds(categoryID).length > 0) {
	                        $('.category-page[categoryid="' + categoryID + '"] .childDiv').show();
	                    }
	                }
	                setFieldValue(ch.NewValue, ch.FieldId, block,null,null,null,doNotCalculateExpression);
	            }
	       }
        }
        else {
            if (!recoverSet) {
                $('.category-page[categoryid="' + categoryID + '"] .auxrecover').hide();
                var inputControl = $('.category-page[categoryid="' + categoryID + '"] .input[recovery]');
                $(inputControl).removeAttr('disabled recovery');
                $('.category-page[categoryid="' + categoryID + '"] .auxrecover').removeAttr('AuxROWUID SourceTable ChangeId');
                if (getDescendantCategoryIds(categoryID).length > 0 && getChildCategories(categoryID).length > 0) {
                    $('.category-page[categoryid="' + categoryID + '"] .childDiv').show();
                }
            }
        }
        recordSwitching.switches = [];
        var categoryFields = [];
        if(firstTimeQCLoad){
		  $('.parcel-field-values .value', block).each(function () { 
			  var fieldid = $(this).attr('fieldid');
			  if(datafields[fieldid] && datafields[fieldid].AutoSelectFirstitem) categoryFields.push($(this).attr('fieldid'));
		  });
		  if(categoryFields.length === 0){
			  $('.parcel-field-values .value', block).each(function () { 
				  categoryFields.push($(this).attr('fieldid'));
				  return false;
			  });  
		  }
	  } else if(CalcFieldsOnly)
	  	categoryFields = CalcFieldsOnly;
        $('.parcel-field-values .value', block).each(function (eIndex) { recordSwitching.switches[eIndex] = false });
        $('.parcel-field-values .value', block).each(function (a, f) {
            var t = $(f).attr('fieldname');
            var fieldid = $(f).attr('fieldid');
            if ((firstTimeQCLoad || CalcFieldsOnly) && !categoryFields.includes(fieldid))
                return true;
            var field = first[t];
            var fieldIndex = a;
            $('.input', f).attr('auxrowuid', first.ROWUID);
            $('.input', f).siblings('span.qc').attr('auxrowuid', first.ROWUID);
            setFieldValue(field, t, block,function(fieldIndex){
		recordSwitching.switches[fieldIndex] = true;
		recordSwitching.swichingFn();
            },null,null,doNotCalculateExpression,fieldIndex);
        });
        $('.parcel-field-values td.value', block).removeClass('unsaved');
        for (x in activeParcelEdits) {
            var ed = activeParcelEdits[x];
            if (ed.AuxROWUID && ed.AuxROWUID.toString() == $('.parcel-field-values .value[fieldid="' + ed.FieldId + '"] .input', block).attr('auxrowuid')) {
                $('.parcel-field-values td.value[fieldid="' + ed.FieldId + '"]', block).addClass('unsaved');
                setFieldValue(ed.QCValue, ed.FieldId, block,null,null,null,doNotCalculateExpression);
            }
        }
        if ($('.aux-grid-view[catid="' + categoryID + '"]').css("display") == 'block' && $('.grid-view[categoryid="' + categoryID + '"]').css("display") == 'block')
        $('.auxrecover', block).hide();
        if ($('.aux-grid-view[catid="' + categoryID + '"]').css("display") != 'block') {
        	$('.parcel-field-values', block).show();
        	if (!__DTR && localStorage && localStorage.internalSupportView == '1' && localStorage.parcelDataSupportView == '1') 
    			$('.support__catHead', block).show();
        }	
        
        $('.category-page[categoryid="' + categoryID + '"] .aux-ROWUID').text('AUXROWUID : ' + first.ROWUID);
    } else {
        index = -1;
        $('.category-page[categoryid="' + categoryID + '"] .auxrecover').hide();
        $('.parcel-field-values', block).hide();
       // $('.support__catHead', block).hide();
        $('.parcel-field-values .input').removeAttr('i');
        $('.childDiv', auxForm).hide();
        $('.sublevelDiv', auxForm).hide();
    }

    if (index < 1) {
        $('.move-first', block).attr('disabled', 'disabled');
        $('.move-prev', block).attr('disabled', 'disabled');
    } else {
        $('.move-first', block).removeAttr('disabled');
        $('.move-prev', block).removeAttr('disabled');
    }

    if (index == count - 1) {
        $('.move-last', block).attr('disabled', 'disabled');
        $('.move-next', block).attr('disabled', 'disabled');
    } else {
        $('.move-last', block).removeAttr('disabled');
        $('.move-next', block).removeAttr('disabled');
    }
    $('.new-item', block).show();
    if ($('button.new-item', block).attr('IsReadOnly') == "True" || $('button.new-item', block).attr('AllowAddDelete') == "False" || $('button.new-item', block).attr('allowdeleteonly') == "True") {
        $('.new-item', block).hide();
    }
    let isReadOnlyUserDTR = __DTR && DTRRoles && DTRRoles.ReadOnly ? true : false;
    if (isReadOnlyUserDTR) {
        $('.new-item', block).hide();
        $('.del-item', block).hide();
    }
    if (count == 0) {
        $('.del-item', block).hide();
    } else {
        $('.del-item', block).unbind();
        if (!recoveryDivControl)
            $('.del-item', block).show();
        $('.del-item', block).bind('click', function (e) {
            e.preventDefault();
            //vineesh
            //                        $qc('deleteauxrecord', { ParcelId: activeParcel.Id, SourceTable: sourceTable, ROWUID: first.ROWUID }, function (data) {
            //                            if (data.status == "OK") {
            //                                getParcelDataUpdates();
            //                            }
            //                        });
            //vineesh
            disableSwitch = false;
            var delRecord = { ParcelId: activeParcel.Id, SourceTable: sourceTable, ROWUID: first.ROWUID, action: "delete" };
            deleteCurrentRecord(delRecord, first, source, categoryID);
        });

        if ($('.del-item', block).attr('IsReadOnly') == "True" || ($('.del-item', block).attr('AllowAddDelete') == "False" && $('.del-item', block).attr('allowdeleteonly') == "False") || (ccma.Session.RealDataReadOnly == true && activeParcel.IsRPProperty)) {
            $('.del-item', block).hide();
        }
        if ($('.del-item', block).attr('DoNotAllowDeleteFirstRecord') == "True" && ($('.del-item', block).attr('AllowAddDelete') == "True" ||  $('.del-item', block).attr('allowdeleteonly') == "True")) {
            if (isReadOnlyUserDTR || index == 0 || (index > 0 && source[index].CC_Deleted == true))
                $('.del-item', block).hide();
            else
                $('.del-item', block).show();
        }
        var isExp = true
        if ( cat.EnableDeleteExpression && cat.EnableDeleteExpression != '' )
            isExp = activeParcel.EvalValue( sourceTable + '[' + filteredIndexes[index] + ']', cat.EnableDeleteExpression, null, parentSpec ? parentSpec.source + '[' + parentSpec.index + ']' : null );

        var doNotAllowEditExistingRecords = fieldCategories.filter( function ( x ) { return x.SourceTable == sourceTable } ).map( function ( p ) { return p.DoNotAllowEditExistingRecords } )[0]
        if ( ( doNotAllowEditExistingRecords == 'true' || doNotAllowEditExistingRecords == true ) && isExistingAuxDetail( activeParcel, sourceTable, index ) )
        {  //to check if it is the existing record
            $( '.parcel-field-values .input', block ).attr( { 'doNotAllowEditExistingRecords': 'doNotAllowEditExistingRecords', "disabled": "disabled" } );
            $( '.parcel-field-values input:radio', block ).attr( 'disabled', 'disabled' );
            $( '.del-item', block ).attr( 'disabled', 'disabled' );
            $( '.customddlspan', block ).hide();
        }
        else if (isReadOnlyUserDTR || !isExp )
            $( '.del-item', block ).attr( 'disabled', 'disabled' );
        else
            $( '.del-item', block ).removeAttr( 'disabled', 'disabled' );
    }

    var parcat = $('#AllCategories li[category="' + categoryID + '"]').attr('parentid');
    var parSource = $('.category-page[categoryid="' + parcat + '"]').attr("auxdata");
    if(sourceTable != undefined && sourceTable == parSource) count=1;
   
    $('.aux-records', block).html(count ? count : '0');
    $('.aux-index', block).html(index + 1);
    $('.aux-grid-switch', block).attr('index', index);
    highlightwithFilter(categoryID);
    //setTimeout('highlightwithFilter(' + categoryID + ');', 500);
    //}
    //    catch (e) {
    //        console.log("AuxDataError: " + e);
    ////    }
    //$('.parcel-field-values .value', block).each(function (a, f) {
    //    var x = $(f).parent().parent().parent().find('tr')[a];
    //    if ($(f).attr('fieldid') || $(f).attr('fieldid') != '') {
    //        var dfield = datafields[$(f).attr('fieldid')];
    //        if (dfield && (dfield.IsClassCalculatorAttribute == true || dfield.IsClassCalculatorAttribute == "true"))
    //            $(x).attr('style', 'display: none');
    //        else
    //            $(x).show();
    //    }
    //});
    checkRequiredIfEditsFieldsInForm(categoryID, index);
    refreshAuxCount(categoryID, block)
    initRadioButtonChangeTracker();
    if (callback) callback(filteredIndexes[index]);
}

var recordSwitching = {
	switches : [],
	swichingFn : function(sfield){
			if(autoselectloop.length > 0){
				if(sfield && autoselectloop.indexOf(sfield.Id) > -1){
					autoselectloop.splice(autoselectloop.indexOf(sfield.Id),1);
				}
			}
			if(recordSwitching.switches.reduce(function(x,y){ return x && y}, true)){
				disableSwitch = false;
			}
	}
}

function deleteCurrentRecord(delRecord, first, source, categoryID) {
    if (confirm('Do you really want to delete this record?')) {
        showMask();
        setTimeout(() => {
            var index = source.indexOf(first);
            source[index].CC_Deleted = true;
            var removed = source[index];//source.splice(index, 1);
            var pcateID = getParentCategories(categoryID);
            let delItem = { catID: categoryID, sTable: delRecord.SourceTable, rowuid: delRecord.ROWUID, childs: [] }, childreIndex = -1, Piindex = -1, psourceTable = null;
            if (pcateID) {
                let rowwwId = removed.ROWUID, prowwwId = removed.ParentROWUID;
                psourceTable = getCategory(pcateID).SourceTable;
                Piindex = activeParcel[psourceTable].indexOf(activeParcel[psourceTable].filter(function (x) { return x.ROWUID == prowwwId })[0]);
                childreIndex = activeParcel[psourceTable][Piindex][delRecord.SourceTable].indexOf(removed);              

                delItem.prowuid = prowwwId; delItem.psTable = psourceTable;
            }
            //var obj = activeParcel[delRecord.SourceTable];
            let removedIndex = activeParcel[delRecord.SourceTable].indexOf(removed);
            if (removedIndex >= 0) {
                //activeParcel[delRecord.SourceTable].splice(removedIndex, 1);
                if (childreIndex > -1 && Piindex > -1 && psourceTable) {
                    //activeParcel[psourceTable][Piindex][delRecord.SourceTable].splice(childreIndex, 1);
                    activeParcel[psourceTable][Piindex][delRecord.SourceTable][childreIndex].CC_Deleted = true;
                }

                let childs = getChildCategories(categoryID);
                childs.forEach(function (cld) {
                    let sourceTb = getSourceTable(cld), crowIds = [];
                    //activeParcel[sourceTb].filter((rec) => { return rec.ParentROWUID == delRecord.ROWUID }).map((x) => { return x.CC_Deleted = true });
                    activeParcel[sourceTb].filter(function (rec) { return rec.ParentROWUID == delRecord.ROWUID }).forEach((x) => {
                        x.CC_Deleted = true; crowIds.push(x.ROWUID);
                    });

                    delItem.childs.push({ csTable: sourceTb, rowIds: crowIds });
                });
            }

            $('.category-page[categoryid="' + categoryID + '"] .move-prev').click();
            $('.category-page[categoryid="' + categoryID + '"] .move-next').click();
            if (parseInt(delRecord.ROWUID) < 0) {
                //activeParcelEdits = activeParcelEdits.filter(function (el) { return (el.AuxROWUID != delRecord.ROWUID) });
                delItem.newRecord = true;
            }
            else {
                var delRec = {
                    ParcelId: delRecord.ParcelId,
                    AuxROWUID: delRecord.ROWUID,
                    FieldId: null,
                    ChangeId: null,
                    QCValue: delRecord.SourceTable,
                    action: delRecord.action,
                    parentROWUID: null
                };
                // console.log(delRec);
                activeParcelEdits.push(delRec);
            }
            recoveryDeletedArrays.push(delItem);
            setTimeout(() => { hideMask(); }, 500);
        }, 100);
    }
    else {
        return false;
    }
}
function showAuxDataWithFilter(NodeFilter, filter_name, filterValue, parentROWUID,categoryID) {
    try {

        if (NodeFilter == undefined) {
            return eval('activeParcel.' + NodeFilter);
        }
        var filterCondition = '';
        var iter_value = 0;
        while (iter_value < filter_name.length) {
            var childFilter, parentFilter; parentFilter = childFilter = filter_name[iter_value];
            if (childFilter.indexOf('/') > -1) { parentFilter = childFilter.split('/')[0]; childFilter = childFilter.split('/')[1]; };
            filterCondition += 'd.' + childFilter + ' == ';
            if (typeof filterValue[iter_value] == 'string') {
                filterCondition += '"';
                filterCondition += filterValue[iter_value];
                filterCondition += '"';
            }
            else {
                filterCondition += filterValue[iter_value];
            }
            iter_value += 1;
            if (iter_value < filter_name.length) {
                filterCondition += '&&';
            }
        }
       var category= getCategory(categoryID);
        var parCategory=getCategory(category.ParentCategoryId);
		var fiteredSource = [];
        if(NodeFilter == (parCategory ? parCategory.SourceTable:null))
           fiteredSource=eval('activeParcel.' + NodeFilter + '.filter(function (d, i) { return ((' + filterCondition + ')) })');
       	else
            fiteredSource=eval('activeParcel.' + NodeFilter + '.filter(function (d, i) { return ((!d.ParentROWUID && (' + filterCondition + ')) ||  (d.ParentROWUID && (d.ParentROWUID == ' + parentROWUID + ') )); })');
       //vm  var fiteredSource = eval('activeParcel.' + NodeFilter + '.filter(function (d, i) { return ((!d.ParentROWUID && (' + filterCondition + ')) ||  (d.ParentROWUID && (d.ParentROWUID == ' + parentROWUID + ') )); })');
        //        var fiteredSource;
        //        if (parentROWUID != null) {
        //            fiteredSource = eval('activeParcel.' + NodeFilter + '.filter(function (d, i) { return (d.ParentROWUID == ' + parentROWUID + '); })');
        //        }
        //        else {
        //             var filterCondition = '';
        //             var iter_value = 0;
        //             while (iter_value < filter_name.length) {
        //             filterCondition += 'd.' + filter_name[iter_value] + ' == ' + filterValue[iter_value];
        //             iter_value += 1;
        //             if (iter_value < filter_name.length) {
        //                    filterCondition += '&&';
        //                    }
        //            }
        //            fiteredSource = eval('activeParcel.' + NodeFilter + '.filter(function (d, i) { return (' + filterCondition + '); })');
        //        }
        return fiteredSource;

    }
    catch (e) {
        console.error(e);
    }
}

function getDSTStart(year) {
    var dstYear, dstStartWeek, dstDay, dstdate;
    dstDay = '03/01/' + year;
    dstDay = new Date(dstDay);
    dstStartWeek = dstDay.getDay() + 1;
    switch (dstStartWeek) {
        case 1:
            dstdate = new Date(dstDay.setTime(dstDay.getTime() + 170 * 3600000));
            return dstdate;
            break;
        case 2:
            dstdate = new Date(dstDay.setTime(dstDay.getTime() + 314 * 3600000));
            return dstdate;
            break;
        case 3:
            dstdate = new Date(dstDay.setTime(dstDay.getTime() + 290 * 3600000));
            return dstdate;
            break;
        case 4:
            dstdate = new Date(dstDay.setTime(dstDay.getTime() + 266 * 3600000));
            return dstdate;
            break;
        case 5:
            dstdate = new Date(dstDay.setTime(dstDay.getTime() + 242 * 3600000));
            return dstdate;
            break;
        case 6:
            dstdate = new Date(dstDay.setTime(dstDay.getTime() + 218 * 3600000));
            return dstdate;
            break;
        case 7:
            dstdate = new Date(dstDay.setTime(dstDay.getTime() + 194 * 3600000));
            return dstdate;
            break;

    }

}
function getDSTEnd(year) {
    var dstYear, dstEndWeek, dstDay, dstDate;
    dstDay = '11/01/' + year;
    dstDay = new Date(dstDay);
    //dstDay=dstDay.setDate(dstDay.getDate() + 7);
    dstEndWeek = dstDay.getDay() + 1;
    switch (dstEndWeek) {
        case 1:
            dstDate = new Date(dstDay.setTime(dstDay.getTime() + 2 * 3600000));
            return dstDate;
            break;
        case 2:
            dstDate = new Date(dstDay.setTime(dstDay.getTime() + 146 * 3600000));
            return dstDate;
            break;
        case 3:
            dstDate = new Date(dstDay.setTime(dstDay.getTime() + 122 * 3600000));
            return dstDate;
            break;
        case 4:
            dstDate = new Date(dstDay.setTime(dstDay.getTime() + 98 * 3600000));
            return dstDate;
            break;
        case 5:
            dstDate = new Date(dstDay.setTime(dstDay.getTime() + 74 * 3600000));
            return dstDate;
            break;
        case 6:
            dstDate = new Date(dstDay.setTime(dstDay.getTime() + 50 * 3600000));
            return dstDate;
            break;
        case 7:
            dstDate = new Date(dstDay.setTime(dstDay.getTime() + 26 * 3600000));
            return dstDate;
            break;
    }

}

function setFieldValue() {
    var fieldValue = arguments[0];
    var x = arguments[1];
    var block = arguments[2];
    var callback = arguments[3];
    var isRelatedRefreshCall = arguments[4] || false;
    var relationParent = arguments[5] || null;
    var doNotCalc =  arguments[6] || null;
    var fieldIndex = arguments[7];
    var allowTxtRefreshCall = arguments[8] || false;
    var isFieldId = isFinite(x);
    var ielem = $('.parcel-field-values .value[' + (isFieldId ? 'fieldid' : 'fieldname') + '="' + x + '"] .input', block);
    var fieldId = $(ielem).parents('[fieldid]').attr('fieldid');
    var field = datafields[fieldId];
    var type = $(ielem).attr('type');
    var catPage = $(ielem).parents('.category-page[categoryid]');
    var readonlyexp, visibilityexp, Requiredexp, fcalc, fOvcalc, cat_readonlyexp;
    var roByexp, viByexp, isrByexp, isfOvcalc, cat_roByexp;
    var categoryID = $(block).attr('categoryid');
    //  console.log(ielem)
    // var value = (field && (typeof field != "object")) ? field : '';
    var noConvertionReq = clientSettings['NoTimeConversion'] || '0';
    var value = (fieldValue != null && fieldValue != undefined && (typeof fieldValue != "object")) ? htmlDecode(fieldValue) : '';
        //value = value.trim();
    if (field && field.UIProperties && field.UIProperties != null)
        value = (fieldValue != null && fieldValue != undefined && (typeof fieldValue != "object")) ? customFormatValue(htmlDecode(fieldValue), field.Id) : '';
        //value = value.trim();  
    if (field && field.InputType == '5' && (typeof fieldValue == "number")) {
        if (parseFloat(value) == fieldValue ) value = parseFloat(value);
    }
    var autosize = function ( el )
    {
        if(el)
		    if(el.scrollHeight==0){
			    el.style.cssText = 'height:' + 36 + 'px';
		    }
		    else {		
			    el.style.cssText = 'height:auto';
			    el.style.cssText = 'height:' + el.scrollHeight + 'px';			
		    }
	}
    if (field) {
    ielem.css('background-color',field.Colour);
        if (field.InputType == 3 && (field.RadioFieldValues == '' ||!field.RadioFieldValues))
            value = value == 'true' || value == '1' || value == 1 ? 'true' : value == 'false' || value == '0' ? 'false' : value;
        if (type == "date") {
            if (value != '') {
            	if (value.length == 8){
            		value = value.substring(0,4) + '-' + value.substring(4,6) + '-' + value.substring(6,8);
            	}
            	else if (value.length > 8 ){
            		value = value.substring(0,4) + '-' + value.substring(5,7) + '-' + value.substring(8,10)+' '+value.substring(11,value.length);
            	}
                value = noConvertionReq == '1' ? value.substring(0, 10) : dateTolocal(value);
            }
            if (value == "1900-01-01")      //Handle null dates if encountered.
                value = "";
        }
    }

    (isFieldId ? getDatatypeField(x) : getDatatypeFieldName(fieldId)) == '11' ? value = value.split(',') : '';
   /* Removed trimming -vm
   	if (value) {
        if (value.trim) value = value.trim();
       if (value.length) for (var i in value) { value[i] = value[i].toString().trim(); }
    }*/

    var sourceTable = $(block).attr('auxdata');
    var findex = $('.aux-index', block).attr('findex') || 0;
    var auxrowid = $(ielem).attr('auxrowuid');


    var v = [];
    //if (auxrowid)
      //  $('.aux-ROWUID').text('AUXROWUID : ' + auxrowid);
    var ds = activeParcel;
    if ( sourceTable && activeParcel[sourceTable] ) {
        ds = activeParcel[sourceTable][findex];
    }
    var dscopy = {};
    for (var fx in ds) { dscopy[fx] = ds[fx]; }

    if (field) {
        var tableFieldIds = displayFields.filter(function (x) { return x.CategoryId == field.CategoryId }).map(function (x) { return x.Id; })
        if (sourceTable) {
            activeParcel.ParcelChanges.filter(function (x) { return tableFieldIds.indexOf(x.FieldId) > -1 && x.AuxROWUID == auxrowid }).forEach(function (x) {
                dscopy[x.FieldName] = x.NewValue;
            });
            activeParcelEdits.filter(function (x) { return tableFieldIds.indexOf(parseInt(x.FieldId)) > -1 && x.AuxROWUID == auxrowid }).forEach(function (x) {
                var ff = datafields[x.FieldId].Name;
                dscopy[ff] = x.QCValue;
            });
        } else {
            activeParcel.ParcelChanges.filter(function (x) { return tableFieldIds.indexOf(x.FieldId) > -1 && (x.AuxROWUID == null || x.AuxROWUID == 0) }).forEach(function (x) {
                dscopy[x.FieldName] = x.NewValue;
            });
            activeParcelEdits.filter(function (x) { return tableFieldIds.indexOf(parseInt(x.FieldId)) > -1 && (x.AuxROWUID == null || x.AuxROWUID == 0) }).forEach(function (x) {
                var ff = datafields[x.FieldId].Name;
                dscopy[ff] = x.QCValue;
            });
        }
        if (field) {
            var autoSelectLookup = function (lkdVals) {
                var value = dscopy[field.Name]
                if (ds)
                    if ((parseInt(field.InputType) == 3 || (parseInt(field.InputType) == 5) || parseInt(field.InputType) == 11) && ds.CC_Deleted == false) {
                        //if ($(ielem).html()) {

                            let autoSelectExp = (parseInt(field.InputType) == 5 && isRelatedRefreshCall && $(ielem).val() != value && lkdVals.findIndex(element => element.Id == value) > -1 && lkdVals.findIndex(element => element.Id == $(ielem).val()) == -1) ? true : false;
                            if (autoSelectExp) {
                                var ExpstatusAuto = "";  //Checking
                            }
                        var relationalFieldAutoSelect = (parseInt(field.InputType) == 5 && field.LookupTable == '$QUERY$' && field.LookupQuery && field.AutoSelectFirstitem == true && ($(ielem).val() === "" || ($(ielem).val() != "" && lkdVals.findIndex(element => element.Id == value) === -1 && (!(field.AllowTextInput == true || field.AllowTextInput == 'true') || ((field.AllowTextInput == true || field.AllowTextInput == 'true') && allowTxtRefreshCall))) || autoSelectExp) && ((lkdVals[0].Id == "" || lkdVals[0].Id == '<blank>' || lkdVals[0].Id == null || lkdVals[0].Value == '<blank>') && lkdVals[1])) //(parseInt(field.InputType) == 5 && field.LookupTable == '$QUERY$' && field.LookupQuery && field.AutoSelectFirstitem == true && ($(ielem).val() === "" || ($(ielem).val() != value && $(ielem).find('option[value="' + encodeURI(value) + '"]').length == 0)) && !(($(ielem).find('option:first-child').val() == "" || $(ielem).find('option:first-child').val() == null) && $(ielem).find('option').length == 1))
                            //var relationalFieldAutoSelect = ( parseInt(field.InputType) == 5 && field.LookupTable == '$QUERY$' && field.LookupQuery && field.AutoSelectFirstitem == true && ( $(ielem).val() === "" || ( $(ielem).val() != "" && lkdVals.findIndex(element => element.Id == value) === -1 ) ) && ( (lkdVals[0].Id == "" || lkdVals[0].Id == '<blank>' || lkdVals[0].Id == null || lkdVals[0].Value == '<blank>') && lkdVals[1] ) ) //(parseInt(field.InputType) == 5 && field.LookupTable == '$QUERY$' && field.LookupQuery && field.AutoSelectFirstitem == true && ($(ielem).val() === "" || ($(ielem).val() != value && $(ielem).find('option[value="' + encodeURI(value) + '"]').length == 0)) && !(($(ielem).find('option:first-child').val() == "" || $(ielem).find('option:first-child').val() == null) && $(ielem).find('option').length == 1))
                            var autoSelect = ( value == null && ( field.AutoSelectFirstitem == "true" || field.AutoSelectFirstitem == true ) && ds[field.AssignedName] == null && ( ( parseInt(field.InputType) == 5 && !(  (lkdVals[0].Id == "" || lkdVals[0].Id == '<blank>' || lkdVals[0].Value == '<blank>') && lkdVals[1] ) ) || ( parseInt(field.InputType) != 5 && !( $(ielem).find('option:first-child').val() == '' && $(ielem).find('option').length == 1 ) ) ) )     //(value == null && (field.AutoSelectFirstitem == "true" || field.AutoSelectFirstitem == true) && ds[field.AssignedName] == null && !($(ielem).find('option:first-child').val() == '' && $(ielem).find('option').length == 1))
                            if (relationalFieldAutoSelect || autoSelect) {
                            	if(parseInt(field.InputType) == 5 && lkdVals) {
                            		if(!lkdVals[0]) {
                            			$(ielem).val(''); $(ielem).html('');
                            		}
                            		else {
	                            		var lkv = ((lkdVals[0].Id == ''  || lkdVals[0].Id == '<blank>' || lkdVals[0].Value == '<blank>') && lkdVals[1])? lkdVals[1]: lkdVals[0];
	            						$(ielem).val((lkv.Id || lkv.Id == 0)? lkv.Id: '' );
	            						var disNm = lkv.Name? lkv.Name : ((lkv.Id || lkv.Id == 0)? lkv.Id: '');
	            						$(ielem).html(disNm);
            						}
                            	}
                            	else {
                            		if ($(ielem).find('option:first-child').val() == '')
                                    	$(ielem)[0].selectedIndex = 1;
                                	else
                                    	$(ielem)[0].selectedIndex = 0;
                            	}
                            	
                                var currentValue = value;
                                let tValue;
                                try {
                                    tValue = decodeURI($(ielem).val());
                                } catch { 
                                    tValue = $(ielem).val();
                                }
                                if (currentValue == tValue && !autoSelectExp)
                                    return;
								var customAutoselect = (field.IsCustomField == true || field.IsCustomField == 'true'  ) ? true : false;
                                registerValueEdit(activeParcel.Id, $(ielem).attr('auxrowuid') || 0, $(ielem).parent().attr("fieldid") || 0, 0, $(ielem).val(), 'edit', null, customAutoselect,null,true);
                               // registerValueEdit(activeParcel.Id, $(ielem).attr('auxrowuid') || 0, $(ielem).parent().attr("fieldid") || 0, 0, value, 'edit', null, true);
                            }
                        //}
                    }
            }
			if (parseInt(field.InputType) == 13) {
            	var radioOptions = [];
				if (field.RadioFieldValues && field.RadioFieldValues != "") {
            		var values = []; 
            		values = field.RadioFieldValues.trim().split(',');
            		radioOptions = [{ name : field.Id, text: 'Yes', value: values[0] }, { name : field.Id, text: 'No', value: values[1] }];
        		}
        		else
	        		radioOptions = [{ name : field.Id, text: 'Yes', value: 'true' }, { name : field.Id, text: 'No', value: 'false' }];
	    		if(field.AllowRadioNull == "true"){
	    			var nullOption = { name : field.Id, text: 'Blank', value: '' }
	    			radioOptions.push(nullOption);
	    		}
	    		$(ielem).html('');
				$(ielem).html('<input type="radio" id=${name}${value}  class="radioButton" name=${name} value=${value}><label for =${name}${value} >${text}</label></input>');
				$(ielem).fillTemplate(radioOptions);
				$(ielem).css('border','0px');
				$('input', $(ielem)).css('height','13px');
	        	$('input', $(ielem)).removeAttr('checked'); 
	        	$('input[value = "'+ value +'"]', $(ielem)).attr( "checked", true );
        	}
            var cat = getCategory(field.CategoryId);
            readonlyexp = field.ReadOnlyExpression;
            visibilityexp = field.VisibilityExpression;
            Requiredexp = field.RequiredExpression;
            fOvcalc = field.CalculationOverrideExpression;
            fcalc = field.CalculationExpression;
            cat_readonlyexp = (cat && cat.ReadOnlyExpression) ? decodeHTML(cat.ReadOnlyExpression) : null;
            roByexp = (readonlyexp) ? ((catPage && sourceTable) ? (activeParcel.EvalValue(sourceTable + '[' + findex + ']', readonlyexp)) : (activeParcel.EvalValue(readonlyexp))) : ((cat && cat.ClassCalculatorParameters && cat.ClassCalculatorParameters.split(',')[0] == field.Name) ? true : false);
            viByexp = (visibilityexp) ? ((catPage && sourceTable) ? (activeParcel.EvalValue(sourceTable + '[' + findex + ']', visibilityexp)) : (activeParcel.EvalValue(visibilityexp))) : false;
            isrByexp = (Requiredexp) ? ((catPage && sourceTable) ? (activeParcel.EvalValue(sourceTable + '[' + findex + ']', Requiredexp)) : (activeParcel.EvalValue(Requiredexp))) : false;
            cat_roByexp = (cat_readonlyexp) ? ((catPage && sourceTable) ? (activeParcel.EvalValue(sourceTable + '[' + findex + ']', cat_readonlyexp)) : (activeParcel.EvalValue(cat_readonlyexp))) : ((cat && cat.ClassCalculatorParameters && cat.ClassCalculatorParameters.split(',')[0] == field.Name) ? true : false);
            isfOvcalc = (fOvcalc) ? ((catPage && sourceTable) ? (activeParcel.EvalValue(sourceTable + '[' + findex + ']', fOvcalc)) : (activeParcel.EvalValue(fOvcalc))) : false;
            var index = $(block).attr('index'), isReadOnlyUser = __DTR && DTRRoles && DTRRoles.ReadOnly ? true : false;
		    roByexp = (roByexp && (roByexp == '****')) ? false: roByexp;
            cat_roByexp = (cat_roByexp && (cat_roByexp == '****')) ? false : cat_roByexp;

            let isNewStatusRecord = sourceTable && findex && eval('activeParcel["' + sourceTable + '"]?.[' + findex + ']?.["CC_RecordStatus"]') == "I" ? true : false;

            if (roByexp || (field && field.DoNotAllowEditExistingValue == true && !isNewStatusRecord) || (cat_roByexp && !(cat_readonlyexp && getReadOnlyExpressionOverrideVal(field))) || ( field && field.DoNotEditFirstRecord == true && index == "0" ) || field.ReadOnly || (cat && cat.IsReadOnly && cat.IsReadOnly == "true") || (isBPPParcel && activeParcel.CC_Deleted) || (!isBPPParcel && ccma.Session.RealDataReadOnly) || (__DTR && field.IsReadOnlyDTR) ) {
                if(parseInt(field.InputType) == 13)
                	$('input', ielem).attr('disabled', 'disabled'); 
                else {
                	$(ielem).attr('readonly', 'readonly');  
                	$(ielem).siblings('.cusDpdown-arrow').attr('readonly', 'readonly'); 
					if (parseInt(field.InputType) == 12) $(ielem).siblings('.LoadGeolocationIcon').hide();					
				}                	
                $(ielem).attr('eval-ro', '1');
                $(ielem).siblings('.customddlspan').hide();
                
                $(ielem).on('input', function () {
                    var fieldId = $(this).attr('fieldid');
                    $(this).parent().removeClass('unsaved');
                    $('.check', $('.qc[fieldid="' + fieldId + '"]')).show();
                })
            }
            else {
                var removeReadOnly = true;
                if (__DTR && field.IsReadOnlyDTR) removeReadOnly = false;
                if (field.ReadOnly) removeReadOnly = false;
                if(isReadOnlyUser) removeReadOnly = false;
                if (removeReadOnly){
					if(parseInt(field.InputType) == 13)
                		$('input', ielem).removeAttr('disabled');
                    else {
                        if (!isReadOnlyUser) {
                            $(ielem).removeAttr('readonly');
                            $(ielem).siblings('.cusDpdown-arrow').removeAttr('readonly');
                        }
						
						if (parseInt(field.InputType) == 12) { $(ielem).attr('readonly', 'readonly'); $(ielem).siblings('.LoadGeolocationIcon').show(); }	
					}
				}
                $(ielem).attr('eval-ro', '0');
            }

            if (field && field.ConditionalValidationConfig && activeParcel[sourceTable][findex] && clientSettings?.LoadMethodAfterAppLoad == 'loadValidationTablesForVS8') applyConditionalValidation(field, ielem, activeParcel[sourceTable][findex])

            //FD 24123  
            if (clientSettings.LoadMethodAfterAppLoad == "loadValidationTablesForMI") {
                if (field && field['UI_Settings'] && field['UI_Settings'].MIFields && field['UI_Settings'].MIFields == "true")
                    if (sourceTable && activeParcel[sourceTable]) {
                        applyConditionalValidationForMI(field, activeParcel[sourceTable][findex])
                    }
            }

            if(fcalc != '' && (fOvcalc != '' && fOvcalc != null && fOvcalc != undefined) && !isfOvcalc && field.InputType != "5"){
            	$(ielem).attr('eval-ro', '1');
            	$(ielem).attr('readonly', 'readonly');
            }
            else if (fcalc != '' && (fOvcalc != '' && fOvcalc != null && fOvcalc != undefined) && isfOvcalc && field.InputType != "5" && !isReadOnlyUser){
            	$(ielem).attr('eval-ro', '0');
            	$(ielem).removeAttr('readonly');
            }
            if (Requiredexp && Requiredexp != '') {
                if (isrByexp) {
                    $(ielem).attr('eval-req', '1');
                    $(ielem).parent().siblings().children('.required').show();
                }
                else {
                    //if(!field.IsRequired)
                    $(ielem).attr('eval-req', '0');
                    $(ielem).parent().siblings().children('.required').hide();
                }
            }
            else {
                $(ielem).attr('eval-req', '0');
            }
            if (viByexp) {
            	$(ielem).addClass('hideFieldProp');
            	$(ielem).attr('eval-hi', '1');
            	if (!__DTR && localStorage && localStorage.internalSupportView == '1' && localStorage.parcelDataSupportView == '1') {
            		$(ielem).parents('tr').first().show();
            	}
            	else
                	$(ielem).parents('tr').first().hide();
            }
            else {
            	$(ielem).removeClass('hideFieldProp');
                $(ielem).attr('eval-hi', '0');
                $(ielem).parents('tr').first().show();
            }
        }
        if (isReadOnlyUser) {
            //$('.trackchanges').attr('disabled', 'disabled');
            $('.parcel-priority').css('background-color', '#DDD');
            $('.parcel-priority').on('mousedown', function (event) {
                event.preventDefault();

            });
            $('.parcel-alert-message').attr('disabled', 'disabled');
        }

        let ndt = QC.dataTypes[parseInt(field.InputType)];
        value = getLargeDecToRound(value, ndt);

        if (isFieldId) {
            if (isReadOnly(x) && type != "date" && !(!value && getJsType(getDatatypeFieldName(fieldId)) == "Number")) {
                if (getJsType(getDatatypeFieldName(fieldId)) == "Number") value = value.replace(/,/i, '');
	            value = formatThis(0, x, value,fieldId);
	        }
	    }
	    else {
	        if (isReadOnlyFieldName(x,field.Id) && type != "date") {
                if (value) {
                    if (getJsType(getDatatypeFieldName(fieldId)) == "Number") value = value.replace(/,/i, '');
                    value = formatThis(1, x, value, fieldId);
                }
	        }
	    }
        if (field.DoNotShow) {
        	$(ielem).addClass('alwaysHideFieldProp');
        	if (!__DTR && localStorage && localStorage.internalSupportView == '1' && localStorage.parcelDataSupportView == '1') {
        		$(ielem).parents('tr').first().show();
        	}
        	else
	    		$(ielem).parents('tr').first().hide();
	    }
	    var doNotAllowEditExistingRecords = fieldCategories.filter( function ( x ) { return x.SourceTable == sourceTable } ).map( function ( p ) { return p.DoNotAllowEditExistingRecords } )[0]; 
        var donotEditFirstRecord=(field && field.DoNotEditFirstRecord == "true" && findex == 0)||(cat && cat.DoNotEditFirstRecord == "true" && findex == 0);
        var noEdit = ( doNotAllowEditExistingRecords == 'true' || doNotAllowEditExistingRecords == true ) && isExistingAuxDetail( activeParcel, sourceTable, findex )
        noEdit ? $(ielem).attr('readonly', 'readonly'):''; 
       
	
        $(ielem).attr('fvalue', value); 
    
            
        if ((field.InputType == 5) /*&& (field.LookupTable == '$QUERY$')*/) {
            LFields.push(field.Id);
            getLookupData(field, { source: dscopy }, function (ld) {
            	//ld.forEach(function(x,index){ 
				///	if(x.Id) ld[index].Id = encodeURI(x.Id);
				//});
                //$(ielem).html('<option value="${Id}">${Name}</option>');
                //$(ielem).fillTemplate(ld);
                // var lookupValueCount=clientSettings.LookUpOptionsCount || 25  
    			//var donotEditFirstRecord=(field && field.DoNotEditFirstRecord == "true" && findex == 0)||(cat && cat.DoNotEditFirstRecord == "true" && findex == 0);
               // if((ld.length > lookupValueCount && $(ielem).attr('readonly') != 'readonly') && !noEdit && !donotEditFirstRecord && (ds && !ds.CC_Deleted) ) $(ielem).siblings('.customddlspan').css('display','inline-block');
               // else  $(ielem).siblings('.customddlspan').hide();

                if (ld?.length < 20) {
                    let ndt = QC.dataTypes[parseInt(field.InputType)];
                    ld.forEach((dk) => {
                        dk.Id = getLargeDecToRound(dk.Id, ndt); dk.Name = getLargeDecToRound(dk.Name, ndt);
                    });
                }

                $(ielem)[0]?.getLookup?.setData(ld);
                var curRlnParent = activeParcelEdits.length ? (activeParcelEdits.filter(function (n) { return (n.FieldId == relationParent && n.action == "edit") })).length == 0 : false;               
                if (field.DoNotAllowSelectNull == true || field.DoNotAllowSelectNull == 'true'){
                    let lp = $(ielem)[0]?.getLookup;
                    if (lp && lp.rows) {
                        for (var k of Object.keys(lp.rows)) {
                            if (lp.rows[k].Name == "") {
                                lp.rows[k]._disabled = true;
                                break;
                            }
                        }
                    }
                }
                refreshRelatedFields(field.Id, auxrowid, block,true);
                if (field)
                {
                    let lp = $(ielem)[0]?.getLookup;
                    if (lp && lp.rows) {
                        let rows = { ...lp.rows }, isExists = false;
                        for (var k of Object.keys(rows)) {
                            if (value == __decodeURI(rows[k]?.Id)) {
                                isExists = true;
                                break;
                            }
                        }
                        if (!isExists) {
                            lp.addNonExist(value);
                        }
                    }
                }
                var dispVal = ld.filter(function(x) {return x.Id == value;} )[0];
                if (dispVal && dispVal.Name) { $(ielem).html(dispVal.Name); }
                else { $(ielem).html(value); }
                $(ielem).val(typeof value == 'number' ? value : encodeURI(value));
                if((fieldIndex || fieldIndex === 0 ) && callback) callback(fieldIndex);
                autoSelectLookup(ld);
                LFields.splice(LFields.indexOf(field.Id), 1);
                recordSwitching.swichingFn()
            });
        }
        relationParent = null;
        $('option[nonexist]', $(ielem)).remove();
        function nonexist() {
            $('option[nonexist]', $(ielem)).remove();
            var nonExistOption = document.createElement('option');
            $(nonExistOption).attr({ 'nonexist': '', 'value':  encodeURI(value) });
            $(nonExistOption).html(value);
            // $(ielem).append(nonExistOption);
        }
        ((field.InputType == 5 && (roByexp || (cat_roByexp && !getReadOnlyExpressionOverrideVal(field)) || $(ielem).attr('readonly') == 'readonly')) || (field.InputType == 3 && $(ielem).attr('readonly') == 'readonly')) ? $(ielem).attr("disabled", true) : $(ielem).removeAttr("disabled");
        if(ds && ds.CC_Deleted == true) {
        	if(parseInt(field.InputType) == 13)
        		$('input', ielem).attr({ 'recovery': 'recovery', 'disabled': 'disabled'});	
        	else {
        		$(ielem).attr({ 'recovery': 'recovery', 'disabled': 'disabled' });
        		$(ielem).siblings('.cusDpdown-arrow').attr({ 'disabled': 'disabled' });
        		if (parseInt(field.InputType) == 12) $(ielem).siblings('.LoadGeolocationIcon').hide();	
        	}
        }
        if (cat && cat.ClassCalculatorParameters && cat.ClassCalculatorParameters.split(',')[0] == field.Name && field.InputType == 5) {
            //$(ielem).find('option').remove().end().append('<option value="' + value + '">' + value + '</option>');
            $(ielem).html(value);
            $(ielem).val(value);
            $(ielem).attr("disabled", true);
            $(ielem).siblings('.cusDpdown-arrow').attr("disabled", true);
        }
        if ($(ielem).attr('type') == 'number' && value.length){ 
        //value = parseFloat(value.replace(/,/i,''));
        	value = value.replace(/,/i,'');
        }
        
    	if (!__DTR && localStorage && localStorage.internalSupportView == '1' && localStorage.parcelDataSupportView == '1' && ($(ielem).hasClass('alwaysHideFieldProp') || $(ielem).hasClass('hideFieldProp'))) {
    		$(ielem).attr('disabled', 'disabled'); 
    	}
        
       	if (field && field.InputType == 3 && value != 'true' && value !='false' && (field.RadioFieldValues == '' ||!field.RadioFieldValues)){
       		nonexist()
        } 
        if(field && field.InputType == 5 )
        	$(ielem).val(encodeURI(value));
        else
        	$(ielem).val(value);
        if(field.InputType=="6"  && $('.aux-grid-view[catid="' + categoryID + '"]').css("display") != 'block'){
        	$('.value[fieldid="'+fieldId+'"] textarea').val(value);
        	var txt = $('.value[fieldid="'+fieldId+'"] textarea');
            autosize(txt[0]);
        } 
        if (field) {
            if (!isfOvcalc && field.CalculationExpression && (!doNotCalc || sourceTable == null)) {
               source1 = sourceTable ? sourceTable + '[' + findex + ']': null ;
                if (field.CalculationExpression.trim() != '' && field.CalculationExpression.search(childRecCalcPattern) > -1) {
                	value = calculateChildRecords(field, findex);
                	$(ielem).attr('readonly', 'readonly');  
                	$(ielem).siblings('.cusDpdown-arrow').attr('readonly', 'readonly');
					$(ielem).attr('eval-ro', '1');
					$(ielem).siblings('.customddlspan').hide();
					$(ielem).parent().removeClass('unsaved');
					$('.check',$('.qc[fieldid="' +fieldId+ '"]')).show();					
            	}
            	else value = activeParcel.EvalValue(source1, field.Name);
            	$(ielem).parent().removeClass('unsaved');
            	
            	 //***********************************************// FD_15106 -changed by vishnu
    			var currentValue = null; var fname = field.Name;
    			var auxRowIDCC = ((dscopy.ROWUID == 0) ? null : dscopy.ROWUID) ;
    
    			try {
				    if(activeParcel && activeParcel.ParcelChanges)
				    {
				    	 
				        var curPChanges = activeParcel.ParcelChanges.filter(function (pc) { return (pc.AuxROWUID == auxRowIDCC &&  pc.FieldId == fieldId)})
					    if(curPChanges && curPChanges.length > 0)
					    {
					       var ch = curPChanges[curPChanges.length-1];
					       currentValue = ch.NewValue;
					    }
					    else if (sourceTable)
					    {
					        var curPOrg = activeParcel.Original[sourceTable].filter(function(p){return p.ROWUID == auxRowIDCC });
				 	        if(curPOrg && curPOrg.length > 0)
							{
				    			currentValue = curPOrg[0][field.Name];
							}
					    }
					    else
					    {
					      currentValue = activeParcel.Original[field.Name];
					    }
				    }
				    
				     var _currentRecValue = ( sourceTable ? ( activeParcel[sourceTable].filter(function(ind) { return ind.ROWUID == auxRowIDCC })[0]? ( activeParcel[sourceTable].filter(function(ind) { return ind.ROWUID == auxRowIDCC })[0][fname] ): null  ): activeParcel[fname] );
	   				 var _currentRecValueMatches =  ( _currentRecValue == value || ( _currentRecValue == null  && value == "" ) ) ? true: false; 
	   				 var _currentValueMatches = false;
	
					if ( ( currentValue == value ) || ( currentValue === "" && value == null ) ) {
					    	$( 'td.value[fieldid="' + fieldId + '"]' ).removeClass( 'unsaved' );
					    	_currentRecValueMatches = true;
					    	_currentValueMatches = true;
					    } 
					    
					if (!_currentRecValueMatches || !_currentValueMatches) 
	    				$( 'td.value[fieldid="' + fieldId + '"]' ).addClass( 'unsaved' );
	    				
	    			}
				catch (e) {
					console.error(e);
				}
				//***********************************************// FD_15106 -changed by vishnu
            	
            	//$('.check',$('.qc[fieldid="' +fieldId+ '"]')).hide();
            	
            	
            	if( ( ((sourceTable) && (value != activeParcel[sourceTable][findex][field.Name])) || ((!sourceTable) && (value != activeParcel[field.Name])) ) || ( value != undefined && ( value.toString().replace(/\,/g,'') != $(ielem).val().replace(/\,/g,'') )  ) ) { //9245 , special case added.issue occur if number field value change to empty value uppdate in field 0 then value changed to 0.In this case value matched but not in if case . In this case update with existing qcvalue.
            	
            	 	if(value != undefined && !(value.toString().indexOf(',')>-1) && value != ''){
		 	        	if (isFieldId) {
			 	        	if (isReadOnly(x)) value = formatThis(0, x, value);
		 	        	}
                    	else if (isReadOnlyFieldName(x,field.Id)) if(value) value = formatThis(1, x, value, fieldId)
                	}
                	
                	if ( $(ielem).attr('type') == 'number' && value && value.length )  //numer type clears  value if it's conatain coma
        				value = value.replace(/,/i,'');
                	
					//value = isFieldId ? formatThis(0, x, value) : formatThis(1, x, value);
                	if (value != undefined ) {
                    	var _noPciGen =  (value.toString().replace(/\,/g,'') == $(ielem).val().replace(/\,/g,''))? true: false;
                    	$(ielem).val(value);
                		var delX = -1;
                		if (!_noPciGen) {
	    					for ( x in activeParcelEdits ) {
	                        	var ce = activeParcelEdits[x];
	                        	var rowId= sourceTable ? activeParcel[sourceTable][findex].ROWUID : 0;
	        					if (ce.ParcelId == activeParcel.Id && ce.AuxROWUID == rowId && ce.FieldId == fieldId) delX = x;
	                    	}
                    	}
                    	if ( delX > -1 ) activeParcelEdits.splice( delX, 1 );
                    	var newVal = null;
                    	if (field.CalculationExpression.trim() != '' && field.CalculationExpression.search(childRecCalcPattern) > -1) {
                    		newVal = calculateChildRecords(field, findex);
                    		field.ReadOnly = true;
                		}
                		else newVal = activeParcel.EvalValue(source1, field.Name);
	 					if (!_noPciGen)
	 						activeParcelEdits.push( { AuxROWUID: sourceTable ? activeParcel[sourceTable][findex].ROWUID : 0, ChangeId: null, FieldId: fieldId, ParcelId: activeParcel.Id, QCValue: newVal, action: "edit", parentROWUID: sourceTable ? activeParcel[sourceTable][findex].ParentROWUID : null } )
						if(sourceTable)
              				activeParcel[sourceTable][findex][field.Name] = newVal;
						else
							activeParcel[field.Name] = newVal;
              			//$(ielem).parent().addClass('unsaved');
            		}
            	
            	}
          	 	   
            }

            // FD_15106 -commented by vishnu
            //activeParcelEdits.filter(x => {
			//	if (x.FieldId == field.Id && x.AuxROWUID == dscopy.ROWUID)
			//		$(ielem).parent('td').addClass('unsaved');
			//});
        }

        if ($(ielem)[0] && $(ielem).attr('customWidth')) {
            $(ielem).removeAttr('customWidth');
            $(ielem).css('width', '');
        }
        if (field.InputType == 5) {
            recalculateCustomDropdownTextWidth(ielem, block);
        }
        //if(field.InputType == 5 && field.LookupTable != '$QUERY$')
        	//autoSelectLookup();
    }
    
    if((cat && cat.ClassCalculatorParameters && cat.ClassCalculatorParameters.split(',')[0] == field.Name) || $(ielem).attr('eval-ro') == 1|| $(ielem).attr('disabled') == 1)
    	$(ielem).siblings('.customddlspan').hide();
    if(!(field && (fieldIndex || fieldIndex === 0 ) && (field.InputType == 5) && (field.LookupTable == '$QUERY$'))){
    	if (callback) callback(fieldIndex);
    }

}
function customFormatValue(val, fieldId) {
    var fvalue, yr, m, d, dt, fmt;
    fmt = (fieldId != null || fieldId != undefined) ? checkForCustomFormat(fieldId) : 'YYYYMMDD';
    if (val == '' || val == null || val == undefined) return val;
    if (fmt != '') {
        let v = ''; if (typeof (fmt) == 'object') { v = fmt.Value; fmt = fmt.Name; }
        switch (fmt) {
            case 'YYYYMMDD':
                fvalue = val.toString().trim();
                if(fvalue.length == 8){
                	if (val && val != null) {
						yr = fvalue.substring(0, 4);
						m = fvalue.substring(4, 6);
						d = fvalue.substring(6, 8);
						dt = new Date(yr, m - 1, d);
                	}
                }
                else if(fvalue.length >8){
	                if (val && val != null) {
	                    yr = fvalue.substring(0, 4);
	                    m = fvalue.substring(5, 7);
	                    d = fvalue.substring(8, 10);
	                    dt = new Date(yr, m - 1, d);
	                }
	            }
                else {
                	return fvalue;
                }
                var dtime = dt.getFullYear() + '-' + (dt.getMonth() + 1).toString().padLeft(2, '0') + '-' + dt.getDate().toString().padLeft(2, '0');
                return dtime;
            case 'Factor':
                if (v != '' && !isNaN(parseFloat(v))) {
                    let dc = (v.toString().indexOf('.') > -1) ? v.toString().split('.')[1].length : 0;
                    val = parseFloat(val) * parseFloat(v);
                    if (dc && val.toString().indexOf('.') > -1 && (dc != val.toString().split('.')[1].length)) val = val.toFixed(dc);
                }
                return val;
            default:
                return val;
        }
    }
    else
        return val;
}
function checkForCustomFormat(fieldid) {
    var fmt;
    if (datafields[fieldid].UIProperties)
        var o = JSON.parse(datafields[fieldid].UIProperties.replace(/'/g, '"'));
    else
        return '';
    if (o.CustomFormat && o.CustomFormat != '')
        fmt = o.CustomFormat;
    return ((fmt && fmt != '') ? fmt : '');
}

function deFormatvalue(value, fieldid) {
    var fmt;
    if (value == "" || value == null) return value;
    fmt = checkForCustomFormat(fieldid);

    if (fmt != '') {
        let v = ''; if (typeof (fmt) == 'object') { v = fmt.Value; fmt = fmt.Name; }
        switch (fmt) {
            case 'YYYYMMDD':
                val = value.split(" ")[0].split("-");
                return (val[0] + val[1] + val[2]);
            case 'Factor':
                if (v != '' && !isNaN(parseFloat(v))) {
                    value = parseFloat(value) / parseFloat(v);
                }
                if (datafields[fieldid]?.InputType == '8') value = Math.round(value);
                if (value.toString().indexOf('.') > -1 && (value.toString().split('.')[1].length > 4)) value = value.toFixed(4);
                return value;
            default:
                return value;
        }
    }
    else
        return value;
}

function refreshRelatedFields(fieldId, auxRowId, block,autoselectCall, allowTextInCall) {
    if (relationcycle.indexOf(fieldId) > -1) {
        console.log('realational field cycling'); return false;
    }
    
    var rfields = _.clone(ccma.UI.RelatedFields[fieldId]);
    if ((rfields == undefined) || (rfields == null)) {
        return false;
    }
    relationcycle.push(parseInt(fieldId));
	rfields = rfields.filter(function(item) { if (relationcycle.indexOf(item) == -1) return item;});

    for (var x in rfields) {
        var sfield = datafields[rfields[x]];
        var el = $('.parcel-field-values .value[fieldid="' + sfield.Id + '"] .input', block);
		if (sfield.CalculationExpression && sfield.CalculationExpression.search(childRecCalcPattern) > -1) {
			var tempBlock = $('td.value[fieldid="' + sfield.Id + '"]').parents('.category-page')
			el = $('.parcel-field-values .value[fieldid="' + sfield.Id + '"] .input');
        }
        //CC_3969
        if (document.getElementById($(el).attr('id')) && !document.getElementById($(el).attr('id')).classList.contains("cusDpdownSpan")) {
            var value = document.getElementById($(el).attr('id')).value;
        } else {
            var value = $(el).attr('fvalue');
        }
        //var value = $(el).attr('fvalue');
        if(autoselectCall) autoselectloop.push(sfield.Id)
        setFieldValue(value, sfield.Id, $(el).parents('.category-page'), function () {
            if (el.length == 0) {
                recordSwitching.swichingFn(sfield);
                refreshRelatedFields(sfield, auxRowId, block,true);
                //                selectOwnValue(value, sfield, auxRowId, function () {
                //                    // refreshRelatedFields(sfield, saveMethod);
                //                });
            }else{
                recordSwitching.swichingFn(sfield);
            }
        }, true, fieldId, null, null, allowTextInCall);
    }
    initRadioButtonChangeTracker();
}

function calcRelatedFields(fieldId, block) {    
    var rfields = _.clone(ccma.UI.CalcRelatedFields[fieldId]);
    if ((rfields == undefined) || (rfields == null)) {
        return false;
    }
	relationcycle.push(parseInt(fieldId));
	rfields.forEach(function(rf, i){ if (relationcycle.indexOf(rf) > -1) rfields.splice(i,1); });

    for (var x in rfields) {
        var sfield = datafields[rfields[x]];
        var el = $('.parcel-field-values .value[fieldid="' + sfield.Id + '"] .input', block);
		if (sfield.CalculationExpression && sfield.CalculationExpression.search(childRecCalcPattern) > -1) {			
			el = $('.parcel-field-values .value[fieldid="' + sfield.Id + '"] .input');
		}
        var value = $(el).attr('fvalue');
        setFieldValue(value, sfield.Id, $(el).parents('.category-page'), function () {
            if (el.length == 0) {
                calcRelatedFields(sfield, block);
                //                selectOwnValue(value, sfield, auxRowId, function () {
                //                    // refreshRelatedFields(sfield, saveMethod);
                //                });
            }
        }, true, fieldId);
    }
}

function getFilteredIndexes(NodeFilter, filter_name, filterValue, parentROWUID,categoryID) {
    try {

        if (NodeFilter == undefined) {
            return eval('activeParcel.' + NodeFilter);
        }
        var filterCondition = '';
        var iter_value = 0;
        while (iter_value < filter_name.length) {
            filterCondition += 'd.' + filter_name[iter_value] + ' == ' + (filterValue[iter_value] == null ? 'null' : '"' + filterValue[iter_value] + '"');
            iter_value += 1;
            if (iter_value < filter_name.length) {
                filterCondition += '&&';
            }
        }
        var category= getCategory(categoryID);
        var parCategory=getCategory(category.ParentCategoryId);
		var fiteredSource = [];
        if(NodeFilter == (parCategory ? parCategory.SourceTable:null))
           fiteredSource=eval('activeParcel.' + NodeFilter +  '.map(function (d, i) { return ((' + filterCondition + '))? i : -1; }).filter(function(x) {return x > -1});');
       	else
            fiteredSource=eval('activeParcel.' + NodeFilter + '.map(function (d, i) { return ((!d.ParentROWUID &&(' + filterCondition + ')) || (d.ParentROWUID && d.ParentROWUID == ' + parentROWUID + ')) ? i : -1; }).filter(function(x) {return x > -1});')
       // var fiteredSource = eval('activeParcel.' + NodeFilter + '.map(function (d, i) { return ((!d.ParentROWUID &&(' + filterCondition + ')) || (d.ParentROWUID && d.ParentROWUID == ' + parentROWUID + ')) ? i : -1; }).filter(function(x) {return x > -1});');
        return fiteredSource;

    }
    catch (e) {
        console.error(e);
    }
}

function updateParcelDashboard() {


    var parcelPriorityVal = activeParcelPriority.length === 0 ? activeParcel.Priority : activeParcelPriority[0];
    $('.parcel-priority').val(parcelPriorityVal);
    if (activeParcel.QCDate == null || activeParcel.QCDate == undefined) {
        $(".lastReviewedMain").hide();
        $('.lastReviewedValue').html('');
    }
    else {
        $(".lastReviewedMain").show();
        $('.lastReviewedValue').html(getReviewDate());
    }
    if (activeParcel.CautionFlag == 0 || activeParcel.CautionFlag == undefined ) {
        $(".cautionMessageMain").hide();
        $('.caution-Message').html('');
    }
    else if((clientSettings['DefaultCaution']) && (activeParcel.CautionFlag==1) &&( !activeParcel.CautionMessage))
          
         { 
          $(".cautionMessageMain").show();
          $(".caution-Message").html((clientSettings['DefaultCaution']));
         }
    else {
        $(".cautionMessageMain").show();
        $(".caution-Message").html(activeParcel.CautionMessage)
    }
    if (activeParcel.FailedOnDownSync == 0 || activeParcel.FailedOnDownSync == undefined) {
        $(".parcelRejectMain").hide();
        $('.parcelRejectReason').html('');
    }
    else {
        $(".parcelRejectMain").show();
        $(".parcelRejectReason").html(activeParcel.DownSyncRejectReason)
    }
    if (activeParcel.CC_Error && activeParcel.CC_Error != '') {
        $('.CC_error_message').html(activeParcel.CC_Error.split('||')[0]);
        $(".CC_error_div").show();
    }
    else {
        $('.CC_error_message').html('');
        $(".CC_error_div").hide();
    }

    $('.work-flow-div').empty();
    if (clientSettings?.DTRWorkflowNew == '1' && activeParcel.WorkFlowTypes?.length > 0 && offlineTables?.ParcelWorkFlow) {
        let htm = "<span style='width: 122px; display:block; vertical-align: top; margin-top: 6px; margin-bottom: 2px'>WorkFlow Types:</span>";
        activeParcel.WorkFlowTypes.forEach((w) => {
            let wft = offlineTables.ParcelWorkFlow.find((x) => x.Code === w.code);
            let clr = wft?.colorCode ? wft.colorCode : 'black';
            htm += `<span class="wf-span" title="${w.WorkFlowType}" style="position: relative; display: inline-flex; align-items: center; font-weight: bold; border: 2px solid darkviolet; ${clr}; margin-right: 10px; background-color:darkviolet; color:white; border-radius:5px;">
            <span style="flex-grow: 1; padding-right: 20px;">
                ${w.WorkFlowType}
            </span>
            <span class="WrkFlwclose-btn" style="cursor: pointer;/* padding: 3px; background-color: red;*/ color: white; font-weight: bold; /*border-left: 1px solid*/ ${clr};" title="Close" data-code="${w.code}" onclick="WorkflwCloseBtn(this)">X</span>
            </span>`;
        });
        $('.work-flow-div').html(htm);
        $('.work-flow-div').show();
    }

    var alertData = activeParcelAlert.length === 0 ? activeParcel.ParcelAlert : activeParcelAlert[0];
    $('.parcel-alert-message').val(htmlDecode(alertData));
    $('.selected-estimate').val(activeParcel.SelectedAppraisalType);
    $('.dtr-selected-status').val(activeParcel.DTRStatus);
    $('.field-alert-type').html(activeParcel.FieldAlertName);
    $('.field-alert-notes').html(activeParcel.FieldAlert);
    if ($('.nav-parcel-prev').is(":disabled") == true && $('.nav-parcel-next').is(":disabled") == true) {
        $('.parcel-result-count').html("< 1 of 1 >");
    }
    else {
        var searchStatus = getSearchStatus({ ParcelId: activeParcel.Id });
        if (searchStatus.page == 1) {
            var parcelindex = searchStatus.parcelIndex + 1;
        }
        else {
            var parcelindex = (searchStatus.pageSize * (searchStatus.page - 1)) + searchStatus.parcelIndex + 1;
        }
        $('.parcel-result-count').html("< " + parcelindex + " of " + searchStatus.total + " >");
    }
    if (activeParcel.Reviewed) {
        $('.ReviewSet').text("Set As Not Reviewed");
    }
    else {
        $('.ReviewSet').text("Set As Reviewed");
    }
    $('.field-alert').val(activeParcel.FieldAlert);
    
    if (clientSettings?.EnableNewPriorities == "1") {
        if ((activeParcel.Priority && ['1', '2', '3', '4', '5'].includes(activeParcel.Priority.toString())) || (parcelPriorityVal && ['1', '2', '3', '4', '5'].includes(parcelPriorityVal.toString()))) {
            $('.parcel-alert-message-row').show();
        } else {
            $('.parcel-alert-message-row').hide();
        }
    }
    else {
        if ((activeParcel.Priority == 2) || (activeParcel.Priority == 1) || (parcelPriorityVal == '2') || (parcelPriorityVal == '1')) {
            $('.parcel-alert-message-row').show();
        } else {
            $('.parcel-alert-message-row').hide();
        }
    }

    var qc = activeParcel.QC == null ? -1 : activeParcel.QC ? 1 : 0;
    $('.parcel-qc-pad button[selected]').removeAttr('selected');
    $('.parcel-qc-pad button[qc="' + qc + '"]').attr('selected', 'selected');
    if ((clientSettings['EnableMALite'] == '1' || clientSettings['EnableMALite'] == 'True' || clientSettings['EnableMALite'] == 'true') && (qc== 1) )
           {
               $('.Commit_Change').show()
           }
           else
           {
               $('.Commit_Change').hide()
           }  
 
    //$('.hideKey').html(activeParcel.StreetAddress);
    //if(activeParcel.ShowAlternateField) $('.hideAdd').html(activeParcel.Alternatekeyfieldvalue);
    //$('.hideAdd').html(activeParcel.KeyValue1);
    

    for (x in activeParcel) {
        var field = activeParcel[x];
        if (field == null) {
            $('.parcel-head-values .value[fieldname="' + x + '"]').html('');
        } else if (typeof field != "object") {
            $('.parcel-head-values .value[fieldname="' + x + '"]').html(field);
        }
    }


for (x in activeParcel) {
        var field = activeParcel[x];
        if (field == null) {
            $('.hidedheader .value1[fieldname="' + x + '"]').html('');
        } else if (typeof field != "object") {
            $('.hidedheader .value1[fieldname="' + x + '"]').html(field);
        }
    }
}

function WorkflwCloseBtn(element) {
    if (AtcWorkflowrole === true || AtcWorkflowrole === 'true') {
        var code = $(element).attr('data-code');
        var confirmResult = confirm('Are you sure you want to remove this Workflow?');
        if (confirmResult) {
            dashboardChanged = true;
            var workflowToDelete = { code: code };
            deletedWorkflow.push(workflowToDelete);
            $(element).closest('.wf-span').remove();
        }
    } else {
        alert("You don't have the permission to delete a WorkFlow.");
    }
}


//Refreshing/Load Values and Tabs
function getParcelDataUpdates(forSkh, qcAction, _updatePhotosOnApprove) {

    $qc('parcelupdates', { ParcelId: activeParcel.Id, __dtr: __DTR }, function (data) {
        if (data.status != 'OK') {
            alert('An error occured while refreshing parcel data - ' + data.message);
            hideMask();
            return false;
        }
        dashboardChanged = false;
        for (var x in data) {
            if (x != 'status') {
                activeParcel[x] = data[x];
            }
        }
    	
    	if(qcAction == 'review') applyParcelChanges();
    	
        updateParcelDashboard();

        $('span.qc a').hide();
        $('.parcel-field-values td.value').removeClass('unsaved');
        $('.parcel-field-values .input').attr('i', 'i');
        for (x in activeParcel.ParcelChanges) {
            var ch = activeParcel.ParcelChanges[x];
            if (ch.AuxROWUID == null && $('.parcel-field-values .value[fieldid="' + ch.FieldId + '"] .input').attr('readonly') != 'readonly' ) {
                var qc = $('.qc[fieldid="' + ch.FieldId + '"]'); 
                $('a', qc).hide();
                $((activeParcel.QC && ch.QCChecked) ? '.passed' : '.check', qc).show();
                $((activeParcel.QC && ch.QCChecked) ? '.passed' : '.check', qc).attr('changeid', ch.Id);
                var value=ch.NewValue;         
                if(datafields[ch.FieldId] && datafields[ch.FieldId].InputType=='4') value = dateTolocal(value);
                $('.parcel-field-values td.value[fieldid="' + ch.FieldId + '"] .input').val(value);
            }
        }
        
        if ( _updatePhotosOnApprove || clientSettings?.PhotoDeleteAndRecover == "1" ) {
            activeParcel.Images = activeParcel.Images.filter((x) => { return !activeParcel.QC || (activeParcel.QC && !x.CC_Deleted) });
	        activeParcel.Photos = [];
			for (x in activeParcel.Images) {
				var img = activeParcel.Images[x];
				if (!img.IsSketch) 
					activeParcel.Photos.push(copyDataObject(img));
			}
			if(_updatePhotosOnApprove) {
                removeExceededPhotos();
                showPhotoThumbnail();
            }
		    if( $('.tabs-main li.selected').attr('category') == 'photo-viewer' )
		    	showAllPhotos();
	    }
	    
        if (activeParcel.QC == 1) {
            var clsFCE = 'field-category-edited';
            $('.field-category-link').removeClass(clsFCE);
            //$('.field-category-link').addClass('approved');
        }
        
        if(qcAction && isBPPParcel){ //position changed to set parcel deleted or recover status before loading field.
        	bppParcelBanner(qcAction)
			if(qcAction == "1" && (activeParcel.CC_Deleted== true || activeParcel.CC_Deleted=='true')){
				activeParcel.Photos = [];
				showAllPhotos();
				showPhotoThumbnail();
			}
			if (qcAction == "-2" || qcAction == "-3"){
				for (x in activeParcel) {
		            	var field = activeParcel[x];
		            	var block = getDataField(x, null) ? $('.category-page[categoryid="' + getDataField(x, null).CategoryId + '"]') : null;
		            	setFieldValue(field, x, block,null,null,null,true);
	        		}
	        		if($('.tabs-main li.selected').attr('category') == 'photo-viewer'){
                    		if(activeParcel.CC_Deleted== false || activeParcel.CC_Deleted=='false') $('.parcel-data-title').html('Parcel Photos')
                			showAllPhotos();
               		}
	        }
        }
        highlightCatLink(); 
        $('.category-page[auxdata]').each(function () {
            showAuxData(this, 0,null,null,null,null,true);
        });
        if (activeParcel.Reviewed) {
            $('.ReviewSet').text("Set As Not Reviewed");
        }
        else {
            $('.ReviewSet').text("Set As Reviewed");
        }
        
        updateRecentAuditTrail();
        if(!forSkh)
        	getSketchSegments();
        //highlightCatLink();      position changed to top.
        if (Notificationmsg && Notificationmsg != "") {
            notification(Notificationmsg);
            Notificationmsg = "";
        }
		setTimeout(function(){hideMask()},50);
    });
}

function refreshPriorityValue() {
    $qc('getdetailsonreject', { ParcelId: activeParcel.Id }, function (data) {
        priorityvalues = data.ParcelChanges;
        //To change priority field on reject
        $('.parcel-priority').val(priorityvalues.Priority);
        $('.parcel-alert-message').val(priorityvalues.ParcelAlert);

        if (clientSettings?.EnableNewPriorities == "1") {
            if (priorityvalues.Priority && ['1', '2', '3', '4', '5'].includes(priorityvalues.Priority.toString())) {
                $('.parcel-alert-message-row').show();
            } else {
                $('.parcel-alert-message-row').hide();
            }
        }
        else {
            if ((priorityvalues.Priority == 2) || (priorityvalues.Priority == 1)) {
                $('.parcel-alert-message-row').show();
            } else {
                $('.parcel-alert-message-row').hide();
            }
        }
    });
}

function refreshAuxDataChanges(block, index) {
    var sourceTable = $(block).attr('auxdata');
    var source = eval('activeParcel.' + sourceTable);

    var count = source.length;

    if (source.length > 0) {
        for (x in activeParcel.ParcelChanges) {
            var ch = activeParcel.ParcelChanges[x];
            if (ch.AuxROWUID != null) {
                if (ch.AuxROWUID == first.ROWUID) {
                    var qc = $('.qc[fieldid="' + ch.FieldId + '"]');
                    $((activeParcel.QC && ch.QCChecked) ? '.passed' : '.check', qc).show();
                    $((activeParcel.QC && ch.QCChecked) ? '.passed' : '.check', qc).attr('changeid', ch.Id);
                    if (!ch.QCChecked) {
                        $('.parcel-field-values td.value[fieldid="' + ch.FieldId + '"] .input', block).val(ch.NewValue);
                    }
                }
            }
        }
    }
}

function updateRecentAuditTrail() {
    try {
        var filteredSource = activeParcel.AuditTrail;
        if (filteredSource.length == 0) {
            $('.recent-audit-trail').hide();
            $('.recent-audit-trail table tbody,.recent-audit-trail-main table tbody').html('');
            return false;
        }

        var rat = [];
        var ratMain = [];
        var li = filteredSource.length - 1;
        var ratc = 0;
        while (li >= 0) {
            var at = filteredSource[li];
            //var auditTime = at.EventTime.utcStringToLocalTime();
            var auditTime = new Date(at.LocalEventTime)
            var cdate = auditTime.localeFormat("d");
            var ctime = auditTime.localeFormat("t");
            var today = (new Date()).localeFormat("d");
            var edate = cdate;
            if (cdate == today) {
                edate = ctime;
            }
            var shortDesc = at.Description, LongDesc = at.Description;
            if ( at.Description && at.Description.length > 200 ){
                var aux = / #[0-9]+.$/.exec( at.Description ); 
                LongDesc = at.Description.substring( 0, 150 ) +"..."+ ( aux ? " for Aux Record " + aux : '' );
                shortDesc = at.Description.substring( 0, 100 ) + "..." + ( aux ? " for Aux Record " + aux : '' );
            }
            var atx = {
                AuditTime: edate,
                AuditUser: at.LoginID ? at.LoginID.replace('API', 'SyncService/API') : '',
                Description: shortDesc ? shortDesc.replace( 'API', 'SyncService/API' ) : '',
                CorrespondingChangeId: (at.CorrespondingChangeId == null) ? 'none' : at.CorrespondingChangeId
            };
            var atxMain = {
                AuditTime: at.LocalEventTime,
                AuditUser: at.LoginID ? at.LoginID.replace('API', 'SyncService/API') : '',
                Description: LongDesc ? LongDesc.replace( 'API', 'SyncService/API' ) : '',
                CorrespondingChangeId: (at.CorrespondingChangeId == null) ? 'none' : at.CorrespondingChangeId
            };

            rat.push(atxMain);
            if (rat.length < 10) {
                ratMain.push(atx);
            }
            if (rat.length > 74) {
                break;
            }

            li--;
        }

        $('.recent-audit-trail').show();
        //var InnerHtml = $('.recent-audit-trail-main table tbody').html();
        $('.recent-audit-trail table tbody').html($('.recent-audit-trail table tfoot').html());
        $('.recent-audit-trail-main table tbody').html($('.recent-audit-trail-main table tfoot').html());
        $('.recent-audit-trail-main table tbody').fillTemplate(rat);
        $('.recent-audit-trail table tbody').fillTemplate(ratMain);

        if (activeParcel.countOfAudit > 0 && !__DTR) {
            let nr = '<tr id="viewOldReportRow"><td colspan="3" style="text-align: center; font-size: 15px;"><a href="/protected/reports/Default.aspx?oldReportPId=' + activeParcel.Id + '&plKeyVal=' + $('#hdnKeyValue1').val() + '&plAltVal=' + $('#hdnAlternate').val() + '" class="viewOldReport" onclick="showMask()">Click here to view older Audit Trail data</a></td></tr>';
            $('.recent-audit-trail-main tbody').append(nr);
        }

        //$('.recent-audit-trail-main table tbody').prepend(InnerHtml);
    }
    catch (e) {
        console.log('Error on updateRecentAuditTrail >> ' + e.message);
    }
}

function getLatestParcelChanges() {
    var h = [], k = [], l = [], m = [], n = [], p = [], fieldIds = [], hasFieldID, fieldLenght, AuxLenght, iterValue = 0, iterValueAux = 0, AuxRowIds, isAuxNull = false;
    //to get field ids of each active parcel
    fieldIds = activeParcel.ParcelChanges.map(function (d, i) { return d.FieldId; });
    //Make each field distinct
    fieldIds = fieldIds.filter(function (v, i) { return fieldIds.indexOf(v) == i; });
    fieldLenght = fieldIds.length;
    while (iterValue < fieldLenght) {
        k = activeParcel.ParcelChanges.filter(function (d, i) { return d.FieldId == fieldIds[iterValue]; });
        isAuxNull = k.some(function (d, i) { return d.AuxROWUID == null; });
        if (isAuxNull) {

            k = k.reduce(function (a, b) {
                return (a.UpdatedTime > b.UpdatedTime) ? a : b;
            });
            l = { FieldId: fieldIds[iterValue], Fieldchanges: k }
            k = [];

        }
        else {
            AuxRowIds = k.map(function (d, i) { return d.AuxROWUID; });
            AuxRowIds = AuxRowIds.filter(function (v, i) { return AuxRowIds.indexOf(v) == i; });
            AuxLenght = AuxRowIds.length;
            iterValueAux = 0
            while (iterValueAux < AuxLenght) {
                m = k.filter(function (d, i) { return d.AuxROWUID == AuxRowIds[iterValueAux]; });
                m = m.reduce(function (a, b) {
                    return (a.UpdatedTime > b.UpdatedTime) ? a : b;
                });
                n = { AuxROWId: AuxRowIds[iterValueAux], Auxchanges: m }
                m = [];
                p.push(n);
                iterValueAux += 1;
            }
            l = { FieldId: fieldIds[iterValue], Fieldchanges: p }
            p = [];
        }

        h.push(l);
        l = [];
        iterValue += 1;
    }
    return h;

}


// Change related UI methods
function selectChange(changeId, fieldId, auxRowId, index, useOriginal) {
       var cat = getCategory(datafields[fieldId]?.CategoryId);
   var fName = datafields[fieldId]?.Name;
   var DoNotEditFirst=datafields[fieldId]?.DoNotEditFirstRecord;
   var sourceTable = datafields[fieldId]?.SourceTable;
   var rec = (activeParcel[sourceTable] && activeParcel[sourceTable].length > 0) ? activeParcel[sourceTable].filter(function (f) { return f.ROWUID == auxRowId }) : '';
   if((cat && cat.ClassCalculatorParameters && cat.ClassCalculatorParameters.split(',')[0] == fName)||(DoNotEditFirst == true && index == "0") ||(cat && (cat.DoNotEditFirstRecord==true||cat.DoNotEditFirstRecord=="true")&& index == "0")||(cat && cat.DoNotAllowEditExistingRecords == true && isExistingAuxDetail(activeParcel,sourceTable, index)))
 	  return false;
    var input_type = datafields[fieldId]?.InputType;
    if (rec && rec.length > 0 && rec[0].CC_Deleted == true || $('.parcel-field-values .value[fieldid="' + fieldId + '"] .input').attr('readonly') == 'readonly')
        return false;
    if (activeFieldChanges) {
        for (x in activeFieldChanges) {
            var ch = activeFieldChanges[x];
            if (ch.Id == changeId) {
                var value;
                if (useOriginal) {
                    value = (input_type == "4") ? (ch.OriginalValue ? ch.OriginalValue.slice(0, 11): null): ch.OriginalValue;
                    if (input_type == "4" && ch.OriginalValue != null && value != null) {
                        if (new Date(value) instanceof Date)
                    	{
                    		if(!(value.split('-') && value.split('-').length >1 )) {
				     		value = value.substring(0, 4)+'-'+value.substring(4, 6)+'-'+value.substring(6, 11);
				     	    }
                            var targetTime = new Date( value+' UTC' );
				 	        var timevalue = targetTime.toISOString().substring( 0, 10 );
				 	        value = timevalue;
                    	}
                    	else {
                    		alert("Selected value is not in date format.!");
                    		return false;
                    	}
				    }
                } else {
                     value = (input_type == "4") ? (ch.ChangedValue ? ch.ChangedValue.slice(0, 10): null): ch.ChangedValue;
                }
                if(value === null) value = '';
                $('.parcel-field-values .value[fieldid="' + fieldId + '"] .input').val(value);
                if(input_type == "5" && value !== null) {
                    value = encodeURI(value);
                    if($('.parcel-field-values .value[fieldid="' + fieldId + '"] .input').val() === "")
                    	$('.parcel-field-values .value[fieldid="' + fieldId + '"] .input').val(value);
                }
			if (useOriginal) {
				value = (input_type == "4" && ch.OriginalValue == null) ? "" : value;
			 }
                hideFieldAudit();
                if(datafields[fieldId].IsClassCalculatorAttribute == true)
                registerValueEdit(activeParcel.Id, auxRowId, fieldId, useOriginal ? -1 : changeId, value,"edit",null,null,1);
                else
                registerValueEdit(activeParcel.Id, auxRowId, fieldId, useOriginal ? -1 : changeId, value);
            }
        }
    }
}

function selectOwnValue(field, fieldId, auxRowId, action, callback) {
    var value = $(field).val();
    var iType = datafields[fieldId].InputType
    if(iType == "5"){
     try{
        value = decodeURI(value);
     }
     catch{  }
    }
    var changeId = 0;
    var originalValue = null;
    var action = action;
    for (x in activeParcel.ParcelChanges) {
        var ch = activeParcel.ParcelChanges[x];
        if (!ch.QCChecked)
            if (ch.FieldId == field)
                if (ch.AuxROWUID == auxRowId) {
                    if (!originalValue)
                        originalValue = ch.OriginalValue;
                    if (ch.NewValue == value)
                        changeId = ch.Id;
                }
    }
    if (value == originalValue) {
        changeId = -1;
        value = null;
    }
    if (changeId > 0) value = null;
    registerValueEdit(activeParcel.Id, auxRowId ? parseInt(auxRowId) : 0, fieldId ? parseInt(fieldId) : null, changeId, value, action, callback);


}
function registerValueEdit( parcelId, auxRowId, fieldId, changeId, qcValue, action, callback, isLookupAutoSelected, IsClassCalc,autoLoop ) {
    var field = datafields[fieldId];
    //var parentrowuid = $( '.sublevelDiv', block ).attr( "parentrowuid" )
    var catId = field.CategoryId;
    var index = 0;
    if(field.SourceTable == "_photo_meta_data"){
		var el = $('.value[fieldid="'+fieldId+'"] .input')
    	savePhotoProperty(el);
		if ( callback ) callback();
        return;
	}
	var _category = getCategory( catId );
    var sourceTable = _category.SourceTable;
    if ( sourceTable )
        for ( i = 0 ; i < activeParcel[sourceTable].length ; i++ ) {
            if ( activeParcel[sourceTable][i].ROWUID == auxRowId ){
                index = i;
                var parentrowuid = activeParcel[sourceTable][i].ParentROWUID
                }
        }
    if ( field.IsClassCalculatorAttribute == true ) {
        if ( IsClassCalc == 1 ) {
        	if(qcValue){ 
        	     try{
		     		qcValue = decodeURI(qcValue);
		     }
		     catch{  }
        	}
            activeParcelEdits.push( { AuxROWUID: auxRowId, ChangeId: null, FieldId: fieldId, ParcelId: activeParcel.Id, QCValue: qcValue, action: "edit", parentROWUID: parentrowuid } )
            activeParcel[sourceTable][index][field.Name] = qcValue;
        }
        if ( callback ) callback()
        return;
    }
    var customFormat = false;
    var fmt = '';
    if ( field.UIProperties && field.UIProperties != null )
        fmt = checkForCustomFormat( field.Id );
    if ( fmt != '' ) {
        customFormat = true;
        qcValue = deFormatvalue( qcValue, field.Id );
    }
    var originalqcValue = qcValue;
    if ( field.InputType == "4" && qcValue != '' && !customFormat ) {
        var targetTime = new Date( qcValue );
        //            var timeZoneFromDB = clientSettings["TimeZone"].replace(":", "."); //time zone value from database
        //            var tzDifference = timeZoneFromDB * 60;
        //            var offsetTime = new Date(targetTime.getTime() - tzDifference * 60 * 1000);
        var timevalue = targetTime.toISOString().substring( 0, 10 );
        qcValue = timevalue + ' 12:00:00';
    }
    //if (field.InputType == "10" && (qcValue != 0 || (qcValue == 0 && qcValue.length > 4))) {
    //    var pattern = new RegExp(/^(181[2-9]|18[2-9]\d|19\d\d|2\d{3}|30[0-3]\d|304[0-8])$/);
    //    var res = pattern.test(qcValue);
    //    if (res != true) {
    //        qcValue = '';
    //        $('.parcel-field-values .value[fieldid="' + fieldId + '"] .input').val('');
    //        alert('Not a valid year');
    //    }
    //}
    if (field.InputType == "5") {
        let el = $('td.value[fieldid="' + fieldId + '"] input.cc-drop');
        let lp = $(el)[0] && $(el)[0].getLookup;
        let value = $(el).val();
        if (!lp) return false;
        for (var k of Object.keys(lp.rows)) {
            if (lp.rows[k] && lp.rows[k].nonexist && !lp.rows[k].cinput && lp.rows[k].Name == value) {
                return false;
            }
            else if (lp.rows[k].nonexist && !lp.rows[k].cinput) delete lp.rows[k];
            else if (!lp.rows[k].cinput) lp.rows[k].cinput = false;

        }
    }
	if (field.InputType == "13"){
		$( 'td.value[fieldid="' + fieldId + '"] input' ).removeAttr('checked'); 
        $( 'td.value[fieldid="' + fieldId + '"] input[value = "'+ qcValue +'"]').attr( "checked", true );
	}
    
    //var currentValue = $( 'td.value[fieldid="' + fieldId + '"] .input' ).attr( 'fvalue' );  // FD_15106 -changed by vishnu
   
   
    
    // var currentValue = $('td.value[fieldid="' + fieldId + '"] .input').attr('fvalue')
    var delX = -1;
    for ( x in activeParcelEdits ) {
        var ce = activeParcelEdits[x];
        if ( ce.ParcelId == parcelId )
            if ( ce.AuxROWUID == auxRowId )
                if ( ce.FieldId == fieldId )
                    delX = x;
    }

    if ( delX > -1 )
        activeParcelEdits.splice( delX, 1 );
    if(field.InputType == "5" && qcValue !== null) { 
        try {
            qcValue = decodeURI(qcValue);
        } catch {}        
    }
    var block = $( 'td.value[fieldid="' + fieldId + '"]' ).parents( '.category-page' );
    var source = $( block ).attr( 'auxdata' );
    //var index = $( block ).attr( 'findex' );
    
    //***********************************************// FD_15106 -changed by vishnu
    var currentValue = null;
    
    try {
	    if(activeParcel && activeParcel.ParcelChanges)
	    {
	    	var auxRowIDCC = ((auxRowId == 0) ? null : auxRowId) ; 
	        var curPChanges = activeParcel.ParcelChanges.filter(function (pc) { return (pc.AuxROWUID == auxRowIDCC &&  pc.FieldId == fieldId)})
		    if(curPChanges && curPChanges.length > 0)
		    {
		       var ch = curPChanges[curPChanges.length-1];
		       currentValue = ch.NewValue;
		    }
		    else if (source)
		    {
		        var curPOrg = activeParcel.Original[source].filter(function(p){return p.ROWUID == auxRowIDCC });
	 	        if(curPOrg && curPOrg.length > 0)
				{
	    			currentValue = curPOrg[0][field.Name];
				}
		    }
		    else
		    {
		      currentValue = activeParcel.Original[field.Name];
		    }
	    }
    }
	catch (e) {
		console.error(e);
	}
    //***********************************************// FD_15106 -changed by vishnu
    
    //FD_9245 new functionality to check recValaue and changed value same, If same then only return otherwise execute setfieldvalue actions.But not insert PCI for this
    var fname = field.AssignedName, _currentValueMatches = false; //to check existing value matched to qcvalue, if same value choose two time recvalue match to qc value but it's exists value not matched to qc value need to show blue indication and insert to edits
    var _currentRecValue = ( source ? ( activeParcel[source].filter(function(ind) { return ind.ROWUID == auxRowId })[0]? ( activeParcel[source].filter(function(ind) { return ind.ROWUID == auxRowId })[0][fname] ): null  ): activeParcel[fname] );
    var _currentRecValueMatches =  ( _currentRecValue == qcValue || ( _currentRecValue == null  && qcValue == "" ) ) ? true: false;    
    
	source && activeParcel[source].filter(function(ind){return ind.ROWUID==auxRowId})[0] ? activeParcel[source].filter(function(ind){return ind.ROWUID==auxRowId})[0][fname] = qcValue : activeParcel[fname] = qcValue;
    if ( ( currentValue == qcValue || currentValue == originalqcValue || (currentValue === "" && qcValue == null) ) && _currentRecValueMatches ) {
        refreshRelatedFields( fieldId, auxRowId, block, autoLoop, true);
        $( 'td.value[fieldid="' + fieldId + '"]' ).removeClass( 'unsaved' );
        return;
    }
    else if ( ( currentValue == qcValue )|| (currentValue == originalqcValue) || ( currentValue === "" && qcValue == null ) || ( qcValue === "" && currentValue == null ) ) {
    	$( 'td.value[fieldid="' + fieldId + '"]' ).removeClass( 'unsaved' );
    	_currentRecValueMatches = true;
    	_currentValueMatches = true;
    } 
    if (field.DoNotAllowSelectNull == 'true' && (!qcValue || qcValue == '')) {
        $( 'td.value[fieldid="' + fieldId + '"] .input' ).val(currentValue);
        return;
    }
    
    var edit = {
        ParcelId: parcelId,
        AuxROWUID: auxRowId,
        FieldId: fieldId,
        ChangeId: changeId,
        QCValue: qcValue,
        action: action,
        parentROWUID: parentrowuid,
        isLookupAutoSelected: isLookupAutoSelected ? true : false
    };
    ''
    if( autoLoop && ccma.UI.RelatedFields[fieldId] && source && ccma.UI.RelatedFields[fieldId].length == 0 && ( field.ReadOnlyExpression || field.ReadOnly || ( _category && ( _category.ReadOnlyExpression || _category.IsReadOnly) )  ) ) {
		var readonlyexp = field.ReadOnlyExpression, roByexp, cat_roByexp;
		var cat_readonlyexp = (_category && _category.ReadOnlyExpression) ? decodeHTML(_category.ReadOnlyExpression) : null;
		var _findex = activeParcel[source].indexOf( activeParcel[source].filter( function(x) { return x.ROWUID == auxRowId } )[0] );
		if( _findex > -1 && readonlyexp ) 
			roByexp = ( readonlyexp ) ? ( ( source ) ? ( activeParcel.EvalValue(source + '[' + _findex + ']', readonlyexp) ) : ( activeParcel.EvalValue(readonlyexp) ) ) : ( ( _category && _category.ClassCalculatorParameters && _category.ClassCalculatorParameters.split(',')[0] == field.Name ) ? true : false );
    	if( _findex > -1 && cat_readonlyexp )
    		cat_roByexp = (cat_readonlyexp) ? (( source ) ? (activeParcel.EvalValue(source + '[' + _findex + ']', cat_readonlyexp)) : (activeParcel.EvalValue(cat_readonlyexp))) : ((_category && _category.ClassCalculatorParameters && _category.ClassCalculatorParameters.split(',')[0] == field.Name) ? true : false);
        if (field.ReadOnly || roByexp || (_category && _category.IsReadOnly) || (cat_roByexp && !getReadOnlyExpressionOverrideVal(field)) )
    		$( 'td.value[fieldid="' + fieldId + '"]' ).removeClass( 'unsaved' );
    	else
    		$( 'td.value[fieldid="' + edit.FieldId + '"]' ).addClass( 'unsaved' );
    }
	else if (!_currentRecValueMatches || !_currentValueMatches) 
    	$( 'td.value[fieldid="' + edit.FieldId + '"]' ).addClass( 'unsaved' );

    if(!_currentRecValueMatches || !_currentValueMatches || autoLoop)	
        activeParcelEdits.push(edit);
    // $( 'td.value[fieldid="' + fieldId + '"] .input' ).attr( 'fvalue', qcValue);
    var cur_fieldedits = activeParcelEdits.filter(function(x){ return(x.AuxROWUID == auxRowId && x.FieldId == fieldId)})[0];
    if(_currentRecValueMatches && !cur_fieldedits)
    	$( 'td.value[fieldid="' + edit.FieldId + '"]' ).removeClass( 'unsaved' );

    if (field?.InputType == 5) {
        let elm = $('td.value[fieldid="' + fieldId + '"] input');
        if ($(elm)[0] && $(elm).attr('customWidth')) {
            $(elm).removeAttr('customWidth');
            $(elm).css('width', '');
        }
        recalculateCustomDropdownTextWidth(elm, block);
    }

    let afterTableInsert = function () {
        refreshRelatedFields(fieldId, auxRowId, block, autoLoop, true);
        calcRelatedFields(fieldId, block);
        checkClassCalculated(_category, index, sourceTable);
        if (getReadOnlyExpressionOverrideVal(field)) setTimeout(() => { try { refreshCatRecord(block, auxRowId, source); } catch (ex) { } }, 100);
        if (callback) callback();
    }

    if (_.contains(ccma.UI.LookupQueryCustomFields, field.Id)) {
        let upSql = '', sqlParams = [];
        if (field.SourceTable && field.SourceTable != '') {
            upSql = 'UPDATE ' + field.SourceTable + ' SET [' + field.Name + '] = ? WHERE ROWUID = ' + auxRowId; sqlParams = [qcValue];
        }
        else {
            updateSql = 'UPDATE Parcel SET [' + field.Name + '] = ? WHERE Id = ' + parcelId; sqlParams = [qcValue];
        }
        if (upSql != '') { getData(upSql, sqlParams); }
    }

    afterTableInsert();
}


function checkClassCalculated (_category , index, sourceTable ) { 
     // Checks if selected field is ClassCalculator 
     if (_category && _category.ClassCalculatorParameters != '' && _category.ClassCalculatorParameters != null && _category.ClassCalculatorParameters != undefined && sourceTable && activeParcel[sourceTable] && activeParcel[sourceTable][index]){
		var recDeleted = activeParcel[sourceTable][index].CC_Deleted;
				var calculatorCondition = _category.ClassCalculatorParameters.split(',').length == 3 ? _category.ClassCalculatorParameters.split(',')[2] : null;
				var ShowCalculator;
				if (calculatorCondition)
					ShowCalculator = activeParcel.EvalValue(sourceTable + '[' + index + ']', calculatorCondition)
				if ((ShowCalculator || !calculatorCondition) && !recDeleted)
					$('.classCalc').show();
				else
					$('.classCalc').hide();
}
	
}


// QC Actions

function initQCButtons(callback) {
    $('.parcel-qc-pad button[qc]').click(function () {
        var btn = this;
        var t = activeParcel.CC_Error && activeParcel.CC_Error != ''
        if (activeParcel.CC_Error && activeParcel.CC_Error != '') {
            if (confirm("There are data errors in this parcel. \n\n Click OK to ignore this and approve, click CANCEL to review again.")) {
                doQCAction(btn);
            }
            else
                return
        }
        else
            doQCAction(btn);                
        return false;
    });
    $( '.onoffswitch' ).click( function ( e ) { $( this ).find( '.onoffswitch-checkbox' ).is( ':checked') ? $( this ).find( '.onoffswitch-checkbox' ).removeAttr( 'checked' ) : $( this ).find( '.onoffswitch-checkbox' ).attr( 'checked', true ); } )
    $('.onoffswitch-label').click(function (a) {
        var selectedList = $('li.selected').html();
        var selectedListVal = $(selectedList).attr('category');
        if ($('li:[content="parceldata"].selected').length == 1 || $('li.selected').length == 1)
			showMask('Switching Parcel Data...');
		$('.aux-index[findex]').attr('findex','0');
		fIndx = [];
    	showFutureData = $( this ).parent().find( '.onoffswitch-checkbox' ).is(':checked') ? false : true
    	localStorage.setItem( 'showFutureData', showFutureData );
    	loadFilterParcelData(null,function(){	
    	applyParcelChanges(function(){
			for (x in activeParcel) {
            	var field = activeParcel[x];
            	var block = getDataField(x, null) ? $('.category-page[categoryid="' + getDataField(x, null).CategoryId + '"]') : null;
            	setFieldValue(field, x, block,null,null,null,true);
        }});
        for (var x in activeParcel.ParcelChanges) {
            var ch = activeParcel.ParcelChanges[x];
            if (ch.AuxROWUID == null) {
                var qc = $('.qc[fieldid="' + ch.FieldId + '"]');
                $('a', qc).hide();
                $((activeParcel.QC && ch.QCChecked) ? '.passed' : '.check', qc).show();
                $((activeParcel.QC && ch.QCChecked) ? '.passed' : '.check', qc).attr('changeid', ch.Id);
                setFieldValue(ch.NewValue, ch.FieldId,null,null,null,null,true);
            }
        }
        getAuxiliaryTableNames(function (tableList) {
        	insertDataIntoTables(_.clone(tableList), function(){
            	LoadAuxDataPRC(activeParcel, tableList, function () {
	                loadAuxData(activeParcel, tableList, function (parcel) {
	                    activeParcel = parcel;
	                    sortBySortExpression();
                        loadPRCactiveParcel(TableHierarchical, activeParcel);
                        if ($('li:[content="parceldata"].selected').length == 1) {
                            $('li:[content="parceldata"] a').trigger('click');
                            var cat = $(".category-page:visible")[0];
                            var CategoryId = $(cat).attr('categoryid');
                            var catname = getCategory(CategoryId);
                            openCategory(CategoryId, catname.Name);
                            // hideMask();
                        } else {
                            if (selectedListVal === "dashboard" ||selectedListVal === "digital-prc" || selectedListVal === "sketch-manager" || selectedListVal === "audit-trail" || selectedListVal === "photo-viewer") {
                                openCategory(selectedListVal, selectedList);
                            }
                        }
							
	                    $('div.category-menu a').removeClass('field-category-edited');
	    				$('div.category-menu a').removeClass('approved');
	                    highlightCatLink();
	                    PrintsketchView();
	                    if (sketchEditor && !sketchEditor.closed) 
	                        openSketchEditor();
	                });
                });
            });
        });
    });
    });
    if(callback) callback(); 
}

function doQCAction(btn) {
    $('#classCalculatorFrame').hide();
    var qc = $(btn).attr('qc');
    hide('.field-audit-qc');
    if (!activeParcel.Reviewed && qc > -1) {
        alert('This parcel has not been marked as complete. You cannot change QC status now.');
        return false;
    }

    if ($('.parcel-qc-pad button[selected]').length > 0) {
        if ($('.parcel-qc-pad button[selected]')[0] == btn) {
            if (activeParcelEdits.length == 0) {
                //CHECK AGAIN - return false would be needed.
                //this field commented by 'MSP' to Approve all the changes even there is no edits 
                //return false;
            }
        }
    }
    if (qc == 0) {
        if (!window.confirm('Rejecting parcel changes would permanently delete all parcel changes done by the field user, and reset the priority to Urgent. The deleted changes cannot be recovered later. Photos will be deleted from the parcel as well. Are you sure you want to proceed?'))
            return false;
        Notificationmsg = "Parcel Rejected successfully";
    }
    else if (qc == 1) {
        if (clientSettings?.GeneratePRCPDF == '1') {
            uploadPrcHtml();
            displayPrc(activeParcel.PrcBackup);
        }
        setTimeout(refreshPRC, 5000);
        Notificationmsg = "Parcel Approved successfully";
        
        
        /*openCategory('digital-prc');*/
    }
    else if (qc == -2 )
    	Notificationmsg = "Parcel has been deleted successfully";
    else if (qc == -3 )
    	Notificationmsg = " Deleted property successfully recovered";
    else if (qc == -4 )
    	Notificationmsg = " Commit Change successfully";	
    else { Notificationmsg = "Parcel NOT SET successfully"; }
    var setQCCall = function (callback) {
        showMask();
        $qc('qcset', { ParcelId: activeParcel.Id, QC: qc }, function (resp) {
            //CHECK why resp == null
            if (resp == null) {
                return false;
            }
            if (resp.status == "OK") {
                $('.parcel-qc-pad button[selected]').removeAttr('selected');
                dashboardChanged = false;
                //                getParcelDataUpdates();
                //                getParcel(activeParcel.Id);
                //  refreshPriorityValue();
                $(btn).attr('selected', '');
                if (qc == 0)
                    getParcel(activeParcel.Id, function () {
                        if ($('.tabs-main').find('li.selected').attr('category') == 'photo-viewer') {
                            showAllPhotos();
                            getPhotoUpdates();
                        }
                        getParcelDataUpdates(null,qc);
                        if(openFromPRC)  
                           openCategory('digital-prc','Digital PRC');
                    });
                else {
                    if (callback){ callback(null, qc, true);}
                    else{
                    	getParcelDataUpdates(null, qc, true);
                    }
                }
                if (qc == 1) {
                    activeParcel.CC_Error = '';
                    $(".CC_error_div").hide();
                    
                    displayPrc(activeParcel.PrcBackup);
                }

            }
            //TODO Error handling is not done.
        });
    }
    var saved = checkIfSaved(true, true, setQCCall);
    return false;
}

function checkIfSaved(save, nomove, callback) {
    //var IsCalculatedOnly =  activeParcelEdits.filter(function(e){ return  datafields[e.FieldId] && datafields[e.FieldId].IsCustomField == true && datafields[e.FieldId].CalculationExpression != null}).length > 0 &&  activeParcelEdits.filter(function(e){ return datafields[e.FieldId] && datafields[e.FieldId].IsCustomField == true && datafields[e.FieldId].CalculationExpression == null}).length == 0;
    if ((activeParcelEdits.length > 0) || (recoveredItems.length > 0) || dashboardChanged || activeParcelPhotoEdits.length > 0) {
        /*var countOfAutoSelectTrue = activeParcelEdits.filter(function (item) { return item.isLookupAutoSelected == true });
        var countOfAutoSelectFalse = activeParcelEdits.filter(function (item) { return item.isLookupAutoSelected == false });
        if ((countOfAutoSelectTrue.length > 0 && countOfAutoSelectFalse.length == 0) || IsCalculatedOnly) {
            saveParcelChanges(callback);
            return;
        }*/
        var msg = 'Parcel changes are not saved. Click OK to stay back on this parcel view, or Cancel to ignore and proceed to the next view.';
        if (activeParcelEdits.length == 0 && activeParcelPhotoEdits.length == 0 && !dashboardChanged) {
            var msg = 'One or more deleted records have been recovered. Click OK to stay back on this parcel view, or Cancel to ignore and proceed to the next view.';
        }
        if (nomove) {
            msg = 'Click OK to save your changes and proceed, or Cancel to continue editing';
            if (activeParcelEdits.length == 0 && activeParcelPhotoEdits.length == 0 && recoveredItems.length > 0) {
                msg = 'Click OK to save the recovered records and proceed, or Cancel to discard.';
            }
        }
        if (window.confirm(msg)) {
            if (save) {
                saveParcelChanges(callback);
                return true;
            } else {
            	loadFilterParcelData();
                return false;
            }
        }
        else {
            if (nomove){
				loadFilterParcelData();
				return false;
			}
            else {
                if (callback) callback(getParcelDataUpdates);
                return true;
            }
        };
    } else {
        if (callback) callback(getParcelDataUpdates);
        return true

    };
}

function saveParcelChanges(callback, errorCallback, skipValidation, skipQCAlert) {
    try {
        let isReadOnlyUserss = __DTR && DTRRoles && DTRRoles.ReadOnly ? true : false;
        if (isReadOnlyUserss) {
            alert("You don't have permission to do this operation");
            return false;
        }
        var isCallback = arguments[1] ? arguments[1] : false;
        var _isReviewedTrue = activeParcel.Reviewed ? true : false;

        loadFilterParcelData(true, function () {
            validateParcel((activeParcel.Reviewed? 1: 0), function(warningMsg) {
        	    var _savePCI = function(skcallback) {
                        hide('.field-audit-qc');
                        showMask();
                        //if ((activeParcelEdits.length == 0) && (!dashboardChanged)) return false;
                        if ((activeParcelEdits.length == 0) && (activeParcelPhotoEdits.length == 0) && (!dashboardChanged) && (recoveredItems.length == 0)) {
                            if (skcallback) { skcallback(); return; };    
                            if (childWindows.sketcheditor && childWindows.sketcheditor.closed == false && !isCallback) {
                                if (sketchEditor && sketchEditor.sketchApp) {
                                    sketchEditor.sketchApp.save(function(i, callback) {
                                        if (callback) callback();
                                         hideMask();
                                        }, null, true);
                                }
                            }
                            if (isCallback) {
                                if (callback) callback();
                            }
                            else hideMask();
                            loadFilterParcelData();
                            return false;
                        }
                  // });

                    //        if (activeParcelEdits.filter(function (x) { return x.ParcelId }).length > 0) {
                    //            if (!checkRequiredFields()) {
                    //                return false;
                    //            }
                    //        }

                    var cat = $(".category-page:visible")[0];
                    var CategoryId = $(cat).attr('categoryid');
                    var recRowuid = $(cat).attr('rowuid'), switchToFirst = false;
                    var findex = $('.category-page[categoryid="' + getParentCategories(CategoryId) + '"] span.aux-index').attr('findex');
                    var showAuxParams = getParamOfAuxData(CategoryId)[0];
                    var index = parseInt($('.aux-index', cat).html()) > 0 ? parseInt($('.aux-index', cat).html()) - 1 : 0;
                    if (recRowuid) recRowuid = parseInt(recRowuid);
                    let delRecoveryNewItems = recoveryDeletedArrays.filter((x) => { return x.newRecord });
                    delRecoveryNewItems.forEach((x) => {
                        if (x.rowuid == recRowuid) switchToFirst = true;
                        activeParcelEdits = activeParcelEdits.filter((el) => { return (el.AuxROWUID != x.rowuid) });
                    });
                    if (switchToFirst && index > 0) index = 0;
                    if (activeParcelEdits.length > 0) {
                        var saveData = "";
                        var priorityChange = false;
                        for (x in activeParcelEdits) {
                            var ed = activeParcelEdits[x];
                            var _spDefaultPCIs = ( (ed.specialDefaults && ed.specialDefaults == "0") ? false: true )
                            if (!ed.parentROWUID || ed.parentROWUID == undefined)
                                ed.parentROWUID = 0;
                            if (ed.ParcelId && _spDefaultPCIs) {
                                var line = '^^^' + ed.ParcelId + '|' + ed.AuxROWUID + '|' + ed.FieldId + '|' + ed.ChangeId + '|' + ed.QCValue + '|' + ed.action + '|' + ed.parentROWUID + '\n';
                                saveData += line;
                            } else {
                                priorityChange = false;
                              }
                        }
                    }
                    //MSP
                    if (recoveredItems.length > 0) {
                        var recoveryData = "";
                        for (x in recoveredItems) {
                            var ed = recoveredItems[x];
                            var line = '^^^' + ed.sourcetable + '|' + ed.AuxROWUID + '|' + ed.changeId + '|' + ed.recoverChild +'\n';
                            recoveryData += line;
                        }
                    }
                    if (activeParcelPhotoEdits.length > 0) {
                        var activeParcelPhotoEditsdata = "";
                        for (x in activeParcelPhotoEdits) {
                            var ed = activeParcelPhotoEdits[x];
                            if(ed["MetaData"])
                                var line = '^^^' + ed.ParcelId + '|' + ed.PhotoID + '|' + ed.MetaData + '|' + ed.MetaValue + '\n';
                            else	
                                var line = '^^^' + ed.ParcelId + '|' + ed.PhotoID + '|' + ed.DownSynced + '\n';
                            activeParcelPhotoEditsdata += line;
                        }
                    }
                    //MSP
                    var priority = $('.parcel-priority').val();
                    var alertMessage = $('.parcel-alert-message').val();
                    if (priority == '') {
                        alertMessage = '';
                    }
                    var appraisal_type = $('.selected-estimate').val();
                    if ((priority == activeParcel.Priority) && (alertMessage == activeParcel.ParcelAlert)) {
                        priority = "";
                        alertMessage = "";
                    }
                    var dtrStatus = $('.dtr-selected-status').val();
                    var prioId ="";
                    if(window.location.href.contains("PrioPage=")){
                        prioId = localStorage.getItem("PrioTableId");
                    }
                    var dtrworkflowdelete = "";
                    if (deletedWorkflow.length > 0) {
                        for (var i = 0; i < deletedWorkflow.length; i++) {
                            var dtr_wf = deletedWorkflow[i];
                            dtrworkflowdelete += dtr_wf.code;
                            if (i < deletedWorkflow.length - 1) {
                                dtrworkflowdelete += '|';
                            }
                        }
                    }
                    var saveRequestData = {
                        ParcelId: activeParcel.Id,
                        Priority: priority,
                        AlertMessage: alertMessage,
                        Data: saveData,
                        recoveryData: recoveryData,
                        AppraisalType: appraisal_type,
                        activeParcelPhotoEditsdata: activeParcelPhotoEditsdata,
                        prioId: prioId,
                        deletedWorkflow: dtrworkflowdelete
                    };

                    if (__DTR) {
                        saveRequestData.DTRStatus = dtrStatus;
                        saveRequestData.__dtr = "1";
                    }
                    var countOfAutoSelectTrue = activeParcelEdits.filter(function (item) { return item.isLookupAutoSelected == true });
                    var countOfAutoSelectFalse = activeParcelEdits.filter(function (item) { return item.isLookupAutoSelected == false });

                    $qc('saveparcelchanges', saveRequestData, function (resp) {
                        if (resp.status == "OK") {
                            activeParcelEdits = [];
                            activeParcelAlert = [];
                            activeParcelPriority = [];
                            recoveredItems = [];
                            dashboardChanged = false;
                            var afterSave = function () {
                                //                    if (callback) {
                                //                        callback(getParcelDataUpdates);

                                //                    }
                                //                    else {

                                getParcel(activeParcel.Id, function () {
                                    showAuxParams = getParamOfAuxData(CategoryId)[0];
                                    $('.category-page[categoryid="' + getParentCategoryIds(CategoryId) + '"] span.aux-index').attr('findex', parentIndex);
                                    showAuxData(cat, index, showAuxParams.filter_field, showAuxParams.filter_value, showAuxParams.parentROWUID);
                                    if (!__DTR && localStorage && localStorage.internalSupportView == '1' && localStorage.parcelDataSupportView == '1')
    								    supportViewIconSwitch(); 
    								
				    			    if ($('#photo-properties-frame').is(':visible') == true) { //if the screen is photo metadata screen, then need to load screen for updating qc value with latest photo value.
                                        showPhotoProperties();
                                    } 	
                                
                                    if (currentPhotoIndex) {
                                        if (activeParcel.Photos[currentPhotoIndex] != undefined && activeParcel.Photos[currentPhotoIndex].DownSynced == false || activeParcel.Photos[currentPhotoIndex] != undefined && activeParcel.Photos[currentPhotoIndex].DownSynced == null)
                                            $('.downsycset').hide();
                                    }
                                    else {
                                        //$('.downsycset').show();
                                    }
                                    if (callback) callback();
                                    hideMask();
                                    if (findex)
                                        $('.category-page[categoryid="' + getParentCategories(CategoryId) + '"] span.aux-index').attr('findex', findex);
                                    if (countOfAutoSelectTrue.length == 0 && countOfAutoSelectFalse.length > 0)
                                        notification("Parcel saved successfully");
                                    //Notificationmsg = "";

                                }, true, null, true);
                            }
                            if (skcallback) { skcallback(); return; };    
                            if (childWindows.sketcheditor && childWindows.sketcheditor.closed == false && !isCallback) {
                                if (sketchEditor && sketchEditor.sketchApp) {
                                    sketchEditor.sketchApp.save(function (i, callback) {
                                        afterSave();
                                        if (callback) callback();
                                    }, null, true);
                                }
                            }
                            else if (isCallback) {
                                if (callback) callback();
                            }
                            else {

                                afterSave();
                            }
                        }

                        $('#classCalculatorFrame').hide();
                    });
                }

                if (warningMsg && skipQCAlert && callback) {
                    callback(warningMsg, function (sktcallback) {
                        _savePCI(sktcallback);
                    });
                }
                else
                    _savePCI();
          }, errorCallback, function() { loadFilterParcelData();}, skipQCAlert, skipValidation);//End of Callback
      }, isCallback, _isReviewedTrue);
    }
    catch (e) {
    	loadFilterParcelData();
        console.log(e.message);
    }
    return false;
}

function doSoftReject() {
    var setQCCall = function (callback) {
        showMask();
        $qc('softreject', { ParcelId: activeParcel.Id }, function (resp) {
            Notificationmsg = "Parcel SOFT Rejected successfully";
            getParcelDataUpdates(null,'softreject');

        });

    }
    var saved = checkIfSaved(true, true, setQCCall);
    return false;
}

function doCommitChanges(){
if(clientSettings["EnableMALite"] == 1 ){
var setQCCall = function (callback) {
      showMask();
      $qc('CommitChange', { ParcelId: activeParcel.Id }, function (resp) {
          Notificationmsg = "Commit Changes successfully";
          getParcelDataUpdates(null,'commitchange');

      });

  }
  var saved = checkIfSaved(true, true, setQCCall);
  return false;

}
}

function createNew(callback) {

    if (confirm('Create a new record?')) {
        var parId = activeParcel.Id;
        var cat = $(".category-page:visible")[0];
        var CategoryId = $(cat).attr('categoryid');
        // var parentCategory = $('#AllCategories li[category="' + CategoryId + '"]').attr('parentid');
        //var sourceTable = $('.category-page[categoryid="' + parentCategory + '"]').attr('auxdata');
        var sourceTable = $('.category-page[categoryid="' + CategoryId + '"]').attr('auxdata');
        var data = activeParcel[sourceTable];
        var auxRowId = ccTicks();
        var showAuxParams = getParamOfAuxData(CategoryId)[0];
        var action = "new";
        var newObject = {};
        var fields = Object.keys(datafields).map(function (x) { return datafields[x] }).filter(function (x) { return x.SourceTable == sourceTable });
        createNewForm(parId, auxRowId, CategoryId, action, showAuxParams.parentROWUID, sourceTable);
        fields.forEach(function (item) {
            newObject[item.Name] = null;
        });
        if (showAuxData) {
            for (key in showAuxParams.filter_field) {
                var name = showAuxParams.filter_field[key];
                var value = showAuxParams.filter_value[key];
                newObject[name] = value;
            }
        }
        newObject['CC_Deleted'] = false;
        newObject['CC_ParcelId'] = parId;
        newObject['CC_LastUpdateTime'] = new Date();
        newObject['ClientROWUID'] = auxRowId;
        newObject['ROWUID'] = auxRowId;
        newObject['ParentROWUID'] = showAuxParams.parentROWUID;
        if (showAuxParams.parentROWUID) {
            var catid = getParentCategories(CategoryId)
            var parentTable = getSourceTable(catid);
            var source = activeParcel[parentTable].filter(function (s) { return s.ROWUID == showAuxParams.parentROWUID })[0];
            newObject['parentRecord'] = source;
            if (!source[sourceTable]) source[sourceTable] = [];
            source[sourceTable].push(newObject);
        }
        fields.forEach(function (item) {
            if (item.DefaultValue != null && item.DefaultValue != "" && showAuxParams.filter_field.filter(function (f) { return f == item.Name; }).length == 0) {
                var defaultValueRes = calculateDefaultValues(item, newObject.parentRecord ? newObject.parentRecord : newObject);
                var defaultValue = defaultValueRes.defaultValue;
                var specialDefaults = defaultValueRes.specialDefaults;
                newObject[item.Name] = defaultValue;
                var newRecord = {
                    ParcelId: parId,
                    AuxROWUID: auxRowId,
                    FieldId: item.Id,
                    ChangeId: null,
                    QCValue: (defaultValue != null)? defaultValue: '',
                    action: "edit",
                    parentROWUID: showAuxParams.parentROWUID,
                    specialDefaults: specialDefaults
                };
                activeParcelEdits.push(newRecord);
            }
        });
        data.push(newObject);

        if (ccma.UI.LookupQueryCustomFields?.length > 0) {
            let keys = ['CC_RecordStatus', 'CC_ParcelId', 'CC_Deleted', 'ClientROWUID', 'ParentROWUID', 'ROWUID'],
                fls = fields.filter((x) => { return x.DefaultValue != null && x.DefaultValue != '' && _.contains(ccma.UI.LookupQueryCustomFields, x.Id) }), values = [];

            if (showAuxData && showAuxParams?.filter_field?.length > 0) keys = keys.concat(showAuxParams.filter_field);
            fls.forEach((f) => { if (!_.contains(keys, x.Name)) keys.push(x.Name); });

            keys.forEach((col) => {
                newObject[col] = (col == 'CC_Deleted') ? '' : (col == 'CC_RecordStatus' ? 'I' : newObject[col])
                let v = (newObject[col] || newObject[col] === 0) ? newObject[col].toString().replace(/\'/g, "''") : '';
                values.push(v);
            });

            getData("INSERT INTO " + sourceTable + " ([" + keys.join('],[') + "]) VALUES ('" + values.join("','") + "');", [], () => {
                if (callback) callback();
            }, null, true, () => { if (callback) callback(); });

        }
        else if (callback) callback();
    }
    else {
        return false;
    }

}

function calculateDefaultValues(field, data) {
    var defaultValue = field.DefaultValue, specialDefaults = "0";
    var t = field.DefaultValue.toString().split('.')
    var l = t.length;
    t = t[l - 1]
    if (field.DefaultValue.toString().indexOf('parent.parent.') > -1 && data) {
        if (data.parentRecord)
            defaultValue = data.parentRecord[t];
        specialDefaults = "1";
    } else if (field.DefaultValue.toString().indexOf('parent.') > -1 && data) {
        defaultValue = data[t];
        specialDefaults = "1";
    } else if ((field.DefaultValue.toString().indexOf('parent.') > -1 && !data )|| field.DefaultValue.toString().indexOf('parcel.') > -1) {
           defaultValue = activeParcel[t];
           if(!defaultValue || defaultValue==null||defaultValue===undefined) defaultValue='<blank>';
           specialDefaults = "1";
    }
    if (defaultValue === undefined)
        defaultValue = null;
    switch (field.DefaultValue.toString().toUpperCase()) {
        case "CURRENT_DATE":
        case "CURRENTDATE":
            var dt = (new Date()).formatout("ymd");
            defaultValue = checkForCustomFormat(field.Id) == '' ? dt : deFormatvalue(dt, field.Id);
            specialDefaults = "1";
            break;
        case "CURRENT_USER":
        case "CURRENTUSER":
            defaultValue = localStorage.getItem('username');
            specialDefaults = "1";
            break;
        case "CURRENT_YEAR":
        case "CURRENTYEAR":
            defaultValue = ccma.CurrentYear;
            specialDefaults = "1";
            break;
        case "CURRENT_DAY":
        case "CURRENTDAY":
            defaultValue = ccma.CurrentDay;
            specialDefaults = "1";
            break;
        case "CURRENT_MONTH":
        case "CURRENTMONTH":
            defaultValue = ccma.CurrentMonth;
            specialDefaults = "1";
            break;
        case "FUTUREYEARSTATUS":
        case "FUTUREYEARSTATUS":
            defaultValue = ccma.FutureYearStatus;
            specialDefaults = "1";
            break;
        case "BLANK":
            defaultValue = ' ';
            specialDefaults = "1";
            break;

    }
    if (defaultValue && defaultValue.toString().indexOf('parent.') > -1)
        defaultValue = "";
    return { defaultValue: defaultValue, specialDefaults: specialDefaults };
}
//function createNewForm(parId, auxRowId, CategoryId, action, parentROWUID, sourceTable, callback) {
function createNewForm(parId, auxRowId, CategoryId, action, parentROWUID, sourceTable) {
    // var newParentAuxRowId;
    var newRecord = {
        ParcelId: parId,
        AuxROWUID: auxRowId,
        FieldId: parentROWUID,
        ChangeId: null,
        QCValue: sourceTable,
        action: action,
        parentROWUID: parentROWUID
    };

    activeParcelEdits.push(newRecord);

    //  refreshRelatedFields(fieldId, auxRowId, block);

    //if (callback) callback();

}
var lastccTicks = null;
function ccTicks() {
	var currentYr = new Date().getFullYear(), nextYr =  Math.ceil(currentYr/ 10) * 10;
	nextYr = (((nextYr - currentYr) < 3) ? (nextYr + 10): nextYr);
    var d1 = new Date(nextYr, 12, 31);
    var d2 = (new Date()).getTime();
    while (d2 == lastccTicks) {
        d2 = (new Date()).getTime();
    }
    lastccTicks = d2;
    var diff = d2 - d1.getTime();
    return diff;
}
function doreview() {
    let isReadOnlyUsers = __DTR && DTRRoles && DTRRoles.ReadOnly ? true : false;
    if (isReadOnlyUsers) {
        alert("You dont have permission to do this operation");
        return false; 
    }
    var ss = "";
    var review;
    $('#classCalculatorFrame').hide();
    loadFilterParcelData(true, function(){
    var rec = classCalculatorValidation(activeParcel);
    if (activeParcel.Reviewed) {
        review = 0;
        ss = "Parcel Marked as Not Reviewed successfully"
    }
    else {
        classValidation(rec);
        review = 1;
        ss = "Parcel Marked as  Reviewed successfully"
    }
    var setQCCall = function (callback) {
        validateParcel(review, function () {
        	hide('.field-audit-qc');
            showMask();
            loadFilterParcelData();
            var appType = __DTR ? 'DTR' : 'QC';
            $qc('reviewparcel', { ParcelId: activeParcel.Id, Reviewed: review, appType: appType }, function (resp) {
                Notificationmsg = ss;
                if (callback) callback(null,'review');
                else getParcelDataUpdates(null,'review');
            });
        },null,function(){ loadFilterParcelData(); });
    }
    var saved = checkIfSaved(true, true, setQCCall);
    return false;
    });
}

function loadFilterParcelData(allDataFlag, callback, sketchCallback, _validateSave ){	
	if( sketchCallback && callback) {
		callback();
		return false;
	}
	var futureYearEnabled = clientSettings["FutureYearEnabled"];
    if(futureYearEnabled == '1' || futureYearEnabled == 'true'){
    	var tempParcelData = _.clone(activeParcelAllData);
    	var tempParcelDataOriginal = _.clone(activeParcelAllData.Original);
		tempParcelData = new newParcel(tempParcelData);
    	getAuxiliaryTableNames(function(tablelist){
    		for(var x in tablelist){
    			var sourceTable = tablelist[x];
    			var cat = fieldCategories.filter( function ( d ) { return d.SourceTable == sourceTable } )
    			if ((sourceTable !== undefined || sourceTable != null) && cat.length > 0 && cat[0].YearPartitionField) {
					if(allDataFlag){
						activeParcel[sourceTable] = tempParcelData[sourceTable];
						activeParcel.Original[sourceTable] = tempParcelDataOriginal[sourceTable];
					}
					else{
						activeParcel[sourceTable] = tempParcelData[sourceTable].filter (function(d){ if(showFutureData){return d.CC_YearStatus == 'F'} else { return d.CC_YearStatus == null || d.CC_YearStatus == 'A'} });
    					activeParcel.Original[sourceTable] = tempParcelDataOriginal[sourceTable].filter (function(d){ if(showFutureData){return d.CC_YearStatus == 'F'} else { return d.CC_YearStatus == null || d.CC_YearStatus == 'A'} });
    				}
    			}
    		}
	    	if(allDataFlag) {
				applyParcelChanges(function() {
					applyParcelEditsChanges(_validateSave, function() {	
	    				if(callback) callback();
	    			});
	            });
	        }
	        else
	        	if(callback) callback();
		});
	}
	else{
		if(callback) callback();
	}
}
//Other Functions - Cleanup required

function focusOnField(CorrespondingChangeId) {
    try {
        var categoryId = '', FieldId = '', AuxROWUID = '';
        var changeElement = activeParcel.ParcelChanges.find(function(element) {
            return (element.Id == CorrespondingChangeId && getCategory(element.CategoryId) && getCategory(element.CategoryId).ShowCategory == 1)
        });
        if(changeElement){
            categoryId = changeElement.CategoryId;
            FieldId = changeElement.FieldId;
            AuxROWUID = changeElement.AuxROWUID;
        }
        if (categoryId == '') {
            return false;
        }
        else {
            var rootdetails = getRootDetails(categoryId, AuxROWUID);
            var filter_value = [], filter_field = [];
            var totalParents = rootdetails.length;
            if (totalParents > 0) {
                $('.category-menu').show();
                $('.tabs li').removeAttr('class');
                $('.tabs li[content="parceldata"]').addClass('selected');
            }
            else {
                $('.category-menu').hide();
                return false;
            }
            while (totalParents > 0) {
                totalParents -= 1;
                filter_field = [];
                filter_value = [];
                var nodeCategory = rootdetails[totalParents].category;
                var categoryName = $('#AllCategories li[category="' + nodeCategory + '"]').html().trim();
                openCategory(nodeCategory, categoryName);
                if (rootdetails[totalParents].filter1.length > 0) {
                    var filterLenght = rootdetails[totalParents].filter1.length;
                    while (filterLenght > 0) {
                        filterLenght -= 1;
                        filter_value[filterLenght] = rootdetails[totalParents].filter1[filterLenght].value;
                        filter_field[filterLenght] = rootdetails[totalParents].filter1[filterLenght].field;
                    }
                }
                if ($('.category-page[categoryid="' + nodeCategory + '"]').attr('parceldata') == undefined) {
                    var OrgNodeIndex = rootdetails[totalParents].OrginalNodeIndex
                    var NodeSourceTable = $('.category-page[categoryid="' + nodeCategory + '"]').attr('auxdata');
                    var parentROWUID = activeParcel[NodeSourceTable].length > 0 ? activeParcel[NodeSourceTable][OrgNodeIndex] ? activeParcel[NodeSourceTable][OrgNodeIndex].ParentROWUID : activeParcel[NodeSourceTable][0].ParentROWUID : null
                    showAuxData($('.category-page[categoryid="' + nodeCategory + '"]'), rootdetails[totalParents].NodeIndex, filter_field, filter_value, parentROWUID);
                    selectTab(categoryId, rootdetails[totalParents].NodeIndex);
                }
            }
            var sourcetbl = getCategory(categoryId).SourceTable  
            var sourcedel = activeParcel[sourcetbl].filter(function(x){return x.ROWUID == AuxROWUID})[0].CC_Deleted
            if(sourcedel == true){
                var topcat = getTopParentCategory(categoryId)
                openCategory(topcat)
            }                    
            var focusedSource;
            if ($('.category-page[categoryid="' + categoryId + '"] td[fieldid="' + FieldId + '"] .input.trackchanges').attr('auxrowuid') == undefined) {
                focusedSource = $('.category-page[categoryid="' + categoryId + '"] td[fieldid="' + FieldId + '"] .input.trackchanges');
            }
            else {
                focusedSource = $('.category-page[categoryid="' + categoryId + '"] td[fieldid="' + FieldId + '"] .input.trackchanges[auxrowuid="' + AuxROWUID + '"]');
            }
            focusedSource.focus();
        }
    }
    catch (e) {
        console.log('Error on redirecting from dashboard >> ' + e.message);
        console.dir(e);
    }
}

function getRootDetails(categoryID, AuxROWUID) {
    try {
        var rootDetails = [], filterfield = [], rootvalues = [], filter_value = [];
        var tempParent, tempAuxROWUID, root = categoryID, rootAuxROWUID = AuxROWUID;
        while (true) {
            filter_value = [];
            var parentCategory = $('#AllCategories li[category="' + root + '"]').attr('parentid');
            if (parentCategory == undefined) {
                var indexOfParent = 0, NodeAux;
                var nodeSource = $('.category-page[categoryid="' + root + '"]').attr('auxdata');
                if (nodeSource == undefined) {
                    var isParcelData = $('.category-page[categoryid="' + root + '"]').attr('parceldata');
                    if (isParcelData == '') {
                        var forparceldata = {
                            NodeIndex: 0,
                            filter1: [],
                            category: root,
                            OrginalNodeIndex : 0
                        }
                        rootDetails.push(forparceldata);
                        return rootDetails;
                    }
                }
                indexOfParent = eval('activeParcel.' + nodeSource + '.findIndex(function(element){ return element.ROWUID == rootAuxROWUID})');
                rootvalues = {
                    NodeIndex: indexOfParent,
                    filter1: [],
                    category: root,
                    OrginalNodeIndex : indexOfParent
                }
                rootDetails.push(rootvalues);
                break;
            }
            var filter_fieldCSV = $('#AllCategories li[category="' + parentCategory + '"]').attr('nodefilterfield');
            if (filter_fieldCSV == '') {
                console.log('No filter fields');
                var sourceTable = $('.category-page[categoryid="' + parentCategory + '"]').attr('auxdata');
                var indexCSVnull = 0;
                var sourceCSVnull = eval('activeParcel.' + sourceTable);
                indexCSVnull = sourceCSVnull.findIndex(function(element){ return element.ROWUID == rootAuxROWUID });
                rootvalues = {
                    NodeIndex: indexCSVnull,
                    filter1: [],
                    category: root,
                    OrginalNodeIndex : indexCSVnull
                }
                rootDetails.push(rootvalues);
            } else {
                var sourceTable = $('.category-page[categoryid="' + parentCategory + '"]').attr('auxdata');
                var NodeSourceTable = $('.category-page[categoryid="' + root + '"]').attr('auxdata');
                var iter_value = 0, source, index = 0, values = [];
                var filter_field = filter_fieldCSV.split(',');
                var filter_field_length = filter_field.length;
                source = eval('activeParcel.' + NodeSourceTable + '.filter(function (d, i) { return d.ROWUID == ' + rootAuxROWUID + '; })');
                OrginalNodeIndex = eval('activeParcel.' + NodeSourceTable + '.findIndex(function (d, i) { return d.ROWUID == ' + rootAuxROWUID + '; })');
                if (source.length == 0) {
                    return rootDetails;
                }
                while (iter_value < filter_field_length) {
                    filter_value[iter_value] = source[0][filter_field[iter_value]];
                    values = {
                        field: filter_field[iter_value],
                        value: filter_value[iter_value]
                    }
                    filterfield.push(values);
                    values = [];
                    iter_value += 1;
                }
                index = 0;
                var filtered_source = showAuxDataWithFilter(NodeSourceTable, filter_field, filter_value, source[0].ParentROWUID,categoryID);
                index = filtered_source.findIndex(function(element){ return element.ROWUID == rootAuxROWUID });
                tempParent = $('#AllCategories li[category="' + root + '"]').attr('parentid');
                tempAuxROWUID = source[0]['ParentROWUID'];
                rootvalues = {
                    NodeIndex: index,
                    filter1: filterfield,
                    category: root,
                    OrginalNodeIndex : OrginalNodeIndex
                }
                rootDetails.push(rootvalues);
                filterfield = [];
            }
            if (tempParent == undefined || tempAuxROWUID == undefined) {
                break;
            }
            else {
                root = tempParent;
                rootAuxROWUID = tempAuxROWUID;
            }
        }
        return rootDetails;
    }
    catch (e) {
        console.log('Error on getting root details >> ' + e.message);
    }
}

function markDashboardChanged() {
    try {
        dashboardChanged = true;
        if (activeParcelAlert.length > 0) {
            activeParcelAlert = [];
        }
        var alertMessage = $('.parcel-alert-message').val();
        activeParcelAlert.push(alertMessage)

        if (activeParcelPriority.length > 0) {
            activeParcelPriority = [];
        }
        var alertPriority = $('.parcel-priority').val();
        activeParcelPriority.push(alertPriority);
    }
    catch (e) {
        console.log(e.message);
    }
}

function getReviewDate() {
    try {
        var Time = new Date(activeParcel.LocalQCDate);
        var cdate = Time.localeFormat("d");
        var ctime = Time.localeFormat("t");
        var today = (new Date()).localeFormat("d");
        var todayTime = (new Date()).localeFormat("t");
        var edate = cdate;
        if (cdate == today) {
            edate = 'Today   , ' + ctime;
        }

        return edate;

    }
    catch (e) {
        console.log(e.message);
    }
}

function getClientSettingValue(SettingName) {
    return clientSettings[SettingName];
}
/*
function getParamOfAuxData(CategoryId) {
    try {
        var filter_field = [];
        var filter_value = [];
        var parentROWUID = null;
        var returnValue = [];
        var parentCategory = $('#AllCategories li[category="' + CategoryId + '"]').attr('parentid');
        var sourceTable = $('.category-page[categoryid="' + parentCategory + '"]').attr('auxdata');
        if (parentCategory == undefined || sourceTable == undefined) {
            filter_value = [];
            filter_field = [];
        }
        else {
            parentIndex = $('.category-page[categoryid="' + parentCategory + '"]').attr('findex');
            var filter_fieldCSV = $('#AllCategories li[category="' + CategoryId + '"]').attr('nodefilterfield');
            var parentROWUID = eval('activeParcel.' + sourceTable + '[' + parentIndex + '].ROWUID');
            var iter_value = 0;
            if (filter_fieldCSV != '') {
                filter_field = filter_fieldCSV.split(',');
                var filter_field_lenght = filter_field.length;
                while (iter_value < filter_field_lenght) {
                    filter_field[iter_value] = (filter_field[iter_value].indexOf("/") > -1) ? filter_field[iter_value].split("/")[0] : filter_field[iter_value];
                    filter_value[iter_value] = eval('activeParcel.' + sourceTable + '[' + parentIndex + '].' + filter_field[iter_value]);
                    iter_value += 1;
                }
            }
        }
        var tempForReturn = { filter_field: filter_field, filter_value: filter_value, parentROWUID: parentROWUID };
        returnValue.push(tempForReturn);
        return returnValue;
    }
    catch (e) {
        console.log(e.message);
    }
}
*/
function getParamOfAuxData(CategoryId) {
    try {
        var filter_field = [];
        var filter_value = [];
        var parentROWUID = null;
        var returnValue = [];
        var parentCategory = $('#AllCategories li[category="' + CategoryId + '"]').attr('parentid');
        var sourceTable = $('.category-page[categoryid="' + parentCategory + '"]').attr('auxdata');
        if (parentCategory == undefined || sourceTable == undefined) {
            filter_value = [];
            filter_field = [];
        }
        else {
            //parentIndex = $('.category-page[categoryid="' + parentCategory + '"]').attr('index');
            var filter_fieldCSV = $('#AllCategories li[category="' + CategoryId + '"]').attr('nodefilterfield');
			var prowid= $('.category-page[categoryid="' + parentCategory + '"]').attr('ROWUID')
			if(prowid)
			var parentROWUID=$('.category-page[categoryid="' + parentCategory + '"]').attr('ROWUID')
			else
            var parentROWUID = eval('activeParcel.' + sourceTable + '[' + parentIndex + '].ROWUID');
			var parentIndex=activeParcel[sourceTable].indexOf(activeParcel[sourceTable].filter(function(x){return x.ROWUID==parentROWUID})[0])
            if(parentIndex == -1){
				parentIndex = activeParcel[sourceTable].indexOf(activeParcel[sourceTable].filter(function(x){return x.ClientROWUID==parentROWUID})[0])
				parentROWUID = parentIndex > -1 ? activeParcel[sourceTable].filter(function(x){return x.ClientROWUID==parentROWUID})[0].ROWUID : parentROWUID;
			}
            var iter_value = 0;
            if (filter_fieldCSV != '') {
                filter_field = filter_fieldCSV.split(',');
                var filter_field_lenght = filter_field.length;
                while (iter_value < filter_field_lenght) {
                    filter_field[iter_value] = (filter_field[iter_value].indexOf("/") > -1) ? filter_field[iter_value].split("/")[0] : filter_field[iter_value];
                    filter_value[iter_value] = eval('activeParcel.' + sourceTable + '[' + parentIndex + '].' + filter_field[iter_value]);
                    iter_value += 1;
                }
            }
        }
        var tempForReturn = { filter_field: filter_field, filter_value: filter_value, parentROWUID: parentROWUID };
        returnValue.push(tempForReturn);
        return returnValue;
    }
    catch (e) {
        console.log(e.message);
    }
}
function recoverAuxRecord(source) {
    try {
        var Table = $(source.parentNode).attr('sourcetable');
        var category = getCategoryFromSourceTable(Table) 
		var auxForm = $('.category-page[categoryid="' + category.Id + '"]');
        var rowUID = $(source.parentNode).attr('auxrowuid');
        var changeId = 0;
        var recover = []
        var childTables = parentChild.filter(function (x) { return x.ParentTable == category.SourceTable }).map(function (x) { return x.ChildTable });
        var maxrec = category.MaxRecords;
        var count = activeParcel[Table].filter((f) => { return f.CC_Deleted != true }).length;
        var hasChild = false;
        let deleteAtRecovery = recoveryDeletedArrays.filter((x) => { return x.sTable == Table && x.rowuid == rowUID })[0], tout = 1;
        if (deleteAtRecovery) {
            tout = 50;
            showMask();
        }
        setTimeout(() => {
            childTables.forEach(function (item) {
                //if(activeParcel[item].length > 0 && fieldCategories.filter(function(c){return c.SourceTableName == item}).length > 0 && fieldCategories.filter(function(c){return c.SourceTableName == item})[0].ShowCategory == 1){
                var cat = fieldCategories.filter((c) => { return c.SourceTableName == item })[0];
                var childRecords = activeParcel[item].filter((r) => { return r.ParentROWUID == rowUID });
                if (childRecords.length > 0 && cat && cat.ShowCategory == 1 && ($('.childCategoryTitle[category=' + cat.Id + ']').hasClass('catVisible') == true || cat.HideExpression == null)) {
                    hasChild = true;
                }
            });

            var recoverChildRecord = 0;
            if ((hasChild == true) && !deleteAtRecovery && confirm("Are you sure you want to recover child records also ?")) recoverChildRecord = 1;

            var recoverEl = function (changedId) {
                recover = {
                    sourcetable: Table,
                    AuxROWUID: rowUID,
                    changeId: changedId,
                    recoverChild: recoverChildRecord
                }
            }

            var ob = activeParcel.ParcelChanges.filter(function (pc) { return (pc.AuxROWUID == rowUID && pc.SourceTable == Table && pc.Action == 'delete') })

            if (ob.length > 0) {
                for (x in ob) {
                    changeId = ob[x].Id;
                    if (recoveredItems.filter(function (d) { return d.changeId == changeId; }).length == 0) {
                        recoverEl(changeId);
                        recoveredItems.push(recover);
                    }
                }
            }
            else if (recoveredItems.filter(function (rf) { return rf.sourcetable == Table && rf.AuxROWUID == rowUID }).length == 0 && !deleteAtRecovery) {
                recoverEl(changeId);
                recoveredItems.push(recover);
            }

            var recoverlength = recoveredItems.filter((f) => { return f.sourcetable == Table }).length;
            count += recoverlength;
            if (deleteAtRecovery) count += 1;

            if (maxrec) {
                if (count > maxrec) {
                    recoveredItems.pop();
                    alert('Maximum number of records(' + maxrec.toString() + ') has been reached for this category');
                    return false;
                }
            }

            if (deleteAtRecovery) {
                recoveryDeletedArrays = recoveryDeletedArrays.filter((x) => { return !(x.sTable == Table && x.rowuid == rowUID) });
                activeParcelEdits = activeParcelEdits.filter((x) => { return !(x.QCValue == Table && x.AuxROWUID == rowUID && x.action == 'delete') });

                let rec = activeParcel[Table].filter((x) => { return x.ROWUID == rowUID })[0];
                if (rec) {
                    rec.CC_Deleted = false;
                    let childs = getChildCategories(category.Id);
                    childs.forEach((cld) => {
                        let sourceTb = getSourceTable(cld);
                        activeParcel[sourceTb].filter((rec) => { return rec.ParentROWUID == rowUID }).forEach((x) => { x.CC_Deleted = false; });
                    });
                }
            }

            $(source.parentNode).hide();
            if (deleteAtRecovery) {
                $('.category-page[categoryid="' + category.Id + '"] .move-prev').click();
                $('.category-page[categoryid="' + category.Id + '"] .move-next').click();
                setTimeout(() => { hideMask(); }, 500);
            }
        }, tout)
        
        //for (x in activeParcel.ParcelChanges) {
        //    if (activeParcel.ParcelChanges[x].AuxROWUID == rowUID && activeParcel.ParcelChanges[x].Action == "delete" && activeParcel.ParcelChanges[x].SourceTable == Table) {
        //        var changeId = activeParcel.ParcelChanges[x].Id;
        //        if (recoveredItems.filter(function (d) { return d.changeId == changeId; }).length == 0) {
        //            var recover = {
        //                sourcetable: Table,
        //                AuxROWUID: rowUID,
        //                changeId: changeId
        //            };isExistingAuxDetail
        //            recoveredItems.push(recover);
        //        }
        //        $(source.parentNode).hide();
        //    }
        //}
    }
    catch (e) {
        console.log(e.message);
    }
}

function closeAuxRecover() {
    try {
        hide('.auxrecover');
        return false;
    }
    catch (e) {
        console.log(e.message);
    }
}
function isExistingAuxDetail(sourceParcel, sourceTable, index) {
    var pKeys = [], flag = false;
    if (sourceTable) {
        var keySet = tableKeys.filter(function (d) { return d.SourceTable == sourceTable; });
        (isEmpty(keySet)) ? '' : keySet.forEach(function (x) { pKeys.push(x.Name); });
    }
    else {
        flag = false;
    }
    if (sourceParcel && sourceParcel && sourceTable &&checkIsNull(index)) {
		var curRecord = activeParcel[sourceTable][index];
        if (!isEmpty([curRecord]) && curRecord != undefined) {
            //flag = (curRecord.ROWUID >= 0);
            flag = (curRecord.CC_recrodstatus == 'I' && curRecord.ClientROWUID)?false:true;
            if (pKeys) {
                pKeys.forEach(function (k) {
                    var value = eval('curRecord.' + k);
                    flag = checkIsNull(value) && flag;
                });
            }
        }
        else {
            flag = false;
        }
    }
    return flag;
}


function isEmpty(k) {
    var type = typeof k, retVal = false;
    switch (type) {
        case 'string': retVal = (k == '' || k == null || k == undefined); break;
        case 'object': retVal = (k) ? (k.length > 0 ? false : true) : true; break;
    }
    return retVal;
}

function getDescendantCategoryIds(CategoryId) {
    try {
        var childs = [], tempChildId = $('#AllCategories li[category="' + CategoryId + '"]').attr('childid');
        while (tempChildId != undefined) {
            childs.push(tempChildId);
            tempChildId = $('#AllCategories li[category="' + tempChildId + '"]').attr('childid');
        }
        return childs;
    } catch (e) {
        console.log(e.message);
    }
}

function getParentCategoryIds(CategoryId) {
    try {
        var parents = [], tempparentId = $('#AllCategories li[category="' + CategoryId + '"]').attr('parentid');
        while (tempparentId != undefined) {
            parents.push(tempparentId);
            tempparentId = $('#AllCategories li[category="' + tempparentId + '"]').attr('parentid');
        }
        return parents;
    } catch (e) {
        console.log(e.message);
    }
}
function getSourceTable(categoryId) {
    try {
        return (fieldCategories.filter(function (d) { return d.Id == categoryId; })[0]) ? fieldCategories.filter(function (d) { return d.Id == categoryId; })[0].SourceTable : null;
    }
    catch (e) {
        console.log(e.message);
    }
}

function getInnerHtml(CategoryID) {
    try {
        return getFieldCategoryName(CategoryID);
    }
    catch (e) {
        console.log(e.message);
    }
}

function getCategory(SourceTable) {
    try {
        return $('.category-page[auxdata="' + SourceTable + '"]').attr('categoryid');
    }
    catch (e) {
        console.log(e.message);
    }
}

function getFilterField(categoryId) {

    return (fieldCategories.filter(function (d) { return d.Id == categoryId; })[0]) ? fieldCategories.filter(function (d) { return d.Id == categoryId; })[0].FilterFields : null;
}

function getChildCategories(categoryId) {
    var childs = [], retVal = [];
    if (!__DTR && localStorage && localStorage.internalSupportView == '1' && localStorage.parcelDataSupportView == '1')
    	childs = fieldCategories.filter(function (d) { return d.ParentCategoryId == categoryId })
    else
    	childs = fieldCategories.filter(function (d) { return d.ParentCategoryId == categoryId && d.ShowCategory == 1; })
    childs = childs.sort(function(a,b) {return parseInt(a.Ordinal) - parseInt(b.Ordinal) ; })
    if (childs.length > 0) {
        for (x in childs) {
            retVal.push(childs[x].Id);
        }
    }
    return retVal;
}
function getCategory(categoryId) {
    return fieldCategories.filter(function (d) { return d.Id == categoryId; })[0];
}
//need change for prc markascomplete
var utils = {};
utils.MarkAsComplete = function () {
    showMask();
    if (activeParcel == null) {
        alert("You have to open a parcel to perform this action."); return false;
    }
    //    markasReviewInPRC();
    var review;
    var ss = "Parcel Marked as  Reviewed successfully";
    review = 1;
    var setQCCall = function (callback) {
        showMask();
        $qc('reviewparcel', { ParcelId: activeParcel.Id, Reviewed: review }, function (resp) {
            Notificationmsg = ss;
            if (callback) callback();
        });
    }
    var saved = checkIfSaved(true, true, setQCCall);
    saveParcelChanges();
    getParcel(activeParcel.Id)
    return false;
    alert("Parcel is Marked as reviewed.");
}
utils.OpenCamera = function () {
    showPhotos();
}
function HideDeleteByExpression( catID, block, sourceTable, findex, parentSource ){
    var cat = getCategory( catID );
    if ( !cat ) return
    if ( cat.EnableDeleteExpression && cat.EnableDeleteExpression != '' )
    {
        var isExp = activeParcel.EvalValue( sourceTable + '[' + findex + ']', cat.EnableDeleteExpression, null, parentSource );
        if ( isExp == true )
            $( '.del-item', block ).removeAttr('disabled')
        else
            $( '.del-item', block ).attr( 'disabled', 'disabled' );
    } else $( '.del-item', block ).removeAttr( 'disabled' )
}
function HideCategoryByExpression(catID, sourceTable, findex, parentSource) {
    var cat = getCategory(catID);
    if (!cat) return
    let isSupportView = (!__DTR && localStorage && localStorage.internalSupportView == '1' && localStorage.parcelDataSupportView == '1')? true: false;
    if (cat.HideExpression && cat.HideExpression != '' && cat.ParentCategoryId == null) {
        var isExp = activeParcel.EvalValue(sourceTable + '[' + findex + ']', cat.HideExpression, null, parentSource);
        if (isExp == true)
            $('.category-menu a[category=' + cat.Id + ']').hide();
        else
            $('.category-menu a[category=' + cat.Id + ']').show();
    }
    if (activeParcel[sourceTable] && activeParcel[sourceTable].length > 0) {
        var childcategories = fieldCategories.filter(function (f) { return (f.ParentCategoryId == catID && f.HideExpression && f.HideExpression != '') });
        childcategories.forEach(function (c) {
            var isExp = activeParcel.EvalValue(sourceTable + '[' + findex + ']', c.HideExpression.replace(new RegExp('parent.', 'g'), '').replace(new RegExp('&gt;', 'g'), '>').replace(new RegExp('&lt;', 'g'), '<'))
            if (isExp) {
                $('.childCategoryTitle[category=' + c.Id + ']').hide();
                $('.childCategoryTitle[category=' + c.Id + ']').removeClass('catVisible');
            }
            else {
                $('.childCategoryTitle[category=' + c.Id + ']').show();
                $('.childCategoryTitle[category=' + c.Id + ']').addClass('catVisible');
            }
        })
    }
}
/*
function getChildGridTemplate(cat) {
    var childTables = parentChild.filter(function (x) { return x.ParentTable == cat.SourceTable }).map(function (x) { return x.ChildTable });
    var childCategories = fieldCategories.filter(function (f) { return (f.ParentCategoryId == cat.Id && f.SourceTable != null && (f.DoNotShowInMultiGrid == false || f.DoNotShowInMultiGrid != 'true')); });
    var template = "<div class=' aux-multigrid-view' catid='" + cat.Id + "'>";
    template += "<div class='multigrid'></div><div class='hiddenGridTemplate' style='display:none';><table>";
    childCategories.forEach(function (cat) {
        template += "<tr cid=" + cat.Id + "><td style='padding-left: 14px;'><h3 style='background-color:silver;padding: 3px;font-size: 17px;height: 24px; margin: 0px; '>" + cat.Name + "</h3></td></tr><tr cid=" + cat.Id + "><td>";
        var fields = Object.keys(datafields).map(function (x) { return datafields[x] }).filter(function (x) { return x.CategoryId == cat.Id });
        fields = fields.sort(function (a, b) { return a.Serial - b.Serial; })
        template += getAuxTableTemplate(cat, fields, true);
        template += "</tr></td>";
    });
    template += '</table></div></div>';
    return template;
}
*/

function getChildGridTemplate(cat) {
   
    var childTables = parentChild.filter(function (x) { return x.ParentTable == cat.SourceTable }).map(function (x) { return x.ChildTable });
    var childCategories = fieldCategories.filter(function (f) { return (f.ParentCategoryId == cat.Id && f.SourceTable != null && (f.DoNotShowInMultiGrid == false || f.DoNotShowInMultiGrid != 'true')); });
    var template = "<div class=' aux-multigrid-view' catid='" + cat.Id + "'>";
    template += "<div class='multigrid'></div><div class='hiddenGridTemplate' style='display:none';><table>";
    childCategories.forEach(function (cat) {
        template += "<tr cid=" + cat.Id + "><td style='padding-left: 14px;'><h3 style='background-color:silver;padding: 3px;font-size: 17px;height: 24px; margin: 0px; '>" + cat.Name + "</h3></td></tr><tr cid=" + cat.Id + "><td>";
        var fields = Object.keys(datafields).map(function (x) { return datafields[x] }).filter(function (x) { return x.CategoryId == cat.Id });
        fields = fields.sort(function (a, b) { return a.Serial - b.Serial; })
       // template += getAuxTableTemplate(cat, fields, true)
        var checkgrid = getAuxTableTemplate(cat, fields, true);
        if (checkgrid)
            template += checkgrid
  
        template += "</tr></td>";

      
    });
    template += '</table></div></div>';
    

 	childCategories.forEach(function (cat) {
		 template+=	getsubChildGridTemplate(getCategory(cat.Id));
	});
    return template;
}


function getsubChildGridTemplate(cat) {
	var childTables = parentChild.filter(function (x) { return x.ParentTable == cat.SourceTable }).map(function (x) { return x.ChildTable });
    var childCategories = fieldCategories.filter(function (f) { return (f.ParentCategoryId == cat.Id && f.SourceTable != null && (f.DoNotShowInMultiGrid == false || f.DoNotShowInMultiGrid != 'true')); });
	var pcatId=getParentCategories(cat.Id);
 	var template = "<div class=' aux-sub-multigrid-view' catid='" + pcatId + "' pcatId='" + cat.Id + "'>";
	template += "<div class='sub-multigrid'></div><div class='subhiddenGridTemplate' style='display:none';><table>";
    childCategories.forEach(function (cat) {
        template += "<tr cid=" + cat.Id + "><td style='padding-left: 14px;'><h3 style='background-color:#e6dea1;padding: 3px;font-size: 17px;height: 24px; margin: 0px; '>" + cat.Name + "</h3></td></tr><tr cid=" + cat.Id + "><td>";
        var fields = Object.keys(datafields).map(function (x) { return datafields[x] }).filter(function (x) { return x.CategoryId == cat.Id });
        fields = fields.sort(function (a, b) { return a.Serial - b.Serial; })
       // template += getAuxTableTemplate(cat, fields, true)
        var checkgrid = getAuxTableTemplate(cat, fields, true);
        if (checkgrid)
            template += checkgrid
        template += "</tr></td>";
	
    });
    template += '</table></div></div>';
  
    return template;
}


function getAuxTableTemplate(category, fields, isContext) {
    var source = category.SourceTable;
    var header = "", body = "", footer = "";
    var falsetable = "";
    if (category.ShowGridOnly != "true") {
        header += "<th>&nbsp;</th>"
        footer += "<td class='aux-grid-edit-cell'><a class='aux-grid-edit' href='#'>View/Edit</a></td>"
    }
    var gridFields = fields.filter(function (f) { return f.ShowOnGrid == "true" || f.ShowOnGrid == true });
    gridFields.forEach(function (field) {
        var exprn = field.Name;
        header += "<th>" + field.Label + "</th>";
        var inputType = parseInt(field.InputType);
        var dt = QC.dataTypes[inputType];
        footer += "<td>${" + exprn + ":table," + field.SourceTable + "}</td>";
    });
    var context = isContext == true ? "context='" + source + "'" : "";
    header = "<thead>" + header + "</thead>";
    body = "<tbody " + context + "></tbody>";
    footer = "<tfoot style='display:none;'>" + footer + "</tfoot>";
   
    if (gridFields.length == 0 && source != "")
        return false;
    var tableHtml;
    if (isContext == true)
        tableHtml = "<div class='aux-grid-scroll'><table class='data-collection-table' source='" + source + "' border='1' style='width:100%;margin-top: 0px;' catid='" + category.Id + "'>" + header + body + footer + "</table></div>";
    else
        tableHtml = "<div class='aux-grid-view' style='display:none'; catid='" + category.Id + "'><div class='aux-grid-head'><a class='aux-grid-up a-button'>UP</a><a class='aux-grid-button aux-grid-switch a-button'>FORM</a>" + (__DTR ? "<a class='aux-grid-button new-item a-button' index='0' action='new'></a>" : "") + "<div class='cb'></div></div><div class='aux-grid-header'></div><div class='aux-grid-scroll'><table class='data-collection-table' source='" + source + "' border='1'>" + header + body + footer + "</table></div></div>";
    return tableHtml;

}
function gridView() {
    for (ci in fieldCategories) {
        var cat = fieldCategories[ci], fieldCatSql = '';
        getData('SELECT * FROM Field WHERE CategoryId =' + parseInt(cat.Id).toString() + ' ORDER BY Serial * 1', [], function (fields, cat) {
        	if (!__DTR && localStorage && localStorage.internalSupportView == '1' && localStorage.parcelDataSupportView == '1')
                fieldCatSql = 'SELECT * FROM FieldCategory WHERE ParentCategoryId =' + parseInt(cat.Id).toString() + ' ORDER BY Ordinal * 1';
        	else
                fieldCatSql = 'SELECT * FROM FieldCategory WHERE ParentCategoryId =' + parseInt(cat.Id).toString() + ' AND ShowCategory = 1 ORDER BY Ordinal * 1';
            getData(fieldCatSql, [], function (subcats, args) {
                var fields = args[0];
                var cat = args[1];
                if (cat.SortExpression) {
                    $('.category-page[categoryid="' + cat.Id + '"]').attr('sort-exp', cat.SortExpression);
                }
                var gridForm = $('.grid-view[categoryid="' + cat.Id + '"]');
                if (cat.TabularData == "true" || cat.TabularData == true) {
                    var att;
                    att = getAuxTableTemplate(cat, fields);
                    if (att) {
                        $(gridForm).append($(att));
                        $(gridForm).show();
                    }
                    if (cat.ShowGridOnly == "true" || cat.ShowGridOnly == true) {
                        $('.aux-grid-switch', $(gridForm)).hide();
                    }
                    if (cat.MultiGridStub == "true" || cat.MultiGridStub == true) {
                        var MultiGridTemplate = getChildGridTemplate(cat);
                        $(gridForm).append($(MultiGridTemplate));
                        $(gridForm).show();
                    }
                    if (!att) {
                        $('.grid-view-qc[categoryid="' + cat.Id + '"]').attr('disabled', 'disabled');
                    }
                } else {
                    $('.grid-view-qc[categoryid="' + cat.Id + '"]').attr('disabled', 'disabled');
                }
                $('.aux-grid-up', gridForm).attr('style', 'display:none;');
                if ((subcats.length > 0) || (cat.ParentCategoryId)) {
                    var auxhead = {
                        ParentCategoryId: cat.ParentCategoryId,
                        ShowBack: cat.ParentCategoryId ? '' : 'display:none;',
                        showback: cat.ParentCategoryId ? '' : 'display:none;',
                        subcats: subcats
                    }
                    $('.aux-grid-up', gridForm).attr('style', auxhead.ShowBack);
                    $('.aux-grid-up', gridForm).attr('target', auxhead.ParentCategoryId);
                }
            }, [fields, cat]);
        }, cat);
    }
}
function getGridViewData(categoryId, filteredIndexes) {
    var auxForm = $('.category-page[categoryid="' + categoryId + '"]');
    var cat = getCategory(categoryId);
    var sourceTable = $($('.category-page[categoryid="' + categoryId + '"]')).attr('auxdata');
    var sourceData = activeParcel[sourceTable];
    $(auxForm).attr('state', 'grid');
    if (!filteredIndexes) {
        filteredIndexes = [];
        for (x in sourceData) {
            filteredIndexes.push(x);
        }
    }
    for (x in filteredIndexes) {
        filteredIndexes[x] = filteredIndexes[x].toString();
    }
    var gridData = sourceData ? (sourceData.length != 0 ? sourceData.filter(function (d, i) { return filteredIndexes.indexOf(i.toString()) > -1; }) : []) : [];
    //var gridData = JSON.parse(JSON.stringify(gd));
    //if (sourceData)
    //    if (sourceData.length != 0)
    //        gridData = sourceData.filter(function (d, i) { return filteredIndexes.indexOf(i.toString()) > -1; });

    //var df = Object.keys(datafields).filter(function (x) { return datafields[x].SourceTable == sourceTable && datafields[x].InputType == '4' }).map(function (y) { return datafields[y] })
    //for (x in gridData) {
    //    for (y in df) {
    //        var fieldname = df[y].Name;
    //        if (gridData[x][fieldname]) {
    //            value = gridData[x][fieldname];
    //            gridData[x][fieldname] = dateTolocal(value);
    //        }
    //    }
    //}
    $('.aux-grid-view .data-collection-table tbody', auxForm).html($('.data-collection-table tfoot', auxForm).html());
    $('.aux-grid-view .data-collection-table tbody', auxForm).fillTemplate(gridData);
    var filteredPch = [];
    for (i = 0; i < gridData.length; i++) {
        filteredPch.push(activeParcel.ParcelChanges.filter(function (d, j) { return d.AuxROWUID == gridData[i].ROWUID && d.SourceTable == sourceTable; }));
    }
   // if (filteredPch.length > 0) {
       // for (x in filteredPch) {
          //  if (filteredPch[x].length && filteredPch[x][0].Action == "delete")
            //    $('.aux-grid-view .data-collection-table tbody tr', auxForm).eq(x).addClass('aux-deleted');
        //}
   // }
    for (x in gridData) {
        if (gridData[x].CC_Deleted == true) {
            let $drow = $('.aux-grid-view .data-collection-table tbody tr', auxForm).eq(x), gdclr = activeParcel?.QC ? 'green' : 'red';
            $drow.addClass('aux-deleted');
            $drow.find('td').eq(0).css('position', 'relative');
            $drow.find('.gridDeleteNotify').remove(); $drow.find('.gridEditNotify').remove();
            $drow.find('.aux-grid-edit').eq(0).before('<span class="gridDeleteNotify" style="position: absolute; left: -15px; font-size: medium; color: ' + gdclr + '; font-weight: bold;">X</span>');
            if ($('.aux-grid-view[catid="' + categoryId + '"] .gridDeleteNotify').length > 0) {
                $('.grid-view[categoryid="' + categoryId + '"]').find('.data-collection-table').eq(0).attr('style', 'margin-left : 10px;');
                $('.aux-multigrid-view[catid="' + categoryId + '"]').css('margin-left', '8px');
                $('.aux-sub-multigrid-view[catid="' + categoryId + '"]').css('margin-left', '8px');
            }
        }
    }

    $('.aux-grid-view .data-collection-table tbody .aux-grid-edit', auxForm).click(function (e) {
        e.preventDefault();
        $(auxForm).attr('index', $('.aux-grid-view .data-collection-table tbody .aux-grid-edit', auxForm).index(this));
        var index = parseInt($(auxForm).attr('index'));
        $(auxForm).attr('findex', filteredIndexes[index]);
        if (getParamOfAuxData(categoryId)) {
            var showAuxParams = getParamOfAuxData(categoryId)[0];
            showAuxData(auxForm, index, showAuxParams.filter_field, showAuxParams.filter_value, showAuxParams.parentROWUID);
        }
        $('.aux-records', auxForm).html(filteredIndexes.length);
        $('.aux-index', auxForm).html(index + 1);
        $(auxForm).attr('state', 'form');
        $('.data-navigator', auxForm).show();
        $('.heightdiv', auxForm).show();
        $('.sublevelDiv', auxForm).show();
        $('.parcel-field-values', auxForm).show();
        if (!__DTR && localStorage && localStorage.internalSupportView == '1' && localStorage.parcelDataSupportView == '1') 
    		$('.support__catHead', auxForm).show();
       // var ch = filteredPch.filter(function (d, i) {
          //  for (j = 0; j < d.length; j++) {
          //      if (d[j])
             //       return d[j].AuxROWUID == gridData[index].ROWUID;
          //  }
        //});
       // var deleteAction = false;
       // if (ch.length > 0) {
        //    ch = ch[0];
          //  for (x in ch) {
            //    if (ch[x].Action == "delete")
              //      deleteAction = true;
              //  else
                //    deleteAction = false;
            //}
      //  }
      // if (gridData[index].CC_Deleted == true || deleteAction) {
		var txtarea = $('.category-page[categoryid = "'+ categoryId +'"] textarea');
    	for(var x = 0;x < txtarea.length; x++){
			txtarea[x].style.cssText = 'height:auto';
			txtarea[x].style.cssText = 'height:' + txtarea[x].scrollHeight + 'px';
    	}
        if (gridData[index].CC_Deleted == true) {
            $('.auxrecover', auxForm).show();
            $('.childDiv', auxForm).hide();
        }
        $('.aux-grid-view[catid="' + categoryId + '"]').hide();
        $('.aux-multigrid-view[catid="' + categoryId + '"]').hide();
        $(auxForm).attr('state', 'form');
    });
    $('.grid-view-qc', auxForm).click(function (e) {
        e.preventDefault();
        $('#classCalculatorFrame').hide();
        var categoryId = $(auxForm).attr('categoryid');
        showGrid(auxForm, categoryId);
    });
    $('.grid-view .aux-grid-view .aux-grid-switch', auxForm).unbind('click');
    $('.grid-view .aux-grid-view .aux-grid-switch', auxForm).click(function (e) {
        e.preventDefault();
        var categoryId = $(auxForm).attr('categoryid');
        var index = $('.aux-grid-switch', auxForm).attr('index');
        showForm(auxForm, categoryId, index);
        if (gridData.length > 0) {
            if (gridData[index] && gridData[index].CC_Deleted == true) {
                $('.category-page[categoryid="' + categoryId + '"] .auxrecover').show();
                $('.childDiv', auxForm).hide();
            }
        }
    });
    $('.grid-view .aux-grid-view .new-item', auxForm).unbind('click');
    $('.grid-view .aux-grid-view .new-item', auxForm).click(function (e) {
        $('.data-navigator button.new-item', auxForm).trigger('click');
    });
    $('.aux-grid-up', auxForm).unbind('click');
    $('.aux-grid-up', auxForm).click(function (e) {
        e.preventDefault();
        var tcid = $(this).attr('target');
        var index = parseInt($( '.category-page[categoryid="' + tcid + '"] span.aux-index').html());
        var auxForm = $('.category-page[categoryid="' + tcid + '"]');
        var source = $($('.category-page[categoryid="' + tcid + '"]')).attr('auxdata');
        var showAuxParams = getParamOfAuxData(tcid)[0];
        showAuxData(auxForm, index-1, showAuxParams.filter_field, showAuxParams.filter_value, showAuxParams.parentROWUID);
        var filteredIndexes = getFilteredIndexOfAuxData(tcid, source);
        if ($('.category-page[categoryid="' + tcid + '"]').attr('state') == 'grid') {
            getGridViewData(tcid, filteredIndexes)
            loadMultiGrid(auxForm, index-1, filteredIndexes)
            highlightDeletedRecord(categoryId)
        }
        var parent = getParentCategories(tcid);
        loadParentChildOfCategory(tcid); // CC_2816 parent sublelvel click not working if first time up click from subchild grid up button.
        $('.currentCategoryTitle').html(getInnerHtml(tcid));
        if (parent)
            $('.parentCategoryTitle').html(getInnerHtml(parent));
        if (parent == null)
            $('.category-page[categoryid="' + tcid + '"] .parentCategoryDiv').hide();
        $('.category-page[categoryid="' + categoryId + '"]').hide();
        $('.category-page[categoryid="' + tcid + '"]').show();
        if ($('.category-page[categoryid="' + tcid + '"]').attr('state') == 'grid') {
            $('.sublevelDiv').hide();
        } else {
            $('.sublevelDiv').show();
        }
        $('.category-page[categoryid="' + tcid + '"] .childDiv').show();
        $('.parcel-data-title').html(getInnerHtml(tcid));
        highlightwithFilter(tcid);
    });
    $('.aux-grid-view .data-collection-table tbody tr', auxForm).click(function (e) {
        if ((cat.MultiGridStub == true || cat.MultiGridStub == "true") && !$(this).hasClass('multigridSelected')) {
            $(auxForm).attr('index', $('.aux-grid-view .data-collection-table tbody tr', auxForm).index(this));
            var index = parseInt($(auxForm).attr('index'));
            $(auxForm).attr('findex', filteredIndexes[index]);
            $('.aux-grid-switch', auxForm).attr('index', index);
            var showAuxParams = getParamOfAuxData(cat.Id)[0];
            showAuxData(auxForm, index, showAuxParams.filter_field, showAuxParams.filter_value, showAuxParams.parentROWUID);
            loadMultiGrid(auxForm, index, filteredIndexes);
        }
    });
    if(activeParcel.QC == true)
        $('.aux-grid-view[catid="'+categoryId+'"] td').children().removeClass('EditedValue');
   highlightDeletedRecord(categoryId);
}

function getnewGridView(categoryID,sourceTable,index)
{
	var auxForm = $('.category-page[categoryid="' + categoryID+ '"]');
	var st=0,count=0;
	var cat = getCategory(categoryID);
    var sourceData = activeParcel[sourceTable];
    var sourceDataAux;
    let indicesToRemove = [];
    const filteredFieldData = fieldCategories.filter(item => item.Id == categoryID && item.HideRecordExpression !== null);
    if (filteredFieldData.length > 0) {
        filteredFieldData.forEach(function (categ) {
            for (let uindex = 0; uindex < activeParcel[sourceTable].length; uindex++) {
                var isExp = activeParcel.EvalValue(sourceTable + '[' + uindex + ']', categ.HideRecordExpression);
                if (isExp && isExp != "****") {
                    indicesToRemove.push(uindex);
                }
            }

            sourceDataAux = activeParcel[sourceTable].filter(function (_, uindex) {
                return !indicesToRemove.includes(uindex);
            });
        });
    }


	if(getParentCategories(categoryID)){
		var pctid = getParentCategories(categoryID);
		var pForm=$('.category-page[categoryid="' + getParentCategories(categoryID)+ '"]');
		var prowid=$(pForm).attr('ROWUID');
    }
    if (sourceDataAux && (sourceDataAux.length > 0 || indicesToRemove.length > 0) ) {
        if (prowid) {
            try {
                var Griddata = sourceDataAux.filter(function (x) { return x.ParentROWUID == prowid })
            }

            catch (e) {
                alert('No table found for "' + cat.Name + '". ')
                return;
            }
            if (Griddata.length == 0) {
                Griddata = sourceDataAux.filter(function (x) { return x.ClientParentROWUID == prowid })
                prowid = Griddata.length > 0 ? Griddata[0].ParentROWUID : prowid;
            }
            if (Griddata.length == 0 && cat.SourceTable == getCategory(pctid).SourceTable) {
                Griddata = sourceDataAux.filter(function (x) { return x.ROWUID == prowid })
            }
            $(auxForm).attr('prowuid', prowid);
        }
        else
            var Griddata = sourceDataAux
    }
    else {

        if (prowid) {
            try {
                var Griddata = activeParcel[sourceTable].filter(function (x) { return x.ParentROWUID == prowid })
            }

            catch (e) {
                alert('No table found for "' + cat.Name + '". ')
                return;
            }
            if (Griddata.length == 0) {
                Griddata = activeParcel[sourceTable].filter(function (x) { return x.ClientParentROWUID == prowid })
                prowid = Griddata.length > 0 ? Griddata[0].ParentROWUID : prowid;
            }
            if (Griddata.length == 0 && cat.SourceTable == getCategory(pctid).SourceTable) {
                Griddata = activeParcel[sourceTable].filter(function (x) { return x.ROWUID == prowid })
            }
            $(auxForm).attr('prowuid', prowid);
        }
        else
            var Griddata = activeParcel[sourceTable]
    }


	$(auxForm).attr('state', 'grid');
	if(Griddata && Griddata.length>0 && index>-1)
		$(auxForm).attr('ROWUID',Griddata[index].ROWUID); 
	else
		$(auxForm).attr('ROWUID',''); 
	$('.aux-grid-view .data-collection-table tbody', auxForm).html($('.data-collection-table tfoot', auxForm).html());
    $('.aux-grid-view .data-collection-table tbody', auxForm).fillTemplate(Griddata);
    var filteredPch = [];
    if (Griddata)
    for (i = 0; i < Griddata.length; i++) {
        filteredPch.push(activeParcel.ParcelChanges.filter(function (d, j) { return d.AuxROWUID == Griddata[i].ROWUID && d.SourceTable == sourceTable; }));
        }

	for(st in Griddata) {
		$('.aux-grid-view .data-collection-table tbody tr', auxForm ).eq(st).attr('rowid',Griddata[st].ROWUID);
		//\$('.aux-grid-view .data-collection-table tbody tr', auxForm ).eq(st).attr('No',st);
        if (Griddata.length > 0 && Griddata[st].CC_Deleted == true) {
            let $drow = $('.aux-grid-view .data-collection-table tbody tr', auxForm).eq(st), gdclr = activeParcel?.QC ? 'green' : 'red';
            $drow.addClass('aux-deleted');
            $drow.find('td').eq(0).css('position', 'relative');
            $drow.find('.gridDeleteNotify').remove(); $drow.find('.gridEditNotify').remove();
            $drow.find('.aux-grid-edit').eq(0).before('<span class="gridDeleteNotify" style="position: absolute; left: -15px; font-size: medium; color: ' + gdclr + '; font-weight: bold;">X</span>');
            if ($('.aux-grid-view[catid="' + categoryID + '"] .gridDeleteNotify').length > 0) {
                $('.grid-view[categoryid="' + categoryID + '"]').find('.data-collection-table').eq(0).attr('style', 'margin-left : 10px;');
                $('.aux-multigrid-view[catid="' + categoryID + '"]').css('margin-left', '8px');
                $('.aux-sub-multigrid-view[catid="' + categoryID + '"]').css('margin-left', '8px');
            }
        }           
		count++;
    }

	$('.aux-grid-view .data-collection-table tbody .aux-grid-edit', auxForm).click(function (e) {
	 	e.preventDefault();
	 	relationcycle = [];
        $(auxForm).attr('index', $('.aux-grid-view .data-collection-table tbody .aux-grid-edit', auxForm).index(this));
		$(auxForm).attr('ROWUID', $(this).attr('rowid'));
        var index = parseInt($(auxForm).attr('index'));
        if (getParamOfAuxData(categoryID)) {
            var showAuxParams = getParamOfAuxData(categoryID)[0];
            showAuxData(auxForm, index, showAuxParams.filter_field, showAuxParams.filter_value, showAuxParams.parentROWUID);
        }
        $('.aux-records', auxForm).html(count);
        $('.aux-index', auxForm).html(index + 1);
        $(auxForm).attr('state', 'form');
        $('.data-navigator', auxForm).show();
        $('.heightdiv', auxForm).show();
        $('.sublevelDiv', auxForm).show();
        $('.parcel-field-values', auxForm).show();
        if (!__DTR && localStorage && localStorage.internalSupportView == '1' && localStorage.parcelDataSupportView == '1') 
    		$('.support__catHead', auxForm).show();
        if( Griddata[index].CC_Deleted == true) {
           	$('.auxrecover', auxForm).show();
            $('.childDiv', auxForm).hide();
        }
        $('.aux-grid-view[catid="' + categoryID+ '"]').hide();
		$('.aux-multigrid-view[catid="' + categoryID+ '"]').hide();
		$('.aux-sub-multigrid-view[catid="' + categoryID+ '"]').hide();
        $(auxForm).attr('state', 'form');
	});	
	showForm(auxForm, categoryID, index);
	var txtarea = $('.category-page[categoryid = "'+ categoryID+'"] textarea');
    	for(var x = 0;x < txtarea.length; x++){
			txtarea[x].style.cssText = 'height:auto';
			txtarea[x].style.cssText = 'height:' + txtarea[x].scrollHeight + 'px';
    	}
    	if (showfrm == true){
    	  showGrid(auxForm, categoryID);
    	    showfrm = false;
    	}
        if (Griddata && Griddata.length>0 && Griddata[index].CC_Deleted == true) {
            $('.auxrecover', auxForm).show();
            $('.childDiv', auxForm).hide();
        }
	 $('.grid-view-qc', auxForm).click(function (e) {
        e.preventDefault();
        if(disableSwitch == true ) return false;
        $('#classCalculatorFrame').hide();
        var categoryId = $(auxForm).attr('categoryid');
        showGrid(auxForm, categoryId);
    });
	$('.grid-view .aux-grid-view .aux-grid-switch', auxForm).unbind('click');
    $('.grid-view .aux-grid-view .aux-grid-switch', auxForm).click(function (e) {
        e.preventDefault();
        var categoryId = $(auxForm).attr('categoryid');
        var index = $('.aux-grid-switch', auxForm).attr('index');
        showForm(auxForm, categoryId, index);
        if (Griddata && Griddata.length > 0) {
            if (Griddata[index] && Griddata[index].CC_Deleted == true) {
                $('.category-page[categoryid="' + categoryId + '"] .auxrecover').show();
                $('.childDiv', auxForm).hide();
            }
        }
    });
    $('.grid-view .aux-grid-view .new-item', auxForm).unbind('click');
    $('.grid-view .aux-grid-view .new-item', auxForm).click(function (e) {
        $('.data-navigator button.new-item', auxForm).trigger('click');
    });

	$('.aux-grid-up', auxForm).unbind('click');
    $('.aux-grid-up', auxForm).click(function (e) {
        e.preventDefault();
        var tcid = $(this).attr('target');
        var index = parseInt($( '.category-page[categoryid="' + tcid + '"] span.aux-index').html());
        var auxForm = $('.category-page[categoryid="' + tcid + '"]');
        var source = $($('.category-page[categoryid="' + tcid + '"]')).attr('auxdata');
        var showAuxParams = getParamOfAuxData(tcid)[0];
		var FORM = $('.category-page[categoryid="' + categoryID + '"]');
		var rowid=$(FORM).attr('ROWUID')
		var sourcetable1=getSourceTable(categoryID)
        loadParentChildOfCategory(tcid); // CC_2816 parent sublelvel click not working if first time up click from subchild grid up button
        showAuxData(auxForm, index-1, showAuxParams.filter_field, showAuxParams.filter_value, showAuxParams.parentROWUID);
        if ($('.category-page[categoryid="' + tcid + '"]').attr('state') == 'grid') {
            getnewGridView(tcid,source,index-1 );
            loadnewMultiGrid(auxForm,index-1,rowid,sourcetable1);
            highlightDeletedRecord(categoryID);
        }
        var parent = getParentCategories(tcid);
        $('.currentCategoryTitle').html(getInnerHtml(tcid));
        if (parent)
            $('.parentCategoryTitle').html(getInnerHtml(parent));
        if (parent == null)
            $('.category-page[categoryid="' + tcid + '"] .parentCategoryDiv').hide();
        $('.category-page[categoryid="' + categoryID + '"]').hide();
        $('.category-page[categoryid="' + tcid + '"]').show();
        if ($('.category-page[categoryid="' + tcid + '"]').attr('state') == 'grid') {
            $('.sublevelDiv').hide();
        } else {
            $('.sublevelDiv').show();
            $('.multigrid', auxForm).hide();
            $('.sub-multigrid',auxForm).hide();
        }
        $('.category-page[categoryid="' + tcid + '"] .childDiv').show();
        $('.parcel-data-title').html(getInnerHtml(tcid));
        highlightwithFilter(tcid);
    });
	$('.aux-grid-view .data-collection-table tbody tr', auxForm).click(function (e) {
        if ((cat.MultiGridStub == true || cat.MultiGridStub == "true") && !$(this).hasClass('multigridSelected')) {
           showfrm = true;
            $(auxForm).attr('index',$('.aux-grid-view .data-collection-table tbody tr', auxForm).index(this));
            var index = parseInt($(auxForm).attr('index'));
	    	var rowid=$(this).attr('rowid')
	    	$(auxForm).attr('ROWUID',rowid );
            $('.aux-grid-switch', auxForm).attr('index', index);
            var showAuxParams = getParamOfAuxData(cat.Id)[0];
            showAuxData(auxForm, index, showAuxParams.filter_field, showAuxParams.filter_value, showAuxParams.parentROWUID);
            loadnewMultiGrid(auxForm, index);
        }
    });
    if(activeParcel.QC == true)
        $('.aux-grid-view[catid="'+categoryID+'"] td').children().removeClass('EditedValue');
   highlightDeletedRecord(categoryID);
}

/*
function showGrid(auxForm, categoryId) {
    if (categoryId != 'photo-viewer') {
        $('.data-navigator', auxForm).hide();
        $('.heightdiv', auxForm).hide();
        $('.sublevelDiv', auxForm).hide();
        $('.parcel-field-values', auxForm).hide();
        $('.aux-grid-view[catid="' + categoryId + '"]').show();
        $('.aux-multigrid-view[catid="' + categoryId + '"]').show();
        $('.auxrecover', auxForm).hide();
        $(auxForm).attr('state', 'grid');
        if(activeParcel.QC == true)
        $('.aux-grid-view[catid="'+categoryId+'"] td').children().removeClass('EditedValue');
        highlightDeletedRecord(categoryId);
    }
}
function showForm(auxForm, categoryId, index) {
    var child = getChildCategories(categoryId);
    $('.aux-grid-view[catid="' + categoryId + '"]').hide();
    $('.aux-multigrid-view[catid="' + categoryId + '"]').hide();
    $('.data-navigator', auxForm).show();
    $('.heightdiv', auxForm).show();    
    if (child.length > 0) {
        $('.childDiv', auxForm).show();
    }
    else {
        $('.childDiv', auxForm).hide();
    }
    if (index > -1) {
    	$('.sublevelDiv', auxForm).show();
        $('.parcel-field-values', auxForm).show();
    }
    $(auxForm).attr('state', 'form');
}
*/

function showGrid(auxForm, categoryId) {
    if (categoryId != 'photo-viewer') {
        $('.data-navigator', auxForm).hide();
        $('.heightdiv', auxForm).hide();
        $('.sublevelDiv', auxForm).hide();
        $('.parcel-field-values', auxForm).hide();
        $('.aux-grid-view[catid="' + categoryId + '"]').show();
        $('.aux-multigrid-view[catid="' + categoryId + '"]').show();
		$('.aux-sub-multigrid-view[catid="' + categoryId + '"]').show();
        $('.auxrecover', auxForm).hide();
        $('.support__catHead').hide();
        $(auxForm).attr('state', 'grid');
        if(activeParcel.QC == true)
        $('.aux-grid-view[catid="'+categoryId+'"] td').children().removeClass('EditedValue');
        highlightDeletedRecord(categoryId);
    }
}
function showForm(auxForm, categoryId, index) {
    var child = getChildCategories(categoryId);
    $('.aux-grid-view[catid="' + categoryId + '"]').hide();
    $('.aux-multigrid-view[catid="' + categoryId + '"]').hide();
    $('.aux-sub-multigrid-view[catid="' + categoryId + '"]').hide();
    $('.data-navigator', auxForm).show();
    $('.heightdiv', auxForm).show();    
    if (child.length > 0) {
        $('.childDiv', auxForm).show();
    }
    else {
        $('.childDiv', auxForm).hide();
    }
    if (index > -1) {
    	$('.sublevelDiv', auxForm).show();
        $('.parcel-field-values', auxForm).show(); 
    }
    if (!__DTR && localStorage && localStorage.internalSupportView == '1' && localStorage.parcelDataSupportView == '1') 
    		$('.support__catHead', auxForm).show();
    $(auxForm).attr('state', 'form');
}

function loadMultiGrid(targetForm, index, filteredIndexes) {
    var thisData = $(targetForm).attr('auxdata');
    var sourceData = activeParcel[thisData];
    var catid = $(targetForm).attr('categoryid');
    var prCatid = getParentCategories(catid);
    prCatid? parentTable = parentChild.filter(function(s){return s.ChildTable == thisData}).map(function(m){return m.ParentTable})[0]:parentTable = thisData;
    var prIndex = $('.category-page[categoryid="' + prCatid + '"] span.aux-index').attr('findex');
    prIndex = prIndex? prIndex : "0";
    $(targetForm).attr('findex', filteredIndexes[index]);
    var findex = $(targetForm).attr('findex');
    var deletedRecord = false;
    if (sourceData.length > 0 && activeParcel[thisData][findex]) {
        deletedRecord = activeParcel[thisData][findex].CC_Deleted;
        if (deletedRecord) {
            $('.multigrid', targetForm).html($('.hiddenGridTemplate', targetForm).html());
            $('.multigrid', targetForm).hide();
            return;
        }
        else {
            $('.multigrid', targetForm).show();
        }
    }
    if (!filteredIndexes) {
        filteredIndexes = [];
        for (x in sourceData) {
            filteredIndexes.push(x);
        }
    }
    for (x in filteredIndexes) {
        filteredIndexes[x] = filteredIndexes[x].toString();
    }
    var count = 0;
    var parentpath = 'activeParcel' + '.' + thisData + '[' + findex + ']';
    var childObject = {}
    var childTables = parentChild.filter(function (s) { return s.ParentTable == thisData })
   if (activeParcel[parentTable] && activeParcel[parentTable].length > 0 && (parentTable == thisData? true: activeParcel[parentTable][parseInt(prIndex)][thisData].length > 0))
        childTables.forEach(function (item) {
            if (!getCategoryFromSourceTable(item.ChildTable)) {
                alert('The category for the table "' + item.ChildTable + '" is missing. Please use "Update System Data" if you already added or add a category for the table "' + item.ChildTable + '".');
            }
            else if (!getCategoryFromSourceTable(item.ChildTable).DoNotShowInMultiGrid || getCategoryFromSourceTable(item.ChildTable).DoNotShowInMultiGrid=="false" )
                childObject[item.ChildTable] = eval(parentpath + "." + item.ChildTable) || [];
        });
    $('.multigrid', targetForm).html($('.hiddenGridTemplate', targetForm).html());
    for (var i = 0; i < $('.multigrid .data-collection-table', targetForm).length; i++) {
        $('tbody', $('.multigrid .data-collection-table', targetForm)[i]).html($('tfoot', $('.multigrid .data-collection-table', targetForm)[i]).html());
    }
    $('.multigrid', targetForm).fillTemplate([childObject]);
    for (x in childObject) {
        if (childObject[x].length > 0) {
        	var cat = getCategoryFromSourceTable(x);
            for (y in childObject[x]) {
                if (getCategory($(targetForm).eq(0).attr('categoryid')))
                    if (childObject[x][y].CC_Deleted == true && getCategoryFromSourceTable(Object.keys(childObject)[count]).ParentCategoryId == $(targetForm).eq(0).attr('categoryid')) {
                        let $drow = $('.multigrid .data-collection-table[source="' + x + '"] tbody tr', targetForm).eq(y), gdclr = activeParcel?.QC ? 'green' : 'red';
                        $drow.addClass('aux-deleted');
                        $drow.find('td').eq(0).css('position', 'relative');
                        $drow.find('.gridDeleteNotify').remove(); $drow.find('.gridEditNotify').remove();
                        $drow.find('.aux-grid-edit').eq(0).before('<span class="gridDeleteNotify" style="position: absolute; left: -15px; font-size: medium; color: ' + gdclr + '; font-weight: bold;">X</span>');
                    }
            }
			highlightDeletedRecord(cat.Id,catid);
        }
        count++;
    }
    count = 0;
    $('.aux-grid-view .data-collection-table tbody tr', targetForm).removeClass('multigridSelected');
    $('.aux-grid-view .data-collection-table tbody tr', targetForm).eq(index).addClass('multigridSelected');
    $('.multigrid .aux-grid-edit', targetForm).click(function (e) {
        e.preventDefault();
        var parentgrid = $(this).parents('.data-collection-table').first(':eq(1)');
        var sourceTable = $(parentgrid).attr('source')
        var sourcedata = activeParcel[sourceTable];
        var catgID = $(parentgrid).attr('catid');
        var formView = $('.category-page[categoryid="' + catgID + '"]');
        $(formView).attr('index', $('.aux-grid-edit', parentgrid).index(this));
        var index = parseInt($(formView).attr('index'));
        var showAuxParams = getParamOfAuxData(catgID)[0];
        var filteredIndexes = getFilteredIndexOfAuxData(catgID, sourceTable);
        var grid = sourcedata.filter(function (d, i) { return filteredIndexes.indexOf(i) > -1; });
        $(formView).attr('findex', filteredIndexes[index]);
        showAuxData(formView, index, showAuxParams.filter_field, showAuxParams.filter_value, showAuxParams.parentROWUID);
        $('.aux-records', formView).html(filteredIndexes.length);
        $('.aux-index', formView).html(index + 1);
        $(formView).attr('state', 'form');
        selectTab(catgID, index);
        var parent = getParentCategories(catgID);
        $('.currentCategoryTitle').html(getInnerHtml(catgID));
        if (parent)
            $('.parentCategoryTitle').html(getInnerHtml(parent));
        $('.category-page[categoryid="' + catgID + '"] .parentCategoryDiv').show();
        $('.parcel-data-title').html(getInnerHtml(catgID));
        var txtarea = $('.category-page[categoryid = "'+ catgID +'"] textarea');
    	for(var x = 0;x < txtarea.length; x++){
			txtarea[x].style.cssText = 'height:auto';
			txtarea[x].style.cssText = 'height:' + txtarea[x].scrollHeight + 'px';
    	}
        loadParentChildOfCategory(catgID);
        loadChildCategories(catgID);
        refreshAuxCount(catgID,formView)
        if (grid.length > 0) {
            if (grid[index].CC_Deleted == true) {
                $('.category-page[categoryid="' + catgID + '"] .auxrecover').show();
                $('.childDiv').hide();
            }
        }
    });
    
    let isSupportView = (!__DTR && localStorage && localStorage.internalSupportView == '1' && localStorage.parcelDataSupportView == '1')? true: false;
    var childcategories = fieldCategories.filter(function (f) { return (f.ParentCategoryId == $(targetForm).attr('categoryid') && f.HideExpression && f.HideExpression != '') });
    childcategories.forEach(function (c) {
        var isExp = activeParcel.EvalValue(thisData + '[' + index + ']', c.HideExpression.replace(new RegExp('parent.', 'g'), ''))
        if (isExp) $('.multigrid tr[cid="' + c.Id + '"]', targetForm).hide()
    })
    if(activeParcel.QC == true)
		$('.aux-multigrid-view[catid="'+catid+'"] .multigrid tbody tr .data-collection-table td').children().removeClass('EditedValue');
}

function loadnewMultiGrid(targetForm,index, rrowuid, sourcetable1) {
    var thisData = $(targetForm).attr('auxdata');
    var sourceData = activeParcel[thisData];
    var catid = $(targetForm).attr('categoryid');
    var prCatid = getParentCategories(catid);
	var rowid = $(targetForm).attr('ROWUID')
    //prCatid? parentTable = parentChild.filter(function(s){return s.ChildTable == thisData}).map(function(m){return m.ParentTable})[0]:parentTable = thisData;
    //var prIndex = $('.category-page[categoryid="' + prCatid + '"] span.aux-index').attr('findex');
    //prIndex = prIndex? prIndex : "0";
    var deletedRecord = false;
    if (sourceData && sourceData.length > 0 && rowid) {
    	if (activeParcel[thisData].filter(function (t) { return (t.ROWUID == rowid) })[0] != undefined)
        	deletedRecord = activeParcel[thisData].filter(function (t) { return (t.ROWUID==rowid) })[0].CC_Deleted;
        if (deletedRecord) {
            $('.multigrid', targetForm).html($('.hiddenGridTemplate', targetForm).html());
            $('.multigrid', targetForm).hide();
            $('.sub-multigrid',targetForm).hide();
            return;
        }
        else {
            $('.multigrid', targetForm).show();
        }
    }
    var count = 0, childObject = {}, childObjectCopy = {};;
    var parentpath  = activeParcel[thisData].filter(function (t) { return (t.ROWUID == rowid)})[0];
   // var childTables = parentChild.filter(function (s) { return s.ParentTable == thisData })
   var childTables = fieldCategories.filter(function(f) { return f.ParentCategoryId == catid });
   if (activeParcel[thisData].length > 0 && parentpath != undefined)
        childTables.forEach(function (item) {
            if (!getCategoryFromSourceTable(item.SourceTable)) {
                alert('The category for the table "' + item.SourceTable + '" is missing. Please use "Update System Data" if you already added or add a category for the table "' + item.SourceTable + '".');
            }
            else if (!item.DoNotShowInMultiGrid || item.DoNotShowInMultiGrid == "false" ) {
            	childObject[item.SourceTable] = ( (thisData == item.SourceTable) ? ( parentpath? [parentpath]: [] ) : ( parentpath? parentpath[item.SourceTable]: [] || [] ) );
                for (const table in childObject) {
                    if (Array.isArray(childObject[table])) {
                        let indicesToRemove = [];
                        const filteredFieldData = fieldCategories.filter(item => item.SourceTable == table && item.HideRecordExpression !== null);
                        if (filteredFieldData.length > 0) {
                            filteredFieldData.forEach(function (categ) {
                                for (let uindex = 0; uindex < activeParcel[table].length; uindex++) {
                                    var isExp = activeParcel.EvalValue(table + '[' + uindex + ']', categ.HideRecordExpression);
                                    if (isExp) {
                                        indicesToRemove.push(uindex);
                                    }
                                }
                                childObject[table] = activeParcel[table].filter(function (_, uindex) {
                                    return !indicesToRemove.includes(uindex);
                                });
                            });
                        }
                    }
                }
                childObjectCopy[item.Id] = childObject[item.SourceTable]; // changes added for handle same sourceTable category. If more than one category having same sourceTable then first category is selecting. Now category working based on categoryId not sourecTable.
            }
        });
    $('.multigrid', targetForm).html($('.hiddenGridTemplate', targetForm).html());
    for (var i = 0; i < $('.multigrid .data-collection-table', targetForm).length; i++) {
        $('tbody', $('.multigrid .data-collection-table', targetForm)[i]).html($('tfoot', $('.multigrid .data-collection-table', targetForm)[i]).html());
    }
    $('.multigrid', targetForm).fillTemplate([childObject]);
    if(childTables.length > 0)
		 $('.sub-multigrid',targetForm).hide();	
	var c = 0, chidcatry = getChildCategories(catid);
    for (x in childObjectCopy) {
        if (childObjectCopy[x] && childObjectCopy[x].length > 0) {
			var i = 0, cat = getCategory(x), sTableName = cat.SourceTable, stable;
			if (rrowuid) {
				$(targetForm).attr('ChildROWUID',rrowuid)
				stable = sTableName;
            }
			else {
				if (c == 0) {
					stable = sTableName;
					var chrowid = childObjectCopy[x][0].ROWUID;
					$(targetForm).attr('ChildROWUID', chrowid)
					c++;
           		}
            }
            for (y in childObjectCopy[x]) {
                if (getCategory($(targetForm).eq(0).attr('categoryid')))
	                if (childObjectCopy[x][y].CC_Deleted == true && getCategoryFromSourceTable(Object.keys(childObjectCopy)[count]).ParentCategoryId == $(targetForm).eq(0).attr('categoryid')) {
                        let $drow = $('.multigrid .data-collection-table[source="' + sTableName + '"] tbody tr', targetForm).eq(y), gdclr = activeParcel?.QC ? 'green' : 'red';
                        $drow.addClass('aux-deleted');
                        $drow.find('td').eq(0).css('position', 'relative');
                        $drow.find('.gridDeleteNotify').remove(); $drow.find('.gridEditNotify').remove();
                        $drow.find('.aux-grid-edit').eq(0).before('<span class="gridDeleteNotify" style="position: absolute; left: -15px; font-size: medium; color: ' + gdclr + '; font-weight: bold;">X</span>');
	                }
				$('.multigrid .data-collection-table[source="' + sTableName + '"] tbody tr', targetForm).eq(i).attr('rowid', childObjectCopy[x][y].ROWUID);
				i++;
				
            }

            for (i = 0; i < chidcatry.length; i++){ // To highlight deleted records of sibling categories which have same source table and records.
               var currCat = chidcatry[i];
               var len = $('.multigrid .data-collection-table[source="' + sTableName + '"][catid="' + currCat + '"]  tbody tr').length;
               if (len) {
                  for (s = 0; s < len; s++) {	
                      if (childObjectCopy[x][s] && childObjectCopy[x][s].CC_Deleted == true) {
                          let $drow = $('.multigrid .data-collection-table[source="' + sTableName + '"][catid="' + currCat + '"] tbody tr').eq(s), gdclr = activeParcel?.QC ? 'green' : 'red';
                          $drow.addClass('aux-deleted');
                          $drow.find('td').eq(0).css('position', 'relative');
                          $drow.find('.gridDeleteNotify').remove(); $drow.find('.gridEditNotify').remove();
                          $drow.find('.aux-grid-edit').eq(0).before('<span class="gridDeleteNotify" style="position: absolute; left: -15px; font-size: medium; color: ' + gdclr + '; font-weight: bold;">X</span>');
                      } 
                  }
               }
            }

			highlightDeletedRecord(cat.Id, catid);
			if (rrowuid && stable == sourcetable1) {
				loadMultiGridChild(targetForm, stable, rrowuid, cat.Id);
				rrowuid = null;
            }
			else if (chrowid) {
				loadMultiGridChild(targetForm, stable, chrowid, cat.Id);
				chrowid = null;
       		}
        }
        count++;
    }
    count = 0;
    $('.aux-grid-view .data-collection-table tbody tr', targetForm).removeClass('multigridSelected');
    $('.aux-grid-view .data-collection-table tbody tr', targetForm).eq(index).addClass('multigridSelected');
    $('.multigrid .aux-grid-edit', targetForm).click(function (e) {
    	shomgrid=1;
        e.preventDefault();
        relationcycle = [];
        var parentgrid = $(this).parents('.data-collection-table').first(':eq(1)');
        var sourceTable = $(parentgrid).attr('source')  
        var catgID = $(parentgrid).attr('catid');
		var auxForm=$('.category-page[categoryid="' +getParentCategories(catgID) + '"]');
        var formView = $('.category-page[categoryid="' + catgID + '"]');
		var pRowuid=$(auxForm).attr('ROWUID')
		$(formView).attr('PROWUID', pRowuid);
        $(formView).attr('index', $('.aux-grid-edit', parentgrid).index(this));
		var sourcedata = activeParcel[sourceTable].filter(function(x){return x.ParentROWUID==pRowuid});
        var index = parseInt($(formView).attr('index'));
        var showAuxParams = getParamOfAuxData(catgID)[0];
        showAuxData(formView, index, showAuxParams.filter_field, showAuxParams.filter_value, showAuxParams.parentROWUID);
        $('.aux-records', formView).html(sourcedata.length);
        $('.aux-index', formView).html(index + 1);
        $(formView).attr('state', 'form');
        selectTab(catgID, index);
        var parent = getParentCategories(catgID);
        $('.currentCategoryTitle').html(getInnerHtml(catgID));
        if (parent)
            $('.parentCategoryTitle').html(getInnerHtml(parent));
        $('.category-page[categoryid="' + catgID + '"] .parentCategoryDiv').show();
        $('.parcel-data-title').html(getInnerHtml(catgID));
        var txtarea = $('.category-page[categoryid = "'+ catgID +'"] textarea');
    	for(var x = 0;x < txtarea.length; x++){
			txtarea[x].style.cssText = 'height:auto';
			txtarea[x].style.cssText = 'height:' + txtarea[x].scrollHeight + 'px';
    	}
        loadParentChildOfCategory(catgID);
        loadChildCategories(catgID);
        refreshAuxCount(catgID,formView)
        if (sourcedata.length > 0) {
            if (sourcedata[index].CC_Deleted == true) {
                $('.category-page[categoryid="' + catgID + '"] .auxrecover').show();
                $('.childDiv').hide();
            }
        }
        highlightwithFilter(catgID);
    });
	$('.multigrid .data-collection-table tbody tr', targetForm).click(function (e) {
		var catid = $(this).parents('tr').attr('cid');
		var cate = getCategory(catid);
        if ((cate.MultiGridStub == true || cate.MultiGridStub == "true") && !$(this).hasClass('multigridSelected'))  {
            shwfrm = true;
            var rowid = $(this).attr('rowid'); 
	    	$('.multigrid tbody .data-collection-table tbody tr ', targetForm).removeClass('multigridSelected');  
            $(this).addClass('multigridSelected');
            if(shomgrid==0){
             	$('.aux-grid-view[catid="' + catid + '"]').show();
             	shomgrid++;
       		}
            if (getChildCategories(catid).length > 0){
				$(targetForm).attr('index', $('.aux-multigrid-view .multigrid .data-collection-table[catid="'+ catid +'"] tbody tr', targetForm).index(this));
	            var index = parseInt($(targetForm).attr('index')); 
				var auxForm = $('.category-page[categoryid="' + catid + '"]');
	            var showAuxParams = getParamOfAuxData(catid)[0];
				showAuxData(auxForm, index, showAuxParams.filter_field, showAuxParams.filter_value, showAuxParams.parentROWUID);
	            loadMultiGridChild(targetForm, cate.SourceTable, rowid);
            }
			else{
					$('.aux-sub-multigrid-view[catid="' + $(targetForm).attr('categoryid') + '"]').hide();
				}
        }
	});
	
	let isSupportView = (!__DTR && localStorage && localStorage.internalSupportView == '1' && localStorage.parcelDataSupportView == '1')? true: false;
    var childcategories = fieldCategories.filter(function (f) { return (f.ParentCategoryId == $(targetForm).attr('categoryid') && f.HideExpression && f.HideExpression != '') });
    childcategories.forEach(function (c) {
        var isExp = activeParcel.EvalValue(thisData + '[' + index + ']', c.HideExpression.replace(new RegExp('parent.', 'g'), ''))
        if (isExp) $('.multigrid tr[cid="' + c.Id + '"]', targetForm).hide()
    })
    if(activeParcel.QC == true)
		$('.aux-multigrid-view[catid="'+catid+'"] .multigrid tbody tr .data-collection-table td').children().removeClass('EditedValue');

}

function loadMultiGridChild(targetForm, childTable, rowid, chldId) {
    var thisData = childTable;
    var thisdataparent = $(targetForm).attr('auxdata');
    var sourceData = activeParcel[thisData];
    var catid = $(targetForm).attr('categoryid');
    var childCat = getCategoryFromSourceTableCopy(childTable, chldId); 
	var childview = $('.category-page[categoryid="' + childCat.Id + '"]');
	$(childview).attr('ROWUID', rowid);
	var index = activeParcel[thisData].indexOf(activeParcel[thisData].filter(function(x){return x.ROWUID == rowid})[0])
	var	parentpath = activeParcel[thisData].filter(function (t) { return (t.ROWUID == rowid) })[0];
	
	if ( childCat.ParentCategoryId ) { //to show only seleted category child in childmultigrid view
        var childcategories = fieldCategories.filter(function (f) { return ( f.ParentCategoryId == childCat.ParentCategoryId ) });
		childcategories.forEach(function (c) {
			$('.aux-sub-multigrid-view[pcatid="' + c.Id + '"] .sub-multigrid', targetForm).hide();
		});
    }
	
    if (sourceData.length > 0 && parentpath) {
        if (parentpath.CC_Deleted) {
            $('.aux-sub-multigrid-view[pcatid="'+childCat.Id+'"] .sub-multigrid', targetForm).html($('.aux-sub-multigrid-view[pcatid="'+childCat.Id+'"] .subhiddenGridTemplate', targetForm).html());
            $('.aux-sub-multigrid-view[pcatid="'+childCat.Id+'"] .sub-multigrid', targetForm).hide();
            return;
        }
        else 
            $('.aux-sub-multigrid-view[pcatid="'+childCat.Id+'"] .sub-multigrid', targetForm).show();
    }
    
    var count = 0, childObject = {}, childObjectCopy = {}, subChildCat;
    //var childTables = parentChild.filter(function (s) { return s.ParentTable == thisData });
    var childTables = fieldCategories.filter(function (f) { return ( f.ParentCategoryId == childCat.Id ) });
	if (activeParcel[thisData].length > 0)
        childTables.forEach(function (item) {
        	subChildCat = item; //getCategoryFromSourceTable(item.ChildTable);
            if (!subChildCat) {
                alert('The category for the table "' + item.SourceTable + '" is missing. Please use "Update System Data" if you already added or add a category for the table "' + item.SourceTable + '".');
            }
            else if (!subChildCat.DoNotShowInMultiGrid || subChildCat.DoNotShowInMultiGrid == "false" ) {
                childObject[item.SourceTable] = ( (thisData == item.SourceTable) ? ( parentpath? [parentpath]: [] ) : ( parentpath? parentpath[item.SourceTable]: [] || [] ) );
                childObjectCopy[subChildCat.Id] = childObject[item.SourceTable];
            }
        });
    
    var subMultigridSelector = $('.aux-sub-multigrid-view[pcatid="'+ childCat.Id +'"]', targetForm)
    
    $('.sub-multigrid',subMultigridSelector).html($('.subhiddenGridTemplate', subMultigridSelector).html());
    for (var i = 0; i < $('.sub-multigrid .data-collection-table', subMultigridSelector).length; i++) {
        $('tbody', $('.sub-multigrid .data-collection-table', subMultigridSelector)[i]).html($('tfoot', $('.sub-multigrid .data-collection-table', subMultigridSelector)[i]).html());
    }
    $('.sub-multigrid', subMultigridSelector).fillTemplate([childObject]);
    for (x in childObjectCopy) {
        if (childObjectCopy[x].length > 0) {
        	var cat = getCategory(x);
            for (y in childObjectCopy[x]) {
                if (getCategory($(targetForm).eq(0).attr('categoryid')))
                    if (childObjectCopy[x][y].CC_Deleted == true /*&& getCategoryFromSourceTable(Object.keys(childObject)[count]).ParentCategoryId == $(targetForm).eq(0).attr('categoryid')*/) {
                        let $drow = $('.sub-multigrid .data-collection-table[source="' + cat.SourceTable + '"] tbody tr', subMultigridSelector).eq(y), gdclr = activeParcel?.QC ? 'green' : 'red';
                        $drow.addClass('aux-deleted');
                        $drow.find('td').eq(0).css('position', 'relative');
                        $drow.find('.gridDeleteNotify').remove(); $drow.find('.gridEditNotify').remove();
                        $drow.find('.aux-grid-edit').eq(0).before('<span class="gridDeleteNotify" style="position: absolute; left: -15px; font-size: medium; color: ' + gdclr + '; font-weight: bold;">X</span>');
                    }
            }
			highlightDeletedRecord(cat.Id, catid, childCat.Id);
        }
        count++;
    }
    count = 0;
	if (childTables.length > 0){
    	if($('.aux-grid-view[catid="' + catid + '"]').css("display") == 'block')
			$('.aux-sub-multigrid-view[catid="' + catid + '"]').show();
    }
	else {
		$('.aux-sub-multigrid-view[catid="' + catid + '"]').hide();
	}
	if(childTables.length > 0)
		$('.multigrid tbody .data-collection-table tbody tr[rowid="'+ rowid +'"]', targetForm).addClass('multigridSelected');
	$('.sub-multigrid .aux-grid-edit', targetForm).click(function (e) {
    	 e.preventDefault();
        var parentgrid = $(this).parents('.data-collection-table').first(':eq(1)');
        var sourceTable = $(parentgrid).attr('source')  
        var catgID = $(parentgrid).attr('catid');
		var auxForm=$('.category-page[categoryid="' +getParentCategories(catgID) + '"]');
        var formView = $('.category-page[categoryid="' + catgID + '"]');
		var pRowuid=$(auxForm).attr('ROWUID')
		$(formView).attr('PROWUID', pRowuid);
        $(formView).attr('index', $('.aux-grid-edit', parentgrid).index(this));
        if((catgID) && (activeParcel[sourceTable].filter(function(x){return x.ROWUID==pRowuid}).length>0))
        	var sourcedata= activeParcel[sourceTable].filter(function(x){return x.ROWUID==pRowuid});
        else
			var sourcedata = activeParcel[sourceTable].filter(function(x){return x.ParentROWUID==pRowuid});
		var index = parseInt($(formView).attr('index'));
        var showAuxParams = getParamOfAuxData(catgID)[0];
        showAuxData(formView, index, showAuxParams.filter_field, showAuxParams.filter_value, showAuxParams.parentROWUID);
        $('.aux-records', formView).html(sourcedata.length);
        $('.aux-index', formView).html(index + 1);
        $(formView).attr('state', 'form');
        selectTab(catgID, index);
        var parent = getParentCategories(catgID);
        $('.currentCategoryTitle').html(getInnerHtml(catgID));
        if (parent)
            $('.parentCategoryTitle').html(getInnerHtml(parent));
        $('.category-page[categoryid="' + catgID + '"] .parentCategoryDiv').show();
        $('.parcel-data-title').html(getInnerHtml(catgID));
        var txtarea = $('.category-page[categoryid = "'+ catgID +'"] textarea');
    	for(var x = 0;x < txtarea.length; x++){
			txtarea[x].style.cssText = 'height:auto';
			txtarea[x].style.cssText = 'height:' + txtarea[x].scrollHeight + 'px';
    	}
        loadParentChildOfCategory(catgID);
        highlightwithFilter(catgID);
        loadChildCategories(catgID);
        refreshAuxCount(catgID,formView)
        if (sourcedata.length > 0) {
            if (sourcedata[index].CC_Deleted == true) {
                $('.category-page[categoryid="' + catgID + '"] .auxrecover').show();
                $('.childDiv').hide();
            }
        }
    });
    
    let isSupportView = (!__DTR && localStorage && localStorage.internalSupportView == '1' && localStorage.parcelDataSupportView == '1')? true: false;
	var childcategories = fieldCategories.filter(function (f) { return (f.ParentCategoryId == $(targetForm).attr('categoryid') && f.HideExpression && f.HideExpression != '') });
	childcategories.forEach(function (c) {
		if( c.HideExpression.contains('parent.') ) { //to check hideexpression condition 
            var isExp = activeParcel.EvalValue(thisData + '[' + index + '].parentRecord', c.HideExpression.replace(new RegExp('parent.', 'g'), ''))
	 	    if( isExp ) $('.aux-sub-multigrid-view[pcatid="' + c.Id + '"] .sub-multigrid', targetForm).hide();
	 	}
	 	else
		    $('.aux-sub-multigrid-view[pcatid="' + c.Id + '"] .sub-multigrid', targetForm).hide();
    })
    
    var childcategories = fieldCategories.filter(function (f) { return (f.ParentCategoryId == $(childview).attr('categoryid') && f.HideExpression && f.HideExpression != '') });
    childcategories.forEach(function (c) {
        var isExp = activeParcel.EvalValue(thisData + '[' + index + ']', c.HideExpression.replace(new RegExp('parent.', 'g'), ''))
        if (isExp) $('.sub-multigrid tr[cid="' + c.Id + '"]', targetForm).hide()
    })
    if(activeParcel.QC == true)
		$('.aux-sub-multigrid-view[catid="'+catid+'"] .sub-multigrid tbody tr .data-collection-table td').children().removeClass('EditedValue');
}

function sortNull(sortExp, sourceData, sourceTable, set) {
    var SortItems = sortExp.trim().split(',');
    SortItems.forEach(function (Item) {
        var sortField = Item.trim().split(" ")[0];
        var scriptNullOrder = 99999999
        if (Item.replace(sortField, '').trim().toLowerCase() == "desc")
            scriptNullOrder = -99999999
        for (x in sourceData) {
            if (set == 1) {
                sourceData[x][sortField] = sourceData[x][sortField] != null ? sourceData[x][sortField] : scriptNullOrder;
                activeParcel.Original[sourceTable][x][sortField] = activeParcel.Original[sourceTable][x][sortField] ? activeParcel.Original[sourceTable][x][sortField] : scriptNullOrder;
            } else {
                sourceData[x][sortField] = sourceData[x][sortField] == scriptNullOrder ? null : sourceData[x][sortField];
                activeParcel.Original[sourceTable][x][sortField] = activeParcel.Original[sourceTable][x][sortField] == scriptNullOrder ? null : activeParcel.Original[sourceTable][x][sortField];
            }
        }
    });
}
function selectTab(categoryID, index) {
    var auxForm = $('.category-page[categoryid="' + categoryID + '"]');
    $('.category-page').hide();
    $('.aux-grid-view[catid="' + categoryID + '"]').hide();
    $('.aux-multigrid-view[catid="' + categoryID + '"]').hide();
    $('.category-page[categoryid="' + categoryID + '"]').show();
    showForm(auxForm, categoryID, index);
}
function loadChildCategories(catgID) {
    var childCategories = getChildCategories(catgID);
    var childcat = [];
    $.each(childCategories, function (a, b) {
        if ($(".childCategoryTitle[category='" + childCategories[a] + "']").css('display') != 'none') {
           if(!isBPPParcel){
               if(getCategory(childCategories[x]).Type == '1') return;
               }
          else{
             if(getCategory(childCategories[x]).Type != '1' || getCategory(childCategories[x]).Type != '2')  return;
             }
         //if(!isBPPParcel) if(getCategory(childCategories[a]).Type == '1') return;
             childcat.push(childCategories[a]);
        }
    });
    for (x in childcat) {
        if (x == (childcat.length - 1)) {
        }
        else {
            $(".childCategoryTitle[category='" + childcat[x] + "']").after('<span>,</span>');
        }
    }
}
function getFilteredIndexOfAuxData(categoryId, sourceTable) {
    var showAuxParams = (getParamOfAuxData(categoryId) && getParamOfAuxData(categoryId).length) ? getParamOfAuxData(categoryId)[0] : "";
    if (showAuxParams  && (showAuxParams.filter_field != undefined) && (showAuxParams.filter_value != undefined) && (showAuxParams.filter_value.length > 0) && (showAuxParams.filter_field.length > 0)) {
        source = showAuxDataWithFilter(sourceTable, showAuxParams.filter_field, showAuxParams.filter_value, showAuxParams.parentROWUID,categoryId);
        filteredIndexes = getFilteredIndexes(sourceTable, showAuxParams.filter_field, showAuxParams.filter_value, showAuxParams.parentROWUID,categoryId);
        return filteredIndexes;
    }
    else {
        source = eval('activeParcel.' + sourceTable);
        if (!source)
            filteredIndexes = [];
        else
            filteredIndexes = eval('activeParcel.' + sourceTable + '.map(function(d,i){return i;})');
        return filteredIndexes;
    }
}
/*
function highlightDeletedRecord(categoryId,parentCat) {
    var auxForm = $('.category-page[categoryid="' + categoryId + '"]');
    var cat = getCategory(categoryId);
    var sourceTable = $($('.category-page[categoryid="' + categoryId + '"]')).attr('auxdata');
    var sourceData = activeParcel[sourceTable];
    var childs=getChildCategories(categoryId);
    var flag;
    if (sourceData) {
        getFilteredIndexOfAuxData(categoryId, sourceTable)
        var gridData = sourceData.filter(function (d, i) { return filteredIndexes.indexOf(i) > -1; });
        for (x in gridData) {
            if (gridData[x].CC_Deleted == true) {
                $('.aux-grid-view .data-collection-table tbody tr', auxForm).eq(x).addClass('aux-deleted');
            }
			var curRecordChanges = activeParcel.ParcelChanges.filter(function(pc){return (pc.QCApproved != true  || pc.QCApproved == true) && pc.AuxROWUID == gridData[x].ROWUID  && datafields[pc.FieldId] && datafields[pc.FieldId].ReadOnly != true && datafields[pc.FieldId].DoNotShow != true &&  pc.CategoryId != null && (pc.CategoryId == categoryId || pc.CategoryId == parentCat) && (datafields[pc.FieldId].VisibilityExpression ? !(activeParcel.EvalValue(sourceTable + '[' + x + ']', datafields[pc.FieldId].VisibilityExpression)) : true)});
			var curRecordChangesDTR = curRecordChanges.filter(function(rc){return datafields[rc.FieldId].DoNotShowOnDTR != true});
			var newRecord = activeParcel.ParcelChanges.filter(function(p){return p.AuxROWUID == gridData[x].ROWUID && p.Action=="new" && p.QCApproved != true})
			var editedRecord = activeParcel.ParcelChanges.filter(function(p){return p.AuxROWUID == gridData[x].ROWUID && p.Action=="edit" && p.QCApproved == true && curRecordChanges && curRecordChanges.length > 0})
			var qcRowChanged = ((activeParcel.QC != true || activeParcel.QC == true) && curRecordChanges && curRecordChanges.length > 0 && gridData[x].CC_Deleted != true &&  curRecordChanges[curRecordChanges.length-1].OriginalValue !=curRecordChanges[curRecordChanges.length-1].NewValue ) ? true : false;
			var dtrRowChanged = ((activeParcel.QC != true || activeParcel.QC == true) && __DTR && curRecordChangesDTR && curRecordChangesDTR.length > 0  && gridData[x].CC_Deleted != true && curRecordChangesDTR[curRecordChangesDTR.length-1].OriginalValue !=curRecordChangesDTR[curRecordChangesDTR.length-1].NewValue ) ? true : false; 
			var isRowChanged = (qcRowChanged || dtrRowChanged || (newRecord && newRecord.length > 0 && gridData[x].CC_Deleted != true ) || (editedRecord && editedRecord.length > 0 && gridData[x].CC_Deleted != true ));
			//if(activeParcel.QC != true &&  (__DTR ? ((curRecordChangesDTR.length > 0 || newRecord .length > 0) ? true : false) : ((curRecordChanges.length > 0 || newRecord .length > 0) ? true : false))  && gridData[x].CC_Deleted != true){
				//if(curRecordChanges && curRecordChanges.length > 0 && curRecordChanges[curRecordChanges.length-1].OriginalValue !=curRecordChanges[curRecordChanges.length-1].NewValue){
			if(activeParcel.QC && isRowChanged){
				if(parentCat) {
						var auxform = $('.category-page[categoryid="' + parentCat + '"]');
						$('.aux-multigrid-view[catid="'+parentCat+'"]').find('.multigrid').eq(0).attr('style','margin-left : 0px; ') ;
						$('.grid-view  .aux-multigrid-view .multigrid tbody tr .data-collection-table[catid="'+categoryId+'"] tbody tr', auxform).eq(x).find('td').eq(0).css('position','relative');
						$('.grid-view  .aux-multigrid-view .multigrid tbody tr .data-collection-table[catid="'+categoryId+'"] tbody tr', auxform).eq(x).find('.aux-grid-edit').eq(0).before('<span class = gridApproveNotify style ="position:absolute;left:-15px;font-size: medium;color: green;font-weight: bold;">!</span>')
						if($(' .aux-multigrid-view', auxform).css('margin-left')=='10px' && $('.aux-multigrid-view[catid="'+parentCat+'"] .gridApproveNotify').length > 0) {
                    		$('.grid-view  .aux-multigrid-view .data-collection-table tbody tr', auxform).css('margin-left','20px') ;
						}
						
					}
					else {
	           	 		$('.grid-view[categoryid="'+categoryId+'"]').find('.data-collection-table').eq(0).attr('style','margin-left : 10px; ') ;
						$('.aux-grid-view .data-collection-table tbody tr', auxForm).eq(x).find('td').eq(0).css('position','relative');
						$('.aux-grid-view .data-collection-table tbody tr', auxForm).eq(x).find('.aux-grid-edit').eq(0).before('<span class = gridApproveNotify style ="position:absolute;left:-15px;font-size: medium;color: green;font-weight: bold;">!</span>')
						if($('.aux-grid-view[catid="'+categoryId+'"] .gridApproveNotify').length > 0) $('.aux-multigrid-view[catid="'+categoryId+'"]').css('margin-left','8px');
       		 		}
			}
			else{
				if(isRowChanged){	
		       	 		if(parentCat) {
							var auxform = $('.category-page[categoryid="' + parentCat + '"]');
							$('.aux-multigrid-view[catid="'+parentCat+'"]').find('.multigrid').eq(0).attr('style','margin-left : 0px; ') ;
							$('.grid-view  .aux-multigrid-view .multigrid tbody tr .data-collection-table[catid="'+categoryId+'"] tbody tr', auxform).eq(x).find('td').eq(0).css('position','relative');
							$('.grid-view  .aux-multigrid-view .multigrid tbody tr .data-collection-table[catid="'+categoryId+'"] tbody tr', auxform).eq(x).find('.aux-grid-edit').eq(0).before('<span class = gridEditNotify style ="position:absolute;left:-15px;font-size: medium;color: red;font-weight: bold;">!</span>')
							if($(' .aux-multigrid-view', auxform).css('margin-left')=='10px' && $('.aux-multigrid-view[catid="'+parentCat+'"] .gridEditNotify').length > 0) {
		                		$('.grid-view  .aux-multigrid-view .data-collection-table tbody tr', auxform).css('margin-left','20px') ;
							}
							//$('.grid-view[categoryid="'+parentCat+'"]').find('.data-collection-table').eq(0).attr('style','margin-left : 20px; overflow: auto;') ;
							//$('.aux-grid-view .data-collection-table tbody tr', auxform).eq(x).find('td').eq(0).css('position','relative');
							//$('.aux-grid-view .data-collection-table tbody tr', auxform).eq(x).find('.aux-grid-edit').eq(0).before('<span class = gridEditNotify style ="position:absolute;left:-15px;font-size: medium;color: red;font-weight: bold;">!</span>')
						}
						else {
		           	 		$('.grid-view[categoryid="'+categoryId+'"]').find('.data-collection-table').eq(0).attr('style','margin-left : 10px; ') ;
							$('.aux-grid-view .data-collection-table tbody tr', auxForm).eq(x).find('td').eq(0).css('position','relative');
							$('.aux-grid-view .data-collection-table tbody tr', auxForm).eq(x).find('.aux-grid-edit').eq(0).before('<span class = gridEditNotify style ="position:absolute;left:-15px;font-size: medium;color: red;font-weight: bold;">!</span>')
							if($('.aux-grid-view[catid="'+categoryId+'"] .gridEditNotify').length > 0) $('.aux-multigrid-view[catid="'+categoryId+'"]').css('margin-left','8px');
		   		 		}
		   		 	//}
		       }
           }
        }
    }
}
*/

function highlightDeletedRecord(categoryId,parentCat,ChildVal) {
    var auxForm = $('.category-page[categoryid="' + categoryId + '"]');
    var cat = getCategory(categoryId);
    var sourceTable = $($('.category-page[categoryid="' + categoryId + '"]')).attr('auxdata');
    var sourceData = activeParcel[sourceTable];
    var childs = getChildCategories(categoryId);
    var flag;
    if (sourceData) {
        getFilteredIndexOfAuxData(categoryId, sourceTable)
        var gridData = sourceData.filter(function (d, i) { return filteredIndexes.indexOf(i) > -1; });
        for (x in gridData) {
            if (gridData[x].CC_Deleted == true) {
                let $drow = $('.aux-grid-view .data-collection-table tbody tr', auxForm).eq(x), gdclr = activeParcel?.QC ? 'green' : 'red';
                $drow.addClass('aux-deleted');
                $drow.find('td').eq(0).css('position', 'relative');
                $drow.find('.gridDeleteNotify').remove(); $drow.find('.gridEditNotify').remove();
                $drow.find('.aux-grid-edit').eq(0).before('<span class="gridDeleteNotify" style="position: absolute; left: -15px; font-size: medium; color: ' + gdclr + '; font-weight: bold;">X</span>');
                if ($('.aux-grid-view[catid="' + categoryId + '"] .gridDeleteNotify').length > 0) {
                    $('.grid-view[categoryid="' + categoryId + '"]').find('.data-collection-table').eq(0).attr('style', 'margin-left : 10px;');
                    $('.aux-multigrid-view[catid="' + categoryId + '"]').css('margin-left', '8px');
                    $('.aux-sub-multigrid-view[catid="' + categoryId + '"]').css('margin-left', '8px');
                }
            }
            var childchange = false;

            childs.forEach(function(child) {
                var SourcetableName = getCategory(child).SourceTable;
                var sourceTableData = gridData[x][SourcetableName]
                if(sourceTableData) {
                    sourceTableData.forEach(function(data) {
                        var curRecordChanges = activeParcel.ParcelChanges.filter(function(pc){return (pc.QCApproved != true  || pc.QCApproved == true) && pc.AuxROWUID == data.ROWUID  && datafields[pc.FieldId] && datafields[pc.FieldId].SourceTable == SourcetableName &&  datafields[pc.FieldId].ReadOnly != true && datafields[pc.FieldId].DoNotShow != true &&  pc.CategoryId != checkIsNull && (datafields[pc.FieldId].VisibilityExpression ? !(activeParcel.EvalValue(sourceTable + '[' + x + ']', datafields[pc.FieldId].VisibilityExpression)) : true)});
                        var curRecordChangesDTR = curRecordChanges.filter(function(rc){return datafields[rc.FieldId].DoNotShowOnDTR != true});
                        var newRecord = activeParcel.ParcelChanges.filter(function(p){return p.AuxROWUID == data.ROWUID && p.Action=="new" && p.QCApproved != true && ( datafields[p.FieldId] ? datafields[p.FieldId].SourceTable == SourcetableName: true ) })
                        var editedRecord = activeParcel.ParcelChanges.filter(function(p){return p.AuxROWUID == data.ROWUID && p.Action=="edit" && p.QCApproved == true && curRecordChanges && curRecordChanges.length > 0 && ( datafields[p.FieldId] ? datafields[p.FieldId].SourceTable == SourcetableName: true ) })
                        var qcRowChanged = ((activeParcel.QC != true || activeParcel.QC == true) && curRecordChanges && curRecordChanges.length > 0 && data.CC_Deleted != true &&  curRecordChanges[curRecordChanges.length-1].OriginalValue !=curRecordChanges[curRecordChanges.length-1].NewValue ) ? true : false;
                        var dtrRowChanged = ((activeParcel.QC != true || activeParcel.QC == true) && __DTR && curRecordChangesDTR && curRecordChangesDTR.length > 0  && data.CC_Deleted != true && curRecordChangesDTR[curRecordChangesDTR.length-1].OriginalValue !=curRecordChangesDTR[curRecordChangesDTR.length-1].NewValue ) ? true : false; 
	                    if((qcRowChanged || dtrRowChanged || (newRecord && newRecord.length > 0 && gridData[x].CC_Deleted != true ) || (editedRecord && editedRecord.length > 0 && gridData[x].CC_Deleted != true )))
	                            childchange = true;
                    })
                }
            })

			var curRecordChanges = activeParcel.ParcelChanges.filter(function(pc){return (pc.QCApproved != true  || pc.QCApproved == true) && pc.AuxROWUID == gridData[x].ROWUID  && datafields[pc.FieldId] && datafields[pc.FieldId].SourceTable == sourceTable && datafields[pc.FieldId].ReadOnly != true && datafields[pc.FieldId].DoNotShow != true  && (!(pc.OriginalValue == pc.NewValue)) &&  pc.CategoryId != null && (pc.CategoryId == categoryId || pc.CategoryId == parentCat) && (datafields[pc.FieldId].VisibilityExpression ? !(activeParcel.EvalValue(sourceTable + '[' + x + ']', datafields[pc.FieldId].VisibilityExpression)) : true)});
			var curRecordChangesDTR = curRecordChanges.filter(function(rc){return datafields[rc.FieldId].DoNotShowOnDTR != true});
			var newRecord = activeParcel.ParcelChanges.filter(function(p){return p.AuxROWUID == gridData[x].ROWUID && p.Action=="new" && p.QCApproved != true && ( datafields[p.FieldId] ? datafields[p.FieldId].SourceTable == sourceTable: true ) })
			var editedRecord = activeParcel.ParcelChanges.filter(function(p){return p.AuxROWUID == gridData[x].ROWUID && p.Action=="edit" && p.QCApproved == true && curRecordChanges && curRecordChanges.length > 0 && ( datafields[p.FieldId] ? datafields[p.FieldId].SourceTable == sourceTable: true ) })
			var qcRowChanged = ((activeParcel.QC != true || activeParcel.QC == true) && curRecordChanges && curRecordChanges.length > 0 && gridData[x].CC_Deleted != true &&  curRecordChanges[curRecordChanges.length-1].OriginalValue !=curRecordChanges[curRecordChanges.length-1].NewValue ) ? true : false;
			var dtrRowChanged = ((activeParcel.QC != true || activeParcel.QC == true) && __DTR && curRecordChangesDTR && curRecordChangesDTR.length > 0  && gridData[x].CC_Deleted != true && curRecordChangesDTR[curRecordChangesDTR.length-1].OriginalValue !=curRecordChangesDTR[curRecordChangesDTR.length-1].NewValue ) ? true : false; 
			var isRowChanged = (qcRowChanged || dtrRowChanged || (newRecord && newRecord.length > 0 && gridData[x].CC_Deleted != true ) || (editedRecord && editedRecord.length > 0 && gridData[x].CC_Deleted != true ) || childchange);
			//if(activeParcel.QC != true &&  (__DTR ? ((curRecordChangesDTR.length > 0 || newRecord .length > 0) ? true : false) : ((curRecordChanges.length > 0 || newRecord .length > 0) ? true : false))  && gridData[x].CC_Deleted != true){
				//if(curRecordChanges && curRecordChanges.length > 0 && curRecordChanges[curRecordChanges.length-1].OriginalValue !=curRecordChanges[curRecordChanges.length-1].NewValue){
			if(activeParcel.QC && isRowChanged){
				if(parentCat) {
						var auxform = $('.category-page[categoryid="' + parentCat + '"]');
							if(!ChildVal){
								$('.aux-multigrid-view[catid="'+parentCat+'"]').find('.multigrid').eq(0).attr('style','margin-left : 0px; ') ;
								$('.grid-view  .aux-multigrid-view .multigrid tbody tr .data-collection-table[catid="'+categoryId+'"] tbody tr', 	auxform).eq(x).find('td').eq(0).css('position','relative');
								$('.grid-view  .aux-multigrid-view .multigrid tbody tr .data-collection-table[catid="'+categoryId+'"] tbody tr', auxform).eq(x).find('.aux-grid-edit').eq(0).before('<span class = gridApproveNotify style ="position:absolute;left:-13px;font-size: medium;color: green;font-weight: bold;">!</span>')
								if($(' .aux-multigrid-view', auxform).css('margin-left')=='10px' && $('.aux-multigrid-view[catid="'+parentCat+'"] .gridApproveNotify').length > 0) {
                    				$('.grid-view  .aux-multigrid-view .data-collection-table tbody tr', auxform).css('margin-left','20px') ;
								}
                            }
							else{
								$('.aux-sub-multigrid-view[pcatid="'+ChildVal+'"]').find('.sub-multigrid').eq(0).attr('style','margin-left : 0px; ') ;
								$('.grid-view  .aux-sub-multigrid-view[pcatid="'+ChildVal+'"] .sub-multigrid tbody tr[cid="'+categoryId+'"] td .data-collection-table tbody tr', auxform).eq(x).find('td').eq(0).css('position','relative');
								$('.grid-view  .aux-sub-multigrid-view[pcatid="'+ChildVal+'"] .sub-multigrid tbody tr[cid="'+categoryId+'"] td .data-collection-table tbody tr', auxform).eq(x).find('.aux-grid-edit').eq(0).before('<span class = gridApproveNotify style ="position:absolute;left:-13px;font-size: medium;color: green;font-weight: bold;">!</span>')
								if($(' .aux-multigrid-view', auxform).css('margin-left')=='10px' && $('.aux-multigrid-view[catid="'+parentCat+'"] .gridApproveNotify').length > 0) {
                    				$('.grid-view  .aux-multigrid-view .data-collection-table tbody tr', auxform).css('margin-left','20px') ;
								}
                            }
					}
					else {
	           	 		$('.grid-view[categoryid="'+categoryId+'"]').find('.data-collection-table').eq(0).attr('style','margin-left : 10px; ') ;
						$('.aux-grid-view .data-collection-table tbody tr', auxForm).eq(x).find('td').eq(0).css('position','relative');
						$('.aux-grid-view .data-collection-table tbody tr', auxForm).eq(x).find('.aux-grid-edit').eq(0).before('<span class = gridApproveNotify style ="position:absolute;left:-13px;font-size: medium;color: green;font-weight: bold;">!</span>')
						if($('.aux-grid-view[catid="'+categoryId+'"] .gridApproveNotify').length > 0) $('.aux-multigrid-view[catid="'+categoryId+'"]').css('margin-left','8px');
						if($('.aux-grid-view[catid="'+categoryId+'"] .gridEditNotify').length > 0) {
					 			$('.aux-multigrid-view[catid="'+categoryId+'"]').css('margin-left','8px');
								$('.aux-sub-multigrid-view[catid="'+categoryId+'"]').css('margin-left','8px') ;}
		   		 		}
       		 		
			}
			else{
				if(isRowChanged){	
		       	 		if(parentCat) {
							var auxform = $('.category-page[categoryid="' + parentCat + '"]');
							if(!ChildVal){
								$('.aux-multigrid-view[catid="'+parentCat+'"]').find('.multigrid').eq(0).attr('style','margin-left : 0px; ') ;
								$('.grid-view  .aux-multigrid-view .multigrid tbody tr .data-collection-table[catid="'+categoryId+'"] tbody tr', auxform).eq(x).find('td').eq(0).css('position','relative');
								$('.grid-view  .aux-multigrid-view .multigrid tbody tr .data-collection-table[catid="'+categoryId+'"] tbody tr', auxform).eq(x).find('.aux-grid-edit').eq(0).before('<span class = gridEditNotify style ="position:absolute;left:-13px;font-size: medium;color: red;font-weight: bold;">!</span>')
								if($(' .aux-multigrid-view', auxform).css('margin-left')=='10px' && $('.aux-multigrid-view[catid="'+parentCat+'"] .gridEditNotify').length > 0) {
		                			$('.grid-view  .aux-multigrid-view .data-collection-table tbody tr', auxform).css('margin-left','20px') ;
								}
							//$('.grid-view[categoryid="'+parentCat+'"]').find('.data-collection-table').eq(0).attr('style','margin-left : 20px; overflow: auto;') ;
							//$('.aux-grid-view .data-collection-table tbody tr', auxform).eq(x).find('td').eq(0).css('position','relative');
							//$('.aux-grid-view .data-collection-table tbody tr', auxform).eq(x).find('.aux-grid-edit').eq(0).before('<span class = gridEditNotify style ="position:absolute;left:-15px;font-size: medium;color: red;font-weight: bold;">!</span>')
							}
						else{
								$('.aux-sub-multigrid-view[pcatid="'+ChildVal+'"]').find('.sub-multigrid').eq(0).attr('style','margin-left : 0px; ') ;
								$('.grid-view  .aux-sub-multigrid-view[pcatid="'+ChildVal+'"] .sub-multigrid tbody tr[cid="'+categoryId+'"] td .data-collection-table tbody tr', auxform).eq(x).find('td').eq(0).css('position','relative');
								$('.grid-view  .aux-sub-multigrid-view[pcatid="'+ChildVal+'"] .sub-multigrid tbody tr[cid="'+categoryId+'"] td .data-collection-table tbody tr', auxform).eq(x).find('.aux-grid-edit').eq(0).before('<span class = gridEditNotify style ="position:absolute;left:-13px;font-size: medium;color: red;font-weight: bold;">!</span>')

								if( $('.aux-sub-multigrid-view[pcatid="'+ChildVal+'"]  .gridEditNotify').length > 0) {
                					$('.grid-view  .aux-multigrid-view .data-collection-table tbody tr', auxform).css('margin-left','20px') ;
                				}
                        	}
						
                        }
						else {
		           	 		$('.grid-view[categoryid="'+categoryId+'"]').find('.data-collection-table').eq(0).attr('style','margin-left : 10px; ') ;
							$('.aux-grid-view .data-collection-table tbody tr', auxForm).eq(x).find('td').eq(0).css('position','relative');
							$('.aux-grid-view .data-collection-table tbody tr', auxForm).eq(x).find('.aux-grid-edit').eq(0).before('<span class = gridEditNotify style ="position:absolute;left:-13px;font-size: medium;color: red;font-weight: bold;">!</span>')
							if($('.aux-grid-view[catid="'+categoryId+'"] .gridEditNotify').length > 0) $('.aux-multigrid-view[catid="'+categoryId+'"]').css('margin-left','8px');
							if($('.aux-grid-view[catid="'+categoryId+'"] .gridEditNotify').length > 0) {
					 			$('.aux-multigrid-view[catid="'+categoryId+'"]').css('margin-left','8px');
								$('.aux-sub-multigrid-view[catid="'+categoryId+'"]').css('margin-left','8px') ;}
		   		 			}
		   		 	//}
		       }
           }
        }
    }
}

function sortBySortExpression() {
    var cat = fieldCategories.filter(function (f) { return (f.SortExpression != null) });
    if (!cat) return;
    for (x in cat) {
        if (!cat[x].SortExpression || cat[x].SortExpression == '') continue;
        var source = cat[x].SourceTable;
        var sortexp = cat[x].SortExpression;
        var sourcedata = activeParcel[source];
        var sourcedataoriginal = activeParcel.Original[source];
        if (!sourcedata)
            continue;
        sortNull(sortexp, sourcedata, source, 1);
        //var sortScript = Sortscript(sortexp, source);
        
        var sortScript;
		if((sortexp) && ((sortexp.contains("CASE")) || (sortexp.contains("case")))){
			sortScript = SortscriptCASE(sortexp, source);
		}
		else{
			sortScript = Sortscript(sortexp, source);
		}
        
        if (sortScript != "") {
            eval('sourcedata' + sortScript);
            eval('sourcedataoriginal' + sortScript);
        }
        sortNull(sortexp, sourcedata, source, 0);
    }
}
function numberValidation(qcValue, fieldId, e) {
    var fields = datafields[fieldId];
    var inputType = parseInt(fields.InputType);
    var d = QC.dataTypes[inputType];
    var pattern = new RegExp(d.pattern);
    if(inputType == 11)
        return qcValue;

    let notAllowedCharsRegex = /[#%&{}\\<>*?\/!$'"@:+=`|]|[\u00A0-\uFFFF]|[\uD800-\uDBFF][\uDC00-\uDFFF]/g;

    function isAltCode(charCode) {
        return (charCode > 126);
    }

    function isEmoji(charCode) {
        return (charCode >= 0x1F600 && charCode <= 0x1F64F) || (charCode >= 0x1F300 && charCode <= 0x1F5FF) || (charCode >= 0x1F680 && charCode <= 0x1F6FF) || (charCode >= 0x2600 && charCode <= 0x26FF);
    }

    if (pattern != '') {
        if (qcValue != "") {
            if (!pattern.test(qcValue)) {
                if (e === undefined) {
                    if (fields.InputType == 10) {
                        alert('Invalid input. Please enter a valid 4-digit year for the field.');
                    }
                    else if (fields.InputType == 2) {
                        alert('Invalid input. You can only type 10 digits after the decimal point.');
                    }
                    else if (fields.InputType == 7) {
                        alert('Invalid input. Commas or other characters are not allowed. You can type only numerals and a single period.');
                    }
                    else if (fields.InputType == 9) {
                        alert('Invalid input. Commas or other characters are not allowed. You can type only numerals.');
                    }
                    else if (fields.InputType == 8) {
                        alert('Invalid input. Commas or other characters are not allowed. You can type only numerals.');
                    }
                    $('.parcel-field-values .value[fieldid="' + fieldId + '"] .input').val('');
                }
            }
        }
        if (fields.InputType == 8 || fields.InputType == 10) {
            if (e && (e.keyCode == 43 || e.keyCode == 45 || e.keyCode == 101 || e.keyCode == 46))
                return false;
        }
         if(fields.InputType == 2) {
        	if(e && e.keyCode == 46)
        	{
        		if(qcValue.indexOf( '.' ) > -1) return false;
        		var numPrec = parseInt(fields.NumericPrecision)
                 if ( numPrec && isNaN( qcValue ) == false && qcValue ) {
                    if ( qcValue.replace( '.', '' ).length >= numPrec )
                    return false;
                    }	
        	}
        }

        if (fields.InputType == 1 && fields.DoNotAllowSpecialCharacters && e) {
            let charCode = e.charCode || e.keyCode, charStr = String.fromCharCode(charCode);

            if (notAllowedCharsRegex.test(charStr) || isAltCode(charCode) || isEmoji(charCode)) {
                return false;
            }
        }
    }

    if (e && e.type == "keypress")
        return;
    var maxLength = QC.dataTypes[fields.InputType].maxlength;
    var datestatus = $('.parcel-field-values .value[fieldid="' + fieldId + '"] .input')[0].validity.valid;
    if (fields.InputType == 4 && (!datestatus || qcValue.length > maxLength)) {
        alert('Invalid Date');
        $('.parcel-field-values .value[fieldid="' + fieldId + '"] .input').val('');
    }
    if (fields.InputType == 10 && qcValue.length > maxLength) {
        $('.parcel-field-values .value[fieldid="' + fieldId + '"] .input').val('');
		return;
    }

    if (d.jsType == 'Number') {
        let text = qcValue
        var item = $('.parcel-field-values .value[fieldid="' + fieldId + '"] .input')
        if (text != '' && !isNaN(text) && ((($(item).attr('min') != '' && $(item).attr('min') > parseFloat(text)) || ($(item).attr('max') != '' && $(item).attr('max') < parseFloat(text))) || (text.indexOf('.') > 1 && $(item).attr('allowdecimal') == 'false'))) {
            $(item).val('');
            alert('Please follow on-screen validation.');
            return;
        }
    }

    if (fields) {
        var scale = parseInt(fields.NumericScale);
        var numPrecision = parseInt(fields.NumericPrecision);
        if (qcValue != null && isNaN(qcValue) == false) {
            var qcValueStr = qcValue.toString();
            var integerPartLength, fractionalPartLength;
            if (scale && qcValueStr.indexOf('.') > -1) {
                var parts = qcValueStr.split('.');
                integerPartLength = parts[0].length;
                fractionalPartLength = parts[1].length;

                // Check for scale part length
                if (fractionalPartLength > scale) {
                    alert('Maximum scale of field (' + fields.Label + ') is ' + scale);
                    if (scale == 0) {
                        qcValue = qcValueStr.substring(0, qcValueStr.indexOf('.'));
                    } else {
                        qcValue = qcValueStr.substring(0, qcValueStr.indexOf('.') + parseInt(scale) + 1);
                    }
                    $('.parcel-field-values .value[fieldid="' + fieldId + '"] .input').val(qcValue);
                    qcValueStr = qcValue.toString();
                }
            } else {
                integerPartLength = qcValueStr.length;
            }

            // Check for precision including the integer part
            if (numPrecision && qcValueStr.replace('.', '').length > numPrecision) {
                alert('Maximum precision of field (' + fields.Label + ') is ' + numPrecision);
                if (qcValueStr.indexOf('.') > -1 && qcValueStr.indexOf('.') < numPrecision) {
                    qcValue = qcValueStr.substring(0, numPrecision + 1);
                } else {
                    qcValue = qcValueStr.substring(0, numPrecision);
                }
                qcValue = (qcValueStr.indexOf('.') == qcValue.length - 1) ? qcValue.substring(0, qcValue.length - 1) : qcValue;
                $('.parcel-field-values .value[fieldid="' + fieldId + '"] .input').val(qcValue);
                qcValueStr = qcValue.toString();
            }

            // Check for integer part length
            var maxIntegerPartLength = numPrecision - scale;
            if (integerPartLength > maxIntegerPartLength && (scale != 0 || numPrecision != 0)) {
                alert('Maximum allowed digits before decimal for field (' + fields.Label + ') is ' + maxIntegerPartLength);
                qcValue = qcValueStr.substring(0, maxIntegerPartLength);
                $('.parcel-field-values .value[fieldid="' + fieldId + '"] .input').val(qcValue);
                qcValueStr = qcValue.toString();
            }
        }
    }

    if (fields.InputType == 1 && fields.DoNotAllowSpecialCharacters) {
        qcValue = qcValue ? qcValue.replace(notAllowedCharsRegex, '') : '';
        if (/^\s*$/.test(qcValue)) qcValue = '';
        $('.parcel-field-values .value[fieldid="' + fieldId + '"] .input').val(qcValue);
    }

    if (fields.MaxLength && qcValue != null && (fields.InputType == 1 || fields.InputType == 6 || fields.InputType == 2 || fields.InputType == 8))
        if (fields.MaxLength < qcValue.length  && fields.MaxLength > 0 ) {
            if (qcValue[0] == "0") {
                $('.parcel-field-values .value[fieldid="' + fieldId + '"] .input').val('');
            }
            else {
                qcValue = qcValue.substring(0, fields.MaxLength)
                alert('Maximum Fieldlength of ' + fields.Label + ' is ' + fields.MaxLength);
                $('.parcel-field-values .value[fieldid="' + fieldId + '"] .input').val(qcValue);
                return qcValue;
            }
        }
    return qcValue;
}

function isSiblingsTrue(sourceTable, expression) {
    try {
        var exp = expression.split(/\.IN\./g);
        var comparedSourceData = eval('activeParcel.' + sourceTable + '.parentRecord.' + exp[1].split('.')[0]);
        var value = eval('activeParcel.' + sourceTable + '.' + exp[0]);
        value = (value === null || value === undefined) ? value : value.toString();
        if (comparedSourceData === undefined || comparedSourceData === null || comparedSourceData.length == 0) return true;
        comparedSourceData = comparedSourceData.filter(function (csd) { return csd.CC_Deleted != true });
        return _.contains(comparedSourceData.map(function (cf) { return cf[exp[1].split('.')[1]]; }), value);
    }
    catch (ex) {
        console.error(ex);
        return false;
    }
}

function showInfoButtons() {
	try {
        var infoContentFields = datafieldsArray.filter(function (d) { return (d.InfoContent && d.InfoContent.toString().trim() != ''); });
        if (infoContentFields.length > 0) 
            infoContentFields.forEach(function(field) {
            	var label = $('.parcel-field-values td[fieldid="' + field.Id + '"]').siblings()
                var btn = $(label).children('.infoButton');
                $(btn).attr('fieldId', field.Id);
                $(btn).show();
                var w = $('span', $(label)).eq(0).width();
                if (w > 155) $(label).width(w + 12);
			});
    }
    catch (e) {
        console.log(e.message);
    }
}

function showInfoContent(btn) {
	var fieldId = $(btn).attr('fieldId');
	var infoContent = datafields[fieldId].InfoContent.replace(/&lt;/g,'<').replace(/&gt;/g,'>');
	$('.info_fieldName').html(datafields[fieldId].Label);
	$('.infoContent').html(infoContent);
	$('.infoContentContainer').show();
	$('.mask').show();

}

function hideInfoContent() {
	$('.mask').hide();
	$('.infoContentContainer').hide();
	$('.infoContent').html('');
}

function calculateChildRecords(field, index,rec) {
	var childCats = getChildCategories(field.CategoryId)
	if (childCats.length == 0) return null;
	var value = 0;
	var calcExprSplit = field.CalculationExpression.trim().split(childRecCalcPattern)
	var record = rec? rec: activeParcel[field.SourceTable][index];
	var childTbls = childCats.map(function(ch){return fieldCategories[fieldCategories.findIndex(function(s){ return s.Id == ch})].SourceTable });
	var chTbl = calcExprSplit[0].split('.')[0];
	var chField = calcExprSplit[0].split('.')[1];
	if (childTbls.some(function(c){ return c.indexOf(chTbl) > -1 })) {
		if (record[chTbl]) {
			record = record[chTbl].filter(function(ch){ return (ch.CC_Deleted != 'true' && !ch.CC_Deleted); });
			record.forEach(function(data){
				value += parseFloat((!data[chField] || data[chField].toString().trim() == '')? 0: data[chField]);
			});
			if (calcExprSplit[1].toLowerCase() == 'avg' && record.length > 0) value = value/record.length;
			value = parseFloat(value.toFixed(2));
		}
	} else return null;
	return value;
}

function insertDataIntoTables (tableList, callback){
	if (tableList.length == 0) { if (callback) callback(); return; }
	var tbl_name = tableList.pop();
	var tbl_data = _.clone(activeParcel[tbl_name]);
	if(tbl_name == "Parcel")
		tbl_data = _.clone([activeParcel]);
	if(!tbl_data || (tbl_name != "Parcel" && _includetableList.indexOf(tbl_name) == -1)){ insertDataIntoTables(tableList, callback); return; }
	var columns = [];
	Object.keys(datafields).map(function(dfid){ return datafields[dfid]; }).filter(function(dfs){ if(tbl_name == "Parcel"){return dfs.SourceTable == null } else{return dfs.SourceTable == tbl_name;} }).map(function(dfield){ return dfield.Name; }).forEach(function(cols){
		var regexp = new RegExp('^'+cols+'$', 'i');
		if (!columns.some(function(c){ return c.search(regexp) > -1 }) && (cols != null))
			columns.push(cols);
	});
	['CC_YearStatus', 'CC_RecordStatus', 'CC_ParcelId', 'CC_Deleted', 'ClientParentROWUID', 'ClientROWUID', 'ParentROWUID', 'ROWUID'].forEach(function(flds){ 
		columns.unshift(flds); 
	});
	if(tbl_name == "Parcel")
		['Id','KeyValue1','KeyValue2','KeyValue3'].forEach(function(fld){ columns.unshift(fld); });
	var insertData = function () {
		if (tbl_data.length == 0) { insertDataIntoTables(tableList, callback); return; }
		var data = tbl_data.pop();
		var cols = [], values = [];
		columns.forEach(function(col){  
			if (data[col] || data[col] === 0) {
				cols.push(col);
				values.push(data[col].toString().replace(/\'/g,"''"));
			}
		});
		getData("INSERT INTO " + tbl_name + " ([" + cols.join('],[') + "]) VALUES ('" + values.join("','") + "');", [], function(){
			insertData();
		});
	}
	
	getData("SELECT [" + columns.join('],[') + "] FROM " + tbl_name, [], function() {
		getData("DELETE FROM " + tbl_name, [], function() {
			insertData();
		})
	}, null, true, function() {
		var dropSql = "DROP TABLE IF EXISTS " + tbl_name;
		var createSql = "CREATE TABLE " + tbl_name + "([" + columns.join('] TEXT,[') + "])";
		db.transaction(function(x){
			executeSql(x, dropSql, [], function(tx) {
				executeSql(x, createSql, [], function(tx) {
					insertData();
				});
			});
		});
	});
	
}
function getDatatypeOfAggregateFields(aggr_field) {
	var aggr_Setting = offlineTables.AggrFields.filter(function(agr){ return ((agr.FunctionName + '_' + agr.TableName + '_' + agr.FieldName) == aggr_field); })[0];
	if (aggr_Setting) {
		var df = getDataField(aggr_Setting.FieldName, aggr_Setting.TableName);
		if (df) return QC.dataTypes[parseInt(df.InputType)];
	}
	if (aggr_field.startsWith('SUM') || aggr_field.startsWith('AVG') || aggr_field.startsWith('COUNT') || aggr_field.startsWith('MAX') || aggr_field.startsWith('MIN')) {
    	return QC.dataTypes[2];
    } else {
        return QC.dataTypes[1];
    }
}

function removeExceededPhotos(){
	var photos = activeParcel.Photos;
    var currentPhotos = activeParcel.Photos.length;
    var photosLimit = getClientSettingValue("MaxPhotosPerParcel");
    var imageIDs = "";
    var images = photos.filter(function (p) { return p.IsPrimary != true });
    var photosToBeDeleted = currentPhotos - photosLimit ;
    if (photosToBeDeleted > 0 && !(photosLimit == 1 && activeParcel.Photos.length == 1) && images.length > 0) {
            for (var j = 0 ; j < photosToBeDeleted ; j++) {
                imageIDs = (imageIDs && imageIDs.length != 0) ? imageIDs + '$' + images[j].Id.toString() : images[j].Id.toString();
                for (var k = 0; k < photos.length; k++) {
                    if (photos[k].Id == images[j].Id)
                        photos.splice(k, 1);
                }
            }
            $qc('deleteimage', { ImageId: imageIDs }, function (data) { });                                                          
    }
}

function imageTables(){
	Object.keys(datafields).filter(function(x){return datafields[x].LookupQuery != null}).map(function(s){ return datafields[s]}).forEach(function(item){
		if(item.LookupQuery && item.LookupQuery.search(/from/i) > -1){
			var tableNames = item.LookupQuery.match(/from\s(\w*)/gi);
			tableNames.forEach(function(y){
				var tableName = y.split(' ')[1];
				if(tableName && tblList.indexOf(tableName) == -1) tblList.push(tableName);
			});
		}
	});
}

function calculationExpressionSketch ( callback ) {
    var calcExpressionFields = Object.keys( datafields ).map( function ( x ) { return datafields[x] } ).filter(function (x) { return (x.CalculationExpression != null &&  x.CategoryId != null && x.UI_Settings &&
x.UI_Settings.IsSketchCalcField == "1" ) });
    if (calcExpressionFields.length > 0) {
    	for ( var j = 0; j < calcExpressionFields.length; j++ ) {
    		var field = calcExpressionFields[j], sourceTable = field.SourceTable, sourceData = activeParcel[sourceTable], fName = field.Name;
	        sourceData = sourceData? sourceData: [];
	        
	        try {
		        for( var i = 0; i < sourceData.length; i++) {
		            var source = sourceTable + '[' + i + ']', sData = activeParcel[sourceTable][i];
		            if( sData.CC_Deleted != true && sData.CC_Deleted != "true" ) {
			            var v = activeParcel.EvalValue(source, fName);
			            
			            if (field && field.CalculationExpression && field.CalculationExpression.trim() != '' && field.CalculationExpression.search(childRecCalcPattern) > -1) {
			                var tempVal = calculateChildRecords(field, i);
			                v = ( (tempVal == null)? v: tempVal );
			            }
			
			            if(field && field.CalculationExpression && sourceTable && (v || v === 0) && v.toString() != sData[fName]) { 
			            	activeParcel[sourceTable][i][fName] = v;
			                activeParcelEdits.push( { AuxROWUID: sData.ROWUID, ChangeId: null, FieldId: field.Id, ParcelId: activeParcel.Id, QCValue: v, action: "edit", parentROWUID: sData.ParentROWUID } );
			            }
		            }
		        }
		    }
	        catch(err) {
              console.log(err.message) ;
        	}
	        
    	}
	}
	if (callback) { callback(); return };  	
}

function applyParcelEditsChanges( _ispReviewed, _filterCallback) {
	if( _ispReviewed && activeParcelEdits.length > 0 ) {
		for(var pe in activeParcelEdits) {
			var _pEdits = activeParcelEdits[pe];
            var _cField = datafields[_pEdits.FieldId];
            if( _pEdits.action == "edit" && _cField ) {
                var _cValue = ( _pEdits.QCValue || _pEdits.QCValue == 0 ) ? _pEdits.QCValue : null;
                if ( ( _pEdits.AuxROWUID == null || ( activeParcel[_cField.Name] !== undefined && activeParcel['ROWUID'] == _pEdits.AuxROWUID ) ) && _cField.Name && !( _cField.SourceTable && activeParcel[_cField.SourceTable] ) ) {
					activeParcel[_cField.Name] = _cValue;
				} 
                else {
					for ( ex in activeParcel[_cField.SourceTable] ) {
						var acx = activeParcel[_cField.SourceTable][ex];
						if ( acx.ROWUID == _pEdits.AuxROWUID && _cField.Name )
							activeParcel[_cField.SourceTable][ex][_cField.Name] = _cValue;
					}
				}
            }
		}
	}
	
	if(_filterCallback)	_filterCallback();
}

function openCustomlkd(custlkd, cusdrop) {
	if (cusdrop)
		custlkd = $(custlkd).siblings('.cusDpdownSpan')[0];
	if ($(custlkd).attr("readonly") == 'readonly' || $(custlkd).attr("readonly") == 'true' || $(custlkd).attr("recovery") == 'recovery' || $(custlkd).attr("recovery") == 'true' || $(custlkd).attr("disabled") == 'disabled' || $(custlkd).attr("disabled") == 'true') 
		return false;
	var cat = null, sourceData = null;
	var _cfieldId = $(custlkd).parent().attr("fieldid")? parseInt($(custlkd).parent().attr("fieldid")): null;	
	if ($(custlkd).html() && $(custlkd).html().indexOf('</option>') > -1) {
		var cuslkVal = $(custlkd).val();
		cuslkVal = cuslkVal || cuslkVal === 0? cuslkVal: '';
		$(custlkd).html(cuslkVal);
	}
	if ($(custlkd).hasClass('photoCusDown')) {
		sourceData = activeParcel.Photos[currentPhotoIndex];
		cat = { Id: -2 };
	}
	else{
		var _crowId = $(custlkd).attr('auxrowuid')? parseInt($(custlkd).attr('auxrowuid')): 0;
		var block = $( 'td.value[fieldid="' + _cfieldId + '"]' ).parents( '.category-page' );
	    var source = $( block ).attr( 'auxdata' );
	    sourceData = source? ( activeParcel[source].filter(function(ind) { return ind.ROWUID == _crowId })[0] ): activeParcel;    
    }
    
    var _cfield = _cfieldId ? datafields[_cfieldId] : null;
    if(_cfield)
    	getLookupData(_cfield, { category: cat, source: sourceData }, function (ld) {
    		ld.forEach(function(x, index) { 
    			ld[index].cId = '';
    			if (x.Id || x.Id === 0) ld[index].cId = x.Id;
				if(x.Id) ld[index].Id = encodeURI(x.Id);
			});
    		openCustomddl(custlkd, ld, true); 
    	}, true);
    else
    	return false;
}

function internalFieldProperties(currentField, iconClick) {
	let fldId = iconClick? $(currentField).parent('.fldPropIcons').attr('fieldid'): $(currentField).parent('.fldPropcheckboxIcons').attr('fieldid');
	fldProps = datafields[fldId];
	if (fldProps) {
		$('.mask').show();
		$('.editFieldPropWindow .editFieldPropHead').html('Edit Properties - ' + fldProps.Label + '(' + fldProps.Name + ')')
		let mxLen, visExp, roExp, reqExp, reqSumExp, calExp, calOverExp, lkqueryExp, dv, note, isMassUpdate, isSiblings, isReqEdited, isCustom, precision, scale, autoNum, AutoSelect, isLarge;
		let dataType = fldProps.InputType;
		
		mxLen = parseInt(fldProps.MaxLength) > -1? parseInt(fldProps.MaxLength): ''; $('.mxLenProp').html(mxLen);
		visExp = fldProps.VisibilityExpression? fldProps.VisibilityExpression: ''; $('.hideonExpProps').html(visExp);
		roExp = fldProps.ReadOnlyExpression? fldProps.ReadOnlyExpression: ''; $('.readOnlyExpProps').html(roExp);
        reqExp = fldProps.RequiredExpression?  fldProps.RequiredExpression: ''; $('.reqExpProps').html(reqExp);
        reqSumExp = datafieldsettings.filter(function(x) { return (x.PropertyName == 'RequiredSum' && x.FieldId == fldId) })[0]? datafieldsettings.filter(function(x) { return (x.PropertyName == 'RequiredSum' && x.FieldId == fldId) })[0].Value: '';
        reqSumExp = reqSumExp? reqSumExp: ''; $('.reqSumExpProps').html(reqSumExp);
        dv = fldProps.DefaultValue?  fldProps.DefaultValue: ''; $('.defaultExpProps').html(dv);
        note = fldProps.CC_Note?  fldProps.CC_Note: ''; $('.noteExpProps').html(note);
        
		if (fldProps.IsMassUpdateField) $('.massFieldProp').attr('checkBox', 'checked');
		else $('.massFieldProp').removeAttr('checkBox');
		
		if (fldProps.IsUniqueInSiblings) $('.siblingFieldProp').attr('checkBox', 'checked');
		else $('.siblingFieldProp').removeAttr('checkBox');
		
		if (fldProps.RequiredIfRecordEdited) $('.reqEditedFieldProp').attr('checkBox', 'checked');
		else $('.reqEditedFieldProp').removeAttr('checkBox');
		
		if (fldProps.CustomInputType) $('.cusInpFieldProp').attr('checkBox', 'checked');
		else $('.cusInpFieldProp').removeAttr('checkBox');
		
		precision = fldProps.NumericPrecision? parseInt(fldProps.NumericPrecision): 0;
		scale = fldProps.NumericScale? parseInt(fldProps.NumericScale): 0;
		if (dataType == 2 || dataType == 7 || dataType == 8 || dataType == 9) {
			$('.precisionProp, .scaleProp').parent().show(); 
			$('.precisionProp').html(precision); $('.scaleProp').html(scale);	
		}
		else {
			$('.precisionProp, .scaleProp').parent().hide();
		}
		
		if (dataType == 8 ) {
			if (fldProps.autoNumber) $('.AutoNumberProp').attr('checkBox', 'checked');
			else $('.AutoNumberProp').removeAttr('checkBox', 'checked');
			$('.AutoNumberProp').parent().show();
		}
		else {
			$('.AutoNumberProp').parent().hide();
		}
		
		if (dataType == 5 || dataType == 11 || dataType == 3 || dataType == 4) {
			$('.calcExpProps, .calcOverrideExpProps').parent().hide();
		}
		else {
			calExp = fldProps.CalculationExpression? fldProps.CalculationExpression: '';
			calOverExp = fldProps.CalculationOverrideExpression? fldProps.CalculationOverrideExpression: '';
			$('.calcExpProps').html(calExp); $('.calcOverrideExpProps').html(calOverExp);
			$('.calcExpProps, .calcOverrideExpProps').parent().show();
		}
		
		if (dataType == 5 && fldProps.LookupTable == '$QUERY$') {
			lkqueryExp = fldProps.LookupQuery? fldProps.LookupQuery: '';
			$('.lkQueryExpProps').html(lkqueryExp);
			$('.lkQueryExpProps').parent().show();
		}
		else $('.lkQueryExpProps').parent().hide();
		
		if (dataType == 5 || dataType == 11 || dataType == 3) {
			if (fldProps.AutoSelectFirstitem) $('.AutoSelectProp').attr('checkBox', 'checked');
			else $('.AutoSelectProp').removeAttr('checkBox', 'checked');
			if (fldProps.IsLargeLookup) $('.largeProp').attr('checkBox', 'checked');
			else $('.largeProp').removeAttr('checkBox', 'checked');
			$('.AutoSelectProp, .largeProp').parent().show();
			$('.precisionProp, .scaleProp').parent().show(); 
			$('.precisionProp').html(precision); $('.scaleProp').html(scale);	
		}
		else $('.AutoSelectProp, .largeProp').parent().hide();
		
		if (dataType == 5 || dataType == 11 || dataType == 3) {
			if (fldProps.AutoSelectFirstitem) $('.AutoSelectProp').attr('checkBox', 'checked');
			else $('.AutoSelectProp').removeAttr('checkBox', 'checked');
			$('.AutoSelectProp').parent().show();
		}
		else $('.AutoSelectProp').parent().hide();
		
		$('.editFieldPropWindow').show();
	}
}

function parcelDataSupportView() {
	localStorage.parcelDataSupportView = $('.support-toggle-switch-input')[0].checked ? '1': '0';
	switchSupportView(true);
}

function childCategoryhideAlert(pcatId) {
	let childCategoryDisabled = fieldCategories.filter(function(x) { return x.ParentCategoryId == pcatId });
	childCategoryDisabled.forEach(function(fldcat) {
		$('.support__hidAlert[category=' + fldcat.Id + ']').show();
		childCategoryhideAlert(fldcat.Id);
	});
}

function switchSupportView(reloadCats) {
	if (reloadCats) {
		showMask('Switching...');
		setTimeout(function() { hideMask() }, 5000);
	}
    //To make sure mask is shown
    let tOut = reloadCats? 500: 10;
    setTimeout(function() { 
        let catId = '1', catName = '';
        let supportId = $('.tabs-main li[content="parceldata"] a').attr('supportcategory'), mainId = $('.tabs-main li[content="parceldata"] a').attr('maincategory'); 
        let supportCat = fieldCategories.filter(function(x) { return x.Id == supportId })[0], mainCat = fieldCategories.filter(function(x) { return x.Id == mainId })[0];
        let showCategoryDisabled = fieldCategories.filter(function(x) { return x.ShowCategory != '1' });
        
        if (localStorage && localStorage.internalSupportView == '1' && localStorage.parcelDataSupportView == '1') {
            $('.fldPropIcons').show();
            $('.pk_fkField[showkeypropicons="FK"]').show();
            $('.pk_fkField[showkeypropicons="PK"]').show();
            $('.DoNotShowOnDEField[showFieldPropIcons="IconEnabled"]').show();
            $('.fieldTableColumnLabel').show();
            $('.fieldDisplayLabel').hide();
            $('.alwaysHideFieldProp').closest('tr').show();
            $('.alwaysHideFieldProp').attr('disabled', 'disabled');
            $('.hideFieldProp').closest('tr').show();
            $('.hideFieldProp').attr('disabled', 'disabled');
            $('.field-category-link[showCategoryTrue="0"]').css('display', 'block');
            $('#parceldata').addClass('parcel__supportView');
            if ($('.parcel__supportView .parcel-data-category-container')?.width() > 1250) {
                $('.fldPropcheckboxIcons').show();
                $('.fldPropcheckboxHead').show();
                $('.support__iconToggle').hide();
                $('.category-menu').removeClass('category-menu--wide');
            }
            else {
                if ($('.parcel__supportView .parcel-data-category-container')?.width() > 950)
                    $('.category-menu').addClass('category-menu--wide');
                else
                    $('.category-menu').removeClass('category-menu--wide');
                $('.support__iconToggle').show();
                $('.support__iconToggle-cb').attr('checked', false);
                $('.fldPropcheckboxIcons').hide();
                $('.fldPropcheckboxHead').hide();
            }
            showCategoryDisabled.forEach(function(fldcat) {
                $('.support__hidAlert[category=' + fldcat.Id + ']').show();
                childCategoryhideAlert(fldcat.Id);
            });
            catName = supportCat? supportCat.Name: '';
            catId = supportId;
            if (catName) {
                let onclickString = "openCategory('" + supportId + "', '" + catName + "','parceldata');$('.category-menu').show();return false;"
                $('.tabs-main li[content="parceldata"] a').attr('onclick', onclickString);
                $('.tabs-main li[content="parceldata"] a').attr('category', supportId);
            }		
        }
        else {
            $('.category-menu').removeClass('category-menu--wide');
            $('.fldPropIcons').hide();
            $('.fldPropcheckboxIcons').hide();
            $('.fldPropcheckboxHead').hide(); 
			$('.support__iconToggle-cb').attr('checked', false);            
            $('.pk_fkField').hide();
            $('.DoNotShowOnDEField').hide();
            $('.fieldTableColumnLabel').hide();
            $('.fieldDisplayLabel').show();
            $('.alwaysHideFieldProp').closest('tr').hide();
            $('.alwaysHideFieldProp').removeAttr('disabled');
            $('.hideFieldProp').closest('tr').hide();
            $('.hideFieldProp').removeAttr('disabled');
            $('.support__catHead').hide();
            $('.field-category-link[showCategoryTrue="0"]').css('display', 'none');
            $('#parceldata').removeClass('parcel__supportView');
            catName = mainCat? mainCat.Name: '';
            catId = mainId;
            if (catName) {
                let onclickString = "openCategory('" + mainId + "', '" + catName + "','parceldata');$('.category-menu').show();return false;"
                $('.tabs-main li[content="parceldata"] a').attr('onclick', onclickString);
                $('.tabs-main li[content="parceldata"] a').attr('category', mainId);
            }
        }
        
        if (reloadCats) {
        	let cat = $(".category-page:visible")[0];
            let CategoryId = $(cat).attr('categoryid');
            let findex = $('.category-page[categoryid="' + getParentCategories(CategoryId) + '"] span.aux-index').attr('findex');
            let showAuxParams = getParamOfAuxData(CategoryId)[0];
            let index = parseInt($('.aux-index', cat).html()) > 0 ? parseInt($('.aux-index', cat).html()) - 1 : 0;
            if (index <= 0) {
                let ai = parseInt($('.aux-index', cat).html());
                index = ai == 0 ? -1 : index;
            }

            $('.grid-view').html('');
            gridView();
            $('#classCalculatorFrame').hide();
            
            if(!(showCategoryDisabled.filter((x) => { return x.Id == CategoryId })[0])) {
            	$('.support-toggle').hide();
            	if (localStorage.internalSupportView == '1') {
					$('.support-toggle').show();
					if(localStorage.parcelDataSupportView == '1')
						$('.support-toggle-switch-input')[0].checked = true;
					else
						$('.support-toggle-switch-input')[0].checked = false;
				}
            	if (!__DTR && localStorage && localStorage.internalSupportView == '1' && localStorage.parcelDataSupportView == '1') {
    		        let af = $('.category-page[categoryid="' + CategoryId + '"]');
    		        $('.support__catHead', af).show();
    		   	}
            	$('.category-page[categoryid="' + getParentCategoryIds(CategoryId) + '"] span.aux-index').attr('findex', findex);
            	loadParentChildOfCategory(CategoryId);
            	highlightCatLink();
            	showAuxData(cat, index, showAuxParams.filter_field, showAuxParams.filter_value, showAuxParams.parentROWUID);
            }
            else {
            	$('.category-page').attr('findex', 0);
	            $('.category-page').attr('index', 0)
	            openCategory(catId, catName, 'parceldata', true);
            }
        }
    }, tOut);
}

function supportIconDVToggle(iconToggle) {
	let cat = $(".category-page:visible")[0];
    if($(iconToggle)[0].checked) {
        $('.fldPropIcons', cat).hide();
        if ($('.value', cat).length > 0) {
	        $('.fldPropcheckboxIcons', cat).show();
	        $('.fldPropEmpty', cat).attr('colspan', 2);
	        $('.fldPropcheckboxHead', cat).show();  
		}        
    } else {
        $('.fldPropcheckboxIcons', cat).hide();
        $('.fldPropcheckboxHead', cat).hide();    
        $('.fldPropIcons', cat).show();
    }
}

function loadDvRuleData(currentDvRule) {
	$('.mask').show();
	let catId = $(currentDvRule).parents('.support__catHead').attr('categoryid'); 
	let category = getCategory(catId);
	let stble = category && category.SourceTable? category.SourceTable: '';
	let html = '', headers = [{Name: 'Name', DisplayName: 'Name'}, {Name: 'SourceTable', DisplayName: 'Source Table'}, {Name: 'Condition', DisplayName: 'Condition'}, {Name: 'ErrorMessage', DisplayName: 'Error Message'}, {Name: 'FirstOnly', DisplayName: 'Validate first record only'}, {Name: 'IsSoftWarning', DisplayName: 'Enable soft warning'}];
	$('.mask').show(); 
	$('.dvRulePopupBody').html('');
	html = '<table><tr style="background: #e4e4e4;height: 22px;">';
	headers.forEach(function(hdr) {
    	html += ('<th style="text-align: center;">' + hdr.DisplayName + '</th>');
    });
    
    let dvRules = ccma.UI.Validations.filter(function(r) { return r.SourceTable == stble });
    dvRules.forEach(function(rule) {
    	html += '<tr>'
		headers.forEach(function(hdr) {
    		html += '<td style="text-align: center;">' + rule[hdr.Name] + '</td>';
    	});
		html += '</tr>'    	
    });
    $('.dvRulePopupBody').append(html + '</table>');
    $('.dvRulePopup').show();  
}

function closeFieldPropWindow() {
	$('.editFieldPropWindow').hide();
	$('.mask').hide();
}

function closedvRulePopup() {
	$('.dvRulePopup').hide();
	$('.mask').hide();
}

function getReadOnlyExpressionOverrideVal(field) {
    let categoryLevelReadOnlyExpressionOverride = false;
    if (field && datafieldsettings?.length > 0) {
        let dps = datafieldsettings.filter((x) => x.FieldId == field.Id)[0];
        if (dps && dps.PropertyName == "CategoryLevelReadOnlyExpressionOverride" && dps.Value == '1')
            categoryLevelReadOnlyExpressionOverride = true;
    }
    return categoryLevelReadOnlyExpressionOverride;
}

function refreshCatRecord(block, rowId, sTble) {
    recordSwitching.switches = [];
    $('.parcel-field-values .value', block).each(function (eIndex) { recordSwitching.switches[eIndex] = false });
    $('.parcel-field-values .value', block).each(function (a, f) {
        let t = $(f).attr('fieldname'), src = sTble && rowId ? activeParcel[sTble].filter((x) => { return x.ROWUID == rowId })[0] : activeParcel, field = src[t], fieldIndex = a;

        if (sTble && rowId && src) {
            $('.input', f).attr('auxrowuid', src.ROWUID);
            $('.input', f).siblings('span.qc').attr('auxrowuid', src.ROWUID);
        }

        setFieldValue(field, t, block, function (fieldIndex) {
            recordSwitching.switches[fieldIndex] = true;
            recordSwitching.swichingFn();
        }, null, null, null, fieldIndex);
    });

    $('.parcel-field-values td.value', block).removeClass('unsaved');
    for (x in activeParcelEdits) {
        let ed = activeParcelEdits[x];
        if ((ed.AuxROWUID || ed.AuxROWUID == 0) && ed.AuxROWUID.toString() == rowId.toString()) {
            $('.parcel-field-values td.value[fieldid="' + ed.FieldId + '"]', block).addClass('unsaved');
            setFieldValue(ed.QCValue, ed.FieldId, block);
        }
    }
}

function applyConditionalValidation(field, fieldset, source) {
    let validations = getConditionalValidationInputs(field, source), ptr = $(fieldset).closest('tr');
    $(fieldset).removeAttr('min max title msReqVal');
    $('.conditional-req', ptr).remove();
    $(fieldset).removeAttr('conditional-req');
    if (validations.length > 0) {
        validations = validations[0];
        if (validations.PERCENT_REQUIREMENT_TYPE_ID) {
            $(fieldset).attr({ msReqVal: validations.MAX_VALUE });
            $(fieldset).attr('title', validations.INPUT_TIP_TEXT + ' : ' + validations.MIN_VALUE + ' - ' + validations.MAX_VALUE);
            $(fieldset).attr({ min: validations.MIN_VALUE, max: validations.MAX_VALUE });
        }
        else {
            $(fieldset).attr({ min: validations.MIN_VALUE, max: validations.MAX_VALUE, allowDecimal: validations.DECIMAL_ALLOWED });
            $('.fieldDisplayLabel', ptr).text(validations.DESCRIPTION);
            $(fieldset).attr('title', validations.INPUT_TIP_TEXT);
        }

        if (validations.IS_REQUIRED && $('.required', ptr).length == 0) {
            let reqd = document.createElement('span');
            $(reqd).addClass('reqd conditional-req');
            reqd.innerHTML = '*';
            $('.ColoumnSep', ptr).after(reqd);
            $(reqd).css('color', 'red');
            $(fieldset).attr({ 'required': true, 'conditional-req': true });
        }
    }
    else
        $('.fieldDisplayLabel', ptr).text(field.Label);
}

function getConditionalValidationInputs(field, source) {
    let fieldConfig = field.ConditionalValidationConfig && field.ConditionalValidationConfig.split(',');
    if (!fieldConfig || (fieldConfig && fieldConfig.length < 2)) return false;

    let valTable = fieldConfig[0], expression = fieldConfig[1];
    expression.match(/\{.*?\}/g).forEach(function (fn) {
        let testFieldName = fn.replace('{', '').replace('}', ''), value = source[testFieldName];
        expression = expression.replace(fn, value);
    });

    for (x in ccma.FieldValidations[valTable][0]) {
        let regex = new RegExp("(?=\\b)(" + x + ")(?=\\b)", "g");
        expression = expression.replace(regex, 'thisObject.' + x)
    };

    let validations = eval('ccma.FieldValidations["' + valTable + '"].filter(function(thisObject) { return (' + expression + ') })');
    return validations;
}

function provalResSorting(arr, order) {
    let field = order.split(" ")[0].trim();
    arr.sort((a, b) => {
        if (a[field] === 'B') {
            return -1;
        } else if (b[field] === 'B') {
            return 1;
        } else if (a[field] === 'L') {
            return -1;
        } else if (b[field] === 'L') {
            return 1;
        } else if (a[field] === 'C') {
            return -1;
        } else if (b[field] === 'C') {
            return 1;
        } else if (a.key === '' || a.key === null || a.key === undefined) {
            return 1;
        } else if (b.key === '' || b.key === null || a.key === undefined) {
            return -1;
        } else if (a[field] === 'A') {
            return 1;
        } else if (b[field] === 'A') {
            return -1;
        } else {
            const aVal = parseFloat(a[field]);
            const bVal = parseFloat(b[field]);
            return aVal - bVal;
        }
    });

    return arr;
}

function recalculateCustomDropdownTextWidth(el, auxform) {
    let pr = $(el).parent().parent(), cs = document.createElement('canvas'), ct = cs.getContext('2d'), v = $(el).html();
    ct.font = '13.5px Arial'; let vw = ct.measureText(v).width;
    if (!auxform) {
        auxform = $(el).parents('.category-page')[0] ? $(el).parents('.category-page') : null;
    }
    if (vw > 232 && $(auxform).hasClass('category-page') && $(auxform).css('display') != 'none' && $(pr).css('display') != 'none') {
        let lw = $('.label', pr)[0] ? $('.label', pr)[0].offsetWidth : 0,
            fpw = $('.fldPropIcons', pr)[0] && $('.fldPropIcons', pr).css('display') != 'none' ? $('.fldPropIcons', pr)[0].offsetWidth : 0,
            fcw = $('.fldPropcheckboxIcons', pr)[0] && $('.fldPropcheckboxIcons', pr).css('display') != 'none' ? $('.fldPropcheckboxIcons', pr)[0].offsetWidth : 0,
            fw = $('.parcel-data-category-container')[0].offsetWidth, mw = lw + fpw + fpw + 20;
        if (fw > 0) {
            let aw = fw - mw - 5; vw = Math.round(Math.min(aw, (vw + 5), 500));
            $(el)[0].style.width = vw + 'px';
            $(el).attr('customWidth', vw);
        }
    }
}
    

