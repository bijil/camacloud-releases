﻿
function checkAsBoolean(bVal) {
    (typeof bVal == 'string') ? (bVal != '' ? (isNaN(bVal) ? '' : bVal = parseInt(bVal)) : '') : '';
    var type = typeof bVal, returnType = false;
    switch (type) {
        case 'string': returnType = ((bVal && bVal.toLowerCase() != 'false' && bVal.toLowerCase() != 'null' && bVal != '') || bVal == '1' || bVal.toLowerCase() == 'true') ? true : false; break;
        case 'number': returnType = (bVal == 0) ? false : true; break;
        case 'object': returnType = (bVal) ? true : false; break;
        case 'undefined': returnType = (bVal) ? true : false; break;
        case 'boolean': returnType = (bVal) ? true : false; break;
    }
    return returnType;
}


function getBool(val) {
    return !!JSON.parse(String(val).toLowerCase());
}


function getFieldCategoryName(categoryId) {

    return (fieldCategories.filter(function (d) { return d.Id == categoryId; })[0]) ? fieldCategories.filter(function (d) { return d.Id == categoryId; })[0].Name : null;
}

function getDatatypeFormatter(datatype) {
    try {
        if ((datatype != undefined) && (datatype != null)) {
            return QC.dataTypes[datatype].formatter;
        }
        else {
            return [];
        }
    }
    catch (e) {
        console.log(e.message);
    }
}

function getDatatypeField(FieldId) {
    try {
        if ((FieldId != undefined) && (FieldId != null)) {
            return $('table.parcel-field-values td[fieldid="' + FieldId + '"]').attr('datatype');
        }
    }
    catch (e) {
        console.log(e.message);
    }
}

function isReadOnly(FieldId) {
    try {
        if ((FieldId != undefined) && (FieldId != null)) {
            return ($('td.value[fieldid="' + FieldId + '"] .input').attr('readonly') == 'readonly')
        }
    }
    catch (e) {
        console.log(e.message);
    }
}

function getDatatypeFieldName(fieldId) {
    try {
        if ((fieldId != undefined) && (fieldId != null)) {
            return $('table.parcel-field-values td[fieldid="' + fieldId + '"]').attr('datatype');
        }
    }
    catch (e) {
        console.log(e.message);
    }
}

function isReadOnlyFieldName(FieldName,FieldID) {
    try {
        if ((FieldName != undefined) && (FieldName != null) && (FieldID != undefined) && (FieldID != null)) {
            return ($('td.value[fieldname="' + FieldName + '"][fieldid="'+FieldID+'"] .input').attr('readonly') == 'readonly')
        }
    }
    catch (e) {
        console.log(e.message);
    }
}

function getJsType(datatype) {
    try {
        if ((datatype != undefined) && (datatype != null)) {
            return QC.dataTypes[datatype].jsType;
        }
        else {
            return [];
        }
    }
    catch (e) {
        console.log(e.message);
    }
}

function formatThis(type, value, valueOrg,fieldId) {
    try {
        var typeOrg;
        var treturn;
        switch (type) {
            case 0:
                typeOrg = getJsType(getDatatypeField(value));
                valueOrg = convertDtype(typeOrg, valueOrg);
                return valueOrg.formatout(getDatatypeFormatter(getDatatypeField(value)));
            case 1:
                typeOrg = getJsType(getDatatypeFieldName(fieldId));
                valueOrg = convertDtype(typeOrg, valueOrg);
                return valueOrg.formatout(getDatatypeFormatter(getDatatypeFieldName(fieldId)));


        }
    }
    catch (e) {
        console.log(e.message);
    }
}

function convertDtype(type, value) {
    try {
        switch (type) {
            case 'String':
            case 'string':
                return safeParseString(value);
                break;
            case 'Number': return Number(value);
                break;
            case 'Date': return parseDate(value);
                break;
            case 'Date': return parseDate(value);
                break;
        }
    }
    catch (e) {
        console.log(e.message);
    }
}
