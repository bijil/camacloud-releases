﻿var filterData = { ID:"MainNode",
	Operator : $('#start').text(),
	Children : [],
	Name : ''
};
var lkpdata;
var q = "";
var tabledata = {};
var Issavedfilter = false;
var oldTempName = "";
var CurrNode;
var EditNode;
var SelectedNodeID;
var lookupData;
var modalOpr;
var btnOpr;
var spanOPr;
var modal;
var btn;
var span;
$(function() {
    $('#condition').on('click', function() {
        modal.style.display = "block";
        CurrNode = this;
        clearAllValues();
        loadfieldcategory();
        filterData.Operator = $('#start').text();
    });

    $('.close').first().on('click', function() {
        EditNode = "";
        modal.style.display = "none";
        $(".linkPanel").html("ADD");
    });

    $('.ddlCategory').on("change", function() {
        loadDataSourceField($(this));
        $('#ddlCon').prop('selectedIndex', 0);
        $('#ddlCon').find('option').not(':first').hide();
        $("#txtValue2").val('').css({
            "display": "none"
        });
        $("#txtValue1").val('').css({
            "display": "block"
        });
        $("#ddllkp").val('').hide();
    });

    $('.ddlCategory, .ddlField, .ddlCondition, .ddllookup').hover(function() {
        $('option', this).first().attr('disabled', 'disabled');
    }, function() {
        $('option', this).first().removeAttr('disabled');
    });

    $(".ddlField").on("change", function() {
        selectConditonOption(this);
    });
    modal = document.getElementById("myModal");
    btn = document.getElementById("condition");
    span = document.getElementsByClassName("close")[0];
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }

    modalOpr = document.getElementById("Operator");
    btnOpr = document.getElementById("block");
    spanOPr = document.getElementsByClassName("close")[1];
    btnOpr.onclick = function() {
        modalOpr.style.display = "block";
        CurrNode = btnOpr;
    }
    spanOPr.onclick = function() {
        modalOpr.style.display = "none";
    }
    window.onclick = function(event) {
        if (event.target == modalOpr) {
            modalOpr.style.display = "none";
        }
    }

    $('#Condition-Node').click(function() {
        var valid = validateAddOrUpdate();
        if (valid != "validated") {
            alert(valid);
            return;
        }
        if (EditNode && EditNode != "") {
            EditConditionNode(SelectedNodeID, EditNode);
            buildResultQuery();
        } else {
            EditConditionNode(SelectedNodeID, EditNode, true);
        }
    });
    
    $(".ddlCondition").bind("change", function() {
        if ($(this).val() !== 'BETWEEN') {
            $("#txtValue2").css({
                "display": "none"
            });
        } else {
            $("#txtValue2").css({
                "display": "block"
            });
        }

        if ($(this).val() === 'IS NULL' || $(this).val() === 'IS NOT NULL') {
        	$("#txtValue1, #txtValue2, #ddllkp").val('');
            $("#txtValue1").attr("disabled", "disabled");
            $("#txtValue2").attr("disabled", "disabled");
            $("#ddllkp").attr("disabled", "disabled");
        } else {
            $("#txtValue1").removeAttr("disabled");
            $("#txtValue2").removeAttr("disabled");
            $("#ddllkp").removeAttr("disabled");
        }
    });
    $(document).on("click", "button.DelNode", function() {
        var id = "";
        $(this).closest('li').remove();
        if ($(this).siblings()[0].innerHTML == "AND" || $(this).siblings()[0].innerHTML == "OR")
            id = $(this).closest('ul')[0].id
        else
            id = $(this).closest('li')[0].id
        deleteNodeData(filterData, id);
        buildResultQuery();
    });

    $(document).on("click", "button.add", function() {
        $(this).closest('ul').append("<li><button class='add' id='btnadd'>ADD</button><button class='remove' id='btnremove'>REMOVE</button></li>");
    });
    filterData.Operator = $('#start').text();
    lkpdata = $('#CustomQueryBuilder').data('lookupData');
    loadfieldcategory();
    if ($("#Issave", window.parent.document).val()) {
        var jdata = $("#Issave", window.parent.document).attr('jsondata');
        if (jdata) {
            filterData = JSON.parse(jdata);
            oldTempName = $("#filter-name", window.parent.document).val();
            buildTree(filterData, document.getElementById('MainNode'));
        }
    }
    var query = "SELECT ID,CC_TargetTable FROM DataSourceTable";
    window.parent.$ds('loadfieldcategory', {
        Query: query
    }, function(res) {
        $(res).each(function() {
            tabledata[this.ID] = this.CC_TargetTable;
        });
    });
});
function selectConditonOption(thisElem) {
    $("select#ddlCon").prop('selectedIndex', 0);
    var fieldSelector = thisElem.options[thisElem.selectedIndex];
    var inpType = $(fieldSelector).attr("inputtype");
    var fieldname = $(fieldSelector).attr("fieldname");
    var lookupname = $(fieldSelector).attr("Lookup");
    var id = $(fieldSelector).val();
    switch (inpType) {
        case "1":
        case "6":
        case "5":
            $("#ddlCon option[value='>']").hide();
            $("#ddlCon option[value='<']").hide();
            $("#ddlCon option[value='>=']").hide();
            $("#ddlCon option[value='<=']").hide();
            $("#ddlCon option[value='BETWEEN']").hide();
            $("#ddlCon option[value='0']").show();
            $("#ddlCon option[value='=']").show();
            $("#ddlCon option[value='<>']").show();
            $("#ddlCon option[value='IS NULL']").show();
            $("#ddlCon option[value='IS NOT NULL']").show();
            if(inpType !== '5'){
            		$("#ddlCon option[value='IN']").show();
	            $("#txtValue1").prop({
	                'type': 'text',
	                'placeholder': 'Enter a value'
	            });
	            $("#txtValue2").prop({
	                'type': 'text',
	                'placeholder': 'Enter a value'
	            });
	            $("#txtValue1").show();
	            $("#ddllkp").hide();
	      } else {
	      		document.getElementById('ddlCon').selectedIndex = 1;
	            $("#ddllkp").show();
	            $("#txtValue1").hide();
	            $("#txtValue2").hide();
	            copyLookupToDropdown('.ddllookup', fieldname, lookupname, id);
	      }
            break;
        case "2":
        case "8":
        case "10":
        case "7":
        case "9":
            $("#ddlCon option[value='>']").show();
            $("#ddlCon option[value='<']").show();
            $("#ddlCon option[value='>=']").show();
            $("#ddlCon option[value='<=']").show();
            $("#ddlCon option[value='BETWEEN']").show();
            $("#txtValue1").prop('type', 'number');
            $("#txtValue2").prop('type', 'number');
            $("#txtValue1").show();
            $("#ddllkp").hide();
            $("#ddlCon option[value='0']").show();
            $("#ddlCon option[value='=']").show();
            $("#ddlCon option[value='<>']").show();
            $("#ddlCon option[value='IN']").hide();
            $("#ddlCon option[value='IS NULL']").show();
            $("#ddlCon option[value='IS NOT NULL']").show();
            break;
        case "4":
            $("#ddlCon option[value='>']").show();
            $("#ddlCon option[value='<']").show();
            $("#ddlCon option[value='>=']").show();
            $("#ddlCon option[value='<=']").show();
            $("#ddlCon option[value='BETWEEN']").show();
            $("#txtValue1").prop('type', 'date');
            $("#txtValue2").prop('type', 'date');
            $("#txtValue1").show();
            $("#ddllkp").hide();
            $("#ddlCon option[value='0']").show();
            $("#ddlCon option[value='=']").show();
            $("#ddlCon option[value='<>']").show();
            $("#ddlCon option[value='IN']").hide();
            $("#ddlCon option[value='IS NULL']").show();
            $("#ddlCon option[value='IS NOT NULL']").show();
            break;
        default:
            $("#ddlCon option[value='>']").hide();
            $("#ddlCon option[value='<']").hide();
            $("#ddlCon option[value='>=']").hide();
            $("#ddlCon option[value='<=']").hide();
            $("#ddlCon option[value='BETWEEN']").hide();
            $("#ddlCon option[value='0']").show();
            $("#ddlCon option[value='=']").show();
            $("#ddlCon option[value='<>']").show();
            $("#ddlCon option[value='IN']").hide();
            $("#ddlCon option[value='IS NULL']").show();
            $("#ddlCon option[value='IS NOT NULL']").show();
            $("#txtValue1").prop('type', 'text');
            $("#txtValue2").prop('type', 'text');
            $("#txtValue1").show();
            $("#txtValue2").hide();
            $("#ddllkp").hide();
            break;
    }
}

function switchOperator(Opr) {
    if (Opr.innerHTML == "AND")
        Opr.innerHTML = "OR";
    else
        Opr.innerHTML = "AND";
    buildResultQuery();
    UpdateOprInFilterdata(filterData, Opr);
}

function clearAllValues() {
    document.getElementById("ddlcat").selectedIndex = "0";
    document.getElementById("ddlF").selectedIndex = "0";
    document.getElementById("ddlCon").selectedIndex = "0";
    document.getElementById("txtValue1").value = "";
    document.getElementById("txtValue2").value = "";
    $('#ddlCon').find('option').not(':first').hide();
    $("#txtValue2").css({
        "display": "none"
    });
    $("#txtValue1").css({
        "display": "block"
    });
    $("#ddllkp").val('');
    $("#ddllkp").hide();
    $('.ddlField').find('option').not(':first').remove();
}

$(document).on("click", "button.ConditionNode", function() {
    EditNode = this;
    SelectedNodeID = $(this).closest('li')[0].id;
    $(".linkPanel").html("UPDATE");
    modal.style.display = "block";
    getNodeDetails(filterData, SelectedNodeID);
    return;
});

//Restore selected values into dropdown for edit...
function getNodeDetails(data, id) {
    var Nflag = false;
    var index = 0;
    data.Children.forEach(function(node) {
        if (node.ID == id) {
            SetDropdownForEdit(node);
            Nflag = true;
            return;
        }
    });

    if (!Nflag) {
        data.Children.forEach(function(node) {
            if (node.hasOwnProperty("Children")) {
                getNodeDetails(node, id);
            }
        });
    } else return;
}

function SetDropdownForEdit(NodeObj) {
    loadfieldcategory(function() {
        $('#ddlcat').val('0');
        if (NodeObj.CategoryID && $('#ddlcat').html().indexOf(NodeObj.CategoryID) > -1)
            $('#ddlcat').val(NodeObj.CategoryID);
        loadDataSourceField($('.ddlCategory'), function() {
            $('#ddlF').val('0');
            if (NodeObj.FieldID && $('#ddlF').html().indexOf(NodeObj.FieldID) > -1)
                $('#ddlF').val(NodeObj.FieldID);
            $('#ddlCon').val('0');
            selectConditonOption($('#ddlF')[0]);
            if (NodeObj.ConditionID){
                $('#ddlCon').val(NodeObj.ConditionID);
                NodeObj.ConditionID === 'IS NULL' || NodeObj.ConditionID === 'IS NOT NULL' ? $("#txtValue1, #txtValue2, #ddllkp").attr("disabled", "disabled") : $("#txtValue1, #txtValue2, #ddllkp").removeAttr("disabled");
                NodeObj.ConditionID === 'BETWEEN' ? $("#txtValue1, #txtValue2").show() : $("#txtValue2").hide();
             }
            document.getElementById("txtValue1").value = NodeObj.Value1;
            document.getElementById("txtValue2").value = NodeObj.Value2;
            $('#ddllkp').val(NodeObj.lookupvalue)
        });
    });
}

function EditConditionNode(ID, NodeObj, updateCall) {
    var values;
    var cond;
    var lkpcondition;
    var cat = $('#ddlcat').val();
    var field = $('#ddlF').val();
    var con = $('#ddlCon').val();
    var value = $('#txtValue1').val();
    var value2 = $('#txtValue2').val();
    var lookupvalue = $('#ddllkp').val();
    var datatype = $('#ddlF option:selected').attr('inputtype');
    var opr = getOperator($('#ddlCon option:selected').text());
    if (con !== 'BETWEEN') {
    	  pciCond = $('#ddlF option:selected').attr('fieldname') + " " + opr + " " + getValueSqlString(opr, value, datatype, true);
        if (datatype == "1" || datatype == "6" || datatype == "4" || datatype == "11" || datatype == "12" || datatype == "3")
            cond = $('#ddlF option:selected').attr('fieldname') + " " + opr + " " + getValueSqlString(opr, value, datatype);
        else if (datatype == "5") {
        	if(opr !== 'IS NULL' && opr !== 'IS NOT NULL'){
        	     pciCond = $('#ddlF option:selected').attr('fieldname') + " " + opr + " " + getValueSqlString(opr, lookupvalue, datatype, true);
	            cond = $('#ddlF option:selected').attr('fieldname') + " " + opr + " '"+ $('#ddllkp option:selected').val() + "'";
	            lkpcondition = $('#ddlF option:selected').attr('fieldname') + " " + ($('#ddllkp option:selected').text() == "" ? opr + " " + "''"  : opr + " '" + $('#ddllkp option:selected').text() + "'");
        	} else {
        		cond = $('#ddlF option:selected').attr('fieldname') + " " + opr;
	            lkpcondition = $('#ddlF option:selected').attr('fieldname') + " " + opr;
        	}
        } else
            cond = $('#ddlF option:selected').attr('fieldname') + " " + opr + " " + $('#txtValue1').val();

        var node = {
            ID: null,
            Text: cond,
            DisplayText: lkpcondition || cond,
            ParcelChangeCond : pciCond,
            CategoryID: cat,
            Category: $('#ddlcat option:selected').text(),
            FieldID: field,
            FieldName: $('#ddlF option:selected').text(),
            TableID: $('#ddlF option:selected').attr('TableId'),
            ConditionID: con,
            ConditionString: opr,
            LookupName: $('#ddlF option:selected').attr('Lookup'),
            ValueString: getValueSqlString(opr, value, datatype),
            Value1: value,
            Value2: value2,
            lookupvalue: lookupvalue
        }
        var ConditionBtnText = lkpcondition || cond;
        if (!updateCall) {
            NodeObj.innerHTML = ConditionBtnText;
            UpdateConditionInFilterdata(filterData, NodeObj, node)
        } else
            createConditionNode(ConditionBtnText, node);
        modal.style.display = "none";
        EditNode = "";
    } else {
        switch (datatype) {
            case "2":
	     case "8":
	     case "10":
                values = value + " AND " + value2;
                break;
            case "1":
            case "6":
            case "5":
            case "4":
                values = "'" + value + "' AND '" + value2 + "'";
                break;
        }
       cond = $('#ddlF option:selected').attr('fieldname') + " " + opr + " " + values;
       pciCond = $('#ddlF option:selected').attr('fieldname') + " " + opr + " " + "'" + value + "' AND '" + value2 + "'";
	var node = {ID:null,
				Text : cond,
				DisplayText: lkpcondition || cond,
				ParcelChangeCond : pciCond,
				CategoryID : $('#ddlcat option:selected').val(),
				TableID : $('#ddlF option:selected').attr('TableId'),
				FieldID : $('#ddlF option:selected').val(),
				LookupName : $('#ddlF option:selected').attr('Lookup'),
				ConditionString : opr,
				ConditionID : con,
				ValueString : getValueSqlString(opr,value,datatype),
				Value1 : $('#txtValue1').val(),
				Value2 : $('#txtValue2').val(),
				lookupvalue: lookupvalue
				}
        if (!updateCall) {
            cond = $('#ddlF option:selected').attr('fieldname') + " " + opr + " " + values;
            NodeObj.innerHTML = cond;
            UpdateConditionInFilterdata(filterData, NodeObj, node);
        } else
            createConditionNode(cond, node);
        modal.style.display = "none";
        EditNode = "";
    }
}

function getOperator(condition) {
    var Opr;
    switch (condition) {
        case "Equal to":
            Opr = "=";
            break;
        case "Not Equal to":
            Opr = "<>";
            break;
        case "Greater than":
            Opr = ">";
            break;
        case "Less than":
            Opr = "<";
            break;
        case "Greater than or Equal to":
            Opr = ">=";
            break;
        case "Less than or equal to":
            Opr = "<=";
            break;
        case "IN":
            Opr = "IN";
            break;
        case "Is NULL":
            Opr = "IS NULL";
            break;
        case "Is Not NULL":
            Opr = "IS NOT NULL";
            break;
        case "BETWEEN":
            Opr = "BETWEEN";
            break;
    }
    return (Opr);
}

function validateAddOrUpdate() {
    var cat = $('#ddlcat').val();
    var field = $('#ddlF').val();
    var con = $('#ddlCon').val();
    var value1 = $('#txtValue1').val();
    if (cat == 0) {
        return "Please select a category.";
    } else if (field == 0) {
        return "Please select a field.";
    } else if (con == 0) {
        return "Please select a condition.";
    }
    if (con === 'BETWEEN') {
        var value2 = $('#txtValue2').val();
        if (!value1 || !value2)
            return "Please enter the values";
        else
            return "validated";
    } else {
        var datatype = $('#ddlF option:selected').attr('inputtype');
        if (!value1 && (con !== 'IS NULL' && con != 'IS NOT NULL') && datatype != 5)
            return "Please enter a value";
        else
            return "validated";
    }
}

function getValueSqlString(condition, values, datatype, pciValue) {
    if(pciValue){
    		if(condition === "IN")
    			 values = "('" + values.replace(/,/g, "','") + "')";
    		else if (condition ==='IS NULL' || condition ==='IS NOT NULL')
    			values = values;
    		else
    			 values = "'" + values + "'";
    }
    else if (datatype === "1" || datatype === "6" || datatype === "5" || datatype === "4" || datatype === "12" || datatype === "11" || datatype === "3") {
        switch (condition) {
            case "=":
                values = "'" + values + "'";
                break;
            case "<>":
                values = "'" + values + "'";
                break;
            case "IN":
                values = "('" + values.replace(/,/g, "','") + "')";
                break;
        }
    }
    return values;
}

// To update each Condition Node node data into FilterData..
function UpdateConditionInFilterdata(data, CondNode, obj) {
    var Nflag = false;
    var liID = "";
    var ParentUl = CondNode.closest('ul');
    var i = 0;
    data.Children.forEach(function(node) {
        liID = CondNode.closest('li');
        if (node.ID == liID.id) {
            node.Text = obj.Text;
            node.DisplayText = obj.DisplayText;
            node.ParcelChangeCond = obj.ParcelChangeCond;
            node.CategoryID = obj.CategoryID;
            node.TableID = obj.TableID;
            node.FieldID = obj.FieldID;
            node.LookupName = obj.LookupName;
            node.ConditionString = obj.ConditionString;
            node.ConditionID = obj.ConditionID;
            node.ValueString = obj.ValueString;
            node.Value1 = obj.Value1;
            node.Value2 = obj.Value2;
            node.lookupvalue = obj.lookupvalue;
            Nflag = true;
            return;
        }
    });
    if (!Nflag) {
        data.Children.forEach(function(node) {
            if (node.hasOwnProperty("Children"))
                UpdateConditionInFilterdata(node, CondNode, obj);
        });
    }
}

//Save Filter button click...
$(".savecustomfilter", window.parent.document).click(function() {
    $('.close').first().trigger('click');
    if($('#filter-name', window.parent.document).val() == ""){
	alert("Please enter a name for search filter.");
	return false;
    }
    var test;
    var id = '';
    var sel = $('.qc-selected-template');
    var tid = $(sel).val();

    var qid = $("#filterID", window.parent.document).val();
    if (qid == "") {
        alert("Please enter a name for search filter.");
        return;
    }

    if (ValidateTree("MainNode")) {
        test = getCompiledQuery(filterData);
    } else {
        alert("There is no sufficient Operands.");
        return;
    }
    filterData.Name = $("#filter-name", window.parent.document).val();
    var saveData = {
        QueryId: qid,
        Name: $("#filter-name", window.parent.document).val(),
        FilterData: test + "^" + JSON.stringify(filterData)
    };

    window.parent.$qc('savecustomquery', saveData, function(resp) {
        if (resp.status == "OK") {
        	if(resp.Id)  $("#filterID", window.parent.document).val(resp.Id);
            alert("Query saved successfully.!");
            window.parent.loadQueries();
            window.parent.cancelTemplateSaveForm();
            window.parent.reAssignDefaultTemplate();
            var template_name = $("#filter-name", window.parent.document).val();
            if ($('.templateName', window.parent.document).css('display') != 'none') {
                $('.templateName', window.parent.document).contents().filter(function() {
                    return this.nodeType == 3
                }).each(function() {
                    this.textContent = this.textContent.replace(oldTempName, template_name);
                    if (template_name.length > 21) {
                        $('.templateName', window.parent.document).html(template_name.length > 21 ? template_name.substring(0, 30) + "..." : template_name);
                        var open = document.createElement('a');
                        open.innerHTML = 'Open';
                        open.className = 'open-filter';
                        $('.templateName', window.parent.document).append(open);
                    }
                });
            }
        } else {
            alert(resp.message);

        }
    }, function(err) {
        alert('Save failed! - ' + err);

    });
});

$(".apply", window.parent.document).click(function() {
    if($('#filter-name', window.parent.document).val() == ""){
	alert("Please enter a name for search filter.");
	return false;
    }
    if (ValidateTree("MainNode")) {
    	  window.parent.currentSearchTemplate.clear();	
    	  window.parent.currentSearchTemplate.render('.filter-container');
        window.parent.document.getElementById('custom-filtername').value = $("#filter-name", window.parent.document).val();
        $(".filter-customfilter", window.parent.document).show();
        $(".templateName", window.parent.document).hide();
        $('body', window.parent.document).css('overflow', 'auto');
        $('.windowMask', window.parent.document).hide();
        $('#CustomQueryBuilder', window.parent.document).hide();
        CustQuery = getCompiledQuery(filterData);
        window.parent.doCustomSearch(CustQuery, true);
    } else {
        alert("There is no sufficient Operands.");
        return;
    }
});

function getCompiledQuery(data) {
    var Nflag = false;
    var index = 0;
    var id = data.ID;
    var queryF = "";
    var JoinString = "";
    var opr = data.Operator;
    var tabName = "";
    var output = {
        Join: null,
        Condition: null
    };
    data.Children.forEach(function(node) {
        if (node.ID.startsWith("ul")) {
            queryF = queryF + getCompiledSubQuery(node) + " " + opr + " ";
            JoinString += getJoinStringofSubQuery(node);
        } else {
        	if(node.DisplayText.includes('Is NULL') || node.DisplayText.includes('Is not NULL')){
        		queryF = queryF + " ("+tabledata[node.TableID] + "." + node.DisplayText+ " AND (df.Id = "+node.FieldID+" AND "+node.ParcelChangeCond.replace(node.ParcelChangeCond.substring(0, node.ParcelChangeCond.indexOf(' ')),'pc.NewValue')+" )) " + opr + " ";
        	} else
            		queryF = queryF + " ("+tabledata[node.TableID] + "." + node.Text+ " OR (df.Id = "+node.FieldID+" AND "+node.ParcelChangeCond.replace(node.ParcelChangeCond.substring(0, node.ParcelChangeCond.indexOf(' ')),'pc.NewValue')+" )) " + opr + " ";
            var temp = " LEFT OUTER JOIN " + tabledata[node.TableID] + " ON p.Id = " + tabledata[node.TableID] + ".CC_ParcelId ";
            if (!JoinString.includes(temp))
                JoinString = JoinString + temp;
        }
    });
    queryF = queryF.trim();
    queryF = queryF.replace(/AND$|OR$/, '');
    output.Join = JoinString;
    output.Condition = queryF;
    return JoinString + " ^ " + queryF;
}

function getCompiledSubQuery(obj){
	var queryF = "";
	var JoinString = "";
	var opr = obj.Operator;
		obj.Children.forEach(function(node){
			if(node.ID.startsWith("li")){
				queryF = queryF + tabledata[node.TableID] + "." + node.Text + " " + opr + " ";
			}
			else{
				queryF =  queryF + getCompiledSubQuery(node) + " " + opr + " ";
			}
		});
		queryF = queryF.trim();
		queryF = queryF.replace(/AND$|OR$/, '');
		return "(" + queryF + ")";
}

function getJoinStringofSubQuery(obj) {
    var JoinString = "";
    obj.Children.forEach(function(node) {
        if (node.ID.startsWith("li")) {
            var temp = " LEFT OUTER JOIN " + tabledata[node.TableID] + " ON P.ID = " + tabledata[node.TableID] + ".CC_ParcelId ";
            if (!JoinString.includes(temp))
                JoinString += temp;
        } else {
            JoinString += getJoinStringofSubQuery(node);
        }
    });
    JoinString = JoinString.trim();
    return JoinString;
}

// Used to populate saved filter data into Tree view..
function buildTree(data, container) {
    $("#start").html(data.Operator);
    data.Children.forEach(function(node) {
        if (node.ID.startsWith("li")) {
            AddConditionNode(container, node.DisplayText, node);
        } else if (node.ID.startsWith("ul")) {
            AddOperatorNode(container, btn, node.Operator, node);
        }
    });
}

// To create new condition node into Tree view	
function createConditionNode(text, obj) {
    var ParentUl = CurrNode.closest('ul');
    var liNo = $('ul#MainNode li').length;
    var mainUl = document.getElementById('condition');
    var main = document.getElementById('MainNode');
    var node = document.createElement('div');
    var listItem = document.createElement('li');
    listItem.setAttribute("id", "li-" + (liNo + 1));
    var buttonAdd = document.createElement("button");
    buttonAdd.innerHTML = text;
    buttonAdd.setAttribute("class", "Node ConditionNode");
    buttonAdd.setAttribute("id", "Fld-" + (liNo + 1));
    buttonAdd.setAttribute("style", "text-align:start;word-break:break-all;max-width:67%;min-width:auto;");
    listItem.appendChild(buttonAdd);

    var delbutton = document.createElement("button");
    delbutton.innerHTML = "&#10006";
    delbutton.setAttribute("class", "DelNode");
    delbutton.setAttribute("id", "del-" + (liNo + 1));
    listItem.appendChild(delbutton);
    var pid = mainUl.closest('ul');
    ParentUl.appendChild(listItem);
    var listNo = $("ul#" + ParentUl.id).children('li').length
    var pnode;
    if (ParentUl.id == "MainNode")
        pnode = 0;
    else
        pnode = 1;
    ParentUl.insertBefore(listItem, ParentUl.childNodes[listNo - pnode]);
    obj.ID = "li-" + (liNo + 1);
    buildResultQuery();
    //inserNode(obj);
    insertConditionNode(filterData, ParentUl.id, obj);
    CurrNode = "";
}

// To create new Operator node into Tree view	
function createOperatorNode(btn) {
    var ParentUl = CurrNode.closest('ul');
    var mainUl = document.getElementById('condition');
    var main = document.getElementById('MainNode');
    var node = document.createElement('div');
    var UlNo = $("#container ul").length;
    var liNo = $('ul#MainNode li').length;
    var liH = document.createElement('li');
    liH.setAttribute("id", "li-" + (liNo + 1));
    var list = document.createElement('ul');
    list.setAttribute("class", "tree");
    list.setAttribute("id", "ul-" + (UlNo + 1));
    var buttonAdd = document.createElement("button");
    buttonAdd.innerHTML = $('#' + btn.id).text();
    buttonAdd.setAttribute("id", "opr-" + (UlNo + 1));
    buttonAdd.setAttribute("class", "Node OperatorNode");
    buttonAdd.setAttribute("onclick", "switchOperator(this)");
    list.appendChild(buttonAdd);
    var delbutton = document.createElement("button");
    delbutton.innerHTML = "&#10006";
    delbutton.setAttribute("class", "DelNode");
    delbutton.setAttribute("id", "del-" + (liNo + 1));
    list.appendChild(delbutton);
    liH.appendChild(list);
    ParentUl.appendChild(liH);
    var listNo = $("ul#" + ParentUl.id).children('li').length
    var pnode;
    if (ParentUl.id == "MainNode")
        pnode = 0;
    else
        pnode = 1;
    ParentUl.insertBefore(liH, ParentUl.childNodes[listNo - pnode]);
    pushNodes(list.id);
    insertLogOperatorNode(filterData, buttonAdd, ParentUl.id, list);
    modalOpr.style.display = "none";
    CurrNode = "";
}

function pushNodes(main) {
    var liNo = $('ul#MainNode li').length;
    $('#' + main).append("<li id='li-" + (liNo + 1) + "'><button class='NewNode' id='condition-" + (liNo + 1) + "' onclick='addChildCondition(this)'>a < b</button></li>");
    $('#' + main).append("<li id='li-" + (liNo + 1) + "'><button class='NewNode' id='block-" + (liNo + 1) + "' onclick='addChildBlock(this)'>(...)</button></li>");
}



function clearDropdowns() {
    $('.ddlCategory').find('option').not(':first').remove();
    $('.ddlField').find('option').not(':first').remove();
}


function addChildCondition(btnC) {
    var modal = document.getElementById("myModal");
    var btn = document.getElementById(btnC.id);
    var span = document.getElementsByClassName("close")[0];
    modal.style.display = "block";
    CurrNode = btn;
    clearAllValues();
    span.onclick = function() {
        modal.style.display = "none";
        $(".linkPanel").html("ADD");
        EditNode = "";
        CurrNode = "";
    }
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
}

function addChildBlock(btnB) {
    var modalOpr = document.getElementById("Operator");
    var btnOpr = document.getElementById(btnB.id);
    var spanOPr = document.getElementsByClassName("close")[1];
    modalOpr.style.display = "block";
    CurrNode = btnOpr;
    spanOPr.onclick = function() {
        modalOpr.style.display = "none";
    }
    window.onclick = function(event) {
        if (event.target == modalOpr) {
            modalOpr.style.display = "none";
        }
    }
}

function inserNode(obj){
	filterData.Children.push(obj);
}

//To show derived query from the tree into "Final filter Result" window
function buildResultQuery(){
		var fid = "";
		var filter = "";
		var result = "";		
		var opr = $('#start').text();		
		$("#MainNode").children('li').each(function() {
			if($(this).children('ul').length == 1){
				filter = filter + buildSubquery($(this).children('ul').attr("id")) + " @ ";
			}
			else{
				fid = $(this).children('button').attr("id");
				if (fid.substring(0, 3) == 'Fld'){
					filter = filter + $('#'+fid).text() + " @ ";
				}
			}	
		});
		filter = filter.trim();
		filter = filter.replace(/\@$/, '');//filter.substring(0, filter.length - 1);
		$("#final-filter").val(filter.replace(/@/g,opr));
}

function buildSubquery(parentUl){
	var fid = "";
	var filter = "";
	var opr = $("#"+parentUl).children('button.Node.OperatorNode').text();	
	$('#'+parentUl).children('li').each(function(){
		if($(this).children('ul').length == 1){
			filter = filter + buildSubquery($(this).children('ul').attr("id"));
		}
		else{
			fid = $(this).children('button').attr("id");
			if (fid.substring(0, 3) == 'Fld'){
				filter = filter + $('#'+fid).text() + " @ ";
			}
		}	
	});
	filter = filter.trim();
	filter = filter.replace(/\@$/, '');//filter.substring(0, filter.length - 1);
	return (" (" + filter.replace(/@/g,opr) + ") ");
}

//Load all the field category into dropdown...
function loadfieldcategory(callback) {
    window.parent.$ds('loadfieldcategory', {
        Query: "SELECT DISTINCT Id,Name FROM FieldCategory"
    }, function(res) {
        var ddlFieldcat = $(".ddlCategory");
        ddlFieldcat.empty();
        ddlFieldcat.append("<option value='0'>-Select Category-</option>");
        $(res).each(function() {
            $(ddlFieldcat).append($("<option></option>")
            		.attr("value",this.Id)
            		.text(this.Name)); 
        });
        if(callback) callback();
    }, function(){
    	if(callback) callback();
    });
}

//Load all fields of selected category into Dropdown...
function loadDataSourceField(FieldData, callback) {
    window.parent.$ds('loadfieldcategory', {
        Query: "SELECT Id, AssignedName, COALESCE(DisplayLabel, '') +' ('+COALESCE('' + AssignedName, '')+')' as Name, Datatype AS TYPE1, TableId, isnull(LookupTable,'NULL') as LookupTable FROM DataSourceField WHERE CategoryId = " + FieldData.val() + " ORDER BY Name"
    }, function(res) {
        var ddlField = $(".ddlField");
        ddlField.empty();
        ddlField.append("<option value='0' inputtype='' TableId='' fieldname='' Lookup=''>-Select Field-</option>");
        $(res).each(function() {
            $(ddlField).append($("<option></option>")
                .attr("value", this.Id)
                .attr('inputtype', this.TYPE1)
                .attr('TableId', this.TableId)
                .attr('fieldname', this.AssignedName)
                .attr('Lookup', this.LookupTable)
                .text(this.Name));
        });
        if (callback) callback();
    }, function() {
        if (callback) callback();
    });
}

// To insert each condition node data into FilterData..
function insertConditionNode(data,parent,obj){
	var Nflag = false;
	if(parent == "MainNode"){
		data.Children.push(obj);
		return;
	}
		data.Children.forEach(function(node){
			if(node.ID == parent){
				node.Children.push(obj);
				Nflag = true;
				return;
			}
		});
		
		if(!Nflag){
			data.Children.forEach(function(node){
				if(node.hasOwnProperty("Children")){
					insertConditionNode(node,parent,obj)
				}
			});
		}
}

// To insert each Operator Node node data into FilterData..
function insertLogOperatorNode(data,Operator,Parent,ul){
	var Nflag = false;
	if (Operator.id == 'start'){
		data.OprID = Operator.id;
		data.Operator = Operator.textContent;
	}
		
	var obj = {ID : ul.id, Operator : Operator.textContent, Children : []}
	if(Parent == "MainNode"){
		data.Children.push(obj);
		return;
	}
	data.Children.forEach(function(node){
		if(node.ID == Parent){
			node.Children.push(obj);
			Nflag = true;
			return;
		}
	});
	if(!Nflag){
		data.Children.forEach(function(node){
			if(node.hasOwnProperty("Children")){
				insertLogOperatorNode(node,Operator,Parent,ul);
			}
		});
	}
}

// To update each Operator Node node data into FilterData..
function UpdateOprInFilterdata(data,OpNode){
	var Nflag = false;
	var ParentUl = OpNode.closest('ul');
	if(data.ID == ParentUl.id){
		data.Operator = OpNode.textContent;
	}
	else{
		data.Children.forEach(function(node){
			if(node.ID == ParentUl.id){
				node.Operator = OpNode.textContent;
				Nflag = true;
				return;
			}
		});
		if(!Nflag){
			data.Children.forEach(function(node){
				if(node.hasOwnProperty("Children")){
					UpdateOprInFilterdata(node,OpNode);
				}
			});
		}
	}
}

// To validate the tree eg: If there is a AND operator, then it must have atleast two operands.. X AND Y
function ValidateTree(parentUl){
	var Isvalid = true;
	if($('#'+parentUl).children('li').length < 4)
		Isvalid = false ;
	else{
		$('#'+parentUl).children('li').each(function(){
			if($(this).children('ul').length >= 1){
				Isvalid = ValidateTree($(this).children('ul').attr("id"));
				if (Isvalid) return;
			}
		});
	}
	if (!Isvalid){
		return false;
	}
	else{
		return true;
	}
}

//To delete a node data from the filterData
function deleteNodeData(data,id){
	var Nflag = false;
	var index = 0;
	if(id == "MainNode"){
		data.Children.push(obj);
		return;
	}
		data.Children.forEach(function(node){
			if(node.ID == id){
				index = data.Children.indexOf(node);
				data.Children.splice(index, 1);
				Nflag = true;
				return;
			}
		});
		
		if(!Nflag){
			data.Children.forEach(function(node){
				if(node.hasOwnProperty("Children")){
					deleteNodeData(node,id);
				}
			});
		}
}

// If the field inputtype is 5 then its needs to show a lookupdropdown instead of values Textbox in filter seletion dropdown.
function copyLookupToDropdown(ddl, fieldName, lookupName,fieldId) {
    if (!lookupName || lookupName == '') {
    	$('.create-filter select.value').empty();
        //alert('Lookup Field ' + searchFields[fieldName].label + ' has no lookup table/data loaded.')
        return;
    }
    $(ddl).html('');
    var opt = document.createElement('option');
    $(opt).attr('value', '');
    opt.innerHTML = '';
    $(ddl).append(opt);
    lookupData = window.parent.lookup;
    if (lookupName.toString() == "$QUERY$") {
        var t = fieldName;
        var opt = fieldId ?  lookupData['#FIELD-'+fieldId] : lookupData[t];
        var sortedopt = Object.keys(opt).sort(function(a,b){return opt[a].Ordinal-opt[b].Ordinal})
        for (o in sortedopt) {
            var text = opt[sortedopt[o]].Name;
            var value = opt[sortedopt[o]].Id;
            var fieldId = fieldId;
            addToDDLSearch(ddl, text, value,fieldId);
        }
    }
    else {
    	var x = lookupData[lookupName];
    	var sortOpt = Object.keys(x).sort(function(a,b){return x[a].Ordinal-x[b].Ordinal});
    	for (o in sortOpt) {
            var text = x[sortOpt[o]].Name;
            var value = x[sortOpt[o]].Id;
            addLookupToDDL(ddl, text, value);
        }
    }
}



function addLookupToDDL(ddl, text, value) {
    var opt = document.createElement('option');
    $(opt).attr('value', value);
    opt.innerHTML = text;
    $(ddl).append(opt);
}

// To populate Condition node into Tree view..
function AddConditionNode(CurrNode,text,obj){
	var ParentUl= CurrNode.closest('ul');
	var liNo = $('ul#MainNode li').length;
	var mainUl = document.getElementById('condition');
	var main = document.getElementById('MainNode');
	var node = document.createElement('div');
    var listItem = document.createElement('li');
	listItem.setAttribute("id","li-"+(liNo+1));
	var buttonAdd = document.createElement("button");
		buttonAdd.innerHTML = text;
		buttonAdd.setAttribute("class","Node ConditionNode");
		buttonAdd.style.textAlign  = 'left';
		buttonAdd.setAttribute("id","Fld-"+(liNo+1));
		listItem.appendChild(buttonAdd);
		
		var delbutton = document.createElement("button");
		delbutton.innerHTML = "&#10006";
		delbutton.setAttribute("class","DelNode");
		delbutton.setAttribute("id","del-"+(liNo+1));
		listItem.appendChild(delbutton);
		var pid = mainUl.closest('ul');
    ParentUl.appendChild(listItem);
	var listNo = $("ul#"+ParentUl.id).children('li').length
	var pnode;
	if (ParentUl.id == "MainNode")
		pnode = 0;
	else
		pnode = 1;
	ParentUl.insertBefore(listItem, ParentUl.childNodes[listNo-pnode]);
	obj.ID = "li-"+(liNo+1);
	buildResultQuery();
	CurrNode ="";
}

// To populate Operator node into Tree view..
function AddOperatorNode(CurrNode,btn,OprText,obj){
	var ParentUl= CurrNode.closest('ul');
	var mainUl = document.getElementById('condition');
	var main = document.getElementById('MainNode');
	var node = document.createElement('div');
	var UlNo = $("#container ul").length;
	var liNo = $('ul#MainNode li').length;
	var liH = document.createElement('li');	
	liH.setAttribute("id","li-"+(liNo+1));
	var list = document.createElement('ul');
	list.setAttribute("class","tree");
	list.setAttribute("id","ul-"+(UlNo+1));
	var buttonAdd = document.createElement("button");
	buttonAdd.innerHTML = OprText;
	buttonAdd.setAttribute("id","opr-" + (UlNo+1));
	buttonAdd.setAttribute("class","Node OperatorNode");
	buttonAdd.setAttribute("onclick","switchOperator(this)");
	list.appendChild(buttonAdd);
		var delbutton = document.createElement("button");
		delbutton.innerHTML = "&#10006";
		delbutton.setAttribute("class","DelNode");
		delbutton.setAttribute("id","del-"+(liNo+1));
		list.appendChild(delbutton);
	liH.appendChild(list);
	ParentUl.appendChild(liH);
	var listNo = $("ul#"+ParentUl.id).children('li').length
	var pnode;
	if (ParentUl.id == "MainNode")
		pnode = 0;
	else
		pnode = 1;
	ParentUl.insertBefore(liH, ParentUl.childNodes[listNo-pnode]);
	pushNodes(list.id);
	CurrNode ="";
	buildTree(obj,buttonAdd);
}

function addToDDLSearch(ddl, text, value,fieldId) {
    var opt = document.createElement('option');
    $(opt).attr('value', value);
    opt.innerHTML = text
    $(opt).attr('fieldId', fieldId);
    $(ddl).append(opt);
}

