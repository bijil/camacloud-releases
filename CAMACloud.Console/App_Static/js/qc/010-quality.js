﻿var activeParcel;
var fieldCategories = [];
var activeFieldChanges;
var activeParcelEdits = [];
var shapeList = [];
var parcelBoundaries = new google.maps.Polygon();
var offlineTables;
var searchFields = {};
var gismapview = false;
var currentInputElement;
var currentInputValue;
var map;
var parcelPolygon;
var idleCounter;
var openFromPRC = false;
var childCatId;
var lookupFieldsInitiated = false;
var shomgrid = 0;
var effect1 = {
    duration: 100
};
var datafields = {};
var sketchrenderer;
var printWindow = null;
var rapidMenuClick = false; 		// to restrict the mix-up of menu options on rapid clicks 
var tblList = [];
var OsMap;
var _includetableList = [];
var Locationmarker = [];
var globalms;
function hideSplash() {
    $('.splash-screen').hide();
}

function showSplash() {
    $('.splash-screen').show();
}

function showMask(info, error, errorActionName, callback) {
    $('.masklayer').show();
    if (!info)
        info = 'Please wait ...'
    var helplink = ''
    if (callback && errorActionName)
        helplink = '<a href="#" class="app-mask-error-help-link">Click here<a/> to ' + errorActionName
    if (error)
        info = '<span style="color:Red;">' + info + '</span> ' + helplink
    $('.app-mask-layer-info').html(info);
}

function hideMask() {
    setTimeout(function () { $('.masklayer').hide(); }, 1);
}

function notification(message, title, callback) {
    $('#webmessage').remove();
    if (title == null)
        title = "CAMA Cloud Success notification";
    var wm = '<div id="webmessage" style="display: none;">' + "<h4>" + title + "<span style='float:right; margin-right:10px;color:gray;margin-top:-10px;cursor:pointer' onclick='closenotification()'>x</span></h4><p>" + message + "</p></div>"
    $('.mask').after(wm);
    $('#webmessage').fadeIn(1000, "swing", function () {
        $('#webmessage').delay(4000).fadeOut(2000, "swing", callback);
    });
}
function closenotification() {
    $('#webmessage').remove();
}
function setPageLayout(w, h) {
    $('.qc-tabs').removeClass('hidden');
    var cw = w - $('.left-panel')[0].offsetWidth;
    $('.main-content-area').height(h);
    $('.main-content-area').css('width', '100%');
    $('.search-window').height(h);
    var parcelDetailsHeight = h - $('.parcel-toolbar').height() - $('.parcel-head')[0].offsetHeight - $('.parcel-data-head').height() - $('.tabs-main').height();
    if ($('.parcel-hideHead').is(':visible')) parcelDetailsHeight = parcelDetailsHeight - $('.parcel-hideHead').height() - 10;
    $('.parcel-data-category-container').height(parcelDetailsHeight + 2);
    cw = $('#contentTable').width() - $('.left-panel')[0].offsetWidth;
    $('.parcel-data-view').height(parcelDetailsHeight + 2);
    $('.category-menu').height(parcelDetailsHeight);
    if ($(window).height() <= 612) $('.filter-container').css('max-height', '100px')
    else $('.filter-container').css('max-height', '135px')
    if (isAdhocCreator == true)
        $(".parcel-results").SetHeight($('#contentTable').innerHeight() - $('.adhoc-creator').height());
    else
        $(".parcel-results").SetHeight($('#contentTable').innerHeight() - $('.bulk-editor').height());
    $('#contentTable').height(h + 3);
    //$('.bDiv').width(cw);
    $(".parcel-results").SetWidth($('#contentTable').innerWidth() - $('.left-panel')[0].offsetWidth);
    var totaltop = $('.parcel-toolbar').height() + $('.parcel-head').height() + $('.parcel-data-head').height() + $('.tabs-main').height() + $('.data-navigator').height();
    $('.photo-frame').height(parcelDetailsHeight - 40 - 3);
    $('.photo-frame-allPhotos').height(parcelDetailsHeight - 50 - 3);
    $('.sketch-frame').height(parcelDetailsHeight - 40 - 3);
    $('.photo-nav-button').height(parcelDetailsHeight - 40 - 3);
    $('.googlemap').height(parcelDetailsHeight + 2);
    $('.custommap').height(parcelDetailsHeight + 2);
    if ($('#nylPanel').is(":visible"))
        $('.googlemap').width('100%');
    else
        $('.googlemap').width(cw - 2);
    $('.custommap').width(cw - 2);
    var parcelHeight = $('body').height();
    var parcelWidth = $('body').width();
    //    $('.maskForParcelLoad').height(parcelHeight);
    //    $('.maskForParcelLoad').width(parcelWidth);
    //    $('.maskForParcelLoad').css('margin-top', -parcelHeight);
    if (map && gismapview)
        resetMap();
    if (!__DTR && localStorage && localStorage.internalSupportView == '1' && localStorage.parcelDataSupportView == '1')
        supportViewIconSwitch();
}

function setPageDimensions(h) {
    $('.main-content-area').height(h);
    $('.search-window').height(h);
}

// Hide Field Audit Trail
$(window).bind('click', function (e) {
    var src = e.srcElement;
    if ($(src).hasClass('qc') || $(src).parents('.qc').length > 0 || $(src).hasClass('field-audit-qc') || $(src).parents('.field-audit-qc').length > 0) {

    } else {
        hideFieldAudit();
    }
});

function hideFieldAudit() {
    hide('.field-audit-qc');
    $('.qc a').removeAttr("clicked");
}

function hide(className) {
    $(className).hide(effect1);
}

function show(className) {
    $(className).show(effect1);
}

function hashProcessor(e, callback) {
    //loadPageData(function () {
    var checkView = false;
    var hash = window.location.hash.substr(2);
    if ((hash != '') && (hash != null)) {
        var hashparts = hash.split("/");
        var cmd = hashparts[0]
        switch (cmd) {
            case "parcel":
                checkView = true;
                var pid = parseInt(hashparts[1]);
                switchToParcelView(pid, checkView);
                break;
        }
    }
    if (callback) callback(checkView);
    //});
} function setgridcolumns() {
    var line = $('#lbColumnsRight option').map(function () {
        return this.value;
    }).get().join('|');
    $qc('modifygridcolumns', { line: line }, function (data) {
        initSearchResultsGrid(500);
        setTimeout(function () {
            $qc('getofflinetables', null, function (data) {
                offlineTables = data;
                if (window.location.href.indexOf("/search/") > -1) {
                    //FD_25290
                    doSearch();
                    if (__DTR) {
                        $('.app-search-results-title').html('Desktop Review : ' + $('.app-dtr-selected-task option:selected').text());
                    }
                }
            })
        }, 100);

        hidePopup();
    })
}
function log(x) {
    //console.log(x);
}

function refreshPRC() {
    // Get the list of all <li> elements within the tabs
    var tabList = document.querySelectorAll('.tabs-main .tabs li');

    // Loop through each <li> element
    for (var i = 0; i < tabList.length; i++) {
        var liElement = tabList[i];

        // Check if the <li> element has the id "checkdigitalPRC" and is currently displaying
        if (liElement.id === 'checkdigitalPRC' && liElement.style.display !== 'none') {
            if (liElement.id === 'digital-prc') {
                openCategory('digital-prc');
                console.log("The currently displaying <li> has id 'checkdigitalPRC'");
            }
        }
    }

}

//function countIdle() {
//    if (idleCounter) {
//        window.clearInterval(idleCounter)
//        idleCounter = null;
//        idleCount = 0;
//    }
//    idleCounter = window.setInterval(function () {
//        idleCount += 0.5;
//    }, 1000 * 30);
//}

function refreshApp() {
    try {
        setScreenDimensions();
        window.applicationCache.update();
    }
    catch (e) {

    }
}

function lastUpdatedCounter(callback) {
    $qc('lastupdatedcounter', null, function (data) {
        if (data.message) {
            if (callback)
                callback(data.message);
            else
                localStorage.setItem("lastUpdatedCounter", data.message);
        }
    });
}

function resetApplication() {
    showMask('Clearing cached system data ...')
    clearDatabase(function () {
        dropSqlLiteIndexedDb(() => {
            window.location.reload();
        });
    });
}
function clearDatabase(callback) {
    var listsql = "SELECT tbl_name FROM sqlite_master WHERE type = 'table' AND name NOT LIKE '#_%' ESCAPE '#'";
    getData(listsql, [], function (names) {
        var sqls = names.map(function (x) { return "DROP TABLE " + x.tbl_name + ";" })
        executeQueries(sqls, function () {
            console.log('All websql tables cleared.');
            if (callback) callback();
        });
    });
}

function dropSqlLiteIndexedDb(cback) {
    if (localDBName && indxDB && typeof (openDatabase) == 'undefined') {
        let p1 = performance.now(), req = indexedDB.deleteDatabase(localDBName);
        indxDB.db.close();
        req.onsuccess = function () {
            console.log('dropped sqlLite indexedDB within ' + Math.round((performance.now() - p1)));
            if (cback) cback();
        }
        req.onblocked = function (e) {
            console.log('sqlLite indexedDB drop blocked: ');
            if (cback) cback();
        }
    }
    else if (cback) cback();
}

function executeQueries(queries, callback, failedCallback, progressCallback) {
    var total = 0, completed = 0;
    var _exec = function (sql, callback) {
        _dB.transaction(function (tx) {
            tx.executeSql(sql, [], function (tx1, res) {
                completed += 1;
                if (progressCallback) progressCallback(total, completed);
                if (callback) callback();
            }, function (x, e) { console.warn(sql); console.error(e); if (failedCallback) failedCallback(); });
        });
    }

    var _recExec = function (list, callback) {
        if (list.length == 0) {
            if (callback) callback();
        } else {
            var first = list.shift();
            _exec(first, function () {
                _recExec(list, callback);
            });
        }
    }

    var list = queries.join ? queries : [queries];
    total = list.length;
    _recExec(list, callback);
}

window.onkeyup = function (e) { if (e.ctrlKey && e.altKey && e.which == 48) resetApplication(); }

function initApplication(clearCache) {
    $('.app-action-reset-cache').click(function () {
        resetApplication();
    });

    showMask('Initializing application ...');

    ccma.UI.RelatedFields = {};
    ccma.UI.FieldProps = {};
    ccma.UI.RelatedFields = {};
    ccma.UI.SortScreenFields = [];
    ccma.UI.EstimateChartFields = [];
    ccma.UI.CalcRelatedFields = {};

    $('.qc-tabs').tabs({
        select: function (event, ui) {
            if (ui.index == 0) {
                cancelTemplateSaveForm();
            }
        }
    }); 

    var skipDownload = function (callback) { if (callback) callback(); }

    initLocalDatabase(function (options) {
        var downloadMethod = DownloadAppData;
        if (options.useCache && !clearCache) {
            downloadMethod = skipDownload;
        }
        downloadMethod(function () {
            if (!options.useCache || clearCache)
                lastUpdatedCounter();
            showMask('Loading local cache ...');            
            loadLookup(function () {
                loadFieldsettings(function () {
                    loadFields(function () {
                        showRequiredFields();
                        showInfoButtons();
                        var isDTR = __DTR ? 1 : 0;
                        showMask('Downloading UI settings ...');
                        $qc('getofflinetables', { dtr: isDTR }, function (data) {
                            if (data.DbStatus && data.DbStatus.length > 0) {
                                var updatedTime = parseInt(data.DbStatus[0].Value);
                                if (!(localStorage.getItem('DbUpdatedTime')) || (updatedTime > parseInt(localStorage.getItem('DbUpdatedTime')))) {
                                    localStorage.setItem('DbUpdatedTime', updatedTime);
                                    if (downloadMethod == skipDownload) {
                                        initApplication(true);
                                        return;
                                    }
                                }
                            }

                            if (data?.ForceUpdateApplicationStatus?.length > 0 && downloadMethod == skipDownload) {
                                initApplication(true);
                                return;
                            }

                            offlineTables = data;
                            let ccl = (!options.useCache || clearCache) ? true : false;
                            updateForceStatus(ccl, () => {
                                initSearchPanel(data, function () {
                                    initSearchResultsGrid(500, function () {
                                        loadCategorySettings(function () {
                                            loadCategories(function () {
                                                loadImportSettings(function () {
                                                    loadSketchSettings(function () {
                                                        //clientSettings = resp;
                                                        //loadAdditionalLookups();
                                                        loadSketchExtraLookups();
                                                        showMask('Initializing UI ...');

                                                        loadCustomFunctionsOnAppLoad(function () { });

                                                        initFieldsAndCategories();
                                                        initLookupFields(function () {
                                                            loadParentChild(function () {
                                                                imageTables();
                                                                showMask('Loading components ...');
                                                                if (__DTR && initDTRApplication)
                                                                    initDTRApplication();

                                                                if (__DTR && data?.ParcelWorkFlow?.length > 0 && clientSettings?.DTRWorkflowNew == '1') {
                                                                    let wfc = 0, wfColorCode = ["#FF5733", "#3498DB", "#2ECC71", "#9B59B6", "#E74C3C", "#F39C12", "#1ABC9C", "#E67E22", "#34495E", "#27AE60", "#8E44AD", "#C0392B", "#F1C40F", "#3498DB", "#16A085", "#FFC0CB", "#00BCD4", "#FF4500", "#673AB7", "#FF1493"];
                                                                    data.ParcelWorkFlow.forEach((pwf) => {
                                                                        pwf.colorCode = wfColorCode[wfc]; wfc++;
                                                                        if (wfc == 20) wfc = 0;
                                                                    });
                                                                }

                                                                sketchrenderer = new CCSketchEditor('.sketch-renderer', CAMACloud.Sketching.Formatters.TASketch);
                                                                if (getClientSettingValue("SketchConfig") != null || sketchSettings["SketchConfig"] != null)
                                                                    ccma.Sketching.Config = eval('CAMACloud.Sketching.Configs.' + (getClientSettingValue("SketchConfig") || sketchSettings["SketchConfig"]));
                                                                else
                                                                    ccma.Sketching.Config = CAMACloud.Sketching.Configs.GetConfigFromSettings();

                                                                if (getClientSettingValue("SketchFormat") != null || sketchSettings["SketchFormat"])
                                                                    ccma.Sketching.SketchFormatter = eval('CAMACloud.Sketching.Formatters.' + (getClientSettingValue("SketchFormat") || sketchSettings["SketchFormat"]));

                                                                if (ccma.Sketching.Config && !ccma.Sketching.SketchFormatter)
                                                                    if (ccma.Sketching.Config.formatter)
                                                                        ccma.Sketching.SketchFormatter = eval('CAMACloud.Sketching.Formatters.' + ccma.Sketching.Config.formatter);

                                                                if (ccma.Sketching.Config && !ccma.Sketching.SketchFormatter)
                                                                    ccma.Sketching.SketchFormatter = CAMACloud.Sketching.Formatters.TASketch;
                                                                if ((clientSettings && clientSettings["sketchOriginPosition"]) || (sketchSettings && sketchSettings["sketchOriginPosition"]))
                                                                    ccma.Sketching.SketchFormatter.originPosition = clientSettings["sketchOriginPosition"] || sketchSettings["sketchOriginPosition"];
                                                                if (sketchSettings["RoundToDecimalPlace"] == 'Half Foot' || sketchSettings["RoundToDecimalPlace"] == 'Quarter Foot')
                                                                    sketchSettings.SegmentRoundingFactor = sketchSettings["RoundToDecimalPlace"] == 'Half Foot' ? 2 : 4;
                                                                if (clientSettings && (clientSettings["doNotShowSelectedEstimate"] == "1" || clientSettings["doNotShowSelectedEstimate"] == "true"))
                                                                    $('.tab_selected_estimate').hide();
                                                                $('.menu-trigger').click(function () {
                                                                    var mtstatus = $(this).attr('status');
                                                                    var menu = !(mtstatus == "on");
                                                                    $(this).attr('status', menu ? 'on' : 'off');
                                                                    menu ? show('.category-menu') : hide('.category-menu');
                                                                });
                                                                if (clientSettings["HideSketchTab"] == "1") {
                                                                    $('.dtr_sketchviewer').css("display", "none")
                                                                }
                                                                else {
                                                                    $('.dtr_sketchviewer').css("display", "block")
                                                                }

                                                                $('.release-category-menu').click(function () {
                                                                    hideCategoryMenu();
                                                                });


                                                                $('.photo-thumb').click(function () {
                                                                    if (__DTR && (localStorage.getItem('PhotosPopupSatus') == "true")) {
                                                                        loadParcelPhotos();
                                                                    }
                                                                    else {
                                                                        openCategory('photo-viewer', 'Parcel Photos');
                                                                        showAllPhotos();
                                                                    }

                                                                });

                                                                $('.parcel-priority').change(function () {
                                                                    var pr = $(this).val();
                                                                    if (clientSettings?.EnableNewPriorities == "1") {
                                                                        if (pr && ['1', '2', '3', '4', '5'].includes(pr.toString())) {
                                                                            $('.parcel-alert-message-row').show();
                                                                        } else {
                                                                            $('.parcel-alert-message-row').hide();
                                                                        }
                                                                    }
                                                                    else {
                                                                        if ((pr == 2) || (pr == 1)) {
                                                                            $('.parcel-alert-message-row').show();
                                                                        } else {
                                                                            $('.parcel-alert-message-row').hide();
                                                                        }
                                                                    }
                                                                    activeParcelEdits.push({ ParcelId: false });
                                                                });

                                                                $('.parcel-alert-message-row').hide();
                                                                $('.bulk-priority').change(function () {
                                                                    var pr = $(this).val();
                                                                    console.log(pr);
                                                                    if (clientSettings?.EnableNewPriorities == "1") {
                                                                        if ((pr && ['1', '2', '3', '4', '5'].includes(pr.toString())) && !$('#chk_clearAlertMsg').is(':checked')) {
                                                                            $('.bulk-alert-message').removeAttr('disabled');
                                                                        } else {
                                                                            $('.bulk-alert-message').attr('disabled', 'disabled');
                                                                        }
                                                                    }
                                                                    else {
                                                                        if (((pr == 2) || (pr == 1)) && !$('#chk_clearAlertMsg').is(':checked')) {
                                                                            $('.bulk-alert-message').removeAttr('disabled');
                                                                        } else {
                                                                            $('.bulk-alert-message').attr('disabled', 'disabled');
                                                                        }
                                                                    }
                                                                    
                                                                });

                                                                $('#chk_clearAlertMsg').on('change', function () {
                                                                    if ($(this).is(':checked')) {
                                                                        $('.bulk-alert-message').attr('disabled', 'disabled');
                                                                    } else {
                                                                        if ($('.bulk-priority').val() != '0') {
                                                                            $('.bulk-alert-message').removeAttr('disabled');
                                                                        }
                                                                    }
                                                                });


                                                                $('.bulk-alert-message').attr('disabled', 'disabled');

                                                                if ((clientSettings['EnableMALite'] == '1' || clientSettings['EnableMALite'] == 'True' || clientSettings['EnableMALite'] == 'true'))// && (activeParcel.QC == 'true' || activeParcel.QC == "True" || activeParcel.QC == '1') )
                                                                {
                                                                    $('.QC_CommitChange').css("display", "inline-block")
                                                                    $('.QC_CommitChange').hide() //Added to hide committ button
                                                                }
                                                                else {
                                                                    $('.QC_CommitChange').hide()
                                                                }                                                              

                                                                initChangePopupLink(function () {
                                                                    initDataNavigator(function () {
                                                                        initDataChangeTracker(function () {
                                                                            initQCButtons(function () {
                                                                                initSmartScreen(function () {
                                                                                    hashProcessor(null, function (checkView) {
                                                                                        initGoogleMap(function () {
                                                                                            loadNeighborhoods(function () {
                                                                                                loadValidations(function () {
                                                                                                    loadTableKeys(function () {
                                                                                                        BppUserSettings(function () {
                                                                                                            redirectedPriorityListParcels(function () {
                                                                                                                if (RedirectedPriorityListFlag != '')
                                                                                                                    doSearch();
                                                                                                                if (!checkView) {
                                                                                                                    checkSchemaUpdates();
                                                                                                                    hideMask();
                                                                                                                }
                                                                                                                _includeTableNames();
                                                                                                            });
                                                                                                        });
                                                                                                    });
                                                                                                });
                                                                                            });
                                                                                        });
                                                                                    });
                                                                                });
                                                                            });
                                                                        });
                                                                    });
                                                                });
                                                            });
                                                        });
                                                    });
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
             });
           
        });
    });
}

$(function () {
    $('.parcel-data-view').bind('scroll', function () { $(this).find('.data-navigator').css('top', ($('.parcel-data-view').scrollTop())) });
});

function evalYesNo(value) {
    return value == 'true' || value == '1' || value == 1 ? 'Yes' : value == 'false' || value == '0' || value == 0 ? 'No' : value;
}

function evalvalue(fieldId, value, newValue) {
    var displayText = value;
    if ($('.parcel-field-values .value[fieldid="' + fieldId + '"] select').length == 1) {
        displayText = $('.parcel-field-values .value[fieldid="' + fieldId + '"] select option[value="' + value + '"]').html();
        if (!displayText && newValue && newValue.replace(/\s/g, '').length) {
            value = encodeURI(newValue);
            displayText = $('.parcel-field-values .value[fieldid="' + fieldId + '"] select option[value="' + value + '"]').html();
        }
        if (displayText == "" || displayText == null) {
            displayText = value;
        }
    } else if ($('.parcel-field-values .value[fieldid="' + fieldId + '"] .cc-drop').length == 1) {
        let parentField = $('.parcel-field-values .value[fieldid="' + fieldId + '"]');
        let lp = $('.cc-drop', parentField) && $('.cc-drop', parentField)[0] && $('.cc-drop', parentField)[0].getLookup;
        if (!lp) displayText = value
        else {
            let rows = lp.rows;
            for (var k of Object.keys(rows)) {
                if (rows[k].Id == value) {
                    displayText = rows[k].Name;
                    break;
                }
            }
            if (!displayText && newValue && newValue.replace(/\s/g, '').length) {
                value = encodeURI(newValue);
                for (var k of Object.keys(rows)) {
                    if (rows[k].Id == value) {
                        displayText = rows[k].Name;
                        break;
                    }
                }
            }
            if (displayText == "" || displayText == null) displayText = value;
        }
    }
    if (displayText == "") {
        displayText = "[blank];"
    }
    return displayText;
}

function hideCategoryMenu() {
    //hide('.category-menu');
    //$('.menu-trigger').attr('status', 'off');
}

function hideTabsInQC() {
    var hideSketchTab = clientSettings["HideSketchTab"];
    if (hideSketchTab == '1') {
        $('.tabs-main li[sketch]').hide();
    }
}

var LFlag = false;

function openCategory(categoryId, title, node, dtHideMask) {
    $('.photo-Manager').hide();
    if (rapidMenuClick) return false;
    //rapidMenuClick = true;
    setTimeout(function () { rapidMenuClick = false }, 300);
    var cat = getCategory(categoryId);
    var parent = getParentCategories(categoryId);
    var auxForm = $('.category-page[categoryid="' + categoryId + '"]');
    $('.photo-preview').hide();
    $('.fy_selector').hide();
    $('.support-toggle').hide();
    openFromPRC = false;
    disableSwitch = false;
    autoselectloop = [];
    LFields = [];
    if (categoryId == 'photo-viewer') {
        $('.photo-Manager').show();
    }

    if (node == 'parceldata') {
        $('.category-menu a').removeClass('field-category-edited');
        highlightCatLink();
        var w = $(window).width()
        var h = $(document).height();
        //setScreen(w,h)
        if (isBPPParcel) {
            if (fieldCategories.filter(function (f) { return ((f.Type == '1' || f.Type == '2') && f.ParentCategoryId == null) }).length > 0) {
                var catid = fieldCategories.filter(function (f) { return ((f.Type == '1' || f.Type == '2') && f.ParentCategoryId == null) })[0].Id
                cat = getCategory(catid);
                if (cat) {
                    openCategory(catid, cat.Name, null)
                    return;
                }
            }
        }
        //$('.category-menu a').removeClass('field-category-edited');
        var calcExpressionFields = Object.keys(datafields).map(function (x) { return datafields[x] }).filter(function (x) { return (x.CalculationExpression != null && x.CategoryId != null) });
        var calcExps = {}
        calcExpressionFields.forEach(function (CalField) {
            var categoryId = CalField.CategoryId;
            var fieldId = CalField.Id;
            if (calcExps[categoryId]) calcExps[categoryId].push(fieldId.toString());
            else {
                calcExps[categoryId] = [];
                calcExps[categoryId].push(fieldId.toString());
            }
        });
        var showAuxIds = [];
        calcExpressionFields = _.uniq(calcExpressionFields.map(function (field) { return field.CategoryId }));
        calcExpressionFields.forEach(function (item) {
            var pCalcId = getParentCategoryIds(item);
            while (pCalcId.length > 0) {
                var eachId = pCalcId.pop();
                if (calcExpressionFields.includes(parseInt(eachId)) && !showAuxIds.includes(parseInt(eachId)))
                    showAuxIds.push(parseInt(eachId));
            }
            if (!showAuxIds.includes(parseInt(item)))
                showAuxIds.push(item);
        });
        showAuxIds.forEach(function (catId) {
            showAuxData($('.category-page[categoryid="' + catId + '"]'), 0, null, null, null, null, null, null, calcExps[catId]);
        });
        if (clientSettings["FutureYearEnabled"] == "1" || clientSettings["FutureYearEnabled"] == "true") {
            $('.fy_selector').show();
            //hideMask();
        }
    }

    $('#classCalculatorFrame').hide();
    $('.classCalcAttributes').val("");
    $('.category-page[categoryid="' + parent + '"]').hide();
    $('.classCalc').hide();
    try {
        $('.aux-ROWUID').text('');
        $('.grid-view-qc').html('&#8803;')
        $('.new-item').html('&#9679; Add New')
        $('.del-item').html('&#935; Delete')
        gismapview = false;
        //$('.prcselect').hide();
        //$('.bckup').hide();
        $('#printPRC').hide();
        $('.prcbkup').hide();
        $('#printSketch').hide();
        $('.sketch-details').hide();
        $('.nostreetview').hide();
        $('.pictometry').hide();
        $('.eagleView').hide();
        $('.nearmap').hide();
        $('.nearmapwms').hide();
        $('.woolpert').hide();
        $('.cyclomedia').hide();
        $('.heightdiv').hide();
        $('#exportreport').hide();
        $('.qPublic').hide();
        $('.customwms').hide();
        $('.sanborn').hide();
        $('#viewreport').hide();
        $('#groupByRowUid').hide();
        $('.bpp-child-parcels-div').hide();
        $('.nbhd-profile-area').addClass("hidden");
        hide('.field-audit-qc');
        if (node != 1) {
            $('.category-page[categoryid="' + categoryId + '"]').attr('findex', 0);
            $('.category-page[categoryid="' + categoryId + '"]').attr('index', 0);
        }
        if (cat) {
            if (clientSettings["FutureYearEnabled"] == "1" || clientSettings["FutureYearEnabled"] == "true")
                $('.fy_selector').show();
        }

        if (localStorage.internalSupportView == '1' && (node == 'parceldata' || cat) && !__DTR) {
            $('.support-toggle').show();
            if (localStorage.parcelDataSupportView == '1')
                $('.support-toggle-switch-input')[0].checked = true;
            else
                $('.support-toggle-switch-input')[0].checked = false;
        }
        if (!__DTR && node == 'parceldata' && typeof (isInternalSupportUser) != 'undefined' && isInternalSupportUser)
            switchSupportView();
        change_category_menu_color(categoryId);
        $('.qc a').removeAttr("clicked");
        $('.parcel-data-title').html(title);
        //MSP 
        $('.currentCategoryTitle').html(title);
        loadParentChildOfCategory(categoryId);
        highlightCatLink();
        //$('.field-audit-qc').height($('.category-menu').height() - $('.category-page[categoryid="' + categoryId + '"] .data-navigator').height());
        //MSP 
        $('.category-page').hide();
        show('.category-page[categoryid="' + categoryId + '"]');
        // hideCategoryMenu();
        //hidePhotoManagerMenu
        if (!dtHideMask) setTimeout(hideMask, 1000);
        //$('.photo-Manager').hide();
        $('.photo-Manager-Back').hide();
        $('.photo-frame').hide();
        if (title == 'Parcel Photos') {
            $('.photo-Manager').show();
            $('.photo-frame-allPhotos').show();
        }
        if (categoryId == 'mapview') {
            gismapview = true;
            hide('.category-menu');
            $('#googlemap').removeClass('hidden');
            resetMap();
            $('.nostreetview').hide();
            if (clientSettings["EagleView.APIKey"] && clientSettings["EagleView.APIKey"].trim() != "")
                $('.pictometry').show();
            if (clientSettings["NewEV.ClientID"] && clientSettings["NewEV.EVClientSecret"].trim() != "")
                $('.eagleView').show();
            if (clientSettings['EnableCyclomedia'] == "1")
                $('.cyclomedia').show();
            if (clientSettings['EnableqPublic'] == "1" && clientSettings['qPublicKeyField'] && clientSettings['qPublicKeyField'].trim() != '')
                $('.qPublic').show();
            if (clientSettings['EnableSanborn'] == "1" && clientSettings["SanbornViewerName"])
                $('.sanborn').show();
            if (clientSettings['EnableNearmap'] && clientSettings['EnableNearmap'].includes('DTR') && clientSettings["Nearmap.APIKey"])
                $('.nearmap').show();
            if (clientSettings['EnableNearmapWMS'] && clientSettings['EnableNearmapWMS'].includes('DTR') && clientSettings["NearmapWMS.APIKey"])
                $('.nearmapwms').show();
            if (clientSettings['woolpertApikey'] && __DTR)
                $('.woolpert').show();
            if (clientSettings.customWMS && clientSettings.customWMS.trim() != "" && clientSettings.EnableCustomWMS && clientSettings.EnableCustomWMS.includes('DTR') && __DTR) {
                $('#customwms').val('Show Imagery');
                $('.customwms').show();
                if ($('#custommap').hasClass('hidden') == false)
                    $('#custommap').addClass('hidden');
            }
            if (clientSettings.customWMS && clientSettings.customWMS.trim() != "" && clientSettings.EnableCustomWMS && clientSettings.EnableCustomWMS.includes('QC') && !__DTR) {
                $('#customwms').val('Show Imagery');
                $('.customwms').show();
                if ($('#custommap').hasClass('hidden') == false)
                    $('#custommap').addClass('hidden');
            }
            if (clientSettings.DefaultImagery && clientSettings.DefaultImagery.toLowerCase() == 'webservice') {
                if (fLoad) {
                    $('.googlemap').removeClass('hidden');
                    $('#custommap').addClass('hidden');
                    setTimeout(loadCustomWMS, 1000);
                    $('.googlemap').width('100%');
                    fLoad = false;
                }
                else
                    loadCustomWMS();
            }

        }

        $('.invalidalerticon-blk').remove();
        $('.invalidalerticon').remove();

        if (categoryId == 'dashboard') {
            $('.parcel-data-title.release-category-menu').html("Parcel Dashboard");
            $('.tabs li').removeAttr('class');
            $('.tabs li:first-child').click();
            $('.tabs li:first-child').addClass('selected');
            $('.category-menu').hide();
            if (clientSettings["FutureYearEnabled"] == "1" || clientSettings["FutureYearEnabled"] == "true")
                $('.fy_selector').show();
            if ((isBPPParcel && activeParcel.CC_Deleted) || (!isBPPParcel && ccma.Session.RealDataReadOnly)) {
                $('.parcel-priority').attr('disabled', 'disabled');
                $('.parcel-alert-message').attr('readonly', 'readonly');
            }
            else {
                $('.parcel-priority').removeAttr('disabled');
                $('.parcel-alert-message').removeAttr('readonly');
            }
        }
        if (categoryId == 'photo-viewer') {
            $('.tabs li').removeAttr('class');
            $('.tabs li[category="photo-viewer"]').click();
            $('.tabs li[category="photo-viewer"]').addClass('selected');
            $('.category-menu').hide();
            $('.add-photo-frame').hide();
            if (clientSettings["FutureYearEnabled"] == "1" || clientSettings["FutureYearEnabled"] == "true") {
                $('.fy_selector').show();
            }
        }
        if (categoryId == 'digital-prc') {
            //openFromPRC = true;
            $('#printPRC').show();
            $('.prcselect').val('');
            //$('.prcselect').show();
            // $('.bckup').show();
            if (clientSettings["FutureYearEnabled"] == "1" || clientSettings["FutureYearEnabled"] == "true") {
                $('.fy_selector').show();
            }
            if (clientSettings.GeneratePRCPDF == '1') {
                displayPrc(activeParcel.PrcBackup);
                $('.prcbkup').show();
            }
            refreshDigitalPrc();
        }
        if (categoryId == 'nbhd-profile') {
            $('.nbhd-profile-area').removeClass("hidden");
            $('#nbhd-profile-template').removeClass("hidden");
            $('.category-menu').hide();
            ShowAssignmentGroup();
        }
        if (categoryId == 'sketch-manager') {
            $(".sketch-frame").css('background-image', 'none');
            $('#printSketch').show();
            $('.sketch-details').show();
            var sketches = getSketchSegments(undefined, undefined, 'sketch-manager');
            $('.select-sketch-segments').find('option').remove();
            for (x in sketches) {
                var skt = sketches[x];
                $('.select-sketch-segments').append($('<option>', { value: skt.uid, text: skt.label }));
            }
            showSketchesInQC(false, false, categoryId);
            showPreviousSketch('0');
            if (clientSettings["FutureYearEnabled"] == "1" || clientSettings["FutureYearEnabled"] == "true") {
                $('.fy_selector').show();
            }
        }

        if (categoryId == 'bpp-child-parcels') {
            showBPPChildCategory();
        }

        if (categoryId == 'audit-trail') {
            if ($('.recent-audit-trail-main tbody tr').length > 0) {
                if (!__DTR) {
                    $('#exportreport').show();
                    $('#viewreport').show();
                    //$('#groupByRowUid').show();
                    var reportLink = $('#viewReport').attr('href');
                    var newreportLink = reportLink.split("id=");
                    var Link = newreportLink[1].split(',');
                    Link[0] = activeParcel.Id + '"';
                    output = newreportLink[0] + 'id=' + Link.join();
                    $('#viewReport').attr('href', output);

                    if (LFlag)
                        auditSorted = !auditSorted;
                    else LFlag = true;

                    //checkSort();
                    if (localStorage.internalSupportView == 1) {
                        $('#groupByRowUid').show();
                        checkSort();
                    }
                }
            }
            if (clientSettings["FutureYearEnabled"] == "1" || clientSettings["FutureYearEnabled"] == "true") {
                $('.fy_selector').show();
            }
            }

        if ($('.category-page[categoryid="' + categoryId + '"]').attr("parceldata") == null) {
            $(".submenu-child").hide();
            show('.submenu-child[parentid="' + categoryId + '"]');
            if (getParentCategoryIds(categoryId).length <= 0) {
                shomgrid = 0;
                showAuxData($('.category-page[categoryid="' + categoryId + '"]'), 0)
                // refreshAuxLink(categoryId, auxForm)
            }
        }
        if ($('.grid-view-qc[categoryid="' + categoryId + '"]').attr('disabled') != 'disabled') {
            showGrid(auxForm, categoryId);
        }
        else {
            if (!__DTR && localStorage && localStorage.internalSupportView == '1' && localStorage.parcelDataSupportView == '1')
                $('.support__catHead', auxForm).show();
            if (auxForm) {
                $('.parcel-field-values tr:visible .input', auxForm).each((ki, elm) => {
                    if ($(elm)[0] && $(elm).attr('customWidth')) {
                        $(elm).removeAttr('customWidth');
                        $(elm).css('width', '');
                    }
                    if ($($(elm).parent()).attr('datatype') == '5')
                        recalculateCustomDropdownTextWidth(elm, auxForm);
                });
            }
        }
        // if ($('.aux-grid-view[catid="' + categoryId + '"]').css("display") != 'block') {
        setTimeout(function () {
            var txtarea = $('.category-page[categoryid = "' + categoryId + '"] textarea');
            for (var x = 0; x < txtarea.length; x++) {
                txtarea[x].style.cssText = 'height:auto';
                txtarea[x].style.cssText = 'height:' + txtarea[x].scrollHeight + 'px';
            }
        }, 0);
        if (node == 'parceldata') highlightCatLink();
    }
    // }
    catch (e) {
        console.error(e);
    }
}
function refreshDigitalPrc() {
    let s = prcPreviewSketch(true), validvectorstring = s.flg;


    if (isBPPParcel)
        $('.digital-prc').html($('#prc-template-bpp').html());
    else
        $('.digital-prc').html($('#prc-template').html());

    let sktchImgPrc = $('.digital-prc img[srcx="${SketchPreview}"]')[0] ? $('.digital-prc img[srcx="${SketchPreview}"]') : $('.digital-prc img[src="${SketchPreview}"]');
    if (sktchImgPrc[0] && s.skArray && s.skArray.length > 0) {

        if ($('.digital-prc .select-sketch-segments-prc')[0]) $('.digital-prc .select-sketch-segments-prc').remove();

        let html = '';

        s.skArray.forEach((sk, i) => { html += '<option val="' + i + '">' + sk + '</option>'; });


        if (!validvectorstring) {
            $(sktchImgPrc).parent().prepend('<div style="display:flex;justify-content: end;"><span class="invalidalerticon"></span><span class="select-sketch-segments-prc" style="width: 100px; float: right; height: 24px;"><select onchange="prcPreviewSketch()" style="width: 100px;">' + html + '</select></span></div>')
        }
        else {
            $(sktchImgPrc).parent().prepend('<span class="select-sketch-segments-prc" style="width: 100px; float: right; height: 24px;"><select onchange="prcPreviewSketch()" style="width: 100px;">' + html + '</select></span>')
        }
    }

    $('.digital-prc').fillTemplate([activeParcel]);
    $("button:contains('Mark As Complete')").hide();
    if (clientSettings?.ShowEmptyCategoryOnPRC != '1') {
        $('.digital-prc table:not(:has(tbody))').each((i, el) => {
            let onclickAttributeValue = el.getAttribute('onclick'), ptd = false;
            if (!onclickAttributeValue && $(el).parent('td')[0]?.getAttribute('onclick')) {
                onclickAttributeValue = $(el).parent('td')[0].getAttribute('onclick'); ptd = true;
            }
            if (ptd && onclickAttributeValue?.includes('showDataCollection')) $(el).parent('td').hide();
            else if (onclickAttributeValue?.includes('showDataCollection')) $(el).hide();
        });
    }
    $('[onclick]').each(function () {
        var onclickHideSketch = $(this).attr('onclick');
        if (clientSettings["HideSketchTab"] == "1") {
            if (onclickHideSketch.includes('showSketches')) {
                $(this).closest('td').hide();
            }
        }
    });

    if (clientSettings?.PRCDisplayDeletedRecords == '1') {
        showDeletedImageOnPRC();
    }
}

function showDeletedImageOnPRC() {
    $('div.prc-format table').each(function () {
        var contextValue = $(this).find('tbody').attr('context');
        var parentTr = $(this).closest('tr');
        var tdCenter = parentTr.find('td[style="text-align: center"]');
        var caption = parentTr.find('caption');
        var selector = tdCenter.length > 0 ? 'td[style="text-align: center"]' : 'caption';
        var tablename = parentTr.find(selector);
        var aTag = tablename.find('a').first();
        var textValue = aTag.text().trim();

        console.log(textValue);
        console.log(contextValue);

        if (textValue && contextValue) {
            var deletedFlags = activeParcel[contextValue].reduce((acc, record) => acc || (record && record.CC_Deleted === true), false);

            if (deletedFlags) {
                if (!(aTag.find('img[src="/App_Static/css/quality/prc.png"]').length)) {
                    var imgHtml = '<img style="width: 25px; display: inline;" src="/App_Static/css/quality/prc.png">';
                    aTag.append(imgHtml);
                }
            } else {
                aTag.find('img[src="/App_Static/css/quality/prc.png"]').remove();
            }
        }
    });
}

function showBPPChildCategory() {
    if (activeParcel.ChildParcels && activeParcel.ChildParcels != []) {
        $('.bpp-child-parcels-div').show();
        $('.bpp-child-ul').html('<li value="${Id}"  onclick="openBPPParcel(this);" onMouseover="showBPPPopup(this)" onMouseLeave="closeBPPPopup()">${KeyValue1}</li>')
        $('.bpp-child-ul').fillTemplate(activeParcel.ChildParcels);
        activeParcel.ChildParcels.forEach(function (f) {
            if (f.ClientROWUID && f.ClientROWUID < 0 && (f.cc_deleted == false || f.cc_deleted == 'false'))
                $(".bpp-child-parcels-div .bpp-child-ul li[value=" + f.Id + "]").css('color', 'green');
            else if (f.cc_deleted == true || f.cc_deleted == 'true')
                $(".bpp-child-parcels-div .bpp-child-ul li[value=" + f.Id + "]").css('color', 'red');
        });
    }
}
function openBPPParcel(that) {
    var pid = $(that).attr('value')
    //getParcel(pid);
    var loc = "../quality/#/parcel/" + pid;
    window.open(loc);
}
function setScreen(width, height) {
    var ch = height - $('#topNavBar').height() - $('#footer').height() - 12;
    $('#contentTable').height(ch);
    setPageLayout(width, ch);
}
function expandScreenUp() {
    var w = $(window).width()
    var h = $(window).height();
    $('.screen-down').show();
    $('.screen-up').hide();
    $('.parcel-hideHead').show();
    $('.parcel-head').hide();
    setScreen(w, h);
    if (__DTR) {
        var docHeight = $(window).height();
        var cHeight = docHeight - $('#topNavBar').innerHeight() - $('#footer').innerHeight();
        cHeight = cHeight + ($(document).height() - $(window).height());
        setPageLayout($('#contentTable').width(), cHeight - 3);
    }
    if (OsMap) setTimeout(function () { OsMap.invalidateSize(); }, 50);
}
function shortenScreen() {
    var w = $(window).width()
    var h = $(window).height();
    $('.screen-down').hide();
    $('.screen-up').show();
    $('.parcel-hideHead').hide();
    $('.parcel-head').show();
    setScreen(w, h);
    if (__DTR) {
        var docHeight = $(window).height();
        var cHeight = docHeight - $('#topNavBar').innerHeight() - $('#footer').innerHeight();
        cHeight = cHeight + ($(document).height() - $(window).height());
        setPageLayout($('#contentTable').width(), cHeight - 3);
    }
    if (OsMap) setTimeout(function () { OsMap.invalidateSize(); }, 50);
}
function hideSearchScreen() {
    var w = $(window).width()
    var h = $(window).height();
    $('.left-panel').hide();
    $('.hideSidePanel').hide();
    $('.showSidePanel').show();
    $('.left-panel2').show();
    setScreen(w, h);
    if (OsMap) setTimeout(function () { OsMap.invalidateSize(); }, 50);
}
function getSearchScreen() {
    var w = $(window).width()
    var h = $(window).height();
    $('.hideSidePanel').show();
    $('.left-panel').show();
    $('.showSidePanel').hide();
    $('.left-panel2').hide();
    setScreen(w, h);

}
function refreshAuxCount(categoryId, auxForm) {
    var child = getChildCategories(categoryId);
    var parentTable = getSourceTable(categoryId);
    var indexval = $(auxForm).attr('findex');
    if (activeParcel[parentTable] && activeParcel[parentTable].length) {
        for (x in child) {
            var cat = getCategory(child[x]);
            $('.category-page[categoryid="' + getParentCategories(cat.Id) + '"] span.aux-index').attr('findex', indexval);
            var sourceTable = cat.SourceTable;
            var ParamOfAuxData = getParamOfAuxData(cat.Id);
            var showAuxParams = (ParamOfAuxData && ParamOfAuxData.length) ? ParamOfAuxData[0] : "";
            var form = $('.childCategoryTitle[category="' + cat.Id + '"]', auxForm);
            let indicesToRemove = [];
            var sourceDataAux;
            const filteredFieldData = fieldCategories.filter(item => item.Id == cat.Id && item.HideRecordExpression !== null);
            if (filteredFieldData.length > 0) {
                filteredFieldData.forEach(function (categ) {
                    for (let uindex = 0; uindex < activeParcel[sourceTable].length; uindex++) {
                        var isExp = activeParcel.EvalValue(sourceTable + '[' + uindex + ']', categ.HideRecordExpression);
                        if (isExp) {
                            indicesToRemove.push(uindex);
                        }
                    }

                    sourceDataAux = activeParcel[sourceTable].filter(function (_, uindex) {
                        return !indicesToRemove.includes(uindex);
                    });
                });
            }
            if (showAuxParams && (showAuxParams.filter_field != undefined) && (showAuxParams.filter_value != undefined) && (showAuxParams.filter_value.length > 0) && (showAuxParams.filter_field.length > 0)) {
                var source = showAuxDataWithFilter(sourceTable, showAuxParams.filter_field, showAuxParams.filter_value, showAuxParams.parentROWUID, cat.Id);
                if (sourceDataAux && (sourceDataAux.length > 0 || indicesToRemove.length > 0)) {
                    source = source.filter(item => {
                        return sourceDataAux.some(auxItem => auxItem.ROWUID === item.ROWUID);
                    });
                }
                var count = source.length;
                $('span', form).html(count);
            }
            else if (sourceTable == parentTable && cat.ShowCategory != '1') {
                $('span', form).parent().parent('.childCategoryTitle').hide();
            }
        }
    }
    else if (parentTable == null && child.length > 0) {
        for (x in child) {
            var cat = getCategory(child[x]);
            var st = cat.SourceTable;
            if (st) {
                var form = $('.childCategoryTitle[category="' + cat.Id + '"]', auxForm)
                $('span', form).html(eval('activeParcel.' + st).length);
            }
        }
    }
    else {
        for (x in child) {
            var cat = getCategory(child[x]);
            var form = $('.childCategoryTitle[category="' + cat.Id + '"]', auxForm)
            $('span', form).html(0);
        }
    }

}
function ShowAssignmentGroup() {
    var nbhdLoaded = false;
    var EnableGroupProfile = '0';
    if (clientSettings["EnableGroupProfileView"]) {
        if (__DTR)
            EnableGroupProfile = (clientSettings["EnableGroupProfileView"].toUpperCase().indexOf("DTR") > -1 || clientSettings["EnableGroupProfileView"] == '1') ? '1' : '0';
        else
            EnableGroupProfile = (clientSettings["EnableGroupProfileView"].toUpperCase().indexOf("QC") > -1 || clientSettings["EnableGroupProfileView"] == '1') ? '1' : '0'
    }
    else
        EnableGroupProfile = clientSettings["EnableGroupProfileView"] == undefined ? '1' : clientSettings["EnableGroupProfileView"] == '0' ? '0' : '0';
    if (EnableGroupProfile == '1') {
        $('.tabs-main li[category="nbhd-profile"]').css({ "display": "" });
    }
    else {
        $('.tabs-main li[category="nbhd-profile"]').css({ "display": "none" });
    }
    if (!nbhdLoaded && activeParcel['NeighborhoodName']) {
        try {
            var nbhd = activeParcel['NeighborhoodName'].toString();
            var n = neighborhoods[nbhd.toString()];

            if (!n) {
                n = [{ Name: nbhd, Number: nbhd }];
            }
            if (n) {
                nb = new Parcel(nbhd, n[0]);
                $('.nbhd-profile-area').html($('#nbhd-profile-template').html());
                $('.nbhd-profile-area').fillTemplate([nb]);
                // $('.nbhd-profile-area').fillTemplate([activeParcel]);
            }
        }

        catch (e) {
            console.error(e);
            $('.nbhd-profile-area').html(e);
        }

    }
}
var qPublicWindow;
function formatString(formatter, value) {
    if (!formatter || !value) return value;
    var s = value.toString(), r = '';
    for (var i = 0, j = 0; j < s.length; i++) {
        r += (formatter.charAt(i) == '#' || formatter.charAt(i) == '') ? s.charAt(j++) : formatter.charAt(i);
    }
    return r;
}
function loadQPublic(svOpen) {
    var svqPublic = null;
    if (svOpen && svOpen.sv) {
        if (svOpen.autoOpen && localStorage.getItem('qPublicDisabled') == "1") { return false; }
        else { localStorage.setItem('qPublicDisabled', 0); }
        prevQpublic = localStorage.getItem('qPublicDisabled');
        svqPublic = 'qPublicDisabled';
    }

    var keyvalue = (clientSettings['qPublicFormatter'] && clientSettings['qPublicFormatter'].trim() != "") ? formatString(clientSettings['qPublicFormatter'], activeParcel[clientSettings['qPublicKeyField'].trim()]) : activeParcel[clientSettings['qPublicKeyField'].trim()];
    var url = "https://qpublic.schneidercorp.com/application.aspx?app=" + countyName + 'County' + stateName + "&layer=Parcels&PageType=Report&KeyValue=" + encodeURIComponent(keyvalue);
    //console.log(url)
    //var url1='https://qpublic.schneidercorp.com/application.aspx?app=GilmerCountyGA&layer=Parcels&PageType=Report&KeyValue=1081%20%20%20080';
    //if( qPublicWindow)  qPublicWindow.close();
    qPublicWindow = openWindow(qPublicWindow, 'qPublic', '/protected/Svo/qPublic.html', { resizable: 'true', width: 1200, height: 700 });
    if (svOpen && svOpen.sv) {
        if (qPublicWindow) {
            qPublicWindow.onunload = function () {
                if (!isBeforeUnload)
                    localStorage.setItem('qPublicDisabled', "1");
            }
        }
    }
}
function loadParentChildOfCategory(categoryId, highlight) {
    try {
        var parentId, childId, ParentFilterField, NodeFilterField, ChildFilterField;
        // var parentCategory = $('#AllCategories li[category="' + categoryId + '"]').attr('parentid');
        var parentCategory = (fieldCategories.filter(function (d) { return d.Id == categoryId; })[0]) ? fieldCategories.filter(function (d) { return d.Id == categoryId; })[0].ParentCategoryId : null;
        //  var childCategories = $('#AllCategories li[category="' + categoryId + '"]').attr('childid');
        var childCategories = getChildCategories(categoryId);
        if (parentCategory) {
            show('.parentCategoryDiv');
            $('.category-page[categoryid="' + categoryId + '"] .parentCategoryDiv a').attr('category', parentCategory);
            $('.category-page[categoryid="' + categoryId + '"] .parentCategoryDiv a').attr('filter_field', $('#AllCategories li[category="' + parentCategory + '"]').attr('nodefilterfield'));
            $('.parentCategoryTitle').html($('#AllCategories li[category="' + parentCategory + '"]').html());
        }
        else {
            $('.parentCategoryDiv').css('display', 'none');
        }
        // isNaN(categoryId)?'':( getSourceTable(categoryId)? eval('activeParcel.' + getSourceTable(categoryId)) .length > 0 :false) ? $('.currentCategoryTitle').show() : $('.currentCategoryTitle').hide();

        if (childCategories.length <= 0) {
            $('.childDiv').css('display', 'none');

        }
        else {
            var con_falg = true;
            for (yFlag in childCategories) {
                con_falg = con_falg || (getSourceTable(childCategories[yFlag]) ? (eval('activeParcel.' + getSourceTable(childCategories[yFlag])).length > 0) : false);
            }
            if (con_falg) {
                $('.childDiv').css('display', 'block');
                $('.childCategoryDiv').css('display', 'block');
                $('.childCategoryDiv').css('width', '100%');
                var childIds = childCategories;
                var childNo = childIds.length;
                var childHtml = '';
                var filter_field = [];
                var index = 0;
                for (x in childCategories) {
                    // var childName = $('#AllCategories li[category="' + childIds[0] + '"]').html();
                    if (clientSettings["EnablePersonalProperty"] == '1' && !isBPPParcel) {
                        if (getCategory(childCategories[x]).Type == '1') continue;
                    }
                    else if (clientSettings["EnablePersonalProperty"] == '1') {
                        if (getCategory(childCategories[x]).Type != '1' || getCategory(childCategories[x]).Type != '2') continue;
                    }
                    var childName = getFieldCategoryName(childCategories[x]);
                    //var childCount = getFilteredIndexOfAuxData(childCategories[x], getSourceTable(childCategories[x])).length || '0';
                    //filter_field = $('#AllCategories li[category="' + childIds[0] + '"]').attr('nodefilterfield');
                    filter_field = getFilterField(childCategories[x]);
                    childHtml += '<a class="childCategoryTitle" category="' + childCategories[x] + '" filter_field="' + filter_field + '" nodetype="child" onclick="openSublevel(this);"style="cursor: pointer;">' + childName + '  <label>(<span></span>)</label></a>';
                    if (x == (childCategories.length - 1)) {
                        childHtml += ''
                    }

                }
                $('.category-page[categoryid="' + categoryId + '"] .childCategoryDiv').html(childHtml);
                //highlightCatLink();
                refreshAuxCount(categoryId, $('.category-page[categoryid="' + categoryId + '"]'));
            }
            else {
                $('.childDiv').css('display', 'none');
            }
        }
        if (!highlight)
            highlightCatLink();
        if (!parentCategory && !con_falg) {
            $('.sublevelDiv').css('display', 'none');
        }
        else {
            $('.sublevelDiv').css('display', 'block');
        }


        return;
    }
    catch (e) {
        console.error(e);
    }
}

function highlightCatLink() {
    var highLightedCats = []
    var clsFCE = 'field-category-edited';
    $('div.sublevelDiv a').removeClass('field-category-edited');
    $('div.sublevelDiv a').removeClass('approved');
    //$('.field-category-link').removeClass('approved');
    var cat_Id, sourceTable, pindex, auxROWuid;
    /*fieldCategories.filter(function(x) { 
        if($('.category-page [categoryid = "'+x.Id+'"]').is(':visible') == true && activeParcel){
            cat_Id = x.Id;
            sourceTable = $('.category-page[categoryid="' + x.Id + '"]').attr('auxdata');
            pindex = $('.category-page[categoryid="' + x.Id + '"] span.aux-index').attr('findex');
            auxROWuid = (sourceTable && pindex)? eval('activeParcel.' + sourceTable + '[' + pindex + '].ROWUID'): null;
        }
    })*/
    var childCategories = cat_Id ? getChildCategories(cat_Id) : [];
    for (x in activeParcel.ParcelChanges) {
        var ch = activeParcel.ParcelChanges[x];
        if ((activeParcel[ch.SourceTable] && activeParcel[ch.SourceTable].filter(function (f) { return f.ROWUID == ch.AuxROWUID }).length > 0) || ch.AuxROWUID == null) {
            if (((ch.Action == 'new') || (ch.Action == 'edit') || (ch.Action == 'delete') || (ch.Action == 'futurecopy')) && ((getCategory(ch.CategoryId) && getCategory(ch.CategoryId).IsReadOnly != true) && ((ch.Action == 'delete') || (datafields[ch.FieldId] && datafields[ch.FieldId].ReadOnly != true && datafields[ch.FieldId].DoNotShow != true)) || (datafields[ch.FieldId] && (ch.Action == 'new' && (datafields[ch.FieldId].DoNotShow == true || datafields[ch.FieldId].DoNotShow == 'true'))))) {  // if delete PCI exists then not considering delete PCI field properties.Only checking category property. // Also checking if PCI is action = 'new' then show the change indication  // condition changed to correct new action

                // if(ch.QCChecked && catId )
                //$(cat).addClass('approved');
                // else {
                //$(cat).removeClass('approved');
                var parentCategory, tempParentCategory, childCategory;
                parentCategory = ch.CategoryId;
                /*for (index in childCategories) {
                    var childName = $('.category-page[categoryid="' + childCategories[index] + '"]').attr('auxdata');
                    var childCat_Id = childCategories[index];
                    var categorySameTable = (datafields[ch.FieldId] && datafields[ch.FieldId].CategoryId == childCat_Id && getSourceTable(childCat_Id) == getSourceTable(cat_Id)) ? true : false;
                    if(auxROWuid && childCat_Id == ch.CategoryId){
                        var source = activeParcel[ch.SourceTable], PAuxRowId = null, categorySameTable = null;
                        if (source) {
                            var curCat = source.filter(function (row) { return row.ROWUID == ch.AuxROWUID })
                            PAuxRowId = (curCat.length > 0 && curCat[0].parentRecord) ? curCat[0].parentRecord.ROWUID : null
                        }
                        if(ch.SourceTable == childName && (ch.ParentAuxROWUID == auxROWuid || PAuxRowId == auxROWuid || categorySameTable)){
                            var cls1 = $('.childCategoryTitle[category=' + childCat_Id + ']');
                            cls1.addClass(clsFCE);
                            if(ch.QCChecked && activeParcel.QC)
                                cls1.addClass('approved');
                            else
                                cls1.removeClass('approved');
                        }
                        else {
                            var cls1 = '.childCategoryTitle[category=' + childCat_Id + ']';
                            $(cls1).removeClass(clsFCE);
                            $(cls1).removeClass('approved');
                            var cls0 = 'div.sublevelDiv  a[category="' + childCat_Id + '"]';
                            $(cls0).removeClass(clsFCE);
                                $(cls0).removeClass('approved');	
                        }
                    }
                }*/
                if (highLightedCats.indexOf(parentCategory) > -1) continue;
                highLightedCats.push(parentCategory);
                if (parentCategory == null) {
                    parentCategory = getCategoryFromSourceTable(ch.SourceTable).Id || null;
                    childCategory = getCategoryFromSourceTable(ch.SourceTable).Id;
                } else {
                    childCategory = ch.CategoryId;
                }
                do {
                    tempParentCategory = $('#AllCategories li[childid="' + parentCategory + '"]').attr('category');

                    if (undefined == tempParentCategory) {
                        break;
                    }
                    else {
                        parentCategory = tempParentCategory;
                    }


                } while (true);

                var cls1 = 'div.sublevelDiv  a[category="' + ch.CategoryId + '"]';
                var clsO = '.field-category-link[category="' + parentCategory + '"]';
                var cls3 = '.parentCategoryTitle[category="' + parentCategory + '"]';
                if (!$(clsO).hasClass(clsFCE))   // && !activeParcel.QC)
                    $(clsO).addClass(clsFCE);
                $(cls1).addClass(clsFCE);
                $(cls3).addClass(clsFCE);
                if (ch.QCChecked && activeParcel.QC) {
                    if (!$(clsO).hasClass('approved'))   // && !activeParcel.QC)
                        $(clsO).addClass('approved');
                    $(cls1).addClass('approved');
                    $(cls3).addClass('approved');
                }
                else {
                    $(clsO).removeClass('approved');
                    $(cls1).removeClass('approved');
                    $(cls3).removeClass('approved');
                }

                //}
            }
        }
    }
    /*for( var x in tableListGlobal ){
        var tablename = tableListGlobal[x];
        if(activeParcel[tablename] && activeParcel[tablename].filter(function(chk){ return chk.CC_Deleted == true }).length > 0){
            var categoryId = getCategoryFromSourceTable(tablename) ? getCategoryFromSourceTable(tablename).Id : null;
            if (categoryId){
                var topCat = getTopParentCategory(categoryId);
                var subLevelselector = 'div.sublevelDiv  a[category="' + categoryId + '"]';
                var parentTitleselector = '.parentCategoryTitle[category="' + topCat + '"]';
                var linkselector = '.field-category-link[category="' + topCat + '"]';
                if(activeParcel.QC){
                    if(!$(linkselector).hasClass('approved')){
                        $(linkselector).removeClass(clsFCE).addClass('approved');
                        $(subLevelselector).removeClass(clsFCE).addClass('approved');
                        $(parentTitleselector).removeClass(clsFCE).addClass('approved');
                    }
                }
                else{
                    if(!$(linkselector).hasClass(clsFCE)){
                        $(linkselector).removeClass('approved').addClass(clsFCE);
                        $(subLevelselector).removeClass('approved').addClass(clsFCE);
                        $(parentTitleselector).removeClass('approved').addClass(clsFCE);
                    }
                }
            }	
        }
    }*/
}

function openSublevel(source) {
    try {
        var ROWUID;
        var category = $(source).attr('category');
        //openCategory(category, source.innerHTML);
        var nodetype = $(source).attr('nodetype');
        var auxForm = $('.category-page[categoryid="' + category + '"]');
        var source = $('.category-page[categoryid="' + category + '"]').attr('auxdata');

        if (nodetype == 'parent') {
            var Pindex = $('.category-page[categoryid="' + category + '"] span.aux-index').html();
            var filter_value = [];
            var filter_field = [];
            var parentCategory = $('#AllCategories li[category="' + category + '"]').attr('parentid');
            if (parentCategory != undefined) {
                var filter_fieldCSV = $('#AllCategories li[category="' + category + '"]').attr('nodefilterfield');
                if (filter_fieldCSV == '') {
                }
                else {
                    var sourceTable = $('.category-page[categoryid="' + parentCategory + '"]').attr('auxdata');
                    var index = $('.category-page[categoryid="' + parentCategory + '"] span.aux-index').attr('findex');
                    var iter_value = 0;
                    filter_field = filter_fieldCSV.split(',');
                    var filter_field_lenght = filter_field.length;
                    while (iter_value < filter_field_lenght) {
                        var childFilter, parentFilter; parentFilter = childFilter = filter_field[iter_value];
                        if (childFilter.indexOf('/') > -1) { parentFilter = childFilter.split('/')[0]; childFilter = childFilter.split('/')[1]; }
                        filter_value[iter_value] = eval('activeParcel.' + sourceTable + '[' + index + '].' + parentFilter);
                        iter_value += 1;
                    }
                }

                ROWUID = eval('activeParcel.' + sourceTable + '[' + index + '].ROWUID');
            }


            openCategory(category, getInnerHtml(category), 1);
            loadParentChildOfCategory(category);
            highlightCatLink();
            showAuxData($('.category-page[categoryid="' + category + '"]'), Pindex - 1, filter_field, filter_value, ROWUID);
            var index = $('.aux-grid-switch', auxForm).attr('index');
            if (source)
                showForm(auxForm, category, index)
        }
        else if (nodetype == 'child') {
            var filter_value = [];
            var filter_field = [];
            var parentCategory = $('#AllCategories li[category="' + category + '"]').attr('parentid');
            var sourceTable = $('.category-page[categoryid="' + parentCategory + '"]').attr('auxdata');
            var roowid = $('.category-page[categoryid="' + parentCategory + '"]').attr('ROWUID');
            if (sourceTable) {
                if (roowid)
                    var index = activeParcel[sourceTable].indexOf(activeParcel[sourceTable].filter(function (x) { return x.ROWUID == roowid })[0])
                else
                    var index = $('.category-page[categoryid="' + parentCategory + '"] span.aux-index').attr('findex');
                if (parentCategory && activeParcel[sourceTable].length) {
                    var filter_fieldCSV = $('#AllCategories li[category="' + category + '"]').attr('nodefilterfield');
                    if (filter_fieldCSV == '') {
                    }
                    else {
                        var iter_value = 0;
                        filter_field = filter_fieldCSV.split(',');
                        var filter_field_lenght = filter_field.length;
                        while (iter_value < filter_field_lenght) {
                            var childFilter, parentFilter; parentFilter = childFilter = filter_field[iter_value];
                            if (childFilter.indexOf('/') > -1) { parentFilter = childFilter.split('/')[0]; childFilter = childFilter.split('/')[1]; }
                            filter_value[iter_value] = eval('activeParcel.' + sourceTable + '[' + index + '].' + parentFilter);
                            iter_value += 1;
                        }
                    }
                }
                else {
                    if (parentCategory) {
                        alert(getFieldCategoryName(parentCategory) + ' is Empty.');
                    }
                }
                ROWUID = eval('activeParcel.' + sourceTable + '[' + index + '].ROWUID');
            } else {
                var filter_fieldCSV = $('#AllCategories li[category="' + category + '"]').attr('nodefilterfield');
                if (filter_fieldCSV == '') {
                }
                else {
                    var iter_value = 0;
                    filter_field = filter_fieldCSV.split(',');
                    var filter_field_lenght = filter_field.length;
                    while (iter_value < filter_field_lenght) {
                        filter_value[iter_value] = eval('activeParcel.' + filter_field[iter_value]);
                        iter_value += 1;
                    }
                }
            }
            if (showAuxDataWithFilter(getSourceTable(category), filter_field, filter_value, ROWUID, category).length > 0 || 1 == 1) {
                openCategory(category, getInnerHtml(category));
                loadParentChildOfCategory(category);
                //var parCatId = getParentCategoryIds(category)[0];
                // var af = $('.category-page[categoryid="' + parCatId + '"]');
                // if( $('.childCategoryTitle span',af).html() == "0")
                //    $('.childCategoryTitle ').hide();
                showAuxData($('.category-page[categoryid="' + category + '"]'), 0, filter_field, filter_value, ROWUID);
            }
            else {
                showDataNavOnly(category);
                //            Vm
                //                 alert('There are no ' + getInnerHtml(category) + ' records available');
                //                                return false;
                //            Vm
            }
        }

        //        loadParentChildOfCategory(category);
        var txtarea = $('.category-page[categoryid = "' + category + '"] textarea');
        for (var x = 0; x < txtarea.length; x++) {
            txtarea[x].style.cssText = 'height:auto';
            txtarea[x].style.cssText = 'height:' + txtarea[x].scrollHeight + 'px';
        }
        return;
    }
    catch (e) {
        console.log(e.message);
    }
}
function showDataNavOnly(category) {
    console.log(category);
}
function showchildCategoryDiv(source) {
    try {
        $(source).siblings('.childCategoryDiv').show();
        return;
    }
    catch (e) {
        console.error(e);
    }
}
function initSmartScreen(callback) {
    $('.hideSearchPanel').show();
    $('.showSearchPanel').hide();
    $('.showSearchPanel').click(function () {
        getSearchScreen();
    });
    $('.hideSearchPanel').click(function () {
        hideSearchScreen();
    });
    $('.screen-up').show();
    $('.screen-down').hide();
    if (callback) callback();
}

function initChangePopupLink(callback) {
    $('.qc a').click(function (e) {
        hide('.field-audit-qc');
        $('.qc a').removeAttr("clicked");
        var qc = this;
        $(qc).attr("clicked", "yes");
        var fieldId = parseInt($(this).parent().attr('fieldid'));
        var divX = e.clientX - e.offsetX + 25;
        var divY = e.clientY - e.offsetY;
        var top = e.clientY;
        var block = qc.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode;
        var index = $(block).attr('findex');
        if ($(block).attr('Id') == "classCalculatorFrame")
            index = (index || index === 0) ? index : '-1';
        var position1 = $('.parcel-data-head').offset().top;
        var blockActualPosition = $(block).position();
        var blockVariablePosition = blockActualPosition.top;
        var fixedposition = position1 + 110 - (blockVariablePosition);
        if ($('.parcel-head')[0].offsetHeight == 0) fixedposition = fixedposition + 140;
        currentInputValue = qc.parentNode.parentNode.children[0].value;
        var hgt = (($(block).children('.data-navigator')[0] && $(block).children('.data-navigator').is(":visible")) ? '286px' : '320px');
        $('.field-audit-qc').css({
            'overflow-y': 'auto',
            'position': 'absolute',
            'z-index': '10000',
            'right': '10px',
            'height': hgt,
            'width': '273px',
            'top': fixedposition
        });

        var fieldChanges = [];
        var auxROWuid;
        if (qc.parentNode.attributes['auxrowuid'] != undefined) {
            auxROWuid = qc.parentNode.attributes['auxrowuid'].value;
        }
        else {
            auxROWuid = null;
        }
        for (x in activeParcel.ParcelChanges) {
            var ch = activeParcel.ParcelChanges[x];
            if (ch.FieldId == fieldId && ch.AuxROWUID == auxROWuid && !(ch.NewValue == null && ch.OriginalValue == null)) {
                var customFormat = false;
                var format = checkForCustomFormat(fieldId);
                customFormat = (format == '' || format == null) ? false : true;
                //  var changedTime = ch.ChangedTime.utcStringToLocalTime();
                var changedTime = new Date(ch.LocalChangedTime);
                var cdate = changedTime.localeFormat("d");
                var ctime = changedTime.localeFormat("t");
                var today = (new Date()).localeFormat("d");
                var newValue = ch.NewValue;
                newValue = (newValue && newValue.length) ? newValue.replace('&gt;', '>') : ""
                newValue = (newValue && newValue.length) ? newValue.replace('&lt;', '<') : ""
                newValue = (newValue && newValue.length) ? newValue.replace(/\\/g, "\\\\").replace(/"/g, '\\"') : ""
                //var displayText = evalvalue(fieldId, customFormat ? customFormatValue(ch.NewValue) : ch.NewValue);
                var displayText = evalvalue(fieldId, customFormat ? customFormatValue(newValue, fieldId) : newValue, ch.NewValue);
                var edate = cdate;
                if (cdate == today) {
                    edate = ctime;
                }
                if (displayText == null || displayText == '') {
                    displayText = '[blank]';
                }
                /*var shortDisplayText = displayText.replace(/"/g, '\'');
                if (displayText.toString().length > 20) {
                    shortDisplayText = displayText.toString().substr(0, 20) + '...'*/
                var shortDisplayText = displayText;
                var strDisplayText = displayText.toString().replace(/\\"/g, "&quot;");
                if (displayText.length > 20) {
                    shortDisplayText = strDisplayText.substr(0, 20) + '...';
                    displayText = displayText.replace(/\\"/g, '&quot;');
                }
                var fc = {
                    Id: ch.Id,
                    AuxROWUID: ch.AuxROWUID ? ch.AuxROWUID : 0,
                    FieldId: fieldId,
                    ChangedValue: customFormat ? customFormatValue(ch.NewValue, fieldId) : ch.NewValue,
                    OriginalValue: customFormat ? customFormatValue(ch.OriginalValue, fieldId) : ch.OriginalValue,
                    DisplayText: shortDisplayText,
                    FullDisplayText: displayText,
                    ReviewedBy: ch.LoginID,
                    Index: index,
                    ChangedOn: edate,
                    ChangedDate: cdate,
                    ChangedTime: ctime
                };
                fieldChanges.push(fc);
            }
        }

        activeFieldChanges = fieldChanges;
        var firstChange = fieldChanges[0];
        var initialValue = fieldChanges[0]?.OriginalValue;
        var initialValueText = evalvalue(fieldId, initialValue, initialValue);
        initialValueText = (initialValueText == null || initialValueText == '') ? "[blank]" : initialValueText;
        $('.initial-value', '.field-audit-qc').html(initialValueText);
        $('.initial-value', '.field-audit-qc').unbind('click');
        $('.initial-value', '.field-audit-qc').bind('click', function () {
            let lp = $(`td.value[fieldId=${fieldId}] input.cc-drop`);
            let rows = lp && lp[0] && lp[0].getLookup && lp[0].getLookup.rows;
            if (rows) {
                let isExists = false;
                for (var k of Object.keys(rows)) {
                    if (initialValueText == rows[k]?.Id || initialValueText == rows[k]?.Name || initialValueText == '[blank]') {
                        isExists = true;
                        break;
                    }
                }
                if (isExists) selectChange(firstChange?.Id, firstChange?.FieldId, firstChange?.AuxROWUID, index, true);
                else {
                    alert('The value you are trying to select is a non existing value in the lookup. Please try another one.')
                }
            } else selectChange(firstChange?.Id, firstChange?.FieldId, firstChange?.AuxROWUID, index, true);
        });

        $('table.parcel-changes tbody').html($('table.parcel-changes tfoot').html());
        $('table.parcel-changes tbody').fillTemplate(fieldChanges);
        show('.field-audit-qc');

    });
    if (callback) { callback(); }
}

function initDataNavigator(callback) {
    $('.data-navigator button[ddir]').attr('disabled', 'disabled');
    $('.data-navigator button').click(function () {
        try {
            var cat = $(this).parents('.category-page');
            var CategoryId = $(cat).attr('categoryid');
            var auxForm = $('.category-page[categoryid="' + CategoryId + '"]');
            var sourceTable = $(auxForm).attr('auxdata')
            var showAuxParams = getParamOfAuxData(CategoryId)[0];
            var index = parseInt($('.aux-index', cat).html()) - 1;
            var count = parseInt($('.aux-records', cat).html());
            var category = fieldCategories.filter(function (d) { return d.Id == CategoryId; })[0];
            switch (this.className) {
                case "move-first":
                    if (disableSwitch == true) return false;
                    //disableSwitch = true; 
                    autoselectloop = [];
                    LFields = [];
                    index = 0;
                    break;
                case "move-prev":
                    if (disableSwitch == true) return false;
                    //disableSwitch = true; 
                    autoselectloop = [];
                    LFields = [];
                    index = Math.max(index - 1, 0);
                    break;
                case "move-next":
                    if (disableSwitch == true) return false;
                    //disableSwitch = true; 
                    autoselectloop = [];
                    LFields = [];
                    index = Math.min(index + 1, count - 1);
                    break;
                case "move-last":
                    if (disableSwitch == true) return false;
                    //disableSwitch = true; 
                    autoselectloop = [];
                    LFields = [];
                    index = count - 1;
                    break;

            }
            relationcycle = [];
            $('.aux-grid-switch', cat).attr('index', index);
            var filteredIndexes = getFilteredIndexOfAuxData(CategoryId, sourceTable)
            if (filteredIndexes)
                $(auxForm).attr('findex', filteredIndexes[index]);
            else
                $(auxForm).attr('findex', 0);
            if (this.className == "new-item") {
                disableSwitch = false;
                if (category.MaxRecords && count >= category.MaxRecords) {
                    alert('Maximum number of records(' + category.MaxRecords.toString() + ') has been reached for this category')
                    return false;
                }
                createNew(function () {
                    showAuxData(cat, count, showAuxParams.filter_field, showAuxParams.filter_value, showAuxParams.parentROWUID, function (fIndex) {
                        $(cat).attr('findex', fIndex);
                        var catId = $(cat).attr('categoryid');
                        refreshAuxCount(catId, cat)
                    });
                });
            }
            else
                showAuxData(cat, index, showAuxParams.filter_field, showAuxParams.filter_value, showAuxParams.parentROWUID);

        } catch (e) {
            console.error(e);
        }
        return false;
    });
    if (callback) callback();
}

function checkSchemaUpdates() {
    if (localStorage.getItem('lastUpdatedCounter') == null)
        localStorage.setItem("lastUpdatedCounter", '0');
    var updatedCounter = parseInt(localStorage.getItem("lastUpdatedCounter"));
    lastUpdatedCounter(function (data) {
        var DBUpdatedCounter = parseInt(data);
        if (DBUpdatedCounter > updatedCounter) {
            lastUpdatedCounter();
            notification("This notification is to indicate a settings change has been made. <a class='reset-app-link' onclick='$(\".app-action-reset-cache\").trigger(\"click\")'>Click here</a> to Update System Data", "Updates available");
        }
    });
}


function initDataChangeTracker(callback) {
    $('.trackchanges').focus(function () {
        currentInputElement = this;
        currentInputValue = $(this).val();
    });
    $('.trackchanges').keypress(function (event) {
        var fieldId = $(this).parent().attr("fieldid");
        var fields = datafields[fieldId];
        if (event && event.keyCode == 46) {
            var value = $(this).val();
            if (!value && $(this).attr("type") == "number") $(this).val("");
        }
        return numberValidation($(this).val(), fieldId, event)
    });
    $('.trackchanges').blur(function () {
        relationcycle = [];
        currentInputElement = null;
        var value = $(this).val();
        if (!value && $(this).attr("type") == "number") $(this).val("");
        if (!$(this).attr('readonly'))
            value = numberValidation(value, $(this).parent().attr("fieldid"));
        if (currentInputValue != value && !(currentInputValue == null && value == '')) {
            selectOwnValue(this, $(this).parent().attr("fieldid"), $(this).attr('auxrowuid'), "edit");
        }
    });
    if (callback) callback();
}

function initRadioButtonChangeTracker() {
    $('.trackchanges input[type=radio]').change(function () {
        relationcycle = [];
        currentInputElement = null;
        var value = $(this).val();
        selectOwnValue(this, $(this).parents('td').attr("fieldid"), $(this).parents('span.input').attr('auxrowuid'), "edit");
    });
}

var mapCenter, mapZoom, mapPosition, panorama, enablestreetview = 0, bounds;
var sv;
let GeoLocationmap;

function initGoogleMap(callback) {
    let geoMapOptions = { zoom: 18, maxZoom: 19, minZoom: 14, mapTypeId: google.maps.MapTypeId.HYBRID, gestureHandling: 'greedy', tilt: 0, center: new google.maps.LatLng(35.56, -96.84) }
    GeoLocationmap = new google.maps.Map(document.getElementById("GeoMAP"), geoMapOptions);

    if ((clientSettings.DefaultImagery && clientSettings.DefaultImagery.toLowerCase() == 'webservice') || (clientSettings.customWMS && clientSettings.customWMS.trim() != "" && ((clientSettings.EnableCustomWMS && clientSettings.EnableCustomWMS.includes('DTR') && __DTR) || (clientSettings.EnableCustomWMS && clientSettings.EnableCustomWMS.includes('QC') && !__DTR)) && !OsMap)) {
        //$('.mapview').removeClass('hidden');
        $('.custommap').height($('.custommap').height());
        $('.custommap').width($('.custommap').width());
        if (clientSettings.customMapIsImage && clientSettings.customMapIsImage == 1) {

            //arcgisImageserver like image maps needs to be processed in a differnt way unlike WMS/WMTS maps.
            //So we include esri-leaflet.js in our  projectcode. 
            var mapOptions = {
                center: [43.996027041746245, -121.3533050715885],
                zoom: 19,
                maxZoom: 19,
                minZoom: 10
            }
            OsMap = L.map('custommap', mapOptions);

            L.esri.imageMapLayer({
                url: clientSettings['customWMS'],
                attribution: 'CC'
            }).addTo(OsMap);
            OsMap.zoomControl.setPosition('topright');
            //$('.mapview').addClass('hidden');
            $('.leaflet-top').css('z-index', 10);

        }
        else {
            var customWMS = L.tileLayer(clientSettings['customWMS'] + '{z}/{y}/{x}', {
                maxZoom: 19,
                minZoom: 10
            });
            var mapOptions = {
                center: [43.996027041746245, -121.3533050715885],
                zoom: 19
            }
            OsMap = L.map('custommap', mapOptions);
            customWMS.addTo(OsMap);
            OsMap.zoomControl.setPosition('topright');
            //$('.mapview').addClass('hidden');
            $('.leaflet-top').css('z-index', 10);
        }
    } else {
        $('#custommap').addClass('hidden');
    }
    mapZoom = 5;
    mapPosition = new google.maps.LatLng(42.345573, -71.098326);
    mapCenter = new google.maps.LatLng(35.56, -96.84);
    var mapOptions = {
        zoom: mapZoom,
        minZoom: 5,
        mapTypeId: google.maps.MapTypeId.HYBRID,
        center: mapCenter,
        gestureHandling: 'greedy',
        rotateControlOptions: {
            position: google.maps.ControlPosition.LEFT_BOTTOM,
        },
    };

    $('.googlemap').height($('.googlemap').height());
    $('.googlemap').width($('.googlemap').width());

    map = new google.maps.Map(document.getElementById('googlemap'), mapOptions);
    sv = new google.maps.StreetViewService();
    //street view map settings
    enablestreetview = clientSettings['EnableStreetView'] || 0;
    // Try HTML5 geolocation
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            mapCenter = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            mapPosition = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            resetMap();
        }, function () {
            handleNoGeolocation(true);
        });
    } else {
        //.Browser doesn't support Geolocation
        handleNoGeolocation(false);
    }
    google.maps.event.addListener(map.getStreetView(), 'visible_changed', function () {
        if (this.getVisible()) clearMarkers();
        else {
            if (__DTR) showAdjacentParcels();
        }
    });

    var measure;
    var measureDiv = document.createElement('div');
    measure = new MeasurementScale(measureDiv, map);
    measureDiv.index = 1;
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(measureDiv);

    google.maps.event.addListener(map, 'click', (function (e) {
        if (measure.on) {
            if (measure.start == null) {
                measure.start = e.latLng;
            } else {
                if (measure.end == null) {
                    measure.end = e.latLng;
                } else {
                    measure.start = e.latLng;
                    measure.end = null;
                }
            }

            measure.updateGraphics();
            // $('.sv-logo', window.parent.document).click().focus();
            e.stop();
        }
    }));

    if (callback) callback();

}

function handleNoGeolocation() {

}
function openThisParcel(pid) {
    try {
        checkIfSaved(null, null, function () {
            openParcelWithHash(pid);
        });

    } catch (e) { }
    return false;
}
function clearMarkers() {
    if (adjMapMarkers) {
        var keys = Object.keys(adjMapMarkers);
        for (x in keys) {
            if (adjMapMarkers[keys[x]].setMap) {
                adjMapMarkers[keys[x]].setMap(null);
                adjMapMarkers[keys[x]] = null;
            }
        }
        adjMapMarkers = [];
    }
    infoBubble.close();
}
var adjParcels;
var markers = [];
var adjMapMarkers = [];
// var info = new google.maps.InfoWindow();
var infoBubble = new InfoBubble({
    map: map,
    shadowStyle: 1,
    padding: 2,
    backgroundColor: '#fafafa',
    arrowSize: 10,
    disableAutoPan: true,
    hideCloseButton: true,
    arrowPosition: 30,
    borderRadius: 0,
    backgroundClassName: 'infoDiv',
    arrowStyle: 2
});

function createCustomMarker(pl) {
    let color = '#f602bb';
    if (pl.DTRStatus && pl.DTRStatus != '') {
        let dts = offlineTables.DTRStatusTypeAll.filter((x) => { return x.Code == pl.DTRStatus })[0];
        if (dts && dts.GIS_Flag_Color && dts.GIS_Flag_Color != '') {
            color = dts.GIS_Flag_Color;
        }
    }

    return {
        path: 'M121.1,10.1c-0.9,0.1-3.6,0.4-6,0.7c-5.5,0.6-15.5,3.1-21,5.3C65.8,27.3,47,51.7,42.8,82.8c-1,6.9-1,20.4,0,26.9c1.5,10.4,4.2,18.8,9.5,29.2c5.4,10.6,11.4,19.3,29.8,42.9c19.1,24.6,30.2,39.9,40.3,55.8c3,4.7,5.5,8.5,5.6,8.5c0.1,0,0.8-1,1.6-2.2c0.8-1.2,4.3-6.7,8-12.1c10.4-15.9,18.2-26.4,37.1-50.8c17.8-22.9,23.8-31.6,29.3-42.5c7.2-14.3,10-26,10-42.2c0-15.3-2.7-27.7-8.5-40c-11.7-24.4-34.7-40.9-62.9-45.2C138,10.3,124.2,9.7,121.1,10.1z M153.5,69.8c19.1,19.1,24.4,24.6,24.4,25.6c0,0.8-0.9,2-2.9,4c-2.3,2.3-3.2,2.9-4.3,2.9c-1.2,0-3.9-2.5-21.5-20.1c-15.6-15.5-20.4-20.1-21.3-20.1c-0.9,0-5.7,4.6-21.3,20.1c-17.4,17.4-20.3,20.1-21.5,20.1c-1.8,0-7.2-5.2-7.2-6.9c0-1.7,48.2-49.9,49.9-49.9C128.9,45.4,134.3,50.6,153.5,69.8z M148.7,88l19.1,19.1v18.8c0,18.3,0,18.9-0.9,19.8c-0.9,0.9-1.5,0.9-12.4,0.9c-10.4,0-11.6-0.1-12.2-0.8c-0.6-0.7-0.8-2.7-0.9-14.7c-0.1-12.9-0.2-14-1-14.9c-0.8-0.9-1.3-1-12.3-1c-11,0-11.5,0-12.3,1c-0.8,0.9-0.9,1.9-1,14.9c-0.1,12-0.2,14-0.9,14.7c-0.7,0.7-1.8,0.8-12.2,0.8c-10.9,0-11.5,0-12.4-0.9c-0.9-0.9-0.9-1.5-0.9-19.8v-18.8L107.3,88c17.7-17.7,19.2-19,20.7-19C129.5,69,130.9,70.3,148.7,88z',
        fillColor: color,
        fillOpacity: 1,
        strokeColor: 'none', 
        strokeWeight: 0,     
        scale: 0.16,  
        origin: new google.maps.Point(240, 290), 
        anchor: new google.maps.Point(240, 290) 
    };
}

function showAdjacentParcels() {
    clearMarkers();
    var adjMapPoints = activeParcel.AdjacentParcels;
    adjParcels = [];
    for (x in adjMapPoints) {
        var point = adjMapPoints[x];
        var pid = point.ParcelId.toString();
        if (adjParcels[pid] == null)
            adjParcels[pid] = {
                ParcelId: point.ParcelId,
                Keyvalue1: point.Keyvalue1,
                Location: null,
                infoSub: null,
                Reviewed: point.Reviewed,
                DTRStatus: point.DTRStatus,
                Points: [],
            };
        adjParcels[pid].Points.push(point.Points);

    }
    var totalParcels = Object.keys(adjParcels).length;
    for (x in adjParcels) {
        parcel = adjParcels[x];
        var b = new google.maps.LatLngBounds();
        if (activeParcel.Id != parcel.ParcelId) {
            for (i in parcel.Points) {
                pstr = parcel.Points[i].split(',');
                loc = new google.maps.LatLng(parseFloat(pstr[0].replace('[', '')), parseFloat(pstr[1].replace('[', '')));
                b.extend(loc)
            }
            var parcelLoc = new google.maps.LatLng(b.getCenter().lat(), b.getCenter().lng());
            
            if (adjMapMarkers[parcel.ParcelId.toString()] == null) {
                adjMapMarkers[parcel.ParcelId.toString()] = new google.maps.Marker({
                    position: parcelLoc,
                    map: map,
                    icon: createCustomMarker(parcel)
                });
                adjMapMarkers[parcel.ParcelId.toString()].pid = parcel.ParcelId;
                adjMapMarkers[parcel.ParcelId.toString()].Keyvalue1 = parcel.Keyvalue1;
            }
            adjMapMarkers[parcel.ParcelId.toString()].setMap(map);
            google.maps.event.addListener(adjMapMarkers[parcel.ParcelId.toString()], 'click', function () {
                var e = this;
                var pdata = adjParcels[this.pid]
                pdata.infoSub = "<table><tr><td colspan='2' style='font-weight: bolder; font-size: 11px; padding: 5px 0px 0px 6px; white-space:nowrap;'>Parcel# : " + pdata.Keyvalue1.toString() + "</td></tr><tr><td><button class='mapButton' onclick='openThisParcel(" + pdata.ParcelId + ")'>Open</button></td><td><button class='mapButton' onclick='infoBubble.close()' style = 'float : right;'>Close</button></td></tr></table>";
                infoBubble.setContent(pdata.infoSub);
                $('.infoDiv').parent().css('height', '100%');
                $('.infoDiv').parent().css('width', '100%');
                infoBubble.open(map, e);

            });
        }
    }
    if ((!activeParcel.MapPoints || activeParcel.MapPoints.length == 0) && b) {
        map.setCenter(b.getCenter());
        map.fitBounds(b);
    }
}
function resetMap() {
    google.maps.event.trigger(map, "resize");
    if (parcelBoundaries) {
        parcelBoundaries.setMap(null);
    }
    if (map.getStreetView().visible) google.maps.event.trigger(window, 'resize', {});
    if (activeParcel) {
        showActiveParcelOnMap();
        if (__DTR) {
            if (map.getStreetView().visible) clearMarkers();
            else showAdjacentParcels();
        }
    } else {
        map.setCenter(mapCenter);
        map.setZoom(mapZoom);
    }
}

function showActiveParcelOnMap() {
    class Popup extends google.maps.OverlayView {
        constructor(position, content) {
            super();
            this.position = position;
            content.classList.add("popup-bubble"); // This zero-height div is positioned at the bottom of the bubble.

            const bubbleAnchor = document.createElement("div");
            bubbleAnchor.classList.add("popup-bubble-anchor");
            bubbleAnchor.appendChild(content); // This zero-height div is positioned at the bottom of the tip.

            this.containerDiv = document.createElement("div");
            this.containerDiv.classList.add("popup-container");
            this.containerDiv.appendChild(bubbleAnchor); // Optionally stop clicks, etc., from bubbling up to the map.

            Popup.preventMapHitsAndGesturesFrom(this.containerDiv);
        }
        /** Called when the popup is added to the map. */

        onAdd() {
            this.getPanes().floatPane.appendChild(this.containerDiv);
        }
        /** Called when the popup is removed from the map. */

        onRemove() {
            if (this.containerDiv.parentElement) {
                this.containerDiv.parentElement.removeChild(this.containerDiv);
            }
        }
        /** Called each frame when the popup needs to draw itself. */

        draw() {
            const divPosition = this.getProjection().fromLatLngToDivPixel(
                this.position
            ); // Hide the popup when it is far out of view.

            const display =
                Math.abs(divPosition.x) < 4000 && Math.abs(divPosition.y) < 4000
                    ? "block"
                    : "none";

            if (display === "block") {
                this.containerDiv.style.left = divPosition.x + "px";
                this.containerDiv.style.top = divPosition.y + "px";
            }

            if (this.containerDiv.style.display !== display) {
                this.containerDiv.style.display = display;
            }
        }
    }

    var clearLocationMarker = function () {
        for (let i = 0; i < Locationmarker.length; i++) {
            Locationmarker[i].setMap(null);
        }
        Locationmarker = [];
    }
    var addMarker = function (x, index) {
        if (x.Label) {
            var label = document.createElement('span')
            label.innerHTML = x.Label
            var marker = new Popup(
                new google.maps.LatLng(x.Latitude, x.Longitude),
                label
            );
        } else {
            var marker = new google.maps.Marker({
                position: { lat: x.Latitude, lng: x.Longitude },
                map: map,
                icon: '/App_Static/images/location.png'
            });
        }
        Locationmarker.push(marker);
        Locationmarker[index].setMap(map);
    }
    clearLocationMarker();
    if (activeParcel.ParcelGISPoints && activeParcel.ParcelGISPoints.length > 0) {
        map.setZoom(19);
        activeParcel.ParcelGISPoints.forEach(function (x, index) {
            addMarker(x, index);
        });
        var LocCenter = new google.maps.LatLng(x.Latitude, x.Longitude)
        activeParcel.mapCenter = [LocCenter.lat(), LocCenter.lng()];
        map.setCenter(LocCenter);
        showStreet(LocCenter);
    }
    if (!activeParcel.MapPoints || activeParcel.MapPoints.length == 0) {
        //alert('No map points available.');
        map.setZoom(5);
        if (parcelBoundaries) {
            parcelBoundaries.setMap(null);
            parcelBoundaries = new google.maps.Polygon();

        }
        return;
    }
    if (activeParcel.MapPoints.length > 0) {
        var points = [], shapes = [];
        bounds = new google.maps.LatLngBounds();
        for (x in activeParcel.MapPoints.sort(function (x, y) { return x.Ordinal - y.Ordinal })) {
            var p = activeParcel.MapPoints[x];
            var latlng = new google.maps.LatLng(p.Latitude, p.Longitude);
            points.push(latlng);
            bounds.extend(latlng);
            if (shapes[p.RecordNumber || 0] == null) {
                shapes[p.RecordNumber || 0] = [];
            }
            shapes[p.RecordNumber || 0].push(latlng);
            //shapes[p.RecordNumber || 0].push({ y: p.Latitude, x: p.Longitude });
        }
        shapeList = shapes.filter(function (el) {
            return el != null;
        });
        parcelBoundaries = new google.maps.Polygon({
            paths: shapeList,
            strokeColor: "#FF0000",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#FF0000",
            fillOpacity: 0.1,
            clickable: false
        });
        parcelBoundaries.setMap(map);

        var center = bounds.getCenter();
        activeParcel.bounds = bounds;
        if (center)
            activeParcel.mapCenter = [center.lat(), center.lng()];
        map.setCenter(center);
        map.fitBounds(bounds);
        showStreet(center);
    }
    if ((clientSettings.DefaultImagery && clientSettings.DefaultImagery.toLowerCase() == 'webservice') || (clientSettings.customWMS && clientSettings.customWMS.trim() != "" && ((clientSettings.EnableCustomWMS && clientSettings.EnableCustomWMS.includes('DTR') && __DTR) || (clientSettings.EnableCustomWMS && clientSettings.EnableCustomWMS.includes('QC') && !__DTR)))) {
        var WMSshapes = [];
        for (i in OsMap._layers) {
            if (OsMap._layers[i]._path != undefined) {
                try {
                    OsMap.removeLayer(OsMap._layers[i]);
                }
                catch (e) {
                    console.log("problem with " + e + OsMap._layers[i]);
                }
            }
        }
        for (x in activeParcel.MapPoints.sort(function (x, y) { return x.Ordinal - y.Ordinal })) {
            var p = activeParcel.MapPoints[x];
            if (WMSshapes[p.RecordNumber || 0] == null) {
                WMSshapes[p.RecordNumber || 0] = [];
            }
            WMSshapes[p.RecordNumber || 0].push([p.Latitude, p.Longitude]);
        };
        var WMSpolygon = [];
        WMSshapes.forEach(function (w) {
            WMSpolygon.push(w);
        });
        L.polygon(WMSpolygon, {
            color: '#fcfc2b'
        }).addTo(OsMap);
        if (center)
            OsMap.panTo(activeParcel.mapCenter);
    }
}
//street view map
function showStreet(center) {
    if (enablestreetview == '1' && __DTR) {
        streetviewSetup(center);
    }
}
function streetviewSetup(viewPosition) {
    var sv = new google.maps.StreetViewService();
    var STREETVIEW_MAX_DISTANCE = 200;
    sv.getPanoramaByLocation(viewPosition, STREETVIEW_MAX_DISTANCE, function (streetViewPanoramaData, status) {
        if (status === google.maps.StreetViewStatus.OK) {
            $('.nostreetview').hide();
            panorama = new google.maps.StreetViewPanorama(
                document.getElementById('googlemap'), {
                position: streetViewPanoramaData.location.latLng,
                pov: {
                    heading: 36,
                    pitch: 10
                }
            });
            map.setStreetView(panorama);
            panorama.setOptions({ enableCloseButton: true });

        } else {
            if (gismapview) {
                $('.nostreetview').show();
            }
            if (panorama) {
                panorama.setVisible(false);
            }
        }
    });
}

function commitAllChanges(qcno) {
    try {

    }
    catch (e) {
        console.log(e);
    }
}

function change_category_menu_color(category_no) {
    try {
        var parentCategory = findParent(category_no);
        $('.category-menu a').removeClass('categorymenucolor');
        $('.category-menu a[category="' + parentCategory + '"]').addClass('categorymenucolor');
    }
    catch (e) {
        console.log(e.message);
    }
}

function findParent(category_no) {
    var parentCategory, temp;
    temp = category_no;
    while (temp != null) {
        parentCategory = temp;
        temp = $('#AllCategories li[childid="' + temp + '"]').attr('category');

    }
    return parentCategory;
}

function toggleEditNotes() {
    try {
        if (document.getElementById('editNotes').checked) {
            $('.bulk-Notes-row').show();
            $('.bulk-Notes').val("");
            $('.updateBulk').attr('notes', 'notes');
            $('.bulk-editor').css({ 'height': '340px' });
            setScreenDimensions();
        }
        else {
            $('.bulk-Notes-row').hide();
            $('.bulk-editor').css({ 'height': '250px' });
            $('.updateBulk').removeAttr('notes');
            setScreenDimensions();
        }
        return true;
    }
    catch (e) {
        console.log(e.message);
    }
}


//function loadPageData(callback) {
//    loadFieldCategories(function () {
//        loadClientSettings(function () {
//            callback();
//        });
//    });
//    //loadClientSettings(function () { callback(); });
//}


//MSP ends


function initFieldsAndCategories() {
    displayFields = Object.keys(datafields).map(function (x) { return datafields[x] }).filter(function (x) { return x.CategoryId != null });
    displayFields.forEach(function (f) {
        if (f.CategoryId != null) {
            f.Category = fieldCategories.filter(function (x) { return x.Id == f.CategoryId })[0];
            if (f.CategoryId == -2) {
                f.Category = { Id: -2, Name: "Photo Meta Data" };
            }
        }

        ["ReadOnlyExpression", "VisibilityExpression", "CalculationExpression", "LookupQuery", "RequiredExpression", "CalculationOverrideExpression", "ConditionalValidationConfig"].forEach(function (x) {
            if (f[x] && f.Category) mapRelatedFields(displayFields, f, f.Category, f[x], x);
        });

    });

    getLookupQueryCustomFields();
}

function getLookupQueryCustomFields() {
    let lkFields = Object.keys(datafields).map((x) => { return datafields[x] }).filter((x) => { return x.LookupQueryCustomFields && x.LookupQueryCustomFields != '' });
    lkFields.forEach((fl) => {
        fl['LookupQueryCustomFields'].split(",").forEach((f) => {
            f = f.trim();
            let tb = f.split('.')[0], fn = f.split('.')[1];
            if (tb && fn) {
                let df = getDataField(fn, tb);
                if (df && !_.contains(ccma.UI.LookupQueryCustomFields, df.Id)) ccma.UI.LookupQueryCustomFields.push(df.Id);
            }
        });
    });
}

function mapRelatedFields(displayFields, field, category, expression, expType) {

    //vm - start
    //var tfields = expression.match(/\{.*?\}/g)
    //var relfields = allFields.filter(function (f) { return (category.Id == -2) ? (f.CategoryId == -2) : (category.TabularData == true) ? parseInt(f.CategoryId) == parseInt(category.Id) : f.ImportTypeId == 0 }).filter(function (x) { return _.contains(tfields, '{' + x.Name + '}') });
    //relfields.forEach(function (x) {
    //    if (ccma.UI.RelatedFields[x.Id] == undefined) ccma.UI.RelatedFields[x.Id] = [];
    //    if (!_.contains(ccma.UI.RelatedFields[x.Id], field.Id) && (x.Id != field.Id)) {
    //        ccma.UI.RelatedFields[x.Id].push(field.Id)
    //        field.IsRelatedField = true;
    //        x.HasRelatedFields = true;
    //        console.log(field, x);
    //    };
    //});
    //vm - end
    var tfields = [], relfields = [];
    if (expType == 'CalculationExpression') {
        if (expression.search(childRecCalcPattern) > -1) {
            var expr = expression.split(childRecCalcPattern)[0].split(/\./g);
            relfields = displayFields.filter(function (f) { return f.Name == expr[1] && f.SourceTable == expr[0]; })
        }
        else {
            tfields = expression.match(/([a-zA-Z_][a-zA-Z0-9_]+)/g)
            relfields = displayFields.filter(function (f) { return (category.TabularData == "true" || category.TabularData == true) ? parseInt(f.CategoryId) == parseInt(category.Id) : f.ImportTypeId == 0 }).filter(function (x) { return _.contains(tfields, x.Name) });
        }
        relfields.forEach(function (x) {
            if (ccma.UI.CalcRelatedFields[x.Id] == undefined) ccma.UI.CalcRelatedFields[x.Id] = [];
            if (!_.contains(ccma.UI.CalcRelatedFields[x.Id], field.Id) && (x.Id != field.Id)) { ccma.UI.CalcRelatedFields[x.Id].push(field.Id); }
        });
    }
    else {
        if (expType == 'LookupQuery') {
            tfields = expression.match(/\{.*?\}/g);
            for (len in tfields) {
                while (tfields[len].indexOf('parent.') > -1) {
                    tfields[len] = tfields[len].replace('parent.', '');
                }
            }
            relfields = displayFields.filter(function (f) { return (category.Id == -2) ? (f.CategoryId == -2) : (category.TabularData == "true" || category.TabularData == true) ? ((parseInt(f.CategoryId) == parseInt(category.Id)) || (parseInt(f.CategoryId) == parseInt(category.ParentCategoryId))) : f.ImportTypeId == 0 }).filter(function (x) { return _.contains(tfields, '{' + x.Name + '}') });
        }
        else {
            tfields = expression.match(/([a-zA-Z_][a-zA-Z0-9_]+)/g)
            relfields = displayFields.filter(function (f) { return (category.Id == -2) ? (f.CategoryId == -2) : (category.TabularData == "true" || category.TabularData == true) ? parseInt(f.CategoryId) == parseInt(category.Id) : f.ImportTypeId == 0 }).filter(function (x) { return _.contains(tfields, x.Name) });
        }
        relfields.forEach(function (x) {
            if (ccma.UI.RelatedFields[x.Id] == undefined) ccma.UI.RelatedFields[x.Id] = [];
            if (!_.contains(ccma.UI.RelatedFields[x.Id], field.Id) && (x.Id != field.Id)) { ccma.UI.RelatedFields[x.Id].push(field.Id); }
        });
    }
}
function saveLookup(elem) {
    if ($(elem).hasClass('classCalcAttributes') && $(elem).attr('calcField')) {
    } else {
        selectOwnValue(elem, $(elem).parent().attr("fieldid"), $(elem).attr('auxrowuid'), null);
    }
}
function initLookupFields(callback) {
    var lc = $('[lookuptable="$QUERY$"]').length;
    var pc = 0;
    $('.cc-drop.cusDpdownSpan').each((i, e) => {
        let ptd = $(e).parent('td')[0], allowTextInput = false;
        if (ptd) {
            let fd = datafields[$(ptd).attr('fieldid')];
            if (fd?.AllowTextInput) allowTextInput = true;
        }
        $(e).lookup({ popupView: true, searchActive: true, searchActiveAbove: 8, searchAutoFocus: false, onSave: saveLookup, customText: allowTextInput }, []);
    })
    //   $('span.cusDpdownSpan').siblings('.cusDpdown-arrow').css('display', 'inline-block');  //to show arrow for lookup field, if any issue in field settings then will not call setfieldvalue and arrow will not show.
    if (lc == 0) {
        if (callback) callback();
        return;
    }
    getAuxiliaryTableNames(function (tableList) {
        var _includetableList = [];
        tableList.forEach(function (_tblname) {
            if ((datafieldsArray.filter(function (x) { return x.LookupQuery && x.LookupQuery.length && x.LookupQuery.toUpperCase().search(_tblname.toUpperCase()) > -1 }).length > 0) && (_includetableList.indexOf(_tblname) == -1))
                _includetableList.push(_tblname);
        });
        create_includetableList(_includetableList, function () {
            $('[lookuptable="$QUERY$"]').each(function (i, x) {
                var ff = x;
                var fieldid = $(x).parents('[fieldid]').attr('fieldid');
                var f = datafields[fieldid];
                if (!f) { pc += 1; return; }
                var results1 = 0;
                tableList.forEach(function (tableLists) {
                    var LookupQuery = f.LookupQuery;
                    if (LookupQuery != null) {
                        var results = LookupQuery.includes(" " + tableLists + " ")
                        if (results)
                            results1 = 1;
                    }
                });
                if (results1 == 1) {
                    pc += 1;
                    return;
                }
                getLookupData(f, {}, function (d) {
                    d.forEach(function (x, index) {
                        if (x.Id) d[index].Id = encodeURI(x.Id);
                    });
                    if (f && f.InputType == '5') {
                        // $(ff).siblings('.cusDpdown-arrow').css('display', 'inline-block'); 
                        $(ff)[0]?.getLookup?.setData(d)
                    }
                    else {
                        $(ff).html('<option value="${Id}">${Name}</option>');
                        $(ff).fillTemplate(d);
                    }
                    pc += 1;
                    if (pc == lc) {
                        if (callback) callback();
                    }
                });
                if (pc == lc) {
                    if (callback) callback();
                }
            });
        });
    });
    encodeLookupTableValues();
}

function showDataCollection(id) {
    if (event) {
        event.stopPropagation();
    }
    hideCategoryMenu();
    if (id == undefined) {
        id = 0;
    }
    var cat = getCategory(id);
    if (cat == undefined || !cat) return;
    var parentCategory = findParent(id);
    $('.parcel-data-title').html('Parcel Data');
    if (parentCategory)
        openCategory(parentCategory, $('.category-menu a[category="' + parentCategory + '"]').html());
    else
        openCategory(id, $('.category-menu a[category="' + parentCategory + '"]').html());
    $('.tabs li').removeAttr('class');
    $('.tabs li[content="parceldata"]').addClass('selected');
    $('.category-menu').show();
    return false;
}

function showPhotos() {
    openCategory('photo-viewer', this.innerHTML);
    $('.photo-Manager').show();
    $('.photo-frame-allPhotos').show();
    $('.parcel-data-title').html('Parcel Photos');
    showAllPhotos();
    $('.category-menu').hide();
    return false;
}
function showSketches() {
    openCategory('sketch-manager', this.innerHTML);
    $('.parcel-data-title').html('Sketch');
    $('.tabs li').removeAttr('class');
    $('.tabs li a[category="sketch-manager"]').parent().addClass('selected');
    openSketchEditor();
    $('.category-menu').hide();
    return false;
}

function prtSketch() {
    //PrintsketchView(function(){if(printWindow && !printWindow.closed) printWindow.print(); 
    //						   else setTimeout( 
    Popup($('.all-sketch-frame').html(), null, "Sketch")
    //,1500)});
}
function prtPRC(elem, title) {
    let elHtml = $(elem).html();
    if (title == 'DigitalPRC' && $(elem).find('.select-sketch-segments-prc')[0] && $(elem).find('.select-sketch-segments-prc').siblings('img')[0] && ($('.prcselect').val() && ($('.prcselect').val() == 'Current' || $('.prcselect').val() == ""))) {
        let imgSrc = $(elem).find('.select-sketch-segments-prc').siblings('img')[0].src;
        $(elem).find('.select-sketch-segments-prc').siblings('img')[0].src = activeParcel.newSketchPreview;
        $(elem).find('.select-sketch-segments-prc').hide();
        setTimeout(function () { //first time image is not showing so added a settimeout to load image correctly.
            elHtml = $(elem).html();
            $(elem).find('.select-sketch-segments-prc').siblings('img')[0].src = imgSrc;
            $(elem).find('.select-sketch-segments-prc').show();
            Popup(elHtml, null, title);
        }, 200);
    }
    else
        Popup(elHtml, null, title);
}
//prcbackup
function uploadPrcHtml(callback) {
    refreshDigitalPrc();
    var htmlContent = $('.digital-prc').html();
    $qc('uploadhtmlfile', { htmlContent: htmlContent, parcelId: activeParcel.Id }, function (res) {
        console.log('Success');
        if (callback) callback();
    }, (ex) => {
        console.log(ex.message);
        if (callback) callback();
    });
}



function downloadPrcHtml(prcId, callback) {
    showMask();
    $.ajax({
        url: '/quality/downloadhtmlfile.jrq',
        dataType: 'html',
        data: { PrcId: prcId },
        success: function (response) {
            $('.digital-prc').html(response);
            $("button[onclick='utils.MarkAsComplete()']").hide();
            $('*[onclick^="showDcTab"]').removeAttr("onclick");
            hideMask();
            if (callback) callback();
        },
        error: function (xhr, status, error) {
            console.log(error);
            hideMask();
            if (callback) callback();
        }
    });
}

function displayPrc(prcBackup) {
    
    prcBackup.sort((a, b) => new Date(b.UploadTime) - new Date(a.UploadTime));
    let uhtml = "<option value=''>Current</option>";
    prcBackup.forEach((u) => {
        if (u.Path.contains("beforeMAC"))
        {
            uhtml += "<option value=" + u.Id + " style = 'color: red;'>" + u.UploadTime + "</option>";
         }
        else {
            uhtml += "<option value=" + u.Id + ">" + u.UploadTime + "</option>";
        }
    });
    $('.prcselect').empty().append(uhtml);
}


function Popup(data, dataUrl, title) {
    //var title = title + '-' + activeParcel.NeighborhoodName + '-' + activeParcel.KeyValue1;

    //To get current date

    const dateObj = new Date();

    var month = dateObj.getMonth() + 1;
    var strmonth = month.toString();
    if (strmonth.length == 1) {
        month = '0' + month;
    }

    var day = dateObj.getDate();
    var strday = day.toString();
    if (strday.length == 1) {
        day = '0' + day;
    }

    const year = dateObj.getFullYear();
    const currentDate = month + '-' + day + '-' + year;
    //To get current date
    if (dataUrl != "ReportView") {
        var title = '';
        if (activeParcel.ShowAlternateField == 1) {
            title = activeParcel.Alternatekeyfieldvalue + '_' + currentDate;
        }
        else if (activeParcel.ShowAlternateField == 0) {
            title = activeParcel.KeyValue1 + '_' + currentDate;
        }
    }
    if ((printWindow == null) || (printWindow.closed)) {
        printWindow = window.open('', '', 'height=700px,width=1200px');
        printWindow.document.write('<html><head><title>' + title + '</title>');
        printWindow.document.write('<style media="print">table thead { display: table-row-group } .EditedValue{color:red;}</style>');
        printWindow.document.write('<style>.estimate-chart-table {text-align:center;} </style>');
        printWindow.document.write('<style> body {font-family : Segoe UI, "sans-serif", Times, serif;} </style>');
        printWindow.document.write('</head><body >');
        printWindow.document.write(data);
        printWindow.document.write('<img style="width: 100%;page-break-after:always;" src="' + dataUrl + '">');
        printWindow.document.write('<script>window.print(); window.onafterprint = function() { window.close(); };</script>');
        printWindow.document.write('</body></html>');
        printWindow.document.close();
        printWindow.focus();
        //printWindow.print();
        //printWindow.close();
    }
    else {
        printWindow.print();
    }
    return true;
}

function highlightwithFilter(categoryId) {
    var clsFCE = 'field-category-edited';
    var childCategories = getChildCategories(categoryId);
    var sourceTable = $('.category-page[categoryid="' + categoryId + '"]').attr('auxdata');
    if (!sourceTable || sourceTable == '' || childCategories.length == 0) {
        if (childCategories.length == 0 && getParentCategoryIds(categoryId).length > 1) {
            var childOfChild = true;
            var subPar = getParentCategories(categoryId)
            var par = getParentCategories(subPar)
            var index = $('.category-page[categoryid="' + par + '"] span.aux-index').attr('findex');
            highLightChildOfChild(par, subPar, index, childOfChild)
        }
        return;
    }
    var pindex = $('.category-page[categoryid="' + categoryId + '"] span.aux-index').attr('findex');
    if ((activeParcel[sourceTable] && activeParcel[sourceTable].length == 0) || !activeParcel[sourceTable]?.[pindex])
        return;
    var auxROWuid = eval('activeParcel.' + sourceTable + '[' + pindex + '].ROWUID');
    for (index in childCategories) {
        var childName = $('.category-page[categoryid="' + childCategories[index] + '"]').attr('auxdata');
        childCatId = childCategories[index];
        var currentCategoryChanges = activeParcel.ParcelChanges.filter(function (a) { return a.CategoryId == childCatId });
        if (currentCategoryChanges && currentCategoryChanges.length > 0) {
            for (x in currentCategoryChanges) {
                var ch = currentCategoryChanges[x];
                var source = activeParcel[ch.SourceTable]
                var categorySameTable = null;
                if (source) {
                    var PAuxRowId = null;
                    var curCat = source.filter(function (row) { return row.ROWUID == ch.AuxROWUID })
                    PAuxRowId = (curCat.length > 0 && curCat[0].parentRecord) ? curCat[0].parentRecord.ROWUID : null
                }
                // categorySameTable = (datafields[activeParcel.ParcelChanges[0].FieldId].CategoryId == childCatId && getSourceTable(childCatId) == getSourceTable(categoryId)) ? true : false;
                categorySameTable = (datafields[ch.FieldId] && datafields[ch.FieldId].CategoryId == childCatId && getSourceTable(childCatId) == getSourceTable(categoryId)) ? true : false;
                if ((ch.ParentAuxROWUID && ch.ParentAuxROWUID != ch.AuxROWUID) || PAuxRowId || categorySameTable) {
                    if ((ch.Action == "new" || ch.Action == "edit" || ch.Action == "delete" || ch.Action == "futurecopy") && ch.CategoryId == childCatId && ((datafields[ch.FieldId] && datafields[ch.FieldId].ReadOnly != true && datafields[ch.FieldId].DoNotShow != true && (getCategory(ch.CategoryId) && getCategory(ch.CategoryId).IsReadOnly != true)) || (datafields[ch.FieldId] && (ch.Action == "delete" && datafields[ch.FieldId].DoNotShow != true))) && ch.SourceTable == childName && (ch.ParentAuxROWUID == auxROWuid || PAuxRowId == auxROWuid || categorySameTable)) { //added condition to check the field is hidden, if hidden then will not consider the change, modified delete case , if delete and field is not hidden then change will show even the field category is readonly.
                        var cls1 = $('.childCategoryTitle[category=' + childCatId + ']');
                        cls1.addClass(clsFCE);
                        if (ch.QCChecked && activeParcel.QC)
                            cls1.addClass('approved');
                        else
                            cls1.removeClass('approved');
                        break;
                    }
                    else {
                        //childCategory = getCategoryFromSourceTable(sourceTable);
                        //if(childCategory == childCatId)
                        //if (ch.CategoryId == childCatId) {
                        var cls1 = '.childCategoryTitle[category=' + childCatId + ']';
                        $(cls1).removeClass(clsFCE);
                        $(cls1).removeClass('approved');
                        var cls0 = 'div.sublevelDiv  a[category="' + childCatId + '"]';
                        $(cls0).removeClass(clsFCE);
                        $(cls0).removeClass('approved');
                        //}

                    }
                }

            }
        }
        if (getChildCategories(childCatId).length != 0) {
            highLightChildOfChild(categoryId, childCatId, pindex)
        }
    }
}

function highLightChildOfChild(categoryId, childCatId, pindex, childOfChild) {
    var clsFCE = 'field-category-edited';
    var childCategories = getChildCategories(childCatId)
    var sourceTable = $('.category-page[categoryid="' + categoryId + '"]').attr('auxdata');
    var childName = $('.category-page[categoryid="' + childCatId + '"]').attr('auxdata');
    if (!sourceTable || sourceTable == '' || childCategories.length == 0 || !childName || childName == '') return;
    if (activeParcel[sourceTable] && activeParcel[sourceTable].length == 0 || (activeParcel[sourceTable][pindex] && activeParcel[sourceTable][pindex][childName] && activeParcel[sourceTable][pindex][childName].length == 0))
        return;

    if (childOfChild && activeParcel[sourceTable] && activeParcel[sourceTable][pindex] && activeParcel[sourceTable][pindex][childName]) {
        var parentChildFindx = $('.category-page[categoryid="' + childCatId + '"] span.aux-index').attr('findex');
        if (!activeParcel[sourceTable][pindex][childName][parentChildFindx])
            return;
        var auxROWuid = activeParcel[sourceTable][pindex][childName][parentChildFindx].ROWUID;  //eval('activeParcel.' + childName + '[' + parentChildFindx + '].ROWUID')
        var cls0 = '.parentCategoryTitle[category="' + childCatId + '"]';
        $(cls0).removeClass(clsFCE);
        $(cls0).removeClass('approved');
        var currentCategoryChanges = activeParcel.ParcelChanges.filter(function (a) { return a.CategoryId == childCatId });

        if (currentCategoryChanges && currentCategoryChanges.length > 0) {
            for (x in currentCategoryChanges) {
                var ch = currentCategoryChanges[x];
                var source = activeParcel[ch.SourceTable]
                var categorySameTable = null;
                if (source) {
                    var PAuxRowId = null;
                    var curCat = source.filter(function (row) { return row.ROWUID == ch.AuxROWUID })
                    PAuxRowId = (curCat.length > 0 && curCat[0].parentRecord) ? curCat[0].parentRecord.ROWUID : null
                }
                // categorySameTable = (datafields[activeParcel.ParcelChanges[0].FieldId].CategoryId == childCatId && getSourceTable(childCatId) == getSourceTable(categoryId)) ? true : false;
                categorySameTable = (datafields[activeParcel.ParcelChanges[0].FieldId] && datafields[activeParcel.ParcelChanges[0].FieldId].CategoryId == childCatId && getSourceTable(childCatId) == getSourceTable(categoryId)) ? true : false;
                if ((ch.ParentAuxROWUID && ch.ParentAuxROWUID != ch.AuxROWUID) || PAuxRowId || categorySameTable) {
                    if ((ch.Action == "new" || ch.Action == "edit" || ch.Action == "delete" || ch.Action == "futurecopy") && ch.SourceTable == childName && (ch.AuxROWUID == auxROWuid || PAuxRowId == auxROWuid) && (datafields[ch.FieldId] && datafields[ch.FieldId].ReadOnly != true && (getCategory(ch.CategoryId) && getCategory(ch.CategoryId).IsReadOnly != true) && datafields[ch.FieldId].DoNotShow != true)) {
                        cls0 = '.parentCategoryTitle[category="' + childCatId + '"]';
                        $(cls0).addClass(clsFCE);
                        if (ch.QCChecked && activeParcel.QC)
                            $(cls0).addClass('approved');
                        else
                            $(cls0).removeClass('approved');
                        // break;
                    }
                }
            }
        }
    }

    childCategories.forEach(function (cat) {
        var childOfChildName = $('.category-page[categoryid="' + cat + '"]').attr('auxdata');
        if (!childOfChildName || childOfChildName == '') return;

        if (activeParcel[sourceTable][pindex] && activeParcel[sourceTable][pindex][childName]) {
            var catEl = activeParcel[sourceTable][pindex][childName];
            for (i in catEl) {
                if (!activeParcel[sourceTable][pindex][childName][i])
                    return;
                var auxROWuid = eval('activeParcel.' + sourceTable + '[' + pindex + ']' + '.' + childName + '[' + i + '].ROWUID')
                var currentChildChanges = activeParcel.ParcelChanges.filter(function (a) { return a.CategoryId == cat });
                if (currentChildChanges && currentChildChanges.length > 0) {
                    for (x in activeParcel.ParcelChanges) {
                        var ch = activeParcel.ParcelChanges[x];
                        var source = activeParcel[ch.SourceTable]
                        var categorySameTable = null;
                        if (source) {
                            var PAuxRowId = null;
                            var curCat = source.filter(function (row) { return row.ROWUID == ch.AuxROWUID })
                            PAuxRowId = (curCat.length > 0 && curCat[0].parentRecord) ? curCat[0].parentRecord.ROWUID : null
                        }
                        //categorySameTable = (datafields[activeParcel.ParcelChanges[0].FieldId].CategoryId == childCatId && getSourceTable(childCatId) == getSourceTable(categoryId)) ? true : false;
                        categorySameTable = (datafields[activeParcel.ParcelChanges[0].FieldId] && datafields[activeParcel.ParcelChanges[0].FieldId].CategoryId == childCatId && getSourceTable(childCatId) == getSourceTable(categoryId)) ? true : false;
                        if ((ch.ParentAuxROWUID && ch.ParentAuxROWUID != ch.AuxROWUID) || PAuxRowId || categorySameTable) {
                            if ((ch.Action == "new" || ch.Action == "edit" || ch.Action == "delete" || ch.Action == "futurecopy") && ch.SourceTable == childOfChildName && (ch.ParentAuxROWUID == auxROWuid || PAuxRowId == auxROWuid || categorySameTable)) {
                                if (datafields[ch.FieldId] && datafields[ch.FieldId].ReadOnly != true && (getCategory(ch.CategoryId) && getCategory(ch.CategoryId).IsReadOnly != true) && datafields[ch.FieldId].DoNotShow != true) {
                                    var cls1 = $('.childCategoryTitle[category=' + childCatId + ']');
                                    cls1.addClass(clsFCE);
                                    cls0 = '.parentCategoryTitle[category="' + childCatId + '"]';
                                    $(cls0).addClass(clsFCE);
                                    if (ch.QCChecked && activeParcel.QC) {
                                        $(cls1).addClass('approved');
                                        $(cls0).addClass('approved');
                                    }
                                    else {
                                        $(cls1).removeClass('approved');
                                        $(cls0).removeClass('approved');
                                    }
                                    // break;
                                }
                            }
                            else {
                                //childCategory = getCategoryFromSourceTable(sourceTable);
                                //if(childCategory == childCatId)
                                if (ch.CategoryId == cat && ch.ParentAuxROWUID == auxROWuid) {
                                    var cls1 = '.childCategoryTitle[category=' + childCatId + ']';
                                    $(cls1).removeClass(clsFCE);
                                    $(cls1).removeClass('approved');;
                                    cls0 = '.parentCategoryTitle[category="' + childCatId + '"]';
                                    $(cls0).removeClass(clsFCE);
                                    $(cls0).removeClass('approved');
                                }

                            }
                        }
                    }
                }
            }
        }
    });
}

function ToMoveLeft() {
    var fixed = false;
    if ($("#lbColumnsRight option").length == 1) {
        alert('Atleast one field should be in Parcel Search Results.');
    }
    else {
        if ($("#lbColumnsRight option:selected").length <= 0) return;
        ItemValue = $("#lbColumnsRight option:selected").text();
        if (offlineTables.GridColumns.filter(function (x) { return x.Name == ItemValue }).length > 0) {
            fixed = true;
        }
        else {
            $("#lbColumnsLeft").append("<option value='" + ItemValue + "'>" + ItemValue + "</option>");
        }
        if (fixed == true) {
            alert('The ' + ItemValue + ' field cannot be removed in Parcel Search Results.');
        }
        else {
            $("#lbColumnsRight option:selected").remove();
        }
    }
}
function ToMoveRight() {
    if ($("#lbColumnsRight option").length > 19) {
        alert('We cannot add more than 20 fields in Parcel Search Results.');
    }
    else {
        $("#lbColumnsLeft option:selected").each(function () {
            ItemValue = $(this).val();
            $("#lbColumnsRight").append("<option value='" + ItemValue + "'>" + ItemValue + "</option>");
        });
        $("#lbColumnsLeft option:selected").remove()
    }
}
//function ToMoveAllLeft() {
//    alert('We can not move all values ');
//            //  $("#lbColumnsRight option").each(function () {
//            //    ItemValue = $(this).val();            
//            //        $("#lbColumnsLeft").append("<option value='" + ItemValue + "'>" + ItemValue + "</option>");       
//            //});          
//            //  $("#lbColumnsRight option").remove()
//}
//function ToMoveAllRight() {
//            $("#lbColumnsLeft option").each(function () {
//                ItemValue = $(this).val();
//                $("#lbColumnsRight").append("<option value='" + ItemValue + "'>" + ItemValue + "</option>");
//            });
//            $("#lbColumnsLeft option").remove()    
//}
function hidePopup() {
    $('.columns').dialog('close');

}
function getCategoryFromSourceTable(sourceTable) {
    try {
        var category = getCategory($('.category-page[auxdata="' + sourceTable + '"]').attr('categoryid'));
        return (category ? category : {});
    }
    catch (e) {
        console.log(e.message);
    }
}

function getCategoryFromSourceTableCopy(sourceTable, Id) {
    return (fieldCategories.some(function (d) { return d.SourceTable == sourceTable; })) ? Id ? fieldCategories.filter(function (d) { return d.SourceTable == sourceTable && d.Id == Id; })[0] : fieldCategories.filter(function (d) { return d.SourceTable == sourceTable; })[0] : {};
}

//function initiateQcVisualStats(){
//window.setInterval(function () {
//	qcVisualStats()
//}, 1000 * 60 * 10);
//}

function qcVisualStats() {
    var data = ""
    $qc("qcvisualstats", data, function (resp) {
        if (resp.TotalCount > 0)
            $('.noti_bubble').css("display", "inline-block");
        if (resp.TotalCount > 99)
            $("#total-count").text('99+');
        else
            $("#total-count").text(resp.TotalCount);

        if (resp.SyncFailuresCount > 0)
            $(".sync-failure-count").text(resp.SyncFailuresCount);
        else
            $("#sync-failure").css("display", "none");

        if (resp.CCErrorCount > 0)
            $(".cc-error-count").text(resp.CCErrorCount);
        else
            $("#cc-error").css("display", "none");

        if (resp.PendingChangesMoreThan7DaysCount > 0)
            $(".PendingChanges-MoreThan-7Days-count").text(resp.PendingChangesMoreThan7DaysCount);
        else
            $("#PendingChanges-MoreThan-7Days").css("display", "none");

        $(".field-alert-count").text(resp.FieldAlertCount);
        $(".8Days-count").text(resp.ReviewedMoreThan8Days);
        $(".4_8Days-count").text(resp.Reviewed5_8Days);
        $(".2_4Days-count").text(resp.Reviewed3_4Days);
        $(".1_2Days-count").text(resp.Reviewed1_2Days);
        $('.leftContent_Style').css('margin', '34px 6px');

        setSyncDate('lastDownSync', resp.LastDownSyncDate, resp.LastDownSyncDateColour)
        setSyncDate('lastUpSync', resp.LastUpSyncDate, resp.LastUpSyncDateColour);
        if (clientSettings.DisableLastSyncStatuses && clientSettings.DisableLastSyncStatuses == '1') {
            $('.lastDownSync').hide();
            $('.lastUpSync').hide();
        }
        // initiateQcVisualStats();
    })
}

function setSyncDate(syncType, syncDate, colourFormat) {
    if (syncDate != "") {
        $('.' + syncType + 'Date').html(syncDate);
        $('.' + syncType + '').css('display', 'inline-block');
        $('.' + syncType + '').addClass('tip');
        $('.tip').tipr();
        if (colourFormat == 1) {
            $('.' + syncType + 'Date').css({ 'color': 'Red', 'font-weight': 'bold' });
        }
        else {
            $('.' + syncType + 'Date').css({ 'color': 'Black', 'font-weight': 'normal' });
        }
    }
}

function clearFindex() {
    $('.aux-index').removeAttr('findex');
}

function getDateDiif(difference) {
    var d = new Date();
    d.setDate(d.getDate() + difference);
    var output = d.getFullYear() + '-' + ((d.getMonth() + 1) < 10 ? '0' : '') + (d.getMonth() + 1) + '-' + (d.getDate() < 10 ? '0' : '') + d.getDate();
    return output;
}

function qcVisualStatsClick(link) {
    var tplMACFilter = [];
    $('.templateName').hide();
    refreshSearch();
    switch (link.id) {
        case "PendingChanges-MoreThan-7Days":
            tplMACFilter = [
                new Filter("Pending Changes Without MAC 7+ Days", "~PendingChangesMoreThan7Days", "eq", "", null)
            ];
            break;
        case "field-alert":
            tplMACFilter = [
                new Filter("MAC With Field Alert Or Field Alert Type Is Not Null", "~MACFieldAlertOrFieldAlertTypeISNOTNULL", "eq", "", null)
            ];
            break;
        case "8-Days":
            tplMACFilter = [
                new Filter("Pending Changes Marked As Complete", "~ChangesPendingApproval", "eq", "", null),
                new Filter("Reviewed On", "p.ReviewDate", "lt", getDateDiif(-8), null),
                new Filter("Field Alert Type", "p.FieldAlertType", "eq", "-2", -3),
                new Filter("Alert from Field", "p.FieldAlert", "nl", "undefined", null)
            ];
            break;
        case "5-8Days":
            tplMACFilter = [
                new Filter("Pending Changes Marked As Complete", "~ChangesPendingApproval", "eq", "", null),
                new Filter("Reviewed On", "p.ReviewDate", "bw", getDateDiif(-8), getDateDiif(-5)),
                new Filter("Field Alert Type", "p.FieldAlertType", "eq", "-2", -3),
                new Filter("Alert from Field", "p.FieldAlert", "nl", "undefined", null)
            ];
            break;
        case "3-4Days":
            tplMACFilter = [
                new Filter("Pending Changes Marked As Complete", "~ChangesPendingApproval", "eq", "", null),
                new Filter("Reviewed On", "p.ReviewDate", "bw", getDateDiif(-4), getDateDiif(-3)),
                new Filter("Field Alert Type", "p.FieldAlertType", "eq", "-2", -3),
                new Filter("Alert from Field", "p.FieldAlert", "nl", "undefined", null)
            ];
            break;
        case "1-2Days":
            tplMACFilter = [
                new Filter("Pending Changes Marked As Complete", "~ChangesPendingApproval", "eq", "", null),
                new Filter("Reviewed On", "p.ReviewDate", "bw", getDateDiif(-2), getDateDiif(-1)),
                new Filter("Field Alert Type", "p.FieldAlertType", "eq", "-2", -3),
                new Filter("Alert from Field", "p.FieldAlert", "nl", "undefined", null)
            ];
            break;
        case "cc-error":
            tplMACFilter = [
                new Filter("CC Error", "~CC_Error", "eq", "", null)
            ];
            break;
    }
    if (link.id == "sync-failure") {
        $('.qc-selected-template option[value=SYNCFAIL]').attr('selected', 'selected');
        showSelectedTemplate(link.id);
    }
    else {
        loadFilters(tplMACFilter)
        $('.qc-tabs').tabs("select", 0);
    }
    removeFilter();
    //doSearch();
}

function redirectedPriorityListParcels(callback) {
    if (RedirectedPriorityListFlag != '') {
        var tplPriorityListParcelsSearch = [];
        $('.templateName').hide();
        // refreshSearch();
        tplPriorityListParcelsSearch = [
            new Filter("Redirected Priority List Parcels", "~RedirectedPriorityListParcels", "eq", RedirectedPriorityListFlag, null)
        ];
        loadFilters(tplPriorityListParcelsSearch)
        $('.qc-tabs').tabs("select", 0);
        //doSearch();
    }
    if (callback) callback();
}

function openAdhocCreator() {
    closeBulkEditor();
    if (hasTaskManagerRole == 'False') {
        alert('You do not have the Appraisal Task Control user role, which is needed in order to create Assignment Groups.')
        return false;
    }

    isAdhocCreator = true;
    $('.adhoc-creator').css({ 'height': '170px' });
    $('.adhoc-creator').show();
    setScreenDimensions()
}

function closeAdhocCreator() {
    $('.chkDoNotReset input').attr('checked', false);
    $('.txt-adhoc-group-name').val('');
    $('.ddlAssignTo').prop('selectedIndex', 0);
    $('.adhoc-creator').css({ 'height': '0px' });
    $('.adhoc-creator').hide();
    setScreenDimensions()
    return false;
}
var previousSketch = false;
function showPreviousSketch(mode) {
    if (mode == '1') {
        var sketches = getSketchSegments(true);
        $('option', '.select-sketch-segments').remove();
        for (x in sketches) {
            var skt = sketches[x];
            $('.select-sketch-segments').append($('<option>', {
                value: skt.uid,
                text: skt.label
            }));
        }
        showSketchesInQC(false, true);
        $('.before-sketch').text('Current View');
        $('.before-sketch').attr('mode', '0')
        previousSketch = true;
    } else {
        var sketches = getSketchSegments(false);
        $('option', '.select-sketch-segments').remove();
        for (x in sketches) {
            var skt = sketches[x];
            $('.select-sketch-segments').append($('<option>', {
                value: skt.uid,
                text: skt.label
            }));
        }
        showSketchesInQC(false, false);
        $('.before-sketch').text('Sketch Before');
        $('.before-sketch').attr('mode', '1')
        previousSketch = false;
    }
}

function encodeLookupTableValues() {
    var LookupFields = Object.keys(datafields).filter(function (k) { return (datafields[k].InputType == '5' && datafields[k].LookupTable != '$QUERY$') }).map(function (field) { return datafields[field].Id });
    LookupFields.forEach(function (eachField) {
        $('.parcel-field-values .value[fieldid="' + eachField + '"] select option').each(function (index, options) {
            var Lvalue = $(options).attr('value');
            if (Lvalue !== null)
                Lvalue = encodeURI(Lvalue);
            $(options).attr('value', Lvalue);
        });
    });
}

function loadCustomWMS() {
    if ($('#custommap').hasClass('hidden')) {
        $('#customwms').val('Hide Imagery');
        $('#custommap').removeClass('hidden');
        $('.googlemap').addClass('hidden');
        OsMap.invalidateSize();
    } else {
        $('#customwms').val('Show Imagery');
        $('#custommap').addClass('hidden');
        $('.googlemap').removeClass('hidden');
        resetMap();
    }
}

function BppUserSettings(callback) {
    ccma.Session.RealDataReadOnly = false;
    if (clientSettings && clientSettings["EnablePersonalProperty"] == '1') {
        $qc('userbppsettings', "", function (data) {
            if (data && data.status == "OK") {
                if (data.usertype == 'BPP') {
                    if (data.RealDataReadOnly) {
                        ccma.Session.RealDataReadOnly = true;
                    }
                    else {
                        ccma.Session.RealDataReadOnly = false;
                    }
                }
                if (callback) callback();
            }
            else {
                if (callback) callback();
            }
        }, function () { if (callback) callback(); });
    }
    else {
        if (callback) callback();
    }
}

function _includeTableNames() {
    _includetableList = [];
    var _tableList = fieldCategories.filter(function (item) { if (item.SourceTable) { return item.SourceTable } }).map(function (item) { return item.SourceTable });
    _tableList.forEach(function (_tblname) {
        if ((datafieldsArray.filter(function (x) { return x.LookupQuery && x.LookupQuery.length && x.LookupQuery.toUpperCase().search(_tblname.toUpperCase()) > -1 }).length > 0) && (_includetableList.indexOf(_tblname) == -1))
            _includetableList.push(_tblname);
    });
}

function create_includetableList(tableList, callback) {
    if (tableList.length == 0) { if (callback) callback(); return; }
    var tbl_name = tableList.pop();
    var columns = [];
    Object.keys(datafields).map(function (dfid) { return datafields[dfid]; }).filter(function (dfs) { if (tbl_name == "Parcel") { return dfs.SourceTable == null } else { return dfs.SourceTable == tbl_name; } }).map(function (dfield) { return dfield.Name; }).forEach(function (cols) {
        var regexp = new RegExp('^' + cols + '$', 'i');
        if (!columns.some(function (c) { return c.search(regexp) > -1 }))
            columns.push(cols);
    });
    ['CC_YearStatus', 'CC_RecordStatus', 'CC_ParcelId', 'CC_Deleted', 'ClientParentROWUID', 'ClientROWUID', 'ParentROWUID', 'ROWUID'].forEach(function (flds) {
        columns.unshift(flds);
    });
    var dropSql = "DROP TABLE IF EXISTS " + tbl_name;
    var createSql = "CREATE TABLE " + tbl_name + "([" + columns.join('] TEXT,[') + "])";
    db.transaction(function (x) {
        executeSql(x, dropSql, [], function (tx) {
            executeSql(x, createSql, [], function (tx) {
                create_includetableList(tableList, callback);
            });
        });
    });
}

function MeasurementScale(div, map) {
    var ms = this;
    globalms = ms;
    this.control = div;
    this.on = false;
    this.start = null;
    this.end = null;

    var mA, mB, line;
    var markIcon = new google.maps.MarkerImage("http://maps.google.com/mapfiles/ms/icons/red-dot.png", new google.maps.Size(27, 28));
    mA = new google.maps.Marker({
        icon: markIcon
    });
    mB = new google.maps.Marker({
        icon: markIcon
    });
    line = new google.maps.Polyline({
        strokeColor: 'Yellow'
    });

    this.clear = function () {
        this.start = null;
        this.end = null;
        this.updateGraphics();
        label.innerHTML = '0.0ft';
    }

    this.updateGraphics = function () {
        mA.setMap(null);
        mB.setMap(null);
        line.setMap(null);

        if (ms.start != null) {
            mA.setPosition(ms.start);
            mA.setMap(map);
        }

        if (ms.end != null) {
            mB.setPosition(ms.end);
            mB.setMap(map);
        }

        if ((ms.start != null) && (ms.end != null)) {
            line.setPath([ms.start, ms.end]);
            line.setMap(map);
            label.innerHTML = Math.round(ms.distance() * 10) / 10 + 'ft';
        }
    }

    div.className = 'measurement-tool measure-off tip';
    $(div).attr({
        "abbr": 'Click on the measuring tool to activate it and measure. Click again to deactivate the tool.',
        "width": '400px;'
    });

    var button = document.createElement('span');
    button.className = 'button';
    div.appendChild(button);

    var label = document.createElement('span');
    label.className = 'distance';
    div.appendChild(label);
    label.innerHTML = '0.0ft'

    google.maps.event.addDomListener(button, 'click', function (e) {
        if (ms.on) {
            map.setOptions({
                draggableCursor: null
            });
            div.className = 'measurement-tool measure-off tip';
            $(div).attr({
                "abbr": 'Click on the measuring tool to activate it and measure. Click again to deactivate the tool.',
                "width": '400px;'
            });
            $('.overlay').removeClass('pointer')
            ms.clear();
        } else {
            map.setOptions({
                draggableCursor: 'crosshair'
            });
            div.className = 'measurement-tool measure-on tip';
            $(div).attr({
                "abbr": 'Click on the measuring tool to activate it and measure. Click again to deactivate the tool.',
                "width": '400px;'
            });
            ms.clear();
            $('.overlay').addClass('pointer')
        }

        ms.on = !ms.on;
        e.preventDefault();
    });

    this.distance = function () {
        if ((ms.start != null) && (ms.end != null)) {
            return google.maps.geometry.spherical.computeDistanceBetween(ms.start, ms.end) * 3.28084;
        } else {
            return 0;
        }
    }
}

function supportViewIconSwitch() {
    let cat = $(".category-page:visible")[0];
    if ($('.parcel__supportView')?.length) {
        let dataWidth = $('.parcel__supportView .parcel-data-category-container')?.width()
        if (dataWidth > 1250) {
            $('.fldPropIcons').show();
            $('.fldPropcheckboxIcons').show();
            $('.fldPropcheckboxHead').show();
            $('.fldPropEmpty').attr('colspan', 3);
            $('.support__iconToggle').hide();
            $('.category-menu').removeClass('category-menu--wide');
        } else {
            if (dataWidth > 950)
                $('.category-menu').addClass('category-menu--wide');
            else
                $('.category-menu').removeClass('category-menu--wide');
            $('.fldPropIcons').show();
            $('.fldPropcheckboxIcons').hide();
            $('.fldPropcheckboxHead').hide();
            $('.fldPropEmpty').attr('colspan', 2);
            $('.support__iconToggle').show();
            if ($('.support__iconToggle-cb', cat)[0]) {
                let iconToggleCheck = $('.support__iconToggle-cb', cat)[0].checked ? true : false;
                $('.support__iconToggle-cb').attr('checked', false);
                if (iconToggleCheck) {
                    $('.support__iconToggle-cb', cat).attr('checked', true);
                    $('.fldPropIcons', cat).hide();
                    $('.fldPropcheckboxIcons', cat).show();
                    $('.fldPropcheckboxHead', cat).show();
                }
            }
            else
                $('.support__iconToggle-cb').attr('checked', false);
        }
    } else {
        $('.fldPropIcons').hide();
        $('.fldPropcheckboxIcons').hide();
        $('.support__iconToggle').hide();
        $('.category-menu').removeClass('category-menu--wide');
    }
}

let MarkerArray = [], CurrentGeoElement, CurrentGeoValue, GeoLocationIcon;

function GeolocationLoad(source) {
    GeoLocationIcon = new google.maps.MarkerImage('/App_Static/images/Geo_location.png', new google.maps.Size(32, 37), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
    let element = $(source).siblings('.input')[0];
    $(element).focusout();
    for (var i = 0; i < MarkerArray.length; i++) {
        MarkerArray[i].setMap(null);
    }

    MarkerArray = []; CurrentGeoElement = element;
    $('.mask').show(); $('.GeoLocationMap').show();
    setGeoLocationScreen($(window).height(), $(window).width());
    google.maps.event.trigger(GeoLocationmap, 'resize');
    let value = $(element).val() ? $(element).val().split(",") : "", Location = null, GeoMarker = null;

    var plotGeoLocation = function () {
        if (Location) {
            MarkerArray.push(GeoMarker);
            GeoMarker.setPosition(Location);
            GeoMarker.setMap(GeoLocationmap);
            GeoLocationmap.setCenter(Location);
        }

        GeoLocationmap.setZoom(13);
        google.maps.event.addListener(GeoLocationmap, 'click', function (event) {
            for (var i = 0; i < MarkerArray.length; i++) { MarkerArray[i].setMap(null); }
            MarkerArray = [];
            var marker = new google.maps.Marker({
                position: event.latLng,
                icon: GeoLocationIcon,
                map: GeoLocationmap
            });

            CurrentGeoValue = event.latLng;
            MarkerArray.push(marker);
        });
        if (Location) CurrentGeoValue = Location;
    }

    if (value.length > 1) {
        Location = new google.maps.LatLng(value[0], value[1]);
        GeoMarker = new google.maps.Marker({ position: Location, icon: GeoLocationIcon, map: GeoLocationmap });
        plotGeoLocation();
    }
    else if (navigator && navigator.geolocation) {
        navigator.geolocation.getCurrentPosition((position) => {
            Location = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            GeoMarker = new google.maps.Marker({ position: Location, icon: GeoLocationIcon, map: GeoLocationmap });
            plotGeoLocation();
        }, () => { plotGeoLocation(); });
    }
    return false;
}

function CloseGeoLocation() {
    $('.mask').hide();
    $('.GeoLocationMap').hide();
}

function SetGeoLocationInField() {
    if (CurrentGeoValue) {
        $(CurrentGeoElement).val(CurrentGeoValue.lat().toFixed(8) + "," + CurrentGeoValue.lng().toFixed(8));
    }
    else
        alert("Select a point to save");
    CurrentGeoValue = null;
    CloseGeoLocation();
    $(CurrentGeoElement).blur();
    $(CurrentGeoElement).show();
}

function setGeoLocationScreen(h, w) {
    var left = (w - (w * 70 / 100)) / 2
    var height = (h - (h * 85 / 100)) / 2
    $('.GeoLocationMap').width(w * 70 / 100);
    $('.GeoLocationMap').height(h * 75 / 100);
    $('.GeoLocationMap').css("top", height);
    $('.GeoLocationMap').css("left", left);
}

function updateForceStatus(sts, cback) {
    if (!sts) {
        if (cback) cback();
        return;
    }
    isDTR = __DTR ? 1 : 0;
    $qc('updateforcestatus', { dtr: isDTR }, (data) => {
        if (cback) cback();
    }, () => { if (cback) cback(); });
}

function auditLock() {
    if ($('.audit-lock').hasClass('audit-lock-lock')) {
        $('.audit-lock').removeClass('audit-lock-lock').addClass('audit-lock-unlock');
        localStorage.setItem('auditLock', '0');
    }
    else {
        $('.audit-lock').removeClass('audit-lock-unlock').addClass('audit-lock-lock');
        localStorage.setItem('auditLock', '1');
    }
}