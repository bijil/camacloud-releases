﻿var sketchEditor;
var sketchApp;

function validate_QC_SketchConfig() {
    var result = Object.keys(clientSettings).filter(function (x) { return x.startsWith("Sketch"); })
    if(result.length == 0)
    	result = Object.keys(sketchSettings).filter(function (x) { return x.startsWith("Sketch"); })
    if (result.length < 1) {
        return false;
    }
    return true;
}
function openSketchEditor(showPrevious) {
    try {
    	if ((ccma.Session.RealDataReadOnly == true && activeParcel.IsRPProperty) || activeParcel.CC_Deleted == true){
    		var msg = 'Sketch Editor is not visible for RP parcels when Real Property Data Read-only enabled for the user';
    		if(activeParcel.CC_Deleted == true)
    			msg = 'This option is available for the selected parcel.';
            alert (msg);
            return false;
        }

        var sketches = []; activeParcel.isInvalidSketchString = false;
        try {
            sketches = getSketchSegments(showPrevious, null, null, true);
        }
        catch (ex) { activeParcel.isInvalidSketchString = true; }

        activeParcel.CCSketchSegments = sketches;
        showPrevious ? activeParcel.CC_beforeSketch = true : activeParcel.CC_beforeSketch = false; 
        sketchEditor = openWindow(sketchEditor, 'sketcheditor', '/protected/sketched/', { resizable: 'true', width: 1200, height: 700 });
    }
    catch (e) {

    }
    return false;
}

function addexclamationmark(el) {
    $('.invalidalerticon-blk', el).remove();
    el.prepend('<div class="invalidalerticon-blk"><span class="invalidalerticon"></span></div>');
}

function getSketchSegments(showPrevious, previousLookup, categoryId, prcView) {
    if ( clientSettings["SketchFormat"] || sketchSettings["SketchFormat"] )
        ccma.Sketching.SketchFormatter = eval( 'CAMACloud.Sketching.Formatters.' + ( clientSettings["SketchFormat"] || sketchSettings["SketchFormat"] ) );
    ccma.Sketching.Config = CAMACloud.Sketching.Configs.GetConfigFromSettings();
    if ( !ccma.Sketching.SketchFormatter )
        if ( ccma.Sketching.Config.formatter )
            ccma.Sketching.SketchFormatter = eval( 'CAMACloud.Sketching.Formatters.' + ccma.Sketching.Config.formatter );
    var sketchedited = false;
    var IsSketchApproved = false;
    var processor = ccma.Sketching.SketchFormatter;
    var config = ccma.Sketching.Config;
    if ( !config ) return;
    if (!processor)
        processor = CAMACloud.Sketching.Formatters.TASketch;
    var isPages = false;
    var sketches = [];
    var _sktLookupArray = ( sketchSettings["sketchLookupQuery"] && sketchSettings["sketchLookupQuery"] != '' )? sketchSettings["sketchLookupQuery"].split('$'): [];
    var sk_count = -1;
    for (var ski in config.sources) {
    	var sk_count = sk_count + 1;
        var sksource = config.sources[ski];
        var keyPrefix = sksource.Key ? (sksource.Key + '--') : '';
        var sketchTable = sksource.SketchSource.Table ? eval('activeParcel.' + sksource.SketchSource.Table) : [activeParcel];
        sketchTable = !showPrevious? (sketchTable && sketchTable.filter ? sketchTable.filter(function (st) { return st.CC_Deleted != true && eval( sksource.SketchSource.sketchSourceFilter || true ); }) : sketchTable): (sketchTable && sketchTable.filter ? sketchTable.filter(function (st) { return eval( sksource.SketchSource.sketchSourceFilter || true ); }) : sketchTable);
        var sketchTableOriginal =  sksource.SketchSource.Table ? ( activeParcel.Original[sksource.SketchSource.Table] ? ( !showPrevious ?  ( activeParcel.Original[sksource.SketchSource.Table].filter(function(st) { return st.CC_Deleted != true &&  eval( sksource.SketchSource.sketchSourceFilter || true ); }) ) : ( activeParcel.Original[sksource.SketchSource.Table].filter(function(st) { return eval( sksource.SketchSource.sketchSourceFilter || true ); }) ) ) : [] ) : [activeParcel.Original];    
        var notesTable = sksource.NotesSource && sksource.NotesSource.Table ? eval('activeParcel.' + sksource.NotesSource.Table) : [];
        notesTable = notesTable && notesTable.filter ? notesTable.filter(function (x) { return x.CC_Deleted != true }) : notesTable;
        var notesTableOriginal = sksource.NotesSource && sksource.NotesSource.Table ? eval('activeParcel.Original.' + sksource.NotesSource.Table) : [];
        if(showPrevious)
            notesTable = notesTableOriginal.filter(function (item) {
                return item.CC_RecordStatus != "I";
            })
        var sketchKeyField = sksource.SketchSource.KeyField || 'ROWUID';
        
        if (sksource.SketchSource.sketchSourceSorting) {
           sketchTable = sketchSorting(sketchTable, sksource.SketchSource.sketchSourceSorting, (sksource.SketchSource.Table? sksource.SketchSource.Table: null));
           var orgSortOrder = sketchTable.map(function(st) { return st.ROWUID });
           var orgmap = orgSortOrder.reduce((r, v, i) => ((r[v] = i), r), {});
           sketchTableOriginal = sketchTableOriginal.sort((a, b) => orgmap[a['ROWUID']] - orgmap[b['ROWUID']]);
       	}
        
        for (var sk_index in sketchTable) {
            var pi = sketchTable[sk_index];
            var orgTable = sketchTableOriginal[sk_index];
            var originalTab = sketchTable[sk_index].Original;
            if(showPrevious && originalTab)
        	pi = originalTab;
            var sketchKey = keyPrefix + pi[sketchKeyField];
            var labelField = sksource.SketchSource.LabelField ? sksource.SketchSource.LabelField.split('/') : [];
            if (sketchSettings && sketchSettings.SketchDDLOptions)
            	labelField = labelField.concat(sketchSettings.SketchDDLOptions.split(','));
            if(showPrevious) {
            	var labelLength = labelField.length;
            	var l;
            	for(l = 0 ; l < labelLength ; l++){
					if(labelField[l].indexOf("parentRecord.") > -1)
						labelField[l] = labelField[l].split("parentRecord.")[1];
            	}
            }
            
            var labelFieldValue = '', labelFieldDelimter = sksource.SketchSource.LabelFieldDelimter? sksource.SketchSource.LabelFieldDelimter: '/';
            for(var k = 0; k < labelField.length; k++){
            	var splitField = labelField[k].split(' %# ');
            	for ( var sf = 0; sf < splitField.length; sf++ ) {
            		var lblField = splitField[sf].trim(), desc = '', bracketlblField = splitField[sf].trim(), bracketExists = false;
	                if (bracketlblField.indexOf('{') > -1) {
            			lblField = bracketlblField.match(/\{.*?\}/g)[0].match(/\{.*?\}/g)[0].replace( '{', '' ).replace( '}', '' );
            			bracketExists = true;
            		}
            		
            		var value = '';

                    if (lblField == 'KeyValue1' || lblField == 'AlternateKey') {
                        if (activeParcel.ShowAlternateField == 1) desc = activeParcel.Alternatekeyfieldvalue;
                        if (!desc || desc == '') desc = eval('activeParcel.KeyValue1');
                    }
                    else {
                        if (lblField.indexOf('parentRecord.') > -1 && !pi.parentRecord) {
                            if (!parent_missing_msg) {
                                parent_missing_msg = true;
                                alert("The sketch is missing its related improvement record. Please contact support with this parcel number and do NOT save the sketch.");
                            }
                        }
                        else
                            value = (eval('pi.' + lblField) || '');
                        var tbl = sksource.SketchSource.Table ? sksource.SketchSource.Table : null;
                        if (lblField.indexOf('parentRecord.') > -1) {
                            lblField = lblField.replace('parentRecord.', '');
                            tbl = parentChild ? (parentChild.filter(function (pc) { return pc.ChildTable == tbl })[0] ? parentChild.filter(function (pc) { return pc.ChildTable == tbl })[0].ParentTable : tbl) : tbl;
                        }
                        var field = getDataField(lblField, tbl);
                        if (field && field.InputType == '5') {
                            desc = evalLookup(lblField, value, null, field);
                            desc = ((desc == '???') || (desc == '') || (desc == null)) ? value : desc;
                        }
                        else
                            desc = value;
                        if (config.isPVDConfig)
                            desc = ((lblField == 'ResidenceType' || lblField == 'CommercialType2') ? (desc == 'None' ? '' : desc) : (lblField == 'YearConstrcuted' ? (desc == '0' || desc == 0 ? '' : desc) : desc));
                    }
                    if (bracketExists) {
                		bracketlblField = bracketlblField.replace('{' + lblField + '}', desc);
                		desc = bracketlblField;
                	}		
	                labelFieldValue = labelFieldValue + desc;
	                if( sf != (splitField.length -1) && desc && desc != '' )
	                	labelFieldValue = labelFieldValue + labelFieldDelimter;
                }
                if(k != (labelField.length -1) && labelFieldValue[labelFieldValue.length-1] != labelFieldDelimter)
                	labelFieldValue = labelFieldValue + labelFieldDelimter;
            }
            var sketch = {
                uid: sketchKey.toString(),
                sid: pi[sketchKeyField],
                label: (sksource.SketchLabelPrefix ? (sksource.SketchLabelPrefix + ' ') : '') + (labelFieldValue) + (eval('pi.' + sksource.SketchSource.KeyFields[0]) ? (' [' + eval('pi.' + sksource.SketchSource.KeyFields[0]) + ']') : ''),
                sketches: [],
                notes: [],
                config: sksource,
                parentRow: sksource.SketchSource.Table ? pi : null
            }
            
            if(sksource.GroupingField && sksource.GroupingField == 'ParcelId')
                	sketch.GroupingValue = activeParcel.Id;
            else if(sksource.GroupingField && sksource.DGroupingField)
                sketch.GroupingValue = pi[sksource.GroupingField] || pi['ROWUID'];
            else if(sksource.GroupingField && sksource.OGroupingField)
                sketch.GroupingValue = pi[sksource.GroupingField] || pi['ParentROWUID'];
            else if ( sksource.GroupingField && sksource.SketchSource.Table )
                sketch.GroupingValue = pi[sksource.GroupingField] || pi['ParentROWUID'];

            let sectParentTable = null;
            if(config.ShowSectNum){
                	var isBristol = (config.isBristolTable ? true : false);
        			var sectTables = {}, isShowSectNum = false;
        			if (isBristol)
            			sectTables = { Residential: 'CCV_CONSTR_RESDEP', Commercial: 'CCV_CONSTR_COMDEP', CondoMain: 'CCV_CONSTR_CDMDEP', CondoUnit: 'CCV_CONSTR_CDUDEP' }
        			else
            			sectTables = { Residential: 'CCV_CONSTRSECTION_RES', Commercial: 'CCV_CONSTRSECTION_COM', CondoMain: 'CCV_CONSTRSECTION_CDM', CondoUnit: 'CCV_CONSTRSECTION_CDU' }
            		if(!showPrevious){	
                        if (pi[sectTables.Residential] && pi[sectTables.Residential].filter(function (res) { return res.CC_Deleted != true }).length > 0){
            				if(pi[sectTables.Residential].filter(function (res) { return res.CC_Deleted != true }).length > 1)
                                isShowSectNum = true;
                            sectParentTable = sectTables.Residential;
            			}
                        else if (pi[sectTables.Commercial] && pi[sectTables.Commercial].filter(function (comm) { return comm.CC_Deleted != true }).length > 0){
            				if (pi[sectTables.Commercial].filter(function (comm) { return comm.CC_Deleted != true }).length > 1)
                                isShowSectNum = true;
                            sectParentTable = sectTables.Commercial;
            			}
                        else if (pi[sectTables.CondoMain] && pi[sectTables.CondoMain].filter(function (comn) { return comn.CC_Deleted != true }).length > 0){
            				if (pi[sectTables.CondoMain].filter(function (comn) { return comn.CC_Deleted != true }).length > 1)
                                isShowSectNum = true;
                            sectParentTable = sectTables.CondoMain;
            			}
                        else if (pi[sectTables.CondoUnit] && pi[sectTables.CondoUnit].filter(function (conut) { return conut.CC_Deleted != true }).length > 0){
            				if (pi[sectTables.CondoUnit].filter(function (conut) { return conut.CC_Deleted != true }).length > 1)
                                isShowSectNum = true;
                            sectParentTable = sectTables.CondoUnit;
            			}
            		}
            		if(isShowSectNum)
            			sketch.isShowSectNum = true;
            }
            
            var query = sketchSettings["sketchLookupQuery"]? _sktLookupArray[sk_count]: ( sksource.LookUpQuery? sksource.LookUpQuery: '');
            if ( query && query != '' ) {
				var testField;
                if ( query.search( /\{.*?\}/g ) > -1 ){
                    query.match( /\{.*?\}/g ).forEach( function ( fn ) {
                        var testFieldName = fn.replace( '{', '' ).replace( '}', '' );
                        if ( testFieldName.contains('parcel.') )
                        	testField = activeParcel[testFieldName]? activeParcel[testFieldName]: null;
                        else if ( testFieldName.contains('parent.parent.') )
                        	testField = ( pi['parentRecord']? ( pi['parentRecord']['parentRecord']? pi['parentRecord']['parentRecord'][testFieldName]: null ): null );  
						else if ( testFieldName.contains('parent.') )
							testField = ( pi['parentRecord']? pi['parentRecord'][testFieldName]: null);
						else
							testField = pi[testFieldName];
                        query = query.replace( fn, pi[testFieldName] )
                    } );
                }
                sketch.lookUp = [];
				if (previousLookup && previousLookup.length > 0) { 
					var prelk = previousLookup.filter(function(x){return x.sid == pi[sketchKeyField]})[0]? previousLookup.filter(function(x){return x.sid == pi[sketchKeyField]})[0].lk: [];
			    	sketch.lookUp = prelk;
			   	}
				else{
                	getData( query, [], function ( ld, skdata ) {
                		var lookup = {};
                    	if (ld.length > 0) {
                            var props = Object.keys(ld[0]);
                            var IdField = props[0]; var NameField = props[1]; var DescField = props[1]
                            if (!NameField) NameField = IdField;
                            if (!DescField) DescField = NameField;
                            for (var x in ld) {
                                var d = ld[x];
                                var v = d[IdField];
                                if(v && typeof(v) == "string") v = v.replace('&gt;','>').replace('&lt;','<');
                                if (lookup[v] === undefined)
                                    lookup[v] = { Name: d[NameField], Description: d[DescField], Ordinal: x, SortType: null, Id: v, Object: d };
                            }

                            skdata.lookUp = lookup;
                        }
                        else
                    		skdata.lookUp = ld;
                	}, sketch )
				}
            }
            
            for (var vs in sksource.VectorSource) {
                var vectorSource = sksource.VectorSource[vs];
                var sketchVectorTable = vectorSource.Table ? eval('activeParcel.' + vectorSource.Table) : [activeParcel];
                let sketchVectorTableAll = sketchVectorTable;// index is wrong if deleted record is exists.Readonly property checking based on index of all vector table record.
                sketchVectorTable = !showPrevious ? (sketchVectorTable && sketchVectorTable.filter ? sketchVectorTable.filter(function (x) { return x.CC_Deleted != true }) : sketchVectorTable): sketchVectorTable;
                //sketchVectorTable = sketchVectorTable && sketchVectorTable.filter ? sketchVectorTable.filter(function (x) { return x.CC_Deleted != true }) : sketchVectorTable;
                var sketchVectorTableOrginal = vectorSource.Table ? eval('activeParcel.Original.' + vectorSource.Table) : [activeParcel.Original];
                var sketchVectorOrig= sketchVectorTable ? sketchVectorTable.Original:[];

                var sketchVectorKeyField = vectorSource.KeyField || 'ROWUID';
                var vectorCategory = getCategoryFromSourceTable(vectorSource.Table);
                vectorCategory = (Object.keys(vectorCategory).length == 0)? null: vectorCategory;
                var doNotAllowDeleteFirstVector=null, maxVectorLimit=null;
                if(vectorCategory){
                	doNotAllowDeleteFirstVector = vectorCategory.DoNotAllowDeleteFirstRecord;                	
                	maxVectorLimit = vectorCategory.MaxRecords                	
                }
                doNotAllowDeleteFirstVector = (doNotAllowDeleteFirstVector == null || doNotAllowDeleteFirstVector == 'false' || doNotAllowDeleteFirstVector == false ? false : true);
                maxVectorLimit = (maxVectorLimit == null  ? 0 : parseInt(maxVectorLimit));
                var sketchChanges = activeParcel.ParcelChanges.filter(function(ch){ if(ch.FieldName == vectorSource.CommandField || ch.FieldName == vectorSource.LabelField) return ch })
                var approvedSketchChanges = activeParcel.ParcelChanges.filter(function(ch){ if((ch.QCChecked) && (ch.FieldName == vectorSource.CommandField || ch.FieldName == vectorSource.LabelField)) return ch })
			    if(sketchChanges && approvedSketchChanges && sketchChanges.length > 0 && (sketchChanges.length == approvedSketchChanges.length))
				 	IsSketchApproved = true;
			    var labelRequiredTrue = vectorSource.IsLabelRequired == undefined ? true : vectorSource.IsLabelRequired;
			  
                if (sksource.SketchSource.Table == vectorSource.Table) {
                    var fullVector = eval('pi.' + vectorSource.CommandField);
                    var fullVectorOrg = orgTable ? eval('orgTable.' + vectorSource.CommandField) : null;
                    if (!processor) {
                        processor = CAMACloud.Sketching.Formatters.Sigma;
                    }
                    if(config.encoder){
                        var encodeName = config.encoder; 
                        fullVector = (sketchApp && sketchApp.encoder) ? sketchApp.encoder.decode(fullVector) : CAMACloud.Sketching.Encoders[encodeName].decode(fullVector);
                        fullVectorOrg = (sketchApp && sketchApp.encoder) ? sketchApp.encoder.decode(fullVectorOrg) : CAMACloud.Sketching.Encoders[encodeName].decode(fullVectorOrg);
                    }  

                    let customParameters = {};

                    if (config.isHelionRS) {
                        customParameters.isHelionRS = true;
                    }

                    try {
                        var parts = processor.getParts ? processor.getParts(fullVector, config.doNotDecodeEncode, { sectParentTable: sectParentTable, skRecord: pi }, customParameters): [];
                        var orgParts = fullVectorOrg ? processor.getParts(fullVectorOrg, config.doNotDecodeEncode, { sectParentTable: sectParentTable, skRecord: pi }, customParameters) : null;
                    }
                    catch (err) {
                        if (prcView) {
                            throw "Sketch cannot be rendered due to the Vector sketch data entered by the user.";
                        }
                        else {
                            if (!_sketchStringThrowError) {
                                _sketchStringThrowError = true;
                                alert(" Invalid sketch string format!");
                            }

                            if (categoryId == 'sketch-manager') { addexclamationmark($('.select-sketch-segments').closest('.parcel-data-head')); } console.log(err);
                            return [];
                        }
                    }
                    var pcount = 0, boundaryScale = 0;
                    var filteredFreeFormTextLabels = processor.getFreeFormTextLabels ? processor.getFreeFormTextLabels(fullVector) : [];
                    var filteredfreeFormLineEntries = processor.getLineEntries ? processor.getLineEntries(fullVector) : [];
                    if(config.IsJsonFormat){
                        var jString = (fullVector && fullVector != '') ? JSON.parse(fullVector) : "";
                      	if(jString == ""){
                       		jString = {
                       			JSON: {
                       				GlobalSettings: {SketchScale: 12, AreaSizeDecimals: 1, AreaSizeSuffix: "sf", DimensionDecimals: 2, DimensionSuffix: "'", DimensionsMatchLineColor: false},
                       				LabelEntries: [],
                       				LineEntries: []
                       			},
                       			DCS: ""
                       		}
                       	}
                       	sketch.jsonString = jString;
                    }
                    var mvpncount = 0;
                    
                    for (var x in parts) {
                        pcount++;
                        var part = parts[x];
                        var orgPart = orgParts ? orgParts[x] : null;
                        var partSplitfirst = null, orgPartSplitfirst = null;
                        
                        if (part.isMVPNote) {
                        	var mData = part.mvpData, isNoteChanged = false;
                        	
                        	if((!orgPart || (orgPart && !orgPart.vector)) && part.vector)
                                isNoteChanged =  true;
                            else if (orgPart && orgPart.vector && part.vector)
                                isNoteChanged = part.vector.replace(/ /g, '') != orgPart.vector.replace(/ /g, '');
                                
                        	for(var nt in part.mvpNotes) {
                        		var note = part.mvpNotes[nt];
                        		mvpncount++;
                            	var n = {
	                            	uid: sketchKey.toString() + "/" + mvpncount,
	                           	 	text: ( note['noteText'] || '' ).replace( /&lt;/g, '' ).replace( /&gt;/g, '' ),
	                            	x: parseFloat( note['xPosition'] || 0 ) * ( 1 ),
	                            	y: parseFloat( note['yPosition'] || 0 ) * ( 1 ),
									fontSize: note['fontSize'],
									mvpData: mData
	                        	}
	                        	sketch.notes.push( n );
                        	}
                        	if (isNoteChanged && !showPrevious)
                            	sketchedited = true;
                        	continue;
                        }

                        if (part.isVisionNote || part.isHelionNote) {
                            part.note.uid = sketchKey.toString() + "/" + part.note.uid;
                            sketch.notes.push(part.note);
                            continue;
                        }
                        
                        if (config.SkipRowuidInChange) {
                        	var vstring = part.vector;
                        	var partsplit = vstring.split('[')[1].split(']')[0].split(/\,/);
                        	if (partsplit[partsplit.length - 1].indexOf('{') > -1 && partsplit[partsplit.length - 1].indexOf('}') > -1)
                            	partSplitfirst = partsplit.pop();
                        	part.vector = vstring.split('[')[0] + '[' + partsplit.toString() + ']' + vstring.split(']')[1];
                        	if (orgPart) {
                        		var ostring = orgPart.vector;
                        		var opartsplit = ostring.split('[')[1].split(']')[0].split(/\,/);
                        		if (opartsplit[opartsplit.length - 1].indexOf('{') > -1 && opartsplit[opartsplit.length - 1].indexOf('}') > -1)
                            		orgPartSplitfirst = opartsplit.pop();
                        		orgPart.vector = ostring.split('[')[0] + '[' + opartsplit.toString() + ']' + ostring.split(']')[1];
                        	}
                        }
                        
                        var vectorDeleted = orgParts && parts ? orgParts.length > parts.length : false;
                        var isChanged = false;
                        
                        if (part.brightModified) {
                        	isChanged = part.brightModified.isModified? true: false;
                        }
                        else {
	                        if (orgPart && orgPart.vector)
	                            isChanged = !vectorDeleted && part.vector.replace(/ /g, '') != orgPart.vector.replace(/ /g, '');
	                        else isChanged = true;
                        }
                        
                        if(config.EnableBoundary)
                            boundaryScale = ( part.boundScale > boundaryScale ) ? part.boundScale: boundaryScale;
                        
                        if (config.isMVP) {
                        	var partFirst = part.vector.split(':')[0], partSecond = part.vector.split(':')[1];
                            var hPartFirst = partFirst.replace(/[\[\]']+/g,'');
                            hPartFirst = hPartFirst.split(",");
                            hPartFirst[41] = part.bbScale;
                            part.vector = '[' + hPartFirst.toString() + ']:' + partSecond; 
                        }
                        
                        if (config.SkipRowuidInChange) {
							if (partSplitfirst) {
								var vstring = part.vector;
                        		var partsplit = vstring.split('[')[1].split(']')[0].split(/\,/);
                        		partsplit.push(partSplitfirst);
                        		part.vector = vstring.split('[')[0] + '[' + partsplit.toString() + ']' + vstring.split(']')[1];
							}
                        	if (orgPartSplitfirst) {
                        		var ostring = orgPart.vector;
                        		var opartsplit = ostring.split('[')[1].split(']')[0].split(/\,/);
                        		opartsplit.push(orgPartSplitfirst);
                        		orgPart.vector = ostring.split('[')[0] + '[' + opartsplit.toString() + ']' + ostring.split(']')[1];
                        	}
                        }
                        
                        var label = [], swapLookupIdName = false, swapName;
                        if(sksource.EnableSwapLookupIdName){
                        	swapLookupIdName = true;
                           	swapName = part.name;
                        }
                        var lkName = ( sketchSettings["sketchLookupQuery"] && sketchSettings["sketchLookupQuery"] != '' && query != '')? null : (clientSettings['SketchLabelLookup'] || sketchSettings['SketchLabelLookup'] || vectorSource.LabelLookup);

                        if (lkName?.contains("/")) {
                            let slashlist = lkName.split("/");
                            for (slt in slashlist) {
                                if (lookup[slashlist[slt]]) {
                                    lkName = slashlist[slt]; break;
                                }
                            }
                        }

                        var fLookUp = ccma.Data.LookupMap[lkName];
	                    var isLookUpQueryField = fLookUp ? fLookUp.LookupTable == '$QUERY$' : false;
                        var labelValue = vectorSource.GetLabelValueFromField ? pi[vectorSource.LabelField] : ( sksource.AssignLookUpNameAsLabel ? part[vectorSource.LabelTarget] : ( (config.ApexEditSketchLabel && sketchSettings['ApexEditSketchLabel'] == '1' && part.code) ? part.code: part.label )  ); // 
                        var useLookUpNameAsValue = ( sksource.AssignLookUpNameAsLabel || (config.ApexEditSketchLabel && sketchSettings['ApexEditSketchLabel'] == '1') ) ? true: false;
                        var labelNameValue = ( useLookUpNameAsValue ) ? part.label : part[vectorSource.LabelTarget];
                        var desc = lkName && lookup[lkName] ? (swapLookupIdName ? lookup[lkName][swapName] : lookup[lkName][labelValue]) : null;
                        var clrLabelValue = sksource.splitByDelimiter && labelValue.indexOf(sksource.splitByDelimiter) > -1? labelValue.split(sksource.splitByDelimiter)[0]: labelValue;
                        var clrdesc = lkName && lookup[lkName] ? (swapLookupIdName ? lookup[lkName][swapName] : lookup[lkName][clrLabelValue]) : null;
                        var lookDesc = ( !sksource.DoNotShowLabelDescription && sketchSettings["DoNotShowLabelDescriptionSketch"] != '1' && clientSettings["DoNotShowLabelDescriptionSketch"] != "1") ? desc : null
                        var labelDetails = {}, BasementFunctionality = false, mvpDisplayLabel = '';
                        labelDetails = part.mvpLabelDetails;
                        if(part.sketchType == 'outBuilding'){
                        	labelValue = labelValue.replace('&lt;','<');
                    		labelValue = labelValue.replace('&gt;','>');
                    		mvpDisplayLabel = part.mvpDisplayLabel;
                    	}
                    	var _hlfs = vectorSource.HideLabelFromShow? vectorSource.HideLabelFromShow: ( (config.ApexEditSketchLabel && sketchSettings['ApexEditSketchLabel'] == '1') ? true: false );
                        label.push( { Field: vectorSource.LabelField, Value: labelValue, Description: lookDesc, IsEdited: isChanged, hiddenFromEdit: vectorSource.HideLabelFromEdit, hiddenFromShow: _hlfs, lookup: lkName, Caption: vectorSource.labelCaption, NameValue: labelNameValue, ShowCurrentValueOnly: vectorSource.ShowCurrentValueOnly, lookUpQuery: sksource.LookUpQuery, splitByDelimiter: sksource.splitByDelimiter, Target: vectorSource.LabelTarget, UseLookUpNameAsValue: useLookUpNameAsValue, IsLargeLookup: sksource.IsLargeLookup, IsRequired: labelRequiredTrue, colorCode: ( clrdesc && clrdesc.color ? clrdesc.color : null ), labelDetails: labelDetails, IsSwapLookupIdName: swapLookupIdName, showLabelDescriptionOnly: isLookUpQueryField, EnableFieldValidation: vectorSource.EnableFieldValidation  } );
                        if ( vectorSource.ExtraLabelFields && ( !config.ApexEditSketchLabel || ( config.ApexEditSketchLabel && sketchSettings['ApexEditSketchLabel'] == '1' ) ) ) {
                            vectorSource.ExtraLabelFields.forEach(function (item) {
                                var value = part[item.Target]
                                if (item.ValueRegx) {
                                    value = value.match(item.ValueRegx)
                                    if (value && value[0]) value = value[0]
                                }
                                let lknm = item.LookUp;
                                if (lknm?.contains("/")) {
                                    let slashlist = lknm.split("/");
                                    for (slt in slashlist) {
                                        if (lookup[slashlist[slt]]) {
                                            lknm = slashlist[slt]; break;
                                        }
                                    }
                                }

                                fLookUp = ccma.Data.LookupMap[lknm]
	                            isLookUpQueryField = fLookUp ? fLookUp.LookupTable == '$QUERY$' : false;
                                label.push({ Field: item.Name, Value: value, lookup: lknm, Target: item.Target, Caption: item.Caption, hiddenFromShow: item.HideLabelFromShow, DoNotShowLabelDescription: item.DoNotShowLabelDescription, IsLargeLookup: item.IsLargeLookup, ValidationRegx: item.ValidationRegx, requiredAny: item.requiredAny, showLabelDescriptionOnly: isLookUpQueryField, EnableFieldValidation: item.EnableFieldValidation })
                            })
                        }
                        if (isChanged)
                            sketchedited = true;
                        var hideArea_value = false;
						if(part.isUnSketchedArea) 
							hideArea_value = true;						
                        var sk = {
                            uid: sketchKey.toString() + "/" + pcount,
                            label: label,
                            labelPosition: part.labelPosition,
                            referenceIds: part.referenceIds,
                            isChanged: showPrevious ? false : isChanged,
                            vector: part.vector,
                            otherValues: part.otherValues,
                            mvpData: part.mvpData,
                            AreaLabelPosition: part.AreaLabelPosition,
                            hideAreaValue:hideArea_value,
                            sketchType: part.sketchType,
                            isUnSketchedArea: part.isUnSketchedArea,
                            Disable_CopyVector_OI: part.Disable_CopyVector_OI,
                            LabelDelimiter: sksource.LabelDelimiter === undefined ? '/' : '',
                            vectorConfig: vectorSource,
                            doNotAllowDeleteFirstVector: doNotAllowDeleteFirstVector,
                            maxVectorLimit: maxVectorLimit,
                            BasementFunctionality: part.BasementFunctionality,
                            mvpDisplayLabel: mvpDisplayLabel
                        }

                        var hideSketch = (sksource.HideNullSketches && !sk.vector) || false;
                        if (!hideSketch) sketch.sketches.push(sk);
                    }
                    
                    if(config.EnableBoundary){
                    	sketch.boundaryScale = (boundaryScale != 0 && boundaryScale != '') ? boundaryScale : 80;
                    }
                    
                    if(config.NoteMaxLength) {
	                	sketch.maxNotelen = config.NoteMaxLength;
	                }
                    
                    for (var x in filteredfreeFormLineEntries){
                        var LineEntries = filteredfreeFormLineEntries[x];
                        var label = [], Line_Type = false;
                        if(LineEntries['LinePattern'].contains('Dashed'))
                        	Line_Type = true;
                        var sk = {
                              uid: LineEntries['KeyCode'],
                              label: label,
                              otherValues:[],
                              vector: '[-1,-1]:' +  LineEntries['Path'],
                              referenceIds: '-1,-1',
                              isChanged: false,
                              isFreeFormLineEntries : true,
                              vectorConfig: vectorSource,
                              PageNum: LineEntries['PageNum'],
                              Width: LineEntries['Width'],
                              Color: LineEntries['Color'],
                              LinePattern: LineEntries['LinePattern'],
                              Line_Type: Line_Type,
                              KeyCode: LineEntries['KeyCode'],
                              hideAreaValue :true
                               	
                          }
                         var hideSketch = (sksource.HideNullSketches && !sk.vector) || false;
                         if (!hideSketch) sketch.sketches.push(sk);
                    }
                    for (var x in filteredFreeFormTextLabels){
                        var note = filteredFreeFormTextLabels[x];
                        var notePosition = note["Position"].split(",");
                        var n = {
                            uid: note['KeyCode'],
                           	 text: ( note['Text'] || '' ).replace( /&lt;/g, '' ).replace( /&gt;/g, '' ),
                            x: parseInt( notePosition[0] || 0 ) * ( 1 ),
                            y: parseInt( notePosition[1] || 0 ) * ( 1 ),
                            //lx: parseInt( note[sksource.NotesSource.LineXField] || 0 ) * ( sksource.NotesSource.ScaleFactor || 1 ),
                            //ly: parseInt( note[sksource.NotesSource.LineYField] || 0 ) * ( sksource.NotesSource.ScaleFactor || 1 ),
                            Bold: note['Bold'],
							Color: note['Color'],
							FontFace: note['FontFace'],
							FontSize: note['FontSize'],
							Italic: note['Italic'],
							PageNum: note['PageNum'],
							Rotation: note['Rotation'],
							KeyCode: note['KeyCode']
                        }
                        sketch.notes.push( n );
                    }

                } else {

                    //var filteredDetails = sketchVectorTable.filter(function (ivx) { if (ivx.ParentROWUID) return (ivx.ParentROWUID == pi.ROWUID); else return (eval('ivx.' + sksource.VectorSource.ConnectingFields[0]) == eval('pi.' + sksource.SketchSource.KeyFields[0])); });
                    var filteredDetails = sketchVectorTable && sketchVectorTable.filter( function ( ivx ) { if ( ivx.ParentROWUID ) return ( ivx.ParentROWUID == pi.ROWUID && eval( vectorSource.filter || true ) ); else if ( pi && pi.constructor.name == "Parcel" ) { return ( ivx.CC_ParcelId == pi.Id ); } else return ( eval( 'ivx.' + vectorSource.ConnectingFields[0] ) == eval( 'pi.' + sksource.SketchSource.KeyFields[0] ) && eval( vectorSource.filter || true ) ); } );
					//var filteredDetailsOriginal = sketchVectorTableOrginal.filter(function (ivx) { if (ivx.ParentROWUID) return (ivx.ParentROWUID == pi.ROWUID); else if (pi && pi.constructor.name == "Parcel") { return (ivx.CC_ParcelId == pi.Id); } else return (eval('ivx.' + sksource.VectorSource.ConnectingFields[0]) == eval('pi.' + sksource.SketchSource.KeyFields[0])); });
                    
                    if( config.IsProvalConfig && ( vectorSource.Table == 'sktsegment' || vectorSource.Table == 'ccv_SktSegment' ) )
                        filteredDetails = filteredDetails.sort(function(x,y){ return x.CC_segment_id - y.CC_segment_id});
                    if(config.EnableBoundary && config.IsProvalConfig && config.ConfigTables){
                        var ConfigTables = config.ConfigTables;
                        var boundaryScales = ( showPrevious ? 250 : ( ( pi[ConfigTables.SktHeader] && pi[ConfigTables.SktHeader][0] && pi[ConfigTables.SktHeader][0]['scale'] )? pi[ConfigTables.SktHeader][0]['scale'] : 0 ) );
                        sketch.boundaryScale = (boundaryScales != 0 && boundaryScales != '') ? boundaryScales : 100;
                    }
                    if (config.OutBuildingDefinition && vectorSource.type == 'outBuilding' ) {
                    	for (var x in filteredDetails) {
                        	var id = filteredDetails[x];
                        	//if(showPrevious) id = filteredDetails[x].Original;
                            var outB = config.OutBuildingDefinition( id, vectorSource, {sksource}, null, showPrevious);
                            if(!isEmpty(outB)) {
                            	var outBd = outB.pop();
                            	if (outBd.isChanged)
                            		sketchedited = true;
                            	sketch.sketches.push(outBd);
                            }
                        }
                    }
                    else if (config.SketchDefinition) {
                    	var sks;
						if(showPrevious) {
							var originalfilteredDetails = [];
							for (var x in filteredDetails) {
								originalfilteredDetails.push(filteredDetails[x].Original);
							}
							sks = config.SketchDefinition(originalfilteredDetails, isChanged, true);
						}
						else
                    		sks = config.SketchDefinition(filteredDetails, isChanged);
                        sks.forEach(function(sk){
                        	if (sk.isChanged && !showPrevious)
                            	sketchedited = true; 
                        	sketch.sketches.push(sk);
                        });
                    }
                    else 
                    	for (var x in filteredDetails) {
                        	var id = filteredDetails[x];
							if(!config.VectorDefinition){
								if(showPrevious) id = filteredDetails[x].Original;
                        	}
                        	var org = id && id.Original ? id.Original : [];
                        	var readonly = false
                        	var vectorKey = id[sketchVectorKeyField];
                        	var vector = '';
                        	var isChanged = false;
                        	var labelValue, field, index, vector, vectorDetail, labelPosition, isChanged, areaFieldValue = null, noVectorSegment = false, areaUnit = null, Line_Type = false, isUnSketchedTrueArea = false, perimeterFieldValue = null, BasementFunctionality = false, yearValueToDisplay;
                        	if ( config.VectorDefinition ){
                        		if(showPrevious){
									if(id.CC_RecordStatus && id.CC_RecordStatus == 'I')
										continue;
								}
                            	vector = config.VectorDefinition( id, vectorSource, processor,showPrevious );
                           		vectorDetail = config.VectorDefinition( id, vectorSource, null, showPrevious);
                            	vector = vectorDetail.vector_string;
                            	isChanged = vectorDetail.isChanged;
                            	noVectorSegment = vectorDetail.noVectorSegment;
                            	Line_Type = vectorDetail.Line_Type;
	                        	isUnSketchedTrueArea = vectorDetail.isUnSketchedTrueArea;
	                        	perimeterFieldValue = vectorDetail.perimeterFieldValue ? vectorDetail.perimeterFieldValue: null
                            	areaFieldValue = vectorDetail.areaFieldValue ? vectorDetail.areaFieldValue : null;
                            	areaUnit = vectorDetail.areaUnit ? vectorDetail.areaUnit : null;
                        	}
                        	else
                        	{
                            	vector = eval( 'id.' + vectorSource.CommandField );
                            	labelPosition = eval('id.' + vectorSource.LabelCommandField);
                            	if(org && config.IsVectorStartCommandField)
	                                isChanged= (eval('id.' + vectorSource.CommandField) != eval('org.' + vectorSource.CommandField)) || (eval('id.' + vectorSource.LabelField) != eval('org.' + vectorSource.LabelField)) || (eval('id.' + vectorSource.VectorStartCommandField) != eval('org.' + vectorSource.VectorStartCommandField))
                            	else if ( org )
                                	isChanged = eval( 'id.' + vectorSource.CommandField ) != eval( 'org.' + vectorSource.CommandField ) || eval( 'id.' + vectorSource.LabelField ) != eval( 'org.' + vectorSource.LabelField );
                        		if (sksource.EnableMarkedAreaDrawing) 
                                	areaFieldValue = eval( 'id.' + vectorSource.AreaField);  
                        	}
                        	if (isChanged && !showPrevious)
                            	sketchedited = true;
                        	var field = getDataField(vectorSource.LabelField, vectorSource.Table)
                        	var index = sketchVectorTableAll.indexOf(id)
                        	if (field && field.ReadOnlyExpression)
                            	readonly = activeParcel.EvalValue( vectorSource.Table + '[' + index + ']', field.ReadOnlyExpression )

                        	var labelValue
                        	if ( config.LabelPositionDefinition )
                            	labelPosition = config.LabelPositionDefinition( id, vectorSource, showPrevious );
                        	var labelDetails = {};
                        	if ( config.LabelDefinition )
                        	{
                            	labelDetails = config.LabelDefinition( id, vectorSource, showPrevious );
                            	labelValue = labelDetails.labelValue;
                            	BasementFunctionality = labelDetails.BasementFunctionality ? labelDetails.BasementFunctionality : false
                            	isChanged = isChanged || labelDetails.isChanged;
                            	//if ( labelValue == '' ) labelPosition = null;
                        	}
                        	else labelValue = eval( 'id.' + vectorSource.LabelField )
                        	if(sksource.ShowYearBuilt && clientSettings['ShowYearValue'] == "1" && clientSettings['YearValueToDisplay']){
	                            var yearFieldValue = clientSettings['YearValueToDisplay'];
	                            var yearField = getDataField( yearFieldValue, vectorSource.Table);
	                            if(yearField)
	                            	yearValueToDisplay = eval( 'id.' + yearFieldValue);
	                        }
	                        
	                        let lblLookup = vectorSource.LabelLookup;
	                        if (vectorSource.LabelLookupSelector) {
	                        	let lblkField = getDataField(vectorSource.LabelLookupSelector.FieldName, vectorSource.LabelLookupSelector.Table);
	                        	lblLookup = lblkField? (lookup['#FIELD-'+ lblkField.Id]? '#FIELD-'+ lblkField.Id: lblLookup): lblLookup;
	                        }

                            if (lblLookup?.contains("/")) {
                                let slashlist = lblLookup.split("/");
                                for (slt in slashlist) {
                                    if (lookup[slashlist[slt]]) {
                                        lblLookup = slashlist[slt]; break;
                                    }
                                }
                            }

                        	var fLookUp = ccma.Data.LookupMap[lblLookup];
                        	var isLookUpQueryField = fLookUp ? fLookUp.LookupTable == '$QUERY$' : false;
                        	var desc = ( sketchSettings["sketchLookupQuery"] && sketchSettings["sketchLookupQuery"] != '' && query != '')? null: (lblLookup && lookup[lblLookup] ? lookup[lblLookup][labelValue] : null);
                        	var lookDesc = ( !sksource.DoNotShowLabelDescription && sketchSettings["DoNotShowLabelDescriptionSketch"] != '1' && clientSettings["DoNotShowLabelDescriptionSketch"] != "1" )? desc : null;
                        	var label = []; 
                        	var _hlfs = vectorSource.HideLabelFromShow? vectorSource.HideLabelFromShow: ( (config.ApexEditSketchLabel && sketchSettings['ApexEditSketchLabel'] == '1') ? true: false );
                        	label.push( { Field: vectorSource.LabelField, Value: labelValue, Description: lookDesc, labelDetails:labelDetails,IsEdited: false, lookup: lblLookup, ReadOnly: readonly, Caption: vectorSource.labelCaption, IsLargeLookup: sksource.IsLargeLookup, IsRequired: labelRequiredTrue, showLabelDescriptionOnly: isLookUpQueryField, hiddenFromShow: _hlfs, colorCode: ( desc && desc.color ? desc.color : null ), EnableFieldValidation: vectorSource.EnableFieldValidation } )
                        	if ( vectorSource.ExtraLabelFields && ( !config.ApexEditSketchLabel || ( config.ApexEditSketchLabel && sketchSettings['ApexEditSketchLabel'] == '1' ) ) ) {
                            	vectorSource.ExtraLabelFields.forEach(function (item) {
                                	field = getDataField(item.Name, vectorSource.Table)
                                    var val = eval('id.' + item.Name)
                                    let lknm = item.LookUp;
                                    if (lknm?.contains("/")) {
                                        let slashlist = lknm.split("/");
                                        for (slt in slashlist) {
                                            if (lookup[slashlist[slt]]) {
                                                lknm = slashlist[slt]; break;
                                            }
                                        }
                                    }
                                    var t = lknm && lookup[lknm] ? lookup[lknm][val] : val
                                	if (field && field.ReadOnlyExpression)
                                    	readonly = activeParcel.EvalValue( vectorSource.Table + '[' + index + ']', field.ReadOnlyExpression )
                                    fLookUp = ccma.Data.LookupMap[lknm]
                                	isLookUpQueryField = fLookUp ? fLookUp.LookupTable == '$QUERY$' : false;
                                    label.push({ Field: item.Name, Value: val, Description: t, lookup: lknm, ReadOnly: readonly, Caption: item.Caption, showLabelDescriptionOnly: isLookUpQueryField, hiddenFromShow: item.HideLabelFromShow, EnableFieldValidation: item.EnableFieldValidation } )
                            	})
                        	}
                        	var sk = {
                            	uid: vectorKey.toString(),
                            	label: label,
                            	labelPosition: labelPosition,
                            	vector:vector,
                            	isChanged: showPrevious ? false : isChanged,
                            	vectorConfig: vectorSource,
                            	noVectorSegment: noVectorSegment,
                            	Line_Type: Line_Type,
                            	areaFieldValue: areaFieldValue,
                            	isUnSketchedTrueArea: isUnSketchedTrueArea,
	                        	perimeterFieldValue: perimeterFieldValue,
	                        	BasementFunctionality: BasementFunctionality,
	                        	yearValueToDisplay: yearValueToDisplay,
                            	doNotAllowDeleteFirstVector: doNotAllowDeleteFirstVector,
                            	maxVectorLimit: maxVectorLimit,
                            	vectorStart: eval( 'id.' + vectorSource.VectorStartCommandField )
                        	}
                        	if ( vectorSource.FootModeField ) {
                            	var footMode = id[vectorSource.FootModeField]
                            	if ( footMode == true || footMode == 'true' )
                                	sk.footMode = footMode;
                       		}
                        	if (vectorSource.IdField) 
                            	sk.rowId = eval('id.' + vectorSource.IdField);

                        	if (processor.decodeSketch) 
                            	sk.vector = processor.decodeSketch(sk.vector);

                        	sketch.index = parseInt( sk_index );
                        	if ( vectorSource.vectorPagingField ){
                            	var page = id[vectorSource.vectorPagingField]
                            	if ( page == true || page == 'true' ){
                                	sk.page = page;
                                	var s = _.clone( sketch )
                                	s.sketches = [];
                                	s.sketches.push( sk )
                                	sketches.push( s );
                                	isPages = true;
                                	s.isNewPage = true;
                                	s.index = ( sketches.length )
                                	continue;
                            	}
                        	}
                        	var hideSketch = (sksource.HideNullSketches && !sk.vector) || false;
                        	if (!hideSketch) sketch.sketches.push(sk);
                    	}
                }
            }
            
            if(config.IsNoteMaxLength){
               if(sksource.NotesSource){
                	var maxNotelen, noteField = sksource.NotesSource.TextField, sNoteTable = sksource.NotesSource.Table;
                	maxNotelen = (getDataField(noteField,sNoteTable) && getDataField(noteField,sNoteTable).MaxLength && getDataField(noteField,sNoteTable).MaxLength != "" && getDataField(noteField,sNoteTable).MaxLength != '0') ? getDataField(noteField,sNoteTable).MaxLength: "34";
                	maxNotelen = maxNotelen.toString();
                	sketch.maxNotelen = maxNotelen;
                }
            }
            else if (sksource.NotesSource && sksource.NotesSource.Table && sksource.NotesSource.TextField ) {
				let noteFld = getDataField && getDataField(sksource.NotesSource.TextField, sksource.NotesSource.Table)
				if ( noteFld && noteFld.MaxLength)
					sketch.maxNotelen = noteFld.MaxLength;
			}
			
            if (config.NoteDefinition)
                sketch.notes = config.NoteDefinition(pi, showPrevious);
            else if (notesTable && notesTable.length > 0) {
                var cf = sksource.NotesSource.ConnectingFields;
                var filterCondition = "1==1";
                cf.forEach(function (f) {
                    filterCondition += " && ivx." + f + " == pi." + f
                });
                var filteredNotes = notesTable.filter(function (ivx) { if (ivx.ParentROWUID) return (ivx.ParentROWUID == pi.ROWUID); else return (eval(filterCondition)) });
                for (var x in filteredNotes) {
                    var note = filteredNotes[x];
                    var n = {
                        uid: note[sksource.NotesSource.KeyField || 'ROWUID'],
                        text: ( note[sksource.NotesSource.TextField] || '' ).replace(/&lt;/g,'').replace(/&gt;/g,''),
                        x: parseInt(note[sksource.NotesSource.PositionXField]) * (sksource.NotesSource.ScaleFactor || 1),
                        y: parseInt(note[sksource.NotesSource.PositionYField]) * (sksource.NotesSource.ScaleFactor || 1),
                        lx: parseInt(note[sksource.NotesSource.LineXField]) * (sksource.NotesSource.ScaleFactor || 1),
                        ly: parseInt(note[sksource.NotesSource.LineYField]) * (sksource.NotesSource.ScaleFactor || 1)
                    }
                    sketch.notes.push(n);
                }
            }

            if ((sketch.config.AllowSegmentAddition || sketch.config.AllowSegmentAdditionByCustomButtons) || sketch.sketches.length > 0) {
                sketches.push(sketch);
            }
        }
    }
    
    if (sketchedited) {
    	if((IsSketchApproved && activeParcel.QC) || (activeParcel.QC && config.SkipIsApprovedChanges)) {    // if(IsSketchApproved && activeParcel.QC) in some config IsSketchApproved not updating (proval or mvp, no commandfield) also, if the parcel is QC approved then need to be show in green
    		$('.tabs li a[category="sketch-manager"]').removeClass('edited-tab');
    		$('.tabs li a[category="sketch-manager"]').addClass('approved');
    	}
    	else {
    		$('.tabs li a[category="sketch-manager"]').removeClass('approved');
       		$('.tabs li a[category="sketch-manager"]').addClass('edited-tab');
       	}
    }
    else {
    	$('.tabs li a[category="sketch-manager"]').removeClass('approved');
        $('.tabs li a[category="sketch-manager"]').removeClass('edited-tab');
    }
    if ( isPages && sketches.length > 1 ) {
        sketches = sketches.sort( function ( x, y ) { return x.index - y.index } )
        sketches.forEach( function ( sk, i ) {
            sk.label = sk.label + ' - ' + ( i + 1 );
        } )
    }
    return sketches;
}

function showSketchesInQC(showAll, showPrevious, categoryId) {
    var index = $(".select-sketch-segments option").length > 0 ?  $(".select-sketch-segments option:selected").index() : -1 ;
    var sketches = getSketchSegments(showPrevious);
    //activeParcel.ParcelChanges.filter(function (x) { return (x.SourceTable == getClientSettingValue("SketchTable")) && (x.FieldName == getClientSettingValue('SketchLabel')) }).forEach(function (x) { sketches.filter(function (s) { return s.uid == x.AuxROWUID }).forEach(function (s1) { s1.label = x.NewValue; }) });
    // activeParcel.ParcelChanges.filter(function (x) { return (x.SourceTable == getClientSettingValue("SketchVectorTable")) && (x.FieldName == getClientSettingValue('SketchVectorLabelField')) }).forEach(function (x) { sketches.forEach(function (s) { var cn = 0; s.sketches.forEach(function (v) { cn += 1; if (v.uid == x.AuxROWUID) { v.label = x.NewValue } }) }) });

    var formatter = ccma.Sketching.SketchFormatter;
    var config = ccma.Sketching.Config;

    if (!formatter)
        formatter = CAMACloud.Sketching.Formatters.TASketch;

    sketchrenderer.formatter = formatter;
    sketchrenderer.config = config;
    sketchrenderer.showOrigin = false;
    try { sketchrenderer.open(sketches, null, true); }
    catch (e) {
        if (categoryId == 'sketch-manager') { addexclamationmark($('.select-sketch-segments').closest('.parcel-data-head')); } console.log(e);
    }


    if (index == 0 || index > 0 && !showAll) {
        if (sketchrenderer.sketches[index] != undefined || '') {
            sketchrenderer.currentSketch = sketchrenderer.sketches[index];
            var ab = sketchrenderer.sketchBounds(true);
            sketchrenderer.resizeCanvas((ab.xmax - ab.xmin) * DEFAULT_PPF + 400, (ab.ymax - ab.ymin) * DEFAULT_PPF + 95);
            sketchrenderer.pan(sketchrenderer.formatter.originPosition == "topRight" ? applyRoundToOrigin(-ab.xmax * DEFAULT_PPF + 15) : applyRoundToOrigin(-ab.xmin * DEFAULT_PPF + 15), (sketchrenderer.formatter.originPosition == "topRight" || sketchrenderer.formatter.originPosition == "topLeft") ? applyRoundToOrigin(-ab.ymax * DEFAULT_PPF + 15) : applyRoundToOrigin(-ab.ymin * DEFAULT_PPF + 15));
            sketchrenderer.clearCanvas();
            clearVectorlabelPosition();
            for (var x in sketchrenderer.sketches[index].vectors) {
                sketchrenderer.sketches[index].vectors[x].render();
            }
            if (sketchrenderer.displayNotes) sketchrenderer.sketches[index].renderNotes();
            var url = ""
            if (sketchrenderer.width < 25000 && sketchrenderer.width < 25000) {
                url = sketchrenderer.toDataURL();
                $('.prtsketch').show();
            }
            else
                $('.prtsketch').hide();
            if ($('.category-page[categoryid="sketch-manager"]').css('display') == 'block') {
                $('.sketch-details').show();
            }
            $('.sketch-frame').css({ 'background-image': 'url(' + url + ')' });
        }
        else {
            $('.sketch-details').hide();
        }
    }
    if (showAll) {
        $('option', '.select-sketch-segments').remove();
        for (x in sketches) {
            var skt = sketches[x];
            $('.select-sketch-segments').append($('<option>', { value: skt.uid, text: skt.label }));
        }
        var ab = sketchrenderer.allbounds(true);
        sketchrenderer.resizeCanvas((ab.xmax - ab.xmin) * DEFAULT_PPF + 400, (ab.ymax - ab.ymin) * DEFAULT_PPF + 95);
        sketchrenderer.pan(sketchrenderer.formatter.originPosition == "topRight" ? applyRoundToOrigin(-ab.xmax * DEFAULT_PPF + 15) : applyRoundToOrigin(-ab.xmin * DEFAULT_PPF + 15), (sketchrenderer.formatter.originPosition == "topRight" || sketchrenderer.formatter.originPosition == "topLeft") ? applyRoundToOrigin(-ab.ymax * DEFAULT_PPF + 15) : applyRoundToOrigin(-ab.ymin * DEFAULT_PPF + 15));
        sketchrenderer.clearCanvas();
        var currentSketches = sketchrenderer.currentSketch;
        sketchrenderer.currentSketch = null;  //showing all sketchs in prc
        sketchrenderer.renderAll();
        sketchrenderer.currentSketch = currentSketches;  //showing all sketchs in prc
        var allSketchUrl = ""
        if (sketchrenderer.width < 25000 && sketchrenderer.width < 25000)
            allSketchUrl = sketchrenderer.toDataURL();
        activeParcel.SketchPreview = allSketchUrl;
        activeParcel.newSketchPreview = allSketchUrl;
    }
}

function applyRoundToOrigin(v) {
    if (sketchrenderer?.config?.formatter == 'Vision') v = Math.ceil(v);
    return v;
}

function saveSketchChanges(data, Notedata, cc_error_msg, auditTrailEntry, callback) {
    try {
        var saveData = "", noteSaveData = "", CC_YearStatus = "";
        
        if ( clientSettings['FutureYearEnabled'] == '1') 
			CC_YearStatus = ( showFutureData ? 'F' : 'A' );
			
        for (var x in data) {
            var sd = data[x];
            if (sd.sid.includes("--")) sd.sid = sd.sid.split("--")[1]
            var vectorString = sd.vectorString;
            var fieldId = sd.sourceFieldId;
            var fieldIds = Object.keys(sd.segments).map(function (x) { return sd.segments[x].fieldId }).filter(function (x) { return x > -1 }).reduce(function (x, y) { return x + "|" + y }, fieldId) + '|' + (sd.perimeterFieldId || -1) + '|' + (sd.vectorStartFieldId || -1) + '|' + (sd.vectorPageFieldId || -1) + '|' + (sd.footModeFieldId || -1) + '|' + (sd.labelCommandFieldId || -1) + '|' + (sd.dimensionCommandFieldId || -1) + '|' + (sd.labelFieldId || -1) + '|' + ((sd.extraLabels.map(function (a) { return (a.FieldId || -1) }).length > 0) ? sd.extraLabels.map(function (a) { return (a.FieldId || -1) }).join('|') : '-1') + '|' + (sd.sketchProFieldId || -1);
            var fieldData = Object.keys(sd.segments).map(function (x) { return sd.segments[x] }).filter(function (x) { return x.fieldId > -1 }).map(function (x) { return x.area || 0 }).reduce(function (x, y) { return x + "|" + y }, vectorString) + '|' + (sd.perimeter || 0) + '|' + (sd.vectorStart || '') + '|' + (sd.vectorPage || 'false') + '|' + (sd.footMode || 'false') + '|' + (sd.labelCommand || '') + '|' + (sd.dimensionCommand || '') + '|' + (sd.label || '') + '|' + sd.extraLabels.map(function (a) { return (a.Value || '') }).join('|') + '|' + (sd.sketchProData || '');
            if (!sd.clientId)
                sd.clientId = sd.sid;
            saveData += sd.clientId + '|' + (sd.newRecord ? sd.sid : sd.clientId) + '|' + Base64.encode(fieldIds) + '|' + Base64.encode(fieldData) + '|' + CC_YearStatus + '\n';
        }
        for (var x in Notedata) {
            var nt = Notedata[x];
            if ( nt.noteid && nt.noteid.toString().includes("--")) nt.noteid = nt.noteid.split("--")[1];
            var fieldIds = nt.noteFieldID + '|' + nt.xPositionFieldId + '|' + nt.yPositionFieldId;
            var data = '$note$' + nt.noteText + '|' + nt.xPosition + '|' + nt.yPosition;
            if (!nt.clientId)
                nt.clientId = nt.noteid
            noteSaveData += nt.clientId + '|' + (nt.newRecord ? nt.noteid: nt.clientId) + '|' + Base64.encode(fieldIds) + '|' + Base64.encode(data) + '|' + CC_YearStatus + '\n';
        }
        var isCC_Error = 0;
        if (activeParcel.CC_Error && (activeParcel.CC_Error.indexOf('Sketch labels are invalid') > -1 || activeParcel.CC_Error.indexOf('There are multiple Sections for this Building') > -1))
        	isCC_Error = 1;
        $qc('savesketch', { ParcelId: activeParcel.Id, Data: saveData + '#' + noteSaveData, CC_Error: cc_error_msg? cc_error_msg: '', isCC_Error: isCC_Error, auditTrailEntry: auditTrailEntry }, function (resp) {
            console.log('Sketch successfully updated');
            if (callback) callback();
            else
                getParcel(activeParcel.Id, null, true);
        });
    }
    catch (e) {
        console.log(e.message);
    }
}

function PrintsketchView(callback) {
    var sketches = getSketchSegments();
    $('.all-sketch-frame').empty();
    for (var index in sketches) {
        activeParcel.ParcelChanges.filter(function (x) { return (x.SourceTable == (getClientSettingValue("SketchTable") || sketchSettings['SketchTable'])) && ( x.FieldName == ( getClientSettingValue('SketchLabel') || sketchSettings["SketchLabel"] ) ) }).forEach(function (x) { sketches.filter(function (s) { return s.uid == x.AuxROWUID }).forEach(function (s1) { s1.label = x.NewValue; }) });
        activeParcel.ParcelChanges.filter(function (x) { return (x.SourceTable == (getClientSettingValue("SketchVectorTable") || sketchSettings['SketchVectorTable'])) && (x.FieldName == (getClientSettingValue('SketchVectorLabelField') || sketchSettings["SketchVectorLabelField"] )) }).forEach(function (x) { sketches.forEach(function (s) { var cn = 0; s.sketches.forEach(function (v) { cn += 1; if (v.uid == x.AuxROWUID) { v.label.filter(function (l) { return l.Field == x.FieldName })[0].Value = x.NewValue } }) }) });
        var formatter = ccma.Sketching.SketchFormatter;
        var config = ccma.Sketching.Config;
        if (!formatter)
            formatter = CAMACloud.Sketching.Formatters.TASketch;
        sketchrenderer.formatter = formatter;
        sketchrenderer.config = config;
        sketchrenderer.showOrigin = false;
        try { sketchrenderer.open(sketches, null, true); } catch (e) { console.log(e); }
        var ab = sketchrenderer.allbounds(true);
        sketchrenderer.resizeCanvas((ab.xmax - ab.xmin) * DEFAULT_PPF + 400, (ab.ymax - ab.ymin) * DEFAULT_PPF + 95);
        sketchrenderer.pan(sketchrenderer.formatter.originPosition == "topRight" ? applyRoundToOrigin(-ab.xmax * DEFAULT_PPF + 15) : applyRoundToOrigin(-ab.xmin * DEFAULT_PPF + 15), (sketchrenderer.formatter.originPosition == "topRight" || sketchrenderer.formatter.originPosition == "topLeft") ? applyRoundToOrigin(-ab.ymax * DEFAULT_PPF + 15) : applyRoundToOrigin(-ab.ymin * DEFAULT_PPF + 15));
        //  sketchrenderer.pan(-ab.xmin * DEFAULT_PPF + 15, -ab.ymin * DEFAULT_PPF + 15);
        sketchrenderer.clearCanvas();
        if (index == 0 || index > 0) {
            if ( sketchrenderer.sketches[index] != undefined || '' ){
                clearVectorlabelPosition();
                for (var x in sketchrenderer.sketches[index].vectors) {
                    sketchrenderer.sketches[index].vectors[x].render();
                }
                if (sketchrenderer.displayNotes) sketchrenderer.sketches[index].renderNotes();
                var url = sketchrenderer.toDataURL();
                var dataURL = new Array();
                dataURL[index] = sketchrenderer.toDataURL();
                var area = 0;
                var perimeter = 0;
                for (var y in sketchrenderer.sketches[index].vectors) {
                    area = area + sketchrenderer.sketches[index].vectors[y].area();
                    perimeter = perimeter + sketchrenderer.sketches[index].vectors[y].perimeter();
                }
                var nbhd = $('#nbhdname').val() + ":" + activeParcel.NeighborhoodName;
                var keyvalue = $('#keyvalue1').val() + ": " + activeParcel.KeyValue1;
                var Address = "Address: " + activeParcel.StreetAddress;
                var Area = "Area: " + area.toString() + " sq.ft";
                var Perimeter = "Perimeter: " + perimeter.toString() + " ft";
                dataURL[index] = sketchrenderer.toDataURL();
                $('.all-sketch-frame').append('<table border="1" style="margin:20px 10px 10px 100px;font-size:16px;width:80%;page-break-inside:avoid;page-break-after:avoid;"><tr><td colspan="2" >' + keyvalue + '</td></tr><tr><td>' + nbhd + '</td><td>' + Address + '</td></tr><tr><td>' + Area + '</td><td>' + Perimeter + '</td></tr></table><img style="width: 100%;height:90%;position: static;margin-buttom:500px;page-break-before:avoid;page-break-after:always" src="' + dataURL[index] + '">');
            }
        }

    }
    if(callback) callback();
}

function clearVectorlabelPosition() {
	if (window.opener)
		window.opener.vectorLabelPositions = []; 
	vectorLabelPositions = [];
}

var sketchSorting = function (skRecord, stExps, stable) {
    if (!stExps || !skRecord || skRecord && skRecord.length == 0)
        return skRecord;

    let splitstExp = stExps.split(','), sortResultRecord = [];
    splitstExp.forEach((ex, i) => { if (ex.contains('ROWUID')) { splitstExp.splice(i, 1); splitstExp.unshift(ex); } });

    splitstExp.forEach((splitExp) => {
        let stExp = splitExp.trim(), stField = stExp.split(" ")[0], fld = getDataField(stField, stable),
            fldType = fld ? fld.InputType : (stField == 'ROWUID' ? '2' : null),
            stOrder = stExp.split(" ")[1] && stExp.split(" ")[1] == 'DESC' ? 'DESC' : 'ASC';

	    sortResultRecord = skRecord.sort(function(a, b) { 
            let val1 = (a[stField] ? a[stField] : ((stOrder == 'DESC') ? -99999999 : 99999999)),
                val2 = (b[stField] ? b[stField] : ((stOrder == 'DESC') ? -99999999 : 99999999));

	        if (fldType == "2" || fldType == "7" || fldType == "8" || fldType == "9") {
	            val1 = val1? parseFloat(val1): val1;
	            val2 = val2? parseFloat(val2): val2;
            }

	        if (val1 == val2) { return 0; }
	        else if ( (val1 == null || val1 == '' || val1 == ' ') && val1 !== 0) { return 1; }
	        else if ( (val2 == null || val2 == '' || val2 == ' ') && val2 !== 0 ) { return -1; }
	        else if (stOrder == 'DESC') { return val1 < val2 ? 1 : -1; }
	        else { 
	            if (stField == 'ROWUID' && val2 > 0 && val1 < 0) return 0;
                else return val1 < val2 ? -1 : 1;
	        }
	    }); 	
    });
    
    return (sortResultRecord ? sortResultRecord : []);
}

function prcPreviewSketch(firstTimePrc) {
	let index = ($(".select-sketch-segments-prc select")[0] && $(".select-sketch-segments-prc select option").length > 0) ?  $(".select-sketch-segments-prc select")[0].selectedIndex : -1 ,
        sketches = [], validvectorstring = true, skArray = [],
        blank_image = "data:image/png;base64,R0lGODlhFAAUAIAAAP///wAAACH5BAEAAAAALAAAAAAUABQAAAIRhI+py+0Po5y02ouz3rz7rxUAOw==", url = blank_image;

    index = firstTimePrc ? 0 : index; sketchrenderer.showOrigin = false;

    try { sketches = getSketchSegments(null, null, null, true); } catch (e) { validvectorstring = false; console.log(e); }

    try { sketchrenderer.open(sketches, null, true); } catch (e) { validvectorstring = false; console.log(e); }

    if ((index == 0 || index > 0) && sketchrenderer.sketches[index]) {
        sketchrenderer.currentSketch = sketchrenderer.sketches[index];
        let ab = sketchrenderer.sketchBounds(true);
        sketchrenderer.resizeCanvas((ab.xmax - ab.xmin) * DEFAULT_PPF + 400, (ab.ymax - ab.ymin) * DEFAULT_PPF + 95);
        sketchrenderer.pan(sketchrenderer.formatter.originPosition == "topRight" ? applyRoundToOrigin(-ab.xmax * DEFAULT_PPF + 15) : applyRoundToOrigin(-ab.xmin * DEFAULT_PPF + 15), (sketchrenderer.formatter.originPosition == "topRight" || sketchrenderer.formatter.originPosition == "topLeft") ? applyRoundToOrigin(-ab.ymax * DEFAULT_PPF + 15) : applyRoundToOrigin(-ab.ymin * DEFAULT_PPF + 15));
        sketchrenderer.clearCanvas();
        clearVectorlabelPosition();
        for (var x in sketchrenderer.sketches[index].vectors) { sketchrenderer.sketches[index].vectors[x].render(); }
        sketchrenderer.sketches[index].renderNotes();
        if (sketchrenderer.width < 25000 && sketchrenderer.width < 25000) { url = sketchrenderer.toDataURL(); }
    }

    if (firstTimePrc)
    	activeParcel.SketchPreview = url=="data:," ? blank_image : url;
    else if ($('.select-sketch-segments-prc').siblings('img')[0])
        $('.select-sketch-segments-prc').siblings('img')[0].src = url == "data:," ? blank_image : url;

    sketchrenderer.sketches.forEach((sk) => { skArray.push(sk.label); });

    if (firstTimePrc) return { skArray: skArray, flg: validvectorstring };
}

var sketchPreviewGenerations = (imgType, index) => {
    let url = "data:image/png;base64,R0lGODlhFAAUAIAAAP///wAAACH5BAEAAAAALAAAAAAUABQAAAIRhI+py+0Po5y02ouz3rz7rxUAOw==",
        type = (imgType == 'png') ? 'image/jpeg' : 'image/png';

    sketchrenderer.showOrigin = false;
    sketchrenderer.currentSketch = sketchrenderer.sketches[index];
    let ab = sketchrenderer.sketchBounds(true);
    sketchrenderer.resizeCanvas((ab.xmax - ab.xmin) * DEFAULT_PPF + 400, (ab.ymax - ab.ymin) * DEFAULT_PPF + 95);
    sketchrenderer.pan(sketchrenderer.formatter.originPosition == "topRight" ? applyRoundToOrigin(-ab.xmax * DEFAULT_PPF + 15) : applyRoundToOrigin(-ab.xmin * DEFAULT_PPF + 15), (sketchrenderer.formatter.originPosition == "topRight" || sketchrenderer.formatter.originPosition == "topLeft") ? applyRoundToOrigin(-ab.ymax * DEFAULT_PPF + 15) : applyRoundToOrigin(-ab.ymin * DEFAULT_PPF + 15));
    sketchrenderer.clearCanvas();
    clearVectorlabelPosition();
    for (var x in sketchrenderer.sketches[index].vectors) {
        sketchrenderer.sketches[index].vectors[x].render();
    }

    sketchrenderer.sketches[index].renderNotes();
    if (sketchrenderer.width < 25000 && sketchrenderer.width < 25000) {
        url = sketchrenderer.toDataURL(type, 1.0);
    }
    return url;
}