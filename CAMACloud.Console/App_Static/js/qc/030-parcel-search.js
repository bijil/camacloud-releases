﻿var filterIdIndex = 0;
var randomStartValue;
var randomValue;
var lookupData;
var fLoad;
var IsCustomSearch = false;
var CustQuery;
var fttime = 0;
var _sketchStringThrowError = false;
var parent_missing_msg = false;
var auditSorted = true;
var randomTrackerId = 0;
var tplPropertySearch = [
    new Filter("Quick Ref ID", "p.KeyValue1", "eq", "", null),
    new Filter("Property Number", "p.KeyValue2", "eq", "", null),
    new Filter("Property ID", "p.KeyValue3", "eq", "", null)
];

var tplFieldVisitSearch = [
    new Filter("Field Reviewed by", "p.ReviewedBy", "eq", "", null),
    new Filter("Reviewed On", "p.ReviewDate", "bw", "", "")
];

var tplNbhdList = [
    new Filter("Neighborhood", "nbhd.Number", "eq", "", null)
];
var txtNbhd = [
    new Filter("Assignment Group", "pdata.CC_GROUP", "eq", "", "")
]
var customTemplates = [];

var tplSyncFailure = [
    new Filter("Sync Failed", "p.FailedOnDownSyncStatus", "eq", "-1", null)
]

var saveOptions = {
    isNew: false,
    templateOnly: false,
    deleteSelected: false
}

function Filter(label, field, op, value1, value2) {
    this.ID = filterIdIndex++;
    this.Label = label || "";
    this.Field = field || "";
    this.Operator = op || "";
    this.Value1 = value1 || "";
    this.Value2 = value2 || "";
    this.DataType = "";

    this.toString = function (withData) {
        if (withData)
            return this.Label + '|' + this.Field + '|' + this.Operator + '|' + this.Value1 + '|' + this.Value2;
        else
            return this.Label + '|' + this.Field + '|' + this.Operator + '||';
    }


    this.form = function () {
        if(this.Field.contains('aggfield')){
            var aggval = this.Field.split('#');
            var aggrowuid = aggval[1];
            var field = offlineTables.AggrFields.filter(function(agr){ return (agr.ROWUID)==aggrowuid; })[0];
            var fieldName = field.FunctionName + '_' + field.TableName + '_' + field.FieldName;
            var label = fieldName;
            if(field.FunctionName !='FIRST' && field.FunctionName !='LAST' && field.FunctionName !='COUNT')
            var type = 'number';
            var labelSuffix = "";
        }
        else {
            var field = searchFields[this.Field];
            if (field == undefined) { return; }
            var fieldName = field.name;
            var label = $('#search-fields option[value="' + fieldName + '"]').text();
            if (label == '') { var label = field.label; }
            var type = field.inputType;
            var labelSuffix = "";
        }

        switch (this.Operator) {
            case "bw":
                labelSuffix = " between";
                break;
            case "gt":
                labelSuffix = " greater than";
                break;
            case "lt":
                labelSuffix = " lesser than";
                break;
            case "cn":
                labelSuffix = " contains";
                break;
            case "sw":
                labelSuffix = " starting with";
                break;
            case "ew":
                labelSuffix = " ending with";
                break;
            case "ge":
                labelSuffix = " greater than or equals"
                break;
            case "le":
                labelSuffix = " lesser than or equals"
                break;
            case "nl":
                labelSuffix = " is null"
                break;
            case "nn":
                labelSuffix = " is not null"
                break;
        }

        if (type == "date") {
            switch (this.Operator) {
                case "bw":
                    labelSuffix = " between";
                    break;
                case "gt":
                    labelSuffix = " after";
                    break;
                case "ge":
                    labelSuffix = " on or after";
                    break;
                case "lt":
                    labelSuffix = " before";
                    break;
                case "le":
                    labelSuffix = " on or before";
                    break;
            }
            label = label.replace(' On', '');
        }

        label = label + labelSuffix;

        var formDiv = document.createElement("div")
        formDiv.className = 'search-filter';
        if (this.Field.contains('aggfield')) 
            $(formDiv).attr('field', this.Field);
        else											
            $(formDiv).attr('field', field.name);
        $(formDiv).attr('fieldId', field.fieldId); /*FD_2480*/
        $(formDiv).attr('filter-id', this.ID);
        var aClose = document.createElement('a');
        aClose.innerHTML = '';
        aClose.className = 'a16 remove16';
        $(formDiv).append(aClose);

        $(aClose).click(function () {
            $('.templateName').hide();
            //var val = $(this).nextAll('.filter-value').find('.value').val();
            //currentSearchTemplate.remove(field.name, val);
            var val = $(this).parent().attr('filter-id');
            if(field.name == undefined)				
                currentSearchTemplate.remove('aggfieldId#'+field.ROWUID,val);
            else													
                currentSearchTemplate.remove(field.name, val);
            currentSearchTemplate.render('.filter-container');
            // doSearch();
            if (__DTR) {
                var docHeight = $(window).height();
                var cHeight = docHeight - $('#topNavBar').innerHeight() - $('#footer').innerHeight();
                cHeight = cHeight + ($(document).height() - $(window).height());
                setPageLayout($('#contentTable').width(), cHeight - 6);
            }
            return false;
        });
        var sTitle = document.createElement('span');
        sTitle.innerHTML = label;
        $(formDiv).append(sTitle);

        var vDiv = document.createElement("div")
        vDiv.className = 'filter-value';
        var input, input2;

        if ((this.Operator != "nl") && (this.Operator != "nn")) {
            switch (type) {
                case "none":
                    var f = this;
                    var fname = f.Label;
                    if (fname == "Pending Photos" || fname == "CAUTION: Pending Changes (All)" || fname == "CAUTION: Pending Changes 24+ hrs") {
                        var tempUsers = offlineTables.Users.slice(0);
                        tempUsers.unshift({ id: "Search All Users", name: "Search All Users" });
                        input = document.createElement('select');
                        fillDDL(input, tempUsers);
                        $(input).val(this.Value1);
                    }
                    break;
                case "text":
                    input = document.createElement('input');
                    $(input).val(this.Value1);
                    break;
                case "date":
                    input = document.createElement('input');
                    $(input).attr('type', 'date');
                    $(input).val(this.Value1);
                    break;
                case "number":
                    input = document.createElement('input');
                    $(input).attr('type', 'number');
                    $(input).val(this.Value1);
                    $(input).attr('onkeydown', 'preventExponentAndPlus(event);');
                    $(input).attr('tp', 'num');
                    break;
                case "yesno":
                    let dt = (this.Label === 'Sync Failed' || this.Label == "NbhdDownloadStatus") ? offlineTables.YesNo.filter(yesno => yesno.id == 0 || yesno.id == 1) : offlineTables.YesNo;
                    input = document.createElement('select');
                    fillDDL(input, dt);
                    $(input).val(this.Value1);
                    break;
                case "table":
                    if ((this.Operator != "cn") && (this.Operator != "sw") && (this.Operator != "ew") && (this.Operator != "ntl")) {
                        input = document.createElement('select');
                        fillDDL(input, offlineTables[field.lookup]);
                    }
                    else
                        input = document.createElement('input');
                    $(input).val(this.Value1);
                    break;
                case "numtable":
                    input = document.createElement('select');
                    fillDDL(input, offlineTables[field.lookup]);
                    $(input).val(this.Value1);
                    break;
                case "lookup":
                    input = document.createElement('select');
                    copyLookupToFilter(input, field.name, field.lookup, field.fieldId);
                    $(input).val(this.Value1);
                    break;
                default:
                    input = document.createElement('input');
                    $(input).val(this.Value1);
                    break;
            }
            $(input).removeClass('half');
            $(input).addClass('value');
            $(input).addClass('control');
            $(input).addClass('checkyear1');
            $(input).attr('oninput', 'checkyear(this);');
            $(input).attr('min', '1000-01-01');
            $(input).attr('max', '9999-12-31'); /*FD_2480*/

        }

        if (this.Operator == "bw") {
            $(input).addClass('half');
            switch (type) {
                case "text":
                    input2 = document.createElement('input');
                    $(input2).val(this.Value2);
                    break;
                case "date":
                    input2 = document.createElement('input');
                    $(input2).attr('type', 'date');
                    $(input2).val(this.Value2);
                    break;
                case "number":
                    input2 = document.createElement('input');
                    $(input2).attr('type', 'number');
                    $(input2).val(this.Value2);
                    $(input2).attr('onkeydown', 'preventExponentAndPlus(event);');
                    $(input2).attr('tp', 'num');
                    break;
                case "numtable":
                    input2 = document.createElement('select');
                    fillDDL(input2, offlineTables[field.lookup]);
                    $(input2).val(this.Value2);
                    break;
                default:
                    input2 = document.createElement('input');
                    $(input2).val(this.Value2);
                    break;
            }
            if (input2) {
                $(input2).addClass('half');
                $(input2).addClass('value');
                $(input2).addClass('control');
                $(input2).addClass('checkyear2');
                $(input2).attr('oninput', 'checkyear(this);');
                $(input2).attr('min', '1000-01-01');
                $(input2).attr('max', '9999-12-31'); /*FD_2480*/

            }

        }

        if (input)
            $(vDiv).append(input);
        if (input2) {
            var sAnd = document.createElement('span');
            sAnd.innerHTML = " and ";
            sAnd.className = "between-and";
            $(vDiv).append(sAnd);
            $(vDiv).append(input2);
        }

        $(formDiv).append(vDiv);
        return formDiv;
    }
}

function SearchTemplate() {
    var filters = [];

    this.toString = function (withData) {
        var ststr = '';
        for (x in filters) {
            var fx = filters[x];
            if (ststr != '')
                ststr += '^';
            ststr += fx.toString(withData);
        }
        return ststr;
    }
    this.addFilter = function (label, field, op, value1, value2) {
        var f;
        if (typeof label == "object") {
            if (label.Field && label.Label) {
                f = label;
            }
        } else {
            f = new Filter(label, field, op, value1, value2);
            /*vm-str for (x in filters) {
                var fx = filters[x];
                if (fx.Field == field && value2 != "or") {
                    f = fx;
                    f.Operator = op;
                    f.Value1 = value1;
                    f.Value2 = value2;
                    filters[x] = f;
                    return;
                }
            } vm-end */
        }
        if (f)
            filters.push(f);
    }
    this.__defineGetter__("Filters", function () { return filters; });

    this.clear = function () {
        filters = [];
    }

    this.remove = function (field, val) {
        updateFilters(); /*FD_2480*/
        var dx = -1;
        for (x in filters) {
            var fx = filters[x];
            if (fx.Field == field && fx.ID == val) {
                dx = x;
            }
        }
        filters.splice(dx, 1);
    }

    this.render = function (container) {
        $(container).html('');
        $.each(filters, function (i, filter) {
            $(container).append(filter.form());
        });

        var t = [];
        currentSearchTemplate.Filters.forEach(function (f) { if (searchFields[f.Field] != undefined || f.Field?.contains('aggfieldId')) t.push(searchFields[f.Field]) });
        if (t.length > 1) $('.andOr-filter').show();
        else $('.andOr-filter').hide();

        /* Always show search button
        if (filters.length == 0) {
          //   $('.current-filter .buttons').hide();
         //} else {
          //   $('.current-filter .buttons').show();
         }*/
    }
}

function initSearchResultsGrid(height, callback) {
    $qc('searchresultfields', null, function (cols) {
        $(".grid-container").html('<table class="parcel-results" style="display: none" onselectstart="return false;" ondragstart="return false;" selectable="false"></table>');
        $(".parcel-results").flexigrid({
            url: '/quality/search.jrq',
            dataType: 'json',
            colModel: cols,
            usepager: true,
            resizable: false,
            title: 'Parcel Search Results',
            useRp: true,
            rp: 50,  //25 CC_QC_3277
            showTableToggleBtn: false,
            width: '100%',
            height: height,
            onSuccess: function () {
                $('.current-filter .control').removeAttr('disabled');
            },
            onNoData: function () {
                $('.current-filter .control').removeAttr('disabled');
            },
            onDblClick: function (id, row) {
                openParcelWithHash(id);
            },
            onColResize: function (a, b) {
                $qc('setsearchresultview', {
                    Property: 'DefaultWidth',
                    SettingsId: a,
                    Value: b
                }, function () { });
            },
            onDragCol: function (a) {
                $qc('qccolumnswap', {
                    Property: 'Ordinal',
                    SettingsId: a
                }, function () { });
            },
            onSearch: function () {
                closeBulkEditor();
                closeAdhocCreator();
            }
        });
        $('.tip').tipr();
        qcVisualStats();
        setScreenDimensions();
        if (callback) callback();
    });

}

var currentSearchTemplate = new SearchTemplate();

function initSearchTemplate() {
    currentSearchTemplate = new SearchTemplate();
    loadFilters(tplNbhdList);
    console.log('here');
    var sel = $('#qc-search .search-filter').text();
    if (sel == "" || '' || null) {
        loadFilters(txtNbhd);
    }
}

function loadFilters(list, skipClearing) {
    if (!skipClearing)
        currentSearchTemplate.clear();
    for (x in list) {
        currentSearchTemplate.addFilter(list[x]);
    }
    currentSearchTemplate.render('.filter-container');
}

function initSearchPanel(data, callback) {
    $('.create-filter .operator').hide();
    $('.create-filter .value').hide();
    $('.create-filter .value2').hide();
    $('.create-filter .filter-add').hide();
    addToDDL('.create-filter .field', '-- Select --', '', null);

    if (internalSupportUsers.indexOf(currentSupportLoginId) == -1) {
        let ix = data.SearchFields.findIndex((x) => x.name == 'p.DownloadStatus');
        if (ix > -1) {
            data.SearchFields.splice(ix, 1);
        }
    }

    for (x in data.SearchFields) {
        var sf = data.SearchFields[x];
        addToDDL('.create-filter .field', sf.label, sf.name, sf.fieldId);
        searchFields[sf.name] = sf;
    }
        for (x in data.AggrFields) {		//commented section to hold SC_741
            var sf = data.AggrFields[x];
            var AggrFields = {};
            addToDDL('.create-filter .field', sf.FunctionName+'_'+sf.TableName+'_'+sf.FieldName, 'aggfieldId#'+sf.ROWUID+'/'+sf.FunctionName+'/'+sf.SchemaDataType+'/'+sf.FunctionName+'_'+sf.TableName+'_'+sf.FieldName);
           AggrFields[sf.name] = sf;
       }									//commented section to hold SC_741
    $('.create-filter .field').change(function () {
        $('.operator').off();
        $('#searchOr').hide();
        $('.remove').hide();
        $('.operator').hide();
        $('.val').hide();
        var f = this;
        var fname = $(f).val();
        $('.create-filter .operator option[value="cn"]').remove()
        $(".create-filter .operator option").eq(7).before($("<option></option>").val("cn").text("Contains"));
        $('.create-filter .operator').change(function () {
            $('.remove').hide();
            if ($(this).val() == "bw")
                $('.create-filter .value2').show();
            else
                $('.create-filter .value2').hide();
            if ($(this).val() == "nl" || $(this).val() == "nn" || $(this).val() == "al")
                $('.create-filter .values1').hide();
            else
                $('.create-filter .values1').show();
        });
        if (fname == "~AllPendingChanges" || fname == "~BacklogChanges" || fname == "~PendingPhotosOrNotDownsynced") {
            var tempUsers = data.Users.slice(0);
            tempUsers.unshift({ id: "Search All Users", name: "Search All Users" });
            fillDDL('.create-filter select.value', tempUsers);
            $('.create-filter select.value').show();
            $('.create-filter select.value').addClass('selected-input');
        }
        $('.create-filter .value').removeClass('selected-input');
        $('.create-filter .value').val('');
        $('.create-filter .values1').show();
        if (fname == '' || fname == null) {
            $('.create-filter .operator').hide();
            $('.create-filter .value').hide();
            $('.create-filter .filter-add').hide();
        } else {
            $('.create-filter .operator').show();
            $('.create-filter .value').hide();
            $('.create-filter .operator').val('eq');
            $('.create-filter .value2').hide();
            $('.create-filter .filter-add').show();
            if (searchFields[fname]!=undefined) { //commented section to hold SC_741
                var sf = searchFields[fname];
                switch (sf.inputType) {
                    case 'none':
                        $('.create-filter .operator').hide();
                        if (fname == "~AllPendingChanges" || fname == "~BacklogChanges" || fname == "~PendingPhotosOrNotDownsynced") {
                            var tempUsers = data.Users.slice(0);
                            tempUsers.unshift({ id: "Search All Users", name: "Search All Users" });
                            fillDDL('.create-filter select.value', tempUsers);
                            $('.create-filter select.value').show();
                            $('.create-filter select.value').addClass('selected-input');
                        }
                        break;
                    case 'text':
                        $('.operator').change(function () {
                            if ($(this).val() == "sw") {
                                $('.remove').remove();
                                $('#searchOr').show();
                            }
                            else
                                $('#searchOr').hide();
                        });
                        $('.create-filter .value:text').show();
                        $('.val').remove();
                        $('.create-filter .value:text').addClass('selected-input');
                        $('.create-filter .operator option[value="cn"]').remove()
                        $('.create-filter .operator').prepend("<option value='cn' >Contains</option>");
                        $('.create-filter .operator').val('cn');
                        disableOperators(["gt", "lt", "ge", "le", "bw", "al"]);
                        break;
                    case 'date':
                        $('.create-filter .value[type="date"]').show();
                        $('.create-filter .value[type="date"]').addClass('selected-input');
                        disableOperators(["ne", "cn", "sw", "ew", "al", "ntl"]);
                        break;
                    case 'number':
                        $('.create-filter .value[type="number"]').show();
                        $('.create-filter .value[type="number"]').addClass('selected-input');
                        disableOperators(["cn", "sw", "ew", "al", "ntl"]);
                        break;
                    case "yesno":
                        let dt2 = (fname === 'p.FailedOnDownSync' || fname == "p.DownloadStatus") ? data.YesNo.filter(yesno => yesno.id == 0 || yesno.id == 1) : data.YesNo;
                        fillDDL('.create-filter select.value', dt2);
                        $('.create-filter select.value').show();
                        $('.create-filter select.value').addClass('selected-input');
                        disableOperators();
                        break;
                    case "numtable":
                        fillDDL('.create-filter select.value', data[sf.lookup]);
                        $('.create-filter select.value').show();
                        $('.create-filter select.value').addClass('selected-input');
                        disableOperators(['ne', 'cn', 'sw', 'ew', "al", "ntl"]);
                        break;
                    case "table":
                        fillDDL('.create-filter select.value', data[sf.lookup]);
                        $('.create-filter select.value').show();
                        $('.create-filter select.value').addClass('selected-input');
                        if ($('.create-filter .field').val() == "nbhd.Number") {
                            disableOperators(["gt", "lt", "ge", "le", "bw", "ne"]);
                            $('.operator').change(function () {
                                if ($(this).val() == "sw" || $(this).val() == "cn" || $(this).val() == "ew" || $(this).val() == "ntl") {
                                    $('.create-filter .value').removeClass('selected-input');
                                    $('.create-filter .value:text').show();
                                    $('.val').remove();
                                    $('.create-filter select.value').hide();
                                    $('.create-filter .value:text').addClass('selected-input');
                                }
                                else {
                                    $('.create-filter select.value').show();
                                    $('.create-filter .value:text').removeClass('selected-input');
                                    $('.create-filter select.value').addClass('selected-input');
                                    $('.create-filter .value:text').hide();
                                }
                                if ($(this).val() == "sw")
                                    $('#searchOr').show();
                                else
                                    $('#searchOr').hide();
                            })

                        }
                        else {
                            disableOperators();
                        }
                        break;
                    case "lookup":
                        copyLookupToFilter('.create-filter select.value', fname, sf.lookup, sf.fieldId);
                        $('.create-filter select.value').show();
                        $('.create-filter select.value').addClass('selected-input');
                        disableOperators();
                        break;
                    default:
                        break;
                }
            } //commented section to hold SC_741
            else {
                var aggval = fname.split('/');
                var aggfnname = aggval[1];
                var aggdatatype = aggval[2];
                if (aggdatatype && aggdatatype != '') aggdatatype = aggdatatype.toUpperCase();

                if (aggdatatype =="INT"||aggdatatype.contains('NUMERIC')||aggdatatype.contains('FLOAT')) {
                    $('.create-filter .value[type="text"]').show();
                    $('.create-filter .value[type="text"]').addClass('selected-input');
                    disableOperators( [ "cn","bw","al"] );
                }

                if (aggfnname !='FIRST' && aggfnname !='LAST' && aggfnname !='COUNT') {
                    $('.create-filter .value[type="number"]').show();
                    $('.create-filter .value[type="number"]').addClass('selected-input');
                    $( '.create-filter .value:text').hide();
                    $('.create-filter .value[type="text"]').removeClass('selected-input');
                }

                if (aggdatatype.contains('VARCHAR')) {
                    $('.operator').change(function () {
                        if ($(this).val() == "sw") {
                            $('.remove').remove();
                            $('#searchOr').show();
                    }
                        else 
                            $('#searchOr').hide();
                    });
                    $('.create-filter .value:text').show();
                    $('.val').remove();
                    $('.create-filter .value:text').addClass('selected-input');
                    $('.create-filter .operator option[value="cn"]').remove()
                    $('.create-filter .operator').prepend("<option value='cn' >Contains</option>");
                    $('.create-filter .operator').val('cn');
                    disableOperators(["gt", "lt", "ge", "le", "bw", "al"]);
                }
            }				//commented section to hold SC_741
        }
    });
    $('#myandorswitch').off('click');
    //$('#myandorswitch').click(function () { doSearch(); })
    $('.search_or').unbind('click');
    $('.search_or').click(function () {
        var div = $('.values1');
        var boxcount = $('#qc-search .values1 .countbox');
        if (boxcount.length > 8) {
            return false;
        }
        else
            div.append('<input type="text" class="value val countbox" style="margin-top:6px;"/><a class="a16 remove16 remove"></a>');
        $('.remove').unbind();
        $('.remove').bind('click', function () {
            $(this).prev('input').remove();
            $(this).remove();
        });
    });

    $('.filter-add').click(function () {
        $('#searchOr').hide();
        $('.remove').remove();
        var field = $('.create-filter .field').val();
        var field1 = $('.create-filter select.value').val();
        var op = $('.create-filter .operator').val();
        var value1;
        if (field == null || field == "") return;
        if (searchFields[field] != undefined && (searchFields[field].inputType == "text" || searchFields[field].inputType == "table") && $('.operator').val() == "sw") {
            $('.create-filter .values1 input[type="text"].value').each(function (i, c) {
                var val = $(c).val();
                if (field) {
                    currentSearchTemplate.addFilter(searchFields[field].label, field, op, val, "or");
                    currentSearchTemplate.render('.filter-container');
                }
            });
        }
        else {
            if ((field == "~AllPendingChanges" || field == "~BacklogChanges" || field == "~PendingPhotosOrNotDownsynced") && field1 != "Search All Users")
                value1 = field1;
            else
                value1 = $('.create-filter .values1 .selected-input').val();
            var value2 = $('.create-filter .value2 .selected-input').val();
            if (field) {
                if (field.contains('aggfield')) {		//commented section to hold SC_741
                    var aggval = field.split('/');
                    currentSearchTemplate.addFilter(aggval[3], aggval[0], op, value1, value2);
                    currentSearchTemplate.render('.filter-container');
                }
                else {
                    currentSearchTemplate.addFilter(searchFields[field].label, field, op, value1, value2);
                    currentSearchTemplate.render('.filter-container');
                }										//commented section to hold SC_741
            }
        }
        $('.current-filter').show();
        $('.create-filter').hide();
        $('.create-filter .field').val('');
        //doSearch();
        return false;
    });


    $('.filter-cancel').click(function () {
        removeFilter();
        return false;
    });

    $('.filters-add-filter').click(function () {
        IsCustomSearch = false;
        updateFilters();
        $('.templateName').hide();
        if (currentSearchTemplate.Filters.length > 6) {
            alert('You are not allowed to add more than 7 filters.');
            return;
        }
        $('.qc-save-query').show();
        $('.filter-customfilter').hide();
        $('#searchOr').hide();
        $('.current-filter').hide();
        $('.create-filter').show();
        $('.create-filter .field').val('');
        $('.create-filter .operator').hide();
        $('.create-filter .value').hide();
        $('.create-filter .filter-add').hide();
        $(".field option").each(function () {
            var $option = $(this);
            $option.siblings()
                .filter(function () { return $(this).text() == $option.text() && $(this).attr('fieldid') == $option.attr('fieldid') })
                .remove()
        });
        // if(__DTR){
        //    var docHeight = $(window).height();
        //    var cHeight = docHeight - $('#topNavBar').innerHeight() - $('#footer').innerHeight();
        //    cHeight = cHeight + ($(document).height() - $(window).height());
        //    setPageLayout($('#contentTable').width(), cHeight);
        //}
        return false;
    });

    $('.filters-clear-filters').click(function () {
        IsCustomSearch = false;
        CustQuery = "";
        $('.templateName').hide();
        $('.filter-customfilter').hide();
        $('.qc-save-query').show();
        currentSearchTemplate.clear();
        currentSearchTemplate.render('.filter-container');
        // if(__DTR){
        //    var docHeight = $(window).height();
        //    var cHeight = docHeight - $('#topNavBar').innerHeight() - $('#footer').innerHeight();
        //    cHeight = cHeight + ($(document).height() - $(window).height());
        //    setPageLayout($('#contentTable').width(), cHeight);
        //}
        return false;
    });

    $('.filters-adv-filters').click(function () {
        IsCustomSearch = true;
        automask();
        lookupData = lookup;
        document.getElementById("CustomFilter").innerHTML = '<iframe style="width: 100%; height: 400px;border: none;" src="/protected/CustomFilter/Custom-filter.html?32" ></iframe>';
        $('#CustomQueryBuilder').data('lookupData', lookup);
        $('.templateName').html('');
        document.getElementById('filterID').value = 0;
        document.getElementById('filter-name').value = "Custom_" + new Date().getTime();
        document.getElementById('Issave').value = true;
        $('#Issave').attr('jsondata', '');
        $('#CustomQueryBuilder').show();
        return false;
    });

    $('.current-filter .buttons button').click(function () {
        try {
            doSearch();
        } catch (e) {
            console.error(e);
        }
        return false;
    });

    $('.qc-selected-template').change(function () {
        var tid = $(this).val();
        var std = ($('option[value="' + tid + '"]', this).attr('standard') != null);
        if (tid && $('option[value="' + tid + '"]', this).text().contains('(CustomQuery)'))
            $('.qc-saved-edit-name').hide();
        else
            $('.qc-saved-edit-name').show();
        if (std || (tid == null)) {
            $('.qc-delete-template').hide();
            $('.qc-saved-name').attr('disabled', 'disabled');
            $('.qc-saved-save-name').hide();
        } else {
            $('.qc-delete-template').show();
            $('.qc-saved-name').removeAttr('disabled');
            $('.qc-saved-save-name').show();

            saveOptions.isNew = false;
            saveOptions.templateOnly = false;
        }

        if (tid == null) {
            $('.template-control').hide();
            return;
        }

        $('.template-control').show();
        var name = $('option[value="' + tid + '"]', this).text();
        $('.qc-saved-name').val(name);



    });

    $('.qc-use-template').click(function () {
        showSelectedTemplate();
        if (__DTR) {
            var docHeight = $(window).height();
            var cHeight = docHeight - $('#topNavBar').innerHeight() - $('#footer').innerHeight();
            cHeight = cHeight + ($(document).height() - $(window).height());
            setPageLayout($('#contentTable').width(), cHeight - 6);
        }
        return false;
    });

    $('.qc-selected-template').dblclick(function () {
        showSelectedTemplate();
        if (__DTR) {
            var docHeight = $(window).height();
            var cHeight = docHeight - $('#topNavBar').innerHeight() - $('#footer').innerHeight();
            cHeight = cHeight + ($(document).height() - $(window).height());
            setPageLayout($('#contentTable').width(), cHeight - 6);
        }
    });

    $('.qc-save-template').click(function () {
        saveOptions.isNew = true;
        saveOptions.templateOnly = true;
        saveOptions.deleteSelected = false;
        showTemplateSaveForm();
    });

    $('.qc-save-query').click(function () {
        saveOptions.isNew = true;
        saveOptions.templateOnly = false;
        saveOptions.deleteSelected = false;
        showTemplateSaveForm();
    });

    $('.qc-cancel-save').click(function () {
        cancelTemplateSaveForm();
        $('.qc-tabs').tabs("select", 0);
    });

    $('.qc-delete-template').click(function () {
        deleteTemplate();
    });

    $('.qc-saved-save-name').click(function () {
        try {
            saveQuery();

        } catch (e) {
            console.error(e);
        }
        return false;
    });

    $('.randomizer').change(function () {
        var r = $(this).val();
        $('.randomizer-value').html(r);
    });
    loadQueries();
    if (callback) callback()
}

function disableOperators(ops) {
    if (!ops) {
        $('.create-filter .operator').attr('disabled', 'disabled');
    } else if (ops.length) {
        $('.create-filter .operator').removeAttr('disabled');
        $('.create-filter .operator option').show();
        for (x in ops) {
            var op = ops[x];
            //$('.create-filter .operator option[value="' + op + '"]').attr('disabled', 'disabled');
            $('.create-filter .operator option[value="' + op + '"]').hide();
        }
    }
}

function removeFilter() {
    $('.remove').remove();
    $('.current-filter').show();
    $('.create-filter').hide();
    $('.create-filter .field').val('');
    $('.create-filter .operator').hide();
    $('.create-filter .value').hide();
    $('.create-filter .filter-add').hide();
}

function addToDDL(ddl, text, value, fieldId) {
    var opt = document.createElement('option');
    $(opt).attr('value', value);
    opt.innerHTML = text
    $(opt).attr('fieldId', fieldId);
    if (text == 'Assigned Appraiser' && localStorage?.toolTipEnabled == '1') $(opt).attr('title', 'Shows the appraiser that is assigned to the Assignment Group the parcel is in.');
    $(ddl).append(opt);
}

function fillDDL(ddl, table, fieldId) {
    $(ddl).html('');
    for (x in table) {
        var d = table[x];
        addToDDL(ddl, d.name, d.id, d.fieldId);
    }
}

function copyLookupToFilter(ddl, fieldName, lookupName, fieldId) {
    if (!lookupName || lookupName == '') {
        $('.create-filter select.value').empty();
        alert('Lookup Field ' + searchFields[fieldName].label + ' has no lookup table/data loaded.')
        return;
    }
    $(ddl).html('');
    if (lookupName.toString() == "$QUERY$") {
        var t = fieldName.replace('pdata.', '').replace('p.', '')
        var opt = fieldId ? lookup['#FIELD-' + fieldId] : lookup[t];
        opt = opt ? opt : {};
        var sortedopt = Object.keys(opt).sort(function (a, b) { return opt[a].Ordinal - opt[b].Ordinal })
        for (o in sortedopt) {
            var text = opt[sortedopt[o]].Name;
            if (opt[sortedopt[o]].Object && opt[sortedopt[o]].Object.SearchValueInQC)
                var value = opt[sortedopt[o]].Object.SearchValueInQC.toString();
            else
                var value = opt[sortedopt[o]].Id;
            var fieldId = opt[sortedopt[o]].fieldId;
            addToDDL(ddl, text, value, fieldId);
        }
    }
    else {
        var field = datafields && datafields[fieldId];
        if (field && field.InputType == 5 && lookup && lookup[lookupName]) {
            for (var keys in lookup[lookupName]) {
                var lkdata = lookup[lookupName][keys];
                if (lkdata) {
                    var id = lkdata['Id'], name = lkdata['Name'];
                    if (id == "True" || id == "TRUE" || id == "False" || id == "FALSE")
                        id = id.toLowerCase();
                    $(ddl).append($('<option/>', { value: id, text: name }));
                }
            }
        }
        else {
            $(ddl).html($('[lookuptable="' + lookupName + '"]').html());
            $(ddl).html($('[lookuptable="' + lookupName + '"]').html());
        }
        $('option[value=""]', ddl).remove();
        $(ddl).prepend($('<option/>', { value: -1, text: 'Is NULL' }));
        $(ddl).prepend($('<option/>', { value: -2, text: 'Is not NULL' }));
    }
}
function doSearch() {
    closeBulkEditor();
    closeAdhocCreator();
    $('.current-filter .control').attr('disabled', 'disabled');  // enabled on flexgrid, onSucess, see above
    randomValue = parseInt($('.randomizer').val());
    var op = $('#myandorswitch').attr('checked') ? 'AND' : 'OR';
    var defaultFilter = $('.app-dtr-selected-task').val();
    randomTrackerId = 0;
    var data = {
        query: getSearchParam(),
        random: randomValue,
        dtrfilter: defaultFilter,
        PrioTableId: localStorage.getItem("PrioTableId"),
        Operation: op,
        CustomSearch: IsCustomSearch
    };
    if (defaultFilter && __DTR) {
        try {
            if (DTRRoles) { }
        }
        catch (err) {
            alert("Please set the user role.");
            $('.current-filter .control').removeAttr('disabled');
            return false;
        }
    }
    $(".parcel-results").flexOptions({ page: 1 });
    switchToSearchView();
    $('.parcel-results').Search(data);
    switchToActualYear();
}

function exporttoformats(formattype) {

    randomValue = parseInt($('.randomizer').val());
    var defaultFilter = $('.app-dtr-selected-task').length > 0 ? $('.app-dtr-selected-task').val() : -1;
    var op = $('#myandorswitch').attr('checked') ? 'AND' : 'OR';
    var Fields = $('.hDivBox tr th').map(function () {
       var fields = this.abbr + ',' + this.innerText + ',' + parseInt($(this).children('div').css('width'));
       return fields;
   }).get().join('|');

    var data = {
        query: getSearchParam(),
        searchfields: Fields,
        random: randomValue,
        dtrfilter: defaultFilter,
        export: formattype,
        PrioTableId: localStorage.getItem("PrioTableId"),
        Operation: op
    };
    if (data.query.contains('LEFT OUTER JOIN'))
        data.CustomSearch = true;
    else
        data.CustomSearch = false;

    var stt = false;
    if (data.query != lastPostData.query || data.random != lastPostData.random || data.Operation != lastPostData.Operation || (__DTR && data.dtrfilter != lastPostData.dtrfilter)) {
        var stt = confirm("The search results you are exporting do not match the current search filters in place.");
        if (!stt) return false;
    }
    let raTracker = ''
    if (randomTrackerId && randomTrackerId != 0 && !stt) raTracker = randomTrackerId;

    Fields = Fields.split('#').join('~~');
    window.open('/quality/search.jrq?equery=' + Base64.encode(getSearchParam()).replace('+', '_') + '&dtrfilter=' + defaultFilter + '&searchfields=' + Fields + '&export=' + formattype + '&PrioTableId=' + localStorage.getItem("PrioTableId") + '&Operation=' + op + '&CustomSearch=' + data.CustomSearch + '&random=' + data.random + '&randomTrackerId=' + raTracker)
}


function getSearchParam() {
    var searchParam = '';
    var filters = [];
    if (currentSearchTemplate.Filters) {
        filters = currentSearchTemplate.Filters.slice(0);
        filters = filters.sort(function (a, b) {
            var fieldA = a.Field.toLowerCase(), fieldB = b.Field.toLowerCase();
            if (fieldA < fieldB) return -1;
            if (fieldA > fieldB) return 1;
            return 0;
        }).sort(function (a, b) {
            var opA = a.Operator.toLowerCase(), opB = b.Operator.toLowerCase();
            if (opA < opB) return -1;
            if (opA > opB) return 1;
            return 0
        });
    }
    for (var j = 0; j < filters.length; j++) {
        var f = filters[j];
        var form = $('.search-filter[filter-id="' + f.ID + '"]');
        var inputs = $('.value', form);
        if (f.Operator == "sw" && (f.Value2 == "or" || (f.Value2 === "" ? f.Value2 = "or" : f.Value2)) && !(f.Field.contains('aggfield'))) {
            for (k = 0; k < inputs.length; k++) {
                var val = $(inputs[k]).val();
                f['Value'+(k+1)] = val;
                if (k == 0) {
                    var a = '*'
                    if (inputs.length == 1) a += '#';
                    searchParam += "^^^" + f.Field + "|" + f.Operator + "|" + val + a + '___@@@\n';

                }
                else if (k == (inputs.length) - 1)
                    searchParam += "^^^" + f.Field + "|" + f.Operator + "|" + val + '#' + '___@@@\n';
                else
                    searchParam += "^^^" + f.Field + "|" + f.Operator + "|" + val + '___@@@\n';
                j++;
            }
            j--;
        }
        else {
            //f.Value1 = $(inputs[0]).val() || f.Value1;
            if (f.Label == "Redirected Priority List Parcels") {
                f.Value1 = $(inputs[0]).val() || f.Value1;
            }
            else {
                f.Value1 = $(inputs[0]).val();
            }

            if (inputs.length == 2) {
                //vm f.Value2 = $(inputs[1]).val();
            }
            f.Value2 = $(inputs[1]).val() || "";
            if (f.Operator == "bw" && f.Value1 != "" && f.Value2 != "")
                searchParam += "^^^" + f.Field + "|" + f.Operator + "|" + f.Value1 + '¤' + f.Value2 + '___@@@\n';
            else if (f.Value1 == "Search All Users")
                searchParam += "^^^" + f.Field + "|" + f.Operator + "|" + '___@@@\n';
            else {
                if (f.Value1 == '-3' && f.Field == 'p.QC') searchParam += "^^^p.StatusFlag|" + f.Operator + "|" + f.Value1 + '___@@@\n';
                else searchParam += "^^^" + f.Field + "|" + f.Operator + "|" + f.Value1 + '___@@@\n';
            }
        }

    }
    if (IsCustomSearch && CustQuery) {
        var query = CustQuery.split("^")[0] + "^" + CustQuery.split("^")[1]
        searchParam = query;
    }

    return searchParam;
}

function getSearchStatus(o) {
    var parIndex;
    if (o) {
        if (o.ParcelId) {
            parIndex = $('.parcel-results tr[parcelid]').index($('tr[parcelid="' + o.ParcelId + '"]'));
        }
    }
    var ss = {
        page: $('.parcel-results').flexOptions("page"),
        total: $('.parcel-results').flexOptions("total"),
        pageSize: $('.parcel-results').flexOptions("rp"),
        parcelIndex: parIndex
    };

    var minIndex = 0;
    var maxIndex = ss.pageSize - 1;
    var maxPages = Math.ceil(ss.total / ss.pageSize);
    if (ss.page == maxPages) {
        maxIndex = Math.min(ss.total % ss.pageSize, ss.pageSize) - 1;
        if (ss.total % ss.pageSize == 0) {
            maxIndex = ss.pageSize - 1;
        }
    }

    return {
        page: ss.page,
        total: ss.total,
        pageSize: ss.pageSize,
        parcelIndex: ss.parcelIndex,
        minIndex: 0,
        maxIndex: maxIndex,
        minPage: 1,
        maxPage: maxPages,
        isFirst: (ss.page == 1) && (parIndex == 0),
        isLast: (ss.page == maxPages) && (parIndex == maxIndex)
    };
}
function switchToParcelView(parcelId, checkView) {
    closeBulkEditor();
    closeAdhocCreator();
    clearFindex();
    fLoad = true;
    shortenScreen()
    $('.search-window').addClass('hidden');
    if (fttime == 0) {
        $('#printPRC').hide()
        $('.sketch-details').hide()
        $('#exportreport').hide()
        $('#viewreport').hide();
        $('#customwms').hide()
        $('td.value.medtext.rightpad').hide()
        $('button.Commit_Change').hide()
        fttime = 1;
    }
    $('.parcel-details').removeClass('hidden');
    _sketchStringThrowError = false;
    parent_missing_msg = false;
    auditSorted = true;
    getParcel(parcelId, function () {
        if (clientSettings?.EnableAuditLock == '1') {
            $('.audit-lock').show();
            if (__DTR) $('a[category="digital-prc"]').css('padding-right', '45px');
            else $('a[category="digital-prc"]').css('padding-right', '25px');

            if (localStorage.getItem("auditLock") == "1") {
                $('.audit-lock').removeClass('audit-lock-unlock').addClass('audit-lock-lock');
                openCategory('digital-prc', 'Digital PRC');
                $('.tabs li').removeAttr('class');
                $('.tabs li[category="digital-prc"]').click();
                $('.tabs li[category="digital-prc"]').addClass('selected');
                $('.category-menu').hide();
            }
            else {
                $('.audit-lock').removeClass('audit-lock-lock').addClass('audit-lock-unlock');
                localStorage.setItem('auditLock', '0');
                openCategory('dashboard', 'Parcel Dashboard');
            }
        }
        else {
            $('.audit-lock').hide();
            if (__DTR) $('a[category="digital-prc"]').css('padding-right', '21px');
            else $('a[category="digital-prc"]').css('padding-right', '');
            localStorage.removeItem('auditLock');
            openCategory('dashboard', 'Parcel Dashboard');
        }

        $('#photouploader').attr('src', '/protected/tools/upload-photo.aspx#' + parcelId);
        hideMask();
        if (checkView)
            checkSchemaUpdates();
        activeParcel.StreetAddress ? document.title = activeParcel.KeyValue1 + ' -  ' + activeParcel.StreetAddress + ' : CAMA Cloud' : document.title = activeParcel.KeyValue1 + ' : CAMA Cloud';
    }, null, true);
    showMask();
    $('#kyf').show()
    var ss = getSearchStatus({ ParcelId: parcelId });
    if (ss.parcelIndex == -1) {
        $('.nav-parcel').attr('disabled', 'disabled');
    } else {
        if (ss.isFirst) {
            $('.nav-parcel-prev').attr('disabled', 'disabled');
        } else {
            $('.nav-parcel-prev').removeAttr('disabled');
        }
        if (ss.isLast) {
            $('.nav-parcel-next').attr('disabled', 'disabled');
        } else {
            $('.nav-parcel-next').removeAttr('disabled');
        }
    }
    setScreenDimensions();
}
function refreshSearch() {
    $('.current-filter .control').keypress(function (e) {
        if (e.keyCode === 13) {
            e.preventDefault();
            //doSearch();
            return false;
        }
    });
}

function switchToSearchView() {
    $('.search-window').removeClass('hidden');
    $('.parcel-details').addClass('hidden');
    setScreenDimensions();
    $('.gBlock').css({ top: $('.mDiv').height() + $('.hDiv').height() });
    $('.erow').removeClass('trSelected');
    document.title = 'Advanced Parcel Search - CAMA Cloud';
    window.location.hash = "/search/";
    refreshSearch();
    clearChildWindows();
}

function showSelectedTemplate(linkId) {
    try {
        var sel = $('.qc-selected-template');
        var tid = $(sel).val();
        if (tid == null) return false;
        var std = ($('option[value="' + tid + '"]', sel).attr('standard') != null);
        var template_name = $('option[value="' + tid + '"]', sel).html();
        if (template_name.includes("CustomQuery")) {
            template_name = template_name.trim().split("(CustomQuery)")[0];
            IsCustomSearch = true;
            getCustomQuery(tid);
            $('.templateName').show();
            $('.templateName').html(template_name.length > 25 ? template_name.substring(0, 25) + "..." : template_name);
            var open = document.createElement('a');
            open.innerHTML = 'Open';
            open.className = 'open-filter';
            $('.templateName').append(open);
            $(open).click(function () {
                loadCustomFilterselection(template_name, tid);
            });
        } else {            
            IsCustomSearch = false;
            $('.templateName').show();
            $('.templateName').html(template_name);            
            if (customTemplates[tid] && customTemplates[tid].MainOp != '') {
                if (customTemplates[tid].MainOp == 'AND') {
                    $('#myandorswitch').prop("checked", true);
                }
                else {
                    $('#myandorswitch').prop("checked", false);
                }
            }             
        }
        if (std) {
            switch (tid) {
                case 'PROPSEARCH':
                    loadFilters(tplPropertySearch);
                    break;
                case 'FIELDVISIT':
                    loadFilters(tplFieldVisitSearch);
                    break;
                case 'NBHD':
                    loadFilters(tplNbhdList);
                    break;
                case 'SYNCFAIL':
                    if (linkId == "sync-failure") tplSyncFailure[0].Value1 = "-1";
                    loadFilters(tplSyncFailure);
                    break;
            }
        } else {
            loadFilters(customTemplates[tid].Filters);
        }
        $('.filter-customfilter').hide();
        $('.qc-tabs').tabs("select", 0);
        $('.create-filter').hide();
        $('.current-filter').show();
    } catch (e) { }
    return false;
}

function showTemplateSaveForm() {
    $('.qc-saved-edit-name').show();
    $('.qc-tabs').tabs("select", 1);
    $('.qc-saved-name').val('');
    $('.qc-saved-save-name').show();
    $('.qc-saved-name').removeAttr('disabled');
    $('.qc-selected-template').hide();
    $('.template-control').hide();
    $('.qc-cancel-save').show();
    $('.qc-selected-template').val(null);
}

function cancelTemplateSaveForm() {
    //$('.qc-tabs').tabs("select", 1);
    $('.qc-saved-edit-name').hide();
    $('.qc-saved-name').val('');
    $('.qc-saved-save-name').hide();
    $('.qc-saved-name').attr('disabled', 'disabled');
    $('.qc-selected-template').show();
    $('.template-control').hide();
    $('.qc-cancel-save').hide();
    $('.qc-selected-template').val(null);
}

function reAssignDefaultTemplate() {
    tplPropertySearch = [
        new Filter("Quick Ref ID", "p.KeyValue1", "eq", "", null),
        new Filter("Property Number", "p.KeyValue2", "eq", "", null),
        new Filter("Property ID", "p.KeyValue3", "eq", "", null)
    ];
    tplFieldVisitSearch = [
        new Filter("Field Reviewed by", "p.ReviewedBy", "eq", "", null),
        new Filter("Reviewed On", "p.ReviewDate", "bw", "", "")
    ];
    tplNbhdList = [new Filter("Neighborhood", "nbhd.Number", "eq", "", null)];
    txtNbhd = [new Filter("Assignment Group", "pdata.CC_GROUP", "eq", "", "")]
    tplSyncFailure = [new Filter("Sync Failed", "p.FailedOnDownSyncStatus", "eq", "-1", null)]
}

function updateFilters() {
    for (var i = 0; i < currentSearchTemplate.Filters.length; i++) {
        var j = currentSearchTemplate.Filters[i];
        var form = $('.search-filter[filter-id="' + j.ID + '"]');
        var inputs = $('.value', form);
        j.Value1 = $(inputs[0]).val();
        if (inputs.length == 2) {
            j.Value2 = $(inputs[1]).val();
        }

    }
}

function saveQuery() {
    updateFilters();
    var name = $('.qc-saved-name').val();
    if (name.trim() == "") {
        alert('Please enter a name for the template.');
        return false;
    }
    var tid = $('.qc-selected-template').val();
    var filterString = currentSearchTemplate.toString(!saveOptions.templateOnly);
    var op = $('#myandorswitch').attr('checked') ? 'AND' : 'OR';
    if ((!filterString) && (filterString == '')) {
        alert('No filters available. Please choose one.');
        $('a[href="#qc-search"]').click();
        $('.filters-add-filter').click();
        return false;
    }
    if (tid == null)
        tid = 0;
    //    if (!saveOptions.isNew)
    //        filterString = '';

    var saveData = {
        QueryId: tid,
        Name: name,
        Operator:op,
        FilterData: filterString
    };

    $('.qc-save-control').attr('disabled', 'disabled');
    $('.qc-selected-template').addClass('component-loading');
    $qc('savesearchquery', saveData, function (resp) {
        if (resp.status == "OK") {
            loadQueries();
            cancelTemplateSaveForm();
            reAssignDefaultTemplate();
        } else {
            alert(resp.message);
            $('.qc-save-control').removeAttr('disabled');
            $('.qc-selected-template').removeClass('component-loading');
            reAssignDefaultTemplate();
        }
    }, function (err) {
        alert('Save failed! - ' + err);
        $('.qc-save-control').removeAttr('disabled');
        $('.qc-selected-template').removeClass('component-loading');
        reAssignDefaultTemplate();
    });
}

function loadQueries() {
    customTemplates = {};
    cancelTemplateSaveForm();
    $('.qc-save-control').attr('disabled', 'disabled');
    $('.qc-selected-template').addClass('component-loading');
    $qc('getsearchqueries', [], function (resp) {
        var queries = resp.data;
        for (x in queries) {
            var q = queries[x];
            var template = { Id: q.Id, Name: q.Name, IsCustom: q.IsCustom, MainOp: q.MainOp, Filters: [] };
            for (y in q.Filters) {
                var ff = q.Filters[y];
                var f = new Filter(ff.Label, ff.Field, ff.Operator, ff.Value1, ff.Value2);
                template.Filters.push(f);
            }
            customTemplates[q.Id] = template;
        }

        var utpl = $('optgroup[label="My Templates"]', '.qc-selected-template');
        $(utpl).html('');
        var tplhtml = '';
        for (x in customTemplates) {
            var tpl = customTemplates[x];
            var o = document.createElement('option');
            o.innerHTML = tpl.IsCustom ? tpl.Name + "(CustomQuery)" : tpl.Name;
            o.setAttribute('value', tpl.Id);
            o.setAttribute('style', 'text-overflow: ellipsis;white-space: nowrap;overflow: hidden;');
            $(utpl).append(o);
        }

        $('.qc-save-control').removeAttr('disabled');
        $('.qc-selected-template').removeClass('component-loading');
    }, function (err) {

        $('.qc-save-control').removeAttr('disabled');
        $('.qc-selected-template').removeClass('component-loading');
    });
}

function deleteTemplate() {
    if (!confirm('Are you sure you want to delete this TEMPLATE from your list?')) return false;
    var tid = $('.qc-selected-template').val();
    $('.filter-customfilter').hide();
    $('.templateName').hide();
    IsCustomSearch = false;
    CustQuery = "";
    $qc('deletesearchquery', {
        QueryId: tid
    }, function (resp) {
        if (resp.status == "OK") {
            loadQueries();
        }
    });
}

function resetRandom() {
    setRandom(100);
}

function setRandom(value) {
    $('.randomizer').val(value);
    $('.randomizer-value').html(value);
}

function doCustomSearch(Query, IsCust) {
    IsCustomSearch = IsCust;
    CustQuery = Query;
    doSearch();
}

function getCustomQuery(Id, callback) {
    $qc('getcustomquery', { QueryId: Id }, function (data) {
        CustQuery = data.joinStr + "^" + data.Condition.replace(/&lt;/g, '<').replace(/&gt;/g, '>') + "^" + data.filter.replace(/&lt;/g, '<').replace(/&gt;/g, '>')
        IsCustomSearch = true;
        if (callback) callback();
    }, function (err) {
        alert('An error occured while getting custom query - ' + err);
    });
}

function loadCustomFilterselection(name, queryId) {
    getCustomQuery(queryId, function () {
        IsCustomSearch = true;
        automask();
        lookupData = lookup;
        document.getElementById("CustomFilter").innerHTML = '<iframe style="width: 100%; height: 400px;border: none;" src="/protected/CustomFilter/Custom-filter.html?32" ></iframe>';
        $('#CustomQueryBuilder').data('lookupData', lookup);
        document.getElementById('filter-name').value = name;
        var jsonstring = CustQuery.split("^")[2];
        var filterdata = JSON.parse(jsonstring);
        $('#CustomQueryBuilder').show();
        document.getElementById('Issave').value = true;
        document.getElementById('filterID').value = queryId;
        $('#Issave').attr('jsondata', jsonstring);
        PopulateFilterIntoTree(filterdata, name);
    });
}

function PopulateFilterIntoTree(data, name) {
    filterData = data;
    Issavedfilter = true;
}

function automask() {
    $('body').css('overflow', 'hidden');
    $('.windowMask').height($(document).height());
    $('.windowMask').show();
}

function closeCustmsg() {
    hideautomask();
    oldTempName = "";
    IsCustomSearch = false;
    $('#CustomQueryBuilder, .templateName').hide();
    $('.qc-save-query').show();
}

function hideautomask() {
    $('body').css('overflow', 'auto');
    $('.windowMask').hide();
}

function load_Filter() {
    document.getElementById("CustomFilter").innerHTML = '<object type="text/html" data="CustomFilter.html" ></object>';
}

function preventExponentAndPlus(event) {
    let key = event.key;
    let isNumericKey = (key >= '0' && key <= '9') || (key >= 'NumPad0' && key <= 'NumPad9');

    // Check if the key is 'e', '+', or not a numeric key, Backspace, or Delete
    if (key === 'e' || key === '+' || (!isNumericKey && key !== 'Backspace' && key !== 'Delete')) {
        event.preventDefault();
    }
}

function checkyear(el) {
    let inputType = $(el).attr('fieldid');
    if ((inputType && datafields[inputType].InputType == 10) || $(el).attr('tp') == 'num') {
        if ($(el).val().length > 10) {
            $(el).val($(el).val().slice(0, 4));
        }
        /*checkyear1Input.off('keydown');
        checkyear1Input.on('keydown', function (event) {
            let key = event.key;
            let isNumericKey = (key >= '0' && key <= '9') || (key >= 'NumPad0' && key <= 'NumPad9');
            if (key === 'e' || key === '+' || (!isNumericKey && key !== 'Backspace' && key !== 'Delete')) {
                event.preventDefault();
            }
        });*/
    }

    let selectedOption = document.getElementById("mySelect").options[document.getElementById("mySelect").selectedIndex];
    if (selectedOption.getAttribute("fieldid")) {
        let fieldidValue = selectedOption.getAttribute("fieldid");
        if (datafields[fieldidValue].InputType == 10) {
            let selectedInput1 = $(".values1 .selected-input");
            if (selectedInput1.val().length > 4) {
                selectedInput1.val(selectedInput1.val().slice(0, 4));
            }

            if ($("#myOperator").val() == 'bw') {
                let value2Input = $(".value2 .selected-input");
                if (value2Input.val() !== undefined) {
                    value2Input.val(value2Input.val().replace(/\D/g, ""));
                    if (value2Input.val().length > 4) {
                        value2Input.val(value2Input.val().slice(0, 4));
                    }
                }
            }
        }
    }
}