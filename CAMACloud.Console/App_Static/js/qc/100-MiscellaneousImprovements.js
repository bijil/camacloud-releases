﻿function loadCustomFunctionsOnAppLoad(callBack) {
    var fn = clientSettings.LoadMethodAfterAppLoad;
    if (fn) {
        fn = window[fn];
        if (typeof (fn) == "function") {
            fn(callBack);
        }
        else {
            if (clientSettings.LoadMethodAfterAppLoad !== 'loadValidationTablesForNS') {

                alert('Invalid client setting value for LoadMethodAfterAppLoad.');
            }

            callBack();
        }
    } else
        callBack();
}
function loadValidationTablesForMI(callback) {
    var arr = [];
    ccma.MIFieldValidations = {};
    getData("select * from ccv_tAA_Misc_Catalog ", [], function (results) {
        for (var x in results) {
            var n = results[x]
            arr.push(n);
        }
        ccma.MIFieldValidations['MICATALOG'] = arr;

        if (callback) callback();
    }, null, false, function () {
        if (callback) callback();
    });
}
function LoadRelatedFieldsForMI(field, expType, expression) {

    var MIfields = [], MIrelfields = [];
   // var MIallFields = Object.keys(datafields).map(function (x) { return datafields[x] }).filter(function (x) { return x.CategoryId == field.CategoryId });
    var MIallFields = Object.keys(datafields)
        .map(function (key) {
            return datafields[key];
        })
        .filter(function (x) {
            return (x.CategoryId == field.CategoryId &&
                (x['UI_Settings'] &&
                    x['UI_Settings'].MIFields &&
                    x['UI_Settings'].MIFields === "true") && x.Name != "RollYear");
        });
    if (expType == 'LookupQuery') {
        MIallFields.forEach(function (x) {
            if (ccma.UI.RelatedFields[field.Id] == undefined) ccma.UI.RelatedFields[x.Id] = [];
            if (!_.contains(ccma.UI.RelatedFields[field.Id], x.Id) && (x.Id != field.Id)) ccma.UI.RelatedFields[field.Id].push(x.Id);
        });
    }
    else if (expType == 'CalculationExpression') {
        MIfields = expression.match(/([a-zA-Z_][a-zA-Z0-9_]+)/g)
        MIrelfields = MIallFields.filter(function (x) { return _.contains(MIfields, x.Name) });
        MIrelfields.forEach(function (x) {
            if (ccma.UI.CalcRelatedFields[x.Id] == undefined) ccma.UI.CalcRelatedFields[x.Id] = [];
            if (!_.contains(ccma.UI.CalcRelatedFields[x.Id], field.Id) && (x.Id != field.Id)) ccma.UI.CalcRelatedFields[x.Id].push(field.Id);
        });
    }
}
function getConditionalValidationForMI(field, source) {
    var validations = [];
    var MICode;
    var MIddl;
    var MIDecimal;
    var MIFormula;
    var MILabel;
    var MIRequire;
    var fieldname = field.Name;
    var col = fieldname.match(/\d+/g);

    var MICodeid;
    var RollYear;

    if (source.MICodeID != null && source.RollYear != null) {
        MICodeid = source.MICodeID;
        RollYear = source.RollYear;
        var MIFieldValidations = ccma.MIFieldValidations["MICATALOG"].filter(function (thisObject) {
            return (thisObject.MICodeID == MICodeid && thisObject.RollYear == RollYear);
        }).shift();
    } else {
        //RollYear = 2017;
        var MIFieldValidations = ccma.MIFieldValidations["MICATALOG"].filter(function (thisObject) {
            return (thisObject.RollYear == RollYear);
        }).shift();
    }

    for (x in MIFieldValidations) {
        MICode = MIFieldValidations.MICode;
        switch (col[0]) {
            case '01':
                MIddl = MIFieldValidations.C01;
                MIDecimal = MIFieldValidations.D01;
                MIFormula = MIFieldValidations.F01;
                MILabel = MIFieldValidations.L01;
                MIRequire = MIFieldValidations.S01;
                break;
            case '02':
                MIddl = MIFieldValidations.C02;
                MIDecimal = MIFieldValidations.D02;
                MIFormula = MIFieldValidations.F02;
                MILabel = MIFieldValidations.L02;
                MIRequire = MIFieldValidations.S02;
                break;
            case '03':
                MIddl = MIFieldValidations.C03;
                MIDecimal = MIFieldValidations.D03;
                MIFormula = MIFieldValidations.F03;
                MILabel = MIFieldValidations.L03;
                MIRequire = MIFieldValidations.S03;
                break;
            case '04':
                MIddl = MIFieldValidations.C04;
                MIDecimal = MIFieldValidations.D04;
                MIFormula = MIFieldValidations.F04;
                MILabel = MIFieldValidations.L04;
                MIRequire = MIFieldValidations.S04;
                break;
            case '05':
                MIddl = MIFieldValidations.C05;
                MIDecimal = MIFieldValidations.D05;
                MIFormula = MIFieldValidations.F05;
                MILabel = MIFieldValidations.L05;
                MIRequire = MIFieldValidations.S05;
                break;
            case '06':
                MIddl = MIFieldValidations.C06;
                MIDecimal = MIFieldValidations.D06;
                MIFormula = MIFieldValidations.F06;
                MILabel = MIFieldValidations.L06;
                MIRequire = MIFieldValidations.S06;
                break;
            case '07':
                MIddl = MIFieldValidations.C07;
                MIDecimal = MIFieldValidations.D07;
                MIFormula = MIFieldValidations.F07;
                MILabel = MIFieldValidations.L07;
                MIRequire = MIFieldValidations.S07;
                break;
            case '08':
                MIddl = MIFieldValidations.C08;
                MIDecimal = MIFieldValidations.D08;
                MIFormula = MIFieldValidations.F08;
                MILabel = MIFieldValidations.L08;
                MIRequire = MIFieldValidations.S08;
                break;
            case '09':
                MIddl = MIFieldValidations.C09;
                MIDecimal = MIFieldValidations.D09;
                MIFormula = MIFieldValidations.F09;
                MILabel = MIFieldValidations.L09;
                MIRequire = MIFieldValidations.S09;
                break;
            case '10':
                MIddl = MIFieldValidations.C10;
                MIDecimal = MIFieldValidations.D10;
                MIFormula = MIFieldValidations.F10;
                MILabel = MIFieldValidations.L10;
                MIRequire = MIFieldValidations.S10;
                break;
            case '11':
                MIddl = MIFieldValidations.C11;
                MIDecimal = MIFieldValidations.D11;
                MIFormula = MIFieldValidations.F11;
                MILabel = MIFieldValidations.L11;
                MIRequire = MIFieldValidations.S11;
                break;
            case '12':
                MIddl = MIFieldValidations.C12;
                MIDecimal = MIFieldValidations.D12;
                MIFormula = MIFieldValidations.F12;
                MILabel = MIFieldValidations.L12;
                MIRequire = MIFieldValidations.S12;
                break;

        }
        validations.push({ txtDDL: MIddl, txtDecimal: MIDecimal, txtFormula: MIFormula, txtLabel: MILabel, txtRequire: MIRequire });
        return validations;
    }

}
function applyConditionalValidationForMI(field, source) {
    var fieldname = field.Name;
    var stringcol = fieldname.match(/[a-z]+/gi);
    var numcol = fieldname.match(/\d+/g); //match(/[a-z]+/gi)
    if (numcol != null) {
        var validations = getConditionalValidationForMI(field, source);
        if (validations && validations.length > 0) {
            validations = validations[0];
            var cId = field.CategoryId;
            var cat = $('.category-page[categoryid="' + cId + '"]')
            var displayfield = $('.parcel-field-values tr', cat);
            var displaylabel;
            var displayrequire;

            for (var i = 0; i < displayfield.length; i++) {

                var s = displayfield[i].children[1];
                var s1 = s.getAttribute("fieldid");
                var checked = $('.value', displayfield[i]);
                if (checked.length > 0) {
                    if (s1 == field.Id) {
                        if (validations.txtLabel != null) {
                            displayfield[i].style.display = "table-row";
                            displaylabel = $('.fieldDisplayLabel', displayfield[i]);
                            displaylabel.text(validations.txtLabel);
                            if (__DTR) {
                                $(".label span:first-child", displayfield[i]).text(validations.txtLabel);
                            }

                            field.CalculationExpression = null;
                            field.LookupQuery = null;

                            if (validations.txtDecimal != "") {
                                field.NumericScale = validations.txtDecimal;
                            }

                            if (validations.txtRequire != 0) {
                                displayrequire = $('.required', displayfield[i]);
                                displayrequire.removeAttr("style");
                                displayrequire.attr({ "style": "display: inline" });

                            }
                            else {
                                displayrequire = $('.required', displayfield[i]);
                                displayrequire.removeAttr("style");
                            }
                            if (validations.txtFormula != null) {
                                if (stringcol == "V") {
                                    var Formula = decodeHTML(validations.txtFormula).replace(/[\[\]']+/g, '');
                                    //field.CalculationExpression = Formula;
                                    field.DoNotShow = false;
                                    displayfield[i].style.display = "table-row";
                                    $('.value input', displayfield[i]).attr('readonly', 'readonly');
                                    var MIexpType = 'CalculationExpression';

                                    var MIFormulaFields
                                    if (typeof Formula === 'string') {
                                        var Formulafield = Formula.match(/([a-zA-Z_][a-zA-Z0-9_]+)/g);

                                        if (Formulafield !== null && typeof Formulafield !== 'undefined' && Array.isArray(Formulafield)) {
                                            var ffieldid = Formulafield.filter((item, index) => Formulafield.indexOf(item) === index);
                                            ffieldid.forEach(function (y) {
                                                if (y.toUpperCase() != "IF") {

                                                    MIFormulaFields = Object.keys(datafields)
                                                        .map(function (key) {
                                                            return datafields[key];
                                                        })
                                                        .filter(function (x) {
                                                            return (x.CategoryId == field.CategoryId &&
                                                                (x['UI_Settings'] &&
                                                                    x['UI_Settings'].MIFields &&
                                                                    x['UI_Settings'].MIFields === "true") && x.Name == y);
                                                        });

                                                    if (MIFormulaFields.length == 0) {

                                                        MIFormulaFields = Object.keys(datafields)
                                                            .map(function (key) {
                                                                return datafields[key];
                                                            })
                                                            .filter(function (x) {
                                                                return (x.CategoryId == field.CategoryId &&
                                                                    (x['UI_Settings'] &&
                                                                        x['UI_Settings'].MIFields &&
                                                                        x['UI_Settings'].MIFields === "true") && x.Label == y);
                                                            });


                                                        if (MIFormulaFields.length > 0) {
                                                            if (y != MIFormulaFields[0].Name) {
                                                                Formula = Formula.replaceAll(y, MIFormulaFields[0].Name)
                                                            }
                                                        }
                                                    }
                                                    else {
                                                        var Formulavalidations = getConditionalValidationForMI(MIFormulaFields[0], source);
                                                        var fname = MIFormulaFields[0].Name.match(/\d+/g);
                                                        if (Formulavalidations[0].txtDDL != 0) {
                                                            Formula = Formula.replaceAll('V' + fname, 'MIChoiceID' + fname)
                                                        }
                                                    }
                                                }

                                            });
                                        }
                                    }

                                    var MIexpression = Formula;
                                    field.CalculationExpression = Formula;
                                    LoadRelatedFieldsForMI(field, MIexpType, MIexpression);

                                }
                                else {
                                    field.CalculationExpression = null;
                                    field.DoNotShow = true;
                                    displayfield[i].style.display = "none";
                                    displayrequire = $('.required', displayfield[i]);
                                    displayrequire.removeAttr("style");
                                }

                            }
                            else if (validations.txtDDL != 0) {

                                if (stringcol == "MIChoiceID") {
                                    displayfield[i].style.display = "table-row";
                                    field.DoNotShow = false;
                                    field.LookupQuery = "select ItemValue, ItemName from ccv_tAA_Misc_CatalogChoice_{parcel.CC_MiscRollYear} where FieldNum =" + numcol + " and MICodeID = '{MICodeID}' and RollYear = '{RollYear}'  order by ItemName";
                                }
                                else {

                                    field.LookupQuery = null;
                                    displayfield[i].style.display = "none";
                                    field.DoNotShow = true;
                                    displayrequire = $('.required', displayfield[i]);
                                    displayrequire.removeAttr("style");
                                }
                            }
                            else {
                                if (stringcol == "V") {
                                    field.CalculationExpression = null;                                  
                                    displayfield[i].style.display = "table-row";
                                    field.DoNotShow = false;
                                }
                                else {
                                    field.LookupQuery = null;
                                    displayfield[i].style.display = "none";
                                    field.DoNotShow = true;
                                    displayrequire = $('.required', displayfield[i]);
                                    displayrequire.removeAttr("style");
                                }
                            }
                        }
                        else {
                            field.CalculationExpression = null;                           
                            displayfield[i].style.display = "none";
                            field.DoNotShow = true;
                            displayrequire = $('.required', displayfield[i]);
                            displayrequire.removeAttr("style");
                        }

                        if (displayfield[i].style.display === "none") {                          
                            $('.value[fieldid="' + field.Id + '"] .trackchanges').val("");
                            $('.value[fieldid="' + field.Id + '"] .trackchanges').trigger("blur");
                        }
                    }
                }
            }
        }
    }
    else {
        //if (fieldname == "RollYear") {
           // field.LookupTable = "$QUERY$";
          //  field.LookupQuery = "select RollYear from ccv_tAA_Misc_Catalog group by RollYear";
       // }
        if (fieldname == "MICodeID") {
           // field.LookupTable = "$QUERY$";
            //field.LookupQuery = "select MICodeID, MICode || ' - ' || Description Description from tAA_Misc_Catalog where  RollYear = case when '{RollYear} ' = 'null' then '{parcel.RollYear} ' else '{RollYear}' end";
            var MIexpType = 'LookupQuery';
            var MIexpression = field.LookupQuery;

            LoadRelatedFieldsForMI(field, MIexpType, MIexpression);
        }


    }

}
function IF(value, exp1, exp2) {
    return value ? exp1 : exp2;
}
