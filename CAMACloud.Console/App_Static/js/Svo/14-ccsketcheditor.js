﻿//    DATA CLOUD SOLUTIONS, LLC ("COMPANY") CONFIDENTIAL 
//    Unpublished Copyright (c) 2010-2013 
//    DATA CLOUD SOLUTIONS, an Ohio Limited Liability Company, All Rights Reserved.

//    NOTICE: All information contained herein is, and remains the property of COMPANY.
//    The intellectual and technical concepts contained herein are proprietary to COMPANY
//    and may be covered by U.S. and Foreign Patents, patents in process, and are protected
//    by trade secret or copyright law. Dissemination of this information or reproduction
//    of this material is strictly forbidden unless prior written permission is obtained
//    from COMPANY. Access to the source code contained herein is hereby forbidden to
//    anyone except current COMPANY employees, managers or contractors who have executed
//    Confidentiality and Non-disclosure agreements explicitly covering such access.</p>

//    The copyright notice above does not evidence any actual or intended publication
//    or disclosure of this source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
//    DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
//    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
//    IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
//    OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
//    TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
//    ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

/*
** CAMA Cloud Sketch Editor - Version 1.5
** HTML5 Sketch Editor for CAMA Cloud Sketches - Desktop & Mobile Version
** Developer: Sarath
*/
var CAMACloud = {}
if (CAMACloud === undefined) CAMACloud = {};

var DEFAULT_PPF = 10;
var ARC_SIGN = 1;
var DISTANCE_UNIT = ''
var AREA_UNIT = 'sq. ft'
CAMACloud.Sketching = {
    MODE_DEFAULT: 0x0000,
    MODE_NEW: 0x0001,
    MODE_EDIT: 0x0002,
    MODE_MOVE: 0x0003,
    MODE_ERASER: 0x0100,
    MODE_SPLITTER: 0x0110,
    MODE_SCROLL: 0x9001,
    MODE_ZOOM: 0x9010,
    MODE_SCRIBBLE: 0x8001,
    MODE_ERASESCRIBBLE: 0x8010
};
var windowsTouch = /Win/.test(window.navigator.userAgent) ? (window.navigator.msMaxTouchPoints || ("ontouchstart" in window)) ? true : false : false;
CAMACloud.Sketching.Formatters = {};
CAMACloud.Sketching.Encoders = {};
CAMACloud.Sketching.roundFactor = null;

function CCSketchEditor(canvas, formatter) {

    console.log('Initializing Sketch Editor');

    if (!formatter) formatter = CAMACloud.Sketching.Formatters.CCSketch;
    this.showOrigin = true;
    this.formatter = formatter;
    this.encoder = null;
    var _mode = CAMACloud.Sketching.MODE_DEFAULT;
    var editor = this;
    this.scale = 1;
    this.origin = new PointX(0, 0);
    this.offset = new PointX(0, 0);
    this.showStatus = function (message) { return false; }
    this.setZoomValue = function (z) { return false; }
    var ccse;
    this.drawing = null;
    this.brush = null;
    this.width = 500;
    this.height = 400;
    this.origin = RoundX(0, 0);
    this.actualOrigin = RoundX(0, 0);
    this.refOrigin = new PointX(this.origin.x, this.origin.y);
    this.keypadActive = false;
    this.vectorSelector = null;
    this.sketchSelector = null;
    this.viewOnly = false;
    this.saving = false;
    this.sketchMovable = false;
    this.displayNotes = true;
    this.isPreview = false;
    this.external = {
        datafields: {},
        lookup: {},
        fieldCategories: {}
    }

    this.setFormatter = function (ftr) { formatter = ftr; this.formatter = ftr; }

    this.minZoom = 0.05;
    this.maxZoom = 2
    this.__defineGetter__("zoomRange", function () { return this.maxZoom - this.minZoom; });

    this.pen = false;
    this.ink = false;
    this.currentSketch = null;
    this.currentVector = null;
    this.currentNode = null;
    this.currentSketchNote = null;
    this.vectors = [];
    this.sketches = [];
    this.angleLock = false;
    this.keypad = null;

    this.__defineGetter__("mode", function () { return _mode; });
    this.__defineSetter__("mode", function (val) {
        _mode = val;
    });

    this.onModeChange = function (mode, currentVector, currentNode, currentSketchNote) { }
    this.onDrawLine = function (p1, p2, v) { }
    this.onClose = function () { }
    this.onSave = function (data) { }

    // Initialize Canvas

    function init() {
        if (typeof canvas == "string") {
            ccse = $(canvas)[0];
        } else {
            ccse = canvas;
        }
        if (!ccse) {
            return;
        }
        editor.drawing = ccse.getContext("2d");
        editor.width = $(ccse).width();
        editor.height = $(ccse).height();
        editor.origin = GetOrigin(editor, editor.formatter.originPosition)
        editor.refOrigin = new PointX(editor.origin.x, editor.origin.y);
        editor.actualOrigin = new PointX(editor.origin.x, editor.origin.y);
        editor.drawing.imageSmoothingEnabled = true;
        editor.brush = new Brush(editor.drawing, editor);

        ccse.editor = editor;
        setCanvasEvents(ccse, editor);
    }

    this.refresh = function () {
        editor.scale = 1;
        editor.width = $(ccse).width();
        editor.height = $(ccse).height();
        editor.origin = GetOrigin(editor, editor.formatter.originPosition)
        editor.refOrigin = new PointX(editor.origin.x, editor.origin.y);
        editor.actualOrigin = new PointX(editor.origin.x, editor.origin.y);
        editor.brush = new Brush(editor.drawing, editor);
    }

    this.startNewVector = function () {
        if (_mode == CAMACloud.Sketching.MODE_DEFAULT) {
            _mode = CAMACloud.Sketching.MODE_NEW;
            this.isClosed = false;
            this.raiseModeChange();
        }
    }

    this.raiseModeChange = function () {
        if (this.onModeChange)
            this.onModeChange(_mode, this.currentVector, this.currentNode, this.currentSketchNote);
    }

    // Editor Functions
    this.clearCanvas = function () {
        this.drawing.clearRect(0, 0, ccse.width, ccse.height);
    }

    this.zoomPercent = function (zp) {
        var z = (zp / 100 * editor.zoomRange) + editor.minZoom;
        editor.zoom(z);
    }

    this.zoom = function (z, p) {
        z = editor.normalizeScale(z);
        editor.scale = z;
        editor.setZoomValue(Math.round(z * 100));
        editor.brush.setScale(this.scale);
        editor.render();
    }

    this.setZoom = function (z) {

    }

    this.toDataURL = function () {
        return ccse.toDataURL();
    }

    this.undo = function () {
        if (this.currentVector != null)
            if (this.currentVector.endNode) {
                if (this.currentVector.startNode && (this.currentVector.endNode == this.currentVector.startNode)) {
                    var sn = this.currentVector.startNode;
                    this.currentVector.endNode = null;
                    this.currentVector.startNode = null;
                    delete sn;

                } else {
                    var nn = this.currentVector.endNode;
                    var pp = nn.prevNode;
                    this.currentVector.endNode = pp;
                    pp.nextNode = null;
                    delete nn;
                    $('.ccse-meter-length').val(sRound(this.currentVector.endNode.length || 0))
                    $('.ccse-meter-angle').val('');
                }
                this.currentNode = this.currentVector.endNode;
                this.render();
            }
    }

    this.normalizeScale = function (z) {
        if (z > editor.maxZoom) {
            return editor.maxZoom;
        } else if (z < editor.minZoom) {
            return editor.minZoom;
        } else {
            return z;
        }
    }

    this.resetOrigin = function () {
        this.origin = this.actualOrigin.copy();
        this.refOrigin = this.origin.copy();
        //this.render();
    }

    this.panOrigin = function (sb, originPosition, scale) {
        if (!scale)
            scale = this.scale;
        if (originPosition == "topRight") {
            x = -sb.xmax * DEFAULT_PPF * scale;
            y = -sb.ymax * DEFAULT_PPF * scale;
        }
        else {
            x = -sb.xmin * DEFAULT_PPF * scale;
            y = -sb.ymin * DEFAULT_PPF * scale
        }
        this.offset = new PointX(x, y);
        this.offset = RoundX(this.offset.x, this.offset.y, scale);
        this.origin.setOffset(this.offset);
        this.refOrigin = this.origin.copy();
        this.render();
    }

    this.pan = function (x, y) {
        this.offset = new PointX(x, y);
        this.origin.setOffset(this.offset);
        this.render();
    }

    this.resetPan = function (x, y) {
        this.origin = this.refOrigin.copy();
        this.render();
    }

    this.toggleSketchMovable = function () {
        this.sketchMovable = !this.sketchMovable;
        this.render();
    }

    this.resetSketchMovable = function () {
        if (this.sketchMovable) {
            this.sketchMovable = false;
            $('.ccse-sketch-move').html('Move');
            this.render();
        }
    }

    this.panUp = function () { editor.pan(0, 50); }
    this.panDown = function () { editor.pan(0, -50); }
    this.panLeft = function () { editor.pan(-50, 0); }
    this.panRight = function () { editor.pan(50, 0) }
    this.panReset = function () { editor.resetPan(); }

    this.attachPanControl = function (cpan) {
        cpan.panLeft = editor.panLeft;
        cpan.panRight = editor.panRight;
        cpan.panUp = editor.panUp;
        cpan.panDown = editor.panDown;
        cpan.panReset = editor.panReset;
    }

    this.attachZoomControl = function (czoom) {
        czoom.zoom = editor.zoomPercent;
        editor.setZoom = czoom.setZoom;
    }

    this.attachKeypad = function (ckeypad) {
        editor.keypad = ckeypad;
    }

    this.refreshControlList = function () {
        if (!this.sketchSelector || !this.vectorSelector) return false;

        if (this.sketchSelector) {
            this.sketchSelector.html('');
            var nsel = document.createElement('option');
            nsel.innerHTML = '-- Select --';
            nsel.setAttribute('value', '');
            this.sketchSelector[0].appendChild(nsel);
        }

        if (this.vectorSelector) {
            this.vectorSelector.html('');
            var nsel = document.createElement('option');
            nsel.innerHTML = '-- Select Vector --';
            nsel.setAttribute('value', '');
            this.vectorSelector[0].appendChild(nsel);
        }
        for (var s in this.sketches) {
            var sk = this.sketches[s];

            if (this.sketchSelector) {
                if (this.sketchSelector.length > 0) {
                    var opt = document.createElement('option');
                    opt.innerHTML = sk.label || ('&lt;No Label&gt;' + sk.uid);
                    opt.setAttribute('value', sk.uid);
                    if (this.currentSketch != null) {
                        if (sk.uid == this.currentSketch.uid) {
                            opt.setAttribute('selected', 'selected');
                        }
                    }
                    this.sketchSelector[0].appendChild(opt);
                }
            }
        }

        $(this.sketchSelector).unbind('change');
        $(this.sketchSelector).bind('change', function () {
            editor.zoom(1);
            event.preventDefault();
            editor.loadVectorSelector(this);
            var sb = editor.sketchBounds();
            var ss = editor.screenBounds();
            var gg, hh = 1, pp, xx = 1;
            var ysketch = sb.ymax + Math.abs(sb.ymin);
            var yscreen = ss.uy + Math.abs(ss.ly);
            var xsketch = sb.xmax + Math.abs(sb.xmin);
            var xscreen = ss.ux + Math.abs(ss.lx);
            if (ysketch > yscreen) {
                gg = yscreen / ysketch;
                hh = Math.ceil(gg / .05) * .05;
            }
            if (xsketch > xscreen) {
                pp = xscreen / xsketch;
                xx = Math.ceil(pp / .05) * .05;
            }
            if (xx > hh) editor.zoom((hh - .1))
            else editor.zoom((xx - .15))
            if (ysketch == 0 && xsketch == 0)
                editor.zoom(1);
            editor.resetOrigin();
            editor.panOrigin(sb, editor.formatter.originPosition);
            editor.mode = CAMACloud.Sketching.MODE_DEFAULT;
            editor.raiseModeChange();
            editor.render();


        });



        $(this.vectorSelector).unbind('change');
        $(this.vectorSelector).bind('change', function () {
            event.preventDefault();
            var oneSelected = false;
            for (var x in editor.currentSketch.vectors) {
                var v = editor.currentSketch.vectors[x];
                if ($(this).val() == v.uid) {
                    editor.currentVector = v;
                    editor.currentNode = null;

                    if ((v.toString() == "")) {
                        editor.promptAndCreateVector();
                    } else if (editor.currentVector.isClosed) {
                        editor.mode = CAMACloud.Sketching.MODE_EDIT;
                    } else {
                        editor.promptAndContinueVector();
                    }
                    oneSelected = true;
                }
            }
            if (!oneSelected) {
                editor.currentVector = null;
                editor.mode = CAMACloud.Sketching.MODE_DEFAULT;
            }
            editor.raiseModeChange();
            editor.render();
        });

    }

    this.loadVectorSelector = function (ss, cv) {
        var oneSelected = false;

        if (editor.vectorSelector) {
            editor.vectorSelector.html('');
            var nsel = document.createElement('option');
            nsel.innerHTML = '-- Select --';
            nsel.setAttribute('value', '');
            editor.vectorSelector[0].appendChild(nsel);
        }

        for (var x in editor.sketches) {
            var s = editor.sketches[x];
            if (s.uid == $(ss).val()) {
                editor.currentSketch = s;
                editor.currentVector = cv;
                editor.currentNode = null;
                oneSelected = true;
                for (var x in s.vectors) {
                    s.vectors[x].render();
                    if (editor.vectorSelector) {
                        if (editor.vectorSelector.length > 0) {
                            var v = s.vectors[x];
                            var opt = document.createElement('option');
                            opt.innerHTML = v.label || ('&lt;No Label&gt;' + v.uid);
                            opt.setAttribute('value', v.uid);
                            if (editor.currentVector != null) {
                                if (v.uid == editor.currentVector.uid) {
                                    opt.setAttribute('selected', 'selected');
                                }
                            }
                            editor.vectorSelector[0].appendChild(opt);
                        }
                    }
                }


            }




        }
        if (!oneSelected) {
            editor.currentSketch = null;
        }
    }

    this.promptAndCreateVector = function () {
        messageBox('The selected segment does not have a sketch definition. Proceed to draw a new one? ', ["Yes", "No"], function () {
            messageBox('Tap on any point to start a new sketch segment.', function () {
                editor.mode = CAMACloud.Sketching.MODE_DEFAULT;
                $('.ccse-meter-length').val('');
                $('.ccse-meter-angle').val('');
                editor.startNewVector();
                if (editor.currentVector)
                    editor.currentVector.startNode = null;
                return;
            })
        });
    }

    //this.promptAndContinueVector = function (callback) {
    //    messageBox('The selected segment is not a closed shape. Do you want to continue drawing? ', ["Yes", "No"], function () {
    //        messageBox('Tap on any point to continue drawing.', function () {
    //            console.log("sadf");
    //            editor.currentNode = editor.currentVector.endNode  ;
    //            editor.mode = CAMACloud.Sketching.MODE_NEW;
    //            editor.raiseModeChange();
    //            if (callback) callback();
    //        })
    //    }, function () {
    //        editor.mode = CAMACloud.Sketching.MODE_EDIT;
    //        editor.raiseModeChange();
    //    });
    //}

    this.promptAndContinueVector = function (callback) {
        messageBox('The selected segment is not a closed shape. Do you want to continue drawing? ', ["Yes", "No"], function () {
            messageBox('Tap on any point to continue drawing.', function () {
                editor.currentNode = editor.currentVector.endNode;
                editor.mode = CAMACloud.Sketching.MODE_NEW;
                editor.raiseModeChange();
                if (callback) callback();
            })
        }, function () {
            editor.mode = CAMACloud.Sketching.MODE_EDIT;
            editor.raiseModeChange();
        });
    }

    this.createNewSketchNote = function () {
        input('', "New Sketch Note:", function (newnote) {
            var vnum = 0;
            if (editor.currentSketch.notes.length > 0) {
                vnum = editor.currentSketch.notes.length;
            }
            var id = editor.currentSketch.uid + "/" + vnum;
            var note = new Annotation(editor, editor.currentSketch, newnote, new PointX(0, 0), new PointX(0, 0), new PointX(100, 100), id.toString());
            note.isModified = true;
            note.newRecord = true;
            note.clientId = ccTicks().toString();
            editor.currentSketch.notes.push(note);
            editor.render();
        }, function () { });
    };
    this.createNewVector = function () {
        var labelselectionLookup = clientSettings['SketchLabelLookup'] || editor.currentSketch.config.SketchLabelLookup;
        var options = null;
        var labelQuestion = 'Enter label:';
        if (labelselectionLookup) {
            if (editor.external.lookup[labelselectionLookup] === undefined) {
                input('There are no values listed in ' + labelselectionLookup + ' lookup');
                return;
            }
            labelQuestion = 'Select the type of segment:';
            options = editor.external.lookup[labelselectionLookup];
        }

        input(labelQuestion, options, function (newlabel) {
            if (newlabel != "") {
                messageBox('Tap on any point to start the new sketch segment.', function () {

                    var vnum = 0;
                    if (editor.currentSketch.vectors.length > 0) {
                        vnum = editor.currentSketch.vectors[editor.currentSketch.vectors.length - 1].index + 1;
                    }

                    var newv = new Vector(editor, editor.currentSketch);
                    newv.uid = editor.currentSketch.uid + "/" + vnum;
                    newv.index = vnum;
                    var lbldesc = labelselectionLookup && options[newlabel] ? '-' + options[newlabel].Name : '';
                    newv.label = '[' + vnum + '] ' + newlabel + lbldesc;
                    newv.name = newlabel;
                    newv.clientId = ccTicks().toString();
                    newv.newRecord = true;
                    newv.header = newlabel + (editor.currentSketch.config.NewVectorHeaderSuffix || editor.formatter.newVectorHeaderSuffix || ':');
                    editor.currentSketch.vectors.push(newv);
                    editor.currentVector = newv;
                    editor.loadVectorSelector(editor.sketchSelector, newv);
                    editor.mode = CAMACloud.Sketching.MODE_DEFAULT;
                    editor.startNewVector();
                    if (editor.currentVector)
                        editor.currentVector.startNode = null;
                    editor.currentVector.isModified = true;
                    $('.ccse-meter-length').val('')
                    $('.ccse-meter-angle').val('');
                    return;
                })
            }
        }, function () {
            // do nothing on cancel
        });

    }

    this.createNewVectorSegment = function () {
        messageBox('Tap on any point to start the new vector segment.', function () {
            var vnum = 0;
            var counter = 0;
            var newlabel = editor.currentVector.label.substr(editor.currentVector.label.indexOf(']') + 1);
            var vuid = editor.currentVector.uid;
            if (vuid.indexOf("/") > -1) vuid = vuid.substr(0, vuid.indexOf("/"));

            for (var vi in editor.currentSketch.vectors) {
                var v = editor.currentSketch.vectors[vi];
                var vnum2 = v.uid.indexOf('/') > -1 ? parseInt(v.uid.substr(v.uid.indexOf('/') + 1)) : 0;
                var vnum1 = parseInt(vi || 0) + 1;
                if (vnum1 > vnum) vnum = vnum1;
                if (vnum2 > vnum) vnum = vnum2;
            }
            if (editor.currentSketch.vectors.length > 0) {
                counter = editor.currentSketch.vectors[editor.currentSketch.vectors.length - 1].index + 1;
            }

            var newv = new Vector(editor, editor.currentSketch);
            newv.uid = vuid + "/" + vnum;
            newv.index = counter;
            newv.label = '[' + counter + '] ' + newlabel
            newv.clientId = ccTicks().toString();
            newv.header = newlabel + (editor.currentSketch.config.NewVectorHeaderSuffix || editor.formatter.newVectorHeaderSuffix || ':');
            editor.currentSketch.vectors.push(newv);
            editor.currentVector = newv;
            editor.loadVectorSelector(editor.sketchSelector, newv);
            editor.mode = CAMACloud.Sketching.MODE_DEFAULT;
            editor.startNewVector();
            if (editor.currentVector)
                editor.currentVector.startNode = null;
            editor.currentVector.isModified = true;
            return;
        })

    }


    this.transferVector = function () {
        if (!_) {
            alert('Requires underscore.js');
            return;
        }
        //        var configSources = ccma.Sketching.Config.sources;
        //        var targets = _.where(configSources, { AllowTransferIn: true });
        //        if (targets.length == 0) {
        //            messageBox('No transfer targets selected in the configuration. Use AllowTransferIn in source config.');
        //            return;
        //        }
        //        var c = targets[0];
        //        var targetElems = activeParcel[c.SketchSource.Table];
        var data = [];
        for (var x in editor.sketches) {
            var sx = editor.sketches[x];
            var cx = sx.config;
            if (cx.AllowTransferIn) {
                data.push({ Id: sx.uid, Name: sx.label });
            }
            //            var t = targetElems[x];
            //            data.push({ Id: (c.Key ? (c.Key + '-') : '') + t[c.SketchSource.KeyField || 'ROWUID'], Name: t[c.SketchSource.LabelField] });
        }


        if (data.length == 0) {
            messageBox('There is no record to transfer this sketch to. Please verify that at least one other improvement record exists and then transfer the sketch as needed. (AllowTransferIn)');
            return;
        }

        input('Select Sketch:', data, function (targetRowUID) {
            if (targetRowUID != "") {
                var v = editor.currentVector;
                var targetSketch = null;
                var vi = 0, ri = -1;
                for (var x in editor.currentSketch.vectors) {
                    if (v == editor.currentSketch.vectors[x]) {
                        ri = vi;
                    }
                    vi++;
                }

                for (var x in editor.sketches) {
                    var sk = editor.sketches[x];
                    if (sk.uid == targetRowUID) {
                        targetSketch = sk;
                    }
                }

                if (ri > -1 && targetSketch) {
                    var vectorList = [];
                    for (var x in targetSketch.vectors) {
                        var vx = targetSketch.vectors[x];
                        vectorList.push({ Id: vx.uid, Name: vx.label });
                    }

                    if (vectorList.length == 0) {
                        if (data.length == 0) {
                            messageBox('The selected sketch does not contain segment to copy the data into.');
                            return;
                        }
                    }

                    input('Select Segment:', vectorList, function (targetVectorID) {
                        var targetVector = null;
                        for (var x in targetSketch.vectors) {
                            var tk = targetSketch.vectors[x];
                            if (tk.uid == targetVectorID) {
                                targetVector = tk;
                            }
                        }

                        if (targetVector) {
                            var copyVectorData = function () {
                                var zm = editor.scale;
                                editor.zoom(1);
                                v = editor.currentSketch.vectors.splice(ri, 1)[0];
                                targetVector.vectorString = v.toString();
                                editor.formatter.updateVectorFromString(editor, targetVector, v.vectorString);
                                targetVector.isModified = true;
                                editor.currentSketch.isModified = true;
                                editor.currentVector.isModified = true;
                                editor.currentVector = null;
                                editor.loadVectorSelector(editor.sketchSelector);
                                editor.mode = CAMACloud.Sketching.MODE_DEFAULT;
                                editor.renderAll();
                                editor.zoom(zm);
                            }
                            if (targetVector.vectorString != null && targetVector.vectorString != '') {
                                messageBox('The selected segment contains data already. Are you sure you want to overwrite?', ["OK", "Cancel"], function () {
                                    copyVectorData();
                                }, function () {
                                    return;
                                })
                            } else {
                                copyVectorData();
                            }
                        }
                    }, function () {
                        //Do Nothing - Cancel Button
                    });

                }


            }
        }, function () {
            //Do Nothing - Cancel Button
        });

    }

    this.setControlList = function (sketchSel, vectorSel) {
        this.vectorSelector = $(vectorSel);
        this.sketchSelector = $(sketchSel);

        this.refreshControlList();
    }


    this.resizeCanvas = function (w, h) {
        ccse.width = w;
        ccse.height = h;
        $(ccse).width(w);
        $(ccse).height(h);
        editor.width = $(ccse).width();
        editor.height = $(ccse).height();
        editor.origin = GetOrigin(editor, editor.formatter.originPosition)
        editor.refOrigin = new PointX(editor.origin.x, editor.origin.y);
        editor.brush = new Brush(editor.drawing, editor);
    }

    this.renderAll = function () {
        this.clearCanvas();
        this.brush.setScale(this.scale);
        this.brush.moveTo(this.origin);

        if (this.currentSketch) {
            for (var x in this.currentSketch.vectors) {
                this.currentSketch.vectors[x].render();
            }
            if (this.displayNotes) this.currentSketch.renderNotes();

        } else {
            for (var i in this.sketches) {
                for (var x in this.sketches[i].vectors) {
                    this.sketches[i].vectors[x].render();
                }
                if (this.displayNotes) this.sketches[i].renderNotes();
            }
        }
    }

    this.render = function (currentPoint, options) {
        var moving = false;
        this.clearCanvas();

        if (this.showOrigin) this.drawOrigin();
        if (options) {
            moving = options.moving;
            if (options.renderMode == "fit") {

            }
            if (options.cursor)
                this.drawCursor(currentPoint);
        }

        this.brush.setScale(this.scale);
        this.brush.moveTo(this.origin);

        if (this.currentSketch) {
            for (var x in this.currentSketch.vectors) {
                this.currentSketch.vectors[x].render(currentPoint, moving);
            }

            if (!moving && this.displayNotes) {
                this.currentSketch.renderNotes();
            }
            else if (this.currentSketchNote && moving) { this.currentSketch.renderNotes(); }
        }


    }

    this.drawOrigin = function (o) {
        o = o || this.origin;
        var tempScale = this.scale;
        this.brush.setScale(1);

        this.brush.setColor('#999', 'Red');     // origin colors
        this.brush.begin();
        this.brush.moveTo(o);
        this.brush.up(50, true, true);
        this.brush.right(50, true, true);
        this.brush.stroke();
        this.brush.end()

        this.brush.begin();
        this.brush.moveTo(o);
        this.brush.rectX(9, 9);
        this.brush.strokeAndFill();
        this.brush.end();

        this.brush.setScale(tempScale);
    }

    this.drawNode = function (current, s, e, ol, angle) {
        var nodeColor = '#CFCFCF';
        if (s) nodeColor = '#00FF00';
        if (e) nodeColor = 'Black';
        if (current) nodeColor = 'Blue';
        if (ol) nodeColor = 'Magenta';

        this.drawing.lineWidth = 1;
        var nz = 7;
        var tempScale = this.scale;
        this.brush.setScale(1);

        if (s) nz = 11;
        var p = this.brush.begin();

        s && (angle != null) ? this.brush.pointer(nz, nz, 360 - angle) : this.brush.rectX(nz, nz);
        this.brush.stroke();
        this.brush.setColor('Black', nodeColor);    //node - black + choice
        this.brush.fill();
        this.brush.end();
        this.brush.moveTo(p);

        this.brush.setScale(this.scale);
    }

    this.drawCursor = function (p) {
        try {
            var crosshair = 2400;
            var cp = p.copy();
            var hx = false, hy = false;
            if (this.currentVector != null) {
                var t = p.copy().alignToGrid(this);
                var sn = this.currentVector.startNode;
                while (sn != null) {
                    if ((this.mode == CAMACloud.Sketching.MODE_NEW) || ((this.mode == CAMACloud.Sketching.MODE_EDIT) && (sn != this.currentNode) && (sn != this.currentVector.endNode))) {
                        if (sn.p.x == t.x) hy = true;
                        if (sn.p.y == t.y) hx = true;
                    }
                    sn = sn.nextNode;
                }
            }

            this.drawing.lineWidth = 1;
            var tempScale = this.scale;
            this.brush.setScale(1);
            this.brush.begin();
            this.brush.setColor(hx ? '#F09' : '#BBB', 'Red');   //guide cursors
            this.brush.moveTo(cp);
            this.brush.left(crosshair, true, true);
            this.brush.right(crosshair, true, true);
            this.brush.stroke();
            this.brush.end()

            this.brush.setColor(hy ? '#F09' : '#BBB', 'Red');   //guide cursors
            this.brush.begin();
            this.brush.moveTo(cp);
            this.brush.up(crosshair, true, true);
            this.brush.down(crosshair, true, true);
            this.brush.stroke();
            this.brush.end()

            this.brush.setScale(tempScale);
        }
        catch (e) {
            alert(e);
        }

        //        var img = new Image();
        //        img.onload = function () {
        //            editor.drawing.drawImage(img, p.x - 13, p.y - 13);
        //        }
        //        img.src = 'static/images/sketch-cursor.png';
    }

    this.screenBounds = function () {
        var lxy = (new PointX(0, 0)).alignToGrid(this);
        var uxy = (new PointX(this.width, this.height)).alignToGrid(this);
        return {
            lx: lxy.x,
            ly: uxy.y,
            ux: uxy.x,
            uy: lxy.y
        };
    }

    this.getCoords = function (e) {
        var p = new PointX(0, 0);
        if (event.touches) {
            var t = event.touches[0];
            if (!t)
                t = event.changedTouches[0];
            p = new PointX(t.clientX - t.target.offsetLeft, t.clientY - t.target.offsetTop);
        } else {
            p = new PointX(event.clientX - event.target.offsetLeft, event.clientY - event.target.offsetTop);
        }
        return p;
    }

    this.registerNode = function (p) {
        if (!p)
            p = this.origin.copy();
        var ap = p.copy();
        if (this.currentVector == null) {
            this.currentVector = new Vector(this, this.currentSketch, p);
        } else {
            if (this.currentVector.startNode)
                this.currentVector.connect(p);
            else
                this.currentVector.start(p);
        }
        this.render();
    }

    this.open = function (data, defaultVectorUID, isPreview, colorCode, labelColorCode) {
        //        if (!this.formatter) throw "A Sketch formatter is required to open the vector.";
        //        this.formatter = formatter;  
        this.labelColorCode = labelColorCode
        this.colorCode = colorCode
        this.isPreview = isPreview;
        this.currentVector = null;
        this.currentSketch = null;
        this.currentNode = null;
        this.displayNotes = true;
        this.formatter.open(this, data);
        this.mode = CAMACloud.Sketching.MODE_DEFAULT;
        this.hideKeypad();
        this.raiseModeChange();
        this.refreshControlList();
        this.sketches.forEach(function (sk, index) {
            //if (sk.config.AutoSelectFistItem == true && index == 0 && !isPreview) {
            if ((sk.config.AutoSelectFirstItem != false || sk.config.AutoSelectFirstItem == 'undefined') && (index == 0 && !isPreview)) {
                $(editor.sketchSelector).val(sk.uid);
                $(editor.sketchSelector).change();
            }
            if (defaultVectorUID == sk.uid) { // if sketchId given as default one
                $(editor.sketchSelector).val(sk.uid);
                editor.currentSketch = sk;
                editor.currentNode = null;
            }
            var sectionFilterField = sk.config.sectionFilterField;
            sk.vectors.forEach(function (v) {
                sectionValue = v.section || 1;
                if (!sk.sections[sectionValue]) sk.sections[sectionValue] = [];
                sk.sections[sectionValue].push(v);
                if (v.uid == defaultVectorUID) {
                    $(editor.sketchSelector).val(sk.uid);
                    editor.loadVectorSelector($(editor.sketchSelector));
                    $(editor.vectorSelector).val(v.uid);
                    editor.currentVector = v;
                    editor.currentSketch = sk;
                    editor.currentNode = null;
                    if (v.toString() == "") {
                        if (!editor.viewOnly)
                            editor.promptAndCreateVector();
                    } else {
                        editor.mode = CAMACloud.Sketching.MODE_EDIT;
                        editor.raiseModeChange();
                    }
                }
            });
        });
        //        for (var x in this.vectors) {
        //            if (this.vectors[x].uid == defaultVectorUID) {
        //                this.currentVector = this.vectors[x];
        //            }
        //        }
    }

    this.deleteCurrentNode = function () {
        var c = this.currentNode;
        if (c.isEllipse)
            if (c.nextNode)
                this.deleteNode(c.nextNode);
        this.deleteNode(c);
        if (this.currentVector)
            this.currentVector.isModified = true;
    }
    this.deleteCurrentSketchNote = function () {
        var note = this.currentSketchNote;
        note.isDeleted = true;
        this.currentSketchNote.isModified = true;
        this.currentSketchNote = null;
        editor.mode = CAMACloud.Sketching.MODE_DEFAULT;
        editor.raiseModeChange();
        this.render();
    }

    this.copyCurrentVector = function () {
        var _getCurrentVector = function () {
            for (key in editor.currentVector) {
                copiedVector[key] = editor.currentVector[key]
            };
            if (window.opener)
                window.opener.copyVector = copiedVector;
            else
                copyVector = copiedVector;
        }
        if (this.currentVector) {
            if (this.currentVector.isModified == true)
                messageBox('Please save the changes before you copy the vector');
            else {
                if (window.opener)
                    window.opener.copyVector = null;
                var copiedVector = {};
                _getCurrentVector();
            }
        }
    }
    this.pasteVector = function () {
        // var cv = editor.copiedVector;
        var t = editor.scale;
        editor.zoom(1);
        if (window.opener)
            copyVector = window.opener.copyVector;
        var cv = editor.formatter.vectorFromString(editor, copyVector.vectorString);
        cv.sketch = editor.currentSketch;
        var vnum = 0;
        if (editor.currentSketch.vectors.length > 0) {
            vnum = editor.currentSketch.vectors[editor.currentSketch.vectors.length - 1].index + 1;
        }
        cv.uid = editor.currentSketch.uid + "/" + vnum;
        cv.index = vnum;
        cv.label = '[' + vnum + '] ' + copyVector.label.split(']')[1].trim();
        cv.name = copyVector.name;
        cv.clientId = ccTicks().toString();
        cv.newRecord = true;
        cv.isChanged = true;
        cv.labelPosition = new PointX(copyVector.labelPosition.x, copyVector.labelPosition.y);
        editor.currentSketch.vectors.push(cv);
        editor.currentVector = cv;
        editor.loadVectorSelector(editor.sketchSelector, cv);
        editor.mode = CAMACloud.Sketching.MODE_EDIT;
        editor.raiseModeChange();
        editor.render();
        editor.currentSketch.isModified = true;
        editor.currentVector.isModified = true;
        copyVector = null;
        if (window.opener)
            window.opener.copyVector = null;
        $('.ccse-meter-length').val('');
        $('.ccse-meter-angle').val('');
        editor.zoom(t);
        return;

    }
    this.deleteCurrentVector = function () {
        var _removeCurrentVector = function () {
            var cvi = editor.currentSketch.vectors.indexOf(editor.currentVector);
            editor.currentSketch.vectors.splice(cvi, 1);
            editor.currentSketch.isModified = true;
            editor.currentVector = null;
            editor.currentNode = null;
            editor.render();
            editor.loadVectorSelector(editor.sketchSelector);
        }

        var _clearCurrentVector = function () {
            editor.currentVector.vectorString = null;
            editor.currentVector.startNode = null;
            editor.currentVector.endNode = null;
            editor.currentVector.commands = [];
            editor.currentVector.header = null;
            editor.currentVector.isClosed = false;
            editor.currentVector.isModified = true;
            editor.currentVector.labelPosition = null;
            editor.render();
            editor.currentNode = null;
            editor.currentVector = null;
        }

        if (this.currentVector) {
            if (this.currentSketch.config.ClearSegmentOnDeletion) {
                _clearCurrentVector();
            }
            else if (this.formatter.allowSegmentDeletion || this.currentSketch.config.AllowSegmentDeletion) {
                _removeCurrentVector();
            } else if (this.currentSketch.config.AllowMultiSegmentAddDelete) {
                var testuid = this.currentVector.uid;
                if (testuid.indexOf("/") > -1) testuid = testuid.substr(0, testuid.indexOf("/"));
                var scount = 0;
                for (var vi in this.currentSketch.vectors) {
                    var v = this.currentSketch.vectors[vi];
                    var vuid = v.uid;
                    if (vuid.indexOf("/") > -1) vuid = vuid.substr(0, vuid.indexOf("/"));
                    if (vuid == testuid) {
                        scount += 1;
                    }
                }
                if (scount > 1) {
                    _removeCurrentVector()
                } else {
                    _clearCurrentVector();
                }
            } else {
                _clearCurrentVector();
            }

            $(editor.vectorSelector).val('');
            editor.mode = CAMACloud.Sketching.MODE_DEFAULT;
            editor.raiseModeChange();
        }
    }

    this.deleteNode = function (c) {
        if (c == this.currentVector.startNode) {
            if (c.nextNode) {
                if (c.nextNode.overlaps(c.p)) {
                    this.deleteNode(c.nextNode);
                }
            }
            if (c.nextNode) {
                messageBox('You are not allowed to delete the anchor node of the segment until all other nodes are deleted.');
                return;
                //                var n = c.nextNode;
                //                this.currentVector.startNode = n;
                //                delete c;
                //                this.currentVector.startNode.recalculateAll();

            } else {
                this.currentVector.endNode = null;
                this.currentVector.startNode = null;
                //                var delIndex = this.vectors.indexOf(this.currentVector);
                //                this.vectors.splice(delIndex, 1);
                //                this.currentVector = null;
                this.mode = CAMACloud.Sketching.MODE_NEW;
                this.raiseModeChange();
                this.currentNode = null;
                this.currentVector.isClosed = false;

            }
        } else {
            var p = c.prevNode;
            var n = c.nextNode;
            if (p == this.currentVector.startNode && n == null) {
                p.nextNode = null;
                this.currentVector.isClosed = false;
                this.currentVector.endNode = p;
                this.mode = CAMACloud.Sketching.MODE_NEW;
                this.raiseModeChange();
            } else if (p == this.currentVector.startNode && p.overlaps(n.p)) {
                p.nextNode = null;
                this.currentVector.isClosed = false;
                this.currentVector.endNode = p;
                this.mode = CAMACloud.Sketching.MODE_NEW;
                this.raiseModeChange();
            } else {
                p.nextNode = n;
                if (n) n.prevNode = p;
            }

            delete c;
            p.adjustEllipse();
            this.currentNode = p;
            this.currentVector.startNode.recalculateAll();
        }
        this.currentVector.isModified = true;
        this.render();
    }

    this.addNodeBeforeCurrent = function () {
        var c = this.currentNode;
        if (c == this.currentVector.startNode) {
            messageBox('New node cannot be added before the start node.');
            return false;
        } else if (c.isEllipse || c.isEllipseEndNode) {
            messageBox('You have selected an ellipse resizer node. Add Node cannot be applied here.')
            return false;
        } else {
            var p = c.prevNode;
            var np = new PointX(Math.round((c.p.x + p.p.x) / 2), Math.round((c.p.y + p.p.y) / 2));
            var n = new Node(np, this.currentVector);
            p.nextNode = n;
            n.nextNode = c;
            c.prevNode = n;
            n.prevNode = p;
            p.recalculateAll();
            this.currentNode = n;
            this.render();
            this.currentVector.isModified = true;
        }
    }

    this.addNodeAfterCurrent = function (overlay) {
        var c = this.currentNode;
        if (!c) return false;
        var p = c.nextNode;
        var np;
        if (p && !overlay)
            np = new PointX(Math.round((c.p.x + p.p.x) / 2), Math.round((c.p.y + p.p.y) / 2));
        else
            np = c.p.copy();
        var n = new Node(np, this.currentVector);
        if (p) {
            c.nextNode = n;
            n.nextNode = p;
            n.prevNode = c;
            p.prevNode = n;
            c.recalculateAll();
        } else {
            c.nextNode = n;
            n.prevNode = c;
            n.nextNode = null;
            this.currentVector.endNode = n;
        }
        this.currentNode = n;
        this.render();
        this.currentVector.isModified = true;
        return n;
    }

    this.editCurrentVectorLabel = function () {
        var labelselectionLookup = clientSettings['SketchLabelLookup'] || editor.currentSketch.config.SketchLabelLookup;
        var options = null;
        var labelQuestion = 'Enter label:';
        if (labelselectionLookup) {
            if (editor.external.lookup[labelselectionLookup] === undefined) {
                messageBox('There are no values listed in ' + labelselectionLookup + ' lookup');
                return;
            }
            labelQuestion = 'Select the type of segment:';
            options = editor.external.lookup[labelselectionLookup];
        }

        input(labelQuestion, options, function (newlabel) {
            if (newlabel != "") {
                var lbldesc = labelselectionLookup && options[newlabel] ? '-' + options[newlabel].Name : '';
                editor.currentVector.name = newlabel;
                editor.currentVector.label = "[" + editor.currentVector.index + "]" + newlabel + lbldesc;
                editor.loadVectorSelector(editor.sketchSelector, editor.currentVector);
                editor.currentVector.labelEdited = true;
                editor.currentVector.isModified = true;
                editor.render();
            }
        }, function () {
            //Do Nothing - Cancel Button
        });
    }

    this.save = function (afterSaveAction) {
        var onError = function (e) {
            messageBox('Sketch changes are not saved - ' + e);
            editor.saving = false;
        }

        //Save sketch changes
        function proceedWithSave() {
            $('.ccse-canvas').addClass('dimmer');
            $('.dimmer').show();
            editor.saving = true;
            var data = [];
            var noteData = [];
            var sketchDataArray = [];
            var changeCount = 0;
            editor.sketches.forEach(function (sk) {
                if (sk.isModified) {
                    var saveAll = editor.formatter.saveAllSegments || sk.config.SaveAllSegments;
                    if (sk.vectors.length == 0 && saveAll) {
                        var vx = {
                            sketchid: sk.uid,
                            sid: sk.sid,
                            uid: null,
                            label: "",
                            vector: "",
                            area: 0,
                            config: sk.config,
                            parentRow: sk.parentRow
                        }
                        data.push(vx);
                    }

                    sk.vectors.filter(function (x) { return x.isModified || saveAll }).forEach(function (v) {
                        var vx = {
                            sketchid: sk.uid,
                            sid: sk.sid,
                            uid: v.uid,
                            label: v.name || v.label,
                            vector: v.toString(),
                            area: v.area(),
                            perimeter: v.perimeter(),
                            labelCommand: v.labelCommandString(),
                            dimensionCommand: v.dimensionCommandString(),
                            config: sk.config,
                            newRecord: v.newRecord,
                            clientId: v.clientId,
                            parentRow: sk.parentRow
                        }
                        data.push(vx);
                    });

                }
                sk.notes.filter(function (x) { return x.isModified }).forEach(function (n) {
                    var table = sk.config.NotesSource.Table
                    var nt = {
                        noteid: n.noteid,
                        noteText: n.isDeleted == true ? '*DEL*' : n.noteText,
                        noteField: sk.config.NotesSource.TextField,
                        noteFieldID: editor.getDataField(table, sk.config.NotesSource.TextField),
                        xPosition: Math.floor(n.notePosition.x / (sk.config.NotesSource.ScaleFactor || 1)),
                        yPosition: Math.floor(n.notePosition.y / (sk.config.NotesSource.ScaleFactor || 1)),
                        xPositionField: sk.config.NotesSource.PositionXField,
                        yPositionField: sk.config.NotesSource.PositionYField,
                        xPositionFieldId: editor.getDataField(table, sk.config.NotesSource.PositionXField),
                        yPositionFieldId: editor.getDataField(table, sk.config.NotesSource.PositionYField),
                        sourceName: table,
                        clientId: n.clientId,
                        parentId: sk.sid,
                        newRecord: n.newRecord,
                        config: sk.config
                    }
                    noteData.push(nt);
                });
            });
            var sketchData = {};
            for (var x in data) {
                var d = data[x];
                var c = d.config;
                var sourceName = c.VectorSource.Table;
                var sourceField = c.VectorSource.CommandField;
                var areaField = c.VectorSource.AreaField;
                var perimeterField = c.VectorSource.PerimeterField;
                var labelCommandField = c.VectorSource.LabelCommandField;
                var dimensionCommandField = c.VectorSource.DimensionCommandField;
                var LabelField = c.VectorSource.LabelField;
                var sourceFieldId = -1;
                var areaFieldId = -1;
                var perimeterFieldId = -1;
                var labelCommandFieldId = -1;
                var dimensionCommandFieldId = -1;
                var labelFieldId = -1;

                try {
                    sourceFieldId = editor.getDataField(sourceName, sourceField);
                    areaFieldId = areaField ? editor.getDataField(sourceName, areaField) : -1;
                    perimeterFieldId = perimeterField ? editor.getDataField(sourceName, perimeterField) : -1;
                    labelCommandFieldId = labelCommandField ? editor.getDataField(sourceName, labelCommandField) : -1;
                    dimensionCommandFieldId = dimensionCommandField ? editor.getDataField(sourceName, dimensionCommandField) : -1;
                    labelFieldId = LabelField && c.AllowLabelEdit ? editor.getDataField(sourceName, LabelField) : -1;
                }
                catch (e) {
                    if (onError) onError(e.message || e);
                    editor.saving = false;
                    return;
                }

                var insertRecord = false;
                var keyValue = (c.SketchSource.Table == c.VectorSource.Table ? d.sid : d.uid).toString();
                if (c.InsertDataOnAddition && c.SketchSource.Table != c.VectorSource.Table && keyValue.indexOf('/') > -1 && d.newRecord)
                    insertRecord = true;

                if (!insertRecord)
                    if (keyValue.indexOf('/') > -1) keyValue = keyValue.substr(0, keyValue.indexOf('/'));
                var itemKey = sourceName + '~' + keyValue;

                if (sketchData[itemKey] == undefined) {
                    sketchData[itemKey] = {
                        sid: keyValue,
                        vectorString: "",
                        sourceName: sourceName,
                        sourceFieldId: sourceFieldId,
                        sourceField: sourceField,
                        areaFieldId: areaFieldId,
                        areaField: areaField,
                        perimeterFieldId: perimeterFieldId,
                        perimeterField: perimeterField,
                        labelCommandField: labelCommandField,
                        labelCommandFieldId: labelCommandFieldId,
                        dimensionCommandField: dimensionCommandField,
                        dimensionCommandFieldId: dimensionCommandFieldId,
                        newRecord: d.newRecord,
                        insertRecord: insertRecord,
                        isDeleted: false,
                        label: d.label,
                        labelField: LabelField,
                        labelFieldId: labelFieldId,
                        area: 0,
                        perimeter: 0,
                        labelCommand: d.labelCommand,
                        clientId: c.SketchSource.Table == c.VectorSource.Table ? keyValue : d.clientId,
                        dimensionCommand: d.dimensionCommand,
                        segments: {},                            //Segments for Split-Area calculation,
                        parentRow: d.parentRow
                    };

                    if (c.LabelAreaFields) {
                        for (var lbl in c.LabelAreaFields) {
                            var lf = c.LabelAreaFields[lbl];
                            var lfid = lf ? editor.getDataField(sourceName, lf) : -1;
                            var key = 'a+' + (lf || '*');
                            sketchData[itemKey].segments[key] = {
                                type: 'Area',
                                label: lbl,
                                field: lf,
                                fieldId: lfid,
                                area: 0.0
                            }
                        }
                    }
                }
                if (d.vector != null && d.vector != '') {
                    if (sketchData[itemKey].vectorString != "")
                        sketchData[itemKey].vectorString += (editor.formatter.vectorSeperator || ",");
                    sketchData[itemKey].vectorString += d.vector;
                }

                //Split-Area calculation - populate the segments
                try {
                    var af = c.LabelAreaFields ? c.LabelAreaFields[d.label] || areaField : areaField;  //Split-Area calculation - picking the label/area field from sketch config
                    var areaSegmentLabel = 'a+' + (af || '*');
                    if (!sketchData[itemKey].segments[areaSegmentLabel]) {
                        var afId = af ? editor.getDataField(sourceName, af) : -1;
                        sketchData[itemKey].segments[areaSegmentLabel] = {
                            type: 'Area',
                            label: d.label,
                            field: af,
                            fieldId: afId,
                            area: 0.0
                        }
                    }

                    //Split-Area calculation - accumulate area in case of multiple segments with same label.
                    sketchData[itemKey].segments[areaSegmentLabel].area += d.area || 0;

                } catch (ex) {
                    if (onError) onError(ex.message || ex);
                    editor.saving = false;
                    return;
                }

                //Split-Area calculation - Compute and append the summary fields.
                //Provision added for multiple summary fields, if required in future
                for (var sumField in c.AreaSummaryFields) {
                    var sfId = sumField ? editor.getDataField(sourceName, sumField) : -1;
                    var sumLabel = "as+" + sumField;
                    if (!sketchData[itemKey].segments[sumLabel]) {
                        sketchData[itemKey].segments[sumLabel] = {
                            type: 'AreaSummary',
                            label: sumLabel,
                            field: sumField,
                            fieldId: sfId,
                            area: 0.0
                        }
                    }

                    if (c.AreaSummaryFields[sumField].indexOf(d.label) > -1) {
                        sketchData[itemKey].segments[sumLabel].area += d.area;
                    }
                }
                //Split-Area calculation - end of processing

                sketchData[itemKey].area += d.area || 0;
                sketchData[itemKey].perimeter += d.perimeter || 0;
            }


            for (var x in sketchData) {
                var skd = sketchData[x];
                if (c.DeleteBlankSegmentsOnSave) {
                    if (skd.vectorString == "") {
                        skd.isDeleted = true;
                        skd.vectorString = "*DEL*";
                    }
                }
                if (skd.vectorString != "*DEL*")
                    if (editor.formatter.encodeSketch) {
                        skd.vectorString = editor.formatter.encodeSketch(skd.vectorString);
                    }
                sketchDataArray.push(skd);
            }

            changeCount = sketchDataArray.length;
            NoteChangeCount = noteData.length;
            if (editor.onSave) editor.onSave(sketchDataArray, noteData, function () {

            }, function () {
                editor.saving = false;
                editor.sketches.forEach(function (sk) {
                    sk.isModified = false;
                    sk.vectors.forEach(function (v) {
                        v.isModified = false;
                        v.newRecord = false;
                    });
                    sk.notes.forEach(function (n) {
                        n.isModified = false;
                        n.newRecord = false;
                    });
                });
                var sb = editor.sketchBounds();
                editor.resetOrigin();
                editor.panOrigin(sb, editor.formatter.originPosition);

                if (afterSaveAction) {
                    $('.ccse-canvas').removeClass('dimmer');
                    $('.dimmer').hide();
                    afterSaveAction();
                }
                else {
                    $('.ccse-canvas').removeClass('dimmer');
                    $('.dimmer').hide();
                    if (changeCount > 0 || NoteChangeCount > 0) {
                        messageBox('All changes to the sketch have been saved.', ['Close', 'Continue'], function () { editor.close(); sketchopened = false; }, function () { return true; });
                    }
                    else {
                        messageBox('No changes found!');
                    }
                };
            }, onError);
            return sketchDataArray;
        }

        var warning = 'There is an incomplete sketch segment. Please finalize (close) the sketch, or delete the segment.';
        var valid = true;
        var continueWithWarning = false;
        this.sketches.forEach(function (sk) {
            var res = sk.validateVectors();
            if (!res.valid) {
                valid = false;
                warning = res.error;
                if (!(sk.config.DoNotAllowOpenSegments || editor.formatter.preventSavingOpenSegments)) {
                    warning += " Are you sure you want to continue saving this?";
                    continueWithWarning = true;
                }
            }
        });

        if (valid)
            return proceedWithSave();
        else {
            if (continueWithWarning) {
                messageBox(warning, ["OK", "Cancel"], function () {
                    return proceedWithSave();
                }, function () {

                });
            } else {
                messageBox(warning);
            }
        }

    }

    this.getDataField = function (sourceName, fieldName) {
        var checkField = _.where(editor.external.datafields, { "SourceTable": sourceName, "Name": fieldName });
        if (checkField.length == 0) { throw { message: ('Field not found - ' + fieldName + ' in ' + (sourceName || 'Parcel Data')) }; return; }
        return checkField[0].Id;
    }

    this.close = function () {
        if (editor.saving) {
            messageBox('Please wait.. Sketch changes being saved.');
            return;
        }

        var isModified = false;
        editor.sketches.forEach(function (sk) {
            isModified = isModified || sk.isModified;
            sk.vectors.forEach(function (v) {
                isModified = isModified || v.isModified;
            });
            sk.notes.forEach(function (n) {
                isModified = isModified || n.isModified;
            });
        });

        var t = this;

        if (isModified) {
            //            messageBox('There are unsaved changes in the sketch. Tap OK to continue on this editor or Cancel to lose changes and return to last screen.', ["OK", "Cancel"], function () {
            //                return;
            //            }, function () {
            //                t.currentVector = null;
            //                t.currentSketch = null;
            //                t.currentNode = null;
            //                t.onClose();
            //            });
            messageBox(msg_sketcheditor_close, ["OK", "Cancel"], function () {
                t.currentVector = null;
                t.currentSketch = null;
                t.currentNode = null;
                t.onClose();
                sketchopened = false;
            }, function () {
                return;
            });
            return;
        }

        this.currentVector = null;
        this.currentSketch = null;
        this.currentNode = null;
        this.onClose();
        sketchopened = false;
    }

    this.bounds = function () {
        var bb = new PointX(0, 0);
        this.sketches.forEach(function (sk) {
            for (x in sk.vectors) {
                var b = new PointX(0, 0);
                var v = sk.vectors[x];
                var sn = v.startNode;
                while (sn != null) {
                    if (sn.sdx == "R") {
                        b.x += sn.dx;
                    }
                    if (sn.sdy == "U") {
                        b.y += sn.dy;
                    }

                    sn = sn.nextNode;
                }

                if (b.x > bb.x) {
                    bb.x = b.x;
                }
                if (b.y > bb.y) {
                    bb.y = b.y;
                }
            }
        });


        return bb;
    }

    this.lbounds = function () {
        var bb = new PointX(0, 0);
        this.sketches.forEach(function (sk) {
            for (x in sk.vectors) {
                var b = new PointX(0, 0);
                var v = sk.vectors[x];
                var sn = v.startNode;
                while (sn != null) {
                    if (sn.sdx == "L") {
                        b.x -= sn.dx;
                    }
                    if (sn.sdy == "D") {
                        b.y -= sn.dy;
                    }

                    sn = sn.nextNode;
                }

                if (b.x < bb.x) {
                    bb.x = b.x;
                }
                if (b.y < bb.y) {
                    bb.y = b.y;
                }
            }
        });

        return bb;
    }

    this.allbounds = function () {
        var x1 = 0, x2 = 0, y1 = 0, y2 = 0;
        this.sketches.forEach(function (sk) {
            for (x in sk.vectors) {
                var b = new PointX(0, 0);
                var v = sk.vectors[x];
                var sn = v.startNode;
                while (sn != null) {
                    x1 = Math.min(x1, isNaN(sn.p.x) ? 0 : sn.p.x);
                    x2 = Math.max(x2, isNaN(sn.p.x) ? 0 : sn.p.x);
                    y1 = Math.min(y1, isNaN(sn.p.y) ? 0 : sn.p.y);
                    y2 = Math.max(y2, isNaN(sn.p.y) ? 0 : sn.p.y);

                    if (sn.isArc) {
                        var mp = sn.midpoint;
                        x1 = Math.min(x1, isNaN(mp.x) ? 0 : mp.x);
                        x2 = Math.max(x2, isNaN(mp.x) ? 0 : mp.x);
                        y1 = Math.min(y1, isNaN(mp.y) ? 0 : mp.y);
                        y2 = Math.max(y2, isNaN(mp.y) ? 0 : mp.y);
                    }

                    sn = sn.nextNode;
                }
            }
        });
        return { xmax: x2, ymax: y2, xmin: x1, ymin: y1 };
    }

    this.sketchBounds = function () {
        var x1 = 0, x2 = 0, y1 = 0, y2 = 0;
        if (this.currentSketch) {
            var sk = this.currentSketch;
            for (x in sk.vectors) {
                var b = new PointX(0, 0);
                var v = sk.vectors[x];
                var sn = v.startNode;
                while (sn != null) {
                    x1 = Math.min(x1, isNaN(sn.p.x) ? 0 : sn.p.x);
                    x2 = Math.max(x2, isNaN(sn.p.x) ? 0 : sn.p.x);
                    y1 = Math.min(y1, isNaN(sn.p.y) ? 0 : sn.p.y);
                    y2 = Math.max(y2, isNaN(sn.p.y) ? 0 : sn.p.y);

                    if (sn.isArc) {
                        var mp = sn.midpoint;
                        x1 = Math.min(x1, isNaN(mp.x) ? 0 : mp.x);
                        x2 = Math.max(x2, isNaN(mp.x) ? 0 : mp.x);
                        y1 = Math.min(y1, isNaN(mp.y) ? 0 : mp.y);
                        y2 = Math.max(y2, isNaN(mp.y) ? 0 : mp.y);
                    }

                    sn = sn.nextNode;
                }
            }
        }
        return { xmax: x2, ymax: y2, xmin: x1, ymin: y1 };
    }

    this.processCmd = function (cmd, dist, angle, doNotOveralap) {
        if (!this.currentVector) return false;
        if (!this.currentNode) {
            if (cmd != "+")
                return false;
        };
        switch (cmd) {
            case "U":
                if (dist == 0) dist = 1;
                this.currentNode.p.moveBy(0, -1 * dist);
                this.currentNode.recalculateAll();
                break;
            case "D":
                if (dist == 0) dist = 1;
                this.currentNode.p.moveBy(0, 1 * dist);
                this.currentNode.recalculateAll();
                break;
            case "L":
                if (dist == 0) dist = 1;
                this.currentNode.p.moveBy(-1 * dist, 0);
                this.currentNode.recalculateAll();
                break;
            case "R":
                if (dist == 0) dist = 1;
                this.currentNode.p.moveBy(1 * dist, 0);
                this.currentNode.recalculateAll();
                break;
            case "A":
                this.processCmd("+", true);
                if (!this.currentNode)
                    break;
                var sgn = 1;
                if (angle != 0)
                    sgn = angle / Math.abs(angle);
                angle = sgn * (180 - Math.abs(angle));

                var rAngle = 90;
                var p1 = this.currentNode.p;
                var p2 = p1.copy();
                var a = angle;

                if (this.currentNode.prevNode) {
                    if (this.currentNode.prevNode.prevNode) {
                        p2 = this.currentNode.prevNode.prevNode.p;
                        rAngle = Math.atan((p1.y - p2.y) / (p1.x - p2.x)) * (180 / Math.PI);
                        if ((p2.y < p1.y) && (p2.x > p1.x)) rAngle = 180 + rAngle;
                        else if ((p2.y > p1.y) && (p2.x > p1.x)) rAngle = rAngle - 180;
                        else if ((p2.y == p1.y) && (p2.x > p1.x)) rAngle = 180 - rAngle;
                        if (isNaN(rAngle)) rAngle = 0;
                    }
                }

                a = rAngle + angle;
                //console.log("Effective angle: ", a);
                var dx = Math.round(Math.cos(a * Math.PI / 180) * dist, 2);
                var dy = Math.round(Math.sin(a * Math.PI / 180) * dist, 2);
                //console.log(rAngle, a, dx, dy);
                this.currentNode.p.moveBy(0, -1 * dy);
                this.currentNode.p.moveBy(1 * dx, 0);
                this.currentNode.recalculateAll();
                break;
            case "+":
                if (this.mode == CAMACloud.Sketching.MODE_NEW) {
                    if (!this.currentNode && !this.currentVector.startNode) {
                        this.registerNode();
                        return false;
                    }
                    if (this.currentNode.prevNode)
                        if (this.currentNode.overlaps(this.currentNode.prevNode.p)) {
                            if (!dist)
                                messageBox('Node created already. Please move it to a different location before creating a new one.');
                            return false;
                        }
                    this.currentVector.connect(this.currentNode.p.copy(), true, false, doNotOveralap);
                } else if (this.mode == CAMACloud.Sketching.MODE_EDIT) {
                    if (this.currentNode) {
                        this.addNodeAfterCurrent();
                    }
                }

                break;
            case "OK":
                if (!this.currentVector.isClosed) {
                    this.currentVector.terminateNode(this.currentVector.startNode);
                    this.currentVector.isModified = true;
                    this.currentVector.close();
                }
                this.hideKeypad();
                break;
            case "BOX":
                if ((this.currentNode == this.currentVector.startNode) && (this.currentNode.nextNode == null)) {
                    if (dist == 0) {
                        messageBox('Enter a valid length.');
                        return;
                    }
                    this.currentVector.connect(this.currentNode.p.copy(), true);
                    this.currentNode.p.moveBy(0, -1 * dist);
                    this.currentNode.recalculateAll();
                    this.currentVector.connect(this.currentNode.p.copy(), true);
                    this.currentNode.p.moveBy(1 * dist, 0);
                    this.currentNode.recalculateAll();
                    this.currentVector.connect(this.currentNode.p.copy(), true);
                    this.currentNode.p.moveBy(0, 1 * dist);
                    this.currentNode.recalculateAll();
                    this.currentVector.terminateNode(this.currentVector.startNode);
                    this.currentVector.isModified = true;
                    this.currentVector.close();
                    this.hideKeypad();
                } else {
                    messageBox('You cannot create box from this point.');
                    return;
                }
                break;
            case "ARC":
                if (this.currentNode && this.currentNode.prevNode) {
                    if (this.currentNode.overlaps(this.currentNode.prevNode.p)) {
                        return;
                    }
                    var cn = this.currentNode;

                    this.currentNode.arcLength = dist;
                    this.currentNode.isArc = (dist != 0);

                    cn.recalculateAll();
                    this.currentVector.isModified = true;

                }
                break;
            case "ARX":
                if (this.currentNode && this.currentNode.prevNode) {
                    if (this.currentNode.overlaps(this.currentNode.prevNode.p)) {
                        return;
                    }
                    var cn = this.currentNode;

                    this.currentNode.arcLength = -dist;
                    this.currentNode.isArc = (dist != 0);

                    cn.recalculateAll();
                    this.currentVector.isModified = true;

                }
                break;
            case "ELL":
                if (this.currentNode) {
                    if (this.currentNode.nextNode)
                        if (this.currentNode.nextNode.isEllipse) {
                            messageBox('Another ellipse exists on the same point.');
                            return;
                        }
                    if (this.currentNode.isEllipse) {
                        messageBox('You cannot create ellipse from this point.');
                        return;
                    }
                    var cn = this.currentNode;
                    var a = this.addNodeAfterCurrent(true);
                    a.p.moveBy(0, -1 * dist);
                    a.p.moveBy(1 * dist, 0);
                    a.recalculateAll();
                    a.isEllipse = true;

                    var b = this.addNodeAfterCurrent(true);
                    b.p.moveBy(0, 1 * dist);
                    b.p.moveBy(-1 * dist, 0);
                    b.recalculateAll();
                    b.isEllipseEndNode = true;

                    cn.recalculateAll();
                    this.currentVector.isModified = true;
                }
                break;
            case "POLY":
                if (this.currentNode) {
                    if (this.currentNode.prevNode == this.currentVector.startNode && this.currentNode.nextNode == null) {
                        var p1 = this.currentVector.startNode.p;
                        var p2 = this.currentNode.p;
                        console.log(p1.distanceFrom(p2), DEFAULT_PPF, this.scale);
                        var dist = Math.round(p1.distanceFrom(p2) * 100) / 100;
                        if (dist == 0) {
                            messageBox('Draw a line of non-zero length.');
                        } else {
                            var numNodes = parseInt($('.sketch-poly-sides').val());
                            var sn = this.currentNode;
                            var ang = (360 / numNodes) - 180;
                            for (var ni = 0; ni < numNodes - 2; ni++) {
                                this.processCmd("A", dist, ang);
                            }
                            this.processCmd("OK");
                        }
                    } else {
                        messageBox("Polygons can be created only from a single line drawn from a start node. The polygon will be drawn clockwise from the line you have created.")
                    }
                } else {
                    messageBox("Invalid state to generate polygon.")
                }
                break;
        }
        if (this.currentVector) this.currentVector.isModified = true;
        if (this.currentNode) {
            var sb = this.screenBounds();
            var p = this.currentNode.p;
            if (p.x < sb.lx) {
                this.pan(((sb.lx - p.x) * DEFAULT_PPF) * this.scale + 25, 0)
            }
            if (p.x > sb.ux) {
                this.pan(((sb.ux - p.x) * DEFAULT_PPF) * this.scale - 25, 0)
            }
            if (p.y < sb.ly) {
                this.pan(0, ((sb.ly - p.y) * DEFAULT_PPF) * this.scale + 25)
            }
            if (p.y > sb.uy) {
                this.pan(((sb.uy - p.y) * DEFAULT_PPF) * this.scale - 25, 0)
            }
        }


        this.render();
    }

    var pz = {};
    this.autoPan = function (dir, clear, dp) {
        if (dp) dp = Math.abs(dp);

        if (!clear) {
            if (!pz[dir]) {
                var cmd = 'sketchApp.panCanvas("' + dir + '")';
                pz[dir] = window.setTimeout(cmd, 1500);
            }
        } else {
            if (pz[dir]) {
                window.clearTimeout(pz[dir]);
                pz[dir] = null;
            }
        }
    }

    this.clearAutoPanning = function () {
        for (var x in pz) {
            if (pz[x]) {
                window.clearTimeout(pz[x]);
                pz[x] = null;
            }
        }
    }

    this.panCanvas = function (dir) {
        var pl = 120;
        if (pz[dir]) pz[dir] = null;
        switch (dir) {
            case "U":
                editor.pan(0, pl);
                break;
            case "D":
                editor.pan(0, -pl);
                break;
            case "L":
                editor.pan(-pl, 0);
                break;
            case "R":
                editor.pan(pl, 0);
                break;
        }
    }


    this.CVProfile = function (a) {
        if (!this.currentVector) return false;
        if (!a) a = 0;
        var sn = this.currentVector.startNode;
        while (sn != null) {
            var p1 = sn.p;
            var p2 = p1.copy();
            if (sn.nextNode) {
                var n = sn.nextNode;
                p2 = n.p;
                var dsq = 0;
                for (var x in n.path) {
                    dsq += n.path[x].dist * n.path[x].dist;
                }
                var dist = Math.round(Math.sqrt(dsq));
                var rad = Math.abs(Math.atan((p2.y - p1.y) / (p2.x - p1.x)));
                var sgn = 1;
                if ((p2.y < p1.y) && (p2.x < p1.x)) {
                    sgn = -1;
                } else if ((p2.y == p1.y) && (p2.x < p1.x)) {
                    sgn = -1;
                } else if ((p2.y < p1.y) && (p2.x == p1.x)) {
                    sgn = -1;
                }

                rAngle = (rad + a) * (180 / Math.PI);
                if (isNaN(rAngle)) rAngle = 0;
                console.log(dist + 'ft ' + Math.round(sgn * rAngle) + '°');
            }

            sn = sn.nextNode;
        }

        return "";
    }

    this.showKeypad = function () { if (this.keypad) { $(this.keypad.selector).show(); this.keypadActive = true } }

    this.hideKeypad = function () { if (this.keypad) $(this.keypad.selector).hide(); this.keypadActive = false; }

    this.loadNotesForSketch = function (sketch, notes) {
        for (var i in notes) {
            var n = notes[i];
            var note = new Annotation(this, sketch, n.text, new PointX(n.x, n.y), new PointX(n.lx, n.ly), new PointX(n.x, n.y), n.uid);
            sketch.notes.push(note);
        }
    }

    this.$ = function () {
        if (this.currentVector) {
            return this.currentVector.toString();
        }
    }

    init();
}

function Node(p, v) {
    this.vector = null;
    this.p = new PointX(); this.dx = 0; this.dy = 0; this.sdx = ""; this.sdy = ""; this.path = [];

    this.isVeer = false;
    this.isEllipse = false;
    this.isEllipseEndNode = false;
    this.isStart = false;
    this.commands = [];
    this.isArc = false;
    this.arcLength = 0;
    this.hideDimensions = false;

    this.nextNode = null;
    this.prevNode = null;

    this.__defineGetter__("length", function () {
        if (this.isStart) return NaN;
        var p1 = this.prevNode.p;;
        var p2 = this.p;
        return p1.getDistanceTo(p2);
    });

    this.__defineGetter__("direction", function () {
        if (this.isStart) return NaN;
        var p1 = this.prevNode.p;;
        var p2 = this.p;
        return p1.getDirectionInDegrees(p2);
    });

    this.__defineGetter__("midpoint", function () {
        if (this.isStart) return null;
        var p1 = this.prevNode.p;;
        var p2 = this.p;
        var mp = p1.midpointTo(p2);
        if (this.arcLength == 0) {
            return mp;
        }
        var angle = p1.getDirectionInDegrees(p2);
        var pangle = angle - ARC_SIGN * sign(this.arcLength) * 90;   //Reverse ARC_SIGN here, since theoretical graph and computer graph differs
        if (pangle > 360) pangle = pangle - 360;
        mp = mp.getPointAt(pangle, this.arcLength);
        return mp;
    });

    this.__defineGetter__("radius", function () {
        if (this.isStart) return NaN;
        if (this.arcLength == 0) return Infinity;
        var p1 = this.prevNode.p;
        var p2 = this.p;
        var angle = this.direction;
        var p3 = this.midpoint;
        var ap1p3 = p1.getDirectionInDegrees(p3);
        var leftInclusive = Math.abs(ap1p3 - angle);
        if (leftInclusive > 90) leftInclusive = 360 - leftInclusive;
        var topInclusive = 180 - 2 * leftInclusive;
        var dp1p3 = p1.getDistanceTo(p3);
        return radius = Math.abs((dp1p3 / 2) / Math.cos(topInclusive / 2 * Math.PI / 180));
    });

    this.__defineGetter__("arcCenter", function () {
        if (this.isStart) return null;
        if (this.arcLength == 0) return null;

        var pangle = this.direction - ARC_SIGN * sign(this.arcLength) * 90; //Reverse ARC_SIGN here, since theoretical graph and computer graph differs
        if (pangle >= 360) pangle -= 360;
        var rpangle = pangle + 180;
        if (rpangle >= 360) rpangle -= 360;
        return this.midpoint.getPointAt(rpangle, this.radius);
    });

    this.arcArea = function () {
        var area = 0;
        if (this.isStart) return 0;
        if (this.arcLength == 0) return 0;
        var c = this.arcCenter;
        var r = this.radius;
        var pi = Math.PI;

        var p1 = this.prevNode.p;
        var p2 = this.p;
        var mp = this.midpoint;

        var halfAngle = c.getDirectionInDegrees(mp) - c.getDirectionInDegrees(p1);
        if (halfAngle > 180) halfAngle = halfAngle - 360; if (halfAngle < -180) halfAngle = 360 + halfAngle;
        halfAngle = Math.abs(halfAngle);

        var fullAngle = halfAngle * 2;
        var intersect = p1.getDistanceTo(p2);
        var angleHeight = Math.sqrt(r * r - intersect * intersect / 4)

        if (halfAngle == 90) {
            area = pi * r * r / 2;
        } else if (halfAngle < 90) {
            area = pi * r * r * fullAngle / 360 - angleHeight * (intersect / 2);
        } else {
            area = pi * r * r * fullAngle / 360 + angleHeight * (intersect / 2);
        }

        return area;
    }

    this.arcPerimeter = function () {
        var perimeter = 0;
        if (this.isStart) return 0;
        if (this.arcLength == 0) return this.length;
        var c = this.arcCenter;
        var r = this.radius;
        var pi = Math.PI;

        var p1 = this.prevNode.p;
        var p2 = this.p;
        var mp = this.midpoint;

        var halfAngle = c.getDirectionInDegrees(mp) - c.getDirectionInDegrees(p1);
        if (halfAngle > 180) halfAngle = halfAngle - 360; if (halfAngle < -180) halfAngle = 360 + halfAngle;
        halfAngle = Math.abs(halfAngle);

        var fullAngle = halfAngle * 2;

        perimeter = 2 * pi * r * fullAngle / 360;

        return perimeter
    }

    this.ellipseArea = function () {
        if (this.isEllipse) {
            return Math.PI * this.dx * this.dy;
        }
        return 0;
    }

    this.connect = function (p) {
        var n = new Node(p, this.vector);
        this.nextNode = n;
        n.prevNode = this;
        n.calculateVector(this.p);
        //        this.calculateVector(p);
        this.vector.endNode = n;
        return n;
    }

    this.overlaps = function (p, tol) {
        if (tol) {
            if ((p.x >= this.p.x - tol) && (p.x <= this.p.x + tol) && (p.y >= this.p.y - tol) && (p.y <= this.p.y + tol)) {
                return true;
            }
        }
        if ((p.x == this.p.x) && (p.y == this.p.y)) {
            return true;
        } else {
            return false;
        }
    }

    this.moveTo = function (p) {
        this.p.x = p.x;
        this.p.y = p.y;
        this.recalculateAll();
    }
    this.recalculateAll = function () {
        if (this.vector.editor.mode == CAMACloud.Sketching.MODE_NEW && this.prevNode) {
            var p1 = this.p; var editor = this.vector.editor;
            var p2 = this.prevNode.p; var p3, p4
            var d = sRound(p1.distanceFrom(p2));
            var angle = 360 - (p2.getDirectionInDegrees(p1));
            $('.ccse-meter-length').val(d + (editor.formatter.LengthUnit || DISTANCE_UNIT || ''));
            if (editor.currentNode) {
                p3 = editor.currentNode.prevNode.p;
                prevnode = editor.currentNode.prevNode
            }
            if (prevnode && prevnode.prevNode)
                p4 = editor.currentNode.prevNode.prevNode.p;
            if (p3 && p4) {
                var angle2 = p3.getDirectionInDegrees(p4);
                var angle = (360 - angle2) - angle;
                if (angle < 0) {
                    angle = 360 + angle
                }
                $('.ccse-meter-angle').val(parseInt(angle) + ' °');
            }
        }
        this.adjustEllipse();
        var nn = this;
        var ref;
        while (nn != null) {
            if (nn.prevNode)
                ref = nn.prevNode.p;
            else
                ref = nn.vector.editor.origin.copy().alignToGrid(nn.vector.editor);  // Math.ceil
            nn.calculateVector(ref);
            nn = nn.nextNode;
        }
    }

    //node-calculate-vector
    this.calculateVector = function (ref) {
        var ed = this.vector.editor;
        var p = this.p;

        var scale = ed.scale;
        //Decimation
        //        var pdx = Math.round((p.x - ref.x));
        //        var pdy = Math.round((p.y - ref.y));
        var pdx = Number((p.x - ref.x).toFixed(2));
        var pdy = Number((p.y - ref.y).toFixed(2));
        this.sdx = pdx >= 0 ? "R" : "L";
        this.sdy = pdy >= 0 ? "U" : "D";
        this.dx = Math.abs(pdx);
        this.dy = Math.abs(pdy);

        this.isVeer = (this.dx > 0) && (this.dy > 0) && (this.isStart == false);
        this.path = [];

        if (this.dy > 0) {
            this.path.push({ "dir": this.sdy, "dist": this.dy });
        }

        if (this.dx > 0) {
            this.path.push({ "dir": this.sdx, "dist": this.dx });
        }

    }

    this.adjustEllipse = function () {
        var n = this.nextNode;
        if (n) {
            var q = this.p;
            if (n.isEllipse) {
                var t = n.p;
                if (n.nextNode) {
                    var a = n.nextNode.p;
                    var r = new PointX(t.x + q.x - a.x, t.y + q.y - a.y);
                    n.nextNode.moveTo(q);
                    n.moveTo(r);
                }
            }
        }

    }

    this.repos = function () {
        var sf = DEFAULT_PPF * this.vector.editor.scale;
        var n = this;
        if (this.prevNode) {
            var r = this.prevNode.p;
            var dx = n.dx * ((n.sdx == "L") ? -1 : 1);
            var dy = n.dy * ((n.sdy == "U") ? -1 : 1);
            console.log(dx, dy);
        }
    }

    this.vectorString = function () {
        return this.vector.editor.formatter.nodeToString(this, this.vector.editor);
    }

    this.p = p;
    this.vector = v;
}

function Vector(editor, sketch, startPoint) {
    if (!editor)
        throw "Cannot create a vector without editor parameter.";
    this.editor = editor;
    this.sketch = sketch;
    var v = this;
    this.index = 0;
    this.uid = "";
    this.label = "vector";
    this.name = null;
    this.vectorString = null;
    this.startNode = null;
    this.endNode = null;
    this.commands = [];
    this.header = null;
    this.labelPosition = null;
    this.level = 1;
    this.labelEdited = false;
    this.isClosed = false;
    this.isChanged = false;
    this.section = null;
    this.newRecord = false;
    var _isModified = false;
    //this.radiusNode = null;
    this.__defineGetter__("isModified", function () { return _isModified; });
    this.__defineSetter__("isModified", function (value) {
        _isModified = value;
        var cv = this;
        var cvuid = cv.uid; if (cvuid.indexOf("/") > -1) cvuid = cvuid.substr(0, cvuid.indexOf("/"));
        if (value && this.sketch) {
            this.isChanged = true;
            this.sketch.isModified = value;
            if (this.sketch.config.AllowMultiSegmentAddDelete) {
                this.sketch.vectors.forEach(function (v) {
                    var vuid = v.uid; if (vuid.indexOf("/") > -1) vuid = vuid.substr(0, vuid.indexOf("/"));
                    if (vuid == cvuid && v.uid != cv.uid && !v.isModified) { v.isModified = true; }
                });
            }
        }
    });

    this.isIncomplete = function () {
        return this.startNode != null && this.isClosed == false;
    }

    this.connect = function (p, noalign, doNotTerminate, doNotSetOverlap) {
        if (!noalign) p = p.alignToGrid(this.editor, function (x) { return x; });

        if (this.endNode.prevNode && (this.endNode.prevNode != this.startNode)) {
            var overlaplimit = 0.9;
            if (editor.mode == CAMACloud.Sketching.MODE_NEW && !doNotSetOverlap) overlaplimit = 2;
            if (this.startNode.overlaps(p, overlaplimit) && !doNotTerminate) {
                this.terminateNode(this.startNode);
                this.isModified = true;
                this.close();
            } else {
                this.endNode = this.endNode.connect(p);
                this.editor.currentNode = this.endNode;
            }
        } else {
            this.endNode = this.endNode.connect(p);
            this.editor.currentNode = this.endNode;
        }
        return this;
    }

    this.terminateNode = function (n) {
        this.endNode = this.endNode.connect(n.p);
        return this;
    }

    this.start = function (p, noAlign) {
        var ref = this.editor.origin.copy().alignToGrid(this.editor, Math.round); //Origin is rounded
        if (!noAlign) p = p.alignToGrid(this.editor);
        this.startNode = new Node(p, this);
        this.startNode.isStart = true;
        this.startNode.calculateVector(ref);
        this.endNode = this.startNode;
        this.editor.currentNode = this.startNode;
    }

    this.close = function () {
        this.isClosed = true;
        // NEW FUNCTIONALITY BEGINS
        this.editor.currentNode = null;
        editor.mode = CAMACloud.Sketching.MODE_EDIT;
        editor.raiseModeChange();
        //        this.editor.currentVector = null;
        //        this.editor.currentNode = null;
        //        $(editor.vectorSelector).val('');
        //        editor.mode = CAMACloud.Sketching.MODE_DEFAULT;
        //        editor.raiseModeChange();
    }

    this.render = function (currentPoint, onMove) {
        this.renderPart(currentPoint);
        if (!onMove) this.renderPart(currentPoint, true);        //Render Nodes
        if (!onMove) this.renderPart(currentPoint, false, true); //Render Labels
    }

    this.renderPart = function (currentPoint, nodesOnly, labelsOnly) {
        try {
            var ed = this.editor;
            var brush = ed.brush;
            var v = this;
            var current = ed.currentVector == this;

            if (nodesOnly && !current) return false;
            if (ed.colorCode)
                brush.setColor(ed.colorCode)
            else if (this.isChanged)
                brush.setColor(ed.sketchMovable ? 'Blue' : ' #F87217', '#FBB917');
            else
                brush.setColor(ed.sketchMovable ? 'Blue' : 'Black', '#444');

            ed.drawing.lineWidth = 3;
            var top = new PointX(10000, 10000);
            brush.begin();
            brush.moveTo(ed.origin);
            var sn = this.startNode;
            while (sn != null) {
                if (labelsOnly && sn.hideDimensions) {
                    sn = sn.nextNode;
                    continue;
                }
                if (this.placeHolder) {
                    if (nodesOnly) {
                        brush.drawPath(v, sn.path, sn.isVeer, false);
                        editor.drawNode((currentPoint == null) && (sn == this.editor.currentNode))
                        // brush.moveBy(this.placeHolderSize * DEFAULT_PPF, 0)
                        // editor.drawNode((currentPoint == null) && (this.radiusNode == this.editor.currentNode))
                    } else if (labelsOnly) {
                    }
                    else {
                        //this.radiusNode = new Node(sn.p.copy().moveBy(this.placeHolderSize, 0), this)
                        // this.radiusNode.p = sn.p.copy().moveBy(this.placeHolderSize, 0);
                        brush.drawPath(v, sn.path, sn.isVeer, false);
                        brush.circle(this.placeHolderSize);
                    }
                    break;
                } else if (sn.isEllipse) {
                    if (nodesOnly) {
                        brush.drawPath(v, sn.path, sn.isVeer, false);
                        if ((!this.editor.currentVector.isClosed) || (sn != this.editor.currentVector.endNode))
                            ed.drawNode((currentPoint == null) && (sn == this.editor.currentNode));
                    } else if (labelsOnly) {

                    } else {
                        brush.drawEllipse(sn.path, false);
                    }
                } else if (sn.isArc) {
                    if (nodesOnly) {
                        brush.drawPath(v, sn.path, sn.isVeer, false);
                        if ((!this.editor.currentVector.isClosed) || (sn != this.editor.currentVector.endNode))
                            ed.drawNode((currentPoint == null) && (sn == this.editor.currentNode));
                    } else if (labelsOnly) {

                    } else {
                        brush.drawArc(sn.path, sn.arcLength, sn);
                    }
                }
                else if (sn.isEllipseEndNode) {
                    if (nodesOnly) {
                        brush.drawPath(v, sn.path, sn.isVeer, false);
                    } else if (labelsOnly) {

                    } else {
                        //brush.drawPath(sn.path, sn.isVeer, false);
                    }
                } else {
                    if (nodesOnly) {
                        brush.drawPath(v, sn.path, sn.isVeer, false);
                        if ((!this.editor.currentVector.isClosed) || (sn != this.editor.currentVector.endNode)) {
                            ed.drawNode((currentPoint == null) && (sn == this.editor.currentNode), sn == this.editor.currentVector.startNode, sn == this.editor.currentVector.endNode, sn.prevNode ? sn.overlaps(sn.prevNode.p) : false, (sn == this.editor.currentVector.startNode) && sn.nextNode ? sn.p.getDirectionInDegrees(sn.nextNode.p) : null);
                        }
                    } else if (labelsOnly) {

                    } else {
                        brush.drawPath(v, sn.path, sn.isVeer, !sn.isStart);
                    }
                }

                if (brush.currentPoint.x < top.x) top.x = brush.currentPoint.x;
                if (brush.currentPoint.y < top.y) top.y = brush.currentPoint.y;

                sn = sn.nextNode;
            }


            if ((!nodesOnly) && (!labelsOnly) && (this.isClosed)) {
                if (current) {
                    var grd = editor.drawing.createLinearGradient(top.x, top.y, top.x + 500, top.y + 500);

                    //brush.setColor('Black', '#C5E7FF');
                    brush.setColor(ed.colorCode, grd);
                    //   brush.fill();
                } else {
                    var grd = editor.drawing.createLinearGradient(top.x, top.y, top.x + 500, top.y + 500);

                    brush.setColor(ed.colorCode, grd);
                    //  brush.fill();
                }
            }


            if (labelsOnly) {
                sn = this.startNode;
                while (sn != null) {
                    if (sn.isEllipse) {

                    } else if (sn.isEllipseEndNode) {
                    }
                    else if (v.placeHolder) {
                        brush.drawCircleRadiuslabel(v, v.placeHolderSize, sn.p)
                    } else if (sn.isArc) {
                        var p1 = sn.prevNode.p;
                        var mp = sn.midpoint;
                        brush.drawArcLabel(v, mp.x - p1.x, mp.y - p1.y, sn.p.x - p1.x, sn.p.y - p1.y, sn.arcPerimeter(), Math.abs(sn.arcLength));
                    } else {
                        brush.drawPath(v, sn.path, sn.isVeer, false, !sn.isStart);
                    }
                    sn = sn.nextNode;
                }
                var cp;
                if (this.labelPosition && this.isClosed) {
                    cp = new PointX(this.labelPosition.x * editor.scale, this.labelPosition.y * editor.scale)
                }
                else {
                    cp = this.getCenter();
                    if (cp.x) this.labelPosition = new PointX(sRound(cp.x / editor.scale), sRound(cp.y / editor.scale))
                }
                if (this.isClosed || ed.mode != CAMACloud.Sketching.MODE_NEW) {
                    if (editor.labelColorCode) {
                        editor.brush.setColor(editor.labelColorCode, editor.labelColorCode);
                    }
                    else if (this.isChanged)
                        brush.setColor('#ECA114', '#ECA114');
                    else
                        brush.setColor('Black', 'Black');
                    editor.drawing.font = brush.getFont(this, "label");
                    var tw = editor.drawing.measureText(this.label).width / DEFAULT_PPF;
                    var cx = cp.copy();
                    cx.x -= tw * editor.scale / 2;
                    brush.write(cx, this.label || '???', brush.getFont(this, "label"));
                    var th = brush.getFontSize(editor.scale, this) * 1.5;
                    var dx = cp.copy();;
                    editor.drawing.font = brush.getFont(this, "area");
                    var areaText = this.area() + (editor.formatter.AreaUnit || AREA_UNIT || '');
                    var atw = editor.drawing.measureText(areaText).width;
                    dx.x -= atw / DEFAULT_PPF * editor.scale / 2;
                    dx.y -= th / DEFAULT_PPF * 1.6
                    brush.write(dx, areaText, brush.getFont(this, "area"));
                }
            }



            if (current && currentPoint != null) {
                if (nodesOnly) {
                    var lp = brush.currentPoint.copy();
                    brush.moveTo(currentPoint);
                    this.editor.onDrawLine(lp, currentPoint.copy(), ed.currentVector);
                    ed.drawNode(true);
                } else if (labelsOnly) {

                } else {
                    if (ed.mode == CAMACloud.Sketching.MODE_NEW) {
                        if (!this.isClosed) {
                            brush.lineTo(currentPoint);
                        }
                    }
                }
            }

            if (nodesOnly) {
                brush.fill();
            } else if (labelsOnly) {

            } else {
                brush.stroke();
            }
            brush.end();

            brush.moveTo(ed.origin);
        } catch (e) {
            alert(e);
        }

    }

    this.toPoints = function () {
        var ps = [];
        var sn = this.startNode;
        while (sn != null) {
            if (!(sn != this.startNode && sn.overlaps(this.startNode.p)))
                ps.push(sn.p.copy());
            sn = sn.nextNode;
        }
        return ps;
    }

    this.getCenter = function () {
        var points = this.toPoints();
        var sx = 0, sy = 0;
        for (x in points) {
            var pt = points[x];
            sx += pt.x;
            sy += pt.y;
        }

        var cx = new PointX(sRound((sx / points.length) * this.editor.scale), sRound((sy / points.length) * this.editor.scale));
        return cx;
    }

    this.area = function () {
        var points = this.toPoints();
        if (this.placeHolder) {
            var r = this.placeHolderSize;
            return Number((Math.PI * r * r).toFixed(2))
        }
        var ellipseArea = 0;
        var n = this.startNode;
        var ax = [];
        var ay = [];
        while (n != null) {
            if (n.isEllipse) {
                ellipseArea += n.ellipseArea();
            }
            if (n.isArc) {
                if (this.containsInFrame(n.midpoint)) {
                    ellipseArea += -n.arcArea();
                } else {
                    ellipseArea += n.arcArea();
                }
            }
            ax.push(n.p.x);
            ay.push(n.p.y);
            n = n.nextNode;
        }

        var len = ax.length;
        var polyArea = polygonArea(ax, ay, len);
        return Number((ellipseArea + polyArea).toFixed(2));
    }

    this.perimeter = function () {
        var p = 0;
        var n = this.startNode;
        while (n != null) {
            if (isFinite(n.length))
                p = p + sRound(n.length);
            n = n.nextNode;
        }
        return p;
    }

    function polygonArea(X, Y, numPoints) {
        area = 0;         // Accumulates area in the loop
        j = numPoints - 1;  // The last vertex is the 'previous' one to the first
        for (i = 0; i < numPoints; i++) {
            area = area + (X[j] + X[i]) * (Y[j] - Y[i]);
            j = i;  //j is previous vertex to i
        }
        return Math.abs(area / 2);
    }

    this.containsInFrame = function (pt) {
        var poly = this.toPoints();
        for (var c = false, i = -1, l = poly.length, j = l - 1; ++i < l; j = i)
            ((poly[i].y <= pt.y && pt.y < poly[j].y) || (poly[j].y <= pt.y && pt.y < poly[i].y)) && (pt.x < (poly[j].x - poly[i].x) * (pt.y - poly[i].y) / (poly[j].y - poly[i].y) + poly[i].x) && (c = !c);
        if (c) return c;

        var sn = this.startNode;
        while (sn != null) {
            if (sn.isEllipse) {
                var ex = sn.p.x;
                var ey = sn.prevNode.p.y;
                var ea = sn.dx;
                var eb = sn.dy;
                var px = pt.x;
                var py = pt.y;
                var dx = px - ex;
                var dy = py - ey;
                c = ((dx * dx) / (ea * ea) + (dy * dy) / (eb * eb) <= 1);
                if (c) return c;
            }
            sn = sn.nextNode;
        }

        return c;
    }

    this.containsOn = function (pt) {
        var poly = [];
        var sn = this.startNode;
        while (sn != null) {
            if (!sn.isStart) {
                poly.push(sn.midpoint.copy());
            }
            poly.push(sn.p.copy());
            sn = sn.nextNode;
        }

        for (var c = false, i = -1, l = poly.length, j = l - 1; ++i < l; j = i)
            ((poly[i].y <= pt.y && pt.y < poly[j].y) || (poly[j].y <= pt.y && pt.y < poly[i].y)) && (pt.x < (poly[j].x - poly[i].x) * (pt.y - poly[i].y) / (poly[j].y - poly[i].y) + poly[i].x) && (c = !c);
        if (c) return c;

        var sn = this.startNode;
        while (sn != null) {
            if (sn.isEllipse) {
                var ex = sn.p.x;
                var ey = sn.prevNode.p.y;
                var ea = sn.dx;
                var eb = sn.dy;
                var px = pt.x;
                var py = pt.y;
                var dx = px - ex;
                var dy = py - ey;
                c = ((dx * dx) / (ea * ea) + (dy * dy) / (eb * eb) <= 1);
                if (c) return c;
            }
            sn = sn.nextNode;
        }

        return c;
    }

    this.contains = function (pt) {
        if (this.isOnPerimeter(pt)) { return true; }

        var poly = [];
        var sn = this.startNode;
        if (this.placeHolder) {
            return Math.pow(pt.x - sn.p.x, 2) + Math.pow(pt.y - sn.p.y, 2) < Math.pow(this.placeHolderSize, 2)
        }
        while (sn != null) {
            if (!sn.isStart) {
                poly.push(sn.midpoint.copy());
            }
            poly.push(sn.p.copy());
            sn = sn.nextNode;
        }

        for (var c = false, i = -1, l = poly.length, j = l - 1; ++i < l; j = i)
            ((poly[i].y <= pt.y && pt.y < poly[j].y) || (poly[j].y <= pt.y && pt.y < poly[i].y)) && (pt.x < (poly[j].x - poly[i].x) * (pt.y - poly[i].y) / (poly[j].y - poly[i].y) + poly[i].x) && (c = !c);
        if (c) return c;

        var sn = this.startNode;
        while (sn != null) {
            if (sn.isEllipse) {
                var ex = sn.p.x;
                var ey = sn.prevNode.p.y;
                var ea = sn.dx;
                var eb = sn.dy;
                var px = pt.x;
                var py = pt.y;
                var dx = px - ex;
                var dy = py - ey;
                c = ((dx * dx) / (ea * ea) + (dy * dy) / (eb * eb) <= 1);
                if (c) return c;
            }
            sn = sn.nextNode;
        }

        return c;
    }

    this.isOnPerimeter = function (pt) {
        var poly = this.toPoints();
        for (var x in poly) {
            var cp = poly[x];
            if (cp.overlaps(pt)) {
                return true;
            }
        }

        for (var i = 0; i <= poly.length - 2; i += 2) {
            var p1 = poly[i];
            var p2 = poly[i + 1];
            if (p1.getDistanceTo(p2) == p1.getDistanceTo(pt) + p2.getDistanceTo(pt)) {
                return true;
            }
        }

        return false;
    }

    this.moveBy = function (d, micromove) {
        var sn = this.startNode;
        if (!sn) return;
        var p2 = new PointX(Math.floor(sn.p.x), Math.floor(sn.p.y));
        var diff = new PointX(sn.p.x - p2.x, sn.p.y - p2.y);
        var moveIndex = 0;
        if (micromove) diff = new PointX(0, 0);
        while (sn != null) {
            if ((sn == this.endNode) && (sn.overlaps(this.startNode.p))) {

            } else {
                sn.p.x = Number((sn.p.x - d.x - diff.x).toFixed(2));
                sn.p.y = Number((sn.p.y - d.y - diff.y).toFixed(2));
            }
            sn = sn.nextNode;
            moveIndex++;
        }
        this.labelPosition.x -= d.x;
        this.labelPosition.y -= d.y;
        this.labelEdited = true;
        if (this.startNode) {
            this.startNode.recalculateAll();
        }

        this.isModified = true;
    }

    this.analyze = function (d) {
        var sn = this.startNode;
        while (sn != null) {
            sn = sn.nextNode;
        }
    }

    this.reconstruct = function () {
        var o = this.editor.origin.copy();
        var sn = this.startNode;
        while (sn != null) {
            var ref = sn.prevNode ? sn.prevNode.p : o;
            var dx = sn.dx * ((sn.sdx == "L") ? -1 : 1);
            var dy = sn.dy * ((sn.sdy == "U") ? -1 : 1);
            sn.x = ref.x - dx;
            sn.y = ref.y - dy;
            sn = sn.nextNode;
        }
        if (this.startNode) this.startNode.recalculateAll();
    }

    this.toString = function () {
        this.vectorString = this.editor.formatter.vectorToString(this);
        return this.vectorString;
    }

    this.labelCommandString = function () {
        if (this.editor.formatter.vectorToLabelCommand)
            return this.editor.formatter.vectorToLabelCommand(this);
        else
            return null;
    }

    this.dimensionCommandString = function () {
        if (this.editor.formatter.vectorToDimensionCommand)
            return this.editor.formatter.vectorToDimensionCommand(this);
        else
            return null;
    }

    this.toPointString = function () {
        var points = this.toPoints();
        var str = "";
        for (var x in points) { if (str != "") str += ", "; str += points[x].toString(); }
        return str;
    }

    if (startPoint != null)
        this.start(startPoint);

    editor.vectors.push(this);
}

function Sketch(editor) {
    this.uid = null;
    this.label = null;
    this.vectors = [];
    this.sections = {};
    this.notes = [];
    this.editor = editor;
    this.isModified = false;
    this.config = {};
    var s = this;

    this.reconstruct = function (actionCallback, callback) {
        for (var x in s.vectors) {
            var o = this.editor.origin.copy().alignToGrid(this.editor, Math.round);     //origin is rounded
            var cv = s.vectors[x];
            var nn = cv.startNode;
            while (nn != null) {
                if (actionCallback) actionCallback(nn, cv);
                //if (nn.sdx == "L") nn.sdx = "R"; else nn.sdx = "L";
                var dx = nn.dx; var dy = nn.dy;

                if (dx > 0) {
                    if (nn.sdx == "L") {
                        o.moveBy(-dx, 0)
                    } else {
                        o.moveBy(dx, 0);
                    }
                }
                if (dy > 0) {
                    if (nn.sdy == "D") {
                        o.moveBy(0, dy);
                    } else {
                        o.moveBy(0, -dy);
                    }
                }

                nn.p.x = o.x;
                nn.p.y = o.y;
                nn = nn.nextNode;
            }

            if (cv.startNode) {
                cv.startNode.recalculateAll();
            }
            if (callback) callback(cv);
            cv.isModified = true;
            //cv.reconstruct();
        }

        var sb = this.editor.sketchBounds();
        this.editor.resetOrigin();
        editor.panOrigin(sb, editor.formatter.originPosition);
        //this.editor.render();
    }

    this.flipHorizontal = function () {
        this.reconstruct(function (nn) {
            if (nn.sdx == "L") nn.sdx = "R"; else nn.sdx = "L";
            nn.arcLength = -nn.arcLength;
        }, function (cv) {
            if (!cv.labelPosition) return
            if (cv.labelPosition.x > 0) {
                cv.labelPosition.x = -cv.labelPosition.x;
            }
            else {
                cv.labelPosition.x = -cv.labelPosition.x;
            }
            cv.labelEdited = true;
        });
    }

    this.flipVertical = function () {
        this.reconstruct(function (nn) {

            if (nn.sdy == "U") nn.sdy = "D"; else nn.sdy = "U";
            nn.arcLength = -nn.arcLength;
        }, function (cv) {
            if (!cv.labelPosition) return
            if (cv.labelPosition.y > 0) {
                cv.labelPosition.y = -cv.labelPosition.y;
            }
            else {
                cv.labelPosition.y = -cv.labelPosition.y;
            }
            cv.labelEdited = true;
        });
    }

    this.renderNotes = function () {
        this.notes.forEach(function (x) { x.render() });
    }

    this.validateVectors = function () {
        var errorMessage = "";
        for (var x in this.vectors) {
            var v = this.vectors[x];
            if (v.isIncomplete() && (v.area() == 0 || v.area() > 1)) {
                return { valid: false, error: 'One or more segments are not closed. ' };
            }
        }

        return { valid: true };
    }

    //    this.notes.push(new Annotation(editor, "YES!", new PointX(30, 30)));
    //    this.notes.push(new Annotation(editor, "This is GOOD", new PointX(40, 40)));
}

function Annotation(editor, sketch, text, p, lp, bo, uid) {
    this.noteText = text;
    this.notePosition = p;
    this.lineStart = lp;
    this.editor = editor;
    this.sketch = sketch;
    this.boxOrgin = bo;
    this.boxWidth = 300;
    this.boxHeight = 100;
    this.isModified = false;
    this.noteid = uid;
    var n = this;
    this.isDeleted = false;
    this.newRecord = false;
    this.render = function () {
        if (this.isDeleted == true)
            return false;
        var ed = this.editor;
        var brush = ed.brush;
        var drawing = ed.drawing;

        var o = ed.origin;
        var sp = new PointX(o.x + this.notePosition.x * DEFAULT_PPF * ed.scale, o.y - this.notePosition.y * DEFAULT_PPF * ed.scale);
        var ls;
        if (this.lineStart) {
            if (this.lineStart.x != 0 && this.lineStart.y != 0) {
                ls = new PointX(o.x + this.lineStart.x * DEFAULT_PPF * ed.scale, o.y - this.lineStart.y * DEFAULT_PPF * ed.scale);
            }
        }


        brush.drawNotes(sp.x, sp.y, this, ls);

    }

    this.contains = function (pt) {
        var noteBox = [];
        var bo = this.boxOrgin;
        noteBox.push(bo);
        var so = new PointX(bo.x + this.boxWidth, bo.y)
        noteBox.push(so);
        noteBox.push(new PointX(so.x, so.y - this.boxHeight));
        noteBox.push(new PointX(bo.x, bo.y - this.boxHeight));
        for (var c = false, i = -1, l = noteBox.length, j = l - 1; ++i < l; j = i)
            ((noteBox[i].y <= pt.y && pt.y < noteBox[j].y) || (noteBox[j].y <= pt.y && pt.y < noteBox[i].y)) && (pt.x < (noteBox[j].x - noteBox[i].x) * (pt.y - noteBox[i].y) / (noteBox[j].y - noteBox[i].y) + noteBox[i].x) && (c = !c);
        if (c) return c;
        return c;
    }
    this.moveBy = function (d) {
        var sn = this.notePosition;
        var p2 = new PointX(Math.floor(sn.x), Math.floor(sn.y));
        sn.x -= d.x;
        sn.y -= d.y;
        this.isModified = true;
    }
}
function GetOrigin(editor, originPosition, scale) {
    var o;
    if (originPosition == "topRight")
        o = RoundX(editor.width - 80, 60, scale);
    else
        o = RoundX(40, editor.height - 40, scale);
    return o;
}
function PointX(x, y) {
    this.x = x;
    this.y = y;

    this.scale = function (s) {
        this.x *= Math.round(this.x * s);
        this.y *= Math.round(this.y * s);
        return this;
    }

    this.toUnits = function (editor) {
        var up = new PointX();
        up.x = (this.x - editor.origin.x) / DEFAULT_PPF / editor.scale;
        up.y = (editor.origin.y - this.y) / DEFAULT_PPF / editor.scale;
        return up;
    }

    this.moveBy = function (x, y) {
        this.x += x;
        this.y -= y;
        return this;
    }

    this.shift = function (x, y) {
        var np = new PointX(this.x, this.y);
        np.x += x;
        np.y -= y;
        return np;
    }

    this.setOffset = function (p) {
        this.x += p.x;
        this.y -= p.y;
        return this;
    }

    this.getOffset = function (dx, dy, scale) {
        if (!scale) scale = 1;
        return new PointX(this.x + dx * scale, this.y + dy * scale);
    }

    this.scale = function (s) {
        this.x *= s;
        this.y *= s;
        return this;
    }

    this.getDirectionInDegrees = function (p2) {
        var p1 = this;
        var angle = (Math.atan((p1.y - p2.y) / (p2.x - p1.x)) * 180 / Math.PI);

        if (p1.y == p2.y && p1.x > p2.x) {
            angle = 180;
        } else if (p1.x > p2.x && p1.y > p2.y) {
            angle = 180 + angle;
        } else if (p1.x > p2.x && p1.y < p2.y) {
            angle = 180 + angle;
        } else if (p1.x < p2.x && p1.y < p2.y) {
            angle = 360 + angle;
        } else if (p1.x == p2.x && p1.y < p2.y) {
            angle = 270;
        }

        return angle;
    }

    this.getDistanceTo = function (p2) {
        var p1 = this;
        return Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));
    }

    this.getPointAt = function (degree, distance) {
        distance = Math.abs(distance);
        if (degree >= 360) degree = degree - 360;
        var dH = distance * Math.cos(degree * Math.PI / 180);
        var dV = -distance * Math.sin(degree * Math.PI / 180);

        return new PointX(this.x + dH, this.y + dV);
    }

    this.midpointTo = function (p2) {
        var p1 = this;
        return new PointX((p1.x + p2.x) / 2, (p1.y + p2.y) / 2);
    }

    this.copy = function () {
        return new PointX(this.x, this.y);
    }

    this.overlaps = function (p) {
        if ((p.x == this.x) && (p.y == this.y)) {
            return true;
        } else {
            return false;
        }
    }

    this.mCopy = function (dx, dy) {
        return new PointX(this.x + dx, this.y + dy);
    }

    this.toString = function () {
        return "{" + Math.round(this.x) + "," + Math.round(this.y) + "}";
    }

    this.toYX = function () {
        return "{" + this.y + "," + this.x + "}";
    }

    this.alignToGrid = function (editor, mathOp) {
        if (mathOp == null)
            mathOp = sRound;

        var scale = editor.scale;
        var ppf = DEFAULT_PPF * scale;
        var o = this.copy();
        this.x = mathOp((this.x - editor.origin.x) / ppf);
        this.y = mathOp((editor.origin.y - this.y) / ppf);
        return this;
    }


    this.alignUnit = function (editor, mathOp) {
        if (mathOp == null)
            mathOp = sRound;

        this.alignToGrid(editor, mathOp);

        var scale = editor.scale;
        var ppf = DEFAULT_PPF * scale;
        var o = this.copy();
        this.x = CAMACloud.Sketching.Utilities.toPixel(this.x) * scale + editor.origin.x;
        this.y = editor.origin.y - CAMACloud.Sketching.Utilities.toPixel(this.y) * scale;
        return this;
    }

    this.distanceFrom = function (p) {
        return Math.sqrt(Math.pow(this.x - p.x, 2) + Math.pow(this.y - p.y, 2))
    }
}

function RoundX(x, y, scale) {
    var xx, yy;
    if (!scale) scale = 1;
    var sc = scale * 10;
    xx = Math.round(Math.round(x / sc) * sc);
    yy = Math.round(Math.round(y / sc) * sc);
    return new PointX(xx, yy);
}

function Brush(dwg, editor) {
    var brush = this;
    var drawing = dwg;
    var scale = 1;
    var offsetX = 0;
    var offsetY = 0;
    var currentPoint;
    this.editor = editor;

    drawing.lineWidth = 1;
    drawing.strokeStyle = 'Black';
    drawing.fillStyle = '#333';
    // this.lengthLabelFont = 'italic 8pt Arial';
    //this.labelFont = 'bold 10pt Arial';
    //this.areaFont = 'bold 9pt Arial';
    this.notesFont = '10pt Arial';

    this.__defineGetter__("currentPoint", function () { return currentPoint; });

    this.getFontSize = function (scale, vector) {
        var size = 0;
        if (!scale) scale = editor.scale;
        if (vector && vector.area() > 20000)
            size = 5;
        else if (vector && vector.area() > 5000)
            size = 4;
        else if (vector && vector.area() > 3000)
            size = 3;
        else if (vector && vector.area() > 1000 && scale < 0.6)
            size = 1;
        else if (vector && vector.area() > 500 && scale < 0.4)
            size = 2;
        if (scale == 1)
            return 8 + size;
        else if (scale < 0.1) {
            return 1;
        } else if ((scale >= 0.1) && (scale < 0.2)) {
            return 2 + size;
        } else if ((scale >= 0.2) && (scale < 0.4)) {
            return 4 + size;
        } else if ((scale >= 0.4) && (scale < 0.7)) {
            return 6 + size;
        } else if ((scale >= 0.7) && (scale < 1.2)) {
            return 8 + size;
        } else if ((scale >= 1.2) && (scale < 1.6)) {
            return 11 + size;
        } else if ((scale >= 1.6)) {
            return 13 + size;
        }
    }
    this.getFont = function (vector, type) {
        var fs = this.getFontSize(null, vector);
        if (type == "length")
            return 'italic ' + fs + 'pt Arial';
        else if (type == "label")
            return 'bold ' + Math.ceil(fs * 10 / 8) + 'pt Arial';
        else if (type == "area")
            return 'bold ' + Math.ceil(fs * 9 / 8) + 'pt Arial';
    }
    this.setScale = function (s) {
        scale = s;
        var fs = this.getFontSize(s);
        ////  this.lengthLabelFont = 'italic ' + fs + 'pt Arial';
        //this.labelFont = 'bold ' + Math.ceil(fs * 10 / 8) + 'pt Arial';
        //this.areaFont = 'bold ' + Math.ceil(fs * 9 / 8) + 'pt Arial';
        this.notesFont = '' + Math.ceil(fs * 10 / 8) + 'pt Arial';
    }

    this.setColor = function (stroke, fill) {
        if (stroke)
            drawing.strokeStyle = stroke;
        if (fill)
            drawing.fillStyle = fill;
    }

    this.setFillColor = function (c) {
        drawing.fillStyle = c;
    }

    this.setStrokeColor = function (c) {
        drawing.strokeStyle = c;
    }


    this.moveTo = function (p) {
        currentPoint = p;
        drawing.moveTo(currentPoint.x, currentPoint.y);
    }


    this.lineTo = function (p) {
        var o = currentPoint;
        currentPoint = p;
        drawing.lineTo(currentPoint.x, currentPoint.y);
    }

    this.moveBy = function (dx, dy) {
        currentPoint = new PointX(currentPoint.x + dx * scale, currentPoint.y - dy * scale);
        drawing.moveTo(currentPoint.x, currentPoint.y);
    }

    this.begin = function () {
        drawing.beginPath();
        return currentPoint;
    }

    this.end = function () {
        drawing.closePath();
        return currentPoint;
    }

    this.up = function (dy, stroke, stayAtStart) {
        this.any(0, -dy, stroke, stayAtStart);
    }

    this.down = function (dy, stroke, stayAtStart) {
        this.any(0, dy, stroke, stayAtStart);
    }

    this.left = function (dx, stroke, stayAtStart) {
        this.any(-dx, 0, stroke, stayAtStart);
    }

    this.right = function (dx, stroke, stayAtStart) {
        this.any(dx, 0, stroke, stayAtStart);
    }

    this.drawPath = function (vector, path, veer, stroke, labels) {
        if (veer) return this.veer(vector, path, stroke, labels);
        for (var pi in path) {
            var p = path[pi];
            brush.go(vector, p.dir, p.dist, stroke, false, labels);
        }
        return this;
    }

    this.go = function (vector, dir, dist, stroke, stayAtStart, labelsOnly) {
        var dx = 0, dy = 0;
        var sd = CAMACloud.Sketching.Utilities.toPixel(dist); // * scale;
        switch (dir) {
            case "U":
                dy = -sd;
                break;
            case "D":
                dy = sd;
                break;
            case "L":
                dx = -sd;
                break;
            case "R":
                dx = sd;
                break;
        }
        if ((dx != 0) || (dy != 0)) {
            if (labelsOnly)
                this.drawLabels(vector, dx, dy, dist);
            else
                this.any(dx, dy, stroke, stayAtStart, dist);
        }
    }

    this.veer = function (vector, path, stroke, labelsOnly) {
        var dx = 0, dy = 0;
        var dsq = 0;
        for (var x in path) {
            var p = path[x];
            var sd = CAMACloud.Sketching.Utilities.toPixel(p.dist); // * scale;
            dsq += p.dist * p.dist;
            switch (p.dir) {
                case "U":
                    dy = -sd;
                    break;
                case "D":
                    dy = sd;
                    break;
                case "L":
                    dx = -sd;
                    break;
                case "R":
                    dx = sd;
                    break;
            }
        }
        var adst = sRound(Math.sqrt(dsq));
        if (labelsOnly)
            this.drawLabels(vector, dx, dy, adst);
        else
            this.any(dx, dy, stroke, false, adst);
        return this;
    }

    this.any = function (dx, dy, stroke, stayAtStart, dist) {
        var nextPoint = new PointX(currentPoint.x + dx * scale, currentPoint.y + dy * scale);
        if (stroke) {
            drawing.lineTo(nextPoint.x, nextPoint.y);
            drawing.stroke();
            var o = currentPoint;
            var p = nextPoint;
        } else {
            drawing.moveTo(nextPoint.x, nextPoint.y);
        }
        if (!stayAtStart) { currentPoint = nextPoint } else drawing.moveTo(currentPoint.x, currentPoint.y);

        return this;
    }

    this.drawNotes = function (x, y, note, ls) {
        var text = note.noteText;
        if (!text)
            return true;
        function wrapText(context, text, x, y, maxWidth, lineHeight, measureOnly) {
            var textlines = text.split("\n");
            var textHeight = lineHeight;
            var line = '';
            for (var i in textlines) {
                var tline = textlines[i];
                var words = tline.split(' ');
                for (var n = 0; n < words.length; n++) {
                    var testLine = line + words[n] + ' ';
                    var metrics = context.measureText(testLine);
                    var testWidth = metrics.width;
                    if (testWidth > maxWidth - 10 && n > 0) {
                        if (!measureOnly) context.fillText(line, x, y);
                        line = words[n] + ' ';
                        y += lineHeight;
                        textHeight += lineHeight
                    }
                    else {
                        line = testLine;
                    }
                }
                if (!measureOnly) context.fillText(line, x, y);
                y += lineHeight;
                textHeight += lineHeight
                line = ''
            }
            // if (!measureOnly) context.fillText(line, x, y);
            return textHeight - lineHeight;
        }

        drawing.font = this.notesFont;
        brush.setColor('black', 'black');

        var lineHeight = brush.getFontSize(editor.scale) * 10 / 8 * 1.5;
        var metrics = drawing.measureText(text);
        var boxHeight = wrapText(drawing, text, x, y, 300 * editor.scale, lineHeight);
        var boxWidth = metrics.width > 300 * editor.scale ? 300 * editor.scale : metrics.width;

        var margin = 10 * editor.scale;
        drawing.beginPath();
        drawing.shadowBlur = 10;
        drawing.shadowColor = 'rgba(140,140,140,1)';

        //box Points
        var x1 = x - margin, y1 = y - lineHeight - margin, x2 = x + boxWidth + margin, y2 = y - lineHeight + boxHeight + margin;

        var bw = x2 - x1; var bh = y2 - y1;
        drawing.rect(x1, y1, bw, bh);
        note.boxWidth = Math.round((bw / editor.scale) / DEFAULT_PPF);
        note.boxHeight = Math.round((bh / editor.scale) / DEFAULT_PPF);
        note.boxOrgin = new PointX(x1, y1).alignToGrid(editor, Math.round);
        var grd = editor.drawing.createLinearGradient(x1, y1, x2, y2);
        grd.addColorStop(0, 'rgba(248,237,98,1)')
        grd.addColorStop(1, 'rgba(248,237,98,1)')
        brush.setColor('#dab600', grd)
        if (note == editor.currentSketchNote) {
            drawing.fillStyle = '#FFBB00';
        }
        drawing.stroke();
        drawing.fill()
        drawing.shadowBlur = 0;
        drawing.shadowColor = null;
        brush.setColor('Black', 'Black');
        wrapText(drawing, text, x, y, 300 * editor.scale, lineHeight);

        if (ls) {
            var cp = (new PointX(x1, y1)).midpointTo(new PointX(x2, y1));
            if (ls.getDistanceTo(new PointX(x2, y1)) < ls.getDistanceTo(cp)) cp = (new PointX(x2, y1)).midpointTo(new PointX(x1, y1));
            if (ls.getDistanceTo(new PointX(x1, y2)) < ls.getDistanceTo(cp)) cp = (new PointX(x1, y2)).midpointTo(new PointX(x2, y2));
            if (ls.getDistanceTo(new PointX(x2, y2)) < ls.getDistanceTo(cp)) cp = (new PointX(x2, y2)).midpointTo(new PointX(x1, y2));

            drawing.beginPath();
            drawing.moveTo(ls.x, ls.y);
            drawing.lineWidth = 5;
            drawing.lineTo(cp.x, cp.y);
            brush.setColor('#777', 'rgba(248,237,98,1)');
            drawing.stroke();
            drawing.beginPath();
            drawing.lineWidth = 1;
            brush.setColor('Black', 'rgba(248,237,98,1)');
            drawing.arc(ls.x, ls.y, 3, 0, 2 * Math.PI, 0);
            drawing.fill();
            drawing.stroke();
        }
        return this;
    }

    this.drawLabels = function (vector, dx, dy, dist) {
        dist = sRound(dist);
        if (editor.labelColorCode) {
            editor.brush.setColor(editor.labelColorCode, editor.labelColorCode);
        }
        else if (vector == editor.currentVector) {
            editor.brush.setColor('Black', 'Blue');
        } else {
            editor.brush.setColor('Red', 'Red');
        }
        var nextPoint = new PointX(currentPoint.x + dx * scale, currentPoint.y + dy * scale);

        var fs = brush.getFontSize(null, vector);
        if (fs > 5 || (vector.area() > 1000 && fs > 3)) {
            if (dist) {
                var mp = new PointX((currentPoint.x + nextPoint.x) / 2, (currentPoint.y + nextPoint.y) / 2);
                var distText = dist + (this.editor.formatter.LengthUnit || DISTANCE_UNIT || '');
                drawing.font = this.getFont(vector, "length");
                var margin = 2 * editor.scale;
                var th = brush.getFontSize(editor.scale, vector) * 1.5;
                var tw = drawing.measureText(distText).width + 2 * editor.scale;

                var dir = "*";
                if (dx == 0) dir = "V";
                if (dy == 0) dir = "H";
                var lp = mp.copy();
                var labelPosition = "*";
                if (dir != "*") {
                    if (dir == "H") if (vector.contains(lp.toUnits(editor).mCopy(0, 2))) { lp = new PointX(mp.x - (tw / 2), mp.y + th); labelPosition = "B"; }       //BOTTOM
                    if (dir == "H") if (vector.contains(lp.toUnits(editor).mCopy(0, -2))) { lp = new PointX(mp.x - (tw / 2), mp.y - margin); labelPosition = "T"; }       // TOP
                    if (dir == "V") if (vector.contains(lp.toUnits(editor).mCopy(2, 0))) { lp = new PointX(mp.x - tw - margin, mp.y - th / 2); labelPosition = "L"; }    //LEFT
                    if (dir == "V") if (vector.contains(lp.toUnits(editor).mCopy(-2, 0))) { lp = new PointX(mp.x + margin, mp.y - th / 2); labelPosition = "R"; }         // RIGHT
                } else {
                    if (dx > 0 && dy < 0) if (vector.contains(mp.copy().toUnits(editor).mCopy(-1, 1))) { lp = new PointX(mp.x + th, mp.y + th + margin); labelPosition = "BR"; } else { lp = new PointX(mp.x - tw, mp.y - th / 2); labelPosition = "BR"; }      // TOP-LEFT
                    if (dx < 0 && dy < 0) if (vector.contains(mp.copy().toUnits(editor).mCopy(-1, -1))) { lp = new PointX(mp.x + margin, mp.y - th / 2); labelPosition = "TR"; } else { lp = new PointX(mp.x - tw, mp.y + tw); labelPosition = "TR"; }  //LEFT
                    if (dx < 0 && dy > 0) if (vector.contains(mp.copy().toUnits(editor).mCopy(1, -1))) { lp = new PointX(mp.x - tw - th / 2, mp.y - th / 2); labelPosition = "TL"; } else { lp = new PointX(mp.x + margin, mp.y + th); labelPosition = "TL"; }       // TOP
                    if (dx > 0 && dy > 0) if (vector.contains(mp.copy().toUnits(editor).mCopy(1, 1))) { lp = new PointX(mp.x - tw - margin, mp.y + th); labelPosition = "BL"; } else { lp = new PointX(mp.x + margin, mp.y - th / 2); labelPosition = "BL"; }    //BOTTOM-LEFT
                }


                //if (vector == editor.currentVector) console.log(dx, dy, labelPosition, distText, tw, th, margin, mp.toString(), dir,  lp.toString())
                drawing.fillText(distText, lp.x, lp.y);
            }
        }

        currentPoint = nextPoint;
        return this;
    }
    this.drawCircleRadiuslabel = function (vector, radius, center) {
        radius = sRound(radius);
        var mpl = new PointX(currentPoint.x + CAMACloud.Sketching.Utilities.toPixel(center.x + (radius / 2)) * scale, currentPoint.y - CAMACloud.Sketching.Utilities.toPixel(center.y) * scale);
        drawing.font = this.getFont(vector, "length");
        drawing.fillText(radius, mpl.x, mpl.y - 5)
    }
    this.drawArcLabel = function (vector, mx, my, dx, dy, length, arcLength) {
        var unit = (this.editor.formatter.LengthUnit || DISTANCE_UNIT || '');
        length = sRound(length);
        arcLength = sRound(arcLength);

        var mpl = new PointX(currentPoint.x + CAMACloud.Sketching.Utilities.toPixel(mx) * scale, currentPoint.y - CAMACloud.Sketching.Utilities.toPixel(my) * scale);
        drawing.font = this.getFont(vector, "length");
        drawing.fillText(length + unit + " / " + arcLength + unit, mpl.x + 1, mpl.y - 2);

        var nextPoint = new PointX(currentPoint.x + CAMACloud.Sketching.Utilities.toPixel(dx) * scale, currentPoint.y - CAMACloud.Sketching.Utilities.toPixel(dy) * scale);
        currentPoint = nextPoint;
        return this;
    }

    this.rect = function (w, h) {
        drawing.rect(currentPoint.x, currentPoint.y, w * scale, h * scale);
    }

    this.rectX = function (w, h) {
        var xx = Math.floor((w * scale) / 2);
        var yy = Math.floor((h * scale) / 2);
        currentPoint = new PointX(currentPoint.x - xx, currentPoint.y - yy);
        drawing.rect(currentPoint.x, currentPoint.y, w * scale, h * scale);
    }

    this.pointer = function (w, h, angle) {
        var hs = h * scale;
        var ws = w * scale;
        var xx = Math.floor((ws) / 2);
        var yy = Math.floor((hs) / 2);
        var px = Math.ceil(ws / 2);
        var py = Math.ceil(hs / 2);
        var cx = currentPoint.x, cy = currentPoint.y;
        drawing.translate(cx, cy);
        drawing.rotate(-angle * Math.PI / 180);
        drawing.translate(-cx, -cy);

        currentPoint = new PointX(currentPoint.x - xx, currentPoint.y - yy);
        drawing.moveTo(currentPoint.x, currentPoint.y);
        drawing.lineTo(currentPoint.x + ws, currentPoint.y);
        drawing.lineTo(currentPoint.x + ws + px, currentPoint.y + py);
        drawing.lineTo(currentPoint.x + ws, currentPoint.y + hs);
        drawing.lineTo(currentPoint.x, currentPoint.y + hs);
        drawing.lineTo(currentPoint.x, currentPoint.y);

        drawing.translate(cx, cy);
        drawing.rotate(angle * Math.PI / 180);
        drawing.translate(-cx, -cy);

    }


    this.circle = function (r) {
        drawing.arc(currentPoint.x, currentPoint.y, r * scale * DEFAULT_PPF, 0, 2 * Math.PI);
    }

    this.drawEllipse = function (path, stroke, labels) {
        if (path.length < 2) return this;
        var a = CAMACloud.Sketching.Utilities.toPixel(path[0].dist);
        var b = CAMACloud.Sketching.Utilities.toPixel(path[1].dist);
        this.ellipse(b, a, stroke);
        return this;
    }

    this.drawArc = function (path, arcLength, node) {
        var p1 = new PointX(currentPoint.x, currentPoint.y);
        var dx = 0, dy = 0;
        var dsq = 0;
        for (var x in path) {
            var p = path[x];
            var sd = CAMACloud.Sketching.Utilities.toPixel(p.dist); // * scale;
            dsq += p.dist * p.dist;
            switch (p.dir) {
                case "U":
                    dy = -sd;
                    break;
                case "D":
                    dy = sd;
                    break;
                case "L":
                    dx = -sd;
                    break;
                case "R":
                    dx = sd;
                    break;
            }
        }
        var diam = CAMACloud.Sketching.Utilities.toPixel(sRound(Math.sqrt(dsq)));

        var p2 = p1.getOffset(dx, dy, scale);
        var c = p1.midpointTo(p2);

        var angle = p1.getDirectionInDegrees(p2);

        var pangle = angle + ARC_SIGN * sign(arcLength) * 90;
        if (pangle > 360) pangle = pangle - 360;

        var p3 = c.getPointAt(pangle, CAMACloud.Sketching.Utilities.toPixel(arcLength) * scale);

        var ap1p3 = p1.getDirectionInDegrees(p3);
        var leftInclusive = Math.abs(ap1p3 - angle);
        if (leftInclusive > 90) leftInclusive = 360 - leftInclusive;
        var topInclusive = 180 - 2 * leftInclusive;
        var dp1p3 = p1.getDistanceTo(p3);
        var radius = Math.abs((dp1p3 / 2) / Math.cos(topInclusive / 2 * Math.PI / 180));

        var rpangle = pangle + 180;
        if (rpangle > 360) rpangle = rpangle - 360;
        var cc = p3.getPointAt(rpangle, radius);

        var startAngle = (360 - cc.getDirectionInDegrees(p1))
        var endAngle = (360 - cc.getDirectionInDegrees(p2))
        var midAngle = (360 - cc.getDirectionInDegrees(p3));

        //console.log("*", startAngle - midAngle, midAngle - endAngle);

        var check1 = startAngle - midAngle; if (check1 > 180) check1 = check1 - 360; if (check1 < -180) check1 = 360 + check1;
        var check2 = midAngle - endAngle; if (check2 > 180) check2 = check2 - 360; if (check2 < -180) check2 = 360 + check2;
        //console.log(check1, check2);
        drawing.arc(cc.x, cc.y, radius, startAngle * Math.PI / 180, endAngle * Math.PI / 180, check1 > 0);

        //        this.moveTo(cc);
        //        this.circle(4)
        //        this.moveTo(p3)
        //        this.rectX(4, 4)
        this.lineTo(p2);
        return this;
    }

    this.arc180 = function (rx, ry, angle, stroke) {
        var center = new PointX(currentPoint.x + rx * scale, currentPoint.y);

        rx = rx * scale;
        ry = ry * scale;
        var sx = 1, sy = 1;
        if (rx > ry)
            sy = ry / rx;
        if (ry > rx)
            sx = rx / ry;
        drawing.scale(sx, sy);
        drawing.arc(center.x / sx, center.y / sy, rx / sx, Math.PI, 0, 0);
        if (stroke)
            drawing.stroke();
        drawing.scale(1 / sx, 1 / sy);
    }

    this.ellipse = function (rx, ry, stroke) {
        var center = new PointX(currentPoint.x + rx * scale, currentPoint.y);

        rx = rx * scale;
        ry = ry * scale;
        var sx = 1, sy = 1;
        if (rx > ry)
            sy = ry / rx;
        if (ry > rx)
            sx = rx / ry;
        drawing.scale(sx, sy);
        drawing.arc(center.x / sx, center.y / sy, rx / sx, -1 * Math.PI, 1 * Math.PI);
        if (stroke)
            drawing.stroke();
        drawing.scale(1 / sx, 1 / sy);
    }

    this.write = function (p, text, style) {
        var o = editor.origin.copy();
        drawing.font = style;
        drawing.fillText(text, (p.x * DEFAULT_PPF + o.x), (o.y - p.y * DEFAULT_PPF));
    }

    this.stroke = function () {
        drawing.stroke();
    }

    this.fill = function () {
        drawing.fill();
    }

    this.strokeAndFill = function () {
        drawing.stroke();
        drawing.fill();
    }
}

function PanControl(selector, editor) {
    var control;
    var pc = this;
    this.panUp = function () { }
    this.panDown = function () { }
    this.panLeft = function () { }
    this.panRight = function () { }
    this.panReset = function () { }

    function init() {
        $(control).bind('touchstart mousedown', function () {
            var x, y
            if (event.touches) {
                x = event.touches[0].clientX - event.srcElement.offsetLeft;
                y = event.touches[0].clientY - event.srcElement.offsetTop;
            } else {
                x = event.offsetX;
                y = event.offsetY;
            }
            if ((x >= 50) && (x <= 100) && (y >= 2) && (y <= 50)) {
                if (editor.currentVector)
                    editor.currentVector.moveBy(new PointX(0, -0.1), true);
                else {
                    $(control).attr('class', 'control-window pan-control pan-up');
                    pc.panUp();
                }
            }
            else if ((x >= 50) && (x <= 100) && (y >= 100) && (y <= 148)) {
                if (editor.currentVector)
                    editor.currentVector.moveBy(new PointX(0, 0.1), true);
                else {
                    $(control).attr('class', 'control-window pan-control pan-down');
                    pc.panDown();
                }
            }
            else if ((x >= 2) && (x <= 50) && (y >= 50) && (y <= 148)) {
                if (editor.currentVector)
                    editor.currentVector.moveBy(new PointX(0.1, 0), true);
                else {
                    $(control).attr('class', 'control-window pan-control pan-left');
                    pc.panLeft();
                }
            }
            else if ((x >= 100) && (x <= 150) && (y >= 50) && (y <= 148)) {
                if (editor.currentVector)
                    editor.currentVector.moveBy(new PointX(-0.1, 0), true);
                else {
                    $(control).attr('class', 'control-window pan-control pan-right');
                    pc.panRight();
                }
            }
            else if ((x > 50) && (x < 100) && (y > 50) && (y < 150)) {
                if (!editor.currentVector) {
                    $(control).attr('class', 'control-window pan-control pan-reset');
                    pc.panReset();
                }
            }
            else {
                $(control).attr('class', 'control-window pan-control pan-normal');
            }
            if (editor.currentVector) sketchApp.render();
        });
        $(control).bind('touchend mouseup', function () {
            $(control).attr('class', 'control-window pan-control pan-normal');
        });
        $(control).bind('touchleave touchcancel mouseleave', function () {
            $(control).attr('class', 'control-window pan-control pan-normal');
        });
    }


    control = $(selector);
    if (control.length == 0) {
        return;
    }
    init();
}

function ZoomControl(selector) {
    var control;
    var range, plus, minus;
    var c = this;
    var mouseLock;


    var zoomPercent = 49;
    var rangeStartValue;
    var rangeFinalValue;

    this.zoom = function (z) {

    }

    this.setZoom = function (z) {
        z = Math.round(z);
        c.updateRange(z);
    }

    function init() {
        range = $('.range', control);
        minus = $('.minus', control);
        plus = $('.plus', control);

        $(plus).unbind(touchClickEvent);
        $(plus).bind(touchClickEvent, function (e) {
            e.preventDefault();
            zoomPercent += 2.5;
            if (zoomPercent > 100) {
                zoomPercent = 100;
            }
            c.updateRange();
            c.zoom(zoomPercent);
        });

        $(minus).unbind(touchClickEvent);
        $(minus).bind(touchClickEvent, function (e) {
            e.preventDefault();
            zoomPercent -= 2.5;
            if (zoomPercent < 0) {
                zoomPercent = 0;
            }
            c.updateRange();
            c.zoom(zoomPercent);
        });

        $(range).bind('touchstart touchmove touchend touchleave touchcancel mousedown mousemove mouseup mouseleave', function () {
            event.preventDefault();
            if (event.touches) {
                switch (event.type) {
                    case 'touchstart':
                        rangeStartValue = Math.round((event.touches[0].clientX - event.srcElement.offsetLeft) / 302 * 100);
                        c.updateRange(rangeStartValue);
                        break;
                    case 'touchmove':
                        rangeFinalValue = Math.round((event.touches[0].clientX - event.srcElement.offsetLeft) / 302 * 100);
                        c.updateRange(rangeFinalValue);
                        c.zoom(zoomPercent);
                        break;
                    case 'touchend':
                        if (event.touches)
                            if (event.touches.length > 0) {
                                rangeFinalValue = Math.round((event.touches[0].clientX - event.srcElement.offsetLeft) / 302 * 100);
                                c.updateRange(rangeFinalValue);
                                c.zoom(zoomPercent);
                            }
                        break;
                    default:
                        break;
                }
            } else {
                switch (event.type) {
                    case 'mousedown':
                        mouseLock = true;
                        rangeStartValue = Math.round((event.offsetX) / 302 * 100);
                        c.updateRange(rangeStartValue);
                        break;
                    case 'mousemove':
                        if (mouseLock) {
                            rangeFinalValue = Math.round((event.offsetX) / 302 * 100);
                            c.updateRange(rangeFinalValue);
                            c.zoom(zoomPercent);
                        }
                        break;
                    case 'mouseup':
                        mouseLock = false;
                        rangeFinalValue = Math.round((event.offsetX) / 302 * 100);
                        c.updateRange(rangeFinalValue);
                        c.zoom(zoomPercent);
                        break;
                    default:
                        mouseLock = false;
                        break;
                }
            }

        });
    }


    this.updateRange = function (z) {
        if (z)
            zoomPercent = z;
        if (zoomPercent > 100) {
            zoomPercent = 100;
        }
        if (zoomPercent < 0) {
            zoomPercent = 0;
        }

        var rperc = Math.round(zoomPercent) + '% 100%'
        $(range).css({ 'background-size': rperc });
    }

    control = $(selector);
    if (control.length == 0) {
        return;
    }

    init();
}

function SketchEditorToolbar(selector, editor) {
    var control;
    this.editor = editor;
    var tb = this;

    function activate(c) {
        $(c).removeClass('ccse-tool-inactive');
        $(c).addClass('ccse-tool-active');
    }

    function deactivate(c) {
        $(c).removeClass('ccse-tool-active');
        $(c).addClass('ccse-tool-inactive');
    }

    function isActive(c) {
        return $(c).hasClass('ccse-tool-active');
    }

    function init() {
        $('.control-window', control).hide();
        $('.ccse-toolbutton', control).removeClass('ccse-tool-active');
        $('.ccse-toolbutton', control).addClass('ccse-tool-inactive');

        $('.ccse-toolbutton', control).unbind();
        bindToolbarButton('.ccse-pan', '.pan-control');
        bindToolbarButton('.ccse-zoom', '.zoom-control');
        bindToolbarButton('.ccse-angle-lock');
        $('.ccse-undo').unbind(touchClickEvent);
        $('.ccse-undo').bind(touchClickEvent, function (e) {
            e.preventDefault();
            if (tb.editor && tb.editor.undo) tb.editor.undo();
            return false;
        });

        $('.ccse-show-pad').unbind(touchClickEvent);
        $('.ccse-show-pad').bind(touchClickEvent, function (e) {
            e.preventDefault();
            tb.editor.showKeypad();
            return false;
        });

        $('.ccse-delete-node').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                if (tb.editor) {
                    tb.editor.deleteCurrentNode();
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });

        $('.ccse-delete-vector').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                if (tb.editor) {
                    messageBox(' Are you sure you want to delete the selected segment ?', ["OK", "Cancel"], function () {
                        tb.editor.deleteCurrentVector();
                    }, function () {
                        return;
                    })
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });

        $('.ccse-copy-vector').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                if (tb.editor) {
                    tb.editor.copyCurrentVector();
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });
        $('.ccse-paste-vector').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                e.preventDefault();
                if (tb.editor) {
                    if (editor.currentVector) {
                        $(sketchApp.vectorSelector).val('');//   While mid-Sketching
                        sketchApp.currentVector = null;
                        sketchApp.raiseModeChange(); sketchApp.renderAll();
                        sketchApp.mode = CAMACloud.Sketching.MODE_DEFAULT; sketchApp.currentNode = null;
                    }
                    tb.editor.pasteVector();
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });

        $('.ccse-add-vector').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                if (tb.editor) {
                    if (editor.currentVector) {
                        $(sketchApp.vectorSelector).val('');//   While mid-Sketching
                        sketchApp.currentVector = null;
                        sketchApp.raiseModeChange(); sketchApp.renderAll();
                        sketchApp.mode = CAMACloud.Sketching.MODE_DEFAULT; sketchApp.currentNode = null;
                    }
                    tb.editor.createNewVector();
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });

        $('.ccse-add-segment').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                if (tb.editor) {
                    tb.editor.createNewVectorSegment();
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });

        $('.ccse-transfer-vector').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                if (tb.editor) {
                    tb.editor.transferVector();
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });

        $('.ccse-add-node-before').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                if (tb.editor) {
                    tb.editor.addNodeBeforeCurrent();
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });

        $('.ccse-save').click(function () {
            try {
                if (tb.editor) {
                    tb.editor.save();
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });


        $('.ccse-close').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                if (tb.editor) {
                    tb.editor.close();

                }
            } catch (e) {
                alert(e);
            }
            return false;
        });

        $('.ccse-flip-v').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                if (tb.editor) {
                    tb.editor.resetSketchMovable();
                    tb.editor.currentSketch.flipVertical();
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });


        $('.ccse-flip-h').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                if (tb.editor) {
                    tb.editor.resetSketchMovable();
                    tb.editor.currentSketch.flipHorizontal();
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });

        $('.ccse-sketch-move').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                if (tb.editor) {
                    tb.editor.toggleSketchMovable();
                    if (tb.editor.sketchMovable) {
                        $('.ccse-sketch-move').html('Release');
                    } else {
                        $('.ccse-sketch-move').html('Move');
                    }
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });

        $('.ccse-notes-toggle').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                if (tb.editor) {
                    tb.editor.displayNotes = !tb.editor.displayNotes;
                    if (tb.editor.displayNotes) {
                        $('.ccse-notes-toggle').html('Hide Notes');
                    } else {
                        $('.ccse-notes-toggle').html('Show Notes');
                    }
                    tb.editor.render();
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });
        $('.ccse-notes-edit').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                if (tb.editor) {
                    var note = tb.editor.currentSketchNote;
                    input('', "Edit Note:", function (newnote) {
                        note.noteText = newnote;
                        note.isModified = true;
                        tb.editor.render();
                    }, function () { }, note.noteText, 'textarea');
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });
        $('.ccse-notes-delete').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                if (tb.editor) {
                    tb.editor.deleteCurrentSketchNote();
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });
        $('.ccse-notes-new').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                if (tb.editor) {
                    tb.editor.createNewSketchNote();
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });
        $('.ccse-edit-label').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                if (tb.editor) {
                    tb.editor.editCurrentVectorLabel();
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });
        $('.ccse-move-label').bind(touchClickEvent, function (e) {
            e.preventDefault();
            try {
                if (tb.editor) {
                    messageBox('Tap on any point to move the selected vector label.', function () {
                        tb.editor.mode = CAMACloud.Sketching.MODE_MOVE;
                    });
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });
        if (tb.editor) {
            tb.editor.setControlList('.ccse-sketch-select', '.ccse-vector-select');
            tb.editor.onModeChange = function (mode, currentVector, currentNode, currentSketchNote) {
                $('.ccse-mode').hide();
                $('.ccse-sketch-move').html('Move');
                var wasMovable = false;
                if (tb.editor.sketchMovable) { wasMovable = true }
                tb.editor.sketchMovable = false;
                if (wasMovable)
                    tb.editor.render();
                switch (mode) {
                    case CAMACloud.Sketching.MODE_NEW:
                        $('.ccse-mode-add-segment').css({ 'display': "inline-block" });
                        $('.ccse-mode-line').css({ 'display': "inline-block" });
                        $('.ccse-mode-select-vector').css({ 'display': "inline-block" });
                        if ((tb.editor.formatter.allowSegmentAddition || tb.editor.currentSketch.config.AllowSegmentAddition) && tb.editor.formatter.allowSegmentAdditionOnMidsketch)
                            $('.ccse-mode-select-sketch-plus').css({ 'display': "inline-block" });
                        if (tb.editor.formatter.allowSegmentAddition || tb.editor.currentSketch.config.AllowSegmentAddition || tb.editor.currentSketch.config.AllowMultiSegmentAddDelete)
                            $('.ccse-mode-copy-vector').css({ 'display': "inline-block" });
                        break;
                    case CAMACloud.Sketching.MODE_EDIT:
                        if (tb.editor.currentNode) {
                            $('.ccse-mode-select-node').css({ 'display': "inline-block" });

                        } else if (tb.editor.currentVector) {
                            if (tb.editor.formatter.allowLabelEdit || tb.editor.currentSketch.config.AllowLabelEdit)
                                $('.ccse-mode-select-vector-plus').css({ 'display': "inline-block" });
                            if (tb.editor.currentSketch.config.AllowMultiSegmentAddDelete)
                                $('.ccse-mode-multi-segment').css({ 'display': "inline-block" });
                            $('.ccse-mode-select-vector').css({ 'display': "inline-block" });
                            if (tb.editor.currentSketch.config.AllowTransferOut)
                                $('.ccse-mode-transfer-segment').css({ 'display': "inline-block" });
                            if (tb.editor.formatter.allowSegmentAddition || tb.editor.currentSketch.config.AllowSegmentAddition || tb.editor.currentSketch.config.AllowMultiSegmentAddDelete)
                                $('.ccse-mode-copy-vector').css({ 'display': "inline-block" });
                            $('.ccse-mode-select-label-move').css({ 'display': "inline-block" });
                            tb.editor.hideKeypad();
                        } else if (tb.editor.currentSketchNote)
                        { $('.ccse-mode-notes-edit').css({ 'display': "inline-block" }); $('.ccse-mode-notes-delete').css({ 'display': "inline-block" }); $('.ccse-mode-notes-add').css({ 'display': "inline-block" }); }
                        else if (tb.editor.currentSketch) {
                            if (tb.editor.formatter.allowSegmentAddition || tb.editor.currentSketch.config.AllowSegmentAddition)
                                $('.ccse-mode-select-sketch-plus').css({ 'display': "inline-block" });

                            $('.ccse-mode-select-sketch').css({ 'display': "inline-block" });

                            tb.editor.hideKeypad();
                        } else if (copyVector) {
                            $('.ccse-mode-paste-vector').css({ 'display': "inline-block" });
                        } else if (window.opener) {
                            if (window.opener.copyVector)
                                $('.ccse-mode-paste-vector').css({ 'display': "inline-block" });
                        }

                        else {
                            tb.editor.hideKeypad();
                        }
                        break;
                    case CAMACloud.Sketching.MODE_DEFAULT:
                        if (tb.editor.currentSketch) {
                            if (tb.editor.currentSketch.config.NotesSource && tb.editor.external.fieldCategories.filter(function (u) { return u.SourceTableName == tb.editor.currentSketch.config.NotesSource.Table }).length > 0)
                                $('.ccse-mode-notes-new').css({ 'display': "inline-block" });
                            if (tb.editor.formatter.allowSegmentAddition || tb.editor.currentSketch.config.AllowSegmentAddition)
                                $('.ccse-mode-select-sketch-plus').css({ 'display': "inline-block" });
                            if (tb.editor.formatter.allowSegmentAddition || tb.editor.currentSketch.config.AllowSegmentAddition || tb.editor.currentSketch.config.AllowMultiSegmentAddDelete) {
                                if (tb.editor.currentVector)
                                    $('.ccse-mode-copy-vector').css({ 'display': "inline-block" });
                                if (copyVector)
                                    $('.ccse-mode-paste-vector').css({ 'display': "inline-block" });
                                if (window.opener) {
                                    if (window.opener.copyVector)
                                        $('.ccse-mode-paste-vector').css({ 'display': "inline-block" });
                                }
                            }
                            $('.ccse-mode-select-sketch').css({ 'display': "inline-block" });
                            tb.editor.hideKeypad();
                        } else {
                            tb.editor.hideKeypad();
                        }

                        break;
                }

                if (tb.editor.currentSketch)
                    if (tb.editor.currentSketch.notes.length > 0 && !tb.editor.currentSketchNote) {
                        $('.ccse-mode-notes').css({ 'display': "inline-block" });
                        if (tb.editor.displayNotes) {
                            $('.ccse-notes-toggle').html('Hide Notes');
                        } else {
                            $('.ccse-notes-toggle').html('Show Notes');
                        }
                    }

            }
            tb.editor.onDrawLine = function (p1, p2, v) {
                var p3, p4, prevnode;
                var d = sRound(p1.distanceFrom(p2) / (DEFAULT_PPF * tb.editor.scale));
                var dy = p2.y - p1.y;
                var dx = p2.x - p1.x;
                var angle = p1.getDirectionInDegrees(p2);
                $('.ccse-meter-length').val(d + (tb.editor.formatter.LengthUnit || DISTANCE_UNIT || ''));
                if (tb.editor.currentNode) {
                    p3 = tb.editor.currentNode.p;
                    prevnode = tb.editor.currentNode.prevNode
                }
                if (prevnode)
                    p4 = tb.editor.currentNode.prevNode.p;
                if (p3 && p4) {
                    var angle2 = p3.getDirectionInDegrees(p4);
                    var angle = (360 - angle2) - angle;
                    if (angle < 0) {
                        angle = 360 + angle
                    }
                    $('.ccse-meter-angle').val(parseInt(angle) + ' °');
                }
            }

            tb.editor.showStatus = function (message) {
                $('.sketch-label').html(message);
            }
            tb.editor.setZoomValue = function (z) { $('.ccse-zoom-value span').html(z); }
        }
    }

    function bindToolbarButton(buttonSelector, controlSelector) {
        $(buttonSelector, control).bind('mouseup touchend', function () {
            event.preventDefault();
            $('.control-window', control).hide();
            if (isActive(this)) {
                deactivate(this);
                if (controlSelector) $(controlSelector, control).hide();
                if (buttonSelector == '.ccse-angle-lock') tb.editor.angleLock = false;
            } else {
                $('.ccse-toolbutton', control).removeClass('ccse-tool-active');
                $('.ccse-toolbutton', control).addClass('ccse-tool-inactive');
                activate(this);
                if (controlSelector) $(controlSelector, control).show();
                if (buttonSelector == '.ccse-angle-lock') tb.editor.angleLock = true
            }

        });
    }

    control = $(selector);

    if (control.length == 0) {
        throw 'No toolbar found';
        return;
    }

    init();
}

function CCSketchPad(s, e) {
    var p = this;
    this.selector = s;
    this.editor = e;
    this.activeText = null;
    this.activeTextValue = 0;
    this.decimal = false;
    this.minus = false;
    this.activeTextDecimal = '';
    this.activeDecimalPoints = 0;
    var pos, ref, mov;
    var locked = false;

    function lockKeys() {
        locked = true;
        window.setTimeout(function () { locked = false; }, 200);
    }

    this.reset = function () {
        this.activeTextValue = 0;
        this.activeTextDecimal = '';
        this.activeDecimalPoints = 0;
        this.decimal = false;
        this.minus = false;
        $('.box-length span', this.selector).text('0');
        $('.box-angle span', this.selector).text('0');
        $('.sp-text-box', this.selector).removeAttr('selected');
        $('.box-length', this.selector).attr('selected', 'true');
    }

    this.reset = function () {
        this.activeTextValue = 0;
        this.activeTextDecimal = '';
        this.activeDecimalPoints = 0;
        this.decimal = false;
        this.minus = false;
        $('.box-length span', this.selector).text('0');
        $('.box-angle span', this.selector).text('0');
        $('.sp-text-box', this.selector).removeAttr('selected');
        $('.box-length', this.selector).attr('selected', 'true');
        this.activeText = $('.sp-text-box[selected]', this.selector)[0];
    }

    this.resetText = function () {
        this.activeTextValue = 0;
        this.activeTextDecimal = '';
        this.activeDecimalPoints = 0;
        this.decimal = false;
        this.minus = false;
        $('.sp-text-box[selected] span', this.selector).text('0');
    }

    function getTextValue(txt) {
        p.activeTextValue = parseInt($('span', txt).text());
        p.activeTextDecimal = (parseFloat($('span', txt).text()) - parseInt($('span', txt).text())).toString().replace('0.', '');
        if (p.activeTextDecimal == '0') p.activeTextDecimal = '';
    }

    function setTextValue(txt) {
        var num = (p.minus ? '-' : '') + p.activeTextValue + (p.activeTextDecimal != '' ? '.' + p.activeTextDecimal : '');
        $('span', txt).text(num)
    }

    function distance() {
        return parseFloat($('.box-length span', this.selector).text())
    }

    function angle() {
        return parseFloat($('.box-angle span', this.selector).text())
    }

    function initKeys() {
        var sel = this.selector;

        p.activeText = $('.sp-text-box[selected]', sel)[0];
        getTextValue(p.activeText);


        $('.sp-text-box', sel).bind(touchClickEvent, function (e) {
            e.preventDefault();
            $('.sp-text-box', sel).removeAttr('selected');
            $(this).attr('selected', 'true');
            p.activeText = this;
            getTextValue(p.activeText);
        });

        $('.sketch-pad-num', sel).bind('touchend mouseup', function (e) {
            e.preventDefault();
            if (locked)
                return;
            lockKeys();
            var key = $(this).text();
            var intval = null;
            var maxValue = parseInt($(p.activeText).attr('max'));
            var minValue = parseInt($(p.activeText).attr('min'));
            switch (key) {
                case "CLEAR":
                    p.resetText();
                    return;
                    break;
                case ".":
                    p.decimal = true;
                    break;
                case "-":
                    if (minValue < 0)
                        p.minus = !p.minus;
                    break;
                default:
                    if (isFinite(key)) {
                        var a = p.activeTextValue;
                        var b = p.activeTextDecimal;
                        intval = parseInt(key);
                        if (!p.decimal) {
                            p.activeTextValue = (p.activeTextValue * 10) + intval;
                        } else {
                            p.activeTextDecimal = p.activeTextDecimal + intval.toString();
                        }
                        if (parseFloat((p.minus ? '-' : '') + p.activeTextValue + (p.activeTextDecimal != '' ? '.' + p.activeTextDecimal : '')) > maxValue) {
                            p.activeTextValue = a;
                            p.activeTextDecimal = b;
                        }
                        if (parseFloat((p.minus ? '-' : '') + p.activeTextValue + (p.activeTextDecimal != '' ? '.' + p.activeTextDecimal : '')) < minValue) {
                            p.activeTextValue = a;
                            p.activeTextDecimal = b;
                            p.minus = false;
                        }
                    }
                    break;
            }

            if (parseFloat((p.minus ? '-' : '') + p.activeTextValue + (p.activeTextDecimal != '' ? '.' + p.activeTextDecimal : '')) < minValue) {
                p.minus = false;
            }

            setTextValue(p.activeText);

        });

        $('.sketch-pad-cmd').bind('touchend mouseup', function (e) {
            e.preventDefault();
            var cmd = $(this).attr('cmd');
            if (p.editor) {
                p.editor.processCmd(cmd, distance(), angle());
            }
            p.reset();
        });


        $('.movable-title-bar').bind('touchstart mousedown', function (e) { ref = getCoords(); pos = $('.sketch-pad').position(); });
        $('.movable-title-bar').bind('touchmove mousemove', function (e) { e.preventDefault(); if (ref == null) return false; mov = getCoords(); var x = pos.left + (mov.x - ref.x); var y = pos.top + (mov.y - ref.y); $('.sketch-pad').css({ 'top': y + 'px', 'left': x + 'px' }); });
        $('.movable-title-bar').bind('touchend touchcancel mouseup mouseleave', function (e) { ref = null; pos = null; mov = null; });

        $('.hide-sketch-pad').bind(touchClickEvent, function (e) { e.preventDefault(); if (p.editor) p.editor.hideKeypad(); else $('.sketch-pad').hide(); });

        var getCoords = function () {
            var p = new PointX(0, 0);
            if (event.touches) {
                var t = event.touches[0];
                if (!t)
                    t = event.changedTouches[0];
                p = new PointX(t.clientX - t.target.offsetLeft, t.clientY - t.target.offsetTop);
            } else {
                p = new PointX(event.clientX - event.target.offsetLeft, event.clientY - event.target.offsetTop);
            }
            return p;
        }


    }


    initKeys();

    if (p.editor) {
        p.editor.attachKeypad(this);
    }
}

function SketchHistory() {
    /*
    1. Move segment
    2. Move Sketch
    3. Move Node
    4. Add Node
    5. Delete Segment
    6. FlipV
    7. FlipH
    8. Add Point
    9. Arc Function
    10. Delete Node
    11. Add Node Before
    */
}

function setCanvasEvents(ccse, editor) {

    var refPoint;

    var sbl = 30
    var sx1 = sbl, sx2 = ccse.width - sbl;
    var sy1 = sbl, sy2 = ccse.height - sbl;

    $(ccse).bind('touchstart mousedown', function () {
        sx2 = ccse.width - sbl;
        sy2 = ccse.height - sbl;

        event.preventDefault();
        var p = editor.getCoords();
        // Pen Down - for any action of touch
        editor.pen = true;
        var q = p.copy().alignToGrid(editor, Math.round);
        if (editor.currentVector && editor.mode == CAMACloud.Sketching.MODE_MOVE) {
            editor.currentVector.labelPosition = q.copy();
            editor.currentVector.isModified = true;
            editor.currentVector.labelEdited = true;
            editor.mode = CAMACloud.Sketching.MODE_EDIT
        }
        else if ((editor.currentSketchNote || editor.currentVector) && (editor.mode == CAMACloud.Sketching.MODE_EDIT)) {
            if (editor.currentVector) {
                editor.currentNode = null;
                refPoint = null;
                //                if (editor.currentVector.placeHolder) {
                //                    var rn = editor.currentVector.radiusNode;
                //                    if (rn.overlaps(q, 1)) {
                //                        editor.currentNode = rn;
                //                    }
                //                }
                var sn = editor.currentVector.startNode;
                var nearest = null;
                var nearestDistance = 9999;
                var i = 0;
                while (sn != null) {
                    var d = Math.sqrt(Math.pow(sn.p.x - q.x, 2) + Math.pow(sn.p.y - q.y, 2));
                    if (sn.overlaps(q)) {
                        editor.currentNode = sn;
                        break;
                    } else if (sn.overlaps(q, 1)) {
                        if (d < nearestDistance) {
                            nearestDistance = d;
                            nearest = sn;
                        }
                    }
                    sn = sn.nextNode;
                    i++;
                }
            }
            if (editor.currentNode) {
            } else if (!editor.currentNode && nearest) {
                editor.currentNode = nearest;
            } else if (editor.currentVector && !editor.currentNode && editor.currentVector.contains(q)) {
                refPoint = q;
            } else if (editor.currentSketch) {
                // NEW FUNCTIONALITY BEGIN
                var selectedNote = null;
                editor.currentSketch.notes.forEach(function (nt) {
                    if (!selectedNote && nt.contains(q)) {
                        editor.currentSketchNote = nt;
                        selectedNote = nt;
                        refPoint = q;
                    }
                });
                if (!selectedNote) {
                    editor.currentSketchNote = null;
                    editor.raiseModeChange();
                    //refPoint = p;
                }
                var selectedVector = null;
                editor.currentSketch.vectors.forEach(function (v) {
                    if (!selectedNote && !selectedVector && v.contains(q)) {
                        editor.currentVector = v;
                        $(editor.vectorSelector).val(v.uid);
                        selectedVector = v;

                        if (v.isClosed) {
                            refPoint = q;
                        } else {
                            editor.promptAndContinueVector(function () {
                                editor.render();
                            });
                        }
                    }
                });
                if (!selectedVector) {
                    $(editor.vectorSelector).val('');
                    editor.currentVector = null;
                    editor.raiseModeChange();
                    //  refPoint = p;
                }
                if (!selectedVector && !selectedNote) {
                    refPoint = p;
                    editor.mode = CAMACloud.Sketching.MODE_DEFAULT;
                }
                // NEW FUNCTIONALITY ENDS
            }
        }
        else if (editor.mode == CAMACloud.Sketching.MODE_DEFAULT) {
            if (editor.sketchMovable) {
                refPoint = q;
            } else {
                // NEW FUNCTIONALITY BEGIN
                if (editor.currentSketch) {
                    var selectedNote = null;
                    editor.currentSketch.notes.forEach(function (nt) {
                        if (!selectedNote && nt.contains(q)) {
                            editor.currentSketchNote = nt;
                            selectedNote = nt;
                            refPoint = q;
                            editor.mode = CAMACloud.Sketching.MODE_EDIT;
                        }
                    });
                    if (!selectedNote) {
                        refPoint = p;
                        editor.currentSketchNote = null;
                        editor.raiseModeChange();
                    }
                    if (editor.currentSketchNote)
                        return;
                    var selectedVector = null;
                    editor.currentSketch.vectors.forEach(function (v) {
                        if (!selectedNote && !selectedVector && v.contains(q)) {
                            editor.currentVector = v;
                            $(editor.vectorSelector).val(v.uid);
                            selectedVector = v;

                            if (v.isClosed) {
                                refPoint = q;
                                editor.render();
                                editor.mode = CAMACloud.Sketching.MODE_EDIT;
                                editor.raiseModeChange();
                            } else {
                                editor.promptAndContinueVector(function () {
                                    editor.render();
                                });
                            }
                        }
                    });
                    if (!selectedVector) {
                        refPoint = p;
                    }
                }

                // NEW FUNCTIONALITY ENDS
                //refPoint = p;
            }

        }
        else if (editor.currentSketchNote && (editor.mode == CAMACloud.Sketching.MODE_EDIT)) { }
    });

    if (windowsTouch == true && Hammer)
        enablePinch();
    $(ccse).bind('touchmove mousemove', function () {
        event.preventDefault();
        var p = editor.getCoords();

        if (editor.pen) {

            if (editor.mode != CAMACloud.Sketching.MODE_DEFAULT) {
                if (p.x < sx1) { editor.autoPan("R", false, p.x - sx1); } else { editor.autoPan("R", true); }
                if (p.x > sx2) { editor.autoPan("L", false, p.x - sx2); } else { editor.autoPan("L", true); }
                if (p.y < sy1) { editor.autoPan("D", false, p.y - sy1); } else { editor.autoPan("D", true); }
                if (p.y > sy2) { editor.autoPan("U", false, p.y - sy2); } else { editor.autoPan("U", true); }
            }
            if ((editor.mode == CAMACloud.Sketching.MODE_NEW)) {
              //  if (!editor.keypadActive) {
                    var q = p.copy().alignUnit(editor, Math.round);
                    editor.render(q, { moving: false, cursor: true });
            //    }
            }
            else if ((editor.currentNode || refPoint) && (editor.mode == CAMACloud.Sketching.MODE_EDIT)) {
                var moving = false;
                var showCursor = false;
                var q = p.copy().alignToGrid(editor, Math.round);
                if (editor.currentNode) {
                    var d = new PointX(editor.currentNode.p.x - q.x, editor.currentNode.p.y - q.y);
                    editor.currentNode.moveTo(q);
                    if (editor.currentVector && editor.currentVector.placeHolder) {
                        editor.currentVector.labelPosition.x -= d.x;
                        editor.currentVector.labelPosition.y -= d.y;
                    }
                    //                    if (editor.currentVector && editor.currentVector.placeHolder) {
                    //                        if (editor.currentVector.radiusNode == editor.currentNode) {
                    //                            editor.currentVector.placeHolderSize = sRound(editor.currentNode.p.getDistanceTo(editor.currentVector.startNode.p));
                    //                        }
                    //                    }
                    if (editor.currentNode.nextNode) {
                        if (editor.currentNode.nextNode.isEllipse) {
                            var t = editor.currentNode.nextNode.p;
                            var a = editor.currentNode.nextNode.nextNode.p;
                            var r = new PointX(t.x + q.x - a.x, t.y + q.y - a.y);
                            editor.currentNode.nextNode.nextNode.moveTo(q);
                            editor.currentNode.nextNode.moveTo(r);
                        }
                    }
                    //                    if (editor.currentNode == editor.currentVector.endNode) {
                    //                        if (!editor.currentNode.overlaps(editor.currentVector.startNode.p)) {
                    //                            editor.currentVector.terminateNode(editor.currentVector.startNode);
                    //                        }
                    //                    }
                    //                    if (editor.currentNode == editor.currentVector.startNode) {
                    //                        if (!editor.currentNode.overlaps(editor.currentVector.endNode.p)) {
                    //                            editor.currentVector.terminateNode(editor.currentVector.startNode);
                    //                        }
                    //                    }
                    showCursor = true;
                } else if (refPoint) {
                    //Move the entire segment when no point is selected.
                    var d = new PointX(refPoint.x - q.x, refPoint.y - q.y);
                    if (editor.currentVector) {
                        editor.currentVector.moveBy(d);
                    }
                    else if (editor.currentSketchNote) {
                        editor.currentSketchNote.moveBy(d);
                    }
                    refPoint = q;
                    moving = true;
                }
                editor.render(p, { moving: moving, cursor: showCursor });
                if (editor.currentVector) editor.currentVector.isModified = true;
            } else if (editor.mode == CAMACloud.Sketching.MODE_DEFAULT) {
                // Pan Canvas when no segment is selected.
                if (editor.sketchMovable) {
                    var q = p.copy().alignToGrid(editor, Math.round);       //Rounding of point to nearest foot when Touch/Click is used.
                    var d = new PointX(refPoint.x - q.x, refPoint.y - q.y);
                    editor.currentSketch.vectors.forEach(function (v) {
                        v.moveBy(d);
                    });

                    refPoint = q;
                } else {
                    if (refPoint) editor.pan(p.x - refPoint.x, refPoint.y - p.y);
                    refPoint = p;
                }

                editor.render(null, { moving: true });
            }
        }
    });

    $(ccse).bind('touchend mouseup', function () {
        var raiseModeChange = true;
        event.preventDefault();
        editor.clearAutoPanning();
        var p = editor.getCoords();
        // Pen Down - for any action of touch
        if (editor.pen) {
            if (editor.mode == CAMACloud.Sketching.MODE_NEW) {
              //  if (!editor.keypadActive) {
                    var q = p.copy().alignUnit(editor, Math.round);
                    editor.render(q, { moving: false, cursor: true });
                    if (editor.currentNode && editor.angleLock) {
                        var cp = q.copy().alignToGrid(editor, function (x) { return x; });
                        var np = editor.currentNode.p;
                        var angle = np.getDirectionInDegrees(cp); var d = "";
                        var length = np.getDistanceTo(cp);
                        length = Math.round(length)
                        editor.processCmd("+", null, null, true);
                        if (angle >= 45 && angle <= 135) editor.processCmd("D", length);
                        if (angle >= 135 && angle <= 225) editor.processCmd("L", length);
                        if (angle >= 225 && angle <= 315) editor.processCmd("U", length);
                        if (angle > 315 || angle <= 45) editor.processCmd("R", length);
                    }
                    else editor.registerNode(q);
                    if (editor.currentVector) editor.currentVector.isModified = true;
            //    }
            } else if (editor.mode == CAMACloud.Sketching.MODE_EDIT) {
                //editor.currentNode = null;
                refPoint = null;
                editor.render();

            } else if (editor.mode == CAMACloud.Sketching.MODE_DEFAULT) {
                if (editor.sketchMovable) {
                    raiseModeChange = false;
                } else {

                }
                refPoint = null;
                editor.render();
            }
        }
        if (raiseModeChange)
            editor.raiseModeChange();
        editor.pen = false;
    });

    $(ccse).bind('touchcancel mouseleave', function () {
        var p = editor.getCoords();
        event.preventDefault();
        // Pen Down - for any action of touch
        editor.pen = false;

    });

    $(ccse).bind('mousewheel', function (e, d) {
        var ev = e;
        if (e.originalEvent)
            ev = e.originalEvent;
        var delta = ev.wheelDeltaY;
        var dd = (delta < 0 ? -0.05 : delta > 0 ? 0.05 : 0);
        var tscale = isNaN(editor.scale) ? 1 : editor.scale;
        editor.scale = Math.round((tscale + dd) * 100) / 100;
        var px = new PointX(ev.offsetX, ev.offsetY)
        editor.zoom(editor.scale, px);
        var percent = Math.round((editor.scale - editor.minZoom) / editor.zoomRange * 100);
        editor.setZoom(percent);
    });

    var refScale = null;
    $(ccse).bind('gesturestart', function () {
        event.preventDefault();
        refScale = editor.scale;
    });

    $(ccse).bind('gestureend', function () {
        event.preventDefault();
        refScale = null;
    });

    $(ccse).bind('gesturechange', function () {
        try {
            event.preventDefault();
            editor.scale = editor.normalizeScale(Math.round((refScale * Math.round(Math.pow(event.scale, 2) * 100) / 100) * 100) / 100);
            editor.zoom(editor.scale, null);

            //Calculate Percentage for the slider control.
            var percent = Math.round((editor.scale - editor.minZoom) / editor.zoomRange * 100);
            editor.setZoom(percent);
        }
        catch (e) {
            editor.showStatus(e);
        }
    });

}
function enablePinch() {
    editor = sketchApp;
    var mc = new Hammer(document.getElementById('sketch-canvas'));
    mc.get('pinch').set({
        enable: true
    });
    var scale, zoom;
    mc.on('pinchstart', function (ev) {
        ev.preventDefault();
        refScale = editor.scale;
    });
    mc.on('pinch', function (ev) {
        if (refScale) {
            try {
                editor.scale = editor.normalizeScale(Math.round((refScale * Math.round(Math.pow(ev.scale, 2) * 100) / 100) * 100) / 100);
                editor.zoom(editor.scale, null);

                //var delta = (scale - ev.scale);
                //map.setZoom(Math.round(zoom - delta));

                //Calculate Percentage for the slider control.
                var percent = Math.round((editor.scale - editor.minZoom) / editor.zoomRange * 100);
                editor.setZoom(percent);
            }
            catch (e) {
                editor.showStatus(e);
            }
        }
    });
}

/*

Utility Functions

*/

CAMACloud.Sketching.Utilities = {}

splitString = function (string, splitters) {
    var list = [string];
    for (var i = 0, len = splitters.length; i < len; i++) {
        traverseList(list, splitters[i], 0);
    }
    return flatten(list);
}

traverseList = function (list, splitter, index) {
    if (list[index]) {
        if ((list.constructor !== String) && (list[index].constructor === String))
            (list[index] != list[index].split(splitter)) ? list[index] = list[index].split(splitter) : null;
        (list[index].constructor === Array) ? traverseList(list[index], splitter, 0) : null;
        (list.constructor === Array) ? traverseList(list, splitter, index + 1) : null;
    }
}

flatten = function (arr) {
    return arr.reduce(function (acc, val) {
        return acc.concat(val.constructor === Array ? flatten(val) : val);
    }, []);
}

function sRound(v) {
    if (CAMACloud.Sketching.roundFactor == null) {
        var dp = clientSettings['SketchDecimals'];
        if ((dp == null) || (dp === undefined)) {
            dp = 0;
        }
        if (isNaN(dp)) { dp = 0; } else if (dp > 3) { dp = 3; } else if (dp < 0) { dp = 0; }
        CAMACloud.Sketching.roundFactor = Math.pow(10, dp);
    }

    var rv = parseInt(v * CAMACloud.Sketching.roundFactor) / CAMACloud.Sketching.roundFactor;
    return rv;

    //return Math.round(v * CAMACloud.Sketching.roundFactor) / CAMACloud.Sketching.roundFactor;

}

function sign(x) {
    return x < 0 ? -1 : 1;
}

CAMACloud.Sketching.Utilities.toPixel = function (x) {
    return isNaN(x) ? 0 : Math.round(Number(x) * (DEFAULT_PPF * 10000)) / 10000;
}

CAMACloud.Sketching.Utilities.fromPixel = function (x) {
    return x / DEFAULT_PPF;
}