﻿var tileSize = 256;
var CENTER="";
var STYLE=google.maps.MapTypeControlStyle.HORIZONTAL_BAR;

var countyMapOptions = {
    getTileUrl: function (coord, zoom) {
        var x = coord.x;
        var y = coord.y;
        var lng = ((x * tileSize) - (tileSize * (Math.pow(2, zoom - 1)))) / ((tileSize * (Math.pow(2, zoom))) / 360);
        var ex = ((y * tileSize) - (tileSize * (Math.pow(2, zoom - 1)))) / ((-tileSize * Math.pow(2, zoom)) / (2 * Math.PI));
        var lat = ((2 * Math.atan(Math.exp(ex))) - (Math.PI / 2)) / (Math.PI / 180);
        if (lng > 180) lng -= 360;
        if (lng < -180) lng += 360;
        if (lat < -90) lat = -90;
        if (lat > 90) lat = 90;
        return "/mapsvc/tile/?lat=" + lat + "&lng=" + lng + '&zoom=' + zoom;
        //return "http://localhost:8808/?ll=" + lat + "," + lng + '&z=' + zoom;
    },
    tileSize: new google.maps.Size(tileSize, tileSize),
    maxZoom: 20,
    minZoom: 12,
    zoom: 19,
    name: "County Imagery"
};

var countyMapType = new google.maps.ImageMapType(countyMapOptions);

function loadSearchMap() {
	options = ["roadmap", "satellite","hybrid"];
	if(nearmapApiKey !=="" && nearmapAccess.includes('SV'))
		options.push("Nearmap");
	if (nearmapWMSApiKey !== "" && nearmapWMSAccess.includes('SV'))
		options.push("NearmapWMS");
	if(woolpertApikey  !== "" && woolpert3inchId  !== "" && woolpertAccess.includes('SV'))
		options.push("Woolpert3");
	if(woolpertApikey !== "" && woolpert6inchId !== "" && woolpertAccess.includes('SV') )
		options.push("Woolpert6");
	if(customWMS !=="" && customMapIsImage !=='1' && customWMSAccess.includes('SV'))
		options.push("customWMS");
	if(options.length > 5)
		STYLE=google.maps.MapTypeControlStyle.DROPDOWN_MENU;
	//setting a different default center if Woolpert imagery is available
    if(woolpertApikey  !== "" )
    	CENTER = new google.maps.LatLng(33.04297880884355, -97.02963228119779);
    else
    	CENTER = new google.maps.LatLng(41.68953 , -83.576147);
    
    var mapOptions = {
        zoom: 18,
        maxZoom: 20,
        minZoom: 6,
        center: CENTER,
        tilt: 0,
        streetViewControl: false,
        mapTypeControlOptions: {
        	style: STYLE,
	      mapTypeIds: options
	    }

    };
    var mc = document.getElementById('search-map');
    map = new google.maps.Map(mc, mapOptions);

    google.maps.event.addListener(map, 'bounds_changed', (function () {
    }));

    google.maps.event.addListener(map, 'idle', (function () {
    }));
    map.setMapTypeId('hybrid');
  
    // adding custom WMS to the map using ImageMapType function
     if(customWMS!=='' && customMapIsImage !=='1' && customWMSAccess.includes('SV')){
    	const cMap = new google.maps.ImageMapType({
		getTileUrl: function(coord, zoom) {
		return  customWMS + zoom + '/' + coord.y + '/' + coord.x ;
		},
		tileSize: new google.maps.Size(256, 256),
		maxZoom: 19,
		minZoom: 6,
		
		name: 'CustomMap'
		});
		map.mapTypes.set("customWMS", cMap);
		}
    //adding woolpert 6 inch imagery to the map using ImageMapType function
    if(woolpertApikey !==""  && woolpert6inchId!==""){
    	const woolpertMapType6 = new google.maps.ImageMapType({
		getTileUrl: function(coord, zoom) {
		return 'https://raster.stream.woolpert.io/layers/' + woolpert6inchId +'/tiles/'
			     + zoom + '/' + coord.x + '/' + coord.y + '?key='+ woolpertApikey;
		},
		tileSize: new google.maps.Size(256, 256),
		maxZoom: 20,
		minZoom: 6,
		
		name: 'Woolpert6"'
		});
		map.mapTypes.set("Woolpert6", woolpertMapType6);
		//map.setMapTypeId("Woolpert6");
    
    }
    //console.log(' arrived key'+woolpertApikey);
    //adding woolpert 3 inch imagery to the map using ImageMapType function
    if(woolpertApikey !==""  && woolpert3inchId!=="" ){
    	const woolpertMapType3 = new google.maps.ImageMapType({
		getTileUrl: function(coord, zoom) {
		return 'https://raster.stream.woolpert.io/layers/' + woolpert3inchId +'/tiles/'
			     + zoom + '/' + coord.x + '/' + coord.y + '?key='+ woolpertApikey;
		},
		tileSize: new google.maps.Size(256, 256),
		maxZoom: 20,
		minZoom: 6,
		
		name: 'Woolpert3"'
		});
		map.mapTypes.set("Woolpert3", woolpertMapType3);
		//map.setMapTypeId("Woolpert3");
    }
 
    if(nearmapApiKey !=="" && nearmapAccess.includes('SV')){
	    const NearmapMapType = new google.maps.ImageMapType({
		getTileUrl: function(coord, zoom) {
			const normalizedCoord = getNormalizedCoord(coord, zoom);
			if (!normalizedCoord) {
			return "";
			}
		
		const bound = Math.pow(2, zoom);
		return "https://api.nearmap.com/tiles/v3/Vert" +
	    "/" +
	    zoom +
	    "/" +
	    normalizedCoord.x +
	    "/" +
	    (normalizedCoord.y) +
	    ".img?apikey="+nearmapApiKey+"&tertiary=satellite"
		},
		tileSize: new google.maps.Size(256, 256),
		maxZoom: 20,
		minZoom: 6,
		radius: 1738000,
		name: "Nearmap"
		});
		function getNormalizedCoord(coord, zoom) {
	        const y = coord.y;
	        let x = coord.x; // tile range in one direction range is dependent on zoom level
	        // 0 = 1 tile, 1 = 2 tiles, 2 = 4 tiles, 3 = 8 tiles, etc
	
	        const tileRange = 1 << zoom; // don't repeat across y-axis (vertically)
	
	        if (y < 0 || y >= tileRange) {
	          return null;
	        } // repeat across x-axis
	
	        if (x < 0 || x >= tileRange) {
	          x = ((x % tileRange) + tileRange) % tileRange;
	        }
	
	        return {
	          x: x,
	          y: y
	        };
	        }
		map.mapTypes.set("Nearmap", NearmapMapType);
		map.setMapTypeId("Nearmap");
	}

	if (nearmapWMSApiKey !== "" && nearmapWMSAccess.includes('SV')) {
		const NearmapWMSMapType = new google.maps.ImageMapType({
			getTileUrl: function (coord, zoom) {
				const normalizedCoord = getNormalizedCoord(coord, zoom);
				if (!normalizedCoord) {
					return "";
				}

				const bound = Math.pow(2, zoom);
				return "https://api.nearmap.com/tiles/v3/Vert" +
					"/" +
					zoom +
					"/" +
					normalizedCoord.x +
					"/" +
					(normalizedCoord.y) +
					".img?apikey=" + nearmapWMSApiKey + "&tertiary=satellite"
			},
			tileSize: new google.maps.Size(256, 256),
			maxZoom: 20,
			minZoom: 6,
			radius: 1738000,
			name: "Nearmap"
		});
		function getNormalizedCoord(coord, zoom) {
			const y = coord.y;
			let x = coord.x; // tile range in one direction range is dependent on zoom level
			// 0 = 1 tile, 1 = 2 tiles, 2 = 4 tiles, 3 = 8 tiles, etc

			const tileRange = 1 << zoom; // don't repeat across y-axis (vertically)

			if (y < 0 || y >= tileRange) {
				return null;
			} // repeat across x-axis

			if (x < 0 || x >= tileRange) {
				x = ((x % tileRange) + tileRange) % tileRange;
			}

			return {
				x: x,
				y: y
			};
		}
		map.mapTypes.set("NearmapWMS", NearmapWMSMapType);
		map.setMapTypeId("NearmapWMS");
	}
}

