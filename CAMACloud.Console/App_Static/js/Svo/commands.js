﻿$(function () {
    var divcounter = 1;
    $('#SrchId').attr('disabled', 'disabled');
    $('.Operator').prop('disabled', 'true');
    $('.Operator').val('select');
    $('#section1').hide();
    $('#divSearchTop1').hide();
    $('#section2').show();
    $('.addclrfltr').show();
    $('.ddlUsersSV').off('change');
    $('#SrchId').show();
    $('#SrchId2').hide();
    $('#closefilter').on('click', function () {

        $('#section1').hide();
        $('#divSearchTop1').show();
        $('#section2').show();
        $('.addclrfltr').show();
    });

    $('#toggleSwitch').unbind('change');
    $('#toggleSwitch').bind('change', () => {
        toggleSwitch = true;
    });

    var val = $('.ddlUsersSV').val().split('/');
    $('.addfilter, .addfltr').on('click', function () {
        $('#SrchId2').hide();
        $('#SrchId').show();
        $('#SrchId').css('background-color', '#d6d6d6');
        $('#SrchId').attr('disabled', 'disabled');
        $('.Operator').prop('disabled', 'true');
        $('.Operator').val('select');
        openfltrwindow();

    });

    $('#addcstmfltr').on('click', function () {
        svPromptAction(() => {
            showMask();
            setTimeout(function () { appendFilters(); }, 100);
        }, (!activeParcel ? true : false));
    });

    $('.clearfilter').on('click', function () {
        //$('#TabSearchContents tbody tr').remove();
        //$('#lblCntParcl').html("0");
        $('#divSearchTop1').empty();
        $('.addfilter').show();
        $('.clearfilter')[0].text = ' Clear Filter';
        divcounter = 1;
    });

    $('.ddlUsersSV').on('change', function () {
        if ($(this).val() != "") {
            $('#SrchId').prop('type', 'text');
            $('#SrchId').css('width', '90%');
            var val = $('.ddlUsersSV').val().split('/');
            if (val[3] && datafields[val[3]] == undefined)
                location.reload();
            if (val[3] && datafields[val[3]] != undefined) {//to check if table id exixts
                if (val[2] == '') val[2] = 'VARCHAR';
                val[2] = val[2].toUpperCase();
                if (datafields[val[3]].LookupTable) {//if lookup table exists
                    $('#SrchId').hide();
                    $('#SrchId2').show();
                    $('.Operator').val(' = ');
                    $('.Operator').prop('disabled', 'disabled');
                    copylookupfilter('#SrchId2', val[3]);
                    $('#SrchId2').show();
                    $('#SrchId2').addClass('#SrchId2');
                }
                if (val[2] && datafields[val[3]].LookupTable == null) {
                    $('#SrchId').show();
                    $('#SrchId2').hide();
                    val[2] = val[2].toUpperCase();
                    if (val[2] == "INT" || val[2].contains('NUMERIC') || val[2].contains('FLOAT')) {
                        $('.Operator').removeAttr('disabled');
                        enableOperators([" = ", " LIKE ", " <> ", "startwith", "notstrtwith", "endwith", " IS NOT NULL ", " IS NULL ", " > ", " >= ", " <= ", " < "]);
                        disableOperators([" LIKE "]);
                        $('.Operator').val(' = ');
                    }
                    if (val[2].contains('DATE') && datafields[val[3]].LookupTable == null) {
                        $('#SrchId').show();
                        $('#SrchId2').hide();
                        enableOperators([" = "]);
                        disableOperators([" > ", " >= ", " <= ", " < ", " LIKE ", " <> ", "startwith", "endwith", "notstrtwith", " IS NOT NULL ", " IS NULL "]);
                        $('.Operator').val(' = ');
                    }
                    else if (val[2].contains('VARCHAR') && datafields[val[3]].LookupTable == null) {
                        $('.Operator').removeAttr('disabled');
                        enableOperators([" = ", " LIKE ", " <> ", "startwith", "endwith", " IS NOT NULL ", " IS NULL ", "notstrtwith"]);
                        disableOperators([" > ", " >= ", " <= ", " < "]);
                        $('.Operator').val(' LIKE ');
                    }
                }

            }
            else if (val[0] == 0 || val[0] == 1 || val[0] == 2 || val[0] == 6) {
                if ($('.ddlUsersSV').val() == "1") {
                    $('#SrchId').show();
                    $('#SrchId2').hide();
                    enableOperators([" = ", " LIKE ", " <> ", "startwith", "notstrtwith", "endwith", " IS NOT NULL ", " IS NULL "]);
                    disableOperators([" > ", " >= ", " <= ", " < "]);
                    $('.Operator').val(' = ');
                }

                if ($('.ddlUsersSV').val() == "0" || $('.ddlUsersSV').val() == "2") {
                    $('#SrchId').show();
                    $('#SrchId2').hide();
                    $('.Operator').removeAttr('disabled');
                    enableOperators([" = ", " LIKE ", " <> ", "startwith", "notstrtwith", "endwith", " IS NOT NULL ", " IS NULL "]);
                    disableOperators([" > ", " >= ", " <= ", " < "]);
                    $('.Operator').val(' LIKE ');
                }

                if ($('.ddlUsersSV').val() == "6") {
                    $('#SrchId').hide();
                    $('#SrchId2').show();
                    enableOperators([" = ", " IS NOT NULL ", " IS NULL "]);
                    disableOperators(["select", " > ", " >= ", " <= ", " < ", " LIKE ", " <> ", "startwith", "endwith", "notstrtwith"]);
                    let $dropdown = $('#SrchId2');
                    $dropdown.empty();
                    [{ Id: 1, Name: 'Yes' }].forEach((xk) => {
                        let option = $('<option>', {
                            value: xk.Id,
                            text: xk.Name
                        });
                        $dropdown.append(option);
                    });
                    $('.Operator').val(' = ');
                }
            }
            else if (val[2]) {
                if (val[2] == null) val[2] = 'VARCHAR';
                val[2] = val[2].toUpperCase();
                if (val[0].contains('aggfield') && val[2]) { //for aggregate fields
                    var aggvalidate = $('#MainContent_ddlUsersSV option:selected').text();
                    aggvalidate = aggvalidate.split('_');
                    if (aggvalidate[0] && aggvalidate[0] != 'FIRST' && aggvalidate[0] != 'LAST' && aggvalidate[0] != 'COUNT') {
                        $('#SrchId').prop('type', 'number');
                        $('#SrchId').css('width', '85%');
                    }
                    $('#SrchId').show();
                    $('#SrchId2').hide();
                    val[2] = val[2].toUpperCase();
                    if (val[2] == "INT" || val[2].contains('NUMERIC') || val[2].contains('FLOAT')) {
                        $('.Operator').removeAttr('disabled');
                        enableOperators([" = ", " LIKE ", " <> ", "startwith", "notstrtwith", "endwith", " IS NOT NULL ", " IS NULL ", " > ", " >= ", " <= ", " < "]);
                        disableOperators([" LIKE "]);
                        $('.Operator').val(' = ');
                    }
                    if (val[2].contains('DATE')) {
                        $('#SrchId').show();
                        $('#SrchId2').hide();
                        enableOperators([" = "]);
                        disableOperators([" > ", " >= ", " <= ", " < ", " LIKE ", " <> ", "startwith", "endwith", "notstrtwith", " IS NOT NULL ", " IS NULL "]);
                        $('.Operator').val(' = ');
                    }
                    else if (val[2].contains('VARCHAR')) {
                        $('.Operator').removeAttr('disabled');
                        enableOperators([" = ", " LIKE ", " <> ", "startwith", "endwith", " IS NOT NULL ", " IS NULL ", "notstrtwith"]);
                        disableOperators([" > ", " >= ", " <= ", " < "]);
                        $('.Operator').val(' LIKE ');
                    }
                }

            }

            else {
                $('.Operator').prop('disabled', 'true');
            }

            //$('.Operator').removeAttr('disabled');
            $('#SrchId').css('background-color', '#ffffff');
            $('#SrchId').removeAttr('disabled');
            var Optns = $(this).val().split('/');
            var fieldDataTypes = Optns[1];
            $('#SrchId').val('');
            if (fieldDataTypes == 4 && $('#SrchId').prop('type') == 'text') {
                $('#SrchId').prop('type', 'date');
            }
            else if (fieldDataTypes != 4 && $('#SrchId').prop('type') == 'date') {
                $('#SrchId').prop('type', 'text');
            }
            if ($('.ddlUsersSV').val() == "3") {
                $('#SrchId').hide();
                $('#SrchId2').show();
                enableOperators([" = "]);
                disableOperators(["select", " > ", " >= ", " <= ", " < ", " LIKE ", " <> ", "startwith", "endwith", "notstrtwith", " IS NOT NULL ", " IS NULL "]);
                $('.Operator').val(' = ');
                if (val[0] == "3") {
                    var DT = DTR_Tasks
                    var $dropdown = $('#SrchId2');
                    $dropdown.empty();
                    DT.forEach(function (Tasks) {
                        var option = $('<option>', {
                            value: Tasks.Name,
                            text: Tasks.Name
                        });
                        $dropdown.append(option);
                    });
                }
            }
            if ($('.ddlUsersSV').val() == "4") {
                $('#SrchId').hide();
                $('#SrchId2').show();
                enableOperators([" = ", " LIKE ", " <> ", "startwith", "endwith", "notstrtwith", " IS NOT NULL ", " IS NULL "]);
                disableOperators(["select"," > ", " >= ", " <= ", " < "]);
                $('.Operator').val(' = ');
                if (val[0] == "4") {
                    var dst = DTR_StatusType
                    var $dropdown = $('#SrchId2');
                    $dropdown.empty();
                    dst.forEach(function (status) {
                        var option = $('<option>', {
                            value: status.Name,
                            text: status.Description
                        });
                        $dropdown.append(option);
                    });
                }
            }
            $('.Operator').removeAttr('disabled');
        } else {
            if ($('#SrchId').prop('type') == 'date') {
                $('#SrchId').prop('type', 'text');
            }
            $('#SrchId').val('');
            $('#SrchId').show();
            $('#SrchId2').hide();
            $('#SrchId').css('background-color', '#d6d6d6');
            $('#SrchId').attr('disabled', 'disabled');
            $('.Operator').prop('disabled', 'true');
            $('.Operator').val('select');

        }
    });

    function populateDropdown(elementId, dataTable) {
        var dropdown = $(elementId);
        dropdown.empty();
        for (var i = 0; i < dataTable.rows.length; i++) {
            var row = dataTable.rows[i];
            dropdown.append($('<option></option>').val(row[0]).html(row[1]));
        }
    }

    function openfltrwindow() {
        $('#MainContent_ddlUsersSV').val("selected");
        $('#SrchId2').hide();
        $('#SrchId').show();
        $('#SrchId').prop('type', 'text');
        $('#SrchId').css('background-color', '#d6d6d6');
        $('#SrchId').attr('disabled', 'disabled');
        $('#SrchId').val('');
        $('.Operator').prop('disabled', 'true');
        $('.Operator').prop('disabled', 'true');
        $('.Operator').val('select');
        $('#section1').show();
        $('#section2').hide();
        $('.addclrfltr').hide();
        $('#divSearchTop1').hide();
    }
    function copylookupfilter(ddl, fieldId) {//copy lookup to each variables
        var val = $('.ddlUsersSV').val().split('/');
        $(ddl).html('');
        if (val[0] == '3') {
            var dtr_tk = DTR_Tasks
            for (o in dtr_tk) {
                var text = dtr_tk[o].Name;
                var value = dtr_tk[o].Name;
                if (text == '' && value == '0') text = '0';
                addToDDL(ddl, text, value);
            }
        }
        else if (val[0] == '4') {
            var dtr_st = DTR_StatusType
            for (o in dtr_st) {
                var text = dtr_st[o].Description;
                var value = dtr_st[o].Name;
                if (text == '' && value == '0') text = '0';
                addToDDL(ddl, text, value);
            }
        }
        else if (val[0] == '6') {
            [{ Id: 1, Name: 'Yes' }].forEach((xk) => {
                let value = xk.Id;
                let text = xk.Name;
                if (text == '' && value == '0') text = '0';
                addToDDL(ddl, text, value);
            });
        }
        else if (datafields[val[3]].LookupTable.toString() == "$QUERY$") {
                var t = val[0];
                var opt = fieldId ? lookup['#FIELD-' + fieldId] : lookup[t];
                opt = opt ? opt : {};
                var sortedopt = Object.keys(opt).sort(function (a, b) { return opt[a].Ordinal - opt[b].Ordinal })
                for (o in sortedopt) {
                    var text = opt[sortedopt[o]].Name;
                    var value = opt[sortedopt[o]].Id;
                    if (text == ' ' && value == '0') text = '0';
                    var fieldId = opt[sortedopt[o]].fieldId;
                    addToDDL(ddl, text, value, fieldId);
                }
        }
        else {
            var lookupName = datafields[val[3]].LookupTable.toString()
            var field = datafields && datafields[val[3]];
            if (field && field.InputType == 5 && lookup && lookup[lookupName]) {
                for (var keys in lookup[lookupName]) {
                    var lkdata = lookup[lookupName][keys];
                    if (lkdata) {
                        var id = lkdata['Id'], name = lkdata['Name'];
                        if (id == "True" || id == "TRUE" || id == "False" || id == "FALSE")
                            id = id.toLowerCase();
                        $(ddl).append($('<option/>', { value: id, text: name }));
                    }
                }
            }
            else {
                $(ddl).html($('[lookuptable="' + lookupName + '"]').html());
                $(ddl).html($('[lookuptable="' + lookupName + '"]').html());
            }
            $('option[value=""]', ddl).remove();
            $(ddl).prepend($('<option/>', { value: -1, text: 'Is NULL' }));
            $(ddl).prepend($('<option/>', { value: -2, text: 'Is not NULL' }));
        }
    }
    function addToDDL(ddl, text, value, fieldId) { //append each lookup to dropdown
        var opt = document.createElement('option');
        $(opt).attr('value', value);
        opt.innerHTML = text
        $(opt).attr('fieldId', fieldId);
        $(ddl).append(opt);
    }
    function enableOperators(ops) {//enable operators
        if (!ops) {
            $('.Operator').attr('disabled', 'disabled');
        } else if (ops.length) {
            $('.Operator').removeAttr('disabled');
            $('.Operator option').show();
            for (x in ops) {
                var op = ops[x];
                //$('.create-filter .operator option[value="' + op + '"]').attr('disabled', 'disabled');
                $('.Operator option[value="' + op + '"]').show();
            }
        }
    }

    function disableOperators(ops) { //disable operators
        if (!ops) {
            $('.Operator').attr('disabled', 'disabled');
        } else if (ops.length) {
            $('.Operator').removeAttr('disabled');
            $('.Operator option').show();
            for (x in ops) {
                var op = ops[x];
                //$('.create-filter .operator option[value="' + op + '"]').attr('disabled', 'disabled');
                $('.Operator option[value="' + op + '"]').hide();
            }
        }
    }
    function showMask(info, error, errorActionName, callback) {
        $('.masklayer').show();
        if (!info)
            info = 'Please wait ...'
        var helplink = ''
        if (callback && errorActionName)
            helplink = '<a href="#" class="app-mask-error-help-link">Click here<a/> to ' + errorActionName
        if (error)
            info = '<span style="color:Red;">' + info + '</span> ' + helplink
        $('.app-mask-layer-info').html(info);
    }

    function hideMask() {
        setTimeout(function () { $('.masklayer').hide(); }, 1);
    }

    function validateData(optns, sval, qcontn1) {
        var fieldName = "";
        var fieldValue = "";
        if (optns == undefined) optns = "0";
        if (optns == "0" || optns == "1" || optns == "2")
            var Optn = optns;
        else
            var Optn = optns.split('/'); // To retrieve datatype of field along with fieldname.
        var srchOptn = Optn[0].trim();
        var fieldDataType = Optn[1];
        var schemaType = Optn[2];
        var cntnOprtr = qcontn1;

        if (schemaType) schemaType = schemaType.toUpperCase();



        if (fieldDataType != undefined) {
            var numericlength = Optn[2].split('(')[1];
        }

        if (srchOptn) {
            fieldName = srchOptn;
            fieldValue = sval;
            if (fieldValue == undefined) fieldValue = "0";
            var val = optns.split('/');
            if (val[3]) {
                if (datafields[val[3]].LookupTable)
                    fieldValue = sval;
            }
            var str = fieldValue;
        }

        if (schemaType != undefined) {
            if (schemaType.contains('INT') && isNaN(fieldValue)) {
                clearAlert();
                return false;
            }
            else if (schemaType.contains('INT') && (fieldValue.includes('.'))) {
                clearAlert();
                return false;
            }
        }

        if ((fieldName == 0 || fieldName == 1 || fieldName == 2) && (fieldValue.includes('%'))) {
            alert('Invalid Special character')
            clearSearch()
            closeParcel();
            $('.masklayer').hide();
            return false;
        }

        if (fieldDataType == 1 || fieldDataType == 2 || fieldDataType == 3 || fieldDataType == 5 || fieldDataType == 6 || fieldDataType == 7 || fieldDataType == 8 || fieldDataType == 9 || fieldDataType == 10 || fieldDataType == 11 || fieldDataType == 12 || fieldDataType == 13) {
            var pattern = new RegExp(QC.dataTypes[fieldDataType].pattern);
            if (cntnOprtr == ' IS NULL ' || cntnOprtr == ' IS NOT NULL ')
                fieldValue = '1';
            var validate1 = (pattern.test(fieldValue));
            if (!validate1 && cntnOprtr != ' IS NULL ' && cntnOprtr != ' IS NOT NULL ') {
                clearAlert();
                return false;
            }
        }

        if (fieldDataType != undefined) {
            if (schemaType.contains('numeric')) {
                var pattern = /^([a-zA-Z]*$)|[ !@#$%^&*()_+\-=\[\]{};':"\\|,<>\/?]/;
                var validate = (pattern.test(fieldValue));
                if (validate || str.length > numericlength) {
                    clearAlert();
                    $('.masklayer').hide();
                    return false;
                }
            } else if (schemaType.contains('BIT')) {
                if (!(fieldValue == 0 || fieldValue == 1)) {
                    clearAlert();
                    $('.masklayer').hide();
                    return false;
                }

            }


        }

        var flag = $('.SrchFlag').val();
        var user = $(".ddlUsers").val();

        if (fieldDataType == 4 && schemaType.contains('DATE')) {
            //var regex = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[13-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;
            //var regex1 = /^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$/;
            //var regex = new RegExp("^((0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2})*$");
            var regex = (schemaType == 'DATETIME') ? /((\b1[8-9]\d{2}|\b2\d{3})[-/.](0[1-9]|1[0-2])[-/.](0[1-9]|[12]\d|3[01]))/ : /(\b[12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/;
            var validdate = (regex.test(fieldValue));
            //var leapyrvalidate = (regex1.test(fieldValue));
            if (!validdate) {
                alert("Please enter a valid Date Format")
                clearSearch()
                closeParcel();
                $('.masklayer').hide();
                return false;
            }
        }

        if (!(user != "" || flag != "" || fieldName != "") || (fieldName != "" && fieldValue == "" && cntnOprtr != " IS NOT NULL " && cntnOprtr != " IS NULL ")) {
            var alrtMsg = 'You need to provide the Search options';
            if (fieldName != "" && fieldValue == "") {
                alrtMsg = 'You need to provide either the ID or Value to search.';
                if (fieldName == 1) alrtMsg = 'You need to provide the Assignment Group to search.';
                else if (fieldName == 2) alrtMsg = 'You need to provide the CC Tag to search.';
                clearSearch();
                closeParcel();
                $('.masklayer').hide();
                $('#divRghtCntnr').hide('fast');
                $('#divFootCntnr').hide();
            }
            alert(alrtMsg);
            $('.masklayer').hide();
            window.location.hash = "";
            return false;
        }
    }
    function appendFilters() {
        if (($('.ddlUsersSV').val()) == "") {
            alert("You have to enter a search condition");
            hideMask();
            return
        }
        showMask();
        var val = $('.ddlUsersSV').val().split('/');
        var remindex = $('.addfltr').last();
        $(remindex).hide();
        var reqfield = $('#MainContent_ddlUsersSV option:selected').text();
        var filtercondition = $('#MainContent_Operator option:selected').text();
        if (filtercondition == '--Select--') {
            alert('You have to select a valid Filter condition');
            hideMask();
            return
        }
        var qfield = $('#MainContent_ddlUsersSV').val();
        var qcontn = $('#MainContent_Operator').val();
        $("<div />", { "class": "filterdiv", "id": "filterdiv" + divcounter })
            .append($("<h2 />", { "class": "filtercntnt", "id": "filtercntnt" + divcounter, "qfield": qfield, "qcontn": qcontn }).html(reqfield + " " + filtercondition))
            .appendTo("#divSearchTop1");
        if (($('#SrchId').is(':visible'))) {
            $('#filterdiv' + divcounter)
                .append($("<input />", { "id": "fltrinpsrch" + divcounter, "type": "text", "autocorrect": "off", "autocapitalise": "off", "maxlength": "50", "class": "fltrinpsrch1" }))
                .appendTo("#divSearchTop1");
            $('#fltrinpsrch' + divcounter).val($('#SrchId').val());
            if (qcontn == ' IS NOT NULL ' || qcontn == ' IS NULL ') {
                $('#fltrinpsrch' + divcounter).attr('disabled', 'disabled');
                $('#fltrinpsrch' + divcounter).css('background-color', '#d6d6d6');
            }
            if (val[2] != undefined && val[2].contains('DATE') && datafields[val[3]].LookupTable == null) {
                $('#fltrinpsrch' + divcounter).prop('type', 'date');
            }
            if (val[0].contains('aggfield')) {
                var aggvalidate = $('#MainContent_ddlUsersSV option:selected').text();
                aggvalidate = aggvalidate.split('_');
                if (aggvalidate[0] && aggvalidate[0] != 'FIRST' && aggvalidate[0] != 'LAST' && aggvalidate[0] != 'COUNT') {
                    $('#fltrinpsrch' + divcounter).prop('type', 'number');

                }
            }
        }
        if (($('#SrchId2').is(':visible'))) {
            $('#filterdiv' + divcounter)
                .append($("<select />", { "id": "fltrselinp" + divcounter, "class": "fltrinpsrch1" }))
                .appendTo("#divSearchTop1");
            copylookupfilter("#fltrselinp" + divcounter, val[3]);
            $("#fltrselinp" + divcounter).val($('#SrchId2').val());
        }
        $('#filterdiv' + divcounter)
            .append($("<span />", { "class": "deletefltr", "id": "deletefltr" + divcounter }).html("-")
                .click(function () {
                    if (($('.filterdiv').length) == 1) {
                        $('.addfilter').show();
                        //$('#TabSearchContents tbody tr').remove();
                        //$('#lblCntParcl').html("0");
                    }
                    if (($('.filterdiv').length) == 2) {
                        $('.clearfilter')[0].text = ' Clear Filter'
                    }
                    jQuery(this).closest("div").remove()
                    var remindex1 = $('.addfltr').last();
                    $('.addfilter').show();
                    $(remindex1).show();
                })
            )
            .append($("<span />", { "class": "addfltr", "id": "addfltr" + divcounter }).html("+")
                .click(function () {
                    openfltrwindow();
                })
            )
            .appendTo("#divSearchTop1");
        divcounter++;
        if (($('.filterdiv').length) == 5) {
            $('.addfltr').hide();
            $('.addfilter').hide();
        }
        if (($('.filterdiv').length) > 1) {
            $('.clearfilter')[0].text = ' Clear Filters';
        }

        generatefetchquery();

        $('#section1').hide();
        $('#section2').show();
        $('.addclrfltr').show();
        $('#divSearchTop1').show();
        hideMask();
    }
    function generatefetchquery() {
        var fquery = "";
        var flimit = $('.filterdiv').length;
        for (i = 0; i < flimit; i++) {
            var hcntntel = $('#divSearchTop1 .filterdiv .filtercntnt')[i];
            var qvalsel = $('#divSearchTop1 .filterdiv .fltrinpsrch1')[i];
            var qval1 = $(qvalsel).val();
            if (qval1.contains(''))
                qval1 = qval1.replace(/'/g, "''");
            var qfield1 = $(hcntntel).attr('qfield').split('/');
            var qcontn1 = $(hcntntel).attr('qcontn');
            var fieldvalidation1 = validateData($(hcntntel).attr('qfield'), qval1, qcontn1);
            if (fieldvalidation1 != undefined && fieldvalidation1 == false)
                return;
            qfield1 = qfield1[0];



            if (qcontn1 == "notstrtwith") {
                //qval1=qval1+"%";
                qcontn1 = " NOT LIKE ";
            }

            fquery = fquery + '^^^' + qfield1 + "|" + qcontn1 + "|" + qval1 + '___@@@\n';
        }
        var optns = $(hcntntel).attr('qfield');
        if (flimit == 0) optns = '';
        var sval = $(qvalsel).val();
        if (qcontn1 == ' IS NOT NULL ' || qcontn1 == ' IS NULL ') {
            sval = "1";
        }
        doSearch(fquery, optns, sval);
    }

    $('.Operator').off('change');
    $('.Operator').on('change', function () {
        if ($('.Operator').val() == " IS NOT NULL " || $('.Operator').val() == " IS NULL " || $('.Operator').val() == "select") {
            $('#SrchId').val('');
            $('#SrchId').attr('disabled', 'disabled');
            $('#SrchId').css('background-color', '#d6d6d6');
        }
        else {
            $('#SrchId').removeAttr('disabled');
            $('#SrchId').css('background-color', '#ffffff');
        }
        if ($('.ddlUsersSV').val() == "4") {
            if ($('.Operator').val() == "select") {
                $('#SrchId').show();
                $('#SrchId2').hide();
                $('#SrchId').attr('disabled', 'disabled');
            }
            else if ($('.Operator').val() == " = " || $('.Operator').val() == " <> ") {
                $('#SrchId').hide();
                $('#SrchId2').show();
                var dst = DTR_StatusType
                var $dropdown = $('#SrchId2');
                $dropdown.empty();
                dst.forEach(function (status) {
                    var option = $('<option>', {
                        value: status.Name,
                        text: status.Description
                    });
                    $dropdown.append(option);
                });
            }
            else {
                $('#SrchId').show();
                $('#SrchId2').hide();
                $('#SrchId').val('');
            }
        }
        else if ($('.ddlUsersSV').val() == "6") {
            if ($('.Operator').val() == " IS NOT NULL " || $('.Operator').val() == " IS NULL ") {
                $('#SrchId').show();
                $('#SrchId2').hide();
            }
            else {
                $('#SrchId').hide();
                $('#SrchId2').show();
                let $dropdown = $('#SrchId2');
                $dropdown.empty();
                [{ Id: 1, Name: 'Yes' }].forEach((xk) => {
                    let option = $('<option>', {
                        value: xk.Id,
                        text: xk.Name
                    });
                    $dropdown.append(option);
                });
            }
        }
    });

    $('#btnSearch').click(function () {
        svPromptAction(() => {
            if (mapWindow) mapWindow.close();
            showMask();
            setTimeout(function () { generatefetchquery(); }, 100);
            searchstrng = '';
        }, (!activeParcel ? true : false));
        return false;
    });

    $("#SrchId").keypress(function (e) {
        if (e.which == 13) {
            if (mapWindow) mapWindow.close();
            $(":focus").blur();
            showMask();
            setTimeout(function () { appendFilters(); }, 100);
            return false;
        }

    });

    $("#SrchOptn").keydown(function (e) {
        if (e.which == 13) {
            return false;
        }
    });

    $('#btnClear').click(function () {
        svPromptAction(() => {
            var b = $(window).width() - $('#divSearchCntnr').width();
            $('#search-map').width(b);
            $('.src-field').val('');
            if (mapWindow) mapWindow.close();
            clearSearch();
            window.location.hash = "";
        }, (!activeParcel ? true : false));
        return false;
    });

    $('#btnRstRvw').click(function () {
        $("#divMainMenuDrop").hide('fast')
        if (activeParcel != null && ($('.review-note').val() != '' || $('#rblStatus input:checked').prop('checked') == true || $('#toggleSwitch')[0].checked || $('.flag-fields input:checked').prop('checked') == true)) {
            $('.review-note').val('');
            $('#rblStatus input:checked').prop('checked', false);
            $('#toggleSwitch')[0].checked = false;
            $('.flag-fields input:checked').prop('checked', false);
            var colorsel = '#divSq-' + activeParcel.Id;
            $(colorsel).css('background-color', '#FF0000');
            saveReview("Reset Done Sucessfully.", 0);
            //notification("Reset Done Sucessfully");
            return false;
            //openParcel(activeParcel.Id);
        }
        return false;
    });

    $('#btnRefresh').click(function () {
        svPromptAction(() => {
            if (activeParcel != null) {
                showMask();
                openParcel(activeParcel.Id);
            }
        }, (!activeParcel ? true : false));
        return false;
    });

    $('#btnSaveRvw').click(function () {
        _saveSVReview();
        return false;
    });
    $('#btnSaveNext').click(function () {
        _saveSVReview(true);
        return false;
    });
    $('#btnKML, #btnSHP').click(function () {
        var shpCall = this.id == 'btnSHP' ? true : false;
        var flag;
        //$("#divMainMenuDrop").hide('fast');
        $('.flag-fields :checkbox').each(function (i, e) {
            if ((e.checked == true && activeParcel.Flags[i].Value == true) || (e.checked == false && activeParcel.Flags[i].Value == false)) {
                flag = 0;
            }
            else {
                flag = 1;
                return false;
            }
        });
        if (mapFrame && mapFrame.contentWindow && !(activeParcel.SketchFlag != $('#rblStatus input:checked').prop('value') || flag == 1 || activeParcel.SketchReviewNote != ($('.review-note').val() == '' ? null : $('.review-note').val()) || activeParcel.SketchRotate != $('.sketch-rotate').val() || document.getElementById('mapFrame').contentWindow.isSketchChanged)) {
            mapFrame.contentWindow._mso(function (_polyArray) {
                if (_polyArray) {
                    var data = { polyArray: (_polyArray.polyPoints).toString(), selectedSketch: _polyArray.selectedSketch, KeyValue1: activeParcel.KeyValue1, StreetAddress: activeParcel.StreetAddress };
                    var path = shpCall ? '/sv/exportshp.jrq' : '/sv/exportkml.jrq';
                    $.ajax({
                        url: path,
                        type: "POST",
                        data: data,
                        dataType: 'text',
                        success: function (resp) {
                            if (resp) {
                                var a = document.createElement('a');
                                if (shpCall) {
                                    var url = 'data:application/zip;base64,' + resp;
                                    a.href = url;
                                    a.download = activeParcel.KeyValue1 + '.zip';
                                } else {
                                    var blob = new Blob([resp], { type: "text/xml;charset=utf-8" });
                                    var url = URL.createObjectURL(blob);
                                    a.href = url;
                                    a.download = activeParcel.KeyValue1 + '.kml';
                                }

                                if (mapFrame.contentWindow.previewSketch) {
                                    mapFrame.contentWindow.previewSketch('white', 1, function (retURL) {
                                        document.body.append(a);
                                        a.click();
                                        a.remove();
                                        if (!shpCall)
                                            window.URL.revokeObjectURL(url);
                                    });
                                }
                                else {
                                    document.body.append(a);
                                    a.click();
                                    a.remove();
                                    if (!shpCall)
                                        window.URL.revokeObjectURL(url);
                                }
                            }
                        },
                        error: function (resp) {
                            alert("Download faild...");
                        }
                    });
                }
                else
                    alert("There is no Map Points available");
            });
        }
        else {
            alert("There are unsaved changes to the sketch.Please save sketch changes");
        }
        return false;
    });

});

$(document).ready(function () {
    $('.hideSidePanel').click(function () {
        hideLeftPanel();
    });

    if (window) {
        window.onbeforeunload = function () {
            isBeforeUnload = true;
            if (typeof (logoutClicked) !== 'undefined' && logoutClicked)
                localStorage.setItem('sanbornDisabled', 0);
            else if (typeof (sanbornWindow) !== 'undefined' && sanbornWindow) {
                if (sanbornWindow.closed)
                    localStorage.setItem('sanbornDisabled', "1");
                else
                    localStorage.setItem('sanbornDisabled', 0);
            }

            if (typeof (activeParcel) != 'undefined' && activeParcel) {
                let valid = _svModified();
                if (valid) return "Caution, there are unsaved changes to the property!."
            }
        }
    }
    function hideLeftPanel() {
        var w = $(window).width() - 5;
        var p = $('#divRghtCntnr').is(':visible');
        if (p) {
            var p1 = $(window).width() - $('#divSearchCntnr').width();
            $('.resultframe').width(p1 + 12);
            $('#mapFrame').width(p1 + 9);
        }
        else {
            $('#search-map').width(w);
        }
        $('#divSearchCntnr').hide();
        $('.showSidePanel').show();
    };

    $('.showSidePanel').click(function () {
        $('#divSearchCntnr').show();
        var fht = $(window).height(); var ht = $('#divtopHeader').height() + $('#divBottomFooter').height() + $('#divSearchTop').height();
        $('#divSearchBtm').height(fht - ht - 20);
        $('.showSidePanel').hide();
        var p = $('#divRghtCntnr').is(':visible');
        if (p) {

            var p2 = $(window).width() - ($('#divSearchCntnr').width() + $('#divRghtCntnr').width());
            $('.resultframe').width(p2 + 3);
            $('#mapFrame').width(p2);
        }
        else {
            var b = $(window).width() - $('#divSearchCntnr').width();
            $('#search-map').width(b);
        }
    });

    $('#TabSearchContentsHd .hdspn').on("click", (e) => {
        //console.log(e.currentTarget.innerHTML)
        if (parcels) {
            if (Object.keys(parcels).length == 0) return false;
            var Header = e.currentTarget.innerHTML;
            SortSearchResults(Header);
        }
    });

});

function _saveSVReview(_nextParcel, cback) {
    let flag, rotFlag, currSketRot, sketchoverlay, qcPassed = $('#toggleSwitch')[0].checked ? '1' : '';
    var skvalue = $('#rblStatus input:checked').val();

    if (skvalue != undefined) {
        var statusflg = activeParcel.sketchstatusflags.filter(function (flg) { return (flg.Id) == skvalue; })[0];
        var colorsel = '#divSq-' + activeParcel.Id;
        $(colorsel).css('background-color', statusflg.ColorCode);
        if ((_nextParcel || cback) && parcels && statusflg.ColorCode) {
            let pl = parcels.filter((x) => { return x.ID == activeParcel.Id })[0];
            if (pl) pl.ColorCode = statusflg.ColorCode;
        }
    }
    if (activeParcel.SketchData) sketchoverlay = $.parseJSON(activeParcel.SketchData);
    if (sketchoverlay && top.$('.sketch-select').val() && top.$('.section-select').val()) {
        let seletedSketch = top.$('.sketch-select').val();
        let skRowuid = top.$('.sketch-select option[value="' + seletedSketch + '"]').attr('skrowuid');
        let skPrefix = top.$('.sketch-select option[value="' + seletedSketch + '"]').attr('skprefix');
        skPrefix = mapFrame.contentWindow.isNewSketchData && skPrefix ? skPrefix : '';
        seletedSketch = mapFrame.contentWindow.isNewSketchData ? (skRowuid + skPrefix) : seletedSketch;
        currSketRot = (sketchoverlay["sketch-" + seletedSketch] && sketchoverlay["sketch-" + seletedSketch][top.$('.section-select').val()]) ? sketchoverlay["sketch-" + seletedSketch][top.$('.section-select').val()].sketchRotation : null;
    }
    rotFlag = currSketRot ? currSketRot : activeParcel.SketchRotate;
    $("#divMainMenuDrop").hide('fast');
    $('.flag-fields :checkbox').each(function (i, e) {
        if ((e.checked == true && activeParcel.Flags[i].Value == true) || (e.checked == false && activeParcel.Flags[i].Value == false)) {
            flag = 0;
        }
        else {
            flag = 1;
            return false;
        }
    });

    if (testMode || activeParcel.SketchFlag != $('#rblStatus input:checked').prop('value') || (activeParcel.qcPassed != qcPassed) || flag == 1 || activeParcel.SketchReviewNote != ($('.review-note').val() == '' ? null : $('.review-note').val()) || rotFlag != $('.sketch-rotate').val() || document.getElementById('mapFrame').contentWindow.isSketchChanged) {
        // if(activeParcel.SketchData && !activeParcel.sketchLatLng && !mapFrame.contentWindow.isSketchChanged) return false;  SV_294, FLAG CHANGED THEN SAVE NOT WORK BEACUSE OF  NOT SKETCHLNG.         
        _checkReviewdSketchs(function () {
            $('#sketchid').val($('.sketch-select').val());
            saveReview("Details Saved Sucessfully.", 1, null, _nextParcel, cback);
            //notification("Details Saved Sucessfully","Sketch Validation & Review");
            document.getElementById('mapFrame').contentWindow.isSketchChanged = false;
        }, (_nextParcel ? false : true));
    }
}

$(document).keypress(function (e) {
    if ($('.masklayer').is(':visible')) { return false; } //if save review clicked disable keys
    if (e.which == 46 && !($("input,textarea").is(":focus") && $(document.activeElement).attr('type') != "radio")) moveNxt();
    else if (e.which == 44 && !($("input,textarea").is(":focus") && $(document.activeElement).attr('type') != "radio")) movePrev();
});

function saveReview(msg, title, mapData, _nextParcel, cback) {
    var data;
    showMask('Saving changes..')
    function saveReviewNew(_plyArray) {
        var _polygonArray = '';
        if (_plyArray) {
            var pla = { polygonArray: _plyArray, parcelId: activeParcel.Id };
            _polygonArray = JSON.stringify(pla)
        }
        if (mapData) {
            data = mapData;
            data.polygonArray = _polygonArray;
        }
        else {
            var newCurrentOverlayPosition = mapFrame.contentWindow.isNewSketchData ? mapFrame.contentWindow.currentOverlayPosition : {};
            if (!mapFrame.contentWindow.isNewSketchData) {
                $('.sketch-select option').each(function (index, opt) {
                    var optval = (opt.value || opt.value == 0) ? opt.value.toString() : opt.value;
                    var skRowId = $(opt).attr('skrowuid');
                    var skPrefix = $(opt).attr('skprefix');
                    skPrefix = skPrefix ? skPrefix : '';
                    if (mapFrame.contentWindow.currentOverlayPosition["sketch-" + optval]) {
                        newCurrentOverlayPosition["sketch-" + skRowId + skPrefix] = mapFrame.contentWindow.currentOverlayPosition["sketch-" + optval];
                    }
                });
            }

            data = {
                Description: title,
                pid: activeParcel.Id,
                notes: $('#txtRevNote').val(),
                flag: '',
                qcflag:'',
                szoom: mapFrame.contentWindow.scale,
                mzoom: mapFrame.contentWindow.mapZoom,
                rotate: mapFrame.contentWindow.degree,
                salat: mapFrame.contentWindow.getParcelCenter().lat(),
                salng: mapFrame.contentWindow.getParcelCenter().lng(),
                status: '',
                sketchData: JSON.stringify(newCurrentOverlayPosition),
                sketchLatLng: mapFrame.contentWindow.sketchLatLng,
                polygonArray: _polygonArray
            };

        }

        if (!activeParcel.sketchLatLng && !(mapFrame.contentWindow.isSketchChanged || data.IsSketchChanged)) {
            data.sketchLatLng = "";
        }

        $('.flag-fields :checkbox').each(function () {
            if (this.checked) {
                if (data['flag'] != '') {
                    data['flag'] += ',';
                }
                data['flag'] += $(this).attr('flag');
            }
        });

        var fstat = $('#rblStatus input:checked').val();
        if ((fstat == undefined) || (fstat == null)) {
            fstat = '';
        }

        data['status'] = fstat; data['qcflag'] = $('#toggleSwitch')[0].checked ? '1' : '';

        var color = $('#rblStatus input:checked:before').css('background');

        $.ajax({
            url: '/sv/savereview.jrq',
            type: "POST",
            data: data,
            dataType: 'json',
            success: function (resp) {
                if (activeParcel != null) {
                    if (cback) {
                        hideMask();
                        cback(); return;
                    }
                    else if ((lightWeight && (autoSaveEnabled || autoForward)) || _nextParcel) {
                        hideMask();
                        moveNxt(_nextParcel, true);
                    } else {
                        openParcel(activeParcel.Id, 'refresh', function () {
                            hideMask();
                            //notification(msg,title)
                        });
                    }

                }
            },
            error: function (resp) { }
        });

    }
    if (mapFrame && mapFrame.contentWindow && mapFrame.contentWindow._mso) {
        mapFrame.contentWindow._mso(function (_polyArray) {
            saveReviewNew(_polyArray);
        });
    }
    else
        saveReviewNew();
}

function enableQuickMode() {
    testMode = true;
    lightWeight = true;
    autoForward = true;
}

function SortSearchResults(Header, firstSort) {
    var FnString = "parcels.sort(function(a, b) {var reA = /[^a-zA-Z]/g;var reN = /[^0-9]/g;"
    var descString = "var aA = nameA.replace(reA, '');var bA = nameB.replace(reA, '');if (aA === bA) {var aN = parseInt(nameA.replace(reN, ''), 10);var bN = parseInt(nameB.replace(reN, ''), 10); aN =isNaN(aN)?0:aN; bN=isNaN(bN)?0:bN; return aN === bN ? 0 : aN < bN ? 1 : -1;} else {return aA < bA ? 1 : -1;}})"
    var ascString = "var aA = nameA.replace(reA, '');var bA = nameB.replace(reA, '');if (aA === bA) {var aN = parseInt(nameA.replace(reN, ''), 10);var bN = parseInt(nameB.replace(reN, ''), 10); aN =isNaN(aN)?0:aN; bN=isNaN(bN)?0:bN; return aN === bN ? 0 : aN > bN ? 1 : -1;} else {return aA > bA ? 1 : -1;}});"
    if (firstSort) ascParcel = false
    if (Header == "Parcel Id") {
        FnString = FnString + "var nameA = a.KeyValue1.toUpperCase(); var nameB = b.KeyValue1.toUpperCase();"
        if (ascParcel) FnString = FnString + descString //sorts descending order
        else FnString = FnString + ascString;
        ascParcel = !ascParcel;
    }
    else if (Header == "Address") {
        FnString = FnString + "var nameA = a.StreetAddress?a.StreetAddress.toUpperCase():''; var nameB = b.StreetAddress?b.StreetAddress.toUpperCase():'';"
        if (ascAddress) FnString = FnString + descString //sorts descending order
        else FnString = FnString + ascString;
        ascAddress = !ascAddress;
    }
    var svSort = new Function(FnString)();
    if (firstSort) return
    var curActive = activeParcel ? activeParcel.Id : null;
    showpagination();
    //var htmlCntnts = "";
    //   if (parcels.length > 0) {
    //      $('#lblCntParcl').html(parcels.length);
    //      for (x in parcels) {
    //          var ParcelAddr = "";
    //          if (parcels[x].StreetAddress != null)
    //              ParcelAddr = parcels[x].StreetAddress;
    //          if (parcels[x].KeyValue1 == '0' || parcels[x].ClientRowuid)
    //              parcels[x].KeyValue1 = parcels[x].ClientRowuid;
    //          var ParcelId = parcels[x].KeyValue1;
    //          var ColorCode = parcels[x].ColorCode;
    //          htmlCntnts += "<tr id=\"tr-" + parcels[x].ID + "\">";
    //          htmlCntnts += "<td width=\"10%\" ><span id=\"divSq-" + parcels[x].ID + "\" class=\"divSquare\" style=\"opacity: 0.7;background-color:" + ColorCode + ";\"></span></td>";
    //          htmlCntnts += "<td width=\"45%\"><span onclick=\"if(mapWindow)mapWindow.close();openParcel(" + parcels[x].ID +" , null,  null, true )\" class=\"PrclId\">" + ParcelId + " </span></td>";
    //          htmlCntnts += "<td width=\"45%\"><span onclick=\"if(mapWindow)mapWindow.close();openParcel(" + parcels[x].ID +" , null, null, true )\" class=\"PrclAdr\">" + ParcelAddr + "</span></td>";
    //          htmlCntnts += "</tr>";
    //      }
    //  } 
    //  $('#TabSearchContents tbody tr').remove();
    //  $('#TabSearchContents tbody').append(htmlCntnts);
    // if (activeParcel && $('#tr-' + curActive).length) {
    //    $('#TabSearchContents tbody tr').css("background-color", "");
    //    $('#tr-' + curActive).css("background-color", "rgb(150, 211, 220)");
    //    var showSaveNextButton = true, showPrevButton = true;
    // var curIndex = $("#TabSearchContents tr[id=tr-" + curActive + "]").index();
    //   var totParcel = $("#TabSearchContents tr").length;
    //    if (curIndex + 1 == totParcel || totParcel == 1)
    //			showSaveNextButton = false;
    // if( curIndex <= 0 )
    //showPrevButton = false;
    //	   showSaveNextButton ? ( $('#btnSaveNext, #btnNext').prop('disabled', false).css('background','linear-gradient(#065a9f,#019ade)').css('pointer-events','all') ) : ( $('#btnSaveNext, #btnNext').css('background','gray').css('pointer-events','none') );
    //	   showPrevButton ? ( $('#btnPrev').prop('disabled', false).css('background','linear-gradient(#065a9f,#019ade)').css('pointer-events','all') ) : ( $('#btnPrev').css('background','gray').css('pointer-events','none') );
    // }
    //showpagination();
}

function svPromptAction(callback, ignoreSprompt, pageLoad) {
    if ((ignoreSprompt && callback) || (!activeParcel && callback)) {
        callback();
        return;
    }

    let valid = _svModified();

    if (valid) {
        $('.sv-prompt').show();
        $('input[name="svprompt"]')[0].checked = true;
        $('.sv-prompt-ok').off('click');
        $('.sv-prompt-ok').on('click', () => {
            $('.sv-prompt').hide();
            if ($('input[name="svprompt"]:checked').val() == 'save') {
                _saveSVReview(false, () => {
                    setTimeout(() => {
                        if (callback) callback();
                    }, 100)
                });
            }
            else if (callback) callback();
        });
    }
    else if (callback) callback();
}

function svPromptClose() {
    $('.sv-prompt').hide();
    return false;
}

function _svModified() {
    let flag = 0, currSketRot, sketchoverlay, valid = false, qcPassed = $('#toggleSwitch')[0]?.checked ? '1' : '';

    if (activeParcel.SketchData) sketchoverlay = $.parseJSON(activeParcel.SketchData);
    if (sketchoverlay && top.$('.sketch-select').val() && top.$('.section-select').val()) {
        let seletedSketch = top.$('.sketch-select').val();
        let skRowuid = top.$('.sketch-select option[value="' + seletedSketch + '"]').attr('skrowuid');
        let skPrefix = top.$('.sketch-select option[value="' + seletedSketch + '"]').attr('skprefix');
        skPrefix = mapFrame.contentWindow.isNewSketchData && skPrefix ? skPrefix : '';
        seletedSketch = mapFrame.contentWindow.isNewSketchData ? (skRowuid + skPrefix) : seletedSketch;
        currSketRot = (sketchoverlay["sketch-" + seletedSketch] && sketchoverlay["sketch-" + seletedSketch][top.$('.section-select').val()]) ? sketchoverlay["sketch-" + seletedSketch][top.$('.section-select').val()].sketchRotation : null;
    }
    rotFlag = currSketRot ? currSketRot : activeParcel.SketchRotate;

    $('.flag-fields :checkbox').each((i, e) => {
        if ((e.checked == true && activeParcel.Flags[i].Value == true) || (e.checked == false && activeParcel.Flags[i].Value == false)) { flag = 0; }
        else { flag = 1; return false; }
    });

    if (testMode || activeParcel.SketchFlag != $('#rblStatus input:checked').prop('value') || flag == 1 || (qcPassed != activeParcel.qcPassed) || activeParcel.SketchReviewNote != ($('.review-note').val() == '' ? null : $('.review-note').val()) || rotFlag != $('.sketch-rotate').val() || document.getElementById('mapFrame').contentWindow.isSketchChanged) {
        valid = true;
    }

    return valid;
}
