﻿var activeParcel;
var mapWindow;
var globalTableList = [];
var parentChild = {};
var activeParcelId = 99988;
var lookup = {}
var openFromPRC = false;
var clientSettings = {}, sketchSettings = {};
var fieldCategories = [];
var TableHierarchical = [];
var datafields = {};
var MVP_Lookup = [];
var MVP_Outbuliding_Lookup = [];
var MVP_Default_Exterior_Cover = [];
var MVP_sketch_codes = [];
var MVP_sketch_labels = [];
var Proval_Lookup = [];
var Proval_Default_Exterior_Cover = [];
var Proval_Outbuliding_Lookup = [];
var Proval_sketch_codes = [];
var FTC_Detail_Lookup = [];
var FTC_AddOns_Lookup = [];
var FTC_Floor_Lookup = [];
var Lafayette_Lookup = [];
var Patriot_Lookup = [];
var Farragut_Lookup = [];
var SigmaCuyahoga_Lookup = [];
var WrightAddn_Lookup = [];
var WrightCom_Lookup = [];
var WrightComType_Lookup = [];
var datafieldsettings = [];
var childRecCalcPattern = /\\.|(sum|avg)|\(\)/i;
var sketchDropDownCount = 0; //total sketch dropdown count
var sketchDropDownChange = []; // update when a sketch drop down change occur to identify user viewed all sketches before save.
var prevEagle, prevCyclo, prevNearmap, prevNearmapWMS, prevQpublic, prevCustomWMS, prevWoolpert;
var polyOptions = {
    strokeOpacity: 0.8,
    strokeWeight: 2,
    fillOpacity: 0.1
}
var autoSaveCount = 0;
var nearmappopup;
var nearmapWMSpopup;					
var customWMSpopup;
var woolpertpopup;

var parcelPolygon = new google.maps.Polygon(polyOptions);
var tableListGlobal = [];
var _dB;
var extendedLookup = [];
var LookupMap = {};
var tableKeys = [];
var DTR_StatusType = [];
var DTR_Tasks = [];

        Object.defineProperties(window, {
            lightWeight: {
                get: function () { return localStorage.getItem('sv-lightweight') == "1"; },
                set: function (v) { localStorage.setItem('sv-lightweight', v ? "1" : "0"); }
            },
            autoForward: {
                get: function () { return localStorage.getItem('sv-autoforward') == "1"; },
                set: function (v) { localStorage.setItem('sv-autoforward', v ? "1" : "0"); }
            },
            autoSaveEnabled: {
                get: function () { return localStorage.getItem('sv-autosave') == "1"; },
                set: function (v) { localStorage.setItem('sv-autosave', v ? "1" : "0"); }
            },
            autoSaveBatch: {
                get: function () { var v = localStorage.getItem('sv-autosavebatch'); return v ? parseInt(v) : 20; },
                set: function (v) { if (isNaN(parseInt(v))) v = 20; if (v <= 0) v = 20; if (v > 100) v = 100; localStorage.setItem('sv-autosavebatch', parseInt(v)); autoSaveCount = 0; }
            },
            testMode: {
                get: function () { return localStorage.getItem('sv-testmode') == "1"; },
                set: function (v) { localStorage.setItem('sv-testmode', v ? "1" : "0"); }
            },
            svOrgnaizationId: {
                get: function () { return localStorage.getItem('sv-orgnaizationId') },
                set: function (v) { localStorage.setItem('sv-orgnaizationId', v); }
            }
        })



function Parcel(p) {
    var points = [];
    var bounds = new google.maps.LatLngBounds();

    this.ID = 0;
    this.KeyValue1 = "";
    this.KeyValue2 = "";
    this.KeyValue3 = "";
    this.StreetAddress = "";
    this.SketchUrl = "";
    this.Latitude = 0.0;
    this.Longitude = 0.0;
    this.ReviewedBy = "";
    this.ReviewedDate = "";
    this.ColorCode = "";
    this.OutBuildings = [];
    this.Original = {};
    this.CopyFrom = function (obj) {
        for (key in obj) {
            eval('this["' + key + '"] = obj["' + key + '"];');
            eval('this.Original["' + key + '"] = obj["' + key + '"];');
            //eval('this.Original.' + key + ' = obj.' + key + ";")
        }
    };

    this.Points = function () { return points; }
    this.Bounds = function () { return bounds; }
    this.Center = function () { return bounds.getCenter(); }

    this.ShowOnMap = function (locate) {
        if (locate == null)
            locate = true;
        if (map) {
            var polyOptions = {
                path: this.Points(),
                strokeColor: this.ColorCode,
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: this.ColorCode,
                fillOpacity: 0.1,
                visible: true,
                clickable: false
            }
            if (parcelPolygon == null) {
                //parcelPolygon = new google.maps.Polygon(polyOptions);
            }
            else {
                if (parcelPolygon.setOptions)
                    parcelPolygon.setOptions(polyOptions);
            }
            parcelPolygon.setMap(map);
            map.setMapTypeId('CountyImagery');
            if (locate == true)
                this.LocateOnMap();
        }
    }

    this.LocateOnMap = function () {
        map.setCenter(this.Center());
        map.fitBounds(this.Bounds());
    }

    this.CopyFrom(p);
    for (x in p._mapPoints) {
        var pt = p._mapPoints[x];
        latlng = new google.maps.LatLng(pt.Latitude, pt.Longitude);
        points.push(latlng);
        bounds.extend(latlng);
    }
}
    $.extend(Parcel.prototype, ccma.Data.Evaluable);
//$.extend( newParcel.prototype, ccma.Data.Evaluable );
function applyParcelChanges(callback) {
    for (x in activeParcel.ParcelChanges) {
        var a = activeParcel.ParcelChanges[x];
        var oldValue = a.OriginalValue;
        var value = a.NewValue ? a.NewValue : null;
        if (a.Action != 'delete') {
            if ((a.AuxROWUID == null || (activeParcel[a.FieldName] !== undefined && activeParcel['ROWUID'] == a.AuxROWUID)) && a.FieldName) {
                activeParcel[a.FieldName] = value;
                activeParcel.Original[a.FieldName] = oldValue;
            } else {
                var source = a.SourceTable;
                for (ex in activeParcel[source]) {
                    var acx = activeParcel[source][ex];
                    if (acx.ROWUID == a.AuxROWUID && a.FieldName) {
                        activeParcel[source][ex][a.FieldName] = value;
                        activeParcel.Original[source].filter(function (f) { return f.ROWUID == a.AuxROWUID })[0][a.FieldName] = oldValue;
                    }
                }
            }
        }
    };
    if (callback) callback();
}
function urlParcelHash(parcelId) {
    window.location.hash = '/parcel/' + parcelId;
}
function getParcelViaHash() {
    var hashString = window.location.hash;
    
    if( localStorage.getItem('sv-orgnaizationId') == _organizationId ) {
		if( localStorage.getItem('sv-lightweight') == "1" )
			$('.switch-input')[0].checked = true;
		else
		    $('.switch-input')[0].checked = false;
	}
	else {
		localStorage.setItem('sv-orgnaizationId', _organizationId);
		localStorage.setItem('sv-lightweight','0');
	}
	
    if (hashString.trim() == "")
        return;
    var method = hashString.split('/')[1];
    var pId = hashString.split('/')[2];
    if (method == "parcel") {
        loadEssentialTables(function () {
            openParcel(pId, null, null, true);
        });
    }
}

function openParcel(pid, type, callback, _clearSketchDropDown, loadFromGrid) {
    //for exiting full screen window in any browser
    let ignoreSprompt = (!loadFromGrid || !activeParcel) ? true : false;
    svPromptAction(() => {
        showMask();
        if (_clearSketchDropDown) {
            sketchDropDownCount = 0;
            sketchDropDownChange = [];
        }

        var showSaveNextButton = true, showPrevButton = true;
        mapFrame.contentWindow.isNewSketchData = false;
        if (pid) {

            var pageSelector = parseInt($('#pageSelector').val(), 10);
            var recordsPerPage = parseInt($('#recordsPerPageSelector').val(), 10);

            var curIndex = $("#TabSearchContents tr[id=tr-" + pid + "]").index();
            var totParcel = $("#TabSearchContents tr").length;
            if (parcels) totParcel = parcels.length - ((pageSelector * recordsPerPage) - recordsPerPage);
            if (curIndex + 1 == totParcel || totParcel == 1)
                showSaveNextButton = false;
            if (curIndex <= 0 && pageSelector == 1)
                showPrevButton = false;
        }
        showSaveNextButton ? ($('#btnSaveNext, #btnNext').prop('disabled', false).css('background', 'linear-gradient(#065a9f,#019ade)').css('pointer-events', 'all')) : ($('#btnSaveNext, #btnNext').css('background', 'gray').css('pointer-events', 'none'));
        showPrevButton ? ($('#btnPrev').prop('disabled', false).css('background', 'linear-gradient(#065a9f,#019ade)').css('pointer-events', 'all')) : ($('#btnPrev').css('background', 'gray').css('pointer-events', 'none'));

        lightWeight ? $('#onprc').hide() : $('#onprc').show();

        if (!document.fullscreenElement &&
            !document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement) {
        }
        else {
            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.msExitFullscreen) {
                document.msExitFullscreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            }
        }
        $('#sketchid').val("0");

        if ($('#tr-' + pid).length) {
            $('#TabSearchContents tbody tr').css("background-color", "");
            $('#tr-' + pid).css("background-color", "rgb(150, 211, 220)");
        }

        if ($('#divSearchCntnr').is(':hidden')) {
            var b = $(window).width() - $('#divSearchCntnr').width();
            $('.resultframe').width(b + 12);
            $('#mapFrame').width(b + 9);
        }
        else {
            var p2 = $(window).width() - ($('#divSearchCntnr').width() + $('#divRghtCntnr').width());
            $('.resultframe').width(p2);
            $('#mapFrame').width(p2);
        }
        $.ajax({
            url: '/sv/getparcelfromid.jrq',
            data: { pid: pid, quick: lightWeight ? "1" : "0" },
            dataType: 'json',
            success: function (resp) {
                activeParcel = new Parcel(resp);
                urlParcelHash(activeParcel.Id)
                $('.resultframe').show();
                $('.tip').tipr();
                activeParcel.Original = JSON.parse(JSON.stringify(activeParcel));
                if (activeParcel.FirstPhoto == "")
                    activeParcel.FirstPhoto = "/App_Static/images/residential.png";
                if (activeParcel.KeyValue1 == '0')
                    activeParcel.KeyValue1 = activeParcel.ClientROWUID;
                applyParcelChanges(function () {
                    var tableList = globalTableList.slice();
                    LoadAuxDataPRC(activeParcel, tableList, function () {
                        loadAuxData(activeParcel, tableList, function (parcel) {
                            activeParcel = parcel;
                            if (activeParcel.NewSketchData)
                                mapFrame.contentWindow.isNewSketchData = true;
                            else
                                mapFrame.contentWindow.isNewSketchData = false;
                            sortBySortExpression();
                            loadPRCactiveParcel(TableHierarchical, activeParcel, function () {
                                if (mapFrame.contentWindow.openParcel) {
                                    mapFrame.contentWindow.openParcel(function () {
                                        mapFrame.contentWindow.map && mapFrame.contentWindow.map.getStreetView().setVisible(false);
                                        var z = mapFrame.contentWindow.scale;
                                        var d = mapFrame.contentWindow.degree;
                                        if (z) $('.sketch-resize').val(z);
                                        if (d) $('.sketch-zoom-value').html(parseInt(parseFloat(z).toFixed(2) * 100) + '%');
                                    }, _clearSketchDropDown);
                                }
                                if (mapFrame.contentWindow.setPositionOverlayIfDataExist)
                                    window.setTimeout('mapFrame.contentWindow.setPositionOverlayIfDataExist();', 100);
                                $('.parcel-header-data').html($('.parcel-header-template').html());
                                $('.parcel-header-data').fillTemplate([activeParcel]);
                                $('.review-note').val(resp.SketchReviewNote ? resp.SketchReviewNote.replace(/&lt;/g, '<').replace(/&gt;/g, '>') : null);
                                if (resp.MapZoom != 0) {
                                    if (mapFrame.contentWindow.map) {
                                        var copyOverlay = mapFrame.contentWindow.currentOverlayPosition;
                                        var selsketch = mapFrame.contentWindow.selectedSketch;
                                        var selsect = mapFrame.contentWindow.selectedSection;
                                        var skPrefix = '', mZoom = resp.MapZoom;
                                        if (mapFrame.contentWindow.isNewSketchData) {
                                            var skRowuid = top.$('.sketch-select option[value="' + selsketch + '"]').attr('skrowuid');
                                            var skPrefix = top.$('.sketch-select option[value="' + selsketch + '"]').attr('skprefix');
                                            skPrefix = skPrefix ? skPrefix : ''; selsketch = skRowuid;
                                        }
                                        if (copyOverlay && selsketch && selsect && copyOverlay["sketch-" + selsketch + skPrefix][selsect] && copyOverlay["sketch-" + selsketch + skPrefix][selsect].mapZoom)
                                            mZoom = copyOverlay["sketch-" + selsketch + skPrefix][selsect].mapZoom;
                                        mapFrame.contentWindow.map.setZoom(mZoom);
                                        map.setZoom(mZoom);
                                        mapFrame.contentWindow.mapZoom = mZoom;
                                    }
                                }
                                mapFrame.contentWindow.firstTimeLoad = false;
                                showResultsOnMap(true);
                                $('.searchframe').hide();
                                $('#divRghtCntnr').show('fast');
                                var $scroll = $('#divRghtCntnr').children().first();
                                if ($scroll) $scroll.scrollLeft(0);
                                $('#divFootCntnr').show();

                                $('#rblStatus input').each(function () {
                                    this.checked = false;
                                });
                                var rbl = $('#rblStatus input[value="' + resp.SketchFlag + '"]');
                                if (rbl.length > 0) {
                                    rbl[0].checked = true;
                                }
                                $('.otherflags .flag-fields :checkbox').iphoneStyle({ checkedLabel: 'YES', uncheckedLabel: 'NO' });
                                for (x in resp.Flags) {
                                    var id = resp.Flags[x].Id;
                                    $('.flag-fields input[flag="' + id + '"]').attr('checked', resp.Flags[x].Value);
                                    $('.flag-fields input[flag="' + id + '"]').iphoneStyle('refresh');
                                }

                                if (resp.qcPassed == 'True') {
                                    activeParcel.qcPassed = '1';
                                    $('#toggleSwitch')[0].checked = true;
                                }
                                else
                                    $('#toggleSwitch')[0].checked = false;

                                if (activeParcel.OutBuildings.length == 0) {
                                    $('.data-view-switch').hide();
                                    $('.view-sketch').show();
                                    $('.view-oby').hide();
                                } else {
                                    $('.data-view-switch').show();
                                    $('.view-sketch').show();
                                    $('.view-oby').hide();
                                    $('.a-view-sketch').hide();
                                    $('.a-view-oby').show();
                                }

                                if (clientSettings.ShowKeyValue1 == "0" && clientSettings.ShowAlternateField == "1") {
                                    if (activeParcel.alternateFieldvalue != "")
                                        $('#lblPrclId').html(activeParcel.alternateFieldvalue);
                                }
                                else if (clientSettings.ShowKeyValue1 == "1" && clientSettings.ShowAlternateField == "1") {
                                    var val = $('#MainContent_ddlUsersSV').val();
                                    if (val.contains(activeParcel.alternateField))
                                        if (activeParcel.alternateFieldvalue != "")
                                            $('#lblPrclId').html(activeParcel.alternateFieldvalue);
                                }

                                if (clientSettings["SVAuxData"] && clientSettings["SVAuxDisplayData"] && !lightWeight) {
                                    var svauxdata = clientSettings["SVAuxData"].split(',');
                                    var dfileds = clientSettings["SVAuxDisplayData"].split(',');
                                    var fields = [];
                                    var table = svauxdata[0];
                                    svauxdata.shift();
                                    if (svauxdata.length < dfileds.length)
                                        dfileds.shift();
                                    for (i = 0; i < svauxdata.length; i++) {
                                        var _ordinal = (getDataField && getDataField(svauxdata[i], table)) ? getDataField(svauxdata[i], table).Serial : null;
                                        fields.push({ fieldname: svauxdata[i], displayname: dfileds[i], ordinal: _ordinal });
                                    }
                                    fields = fields.sort(function (a, b) { return a.ordinal - b.ordinal; });
                                    getAuxTableTemplate(table, fields);
                                }
                                else
                                    $('.aux-data').hide()
                                for (x in parcels) {
                                    if (parcels[x].ID.toString() == activeParcel.Id)
                                        parcels[x].ColorCode = activeParcel.ColorCode;
                                }


                                for (x in markers) {
                                    var m = markers[x];
                                    if (m.ParcelId.toString() == activeParcel.Id) {
                                        activeMarker = m;
                                        var icon = activeMarker.icon;
                                        activeIcon = icon;
                                        icon.fillColor = activeParcel.ColorCode;
                                        icon.strokeColor = activeParcel.ColorCode;
                                        activeMarker.setIcon(icon);
                                        activeMarker.setMap(map);
                                    }
                                }
                                if (callback) callback();
                                hideMask();
                                if (activeParcel.Points().length == 0 && type != 'refresh')
                                    alert('No map points available for this parcel');
                                $('#divRghtCntnr').width($(window).width() - ($('#divSearchCntnr').width() + $('#mapFrame').width()));
                                loadExtraMapOptions(type);
                                /*if (userAccess == "0" && (activeParcel.ReviewdDate != null || activeParcel.ReviewdDate != '')) //qcpassed moved from normal flags
                                    $("#rblStatus #rblStatus_4").attr('disabled', true);
                                else
                                    $("#rblStatus #rblStatus_4").attr('disabled', false);*/
                                if (SktchEdtrNwWndw && !SktchEdtrNwWndw.closed && type != 'refresh')
                                    OpenSketchNewWndw(1);
                                if (PRCNwWndw && !PRCNwWndw.closed && type != 'refresh' && !lightWeight) OpenSketchNewWndw(2);

                                _showSketchDropDown();
                                if (autoSaveEnabled)
                                    activateAutoSave();
                            })
                        })
                    })

                })
            },
            error: function (resp) { hideMask(); }
        });
    }, ignoreSprompt);
}


function getCategoryFromSourceTable(sourceTable) {
    try {
        var category = window.parent.fieldCategories? window.parent.fieldCategories.filter(function(fcs) { return fcs.SourceTable == sourceTable})[0]: null;
        return (category ? category : {});
    }
    catch (e) {
        console.log(e.message);
    }
}

function getCategory(categoryId) {
	var _category = ( window.parent.fieldCategories? window.parent.fieldCategories.filter(function (d) { return d.Id == categoryId; })[0]: [] );
    return _category;
}

function getAuxTableTemplate(table, fields) {
    var header = "", body = "", footer = "", gridViewLink = "", _gridViewName = 'buildings';
    var sourcetable = activeParcel[table]? activeParcel[table].filter( function(x){ return ( x.CC_Deleted != true && x.CC_Deleted != 'true' ) } ): null;
    var gridFields = fields.filter(function (f) { return f });
    if (gridFields.length > 5)
        gridFields = gridFields.slice(0, 5);
    if (!sourcetable) return false;
    if (sourcetable.length > 5) {
        sourcetable = sourcetable.slice(0, 5);
        _gridViewName = fieldCategories && fieldCategories.filter( function(f){ return f.SourceTable == table } )[0]? fieldCategories.filter( function(f){return f.SourceTable == table } )[0].Name: 'buildings'; 
       	gridViewLink = '<a id="gridLink" onclick = "return OpenSketchNewWndw(2,\''+table+'\');" style="margin-right: 15px;text-decoration: underline;cursor: pointer;font-weight: bold;font-style: italic;float: left;width: 100%;color: black;text-align: center;">Click to view more '+ _gridViewName +'(s)</a>';
    }
    gridFields.forEach(function (field) {
        header += "<th>" + field.displayname + "</th>";
        footer += "<td>${" + field.fieldname + ":table," + table + "}</td>";
    });
    header = "<thead style='font-size:smaller;'>" + header + "</thead>";
    body = "<tbody " + table + "></tbody>";
    footer = "<tfoot style='display:none;'>" + footer + "</tfoot>";
    if (gridFields.length == 0 && table != "")
        return false;
    var tableHtml;
    tableHtml = "<div class='aux-grid-view' style='padding-top:18px;padding-bottom:5px;max-width:96%;overflow-x:auto;float:left;padding-left: 6px;'><table style='width:100%;border-spacing:0px;border-collapse:collapse;' border='1' source='" + table + "'>" + header + body + footer + "</table></div>";
    $('.aux-data').html(tableHtml);
    $('.aux-data').append(gridViewLink);
    
    $('.aux-grid-view tbody').html($('.aux-grid-view tfoot').html());
    $('.aux-grid-view tbody').fillTemplate(sourcetable);
}

var activeMarker;
var activeIcon;
//function closeParcel(refresh) {
//    if (parcelPolygon != null) {
//        parcelPolygon.setMap(null);
//    }
//    if (mapWindow) mapWindow.close();
//    activeParcel = null;
//    var w = $(window).width(); var w1 = $('#divSearchCntnr').width();
//    if ($('#divSearchCntnr').is(':hidden')) {
//        $('#search-map').width(w);
//    }
//    else {
//        $('#search-map').width(w - w1);
//    }
//    $('.resultframe').hide();
//    $('#divRghtCntnr').hide();
//    $('#divFootCntnr').hide();
//    $('.searchframe').show();
//    if (refresh) {
//        setTimeout(function () { showResultsOnMap(true); }, 1000);
//        window.location.hash = "";
//    }
//}
function closeParcel(refresh) {
    svPromptAction(() => {
        if (parcelPolygon != null) {
            parcelPolygon.setMap(null);
        }
        if (mapWindow) mapWindow.close();
        activeParcel = null;
        var w = $(window).width();
        var w1 = $('#divSearchCntnr').width();
        if ($('#divSearchCntnr').is(':hidden')) {
            $('#search-map').width(w);
        } else {
            $('#search-map').width(w - w1);
        }
        $('.resultframe').hide();
        $('#divRghtCntnr').hide();
        $('#divFootCntnr').hide();
        $('.searchframe').show();
        if (refresh) {
            setTimeout(function () { showResultsOnMap(true); }, 1000);
            window.location.hash = "";
        }
    }, !refresh);
}


var currentSketch;
var picH, picW, cH, cW;
var drwg;
var canvas;
var picAngle = 0;



function paintSketch() {
    var zoom = $('.sketch-resize').val();
    var angle = $('.sketch-rotate').val();
    $('.sketch-zoom-value').html(parseInt(parseFloat(zoom).toFixed(2) * 100) + '%');
    $('.sketch-rotate-value').html(angle + '&deg;');

    if (mapFrame) {
    	if(mapFrame.contentWindow.zoomSketch && mapFrame.contentWindow.rotateSketch){
        	mapFrame.contentWindow.zoomSketch(zoom, true);
        	mapFrame.contentWindow.rotateSketch(parseFloat(angle));
    	}
    }
}

$('input:text,textarea').focus(function (e) {
    currentInput = this;
}).blur(function () { currentInput = null; })

function loadEssentialTables(callback) {
    CachelookupTable(function () {
        $.ajax({
            url: '/sv/loadtables.jrq',
            data: {},
            dataType: 'json',
            success: function (resp) {
                var lv = resp.lookup
                for (var x in lv) {
                    var f = lv[x].Source;
                    var v = lv[x].Value.trim();
                    var n = lv[x].Name;
                    var d = lv[x].Description;
                    var o = parseInt(lv[x].Ordinal);
                    var t = lv[x].SortType;
                    if (lookup[f] === undefined) {
                        lookup[f] = {};
                    }
                    var color = lv[x].Color? lv[x].Color: '';
                    var NumericValue = lv[x].NumericValue;
                    if (lookup[f][v] === undefined)
                        lookup[f][v] = { Name: n, Description: d, Ordinal: o, SortType: t, Id: v, color: color, NumericValue: NumericValue, AdditionalValue1: lv[x].AdditionalValue1, AdditionalValue2: lv[x].AdditionalValue2 };
                }
                DTR_StatusType = resp.DTR_StatusType;
                DTR_Tasks = resp.DTR_Tasks
                fieldCategories = resp.fieldCategories;
                fieldCategories = ( fieldCategories && !_.isEmpty(fieldCategories) )? fieldCategories: [];
                parentChild = resp.parentChild;
                fields = resp.fields;
                datafieldsettings = resp.datafieldsettings;
                for (var x in resp.fields) {
                    var n = resp.fields[x];
                    for (var f in n) {
                        if (n[f] == 'true') n[f] = true;
                        if (n[f] == 'false') n[f] = false;
                    }
                    datafields[n.Id.toString()] = n;
                }
                globalTableList = parentChild.map(function (a) { return a.ChildTable })
                for (var x in resp.clientSettings) {
                    var n = resp.clientSettings[x].Name;
                    var v = resp.clientSettings[x].Value;
                    clientSettings[n.toString()] = v;
                }
                for (var x in resp.sketchSettings) {
                    var n = resp.sketchSettings[x].Name;
                    var v = resp.sketchSettings[x].Value;
                    sketchSettings[n.toString()] = v;
                }
                for (var x in resp.tableKeys){
                	var v = resp.tableKeys[x].Name;
		            var n = resp.tableKeys[x].SourceTable;
		            var temp = { Name: v, SourceTable: n };
		            tableKeys.push(temp);
		        }
                var _clientSketchSettingsSkconfig = clientSettings['SketchConfig'] || sketchSettings['SketchConfig'];
                if ( _clientSketchSettingsSkconfig == 'MVP' ) {
                    MVP_Lookup = resp.sk_Lookup;
                    MVP_Outbuliding_Lookup = resp.Outbuliding_Lookup;
                    MVP_Default_Exterior_Cover = resp.Default_Exterior_Cover;
                    MVP_sketch_codes = resp.sketch_codes;
                    MVP_sketch_labels = resp.sketch_labels;
                }
                if ( _clientSketchSettingsSkconfig == 'ProValNew' || _clientSketchSettingsSkconfig == 'ProValNewFYL' ) {
                    Proval_Lookup = resp.sk_Lookup;
                    Proval_Outbuliding_Lookup = resp.Outbuliding_Lookup;
                    Proval_Default_Exterior_Cover = resp.Default_Exterior_Cover;
                    Proval_sketch_codes = resp.sketch_codes;
                }
                if ( _clientSketchSettingsSkconfig == 'FTC' || _clientSketchSettingsSkconfig == 'ApexJson' ) {
                    FTC_Detail_Lookup = resp.detail_Lookup;
                    FTC_AddOns_Lookup = resp.addons_Lookup;
                    FTC_Floor_Lookup = resp.floor_lookup;
                }
                if ( _clientSketchSettingsSkconfig == 'Lafayette' ) {
                    Lafayette_Lookup = resp.lafayette_Lookup;
                }
                if ( _clientSketchSettingsSkconfig == 'Patriot' ) {
                    Patriot_Lookup = resp.patriot_Lookup;
                }
                if ( _clientSketchSettingsSkconfig == 'FarragutJson' ) {
                	Farragut_Lookup = resp.farragut_Lookup;
                }
                if (_clientSketchSettingsSkconfig == 'SigmaCuyahoga_Lookup') {
                    SigmaCuyahoga_Lookup = resp.sigmaCuyahoga_Lookup;
                }
                if (_clientSketchSettingsSkconfig == 'WrightJson') {
                    WrightAddn_Lookup = resp.WrightAddn_Lookup;
                    WrightCom_Lookup = resp.WrightCom_Lookup;
                    WrightComType_Lookup = resp.WrightComType_Lookup;
                }
                if (datafields) {
                    Object.keys(datafields).map(function (x) { return datafields[x] }).forEach(function (x, i) {
                        if (x.LookupTable == "$QUERY$") {
                            getLookupData(x, {}, function () { });
                        }
                        if (x.InputType == 5 || x.InputType == 11) {
                            LookupMap[x.Name] = { LookupTable: x.LookupTable, IdMap: false };
                            LookupMap['#FIELD-' + x.Id] = { LookupTable: x.LookupTable, IdMap: true }
                        }
                    })
                }
                if (callback) callback()
            },
            error: function (resp) { }
        });
    });
}

function getLookupData(field, options, callback) {
    var data = [];
    var cat = options.category;
    if (cat == null)
        cat = field.Category;
    if (field.IsLargeLookup == 'true' || field.IsLargeLookup == true) {
        if (callback) callback([]);
        return;
    }
    if (field.SourceTable == "_photo_meta_data") {
        if (callback) callback([]);
        return;
    }
    var specialType = false;
    if (options.dataSource == 'ccma.YesNo') {
        if (callback) callback(ccma.YesNo);
    } else {
        if (field.LookupTable == '$QUERY$') {
            var query = field.LookupQuery || '';
            if (lookup['#FIELD-' + field.Id] == undefined) {
                var fullQuery = query || '';
                if (query.search(/\{[a-zA-Z0-9_.]*\}/i) > 0) {
                    if (query.search(/where/i) > 0) {
                        if (query.search(/union/i) > 0) {
                            var unionSplit = query.split(/union/i)
                            unionSplit.forEach(function (each, index) { if (unionSplit[index].search(/where/i) > 0) unionSplit[index] = unionSplit[index].substr(0, unionSplit[index].search(/where/i)) });
                            fullQuery = unionSplit.join('union');
                        }
                        else
                            fullQuery = query.substr(0, query.search(/where/i));
                    }
                }
                if (fullQuery && fullQuery.search(/\{.*?\}/g) == -1 || (field.LookupQueryForCache && field.LookupQueryForCache != '')) {
                    if (field.LookupQueryFilter) extendedLookup['#FIELD-' + field.Id] = []
                    if (field.LookupQueryForCache && field.LookupQueryForCache != '') fullQuery = field.LookupQueryForCache;
                    getData(fullQuery, [], function (ld) {
                        if (ld.length > 0) {
                            var f = field.Name;
                            lookup[f] = {};
                            var props = Object.keys(ld[0]);
                            var IdField = props[0]; var NameField = props[1]; var DescField = props[2]
                            if (!NameField) NameField = IdField;
                            if (!DescField) DescField = NameField;
                            for (var x in ld) {
                                var d = ld[x];
                                var v = d[IdField];
                                if (v) v = v.toString().trim();
                                if (lookup[f][v] === undefined)
                                    lookup[f][v] = { Name: d[NameField], Description: d[DescField], Ordinal: x, SortType: null, Id: v, Object: d };
                                if (field.LookupQueryFilter) {
                                    d.Ordinal = x;
                                    d.Name = d[NameField];
                                    d.Description = d[DescField];
                                    d.Id = v;
                                    extendedLookup['#FIELD-' + field.Id].push(d)
                                }
                            }
                            lookup['#FIELD-' + field.Id] = lookup[f];
                        }
                    });
                } else specialType = true;

            }
            var o;
            if (cat && cat.TabularData == "true") {

            } else {
                o = activeParcel;
            }
            if (options.source)
                o = options.source;
            if (o) {
                if (query.search(/\{.*?\}/g) > -1) {
                    query.match(/\{.*?\}/g).forEach(function (fn) {
                        var testFieldName = fn.replace('{', '').replace('}', '');
                        if (testFieldName.indexOf('parent.') > -1) {
                            var fieldName = testFieldName.split('parent.');
                            lookupField = fieldName[fieldName.length - 1];
                            source = o;
                            while (testFieldName.search('parent.') > -1) {
                                source = source["parentRecord"] ? (source["parentRecord"].constructor == Array ? source["parentRecord"][0] : source["parentRecord"]) : []
                                testFieldName = testFieldName.replace('parent.' + lookupField, lookupField);
                            }
                            //source = o["parentRecord"] ? o["parentRecord"] : [];
                            //lookupField = testFieldName.split('.')[1];
                            //if (testFieldName.indexOf('parent.parent') > -1) {
                            //    source = source["parentRecord"] ? source["parentRecord"] : [];
                            //    lookupField = testFieldName.split('.')[2];
                            //}
                        }
                        else if (testFieldName.indexOf('parcel.') > -1) {
                            source = activeParcel;
                            lookupField = testFieldName.split('parcel.')[1];
                        }
                        else { lookupField = testFieldName.split('.')[0]; source = o };
                        value = source[lookupField];
                        if (value && typeof (value) == "string") value = value.replace(/'/g, "''");
                        if (value != 0 && !value && value != '') value = 'null';
                        if (('ImagePath' in source) && ('Id' in source)) {
                            var imgfield = getDataField(lookupField, '_photo_meta_data');
                            var valueField = '';
                            if (imgfield) {
                                //                                valueField = imgfield.AssignedName.replace('PhotoMetaField', 'MetaData');
                                //                                value = o[valueField];
                                //                                if ((value == null || value === undefined || value == '') && imgfield.DefaultValue) value = imgfield.DefaultValue;
                                valueField = imgfield.AssignedName.replace('PhotoMetaField', 'MetaData');
                                value = source[valueField];
                            }

                            if (testFieldName.split('.') > 0) {
                                var attrName = testFieldName.split('.')[1];
                                var lk = getFieldLookup(lookupField);
                                if (lk) {
                                    if (lk[value]) {
                                        if (lk[value].Object) {
                                            value = lk[value].Object[attrName];
                                        }
                                    }
                                }
                            }

                        }

                        query = query.replace(fn, value);

                    });
                }
            }
            if (query) {
                if (query.search(/\{.*?\}/g) == -1) {
                    getData(query, [], function (ld) {
                        if (ld.length > 0) {
                            var f = field.Name;
                            var props = Object.keys(ld[0]);
                            var IdField = props[0]; var NameField = props[1]; var DescField = props[2]
                            if (!NameField) NameField = IdField;
                            for (var x in ld) {
                                if (specialType) lookup[f] = {};
                                var d = ld[x];
                                var v = d[IdField];
                                if (v === 'null') { v = ''; d[IdField] = ''; }
                                var name = d[NameField];
                                if (name) name = name.toString().trim();
                                //if (v && v.trim) {
                                //    v = v.trim();
                                //}
                                if (!name) name = v;
                                data.push({ Name: name, Description: d[DescField], Ordinal: x, SortType: null, Id: v });
                                if (specialType) {
                                    if (lookup[f][v] === undefined)
                                        lookup[f][v] = { Name: d[NameField], Description: d[DescField], Ordinal: x, SortType: null, Id: v, Object: d };
                                }
                            }
                        }
                        if (options.addNoValue) {
                            data.unshift({ Name: '-- No Value --', Id: '' });
                        } else if (isTrue(field.LookupShowNullValue)) {
                            data.unshift({ Id: '', Name: '-- No Value --' });
                        } else {
                            data.unshift({ Name: '', Id: '' });
                        }
                        if (callback) callback(data, options.source, options);
                    }, function () {
                        if (callback) callback([]);
                    });
                } else {
                    if (callback) callback([]);
                }
            }
            else {
                if (callback) callback([]);
            }
        } else {
            var l = lookup[field.LookupTable]
            for (x in l) {
                var Idfield = x;
                // if (field.IsClassCalculatorAttribute == 'true')
                // Idfield = l[x].NumericValue || 0
                data.push({
                    Id: x,
                    Name: l[x].Name,
                    Description: l[x].Description,
                    Ordinal: l[x].Ordinal,
                    SortType: (l[x].SortType && !isNaN(l[x].SortType)) ? parseInt(l[x].SortType) : null
                })
            }
            if (options.addNoValue) {
                data.unshift({ Name: 'NO VALUE', Id: '' });
            }
            if (isTrue(field.LookupShowNullValue)) {
                data.unshift({ Name: '', Value: '<blank>' });
            }
            if (callback) callback(data.sort(function (x, y) { return x.Ordinal ? x.Ordinal - y.Ordinal : x.Id > y.Id ? 1 : -1 }), options.source, options);
        }
    }
}

function getData(query, params, callback, errorCallback, otherData) {
    if (params == null)
        params = [];
    var data = [];
    if (query) {
        _dB.transaction(function (x) {
            x.executeSql(query, params, function (x, results) {
                var len = results.rows.length, i;
                for (i = 0; i < len; i++) {
                    data.push(results.rows.item(i));
                }
                if (callback) callback(data, results, otherData);
            }, function (x, e) {
                if (errorCallback) errorCallback();
                console.warn(e.message);
                console.warn(query);

            })
        });
    }
}

function CachelookupTable(callback) {
    if (typeof(openDatabase) == 'function') {
        _dB = openDatabase('CAMACLOUD', '1.0', 'CAMACloud', 500 * 1024 * 1024);
    }
    else {
        _dB = new SQL.Database();
        _dB.transaction = function (callback) {
            var tx = {
                executeSql: function (query, params, success, failure) {
                    var _tx = this;
                    var resp = {
                        rows: {
                            length: 0,
                            item: function (i) {
                                return this[i];
                            }
                        }
                    };
                    try {
                        if (params?.length == 0) {
                            var raw = _dB.exec(query)[0];
                            if (raw) {
                                var values = raw.values;
                                resp = {
                                    rows: {
                                        length: values.length,
                                        item: function (i) {
                                            return this[i];
                                        }
                                    }
                                };

                                for (var i = 0; i < values.length; i++) {
                                    resp.rows[i] = {};
                                    var k = 0;
                                    for (var c of raw.columns) {
                                        resp.rows[i][c] = values[i][k];
                                        k++;
                                    }
                                }

                            }
                        }
                        else {
                            _dB.run(query, params);
                        }
                    }
                    catch (ex) {
                        failure && failure(_tx, ex);
                        return;
                    }

                    success && success(_tx, resp);
                }
            }

            callback && callback(tx);
        }
    }
    $.ajax({
        url: '/sv/lookupdatatables.jrq',
        data: {},
        dataType: 'json',
        success: function (data) {
            var tableName = '*ld*';
            if ((data && data.length != undefined)) {
                _syncSubTables(tableName, data, function () {
                    if (callback)
                        callback();
                });
            }

        },
        error: function (req, err, msg) {
            alert('Data synchronization failed!');
        }
    });
}

var subTables = [];
var syncProgressList = [];
function _syncSubTables(tableName, data, callback) {
    if (data.length == 0) {
        //log('No data downloaded as sub tables.');
        if (callback) callback();
    }
    var filtered = {};
    for (x in data) {
        var item = data[x];
        var dsName = item.DataSourceName;
        if (filtered[dsName] == null) {
            filtered[dsName] = [];
        }
        filtered[dsName].push(item);
    }
    _syncSubTableFromList(filtered, callback);
}

function _syncSubTableFromList(filtered, callback) {
    if (Object.keys(filtered).length == 0) {
        if (callback) callback();
    }
    for (var key in filtered) {
        _syncTable(key, filtered[key], function () {
            delete filtered[key];
            _syncSubTableFromList(filtered, callback);
        });
        break;
    }
}

function _syncTable(tableName, data, callback) {
    console.log("Downloaded table: " + tableName + ", " + data.length + " rows.");
    if (data.length == 0) {
        syncProgressList.push(tableName);
        if (callback) callback();
        return;
    }
    if (syncProgressList.indexOf(tableName) > -1) {
        syncProgressList.push(tableName);
        _syncData(tableName, data, columnDir[tableName], callback);
        return;
    }

    if (data.length > 0) {
        _dB.transaction(function (x) {
            var dropAction = "DROP TABLE IF EXISTS " + tableName;
            var dropConfirm = tableName + " dropped.";
            executeSql(x, dropAction, [], function (x) {
                //console.log(dropConfirm);
                if (data.length > 0) {
                    var firstRow = data[0];
                    _getColumnInformation(firstRow, function (cInfo) {
                        columnDir[tableName] = cInfo;
                        syncProgressList.push(tableName);
                        _syncData(tableName, data, cInfo, callback);
                    }, tableName);
                }
            });
        });

    }
}

function _syncData(tableName, data, cinfo, callback) {
    var createSql = "CREATE TABLE IF NOT EXISTS " + tableName + " (" + cinfo.CreateColumns + ");";
    _dB.transaction(function (x) {
        executeSql(x, createSql, [], function (tx) {
            //log("Loading local table " + tableName + ".");
            var firstRow = data[0];
            var progress = 0;
            for (i in data) {
                var parcel = data[i];
                var total = data.length;
                var pa = _parseDataForInsert(parcel, firstRow, cinfo);
                var columns = "";
                var phs = "";
                for (key in parcel) {
                    if (cinfo.ColumnKeys.indexOf(key) > -1) {
                        if (key == "Index") key = "SortIndex";
                        if (columns == "") { columns = "[" + key + "]"; phs = "?"; }
                        else { columns += ", " + "[" + key + "]"; phs += ", ?"; }
                    }
                }
                var insertSql = "INSERT INTO " + tableName + " (" + columns + ") VALUES (" + phs + ")";
                executeSql(tx, insertSql, pa, function (tx, res) {
                    progress += 1;
                    if (progress == total) {
                        //log("Finished creating local table " + tableName + ".");
                        if (callback) callback();
                    }
                }, function (e, m) {
                    log(m.message, true);
                });
            }
        });
    });


}

var columnDir = {};

function _getColumnInformation(firstRow, callBack, tableName) {
    var cKeys = [];
    var columns = "";
    var createColumns = "";
    var phs = "";
    for (key in firstRow) {
        var df = getDataField(key, tableName);

        cKeys.push(key);
        var type = "TEXT"

        //        if (key == "Index") {
        //            key = "SortIndex"
        //            type = "INTEGER";
        //        }
        if (key == "Id" || /^.*?Id$/.test(key) || /^.*?_id$/.test(key) || /^.*?UID$/.test(key)) {
            type = "INTEGER"
        }
        if (key == "Synced") {
            type = "INTEGER"
        }
        if (firstRow[key] != null) {
            if ((firstRow[key].toString() == 'true') || (firstRow[key].toString() == 'false')) {
                type = "BOOL";
            }
        }
        if (df) {
            switch (parseInt(df.InputType)) {
                case 1: type = "TEXT"; break;
                case 2: type = "FLOAT"; break;
                case 3: type = "BOOL"; break;
                case 4: type = "DATETIME"; break;
                case 7: type = "FLOAT"; break;
                case 8: type = "INT"; break;
                case 9: type = "INT"; break;
                case 10: type = "INT"; break;
                default: type = "TEXT"; break;
            }
        }
        if (columns == "") {
            columns = key;
            createColumns = "[" + key + "]" + " " + type;
            phs = "?";
        }
        else {
            columns += ", " + key;
            createColumns += ", " + "[" + key + "]" + " " + type;
            phs += ", ?"
        }
    }

    var r = new Object();
    r.Columns = columns;
    r.CreateColumns = createColumns;
    r.PlaceHolders = phs;
    r.ColumnKeys = cKeys;

    if (callBack) callBack(r);
}

function executeSql(tx, query, params, success, failure) {
    tx.executeSql(query, params, function (x, results) { if (success) success(x, results) }, function (x, e) { if (failure) failure(x, e); else console.error(query, e.message) });
}


function _parseDataForInsert(parcel, firstRow, cinfo) {
    var pa = [];
    for (key in parcel) {
        if (cinfo.ColumnKeys.indexOf(key) > -1) {
            var dat;
            dat = parcel[key];
            if (TrueFalseInPCI == "False") {
                if (dat == "True" || dat == "true") dat = true;
                if (dat == "False" || dat == 'false') dat = false;
            }
            if (key == "Index") {
                dat = parseInt(dat);
            }
            dat = dat === null ? dat : dat.toString();
            pa.push(dat);
        }
    }
    return pa;
}

function evalLookup(source, value, getDescription, field, getNumericValue, options) {
    var fieldId = field ? field.Id : null;
    var item;
    if ((value == undefined) || (value == null) || (value == "")) {
        return "";
    }
    if (getFieldLookups(source, fieldId) == null) {
        console.warn('Missing lookup table - ' + source);
        return value;
    }    
    value = value.toString().trim();
    var item = getFieldLookups(source, fieldId, options)[value];
    if (item == null || !item.Name)
        return value
    if (getDescription)
        return item.Description;
    if (getNumericValue)
        return item.NumericValue;
    return item.Name
}

function getFieldLookups(fieldName, fieldId, options) {
    var selector = fieldName;
    if (fieldId)
        selector = '#FIELD-' + fieldId;
    var lk = LookupMap[selector];

    if (lk == undefined) {
        return null;
    } else if (lk.LookupTable == "$QUERY$") {
        var lkd;
        if (!lookup[selector]) {
            lkd = lookup[fieldName];
        } else {
            lkd = lookup[selector];
        }
        if (options && options.referenceObject && options.referenceFilter && extendedLookup[selector].length > 0) {
            var source = options.referenceObject
            var expression = options.referenceFilter
            expression.match(/\{.*?\}/g).forEach(function (fn) {
                var testFieldName = fn.replace('{', '').replace('}', '');
                var tempSource = source;
                while (testFieldName.indexOf('parent.') > -1) {
                    tempSource = source['parentRecord'];
                    testFieldName = testFieldName.replace('parent.', '');
                }
                if (tempSource) {
                    var value = tempSource[testFieldName]
                    expression = expression.replace(fn, value);
                }
            })
            for (x in extendedLookup[selector][0]) { var regex = new RegExp(x, "g"); expression = expression.replace(regex, 'thisObject.' + x) };
            lkd = eval('extendedLookup["' + selector + '"].filter(function (thisObject) { return (' + expression + ') })');
            var obj = {};
            lkd.forEach(function (e) {
                obj[e.Id] = e
            })
            return obj;
        } else
            return lkd;
    } else {
        return lookup[lk.LookupTable];
    }
}

function calculateChildRecords(field, index, rec) {
    return ''
}
function previewSketch() {
    mapFrame.contentWindow.firstTimeLoad = true;
    mapFrame.contentWindow.isSketchChanged = false;
    var _dropDownId = $('.sketch-select').val();
	sketchDropDownChange = $.grep(sketchDropDownChange, function(data, index) { return data.Id != _dropDownId });
    mapFrame.contentWindow.previewSketch();
    var skRowuid = top.$('.sketch-select option[value="' + _dropDownId + '"]').attr('skrowuid');
    var skPrefix = top.$('.sketch-select option[value="' + _dropDownId + '"]').attr('skprefix');
    skPrefix = mapFrame.contentWindow.isNewSketchData && skPrefix? skPrefix: '';
    var selecteddpVal = mapFrame.contentWindow.isNewSketchData? skRowuid: _dropDownId;
    if (selecteddpVal && mapFrame.contentWindow.currentOverlayPosition && mapFrame.contentWindow.currentOverlayPosition['sketch-' + selecteddpVal + skPrefix] && mapFrame.contentWindow.currentOverlayPosition['sketch-' + selecteddpVal + skPrefix ]['1'] && mapFrame.contentWindow.currentOverlayPosition['sketch-' + selecteddpVal + skPrefix]['1'].mapZoom) {
    	mapFrame.contentWindow.firstTimeLoad = false;
    	mapFrame.contentWindow.mapZoom = mapFrame.contentWindow.currentOverlayPosition['sketch-' + selecteddpVal + skPrefix]['1'].mapZoom;
    	mapFrame.contentWindow.map.setZoom(mapFrame.contentWindow.currentOverlayPosition['sketch-' + selecteddpVal + skPrefix]['1'].mapZoom);
    } 
}

var SktchEdtrNwWndw;
var PRCNwWndw;
var sketchMode = ''
function OpenSketchNewWndw( Type, _gridSourceTable ) {
    if (Type == 1) {
    	if ( !clientSettings.SketchConfig && !sketchSettings["SketchConfig"] ) {
        	alert("Please add a sketch config to load sketches");
        	return false;
    	}
        var sketches = []; activeParcel.isInvalidSketchString = false;
        try { sketches = mapFrame.contentWindow.getSketchSegments(); } catch (ex) { activeParcel.isInvalidSketchString = true; }

        activeParcel.CCSketchSegments = sketches;
       	ccma.Sketching.Config = mapFrame.contentWindow.ccma.Sketching.Config;
        ccma.Sketching.SketchFormatter = mapFrame.contentWindow.ccma.Sketching.SketchFormatter;
        SktchEdtrNwWndw = openWindow(SktchEdtrNwWndw, 'sketcheditor', '/protected/sketched/', { resizable: 'true', width: 1200, height: 700 });
        //   SktchEdtrNwWndw = window.open( '/protected/sketched/', 'sketcheditor', 'width=400,height=530,top=115, left=3' );
    }
    else if (Type == 2) {
    	activeParcel._gridSourceTable = _gridSourceTable? _gridSourceTable: null;
        PRCNwWndw = openWindow(PRCNwWndw, 'prc', '/protected/svo/prc.aspx', { resizable: 'true', width: 1200, height: 700 });
    }
    return false;
}
function loadPRCactiveParcel(tablelist, parcel, callback) {
    /*if (lightWeight) {
        if (callback) callback(activeParcel);
        return;
    }*/
    if (tablelist.length == 0) {
        if (callback) callback(activeParcel);
        return;
    }
    var sourceTable = tablelist.pop().tableName;
    if (sourceTable === undefined) {
        if (callback) callback(activeParcel);
        return;
    }
    var childvalue = eval('parcel.' + sourceTable);
    var parenttable = parentChild.filter(function (s) { return s.ChildTable == sourceTable })[0].ParentTable;
    var Parentvalue = eval('parcel.' + parenttable);
    var _skcsettSketchConf = ( clientSettings && clientSettings['SketchConfig'] ) || ( sketchSettings && sketchSettings['SketchConfig'] ) ;
    if(Parentvalue && (_skcsettSketchConf == "ProValNew" || _skcsettSketchConf == "ProValNewFYL" ) && (parenttable == "sktsegment" || parenttable == "ccv_SktSegment")){ // handle proval filter field issues in sktsegment 
    	Parentvalue.sort(function(a, b){return a.CC_Deleted - b.CC_Deleted});
    }
    if (childvalue != undefined && childvalue.length == 0) {
        if (Parentvalue) Parentvalue.forEach(function (pVal) { pVal[sourceTable] = []; })
        loadPRCactiveParcel(tablelist, parcel, callback);
        return;
    }
    var filterfields = fieldCategories.filter(function (x) { return x.SourceTable == sourceTable }).map(function (p) { return p.FilterFields })[0]// fieldCategories.filter(function (x) { return x.SourceTable == sourceTable });
    if (filterfields && filterfields != "") { filterfields = filterfields.split(','); } else { filterfields = [] }
    if (Parentvalue) {
        Parentvalue.forEach(function (x) {
            var filterCondition = '';
            var iter_value = 0;
            while (iter_value < filterfields.length) {
                var childFilter, parentFilter; parentFilter = childFilter = filterfields[iter_value];
                if (childFilter.indexOf('/') > -1) { parentFilter = childFilter.split('/')[0]; childFilter = childFilter.split('/')[1]; }
                var values = eval("x." + parentFilter);
                filterCondition += 'd.' + childFilter + ' == \'' + values + '\'';
                iter_value += 1;
                if (iter_value < filterfields.length) {
                    filterCondition += ' && ';
                }
            }
            if (filterCondition == '') filterCondition = '1==1'
            var parentROWUID = x.ROWUID;
			childvalue = childvalue? childvalue: [];
            var fiteredSource = eval('childvalue.filter(function (d, i) { return ((!d.ParentROWUID &&(' + filterCondition + ')) || (d.ParentROWUID && d.ParentROWUID == ' + parentROWUID + '))})');
            fiteredSource.forEach(function (cc, findex) {
                if (cc.Original) cc.Original["ParentROWUID"] = parentROWUID;
                cc["parentRecord"] = x
                cc["ParentROWUID"] = parentROWUID;
                cc["CC_FIndex"] = findex;
            })
            x[sourceTable] = fiteredSource;
        });
    }
    //eval('delete parcel[sourceTable]');
    loadPRCactiveParcel(tablelist, parcel, callback);
}
function LoadAuxDataPRC(activeParcel, tableListGlobal, callback) {
    /*if (lightWeight) {
        if (callback) callback(activeParcel);
        return;
    }*/

    TableHierarchical = [];
    tableListGlobal.forEach(function (name) {
        var no = LoadTableHierarchical(name);
        if (no > 1) {
            TableHierarchical.push({ 'tableName': name, 'no': no });
        }
    });
    TableHierarchical.sort(function (a, b) {
        return a.no > b.no;
    })
    if (callback) callback(activeParcel);
}
function loadAuxData(parcel, tableList, callback) {
    var parcelId = parcel.Id;
    if (tableList.length == 0) {
        if (callback) callback(parcel);
        return;
    }
    var sourceTable = tableList.pop();
    if (sourceTable === undefined) {
        if (callback) callback(parcel);
        return;
    }
    var data = parcel[sourceTable];
    var originalData = parcel.Original[sourceTable];
    if (data) {
        var index, element, object = [];
        for (d in data) {
            data[d].Original = {};
            object = Object.keys(data[d]);
            for (var o in object) {
                index = object[o];
                element = data[d][index];
                if (typeof (element) != 'object' || element == null)
                    data[d].Original[index] = originalData[d][index];
            }
        }
    }
    parcel[sourceTable] = data;
    loadAuxData(parcel, tableList, callback);
}
function LoadTableHierarchical(TableName) {
    var t = 1;
    var ab = function (child) { var dd = parentChild.filter(function (f) { return f.ChildTable == child }); if (dd.length == 0) { return t } if (dd[0].ParentTable.toLowerCase() == 'parcel') { return t } else { t = t + 1; ab(dd[0].ParentTable) } };
    var dd = ab(TableName); return t;
}
function moveNxt(_nextParcel, ignoreSprompt) {
    svPromptAction(() => {
        if (mapWindow) mapWindow.close();
        if (activeParcel) {
            var currentParcelId = activeParcel.Id
            if (currentParcelId) {
                curIndex = $("#TabSearchContents tr[id=tr-" + currentParcelId + "]").index()
                var totParcel = $("#TabSearchContents tr").length;
                if (curIndex + 1 == totParcel) {
                    if (parcels) {
                        $('#btnNextPage').click();
                        curIndex = 0;
                        _checkReviewdSketchs(function () {
                            let parId = $($("#TabSearchContents tr").eq(curIndex)).attr('id') ? $($("#TabSearchContents tr").eq(curIndex)).attr('id').substring(3) : null;
                            if (parId) { showMask(); openParcel(parId, null, function () { hideMask(); }, true) }
                        });
                        $('#divtab').scrollTop(0);
                        return;
                    }
                }
                else if (totParcel == 1) return;
                if (curIndex > -1) {
                    _checkReviewdSketchs(function () {
                        var parId = $($("#TabSearchContents tr").eq(curIndex + 1)).attr('id') ? $($("#TabSearchContents tr").eq(curIndex + 1)).attr('id').substring(3) : null;
                        if (parId) { showMask(); openParcel(parId, null, function () { hideMask(); }, true) }
                    }, _nextParcel);
                }
            }
        }
    }, ignoreSprompt);
}

function movePrev() {
    svPromptAction(() => {
        if (mapWindow) mapWindow.close();
        if (activeParcel) {
            var currentParcelId = activeParcel.Id
            if (currentParcelId) {
                curIndex = $("#TabSearchContents tr[id=tr-" + currentParcelId + "]").index()
                if (curIndex > 0) {
                    _checkReviewdSketchs(function () {
                        var parId = $($("#TabSearchContents tr").eq(curIndex - 1)).attr('id') ? $($("#TabSearchContents tr").eq(curIndex - 1)).attr('id').substring(3) : null;
                        if (parId) { showMask(); openParcel(parId, null, function () { hideMask(); }, true) }
                    });

                }
                else if (curIndex == 0) {
                    if (parcels) {
                        $('#btnPreviousPage').click();
                        curIndex = parseInt($('#recordsPerPageSelector').val(), 10);
                        _checkReviewdSketchs(function () {
                            var parId = $($("#TabSearchContents tr").eq(curIndex - 1)).attr('id') ? $($("#TabSearchContents tr").eq(curIndex - 1)).attr('id').substring(3) : null;
                            if (parId) { showMask(); openParcel(parId, null, function () { hideMask(); }, true) }
                        });
                        var lastRecord = $('#divtab')[0].scrollHeight;
                        $('#divtab').scrollTop(lastRecord);
                        return;
                    }
                }
            }
        }
    });
}

function getDataField(key, table) {
    var res = Object.keys(datafields).filter(function (x) { return table ? ((datafields[x].Name == key) && (datafields[x].SourceTable == table)) : ((datafields[x].Name == key) && (datafields[x].CategoryId || (datafields[x].QuickReview == "true")) && datafields[x].SourceTable == null) }).map(function (x) { return datafields[x] });
    if (!table && res.length == 0) {
        res = Object.keys(datafields).filter(function (x) { return (datafields[x].Name == key) }).map(function (x) { return datafields[x] });
    }
    if (res.length > 0)
        return res[0];
    else
        return null;
}
function saveSketchChanges(data, Notedata, cc_error_msg, auditTrailEntry, callback) {
    try {
        var saveData = "", noteSaveData = "", CC_YearStatus = "";
        
        if ( clientSettings['FutureYearEnabled'] == '1' ) 
			CC_YearStatus = 'A';
        
        for (var x in data) {
            var sd = data[x];
            if (sd.sid.includes("--")) sd.sid = sd.sid.split("--")[1]
            var vectorString = sd.vectorString;
            var fieldId = sd.sourceFieldId;
            var fieldIds = Object.keys(sd.segments).map(function (x) { return sd.segments[x].fieldId }).filter(function (x) { return x > -1 }).reduce(function (x, y) { return x + "|" + y }, fieldId) + '|' + (sd.perimeterFieldId || -1) + '|' + (sd.labelCommandFieldId || -1) + '|' + (sd.dimensionCommandFieldId || -1) + '|' + (sd.labelFieldId || -1) + '|' + ((sd.extraLabels.map(function (a) { return (a.FieldId || -1) }).length > 0) ? sd.extraLabels.map(function (a) { return (a.FieldId || -1) }).join('|') : '-1');
            var fieldData = Object.keys(sd.segments).map(function (x) { return sd.segments[x] }).filter(function (x) { return x.fieldId > -1 }).map(function (x) { return x.area || 0 }).reduce(function (x, y) { return x + "|" + y }, vectorString) + '|' + (sd.perimeter || 0) + '|' + (sd.labelCommand || '') + '|' + (sd.dimensionCommand || '') + '|' + (sd.label || '') + '|' + sd.extraLabels.map(function (a) { return (a.Value || '') }).join('|')
            if (!sd.clientId)
                sd.clientId = sd.sid;
            saveData += sd.clientId + '|' + (sd.newRecord ? sd.sid : sd.clientId) + '|' + Base64.encode(fieldIds) + '|' + Base64.encode(fieldData) + '|' + CC_YearStatus + '\n';
        }
        for (var x in Notedata) {
            var nt = Notedata[x];
            if (nt.noteid && nt.noteid.toString().includes("--")) nt.noteid = nt.noteid.split("--")[1];
            var fieldIds = nt.noteFieldID + '|' + nt.xPositionFieldId + '|' + nt.yPositionFieldId;
            var data = '$note$' + nt.noteText + '|' + nt.xPosition + '|' + nt.yPosition;
            if (!nt.clientId)
                nt.clientId = nt.noteid
            noteSaveData += nt.clientId + '|' + (nt.newRecord ? nt.noteid : nt.clientId) + '|' + Base64.encode(fieldIds) + '|' + Base64.encode(data) + '|' + CC_YearStatus + '\n';
        }
        var isCC_Error = 0;
        if (activeParcel.CC_Error && (activeParcel.CC_Error.indexOf('Sketch labels are invalid') > -1 || activeParcel.CC_Error.indexOf('There are multiple Sections for this Building') > -1))
            isCC_Error = 1;
        $qc('savesketch', { ParcelId: activeParcel.Id, Data: saveData + '#' + noteSaveData, CC_Error: cc_error_msg ? cc_error_msg : '', isCC_Error: isCC_Error, auditTrailEntry: auditTrailEntry, appType: 'sv' }, function (resp) {
            console.log('Sketch successfully updated');
            if (callback) callback();

        });
    }
    catch (e) {
        console.log(e.message);
    }
}
var saveParcelChanges = function (callback) {
    if (callback) callback();
    //window.opener.hideMask
}
var getParcel = function (pId, callback) {
    openParcel(pId, 'refresh', callback)

}

function getDatatypeOfAggregateFields(aggr_field) {
    if (offlineTables != null) {
        var aggr_Setting = offlineTables.AggrFields.filter(function (agr) { return ((agr.FunctionName + '_' + agr.TableName + '_' + agr.FieldName) == aggr_field); })[0];
        if (aggr_Setting) {
            var df = getDataField(aggr_Setting.FieldName, aggr_Setting.TableName);
            if (df) return QC.dataTypes[parseInt(df.InputType)];
        }
    }
    if (QC && QC.dataTypes) {
        if (aggr_field.startsWith('SUM') || aggr_field.startsWith('AVG') || aggr_field.startsWith('COUNT') || aggr_field.startsWith('MAX') || aggr_field.startsWith('MIN')) {
            return QC.dataTypes[2];
        } else {
            return QC.dataTypes[1];
        }
    }
}
var pictometryPopup;  
function loadPictometry(autoOpen) {
    //openWindow(hwnd, name, url, params)
    if( autoOpen && localStorage.getItem('eagleViewDisabled') == "1" ) { return false; }
    else { localStorage.setItem('eagleViewDisabled', 0); }
    prevEagle = localStorage.getItem('eagleViewDisabled');
    
    pictometryPopup = openWindow(pictometryPopup, 'pictometry', '/protected/Svo/pictometry.aspx', { resizable: 'true', width: 1200, height: 700 });
    if (!pictometryPopup) {
        alert('Popup blocker on your computer is preventing the opening of EagleView. Please disable it to view EagleView.');
    } else {
		pictometryPopup.onunload = function() { 
			if(!isBeforeUnload)
				localStorage.setItem('eagleViewDisabled', "1");
		}    			
    }
}
var cycloPopup;
function loadCyclomedia(autoOpen) {
	if( autoOpen && localStorage.getItem('cyclomediaDisabled') == "1" ) { return false; }
	else { localStorage.setItem('cyclomediaDisabled', 0); }
	prevCyclo = localStorage.getItem('cyclomediaDisabled');
    
    cycloPopup = openWindow(cycloPopup, 'Cyclomedia', '/protected/Svo/cyclomedia.aspx', { resizable: 'true', width: 1200, height: 700 });
    if (cycloPopup) { 
		cycloPopup.onunload = function() { 
			if(!isBeforeUnload)
				localStorage.setItem('cyclomediaDisabled', "1");
		}    			
    	// alert('Popup blocker on your computer is preventing the opening of Cyclomedia. Please disable it to view Cyclomedia.');
    }
}
var sanbornWindow = null;
function loadSanborn(autoOpen) {
    var viewerName = clientSettings["SanbornViewerName"];
    var searchPart;
    if(!autoOpen)
    	localStorage.setItem('sanbornDisabled', 0);
    else if( sanbornWindow && sanbornWindow.closed ) {
    	localStorage.setItem('sanbornDisabled', "1");
    	return false;
    }
    else if( localStorage.getItem('sanbornDisabled') == "1" )
    	return false;   
    
    if (viewerName && viewerName.trim() != "") {
        var url = 'https://oblique.sanborn.com/' + viewerName + '/?'
        if (activeParcel.Center() && activeParcel.Center().lat() && activeParcel.Center().lng())
            url += 'll=' + activeParcel.Center().lat() + ',' + activeParcel.Center().lng();
        else if (activeParcel.StreetAddress != null || activeParcel.StreetAddress != "")
            url += 'addr=' + activeParcel.StreetAddress
        else {
            var parcelField = activeParcel.KeyValue3 || activeParcel.KeyValue1;
            url += 'id=' + parcelField
        }
        
        //var address = activeParcel.mapCenter ? activeParcel.mapCenter : [-73.935242, 40.730610];
        //var url='https://oblique.sanborn.com/'+viewerName+'/?ll='+address[0]+','+address[1];
        if (sanbornWindow && sanbornWindow.closed != true) sanbornWindow.location.href = url;
        //sanbornWindow.close();
        else
            sanbornWindow = openWindow(sanbornWindow, 'sanborn', url, { resizable: 'true', width: 1200, height: 700 });
        
        if (sanbornWindow) 
            sanbornWindow.focus();
    }
}
function loadcustomWMS(autoOpen) {
	if( autoOpen && localStorage.getItem('customWMSDisabled') == "1" ) { return false; }
    else { localStorage.setItem('customWMSDisabled', 0); }
    prevCustomWMS = localStorage.getItem('customWMSDisabled');
    
    customWMSpopup = openWindow(customWMSpopup, 'customWMS', '/protected/Svo/customwms.html', { resizable: 'true', width: 1200, height: 700 });
    if (!customWMSpopup) {
        alert('Popup blocker on your computer is preventing the opening of Custom Map. Please disable it to view Custom Map.');
    }
    else {
		customWMSpopup.onunload = function() { 
			if(!isBeforeUnload)
				localStorage.setItem('customWMSDisabled', "1");
		}    			
    } 
}

var evPopup, prevEv;
function loadEagleView(autoOpen) {
    if (autoOpen && localStorage.getItem('newEagleViewDisabled') == "1") { return false; }
    else { localStorage.setItem('newEagleViewDisabled', 0); }
    prevEv = localStorage.getItem('newEagleViewDisabled');

    evPopup = openWindow(evPopup, 'eagleView', '/protected/eagleview/Default.aspx', { resizable: 'true', width: 1200, height: 700 });
    if (!evPopup) {
        alert('Popup blocker on your computer is preventing the opening of EagleView. Please disable it to view EagleView.');
    } else {
        evPopup.onunload = function () {
            if (!isBeforeUnload)
                localStorage.setItem('newEagleViewDisabled', "1");
        }
    }
}

function loadWoolpert(autoOpen) {
	if( autoOpen && localStorage.getItem('woolpertDisabled') == "1" ) { return false; }
    else { localStorage.setItem('woolpertDisabled', 0); }
    prevWoolpert = localStorage.getItem('woolpertDisabled');
    
    woolpertpopup = openWindow(woolpertpopup, 'woolpert', '/protected/woolpert/wmap.aspx', { resizable: 'true', width: 1200, height: 700 });
    if (!woolpertpopup) {
        alert('Popup blocker on your computer is preventing the opening of Woolpert Imagery. Please disable it to view Woolpert Imagery.');
    }
    else {
		woolpertpopup.onunload = function() { 
			if(!isBeforeUnload)
				localStorage.setItem('woolpertDisabled', "1");
		}    			
    } 
}

function loadNearmap(autoOpen) {
	if( autoOpen && localStorage.getItem('nearmapDisabled') == "1" ) { return false; }
    else { localStorage.setItem('nearmapDisabled', 0); }
    prevNearmap = localStorage.getItem('nearmapDisabled');
    
    nearmappopup = openWindow(nearmappopup, 'nearmap', '/protected/nearmap/', { resizable: 'true', width: 1200, height: 700 });
    if (!nearmappopup) {
        alert('Popup blocker on your computer is preventing the opening of Nearmap. Please disable it to view Nearmap.');
    }
    else {
		nearmappopup.onunload = function() { 
			if(!isBeforeUnload)
				localStorage.setItem('nearmapDisabled', "1");
		}    			
    } 
}

function loadNearmapWMS(autoOpen) {
    if (autoOpen && localStorage.getItem('nearmapWMSDisabled') == "1") { return false; }
    else { localStorage.setItem('nearmapWMSDisabled', 0); }
    prevNearmapWMS = localStorage.getItem('nearmapWMSDisabled');

    nearmapWMSpopup = openWindow(nearmapWMSpopup, 'nearmapwms', '/protected/nearmapwms/', { resizable: 'true', width: 1200, height: 700 });
    if (!nearmapWMSpopup) {
        alert('Popup blocker on your computer is preventing the opening of WMS Nearmap. Please disable it to view WMS Nearmap.');
    }
    else {
        nearmapWMSpopup.onunload = function () {
            if (!isBeforeUnload)
                localStorage.setItem('nearmapWMSDisabled', "1");
        }
    }
}

function loadMapView() {
    mapWindow = openWindow(mapWindow, 'map', '/protected/map/map.html?21', { resizable: 'true', width: 1200, height: 700 });
}

function loadExtraMapOptions(type) {
    if (clientSettings["EagleView.APIKey"] && clientSettings["EagleView.APIKey"].trim() != "") {
        $('.btnpict').show();
        if (type != 'refresh')
            loadPictometry(true);
    }
    else $('.btnpict').hide();

    if (clientSettings["NewEV.ClientID"] && clientSettings["NewEV.EVClientSecret"].trim() != "") {
        $('.btnEv').show();
        if (type != 'refresh')
            loadEagleView(true);
    }
    else $('.btnEv').hide();

    if (clientSettings['EnableSanborn'] == "1") {
        $('.btnSanBorn').show();
        if (type != 'refresh')
            loadSanborn(true);
    }
    else $('.btnSanBorn').hide();
    if (clientSettings['EnableqPublic'] == "1" && clientSettings['qPublicKeyField'] && clientSettings['qPublicKeyField'].trim() != '') {
        $('.btnqPublic').show();
        if (type != 'refresh')
            loadQPublic( {sv: true, autoOpen: true} );
    }
    else $('.btnqPublic').hide();
    if (clientSettings['EnableCyclomedia'] == "1") {
        $('.btnCyclomedia').show();
        if (type != 'refresh')
            loadCyclomedia(true);
    }
    else $('.btnCyclomedia').hide();
    if (clientSettings['EnableNearmap'] && clientSettings['EnableNearmap'].includes('SV') && clientSettings["Nearmap.APIKey"] && clientSettings["Nearmap.APIKey"].trim() != '') {
        $('.btnNearmap').show();
        if (type != 'refresh')
            loadNearmap(true);
    }
    else $('.btnNearmap').hide();

	if (clientSettings['EnableNearmapWMS'] && clientSettings['EnableNearmapWMS'].includes('SV') && clientSettings["NearmapWMS.APIKey"] && clientSettings["NearmapWMS.APIKey"].trim() != '') {
        $('.btnNearmapWMS').show();
        if (type != 'refresh')
            loadNearmapWMS(true);
    }
    else $('.btnNearmapWMS').hide();
																																														 							
	if (clientSettings['customWMS']  && clientSettings['customWMS'].trim() != '' && clientSettings['EnableCustomWMS'] && clientSettings['EnableCustomWMS'].includes('SV')) {
        $('.btnCustomWMS').show();
        if (type != 'refresh')
            loadcustomWMS(true);
    }
    else $('.btnCustomWMS').hide();
    if (clientSettings['woolpertApikey']  && clientSettings['woolpertApikey'].trim() != '' && clientSettings['EnableWoolpert'] && clientSettings['EnableWoolpert'].includes('SV')) {
        $('.btnWoolpert').show();
        if (type != 'refresh')
            loadWoolpert(true);
    }
    else $('.btnWoolpert').hide();
}


function customFormatValue(val, fieldId) {
    var fvalue, yr, m, d, dt, fmt;
    fmt = (fieldId != null || fieldId != undefined) ? checkForCustomFormat(fieldId) : 'YYYYMMDD';
    if (val == '' || val == null || val == undefined) return val;
    if (fmt != '') {
        let v = ''; if (typeof (fmt) == 'object') { v = fmt.Value; fmt = fmt.Name; }
        switch (fmt) {
            case 'YYYYMMDD':
                fvalue = val.toString().trim();
                if (val && val != null) {
                    yr = fvalue.substring(0, 4);
                    m = fvalue.substring(4, 6);
                    d = fvalue.substring(6, 8);
                    dt = new Date(yr, m - 1, d);
                    var dtime = dt.getFullYear() + '-' + (dt.getMonth() + 1).toString().padLeft(2, '0') + '-' + dt.getDate().toString().padLeft(2, '0');
                    return dtime;
                }
            case 'Factor':
                if (v != '' && !isNaN(parseFloat(v))) {
                    let dc = (v.toString().indexOf('.') > -1) ? v.toString().split('.')[1].length : 0;
                    val = parseFloat(val) * parseFloat(v);
                    if (dc && val.toString().indexOf('.') > -1 && (dc > val.toString().split('.')[1].length)) val = val.toFixed(dc);
                }
                return val;
            default:
                return val;
        }
    }
    else
        return val;
}

function checkForCustomFormat(fieldid) {
    var fmt;
    if (datafields[fieldid].UIProperties)
        var o = JSON.parse(datafields[fieldid].UIProperties.replace(/'/g, '"'));
    else
        return '';
    if (o.CustomFormat && o.CustomFormat != '')
        fmt = o.CustomFormat;
    return ((fmt && fmt != '') ? fmt : '');
}

function deFormatvalue(value, fieldid) {
    var fmt;
    if (value == "" || value == null)
        return value;
    fmt = checkForCustomFormat(fieldid)
    if (fmt != '') {
        let v = ''; if (typeof (fmt) == 'object') { v = fmt.Value; fmt = fmt.Name; }
        switch (fmt) {
            case 'YYYYMMDD':
                val = value.split(" ")[0].split("-");
                return (val[0] + val[1] + val[2]);
            case 'Factor':
                if (v != '' && !isNaN(parseFloat(v))) {
                    value = parseFloat(value) / parseFloat(v);
                }
                return value;
            default:
                return value;
        }
    }
    else
        return value;

}

function activateAutoSave() {
    if (autoSaveEnabled)
        if (activeParcel && autoSaveCount < autoSaveBatch) {
            mapFrame.contentWindow.isSketchChanged = true;
            autoSaveCount++
            setTimeout(function () { $('#btnSaveRvw').trigger('click') }, 2000);
        }
}

function sortBySortExpression() {
    var cat = fieldCategories.filter(function (f) { return (f.SortExpression != null) });
    if (!cat) return;
    for (x in cat) {
        if (!cat[x].SortExpression || cat[x].SortExpression == '') continue;
        var source = cat[x].SourceTable;
        var sortexp = cat[x].SortExpression;
        var sourcedata = activeParcel[source];
        var sourcedataoriginal = activeParcel.Original[source];
        if (!sourcedata)
            continue;
        sortNull(sortexp, sourcedata, source, 1);
        //var sortScript = Sortscript(sortexp, source);
        
        var sortScript;
		if((sortexp) && ((sortexp.contains("CASE")) || (sortexp.contains("case")))){
			sortScript = SortscriptCASE(sortexp, source);
		}
		else{
			sortScript = Sortscript(sortexp, source);
		}
        
        if (sortScript != "") {
            eval('sourcedata' + sortScript);
            eval('sourcedataoriginal' + sortScript);
        }
        sortNull(sortexp, sourcedata, source, 0);
    }
}

function sortNull(sortExp, sourceData, sourceTable, set) {
    var SortItems = sortExp.trim().split(',');
    SortItems.forEach(function (Item) {
        var sortField = Item.trim().split(" ")[0];
        var scriptNullOrder = 99999999
        if (Item.replace(sortField, '').trim().toLowerCase() == "desc")
            scriptNullOrder = -99999999
        for (x in sourceData) {
            if (set == 1) {
                sourceData[x][sortField] = sourceData[x][sortField] != null ? sourceData[x][sortField] : scriptNullOrder;
                activeParcel.Original[sourceTable][x][sortField] = activeParcel.Original[sourceTable][x][sortField] ? activeParcel.Original[sourceTable][x][sortField] : scriptNullOrder;
            } else {
                sourceData[x][sortField] = sourceData[x][sortField] == scriptNullOrder ? null : sourceData[x][sortField];
                activeParcel.Original[sourceTable][x][sortField] = activeParcel.Original[sourceTable][x][sortField] == scriptNullOrder ? null : activeParcel.Original[sourceTable][x][sortField];
            }
        }
    });
}

function _showSketchDropDown(){
	if( sketchDropDownCount > 1 ) {
		$('#divSketchReview').scrollTop(0);
		if( $('.sketch-select').width() )
			$('#showSketchOptions').width( $('.sketch-select').width() );
		$('#showSketchOptions').show();
		setTimeout(function(){ $('#showSketchOptions').slideUp("slow"); }, 1500);
	}
	else
		$('#showSketchOptions').hide();
}

function _checkReviewdSketchs( callback, _nextParcel ) {	
	if( sketchDropDownChange == 0 || _nextParcel ) {
		if(callback) { callback(); return; }
	}
	else{
		var _droDownMessage = "";
		sketchDropDownChange.forEach(function(x){
			_droDownMessage = _droDownMessage + "\n{ " + x.Name + " }"
		})
		var _resValue = confirm('Are you sure you want to leave? The Sketch(s) - '+ _droDownMessage +' have not yet been reviewed. \nClick Cancel to stay on the screen and review the sketches.');
		if( _resValue ) {
			if(callback) { callback(); return; }
		}
		else{
			_showSketchDropDown();
			return false;
		}
	}
}

function enableLightMode() {
	$('.switch-input')[0].checked ? (lightWeight = true): (lightWeight = false);
	if( !(activeParcel && activeParcel.Id) ){
		showMask(); 
		setTimeout(function(){ hideMask(); }, 1500);
	}
	else{
		openParcel(activeParcel.Id, null, null, true);
	}
}

function _getChildCategories(categoryId) {
    var childs = [], retVal = [];
    childs = fieldCategories.filter(function (d) { return d.ParentCategoryId == categoryId && d.ShowCategory == 1; })
    childs = childs.sort(function(a,b) {return parseInt(a.Ordinal) - parseInt(b.Ordinal) ; })
    if (childs.length > 0) {
        for (x in childs) {
            retVal.push(childs[x].Id);
        }
    }
    return retVal;
}

function calculateChildRecords(field, index,rec) {
	var childCats = _getChildCategories(field.CategoryId)
	if (childCats.length == 0) return null;
	var value = 0;
	var calcExprSplit = field.CalculationExpression.trim().split(childRecCalcPattern)
	var record = rec? rec: activeParcel[field.SourceTable][index];
	var childTbls = childCats.map(function(ch){return fieldCategories[fieldCategories.findIndex(function(s){ return s.Id == ch})].SourceTable });
	var chTbl = calcExprSplit[0].split('.')[0];
	var chField = calcExprSplit[0].split('.')[1];
	if (childTbls.some(function(c){ return c.indexOf(chTbl) > -1 })) {
		if (record[chTbl]) {
			record = record[chTbl].filter(function(ch){ return (ch.CC_Deleted != 'true' && !ch.CC_Deleted); });
			record.forEach(function(data){
				value += parseFloat((!data[chField] || data[chField].toString().trim() == '')? 0: data[chField]);
			});
			if (calcExprSplit[1].toLowerCase() == 'avg' && record.length > 0) value = value/record.length;
			value = parseFloat(value.toFixed(2));
		}
	} else return null;
	return value;
}

function decodeHTML(encoded) {
    var div = document.createElement('div');
    div.innerHTML = encoded;
    var decoded = div.firstChild.nodeValue;
    return decoded;
}

var calculationExpressionSketch = function ( callback ) {
    saveData = '';
    var calcExpressionFields = datafieldsettings.filter(function (x) { return ( x.PropertyName == 'IsSketchCalcField' && x.Value == '1' ) });
    if (calcExpressionFields.length > 0) {
    	for ( var j = 0; j < calcExpressionFields.length; j++ ) {
    		var IsSketchCalcFieldId = calcExpressionFields[j].FieldId;
    		var IsSketchCalcField = Object.keys( datafields ).map( function ( x ) { return datafields[x] } ).filter(function (x) { return (x.Id == IsSketchCalcFieldId) })[0];
	    	
	    	if(IsSketchCalcField && IsSketchCalcField.CalculationExpression && IsSketchCalcField.CalculationExpression != '' ) {	
	    		var field = IsSketchCalcField, sourceTable = field.SourceTable, sourceData = activeParcel[sourceTable], fName = field.Name;
		        sourceData = sourceData? sourceData: [];
		        
		        try{
			        for( var i = 0; i < sourceData.length; i++) {
			            var source = sourceTable + '[' + i + ']', sData = activeParcel[sourceTable][i];
			            if( sData.CC_Deleted != true && sData.CC_Deleted != "true" ) {
				            var v = activeParcel.EvalValue(source, fName);
				            
				            if (field && field.CalculationExpression && field.CalculationExpression.trim() != '' && field.CalculationExpression.search(childRecCalcPattern) > -1) {
				                var tempVal = calculateChildRecords(field, i);
				                v = ( (tempVal == null)? v: tempVal );
				            }
				
				            if(field && field.CalculationExpression && sourceTable && (v || v === 0) && v.toString() != sData[fName]) 
				            	saveData += '^^^' + activeParcel.Id + '|' + sData.ROWUID + '|' + field.Id + '||' + v + '|edit|' + sData.ParentROWUID + '\n';
			            }
			        }
		        }
		        catch(err) {
	              console.log(err.message) ;
	        	}
        	}
    	}
    	
	}
		
	var saveRequestData = {
        ParcelId: activeParcel.Id,
        Priority: '',
        AlertMessage: '',
        Data: '',
        recoveryData: '',
        AppraisalType: '',
        activeParcelPhotoEditsdata: '',
        appType: 'SV'
    };
    
    if (saveData != '') {
    	console.log(saveData);
        saveRequestData.Data = saveData;
        $.ajax({
            url: '/quality/saveparcelchanges.jrq',
            type: "POST",
            dataType: 'json',
            data: saveRequestData,
            success: function (resp) {
                if (resp.status == "OK") {
                	getParcel( activeParcel.Id, function () {
                    	if (callback) callback();
                    });
                }
            }
        });
    }
    else {
        if (callback) callback();
        return;
    }
}

function provalResSorting(arr, order) {
    let field = order.split(" ")[0].trim();
    arr.sort((a, b) => {
        if (a[field] === 'B') {
            return -1;
        } else if (b[field] === 'B') {
            return 1;
        } else if (a[field] === 'L') {
            return -1;
        } else if (b[field] === 'L') {
            return 1;
        } else if (a[field] === 'C') {
            return -1;
        } else if (b[field] === 'C') {
            return 1;
        } else if (a.key === '' || a.key === null || a.key === undefined) {
            return 1;
        } else if (b.key === '' || b.key === null || a.key === undefined) {
            return -1;
        } else if (a[field] === 'A') {
            return 1;
        } else if (b[field] === 'A') {
            return -1;
        } else {
            const aVal = parseFloat(a[field]);
            const bVal = parseFloat(b[field]);
            return aVal - bVal;
        }
    });

    return arr;
}