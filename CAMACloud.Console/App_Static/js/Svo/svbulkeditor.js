﻿var flagStatuses = {
    1: "Field Inspection",
    2: "Data Entry",
    3: "Further Review",
    4: "Completed",
    5: "QC Passed",
    6: "Internal Question",
    7:"Reset"
}
var qcValue
var otherflags
var otherflagval
var pagefirstload = false;
var parcelcount;
var parcels;
var datafields = {};
var syncProgressList = [];
var columnDir = {};
var lookup = {}
var clientSettings = {};
var activeParcel;
var activeFieldChanges;
var activeTab;
var activeParcelEdits = [];
var activeParcelPhotoEdits = [];
var activeParcelAlert = [];
var activeParcelPriority = [];
var recoveredItems = [];
var activeParcelQC;
var priorityvalues = [];
var dashboardChanged = false;
var validations;
var dataSourceFields;
var Notificationmsg = "";
var clientSettings = {};
var sketchSettings = {};
var neighborhoods = {};
var listparcels = [];
var displayFields = [];
var relationcycle = [];
var TableHierarchical = [];
var FirstPhoto = "";
var SketchPreview = "";
var parentIndex;
var fIndx = [];
var classCalculatorValidationResult = [];
var childRecCalcPattern = /\\.|(sum|avg)|\(\)/i;
var showFutureData = false;
var isBPPParcel = false;
var showfrm = false;
var shwfrm = false;
var activeParcelAllData;
var tableListGlobal = [];
var disableSwitch = false;
var autoselectloop = [];
var LFields = [];
var tableListGlobal = [];
var _dB;
var extendedLookup = [];
var LookupMap = {};
var tableKeys = [];
var skvalue;

var divcounter = 1;
function clearSearch() {

    $('#lblCntParcl').html("0");

    var htmlCntnts = "<tr>";
    htmlCntnts += "<td colspan=\"3\" class=\"TdNodata\">No Data Available</td>";
    htmlCntnts += "</tr>";


    $('#TabSearchContents tbody tr').remove();
    $('#TabSearchContents tbody').append(htmlCntnts);
  

}

$('.hideSidePanel').click(function () {
    $('#divSearchBtm').removeClass('divSearchBtmtableContainer')
    hideLeftPanel();
});

function hideLeftPanel() {
    var w = $(window).width() - 5;
    var p = $('#divRghtCntnr').is(':visible');
    if (p) {
        var p1 = $(window).width() - $('#divSearchCntnr').width();
        $('.resultframe').width(p1 + 12);
        $('#mapFrame').width(p1 + 9);
    }
    else {
        $('#search-map').width(w);
    }
    $('#divSearchCntnr').hide();
    $('.showSidePanel').show();
};
$('.showSidePanel').click(function () {
    $('#divSearchBtm').addClass('divSearchBtmtableContainer')

    $('#divSearchCntnr').show();
    var fht = $(window).height(); var ht = $('#divtopHeader').height() + $('#divBottomFooter').height() + $('#divSearchTop').height();
    $('#divSearchBtm').height(fht - ht - 20);
    $('.showSidePanel').hide();
    var p = $('#divRghtCntnr').is(':visible');
    if (p) {

        var p2 = $(window).width() - ($('#divSearchCntnr').width() + $('#divRghtCntnr').width());
        $('.resultframe').width(p2 + 3);
        $('#mapFrame').width(p2);
    }
    else {
        var b = $(window).width() - $('#divSearchCntnr').width();
        $('#search-map').width(b);
    }
});

/* add ,close,clear filter click events  and functions*/
$('.addfilter, .addfltr').on('click', function (e) {
    e.preventDefault();
    $('#SrchId2').hide();
    $('#SrchId').show();
    //$('#SrchId').css('background-color', '#d6d6d6');
    //$('#SrchId').attr('disabled', 'false');
    //$('.Operator').prop('disabled', 'false');
    $('.Operator').val('select');
    openfltrwindow();


});
$('#closefilter').on('click', function () {

    $('#section1').hide();
    $('#divSearchTop1').show();
    $('#section2').show();
    $('.addclrfltr').show();
});
$('.clearfilter').on('click', function (e) {
    //$('#TabSearchContents tbody tr').remove();
    //$('#lblCntParcl').html("0");
    e.preventDefault()
    $('#divSearchTop1').empty();
    $('.addfilter').show();
    $('.clearfilter')[0].text = ' Clear Filter';
    divcounter = 1;
});
$('#addcstmfltr').on('click', function () {
    showMask();
    setTimeout(function () { appendFilters(); }, 100);

});
function enableOperators(ops) {//enable operators
    if (!ops) {
        $('.Operator').attr('disabled', 'disabled');
    } else if (ops.length) {
        $('.Operator').removeAttr('disabled');
        $('.Operator option').show();
        for (x in ops) {
            var op = ops[x];
            //$('.create-filter .operator option[value="' + op + '"]').attr('disabled', 'disabled');
            $('.Operator option[value="' + op + '"]').show();
        }
    }
}
function disableOperators(ops) { //disable operators
    if (!ops) {
        $('.Operator').attr('disabled', 'disabled');
    } else if (ops.length) {
        $('.Operator').removeAttr('disabled');
        $('.Operator option').show();
        for (x in ops) {
            var op = ops[x];
            //$('.create-filter .operator option[value="' + op + '"]').attr('disabled', 'disabled');
            $('.Operator option[value="' + op + '"]').hide();
        }
    }
}

$('.ddlUsersSV').on('change', function () {
    if ($(this).val() != "") {
        $('#SrchId').prop('type', 'text');
        $('#SrchId').css('width', '90%');
        var val = $('.ddlUsersSV').val().split('/');
        if (val[3] && datafields[val[3]] == undefined)
            location.reload();
        if (val[3] && datafields[val[3]] != undefined) {//to check if table id exixts
            if (val[2] == '') val[2] = 'VARCHAR';
            val[2] = val[2].toUpperCase();
            if (datafields[val[3]].LookupTable) {//if lookup table exists
                $('#SrchId').hide();
                $('#SrchId2').show();
                $('.Operator').val(' = ');
                $('.Operator').prop('disabled', 'disabled');
                copylookupfilter('#SrchId2', val[3]);
                $('#SrchId2').show();
                $('#SrchId2').addClass('#SrchId2');
            }
            if (val[2] && datafields[val[3]].LookupTable == null) {
                $('#SrchId').show();
                $('#SrchId2').hide();
                val[2] = val[2].toUpperCase();
                if (val[2] == "INT" || val[2].includes('NUMERIC') || val[2].includes('FLOAT')) {
                    $('.Operator').removeAttr('disabled');
                    enableOperators([" = ", " LIKE ", " <> ", "startwith", "notstrtwith", "endwith", " IS NOT NULL ", " IS NULL ", " > ", " >= ", " <= ", " < "]);
                    disableOperators([" LIKE "]);
                    $('.Operator').val(' = ');
                }
                if (val[2].includes('DATE') && datafields[val[3]].LookupTable == null) {
                    $('#SrchId').show();
                    $('#SrchId2').hide();
                    enableOperators([" = "]);
                    disableOperators([" > ", " >= ", " <= ", " < ", " LIKE ", " <> ", "startwith", "endwith", "notstrtwith", " IS NOT NULL ", " IS NULL "]);
                    $('.Operator').val(' = ');
                }
                else if (val[2].includes('VARCHAR') && datafields[val[3]].LookupTable == null) {
                    $('.Operator').removeAttr('disabled');
                    enableOperators([" = ", " LIKE ", " <> ", "startwith", "endwith", " IS NOT NULL ", " IS NULL ", "notstrtwith"]);
                    disableOperators([" > ", " >= ", " <= ", " < "]);
                    $('.Operator').val(' LIKE ');
                }
            }

        }
        else if (val[0] == 0 || val[0] == 1 || val[0] == 2) {
            if ($('.ddlUsersSV').val() == "1") {
                $('#SrchId').show();
                $('#SrchId2').hide();
                enableOperators([" = ", " LIKE ", " <> ", "startwith", "notstrtwith", "endwith", " IS NOT NULL ", " IS NULL "]);
                disableOperators([" > ", " >= ", " <= ", " < "]);
                $('.Operator').val(' = ');
            }
            if ($('.ddlUsersSV').val() == "0" || $('.ddlUsersSV').val() == "2") {
                $('#SrchId').show();
                $('#SrchId2').hide();
                $('.Operator').removeAttr('disabled');
                enableOperators([" = ", " LIKE ", " <> ", "startwith", "notstrtwith", "endwith", " IS NOT NULL ", " IS NULL "]);
                disableOperators([" > ", " >= ", " <= ", " < "]);
                $('.Operator').val(' LIKE ');
            }
        }
        else if (val[2]) {
            if (val[2] == null) val[2] = 'VARCHAR';
            val[2] = val[2].toUpperCase();
            if (val[0].includes('aggfield') && val[2]) { //for aggregate fields
                var aggvalidate = $('#MainContent_ddlUsersSV option:selected').text();
                aggvalidate = aggvalidate.split('_');
                if (aggvalidate[0] && aggvalidate[0] != 'FIRST' && aggvalidate[0] != 'LAST' && aggvalidate[0] != 'COUNT') {
                    $('#SrchId').prop('type', 'number');
                    $('#SrchId').css('width', '85%');
                }
                $('#SrchId').show();
                $('#SrchId2').hide();
                val[2] = val[2].toUpperCase();
                if (val[2] == "INT" || val[2].includes('NUMERIC') || val[2].includes('FLOAT')) {
                    $('.Operator').removeAttr('disabled');
                    enableOperators([" = ", " LIKE ", " <> ", "startwith", "notstrtwith", "endwith", " IS NOT NULL ", " IS NULL ", " > ", " >= ", " <= ", " < "]);
                    disableOperators([" LIKE "]);
                    $('.Operator').val(' = ');
                }
                if (val[2].includes('DATE')) {
                    $('#SrchId').show();
                    $('#SrchId2').hide();
                    enableOperators([" = "]);
                    disableOperators([" > ", " >= ", " <= ", " < ", " LIKE ", " <> ", "startwith", "endwith", "notstrtwith", " IS NOT NULL ", " IS NULL "]);
                    $('.Operator').val(' = ');
                }
                else if (val[2].includes('VARCHAR')) {
                    $('.Operator').removeAttr('disabled');
                    enableOperators([" = ", " LIKE ", " <> ", "startwith", "endwith", " IS NOT NULL ", " IS NULL ", "notstrtwith"]);
                    disableOperators([" > ", " >= ", " <= ", " < "]);
                    $('.Operator').val(' LIKE ');
                }
            }

        }

        else {
            $('.Operator').prop('disabled', 'true');
        }

        //$('.Operator').removeAttr('disabled');
        $('#SrchId').css('background-color', '#ffffff');
        $('#SrchId').removeAttr('disabled');
        var Optns = $(this).val().split('/');
        var fieldDataTypes = Optns[1];
        $('#SrchId').val('');
        if (fieldDataTypes == 4 && $('#SrchId').prop('type') == 'text') {
            if (Option[0] = 'SketchReviewedDate') {
                $('#SrchId').show();
                $('#SrchId2').hide();
                enableOperators([" = "]);
                disableOperators([" > ", " >= ", " <= ", " < ", " LIKE ", " <> ", "startwith", "endwith", "notstrtwith", " IS NOT NULL ", " IS NULL "]);
                $('.Operator').val(' = ');
            }
            $('#SrchId').prop('type', 'date');
        }
        else if (fieldDataTypes != 4 && $('#SrchId').prop('type') == 'date') {
            $('#SrchId').prop('type', 'text');
        }
    } else {
        if ($('#SrchId').prop('type') == 'date') {
            $('#SrchId').prop('type', 'text');
        }
        $('#SrchId').val('');
        $('#SrchId').show();
        $('#SrchId2').hide();
        $('#SrchId').css('background-color', '#d6d6d6');
        $('#SrchId').attr('disabled', 'disabled');
        $('.Operator').prop('disabled', 'true');
        $('.Operator').val('select');

    }
});


function validateData(optns, sval, qcontn1) {
    var fieldName = "";
    var fieldValue = "";
    if (optns == undefined) optns = "0";
    if (optns == "0" || optns == "1" || optns == "2")
        var Optn = optns;
    else
        var Optn = optns.split('/'); // To retrieve datatype of field along with fieldname.
    var srchOptn = Optn[0].trim();
    var fieldDataType = Optn[1];
    var schemaType = Optn[2];
    var cntnOprtr = qcontn1;

    if (schemaType) schemaType = schemaType.toUpperCase();



    if (fieldDataType != undefined) {
        var numericlength = Optn[2].split('(')[1];
    }

    if (srchOptn) {
        fieldName = srchOptn;
        fieldValue = sval;
        if (fieldValue == undefined) fieldValue = "0";
        var val = optns.split('/');
        if (val[3]) {
            if (datafields[val[3]].LookupTable)
                fieldValue = sval;
        }
        var str = fieldValue;
    }

    if (schemaType != undefined) {
        if (schemaType.includes('INT') && isNaN(fieldValue)) {
            clearAlert();
            return false;
        }
        else if (schemaType.includes('INT') && (fieldValue.includes('.'))) {
            clearAlert();
            return false;
        }
    }

    if ((fieldName == 0 || fieldName == 1 || fieldName == 2) && (fieldValue.includes('%'))) {
        alert('Invalid Special character')
        clearSearch()
        closeParcel();
        $('.masklayer').hide();
        return false;
    }

    if (fieldDataType == 1 || fieldDataType == 2 || fieldDataType == 3 || fieldDataType == 5 || fieldDataType == 6 || fieldDataType == 7 || fieldDataType == 8 || fieldDataType == 9 || fieldDataType == 10 || fieldDataType == 11 || fieldDataType == 12 || fieldDataType == 13) {
        var pattern = new RegExp(QC.dataTypes[fieldDataType].pattern);
        if (cntnOprtr == ' IS NULL ' || cntnOprtr == ' IS NOT NULL ')
            fieldValue = '1';
        var validate1 = (pattern.test(fieldValue));
        if (!validate1 && cntnOprtr != ' IS NULL ' && cntnOprtr != ' IS NOT NULL ') {
            clearAlert();
            return false;
        }
    }

    if (fieldDataType != undefined) {
        if (schemaType.includes('numeric')) {
            var pattern = /^([a-zA-Z]*$)|[ !@#$%^&*()_+\-=\[\]{};':"\\|,<>\/?]/;
            var validate = (pattern.test(fieldValue));
            if (validate || str.length > numericlength) {
                clearAlert();
                $('.masklayer').hide();
                return false;
            }
        } else if (schemaType.includes('BIT')) {
            if (!(fieldValue == 0 || fieldValue == 1)) {
                clearAlert();
                $('.masklayer').hide();
                return false;
            }

        }


    }

    var flag = $('.SrchFlag').val();
    var user = $(".ddlUsers").val();

    if (fieldDataType == 4 && schemaType.includes('DATE')) {
        //var regex = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[13-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;
        //var regex1 = /^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$/;
        //var regex = new RegExp("^((0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2})*$");
        var regex = (schemaType == 'DATETIME') ? /((\b1[8-9]\d{2}|\b2\d{3})[-/.](0[1-9]|1[0-2])[-/.](0[1-9]|[12]\d|3[01]))/ : /(\b[12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/;
        var validdate = (regex.test(fieldValue));
        //var leapyrvalidate = (regex1.test(fieldValue));
        if (!validdate) {
            alert("Please enter a valid Date Format")
            clearSearch()
            closeParcel();
            $('.masklayer').hide();
            return false;
        }
    }

    if (!(user != "" || flag != "" || fieldName != "") || (fieldName != "" && fieldValue == "" && cntnOprtr != " IS NOT NULL " && cntnOprtr != " IS NULL ")) {
        var alrtMsg = 'You need to provide the Search options';
        if (fieldName != "" && fieldValue == "") {
            alrtMsg = 'You need to provide either the ID or Value to search.';
            if (fieldName == 1) alrtMsg = 'You need to provide the Assignment Group to search.';
            else if (fieldName == 2) alrtMsg = 'You need to provide the CC Tag to search.';
            clearSearch();
            closeParcel();
            $('.masklayer').hide();
            $('#divRghtCntnr').hide('fast');
            $('#divFootCntnr').hide();
        }
        alert(alrtMsg);
        $('.masklayer').hide();
        window.location.hash = "";
        return false;
    }
}

function appendFilters() {
    if (($('.ddlUsersSV').val()) == "") {
        alert("You have to enter a search condition");
        hideMask();
        return
    }
    showMask();
    var val = $('.ddlUsersSV').val().split('/');
    var remindex = $('.addfltr').last();
    $(remindex).hide();
    var reqfield = $('#MainContent_ddlUsersSV option:selected').text();
    var filtercondition = $('#MainContent_Operator option:selected').text();
    if (filtercondition == '--Select--') {
        alert('You have to select a valid Filter condition');
        hideMask();
        return
    }
    var qfield = $('#MainContent_ddlUsersSV').val();
    var qcontn = $('#MainContent_Operator').val();
    $("<div />", { "class": "filterdiv", "id": "filterdiv" + divcounter })
        .append($("<h2 />", { "class": "filtercntnt", "id": "filtercntnt" + divcounter, "qfield": qfield, "qcontn": qcontn }).html(reqfield + " " + filtercondition))
        .appendTo("#divSearchTop1");
    if (($('#SrchId').is(':visible'))) {
        $('#filterdiv' + divcounter)
            .append($("<input />", { "id": "fltrinpsrch" + divcounter, "type": "text", "autocorrect": "off", "autocapitalise": "off", "maxlength": "50", "class": "fltrinpsrch1" }))
            .appendTo("#divSearchTop1");
        $('#fltrinpsrch' + divcounter).val($('#SrchId').val());
        if (qcontn == ' IS NOT NULL ' || qcontn == ' IS NULL ') {
            $('#fltrinpsrch' + divcounter).attr('disabled', 'disabled');
            $('#fltrinpsrch' + divcounter).css('background-color', '#d6d6d6');
        }
        if (val[2] != undefined && val[2].includes('DATE') && datafields[val[3]].LookupTable == null) {
            $('#fltrinpsrch' + divcounter).prop('type', 'date');
        }
     
    }
    if (($('#SrchId2').is(':visible'))) {
        $('#filterdiv' + divcounter)
            .append($("<select />", { "id": "fltrselinp" + divcounter, "class": "fltrinpsrch1" }))
            .appendTo("#divSearchTop1");
        copylookupfilter("#fltrselinp" + divcounter, val[3]);
        $("#fltrselinp" + divcounter).val($('#SrchId2').val());
    }
    $('#filterdiv' + divcounter)
        .append($("<span />", { "class": "deletefltr", "id": "deletefltr" + divcounter }).html("-")
            .click(function () {
                if (($('.filterdiv').length) == 1) {
                    $('.addfilter').show();
                    //$('#TabSearchContents tbody tr').remove();
                    //$('#lblCntParcl').html("0");
                }
                if (($('.filterdiv').length) == 2) {
                    $('.clearfilter')[0].text = ' Clear Filter'
                }
                jQuery(this).closest("div").remove()
                var remindex1 = $('.addfltr').last();
                $('.addfilter').show();
                $(remindex1).show();
            })
        )
        .append($("<span />", { "class": "addfltr", "id": "addfltr" + divcounter }).html("+")
            .click(function () {
                openfltrwindow();
            })
        )
        .appendTo("#divSearchTop1");
    divcounter++;
    if (($('.filterdiv').length) == 5) {
        $('.addfltr').hide();
        $('.addfilter').hide();
    }
    if (($('.filterdiv').length) > 1) {
        $('.clearfilter')[0].text = ' Clear Filters';
    }

    generatefetchquery();

    $('#section1').hide();
    $('#section2').show();
    $('.addclrfltr').show();
    $('#divSearchTop1').show();
    hideMask();
}
function openfltrwindow() {
    $('#MainContent_ddlUsersSV').val("selected");
    $('#SrchId2').hide();
    $('#SrchId').show();
    $('#SrchId').prop('type', 'text');
/*    $('#SrchId').attr('disabled', 'false');*/
    $('#SrchId').val('');
    //$('.Operator').prop('disabled', 'false');
    //$('.Operator').prop('disabled', 'false');
    //$('.Operator').val('select');
    $('#section1').show();
    $('#section2').hide();
    $('.addclrfltr').hide();
    $('#divSearchTop1').hide();
}



/* add filter click event and functions*/

/*search click event*/
$('#btnSearchSvBlk').click(function (e) {
    bulkedit=0
    e.preventDefault()
    showMask();
    setTimeout(function () { generatefetchquery(); }, 100);
    searchstrng = '';
    return false;
});
/*search click event*/

/* refresh btn */

$('#btnRefresh').click(function (e) {
    e.preventDefault();
    showMask();
    $('#rblStatus input:checked').prop('checked', false);
    $('#rblQc input:checked').prop('checked', false);
    hideMask();
});
/* refresh btn */

/*bulk edit click event and function*/
$('#bulkSaveBtnSV').click(function (e) {
    e.preventDefault()
    bulkedit = 1
    _saveSVBulk();
    searchstrng = '';
    return false;
})
$('#btnClear').click(function () {
    svPromptAction(() => {
        var b = $(window).width() - $('#divSearchCntnr').width();
        $('#search-map').width(b);
        $('.src-field').val('');
        $('#divRghtCntnr').hide('fast');
        clearSearch();
        window.location.hash = "";
    }, (!activeParcel ? true : false));
    return false;
});

function _saveSVBulk() {
     skvalue = $('#rblStatus input:checked').val();
    qcValue = $('#rblQc input:checked').val();
    if (qcValue && !skvalue) {
        return
    }
    if (!skvalue &&  !qcValue) {
        alert("Validation Cannot be Empty!!");
    } else {
        let userConfirmed = confirm("This will update the Sketch Validation Flag for " + parcels[0].ActualResCount  +"  parcels in the search results.Would You Like to Proceed?");
        if (userConfirmed) {
            showMask();
            bulkedit = 1
            setTimeout(function () { generatefetchquery(skvalue); }, 100);
            searchstrng = '';
        } else {
            $('#rblStatus input:checked').prop('checked', false);
            $('#rblQc input:checked').prop('checked', false);

        }
    }
    return false;
}


/*bulk edit click event and function end*/

function isTrue(v) {
    return v == "true" || v == true;
}
function SortSearchResults(Header, firstSort) {
    var FnString = "parcels.sort(function(a, b) {var reA = /[^a-zA-Z]/g;var reN = /[^0-9]/g;"
    var descString = "var aA = nameA.replace(reA, '');var bA = nameB.replace(reA, '');if (aA === bA) {var aN = parseInt(nameA.replace(reN, ''), 10);var bN = parseInt(nameB.replace(reN, ''), 10); aN =isNaN(aN)?0:aN; bN=isNaN(bN)?0:bN; return aN === bN ? 0 : aN < bN ? 1 : -1;} else {return aA < bA ? 1 : -1;}})"
    var ascString = "var aA = nameA.replace(reA, '');var bA = nameB.replace(reA, '');if (aA === bA) {var aN = parseInt(nameA.replace(reN, ''), 10);var bN = parseInt(nameB.replace(reN, ''), 10); aN =isNaN(aN)?0:aN; bN=isNaN(bN)?0:bN; return aN === bN ? 0 : aN > bN ? 1 : -1;} else {return aA > bA ? 1 : -1;}});"
    if (firstSort) ascParcel = false
    if (Header == "Parcel Id") {
        FnString = FnString + "var nameA = a.KeyValue1.toUpperCase(); var nameB = b.KeyValue1.toUpperCase();"
        if (ascParcel) FnString = FnString + descString //sorts descending order
        else FnString = FnString + ascString;
        ascParcel = !ascParcel;
    }
    else if (Header == "Address") {
        FnString = FnString + "var nameA = a.StreetAddress?a.StreetAddress.toUpperCase():''; var nameB = b.StreetAddress?b.StreetAddress.toUpperCase():'';"
        if (ascAddress) FnString = FnString + descString //sorts descending order
        else FnString = FnString + ascString;
        ascAddress = !ascAddress;
    }
    var svSort = new Function(FnString)();
    if (firstSort) return
    var curActive = activeParcel ? activeParcel.Id : null;
    showpagination();


}

function getLookupData(field, options, callback) {
    var data = [];
    var cat = options.category;
    if (cat == null)
        cat = field.Category;
    if (field.IsLargeLookup == 'true' || field.IsLargeLookup == true) {
        if (callback) callback([]);
        return;
    }
    if (field.SourceTable == "_photo_meta_data") {
        if (callback) callback([]);
        return;
    }
    var specialType = false;
    if (options.dataSource == 'ccma.YesNo') {
        if (callback) callback(ccma.YesNo);
    } else {
        if (field.LookupTable == '$QUERY$') {
            var query = field.LookupQuery || '';
            if (lookup['#FIELD-' + field.Id] == undefined) {
                var fullQuery = query || '';
                if (query.search(/\{[a-zA-Z0-9_.]*\}/i) > 0) {
                    if (query.search(/where/i) > 0) {
                        if (query.search(/union/i) > 0) {
                            var unionSplit = query.split(/union/i)
                            unionSplit.forEach(function (each, index) { if (unionSplit[index].search(/where/i) > 0) unionSplit[index] = unionSplit[index].substr(0, unionSplit[index].search(/where/i)) });
                            fullQuery = unionSplit.join('union');
                        }
                        else
                            fullQuery = query.substr(0, query.search(/where/i));
                    }
                }
                if (fullQuery && fullQuery.search(/\{.*?\}/g) == -1 || (field.LookupQueryForCache && field.LookupQueryForCache != '')) {
                    if (field.LookupQueryFilter) extendedLookup['#FIELD-' + field.Id] = []
                    if (field.LookupQueryForCache && field.LookupQueryForCache != '') fullQuery = field.LookupQueryForCache;
                    getData(fullQuery, [], function (ld) {
                        if (ld.length > 0) {
                            var f = field.Name;
                            lookup[f] = {};
                            var props = Object.keys(ld[0]);
                            var IdField = props[0]; var NameField = props[1]; var DescField = props[2]
                            if (!NameField) NameField = IdField;
                            if (!DescField) DescField = NameField;
                            for (var x in ld) {
                                var d = ld[x];
                                var v = d[IdField];
                                if (v) v = v.toString().trim();
                                if (lookup[f][v] === undefined)
                                    lookup[f][v] = { Name: d[NameField], Description: d[DescField], Ordinal: x, SortType: null, Id: v, Object: d };
                                if (field.LookupQueryFilter) {
                                    d.Ordinal = x;
                                    d.Name = d[NameField];
                                    d.Description = d[DescField];
                                    d.Id = v;
                                    extendedLookup['#FIELD-' + field.Id].push(d)
                                }
                            }
                            lookup['#FIELD-' + field.Id] = lookup[f];
                        }
                    });
                } else specialType = true;

            }
            var o;
            if (cat && cat.TabularData == "true") {

            } else {
                o = activeParcel;
            }
            if (options.source)
                o = options.source;
            if (o) {
                if (query.search(/\{.*?\}/g) > -1) {
                    query.match(/\{.*?\}/g).forEach(function (fn) {
                        var testFieldName = fn.replace('{', '').replace('}', '');
                        if (testFieldName.indexOf('parent.') > -1) {
                            var fieldName = testFieldName.split('parent.');
                            lookupField = fieldName[fieldName.length - 1];
                            source = o;
                            while (testFieldName.search('parent.') > -1) {
                                source = source["parentRecord"] ? (source["parentRecord"].constructor == Array ? source["parentRecord"][0] : source["parentRecord"]) : []
                                testFieldName = testFieldName.replace('parent.' + lookupField, lookupField);
                            }
                            //source = o["parentRecord"] ? o["parentRecord"] : [];
                            //lookupField = testFieldName.split('.')[1];
                            //if (testFieldName.indexOf('parent.parent') > -1) {
                            //    source = source["parentRecord"] ? source["parentRecord"] : [];
                            //    lookupField = testFieldName.split('.')[2];
                            //}
                        }
                        else if (testFieldName.indexOf('parcel.') > -1) {
                            source = activeParcel;
                            lookupField = testFieldName.split('parcel.')[1];
                        }
                        else { lookupField = testFieldName.split('.')[0]; source = o };
                        value = source[lookupField];
                        if (value && typeof (value) == "string") value = value.replace(/'/g, "''");
                        if (value != 0 && !value && value != '') value = 'null';
                        if (('ImagePath' in source) && ('Id' in source)) {
                            var imgfield = getDataField(lookupField, '_photo_meta_data');
                            var valueField = '';
                            if (imgfield) {
                                //                                valueField = imgfield.AssignedName.replace('PhotoMetaField', 'MetaData');
                                //                                value = o[valueField];
                                //                                if ((value == null || value === undefined || value == '') && imgfield.DefaultValue) value = imgfield.DefaultValue;
                                valueField = imgfield.AssignedName.replace('PhotoMetaField', 'MetaData');
                                value = source[valueField];
                            }

                            if (testFieldName.split('.') > 0) {
                                var attrName = testFieldName.split('.')[1];
                                var lk = getFieldLookup(lookupField);
                                if (lk) {
                                    if (lk[value]) {
                                        if (lk[value].Object) {
                                            value = lk[value].Object[attrName];
                                        }
                                    }
                                }
                            }

                        }

                        query = query.replace(fn, value);

                    });
                }
            }
            if (query) {
                if (query.search(/\{.*?\}/g) == -1) {
                    getData(query, [], function (ld) {
                        if (ld.length > 0) {
                            var f = field.Name;
                            var props = Object.keys(ld[0]);
                            var IdField = props[0]; var NameField = props[1]; var DescField = props[2]
                            if (!NameField) NameField = IdField;
                            for (var x in ld) {
                                if (specialType) lookup[f] = {};
                                var d = ld[x];
                                var v = d[IdField];
                                if (v === 'null') { v = ''; d[IdField] = ''; }
                                var name = d[NameField];
                                if (name) name = name.toString().trim();
                                //if (v && v.trim) {
                                //    v = v.trim();
                                //}
                                if (!name) name = v;
                                data.push({ Name: name, Description: d[DescField], Ordinal: x, SortType: null, Id: v });
                                if (specialType) {
                                    if (lookup[f][v] === undefined)
                                        lookup[f][v] = { Name: d[NameField], Description: d[DescField], Ordinal: x, SortType: null, Id: v, Object: d };
                                }
                            }
                        }
                        if (options.addNoValue) {
                            data.unshift({ Name: '-- No Value --', Id: '' });
                        } else if (isTrue(field.LookupShowNullValue)) {
                            data.unshift({ Id: '', Name: '-- No Value --' });
                        } else {
                            data.unshift({ Name: '', Id: '' });
                        }
                        if (callback) callback(data, options.source, options);
                    }, function () {
                        if (callback) callback([]);
                    });
                } else {
                    if (callback) callback([]);
                }
            }
            else {
                if (callback) callback([]);
            }
        } else {
            var l = lookup[field.LookupTable]
            for (x in l) {
                var Idfield = x;
                // if (field.IsClassCalculatorAttribute == 'true')
                // Idfield = l[x].NumericValue || 0
                data.push({
                    Id: x,
                    Name: l[x].Name,
                    Description: l[x].Description,
                    Ordinal: l[x].Ordinal,
                    SortType: (l[x].SortType && !isNaN(l[x].SortType)) ? parseInt(l[x].SortType) : null
                })
            }
            if (options.addNoValue) {
                data.unshift({ Name: 'NO VALUE', Id: '' });
            }
            if (isTrue(field.LookupShowNullValue)) {
                data.unshift({ Name: '', Value: '<blank>' });
            }
            if (callback) callback(data.sort(function (x, y) { return x.Ordinal ? x.Ordinal - y.Ordinal : x.Id > y.Id ? 1 : -1 }), options.source, options);
        }
    }
}
function getData(query, params, callback, errorCallback, otherData) {
    if (params == null)
        params = [];
    var data = [];
    if (query) {
        _dB.transaction(function (x) {
            x.executeSql(query, params, function (x, results) {
                var len = results.rows.length, i;
                for (i = 0; i < len; i++) {
                    data.push(results.rows.item(i));
                }
                if (callback) callback(data, results, otherData);
            }, function (x, e) {
                if (errorCallback) errorCallback();
                console.warn(e.message);
                console.warn(query);

            })
        });
    }
}

function loadEssentialTables(callback) {
    CachelookupTable(function () {
        $.ajax({
            url: '/sv/loadtables.jrq',
            data: {},
            dataType: 'json',
            success: function (resp) {
                var lv = resp.lookup
                for (var x in lv) {
                    var f = lv[x].Source;
                    var v = lv[x].Value.trim();
                    var n = lv[x].Name;
                    var d = lv[x].Description;
                    var o = parseInt(lv[x].Ordinal);
                    var t = lv[x].SortType;
                    if (lookup[f] === undefined) {
                        lookup[f] = {};
                    }
                    var color = lv[x].Color ? lv[x].Color : '';
                    var NumericValue = lv[x].NumericValue;
                    if (lookup[f][v] === undefined)
                        lookup[f][v] = { Name: n, Description: d, Ordinal: o, SortType: t, Id: v, color: color, NumericValue: NumericValue, AdditionalValue1: lv[x].AdditionalValue1, AdditionalValue2: lv[x].AdditionalValue2 };
                }
                fieldCategories = resp.fieldCategories;
                fieldCategories = (fieldCategories && !_.isEmpty(fieldCategories)) ? fieldCategories : [];
                parentChild = resp.parentChild;
                fields = resp.fields;
                datafieldsettings = resp.datafieldsettings;
                for (var x in resp.fields) {
                    var n = resp.fields[x];
                    for (var f in n) {
                        if (n[f] == 'true') n[f] = true;
                        if (n[f] == 'false') n[f] = false;
                    }
                    datafields[n.Id.toString()] = n;
                }
                for (var x in resp.clientSettings) {
                    var n = resp.clientSettings[x].Name;
                    var v = resp.clientSettings[x].Value;
                    clientSettings[n.toString()] = v;
                }
                for (var x in resp.sketchSettings) {
                    var n = resp.sketchSettings[x].Name;
                    var v = resp.sketchSettings[x].Value;
                    sketchSettings[n.toString()] = v;
                }
                for (var x in resp.tableKeys) {
                    var v = resp.tableKeys[x].Name;
                    var n = resp.tableKeys[x].SourceTable;
                    var temp = { Name: v, SourceTable: n };
                    tableKeys.push(temp);
                }
                var _clientSketchSettingsSkconfig = clientSettings['SketchConfig'] || sketchSettings['SketchConfig'];
                if (_clientSketchSettingsSkconfig == 'MVP') {
                    MVP_Lookup = resp.sk_Lookup;
                    MVP_Outbuliding_Lookup = resp.Outbuliding_Lookup;
                    MVP_Default_Exterior_Cover = resp.Default_Exterior_Cover;
                    MVP_sketch_codes = resp.sketch_codes;
                    MVP_sketch_labels = resp.sketch_labels;
                }
                if (_clientSketchSettingsSkconfig == 'ProValNew' || _clientSketchSettingsSkconfig == 'ProValNewFYL') {
                    Proval_Lookup = resp.sk_Lookup;
                    Proval_Outbuliding_Lookup = resp.Outbuliding_Lookup;
                    Proval_Default_Exterior_Cover = resp.Default_Exterior_Cover;
                    Proval_sketch_codes = resp.sketch_codes;
                }
                if (_clientSketchSettingsSkconfig == 'FTC' || _clientSketchSettingsSkconfig == 'ApexJson') {
                    FTC_Detail_Lookup = resp.detail_Lookup;
                    FTC_AddOns_Lookup = resp.addons_Lookup;
                    FTC_Floor_Lookup = resp.floor_lookup;
                }
                if (_clientSketchSettingsSkconfig == 'Lafayette') {
                    Lafayette_Lookup = resp.lafayette_Lookup;
                }
                if (_clientSketchSettingsSkconfig == 'Patriot') {
                    Patriot_Lookup = resp.patriot_Lookup;
                }
                if (_clientSketchSettingsSkconfig == 'FarragutJson') {
                    Farragut_Lookup = resp.farragut_Lookup;
                }
                if (_clientSketchSettingsSkconfig == 'SigmaCuyahoga_Lookup') {
                    SigmaCuyahoga_Lookup = resp.sigmaCuyahoga_Lookup;
                }
                if (datafields) {
                    Object.keys(datafields).map(function (x) { return datafields[x] }).forEach(function (x, i) {
                        if (x.LookupTable == "$QUERY$") {
                            getLookupData(x, {}, function () { });
                        }
                        if (x.InputType == 5 || x.InputType == 11) {
                            LookupMap[x.Name] = { LookupTable: x.LookupTable, IdMap: false };
                            LookupMap['#FIELD-' + x.Id] = { LookupTable: x.LookupTable, IdMap: true }
                        }
                    })
                }
                if (callback) callback()
            },
            error: function (resp) { }
        });
    });
}

function CachelookupTable(callback) {
    if (typeof (openDatabase) == 'function') {
        _dB = openDatabase('CAMACLOUD', '1.0', 'CAMACloud', 500 * 1024 * 1024);
    }
    else {
        _dB = new SQL.Database();
        _dB.transaction = function (callback) {
            var tx = {
                executeSql: function (query, params, success, failure) {
                    var _tx = this;
                    var resp = {
                        rows: {
                            length: 0,
                            item: function (i) {
                                return this[i];
                            }
                        }
                    };
                    try {
                        if (params?.length == 0) {
                            var raw = _dB.exec(query)[0];
                            if (raw) {
                                var values = raw.values;
                                resp = {
                                    rows: {
                                        length: values.length,
                                        item: function (i) {
                                            return this[i];
                                        }
                                    }
                                };

                                for (var i = 0; i < values.length; i++) {
                                    resp.rows[i] = {};
                                    var k = 0;
                                    for (var c of raw.columns) {
                                        resp.rows[i][c] = values[i][k];
                                        k++;
                                    }
                                }

                            }
                        }
                        else {
                            _dB.run(query, params);
                        }
                    }
                    catch (ex) {
                        failure && failure(_tx, ex);
                        return;
                    }

                    success && success(_tx, resp);
                }
            }

            callback && callback(tx);
        }
    }
    $.ajax({
        url: '/sv/lookupdatatables.jrq',
        data: {},
        dataType: 'json',
        success: function (data) {
            var tableName = '*ld*';
            if ((data && data.length != undefined)) {
                _syncSubTables(tableName, data, function () {
                    if (callback)
                        callback();
                });
            }

        },
        error: function (req, err, msg) {
            alert('Data synchronization failed!');
        }
    });
}

function _syncSubTables(tableName, data, callback) {
    if (data.length == 0) {
        //log('No data downloaded as sub tables.');
        if (callback) callback();
    }
    var filtered = {};
    for (x in data) {
        var item = data[x];
        var dsName = item.DataSourceName;
        if (filtered[dsName] == null) {
            filtered[dsName] = [];
        }
        filtered[dsName].push(item);
    }
    _syncSubTableFromList(filtered, callback);
}
function getDataField(key, table) {
    var res = Object.keys(datafields).filter(function (x) { return table ? ((datafields[x].Name == key) && (datafields[x].SourceTable == table)) : ((datafields[x].Name == key) && (datafields[x].CategoryId || (datafields[x].QuickReview == "true")) && datafields[x].SourceTable == null) }).map(function (x) { return datafields[x] });
    if (!table && res.length == 0) {
        res = Object.keys(datafields).filter(function (x) { return (datafields[x].Name == key) }).map(function (x) { return datafields[x] });
    }
    if (res.length > 0)
        return res[0];
    else
        return null;
}

function _getColumnInformation(firstRow, callBack, tableName) {
    var cKeys = [];
    var columns = "";
    var createColumns = "";
    var phs = "";
    for (key in firstRow) {
        var df = getDataField(key, tableName);

        cKeys.push(key);
        var type = "TEXT"

        //        if (key == "Index") {
        //            key = "SortIndex"
        //            type = "INTEGER";
        //        }
        if (key == "Id" || /^.*?Id$/.test(key) || /^.*?_id$/.test(key) || /^.*?UID$/.test(key)) {
            type = "INTEGER"
        }
        if (key == "Synced") {
            type = "INTEGER"
        }
        if (firstRow[key] != null) {
            if ((firstRow[key].toString() == 'true') || (firstRow[key].toString() == 'false')) {
                type = "BOOL";
            }
        }
        if (df) {
            switch (parseInt(df.InputType)) {
                case 1: type = "TEXT"; break;
                case 2: type = "FLOAT"; break;
                case 3: type = "BOOL"; break;
                case 4: type = "DATETIME"; break;
                case 7: type = "FLOAT"; break;
                case 8: type = "INT"; break;
                case 9: type = "INT"; break;
                case 10: type = "INT"; break;
                default: type = "TEXT"; break;
            }
        }
        if (columns == "") {
            columns = key;
            createColumns = "[" + key + "]" + " " + type;
            phs = "?";
        }
        else {
            columns += ", " + key;
            createColumns += ", " + "[" + key + "]" + " " + type;
            phs += ", ?"
        }
    }

    var r = new Object();
    r.Columns = columns;
    r.CreateColumns = createColumns;
    r.PlaceHolders = phs;
    r.ColumnKeys = cKeys;

    if (callBack) callBack(r);
}

function executeSql(tx, query, params, success, failure) {
    tx.executeSql(query, params, function (x, results) { if (success) success(x, results) }, function (x, e) { if (failure) failure(x, e); else console.error(query, e.message) });
}
function _parseDataForInsert(parcel, firstRow, cinfo) {
    var pa = [];
    for (key in parcel) {
        if (cinfo.ColumnKeys.indexOf(key) > -1) {
            var dat;
            dat = parcel[key];
            if (TrueFalseInPCI == "False") {
                if (dat == "True" || dat == "true") dat = true;
                if (dat == "False" || dat == 'false') dat = false;
            }
            if (key == "Index") {
                dat = parseInt(dat);
            }
            dat = dat === null ? dat : dat.toString();
            pa.push(dat);
        }
    }
    return pa;
}


function _syncSubTableFromList(filtered, callback) {
    if (Object.keys(filtered).length == 0) {
        if (callback) callback();
    }
    for (var key in filtered) {
        _syncTable(key, filtered[key], function () {
            delete filtered[key];
            _syncSubTableFromList(filtered, callback);
        });
        break;
    }
}

function _syncTable(tableName, data, callback) {
    console.log("Downloaded table: " + tableName + ", " + data.length + " rows.");
    if (data.length == 0) {
        syncProgressList.push(tableName);
        if (callback) callback();
        return;
    }
    if (syncProgressList.indexOf(tableName) > -1) {
        syncProgressList.push(tableName);
        _syncData(tableName, data, columnDir[tableName], callback);
        return;
    }
    function _syncData(tableName, data, cinfo, callback) {
        var createSql = "CREATE TABLE IF NOT EXISTS " + tableName + " (" + cinfo.CreateColumns + ");";
        _dB.transaction(function (x) {
            executeSql(x, createSql, [], function (tx) {
                //log("Loading local table " + tableName + ".");
                var firstRow = data[0];
                var progress = 0;
                for (i in data) {
                    var parcel = data[i];
                    var total = data.length;
                    var pa = _parseDataForInsert(parcel, firstRow, cinfo);
                    var columns = "";
                    var phs = "";
                    for (key in parcel) {
                        if (cinfo.ColumnKeys.indexOf(key) > -1) {
                            if (key == "Index") key = "SortIndex";
                            if (columns == "") { columns = "[" + key + "]"; phs = "?"; }
                            else { columns += ", " + "[" + key + "]"; phs += ", ?"; }
                        }
                    }
                    var insertSql = "INSERT INTO " + tableName + " (" + columns + ") VALUES (" + phs + ")";
                    executeSql(tx, insertSql, pa, function (tx, res) {
                        progress += 1;
                        if (progress == total) {
                            //log("Finished creating local table " + tableName + ".");
                            if (callback) callback();
                        }
                    }, function (e, m) {
                        log(m.message, true);
                    });
                }
            });
        });


    }

    if (data.length > 0) {
        _dB.transaction(function (x) {
            var dropAction = "DROP TABLE IF EXISTS " + tableName;
            var dropConfirm = tableName + " dropped.";
            executeSql(x, dropAction, [], function (x) {
                //console.log(dropConfirm);
                if (data.length > 0) {
                    var firstRow = data[0];
                    _getColumnInformation(firstRow, function (cInfo) {
                        columnDir[tableName] = cInfo;
                        syncProgressList.push(tableName);
                        _syncData(tableName, data, cInfo, callback);
                    }, tableName);
                }
            });
        });

    }
}
function generatefetchquery() {
    var fquery = "";
    var flimit = $('.filterdiv').length;
    for (i = 0; i < flimit; i++) {
        var hcntntel = $('#divSearchTop1 .filterdiv .filtercntnt')[i];
        var qvalsel = $('#divSearchTop1 .filterdiv .fltrinpsrch1')[i];
        var qval1 = $(qvalsel).val();
        if (qval1.includes(''))
            qval1 = qval1.replace(/'/g, "''");
        var qfield1 = $(hcntntel).attr('qfield').split('/');
        var qcontn1 = $(hcntntel).attr('qcontn');
        var fieldvalidation1 = validateData($(hcntntel).attr('qfield'), qval1, qcontn1);
        if (fieldvalidation1 != undefined && fieldvalidation1 == false)
            return;
        qfield1 = qfield1[0];



        if (qcontn1 == "notstrtwith") {
            //qval1=qval1+"%";
            qcontn1 = " NOT LIKE ";
        }

        fquery = fquery + '^^^' + qfield1 + "|" + qcontn1 + "|" + qval1 + '___@@@\n';
    }
    var optns = $(hcntntel).attr('qfield');
    if (flimit == 0) optns = '';
    var sval = $(qvalsel).val();
    if (qcontn1 == ' IS NOT NULL ' || qcontn1 == ' IS NULL ') {
        sval = "1";
    }
    doSearch(fquery, optns, sval);
}

function doSearch(searchstrng, optns, sval) {
    $('#rblStatus input:checked').prop('checked', false);
    $('#rblQc input:checked').prop('checked', false);
    $('.masklayer').show();
    var b = $(window).width() - $('#divSearchCntnr').width();
    $('#search-map').width(b);
    $('.resultframe').hide();
    $('.searchframe').show();
    $('#divRghtCntnr').hide();
    $('#divFootCntnr').hide();

    var fieldName = "";
    var fieldValue = "";
    if (optns == undefined) optns = "0";
    if (optns == "0" || optns == "1" || optns == "2")
        var Optn = optns;
    else
        var Optn = optns.split('/'); // To retrieve datatype of field along with fieldname.
    var srchOptn = Optn[0].trim();
    var fieldDataType = Optn[1];
    var schemaType = Optn[2];
    var cntnOprtr = $('#MainContent_Operator').val();

    if (schemaType) schemaType = schemaType.toUpperCase();



    if (fieldDataType != undefined) {
        var numericlength = Optn[2].split('(')[1];
    }

    if (srchOptn) {
        fieldName = srchOptn;
        fieldValue = sval;
        if (fieldValue == undefined) fieldValue = "0";
        var val = optns.split('/');
        if (val[3]) {
            if (datafields[val[3]].LookupTable)
                fieldValue = sval;
        }
        var str = fieldValue;
    }
    if (fieldValue == '' && fieldDataType == "8") {
        fieldValue = "1";
    }
    if (schemaType != undefined) {
        if (schemaType.includes('INT') && isNaN(fieldValue)) {
            clearAlert();
            return false;
        }
        else if (schemaType.includes('INT') && (fieldValue.includes('.'))) {
            clearAlert();
            return false;
        }
    }

    if ((fieldName == 0 || fieldName == 1 || fieldName == 2) && (fieldValue.includes('%'))) {
        alert('Invalid Special character')
        clearSearch()
        closeParcel();
        $('.masklayer').hide();
        return false;
    }

    if (fieldDataType == 1 || fieldDataType == 2 || fieldDataType == 3 || fieldDataType == 5 || fieldDataType == 6 || fieldDataType == 7 || fieldDataType == 8 || fieldDataType == 9 || fieldDataType == 10 || fieldDataType == 11 || fieldDataType == 12 || fieldDataType == 13) {
        var pattern = new RegExp(QC.dataTypes[fieldDataType].pattern);
        var validate1 = (pattern.test(fieldValue));
        if (!validate1 && cntnOprtr != ' IS NULL ' && cntnOprtr != ' IS NOT NULL ') {
            clearAlert();
            return false;
        }
    }

    if (fieldDataType != undefined) {
        if (schemaType.includes('numeric')) {
            var pattern = /^([a-zA-Z]*$)|[ !@#$%^&*()_+\-=\[\]{};':"\\|,<>\/?]/;
            var validate = (pattern.test(fieldValue));
            if (validate || str.length > numericlength) {
                clearAlert();
                $('.masklayer').hide();
                return false;
            }
        } else if (schemaType.includes('BIT')) {
            if (!(fieldValue == 0 || fieldValue == 1)) {
                clearAlert();
                $('.masklayer').hide();
                return false;
            }

        }


    }

    var flag = $('.SrchFlag').val();
    var user = $(".ddlUsers").val();

    if (fieldDataType == 4 && schemaType.includes('DATE')) {
        //var regex = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[13-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;
        //var regex1 = /^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$/;
        //var regex = new RegExp("^((0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2})*$");
        var regex = (schemaType == 'DATETIME') ? /((\b1[8-9]\d{2}|\b2\d{3})[-/.](0[1-9]|1[0-2])[-/.](0[1-9]|[12]\d|3[01]))/ : /(\b[12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/;
        var validdate = (regex.test(fieldValue));
        //var leapyrvalidate = (regex1.test(fieldValue));
        if (!validdate) {
            alert("Please enter a valid Date Format")
            clearSearch()
            closeParcel();
            $('.masklayer').hide();
            return false;
        }
    }

    if (fieldName != "" && fieldValue == "" && cntnOprtr != "IS NOT NULL" && cntnOprtr != "IS NULL") {
        var alrtMsg = 'You need to provide the Search options';
        if (fieldName != "" && fieldValue == "") {
            alrtMsg = 'You need to provide either the ID or Value to search.';
            if (fieldName == 1) alrtMsg = 'You need to provide the Assignment Group to search.';
            else if (fieldName == 2) alrtMsg = 'You need to provide the CC Tag to search.';
            clearSearch();
            closeParcel();
            $('.masklayer').hide();
            $('#divRghtCntnr').hide('fast');
            $('#divFootCntnr').hide();
        }
        alert(alrtMsg);
        $('.masklayer').hide();
        window.location.hash = "";
        return false;
    }


        $.ajax({
            url: '/sv/bulkedit.jrq',
            data: {
                fieldName: fieldName,
                fieldValue: fieldValue,
                flag: flag,
                user: user,
                searchstrng: searchstrng,
                flagstatus: skvalue,
                bulkedits: bulkedit
            },
            dataType: 'json',
            async: false,        
            success: function (resp) {
                if (resp.status != 'Error') {
                    if (bulkedit == 1) {
                        bulkedit = 0
                        if (skvalue == '7') {
                            alert("Sketch Validation Status Reseted for " + parcelcount + " Parcels ")
                        } else {
                            alert("Sketch Validation Status changed for " + parcelcount + " Parcels  to " + flagStatuses[skvalue] + " by " + resp.username)
                        }
                        displaySearchResultsInSearchParcelsSv(resp);
                    } else {
                        displaySearchResultsInSearchParcelsSv(resp);
                    }
                } else {
                    alert(resp.message);

                }

            }
        });

    return false;
}


function displaySearchResultsInSearchParcelsSv(results, rmalert) {

    if (results.status == 'Error') {
        if (results.message.includes('aggfield') && results.message.includes('Invalid column'))
            alert('Please check the aggregate condition given. Column name is invalid.');
        else
            alert(results.message);
        clearSearch();

        hideMask();
        return;
    }
    parcels = results.Parcels;
    //SortSearchResults("Parcel Id", true)
    parcelindex = 0;
    hasNbhdBounds = results.HasNeighborhood;
    var htmlCntnts = "";
    if (parcels.length == 0) {
        if (!rmalert) {
            alert('Your search did not produce any results.\n');
        }
        $('#lblCntParcl').html("0");
        htmlCntnts += "<tr>";
        htmlCntnts += "<td colspan=\"3\" class=\"TdNodata\">No Data Available</td>";
        htmlCntnts += "</tr>";
        hideMask();
        clearSearch()
        $('#divRghtCntnr').hide('fast');
        $('#divFootCntnr').hide();
    }
    else if (parcels.length > 0) {
        pagefirstload = true;
        initParcelCount = parcels[0].ActualResCount
        parcelcount = parcels[0].ActualResCount
        $('#divRghtCntnr').show()
        $('#lblCntParcl').html(parcels.length);
        showpagination(parcels);


        if (parcels.length < 500) {
            additionalMsg = 'Search result produced ' + parcels[0].ActualResCount
        }
        else if (parcels.length === 1) {
            additionalMsg = 'Search result produced ' + parcels[0].ActualResCount + ' parcel'
        } else (additionalMsg = 'Search result produced ' + parcels[0].ActualResCount + ' parcels.Displaying the top 500 results below.')
        if (!rmalert) {
            alert(additionalMsg);
        }
        $('#divprclcnt').show();
        hideMask();
    }
    else {
        $('#lblCntParcl').html("0");
        htmlCntnts += "<tr>";
        htmlCntnts += "<td colspan=\"3\" class=\"TdNodata\">No Data Available</td>";
        htmlCntnts += "</tr>";
    }


}
function showpagination() {
    if (parcels) {
        var table = parcels;
        var recordsPerPage = parseInt($('#recordsPerPageSelector').val(), 10);

        var totalPages = Math.ceil(table.length / recordsPerPage);

        var pageSelector = document.getElementById('pageSelector');


        if (pagefirstload) {
            pageSelector.innerHTML = '';
            for (var i = 1; i <= totalPages; i++) {
                var option = document.createElement('option');
                option.value = i;
                option.text = i;
                pageSelector.appendChild(option);
            }
        }

        var pageIndex = $('#pageSelector').val();
        var start = (pageIndex -1 ) * recordsPerPage;
        var end = start + recordsPerPage;

        var htmlCntnts = "";
        if (parcels.length > 0) {
            let pageparcel = parcels.slice(start, end);
            for (x in pageparcel) {
                var ParcelAddr = "";
                if (pageparcel[x].StreetAddress != null)
                    ParcelAddr = pageparcel[x].StreetAddress;
                if (pageparcel[x].KeyValue1 == '0' || pageparcel[x].ClientRowuid)
                    pageparcel[x].KeyValue1 = pageparcel[x].ClientRowuid;
                var ParcelId = pageparcel[x].KeyValue1;
                var ColorCode = pageparcel[x].ColorCode;
                htmlCntnts += "<tr id=\"tr-" + pageparcel[x].ID + "\">";
                htmlCntnts += "<td width=\"10%\" ><span id=\"divSq-" + pageparcel[x].ID + "\" class=\"divSquare\" style=\"opacity: 0.7;background-color:" + ColorCode + ";\"></span></td>";
                htmlCntnts += "<td width=\"45%\"><span  class=\"PrclId\">" + ParcelId + " </span></td>";
                htmlCntnts += "<td width=\"45%\"><span class=\"PrclAdr\">" + ParcelAddr + "</span></td>";
                htmlCntnts += "</tr>";
            }
        } else {
            $('#lblCntParcl').html("0");
            htmlCntnts += "<tr>";
            htmlCntnts += "<td colspan=\"3\" class=\"TdNodata\">No Data Available</td>";
            htmlCntnts += "</tr>";
        }

        $('#TabSearchContents tbody tr').remove();
        $('#TabSearchContents tbody').append(htmlCntnts);

        closeParcel();
        var btnPrevGrayOut = pageIndex == 1;
        $('#btnPreviousPage').toggleClass('grayed-out', btnPrevGrayOut);
        $('.search-container').removeAttr('style');
        // var btnNextGrayOut = pageIndex == totalPages;
        // $('#btnNextPage').toggleClass('grayed-out', btnNextGrayOut);      
    }
}
$('#recordsPerPageSelector').on('change', function () {
    svPromptAction(() => {
        pagefirstload = true;
        showpagination();
    }, (!activeParcel ? true : false));
});
$('#btnNextPage').on("click", function () {
    var currentPage = parseInt($('#pageSelector').val(), 10);
    $('#pageSelector').val(currentPage + 1);
    $('#pageSelector').change();
});

$('#pageSelector').on('change', function () {
    svPromptAction(() => {
        pagefirstload = false;
        showpagination();
    }, (!activeParcel ? true : false));
});
$('#btnPreviousPage').on("click", function () {
    var currentPage = parseInt($('#pageSelector').val(), 10);
    $('#pageSelector').val(currentPage - 1);
    $('#pageSelector').change();
});

function svPromptAction(callback, ignoreSprompt, pageLoad) {
    if ((ignoreSprompt && callback) || (!activeParcel && callback)) {
        callback();
        return;
    }

    let valid = _svModified();

    if (valid) {
        $('.sv-prompt').show();
        $('input[name="svprompt"]')[0].checked = true;
        $('.sv-prompt-ok').off('click');
        $('.sv-prompt-ok').on('click', () => {
            $('.sv-prompt').hide();
        });
    }
    else if (callback) callback();
}
function closeParcel(refresh) {
    activeParcel = null;
    var w = $(window).width(); var w1 = $('#divSearchCntnr').width();
  
    $('.resultframe').hide();
    $('#divFootCntnr').hide();
   
}