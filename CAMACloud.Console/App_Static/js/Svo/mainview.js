﻿
var Hammer = false;
var globalms; // to set ms as global, when we open a parcel then measuretool set to default
var activeParcel = {}
var map;
var mapZoom;
var drag = false;
var overlay;
var prevOverlayZindex = null;
var polygonCenter;
var degree;
var scale;
var RotationDegree;
var activeParcel;
var Parcelid;
var ParcelDataExist = false;
var parcelPolygon = new google.maps.Polygon();
var ParcelCenter;
var alreadyDone;
var sketchLatLng = '';
var shapeList = [];
var parcelPolygons = new google.maps.Polygon();
var firstTimeLoad = true;
var parcelLoaded = false;
var sketchRenderer = {};
var clientSettings = {};
var sketchSettings = {};
var adjustHandle = null;
var adjustHandleOrigin = null;
var adjusting = false;
var lookup;
var drawFlag = false;
var STYLE=google.maps.MapTypeControlStyle.HORIZONTAL_BAR;
var container;
var _poly = [];
var ccma = {
    Sketching: {
        SketchFormatter: null,
        Config: null
    }
};
$(function () {

    setPageDimensions();
    top.showMask()
    initMap(function () {
        window.parent.loadEssentialTables(function () {
            if (window.parent.processHash) window.parent.processHash();
            top.hideMask()
        })
    });
    sketchRenderer = new CCSketchEditor('.ccse-img', CAMACloud.Sketching.Formatters.TASketch);
    $(document).keydown(function (e) {
        if ($('.masklayer', window.parent.document).is(':visible')) { return false; }
        if (e.which == 190) { top.moveNxt(); }
        else if (e.which == 188) { top.movePrev(); }
    });
});

$(window).resize(setPageDimensions);
$(document).keydown(function (e) {
    if (window.parent.currentInput)
        return true
    var key = e.charCode || e.keyCode;
    window.parent.keybordShortCuts(key, e.shiftKey)
    if (key == 38 || key == 40 || key == 37 || key == 39) {
        window.parent.paintSketch();
        e.preventDefault();
        return false;
    }
});

function openSketchWindow() {
    var divText = top.$(".sketch-big").html();
    var myWindow = window.open('', '', 'width=200,height=100');
    var doc = myWindow.document;
    doc.open();
    doc.write(divText);
    doc.close();
}
function initMap(callback) {
	mOptions = ["roadmap", "satellite","hybrid"];
	if(nearmapApiKey !=="" && nearmapAccess.includes('SV'))
		mOptions.push("Nearmap");
	if (nearmapWMSApiKey !== "" && nearmapWMSAccess.includes('SV'))
        mOptions.push("NearmapWMS");															   								
	if(woolpertApikey  !== "" && woolpert3inchId  !== "" && woolpertAccess.includes('SV'))
		mOptions.push("Woolpert3");
	if(woolpertApikey !== "" && woolpert6inchId !== "" && woolpertAccess.includes('SV'))
		mOptions.push("Woolpert6");
	if(customWMS !=="" && customMapIsImage !== '1'&& customWMSAccess.includes('SV'))
		mOptions.push("customWMS");
	if(mOptions.length > 5)
		STYLE=google.maps.MapTypeControlStyle.DROPDOWN_MENU;
    	
    var options = {
        zoom: 20,
        zoomControl: true,
        center: new google.maps.LatLng(41.655471, -83.534546),
	    mapTypeControlOptions: {
        	style: STYLE,
	      mapTypeIds: mOptions
	    },
        minZoom: 15,
        tilt: 0,
        keyboardShortcuts: false,
        gestureHandling: 'greedy',
        fullscreenControl: false
    };
    var mc = document.getElementById('map');
    map = new google.maps.Map(mc, options);
    map.setZoom(20);
    mapZoom = 20;
    map.setMapTypeId('hybrid');
    //custom imagery
    
     if(customWMS!=='' && customMapIsImage !== '1' && customWMSAccess.includes('SV')){
    	const cMap = new google.maps.ImageMapType({
		getTileUrl: function(coord, zoom) {
		return customWMS + zoom + '/' + coord.y + '/' + coord.x ;
		},
		tileSize: new google.maps.Size(256, 256),
		maxZoom: 20,
		minZoom: 6,
		
		name: 'CustomMap'
		});
		map.mapTypes.set("customWMS", cMap);
    
    }
    //adding woolpert 6 inch imagery to the map using ImageMapType function
    if(woolpertApikey !==""  && woolpert6inchId!=="" && woolpertAccess.includes('SV')){
    	 const woolpertMapType6 = new google.maps.ImageMapType({
		getTileUrl: function(coord, zoom) {
		return 'https://raster.stream.woolpert.io/layers/'+woolpert6inchId+'/tiles/'
			     + zoom + '/' + coord.x + '/' + coord.y + '?key='+woolpertApikey;
		},
		tileSize: new google.maps.Size(256, 256),
		maxZoom: 20,
		minZoom: 6,
		
		name: 'Woolpert6"'
		});
		map.mapTypes.set("Woolpert6", woolpertMapType6);
		//map.setMapTypeId("Woolpert6");
    
    }
    //adding woolpert 3 inch imagery to the map using ImageMapType function
    if(woolpertApikey !==""  && woolpert3inchId!=="" && woolpertAccess.includes('SV') ){
    	 const woolpertMapType3 = new google.maps.ImageMapType({
		getTileUrl: function(coord, zoom) {
		return 'https://raster.stream.woolpert.io/layers/'+woolpert3inchId+'/tiles/'
			     + zoom + '/' + coord.x + '/' + coord.y + '?key='+woolpertApikey;
		},
		tileSize: new google.maps.Size(256, 256),
		maxZoom: 20,
		minZoom: 6,
		
		name: 'Woolpert3"'
		});
		map.mapTypes.set("Woolpert3", woolpertMapType3);
		//map.setMapTypeId("Woolpert3");
    }
   	//Nearmap Imagery
    if(nearmapApiKey !=="" && nearmapAccess.includes('SV')){
	    const NearmapMapType = new google.maps.ImageMapType({
		getTileUrl: function(coord, zoom) {
			const normalizedCoord = getNormalizedCoord(coord, zoom);
			if (!normalizedCoord) {
			return "";
			}
		
		const bound = Math.pow(2, zoom);
		return "https://api.nearmap.com/tiles/v3/Vert" +
	    "/" +
	    zoom +
	    "/" +
	    normalizedCoord.x +
	    "/" +
	    (normalizedCoord.y) +
	    ".img?apikey="+nearmapApiKey+"&tertiary=satellite"
		},
		tileSize: new google.maps.Size(256, 256),
		maxZoom: 20,
		minZoom: 6,
		radius: 1738000,
		name: "Nearmap"
		});
		function getNormalizedCoord(coord, zoom) {
	        const y = coord.y;
	        let x = coord.x; // tile range in one direction range is dependent on zoom level
	        // 0 = 1 tile, 1 = 2 tiles, 2 = 4 tiles, 3 = 8 tiles, etc
	
	        const tileRange = 1 << zoom; // don't repeat across y-axis (vertically)
	
	        if (y < 0 || y >= tileRange) {
	          return null;
	        } // repeat across x-axis
	
	        if (x < 0 || x >= tileRange) {
	          x = ((x % tileRange) + tileRange) % tileRange;
	        }
	
	        return {
	          x: x,
	          y: y
	        };
	        }
		map.mapTypes.set("Nearmap", NearmapMapType);
		map.setMapTypeId("Nearmap");
	}
	
    //NearmapWMS Imagery
    if (nearmapWMSApiKey !== "" && nearmapWMSAccess.includes('SV')) {

        const NearmapWMSMapType = new google.maps.ImageMapType({
            getTileUrl: function (coord, zoom) {
                const normalizedCoord = getNormalizedCoord(coord, zoom);
                if (!normalizedCoord) {
                    return "";
                }

                const bound = Math.pow(2, zoom);
                return "https://api.nearmap.com/tiles/v3/Vert" +
                    "/" +
                    zoom +
                    "/" +
                    normalizedCoord.x +
                    "/" +
                    (normalizedCoord.y) +
                    ".img?apikey=" + nearmapWMSApiKey + "&tertiary=satellite"
            },
            tileSize: new google.maps.Size(256, 256),
            maxZoom: 20,
            minZoom: 6,
            radius: 1738000,
            name: "Nearmap"
        });
        function getNormalizedCoord(coord, zoom) {
            const y = coord.y;
            let x = coord.x; // tile range in one direction range is dependent on zoom level
            // 0 = 1 tile, 1 = 2 tiles, 2 = 4 tiles, 3 = 8 tiles, etc

            const tileRange = 1 << zoom; // don't repeat across y-axis (vertically)

            if (y < 0 || y >= tileRange) {
                return null;
            } // repeat across x-axis

            if (x < 0 || x >= tileRange) {
                x = ((x % tileRange) + tileRange) % tileRange;
            }

            return {
                x: x,
                y: y
            };
        }
        map.mapTypes.set("NearmapWMS", NearmapWMSMapType);
        map.setMapTypeId("NearmapWMS");
    }


    google.maps.event.addListener(map, 'zoom_changed', function (e) {
        var zoomLevel = map.getZoom();
        if( zoomLevel > 21 || zoomLevel < 15 ) return; // zoom max value 20 and min value 15
        var zoomdiff = 0;
        sketchZoom = activeParcel.sketchZoom;
        if (mapZoom > zoomLevel) {
            zoomdiff = mapZoom - zoomLevel;
            zoomSketch(sketchZoom / Math.pow(2, zoomdiff), true, false, 'OUT', zoomLevel);
        } else if (mapZoom < zoomLevel) {
            zoomdiff = zoomLevel - mapZoom;
            zoomSketch(sketchZoom * Math.pow(2, zoomdiff), true, false, 'IN', zoomLevel);
        }
        window.setTimeout('setPositionOverlayIfDataExist();', 200);
        mapZoom = zoomLevel;
    });
    map.streetView.addListener("visible_changed", () => {
          if(map.streetView.visible)
	        adjustHandle.setVisible(false)
	    else
	        adjustHandle.setVisible(true)
    });
    var measure;
    var measureDiv = document.createElement('div');
    measure = new MeasurementScale(measureDiv, map);
    measureDiv.index = 1;
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(measureDiv);

    var rzIcon = '/App_Static/cursors/rotatezoom.png';

    adjustHandle = new google.maps.Marker({
        position: map.getCenter(),
        icon: rzIcon,
        draggable: true,
        map: map
    });

    var startDegree = null;
    google.maps.event.addListener(adjustHandle, 'dragstart', (function (e) {
        startDegree = degree;
        adjusting = true;
        adjustHandleOrigin = getCenterOfOverlay()
    }));

    google.maps.event.addListener(adjustHandle, 'drag', (function (e) {
        var ah = LatLngToPoint(e.latLng);
        var c = LatLngToPoint(adjustHandleOrigin);
        var dist = Math.round(Math.sqrt((ah.x - c.x) * (ah.x - c.x) + (ah.y - c.y) * (ah.y - c.y)));
        var zoom = Math.round((activeParcel.zoomSketch || 0.6) * dist / 200 * 100) / 100;
        zoomSketch(zoom);

        var rotateDegree = Math.round(Math.atan2(ah.y - c.y, ah.x - c.x) * 180 / Math.PI) + 135 + startDegree;
        if (rotateDegree > 180) rotateDegree = rotateDegree - 360;
        if (rotateDegree < -180) rotateDegree = rotateDegree + 360;
        rotateSketch(rotateDegree, true);

    }));

    google.maps.event.addListener(adjustHandle, 'dragend', (function (e) {
        startDegree = null;
        adjusting = false;
        adjustHandleOrigin = null;
        //map.setOptions({ draggableCursor: 'default' });
        //adjustHandle.setVisible(true);
    }));

    google.maps.event.addListener(map, 'click', (function (e) {
        if (measure.on) {
            if (measure.start == null) {
                measure.start = e.latLng;
            	if($('.overlay').length){
            		prevOverlayZindex = $('.overlay')[0].style.zIndex;
            		$('.overlay')[0].style.zIndex = '0';
            		//$('.overlay')[0].style.opacity = 0.35;
					$('.overlay').parent('div')[0].style.cursor = '';            		
            		}
            } else {
                if (measure.end == null) {
                    measure.end = e.latLng;
                    if($('.overlay').length){
            			$('.overlay')[0].style.zIndex = prevOverlayZindex;
            			$('.overlay')[0].style.opacity = 1;
            			$('.overlay').parent('div')[0].style.cursor = 'default'; 
            		}
                } else {
                    measure.start = e.latLng;
                    measure.end = null;
                    if($('.overlay').length){
                		prevOverlayZindex = $('.overlay')[0].style.zIndex;
                		$('.overlay')[0].style.zIndex = '0';
                		//$('.overlay')[0].style.opacity = 0.35;
                		$('.overlay').parent('div')[0].style.cursor = ''; 
                		}
                }
            }

            measure.updateGraphics();
            $('.sv-logo', window.parent.document).click().focus();
            e.stop();
        }
    }));

    google.maps.event.addListenerOnce(map, 'idle', function () {
        adjustHandleOrigin = LatLngToPoint(adjustHandle.position);
        if (callback) callback();
        adjusting = false;
    });

    if (window?.mapFrame?.contentWindow?.map) { window.mapFrame.contentWindow.map.setZoom(20); } else { setTimeout(() => { google.maps.event.trigger(map, 'idle'); }, 500); }
}

function openParcel(callback, _clearSketchDropDown) {
    activeParcel = parent.activeParcel
    lookup = parent.lookup
    parcelLoaded = false;
    isSketchChanged = false;
    alreadyDone = true;
    ParcelDataExist = false;
    firstTimeLoad = true;
    degree = 0;
    var globalms1 = globalms;
    mapZoom = activeParcel.MapZoom;
    currentOverlayPosition = {};
    parcelId = activeParcel.KeyValue1;
 
    if ( top.clientSettings )
    	clientSettings = top.clientSettings;
    if( top.sketchSettings )
    	sketchSettings = top.sketchSettings;
    	
    if( !( ( top.clientSettings && top.clientSettings.SketchConfig ) || ( top.sketchSettings && top.sketchSettings.SketchConfig ) ) ) {
    	// clientSettings["SketchConfig"] = activeParcel.sketchconfig
        // clientSettings["SketchFormat"] = activeParcel.sketchformat
        sketchSettings["SketchConfig"] = activeParcel.sketchconfig;
        sketchSettings["SketchFormat"] = activeParcel.sketchformat;
    }
    
    parcelPolygons.setMap(null);

    if (activeParcel.dataExist && activeParcel.SketchData && activeParcel.SketchData != '{}' && activeParcel._mapPoints && activeParcel._mapPoints.length > 0) {
        $('#divexport', window.top.document).css('display', 'none');
    } else {
        $('#divexport', window.top.document).css('display', 'none');
    }
    var points = [],
        shapes = [];
    shapeList = [];
    var bounds = new google.maps.LatLngBounds();
    if (typeof activeParcel._mapPoints != "undefined") {
        for (x in activeParcel._mapPoints.sort(function (x, y) {
            return x.Ordinal - y.Ordinal
        })) {
            var p = activeParcel._mapPoints[x];
            var latlng = new google.maps.LatLng(p.Latitude, p.Longitude);
            points.push(latlng);
            bounds.extend(latlng);
            if (shapes[p.RecordNumber || 0] == null) {
                shapes[p.RecordNumber || 0] = [];
            }
            shapes[p.RecordNumber || 0].push(latlng);
        }
    }

    shapeList = shapes.filter(function (el) {
              return el != null;
        });
	parcelPolygons = new google.maps.Polygon({
				paths:shapeList,
				strokeColor: "#FF0000",
				strokeOpacity: 0.8,
				strokeWeight: 2,
				fillColor: "#FF0000",
				fillOpacity: 0.1,
				clickable: false,
	    });
	parcelPolygons.setMap(map);
    polygonCenter = bounds.getCenter();
    activeParcel["LatLngPoints"] = points;
    activeParcel["Bounds"] = bounds;
    var parcel = activeParcel;
    if (globalms1 && globalms1.on) {
        var measureDiv = $('.measurement-tool')[0];
        if (measureDiv) {
            map.setOptions({
                draggableCursor: null
            });
            measureDiv.className = 'measurement-tool measure-off tip';
            $(measureDiv).attr({
                "abbr": 'Click on the measuring tool to activate it and measure. Click again to deactivate the tool.',
                "width": '400px;'
            });
            $('.overlay').removeClass('pointer')
            globalms1.clear();
            globalms1.on = !globalms1.on;
        }
    }
    google.maps.event.trigger(map, 'resize');
    drawParcel(0, 0, parcel.SketchVector, parcel.dataExist ? 'white' : 'yellow', function () {
        if (parcel.dataExist) {
            // currentOverlayPosition = new google.maps.LatLng(parcel.lat, parcel.lng);
			var mZoom = null;
            setOverLayPosition(parcel.lat, parcel.lng, parcel.sketchZoom, parcel.SketchRotate, parcel.SketchData);
            ParcelDataExist = true;
            setPositionOverlayIfDataExist();
            scale = isNaN(parseFloat(sketchOB.sketchZoom)) ? 0 : parseFloat(sketchOB.sketchZoom);
            RotationDegree = parseFloat(sketchOB.sketchRotation);
			var skRowuid = top.$('.sketch-select option[value="' + selectedSketch + '"]').attr('skrowuid');
			var skPrefix = top.$('.sketch-select option[value="' + selectedSketch + '"]').attr('skprefix');
			skPrefix = isNewSketchData && skPrefix? skPrefix: '';
			var newSelectedSketch = isNewSketchData? skRowuid: selectedSketch;
            if(currentOverlayPosition && newSelectedSketch && selectedSection && currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection].position){
                var pos = LatLngToPoint(currentOverlayPosition["sketch-" +  newSelectedSketch + skPrefix][selectedSection].position)
                var divW = divH = (400 * scale)/2;
                if(divH && divW && pos){
	                pos.y = pos.y + divH;
	                pos.x = (pos.x + divW);
	                var center = PointToLatLng(pos);;
	                map.setCenter(center);
                }
                else
                    map.setCenter(currentOverlayPosition["sketch-" +  newSelectedSketch + skPrefix][selectedSection].position);
				if (currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection].mapZoom) 
					mZoom = currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection].mapZoom;
            } else
			    map.setCenter(bounds.getCenter());
			mZoom = mapZoom = mZoom? mZoom: parcel.MapZoom;
			map.setZoom(parseInt(mZoom));
            zoomSketch(sketchOB.sketchZoom);
            rotateSketch(parseFloat(sketchOB.sketchRotation), true);

        } else {
            setOverLayPosition(polygonCenter.lat(), polygonCenter.lng(), 0.6, 0);
            setPositionOverlayIfDataExist(true);
            map.setCenter(bounds.getCenter());
            map.setZoom(20);
            zoomSketch(0.6, true);
            rotateSketch(0, true);
        }



        if (callback) callback(parcel);
    }, _clearSketchDropDown);

    //  });

}

DraggableOverlay.prototype = new google.maps.OverlayView();

function DraggableOverlay(map, position, content) {
    if (typeof draw === 'function') {
        this.draw = draw;
    }
    this.setValues({
        position: position,
        container: null,
        content: content,
        map: map
    });
}

DraggableOverlay.prototype.onAdd = function () {
    container = document.createElement('div'),
        that = this;
    if (typeof this.get('content').nodeName !== 'undefined')
        container.appendChild(this.get('content'));
    else {
        if (typeof this.get('content') === 'string')
            container.innerHTML = this.get('content');
        else
            return;
    }
    container.style.position = 'absolute';
    container.draggable = true;
    google.maps.event.addDomListener(this.get('map').getDiv(), 'mouseleave', function () {
        google.maps.event.trigger(container, 'mouseup');
    });
    google.maps.event.addDomListener(this.get('map').getDiv(), 'click', function () {
        google.maps.event.trigger(container, 'mouseup');
    });
    google.maps.event.addDomListener(container, 'mousedown', function (e) {
        this.style.cursor = 'move';
        that.map.set('draggable', false);
        that.set('origin', e);
        that.moveHandler = google.maps.event.addDomListener(that.get('map').getDiv(), 'mousemove', function (e) {
            var origin = that.get('origin');
            if(origin){
            var left = origin.clientX - e.clientX;
            var top = origin.clientY - e.clientY;
            var pos = that.getProjection().fromLatLngToDivPixel(that.get('position'));
            if (pos) {
                var latLng = that.getProjection().fromDivPixelToLatLng(new google.maps.Point(pos.x - left, pos.y - top));
                that.set('origin', e);
                that.set('position', latLng);
                isSketchChanged = true;
                firstTimeLoad = false;
                that.draw();
             }
           } 
        });
    });
    google.maps.event.addDomListener(container, 'mouseup', function (e) {
		var skRowuid = top.$('.sketch-select option[value="' + selectedSketch + '"]').attr('skrowuid');
		var skPrefix = top.$('.sketch-select option[value="' + selectedSketch + '"]').attr('skprefix');
		skPrefix = isNewSketchData && skPrefix? skPrefix: '';
		var newSelectedSketch = isNewSketchData? skRowuid: selectedSketch;
        if (drawFlag) {
            var latLng;
            if (newSelectedSketch && selectedSection) latLng = currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection].position;
            that.set('position', latLng);
            that.draw();
            drawFlag = false;
        }
        else {
            firstTimeLoad = false;
            that.map.set('draggable', true);
            if (typeof measure !== 'undefined' && !measure.on) {
            	this.style.cursor = 'default';
            }
            that.set('origin', e);
            google.maps.event.removeListener(that.moveHandler);
            if (that.position) {
                sketchLatLng = that.position.lat() + ',' + that.position.lng();
                var zoom = window.parent.$('.sketch-resize').val();
                var angle = window.parent.$('.sketch-rotate').val();
                if (currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix] && selectedSection) {
                    currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection] = {
                        "position": new google.maps.LatLng(that.position.lat(), that.position.lng()),
                        "sketchZoom": zoom,
                        "sketchRotation": parseFloat(angle),
						"mapZoom": mapZoom,
                        modified: true
                    }
                }
            }
        }
    });
	
    $('.overlay').parent('div').remove();
    this.set('container', container)
    this.getPanes().floatPane.appendChild(container);
    if (activeParcel && activeParcel.dataExist) {
		var mZoom = null;
        scale = isNaN(parseFloat(sketchOB.sketchZoom)) ? 0 : parseFloat(sketchOB.sketchZoom);
        RotationDegree = parseFloat(sketchOB.sketchRotation);
		var skRowuid = top.$('.sketch-select option[value="' + selectedSketch + '"]').attr('skrowuid');
		var skPrefix = top.$('.sketch-select option[value="' + selectedSketch + '"]').attr('skprefix');
		skPrefix = isNewSketchData && skPrefix? skPrefix: '';
		var newSelectedSketch = isNewSketchData? skRowuid: selectedSketch;
        if(currentOverlayPosition && newSelectedSketch && selectedSection && currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection].position){
            var pos = LatLngToPoint(currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection].position)
            var divW = divH = (400 * scale)/2;
             if(divH && divW && pos){
	            pos.y = pos.y + divH;
	            pos.x = (pos.x + divW);
	            var center = PointToLatLng(pos);;
	            map.setCenter(center);
            }
            else
                map.setCenter(currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection].position);
			if (currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection].mapZoom) 
				mZoom = currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection].mapZoom;
        } else
            map.setCenter(polygonCenter);
		mZoom = mZoom? mZoom: activeParcel.MapZoom;
        map.setZoom(parseInt(mZoom));
        zoomSketch(sketchOB.sketchZoom);
        if (!isNaN(sketchOB.sketchRotation))
            $('#sketch').rotate(parseFloat(sketchOB.sketchRotation));
    } else {
        setOverLayPosition(polygonCenter.lat(), polygonCenter.lng(), 0.6, 0);
        setPositionOverlayIfDataExist(true);
        map.setCenter(activeParcel.Bounds.getCenter());
        map.setZoom(20);
        zoomSketch(0.6, true);
        rotateSketch(0, true);
    }
};

DraggableOverlay.prototype.draw = function () {
    var pos = this.getProjection().fromLatLngToDivPixel(this.get('position'));
    if (pos) {
        this.get('container').style.left = pos.x + 'px';
        this.get('container').style.top = pos.y + 'px';
        if (!activeParcel.sketchLatLng && !isSketchChanged) {
            var divW = parseFloat($('#resizable-wrapper', this.container).css("width")) / 2;
            var divH = parseFloat($('#resizable-wrapper', this.container).css("height")) / 2;
            pos.y = pos.y - divH;
            pos.x = (pos.x - divW);
            this.get('container').style.left = pos.x + 'px';
            this.get('container').style.top = pos.y + 'px';
        }
    }
};

DraggableOverlay.prototype.onRemove = function () {
    this.get('container').parentNode.removeChild(this.get('container'));
    this.set('container', null);
};

function setOverLayPosition(lat, lng, zoom, rotation, sketchData) {
    if (sketchData && sketchData != "") {
        currentOverlayPosition = $.parseJSON(sketchData);
        for (var key in currentOverlayPosition) {
            for (var key2 in currentOverlayPosition[key]) {
                currentOverlayPosition[key][key2].position = new google.maps.LatLng(currentOverlayPosition[key][key2].position.lat, currentOverlayPosition[key][key2].position.lng);
                currentOverlayPosition[key][key2].modified = true;
            }
        }
        return;
    }
    /*var poly_ltlg=LatLngToPoint(polygonCenter);
    if(poly_ltlg){
        poly_ltlg.x=poly_ltlg.x-120;
        poly_ltlg.y=poly_ltlg.y-120;
        polygonCenter=PointToLatLng(poly_ltlg);
        lat=polygonCenter.lat();
        lng=polygonCenter.lng();
    }*/
    if (polygonCenter) {
        lng = polygonCenter.lng();
        lat = polygonCenter.lat();
    }
    var sk = sketchRenderer.sketches;
    sk.forEach(function (s, i) {
		var skRowuid = (s.parentRow && s.parentRow.ROWUID)? s.parentRow.ROWUID: s.sid;
        var skPrefix = (s.config && s.config.SketchLabelPrefixInSv)? s.config.SketchLabelPrefixInSv: '';
		i = isNewSketchData? skRowuid: i;
		skPrefix = isNewSketchData? skPrefix: '';
        currentOverlayPosition["sketch-" + i + skPrefix] = {};
        for (var key in s.sections) {
            if (currentOverlayPosition["sketch-" +  i + skPrefix][key] === undefined)
                currentOverlayPosition["sketch-" +  i + skPrefix][key] = {
                    "position": new google.maps.LatLng(lat, lng),
                    "sketchZoom": zoom,
                    "sketchRotation": rotation,
					"mapZoom": mapZoom,
                    modified: false,
                }
        }
    })
}

function getParcelMapAndVector(parcelid, callback) {
    Parcelid = parcelid;
    $.ajax({
        url: '/sv/getparcel.jrq',
        method: 'POST',
        data: {
            parcel: parcelid
        },
        success: function (resp) {
            var points = [];
            var bounds = new google.maps.LatLngBounds();
            for (var i = 0; i < resp.Points.length; i++) {
                lat = resp.Points[i].Latitude;
                lng = resp.Points[i].Longitude;
                latlng = new google.maps.LatLng(lat, lng);
                points.push(latlng);
                bounds.extend(latlng);
            }
            polygonCenter = bounds.getCenter();
            resp["LatLngPoints"] = points;
            resp["Bounds"] = bounds;

            if (callback) callback(resp);
        }
    });
}

function drawParcel(latitude, longitude, vector, color, callback, _clearSketchDropDown) {
    previewSketch(color, 1, function (sketchUrl) {
        $('.overlay').parent('div').remove();
        var ltlng = polygonCenter
        if (activeParcel.lat && activeParcel.lng)
            ltlng = new google.maps.LatLng(activeParcel.lat, activeParcel.lng);
        if (activeParcel.sketchLatLng)
            ltlng = new google.maps.LatLng(activeParcel.sketchLatLng.split(',')[0], activeParcel.sketchLatLng.split(',')[1]);
        if (activeParcel.SketchData) {
			var skRowuid = top.$('.sketch-select option[value="' + selectedSketch + '"]').attr('skrowuid');
			var skPrefix = top.$('.sketch-select option[value="' + selectedSketch + '"]').attr('skprefix');
			skPrefix = isNewSketchData && skPrefix? skPrefix: '';
			var newSelectedSketch = isNewSketchData? skRowuid: selectedSketch;
            var skd = JSON.parse(activeParcel.SketchData);
            var tag = "sketch-" + newSelectedSketch + skPrefix;
            if (!_.isEmpty(skd[tag])) {
                var pos = skd[tag][selectedSection].position;
                ltlng = new google.maps.LatLng(pos.lat, pos.lng);
            }
        }
        //if (selectedSketch != '') {
        //    var tag = "sketch-" + selectedSketch;
        //    console.log(currentOverlayPosition, tag, currentOverlayPosition[tag] ? 'Yes' : 'NULL', selectedSketch, selectedSection);
        //    latlng = currentOverlayPosition[tag][selectedSection].position;
        //}

        overlay = new DraggableOverlay(map, ltlng, '<div class="overlay" id="resizable-wrapper"><img id="sketch" src="' + sketchUrl + '"><div>');
        $(".overlay").draggable({
            containment: "#map",
            scroll: false
        });
        $("#map").droppable();

        //try {
        //    adjustHandle.setPosition(getAdjHandlePosition());
        //} catch (e) {
        //    console.error('drawParcel {0}', e);
        //}

        if (callback) callback();
    }, undefined, undefined, _clearSketchDropDown);

}

function LatLngToPoint(latlng) {
    if (activeParcel.Id && overlay.getProjection != null && overlay.getProjection())
        return overlay.getProjection().fromLatLngToContainerPixel(latlng);
}

function PointToLatLng(point) {
    return overlay.getProjection().fromContainerPixelToLatLng(point)
}

function moveOverlayBy(topMove, leftMove) {
    var top = parseInt($('.overlay').css("top").replace(/[^-\d\.]/g, ''))
    var left = parseInt($('.overlay').css("left").replace(/[^-\d\.]/g, ''))
    $('.overlay').css({
        'top': top + topMove + 'px',
        'left': left + leftMove + 'px',
        'z-index': '2147483648'
    });

    setCurrentOverlapPos();

}
var sketchOB = {};

function _RoundX(x, y, scale) {
    var xx, yy;
    if (!scale) scale = 1;
    var sc = scale * 10;
    xx = Math.round(Math.round(x / sc) * sc);
    yy = Math.round(Math.round(y / sc) * sc);
    return { x: xx, y: yy };
}
function _calculateSketchScale(ab) {
    var scale = 1
    var sHeight = (ab.ymax - ab.ymin) * 10;
    var sWidth = (ab.xmax - ab.xmin) * 10;
    if (sHeight * 1.1 > 300)
        scale = 3000 / sHeight * 0.9;

    var maxHeight = 300 * 0.9;
    var maxWidth = 300 * 0.9;
    var aspectRatio = parseFloat(sWidth) / parseFloat(sHeight);
    var width, height;

    if ((sWidth < maxWidth) && (sHeight < maxHeight)) {
        scale = 1;
        width = maxWidth;
    } else if (aspectRatio > 1) {
        width = maxWidth;
        height = (width / aspectRatio);
        if (height > maxHeight) {
            height = maxHeight;
            width = (height * aspectRatio);
        }
    } else {
        height = maxHeight;
        width = (height * aspectRatio);
        if (width > maxWidth) {
            width = maxWidth;
            height = (width / aspectRatio);
        }
    }
    scale = parseFloat((width / sWidth).toFixed(2));
    var originPosition = (sketchRenderer.formatter && sketchRenderer.formatter.originPosition) ? sketchRenderer.formatter.originPosition : "bottomLeft";
    var o = { x: 0, y: 0 };
    if (originPosition == "topRight")
        o = _RoundX(sketchRenderer.width - 80, 60);
    else if (originPosition == "topLeft")
        o = _RoundX(40, 60);
    else
        o = _RoundX(40, sketchRenderer.height - 40);
    var _xval = sketchRenderer.formatter.originPosition == "topRight" ? applyRoundToOrigin(-ab.xmax * 10 * scale - 45) : applyRoundToOrigin(-ab.xmin * 10 * scale - 20);
    var _yVal = (sketchRenderer.formatter.originPosition == "topRight" || sketchRenderer.formatter.originPosition == "topLeft") ? applyRoundToOrigin(-ab.ymax * 10 * scale - 45) : applyRoundToOrigin(-ab.ymin * 10 * scale - 20);
    o.x += _xval;
    o.y -= _yVal;
    return o;
}

function applyRoundToOrigin(v) {
    if (sketchRenderer?.config?.formatter == 'Vision') v = Math.ceil(v);
    return v;
}

var previewPolygons = [];

function createNewPreviewPolygon() {
    return new google.maps.Polygon({
        strokeColor: '#FFFF00',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FFFF00',
        fillOpacity: 0.35
    });
}

function createNewPreviewMarker(position, title) {
    var m = new google.maps.Marker({
        position: position,
        title: title,
        map: map
    });
    previewPolygons.push(m);
}

function _mso( _msoCallback ) {
    var _selSkt = selectedSketch ? selectedSketch : '0';
    var _selSel = selectedSection ? selectedSection : '1';

    if (previewPolygons.length > 0) {
        for (var i = previewPolygons.length - 1; i >= 0; i--) {
            var pp = previewPolygons.pop();
            pp.setMap(null);
            pp = null;
        }
    }

    function _rotatePoint(cen, pt, R) {
        var cx = cen.x, cy = cen.y, dx = pt.x - cx, dy = pt.y - cy;
        var cosR = Math.cos(R), sinR = Math.sin(R);
        var x1 = dx * cosR - dy * sinR;
        var y1 = dx * sinR + dy * cosR;
        pt.x = parseInt(x1 * 10) / 10 + cx;
        pt.y = parseInt(y1 * 10) / 10 + cy;

        return pt;
    }

    var _polygons = [];
    if (currentOverlayPosition) {
        for (var skx in currentOverlayPosition) {
            var skgroup = currentOverlayPosition[skx];
            var sk_index = skx.replace('sketch-', ''); 
           	var sketchRecord = null;
				
			for (var srs in sketchRenderer.sketches) {
				var csketch = sketchRenderer.sketches[srs];
				var sprefix = '';
				if ( csketch.config && csketch.config.SketchLabelPrefixInSv)
					sprefix = csketch.config.SketchLabelPrefixInSv;
				var skRowuid = (csketch.parentRow && csketch.parentRow.ROWUID)? csketch.parentRow.ROWUID: csketch.sid;
				var slectVal = top.$('.sketch-select option[skrowuid="'+ skRowuid +'"]').val();
				
				if (isNewSketchData && sk_index == (skRowuid + sprefix)) {
					if (slectVal)
						sk_index = slectVal;
					sketchRecord = csketch;
					break;
				}
				else if (!isNewSketchData && slectVal == sk_index) {
					sketchRecord = csketch;
					break;	
				}
			} 
            
            for (var sky in skgroup) {
                var sksection = skgroup[sky];
                
                if ( sksection && sksection.position && sksection.modified && sketchRecord ) {
                    previewSketch('#000000', 1, function () {
                        var sketchPoly = {};
                        var _zm = parseFloat(sksection.sketchZoom);
                        var _angle = parseFloat(sksection.sketchRotation);

                        if (isNaN(_zm)) _zm = 1;
                        if (isNaN(_angle)) _angle = 0;

                        var R = ((360 - _angle) * Math.PI / 180);
                        var RR = ((_angle) * Math.PI / 180);

                        var _csp = sksection.position;
                        var tt = _csp;
                        _csp = LatLngToPoint(sksection.position);
                        
                        var _olat, _olng, _orgPoint;
                        var _so = _.clone(sketchRenderer.origin);
						if (_angle && _angle != 0) 
                        	_so = _rotatePoint({ x: 200, y: 200 }, _so, RR)

                        _olatsa = (_so.x * _zm) + (_csp.x);
                        _olngsa = (_so.y * _zm) + (_csp.y);
                        var _orgpt = { x: _olatsa, y: _olngsa };
                        _orgPoint = PointToLatLng(_orgpt);
                        _olat = _orgPoint.lat();
                        _olng = _orgPoint.lng();

                        if (testMode) { createNewPreviewMarker(_orgPoint, 'Origin'); }

                        var _cvs = sketchRecord && sketchRecord.vectors.filter(function (x) { return (!x.isUnSketchedTrueArea && !x.isUnSketchedArea) });

                        if (_csp) {
                            sketchPoly = { vectors: [] };
                            var cen = {};
                            if (_angle && _angle != 0) {
                                cen.x = 0; cen.y = 0;

                                _cvs.forEach((vt, ix) => {
                                    var sn = vt.startNode;
                                    while (sn != null) {
                                        if (!((sn == vt.endNode) && (sn.overlaps(vt.startNode.p)))) {
                                            sn.p = _rotatePoint(cen, _.clone(sn.p), R);
                                        }
                                        sn = sn.nextNode;
                                    }
                                    if (vt.startNode) {
                                        vt.startNode.recalculateAll();
                                    }
                                });
                            }

                            //var _cvs = sketchRenderer.sketches[sk_index] && sketchRenderer.sketches[sk_index].vectors.filter(function (x) { return (!x.isUnSketchedTrueArea && !x.isUnSketchedArea) });
                            var _vlen = _cvs.length;
                            cen.x = cen.x + _csp.x;
                            cen.y = cen.y + _csp.y;
                            
                            var _olat2 = _olat + 1,  _olng2 = _olng + 1, _rad = 57.29577951;
                            var tlat1 = _olat / _rad; 
                            var tlat2 = _olat2 / _rad;
                            var tlng1 = _olng / _rad;
                            var tlng2 =  _olng2 / _rad;              
                            var dlat = parseInt( 5280 * 3963 * ( Math.acos(  ( Math.sin(tlat1) * Math.sin(tlat2) ) + Math.cos(tlat1) * Math.cos(tlat2) * Math.cos( tlng1 - tlng1 ) ) ) );
                            var dlng = parseInt( 5280 * 3963 * ( Math.acos(  ( Math.sin(tlat1) * Math.sin(tlat1) ) + Math.cos(tlat1) * Math.cos(tlat1) * Math.cos( tlng2 - tlng1 ) ) ) );
                            
                            _cvs.forEach((vt, ix) => {
                                var gp = createNewPreviewPolygon();
                                var gp_path = [];

                                var vector = { points: "", label: "" };
                                vector.label = vt.label;

                                var points = []
                                var _nn = vt.startNode;
                                var _vlat = _olat;
                                var _vlng = _olng;
                                var pString = ''; 

                                while (_nn != null) {
                                    var _dx = 0, _dy = 0;
                                    for (var pi in _nn.path) {
                                        var _p = _nn.path[pi];
                                        var sd = _p.dist;
                                        switch (_p.dir) {
                                            case "U":
                                                _dy = sd;
                                                continue;
                                            case "D":
                                                _dy = -sd;
                                                continue;
                                            case "L":
                                                _dx = -sd;
                                                continue;
                                            case "R":
                                                _dx = sd;
                                                continue;
                                        }
                                    }
                                    _vlat = (_dy / dlat) + _vlat;
                                    _vlng = (_dx / dlng) + _vlng;
                                    var latlng = { lat: _vlat, lng: _vlng };

                                    gp_path.push(latlng);

                                    pString += _vlng + ',' + _vlat + ',0\n';

                                    _nn = _nn.nextNode;
                                }
                                if (pString != '') {
                                    vector.points = pString;
                                    sketchPoly.vectors.push(vector);

                                    if (testMode || lightWeight) {
                                        try {
                                            gp.setPath(gp_path);
                                            gp.setMap(map);
                                            previewPolygons.push(gp);
                                        } catch (e) {
                                            console.error(e);
                                        }

                                    }
                                }

                            });
                        }

                        _polygons.push(sketchPoly)
                    }, sk_index.toString(), parseInt(sky).toString());

                }
            }

        }

    }

    if (_msoCallback) _msoCallback(_polygons);
}

function setPositionOverlayIfDataExist(multisketch) {
    var position, type;
    if (currentOverlayPosition && selectedSection) {
		var skRowuid = top.$('.sketch-select option[value="' + selectedSketch + '"]').attr('skrowuid');
		var skPrefix = top.$('.sketch-select option[value="' + selectedSketch + '"]').attr('skprefix');
		skPrefix = isNewSketchData && skPrefix? skPrefix: '';
		var newSelectedSketch = isNewSketchData? skRowuid: selectedSketch;
        if (currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix] === undefined || _.isEmpty(currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix])){
            currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix] = {};
            currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection] = {
                "position": polygonCenter,
                "sketchZoom": 0.6,
                "sketchRotation": 0,
                "mapZoom": 20
            }
        }
        sketchOB = currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection]
        if (sketchOB) position = LatLngToPoint(sketchOB.position);
    } else {
        position = LatLngToPoint(polygonCenter);
    }
    if (!position) return;
    var x = parseFloat(position.x);
    var y = parseFloat(position.y);
    var mapH = parseFloat($('#map').height());
    var divW = parseFloat($('#resizable-wrapper').css("width")) / 2;
    var divH = parseFloat($('#resizable-wrapper').css("height")) / 2;
    y = y - mapH - divH;
    x = (x - divW);

    ParcelCenter = PointToLatLng(new google.maps.Point(position.x, position.y));

    if (!adjusting) {
        adjustHandle.setPosition(getAdjHandlePosition())
        adjustHandleOrigin = getCenterOfOverlay();
        //divW = divW > 140 ? 140 : divW;
        //divH = divH > 140 ? 140 : divH;
        //var ahp = PointToLatLng(new google.maps.Point(position.x + divW - 150, position.y + divH - 150));
        //adjustHandle.setPosition(ahp);
        //adjustHandleOrigin = LatLngToPoint(ParcelCenter)
    }
    if (sketchOB && (multisketch || firstTimeLoad)) {
		var mZoom = sketchOB.mapZoom? sketchOB.mapZoom: null;
        rotateSketch(sketchOB.sketchRotation, true);
        zoomSketch(sketchOB.sketchZoom, true, null, null, mZoom);
        drawFlag = true;
        google.maps.event.trigger(container, 'mouseup');
    }
}

function setPageDimensions() {
    $('#map').height($(window).height());
    $('#map').width($(window).width());
    try {
        setPositionOverlayIfDataExist();
    } catch (err) { }
}

function zoomSketch(zoom, noSketchpostionChange, changeAll, Change, mZoom) {
    if (isNaN(zoom))
        return;
    zoom = Number(zoom) > 2.5 ? 2.5: ( Number(zoom) < 0.1 ? 0.1: zoom); //zoom value showing in zoom div 250.  
    scale = zoom * 2;
    $('#sketch').width(parseInt($('.ccse-img').attr('width')) * zoom);
    $('#sketch').height(parseInt($('.ccse-img').attr('height')) * zoom);
    $('#resizable-wrapper').width(parseInt($('.ccse-img').attr('width')) * zoom);
    $('#resizable-wrapper').height(parseInt($('.ccse-img').attr('height')) * zoom);
    if (!noSketchpostionChange) setPositionOverlayIfDataExist();
    $('.sketch-zoom-value', window.parent.document).html(parseInt(parseFloat(zoom).toFixed(2) * 100) + '%');
    $('.sketch-resize', window.parent.document).val(zoom);
    activeParcel.sketchZoom = zoom;
    if (selectedSection) { 
		var skRowuid = top.$('.sketch-select option[value="' + selectedSketch + '"]').attr('skrowuid');
		var skPrefix = top.$('.sketch-select option[value="' + selectedSketch + '"]').attr('skprefix');
		skPrefix = isNewSketchData && skPrefix? skPrefix: '';
		var newSelectedSketch = isNewSketchData? skRowuid: selectedSketch;
		currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection].sketchZoom = zoom;
		if (mZoom) currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection].mapZoom = mZoom;
	}
    if (!changeAll) return
    var sk = sketchRenderer.sketches;
    sk.forEach(function (s, i) {
		var skRowuid = (s.parentRow && s.parentRow.ROWUID)? s.parentRow.ROWUID: s.sid;
        var skPrefix = (s.config && s.config.SketchLabelPrefixInSv)? s.config.SketchLabelPrefixInSv: '';
        i = isNewSketchData? skRowuid: i;
		skPrefix = isNewSketchData? skPrefix: '';
		for (var key in s.sections) {
            if (currentOverlayPosition["sketch-" + i + skPrefix]) {
                if (currentOverlayPosition["sketch-" +  i + skPrefix][key]) {
                    var keyZoom = currentOverlayPosition["sketch-" + i + skPrefix][key].sketchZoom;
                    if (Change == 'OUT') currentOverlayPosition["sketch-" + i + skPrefix][key].sketchZoom = keyZoom / 2;
                    else if (Change == 'IN') currentOverlayPosition["sketch-" + i + skPrefix][key].sketchZoom = keyZoom * 2;
                    currentOverlayPosition["sketch-" + i + skPrefix][key].modified = true;
                    if (mZoom) currentOverlayPosition["sketch-" + i + skPrefix][key].mapZoom = mZoom;
                }
            }
        }
    });
}

function rotateSketch(deg, fastrotate) {
    if (isNaN(deg))
        return;
    degree = deg;
    if (fastrotate) {
        $('#sketch').rotate({
            animateTo: deg,
            duration: 10
        });
        fastrotate = false;
    }
    else {
        $('#sketch').rotate({
            animateTo: deg,
            duration: 1000
        });
    }
    $('.sketch-rotate', window.parent.document).val(degree);
    $('.sketch-rotate-value', window.parent.document).html(degree + '&deg;');
    if (selectedSection) {
		var skRowuid = top.$('.sketch-select option[value="' + selectedSketch + '"]').attr('skrowuid');
		var skPrefix = top.$('.sketch-select option[value="' + selectedSketch + '"]').attr('skprefix');
		skPrefix = isNewSketchData && skPrefix? skPrefix: '';
		var newSelectedSketch = isNewSketchData? skRowuid: selectedSketch;
        currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection].sketchRotation = degree;
        currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection].modified = true;
    };
}

function saveForm(comment, flagChecked, validChecked) {
    $.ajax({
        url: '/sv/saveForm.jrq',
        method: 'POST',
        data: {
            parcel: Parcelid,
            exist: ParcelDataExist,
            comment: comment,
            flag: flagChecked,
            valid: validChecked
        },
        success: function (response) { }
    });
}

function getParcelCenter(doNotSetParcelCenter) {
    var map_width = parseFloat($("#map").css("width"));
    var map_height = parseFloat($("#map").css("height"));
    var left = parseFloat($("#resizable-wrapper").css("left"));
    var top = parseFloat($("#resizable-wrapper").css("top"));
    var x = left + parseFloat($('#sketch').css("width")) / 2;
    var y = map_height + top + parseFloat($('#sketch').css("height")) / 2;
    if (doNotSetParcelCenter)
        return PointToLatLng(new google.maps.Point(x, y));
    ParcelCenter = PointToLatLng(new google.maps.Point(x, y));
    return ParcelCenter;
}

function AnchorSketch() {

    var map_width = parseFloat($("#map").css("width"));
    var map_height = parseFloat($("#map").css("height"));
    var left = parseFloat($("#resizable-wrapper").css("left"));
    var top = parseFloat($("#resizable-wrapper").css("top"));
    var x = left + parseFloat($('#sketch').css("width")) / 2;
    var y = map_height + top + parseFloat($('#sketch').css("height")) / 2;
    var canvasSize = parseFloat($('.ccse-img').attr('width'));
    var sketZoom = parseFloat($('#sketch').css("width")) / parseFloat($('.ccse-img').attr('width'));
    var mapZoom = map.zoom;
    ParcelCenter = PointToLatLng(new google.maps.Point(x, y));

    $.ajax({
        url: '/sv/updatesketch.jrq',
        method: 'POST',
        data: {
            parcel: Parcelid,
            exist: ParcelDataExist,
            degree: degree,
            canvasSize: canvasSize,
            sketchZoom: sketZoom,
            mapZoom: mapZoom,
            lat: ParcelCenter.lat(),
            lng: ParcelCenter.lng()

        },
        success: function (resp) {
            new Messi('Sketch anchored.', {
                autoclose: 700
            });
        }
    });
}

function MeasurementScale(div, map) {
    var ms = this;
    globalms = ms;
    this.control = div;
    this.on = false;
    this.start = null;
    this.end = null;

    var mA, mB, line;
    var markIcon = new google.maps.MarkerImage("http://maps.google.com/mapfiles/ms/icons/red-dot.png", new google.maps.Size(27, 28));
    mA = new google.maps.Marker({
        icon: markIcon
    });
    mB = new google.maps.Marker({
        icon: markIcon
    });
    line = new google.maps.Polyline({
        strokeColor: 'Yellow'
    });

    this.clear = function () {
        this.start = null;
        this.end = null;
        this.updateGraphics();
        label.innerHTML = '0.0ft';
    }

    this.updateGraphics = function () {
        mA.setMap(null);
        mB.setMap(null);
        line.setMap(null);

        if (ms.start != null) {
            mA.setPosition(ms.start);
            mA.setMap(map);
        }

        if (ms.end != null) {
            mB.setPosition(ms.end);
            mB.setMap(map);
        }

        if ((ms.start != null) && (ms.end != null)) {
            line.setPath([ms.start, ms.end]);
            line.setMap(map);
            label.innerHTML = Math.round(ms.distance() * 10) / 10 + 'ft';
        }
    }

    div.className = 'measurement-tool measure-off tip';
    $(div).attr({
        "abbr": 'Click on the measuring tool to activate it and measure. Click again to deactivate the tool.',
        "width": '400px;'
    });

    var button = document.createElement('span');
    button.className = 'button';
    div.appendChild(button);

    var label = document.createElement('span');
    label.className = 'distance';
    div.appendChild(label);
    label.innerHTML = '0.0ft'

    google.maps.event.addDomListener(button, 'click', function (e) {
        if (ms.on) {
        	if(typeof prevOverlayZindex !== 'undefined' && prevOverlayZindex != '' && $('.overlay').length){
            			$('.overlay')[0].style.zIndex = prevOverlayZindex;
            			$('.overlay')[0].style.opacity = 1;
            			$('.overlay').parent('div')[0].style.cursor = 'default'; 
            		}
            map.setOptions({
                draggableCursor: null
            });
            div.className = 'measurement-tool measure-off tip';
            $(div).attr({
                "abbr": 'Click on the measuring tool to activate it and measure. Click again to deactivate the tool.',
                "width": '400px;'
            });
            $('.overlay').removeClass('pointer')
            ms.clear();
        } else {
            map.setOptions({
                draggableCursor: 'crosshair'
            });
            div.className = 'measurement-tool measure-on tip';
            $(div).attr({
                "abbr": 'Click on the measuring tool to activate it and measure. Click again to deactivate the tool.',
                "width": '400px;'
            });
            ms.clear();
            $('.overlay').addClass('pointer')
        }

        ms.on = !ms.on;
        e.preventDefault();
    });

    this.distance = function () {
        if ((ms.start != null) && (ms.end != null)) {
            return google.maps.geometry.spherical.computeDistanceBetween(ms.start, ms.end) * 3.28084;
        } else {
            return 0;
        }
    }

    //this.__defineGetter__("distance", );

}
var currentOverlayPosition = {};

function setCurrentOverlapPos() {
    //isSketchChanged = true;
	var skRowuid = top.$('.sketch-select option[value="' + selectedSketch + '"]').attr('skrowuid');
	var skPrefix = top.$('.sketch-select option[value="' + selectedSketch + '"]').attr('skprefix');
	skPrefix = isNewSketchData && skPrefix? skPrefix: '';
	var newSelectedSketch = isNewSketchData? skRowuid: selectedSketch;
    if (newSelectedSketch && selectedSection && currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection])
        currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection].position = getParcelCenter(true);
}