﻿
CAMACloud.Sketching.Formatters.RapidSketch = {
    name: "RapidSketch",
    saveAllSegments: false,
    allowSegmentAddition: false,
    allowSegmentDeletion: false,
    arcMode: 'ARC',
    vectorSeperator: ";",
    newVectorHeaderSuffix: '[-1,-1]:',
    nodeToString: function (n) {
        var str = "";
        if (n.arcLength > 0) {
            str += "A" + sRound(n.arcLength);
        }
        if (n.arcLength < 0) {
            str += "B" + Math.abs(sRound(n.arcLength));
        }
        if (n.dy > 0) {
            if (n.isStart) {
                if (str != "") str += " ";
            } else {
                if (str != "") str += "/";
            }
            str += n.sdy + sRound(n.dy);
        }
        if (n.dx > 0) {
            if (n.isStart) {
                if (str != "") str += " ";
            } else {
                if (str != "") str += "/";
            }
            str += n.sdx + sRound(n.dx);
        }
        if (n.hideDimensions) {
            if (str != "") str += "/";
            str += "#";
        }
        if (n.isStart) {
            str += " S";
        }
        return str;
    },
    vectorToString: function (v) {
        var str = "";
        if (v.startNode != null) {
            if (!v.labelEdited) {
                if (v.header) {
                    str += v.header;
                } else {
                    if (v.referenceIds) {
                        str += v.name + "[" + v.referenceIds + "]:";
                    } else {
                        if (v.rowId)
                            str += v.name + "[" + v.rowId + ",-1]:";
                        else
                            str += v.name + "[-1,-1]:";
                    }

                };
            }
            else {
                str += v.name + "[" + v.referenceIds + "]:";
            }
            str += v.startNode.vectorString();
            var nn = v.startNode.nextNode;
            while (nn != null) {
                str += " ";
                str += nn.vectorString();
                nn = nn.nextNode;
            }
        }
        return str;
    },
    vectorFromString: function (editor, s) {
        if ((s || '').trim() == '') {
            var nv = new Vector(editor);
            nv.isClosed = false;
            nv.label = "Blank"
            nv.uid = -1;
            return nv;
        }

        var o = editor.origin;
        var p = o.copy();

        var v = new Vector(editor);
        this.updateVectorFromString(editor, v, s);
        v.isModified = false;
        return v;
    },
    updateVectorFromString: function (editor, v, s) {
        v.startNode = null;
        v.endNode = null;


        var o = editor.origin;
        var p = o.copy();

        var hi = s.split(':');
        var head = hi[0];
        var lineString = hi[1] || '';

        v.header = head + ":";

        var isStart = false;
        var hasStarted = false;
        var isVeer = false;
        var veerSteps = 0;

        var nodeStrings = lineString.split(' ');
        for (var i in nodeStrings) {
            var arcLength = 0;
            var hideDimensions = false;
            var ns = nodeStrings[i];
            if (ns.trim() != '') {
                var cmds = ns.split('/');
                for (var c in cmds) {
                    var cmd = cmds[c];
                    if (cmd == '') continue;
                    if (cmd == 'S') {
                        isStart = true;
                    } else {
                        var cp = cmd.match(/[ABUDLR#]|[0-9]+(.[0-9]+)?/g);
                        var dir = cp[0];
                        distance = cp[1] || 0;
                        var pix = CAMACloud.Sketching.Utilities.toPixel(distance);
                        switch (dir) {
                            case "A": arcLength = Number(distance); break;
                            case "B": arcLength = -Number(distance); break;
                            case "U": p.moveBy(0, pix); break;
                            case "D": p.moveBy(0, -pix); break;
                            case "L": p.moveBy(-pix, 0); break;
                            case "R": p.moveBy(pix, 0); break;
                            case "#": hideDimensions = true; break;
                        }
                    }
                }

                p = p.copy();

                if (isStart) {
                    v.start(p.copy());
                    hasStarted = true;
                    isStart = false;
                } else if (hasStarted) {
                    var q = p.copy().alignToGrid(editor);
                    if (!v.startNode.overlaps(q))
                        v.connect(p.copy());
                    else
                        v.terminateNode(v.startNode);
                }

                if (v.endNode) {
                    v.endNode.hideDimensions = hideDimensions;
                    v.endNode.arcLength = arcLength;
                    if (arcLength != 0)
                        v.endNode.isArc = true;
                }

            }
        }

        if (v.startNode)
            if (v.startNode.overlaps(v.endNode.p.copy()))
                v.isClosed = true;
        return v;
    },
    open: function (editor, data) {
        editor.vectors = [];
        editor.sketches = [];
        for (var x in data) {
            var sketch = new Sketch(editor);
            var s = data[x];
            sketch.parentRow = s.parentRow;
            sketch.uid = s.uid;
            sketch.label = s.label;
            sketch.vectors = [];
            sketch.config = s.config || {};
            sketch.sid = s.sid;
            var counter = 0;
            for (var i in s.sketches) {
                var sk = s.sketches[i];
                var segments = (sk.vector || '').split(';');
                var scount = 0;
                if (segments.length < 2) scount = -1;
                for (var si in segments) {
                    counter += 1;
                    scount += 1;
                    var sv = segments[si];
                    var v = this.vectorFromString(editor, sv);
                    v.sketch = sketch;
                    v.uid = sk.uid + (scount ? '/' + scount : '');
                    v.index = counter;
                    v.label = '[' + counter + '] ' + sk.label;
                    v.name = sk.label;
                    v.vectorString = sv;
                    v.referenceIds = sk.referenceIds;
                    v.rowId = sk.rowId;
                    v.isChanged = sk.isChanged;
                    editor.vectors.pop();
                    sketch.vectors.push(v);
                }
            }
            sketch.isModified = false;
            editor.loadNotesForSketch(sketch, s.notes);
            editor.sketches.push(sketch);
        }
    },
    getParts: function (vectorString) {
        var p = [];
        vectorString = Base64.decode((vectorString || '').substr(13));
        var parts = (vectorString || '').split(";");
        for (var i in parts) {
            var part = parts[i];
            if (/(.*?):(.*?)/.test(part)) {
                var hi = part.split(':');
                var head = hi[0];
                var info = hi[1];
                var labels, refString;

                var r1 = /(.*?)\[(.*?)\]/;
                var hparts = r1.exec(head);
                if (hparts == null) {
                    labels = "";
                    refString = "-1,-1";
                } else {
                    labels = hparts[1].replace(/\{(.*?)\}/g, '');
                    refString = hparts[2];
                }

                p.push({
                    label: labels,
                    vector: part,
                    referenceIds: refString
                })

            }
        }

        return p;
    },
    encodeSketch: function (data) {
        var ison = (new Date()).getTime().toString();
        var salt2 = ""
        for (var i = 0; i < ison.length; i++) { salt2 = ison[i] + salt2; };
        var salt = Base64.encode(salt2).substring(0, 13);
        return salt + Base64.encode(data);
    },
    decodeSketch: function (data) {
        return Base64.decode((data || '').substr(13));
    }
}