﻿
CAMACloud.Sketching.Formatters.Patriot = {
    name: "Patriot Sketch Format",
    saveAllSegments: true,
    allowSegmentAddition: true,
    allowSegmentDeletion: true,
    nodeToString: function (n) {
        return "Not Implemented";
    },
    vectorToString: function (v) {
        return "Not Implemented";
    },
    vectorFromString: function (editor, s) {
        return "Not Implemented";
    },
    open: function (editor, data) {

    }
}
