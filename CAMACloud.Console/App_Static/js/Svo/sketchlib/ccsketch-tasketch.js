﻿
CAMACloud.Sketching.Formatters.TASketch = {
    name: "PACS 8.0 Sketch Format",
    saveAllSegments: false,
    allowSegmentAddition: false,
    allowSegmentDeletion: false,
    arcMode: 'ELL',
    /// TASketch.nodeToString
    nodeToString: function (n) {
        var str = "";
        var cmd = "";
        if (n.commands.length > 0)
            cmd = n.commands.reduce(function (x, y) { return x + y });
        if (n.isEllipse) {
            if (n.dx == n.dy) {
                return "C" + sRound(n.dx) + cmd;
            } else {
                return "E" + sRound(n.dy) + "X" + sRound(n.dx) + cmd;
            }
        }
        if (n.isEllipseEndNode) {
            return null;
        }
        if (n.dx > 0) {
            if (n.isStart) str += "M";
            str += n.sdx + sRound(n.dx);
        }
        if (n.dy > 0) {
            if (!n.isVeer) if (str != "") str += ",";
            if (n.isStart) str += "M";
            if (n.isVeer) str += "/";
            str += n.sdy + sRound(n.dy);
        }
        return str + cmd;
    },

    /// TASketch.vectorToString
    vectorToString: function (v) {
        var str = "";
        if (v.startNode != null) {
            str += v.startNode.vectorString();
            var nn = v.startNode.nextNode;
            while (nn != null) {
                var vs = nn.vectorString()
                str += vs ? ("," + vs) : '';
                nn = nn.nextNode;
            }
        }
        if ((str.charAt(0) == ",") || (str.charAt(0) == " ")) {
            str = str.substr(1);
        }

        var cmd = "";
        if (v.commands.length > 0)
            cmd = v.commands.reduce(function (x, y) { return x + "," + y }) + ",";

        return cmd + str;
    },

    /// TASketch.vectorFromString
    vectorFromString: function (editor, s) {
        if ((s == null) || (s == "")) {
            var nv = new Vector(editor);
            nv.isClosed = false;
            nv.label = "Blank"
            nv.uid = -1
            return nv;
        }
        if (s.charAt(0) != "M") {
            s = "MR0," + s;
        }
        var path = s.split(",");
        var o = editor.origin;
        var p = o.copy();

        var v = new Vector(editor);

        var count = path.length;
        var index = -1;
        for (var x in path) {
            index++;
            var step = path[x].toUpperCase();
            var isStart = false;
            var isVeer = false;
            var sl = step.length;
            var cmd = step.substr(sl - 2);
            if (["DD", "DM", "DL", "DP", "CD"].indexOf(cmd) > -1) {
                step = step.substr(0, sl - 2)
            } else {
                cmd = null;
            }

            if ((step.substr(0, 2) == "XA") || (step.substr(0, 2) == "XS") || (step.substr(0, 2) == "XC")) {
                v.commands.push(step);
            }
            else if (step == "X") {
                v.commands.push(step);
            }
            else if (step.substr(0, 2) == "RX") {
                var dim = step.substr(2);
                var parts = splitString(dim, ["x", "X", "/"]);
                var w = parseFloat(parts[0]) * DEFAULT_PPF;
                var h = parseFloat(parts[1]) * DEFAULT_PPF;
                p = p.copy(); p.moveBy(w, 0); v.connect(p.copy()); if (cmd) v.endNode.commands.push(cmd);  //R
                p = p.copy(); p.moveBy(0, h); v.connect(p.copy()); if (cmd) v.endNode.commands.push(cmd);  //U
                p = p.copy(); p.moveBy(-w, 0); v.connect(p.copy()); if (cmd) v.endNode.commands.push(cmd); //L
                p = p.copy(); p.moveBy(0, -h); v.connect(p.copy()); if (cmd) v.endNode.commands.push(cmd); //D
            } else if (step.substr(0, 1) == "E") {
                var dim = step.substr(1);
                var parts = splitString(dim, ["x", "X", "/"]);
                var h = parseFloat(parts[0]) * DEFAULT_PPF;
                var w = parseFloat(parts[1]) * DEFAULT_PPF;
                p = p.copy(); p.moveBy(0, h);
                p = p.copy(); p.moveBy(w, 0);
                v.connect(p.copy()); v.endNode.isEllipse = true; if (cmd) v.endNode.commands.push(cmd);
                p = p.copy(); p.moveBy(0, -h);
                p = p.copy(); p.moveBy(-w, 0);
                v.connect(p.copy()); v.endNode.isEllipseEndNode = true;
            } else if (step.substr(0, 1) == "C") {
                var d = parseFloat(step.substr(1)) * DEFAULT_PPF;
                p = p.copy(); p.moveBy(0, d);
                p = p.copy(); p.moveBy(d, 0);
                v.connect(p.copy()); v.endNode.isEllipse = true; if (cmd) v.endNode.commands.push(cmd);
                p = p.copy(); p.moveBy(0, -d);
                p = p.copy(); p.moveBy(-d, 0);
                v.connect(p.copy()); v.endNode.isEllipseEndNode = true;
            } else if (step.substr(0, 1) == "B") {
                var d = parseFloat(step.substr(1)) * DEFAULT_PPF;
                p = p.copy(); p.moveBy(0, d); v.connect(p.copy()); if (cmd) v.endNode.commands.push(cmd);
                p = p.copy(); p.moveBy(d, 0); v.connect(p.copy()); if (cmd) v.endNode.commands.push(cmd);
                p = p.copy(); p.moveBy(0, -d); v.connect(p.copy()); if (cmd) v.endNode.commands.push(cmd);
                p = p.copy(); p.moveBy(-d, 0); v.connect(p.copy()); if (cmd) v.endNode.commands.push(cmd);
            } else {
                if ((index == 0) || (step.charAt(0) == "M") || (step.charAt(0) == "X")) {
                    isStart = true;
                    step = step.substr(1);
                }
                if (splitString(step, ["x", "X", "/"]) > 1) {
                    isVeer = true;
                }
                var parts = splitString(step, ["x", "X", "/"]);
                for (sx in parts) {
                    var dd = parts[sx];
                    var tdira = dd.match(/^([A-Za-z])+/g) || [];
                    if (tdira.length > 0) {
                        var dir = tdira[0];
                        var dist = dd.substr(dir.length);
                        var pix = dist * DEFAULT_PPF;
                        switch (dir) {
                            case "DU": case "U": p.moveBy(0, pix); break;
                            case "DD": case "D": p.moveBy(0, -pix); break;
                            case "DL": case "L": p.moveBy(-pix, 0); break;
                            case "DR": case "R": p.moveBy(pix, 0); break;
                        }
                        p = p.copy();
                    }
                    //                    var dir = dd.charAt(0);
                    //                    var dist = parseFloat(dd.substr(1));

                }
                if (isStart) {
                    v.start(p.copy());
                } else {
                    var q = p.copy().alignToGrid(editor);
                    if (!v.startNode.overlaps(q))
                        v.connect(p.copy(), false, true);
                    else
                        v.terminateNode(v.startNode);
                }
                if (cmd) v.endNode.commands.push(cmd);

            }


        }

        v.isModified = false;
        if (v.startNode)
            if (v.startNode.overlaps(v.endNode.p.copy()))
                v.isClosed = true;
        return v;
    },

    /// TASketch.open
    open: function (editor, data) {
        editor.vectors = [];
        editor.sketches = [];
        for (var x in data) {
            var sketch = new Sketch(editor);
            var s = data[x];
            sketch.parentRow = s.parentRow;
            sketch.uid = s.uid;
            sketch.label = s.label;
            sketch.vectors = [];
            sketch.config = s.config || {};
            sketch.sid = s.sid;
            var counter = 0;
            for (var i in s.sketches) {
                counter += 1;
                var sk = s.sketches[i];
                var v = this.vectorFromString(editor, sk.vector);
                v.sketch = sketch;
                v.uid = sk.uid;
                v.index = counter;
                v.isChanged = sk.isChanged;
                v.label = '[' + counter + '] ' + sk.label;
                v.name = sk.label;
                v.vectorString = sk.vector;
                editor.vectors.pop();
                sketch.vectors.push(v);
            }
            sketch.isModified = false;
            editor.loadNotesForSketch(sketch, s.notes);
            editor.sketches.push(sketch);
        }
    }
}
