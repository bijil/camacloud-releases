﻿var iResize, iRotate;

function setPageLayout(w, h) {
	$('.screen').height(h);
	$('#mapFrame').height(h+2);
	$('#search-map').height(h+2 );
	$('#search-map').css({ 'overflow': 'hidden' });
	$('#mapFrame').css({ 'overflow': 'hidden' });
	$('#divContentArea').css({ 'overflow': 'hidden' });
	
	w1=$('#divSearchCntnr').width();
	w2=$('#divRghtCntnr').width();
	w3=w-w1;
	//$('.screen').width(w-w1-16);
	x=$("body").height();
	y=$(window).height();
	
	if($('#divSearchCntnr').is(':hidden') && $('#divRghtCntnr').is(':hidden'))
	 {
	    $('#search-map').width(w);
	    $('.resultframe').width(w-6);
	 }
	 else if($('#divSearchCntnr').is(':hidden'))
	   {
	 	$('#search-map').width(w-w2);
	    $('.resultframe').width(w-w2);
	    $('#mapFrame').width(w-w2);
	   }
	else if ($('#divSearchCntnr').is(':visible') && $('#divRghtCntnr').is(':visible'))
	{
		$('#search-map').width(w-w1-w2);
	    $('.resultframe').width(w-w2-w1);
	    $('#mapFrame').width(w-w2-w1);
    }
    else {
    	$('#search-map').width(w-w1);
	    $('.resultframe').width(w-w1-6);
     }		
	$('#divSearchBtm').height(h - $('#divSearchTop').height() - 15);
	if (currentSketch != null) paintSketch();
}

function setPageFunctions() {
	iResize = new Slider(document.getElementById("sketch-resize"), document.getElementById("sketch-resize-input"));
	closeParcel();
	loadSearchMap();
	$('.screen').hide();
	$('#sketch-validation').show();
}
