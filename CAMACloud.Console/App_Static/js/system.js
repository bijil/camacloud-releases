﻿function __(o) {
    if (window.console)
        console.log(o);
}

window.onerror = function (msg, url, line, ex) {
    if (msg.indexOf('Authentication failed.') > -1) {
        window.location.reload();
        return;
    }
    console.error(msg);
    if (msg == "TypeError: 'undefined' is not an object") { return false; }
    if (msg == "TypeError: 'undefined' is not a constructor") {
        if (!serviceUnavailabilityWarned) {
            alert('Some of the online services may be blocked as you are working in offline mode. ');
            serviceUnavailabilityWarned = true;
        }
        return false;
    }
    url = url.substr(url.lastIndexOf("/") + 1);
    if (url.indexOf('?') > -1)
        url = url.substr(url, url.indexOf('?'));
    alert(msg + "\n\nSource: " + url + " @ line " + line);

}

function $$$(url, data, callback, errorCallback) {
    $.ajax({
        url: url,
        type: "POST",
        dataType: 'json',
        data: data,
        success: function (data, status) {
            //			if (data.LoginStatus)
            //				if (data.LoginStatus == "401") {
            //					window.location.href = "/auth/?return=" + escape(window.location.pathname + window.location.search);
            //					return false;
            //				}
            //			if (data.LoginStatus == "403") {
            //				window.location.href = "/auth/?return=" + escape(window.location.pathname + window.location.search);
            //				return false;
            //			}
            if (data && data.failed) {
                if (errorCallback) {
                    errorCallback(data.message);
                    console.warn(data);
                }
                else
                    console.log(data);
            } else
                if (callback) callback(data, status);
        }
        , error: function (xhr, ajaxOptions, thrownError) {
            if (errorCallback)
                errorCallback(thrownError);
            else
                console.log(xhr);
        }
    });
}

function $ds(keyword, data, callback, errorCallback) {
    $$$('/datasetup/' + keyword + '.jrq', data, callback, errorCallback);
}

function $qc(keyword, data, callback, errorCallback) {
    if (__DTR)
        if (data)
            data.__dtr = 1;
        else
            data = { __dtr: 1 };
    $$$('/quality/' + keyword + '.jrq', data, callback, function (x) {
        if (errorCallback) errorCallback(x)
        else {
            showMask('An error occured!', true);
            alert(x);
        }
    });
}

function $dtr(keyword, data, callback, errorCallback) {
    $$$('/dtr/' + keyword + '.jrq', data, callback, errorCallback);
}

function $par(keyword, data, callback, errorCallback) {
    $$$('/parcels/' + keyword + '.jrq', data, callback, errorCallback);
}

window.__defineGetter__('Query', function () { return new URL(location.href).searchParams })
window.__defineGetter__('QueryData', function () { var x = atob(Query.get("d") || ""); return x ? JSON.parse(x) : {}; })
function URLPack(d) { return "d=" + btoa(JSON.stringify(d)); }

jQuery.fn.extend({
    bindEvent: function (events, callback) {
        $(this).off(events).on(events, callback);
    },
    tagName: function () {
        return lower(this[0].nodeName);
    },
    copy: function (x) {
        this[0].innerHTML = $(x)[0].innerHTML;
    },
    selectOne: function (a, v, c) {
        this.each(function (i, x) {
            if ($(x).attr(a) == v) { $(x).addClass(c); } else { $(x).removeClass(c); }
        })
    },
    spanCells: function (ci, l) {
        this.each(function (i, tr) {
            var cells = $('td', tr);
            if (ci + l > cells.length) { l = cell.length - ci; }
            var replace = []; for (var n = 1; n < l; n++) { replace.push(ci + n); }
            replace.map(function (x) { cells[x].remove(); });
            cells[ci].colSpan = l;
        })
    },
    contentHeight: function () {
        return $(this).height() +
            parseInt($(this).css('padding-top').replace('px', '')) +
            parseInt($(this).css('padding-bottom').replace('px', ''))
    },
    clientHeight: function () {
        return $(this).contentHeight() +
            parseInt($(this).css('border-top-width').replace('px', '')) +
            parseInt($(this).css('border-bottom-width').replace('px', ''))
    },
    contentWidth: function () {
        return $(this).width() +
            parseInt($(this).css('padding-left').replace('px', '')) +
            parseInt($(this).css('padding-right').replace('px', ''))
    },
    clientWidth: function () {
        return $(this).contentWidth() +
            parseInt($(this).css('border-left-width').replace('px', '')) +
            parseInt($(this).css('border-right-width').replace('px', ''))
    },
    box: function () {
        var b = {
            left: $(this).position().left,
            top: $(this).position().top,
            width: $(this).clientWidth(),
            height: $(this).clientHeight()
        };
        b.right = b.left + b.width, b.bottom = b.top + b.height;
        return b;
    },
    delayTrigger: function (e, t) {
        var _c = this;
        window.setTimeout(function () { $(_c).trigger(e) }, t || 300);
    },
    $: function (a) {
        return $(a, this);
    },
    fill: function (d, options) {
        this.each(function (i, tr) {
            loadSelect(tr, d, options);
        });
    },
    isIn: function (s) {
        return this.parents(s).length > 0;
    }
});

function loadSelect(s, d, options) {
    options = options || {};
    var defaults = {
        idField: 'ID',
        textField: 'Name',
        showSelect: true,
        selectText: '-- Select --',
        selectValue: -1
    }
    for (var x in defaults) { options[x] = options[x] === undefined ? defaults[x] : options[x]; }
    var opl = [];
    if (options.showSelect) {
        opl.push('<option value="' + options.selectValue + '">' + options.selectText + '</option>')
    }
    d.forEach(function (o) {
        var opt = '<option value="' + o[options.idField] + '">' + o[options.textField] + '</option>';
        opl.push(opt);
    })
    $(s).html(opl.join(''));
}

function formatNumber(v) {
    v = parseFloat(v);
    if (!isNaN(v)) {
        if (v > 1000000) {
            v = v.toExponential(3);
        } else {
            v = v.toFixed(4);
        }
    }
    return v;
}

function showWait(enable) {
    $('body').css({ cursor: ((enable === undefined) || true) ? 'wait': null });
}