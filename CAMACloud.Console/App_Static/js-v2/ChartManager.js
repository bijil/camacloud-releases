﻿Chart.defaults.global.defaultFontFamily = 'Segoe UI, Arial, Helvetica, sans-serif';

function ChartManager() {
    var _cm = this;

    var _charts = {};
    var _instances = {};

    Object.defineProperties(this, {
        'charts': { value: _charts, writable: false },
        'instances': { value: _instances, writable: false }
    });

    this.createChart = function (id, context, type, options) {

        var c = new ChartInstance(context, type, options);
        _charts[id] = c.chart;
        _instances[id] = c;

        return _instances[id];
    }

    this.setData = function (id, label, data, options) {
        _instances[id].chart.data.datasets = [];
        if(id == 'chart1') _instances[id].addTrendLine(options.dataX, options.dataY, options);
        _instances[id].addData(label, data, options);
        _instances[id].refresh();
    }

    this.update = function (id) {
        _instances[id].refresh();
    }

    this.clear = function (id) {
        _charts[id].data.datasets = [];
        _instances[id].refresh();
    }

    this.addData = function (id, label, data, options) {
        _instances[id].addData(label, data, options);
    }


    this.clearAll = function () {
        for (var x in _instances) {
            c = _instances[x];
            if(c.chart && c.chart.config) c.chart.config.options.scales.xAxes[0].type = "linear"
            c.data.labels = [];
            c.clear();
        }
    }

    this.refreshAll = function () {
        for (var x in _instances) {
            c = _instances[x];
            c.refresh();
        }
    }
}

function ChartInstance(context, type, options) {
    var c, _data = [];

    var _paletteColors = [[141, 31, 31], [141, 86, 31], [141, 141, 31], [86, 141, 31], [31, 141, 31], [31, 141, 86]];
    var _backgroundColors = _paletteColors.map(function (x) { return 'rgba(' + x.toString() + ',0.5)'; });
    var _borderColors = _paletteColors.map(function (x) { return 'rgba(' + x.toString() + ',1)'; })
    var _oncanvasclick = () => { };

    var defaultOptions = {
        responsive: true,
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        },
        legend: {
            labels: {
                fontColor: 'black',
                fontSize: 14,
                boxWidth: 12
            }
        }
    };

    var defaultDataOptions = {
        backgroundColor: _backgroundColors,
        borderColor: _borderColors,
        borderWidth: 1,
        pointStyle: 'rectRot',
        pointRadius: 5,
        pointBorderColor: 'rgb(0, 0, 0)'
    }
    function verifyOptions(o, d) {
        o = o || {};
        for (var x in d) {
            if (o[x] === undefined) o[x] = d[x];
        }
        return o;
    }

    options = verifyOptions(options, defaultOptions);
    c = new Chart(context, {
        type: type,
        data: {
            labels: options.labels,
            datasets: []
        },
        options: options
    });

    c.canvas.onclick = (e) => { _oncanvasclick && _oncanvasclick(c.getElementAtEvent(e), c, c.canvas); };

    Object.defineProperties(this, {
        chart: { value: c, writable: false },
        data: { value: _data, writable: false },
        onclick: {
            set: v => { if (v && (typeof v == "function")) { _oncanvasclick = v } },
            get: () => { return _oncanvasclick ? "eventhandler" : null; }
        }
    })

    this.addData = (label, data, options) => {
        var lookupValues = [];
        var lookupLabels = [];
        if(c.id == 0 && options && options.datatype == 5 && options.lookupValues.length > 0) {
        	lookupValues = options.lookupValues;
        	lookupLabels = options.lookupLabels;
        	c.config.options.layout.padding.right = 40;
        	c.config.options.scales.xAxes[0].type = "category";
        	var lookups = [];
        	for(let i=0;i<lookupValues.length;i++) {
			    lookups.push(lookupValues[i] + " (" + lookupLabels[i] + ")");
			}
        	c.data.labels = lookups;
        	for(let i=0; i<data.length; i++) {
		        for(let j=0; j<lookupValues.length; j++) {
		            if(data[i].x == lookupValues[j]) {
		                data[i].x+= " ("+ lookupLabels[j]+")";
	                }
		        }
		    }
		    _data.push(data);
        	c.update();
        	options = verifyOptions(options, defaultDataOptions);
        	c.data.datasets.push({
	            label: label,
	            data: data,
	            backgroundColor: options.backgroundColor,
	            borderColor: options.borderColor,
	            borderWidth: options.borderWidth
	        });
        } else {
        	_data.push(data);
        	if(c.id == 0) {
        		c.config.options.layout.padding.right = 40;
        		c.config.options.scales.xAxes[0].type = "linear";
        		c.update();
        	}
	        options = verifyOptions(options, defaultDataOptions);
	        c.data.datasets.push({
	            label: label,
	            data: data,
	            backgroundColor: options.backgroundColor,
	            borderColor: options.borderColor,
	            borderWidth: options.borderWidth
	        });
        }
        return this;
    }

    this.addTrendLine = function (dataX, dataY, options) {
        options = verifyOptions(options, defaultDataOptions);
        c.options.labels = dataX;
        c.data.datasets.push({
            type: 'line',
            label: 'Trend Line',
            showLine: true,
            fill: false,
            data: [{x: dataX[0], y: dataY[0] }, {x: dataX[1], y: dataY[1]}],
            backgroundColor: options.lineColor,
            borderColor: options.lineColor,
            borderWidth: options.lineWidth,
            radius: 0
        });
        c.update();
    }

    this.clear = () => {
        _data = [];
        c.data.datasets = [];
        c.update();
    }

    this.refresh = () => {
        c.update();
    }

    c.clear();

    Object.freeze(this);
}