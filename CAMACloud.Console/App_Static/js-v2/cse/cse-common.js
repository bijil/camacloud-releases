﻿function enableLayout() {
    $("body").css({ cursor: null });
    $(".d1001").removeAttr("disabled");
    $(".shade").hide();
}

function disableLayout() {
    $(".d1001").attr("disabled", "disabled");
    $(".shade").show();
}


function setSearchPanel(applicationSettings, alternateKeyLabel) {
    $('.comp-clear').hide();
	$(".field1-name").text(!applicationSettings.ShowAlternateField ? applicationSettings.KeyField1 : alternateKeyLabel);
    $(".key-select-box").hide();
    $(".key-select-box").append(
        '<option value="keyValue1">' +
        (!applicationSettings.ShowAlternateField ? applicationSettings.KeyField1 : alternateKeyLabel) +
        "</option>"
    );
    if (applicationSettings.KeyField2) {
        $(".search-field2").css("display", "inline-grid");
        $(".field2-name").text(applicationSettings.KeyField2);
        $(".key-select-box").append('<option value="keyValue2">' + applicationSettings.KeyField2 + "</option>");
    } else {
        $(".search-field2").hide();
    }
    if (applicationSettings.KeyField3) {
        $(".search-field3").css("display", "inline-grid");
        $(".field3-name").text(applicationSettings.KeyField3);
        $(".key-select-box").append('<option value="keyValue3">' + applicationSettings.KeyField3 + "</option>");
    } else {
        $(".search-field3").hide();
    }
    if (!applicationSettings.KeyField2 && !applicationSettings.KeyField3) {
        $(".field1-name").text(!alternateKeyLabel ? applicationSettings.KeyField1 : alternateKeyLabel);
    } else {
        $(".field1-name").hide();
        $(".key-select-box").show();
    }
}

function getParcelPhoto(pId, imgTag) {
    PageMethods.GetParcelImage(pId, (d) => {
        let imgPath = "/App_Static/images/residential.png";
        if (d.Success && d?.Data?.Image) imgPath = d.Data.Image;
            if(imgTag.is("img"))
                imgTag.attr({ src : imgPath });
            else
                imgTag.css({ "background": 'url("' + imgPath + '") #fff no-repeat center', 'background-size': 'cover' });
    });
}

var imageFullscreenParcelId;
var linkOpenedInQc = false;
function allowZoom() {
	$('.zoom-image').off('click').on('click', (e) => {
	    $('.image-fullscreen').remove();
	    $('.image-close-fullscreen').remove();
	    $('.image-fullscreen-container').remove();
	    $('.image-fullscreen-div').remove();
	    var imageurl = "";
	    if($(e.currentTarget)[0].localName == "img") {
    		imageurl = $(e.currentTarget).attr('src');
		} else if($(e.currentTarget)[0].localName == "div") {
			imageurl = $(e.currentTarget).css('background-image').substring(5, $(e.currentTarget).css('background-image').length-2);
		}
	    $('#divContentArea').append('<div class="image-fullscreen-container"></div>');
	    $('.image-fullscreen-container').append('<div class="image-fullscreen-div"></div>');
	    $('.image-fullscreen-div').append('<div class="image-fullscreen-header"></div><img class="image-fullscreen"></img>');
    	$('.image-fullscreen-header').append('<div class="image-fullscreen-header-left"><img src="/App_Static/images/close-button.svg" class="image-close-fullscreen" style="padding: 9px;"/><p class="image-fullscreen-keyvalue"></p></div>');
	    if($(e.currentTarget).attr('pId')) {
			$('.image-fullscreen-header').append('<span class="open-in-qc">Open in QC</span>');
			imageFullscreenParcelId = $(e.currentTarget).attr('pId');
			if(!$(e.currentTarget).hasClass('rp__img')) {
				if(subjectParcel[imageFullscreenParcelId]) var keyValue = subjectParcel[imageFullscreenParcelId].KeyValue1;
				else if(comparableProperties[imageFullscreenParcelId]) var keyValue = comparableProperties[imageFullscreenParcelId].KeyValue1;
			}
			$('.image-fullscreen-keyvalue').html(keyValue);
		}
	    closeEvent();
	    openInQC();
	    $('.image-fullscreen').attr('src', imageurl);
	    e.stopPropagation();
	});
}

function openInQC() {
	$('.open-in-qc').off('click').on('click', (e) => {
	    e.preventDefault();
	    function openInNewTab(href) {
		  Object.assign(document.createElement('a'), {
			target: '_blank',
			href: href,
		  }).click();
		}
		if(!linkOpenedInQc){
			linkOpenedInQc = !linkOpenedInQc;
			openInNewTab("/protected/quality/#/parcel/" + imageFullscreenParcelId);
			setTimeout(function(){linkOpenedInQc = false},60000)
		}
		else {
			say("Opening in QC is disabled for multiple clicks in short time. Try again after completely loading the opened one.")
		}
		
	});
}

function closeEvent() {	
	$('.image-close-fullscreen').off('click').on('click', (e) => {
	    $('.image-fullscreen').remove();
	    $('.image-close-fullscreen').remove();
	    $('.image-fullscreen-container').remove();
	    $('.image-fullscreen-div').remove();
	});
	$('.image-fullscreen-container').off('click').on('click', (e) => {
		$('.image-close-fullscreen').click();
	});
	$('.image-fullscreen-div').off('click').on('click', (e) => {
		e.stopPropagation();
	});
}