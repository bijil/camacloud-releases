﻿var searchParams, comps;
var noOfComparbles = 0;
var compProps;
var subjectPId;
var uiFooter;
var estSalePrice;
var parcelList = "";
var mapVisible = false;
var parcelInfo;
var reportDataTable;
var qPublicEnabled, qKeyField, qFieldFormatter;
var organization, state;
var compList;
$(document).ready(function () {
    var queryParams = new URLSearchParams(window.location.search);
    comps = queryParams.get("c");
    searchParams = queryParams.get("searchparams");
    subjectPId = parseInt(queryParams.get("s")) ? queryParams.get("s") : "";
    parcelList = subjectPId + "," + comps;
    compProps = comps.split(",");
    compList = "";
    for (x in compProps) {
        if (compList != "") compList += ",";
        if (parseInt(compProps[x])) compList += compProps[x];
        noOfComparbles += 1;
    }
    var srchPrmsLength = searchParams.split(",").length;
    estSalePrice = searchParams.split(",")[srchPrmsLength - 1];
    estSalePrice = parseInt(estSalePrice);
    if (subjectPId != "" && compList != "") {
        loadReport(subjectPId, compList, estSalePrice);
    }
    $(".switch-button-checkbox").on("change", e => {
    	$('.gm-style-iw-t').hide();
        if ($(e.target).prop("checked")) {
            mapVisible = true;
            $(".mapview").show();
            $(".report-table-div").hide();
            setMapView();
        } else {
            OpenInMap = false;
            $(".mapview").hide();
            $(".report-table-div").show();
            mapVisible = false;
        }
    });
    initGoogleMap();

});

function loadReport(subject, comparables, estSale) {
    PageMethods.GetReportData(subject, comparables, estSale, d => {
        if (d.Success && d.Data) {
            renderReport(d.Data);
            checkFavStatus();
        } else sayError(d.ErrorMessage);
    });
}

function renderReport(data) {
    const rpHead = $(".rp__table thead"),
        rpBody = $(".rp__table tbody"),
        rpFoot = $(".rp__table tfoot");
    uiFooter = data.UIFooter[0];
    parcelInfo = data.ParcelsInfo;
    qPublicEnabled = data.qPublicEnabled;
    qKeyField = data.qKeyField;
    qFieldFormatter = data.qFieldFormatter;
    organization = data.organizationName;
    state = data.stateName;


    function renderTitleBadge() {
        if (estSalePrice != 1) {
            $(".report-status").text(uiFooter.Uniformity).addClass(uniformityClass()).show();
        }
        let saleData = data.SaleInfo;
        if (saleData && estSalePrice) {
            let text;
            if (saleData[1].Sale_Price) {
                text = saleData[1].Sale_Price + ' : <span style="font-weight:600">' + saleData[0].Sale_Price + '</span>';
            } else if (saleData[1].Column1) {
                text = saleData[1].Column1;
                $(".report-est-sale").addClass('badge--red');
            }
            $(".report-est-sale").show().html(text);
        }
        else {
            $(".report-est-sale").hide();
        }
    }

    function renderHead() {

        let hCat = $("<th>").attr({ class: "rp__fi" }).text("Fields");
        let hSub = $("<th>").attr({ class: "" }).text("Subject");
        let hRow = $("<tr>").attr({ class: "" }).append(hCat, hSub);
        let i = 1;
        for (x in compProps) {
            let rDiv = $("<div>").append($("<span>").text("Comp. #" + i + " (" + compProps[x] + ")").css({'max-width': '90%', 'overflow': 'hidden', 'text-overflow': 'ellipsis', 'white-space': 'pre'}).attr('title', "Comp. #" + i + " (" + compProps[x] + ")"));
            if (noOfComparbles > 5) {
                rDiv.append(
                    $("<span>").attr({
                        class: "close-property no-print",
                        style: "color:red;float:right;cursor:pointer",
                        onclick: 'removeCompfromReport("' + compProps[x] + '")'
                    }).text('X')
                );
            }
            rDiv.css('display','flex').css('justify-content', 'space-between');
            let tth = $("<th>").append(rDiv);
            hRow.append(tth);
            i++;
        }
        //headers += `<tr class="qPublic"><td colspan="2" class="label"></td></tr>`
        rpHead.append(hRow);
    }

    function renderBody() {
        //To make sure photo row is added as first row
        let pRow = $('<tr>').attr({ class: 'photo-row' })
        rpBody.append(pRow)
        renderImages(pRow);
        //render QPublic
        if ((qPublicEnabled == "1" || qPublicEnabled == "true") && qKeyField != '') {
            let qRow = $('<tr>').attr({ class: 'qPublic no-print' })
            rpBody.append(qRow)
            renderQPublic(qRow);
        }
        //Render Comparables
        let catData = data.Report;
        for (x in catData) {
            let row = catData[x];

            //Category
            if (row.IsFirstItem) {
                let catRow = $('<tr>')
                    .attr({ class: 'rp__cat-row', catId: row.CategoryId, collapse: '0' })
                    .append($('<td>').attr({ class: 'rp__cat' }).text(row.Category))

                // NOTE : +1 TO INCLUDE SUBJECT PARCEL TOO
                for (let i = 1; i <= noOfComparbles + 1; i++) {
                    catRow.append($('<td>'))
                }
                rpBody.append(catRow)
            }

            let rLabel = $("<td>").attr({ class: "rp__fi" }).text(row.Label);   //Fields
            let rSubject = $("<td>").attr({ class: "" }).html(row.Subject || '');
            let dRow = $('<tr>').attr({ class: "rp__field-row", cid: row.CategoryId }).append(rLabel, rSubject);
            //Comparable Data
            for (let ni = 1; ni <= noOfComparbles; ni++) {
                let rComp = $('<td>')
                let comp = row['Comp' + ni],
                    adj = row['Adj' + ni]
                if (adj) {
                    rComp.attr({ class: 'rp__adj-row' }).append(
                        $('<span>').attr({ class: 'rp__comp-val' }).html(comp || ''),
                        $('<span>').attr({ class: 'rp__adj-val' }).html(adj))
                    $('.rp__comp-val', rComp).attr({ title: $('.rp__comp-val', rComp).text() })
                    $('.rp__adj-val', rComp).attr({ title: $('.rp__adj-val', rComp).text() })
                    if (adj.toString().includes('class=\"high\"')) rComp.addClass('positive');
                    else if (adj.toString().includes('class=\"low\"')) rComp.addClass('negative');
                } else rComp = $('<td>').html(comp || '')
                dRow.append(rComp)
            }

            rpBody.append(dRow);
        }
    }

    function renderImages(pRow) {
        let pField = $("<td>").attr({ class: "rp__fi" }).text("Photo");

        //Subject Photo
        let subPhoto = $('<img>').attr({ class: "zoom-image rp__img rp__img--" + subjectPId, pId : subjectPId });
        getParcelPhoto(subjectPId, subPhoto);
        let pSub = $("<td>").attr({ class: "" }).html(subPhoto);

        pRow.append(pField, pSub);
        //Comparables Photo
        for (x in compProps) {
            let pId = compProps[x];
            let tPhoto = $('<img>').attr({ class: "zoom-image rp__img rp__img--" + pId, pId : pId });
            getParcelPhoto(pId, tPhoto);
            let tpSub = $("<td>").attr({ class: "" }).html(tPhoto);
            pRow.append(tpSub);
        }
        allowZoom();
    }

    function renderQPublic(qRow) {
        let pField = $("<td>").attr({ class: "rp__fi" }).text("QPublic");

        //Subject QPublic
        let subQLink = $('<a>').attr({ href: '#' }).html("Open in qPublic");
        getQPublicLinkData(subjectPId, subQLink);
        let subQPublic = $('<td>').html(subQLink);
        qRow.append(pField, subQPublic);

        //Comparables qPublic
        for (x in compProps) {
            let pId = compProps[x];
            let compQLink = $('<a>').attr({ href: '#' }).html("Open in qPublic");
            getQPublicLinkData(pId, compQLink);
            let compQPublic = $('<td>').html(compQLink);
            qRow.append(compQPublic);
        }
    }

    function renderFooter() {
        let footerData = data.Footer;
        for (let x in footerData) {
            let foot = footerData[x];

            let fCat = $("<th>").attr({ class: "rp__fi" }).text(foot.Label);
            let fSub = $("<th>").attr({ class: "" }).text(foot.Subject || '');
            let fRow = $("<tr>").attr({ class: "" }).append(fCat, fSub);


            for (let ni = 1; ni <= noOfComparbles; ni++) {
                let comp = foot['Comp' + ni]
                fRow.append($('<th>').text(comp || ''))
            }

            rpFoot.append(fRow)
        }

        if (!uiFooter.HideUIF) {
            let uifCat = $("<th>").attr({ class: "rp__fi" }).text('Uniformity Indication');
            let uifSub = $("<th>").attr({ class: "" }).text(uiFooter.UniformityIndication || '');
            let uifRow = $("<tr>").attr({ class: "" }).append(uifCat, uifSub);
            for (let ni = 1; ni <= noOfComparbles; ni++) {
                //Empty cells to format the table
                uifRow.append($('<td>'))
            }
            rpFoot.append(uifRow);
        }

    }

    renderTitleBadge();
    renderHead();
    renderBody();
    renderFooter();
    reportDataTable = $(".rp__table").DataTable({
        paging: false,
        ordering: false,
        searching: false,
        bInfo: false
    });
    $('.rp__cat').on('click', function () {
        let cRow = $(this).parents('.rp__cat-row');
        if (cRow && cRow.length) collapseReportCat(cRow)
    })
}

function collapseReportCat(catRow) {
    if (!catRow) return;
    let cId = $(catRow).attr('catid')
    let flag = catRow.attr('collapse') == 0;
    catRow.attr('collapse', flag ? 1 : 0);
    $('.rp__field-row[cid="' + cId + '"]').attr('hidden', flag);
    if(flag) 
        $('.rp__cat-row[catid="' + cId + '"]').addClass("no-print") 
    else 
        $('.rp__cat-row[catid="' + cId + '"]').removeClass("no-print");

}

function getQPublicLinkData(pId, qRoq) {
    PageMethods.GetQPublicLink(pId, (d) => {
        if (d.Success) {
            let qData = d.Data && d.Data.QData && d.Data.QData[0];
            let qPublicKey = eval("qData." + qKeyField + "")
            qRoq.attr({
                onclick: `openQpubliclink('${qPublicKey}')`
            })
        }
    });
}

function openQpubliclink(keyvalue) {
    var keyField = (qFieldFormatter && qFieldFormatter.trim() != "") ? formatField(qFieldFormatter, keyvalue) : keyvalue;
    var url = "https://qpublic.schneidercorp.com/application.aspx?app=" + organization + 'County' + state + "&layer=Parcels&PageType=Report&KeyValue=" + encodeURIComponent(keyField);
    //var url ="https://qpublic.schneidercorp.com/application.aspx?app=GilmerCountyGA&layer=Parcels&PageType=Report&KeyValue=1081%20%20%20080"
    window.open(url, '_blank');
}

function uniformityClass() {
    switch (uiFooter.UniformityId) {
        case 1:
            return "badge--green";
        case 2:
            return "badge--red";
        case 3:
            return "badge--yellow";
        default: return "";
    }
}

function isTipEnabled(label) {
    label = label.trim();
    var outt = " ";
    if ((label = "Median Sale for Comps")) {
        outt =
            "tip specialCase' abbr='Median Sale is the median sale price for the applicable parcel. i.e. - if it had more than 1 sale in the last few years. e.g., if Comp 2 had 2 sales of $50,000 and another of $61,000, then what would show in its column would be $55,500. If there was a 3rd of $70,000... then $61,000 would be displayed for the Median Sale value.'";
    }
    return outt;
}

function redirectToSearch() {
    var url = "default.aspx?jtc=1&c=" + comps + "&searchParams=" + searchParams
    window.location.href = url;
}

function downloadReport(){
    var url = location.pathname + "/exportreport";
    url += '?&s=' + subjectPId + '&c=' + compList + '&e=' + estSalePrice 
    location.href = url;
}

function printPage() {
    $('.reportTable').css('width', '100%');
    if (OpenInMap) {
        var body = $('body'),
            mapContainer = $('#gmap'),
            mapContainerParent = mapContainer.parent(),
            printContainer = $('<div>');
        body.prepend(printContainer);
        $("#gmap").css("width", '1500px')
        $("#gmap").css("height", '100%')
        printContainer.addClass('print-container').css('position', 'relative').height($(window).height()).append(mapContainer);
        var content = body.children().not('script').not(printContainer).detach();
        window.print();
        body.prepend(content);
        mapContainerParent.prepend(mapContainer);
        printContainer.remove();
        $("#gmap").css("width", '');
        $("#gmap").css("height", '');
    } else {
        var repOne = $('.rp__table').clone(),repTwo = $('.rp__table').clone(),repThree = $('.rp__table').clone();
        $('tr>th:nth-child(n+8):nth-child(-n+17), tr>td:nth-child(n+8):nth-child(-n+17)',repOne).remove();
        $('tr>th:nth-child(-n+7):not(:first-child), tr>td:nth-child(-n+7):not(:first-child)',repTwo).remove();
        $('tr>th:nth-child(n+8):nth-child(-n+17), tr>td:nth-child(n+8):nth-child(-n+17)',repTwo).remove();
        $('tr>th:nth-child(-n+13):not(:first-child), tr>td:nth-child(-n+13):not(:first-child)',repThree).remove();
        var win = window.open('', '', 'height=700,width=700');
        win.document.write('<html><head><link rel="stylesheet" type="text/css" href="/App_Static/css-v2/cse/cse-common.css?t=10" /><link rel="stylesheet" type="text/css" href="/App_Static/css-v2/cse/comp-report.css?t=15" /><link rel="stylesheet" type="text/css" href="/App_Static/css-v2/cse/report-lib.css?t=5" /><script type="text/javascript" src="/App_Static/jslib/jquery.js"></script>');
        win.document.write('<script>$(document).ready(function () { $(".rp__table").css("width","auto");$(".rp__table").css("margin-bottom","50p");window.print();window.close();})</script></head><body><h1>Comparability Report View</h1>')
        win.document.write($('.result-title-panel').html(),repOne[0].outerHTML,$('tr>th:nth-child(2)',repTwo).length > 0 ? repTwo[0].outerHTML:'',$('tr>th:nth-child(2)',repThree).length > 0 ? repThree[0].outerHTML:'');
        win.document.write('</body></html>');
        win.document.close();
        

        $('.reportTable').css('width', 'auto');
    }
}

function removeCompfromReport(pId) {
    ask('Are you sure you want to remove this Parcel from the Comparable Report ?', function () {
        loading.show('Removing Parcel. Please wait');
        var queryParams = new URLSearchParams(window.location.search);
        var comps = queryParams.get("c");
        var arr = comps.split(",");
        var index = arr.indexOf(pId.toString());
        if (index != -1) arr.splice(index, 1);
        comps = arr.join(",");
        queryParams.set("c", comps);
        history.replaceState(null, null, "?" + queryParams.toString());
        location.reload();
    });
}

function addTofav(){
	 var queryParams = new URLSearchParams(window.location.search);
	 var subject = queryParams.get("s");
	 var comps = queryParams.get("c");
	 var searchQuery = queryParams.get("searchparams");
	 searchQuery = searchQuery.split(",");
	 var searchTerm = `${searchQuery[0]},${searchQuery[1]},${searchQuery[2]}`
	 var srchPrms = `${searchQuery[3]},${searchQuery[4]},${searchQuery[5]},${searchQuery[6]}`
	 PageMethods.AddToFavourite(subject, searchTerm, srchPrms, comps, function(d){
	 	if(d.Data){
	 		$('.addTo-Fav').text('Unpin Report')
	 	}
	 	else {
	 	 	$('.addTo-Fav').text('Pin Report')
	 	 }
	 });
}

function checkFavStatus(){
	 var queryParams = new URLSearchParams(window.location.search);
	 var subject = queryParams.get("s");
	 var comps = queryParams.get("c");
	 var searchQuery = queryParams.get("searchparams");
	 searchQuery = searchQuery.split(",");
	 var searchTerm = `${searchQuery[0]},${searchQuery[1]},${searchQuery[2]}`
	 var srchPrms = `${searchQuery[3]},${searchQuery[4]},${searchQuery[5]},${searchQuery[6]}`
	 PageMethods.checkIfFaved(subject, searchTerm, srchPrms, comps, function(d){
	 	if(d.Data){
	 		$('.addTo-Fav').text('Unpin Report')
	 	}
	 	else {
	 	 	$('.addTo-Fav').text('Pin Report')
	 	 }
	 });
}

function initGoogleMap(callback) {
    mapZoom = 5;
    mapPosition = new google.maps.LatLng(42.345573, -71.098326);
    mapCenter = new google.maps.LatLng(35.56, -96.84);
    var mapOptions = {
        zoom: mapZoom,
        minZoom: 7,
        mapTypeId: google.maps.MapTypeId.HYBRID,
        center: mapCenter,
        gestureHandling: "greedy",
        fullscreenControl: false,
        rotateControlOptions: {
      		position: google.maps.ControlPosition.LEFT_BOTTOM,
    	}
    };
    map = new google.maps.Map(document.getElementById("gmap"), mapOptions);
    //console.log('map initialized');
}

var parcelsInMap = [];
var parcelInfo = [];
var OpenInMap = false;
var firstLoad = true;
var bounds = new google.maps.LatLngBounds();
function setMapView() {
    OpenInMap = true;
    if (!firstLoad) {
        if (map && bounds) {
            map.setCenter(bounds.getCenter());
            map.fitBounds(bounds);
            //map.setZoom(15);
            //setPageSize();
            return;
        }
    }
    PageMethods.GetComparablesMapPoints(parcelList, d => {
        if (d.Success) {
            var res = d.Data.Points;
            parcelsIn = res;
            var parcels = [];
            var parcel;
            // var infowindow = new google.maps.InfoWindow();
            var icon = [
                "/App_Themes/Cloud/images/parcelUncheck.png",
                "/App_Themes/Cloud/images/compParcel.png",
                "/App_Themes/Cloud/images/subParcel.png"
            ];
            for (x in res) {
                var point = res[x];
                point.ParcelId.toString() == subjectPId
                    ? (point.isSubject = "1")
                    : (point.isSubject = "0");
                var pid = point.ParcelId.toString();
                if (parcelsInMap[pid] == null)
                    parcelsInMap[pid] = {
                        ParcelId: point.ParcelId,
                        Keyvalue1: point.Keyvalue1,
                        addToCompare: false,
                        isSubject: point.isSubject,
                        Distance: 0.0,
                        Location: null,
                        infoSub: null,
                        Points: []
                    };
                parcelsInMap[pid].Points.push(point.Points);
            }
            var totalParcels = Object.keys(parcelsInMap).length;
            var pinfo = [];
            for (x in parcelsInMap) {
                parcel = parcelsInMap[x];
                var b = new google.maps.LatLngBounds();
                pinfo = parcelInfo.filter(function (p) {
                    return p.Parcelid == parcelsInMap[x].ParcelId;
                });
                parcel.infoSub =
                    pinfo.length > 0 ? 
                        '<h3 style="margin: 0px 6px 7px 1px;"><b style="margin: 0px; padding:0px;" title="' + pinfo[0].keyvalue1 + '">' + pinfo[0].keyvalue1 + '</b></h3>' +
                        '<div><div style="display: flex"><p class="map-info-head">Address </p><b class="map-info-data" title="' + (pinfo[0].AddressLine ? pinfo[0].AddressLine.replaceAll(/\s\s+/g, ' ') : " --") + '">: ' + (pinfo[0].AddressLine ? pinfo[0].AddressLine.replaceAll(/\s\s+/g, ' ') : " --") + ' </b></div>' +
                        '<div style="display: flex"><p class="map-info-head">Sale Amount </p><b class="map-info-data" title="' + (pinfo[0].SaleAmount ? pinfo[0].SaleAmount : " --") + '">: ' + (pinfo[0].SaleAmount ? pinfo[0].SaleAmount : " --") + ' </b></div>' + 
                        '<div style="display: flex"><p class="map-info-head">Appraised Value </p><b class="map-info-data" title="' + (pinfo[0].AppraisedValue ? pinfo[0].AppraisedValue : " --") + '">: ' + (pinfo[0].AppraisedValue ? pinfo[0].AppraisedValue : " --") + ' </b></div></div>'
                        : "No Data Found";
                for (i in parcel.Points) {
                    pstr = parcel.Points[i].split(",");
                    loc = new google.maps.LatLng(
                        parseFloat(pstr[0].replace("[", "")),
                        parseFloat(pstr[1].replace("[", ""))
                    );
                    if (parcel.isSubject != "1") bounds.extend(loc);
                    b.extend(loc);
                }

                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(
                        b.getCenter().lat(),
                        b.getCenter().lng()
                    ),
                    map: map,
                    icon: parcel.isSubject == "0" ? icon[1] : icon[2],
                    animation: google.maps.Animation.DROP
                });
                var info = new google.maps.InfoWindow();
                info.setContent(parcel.infoSub.toString());
                google.maps.event.addListener(map, 'click', (x) => {
				    info.close();
				});
				   
                //info.open(map, marker);
                google.maps.event.addListener(
                    marker,
                    "click",
                    (function (marker, parcel) {
                        return function () {
                            info.close();
                            info.setContent(parcel.infoSub.toString());
                            info.open(map, marker);
                        };
                    })(marker, parcel)
                );
            }
            var legend = document.getElementById("legend");
            //$('#mapView').append(legend);
            var div = document.createElement("div");
            legend.appendChild(div);
            var innerValue =
                "Estimate Sale Price:" +
                estSalePrice +
                " <br />" +
                "Uniformity Value:" +
                uiFooter.UniformityIndication;
            div.innerHTML = innerValue;
            map.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(legend);
            map.setCenter(bounds.getCenter());
            map.fitBounds(bounds);
            //map.setZoom(15);
            firstLoad = false;
        }
    });
}
