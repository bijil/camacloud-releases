﻿/*!
   Copyright 2010-2021 SpryMedia Ltd.

 This source file is free software, available under the following license:
   MIT license - http://datatables.net/license/mit

 This source file is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE. See the license files for details.

 For details please refer to: http://www.datatables.net
 FixedColumns 3.3.3
 ©2010-2021 SpryMedia Ltd - datatables.net/license
*/
(function (d) { "function" === typeof define && define.amd ? define(["jquery", "datatables.net"], function (v) { return d(v, window, document) }) : "object" === typeof exports ? module.exports = function (v, w) { v || (v = window); w && w.fn.dataTable || (w = require("datatables.net")(v, w).$); return d(w, v, v.document) } : d(jQuery, window, document) })(function (d, v, w, z) {
    var y = d.fn.dataTable, B, q = function (a, b) {
        var c = this; if (this instanceof q) {
            if (b === z || !0 === b) b = {}; var f = d.fn.dataTable.camelToHungarian; f && (f(q.defaults, q.defaults, !0), f(q.defaults,
                b)); a = (new d.fn.dataTable.Api(a)).settings()[0]; this.s = { dt: a, iTableColumns: a.aoColumns.length, aiOuterWidths: [], aiInnerWidths: [], rtl: "rtl" === d(a.nTable).css("direction") }; this.dom = { scroller: null, header: null, body: null, footer: null, grid: { wrapper: null, dt: null, left: { wrapper: null, head: null, body: null, foot: null }, right: { wrapper: null, head: null, body: null, foot: null } }, clone: { left: { header: null, body: null, footer: null }, right: { header: null, body: null, footer: null } } }; if (a._oFixedColumns) throw "FixedColumns already initialised on this table";
            a._oFixedColumns = this; a._bInitComplete ? this._fnConstruct(b) : a.oApi._fnCallbackReg(a, "aoInitComplete", function () { c._fnConstruct(b) }, "FixedColumns")
        } else alert("FixedColumns warning: FixedColumns must be initialised with the 'new' keyword.")
    }; d.extend(q.prototype, {
        fnUpdate: function () { this._fnDraw(!0) }, fnRedrawLayout: function () { this._fnColCalc(); this._fnGridLayout(); this.fnUpdate() }, fnRecalculateHeight: function (a) { delete a._DTTC_iHeight; a.style.height = "auto" }, fnSetRowHeight: function (a, b) {
            a.style.height =
                b + "px"
        }, fnGetPosition: function (a) { var b = this.s.dt.oInstance; if (d(a).parents(".DTFC_Cloned").length) { if ("tr" === a.nodeName.toLowerCase()) return a = d(a).index(), b.fnGetPosition(d("tr", this.s.dt.nTBody)[a]); var c = d(a).index(); a = d(a.parentNode).index(); return [b.fnGetPosition(d("tr", this.s.dt.nTBody)[a]), c, b.oApi._fnVisibleToColumnIndex(this.s.dt, c)] } return b.fnGetPosition(a) }, fnToFixedNode: function (a, b) {
            var c; b < this.s.iLeftColumns ? c = d(this.dom.clone.left.body).find("[data-dt-row=" + a + "][data-dt-column=" +
                b + "]") : b >= this.s.iRightColumns && (c = d(this.dom.clone.right.body).find("[data-dt-row=" + a + "][data-dt-column=" + b + "]")); return c && c.length ? c[0] : (new d.fn.dataTable.Api(this.s.dt)).cell(a, b).node()
        }, _fnConstruct: function (a) {
            var b = this; if ("function" != typeof this.s.dt.oInstance.fnVersionCheck || !0 !== this.s.dt.oInstance.fnVersionCheck("1.8.0")) alert("FixedColumns " + q.VERSION + " required DataTables 1.8.0 or later. Please upgrade your DataTables installation"); else if ("" === this.s.dt.oScroll.sX) this.s.dt.oInstance.oApi._fnLog(this.s.dt,
                1, "FixedColumns is not needed (no x-scrolling in DataTables enabled), so no action will be taken. Use 'FixedHeader' for column fixing when scrolling is not enabled"); else {
                this.s = d.extend(!0, this.s, q.defaults, a); a = this.s.dt.oClasses; this.dom.grid.dt = d(this.s.dt.nTable).parents("div." + a.sScrollWrapper)[0]; this.dom.scroller = d("div." + a.sScrollBody, this.dom.grid.dt)[0]; this._fnColCalc(); this._fnGridSetup(); var c, f = !1; d(this.s.dt.nTableWrapper).on("mousedown.DTFC", function (g) {
                    0 === g.button && (f = !0, d(w).one("mouseup",
                        function () { f = !1 }))
                }); d(this.dom.scroller).on("mouseover.DTFC touchstart.DTFC", function () { f || (c = "main") }).on("scroll.DTFC", function (g) { !c && g.originalEvent && (c = "main"); if ("main" === c || "key" === c) 0 < b.s.iLeftColumns && (b.dom.grid.left.liner.scrollTop = b.dom.scroller.scrollTop), 0 < b.s.iRightColumns && (b.dom.grid.right.liner.scrollTop = b.dom.scroller.scrollTop) }); var e = "onwheel" in w.createElement("div") ? "wheel.DTFC" : "mousewheel.DTFC"; 0 < b.s.iLeftColumns && (d(b.dom.grid.left.liner).on("mouseover.DTFC touchstart.DTFC",
                    function () { f || "key" === c || (c = "left") }).on("scroll.DTFC", function (g) { !c && g.originalEvent && (c = "left"); "left" === c && (b.dom.scroller.scrollTop = b.dom.grid.left.liner.scrollTop, 0 < b.s.iRightColumns && (b.dom.grid.right.liner.scrollTop = b.dom.grid.left.liner.scrollTop)) }).on(e, function (g) { c = "left"; b.dom.scroller.scrollLeft -= "wheel" === g.type ? -g.originalEvent.deltaX : g.originalEvent.wheelDeltaX }), d(b.dom.grid.left.head).on("mouseover.DTFC touchstart.DTFC", function () { c = "main" })); 0 < b.s.iRightColumns && (d(b.dom.grid.right.liner).on("mouseover.DTFC touchstart.DTFC",
                        function () { f || "key" === c || (c = "right") }).on("scroll.DTFC", function (g) { !c && g.originalEvent && (c = "right"); "right" === c && (b.dom.scroller.scrollTop = b.dom.grid.right.liner.scrollTop, 0 < b.s.iLeftColumns && (b.dom.grid.left.liner.scrollTop = b.dom.grid.right.liner.scrollTop)) }).on(e, function (g) { c = "right"; b.dom.scroller.scrollLeft -= "wheel" === g.type ? -g.originalEvent.deltaX : g.originalEvent.wheelDeltaX }), d(b.dom.grid.right.head).on("mouseover.DTFC touchstart.DTFC", function () { c = "main" })); d(v).on("resize.DTFC", function () { b._fnGridLayout.call(b) });
                var h = !0, k = d(this.s.dt.nTable); k.on("draw.dt.DTFC", function () { b._fnColCalc(); b._fnDraw.call(b, h); h = !1 }).on("key-focus.dt.DTFC", function () { c = "key" }).on("column-sizing.dt.DTFC", function () { b._fnColCalc(); b._fnGridLayout(b) }).on("column-visibility.dt.DTFC", function (g, m, l, n, r) { if (r === z || r) b._fnColCalc(), b._fnGridLayout(b), b._fnDraw(!0) }).on("select.dt.DTFC deselect.dt.DTFC", function (g, m, l, n) { "dt" === g.namespace && b._fnDraw(!1) }).on("position.dts.dt.DTFC", function (g, m) {
                    b.dom.grid.left.body && d(b.dom.grid.left.body).find("table").eq(0).css("top",
                        m); b.dom.grid.right.body && d(b.dom.grid.right.body).find("table").eq(0).css("top", m)
                }).on("destroy.dt.DTFC", function () { k.off(".DTFC"); d(b.dom.scroller).off(".DTFC"); d(v).off(".DTFC"); d(b.s.dt.nTableWrapper).off(".DTFC"); d(b.dom.grid.left.liner).off(".DTFC " + e); d(b.dom.grid.left.wrapper).remove(); d(b.dom.grid.right.liner).off(".DTFC " + e); d(b.dom.grid.right.wrapper).remove() }); this._fnGridLayout(); this.s.dt.oInstance.fnDraw(!1)
            }
        }, _fnColCalc: function () {
            var a = this, b = 0, c = 0; this.s.aiInnerWidths = []; this.s.aiOuterWidths =
                []; d.each(this.s.dt.aoColumns, function (f, e) {
                    e = d(e.nTh); if (e.filter(":visible").length) {
                        var h = e.outerWidth(); if (0 === a.s.aiOuterWidths.length) { var k = d(a.s.dt.nTable).css("border-left-width"); h += "string" === typeof k && -1 === k.indexOf("px") ? 1 : parseInt(k, 10) } a.s.aiOuterWidths.length === a.s.dt.aoColumns.length - 1 && (k = d(a.s.dt.nTable).css("border-right-width"), h += "string" === typeof k && -1 === k.indexOf("px") ? 1 : parseInt(k, 10)); a.s.aiOuterWidths.push(h); a.s.aiInnerWidths.push(e.width()); f < a.s.iLeftColumns && (b += h);
                        a.s.iTableColumns - a.s.iRightColumns <= f && (c += h)
                    } else a.s.aiInnerWidths.push(0), a.s.aiOuterWidths.push(0)
                }); this.s.iLeftWidth = b; this.s.iRightWidth = c
        }, _fnGridSetup: function () {
            var a = this._fnDTOverflow(); this.dom.body = this.s.dt.nTable; this.dom.header = this.s.dt.nTHead.parentNode; this.dom.header.parentNode.parentNode.style.position = "relative"; var b = d('<div class="DTFC_ScrollWrapper" style="position:relative; clear:both;"><div class="DTFC_LeftWrapper" style="position:absolute; top:0; left:0;" aria-hidden="true"><div class="DTFC_LeftHeadWrapper" style="position:relative; top:0; left:0; overflow:hidden;"></div><div class="DTFC_LeftBodyWrapper" style="position:relative; top:0; left:0; height:0; overflow:hidden;"><div class="DTFC_LeftBodyLiner" style="position:relative; top:0; left:0; overflow-y:scroll;"></div></div><div class="DTFC_LeftFootWrapper" style="position:relative; top:0; left:0; overflow:hidden;"></div></div><div class="DTFC_RightWrapper" style="position:absolute; top:0; right:0;" aria-hidden="true"><div class="DTFC_RightHeadWrapper" style="position:relative; top:0; left:0;"><div class="DTFC_RightHeadBlocker DTFC_Blocker" style="position:absolute; top:0; bottom:0;"></div></div><div class="DTFC_RightBodyWrapper" style="position:relative; top:0; left:0; height:0; overflow:hidden;"><div class="DTFC_RightBodyLiner" style="position:relative; top:0; left:0; overflow-y:scroll;"></div></div><div class="DTFC_RightFootWrapper" style="position:relative; top:0; left:0;"><div class="DTFC_RightFootBlocker DTFC_Blocker" style="position:absolute; top:0; bottom:0;"></div></div></div></div>')[0],
                c = b.childNodes[0], f = b.childNodes[1]; this.dom.grid.dt.parentNode.insertBefore(b, this.dom.grid.dt); b.appendChild(this.dom.grid.dt); this.dom.grid.wrapper = b; 0 < this.s.iLeftColumns && (this.dom.grid.left.wrapper = c, this.dom.grid.left.head = c.childNodes[0], this.dom.grid.left.body = c.childNodes[1], this.dom.grid.left.liner = d("div.DTFC_LeftBodyLiner", b)[0], b.appendChild(c)); if (0 < this.s.iRightColumns) {
                    this.dom.grid.right.wrapper = f; this.dom.grid.right.head = f.childNodes[0]; this.dom.grid.right.body = f.childNodes[1];
                    this.dom.grid.right.liner = d("div.DTFC_RightBodyLiner", b)[0]; f.style.right = a.bar + "px"; var e = d("div.DTFC_RightHeadBlocker", b)[0]; e.style.width = a.bar + "px"; e.style.right = -a.bar + "px"; this.dom.grid.right.headBlock = e; e = d("div.DTFC_RightFootBlocker", b)[0]; e.style.width = a.bar + "px"; e.style.right = -a.bar + "px"; this.dom.grid.right.footBlock = e; b.appendChild(f)
                } this.s.dt.nTFoot && (this.dom.footer = this.s.dt.nTFoot.parentNode, 0 < this.s.iLeftColumns && (this.dom.grid.left.foot = c.childNodes[2]), 0 < this.s.iRightColumns && (this.dom.grid.right.foot =
                    f.childNodes[2])); this.s.rtl && d("div.DTFC_RightHeadBlocker", b).css({ left: -a.bar + "px", right: "" })
        }, _fnGridLayout: function () {
            var a = this, b = this.dom.grid; d(b.wrapper).width(); var c = this.s.dt.nTable.parentNode.offsetHeight, f = this.s.dt.nTable.parentNode.parentNode.offsetHeight, e = this._fnDTOverflow(), h = this.s.iLeftWidth, k = this.s.iRightWidth, g = "rtl" === d(this.dom.body).css("direction"), m = function (l, n) {
                e.bar ? a._firefoxScrollError() ? 34 < d(l).height() && (l.style.width = n + e.bar + "px") : l.style.width = n + e.bar + "px" : (l.style.width =
                    n + 20 + "px", l.style.paddingRight = "20px", l.style.boxSizing = "border-box")
            }; e.x && (c -= e.bar); b.wrapper.style.height = f + "px"; 0 < this.s.iLeftColumns && (f = b.left.wrapper, f.style.width = h + "px", f.style.height = "1px", g ? (f.style.left = "", f.style.right = 0) : (f.style.left = 0, f.style.right = ""), b.left.body.style.height = c + "px", b.left.foot && (b.left.foot.style.top = (e.x ? e.bar : 0) + "px"), m(b.left.liner, h), b.left.liner.style.height = c + "px", b.left.liner.style.maxHeight = c + "px"); 0 < this.s.iRightColumns && (f = b.right.wrapper, f.style.width =
                k + "px", f.style.height = "1px", this.s.rtl ? (f.style.left = e.y ? e.bar + "px" : 0, f.style.right = "") : (f.style.left = "", f.style.right = e.y ? e.bar + "px" : 0), b.right.body.style.height = c + "px", b.right.foot && (b.right.foot.style.top = (e.x ? e.bar : 0) + "px"), m(b.right.liner, k), b.right.liner.style.height = c + "px", b.right.liner.style.maxHeight = c + "px", b.right.headBlock.style.display = e.y ? "block" : "none", b.right.footBlock.style.display = e.y ? "block" : "none")
        }, _fnDTOverflow: function () {
            var a = this.s.dt.nTable, b = a.parentNode, c = { x: !1, y: !1, bar: this.s.dt.oScroll.iBarWidth };
            a.offsetWidth > b.clientWidth && (c.x = !0); a.offsetHeight > b.clientHeight && (c.y = !0); return c
        }, _fnDraw: function (a) { this._fnGridLayout(); this._fnCloneLeft(a); this._fnCloneRight(a); d(this.dom.scroller).trigger("scroll"); null !== this.s.fnDrawCallback && this.s.fnDrawCallback.call(this, this.dom.clone.left, this.dom.clone.right); d(this).trigger("draw.dtfc", { leftClone: this.dom.clone.left, rightClone: this.dom.clone.right }) }, _fnCloneRight: function (a) {
            if (!(0 >= this.s.iRightColumns)) {
                var b, c = []; for (b = this.s.iTableColumns -
                    this.s.iRightColumns; b < this.s.iTableColumns; b++)this.s.dt.aoColumns[b].bVisible && c.push(b); this._fnClone(this.dom.clone.right, this.dom.grid.right, c, a)
            }
        }, _fnCloneLeft: function (a) { if (!(0 >= this.s.iLeftColumns)) { var b, c = []; for (b = 0; b < this.s.iLeftColumns; b++)this.s.dt.aoColumns[b].bVisible && c.push(b); this._fnClone(this.dom.clone.left, this.dom.grid.left, c, a) } }, _fnCopyLayout: function (a, b, c) {
            for (var f = [], e = [], h = [], k = 0, g = a.length; k < g; k++) {
                var m = []; m.nTr = d(a[k].nTr).clone(c, !1)[0]; for (var l = 0, n = this.s.iTableColumns; l <
                    n; l++)if (-1 !== d.inArray(l, b)) { var r = d.inArray(a[k][l].cell, h); -1 === r ? (r = d(a[k][l].cell).clone(c, !1)[0], e.push(r), h.push(a[k][l].cell), m.push({ cell: r, unique: a[k][l].unique })) : m.push({ cell: e[r], unique: a[k][l].unique }) } f.push(m)
            } return f
        }, _fnClone: function (a, b, c, f) {
            var e = this, h, k, g = this.s.dt; if (f) {
                d(a.header).remove(); a.header = d(this.dom.header).clone(!0, !1)[0]; a.header.className += " DTFC_Cloned"; a.header.style.width = "100%"; b.head.appendChild(a.header); var m = this._fnCopyLayout(g.aoHeader, c, !0); var l =
                    d(">thead", a.header); l.empty(); var n = 0; for (h = m.length; n < h; n++)l[0].appendChild(m[n].nTr); g.oApi._fnDrawHead(g, m, !0)
            } else { m = this._fnCopyLayout(g.aoHeader, c, !1); var r = []; g.oApi._fnDetectHeader(r, d(">thead", a.header)[0]); n = 0; for (h = m.length; n < h; n++) { var t = 0; for (l = m[n].length; t < l; t++)r[n][t].cell.className = m[n][t].cell.className, d("span.DataTables_sort_icon", r[n][t].cell).each(function () { this.className = d("span.DataTables_sort_icon", m[n][t].cell)[0].className }) } } this._fnEqualiseHeights("thead", this.dom.header,
                a.header); "auto" == this.s.sHeightMatch && d(">tbody>tr", e.dom.body).css("height", "auto"); null !== a.body && (d(a.body).remove(), a.body = null); a.body = d(this.dom.body).clone(!0)[0]; a.body.className += " DTFC_Cloned"; a.body.style.paddingBottom = g.oScroll.iBarWidth + "px"; a.body.style.marginBottom = 2 * g.oScroll.iBarWidth + "px"; null !== a.body.getAttribute("id") && a.body.removeAttribute("id"); d(">thead>tr", a.body).empty(); d(">tfoot", a.body).remove(); var C = d("tbody", a.body)[0]; d(C).empty(); if (0 < g.aiDisplay.length) {
                    h = d(">thead>tr",
                        a.body)[0]; for (k = 0; k < c.length; k++) { var x = c[k]; var p = d(g.aoColumns[x].nTh).clone(!0)[0]; p.innerHTML = ""; l = p.style; l.paddingTop = "0"; l.paddingBottom = "0"; l.borderTopWidth = "0"; l.borderBottomWidth = "0"; l.height = 0; l.width = e.s.aiInnerWidths[x] + "px"; h.appendChild(p) } d(">tbody>tr", e.dom.body).each(function (u) {
                            u = !1 === e.s.dt.oFeatures.bServerSide ? e.s.dt.aiDisplay[e.s.dt._iDisplayStart + u] : u; var D = e.s.dt.aoData[u].anCells || d(this).children("td, th"), A = this.cloneNode(!1); A.removeAttribute("id"); A.setAttribute("data-dt-row",
                                u); for (k = 0; k < c.length; k++)x = c[k], 0 < D.length && (p = d(D[x]).clone(!0, !0)[0], p.removeAttribute("id"), p.setAttribute("data-dt-row", u), p.setAttribute("data-dt-column", x), A.appendChild(p)); C.appendChild(A)
                        })
                } else d(">tbody>tr", e.dom.body).each(function (u) { p = this.cloneNode(!0); p.className += " DTFC_NoData"; d("td", p).html(""); C.appendChild(p) }); a.body.style.width = "100%"; a.body.style.margin = "0"; a.body.style.padding = "0"; g.oScroller !== z && (h = g.oScroller.dom.force, b.forcer ? b.forcer.style.height = h.style.height : (b.forcer =
                    h.cloneNode(!0), b.liner.appendChild(b.forcer))); b.liner.appendChild(a.body); this._fnEqualiseHeights("tbody", e.dom.body, a.body); if (null !== g.nTFoot) {
                        if (f) { null !== a.footer && a.footer.parentNode.removeChild(a.footer); a.footer = d(this.dom.footer).clone(!0, !0)[0]; a.footer.className += " DTFC_Cloned"; a.footer.style.width = "100%"; b.foot.appendChild(a.footer); m = this._fnCopyLayout(g.aoFooter, c, !0); b = d(">tfoot", a.footer); b.empty(); n = 0; for (h = m.length; n < h; n++)b[0].appendChild(m[n].nTr); g.oApi._fnDrawHead(g, m, !0) } else for (m =
                            this._fnCopyLayout(g.aoFooter, c, !1), b = [], g.oApi._fnDetectHeader(b, d(">tfoot", a.footer)[0]), n = 0, h = m.length; n < h; n++)for (t = 0, l = m[n].length; t < l; t++)b[n][t].cell.className = m[n][t].cell.className; this._fnEqualiseHeights("tfoot", this.dom.footer, a.footer)
                    } b = g.oApi._fnGetUniqueThs(g, d(">thead", a.header)[0]); d(b).each(function (u) { x = c[u]; this.style.width = e.s.aiInnerWidths[x] + "px" }); null !== e.s.dt.nTFoot && (b = g.oApi._fnGetUniqueThs(g, d(">tfoot", a.footer)[0]), d(b).each(function (u) {
                        x = c[u]; this.style.width = e.s.aiInnerWidths[x] +
                            "px"
                    }))
        }, _fnGetTrNodes: function (a) { for (var b = [], c = 0, f = a.childNodes.length; c < f; c++)"TR" == a.childNodes[c].nodeName.toUpperCase() && b.push(a.childNodes[c]); return b }, _fnEqualiseHeights: function (a, b, c) {
            if ("none" != this.s.sHeightMatch || "thead" === a || "tfoot" === a) {
                var f = b.getElementsByTagName(a)[0]; c = c.getElementsByTagName(a)[0]; a = d(">" + a + ">tr:eq(0)", b).children(":first"); a.outerHeight(); a.height(); f = this._fnGetTrNodes(f); b = this._fnGetTrNodes(c); var e = []; c = 0; for (a = b.length; c < a; c++) {
                    var h = f[c].offsetHeight;
                    var k = b[c].offsetHeight; h = k > h ? k : h; "semiauto" == this.s.sHeightMatch && (f[c]._DTTC_iHeight = h); e.push(h)
                } c = 0; for (a = b.length; c < a; c++)b[c].style.height = e[c] + "px", f[c].style.height = e[c] + "px"
            }
        }, _firefoxScrollError: function () { if (B === z) { var a = d("<div/>").css({ position: "absolute", top: 0, left: 0, height: 10, width: 50, overflow: "scroll" }).appendTo("body"); B = a[0].clientWidth === a[0].offsetWidth && 0 !== this._fnDTOverflow().bar; a.remove() } return B }
    }); q.defaults = { iLeftColumns: 1, iRightColumns: 0, fnDrawCallback: null, sHeightMatch: "semiauto" };
    q.version = "3.3.3"; y.Api.register("fixedColumns()", function () { return this }); y.Api.register("fixedColumns().update()", function () { return this.iterator("table", function (a) { a._oFixedColumns && a._oFixedColumns.fnUpdate() }) }); y.Api.register("fixedColumns().relayout()", function () { return this.iterator("table", function (a) { a._oFixedColumns && a._oFixedColumns.fnRedrawLayout() }) }); y.Api.register("rows().recalcHeight()", function () { return this.iterator("row", function (a, b) { a._oFixedColumns && a._oFixedColumns.fnRecalculateHeight(this.row(b).node()) }) });
    y.Api.register("fixedColumns().rowIndex()", function (a) { a = d(a); return a.parents(".DTFC_Cloned").length ? this.rows({ page: "current" }).indexes()[a.index()] : this.row(a).index() }); y.Api.register("fixedColumns().cellIndex()", function (a) {
        a = d(a); if (a.parents(".DTFC_Cloned").length) {
            var b = a.parent().index(); b = this.rows({ page: "current" }).indexes()[b]; a = a.parents(".DTFC_LeftWrapper").length ? a.index() : this.columns().flatten().length - this.context[0]._oFixedColumns.s.iRightColumns + a.index(); return {
                row: b, column: this.column.index("toData",
                    a), columnVisible: a
            }
        } return this.cell(a).index()
    }); y.Api.registerPlural("cells().fixedNodes()", "cell().fixedNode()", function () { return this.iterator("cell", function (a, b, c) { return a._oFixedColumns ? a._oFixedColumns.fnToFixedNode(b, c) : this.cell(b, c).node() }, 1) }); d(w).on("init.dt.fixedColumns", function (a, b) { if ("dt" === a.namespace) { a = b.oInit.fixedColumns; var c = y.defaults.fixedColumns; if (a || c) c = d.extend({}, a, c), !1 !== a && new q(b, c) } }); d.fn.dataTable.FixedColumns = q; return d.fn.DataTable.FixedColumns = q
});

/*!
   Copyright 2009-2021 SpryMedia Ltd.

 This source file is free software, available under the following license:
   MIT license - http://datatables.net/license/mit

 This source file is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE. See the license files for details.

 For details please refer to: http://www.datatables.net
 FixedHeader 3.1.9
 ©2009-2021 SpryMedia Ltd - datatables.net/license
*/
(function (e) { "function" === typeof define && define.amd ? define(["jquery", "datatables.net"], function (g) { return e(g, window, document) }) : "object" === typeof exports ? module.exports = function (g, h) { g || (g = window); h && h.fn.dataTable || (h = require("datatables.net")(g, h).$); return e(h, g, g.document) } : e(jQuery, window, document) })(function (e, g, h, p) {
    var k = e.fn.dataTable, r = 0, l = function (a, b) {
        if (!(this instanceof l)) throw "FixedHeader must be initialised with the 'new' keyword."; !0 === b && (b = {}); a = new k.Api(a); this.c = e.extend(!0,
            {}, l.defaults, b); this.s = { dt: a, position: { theadTop: 0, tbodyTop: 0, tfootTop: 0, tfootBottom: 0, width: 0, left: 0, tfootHeight: 0, theadHeight: 0, windowHeight: e(g).height(), visible: !0 }, headerMode: null, footerMode: null, autoWidth: a.settings()[0].oFeatures.bAutoWidth, namespace: ".dtfc" + r++, scrollLeft: { header: -1, footer: -1 }, enable: !0 }; this.dom = {
                floatingHeader: null, thead: e(a.table().header()), tbody: e(a.table().body()), tfoot: e(a.table().footer()), header: { host: null, floating: null, placeholder: null }, footer: {
                    host: null, floating: null,
                    placeholder: null
                }
            }; this.dom.header.host = this.dom.thead.parent(); this.dom.footer.host = this.dom.tfoot.parent(); a = a.settings()[0]; if (a._fixedHeader) throw "FixedHeader already initialised on table " + a.nTable.id; a._fixedHeader = this; this._constructor()
    }; e.extend(l.prototype, {
        destroy: function () { this.s.dt.off(".dtfc"); e(g).off(this.s.namespace); this.c.header && this._modeChange("in-place", "header", !0); this.c.footer && this.dom.tfoot.length && this._modeChange("in-place", "footer", !0) }, enable: function (a, b) {
            this.s.enable =
            a; if (b || b === p) this._positions(), this._scroll(!0)
        }, enabled: function () { return this.s.enable }, headerOffset: function (a) { a !== p && (this.c.headerOffset = a, this.update()); return this.c.headerOffset }, footerOffset: function (a) { a !== p && (this.c.footerOffset = a, this.update()); return this.c.footerOffset }, update: function () { var a = this.s.dt.table().node(); e(a).is(":visible") ? this.enable(!0, !1) : this.enable(!1, !1); this._positions(); this._scroll(!0) }, _constructor: function () {
            var a = this, b = this.s.dt; e(g).on("scroll" + this.s.namespace,
                function () { a._scroll() }).on("resize" + this.s.namespace, k.util.throttle(function () { a.s.position.windowHeight = e(g).height(); a.update() }, 50)); var d = e(".fh-fixedHeader"); !this.c.headerOffset && d.length && (this.c.headerOffset = d.outerHeight()); d = e(".fh-fixedFooter"); !this.c.footerOffset && d.length && (this.c.footerOffset = d.outerHeight()); b.on("column-reorder.dt.dtfc column-visibility.dt.dtfc draw.dt.dtfc column-sizing.dt.dtfc responsive-display.dt.dtfc", function () { a.update() }); b.on("destroy.dtfc", function () { a.destroy() });
            this._positions(); this._scroll()
        }, _clone: function (a, b) {
            var d = this.s.dt, c = this.dom[a], f = "header" === a ? this.dom.thead : this.dom.tfoot; !b && c.floating ? c.floating.removeClass("fixedHeader-floating fixedHeader-locked") : (c.floating && (c.placeholder.remove(), this._unsize(a), c.floating.children().detach(), c.floating.remove()), c.floating = e(d.table().node().cloneNode(!1)).css("table-layout", "fixed").attr("aria-hidden", "true").removeAttr("id").append(f).appendTo("body"), c.placeholder = f.clone(!1), c.placeholder.find("*[id]").removeAttr("id"),
                c.host.prepend(c.placeholder), this._matchWidths(c.placeholder, c.floating))
        }, _matchWidths: function (a, b) { var d = function (n) { return e(n, a).map(function () { return 1 * e(this).css("width").replace(/[^\d\.]/g, "") }).toArray() }, c = function (n, q) { e(n, b).each(function (m) { e(this).css({ width: q[m], minWidth: q[m] }) }) }, f = d("th"); d = d("td"); c("th", f); c("td", d) }, _unsize: function (a) {
            var b = this.dom[a].floating; b && ("footer" === a || "header" === a && !this.s.autoWidth) ? e("th, td", b).css({ width: "", minWidth: "" }) : b && "header" === a && e("th, td",
                b).css("min-width", "")
        }, _horizontal: function (a, b) { var d = this.dom[a], c = this.s.position, f = this.s.scrollLeft; d.floating && f[a] !== b && (d.floating.css("left", c.left - b), f[a] = b) }, _modeChange: function (a, b, d) {
            var c = this.dom[b], f = this.s.position, n = function (t) { c.floating.attr("style", function (v, u) { return (u || "") + "width: " + t + "px !important;" }) }, q = this.dom["footer" === b ? "tfoot" : "thead"], m = e.contains(q[0], h.activeElement) ? h.activeElement : null; m && m.blur(); "in-place" === a ? (c.placeholder && (c.placeholder.remove(), c.placeholder =
                null), this._unsize(b), "header" === b ? c.host.prepend(q) : c.host.append(q), c.floating && (c.floating.remove(), c.floating = null)) : "in" === a ? (this._clone(b, d), c.floating.addClass("fixedHeader-floating").css("header" === b ? "top" : "bottom", this.c[b + "Offset"]).css("left", f.left + "px"), n(f.width), "footer" === b && c.floating.css("top", "")) : "below" === a ? (this._clone(b, d), c.floating.addClass("fixedHeader-locked").css("top", f.tfootTop - f.theadHeight).css("left", f.left + "px"), n(f.width)) : "above" === a && (this._clone(b, d), c.floating.addClass("fixedHeader-locked").css("top",
                    f.tbodyTop).css("left", f.left + "px"), n(f.width)); m && m !== h.activeElement && setTimeout(function () { m.focus() }, 10); this.s.scrollLeft.header = -1; this.s.scrollLeft.footer = -1; this.s[b + "Mode"] = a
        }, _positions: function () {
            var a = this.s.dt.table(), b = this.s.position, d = this.dom; a = e(a.node()); var c = a.children("thead"), f = a.children("tfoot"); d = d.tbody; b.visible = a.is(":visible"); b.width = a.outerWidth(); b.left = a.offset().left; b.theadTop = c.offset().top; b.tbodyTop = d.offset().top; b.tbodyHeight = d.outerHeight(); b.theadHeight =
                b.tbodyTop - b.theadTop; f.length ? (b.tfootTop = f.offset().top, b.tfootBottom = b.tfootTop + f.outerHeight(), b.tfootHeight = b.tfootBottom - b.tfootTop) : (b.tfootTop = b.tbodyTop + d.outerHeight(), b.tfootBottom = b.tfootTop, b.tfootHeight = b.tfootTop)
        }, _scroll: function (a) {
            var b = e(h).scrollTop(), d = e(h).scrollLeft(), c = this.s.position; if (this.c.header) {
                var f = this.s.enable ? !c.visible || b <= c.theadTop - this.c.headerOffset ? "in-place" : b <= c.tfootTop - c.theadHeight - this.c.headerOffset ? "in" : "below" : "in-place"; (a || f !== this.s.headerMode) &&
                    this._modeChange(f, "header", a); this._horizontal("header", d)
            } this.c.footer && this.dom.tfoot.length && (b = this.s.enable ? !c.visible || b + c.windowHeight >= c.tfootBottom + this.c.footerOffset ? "in-place" : c.windowHeight + b > c.tbodyTop + c.tfootHeight + this.c.footerOffset ? "in" : "above" : "in-place", (a || b !== this.s.footerMode) && this._modeChange(b, "footer", a), this._horizontal("footer", d))
        }
    }); l.version = "3.1.9"; l.defaults = { header: !0, footer: !1, headerOffset: 0, footerOffset: 0 }; e.fn.dataTable.FixedHeader = l; e.fn.DataTable.FixedHeader =
        l; e(h).on("init.dt.dtfh", function (a, b, d) { "dt" === a.namespace && (a = b.oInit.fixedHeader, d = k.defaults.fixedHeader, !a && !d || b._fixedHeader || (d = e.extend({}, d, a), !1 !== a && new l(b, d))) }); k.Api.register("fixedHeader()", function () { }); k.Api.register("fixedHeader.adjust()", function () { return this.iterator("table", function (a) { (a = a._fixedHeader) && a.update() }) }); k.Api.register("fixedHeader.enable()", function (a) { return this.iterator("table", function (b) { b = b._fixedHeader; a = a !== p ? a : !0; b && a !== b.enabled() && b.enable(a) }) });
    k.Api.register("fixedHeader.enabled()", function () { if (this.context.length) { var a = this.context[0]._fixedHeader; if (a) return a.enabled() } return !1 }); k.Api.register("fixedHeader.disable()", function () { return this.iterator("table", function (a) { (a = a._fixedHeader) && a.enabled() && a.enable(!1) }) }); e.each(["header", "footer"], function (a, b) {
        k.Api.register("fixedHeader." + b + "Offset()", function (d) {
            var c = this.context; return d === p ? c.length && c[0]._fixedHeader ? c[0]._fixedHeader[b + "Offset"]() : p : this.iterator("table", function (f) {
                if (f =
                    f._fixedHeader) f[b + "Offset"](d)
            })
        })
    }); return l
});

/*!
   Copyright 2014-2021 SpryMedia Ltd.

 This source file is free software, available under the following license:
   MIT license - http://datatables.net/license/mit

 This source file is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE. See the license files for details.

 For details please refer to: http://www.datatables.net
 Responsive 2.2.9
 2014-2021 SpryMedia Ltd - datatables.net/license
*/
var $jscomp = $jscomp || {}; $jscomp.scope = {}; $jscomp.findInternal = function (b, k, m) { b instanceof String && (b = String(b)); for (var n = b.length, p = 0; p < n; p++) { var y = b[p]; if (k.call(m, y, p, b)) return { i: p, v: y } } return { i: -1, v: void 0 } }; $jscomp.ASSUME_ES5 = !1; $jscomp.ASSUME_NO_NATIVE_MAP = !1; $jscomp.ASSUME_NO_NATIVE_SET = !1; $jscomp.SIMPLE_FROUND_POLYFILL = !1; $jscomp.ISOLATE_POLYFILLS = !1;
$jscomp.defineProperty = $jscomp.ASSUME_ES5 || "function" == typeof Object.defineProperties ? Object.defineProperty : function (b, k, m) { if (b == Array.prototype || b == Object.prototype) return b; b[k] = m.value; return b }; $jscomp.getGlobal = function (b) { b = ["object" == typeof globalThis && globalThis, b, "object" == typeof window && window, "object" == typeof self && self, "object" == typeof global && global]; for (var k = 0; k < b.length; ++k) { var m = b[k]; if (m && m.Math == Math) return m } throw Error("Cannot find global object"); }; $jscomp.global = $jscomp.getGlobal(this);
$jscomp.IS_SYMBOL_NATIVE = "function" === typeof Symbol && "symbol" === typeof Symbol("x"); $jscomp.TRUST_ES6_POLYFILLS = !$jscomp.ISOLATE_POLYFILLS || $jscomp.IS_SYMBOL_NATIVE; $jscomp.polyfills = {}; $jscomp.propertyToPolyfillSymbol = {}; $jscomp.POLYFILL_PREFIX = "$jscp$"; var $jscomp$lookupPolyfilledValue = function (b, k) { var m = $jscomp.propertyToPolyfillSymbol[k]; if (null == m) return b[k]; m = b[m]; return void 0 !== m ? m : b[k] };
$jscomp.polyfill = function (b, k, m, n) { k && ($jscomp.ISOLATE_POLYFILLS ? $jscomp.polyfillIsolated(b, k, m, n) : $jscomp.polyfillUnisolated(b, k, m, n)) }; $jscomp.polyfillUnisolated = function (b, k, m, n) { m = $jscomp.global; b = b.split("."); for (n = 0; n < b.length - 1; n++) { var p = b[n]; if (!(p in m)) return; m = m[p] } b = b[b.length - 1]; n = m[b]; k = k(n); k != n && null != k && $jscomp.defineProperty(m, b, { configurable: !0, writable: !0, value: k }) };
$jscomp.polyfillIsolated = function (b, k, m, n) {
    var p = b.split("."); b = 1 === p.length; n = p[0]; n = !b && n in $jscomp.polyfills ? $jscomp.polyfills : $jscomp.global; for (var y = 0; y < p.length - 1; y++) { var z = p[y]; if (!(z in n)) return; n = n[z] } p = p[p.length - 1]; m = $jscomp.IS_SYMBOL_NATIVE && "es6" === m ? n[p] : null; k = k(m); null != k && (b ? $jscomp.defineProperty($jscomp.polyfills, p, { configurable: !0, writable: !0, value: k }) : k !== m && ($jscomp.propertyToPolyfillSymbol[p] = $jscomp.IS_SYMBOL_NATIVE ? $jscomp.global.Symbol(p) : $jscomp.POLYFILL_PREFIX + p, p =
        $jscomp.propertyToPolyfillSymbol[p], $jscomp.defineProperty(n, p, { configurable: !0, writable: !0, value: k })))
}; $jscomp.polyfill("Array.prototype.find", function (b) { return b ? b : function (k, m) { return $jscomp.findInternal(this, k, m).v } }, "es6", "es3");
(function (b) { "function" === typeof define && define.amd ? define(["jquery", "datatables.net"], function (k) { return b(k, window, document) }) : "object" === typeof exports ? module.exports = function (k, m) { k || (k = window); m && m.fn.dataTable || (m = require("datatables.net")(k, m).$); return b(m, k, k.document) } : b(jQuery, window, document) })(function (b, k, m, n) {
    function p(a, c, d) { var f = c + "-" + d; if (A[f]) return A[f]; var g = []; a = a.cell(c, d).node().childNodes; c = 0; for (d = a.length; c < d; c++)g.push(a[c]); return A[f] = g } function y(a, c, d) {
        var f = c + "-" +
            d; if (A[f]) { a = a.cell(c, d).node(); d = A[f][0].parentNode.childNodes; c = []; for (var g = 0, l = d.length; g < l; g++)c.push(d[g]); d = 0; for (g = c.length; d < g; d++)a.appendChild(c[d]); A[f] = n }
    } var z = b.fn.dataTable, u = function (a, c) {
        if (!z.versionCheck || !z.versionCheck("1.10.10")) throw "DataTables Responsive requires DataTables 1.10.10 or newer"; this.s = { dt: new z.Api(a), columns: [], current: [] }; this.s.dt.settings()[0].responsive || (c && "string" === typeof c.details ? c.details = { type: c.details } : c && !1 === c.details ? c.details = { type: !1 } : c &&
            !0 === c.details && (c.details = { type: "inline" }), this.c = b.extend(!0, {}, u.defaults, z.defaults.responsive, c), a.responsive = this, this._constructor())
    }; b.extend(u.prototype, {
        _constructor: function () {
            var a = this, c = this.s.dt, d = c.settings()[0], f = b(k).innerWidth(); c.settings()[0]._responsive = this; b(k).on("resize.dtr orientationchange.dtr", z.util.throttle(function () { var g = b(k).innerWidth(); g !== f && (a._resize(), f = g) })); d.oApi._fnCallbackReg(d, "aoRowCreatedCallback", function (g, l, h) {
                -1 !== b.inArray(!1, a.s.current) && b(">td, >th",
                    g).each(function (e) { e = c.column.index("toData", e); !1 === a.s.current[e] && b(this).css("display", "none") })
            }); c.on("destroy.dtr", function () { c.off(".dtr"); b(c.table().body()).off(".dtr"); b(k).off("resize.dtr orientationchange.dtr"); c.cells(".dtr-control").nodes().to$().removeClass("dtr-control"); b.each(a.s.current, function (g, l) { !1 === l && a._setColumnVis(g, !0) }) }); this.c.breakpoints.sort(function (g, l) { return g.width < l.width ? 1 : g.width > l.width ? -1 : 0 }); this._classLogic(); this._resizeAuto(); d = this.c.details; !1 !==
                d.type && (a._detailsInit(), c.on("column-visibility.dtr", function () { a._timer && clearTimeout(a._timer); a._timer = setTimeout(function () { a._timer = null; a._classLogic(); a._resizeAuto(); a._resize(!0); a._redrawChildren() }, 100) }), c.on("draw.dtr", function () { a._redrawChildren() }), b(c.table().node()).addClass("dtr-" + d.type)); c.on("column-reorder.dtr", function (g, l, h) { a._classLogic(); a._resizeAuto(); a._resize(!0) }); c.on("column-sizing.dtr", function () { a._resizeAuto(); a._resize() }); c.on("preXhr.dtr", function () {
                    var g =
                        []; c.rows().every(function () { this.child.isShown() && g.push(this.id(!0)) }); c.one("draw.dtr", function () { a._resizeAuto(); a._resize(); c.rows(g).every(function () { a._detailsDisplay(this, !1) }) })
                }); c.on("draw.dtr", function () { a._controlClass() }).on("init.dtr", function (g, l, h) { "dt" === g.namespace && (a._resizeAuto(), a._resize(), b.inArray(!1, a.s.current) && c.columns.adjust()) }); this._resize()
        }, _columnsVisiblity: function (a) {
            var c = this.s.dt, d = this.s.columns, f, g = d.map(function (t, v) { return { columnIdx: v, priority: t.priority } }).sort(function (t,
                v) { return t.priority !== v.priority ? t.priority - v.priority : t.columnIdx - v.columnIdx }), l = b.map(d, function (t, v) { return !1 === c.column(v).visible() ? "not-visible" : t.auto && null === t.minWidth ? !1 : !0 === t.auto ? "-" : -1 !== b.inArray(a, t.includeIn) }), h = 0; var e = 0; for (f = l.length; e < f; e++)!0 === l[e] && (h += d[e].minWidth); e = c.settings()[0].oScroll; e = e.sY || e.sX ? e.iBarWidth : 0; h = c.table().container().offsetWidth - e - h; e = 0; for (f = l.length; e < f; e++)d[e].control && (h -= d[e].minWidth); var r = !1; e = 0; for (f = g.length; e < f; e++) {
                    var q = g[e].columnIdx;
                    "-" === l[q] && !d[q].control && d[q].minWidth && (r || 0 > h - d[q].minWidth ? (r = !0, l[q] = !1) : l[q] = !0, h -= d[q].minWidth)
                } g = !1; e = 0; for (f = d.length; e < f; e++)if (!d[e].control && !d[e].never && !1 === l[e]) { g = !0; break } e = 0; for (f = d.length; e < f; e++)d[e].control && (l[e] = g), "not-visible" === l[e] && (l[e] = !1); -1 === b.inArray(!0, l) && (l[0] = !0); return l
        }, _classLogic: function () {
            var a = this, c = this.c.breakpoints, d = this.s.dt, f = d.columns().eq(0).map(function (h) {
                var e = this.column(h), r = e.header().className; h = d.settings()[0].aoColumns[h].responsivePriority;
                e = e.header().getAttribute("data-priority"); h === n && (h = e === n || null === e ? 1E4 : 1 * e); return { className: r, includeIn: [], auto: !1, control: !1, never: r.match(/\bnever\b/) ? !0 : !1, priority: h }
            }), g = function (h, e) { h = f[h].includeIn; -1 === b.inArray(e, h) && h.push(e) }, l = function (h, e, r, q) {
                if (!r) f[h].includeIn.push(e); else if ("max-" === r) for (q = a._find(e).width, e = 0, r = c.length; e < r; e++)c[e].width <= q && g(h, c[e].name); else if ("min-" === r) for (q = a._find(e).width, e = 0, r = c.length; e < r; e++)c[e].width >= q && g(h, c[e].name); else if ("not-" === r) for (e =
                    0, r = c.length; e < r; e++)-1 === c[e].name.indexOf(q) && g(h, c[e].name)
            }; f.each(function (h, e) {
                for (var r = h.className.split(" "), q = !1, t = 0, v = r.length; t < v; t++) {
                    var B = r[t].trim(); if ("all" === B) { q = !0; h.includeIn = b.map(c, function (w) { return w.name }); return } if ("none" === B || h.never) { q = !0; return } if ("control" === B || "dtr-control" === B) { q = !0; h.control = !0; return } b.each(c, function (w, D) {
                        w = D.name.split("-"); var x = B.match(new RegExp("(min\\-|max\\-|not\\-)?(" + w[0] + ")(\\-[_a-zA-Z0-9])?")); x && (q = !0, x[2] === w[0] && x[3] === "-" + w[1] ? l(e,
                            D.name, x[1], x[2] + x[3]) : x[2] !== w[0] || x[3] || l(e, D.name, x[1], x[2]))
                    })
                } q || (h.auto = !0)
            }); this.s.columns = f
        }, _controlClass: function () { if ("inline" === this.c.details.type) { var a = this.s.dt, c = b.inArray(!0, this.s.current); a.cells(null, function (d) { return d !== c }, { page: "current" }).nodes().to$().filter(".dtr-control").removeClass("dtr-control"); a.cells(null, c, { page: "current" }).nodes().to$().addClass("dtr-control") } }, _detailsDisplay: function (a, c) {
            var d = this, f = this.s.dt, g = this.c.details; if (g && !1 !== g.type) {
                var l = g.display(a,
                    c, function () { return g.renderer(f, a[0], d._detailsObj(a[0])) }); !0 !== l && !1 !== l || b(f.table().node()).triggerHandler("responsive-display.dt", [f, a, l, c])
            }
        }, _detailsInit: function () {
            var a = this, c = this.s.dt, d = this.c.details; "inline" === d.type && (d.target = "td.dtr-control, th.dtr-control"); c.on("draw.dtr", function () { a._tabIndexes() }); a._tabIndexes(); b(c.table().body()).on("keyup.dtr", "td, th", function (g) { 13 === g.keyCode && b(this).data("dtr-keyboard") && b(this).click() }); var f = d.target; d = "string" === typeof f ? f : "td, th";
            if (f !== n || null !== f) b(c.table().body()).on("click.dtr mousedown.dtr mouseup.dtr", d, function (g) { if (b(c.table().node()).hasClass("collapsed") && -1 !== b.inArray(b(this).closest("tr").get(0), c.rows().nodes().toArray())) { if ("number" === typeof f) { var l = 0 > f ? c.columns().eq(0).length + f : f; if (c.cell(this).index().column !== l) return } l = c.row(b(this).closest("tr")); "click" === g.type ? a._detailsDisplay(l, !1) : "mousedown" === g.type ? b(this).css("outline", "none") : "mouseup" === g.type && b(this).trigger("blur").css("outline", "") } })
        },
        _detailsObj: function (a) { var c = this, d = this.s.dt; return b.map(this.s.columns, function (f, g) { if (!f.never && !f.control) return f = d.settings()[0].aoColumns[g], { className: f.sClass, columnIndex: g, data: d.cell(a, g).render(c.c.orthogonal), hidden: d.column(g).visible() && !c.s.current[g], rowIndex: a, title: null !== f.sTitle ? f.sTitle : b(d.column(g).header()).text() } }) }, _find: function (a) { for (var c = this.c.breakpoints, d = 0, f = c.length; d < f; d++)if (c[d].name === a) return c[d] }, _redrawChildren: function () {
            var a = this, c = this.s.dt; c.rows({ page: "current" }).iterator("row",
                function (d, f) { c.row(f); a._detailsDisplay(c.row(f), !0) })
        }, _resize: function (a) {
            var c = this, d = this.s.dt, f = b(k).innerWidth(), g = this.c.breakpoints, l = g[0].name, h = this.s.columns, e, r = this.s.current.slice(); for (e = g.length - 1; 0 <= e; e--)if (f <= g[e].width) { l = g[e].name; break } var q = this._columnsVisiblity(l); this.s.current = q; g = !1; e = 0; for (f = h.length; e < f; e++)if (!1 === q[e] && !h[e].never && !h[e].control && !1 === !d.column(e).visible()) { g = !0; break } b(d.table().node()).toggleClass("collapsed", g); var t = !1, v = 0; d.columns().eq(0).each(function (B,
                w) { !0 === q[w] && v++; if (a || q[w] !== r[w]) t = !0, c._setColumnVis(B, q[w]) }); t && (this._redrawChildren(), b(d.table().node()).trigger("responsive-resize.dt", [d, this.s.current]), 0 === d.page.info().recordsDisplay && b("td", d.table().body()).eq(0).attr("colspan", v)); c._controlClass()
        }, _resizeAuto: function () {
            var a = this.s.dt, c = this.s.columns; if (this.c.auto && -1 !== b.inArray(!0, b.map(c, function (e) { return e.auto }))) {
                b.isEmptyObject(A) || b.each(A, function (e) { e = e.split("-"); y(a, 1 * e[0], 1 * e[1]) }); a.table().node(); var d = a.table().node().cloneNode(!1),
                    f = b(a.table().header().cloneNode(!1)).appendTo(d), g = b(a.table().body()).clone(!1, !1).empty().appendTo(d); d.style.width = "auto"; var l = a.columns().header().filter(function (e) { return a.column(e).visible() }).to$().clone(!1).css("display", "table-cell").css("width", "auto").css("min-width", 0); b(g).append(b(a.rows({ page: "current" }).nodes()).clone(!1)).find("th, td").css("display", ""); if (g = a.table().footer()) {
                        g = b(g.cloneNode(!1)).appendTo(d); var h = a.columns().footer().filter(function (e) { return a.column(e).visible() }).to$().clone(!1).css("display",
                            "table-cell"); b("<tr/>").append(h).appendTo(g)
                    } b("<tr/>").append(l).appendTo(f); "inline" === this.c.details.type && b(d).addClass("dtr-inline collapsed"); b(d).find("[name]").removeAttr("name"); b(d).css("position", "relative"); d = b("<div/>").css({ width: 1, height: 1, overflow: "hidden", clear: "both" }).append(d); d.insertBefore(a.table().node()); l.each(function (e) { e = a.column.index("fromVisible", e); c[e].minWidth = this.offsetWidth || 0 }); d.remove()
            }
        }, _responsiveOnlyHidden: function () {
            var a = this.s.dt; return b.map(this.s.current,
                function (c, d) { return !1 === a.column(d).visible() ? !0 : c })
        }, _setColumnVis: function (a, c) { var d = this.s.dt; c = c ? "" : "none"; b(d.column(a).header()).css("display", c); b(d.column(a).footer()).css("display", c); d.column(a).nodes().to$().css("display", c); b.isEmptyObject(A) || d.cells(null, a).indexes().each(function (f) { y(d, f.row, f.column) }) }, _tabIndexes: function () {
            var a = this.s.dt, c = a.cells({ page: "current" }).nodes().to$(), d = a.settings()[0], f = this.c.details.target; c.filter("[data-dtr-keyboard]").removeData("[data-dtr-keyboard]");
            "number" === typeof f ? a.cells(null, f, { page: "current" }).nodes().to$().attr("tabIndex", d.iTabIndex).data("dtr-keyboard", 1) : ("td:first-child, th:first-child" === f && (f = ">td:first-child, >th:first-child"), b(f, a.rows({ page: "current" }).nodes()).attr("tabIndex", d.iTabIndex).data("dtr-keyboard", 1))
        }
    }); u.breakpoints = [{ name: "desktop", width: Infinity }, { name: "tablet-l", width: 1024 }, { name: "tablet-p", width: 768 }, { name: "mobile-l", width: 480 }, { name: "mobile-p", width: 320 }]; u.display = {
        childRow: function (a, c, d) {
            if (c) {
                if (b(a.node()).hasClass("parent")) return a.child(d(),
                    "child").show(), !0
            } else { if (a.child.isShown()) return a.child(!1), b(a.node()).removeClass("parent"), !1; a.child(d(), "child").show(); b(a.node()).addClass("parent"); return !0 }
        }, childRowImmediate: function (a, c, d) { if (!c && a.child.isShown() || !a.responsive.hasHidden()) return a.child(!1), b(a.node()).removeClass("parent"), !1; a.child(d(), "child").show(); b(a.node()).addClass("parent"); return !0 }, modal: function (a) {
            return function (c, d, f) {
                if (d) b("div.dtr-modal-content").empty().append(f()); else {
                    var g = function () {
                        l.remove();
                        b(m).off("keypress.dtr")
                    }, l = b('<div class="dtr-modal"/>').append(b('<div class="dtr-modal-display"/>').append(b('<div class="dtr-modal-content"/>').append(f())).append(b('<div class="dtr-modal-close">&times;</div>').click(function () { g() }))).append(b('<div class="dtr-modal-background"/>').click(function () { g() })).appendTo("body"); b(m).on("keyup.dtr", function (h) { 27 === h.keyCode && (h.stopPropagation(), g()) })
                } a && a.header && b("div.dtr-modal-content").prepend("<h2>" + a.header(c) + "</h2>")
            }
        }
    }; var A = {}; u.renderer =
    {
        listHiddenNodes: function () { return function (a, c, d) { var f = b('<ul data-dtr-index="' + c + '" class="dtr-details"/>'), g = !1; b.each(d, function (l, h) { h.hidden && (b("<li " + (h.className ? 'class="' + h.className + '"' : "") + ' data-dtr-index="' + h.columnIndex + '" data-dt-row="' + h.rowIndex + '" data-dt-column="' + h.columnIndex + '"><span class="dtr-title">' + h.title + "</span> </li>").append(b('<span class="dtr-data"/>').append(p(a, h.rowIndex, h.columnIndex))).appendTo(f), g = !0) }); return g ? f : !1 } }, listHidden: function () {
            return function (a,
                c, d) { return (a = b.map(d, function (f) { var g = f.className ? 'class="' + f.className + '"' : ""; return f.hidden ? "<li " + g + ' data-dtr-index="' + f.columnIndex + '" data-dt-row="' + f.rowIndex + '" data-dt-column="' + f.columnIndex + '"><span class="dtr-title">' + f.title + '</span> <span class="dtr-data">' + f.data + "</span></li>" : "" }).join("")) ? b('<ul data-dtr-index="' + c + '" class="dtr-details"/>').append(a) : !1 }
        }, tableAll: function (a) {
            a = b.extend({ tableClass: "" }, a); return function (c, d, f) {
                c = b.map(f, function (g) {
                    return "<tr " + (g.className ?
                        'class="' + g.className + '"' : "") + ' data-dt-row="' + g.rowIndex + '" data-dt-column="' + g.columnIndex + '"><td>' + g.title + ":</td> <td>" + g.data + "</td></tr>"
                }).join(""); return b('<table class="' + a.tableClass + ' dtr-details" width="100%"/>').append(c)
            }
        }
    }; u.defaults = { breakpoints: u.breakpoints, auto: !0, details: { display: u.display.childRow, renderer: u.renderer.listHidden(), target: 0, type: "inline" }, orthogonal: "display" }; var C = b.fn.dataTable.Api; C.register("responsive()", function () { return this }); C.register("responsive.index()",
        function (a) { a = b(a); return { column: a.data("dtr-index"), row: a.parent().data("dtr-index") } }); C.register("responsive.rebuild()", function () { return this.iterator("table", function (a) { a._responsive && a._responsive._classLogic() }) }); C.register("responsive.recalc()", function () { return this.iterator("table", function (a) { a._responsive && (a._responsive._resizeAuto(), a._responsive._resize()) }) }); C.register("responsive.hasHidden()", function () {
            var a = this.context[0]; return a._responsive ? -1 !== b.inArray(!1, a._responsive._responsiveOnlyHidden()) :
                !1
        }); C.registerPlural("columns().responsiveHidden()", "column().responsiveHidden()", function () { return this.iterator("column", function (a, c) { return a._responsive ? a._responsive._responsiveOnlyHidden()[c] : !1 }, 1) }); u.version = "2.2.9"; b.fn.dataTable.Responsive = u; b.fn.DataTable.Responsive = u; b(m).on("preInit.dt.dtr", function (a, c, d) {
            "dt" === a.namespace && (b(c.nTable).hasClass("responsive") || b(c.nTable).hasClass("dt-responsive") || c.oInit.responsive || z.defaults.responsive) && (a = c.oInit.responsive, !1 !== a && new u(c,
                b.isPlainObject(a) ? a : {}))
        }); return u
});