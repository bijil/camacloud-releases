var settings = [];
var applicationSettings = [];
var subjectParcel = [];
var alternateKeyLabel;
var selectedSubject, queryParams;
var comparableSettings = {};
var toolTip = {
    keyValue: 'The KeyValue to be used to search the parcel, If there are multiple keyValues, they can be selected as dropdown',
    chkSale: 'In order for an Estimated Sale Price to be calculated, there must be comparables that are 80%+ comparable.',
    mapLabels: 'Hover over the parcels to see its parcel_id after checking Show Labels',
}
var selectedComps = [];
$(document).ready(function () {
    initGoogleMap();
    $('#spnSubHdRight').append($('<span>').attr({class : 'switch-to-old'}).html('Switch to Old Version'));
    $('.switch-to-old').off('click').on('click',function(){
        window.location.href = '/protected/cse/';
        return false;
    })  
    PageMethods.GetLoadInfo((d) => {
        if (d.Success) {
            settings = d.Data.Settings[0];
            applicationSettings = d.Data.ApplicationSettings[0];
            alternateKeyLabel = d.Data.AlternateKeyLabel;
            setSearchPanel(applicationSettings, alternateKeyLabel);
            onLoadSearch();
        }
    });

    $('.cpSettingsDialog__hideUniValue').on('change', function () {
        $(this).siblings('label').text($(this).prop('checked') ? 'Hidden' : 'Visible');
    })
});

function editCompSettings() {
    loading.show('Loading Settings');

    PageMethods.GetComparableSettings(function (d) {
        if (d.Success && d.Data) {
            comparableSettings = d.Data.ComparableSettings;

            $(".cpSettingsMask").show();
            $(".cpSettingsDialog").show();

            $(".cpSettingsDialog__hideUniValue").prop('checked', comparableSettings.HideUniformityIndicationValue);
            $("#chkHideUniformity").siblings('label').text(comparableSettings.HideUniformityIndicationValue ? 'Hidden' : 'Visible');
            $(".cpSettingsDialog__uniPercent").val(comparableSettings.UniformityIndicationLowPercent?.toFixed(2));
            $(".cpSettingsDialog__maxCount").val(comparableSettings.MaxCompCount);

            loading.hide();
        } else {
            loading.hide();
            sayError('An error occurred', d.ErrorMessage);
        }
    });
}

function clearSearch(){
    $(".subject-parcel-list").empty();
    $(".result-container").hide();
    $(".selection-panel").hide();
    $(".input-field1").val('');
    $('.comp-settings').show();
    $('.comp-clear').hide();
    clearComparables();
    resetURL();
    getRecentHistory();
}

function hideSettingsDialog() {
    $(".cpSettingsDialog").hide();
    $(".cpSettingsMask").hide();
}

function saveCompSettings() {
    loading.show('Saving the settings');
    let id = comparableSettings.Id;
    let uniPercent = $(".cpSettingsDialog__uniPercent").val();
    let maxCount = $(".cpSettingsDialog__maxCount").val();
    let hideUni = $('.cpSettingsDialog__hideUniValue').prop('checked');
    if (id) {
        PageMethods.SaveComparableSettings(id, uniPercent, maxCount, hideUni ? 1 : 0, (d) => {
            if (d.Success && d.Data) {
                comparableSettings = d.Data.ComparableSettings;
                $.extend(settings, comparableSettings);
                if (selectedSubject) refreshSlots();
                hideSettingsDialog();
                loading.hide();
                var selected = getSelectedParcels();
                if (selected && selected.length > settings.MaxCompCount) {
                    say("You have selected more comparables than configured in the Settings. Please unselect the additional comparables.");
                }
            } else say(d.ErrorMessage);
        });
    } else say('Id Missing. Please reload the page')
}

function onLoadSearch() {
    queryParams = new URLSearchParams(window.location.search);
    var jumpToComps = parseInt(queryParams.get("jtc"));
    if (jumpToComps == "1") {
        var searchParameters = queryParams.get("searchParams").split(',');
        var selectedComparables = queryParams.get("c");
        var subjectFromReport;
        if (searchParameters && searchParameters.length == 7) {
            if (searchParameters[0] != "0") {
                subjectFromReport = searchParameters[0]
                $('.key-select-box').val("KeyValue1");
                $(".input-field1").val(searchParameters[0]);
            } else if (searchParameters[1] != "0") {
                subjectFromReport = searchParameters[1]
                $('.key-select-box').val("KeyValue2");
                $(".input-field1").val(searchParameters[2]);
            }
            else if (searchParameters[2] != "0") {
                subjectFromReport = searchParameters[2]
                $('.key-select-box').val("KeyValue3");
                $(".input-field1").val(searchParameters[2]);
            }
            searchParcel(true, subjectFromReport);
        }
    }
    else    
    getRecentHistory();
}

function searchParcel(jumpToComps, subjectFromReport) {
    loading.show("Searching for Parcels");
    if (!jumpToComps) resetURL();
    subjectParcel = [];
    clearComparables();
    $(".subject-parcel-list").hide();
    if (!$(".input-field1").val()) {
        say("Please provide an Input");
        return false;
    }
    var keyValue1 = "", keyValue2 = "", keyValue3 = "";
    var selectedKeyField = $('.key-select-box').val();
    if (selectedKeyField == "keyValue1"){
        keyValue1 = $(".input-field1").val();
    }    
    else if(selectedKeyField == "keyValue2"){
        keyValue2 = $(".input-field1").val();
    }    
    else {
        keyValue3 = $(".input-field1").val();
    }    
    PageMethods.GetParcels(keyValue1, keyValue2, keyValue3, (d) => {
        loading.hide();
        $(".subject-parcel-list").empty();
        $(".result-container").hide();
        $(".selection-panel").hide();
        if (d.Success) {
            if (!d.Data.Parcel.length) {
                say("No parcels found matching your search criteria");
            }
            $('.recent-search-list').hide();
            $('.fav-reports-list').hide();
            $('.comp-settings').hide();
            $('.comp-clear').show();
            d.Data.Parcel.forEach(function (x, i) {
                subjectParcel[x.Id] = x;
            });
            if (subjectParcel) showMultipleSubjects(subjectParcel);
            $(".subject-parcel-list").show();
            if (localStorage.getItem('toolTipEnabled') == 1)
                $('.tip').tipr();
            if (jumpToComps && subjectFromReport) {
                var selectedPId;
                if (selectedKeyField == "keyValue1") {
                    selectedPId = subjectParcel.filter((x) => { return x.KeyValue1 == subjectFromReport })[0].Id;
                }
                else if (selectedKeyField == "keyValue2") {
                    selectedPId = subjectParcel.filter((x) => { return x.KeyValue2 == subjectFromReport })[0].Id;
                }
                else {
                    selectedPId = subjectParcel.filter((x) => { return x.KeyValue3 == subjectFromReport })[0].Id;
                }
                var searchParameters = queryParams.get("searchParams").split(',');
                $('#chkGroup_' + selectedPId).attr("checked", searchParameters[3] == "1" ? true : false);
                $('#chkMatched_' + selectedPId).attr("checked", searchParameters[4] == "1" ? true : false);
                $('#chkSale_' + selectedPId).attr("checked", searchParameters[5] == "1" ? true : false);
                $('#chkEstSalePrice_' + selectedPId).attr("checked", searchParameters[6] == "1" ? true : false);
                getComparables(selectedPId, jumpToComps);
            }
        } else {
            sayError("An error occurred while searching for the parcel: ", d.ErrorMessage);
        }
    });
}

function showSubjectList(subjects) {
    for (x in subjects) {
        with (subjects[x]) {
            StreetAddress = (StreetAddress ? StreetAddress : '--');
            NbhdNo = (NbhdNo ? NbhdNo : '--');
            GroupNo = (GroupNo ? GroupNo : '--');
            allowZoom();
            var html = `<div class="subject-parcel ${Object.keys(subjects).length == 1 ? "subject-single" : ""
                }" style="display:flex;"><div class="s-image"><div class="s-source_${Id} zoom-image" ></div></div>`;
            var header = `<div class="s-header"><div class="s-addrs-desc"><span class="s-header-label s-addrs-label">Address</span><span title="${StreetAddress}" class="s-address s-header-value">${StreetAddress}</span></div>`;
            var key1 = `<div class="s-Keyvaleu1-desc"><span class="s-header-label s-Keyvaleu1-label">${!alternateKeyLabel ? applicationSettings.KeyField1 : alternateKeyLabel
                }</span><span title="${KeyValue1}" class="s-keyvalue1 s-header-value">${KeyValue1}</span></div>`;
            var key2 =
                applicationSettings.KeyField2 && KeyValue2
                    ? `<div class="s-Keyvaleu2-desc"><span class="s-header-label s-Keyvaleu2-label">${applicationSettings.KeyField2}</span><span title="${KeyValue2}" class="s-keyvalue2 s-header-value">${KeyValue2}</span></div>`
                    : ``;
            var key3 =
                applicationSettings.KeyField3 && KeyValue3
                    ? `<div class="s-Keyvaleu3-desc"><span class="s-header-label s-Keyvaleu3-label">${applicationSettings.KeyField3}</span><span title="${KeyValue3}" class="s-keyvalue3 s-header-value">${KeyValue3}</span></div>`
                    : "";
            var grpNbhd = `<div class="flex"><div class="s-Neighborhood-desc"><span class="s-header-label s-Neighborhood-label">NeighborHood</span><span title="${NbhdNo}" class="s-Neighborhood s-header-value">${NbhdNo}</span></div><div class="s-Group-desc"><span class="s-header-label s-Group-label">Group</span><span title="${GroupNo}" class="s-Group s-header-value">${GroupNo}</span></div></div></div>`;
            var overlay = `<div class="s-overlay"><div class="s-options">`;
            var chkMatch = `<div class="flex"><div><input type="checkbox" id="chkMatched_${Id}" class="chk-chkMatched" /><label for="chkMatched_${Id}">Only Absolute Comparables</label> </div>`;
            var chkGroup = `<div><input type="checkbox" id="chkGroup_${Id}" class="chk-chkGroup" /><label for="chkGroup_${Id}">Wthin Group</label></div></div>`;
            var chkSale = `<div class="flex"> <div class="tip" abbr="${toolTip.chkSale}"><input type="checkbox" id="chkSale_${Id}" class="chk-chkSale" /><label for="chkSale_${Id}">With at least one sales</label></div>`;
            var chkEst = `<div class="tip" abbr="${toolTip.chkSale}"><input type="checkbox" id="chkEstSalePrice_${Id}" class="chk-chkEstSalePrice" /><label for="chkEstSalePrice_${Id}">Estimate Subject&apos;s Sale Price</label></div></div>  `;
            overlay += chkMatch + chkGroup + chkSale + chkEst;
            var SearchBt = `<button class="button search-comparables" style="display:flex;" onclick="getComparables(${Id}); return false;">Search Comparables</button> </div></div></div>`;
            html += header + key1 + key2 + key3 + grpNbhd + overlay + SearchBt;
            $(".subject-parcel-list").append(html);
            getParcelPhoto(Id, $(".s-source_" + Id + ""));
            $('.chk-chkEstSalePrice').off('click').on('click', (e) => {
                if ($('#chkEstSalePrice_' + Id).attr('checked')) {
                    $('#chkSale_' + Id).attr('checked', 'checked');
                }
            });
            $('.chk-chkSale').off('click').on('click', (e) => {
                if ($('#chkEstSalePrice_' + Id).attr('checked')) {
                    $('#chkSale_' + Id).attr('checked', 'checked');
                }
            });
        }
    }
}

function showMultipleSubjects(subjects) {
    var singleSubject = Object.keys(subjectParcel).length == 1 ? true : false;
    singleSubject ? $('.subject-parcel-list').addClass("justify-c") : $('.subject-parcel-list').removeClass("justify-c");
    for (x in subjects) {
        with (subjects[x]) {
            // KeyValue2 = KeyValue3 = KeyValue1
            var html = `<article class="card"><div class="card__box ${!singleSubject ? 'o_f_h' : ''}">`
            var imageDiv = `<div class="card__header"><div class="s-source_${Id} card__img zoom-image" pId=${Id} ><div class="key1-parent"><span class="key-1">${KeyValue1}</span></div></div>`

            var address = `<h3 class="card-address" title="${StreetAddress}">${StreetAddress}</h3>`
            var header = imageDiv + address
            var content = `<div class="card__content"><div class="card-row">`
            var key2 =
                applicationSettings.KeyField2 && KeyValue2
                    ? `<div class="card-data-row"><span class="card-label">${applicationSettings.KeyField2}</span><span title="${KeyValue2}" class="card-data">${KeyValue2}</span></div>`
                    : ``;
            var key3 =
                applicationSettings.KeyField3 && KeyValue3
                    ? `<div class="card-data-row"><span class="card-label">${applicationSettings.KeyField3}</span><span title="${KeyValue3}" class="card-data">${KeyValue3}</span></div>`
                    : "";
            var grpNbhd = `<div class="card-data-row"><span class="card-label">NeighborHood</span><span title="${NbhdNo}" class="card-data">${NbhdNo}</span></div><div class="card-data-row"><span class="card-label">Group</span><span title="${GroupNo}" class="card-data">${GroupNo}</span></div></div></div>`;
            content += key2 + key3 + grpNbhd + '</div>'
            var footer = singleSubject ? `<div class="card__actions">` : `<div class="card__actions action__slide">`;
            var chkMatch = `<div><input type="checkbox" id="chkMatched_${Id}" class="chk-chkMatched" /><label for="chkMatched_${Id}">Only Absolute Comparables</label> </div>`;
            var chkGroup = `<div><input type="checkbox" id="chkGroup_${Id}" class="chk-chkGroup" /><label for="chkGroup_${Id}">Wthin Group</label></div>`;
            var chkSale = `<div class="tip" abbr="${singleSubject ? toolTip.chkSale : ''}"><input type="checkbox" id="chkSale_${Id}" class="chk-chkSale" /><label for="chkSale_${Id}">With at least one sales</label></div>`;
            var chkEst = `<div class="tip" abbr="${singleSubject ? toolTip.chkSale : ''}"><input type="checkbox" id="chkEstSalePrice_${Id}" class="chk-chkEstSalePrice" /><label for="chkEstSalePrice_${Id}">Estimate Subject&apos;s Sale Price</label></div>`;
            var SearchBt = `<button class="button search-comparables" style="display:flex;" onclick="getComparables(${Id}); return false;">Search Comparables</button>`;
            footer += chkMatch + chkGroup + chkSale + chkEst + SearchBt + '</div>'

            content += footer + '</div>'
            html += header + content + ' </div></div></article>'
            $(".subject-parcel-list").append(html);
            getParcelPhoto(Id, $(".s-source_" + Id + ""));
            allowZoom();
            $('#chkEstSalePrice_' + Id).off('click').on('click', (e) => {
                if ($('#chkEstSalePrice_' + Id).attr('checked')) {
                    $('#chkSale_' + Id).attr('checked', 'checked');
                }
            });
            $('#chkSale_' + Id).off('click').on('click', (e) => {
                if ($('#chkEstSalePrice_' + Id).attr('checked')) {
                    $('#chkSale_' + Id).attr('checked', 'checked');
                }
            });
            //return false;
        }
    }
}

function showSelectedSubjectParcel(parcel) {
    allowZoom();
    $(".s-address").text((parcel.StreetAddress ? parcel.StreetAddress : '--'));
    $(".s-address").attr('title', (parcel.StreetAddress ? parcel.StreetAddress : '--'));
    $(".s-Keyvaleu1-label").text(!alternateKeyLabel ? applicationSettings.KeyField1 : alternateKeyLabel);
    $(".s-Keyvaleu1-label").attr('title', !alternateKeyLabel ? applicationSettings.KeyField1 : alternateKeyLabel);
    $(".s-keyvalue1").text(parcel.KeyValue1);
    $(".s-keyvalue1").attr('title', parcel.KeyValue1);
    if (applicationSettings.KeyField2 && parcel.KeyValue2) {
        $(".s-Keyvaleu2-desc").show();
        $(".s-Keyvaleu2-label").text(applicationSettings.KeyField2);
        $(".s-Keyvaleu2-label").attr('title', applicationSettings.KeyField2);
        $(".s-keyvalue2").text(parcel.KeyValue2);
        $(".s-keyvalue2").attr('title', parcel.KeyValue2);
    } else {
        $(".s-Keyvaleu2-desc").hide();
    }
    if (applicationSettings.KeyField3 && parcel.KeyValue3) {
        $(".s-Keyvaleu3-desc").show();
        $(".s-Keyvaleu3-label").text(applicationSettings.KeyField3);
        $(".s-Keyvaleu3-label").attr('title', applicationSettings.KeyField3);
        $(".s-keyvalue3").text(parcel.KeyValue3);
        $(".s-keyvalue3").attr('title', parcel.KeyValue3);
    } else {
        $(".s-Keyvaleu3-desc").hide();
    }
    $(".s-Neighborhood").text((parcel.NbhdNo ? parcel.NbhdNo : '--'));
    $(".s-Neighborhood").attr('title', (parcel.NbhdNo ? parcel.NbhdNo : '--'));
    $(".s-Group").text((parcel.GroupNo ? parcel.GroupNo : '--'));
    $(".s-Group").attr('title', (parcel.GroupNo ? parcel.GroupNo : '--'));
    getParcelPhoto(parcel.Id, $(".s-source"));
    $(".s-source").attr('pId', parcel.Id);
}

var mapVisible = false;
var comparableProperties = {};
var compPropsList = "";
var searchFilters = '';
function getComparables(pId, jumpToComps) {
    loading.show("Getting Comparable Properties");
    if (!jumpToComps) resetURL();
    selectedSubject = subjectParcel[pId];
    showSelectedSubjectParcel(selectedSubject);
    var subParcel = pId;
    var chkMatch, chkGroup, chkSale, chkEstSale;
    chkMatch = $("#chkMatched_" + pId + "")[0].checked;
    chkGroup = $("#chkGroup_" + pId + "")[0].checked;
    chkSale = $("#chkSale_" + pId + "")[0].checked;
    chkEstSale = $("#chkEstSalePrice_" + pId + "")[0].checked;
    searchFilters += `${chkGroup ? 1 :0},${chkMatch ? 1 :0},${chkSale ? 1 : 0},${chkEstSale ? 1 : 0}`
    compPropsList = subParcel + ",";
    PageMethods.GetComparables(subParcel, true, chkGroup, chkMatch, chkSale, chkEstSale, (d) => {
        loading.hide();
        if (d.Success) {
            if (!d.Data.Parcels.length) {
                say("No comparable properties are found for the parcel with the current options.");
                return false;
            }
            $(".result-container").hide();
            $(".selection-panel").hide();
            $(".subject-parcel").hide();
            $(".subject-parcel-list").hide();
            var comp = d.Data.Parcels;
            for (c in comp) {
                var x = comp[c];
                x.LastQualifiedSale ? x.LastQualifiedSale = x.LastQualifiedSale.toDateString() : '';
                comparableProperties[x.ID] = x
            }
            renderResultCards(comp, jumpToComps);
            compPropsList = compPropsList.replace(/,\s*$/, "");
            $(".parcel-count").html("Parcel Count: " + d.Data.Parcels.length);
            $(".switch-button-checkbox").prop("checked", false);
            $(".show-map-label").hide();
            $(".show-map-sale").hide();
            $(".show-map-sales").attr("checked", null);
            $(".show-map-labels").attr("checked", null);
            $(".grid-view").show();
            $("#mapview").hide();
            refreshSlots();
            addToRecentSearch();
            mapVisible = false;
            $(".show-map-labels").on("change", (e) => {
                $('.gm-style-iw-c').css('padding', '12px');
                $('.gm-style-iw-d').css({
                    'max-width': '300px',
                    'overflow': 'hidden',
                    'text-overflow': 'ellipsis',
                    'white-space': 'pre',
                });
                if ($(e.target).prop("checked")) {
                    $(".show-map-sale").show();
                } else {
                    $(".show-map-sales").attr("checked", null);
                    $(".show-map-sale").hide();
                }
            });
            $(".switch-button-checkbox").on("change", (e) => {
                if ($(e.target).prop("checked")) {
                    mapVisible = true;
                    $("#mapview").show();
                    $(".grid-view").hide();
                    $(".show-map-label").show();
                    if ($(".show-map-labels").prop("checked")) {
                        $(".show-map-sale").show();
                    }
                    SetMapView()
                    controlMapFullscreen();
                } else {
                    $(".show-map-label").hide();
                    $(".show-map-sale").hide();
                    $(".grid-view").show();
                    $("#mapview").hide();
                    mapVisible = false;
                }
            });
        } else {
            sayError('An error occurred while searching for comparables', d.ErrorMessage);
        }
    });
}

function renderResultCards(Parcels, jumpToComps) {
    $(".result-list").empty();
    $(".result-container").show();
    $(".selection-panel").show();
    for (x in Parcels) {
        compPropsList += Parcels[x].ID + ",";
        var compPer = (Parcels[x].CompPercent ? Parcels[x].CompPercent > 0.95 ? '95%+' : parseInt(Parcels[x].CompPercent * 100) + '%' : '');
        // var parcelImage = '<img class="s-source" id= "parcel-image-' + Parcels[x].ID + '" loading="lazy" src="">';
        var parcelImage = '<div class="s-source zoom-image" id= "parcel-image-' + Parcels[x].ID + '" pId="' + Parcels[x].ID + '"></div>';
        var cardLeft = '<div class="card-left placeholder">' + parcelImage + "</div>";
        var keyValue =
            '<div class="card-data-row"> <span title="' + (!alternateKeyLabel ? applicationSettings.KeyField1 : alternateKeyLabel) + '" class="card-label"> ' +
            (!alternateKeyLabel ? applicationSettings.KeyField1 : alternateKeyLabel) +
            ' </span> <span title="' + Parcels[x].KeyValue1 + '"class="parcel-key-value card-data"> ' +
            Parcels[x].KeyValue1 +
            " </span> </div>";
        var address =
            '<div class="card-data-row card-address-data"> <span class="card-label"> Address </span> <span title="' + (Parcels[x].StreetAddress ? Parcels[x].StreetAddress : "--") + '" class="parcel-address card-data"> ' +
            (Parcels[x].StreetAddress ? Parcels[x].StreetAddress : "--") +
            " </span> " +
            '<span title="' + compPer + '" class="parcel-comp-per">' +
            compPer +
            "</span>" +
            "</div>";
        var neighbourhood =
            '<div class="card-data-row"> <span class="card-label"> NeighbourHood </span> <span title="' + (Parcels[x].NbhdNo ? Parcels[x].NbhdNo : "--") + '" class="parcel-nbhd card-data"> ' +
            (Parcels[x].NbhdNo ? Parcels[x].NbhdNo : "--") +
            " </span> </div>";
        var group =
            '<div class="card-data-row"> <span class="card-label"> Group </span> <span title="' + (Parcels[x].GroupNo ? Parcels[x].GroupNo : "--") + '" class="parcel-group card-data"> ' +
            (Parcels[x].GroupNo ? Parcels[x].GroupNo : "--") +
            " </span> </div>";
        var lastSale =
            '<div class="card-data-row"> <span class="card-label"> Last Sale </span> <span title="' + (Parcels[x].LastQualifiedSale ? Parcels[x].LastQualifiedSale : "--") + '" class="parcel-last-sale card-data"> ' +
            (Parcels[x].LastQualifiedSale ? Parcels[x].LastQualifiedSale : "--") +
            " </span> </div>";
        var cardRight = '<div class="card-right"> ' + keyValue + neighbourhood + group + lastSale + " </div>";

        $(".result-list").append(
            '<div class="result-card" id="result-parcel-' +
            Parcels[x].ID +
            '" pId ="' +
            Parcels[x].ID +
            '" sel="0"> ' +
            address +
            cardLeft +
            cardRight +
            " </div>"
        );
        allowZoom();
        $("#result-parcel-" + Parcels[x].ID + "")
            .off("click")
            .on("click", (e) => {
                var target = $(e.target).parents(".result-card")[0];
                if (!target) {
                    if ($(e.target).hasClass("result-card"))
                        target = $(e.target)
                }
                var selected = $(target).attr("sel");
                var parcelId = parseInt($(target).attr("pid"))
                if (selected == "0") {
                    if (getSelectedParcels().length >= settings.MaxCompCount) {
                        say("You have selected the maximum no of Comparables - " + settings.MaxCompCount);
                        return false;
                    }
                    $(target).attr("sel", "1");
                    parcelId && selectedComps.push(parcelId);
                } else {
                    $(target).attr("sel", "0");
                    parcelId && selectedComps.splice(selectedComps.indexOf(parcelId), 1);
                }
                refreshSlots();
            });
        getParcelPhoto(Parcels[x].ID, $("#parcel-image-" + Parcels[x].ID + ""));
    }
    if (jumpToComps) {
        $('.report-button').removeAttr('disabled');
        var selComps = queryParams.get("c") && queryParams.get("c").split(',');
        for (x in selComps) {
            $("#result-parcel-" + selComps[x] + "").attr("sel", "1");
            selectedComps.push(parseInt(selComps[x]));
        }
        refreshSlots();
    }
}

function addToRecentSearch(){
    var searchTerms = '';
    var selectedKeyField = $('.key-select-box').val();
    if (selectedKeyField == "keyValue1"){
        searchTerms += `${selectedSubject.KeyValue1},0,0`
    }    
    else if(selectedKeyField == "keyValue2"){
        searchTerms += `0,${selectedSubject.KeyValue2},0`
    }    
    else {
        searchTerms += `0,0,${selectedSubject.KeyValue3}`
    }    
    PageMethods.AddToRecent(selectedSubject.Id, searchTerms, searchFilters , function(d){
        if(!d.Success){
            say(d.ErrorMessage)
        }
    })
}

function getRecentHistory(){
    $('.recent-items').html('');
    $('.fav-table tbody').html('');
    $('.recent-search-list').hide();
    $('.fav-reports-list').hide();
    PageMethods.GetRecent(function(d){
        if(d.Success){
            d.Data && d.Data.Recents.length > 0 ? renderRecentHistory(d.Data.Recents) : $('.recent-search-list').hide();
        } else {
            say(d.ErrorMessage)
        }
    })
    PageMethods.GetFavItems(function(d){
        if(d.Success){
            d.Data && d.Data.Recents.length > 0 ? renderFavItems(d.Data.Recents) : $('.fav-reports-list').hide();
        } else {
            say(d.ErrorMessage)
        }
    })
}

function renderRecentHistory(recentSearchs){
    for(x in recentSearchs){
        with(recentSearchs[x]){
            var recentRow = $('<div>').attr({class : "recent-row"})
            var searched = SearchTerms.split(',');
            var subHdr = $('<span>').attr({class : 'sub-header'});
            var subNumber = $('<span>').attr({class : 'sub-number'});
            if(searched.length > 0 && searched[0] != "0"){
                subHdr.text(`${!applicationSettings.ShowAlternateField ? applicationSettings.KeyField1 : alternateKeyLabel}`)
                subNumber.text(`${searched[0]}`)
            } else if(searched.length > 0 && searched[1] != "0"){
                subHdr.text(`${applicationSettings.KeyField2} - ${searched[1]}`)
                subNumber.text(`${searched[1]}`)
            } else {
                subHdr.text(`${applicationSettings.KeyField3} - ${searched[2]}`)
                subNumber.text(`${searched[2]}`)
            }
            recentRow.attr({searchText : SearchTerms, searhParam : SearchParams})
            recentRow.click(function(){
                var recentTerm = $(this).attr("searchtext");
                var recentParams = $(this).attr("searhParam");
                var url = `default.aspx?jtc=1&searchParams=${recentTerm},${recentParams}`
                window.location.href = url;
            })
            recentRow.append(subHdr,subNumber);
            recentRow.append($('<div>').attr({class : "b-top b-bottom", style: "padding: 6px;cursor:pointer;"}).text("Additional Settings"));
            var srchParamas = SearchParams.split(',');
            if(SearchParams.includes('1')){
                recentRow.append( $('<div>').attr({class : 'search-params-outer'}).append(
                            srchParamas[1] == "1" ? $('<div>').attr({class : `srch-params-recent `}).text('Only Absolute Comparables') : '',
                            srchParamas[0] == "1" ? $('<div>').attr({class : `srch-params-recent` }).text('Within Groups') : '',
                            srchParamas[2] == "1" ? $('<div>').attr({class : `srch-params-recent `}).text('With Atleast one Sale') : '',
                            srchParamas[3] == "1" ? $('<div>').attr({class : `srch-params-recent `  }).text("Estimate Subjet's Sale price") : ''      
                    )   
                )
            } else {
                recentRow.append( $('<div>').attr({class : 'search-params-outer'}).append($('<div>').attr({style: "padding: 10px;cursor:pointer;"}).text('No Settings selected')))
            }    
            $('.recent-items').append(recentRow); 
            $('.recent-search-list').show(); 
        } 
    }

}

function renderFavItems(favItems){
    for(x in favItems){
        with(favItems[x]){
            var recentRow = $('<tr>').attr({class : "fav-row"})
            var searched = SearchTerms.split(',');
            var favSubDetails = $('<td>').attr({class : 'fav-sub-details'})
            if(searched.length > 0 && searched[0] != "0"){
                    favSubDetails.append( $('<span>').text(`${!applicationSettings.ShowAlternateField ? applicationSettings.KeyField1 : alternateKeyLabel} - ${searched[0]}`))
            } else if(searched.length > 0 && searched[1] != "0"){
                    favSubDetails.append( $('<span>').text(`${applicationSettings.KeyField2} - ${searched[1]}`))
            } else {
                    favSubDetails.append( $('<span>').text(`${applicationSettings.KeyField3} - ${searched[2]}`))
            }
            favSubDetails.attr({searchText : SearchTerms, searhParam : SearchParams, SelectedC : SelectedComps, subject : ParcelId})
            favSubDetails.click(function(){
                var url ="compreport.aspx?s=" + $(this).attr("subject") +"&c=" + $(this).attr("SelectedC") + "&searchparams=" + $(this).attr("searchtext") +"," + $(this).attr("searhParam");
                window.location.href = url;
            }) 
            recentRow.append(favSubDetails)
            var srchParamas = SearchParams.split(",");
            if(SearchParams.includes('1')){
                recentRow.append( $('<td>').attr({class : 'fav-search-params-outer'}).append(
                        srchParamas[1] == "1" ? $('<span>').attr({class: 'fav-srch-params '}).text('Only Absolute Comparables') : '',
                        srchParamas[0] == "1" ? $('<span>').attr({class : 'fav-srch-params '}).text('Within Groups') : '', 
                        srchParamas[2] == "1" ?  $('<span>').attr({class : 'fav-srch-params '}).text('With Atleast one Sale'): '' ,
                    )   
                )
            }  else {
                recentRow.append( $('<td>').attr({class : 'fav-search-params-outer'}).append($('<div>').attr({style: "padding: 10px;cursor:pointer;"}).text('No Settings selected')))
            }     
            recentRow.append($('<td>').attr({style :"text-align: center;vertical-align: middle;"}).text(`${srchParamas[3] == "1" ? "Yes" : "No"}`))
            var keyFields = ParcelKeyFields.split(',');
            var favSelectedComps = $('<td>').attr({class : 'fav-selected-comps'})
            for(x in keyFields){
                favSelectedComps.append( $('<span>').attr({class : 'comp-keyfields'}).text(keyFields[x]));
            }
            var remFav = $('<td>').attr({class : 'fav-remove', favId : Id}).text("Remove");
            remFav.click(function(){
                var id = $(this).attr("favId");
                PageMethods.DeleteFromFavourite(id, function(d){
                    if(d.Success)
                        getRecentHistory();
                    else
                        say(d.ErrorMessage)
                })
            });
            recentRow.append(favSelectedComps,remFav)
            $('.fav-table tbody').append(recentRow); 
            $('.fav-reports-list').show();   
        } 
    }
}

function initGoogleMap(callback) {
    mapZoom = 5;
    mapPosition = new google.maps.LatLng(42.345573, -71.098326);
    mapCenter = new google.maps.LatLng(35.56, -96.84);
    var mapOptions = {
        zoom: mapZoom,
        minZoom: 7,
        mapTypeId: google.maps.MapTypeId.HYBRID,
        center: mapCenter,
        gestureHandling: "greedy",
        fullscreenControl: false,
    };
    $('.gmap').attr('fullscreen', 'false');
    map = new google.maps.Map(document.getElementById("gmap"), mapOptions);
    console.log("map initialized");
}

var parcelsInMap = [];
var icon = [
    "/App_Themes/Cloud/images/parcelUncheck.png",
    "/App_Themes/Cloud/images/parcelCheck.png",
    "/App_Themes/Cloud/images/subParcel.png",
];
var mapmarkers = [];
var OpenInMap = false;
var firstLoad = true;
var fullscreenControl;
var bounds = new google.maps.LatLngBounds();
var mapcentre = new google.maps.LatLngBounds();
function SetMapView() {
    PageMethods.GetComparablesMapPoints(compPropsList, (d) => {
        if (d.Success) {
            var parcelMap = d.Data.Points;
            //parcelsIn = res;
            var parcels = [];
            var parcel;
            var infowindow = new google.maps.InfoWindow();
            for (x in parcelMap) {
                var point = parcelMap[x];
                point.ParcelId.toString() == selectedSubject.Id.toString()
                    ? (point.isSubject = "1")
                    : (point.isSubject = "0");
                var pid = point.ParcelId.toString();
                if (parcelsInMap[pid] == null)
                    parcelsInMap[pid] = {
                        ParcelId: point.ParcelId,
                        Keyvalue1: point.Keyvalue1,
                        addToCompare: false,
                        LastQualifiedSale: '',
                        isSubject: point.isSubject,
                        Points: [],
                    };
                if (parcelsInMap[pid]) parcelsInMap[pid].Points.push(point.Points);
            }
            for (x in parcelsInMap) {
                if (parcelsInMap[x])
                    parcelsInMap[x].LastQualifiedSale = (comparableProperties[x] && comparableProperties[x].LastQualifiedSale) || '';
            }
            var totalParcels = Object.keys(parcelsInMap).length;
            var selectedParcels = getSelectedParcels();
            for (x in selectedParcels) {
                if (parcelsInMap[selectedParcels[x]]) {
                    parcelsInMap[selectedParcels[x]].addToCompare = true;
                    parcelsInMap[selectedParcels[x]].icon = icon[1];
                }
            }
            setMarkers(() => {
                for (x in comparableProperties) {
                    if (mapmarkers[comparableProperties[x].ID])
                        mapmarkers[comparableProperties[x].ID].setIcon(icon[0]);
                }
                for (x in selectedParcels) {
                    if (mapmarkers[selectedParcels[x]])
                        mapmarkers[selectedParcels[x]].setIcon(icon[1]);
                }
                setMapCentre();
            });
        }
    });
}

function controlMapFullscreen() {
    $('.fullscreen-control').remove();
    $('.map-container').append('<button title="Toggle Full Screen View" class="fullscreen-control"></button>');
    $('.fullscreen-control').empty();
    $('.fullscreen-control').append('<img src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A//www.w3.org/2000/svg%22%20viewBox%3D%220%200%2018%2018%22%3E%3Cpath%20fill%3D%22%23111%22%20d%3D%22M0%200v6h2V2h4V0H0zm16%200h-4v2h4v4h2V0h-2zm0%2016h-4v2h6v-6h-2v4zM2%2012H0v6h6v-2H2v-4z%22/%3E%3C/svg%3E" alt="" style="height: 20px; width: 20px;">');
    $('.fullscreen-control').off("click").on("click", (e) => {
        e.preventDefault();
        if ($('.gmap').attr('fullscreen') == 'false') {
            $('.mapview').toggleClass('mapview-fullscreen');
            $('.map-container').toggleClass('map-container-fullscreen');
            $('.gmap').attr('fullscreen', 'true');
            $('.fullscreen-control').empty();
            $('.fullscreen-control').append('<img src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A//www.w3.org/2000/svg%22%20viewBox%3D%220%200%2018%2018%22%3E%3Cpath%20fill%3D%22%23111%22%20d%3D%22M4%204H0v2h6V0H4v4zm10%200V0h-2v6h6V4h-4zm-2%2014h2v-4h4v-2h-6v6zM0%2014h4v4h2v-6H0v2z%22/%3E%3C/svg%3E" alt="" style="height: 20px; width: 20px;">');
        } else {
            $('.mapview').toggleClass('mapview-fullscreen');
            $('.map-container').toggleClass('map-container-fullscreen');
            $('.gmap').attr('fullscreen', 'false');
            $('.fullscreen-control').empty();
            $('.fullscreen-control').append('<img src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A//www.w3.org/2000/svg%22%20viewBox%3D%220%200%2018%2018%22%3E%3Cpath%20fill%3D%22%23111%22%20d%3D%22M0%200v6h2V2h4V0H0zm16%200h-4v2h4v4h2V0h-2zm0%2016h-4v2h6v-6h-2v4zM2%2012H0v6h6v-2H2v-4z%22/%3E%3C/svg%3E" alt="" style="height: 20px; width: 20px;">');
        }
    });
}

function setMarkers(callback) {
    var parcel;
    var info = new google.maps.InfoWindow();
    for (x in parcelsInMap) {
        parcel = parcelsInMap[x];
        var b = new google.maps.LatLngBounds();
        for (i in parcel.Points) {
            pstr = parcel.Points[i].split(",");
            loc = new google.maps.LatLng(parseFloat(pstr[0].replace("[", "")), parseFloat(pstr[1].replace("[", "")));
            bounds.extend(loc);
            b.extend(loc);
        }
        var parcelLoc = new google.maps.LatLng(b.getCenter().lat(), b.getCenter().lng());
        if (parcel.isSubject == "1") mapcentre = parcelLoc;
        if (mapmarkers[parcel.ParcelId.toString()] == null) {
            mapmarkers[parcel.ParcelId.toString()] = new google.maps.Marker({
                position: parcelLoc,
                map: map,
                icon: parcel.isSubject == "0" ? icon[0] : icon[2],
                animation: google.maps.Animation.DROP,
            });
            mapmarkers[parcel.ParcelId.toString()].pid = parcel.ParcelId;
            mapmarkers[parcel.ParcelId.toString()].Keyvalue1 = parcel.Keyvalue1;
            google.maps.event.addListener(mapmarkers[parcel.ParcelId.toString()], "mouseout", function (e) {
                info.close();
            });
            google.maps.event.addListener(mapmarkers[parcel.ParcelId.toString()], "mouseover", function (e) {
                if ($(".show-map-labels").prop("checked")) {
                    var e = this;
                    var pdata = parcelsInMap[this.pid];
                    var popupData = '<div class="map-popup-parcelid" style="font-weight:600;"> ParcelID: ' + pdata.Keyvalue1 + '</div>';
                    if ($(".show-map-sales").prop("checked")) {
                        popupData += '<div class="map-popup-last-sale" style="font-weight:600;"> Last Qualified Sale: ' + (pdata.LastQualifiedSale ? pdata.LastQualifiedSale : "--") + '</div>';
                    }
                    info.setContent(popupData);
                    info.open(map, mapmarkers[this.pid.toString()]);
                    $('.gm-ui-hover-effect').hide();
                    $('.gm-style-iw-c').css('padding', '12px');
                    $('.gm-style-iw-d').css({
                        'max-width': '300px',
                        'overflow': 'hidden',
                        'text-overflow': 'ellipsis',
                        'white-space': 'pre',
                    });
                }
            });
            google.maps.event.addListener(mapmarkers[parcel.ParcelId.toString()], "click", function (e) {
                var e = this;
                var pdata = parcelsInMap[this.pid];
                // info.close();
                if (pdata.isSubject != "0") {
                    return;
                }
                if (!pdata.addToCompare) {
                    //if (! validateSelection()) return;
                    if (getSelectedParcels().length < settings.MaxCompCount) {
                        $("#result-parcel-" + pdata.ParcelId).attr("sel", "1");
                        selectedComps.push(pdata.ParcelId);
                        mapmarkers[pdata.ParcelId.toString()].setIcon(icon[1]);
                        pdata.addToCompare = true;
                        refreshSlots();
                    } else {
                        say("You have selected the maximum no of Comparables - " + settings.MaxCompCount);
                        return false;
                    }
                } else {
                    $("#result-parcel-" + pdata.ParcelId).attr("sel", "0");
                    selectedComps.splice(selectedComps.indexOf(pdata.ParcelId), 1);
                    mapmarkers[pdata.ParcelId.toString()].setIcon(icon[0]);
                    pdata.addToCompare = false;
                    refreshSlots();
                }
                $('gm-style-iw-d').css('overflow', 'hidden');
            });
        }
    }
    if (callback) callback();
}

function clearMarkers() {
    if (mapmarkers) {
        var keys = Object.keys(mapmarkers);
        for (x in keys) {
            if (mapmarkers[keys[x]].setMap) {
                mapmarkers[keys[x]].setMap(null);
                mapmarkers[keys[x]] = null;
            }
        }
        mapmarkers = [];
    }
    //   info.close();
}


function setMapCentre(callback) {
    if (map && bounds) {
        map.setCenter(bounds.getCenter());
        map.fitBounds(bounds);
    }
    if (callback) callback();
}

function getSelectedParcels() {
    var selectedParcels = [];
    for (var i = 0; i < $(".result-card").length; i++) {
        if ($($(".result-card")[i]).attr("sel") == 1) {
            selectedParcels.push(parseInt($($(".result-card")[i]).attr("id").replace("result-parcel-", "")));
        }
    }
    return selectedParcels;
}

function refreshSlots() {
    /*
    $(".comp-slot").addClass("slot-unselected");
    $(".comp-slot").removeClass("slot-selected");
    $(".comp-slot").html("<i></i>");
    $(".comp-slot").attr("parcelId", "");
    */
    //var selectedParcels = getSelectedParcels();
    var selectedKeyValues = [];
    for (c in selectedComps) {
        var x = comparableProperties[selectedComps[c]];
        if (selectedComps.includes(x.ID)) selectedKeyValues.push({ ID: x.KeyValue1, pId: x.ID, address: x.StreetAddress });
    }
    if (selectedKeyValues?.length) $(".selected-list-items").html('');
    else {
        $(".selected-list-items").html('<p class="slot-info">Click on any Parcel cards on the right to select</p>');
    }
    for (x in selectedKeyValues) {
        var slot = $('<div>')
            .attr({ class: "comp-slot slot-selected", id: 'comp-slot-' + x + 1, parcelId: selectedKeyValues[x].pId })
            .append(
                `<span class="slot-title">
                    <span class="slot-id" title="${selectedKeyValues[x].ID}">${selectedKeyValues[x].ID}</span>
                    <i class="slot-remove icon-delete icon-brown" style=""></i>
                </span >`,
                $("<p>").text(selectedKeyValues[x].address).attr("title", selectedKeyValues[x].address),
            );
        $(".selected-list-items").append(slot);
    }
    $(".slots-free").html(selectedKeyValues.length + "/" + settings.MaxCompCount);
    if ($("#chkEstSalePrice_" + selectedSubject.Id + "")[0] && $("#chkEstSalePrice_" + selectedSubject.Id + "")[0].checked) {
        if (selectedKeyValues.length < 4) {
            $('.report-button').attr('disabled', 'disabled');
        } else {
            $('.report-button').attr('disabled', null);
        }
    } else if (selectedKeyValues.length == 0) {
        $('.report-button').attr('disabled', 'disabled');
    } else {
        $('.report-button').attr('disabled', null);
    }
    $('.slot-remove').off('click').on('click', (e) => {
        var target = $(e.currentTarget).parents(".comp-slot")[0];
        var parcelId = parseInt($(target).attr("parcelId"));
        $("#result-parcel-" + parcelId).attr("sel", "0");
        parcelId && selectedComps.splice(selectedComps.indexOf(parcelId), 1);
        if (mapVisible) {
            if (mapmarkers[parcelId]) mapmarkers[parcelId].setIcon(icon[0]);
            if (parcelsInMap[parcelId]) parcelsInMap[parcelId].addToCompare = false;
        }
        refreshSlots();
    });
}

function clearComparables() {
    clearMarkers();
    comparableProperties = {};
    parcelsInMap = [];
    selectedComps = [];;
    searchFilters = '';
}

function resetURL() {
    var uri = window.location.toString();
    if (uri.indexOf("?") > 0) {
        var clean_uri = uri.substring(0, uri.indexOf("?"));
        window.history.replaceState({}, document.title, clean_uri);
    }
}

function generateReport() {
    loading.show("Generating Report");
    var selected = getSelectedParcels();
    if (selected.length < 1 || !selected) {
        loading.hide();
        say("You must select at least one parcel to compare.");
        return false;
    } else if ($("#chkEstSalePrice_" + selectedSubject.Id + "")[0].checked && selected.length < 4) {
        loading.hide();
        say("You must select at least 4 parcels to compare with Estimate sale price.");
        return false;
    } else if (selected.length > settings.MaxCompCount) {
        loading.hide();
        say("You have selected more comparables than configured in the Settings. Please remove the comparables to generate report.");
        return false;
    }
    var selectedCompList = ''
    for (x in selectedComps) {
        if (selectedCompList != '')
            selectedCompList += ','
        if (parseInt(selectedComps[x]))
            selectedCompList += selectedComps[x];
    }
    var pId = selectedSubject.Id;
    var chkMatch = $("#chkMatched_" + pId + "")[0].checked,
        chkGroup = $("#chkGroup_" + pId + "")[0].checked,
        chkSale = $("#chkSale_" + pId + "")[0].checked,
        chkEstSale = $("#chkEstSalePrice_" + pId + "")[0].checked;
    var keyValues =
        (selectedSubject.KeyValue1 ? selectedSubject.KeyValue1 : 0) +
        "," +
        (selectedSubject.KeyValue2 ? selectedSubject.KeyValue2 : 0) +
        "," +
        (selectedSubject.KeyValue3 ? selectedSubject.KeyValue3 : 0);
    var parameters =
        (chkGroup ? 1 : 0) + "," + (chkMatch ? 1 : 0) + "," + (chkSale ? 1 : 0) + "," + (chkEstSale ? 1 : 0);
    var url =
        "compreport.aspx?s=" +
        selectedSubject.Id +
        "&c=" +
        selectedComps +
        "&searchparams=" +
        keyValues +
        "," +
        parameters;
    window.location.href = url;
}

