﻿function MapView(target, extent, center) {
    var _view;
    var defCenter, defExtent;
    var map, dataLayer, dataMap, layerStyles = {}, lockMapUpdates;

    defCenter = center; defExtent = extent;

    function getLayerStyles(r, g, b) {
        var baseColor = [r, g, b],
            numRange = Array.apply(0, Array(10)).map(function (x, i) {
                return new ol.style.Style({
                    fill: new ol.style.Fill({
                        color: 'rgba(' + baseColor.toString() + "," + ((i + 1) / 15) + ')'
                    })
                });
            })
        return numRange;
    }

    function initOLMap() {
        map = new ol.Map({
            target: target,
            layers: [
              new ol.layer.Tile({
                  source: new ol.source.OSM()
              })
            ],
            view: new ol.View({
                projection: 'EPSG:4326',
                center: center,
                extent: extent,
                zoom: 9,
                minZoom: 9
            }),
            controls: ol.control.defaults().extend([new ol.control.FullScreen()])
        });
        layerStyles.G = getLayerStyles(0, 255, 0);
        layerStyles.R = getLayerStyles(255, 0, 0);
        layerStyles.B = getLayerStyles(0, 0, 255);
        layerStyles.Neon = getLayerStyles(57, 255, 20);

        dataMap = new ol.source.Vector();
        dataLayer = new ol.layer.Vector({
            source: dataMap,
            style: layerStyles.G[2]
        });
        map.addLayer(dataLayer);
    }

    this.addPoint = function (lonlat, r, hi) {
        var px = new ol.Feature(new ol.geom.Circle(lonlat, r));
        px.setStyle(layerStyles.R[hi - 1]);
        dataMap.addFeature(px);
    }

    this.addDataPoint = function (lonlat, r, avgRatio, pid, max, min, count, avgComparedRatio ) {
        var px = new ol.Feature(new ol.geom.Circle(lonlat, r));
        let absRatio = Math.abs(1 - avgRatio);
        px.setProperties({ AvgRatio: avgRatio, MaxRatio: max, MinRatio: min, Count: count, ParcelId : pid, AvgComparedRatio : avgComparedRatio });
        let ri = Math.round(Math.abs(absRatio))
        ri = ri > 10 ? 10 : (ri<5 ? 5 : ri);
        let color = absRatio > 1 ? layerStyles.R[ri-1] : layerStyles.B[ri-1];
        px.setStyle(color);
        dataMap.addFeature(px);
    }

    this.getResolution = function () {
        var resolution = map.getView().getResolution() * 5;
        if (resolution > 0.005) resolution = 0.005;
        if (resolution < 0.00005) resolution = 0.00005;
        return resolution;
    }

    this.clear = function () {
        dataMap.clear();
    }

    var meHandled = false;
    this.resetView = function (centerPoint) {
        //lockMapUpdates = true;/
        if (centerPoint) {
            map.getView().fit(dataLayer.getSource().getExtent(), map.getSize());
            map.getView().setCenter(centerPoint);
        } else {
            map.getView().fit(defExtent, map.getSize());
            map.getView().setCenter(defCenter);
        }

        if (!meHandled) {
            map.on('moveend', function (evt) {
                if (!lockMapUpdates) {
                    var extent = map.getView().calculateExtent();
                    _view.viewChanged && _view.viewChanged(extent);
                }
            });
            map.on("movestart", function (evt) {
                _view.viewChangeStart && _view.viewChangeStart(extent);
              });
            map.on('click', function (evt) {
                _view.mapClick && _view.mapClick(map.getFeaturesAtPixel(evt.pixel),evt);
            });
            meHandled = true;
        }

        //lockMapUpdates = false;
    }

    this.mapClick = ()=>{};

    this.viewChangeStart = () => {};

    this.viewChanged = function () { }

    this.__defineSetter__('locked', function (v) { lockMapUpdates = v; })

    _view = this;
    initOLMap();
}