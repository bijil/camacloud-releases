﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Debug_MA.aspx.vb" Inherits="CAMACloud.MobileAssessor.Debug_MA" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>MA Debugger</title>
    <script type="text/javascript" src="static/js/lib/zepto.min.js"></script>
    <script>
    	/*
    	var reciever = '';
    	window.onload = function() {
    		if ("WebSocket" in window) {
				var url = location.href.split('//')[1].split('/')[0];
			    webSocket = new WebSocket("ws://" + url + "/default.socket");
			    webSocket.onopen = function() {
			        console.log('Connection Opened, if you want to do something while opening connection do it here');
			        webSocket.send('MA_Debugger||MA_Debugger||MA_Debugger opened debug mode||5');
			    };
			    webSocket.onmessage = function (evt) {
			    	var msg = evt.data.split('||');
			        if (msg.length == 4 && msg[3] == '5' && msg[0] != 'MA_Debugger') reciever = msg[0]
			        if (msg[2].indexOf('JSON-') > -1) console.log(JSON.parse(msg[2].split('JSON-')[1]));
			        else console.log(msg[2])
			    };
			 
			    webSocket.onclose = function() {
			        console.warn("Connection is closed");
			    };
			 
			    webSocket.onerror = function (error) {
			        console.error(error.data);
			    };
			}
			else alert("WebSocket NOT supported by your Browser!");
    	}
    	function sendMessage() {
    		var msg = document.getElementById('msg_txt_box').value
			if (typeof msg == "object") msg = "JSON-" + JSON.stringify(msg);
			else if (typeof msg == "number") msg = msg.toString();
			try {
		    	webSocket.send('MA_Debugger||' + reciever + '||' + msg + '||0');
		    } catch(ex) { console.log('error in socket send') }
		}
		*/
		var lastTicks = (new Date()).getTime(), latestId;
		function ticks() {
		    var t = (new Date()).getTime();
		    while (t == lastTicks) {
		        t = (new Date()).getTime();
		    }
		    lastTicks = t;
		    return t;
		}
		
		function fillTable(data) {
			data = JSON.parse(data);
			if (data.length == 0) { if (latestId == 0) $('.loggedDetails tbody tr').remove(); return; }
			if (data.filter(function(d){ return d.LogType == 'result'; }).length > 0) $('.loggedDetails tbody tr.eval').remove();
			latestId = data[data.length - 1].Id;
			var trow = $('.loggedDetails tbody tr');
			var rowNum = $(trow).length;
			data.forEach(function(d){
				var tr = '<tr class="' + d.LogType + '"><td>' + d.LoginId + '</td><td>' + d.ParcelId + '</td><td>' + d.CurrentScreenId + '</td><td>' + d.LogText + '</td><td>' + d.LogTime + '</td><td>' + d.LogType + '</td><td>' + d.Version + '</td></tr>'
				if (rowNum == 0) $('.loggedDetails tbody').append(tr)
				else $('.loggedDetails tbody tr').eq(rowNum-1).after(tr);
				rowNum++;
			})
		}
		
		window.onload = function() {
			var msg = ''
			$.ajax({
				url: '/mobileassessor/geterrorlog.jrq?zt=' + ticks(),
				data: { type: 'all' },
				dataType: 'JSON',
				type: 'POST',
				success:function(resp) { fillTable(resp); },
			 	error:  function() { }
			})
		}
		
		function evalMA() {
			var exp = $('.msg').val();
			exp = 'MA_Debugger|null|null|' + exp + '|' + (new Date()).toISOString() + '|eval|debugger|!\n';
			$.ajax({
				url: '/mobileassessor/recorderrorlog.jrq?zt=' + ticks(),
				data: { message: exp },
				dataType: 'JSON',
				type: 'POST',
				success:function(resp) { $('.msg').val(''); },
			 	error:  function() { }
			})
		}
		function refreshList() {
			if (!latestId) latestId = 0;
			$('.button[value="Refresh Log"]').attr('disabled', 'disabled')
			$.ajax({
				url: '/mobileassessor/geterrorlog.jrq?zt=' + ticks(),
				data: { type: 'latest', id: latestId },
				dataType: 'JSON',
				type: 'POST',
				success:function(resp) { fillTable(resp); $('.button[value="Refresh Log"]').removeAttr('disabled'); },
			 	error:  function() { }
			})
		}
		
		function clearList() {
			if (!latestId) latestId = 0;
			$.ajax({
				url: '/mobileassessor/clearerrorlog.jrq?zt=' + ticks(),
				data: {},
				dataType: 'JSON',
				type: 'POST',
				success:function(resp) { 
							if (JSON.parse(resp).status == "OK") {
								latestId = 0;
								fillTable('[]'); 
							}
						},
			 	error:  function() { }
			})
		}
		
    </script>
    <style>
    	td {
    		padding: 0px 2px;
    	}
    	thead tr td { font-weight: bold; }
    	
    	.button {
    		float: right; 
    		margin: 10px 10px; 
    		padding: 8px;
    	}
    </style>
</head>
<body>
    <form id="form1" runat="server">
    	<!--
    	<div class="outer_div" style=""><h2 style=" text-align: center; width: 100%; ">MA Debugger</h2>
    		<div class="inner_div">
    			<div id="msg_box" style="height: 498px;border: 1px solid silver; white-space:pre-wrap;"></div>
    			<div class="send_msg_div" style=" padding: 8px 0px; ">
    				<input type="text" id="msg_txt_box" style="height: 40px;width: 95%;" />
    				<input type="button" class="send_msg" value="Submit" style="height: 45px;" onclick="sendMessage();" />
    			</div>
    		</div>
    	</div>
    	-->
    	<div class="outer_div">
    		<div class="inner_div" style="text-align: -webkit-center; height: 500px;">
    			<h2 style="width: 100%; ">MA Debugger</h2>
    			<div class=""></div>
    			<div class="table" style="height: 450px; overflow: scroll;">
			    	<table class="loggedDetails" style="border: solid 1px silver;">
			    		<thead>
			    			<tr><td>LoginId</td><td>ParcelId</td><td>Logged ScreenId</td><td>Message</td><td>Logged Time</td><td>Log Type</td><td>Version</td></tr>
			    		</thead>
			    		<tbody></tbody>
			    	</table>
		    	</div>
	    	</div>
	    	<div>
		    	<textarea style="width: 90%; margin-top: 10px;" class="msg"></textarea>
		    	<input type="button" value="Evaluate" class="button" onclick="evalMA();" />
	    	</div>
	    	<div>
	    		<input type="button" value="Refresh Log" class="button" onclick="return refreshList();" />
	    		<input type="button" value="Clear Log" class="button" onclick="return clearList();" />
	    	</div>
    	</div>
    </form>
</body>
</html>
