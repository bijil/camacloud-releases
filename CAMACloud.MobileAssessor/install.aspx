﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="install.aspx.vb" Inherits="CAMACloud.MobileAssessor.install" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CAMA Cloud&#174;</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="static/app/apple-touch-icon-precomposed-72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="static/app/apple-touch-icon-precomposed-144.png" />
    <meta name="apple-mobile-web-app-title" content="CAMA Cloud&#174;">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="user-scalable=no,initial-scale=1.0,maximum-scale=1.0,width=device-width,height=device-height" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <link rel="apple-touch-startup-image" href="images/splash/MAminiAir-min.png" media="(min-device-width: 768px) and (max-device-width: 1024px) and (-webkit-min-device-pixel-ratio: 2) and (orientation: portrait)">
    <link rel="apple-touch-startup-image" href="images/splash/MA10.5-min.png" media="(min-device-width: 834px) and (max-device-width: 834px) and (-webkit-min-device-pixel-ratio: 2) and (orientation: portrait)">
    <link rel="apple-touch-startup-image" href="images/splash/MA12-min.png" media="(min-device-width: 1024px) and (max-device-width: 1024px) and (-webkit-min-device-pixel-ratio: 2) and (orientation: portrait)">
    <link rel="stylesheet" href="/static/install/install.css" />
    <script  type="text/javascript">

         var calauthority = '<%=calauthority%>';
         var iosVersion = '<%=iosVersion%>';
         var macID = '<%=macID%>';
         var isExisting = false
         if ( macID == "" ){
             macID = localStorage.getItem( "macid" );
             isExisting = true;
         }
         if ( iosVersion == '11+' && ( macID == "" || !macID ) )
             window.location.href = calauthority
        if ( navigator.standalone && iosVersion == '11+' ){
            var separator = ( window.location.href.indexOf( "?" ) === -1 ) ? "?" : "&";
            if ( isExisting )
             window.location.href = window.location.origin + "?macid=" + macID + "&type=webapp";
            else
             window.location.href = window.location.href + separator + "type=webapp";
        }
    </script>


</head>
<body>
    <form id="form1" runat="server">
    <div class="ma-logo-for-installation">
    </div>
    <div class="content">
        <h1>
            Installation Instructions:</h1>
        <ul>
            <li>Tap on the Action button of the browser. <br /><img src="static/install/help/action-icon.jpg" /><br /></li>
            <li>Touch on the <strong>Add to Homescreen</strong> option.<br /><img src="static/install/help/addtohomescreen.png" /><br /></li>
            <li>Start <strong>CAMA Cloud<sup>&#174;</sup> MobileAssessor<sup>&#174;</sup></strong> from the Home Screen.</li>
            <li>Enter License Details, activate client and proceed to the application.</li>
        </ul>
    </div>
    </form>
</body>
</html>
