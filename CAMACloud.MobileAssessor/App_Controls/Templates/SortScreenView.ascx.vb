﻿
Partial Class App_Controls_SortScreenView
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim template1 As String = ClientSettings.ContentTemplate("sort-screen-normal")
            If template1 <> "" Then
                SortScreenNormalView.Controls.Clear()
                SortScreenNormalView.Controls.Add(New LiteralControl(template1))
            End If

            Dim template2 As String = ClientSettings.ContentTemplate("sort-screen-expanded")
            If template2 <> "" Then
                SortScreenExpandedView.Controls.Clear()
                SortScreenExpandedView.Controls.Add(New LiteralControl(template2))
            End If
            Dim template3 As String = ClientSettings.ContentTemplate("sort-screen-normal-PersonalProperty")
            If template3 <> "" Then
                SortScreenNormalViewBPP.Controls.Clear()
                SortScreenNormalViewBPP.Controls.Add(New LiteralControl(template3))
            End If

            Dim template4 As String = ClientSettings.ContentTemplate("sort-screen-expanded-PersonalProperty")
            If template4 <> "" Then
                SortScreenExpandedViewBPP.Controls.Clear()
                SortScreenExpandedViewBPP.Controls.Add(New LiteralControl(template4))
            End If
            
            Dim template5 As String = ClientSettings.ContentTemplate("mini-sort-screen-normal-PersonalProperty")
            If template5 <> "" Then
                MiniSSNormalViewBPP.Controls.Clear()
                MiniSSNormalViewBPP.Controls.Add(New LiteralControl(template5))
            End If
            
            Dim template6 As String = ClientSettings.ContentTemplate("mini-sort-screen-expanded-PersonalProperty")
            If template6 <> "" Then
                MiniSSExpandedViewBPP.Controls.Clear()
                MiniSSExpandedViewBPP.Controls.Add(New LiteralControl(template6))
            End If
            
        End If
    End Sub

End Class
