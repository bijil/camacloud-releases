﻿<%@ Control Language="VB" AutoEventWireup="false"
    Inherits="CAMACloud.MobileAssessor.App_Controls_PropertyRecordCard" Codebehind="PropertyRecordCard.ascx.vb" %>
<div id="prc-template" hidden>
 <div class="caution-prc hidden"></div>
    <!-- PRC TEMPLATE -->
    <asp:PlaceHolder runat="server" ID="ContentTemplate">
        <div class="prc-format">
            <table style="width: 96%; font-size: 10pt; border-spacing: 0px;">
                <tr>
                    <td style="width: 60%; vertical-align: top; padding-top: 8px; padding-bottom: 8px;">
                        <table style="width: 100%;">
                            <tr>
                                <td>
                                    Parcel: ${PARCEL_ID}
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td colspan="2">
                                    ${PAR_CITY}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td colspan="2">
                                    Owner: ${PAR_OWNER}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td>
                                    Address:
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                        <div class="vspacer">
                        </div>
                        <table style="width: 100%;">
                            <tr>
                                <td style="width: 33%;">
                                    NBHD: ${Number}
                                </td>
                                <td style="width: 67%;" colspan="2">
                                    Land Use: ${LUC}
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 33%;">
                                    Grade: ${CONST_QUAL}
                                </td>
                                <td style="width: 33%;">
                                    Cond: ${CONDITION}
                                </td>
                                <td style="width: 33%;">
                                    Occup: ${OCCUPANCY}
                                </td>
                            </tr>
                        </table>
                        <div class="vspacer">
                        </div>
                        <div>
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 20%">
                                        Adj Comp Sales:
                                    </td>
                                    <td style="width: 16%">
                                        ${ADJ_SALE_1:toIntMoney}
                                    </td>
                                    <td style="width: 16%">
                                        ${ADJ_SALE_2:toIntMoney}
                                    </td>
                                    <td style="width: 16%">
                                        ${ADJ_SALE_3:toIntMoney}
                                    </td>
                                    <td style="width: 16%">
                                        ${ADJ_SALE_4:toIntMoney}
                                    </td>
                                    <td style="width: 16%">
                                        ${ADJ_SALE_5:toIntMoney}
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="vspacer">
                        </div>
                        <div class="vspacer">
                        </div>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 33%;">
                                    Subj Sale Date: ${SALE_DATE_bb:toDateString}
                                </td>
                                <td style="width: 33%;">
                                    Mths Out: ${MONTH_OUT}
                                </td>
                                <td style="width: 33%;">
                                    Validity: ${SALE_VALID}
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 33%;">
                                    Subj Sale Price: ${SALE_AMOUNT_bb}
                                </td>
                                <td style="width: 33%;">
                                    Adj Fctr: ${ADJ_SALE_PRICE_FCTR}
                                </td>
                                <td style="width: 33%;">
                                    Adj Sale Price: ${ADJ_SALE_PRICE}
                                </td>
                            </tr>
                        </table>
                        <div class="vspacer">
                        </div>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 33%;">
                                    Land Ty: ${LAND_TYPE}
                                </td>
                                <td style="width: 33%;">
                                    Sewer: ${SEWER}
                                </td>
                                <td style="width: 33%;">
                                    Gas: ${GAS}
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 33%;">
                                    Topo$: ${TOPOGRAPHY}
                                </td>
                                <td style="width: 33%;">
                                    Water: ${WATER}
                                </td>
                                <td style="width: 33%;">
                                    Elec: ${ELECTRICITY}
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 33%;">
                                    Lt Shp: ${LOT_SHAPE}
                                </td>
                                <td style="width: 33%;">
                                    Acres: ${ACREAGE}
                                </td>
                                <td style="width: 33%;">
                                    LdInf: ${SITE_FACT}
                                </td>
                            </tr>
                        </table>
                        <table style="width: 100%;">
                            <tr>
                                <td style="width: 50%">
                                    Legal FF: ${LEGAL_FRONT}
                                </td>
                                <td>
                                    Legal Dp: ${LEGAL_DEPTH}
                                </td>
                            </tr>
                        </table>
                        <table style="width: 100%;">
                            <tr>
                                <td style="width: 25%">
                                    Eff Front: ${EFF_FRONT}
                                </td>
                                <td style="width: 25%">
                                    Avg Depth: ${AVG_DEPTH}
                                </td>
                                <td style="width: 25%">
                                    LotSz: ${SQUARE_FEET}
                                </td>
                                <td style="width: 25%">
                                    Fnl Land: ${FINAL_VALUE}
                                </td>
                            </tr>
                        </table>
                        <a onclick="showDataCollection(5);">CLICK FOR OTHER LAND LINES AND TO EDIT</a>
                        <div class="vspacer">
                        </div>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 33%;">
                                    RmTotal: ${ROOMS}
                                </td>
                                <td style="width: 33%;">
                                    BedRooms: ${BEDRM}
                                </td>
                                <td style="width: 33%;">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 33%;">
                                    FullBaths: ${FULLBATH}
                                </td>
                                <td style="width: 33%;">
                                    HalfBaths: ${HALFBATH}
                                </td>
                                <td style="width: 33%;">
                                    AddFixtures: ${ADDFIX}
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 33%;">
                                    FpOp: ${}
                                </td>
                                <td style="width: 33%;">
                                    Heat: ${HEAT_TYPE}
                                </td>
                                <td style="width: 33%;">
                                    A/C: ${AIR_COND}
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 33%;">
                                    BsmtTy: ${BSMT_TYPE}
                                </td>
                                <td style="width: 33%;">
                                    BaseBsmt: ${BSMT_FINISHD}
                                </td>
                                <td style="width: 33%;">
                                    SflaBsmt: ${BSMT_SQ_FT}
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 33%;">
                                    BaseAtt: ${BASEATT}
                                </td>
                                <td style="width: 33%;">
                                    SflaAtt: ${SFLAATT}
                                </td>
                                <td style="width: 33%;">
                                    TLA: ${LIV_AREA_TOT}
                                </td>
                            </tr>
                        </table>
                        <a onclick="showDataCollection(7);">CLICK FOR INTERIOR DETAILS AND TO EDIT</a>
                        <div class="vspacer">
                        </div>
                        <b>Outbuildings:</b>
                        <table style="width: 100%">
                            <tr>
                                <td>
                                    Use
                                </td>
                                <td>
                                    Wall
                                </td>
                                <td>
                                    Cond
                                </td>
                                <td>
                                    SF
                                </td>
                                <td>
                                    Year
                                </td>
                            </tr>
                            <tbody context="OUTBUILDINGS">
                                <tr>
                                    <td>
                                        ${OBUSE}
                                    </td>
                                    <td>
                                        ${OBWALL}
                                    </td>
                                    <td>
                                        ${OBCOND}
                                    </td>
                                    <td>
                                        ${OBAREA}
                                    </td>
                                    <td>
                                        ${OBYEAR}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <a onclick="showDataCollection(9);">CLICK FOR OTHER OBY's, MISC, MOD CODES, FACTORS</a>
                        <div class="vspacer">
                        </div>
                    </td>
                    <td style="width: 40%; vertical-align: top;">
                        <table style="width: 100%;">
                            <tr>
                                <td style="width: 140px;">
                                    Mkt Tot: ${MKT_EST_TOT}
                                </td>
                                <td>
                                    Prior Tot: ${CERTIFIED_TAX_TOTAL}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Med Comp: ${MED_COMP_VAL}
                                </td>
                                <td>
                                    Pri Exempt: ${CERTIFIED_EXEMPT_TOTAL}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Cost Tot: ${COST_TOTAL}
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                        <div class="bigbox">
                            <div style="text-align: center;">
                                <span style="display: none;">${SKETCH}</span>
                                <canvas height="200" width="200" id="prc-sketch">
                                </canvas>
                                <%--<img style="width: 300px; height: 200px;" srcx="${FirstSketch}" />--%>
                                <a onclick="showSketches();">CLICK FOR LARGER VIEW & OTHER SKETCHES</a>
                            </div>
                            <div class="vspacer">
                            </div>
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 120px;">
                                        <div class="smallbox">
                                            <div>
                                                PORCHES</div>
                                            <div>
                                                Open: ${OPPCH}</div>
                                            <div>
                                                Encl: ${ENCPCH}</div>
                                            <div>
                                                Deck: ${DECK}</div>
                                        </div>
                                        <div class="smallbox">
                                            <div>
                                                GARAGE</div>
                                            <div>
                                                Ty: ${GARTYPE}</div>
                                            <div>
                                                SF: ${GARSF}</div>
                                            <div>
                                                Yr: ${GARYR}</div>
                                            <div>
                                                Cnd: ${GARCOND}</div>
                                        </div>
                                        <div class="smallbox">
                                            <div>
                                                POOL</div>
                                            <div>
                                                Ty: ${POOL}</div>
                                            <div>
                                                SF: ${POOLSF}</div>
                                            <div>
                                                Yr: ${POOLYR}</div>
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            <img style="width: 200px; height: 160px;" srcx="${FirstPhoto}" />
                                            <a onclick="showPhotos();">CLICK FOR LARGER VIEW & OTHER PICS</a>
                                        </div>
                                        <table style="width: 100%;">
                                            <tr>
                                                <td style="width: 33%;">
                                                    Sty. Ht: ${STYLE}
                                                </td>
                                                <td style="width: 33%;">
                                                    Wall: ${WALL}
                                                </td>
                                                <td style="width: 33%;">
                                                    Style: ${STYLE}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 33%;">
                                                    Yr. Blt: ${YEARBLT}
                                                </td>
                                                <td style="width: 33%;">
                                                    Remod Yr: ${REMODYR}
                                                </td>
                                                <td style="width: 33%;">
                                                    Remod Ty: ${REMODTYP}
                                                </td>
                                            </tr>
                                        </table>
                                        <a onclick="showDataCollection(6);">CLICK FOR VIEW/EDIT STRUCTURAL DATA</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </asp:PlaceHolder>
    <!-- END OF PRC TEMPLATE -->
</div>
<div id="prc-template-bpp" hidden>
 <div class="caution-prc hidden"></div>
    <!-- BPP PRC TEMPLATE -->
    <asp:PlaceHolder runat="server" ID="ContentTemplate2">
        <div class="prc-format">
            <table style="width: 96%; font-size: 10pt; border-spacing: 0px;">
                <tr>
                    <td style="width: 60%; vertical-align: top; padding-top: 8px; padding-bottom: 8px;">
                        <table style="width: 100%;">
                            <tr>
                                <td>
                                    Parcel: ${PARCEL_ID}
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td colspan="2">
                                    ${PAR_CITY}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td colspan="2">
                                    Owner: ${PAR_OWNER}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td>
                                    Address:
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                        <div class="vspacer">
                        </div>
                        <table style="width: 100%;">
                            <tr>
                                <td style="width: 33%;">
                                    NBHD: ${Number}
                                </td>
                                <td style="width: 67%;" colspan="2">
                                    Land Use: ${LUC}
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 33%;">
                                    Grade: ${CONST_QUAL}
                                </td>
                                <td style="width: 33%;">
                                    Cond: ${CONDITION}
                                </td>
                                <td style="width: 33%;">
                                    Occup: ${OCCUPANCY}
                                </td>
                            </tr>
                        </table>
                        <div class="vspacer">
                        </div>
                        <div>
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 20%">
                                        Adj Comp Sales:
                                    </td>
                                    <td style="width: 16%">
                                        ${ADJ_SALE_1:toIntMoney}
                                    </td>
                                    <td style="width: 16%">
                                        ${ADJ_SALE_2:toIntMoney}
                                    </td>
                                    <td style="width: 16%">
                                        ${ADJ_SALE_3:toIntMoney}
                                    </td>
                                    <td style="width: 16%">
                                        ${ADJ_SALE_4:toIntMoney}
                                    </td>
                                    <td style="width: 16%">
                                        ${ADJ_SALE_5:toIntMoney}
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="vspacer">
                        </div>
                        <div class="vspacer">
                        </div>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 33%;">
                                    Subj Sale Date: ${SALE_DATE_bb:toDateString}
                                </td>
                                <td style="width: 33%;">
                                    Mths Out: ${MONTH_OUT}
                                </td>
                                <td style="width: 33%;">
                                    Validity: ${SALE_VALID}
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 33%;">
                                    Subj Sale Price: ${SALE_AMOUNT_bb}
                                </td>
                                <td style="width: 33%;">
                                    Adj Fctr: ${ADJ_SALE_PRICE_FCTR}
                                </td>
                                <td style="width: 33%;">
                                    Adj Sale Price: ${ADJ_SALE_PRICE}
                                </td>
                            </tr>
                        </table>
                        <div class="vspacer">
                        </div>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 33%;">
                                    Land Ty: ${LAND_TYPE}
                                </td>
                                <td style="width: 33%;">
                                    Sewer: ${SEWER}
                                </td>
                                <td style="width: 33%;">
                                    Gas: ${GAS}
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 33%;">
                                    Topo$: ${TOPOGRAPHY}
                                </td>
                                <td style="width: 33%;">
                                    Water: ${WATER}
                                </td>
                                <td style="width: 33%;">
                                    Elec: ${ELECTRICITY}
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 33%;">
                                    Lt Shp: ${LOT_SHAPE}
                                </td>
                                <td style="width: 33%;">
                                    Acres: ${ACREAGE}
                                </td>
                                <td style="width: 33%;">
                                    LdInf: ${SITE_FACT}
                                </td>
                            </tr>
                        </table>
                        <table style="width: 100%;">
                            <tr>
                                <td style="width: 50%">
                                    Legal FF: ${LEGAL_FRONT}
                                </td>
                                <td>
                                    Legal Dp: ${LEGAL_DEPTH}
                                </td>
                            </tr>
                        </table>
                        <table style="width: 100%;">
                            <tr>
                                <td style="width: 25%">
                                    Eff Front: ${EFF_FRONT}
                                </td>
                                <td style="width: 25%">
                                    Avg Depth: ${AVG_DEPTH}
                                </td>
                                <td style="width: 25%">
                                    LotSz: ${SQUARE_FEET}
                                </td>
                                <td style="width: 25%">
                                    Fnl Land: ${FINAL_VALUE}
                                </td>
                            </tr>
                        </table>
                        <a onclick="showDataCollection(5);">CLICK FOR OTHER LAND LINES AND TO EDIT</a>
                        <div class="vspacer">
                        </div>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 33%;">
                                    RmTotal: ${ROOMS}
                                </td>
                                <td style="width: 33%;">
                                    BedRooms: ${BEDRM}
                                </td>
                                <td style="width: 33%;">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 33%;">
                                    FullBaths: ${FULLBATH}
                                </td>
                                <td style="width: 33%;">
                                    HalfBaths: ${HALFBATH}
                                </td>
                                <td style="width: 33%;">
                                    AddFixtures: ${ADDFIX}
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 33%;">
                                    FpOp: ${}
                                </td>
                                <td style="width: 33%;">
                                    Heat: ${HEAT_TYPE}
                                </td>
                                <td style="width: 33%;">
                                    A/C: ${AIR_COND}
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 33%;">
                                    BsmtTy: ${BSMT_TYPE}
                                </td>
                                <td style="width: 33%;">
                                    BaseBsmt: ${BSMT_FINISHD}
                                </td>
                                <td style="width: 33%;">
                                    SflaBsmt: ${BSMT_SQ_FT}
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 33%;">
                                    BaseAtt: ${BASEATT}
                                </td>
                                <td style="width: 33%;">
                                    SflaAtt: ${SFLAATT}
                                </td>
                                <td style="width: 33%;">
                                    TLA: ${LIV_AREA_TOT}
                                </td>
                            </tr>
                        </table>
                        <a onclick="showDataCollection(7);">CLICK FOR INTERIOR DETAILS AND TO EDIT</a>
                        <div class="vspacer">
                        </div>
                        <b>Outbuildings:</b>
                        <table style="width: 100%">
                            <tr>
                                <td>
                                    Use
                                </td>
                                <td>
                                    Wall
                                </td>
                                <td>
                                    Cond
                                </td>
                                <td>
                                    SF
                                </td>
                                <td>
                                    Year
                                </td>
                            </tr>
                            <tbody context="OUTBUILDINGS">
                                <tr>
                                    <td>
                                        ${OBUSE}
                                    </td>
                                    <td>
                                        ${OBWALL}
                                    </td>
                                    <td>
                                        ${OBCOND}
                                    </td>
                                    <td>
                                        ${OBAREA}
                                    </td>
                                    <td>
                                        ${OBYEAR}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <a onclick="showDataCollection(9);">CLICK FOR OTHER OBY's, MISC, MOD CODES, FACTORS</a>
                        <div class="vspacer">
                        </div>
                    </td>
                    <td style="width: 40%; vertical-align: top;">
                        <table style="width: 100%;">
                            <tr>
                                <td style="width: 140px;">
                                    Mkt Tot: ${MKT_EST_TOT}
                                </td>
                                <td>
                                    Prior Tot: ${CERTIFIED_TAX_TOTAL}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Med Comp: ${MED_COMP_VAL}
                                </td>
                                <td>
                                    Pri Exempt: ${CERTIFIED_EXEMPT_TOTAL}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Cost Tot: ${COST_TOTAL}
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                        <div class="bigbox">
                            <div style="text-align: center;">
                                <span style="display: none;">${SKETCH}</span>
                                <canvas height="200" width="200" id="prc-sketch">
                                </canvas>
                                <%--<img style="width: 300px; height: 200px;" srcx="${FirstSketch}" />--%>
                                <a onclick="showSketches();">CLICK FOR LARGER VIEW & OTHER SKETCHES</a>
                            </div>
                            <div class="vspacer">
                            </div>
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 120px;">
                                        <div class="smallbox">
                                            <div>
                                                PORCHES</div>
                                            <div>
                                                Open: ${OPPCH}</div>
                                            <div>
                                                Encl: ${ENCPCH}</div>
                                            <div>
                                                Deck: ${DECK}</div>
                                        </div>
                                        <div class="smallbox">
                                            <div>
                                                GARAGE</div>
                                            <div>
                                                Ty: ${GARTYPE}</div>
                                            <div>
                                                SF: ${GARSF}</div>
                                            <div>
                                                Yr: ${GARYR}</div>
                                            <div>
                                                Cnd: ${GARCOND}</div>
                                        </div>
                                        <div class="smallbox">
                                            <div>
                                                POOL</div>
                                            <div>
                                                Ty: ${POOL}</div>
                                            <div>
                                                SF: ${POOLSF}</div>
                                            <div>
                                                Yr: ${POOLYR}</div>
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            <img style="width: 200px; height: 160px;" srcx="${FirstPhoto}" />
                                            <a onclick="showPhotos();">CLICK FOR LARGER VIEW & OTHER PICS</a>
                                        </div>
                                        <table style="width: 100%;">
                                            <tr>
                                                <td style="width: 33%;">
                                                    Sty. Ht: ${STYLE}
                                                </td>
                                                <td style="width: 33%;">
                                                    Wall: ${WALL}
                                                </td>
                                                <td style="width: 33%;">
                                                    Style: ${STYLE}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 33%;">
                                                    Yr. Blt: ${YEARBLT}
                                                </td>
                                                <td style="width: 33%;">
                                                    Remod Yr: ${REMODYR}
                                                </td>
                                                <td style="width: 33%;">
                                                    Remod Ty: ${REMODTYP}
                                                </td>
                                            </tr>
                                        </table>
                                        <a onclick="showDataCollection(6);">CLICK FOR VIEW/EDIT STRUCTURAL DATA</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </asp:PlaceHolder>
    <!-- END OF BPP PRC TEMPLATE -->
</div>
