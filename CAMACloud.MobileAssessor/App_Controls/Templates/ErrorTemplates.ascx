﻿<%@ Control Language="VB" ClassName="ErrorTemplates" %>
<div id="error-templates" style="display: none;">
    <div id="gps-access-denied">
        <h1>
            GPS Access Denied!</h1>
        <p>
            GPS information is not accessible! Either the location service is not available,
            or has not been permitted to use on this browser. <br />Please do the following procedure and reload the app again:<br /><br />
            >> <b>Settings</b> >> <b>General</b> >> Scroll down to find the option <b>Reset</b> >> Click <b>Reset Location & Privacy</b><br /><br />
            <b>Caution</b>: This will reset location permission for all applications currently using location services.
        </p>
    </div>
    <div id="no-network-connectivity">
        <h1>
            No Internet Connectivity!</h1>
        <p>
            An important operation from MobileAssessor requires active Internet connection
            via WiFi or 3G, which is found not to be available at this moment or location. Please
            restart the application when you have Internet connectivity again.</p>
        <p>
            <a class="b" href="about:blank" onclick="resetApp();return false;">Restart MobileAssessor now.</a></p>
    </div>
    <div id="inactive-gps">
        <h1>
            Inactive GPS!</h1>
        <p>
            You seem to have turned off both WiFi and Cellular Data, and that has caused your GPS to be inactive. If not, the device GPS is in error temporarily. Please try restarting the application or check your settings and try again later.</p>
        <p>
            <a class="b" href="about:blank" onclick="resetApp();return false;">Restart MobileAssessor now.</a></p>
    </div>
</div>