﻿<%@ Control Language="VB" AutoEventWireup="false"
	Inherits="CAMACloud.MobileAssessor.App_Controls_EstimateChart" Codebehind="EstimateChart.ascx.vb" %>
<div id="estimate-chart-template" style="display: none;">
    <asp:PlaceHolder runat="server" ID="ContentTemplate">
        <table style="width: 100%; border-collapse: collapse; border-color: #CFCFCF;" border="1" class="estimate-chart-table">
		                    <tr>
			                    <td colspan="6" class="b c">2013 Estimates of Value </td>
		                    </tr>
		                    <tr class="b c">
			                    <td style="width: 10%;"></td>
			                    <td style="width: 18%;">ASSESSED</td>
			                    <td style="width: 18%;">APPRAISED</td>
			                    <td style="width: 18%;">COST </td>
			                    <td style="width: 18%;">MARKET</td>
			                    <td style="width: 18%;">OPINION</td>
		                    </tr>
		                    <tr>
			                    <td class="b">&nbsp;Land </td>
			                    <td class="d">&nbsp;</td>
			                    <td class="d">&nbsp;</td>
			                    <td class="d">${cost_land_hstd_val+cost_land_non_hstd_val} </td>
			                    <td class="d">${land_hstd_val+land_non_hstd_val}</td>
			                    <td class="d">&nbsp;</td>
		                    </tr>
		                    <tr>
			                    <td class="b">&nbsp;Impr </td>
			                    <td class="d">&nbsp; </td>
			                    <td class="d">&nbsp; </td>
			                    <td class="d">${cost_imprv_hstd_val + cost_imprv_non_hstd_val} </td>
			                    <td class="d">${imprv_hstd_val + imprv_non_hstd_val} </td>
			                    <td class="d">&nbsp; </td>
		                    </tr>
		                    <tr>
			                    <td class="b">&nbsp;Total </td>
			                    <td class="d">${assessed_val}</td>
			                    <td class="d">${appraised_val}</td>
			                    <td class="d">${cost_value} </td>
			                    <td class="d">${market} </td>
			                    <td class="d">&nbsp;</td>
		                    </tr>
		                    <tr>
			                    <td class="b">&nbsp;$/SF </td>
			                    <td class="d">&nbsp;</td>
			                    <td class="d">&nbsp;</td>
			                    <td class="d">&nbsp; </td>
			                    <td class="d">&nbsp;</td>
			                    <td class="d">&nbsp;</td>
		                    </tr>
		                    <tr class="sort-screen-select" pid="${Id}" selectedtype="${SelectedAppraisalType}">
			                    <td>&nbsp; </td>
			                    <td >&nbsp;</td>
			                    <td >&nbsp;</td>
			                    <td >&nbsp;</td>
			                    <td >&nbsp;</td>
			                    <%--<td class="select" value="appr"><span class="not-selected">Select</span><span class="selected-type">Selected</span> </td>--%>
                                <td >&nbsp;</td>
		                    </tr>
	                    </table>
    </asp:PlaceHolder>
</div>
