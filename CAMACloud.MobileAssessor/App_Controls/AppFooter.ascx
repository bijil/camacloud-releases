﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="AppFooter.ascx.vb" Inherits="CAMACloud.MobileAssessor.AppFooter" %>
<footer class="page-footer unselectable" unselectable="on">
    <section class="logo" style="float: left;">
        <a class="ma-logo" onclick="refreshApp();">
            <div class="cache-update-bar">
                <div class="cache-update-progress">
                </div>
            </div>
            <div class="ma-version">
                <span>
                    v:<%= AssemblyVersion%> i:<% = AppStage() %>-<% = HttpContext.Current.GetCAMASession.OrganizationCharId%></span>
            </div>
        </a>
        <div class="vendor">
            Copyright &copy;
            <a href="http://www.datacloudsolutions.net/" target="_blank">Data Cloud Solutions, LLC</a><br />
            <a href="about:blank" onclick="showAgreement(false, true); return false;">Confidentiality Agreement</a>&nbsp;
            <span class="app-service-code">S/Code: <span><% = CAMACloud.MobileAssessor.General.DebuggerKey %></span></span>
            
        </div>
    </section>
    <section class="tools" style="float: right;">
        <div style="">
            <a class="search image-button" style="display: none;"></a>
            <a class="sort image-button" style="display: none;"></a>
            <a class="dashboard image-button" style="display: none; margin-left: 0px;"></a>
            <a class="sync image-button" style="display: none;"></a>
            <a class="logout image-button" style="display: none;"></a>
        </div>
    </section>
    <script>
         var _app_version = '<%= AssemblyVersion%>';
    </script>
</footer>
