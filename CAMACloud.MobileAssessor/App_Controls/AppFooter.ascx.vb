﻿Public Class AppFooter
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Public Function AssemblyVersion() As String
        Return My.Application.Info.Version.ToString
    End Function

    Public Function AppStage() As String
        HttpContext.Current.GetCAMASession()
        Select Case CAMACloudHost.GetWorkingHost.Stage
            Case HostStage.Production
                Return "L"
            Case HostStage.ReleaseCandidate
                Return "R"
            Case HostStage.Beta
                Return "B"
            Case HostStage.Alpha
                Return "Q"
            Case HostStage.LocalHost
                Return "D"
            Case Else
                Return "?"
        End Select
    End Function

End Class