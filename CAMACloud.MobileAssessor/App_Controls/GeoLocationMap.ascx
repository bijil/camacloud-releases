﻿<%@ Control Language="vb" AutoEventWireup="false" ClassName="GeoLocationMap" %>
<div class="GeoLocationMap" style='display: none; background-color: white; position: absolute;
    top: 20%;'>
    <div style='padding: 4px 3px; font-size: 18px; font-weight: bold; background: #BBB'>
        Select Geo Location</div>
    <div id='GeoMAP' style='width: 100%; height: 100%;'>
    </div>
    <div style='padding: 5px; background: #EFEFEF; text-align: right; padding-right: 48px;'>
        <button onclick='CloseGeoLocation()' style=' margin-right:10px;'>
            Cancel</button>
        <button onclick='SetGeoLocationInField()'>
            Save</button></div>
</div>
