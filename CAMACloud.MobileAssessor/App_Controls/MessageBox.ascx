﻿<%@ Control Language="vb" AutoEventWireup="false" ClassName="MAMessageBox" %>
<div class="window-message-box hidden">
    <div class="full-frame dimmer">
    </div>
    <div class="full-frame">
        <div class="message-box-frame">
            <div class="message-box-title">
                CAMA Cloud&#174;</div>
            <div class="message-box-body" style="word-wrap:break-word">
                ABCD</div>
            <div class="message-box-input">
                <input type="text" class="message-box-input-item message-box-input-text" />
                <select class="message-box-input-item message-box-input-sel">
                </select>
                <textarea class="message-box-input-item message-box-input-textarea" cols="20" rows="3"></textarea>
                <div class="message-box-input-filters"></div>
            </div>
            <div class="message-box-buttons">
                <a class="button message-box-ok">OK</a> <a class="button message-box-cancel">Cancel</a>
            </div>
        </div>
    </div>
</div>
