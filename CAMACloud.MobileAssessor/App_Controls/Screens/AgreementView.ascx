﻿<%@ Control Language="VB" ClassName="AgreementView" %>
<script>

let showLicense = function (e) {	
	e.preventDefault();
	console.log(e);
	showMask("<span style='width: 100vw;position: relative;left: -47%;'>Please wait.. Fetching License Information.</span>");
	let relink = window.location.host && (window.location.host.contains('.ca.camacloud.com') || window.location.host.contains('.ca-beta.camacloud.com'))? 'http://auth.ca.camacloud.com/change': 'http://auth.camacloud.com/change';
	return window.location.href = relink;
};


</script>
<section id="viewagreement" class="screen">
    <header>
        <span>User Agreement</span>
        <a class="page-back"></a>
    </header>
    <div style="padding: 10px;">
        <table style="width: 100%">
            <tr>
                <td colspan="2" style="padding: 4px; background-color: #CFCFCF; font-weight: bold; font-size: larger;"><span class="app-href"></span></td>
            </tr>
            <tr>
                <td style="padding: 4px;">Licensed to: <b>
                    <%= HttpContext.Current.GetCAMASession.OrganizationName %></b></td>
                <td style="text-align: right; font-style: italic; font-weight: bold; padding: 4px;">
                    <%= IIf(CAMACloudHost.GetWorkingHost.Stage <> HostStage.Production, CAMACloudHost.GetWorkingHost.Stage.ToString, "")%></td>
            </tr>
            <tr>
            	<td style="padding: 4px;">Licensed User: <b> <span class="uname"></span></b></td>
            </tr>
        </table>
    </div>
    <div>
        <cc:LicenseAgreement ID="LicenseAgreement1" runat="server" />
        <div align="center">
            <span class="button" id="showlicense" onclick="showLicense(event)" style="position: relative;">Show License</span> 
        </div>
    </div>
</section>
