﻿<%@ Control Language="VB" ClassName="GoogleMap" %>
<section class="screen" id="gis-map">
    <header>
        <span>GIS Interactive Map</span> <a class="page-back"></a>
        <div class="googlemapsetup hidden" style="float: right"><a class="map-pri-option map-pri-only" style="display: none" onclick="loadPrioritiesOnlyInMap();"></a><a class="map-pri-option map-show-all" style="display: none" onclick="loadAllParcelsInMap();"></a><a class="map-pri-option map-Boundaries" style="display: none; width: 117px;" onclick="BoundariesONOFF();"></a></div>
        <div class="osmapsetup hidden" style="float: right">              
            <button class="map-woolpert map-show-woolpert" style=" float: left; margin-left: 5px;" onclick="showWoolpert();">Show Woolpert Imagery</button>
            <button class="map-woolpert map-hide-woolpert" style="display: none; float: left; margin-left: 5px;" onclick="hideWoolpert();">Hide Woolpert Imagery</button>
            <button class="map-pictometry map-show-pictometry" style="display: none; float: left; margin-left: 5px;" onclick="showPictometryInMap();">Show EagleView</button>
            <button class="map-pictometry map-hide-pictometry" style="display: none; float: left; margin-left: 5px;" onclick="hidePictometryInMap();">Hide EagleView</button>
            <button class="map-ev map-show-ev" style="display: none; float: left; margin-left: 5px;" onclick="showEagleViewInMap();">Show EagleViewNew</button>
            <button class="map-ev map-hide-ev" style="display: none; float: left; margin-left: 5px;" onclick="hideEagleView();">Hide EagleViewNew</button>
            <button class="map-nearmap map-show-nearmap" style="display: none; float: left; margin-left: 5px;" onclick="showNearmap();">Show Nearmap</button>
            <button class="map-nearmap map-hide-nearmap" style="display: none; float: left; margin-left: 5px;" onclick="hideNearmap();">Hide Nearmap</button>
            <button class="map-nearmapwms map-show-nearmapwms" style="display: none; float: left; margin-left: 5px;" onclick="showNearmapWMS();">Show WMS Nearmap</button>
            <button class="map-nearmapwms map-hide-nearmapwms" style="display: none; float: left; margin-left: 5px;" onclick="hideNearmapWMS();">Hide WMS Nearmap</button>
            <button class="map-Imagery map-show-Imagery" style="display: none; float: left; margin-left: 5px;" onclick="showImagery();">Show Imagery</button>
            <button class="map-Imagery map-hide-Imagery" style="display: none; float: left; margin-left: 5px;" onclick="hideImagery();">Hide Imagery</button>
            <select class='heatmap-items' style="display: none"></select>
            <select class='map-items' style="display: none"></select>
            <a class="Setting" title="Map Options" id="lblpop" style="display: block; float: left; margin-left:4px;" onclick="ShowMapTypePopUp();"></a>           
        </div>
        <div class="q_public hidden" style="float: right; display: none; margin-right: 10px;">
            <button class="q_public_btn" onclick="show_qPublic();">Show qPublic</button>
        </div>
        <div class="header__buttons" style="float:left;width:100%">
            <div class="svMA" style="margin-top:6px;display:none">
                <div class="select-div">
        	        <select class='sketch-select' style="padding: 0px 2px;"></select>
        	        <select class='section-select' style="padding: 0px 2px;"></select>
                </div>
        	    <button class="btnrch" id="btnSaveRvw">Save</button>
        	    <button class="btnrch" id="btnPrc" onclick="_nav(this);showDigitalPRC();" style="margin-right: 4px;">Open PRC</button>
        	    <button class="btnrch" id="btnSktch" onclick="_nav(this);showSketches()" style="margin-right: 4px;">Open Sketch Editor</button>
            </div>
            
        </div>
    </header>
      <div id="divpop" class="modalDialog hidden" style="width: 223px; height: auto; border:1">
        <table class='poptable' id='tblpopup'>
            <tr id="showLines">
                <td>Show Lines</td>
                <td>
                    <div class='onoffswitch'>
                        <input type='checkbox' class='onoffswitch-checkbox' id='myonoffswitch2' >
                        <label class='onoffswitch-label' >
                            <span class='onoffswitch-inner'></span>
                            <span class='onoffswitch-switch'></span>
                        </label>
                    </div>
                </td>
            </tr>

            <tr id="showall">
                <td>Show All Cached</td>
                <td>
                    <div class='onoffswitch'>
                       
                      <input type='checkbox' class='onoffswitch-checkbox' id='myonoffswitch3' >
                        <label class='onoffswitch-label' >
                            <span class='onoffswitch-inner'></span><span class='onoffswitch-switch'></span>
                        </label>
                    </div>
                </td>
            </tr>

            <tr id="heatMap">
                <td>Heat Map</td>
                <td>
                    <div class='onoffswitch'>
                        <input type='checkbox' class='onoffswitch-checkbox heatclass' id='myonoffswitch4' >
                        <label class='onoffswitch-label' >
                            <span class='onoffswitch-inner'></span><span class='onoffswitch-switch'></span>
                        </label>
                    </div>
                </td>
            </tr>
            <tr>
                <td>Streets Layer</td>
                <td>
                    <div class='onoffswitch'>
                        <input type='checkbox' class='onoffswitch-checkbox' id='myonoffswitch6' >
                        <label class='onoffswitch-label' >
                            <span class='onoffswitch-inner'></span><span class='onoffswitch-switch'></span>
                        </label>
                    </div>
                </td>
            </tr>
         <tr >
             <td>Google Map</td>
                <td>
                    <div class='onoffswitch googleclass'>
                        <input type='checkbox' class='onoffswitch-checkbox' id='myonoffswitch7' >
                        <label class='onoffswitch-label'>
                            <span class='onoffswitch-inner'></span><span class='onoffswitch-switch'></span>
                        </label>
                    </div>
                </td>
            </tr>
            <tr>
             <td id='alternateMap'>Alternate Map</td>
                <td>
                    <div class='onoffswitch customclass'>
                        <input type='checkbox' class='onoffswitch-checkbox' id='myonoffswitch5'>
                        <label class='onoffswitch-label'>
                            <span class='onoffswitch-inner'></span><span class='onoffswitch-switch'></span>
                        </label>
                    </div>
                </td>
            </tr>
            <tr>
                <td style='text-align: center'>
                    <input type='button' class='myButton' value='Submit' onclick='SubmitMapType();'></td>
                <td style='text-align: center'>
                    <input type='button' class='myButton' value='Cancel' onclick='HideMapPopup();'></td>
            </tr>
        </table>
    </div>
    <div id="Div1">
    </div>
    <div id="mapCanvas">
    </div>
    <div id="pictometry" style="display: none;">
    	<iframe id="pictometry_ipa" width="100%" height="100%" src="about:blank"></iframe>
    </div>
    <div id="woolpert" style="display: none;">
			 <div class="imagery" id="imagery">
			  <div class="selection">
			    <input id="layertype1" name='layertype' type="radio" onchange="layerSelect();" checked>
			    <label for="layertype1">6" Imagery</label>
			  </div>
			  <div class="selection">
			    <input id="layertype2" name="layertype" onchange="layerSelect();" type="radio">
			    <label for="layertype2">3" Imagery</label>
			  </div>
			</div>
    		<div id="menu" style="position: fixed; top: 160px; left: 7px;z-index:100; ">
    		<input type="image" src="static/css/images/map-street-icon.png" id="zoom-restore" style="width:40px;height:40px;padding:0px;outline:none">
			</div>
    </div>
    <div id="nearmap" style="display: none;">
        <div class="heading-btn">
            <div class="center">
                <input type="image" title="North" src="/static/css/images/up.png" id="northElementId" value="North" class="directionButton"/>
            </div>
            <div style="padding: 5px; height: 32px;">
                <input type="image" title="West" src="/static/css/images/left.png" id="westElementId" value="West" class="directionButton"/>
                <input type="image" title="Vertical" src="/static/css/images/vert.png" id="vertElementId" alt="Vertical" value="Vert" class="directionButton"/>
                <input type="image" title="East" src="/static/css/images/right.png" id="eastElementId" value="East" class="directionButton"/>
            </div>
            <div class="center">
                <input type="image" title="South" src="/static/css/images/down.png" id="southElementId" class="directionButton" value="South"/>
            </div>
        </div>
        <div class="optionDiv noselect measureTools" style="right: 150px; flex-direction: column">
            <div style="display: flex; justify-content: space-between; padding-left: 3px"><span>Measure</span><span class="tooltip opacity-group">&nbsp;</span></div>
            <div class='measure-tool'>
                <div style="display: flex; flex-direction: row; justify-content: center;">
                    <div class="radio-group opacity-group">
                        <input type="radio" id="option-one" name="measure-selector" checked="checked" value="none" />
                        <label for="option-one">None</label>
                        <input type="radio" id="option-two" name="measure-selector" value="length" />
                        <label for="option-two">Length</label>
                        <input type="radio" id="option-three" name="measure-selector" value="area" />
                        <label for="option-three">Area</label>
                    </div>
                    <button class="clearAll opacity-group wms-button">Clear</button>
                    <button class="drawUndo wms-button" style="display: none;">Undo</button>
                </div>
            </div>
        </div>
        <div class="optionDiv noselect opacity-group">
            <div>
                <span style="display: block;">Available surveys</span>
                <select class='nm-custom-select' style="min-width: 110px; margin-left: 0px"></select>
            </div>
            <div style="display: none;">
                <span>Selected</span>
                <div id="selectedSurveyElementId"></div>
            </div>
            <div style="display: none;">
                <span>Displayed</span>
                <div id="displayedSurveyElementId"></div>
            </div>
        </div>
        <div id='wms-mask' style="display: none;">
            <div class="lds-ripple">
	            <div></div>
	            <div></div>
            </div>
        </div>
    </div>
    <div id="eagleView" style="display: none;">
    </div>
    <div id="nearmapwms" style="display: none;">
        <div class="heading-btn">
            <div class="center">
                <input type="image" title="North" src="/static/css/images/up.png" id="northElementIdwms" value="North" class="directionButton"/>
            </div>
            <div style="padding: 5px; height: 32px;">
                <input type="image" title="West" src="/static/css/images/left.png" id="westElementIdwms" value="West" class="directionButton"/>
                <input type="image" title="Vertical" src="/static/css/images/vert.png" id="vertElementIdwms" alt="Vertical" value="Vert" class="directionButton"/>
                <input type="image" title="East" src="/static/css/images/right.png" id="eastElementIdwms" value="East" class="directionButton"/>
            </div>
            <div class="center">
                <input type="image" title="South" src="/static/css/images/down.png" id="southElementIdwms" class="directionButton" value="South"/>
            </div>
        </div>
        <div class="optionDiv noselect measureTools" style="right: 150px; flex-direction: column">
            <div style="display: flex; justify-content: space-between; padding-left: 3px"><span>Measure</span><span class="tooltip opacity-group">&nbsp;</span></div>
            <div class='measure-tool'>
                <div style="display: flex; flex-direction: row; justify-content: center;">
                    <div class="radio-group opacity-group">
                        <input type="radio" id="option-onewms" name="measure-selector" checked="checked" value="none" />
                        <label for="option-onewms">None</label>
                        <input type="radio" id="option-twowms" name="measure-selector" value="length" />
                        <label for="option-twowms">Length</label>
                        <input type="radio" id="option-threewms" name="measure-selector" value="area" />
                        <label for="option-threewms">Area</label>
                    </div>
                    <button class="clearAll opacity-group wms-button">Clear</button>
                    <button class="drawUndo wms-button" style="display: none;">Undo</button>
                </div>
            </div>
        </div>
        <div class="optionDiv noselect opacity-group">
        <div>
            <span style="display: block;">Available surveys</span>
            <select class='custom-select' style="min-width: 110px; margin-left: 0px"></select>
        </div>
        <div style="display: none;">
            <span>Selected</span>
            <div id="selectedSurveyElementIdwms"></div>
        </div>
        <div style="display: none;">
            <span>Displayed</span>
            <div id="displayedSurveyElementIdwms"></div>
        </div>
    </div>
    <div id='wms-maskwms' style="display: none;">
    	<div class="lds-ripple">
		    <div></div>
		    <div></div>
    	</div>
    </div>
    </div>
    <div id="qPublic" style="display: none;" class="qpublicScroll">
    	<iframe id="qPublic_frame" width="100%" height="100%" src="about:blank"></iframe>
    </div>
</section>
<section class="screen" id="route-map">
    <header>
        <span>GIS Interactive Map</span> <a class="page-back"></a>
    </header>
    <div id="routeMapCanvas">
    </div>
</section>
