﻿<%@ Control Language="VB" ClassName="App_Controls_SketchEditor" %>
<section class="screen sketch-editor" id="sketchedit">
    <header>
        <span>Sketch Editor</span> <a class="page-back"></a>
    </header>
    <div class="editor">
        <div class="editorstatus">
            <table style="width: 100%;">
                <tr>
                    <td>
                        <span class="sketchinfo" id="lblSketchInfo">Use three fingers to scroll, pinch to resize.</span>
                    </td>
                    <td style="width: 85px;" class="label">
                        Line Length:
                    </td>
                    <td style="width: 100px;">
                        <input class="sedtxt sed-length" type="number" maxlength="3" min="0" max="500" id="txtSEDLength"
                            readonly="readonly" />
                    </td>
                    <td style="width: 95px;" class="label">
                        Polygon Area:
                    </td>
                    <td style="width: 100px; text-align: right; padding-right: 4px;">
                        <input class="sedtxt sed-area" id="txtSEDArea" readonly="readonly" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="sketchcanvasholder">
            <canvas id="sketchcanvas">
            </canvas>
            <div class="sketch-inline-window sketch-mapview" id="sketchMapView" style="display: none;">
                <div class="iw-title-bar">
                    <span>Map View</span> <a>&nbsp;</a>
                </div>
                <div class="sketch-inline-map" id="sketchInlineMap" width="354" height="360">
                </div>
            </div>
            <div class="sketch-inline-window sketch-annotation" id="sketchAnnotation" style="display: none;">
                <div class="iw-title-bar">
                    <span>Annotation</span> <a>&nbsp;</a>
                </div>
                <div class="sketch-annotation-container">
                    <span class="inputlabel">Notes</span>
                    <div>
                        <textarea class="sedannotate" id="txtSketchAnnotation" rows="3" onchange="saveAnnotation()"></textarea>
                    </div>
                </div>
            </div>
            <div class="sketch-inline-window sketch-quickhelp" id="sketchHelp" style="display: none;">
                <div class="iw-title-bar">
                    <span>Quick Help</span> <a>&nbsp;</a>
                </div>
                <div class="sketch-help-box" width="354" height="360">
                    <cc:SketchEditorHelp runat="server" />
                </div>
            </div>
        </div>
        <div class="editorcontrols">
            <table class="layout">
                <tr>
                    <td class="l1">
                        <div>
                            Select/Add Shapes:
                        </div>
                        <div>
                            <select class="sketchlayers" id="sketchLayerSelector">
                            </select>
                        </div>
                        <div class="sed-horizpanel">
                            <a class="sedbtn" wide secmd="vector.create" img="addshape"></a><a class="sedbtn"
                                secmd="vector.edit" img="edit"></a><a class="sedbtn" secmd="vector.delete" img="del">
                                </a><a class="sedbtn" secmd="vector.move" img="vector-move"></a>
                        </div>
                    </td>
                    <td class="sediv">
                    </td>
                    <td class="l3">
                        <table class="sedbtnlayout">
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <a class="sedbtn" secmd="cursor.up" img="up" />
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="sedbtn" secmd="cursor.left" img="left" />
                                </td>
                                <td>
                                    <a class="sedbtn" secmd="cursor.down" img="down" />
                                </td>
                                <td>
                                    <a class="sedbtn" secmd="cursor.right" img="right" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <a class="sedbtn" wide secmd="vector.commit" img="vector-ok-wide" />
                                    <%--<a class="sedswt" secmd="sketch.splitter" img="splitnode" />--%>
                                </td>
                                <td>
                                    <a class="sedswt" secmd="sketch.eraser" img="delnode" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="sediv semarkuptools">
                    </td>
                    <td class="l5 semarkuptools">
                        <table class="sedbtnlayout">
                            <tr>
                                <td>
                                    <a class="sedbtn" secmd="sketch.scribble" img="scribble-pen" />
                                </td>
                                <td>
                                    <a class="sedswt" secmd="sketch.scribble.eraser" img="scribble-eraser" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="sedbtn" secmd="underlay.rotate.left" img="rotate-left" />
                                </td>
                                <td>
                                    <a class="sedbtn" secmd="underlay.rotate.right" img="rotate-right" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="sedbtn" secmd="underlay.zoom.in" img="zoom-in" />
                                </td>
                                <td>
                                    <a class="sedswt" secmd="underlay.zoom.out" img="zoom-out" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="sediv">
                    </td>
                    <td class="l4">
                        <table class="sedbtnlayout">
                            <tr>
                                <td>
                                    <a class="sedswt" wide secmd="window.map" img="aerialview" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="sedswt" wide secmd="window.annotate" img="annotate" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="sediv">
                    </td>
                    <td class="l4">
                        <table class="sedbtnlayout">
                            <tr>
                                <td>
                                    <a class="sedbtn" wide secmd="sketch.done" img="savesketch" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="sedbtn" wide secmd="sketch.reset" img="resetall" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</section>