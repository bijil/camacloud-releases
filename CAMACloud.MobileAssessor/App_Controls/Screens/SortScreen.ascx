﻿<%@ Control Language="VB" ClassName="SortScreen" %>
<section class="screen" id="sort-screen">
    <div class="nbhd-profile-float hidden">
        <table style="height: 80px; width: 95%; text-align: center;">
            <tr>
                <td>
                    Loading <%=CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName()%> profile ...
                </td>
            </tr>
        </table>
    </div>
    <header>
        <label class="select-parcelspanHeader"><label class="selectParcelCheckbox"></label></label><span class='headerspan'>Priority Parcels</span> <a id="ss-view-type" class="change-sort-view csv-proximity">
        </a><a class="sort-view"></a><a class="standalone-bpp" style='display:none'></a><a class="page-back"  style='display:none'></a><a class="map-view"></a>  <cc:FYLSwitch runat="server" /> <a class="sort-toggle" style='display:none'><a class="multiple-mac" style='display:none'></a>
    </header>
    <div id="parcel-scroll" class="scrollable">
        <div id="my-parcels">
        </div>
    </div>
</section>