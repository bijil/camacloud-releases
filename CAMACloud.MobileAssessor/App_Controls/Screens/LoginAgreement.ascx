﻿<%@ Control Language="VB" ClassName="LoginAgreement" %>
<script runat="server">

</script>
<section id="agreement" class="screen">
    <div class="scrollable">
        <cc:LicenseAgreement ID="LicenseAgreement1" runat="server" />
        <p style="width: 100%; text-align: center;" class="login-buttons">
            <button style="padding: 20px 40px; margin-right: 20px;" onclick="LoginOnAgree(this);">
                I Agree</button>
            <button style="padding: 20px 40px; margin-left: 20px;" onclick="Logout();">
                Logout</button>
        </p>
        <p style="" class="login-progress">
            <div class="login-progress-bar">
            </div>
        </p>
    </div>
</section>
