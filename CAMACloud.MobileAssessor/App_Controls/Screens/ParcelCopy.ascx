﻿<%@ Control Language="vb" ClassName="ParcelCopyScreen" %>
<section class="screen" id="copyParcel">
 <header>
        <span>Copy Improvements</span> <a class="page-back"></a>
    </header>
<div class="parcel-copy-panel">
  <table class="parcel-copy-params">
  <tr>
                <td colspan="2" style="text-align: center; font-size: 2em; padding: 8px;">
                    Copy Improvements
                </td>
  </tr>
  <tr>
        <td width="220px">
            Copy From:
        </td>
        <td>
            <input type="text" style="width:176px" id="copyType" onchange="return ToggleParcelID(this);"/>
<%--            <select id="copyType" onchange="return ToggleParcelID(this);">
                 <option value="op">Other Parcel</option>--%>
               <%-- <option value="tp">Template Parcel</option>
                 <option value="sp">Same Parcel</option>--%>
               
           <%-- </select>--%>
        </td>
  </tr>
  
   <tr searchcriteria>
        <td>
            Search Criteria:
        </td>
        <td>
            <input type="text" style="width:176px" id="searchCriteria"  onchange="return searchCriteriaClick(this);"/>
<%--         <select id="searchCriteria" onchange="return searchCriteriaClick(this);">
                 <option value="tp">Street Address</option>
                 <option value="pid">Parcel ID</option>
            </select>--%>
        </td>
  </tr>

  <tr parcelid>
        <td>
            Parcel ID:
        </td>
        <td>
            <input type="text" id="parcelIds_copy"  onkeyup="return searchPSCombined(this);"/>
        </td>
  </tr>
  <tr streetaddress>
        <td>
            Street Address:
        </td>
        <td>
            <input type="text" id="streetAddress_copy"  onkeyup="return searchPSCombined(this);"/>
        </td>
  </tr>
  <tr pscombined>
        <td>
        Parcel ID, Street Address:
        </td>
        <td>
        <input type="text" style="width:176px" id="psCombined"/>
        </td>
  </tr>
  <tr fields>
        <td>
            Fields
        </td>
        <td>
            <ul class="ccSelect">
            </ul>
        </td>
  </tr>
  <tr>
                 <td colspan="2" style="text-align:center;">
                    <button class="btn-parcel-copy" onclick="return startCopying();">Copy</button>
                </td>
</tr>
  </table>
</div>
</section>
