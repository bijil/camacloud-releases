﻿<%@ Control Language="VB" ClassName="DigitalPRC" %>
<script runat="server">

</script>
<section class="screen" id="digital-prc">
    <header>
        <span>Property Record Card</span>
<a class="create-Bpp-parcel create-Bpp-parcel-photo" style="position: absolute;right: 140px;z-index: 9;top: -1px;height: 24px;border: 0px;padding: 0px 12px !important;border-radius: 0%;display: none;" onclick="createNewParcel(activeParcel.IsParentParcel == 'true' ?  activeParcel.Id : activeParcel.ParentParcelID)"></a>
<a class="mark-as-delete delete-Bpp-parcel-photo" style="padding: 0px 12px !important;margin-right: 10px;height: 20px;position: absolute;right: 100px;z-index: 9;top: 3px;border: 0px;border-radius: 0%;display:none;" onclick=" markAsDelete(activeParcel.Id,true)"></a>
<a class="page-back"></a>
    </header>    <cc:FYLSwitch runat="server" />
    <div class="scrollable">
        <% If ClientSettings.PropertyValue("EnableNewPriorities") = "1" Then %>
        <div style="padding: 0px 20px 3px 20px; font-size: 15px; font-weight: bold;">
            Priority: <span class="prc_priority"></span>
        </div>
        <%End If %>
        <div class="prc-card">
        </div>
    </div>
</section>