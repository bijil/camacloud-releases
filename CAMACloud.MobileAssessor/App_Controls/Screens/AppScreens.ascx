﻿<%@ Control Language="VB" ClassName="AppScreens" %>
<script runat="server">

</script>
<section id="gpsloading" class="screen">
    <div id="gpsstatus" class="gpsstatus">
    </div>
</section>
<section id="loading" class="screen" style="background: transparent !important">
    <div>
    </div>
</section>
<section class="screen" id="progress">
    <div id="progressbar">
        <span>Syncing ...</span>
        <div id="pbar">
        </div>
    </div>
</section>
<section class="screen" id="error">
    <div id="errordisplay">
        <!-- ERROR MESSAGE WILL REPLACE THIS PLACE -->
    </div>
</section>