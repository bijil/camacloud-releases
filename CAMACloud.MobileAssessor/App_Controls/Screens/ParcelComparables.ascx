﻿<%@ Control Language="VB" ClassName="ParcelComparables" %>
<script runat="server">

</script>
<section class="screen" id="comparables">
    <header>
        <span>Comparables Report</span> 
        <a class="page-back"></a>
    </header>
    <div class="scrollable">
        <div style="margin: 10px; display: flex; justify-content: space-between; align-items: center;">
            <div style="display: flex; align-items: center;">
                <span style="margin-left: 10px; display: flex; align-items: center;">
                    <label style="margin-right: 10px;">Field Alert</label>
                    <input type="text" class="comp-field-alert-flag" style="width: auto; min-width: auto; height: 30px;"/>
                </span>
                <span style="display: flex; align-items: center;" onclick="bindcompalertClick();">
                    <label style="margin-left: 10px;">Alert From Field</label>
                    <textarea readonly class="field-alert-message" style="margin-left: 10px; min-width: 10%; height: 30px; font-size: smaller; resize: none;" rows="3"></textarea>
                </span>
            </div>
            <button class="comp-mac-parcel" style="margin-right: 10px;" onclick="utils.MarkAsComplete()">Mark As Complete</button>
        </div>
        <div class="parcel-comparables">
        </div>
        <div id="comp-estimate-chart">
        </div>
    </div>
</section>
