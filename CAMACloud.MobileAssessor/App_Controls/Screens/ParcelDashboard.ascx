﻿<%@ Control Language="VB" ClassName="ParcelDashboard" %>
<script runat="server">

</script>
<section class="screen" id="parcel-dashboard">
       <header>   
        <div>
            <table style="width:100%">
                <tr>
                    <td><span>Parcel Dashboard</span></td>
                    <td>
                        <div class="parcel-header-temp hidden"></div>
                    </td>
                    <td>  <a class="page-back"></a> <%--<a class="future-year" onclick="futureYearpopup()"></a>--%>

                    </td>
                </tr>
            </table>
           
        </div>
    </header><a class="create-Bpp-parcel create-Bpp-parcel-photo text-button" style="position: absolute;right: 160px;z-index: 9;top: 0px;height: 24px;border: 0px;border-radius: 0%;padding: 0px 12px !important;display:none;"></a> 
    <cc:FYLSwitch runat="server" />
    
<%--     <div class="future-popup modalDialog hidden" style=" right: 71px;">
                 <table class='poptable' id='tblpopup'>
                    <tr id="ShowFutureData">
                        <td>Show Future Year Data</td>
                        <td>
                            <div class='onoffswitch'>
                                <input type='checkbox' class='onoffswitch-checkbox' id='myonoffswitch22' >
                                <label class='onoffswitch-label' >
                                    <span class='onoffswitch-inner'></span>
                                    <span class='onoffswitch-switch'></span>
                                </label>
                            </div>
                        </td>

                    </tr>
                      <tr id="include_images_in_copy">
                        <td>Include images in Future Year Copy</td>
                        <td>
                            <div class='onoffswitch'>
                                <input type='checkbox' class='onoffswitch-checkbox' id='myonoffswitch23' >
                                <label class='onoffswitch-label' >
                                    <span class='onoffswitch-inner'></span>
                                    <span class='onoffswitch-switch'></span>
                                </label>
                            </div>
                        </td>

                    </tr>
                     <tr>
                        <td style='text-align: center'>
                            <input type='button' class='myButton' value='Submit' onclick='SubmitFutureYear();'></td>
                        <td style='text-align: center'>
                            <input type='button' class='myButton' value='Cancel' onclick='HideMapPopup();'></td>
                    </tr>
                 </table>
            </div>--%>
    <div class="scrollable">
        <menu>
            <cc:ScreenMenu runat="server" ID="smDigitalPRC" Action="showDigitalPRC();" CssClass="nav-digital-prc" Title="Property Record Card" Info="View all details in Property Record Card format (Digital PRC)" />
            <cc:ScreenMenu runat="server" ID="smDEC" Action="showDataCollection();" CssClass="nav-data-collection" Title="Data Collection Card" Info="Start entering data" />
            <cc:ScreenMenu runat="server" ID="smSketches" Action="showSketches()" CssClass="nav-sketches" Title="Sketches" Info="Sketches related to parcel" />
            <cc:ScreenMenu runat="server" ID="smPhotos" Action="showPhotos()" CssClass="nav-photos" Title="Photos" Info="View Photos" />
            <cc:ScreenMenu runat="server" ID="smDirections" Action="showGoogleDirections()" CssClass="nav-google-map" Title="Google Map Directions" Info="View directions from current location" />
            <cc:ScreenMenu runat="server" ID="smNavigon" Action="showNavigon();" CssClass="nav-navigon" Title="Navigon Directions" Info="Start Navigon" />
            <cc:ScreenMenu runat="server" ID="smMap" Action="showGISInteractive();" CssClass="nav-gis-map" Title="GIS Interactive Map" Info="View Parcel on Map" />
            <cc:ScreenMenu runat="server" ID="smCopyImprovements" Action="showCopyImprv(4)" CssClass="nav-copy-imprv" Title="Copy Improvements" Info="Copy Improvements from one parcel to another." />
        </menu>
    </div>
</section>
