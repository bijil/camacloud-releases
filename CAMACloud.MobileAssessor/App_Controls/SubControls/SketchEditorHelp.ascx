﻿<%@ Control Language="VB" %>
<p>Use three-finger swipe gestures to scroll the sketch in the editor.</p>
<p>Select any polygon using the labels from the dropdown menu. You can move the polygon by touching the arrow buttons any number of times required. Each move will correspond to one feet on ground.</p>
<p>To move a node in a polygon, select the polygon first, then the required node using single finger, and drag to required position.</p>
