﻿//    DATA CLOUD SOLUTIONS, LLC ("COMPANY") CONFIDENTIAL 
//    Unpublished Copyright (c) 2010-2013 
//    DATA CLOUD SOLUTIONS, an Ohio Limited Liability Company, All Rights Reserved.

//    NOTICE: All information contained herein is, and remains the property of COMPANY.
//    The intellectual and technical concepts contained herein are proprietary to COMPANY
//    and may be covered by U.S. and Foreign Patents, patents in process, and are protected
//    by trade secret or copyright law. Dissemination of this information or reproduction
//    of this material is strictly forbidden unless prior written permission is obtained
//    from COMPANY. Access to the source code contained herein is hereby forbidden to
//    anyone except current COMPANY employees, managers or contractors who have executed
//    Confidentiality and Non-disclosure agreements explicitly covering such access.</p>

//    The copyright notice above does not evidence any actual or intended publication
//    or disclosure of this source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
//    DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
//    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
//    IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
//    OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
//    TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
//    ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.


//App.js
var debug_mode = false;
var enableFocus = false;
var appOptions = {
    skipDownload: quickDebug,
    syncAllData: true
}
var refreshing;
var fylEnabled = false;
var showFutureData = false;
var hasParcels = false;
var parcelCount = 0;
var listMode = 1;
var listType = 2;  //0 -- Parent Parcel  1// Child parcels 2// Bpp Only
var isBPPUser = false;
var isRPUser = false;
var priMapMode = 1//0;
var syncNbhd = [];
var tableKeys = [];
var webSocket, debug_username, socket_reciever, sketchImages, sketchMarkup;
var appState = {
    parcelQuery: '',
    searchTitle: '',
    parcelId: -1,
    screenId: '',
    prevScreenId: '',
    homeParcelSelectedIndex: 0,
    homeParentParcelSelectedIndex: 0,
    vuid: '',
    catId: 0,
    openfromMap: '',
    ignoreCondition: '',
    rowuid: ''
}
var logWorker, maLogoTapped = 0;
var tableData = [];
var ParcelsNotInGroup = [];

function clearAppState() {
    appState = {
        parcelQuery: '',
        searchTitle: '',
        parcelId: -1,
        screenId: '',
        prevScreenId: '',
        homeParcelSelectedIndex: 0,
        homeParentParcelSelectedIndex: 0,
        vuid: '',
        catId: 0,
        openfromMap: '',
        ignoreCondition: '',
        rowuid: '',
        parentFetchSql: ''
    }

    for (x in appState) {
        localStorage.removeItem('appState-' + x);
    }
}

function restoreAppState() {
    if (!wasSavedShortly()) {
        clearAppState();
        return false;
    }
    for (x in appState) {
        appState[x] = localStorage.getItem('appState-' + x);
    }
    if ((appState.parcelQuery == null) || (appState.parcelQuery == '')) {
        clearAppState();
        return false;
    }
    if (appState.screenId == 'sort-screen' || appState.screenId == 'progress')
        return false;

    return true;
}

function saveAppState() {
    updateLastActivityTime();
    updateLastSaveTime();
    for (x in appState) {
        localStorage.setItem('appState-' + x, appState[x]);
    }
}

if (localStorage.getItem("pcount") != null) {
    parcelCount = parseInt(localStorage.getItem("pcount"));
}

function wasLoggedInLast() {
    var now = (new Date()).tickSeconds();
    var last = getLastActivityTime();
    if (isNaN(last)) { loggedIn = false; return false; }
    var hours = parseInt((now - last) / (60 * 60));
    var sessiontimeoutInHours = clientSettings["SessionTimeoutInHours"] || 8;
    if (hours < sessiontimeoutInHours)
        return true;
    else {
        if (!ccma.Environment.iPad) {
            LogoutErrorInsert("page: wasLoggedInLast");
            Logout();
        }
        loggedIn = false;
        return false;
    }
}

function wasActiveShortly() {
    var now = (new Date()).tickSeconds();
    var last = getLastActivityTime();
    if (isNaN(last)) { return false; }
    var hours = parseInt((now - last) / (60 * 60));
    if (hours < 1) {
        return true;
    } else {
        return false;
    }
}

function wasSavedShortly() {
    var now = (new Date()).tickSeconds();
    var last = getLastSaveTime();
    if (isNaN(last)) { return false; }
    var hours = parseInt((now - last) / (60 * 60));
    if (hours < 1) {
        return true;
    } else {
        return false;
    }
}

function blockInputs() { $('.input-blocker').show(); }
function allowInputs() { $('.input-blocker').hide(); $('.mask').hide(); }

function passthrough(callback, options) {
    hasParcels = true;
    if (callback) callback();
}

var testMethod = null;

function startApplication(initiator, options) {
    ccma.UI.RelatedFields = {};
    ccma.UI.FieldProps = {};
    ccma.UI.RelatedFields = {};
    ccma.UI.SortScreenFields = [];
    ccma.UI.EstimateChartFields = [];

    if (appUpdating) return false;

    if (initiator == null)
        initiator = passthrough;
    else
        hideSplash();


    
    //    showDashboard();
    //    return;
    //setMapIcons();
    setCookie('PrevURL', window.location.href, 12);
    initiator(function () {
        showSplash();
        $('.login-buttons button').removeAttr('disabled');
        syncInProgress = false;
        syncNbhd = [];
        if (appUpdating) return false;
        $('menu').html('');
        setSplashProgress('Creating local cache ...', 25);
        loadNeighborhoods();
        if (appUpdating) return false;
        setSplashProgress('Creating local cache ...', 30);
        loadFieldsettings(function () {
            loadCategorySettings(function () {
                loadFields(function () {
                    setSplashProgress('Creating local cache ...', 35);
                    loadLookup(function () {
                        loadStreetPoints(function () {
                            loadHeatmapfields(function () {
                                loadHeatmaplookup(function () {
                                    loadAggregateFields(function () {
                                        loadValidations(function () {
                                            loadTableKeys(function () {
                                                loadImportSettings(function () {
                                                    loadSketchSettings(function () {
                                                        EnableEagleViewInMA = clientSettings.EnableEagleViewInMA == '1' ? true : false
                                                        ShowKeyValue1 = clientSettings.ShowKeyValue1 == '1' ? "True" : "False"
                                                        AlternateKey = clientSettings.AlternateKey || "";
                                                        ShowAlternateField = clientSettings.ShowAlternateField == '1' ? "True" : "False"
                                                        UsingOSM = clientSettings.UseOSM == '1' ? true : false;
                                                        if (isBPPUser && clientSettings['PersonalPropertyDownloadType'] == 'Both' && clientSettings['PersonalPropertyLinkedRPDataReadOnly'] == '1') {
                                                            ccma.Session.RealDataReadOnly = true;
                                                            localStorage.setItem('RealDataReadOnly', true);
                                                        }
                                                        setMapIcons(); //position changed for assigning map icons after UsingOsm property checked. If new settings change occured that need to be affected.
                                                        loadPictometryIPA();
                                                        loadCustomFunctionsOnAppLoad(function () {
                                                            fylEnabled = (clientSettings.FutureYearEnabled == '1' || clientSettings.FutureYearEnabled == 'true');
                                                            osmLoad(function () {
                                                                setSplashProgress('Generating offline map', 40)
                                                                loadFieldAlertTypes(function () {
                                                                    loadAuxTableNames(function () {
                                                                        loadParentChild(function () {
                                                                            setSplashProgress('Loading data collection tabs ...', 45);
                                                                            loadDataCollectionCard(function () {
                                                                                setSplashProgress('Loading photo properties...', 70);
                                                                                loadPhotoPropertiesUI(function () {
                                                                                    setSplashProgress('Loading template data...', 72);
                                                                                    var categoryTemplateInsert = fieldCategories.filter(function (d) { return d.EnableTemplateInsert == 'true' });
                                                                                    loadtemplateInsertData(categoryTemplateInsert, function () {
                                                                                        setSplashProgress('Checking system integrity...', 75);
                                                                                        checkIfComparablesTableExists(function () {
                                                                                            checkIfImageTableExists(function () {
                                                                                                setSplashProgress('Loading EagleView Shapes...', 80);
                                                                                                loadPictometryInMap(function () {
                                                                                                    setMapItems();
                                                                                                    setSplashProgress('Creating views ...', 90);
                                                                                                    createParcelView(function () {
                                                                                                        CheckParcelsInGroup(function () {
                                                                                                            postAppLoadingJobs(function () {
                                                                                                                insert_unsyncedImages(function () {
                                                                                                                    updateSqlLiteDataInIndexedDB(function () {
                                                                                                                        if (appUpdating) return false;
                                                                                                                        setSplashProgress('Ready', 95);
                                                                                                                        if (localStorage.getItem('UpdateScreenMenu') == null)
                                                                                                                            menuRename();
                                                                                                                        setHeatmapFields();
                                                                                                                        hideSplash();
                                                                                                                        var restoreLastSession = restoreAppState() && !isBPPUser;
                                                                                                                        restoreLastSession ? log('Beginning restore') : log('No saved state');
                                                                                                                        var screenId = appState.screenId;
                                                                                                                        refreshNbhdProfile = true;
                                                                                                                        currentNbhd = null;
                                                                                                                        if (isBPPUser && ccma.Session.BPPDownloadType == '1') {
                                                                                                                            listType = 0;
                                                                                                                            isFirstTime = true;
                                                                                                                        }
                                                                                                                        else if (isBPPUser && ccma.Session.BPPDownloadType == '0')
                                                                                                                            listType = 2;
                                                                                                                        combinedView = false;
                                                                                                                        setSortButton();
                                                                                                                        showSortScreen(function () {
                                                                                                                            bindFocusForInputs();
                                                                                                                            if (restoreLastSession) {
                                                                                                                                setSplashProgress('Restoring previous session ...');
                                                                                                                                if (appState.parcelQuery != '') {
                                                                                                                                    loadParcelsFromQuery(appState.parcelQuery, 'my-parcels', function () {
                                                                                                                                        setSearchResultsTitleText(appState.searchTitle);
                                                                                                                                        restoreScreen(screenId);
                                                                                                                                        log('CLEARED APP STATE');

                                                                                                                                    }, null, null, appState.parentFetchSql, true);
                                                                                                                                }
                                                                                                                            }
                                                                                                                            else {
                                                                                                                                clearAppState();
                                                                                                                            }
                                                                                                                            loadExtraLookups();
                                                                                                                        }, restoreLastSession);
                                                                                                                        UploadData();
                                                                                                                        setMapIcons();
                                                                                                                    });
                                                                                                                });
                                                                                                            });
                                                                                                        });
                                                                                                    });
                                                                                                });
                                                                                            });
                                                                                        });
                                                                                    });
                                                                                });
                                                                            });
                                                                        });
                                                                    });
                                                                });
                                                            });
                                                        });
                                                    });
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    }, options);

}

function checkForLoginStatus() {
    if (appUpdating) return false;

    if (wasLoggedInLast()) {
        ccma.Session.IsLoggedIn = true;
        ccma.Session.User = localStorage.getItem('username');
        isBPPUser = localStorage.getItem('usertype') == 'BPP' ? true : false;
        isRPUser = localStorage.getItem('usertype') == 'RP' ? true : false;
        ccma.Session.RealDataReadOnly = (localStorage.getItem('RealDataReadOnly') == "false" || localStorage.getItem('RealDataReadOnly') == null) ? false : true;
        ccma.Session.BPPDownloadType = localStorage.getItem('BPPDownloadType');
        ccma.Session.bppRealDataEditable = localStorage.getItem('bppRealDataEditable');
        ccma.Session.bppRealDataReadOnly = localStorage.getItem('bppRealDataReadOnly') == 'true' ? true : false;
        ccma.Session.IsPPOnly = localStorage.getItem('IsPPOnly');
        PostLoginStartup();
        return true;
    }
    if (loggedIn == false) {
        hideSplash();
        LogoutErrorInsert('sl: checkforloginstatus');
        showLogin();
        return false;
    }
    setSplashProgress('Connecting to CAMA Cloud ...', 20);
    ajaxRequest({
        url: baseUrl + 'mobileassessor/check.jrq?zt=' + ticks(),
        type: 'POST',
        dataType: 'json',
        data: {
            macid: localStorage.getItem("macid")
        },
        success: function (res, stat) {
            if (res.status)
                if (res.status == "OK") {
                    log('Login Succeeded.');
                    ccma.Session.IsLoggedIn = true;
                    ccma.Session.User = localStorage.getItem('username');
                    ccma.Session.RealDataReadOnly = localStorage.getItem('RealDataReadOnly');
                    ccma.Session.BPPDownloadType = localStorage.getItem('BPPDownloadType');
                    ccma.Session.bppRealDataEditable = localStorage.getItem('bppRealDataEditable');
                    ccma.Session.bppRealDataReadOnly = localStorage.getItem('bppRealDataReadOnly') == 'true' ? true : false;
                    ccma.Session.IsPPOnly = localStorage.getItem('IsPPOnly');
                    isBPPUser = localStorage.getItem('usertype') == 'BPP' ? true : false;
                    isRPUser = localStorage.getItem('usertype') == 'RP' ? true : false;
                    PostLoginStartup();
                }
            if (res.LoginStatus) {
                if (res.LoginStatus == "403" || res.LoginStatus == "401") {
                    setSplashProgress('Authenticating ...', 100);
                    ccma.Session.IsLoggedIn = false;
                    hideSplash();
                    showLogin();
                    LogoutErrorInsert('sl: checkforloginstatus364');
                }
                if (res.LoginStatus == "423") {
                    window.location.href = res.RedirectURL;
                    return false;
                }
            }
        },
        error: function (req, e, s) {
            console.log(req, e, s);
            ccma.Session.IsLoggedIn = false;
            log(s, true);
            //alert('Login check failed due to network connectivity issues. Cannot start application.');
            hideSplash();
            if (e == "parsererror") {
                window.location.href = CALAuthority + "cal.aspx?return=" & window.location.host & "&s=" & (window.location.protocol == "http:" ? "0" : "1");
            } else {
                showError('no-network-connectivity');
            }

        }
    });
}

function Logout(isSessionOut) {
    clearAppState();
    clearMapStore();
    //hideDistoClose();
    ajaxRequest({
        url: baseUrl + 'maauth/logout.jrq?zt=' + ticks(),
        type: "POST",
        dataType: 'json',
        data: null,
        success: function (data, status) {
            if (data.LoginStatus) {
                if (data.LoginStatus == "403" || data.LoginStatus == "401") {
                    doLogoutJobs(function () {
                        if (isSessionOut)
                            messageBox("your session has timed out please login in again to continue")
                        log('Logged out.');
                    });
                }
            }
            ccma.Session.IsLoggedIn = false;
            LogoutErrorInsert('sl: logout408');
            showLogin();
        },
        error: function (req, e, s) {
            doLogoutJobs();
            LogoutErrorInsert('sl: logout413');
            showLogin();
            //  var recentPass= localStorage.getItem( 'loginpassword');
            //  showError('no-network-connectivity');
        }
    });
}

function doLogoutJobs(callback) {
    ccma.Session.IsLoggedIn = false;
    var now = new Date(new Date().setDate(new Date().getDate() - 1)).tickSeconds()
    localStorage.setItem("last-activity-time", now);
    if (callback) callback();
}

function postAppLoadingJobs(callback) {
    ccma.Sketching.Config = CAMACloud.Sketching.Configs.GetConfigFromSettings();
    ccma.Sketching.SketchFormatter = null;

    if (ccma.Sketching.Config.formatter && ccma.Sketching.Config.formatter != '') {
        ccma.Sketching.SketchFormatter = eval('CAMACloud.Sketching.Formatters.' + ccma.Sketching.Config.formatter);
    }

    if (!ccma.Sketching.SketchFormatter) {
        if (clientSettings["SketchFormat"] != null || sketchSettings["SketchFormat"])
            ccma.Sketching.SketchFormatter = eval('CAMACloud.Sketching.Formatters.' + (clientSettings["SketchFormat"] || sketchSettings["SketchFormat"]));
        if (!ccma.Sketching.SketchFormatter) {
            ccma.Sketching.SketchFormatter = CAMACloud.Sketching.Formatters.TASketch;
        }
    }
    if (clientSettings["sketchOriginPosition"] || sketchSettings["sketchOriginPosition"])
        ccma.Sketching.SketchFormatter.originPosition = clientSettings["sketchOriginPosition"] || sketchSettings["sketchOriginPosition"];
    if (sketchSettings["RoundToDecimalPlace"] == 'Half Foot' || sketchSettings["RoundToDecimalPlace"] == 'Quarter Foot' || sketchSettings["RoundToDecimalPlace"] == 'Rounded') {
        sketchSettings.SegmentRoundingFactor = sketchSettings["RoundToDecimalPlace"] == 'Half Foot' ? 2 : (sketchSettings["RoundToDecimalPlace"] == 'Quarter Foot' ? 4 : 1);
    }
    if (ccma.Sketching.Config.encoder) {
        var sketchEncoder = CAMACloud.Sketching.Encoders[ccma.Sketching.Config.encoder];
        if (sketchEncoder) {
            sketchApp.encoder = sketchEncoder;
            sketchRenderer.encoder = sketchEncoder;
        } else {
            console.error('Invalid sketch encoder: ' + ccma.Sketching.Config.encoder)
        }
    }

    CAMACloud.Sketching.roundFactor = null;  //when we update clientsettings we need to reload chnage value to null if global declaration.Ma update application need to reset this.
    sketchApp.config = ccma.Sketching.Config;
    sketchApp.formatter = ccma.Sketching.SketchFormatter;
    sketchRenderer.formatter = ccma.Sketching.SketchFormatter;

    sketchApp.external.datafields = datafields;
    sketchApp.external.lookup = lookup;
    sketchApp.external.fieldCategories = fieldCategories;
    sketchApp.external.lookupMap = ccma.Data.LookupMap;

    if (ccma.Sketching.SketchFormatter.arcMode && ccma.Sketching.SketchFormatter.arcMode != 'disable') {
        $('.sp-circle', sketchPad.selector).html(ccma.Sketching.SketchFormatter.arcMode);
        $('.sp-circle', sketchPad.selector).attr('cmd', ccma.Sketching.SketchFormatter.arcMode);
    }

    if (ccma.Sketching.SketchFormatter.arcMode == 'ARC') {
        $('.sp-circle', sketchPad.selector).html('ARC');
        $('.sp-circle', sketchPad.selector).attr('cmd', 'ARC');

        $('.sp-rect', sketchPad.selector).html('-ARC');
        $('.sp-rect', sketchPad.selector).attr('cmd', 'ARX');
        $('.sp-rectx', sketchPad.selector).show();
    }
    else if (ccma.Sketching.SketchFormatter.arcMode == 'disable') {
        $('.sp-circle', sketchPad.selector).removeAttr("cmd")
        $('.sp-circle', sketchPad.selector).css("background", "silver")
        $('.sp-rectx', sketchPad.selector).hide();
    }
    else {
        $('.sp-rectx', sketchPad.selector).hide();
    }
    /*if (ccma.Sketching.Config.arcMode && ccma.Sketching.Config.arcMode == 'ELL'){
    	$('.sp-circle1', sketchPad.selector).html(ccma.Sketching.Config.arcMode);
        $('.sp-circle1', sketchPad.selector).attr('cmd', ccma.Sketching.Config.arcMode);
        $('.sp-rectx', sketchPad.selector).hide();
        $('.sp-ok', sketchPad.selector).hide();
        $('.sp-rectp', sketchPad.selector).show();
        $('.sp-okx', sketchPad.selector).show();
        $('.sp-rectp', sketchPad.selector).css({'width':'28px', 'padding-left':'7px'});
        $('.sp-circle1', sketchPad.selector).css({'width':'37px', 'padding-left':'5px'});
        $('.sp-okx', sketchPad.selector).css('width', '33px');
    }
    else{    	
    	$('.sp-circle1', sketchPad.selector).hide();
    	$('.sp-rectp', sketchPad.selector).hide();
        $('.sp-okx', sketchPad.selector).hide();
        $('.sp-ok', sketchPad.selector).show();      	
    }*/

    $('.ccse-disto').unbind(touchClickEvent);
    $('.ccse-disto').bind(touchClickEvent, function () {
        sketchApp.openDisto(function () {
            CCWarehouse.SketchOpen();
        });
    })
    if (fylEnabled) {
        if (showFutureData) {
            $('#myonoffswitch44').attr('checked', '');
            $('#myonoffswitchPRC').attr('checked', '')
        }
        else {
            $('#myonoffswitch44').removeAttr('checked');
            $('#myonoffswitchPRC').removeAttr('checked');
        }
    }
    else
        $('.fy_selector').hide()

    CCWarehouse.onSketchVectorsReceiveClose = function (sketch) {
        this.logListen("Rendering sketch");
        sketchApp.addVectorFromString(sketch, null, function () { CCWarehouse.SketchClear() });
        this.logListen("Closing sketch");
        //  CCWarehouse.SketchClose();

    }

    CCWarehouse.onSketchMoveNode = function (sketch) {
        sketchApp.processCmdString(sketch, true);
    }

    CCWarehouse.onSketchVectorsReceiveOpen = function (sketch) {
        this.logListen("Rendering sketch");
        sketchApp.addVectorFromString(sketch, true, function () { CCWarehouse.SketchClear() });
        //  CCWarehouse.SketchClear()
    }
    mapDisplayFields($('#estimate-chart-template').html(), ccma.UI.EstimateChartFields);
    var parcelTemplate = document.createElement('div');
    parcelTemplate.innerHTML = document.getElementById('my-parcels-template').innerHTML;
    $('[template="estimate-chart"]', parcelTemplate).html($('#estimate-chart-template').html());
    mapDisplayFields(parcelTemplate.innerHTML, ccma.UI.SortScreenFields);

    if (clientSettings["MaxPhotosPerParcel"] != null) {
        if (!isNaN(clientSettings["MaxPhotosPerParcel"])) {
            var mppp = parseInt(clientSettings["MaxPhotosPerParcel"]);
            if (mppp > 0 && mppp <= 25) {
                ccma.UI.Camera.MaxPhotosPerParcel = mppp;
            }
        }
    }


    if (clientSettings["PhotoCaptureMaximumWidth"] != null) {
        if (!isNaN(clientSettings["PhotoCaptureMaximumWidth"])) {
            var sv = parseInt(clientSettings["PhotoCaptureMaximumWidth"]);
            if (sv >= 600 && sv < 5000) {
                ccma.UI.Camera.MaxCaptureWidth = sv;
            }
        }
    }

    if (clientSettings["PhotoCaptureMaximumHeight"] != null) {
        if (!isNaN(clientSettings["PhotoCaptureMaximumHeight"])) {
            var sv = parseInt(clientSettings["PhotoCaptureMaximumHeight"]);
            if (sv >= 400 && sv < 4000) {
                ccma.UI.Camera.MaxCaptureHeight = sv;
            }
        }
    }

    if (ccma.UI.Camera.MaxCaptureWidth < ccma.UI.Camera.MaxCaptureHeight)
        [ccma.UI.Camera.MaxCaptureHeight, ccma.UI.Camera.MaxCaptureWidth] = [ccma.UI.Camera.MaxCaptureWidth, ccma.UI.Camera.MaxCaptureHeight];

    ccma.UI.Camera.MaxStorageWidth = Math.round(ccma.UI.Camera.MaxCaptureWidth * (5 / 12));
    ccma.UI.Camera.MaxStorageHeight = Math.round(ccma.UI.Camera.MaxCaptureHeight * (5 / 12));

    if (clientSettings["PhotoCaptureDPI"] != null) {
        if (!isNaN(clientSettings["PhotoCaptureDPI"])) {
            var sv = parseInt(clientSettings["PhotoCaptureDPI"]);
            switch (sv) {
                case 72:
                case 96:
                case 150:
                case 300:
                    ccma.UI.Camera.DPI = sv;
                    break;
                default:
                    console.warn('Attempt to set with a non-standard DPI. Default value of 72dpi will be used.');
                    ccma.UI.Camera.DPI = 72;
                    break;
            }
        }
    }

    loadStreetNamesForSearch();

    if (callback) callback();
}

function checkForUpdatedVersion() {
    if (new_worker) {
        if (new_worker.state == 'installed' && navigator.serviceWorker.controller) {
            messageBox('An updated version of CAMA Cloud is now available. Update now?', ["OK", "Not Now"], function () {
                new_worker.postMessage({
                    action: 'skipWaiting'
                });
                showSplash();
                setSplashProgress('Updating application..', 80);
                setScreenDimensions();
            }, function () {
                appUpdating = false;
                hideSplash();
            });
        }
    }
}
$(window).ready(function () {
    initButtons();
    $('.btn-repair-app').hide();
    setSplashProgress('Initializing application ...', 0);
    $('.cache-update-bar').hide();
    $('.ma-version').show();
    initApplicationCache(() => {
        //checkForUpdatedVersion();
        initWorkerService();
        if (!navigator.platform.contains("iPad") && !navigator.platform.contains("iPhone")) {
            camCapture = new cameraCapture("_divWinCameraMain");

            camCapture.onClose = function () {
                document.getElementById("_winCameraDiv").style.display = "none";
            }

            camCapture.onOpen = function () {
                document.getElementById("_winCameraDiv").style.display = "block";

                console.log(camCapture.data);
            }

            camCapture.onAccept = function (data) {
                console.log("accept");
                console.log(document.getElementById("camera"));
                console.log(pstream);
                var d = new pstream.PhotoUpload(camCapture.data.parcelId, camCapture.data.keyvalue, camCapture.data.camera, camCapture.data.onready, data);
            }

            window.onresize = function (event) {
                camCapture.checkOrientation();
            };
        }
        pstream = new PhotoStream();
        photos = new PhotoManager('.overlay-photo-viewer', '.overlay-photo-box');
        sketchImage = new sketchImageManager('.overlay-sketchimage-viewer', '.overlay-sketchimage-box');
        var sketchImageOptions = {
            width: "640",
            height: "400",
            color: "red",
            type: "rectangle",
            images: null,
            linewidth: 2,
            fontsize: "20px",
            bootstrap: true,
            position: "top",
            idAttribute: "id",
            selectEvent: "change",
            unselectTool: false
        }
        sketchMarkup = $(".markup").annotate(sketchImageOptions);

        sketchApp = new CCSketchEditor('.ccse-canvas', CAMACloud.Sketching.Formatters.TASketch);
        sketchRenderer = new CCSketchEditor('.ccse-img', CAMACloud.Sketching.Formatters.TASketch);
        sketchPad = new CCSketchPad('.sketch-pad', sketchApp);

        window.addEventListener('offline', function () { // added to check network availabel or not.If offline then clrear sync status.
            syncInProgress = false;
            uploadInProgress = false;
            if (pstream)
                pstream.syncInProgress = false;
            return false;
        });

        window.addEventListener('online', function () {
            UploadData && UploadData();
            return false;
        });

        ccma.Data.Controller = new CAMACloud.DataController();
        ccma.Data.Controller.startProcess = function (label) {
            showSplash();
            setSplashProgress(label, 0);
        }
        ccma.Data.Controller.updateProgress = function (label, progress) {
            showSplash();
            setSplashProgress(label, progress);
        }
        ccma.Data.Controller.endProcess = function () {
            setSplashProgress('', 0);
            hideSplash();
        }

        cpan = new PanControl('.pan-control', sketchApp);
        czoom = new ZoomControl('.zoom-control');
        ctools = new SketchEditorToolbar('.ccse-sketchtoolbar', sketchApp);
        sketchApp.attachPanControl(cpan);
        sketchApp.attachZoomControl(czoom);
        sketchApp.onClose = function () {
            copyVector = null;
            $('.overlay-sketch-editor').hide();
        }
        setScreenDimensions();   //While Loading

        //if (navigator.platform == 'iPad' || navigator.platform == 'iPhone')
        //    if (!navigator.standalone) {
        //        $('.splash-progress-window').hide();
        //        $('.installation-instructions').show();
        //        return false;
        //    }

        if (!appUpdating) setSplashProgress('Initializing application ...', 5);
        //    initSketchEditor();
        //    initSketchEditorControls();
        //    if (!appUpdating) setSplashProgress('Initializing components ...', 10);

        initScreens();
        SetSearchType();
        if (!appUpdating) setSplashProgress('Initializing components ...', 15);
        //showCacheProgress(0.5);
        //initLocalDatabase(function () {
        if (testMethod == null) {
            if (!appUpdating) setSplashProgress('Waiting for GPS ...', 18);
            geo.startGeo();
            window.setTimeout('checkLocationAndProceed();', 1000);
        }
        else {
            testMethod();
        }
    });
});

window.onunload = function () {
    if (typeof(updateSqlLiteDataInIndexedDB) == "function") {
        updateSqlLiteDataInIndexedDB(function () {
            console.log("cached");
        });
    }
}


function checkLocationAndProceed() {
    if (!iPad || location.hostname === "localhost" || location.hostname.contains("192.168") || location.hostname === "") {
        checkForLoginStatus();
        return;
    }
    if (locationDenied) {
        hideSplash();
        showError("gps-access-denied");
        return;
    }
    if (!located) {
        log('Checking location again ..');
        window.setTimeout('checkLocationAndProceed();', 2000);
        return;
    }
    window.setTimeout('checkForLoginStatus();', 500);
}

function toggleScreenLock(that) {
    if ($('.ccse-screen-lock').attr('islocked') == 'true') {
        $('.ccse-screen-lock').attr('islocked', 'false')
        if ($('.fvSwitch')[0])
            $('.fvSwitch').css('margin-right', '14px')
        $('.mask').hide();
        sketchApp.isScreenLocked = false;
        $('.ccse-screen-lock').show();
        $('.mask .ccse-screen-lock').hide();
    }
    else {
        $('.ccse-screen-lock').attr('islocked', 'true');
        $('.mask').show();
        $('.mask .ccse-screen-lock').remove()
        $(that).clone().appendTo('.mask');
        $(that).hide();
        sketchApp.isScreenLocked = true;
        if ($('.fvSwitch')[0])
            $('.fvSwitch').css('margin-right', '50px')
        $('.mask .ccse-screen-lock').css('background-position-x', '24px');
        $('.mask .ccse-screen-lock').css('margin-right', '60px');
        $('.mask .ccse-screen-lock').unbind(touchClickEvent)
        $('.mask .ccse-screen-lock').bind(touchClickEvent, function () {
            toggleScreenLock(this);
            return;
        });
    }
}

window.__defineGetter__("update", function () { updateApplication(); });
window.__defineGetter__("reset", function () {
    window.location.reload();
});

function debug_mode_log(msg) {
    if (debug_mode) console.log(msg);
}

function bindFocusForInputs(noOnScrollBinding) {
    $('input,textarea').unbind('focus');
    $('input,textarea').bind('focus', function () {
        focussedInputElement = this;
        var position = $(focussedInputElement).position();
        var element = $(focussedInputElement).parents('.scrollable')[0];
        enableFocus = true;
        //       element ? element.scrollTop = element.scrollTop + 100:'';

        var offset = position.top - 240;
        if (Math.abs(window.orientation) == 0 || Math.abs(window.orientation) == 180)
            offset = position.top - 475;



        if (offset > 0 && !($(this).hasClass('cc-drop') || $(this).hasClass('cc-drop-search'))) {
            enableFocus = true;
            element ? element.scrollTop = element.scrollTop + offset : '';
        } 
        
        if ($(this).hasClass('cc-drop-search') || $(this).hasClass('cc-drop-customtext')) {
            let el = $('.cc-drop.cc-drop-focus'),
                parent = $(el).parents('.scrollable')[0],
                position = $(el).position();
            
            if(el && parent) {

                let top = el.offset().top - parent.offsetTop - $('.page-header').height() -20;
                if (Math.abs(window.orientation) == 0 || Math.abs(window.orientation) == 180) top -= parent.offsetTop;
                
                el.attr('mscroll', '0');
                if ($(this).hasClass('cc-drop-customtext')) $(this).attr('mscroll', '0');
                parent ? parent.scrollTo(parent.scrollLeft,  parent.scrollTop + top) : '';

                enableFocus = true;
            }
        }

        //window.setTimeout(function(){ enableFocus = false; }, 100);
        var tempHeight = $(focussedInputElement).height();
        $('textarea').keypress(function () {    
            focussedInputElement = this;
            var temp = 0;
            if (tempHeight < $(focussedInputElement).height()) {
                temp = $(focussedInputElement).height() - tempHeight;

            }
            if (temp > 0) {
                enableFocus = true;
                element ? element.scrollTop = element.scrollTop + temp : '';
                tempHeight = $(focussedInputElement).height();
            }

        });

    });

    if (!noOnScrollBinding)
        $('.scrollable').on('scroll', function () {
            if (focussedInputElement && !enableFocus) $(focussedInputElement).blur();

        });
}

function loadtemplateInsertData(tempInsertCat, callback) {
    if (tempInsertCat.length == 0) { if (callback) callback(); return; }
    var eachInsertCat = tempInsertCat.pop();
    var sql = "SELECT * FROM " + eachInsertCat.SourceTable + " WHERE CC_ParcelId = -99";
    getData(sql, [], function (data) {
        tableData[eachInsertCat.SourceTable] = data;
        loadtemplateInsertData(tempInsertCat, callback);
    });
}

/*
function InitWebSocket() {
	if ("WebSocket" in window) {
		var url = location.href.split('//')[1].split('/')[0];
		if (location.href.search(/localhost/i) > -1)
	    	webSocket = new WebSocket("ws://localhost:300/default.socket");
    	else webSocket = new WebSocket("wss://" + url + "/default.socket");
	    webSocket.onopen = function() {
	        console.log('Connection Opened, if you want to do something while opening connection do it here');
	        debug_username = location.href.split('//')[1].split('.camacloud')[0] + '-' + localStorage.username;
	        if (location.href.search(/localhost/i) > -1) debug_username = 'localhost-' + localStorage.username;
	        sendMessage(debug_username + " opened debug mode", '5');
	    };
	    webSocket.onmessage = function (evt) {
	    	var msg = evt.data.split('||');
	    	if (msg.indexOf('conn_req') > -1) 
	        console.log(evt.data)
	    };
	 
	    webSocket.onclose = function() {
	        console.warn("Connection is closed");
	    };
	 
	    webSocket.onerror = function (error) {
	        console.error(error.data);
	    };
	}
	else console.error("WebSocket NOT supported by your Browser!");
}

function sendMessage(msg, type) {
	if (webSocket.readyState != 1) return;
	if (typeof msg == "object") msg = "JSON-" + JSON.stringify(msg);
	else if (typeof msg == "number") msg = msg.toString();
	if (!type) type = 0;
	try {
    	webSocket.send(debug_username + '||MA_Debugger||' + msg + '||' + type);
    } catch(ex) { console.log('error in socket send') }
} */

function setCookie(cName, cValue, expMnth) {
    var dte = new Date();
    dte.setMonth(dte.getMonth() + expMnth);
    var expires = "expires=" + dte.toUTCString();
    document.cookie = cName + "=" + cValue + ";" + expires + ";domain=.camacloud.com;path=/";
}

function getCookie(cName) {
    var name = cName + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function CheckParcelsInGroup(callback) {
    ParcelsNotInGroup = [];
    if (isBPPUser) {
        let sql = 'select Id from PendingParcels where ParcelInGroup = "false"'
        if(ccma.Session.IsPPOnly == 'true')
            sql = 'select Id from PendingParcels where IsRPProperty = "true"'
        getData(sql, [], (res) => {
            res.forEach((item) => {
                ParcelsNotInGroup.push(parseInt(item.Id));
            });
            if (callback) callback();
        }, null, true, () => {
            if (callback) callback();
        });
    }
    else {
        if (callback) callback();
    }
}