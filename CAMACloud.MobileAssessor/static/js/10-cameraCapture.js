var cameraLocalStream = null;
var cameraCapture = function (selector) {

    this.divMain = document.getElementById(selector);
    this.divMain.style.overflow = "auto";

    //create elements

    //modal
    this.divModal = document.createElement("div");
    this.divModal.setAttribute("style", "border-radius: 10px; width: 90%; margin: 5% auto; background: #EDEDED; overflow: auto; padding: 5px 20px 13px 20px; box-shadow: 1px 1px 3px #000;");
    this.divMain.appendChild(this.divModal);

    //x button
    this.button = document.createElement("a");
    this.button.textContent = "X";
    this.button.parent = this;
    this.divModal.appendChild(this.button);
    this.button.setAttribute("style", "background: #606061; color: #FFFFFF; line-height: 25px; position: absolute; right: 5%; text-align: center; width: 24px; text-decoration: none; font-weight: bold; border-radius: 12px; box-shadow: 1px 1px 3px #000;");
    this.button.addEventListener("click", function () {
        this.parent.close();
    });

    //title
    this.h2Title = document.createElement("h2");
    this.h2Title.appendChild(document.createTextNode("Take a new picture"));
    this.divModal.appendChild(this.h2Title);

    this.hr1 = document.createElement("hr");
    this.divModal.appendChild(this.hr1);

    //landscape view
    this.divLandscape = document.createElement("div");
    this.divLandscape.style.overflow = "auto";
    this.divModal.appendChild(this.divLandscape);

    //portrait view
    this.divPortrait = document.createElement("div");
    this.divPortrait.style.overflow = "auto";
    this.divModal.appendChild(this.divPortrait);

    //portrait warning
    this.h3Warning = document.createElement("h3");
    this.h3Warning.textContent = "Please rotate your device to landscape mode!";
    this.divPortrait.appendChild(this.h3Warning);

    //protrait image
    this.imgRotate = document.createElement("img");
    this.imgRotate.src = "static/images/rotate.png";
    this.divPortrait.appendChild(this.imgRotate);

    //camera view
    this.divCameraView = document.createElement("div");
    this.divCameraView.setAttribute("style", "width: 50%; float: left;");
    this.divLandscape.appendChild(this.divCameraView);

    this.video = document.createElement("video");
    this.divCameraView.appendChild(this.video);

    this.canvas = document.createElement("canvas");
    this.canvas.style.display = "none";
    this.divCameraView.appendChild(this.canvas);

    //control view
    this.divControlView = document.createElement("div");
    this.divControlView.setAttribute("style", "width: 50%; float: left;");
    this.divLandscape.appendChild(this.divControlView);

    //take controls
    this.divControlTake = document.createElement("div");
    this.divControlTake.setAttribute("style", "float: right;");
    this.divControlView.appendChild(this.divControlTake);

    //capture button
    this.imgCapture = document.createElement("img");
    this.imgCapture.parent = this;
    this.imgCapture.addEventListener("click", function () {
        this.parent.capture();
    });

    this.imgCapture.parent = this;
    this.imgCapture.addEventListener("click", function () {
        //  this.parent.capture();
    });

    this.imgCapture.src = "static/images/capture.png";
    this.imgCapture.setAttribute("style", "width:64px;height:64px;");
    this.divControlTake.appendChild(this.imgCapture);
    this.imgUpload = document.createElement("img");
    this.imgUpload.parent = this;
    this.imgUpload.addEventListener("click", function () {
        $('#' + camCapture.data.camera).click();
    });
    this.imgUpload.src = "static/images/folder.png";
    this.imgUpload.setAttribute("style", "width:60px;height:62px;margin-left:10px;");
    this.divControlTake.appendChild(this.imgUpload);
    //edit controls
    this.divControlEdit = document.createElement("div");
    this.divControlEdit.setAttribute("style", "float: right; display: none;");
    this.divControlView.appendChild(this.divControlEdit);

    //exit preview button
    this.imgExitPreview = document.createElement("img");
    this.imgExitPreview.parent = this;
    this.imgExitPreview.addEventListener("click", function () {
        this.parent.exitPreview();
    });
    this.imgExitPreview.src = "static/images/back.png";
    this.imgExitPreview.setAttribute("style", "width:64px;height:64px;");
    this.divControlEdit.appendChild(this.imgExitPreview);

    //accept button
    this.imgAccept = document.createElement("img");
    this.imgAccept.parent = this;
    this.imgAccept.addEventListener("click", function () {
        this.parent.accept();
    });
    this.imgAccept.src = "static/images/accept.png";
    this.imgAccept.setAttribute("style", "width:64px;height:64px;");
    this.divControlEdit.appendChild(this.imgAccept);

    this.context = this.canvas.getContext("2d");
    this.preview = false;
    this.onModeChange = null;
    this.onAccept = null;
    this.onClose = null;
    this.onOpen = null;
    this.image = null;
    this.opened = false;

    this.info = null;

    this.videoObj = { "video": true }, errBack = function (error) {
        console.log("Video capture error: ", error.code);
        alert("Allow camera access to take a picture.");
    };

    this.checkOrientation();
}

cameraCapture.prototype.accept = function () {
    if (this.onAccept != null)
        this.onAccept(this.image);
    this.close();
}

cameraCapture.prototype.open = function () {
    this.opened = true;
    console.log("open camera");
    var video = this.video;

    if (this.onOpen != null)
        this.onOpen();

    this.divMain.style.display = "block";
    this.changeMode(false);
    // Put video listeners into place
    if (navigator.getUserMedia) { // Standard
        navigator.getUserMedia(this.videoObj, function (stream) {
            cameraLocalStream = stream;
            if(iPad) 
            	video.src = window.URL.createObjectURL(stream);
            else 
            	video.srcObject = stream;
            video.play();
        }, errBack);
    } else if (navigator.webkitGetUserMedia) { // WebKit-prefixed
        navigator.webkitGetUserMedia(this.videoObj, function (stream) {
            cameraLocalStream = stream;
            if(iPad) 
            	video.src = window.URL.createObjectURL(stream);
            else 
            	video.srcObject = stream;
            video.play();
        }, errBack);
    } else if (navigator.mozGetUserMedia) { // moz-prefixed
        navigator.mozGetUserMedia(this.videoObj, function (stream) {
            cameraLocalStream = stream;
            if(iPad) 
            	video.src = window.URL.createObjectURL(stream);
            else 
            	video.srcObject = stream;
            video.play();
        }, errBack);
    }
}

cameraCapture.prototype.checkOrientation = function () {

    if (window.innerHeight > window.innerWidth) {
        this.divLandscape.style.display = "none";
        this.divPortrait.style.display = "block";
        return false;
    }
    else {
        this.divLandscape.style.display = "block";
        this.divPortrait.style.display = "none";
        return true;
    }

}

cameraCapture.prototype.capture = function () {
    var videoWidth = this.video.videoWidth;
    var videoHeight = this.video.videoHeight;
    var ratio = videoWidth / videoHeight;
    var width = this.video.offsetWidth;
    var height = width / ratio;
    this.canvas.width = width;
    this.canvas.height = height;
    this.context.drawImage(this.video, 0, 0, width, height);
    var size = getScaledSize({ Width: videoWidth, Height: videoHeight }, width, height);
    this.context.font = "20px Arial";
    var text1 = (new Date()).toLocaleString();
    var text2 = camCapture.data.keyvalue
    if(camCapture.data.keyvalue < 0)
    	text2 = "";
    var t1w = this.context.measureText(text1).width;
    var t2w = this.context.measureText(text2).width;
    this.context.fillRect(0, size.Height - 28, size.Width, 28);
    this.context.fillStyle = 'white';
    this.context.fillText(text1, 10, size.Height - 4);
    this.context.fillText(text2, size.Width - t2w - 10, size.Height - 4);
    this.video.pause();
    this.changeMode(true);
    this.image = this.canvas.toDataURL("image/jpeg");
}

cameraCapture.prototype.changeMode = function (preview) {

    if (preview) {
        this.video.style.display = "none";
        this.canvas.style.display = "block";
        this.divControlEdit.style.display = "block";
        this.divControlTake.style.display = "none";
    }
    else {
        this.video.style.display = "block";
        this.canvas.style.display = "none";
        this.divControlEdit.style.display = "none";
        this.divControlTake.style.display = "block";
    }

    if (this.preview != preview) {

        this.preview = preview;
        if (this.onModeChange != null)
            this.onModeChange(this.preview);
    }
}

cameraCapture.prototype.exitPreview = function () {

    this.video.play();
    this.changeMode(false);
}

cameraCapture.prototype.close = function () {
    this.opened = false;
    if (cameraLocalStream != null) {
        // cameraLocalStream.stop();
        var track = cameraLocalStream.getTracks()[0];
        track.stop();
    }
    this.divMain.style.display = "none";

    if (this.onClose != null)
        this.onClose();
}