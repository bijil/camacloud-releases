﻿//    DATA CLOUD SOLUTIONS, LLC ("COMPANY") CONFIDENTIAL 
//    Unpublished Copyright (c) 2010-2013 
//    DATA CLOUD SOLUTIONS, an Ohio Limited Liability Company, All Rights Reserved.

//    NOTICE: All information contained herein is, and remains the property of COMPANY.
//    The intellectual and technical concepts contained herein are proprietary to COMPANY
//    and may be covered by U.S. and Foreign Patents, patents in process, and are protected
//    by trade secret or copyright law. Dissemination of this information or reproduction
//    of this material is strictly forbidden unless prior written permission is obtained
//    from COMPANY. Access to the source code contained herein is hereby forbidden to
//    anyone except current COMPANY employees, managers or contractors who have executed
//    Confidentiality and Non-disclosure agreements explicitly covering such access.</p>

//    The copyright notice above does not evidence any actual or intended publication
//    or disclosure of this source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
//    DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
//    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
//    IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
//    OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
//    TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
//    ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
var loggedIn = true;
var sketchopened = false;
var __MA = true;
var quickDebug = false;
var debugMode = false;
//var formScroller, childScroller;
var baseUrl = '/';
var debugHostUrl = window.location.protocol + '//' + window.location.hostname + ':3010/';
var sessionId = '';
const iPad = navigator.userAgent.match(/(iPad)/) || (navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1);
const iOS10 = iPad && ((window.navigator.userAgent).indexOf('OS 10') > -1);
var windowsTouch = !!(navigator.platform == 'Win32' && navigator.maxTouchPoints);  // /Win/.test(window.navigator.userAgent) ? (window.navigator.msMaxTouchPoints || ("ontouchstart" in window)) ? true : false : false;
var WNTD = (navigator.platform == 'Win32' && !navigator.maxTouchPoints);    //Windows Non-Touch Desktop
var localDev = ((window.location.hostname == "localhost") || (window.location.hostname.substr(0, 3) == "192") || (window.location.hostname.substr(0, 3) == "127"))
var pstream, tapped = null;
var camCapture;
var photos;
var sketchApp;
var sketchRenderer;
var keyboardActive = false;
var lastClickTime;
//var screenMenuScrolled = false;
var scrolling = false;
var _enableWarehouse = false;
var CCWarehouse = new CCWarehouseAPI();

Object.defineProperties(ccma.Environment, {
    OS: {
        get: function () {
            return (navigator.platform == 'Win32') ?
                'Windows'
                : navigator.userAgent.match(/(iPad)/) ? 'iOS'
                    : (navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1) ? 'iPadOS'
                        : (navigator.platform === 'MacIntel') ? 'Mac OS'
                            : "Other"
        }
    },
    iPad: {
        get: function () { return (this.OS == 'iOS' || this.OS == 'iPadOS'); }
    },
    WindowsTouch: {
        get: function () { return (this.OS == 'Windows' && navigator.maxTouchPoints > 1); }
    },
    WindowsDesktop: {
        get: function () { return (this.OS == 'Windows' && !navigator.maxTouchPoints); }
    }
});

function canClickNow() {
    var now = (new Date()).tickSeconds();
    var clickable = false;
    if (lastClickTime == null || (now - lastClickTime >= 1)) {
        clickable = true;
    }
    lastClickTime = now;
    if (!clickable)
        log('Not ready to accept new clicks.')
    return clickable;
}

function onTransition() {
    if (transiting) log('On Transition. Cancelling event.');
    return transiting;
}

var touchClickEvent = windowsTouch ? 'click' : iPad ? 'touchend' : 'touchend click';

var logCurrentStack = function () {
    var site = window.location.protocol + '//' + window.location.host;
    let rsite = new RegExp(site.replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g');
    var stack = Error().stack.replace(rsite, '').replace(new RegExp('[@]', 'g'), ' at ');
    var lines = stack.split('\n').map(x => x.trim());
    var trans = [];
    for (var line of lines) {
        if (line.indexOf('logCurrentStack') > -1) continue;
        if (line.indexOf('?zt=') > -1) {
            line = line.replace(/[?]zt[=][0-9]*/, '');
        }
        line = '>      ' + line;
        trans.push(line);
    }
    trans.push('--------------------------------------------------')
    var stackTrace = trans.join('\n');
    console.log(stackTrace);
    return stackTrace;
}

//function countIdle() {
//    if (idleCounter) {
//        window.clearInterval(idleCounter)
//        idleCounter = null;
//        idleCount = 0;
//    }
//    idleCounter = window.setInterval(function () {
//        idleCount += 0.5;
//    }, 1000 * 30);
//}

function refreshApp() {
    try {
        setScreenDimensions();
        if (navigator.onLine && typeof (caches) != 'undefined') {
            $$$('getcacheversion', {}, (res) => {
                if (localStorage.macacheVersion != 'MA-' + res) {
                    messageBox('A settings update is available.', ["OK", "Cancel"], () => {
                        $('.splash-progress-window').hide(); $('.splash-screen').show(); $('.splash-progress-continuous').show();
                        window.location.reload();
                    }, () => { });
                }
            });
        }
        //window.applicationCache.update();
    }
    catch (e) { }
}

function updateApplication() {
    try {
        window.applicationCache.update();
    }
    catch (e) {

    }
}

function updateLastActivityTime() {
    if (ccma.Session.IsLoggedIn) {
        var now = (new Date()).tickSeconds();
        localStorage.setItem("last-activity-time", now);
    } else {
        clearLastActivityTime();
    }
}

function getLastActivityTime() {
    return parseInt(localStorage.getItem("last-activity-time"));
}

function clearLastActivityTime() {
    localStorage.removeItem("last-activity-time");
}

function updateLastSaveTime() {
    if (ccma.Session.IsLoggedIn) {
        var now = (new Date()).tickSeconds();
        localStorage.setItem("last-saved-time", now);
    } else {
        clearLastSaveTime();
    }
}

function getLastSaveTime() {
    return parseInt(localStorage.getItem("last-saved-time"));
}

function clearLastSaveTime() {
    localStorage.removeItem("last-saved-time");
}

function saveWindowState() {
    localStorage.setItem("window-state", $(document.body).html());
}

function resetApp(prompt) {
    if (prompt)
        messageBox('Are you sure you want to restart the application? ', ["Yes", "No"], function () {
            window.location.reload();
        });
    else
        window.location.reload();
}

function setLoginProgress(n) {
    $('.login-progress-bar').css({ 'background-position-x': (n * 2) + 'px' });
}

function hideSplash() {
    $('.splash-screen').hide();
}

function showSplash() {
    $('.update-box').hide();
    $('.splash-screen').show();
}

var $T = new $.jQTouch({
    preloadImages: [],
    pageChange: function (target) {
        ccma.UI.ActiveScreenId = $(target).attr('id');
        geo.adjustGISTimerForMap(ccma.UI.ActiveScreenId);
    }
});

if (window.location.search.indexOf('ipad=true') > -1) {
    log("Running debug mode ...");
    quickDebug = true;
}

if (window.location.search.indexOf('debug') > -1) {
    log("Running debug mode ...");
    debugMode = true;
}
function resize() {
    var sHeight = $('.page-content').height() - $('header', $('#' + ccma.UI.ActiveScreenId)).height();
    var scrollContainer = $('#' + ccma.UI.ActiveScreenId);
    var scrollable = $('.scrollable', scrollContainer);
    scrollable.each(function (i, scroll) {
        var scrollerHeight = ccma.UI.Layout.Screen.UsableHeight - $(scrollable).offset().top + $(scrollContainer).offset().top;
        //        console.log(ticks().toString(), $(scrollContainer).attr('id'), ":", ccma.UI.Layout.Screen.UsableHeight, "-", $(scrollable).offset().top, "+", $(scrollContainer).offset().top, "=", scrollerHeight);

        $(scroll).height(scrollerHeight);
    });
    if (ccma.UI.ActiveScreenId == "data-collection") {
        $('#dc-categories').height(sHeight);
        $('#dc-form,#prc-card').css('Height', '');
        $('#dc-form,#prc-card').height(sHeight);
    }
}
function ajaxRequest(params) {
    if (!params.data)
        params.data = {};

    if (localStorage.getItem('nbhd') != null) {
        var nbhd = "", locNbhd = localStorage.getItem('nbhd').split('||');
        for (n in locNbhd)
            nbhd += (locNbhd[n].toString().split('##')[0] + "||")
        params.data['nbhd'] = nbhd.slice(0, -2);
    }
    if (syncNbhd.length > 0) {
        var nbhd = "";
        for (var i = 0; i < syncNbhd.length; i++)
            nbhd += (syncNbhd[i].toString().split('##')[0] + "||");
        params.data['nbhd'] = nbhd.slice(0, -2);
    }
    if (myLocation) {
        params.data['lat'] = myLocation.lat();
        params.data['lon'] = myLocation.lng();
        params.data['gisaccuracy'] = gisAccuracy;
    } else {
        params.data['lat'] = 0;
        params.data['lon'] = 0;
        params.data['gisaccuracy'] = gisAccuracy;
    }


    req = $.ajax(params);
}
/*
var iScrollOptions = {
    useTransition: true,
    hasVerticalScroll: true,
    onBeforeScrollStart: function (e) {
        
        var target = e.target;
        while (target.nodeType != 1) target = target.parentNode;
        var tag = target.tagName ? target.tagName.toUpperCase() : '';
        //if (tag != 'SELECT' && tag != 'INPUT' && tag != 'TEXTAREA')
        //    e.preventDefault();
       // if (inputElementInFocus) $(inputElementInFocus).blur()
    },
    onScrollMove: function (e) {
        scrolling = true;
    },
    onScrollEnd: function (e) {
        scrolling = false;
    }
}
document.addEventListener('touchmove', function(e) {
     e.preventDefault();
    screenMenuScrolled = true;
}, { passive: false });*/

$(document).bind('touchmove', function (e) {
    scrolling = true;
    document.body.scrollTop = 0
    if ((ccma.UI.ActiveScreenId == "data-collection" && document.activeElement.tagName == 'INPUT') || ccma.UI.ActiveScreenId == 'login')
        hideKeyboard();
});
$(document).bind('touchend', function (e) {
    //e.preventDefault();
    //screenMenuScrolled = false;
    scrolling = false;
});
$(window).bind('orientationchange', function () {       //--resize
    if(document.activeElement.tagName== "SELECT") document.activeElement.blur();
    setScreenDimensions(null,null,null,null,true);
    setScreenDimensionsDelayed(null, false);       // to avoid black space during screen orientation . 
    //if (datacollectionsscroll)
    //  refreshScrollable();
});

$(window).bind('touchstart mousedown', function () {
    if (loggedIn && !wasLoggedInLast())
        checkForLoginStatus();

});

$('.page-footer').bind('touchstart mousedown', function () {
    return false;
});

/******************************************************
Keyborad event catch -- MSP starts                              
SSB: This needs reconsideration. All focusin focusout caused setScreenDimensions twice.
******************************************************/
//var clickfocus = false;
document.addEventListener('focusout', function (e) {
    if (sketchopened == true)
        return true;
    setScreenDimensionsDelayed(null, false);
    /*  hideKeyboard();
    setTimeout('resize();', 300);
    if ($(document.body).height() != $(window).height())
        setScreenDimensionsDelayed(null, false, true); */
});
/*
document.addEventListener('mousedown', function (e) {
    // clickfocus = true;
});*/
document.addEventListener('focusin', function (e) {
    if (sketchopened == false && (($(e.target).parents('#data-collection').length > 0) || ($(e.target).attr('type') == 'text') || ($(e.target).attr('type') == 'number') || ($(e.target).attr('type') == 'textarea') || ($(e.target).attr('type') == 'password')))         //SBS Adj
        setScreenDimensionsDelayed(null, false);
    lastFieldValue = $(e.target).val();

	/*{
        //if ($(e.target).val() != "" && ($(e.target).attr('type') == 'text' ))//|| $(e.target).attr('type') == 'textarea'))
        //  e.target.setSelectionRange(0, 9999);
        lastFieldValue = $( e.target ).val();
        keyboardActive = screenHeight > $(window).height() + 100 ? true : false;
        setScreenDimensions(null, keyboardActive, true);
        setScreenDimensionsDelayed(null, false, true);
    }*/

});

document.addEventListener('mouseup', function (e) {

    var browser = window.navigator.userAgent.toLocaleLowerCase();
    if (browser.match(/ipad/i) || browser.match(/iphone/i)) {
        if (sketchopened == false && (($(e.target).parents('#data-collection').length > 0) || ($(e.target).attr('type') == 'text') || ($(e.target).attr('type') == 'number') || ($(e.target).attr('type') == 'textarea') || ($(e.target).attr('type') == 'password'))) {        //SBS Adj        
            if (e.target != null)
                e.preventDefault();
        }
    }

});
/******************************************************
Keyborad event catch -- MSP ends                              
******************************************************/

function initButtons() {
	initSVButton();
    $('button').bind('touchstart mousedown', function (e) {
        //e.preventDefault();
        $(this).addClass("touched");
    });

    $('button').bind('touchend mouseup', function (e) {
        //e.preventDefault();
        $(this).removeClass("touched");
    });
    $('#color_picker').colorPicker();
    $('button').bind('touchcancel mouseleave', function (e) {
        //e.preventDefault();
        $(this).removeClass("touched");
    });

    $('a.button').bind('touchstart mousedown', function (e) {
        //e.preventDefault();
        $(this).attr("active", "active");
        setScreenDimensionsDelayed(null, false);
    });

    $('a.button').bind('touchend mouseup', function (e) {
        //e.preventDefault();
        $(this).removeAttr("active");

    });

    $('a.button').bind('touchcancel mouseleave', function (e) {
        //e.preventDefault();
        $(this).removeAttr("active");
    });
    $('.onoffswitch').bind(touchClickEvent, function (e) {
        if (e.detail > 1) return false;
        var ele = $(this)
        var onoffswitch_id = $(this).find('.onoffswitch-checkbox').attr("id");

        if ($(ele).hasClass('disable') && ccma.UI.ActiveScreenId == 'sort-screen')
            return false;
        if (onoffswitch_id == "") {
            $(this).find('.onoffswitch-checkbox').is(':checked') ? $(this).find('.onoffswitch-checkbox').removeAttr('checked') : $(this).find('.onoffswitch-checkbox').prop('checked', true);
        }
        else {
            $(this).find('#' + onoffswitch_id).is(':checked') ? $(this).find('#' + onoffswitch_id).removeAttr('checked') : $(this).find('#' + onoffswitch_id).prop('checked', true);
        }

        if (ccma.UI.ActiveScreenId != 'sort-screen' && onoffswitch_id == "")
            $('.fy-drag').not(this).not($('.onoffswitch', $('#sort-screen'))).find('.onoffswitch-checkbox').is(':checked') ? $('.fy-drag').not(this).not($('.onoffswitch', $('#sort-screen'))).find('.onoffswitch-checkbox').removeAttr('checked') : $('.fy-drag').not(this).not($('.onoffswitch', $('#sort-screen'))).find('.onoffswitch-checkbox').prop('checked', true);


       

        if ($(this).hasClass('fy-drag')) {
            if (ccma.UI.ActiveScreenId == 'sort-screen') {
                showFutureDataInSortscreen = showFutureData = $('.onoffswitch-checkbox', $('#' + ccma.UI.ActiveScreenId)).is(":checked");
                if (!showFutureDataInSortscreen)
                    $('.fy-drag').not($('.onoffswitch', $('#sort-screen'))).find('.onoffswitch-checkbox').removeAttr('checked')
                else
                    $('.fy-drag').not($('.onoffswitch', $('#sort-screen'))).find('.onoffswitch-checkbox').prop('checked', true);
            }
            else {
                showFutureData = $('.onoffswitch-checkbox', $('#' + ccma.UI.ActiveScreenId)).is(":checked");
                if (ccma.UI.ActiveScreenId == "data-collection") {
                    showFutureData = $('.onoffswitch-checkbox', $('#parcel-dashboard')).is(":checked");
                    if (showFutureData) { $('fy').addClass('flactive'); $('fy').html('FY'); } else { $('fy').removeClass('flactive'); $('fy').html('CY'); }
                }
                else if (ccma.UI.ActiveScreenId == "gis-map") {
                    showFutureData = $('.onoffswitch-checkbox', $('#parcel-dashboard')).is(":checked");
                    if (showFutureData) { $('fy').addClass('flactive'); $('fy').html('FY'); } else { $('fy').removeClass('flactive'); $('fy').html('CY'); }
                }
            }
            //    localStorage.setItem( 'showFutureData', showFutureData );
            if (ccma.UI.ActiveScreenId != 'sort-screen')
                getParcel(activeParcel.Id, function (p) {
                    if (ccma.UI.ActiveScreenId == 'digital-prc') {
                        fySwitchInPrc = true;
                        refreshDigitalPRC();
                    }
                });
            else {
                if (!validateCyFy) {
                    $(ele).addClass('disable')
                    executeQueries(_db_drop.FYLTableSettings, function () {
                        createParcelView(function () {
                            showSortScreen();
                            setTimeout(function () { $(ele).removeClass('disable') }, 2000);
                        })
                    })
                }
            }
        }
    });
    $('.fy_selector', $('#sort-screen')).css('right', '240px');
    //   showFutureData = JSON.parse( localStorage.getItem( 'showFutureData' ) );
    $('.record-add-photo').unbind(touchClickEvent);
    $('.record-add-photo').bind(touchClickEvent, function (e) {
        hideKeyboard();
        if (!checkPPAccess('dccPhoto')) {
            var msg = "This option is not available for the selected parcel.";
            if (activeParcel.CC_Deleted == "true")
                msg = "Photos cannot be taken on a property that is flagged to be deleted. Removing the Deleted option for this parcel will allow you to take photos.";
            messageBox(msg);
            return;
        }
        appState.photoCatId = appState.catId;
        var altKeyValue = ShowAlternateField == "True" ? eval("activeParcel."+AlternateKey) : null;
        photos.openParcel(activeParcel.Id, activeParcel.KeyValue1, { showPrimary: true }, true, altKeyValue);
    });
    $('.record-photo-preview').unbind(touchClickEvent);
    $('.record-photo-preview').bind(touchClickEvent, function (e) {
        e.preventDefault();
        hideKeyboard();
        if (!checkPPAccess('dccPhoto')) {
            var msg = "This option is not available for the selected parcel.";
            if (activeParcel.CC_Deleted == "true")
                msg = "Photos cannot be taken on a property that is flagged to be deleted. Removing the Deleted option for this parcel will allow you to take photos.";
            messageBox(msg);
            return;
        }
        appState.photoCatId = 0;
        var altKeyValue = ShowAlternateField == "True" ? eval("activeParcel."+AlternateKey) : null;
        photos.openParcel(activeParcel.Id, activeParcel.KeyValue1, { showPrimary: true },null, altKeyValue);
    });
}
if (windowsTouch) {
    $(window).bind('resize', function () {       //--resize
        setScreenDimensions();
    });
}


$.onswipe = function (c, dir, callback) {
    if (iPad || windowsTouch) {
        //  alert("swipe");
        var xMin = 40;
        var yMax = 100;
        $(c).unbind('touchstart');
        $(c).bind('touchstart', function (e) {
            //e.preventDefault();
            var startX = e.changedTouches[0].clientX;
            var startY = e.changedTouches[0].clientY;
            $(this).unbind('touchend');
            $(this).bind('touchend', function (e) {
                //e.preventDefault();
                var endX = e.changedTouches[0].clientX;
                var endY = e.changedTouches[0].clientY;
                var swiped = false;
                //  alert(dir + "," + startX + "," + startY + "," + endX + "," + endY);
                var leftSwiped = (startX - endX > xMin) && (dir == 'left' || dir == 'both') && (Math.abs(startY - endY) < yMax);
                var rightSwiped = (endX - startX > xMin) && (dir == 'right' || dir == 'both') && (Math.abs(startY - endY) < yMax);
                if (leftSwiped || rightSwiped) {
                	if (c == ".parcel-item,.parcel-item-new" && $(this).hasClass('parcel-item-new') && !combinedView && isBPPUser) {
                		if (listType == 0) { 
                			$('.parcel-item,.parcel-item-new').removeClass('selected');   
            				$(this).addClass('selected');
                			appState.homeParentParcelSelectedIndex = $('.parcel-item-new').index($(this)); 
                		}
                		else appState.homeParcelSelectedIndex = '0';
                	}
                    if (callback) callback(this, leftSwiped);
                } else {
                    if (c == ".parcel-item,.parcel-item-new" && !scrolling) {
                    	if ($(this).hasClass('parcel-item-new') && !combinedView && isBPPUser) {
                    		if (listType == 0) appState.homeParentParcelSelectedIndex = $('.parcel-item-new').index($(this));
                    		else appState.homeParcelSelectedIndex = '0';
                    	}
                        selectParcelInHomeScreen(this);
                    }
                }
                $(this).unbind('touchend');
            });
        });
    }
    else {
        $(c).unbind('dblclick');
        $(c).bind('dblclick', function () { if (callback) callback(this); });

    }
}



$.ontap = function (c, dir, callback) {
    if (iPad) {
        var xMin = 40;
        var yMax = 100;
        $(c).unbind('touchstart');
        $(c).bind('touchstart', function (e) {
            e.preventDefault();
            var startX = e.changedTouches[0].clientX;
            var startY = e.changedTouches[0].clientY;
            $(this).bind('touchend', function (e) {
                e.preventDefault();
                var endX = e.changedTouches[0].clientX;
                var endY = e.changedTouches[0].clientY;
                var swiped = false;
                var leftSwiped = (startX - endX > xMin) && (dir == 'left') && (Math.abs(startY - endY) < yMax);
                var rightSwiped = (endX - startX > xMin) && (dir == 'right') && (Math.abs(startY - endY) < yMax);
                if (leftSwiped || rightSwiped) {
                    if (callback) callback(this);
                }
                $(this).unbind('touchend');
            });
        });
    }
    else {
        $(c).unbind('click');
        $(c).bind('click', function () { if (callback) callback(this); });

    }
}
function clickLock(btn) {
    $(btn).prop('disabled', true);
    setTimeout(function () {
        $(btn).removeAttr('disabled')
    }, 2000);
}


function doLogin(callback) {
    setLoginProgress(5);
    var uname = $('.login-username').val();
    var pword = $('.login-password').val();
    if ((uname == "") || (pword == "")) {
        log('$' + uname + '$  $' + pword + '$');
        if ((uname == "") && (pword == "")) {
            log('Username and Password are blank');
        } else if (uname == "") {
            log('Username is blank');
        } else if (pword == "") {
            log('Password is blank');
        }
        showLogin();
        return;
    }
    $('.login-password').val('');
    setLoginProgress(20);
    ajaxRequest({
        url: baseUrl + 'maauth/login.jrq?zt=' + ticks(),
        dataType: 'json',
        type: 'POST',
        data: {
            username: uname,
            password: pword
        },
        success: function(data) {
            setLoginProgress(50);
            if (data.status) {
                if (data.status == "OK") {
                    var processor = function() {
                        setLoginProgress(90);
                        ccma.Session.IsLoggedIn = true;
                        loggedIn = true;
                        hasParcels = false;
                        var username = data.username;
                        localStorage.setItem('username', username);
                        if (data.usertype == 'BPP') {
                            ccma.Session.UserType = "BPP"
                            localStorage.setItem('usertype', 'BPP');
                            ccma.Session.bppRealDataEditable = data.BPPDownloadType;
                            ccma.Session.bppRealDataReadOnly = data.RealDataReadOnly;
                            localStorage.setItem('bppRealDataEditable', data.BPPDownloadType);
                            localStorage.setItem('bppRealDataReadOnly', data.RealDataReadOnly);
                            if(!data.RealDataReadOnly && data.BPPDownloadType == 0){
                            	ccma.Session.IsPPOnly = true;
                            	localStorage.setItem('IsPPOnly', ccma.Session.IsPPOnly);
                            }
                            else{
                                ccma.Session.IsPPOnly = false;
                            	localStorage.setItem('IsPPOnly', ccma.Session.IsPPOnly);
                            }
                            if(data.BPPDownloadType === 0) data.RealDataReadOnly = true;
                            localStorage.setItem('RealDataReadOnly', data.RealDataReadOnly);
                            if (data.RealDataReadOnly) {
                                data.BPPDownloadType = 1;
                            }
                            localStorage.setItem('BPPDownloadType', data.BPPDownloadType);
                            isBPPUser = true;
                            ccma.Session.RealDataReadOnly = data.RealDataReadOnly;
                            ccma.Session.BPPDownloadType = data.BPPDownloadType;
                        } else if (data.usertype == 'RP') {
                            ccma.Session.UserType = "RP"
                            localStorage.setItem('usertype', 'RP');
                            isRPUser = true;
                        } else {
                            localStorage.setItem('usertype', '');
                            isBPPUser = false;
                        }
                        if (!preventSchemaUpdate) localStorage.setItem('schema_version', localDBName.split('MA')[1]);
                        ccma.Session.User = username;
                        var hash = CryptoJS.SHA1(pword)
                        localStorage.setItem('loginpassword', hash.toString());
                        if (callback) callback();
                    }
                    if (clearDB) {
                        if (!errorInWebSql && !errorInIDB) dropDatabase(function() {
                            setLoginProgress(60);
                            clearDatabase(function() {
                                setLoginProgress(70);
                                processor();
                            });
                        });
                        else if (!errorInIDB) dropDatabase(function() {
                            setLoginProgress(70);
                            processor();
                        });
                        else if (!errorInWebSql) clearDatabase(function() {
                            setLoginProgress(70);
                            processor();
                        });
                        else processor();
                    } else processor();
                } else {
                    ccma.Session.IsLoggedIn = false;
                    messageBox(data.message, function() {
                        showLogin();
                    });
                }
            }
        },
        error: function(req, e, s) {
            tempLogin(pword, callback)
        }
    })
}

function tempLogin(pword, callback, eStatus) {
    var uname = $('.login-username').val();
    var hash = CryptoJS.SHA1(pword)
    var recentPass = localStorage.getItem('loginpassword');
    var recentUserName = localStorage.getItem('username');
    if (hash.toString() == recentPass && recentUserName == uname) {
        ccma.Session.IsLoggedIn = true;
        loggedIn = true;
        hasParcels = false;
        ccma.Session.User = uname;
        if (callback) callback();
    }
    else if(eStatus === 'abort' && navigator.onLine)
    	resetApp();
    else {
        ccma.Session.IsLoggedIn = false;
        showError('no-network-connectivity');
    }
}
function $$$(url, data, callback, errorCallback) {
    var posturl = 'mobileassessor/' + url.replace('/', '') + '.jrq?zt=' + ticks();
    ajaxRequest({
        url: posturl,
        type: "POST",
        dataType: 'json',
        data: data,
        success: function (data, status) {
            if (data == null) {
                console.log('Service response is null', posturl, data)
                return false;
            }
            if (data.status == "Error") {
                console.warn(data.Message);
                messageBox(data.message);
                return false;
            }
            if (data.LoginStatus) {
                if ((data.LoginStatus == "403") || (data.LoginStatus == "401")) {
                    log('Logging out because login status is ' + data.LoginStatus + ' on ' + baseUrl + 'mobileassessor/' + url.replace('/', '') + '.jrq');
                    LogoutErrorInsert('sl: $$$725')
                    showLogin();
                    return false;
                }
                if (data.LoginStatus == "423") {
                    window.location.href = data.RedirectURL;
                    return false;
                }
            }
            if (url != "licenseagreement" && url != "getcacheversion" && url != "getcachefiles") {
                ccma.Session.IsLoggedIn = true;
                updateLastActivityTime();
            }
            if (callback) callback(data, status);
        },
        error: function (resp, stat, errorMessage) {
            if (errorCallback) errorCallback(resp, stat, errorCallback);
            else
                messageBox('Connection to CAMA Cloud service is broken!');
        }
    });
}

function setPass(p) {
    $('.login-password').val(p);
}

function setProgress(p) {
    $('#pbar').css('background-position', (300 * p / 100) + 'px 0px');
}

function setSplashProgress(text, percent) {
    if (percent == null)
        percent = 0;
    var pbw = $('.splash-progress-window .progress').width();
    var pw = Math.round(pbw * (percent / 100));
    $('.splash-progress-window .progress').css('background-position-x', pw + 'px');
    $('.splash-progress-window label').html(text);
}

function setSplashProgressLabel(text) {
    $('.splash-progress-window label').html(text);
}


function setProgressLabel(text) {
    if (!text)
        text = "Sync in progress ...";
    $('#progressbar span').html(text)
}

function showIfTrue(val) {
    if ((val == true) || (val == 'true') || (val == 'True'))
        return 'auto';
    else
        return 'none';
}

function showIfPriority(val) {
    if (EnableNewPriorities == 'True') {
        if (val == '3')
            return 'auto';
        else
            return 'none';
    }
    else {
        if (val == '1')
            return 'auto';
        else
            return 'none';
    }
}
function showIfUnsync(val) {
    return false;
}

function showIfAlert(val) {
    if (EnableNewPriorities == 'True') {
        if (val == '4')
            return 'auto';
        else
            return 'none';
    }
    else {
        if (val == '2')
            return 'auto';
        else
            return 'none';
    }
}

function showIfCritical(val) {
    if (val == '5' && EnableNewPriorities == 'True')
        return 'auto';
    else
        return 'none';
}

function showIfMedium(val) {
    if (val == '2' && EnableNewPriorities == 'True')
        return 'auto';
    else
        return 'none';
}


function showIfNormal(val) {
    if (val == '1' && EnableNewPriorities == 'True')
        return 'auto';
    else
        return 'none';
}

//function showIfCaution(val) {
//    if (val == '0')
//        return 'auto';
//    else
//        return 'none';
//}
function showIfCaution(val) {
    if (val == '1')
        return 'cautionAlert';
    else
        return ' ';
}

function hideDistoClose() {
    if (_enableWarehouse) {
        $('.disto_close').show();
        if (clientSettings["EnableDisto"] == "1" || clientSettings["EnableDisto"] == 'true')
            $('.ccse-disto').show();
        $('#camera,#camera1,#camera2').remove();
    } else {
        $('.ccse-disto').hide();
        $('.disto_close').hide();
    }
}

log("***!clear");
log("***!useragent");

this.__defineGetter__("EnableWarehouse", function () { return _enableWarehouse; });
this.__defineSetter__("EnableWarehouse", function (val) {
    _enableWarehouse = val;
    hideDistoClose()
});

function invokeUrl(url) {
    function clickLink(link) {
        var cancelled = false;

        if (document.createEvent) {
            var event = document.createEvent("MouseEvents");
            event.initMouseEvent("click", true, true, window,
                0, 0, 0, 0, 0,
                false, false, false, false,
                0, null);
            cancelled = !link.dispatchEvent(event);
        }
        else if (link.fireEvent) {
            cancelled = !link.fireEvent("onclick");
        }

        if (cancelled) {
            window.location = link.href;
        }
    }

    $('.url-invoker').attr('href', url);
    clickLink($('.url-invoker')[0]);
}