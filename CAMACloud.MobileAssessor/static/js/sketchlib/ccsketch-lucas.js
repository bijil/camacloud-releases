﻿
CAMACloud.Sketching.Formatters.Lucas = {
    name: "Lucas ohio",
    saveAllSegments: false,
    arcMode: "ARC",
    allowSegmentAddition: false,
    allowSegmentDeletion: false,
    preventSavingOpenSegments: false,
    allowSegmentAdditionOnMidsketch: true,
    nodeToString: function (n) {
        var str = "";
        if (n.arcLength != 0) {
            str += "B";
        }
        if (!n.isStart && (n.dy > 0) && (n.dx > 0)) {
            str += "V";
        }
        if (n.dy > 0) {
            str += n.sdy + sRound(n.dy);
        }
        if (n.dx > 0) {
            str += n.sdx + sRound(n.dx);
        }
        if (n.isStart) {
            str += "C";
        }
        if (n.arcLength > 0) {
            str += "O" + sRound(n.arcLength);
        }
        if (n.arcLength < 0) {
            str += "I" + Math.abs(sRound(n.arcLength));
        }
        if (n.vector.isMarkedArea && n.isStart) {
            str += "M";
        }
        return str;
    },
    vectorToString: function (v) {
        var str = "";
        if (v.startNode != null) {
            str += v.startNode.vectorString();
            var nn = v.startNode.nextNode;
            while (nn != null) {
                str += nn.vectorString();
                nn = nn.nextNode;
            }
           
        }
        return str;
    },
    labelPositionFromString: function (editor, l) {
        if (!l)
            return null
        var parts = l.split(',');
        return new PointX(parseFloat(parts[0]), -(parseFloat(parts[1])))
    },
    vectorFromString: function (editor, s) {
        if (s == null) {
            var nv = new Vector(editor);
            nv.isClosed = false;
            nv.label = "Blank"
            nv.uid = -1
            return nv;
        }

        var o = editor.origin;
        var p = o.copy();

        var v = new Vector(editor);

        var hi = s.split(':');
        var head = hi[0];
        var lineString = s
        var oppositAction = false;
        v.header = "";
        var isBox = false;
        var isStart = false;
        var hasStarted = false;
        var isVeer = false;
        var veerSteps = 0;
        var lastDirection;
        var ins = lineString.match(/[C]|[V]|[M]|[UDLRXF][0-9]*(\.[0-9]+)?|[B][A-Z]*[0-9]*[A-Z]*[0-9]*[OI]{1}[0-9]*/g);
        if (!ins) {
            var nv = new Vector(editor);
            nv.isClosed = false;
            nv.label = "Blank"
            nv.uid = -1
            return nv;
        }
        for (i = 0; i < ins.length; i++) {
            var cmd = ins[i];
            var distance = 0;
            var isMarkedArea = false;
            var isArcs = false, arcLength = 0;
            if (cmd == "C") {
                isStart = true;
            }
            else if (cmd == "V") {
                isVeer = true;
                veerSteps = 2;
                continue;
            }
            else if (cmd == "M") {
            	isMarkedArea = true; 
            } 
            else if (cmd.indexOf("B") > -1) {
                if ((cmd.match(/B/g) || []).length > 1) { throw "Invalid sketch string format"; }
                isArcs = true;
                var lst = cmd.match(/[V]|[UDLROI][0-9]*/g);
                var z = 0, loopLength = 0, exeLoop = true;
                while(lst.length > z){
                    if(lst[z] == "V"){
                        isVeer = true;
                    }
                    else if(lst[z].indexOf("O") > -1 || lst[z].indexOf("I") > -1){
                       arcLength =  (lst[z].indexOf("O") > -1) ? Number(lst[z].split("O")[1]) : (-Number(lst[z].split("I")[1]));
                       exeLoop = false;
                       z = z + 1;
                    }
                    if(isVeer){
                        z = z + 1;
                        loopLength = z + 1;
                    }
                    if(exeLoop){
                        for(var j = z; j <= loopLength; j++){
                            z++;
                            var cp = lst[j].match(/[UDLRXF]|[0-9]*/g);
                            var dir = cp[0];
                            distance = cp[1];
                            var pix = parseFloat(distance) * DEFAULT_PPF;
                            lastDirection = dir;
                            switch (dir) {
                                case "U": oppositAction ? p.moveBy(0, -pix) : p.moveBy(0, pix); break;
                                case "D": oppositAction ? p.moveBy(0, pix) : p.moveBy(0, -pix); break;
                                case "L": oppositAction ? p.moveBy(pix, 0) : p.moveBy(-pix, 0); break;
                                case "R": oppositAction ? p.moveBy(-pix, 0) : p.moveBy(pix, 0); break;
                            } 
                        }
                        p = p.copy();
                    }
                }
            }
            else {
                var cp = cmd.match(/[UDLRXF]|[0-9]*(\.[0-9]+)?/g);
                var dir = cp[0];
                distance = cp[1];
                if (dir == "X") {
                    dir = lastDirection == 'U' ? 'R' : (lastDirection == 'D' ? 'L' : (lastDirection == 'R' ? 'D' : 'U'));
                    ins[i] = dir + cp[1]
                    i = i - 2;
                    isBox = true;
                }
                else if ( dir == 'F' ) {
                    var x = v.startNode.p.x - v.endNode.p.x
                    var y = v.startNode.p.y - v.endNode.p.y
                    if ( x != 0 ){
                        p.moveBy( parseFloat( x ) * DEFAULT_PPF, 0 );
                        var q = p.copy().alignToGrid( editor );
                        v.connect( p.copy() );
                    }
                    if ( y != 0 ){
                        p.moveBy( 0, parseFloat( y ) * DEFAULT_PPF );
                        q = p.copy().alignToGrid( editor );
                        v.connect( p.copy() );
                    }
                }
                var pix = parseFloat(distance) * DEFAULT_PPF;
                lastDirection = dir;
                switch (dir) {
                    case "U": oppositAction ? p.moveBy(0, -pix) : p.moveBy(0, pix); break;
                    case "D": oppositAction ? p.moveBy(0, pix) : p.moveBy(0, -pix); break;
                    case "L": oppositAction ? p.moveBy(pix, 0) : p.moveBy(-pix, 0); break;
                    case "R": oppositAction ? p.moveBy(-pix, 0) : p.moveBy(pix, 0); break;
                }
                if (!isVeer)
                    p = p.copy();
                if (isBox) oppositAction = true;
            }

            if (isVeer && !isArcs) {
                veerSteps--;
                if (veerSteps == 0) {
                    isVeer = false;
                } else
                    continue;
            }

            if (isMarkedArea) {
            	v.placeHolder = true;
                v.placeHolderSize = 2;
				v.isMarkedArea = true;
				v.terminateNode(v.startNode);
			}
			else if (isStart) {
                v.start(p.copy());
                hasStarted = true;
                isStart = false;
            } else if (hasStarted) {
                var q = p.copy().alignToGrid(editor);
                if (!v.startNode.overlaps(q))
                    v.connect(p.copy());
                else
                    v.terminateNode(v.startNode);
            }
            if (v.endNode){
            	v.endNode.arcLength = arcLength;
                if (arcLength != 0) {
                    v.endNode.isArc = true;
                    isVeer = false;
                }
                if (cmd) v.endNode.commands.push(cmd);
            }
        }

        if (v.startNode)
            if (v.startNode.overlaps(v.endNode.p.copy()))
                v.isClosed = true;
        return v;

    },
    open: function (editor, data) {
        editor.vectors = [];
        editor.sketches = [];
        let isError = false;

        for (var x in data) {
            var sketch = new Sketch(editor);
            var s = data[x];
            sketch.parentRow = s.parentRow;
            sketch.uid = s.uid;
            sketch.label = s.label;
            sketch.vectors = [];
            sketch.config = s.config || {};
            sketch.lookUp = s.lookUp;
            sketch.sid = s.sid;
            sketch.maxNotelen = s.maxNotelen;
            var counter = 0;
            try {
                for (var i in s.sketches) {
                    counter += 1;
                    var sk = s.sketches[i];
                    var v = this.vectorFromString(editor, sk.vector);
                    v.sketch = sketch;
                    v.uid = sk.uid;
                    v.index = counter;
                    v.name = sk.label[0].Value;
                    if (sk.label[0] && sk.label[0].colorCode) {
                        var clrcode = sk.label[0].colorCode.split('~');
                        v.colorCode = clrcode[0] ? clrcode[0] : null;
                        v.newLineColor = (clrcode[1] && sketchSettings['SketchLineColorCustomizations'] && sketchSettings['SketchLineColorCustomizations'].contains('MA')) ? clrcode[1] : null;
                        v.newLabelColorCode = (clrcode[2] && sketchSettings['SketchTextColorCustomizations'] && sketchSettings['SketchTextColorCustomizations'].contains('MA')) ? clrcode[2] : null;
                    }
                    v.label = '[' + counter + '] ' + sk.label.filter(function (a) { return !a.hiddenFromShow }).map(function (a) { return ((a.Description && !a.DoNotShowLabelDescription && sketchSettings["DoNotShowLabelDescriptionSketch"] != '1' && clientSettings["DoNotShowLabelDescriptionSketch"] != '1' && a.Description.Name) ? ((sketchSettings["DoNotShowLabelCode"] == '1' || editor.formatter.showLabelDescriptionOnly || a.showLabelDescriptionOnly || clientSettings.DoNotShowLabelCodeSketch == '1') ? a.Description.Name : a.Value + '-' + a.Description.Name) : a.Value) }).join('/');
                    if (s.config.FirstRecordlabelValue && i == 0) {
                        var labelField = s.config.FirstRecordlabelValue.split('/');
                        var labelFieldValue = labelField[1] ? (s.parentRow[labelField[0]] || '---') + '/' + (s.parentRow[labelField[1]] || '---') : s.parentRow[labelField[0]] || '---';
                        v.label = '[' + counter + '] ' + labelFieldValue;
                    }
                    v.labelFields = sk.label;
                    v.vectorString = sk.vector;
                    v.isChanged = sk.isChanged;
                    v.section = sk.section;
                    v.labelPosition = this.labelPositionFromString(editor, sk.labelPosition);
                    v.vectorConfig = sk.vectorConfig;
                    v.doNotAllowDeleteFirstVector = sk.doNotAllowDeleteFirstVector;
                    if (v.isMarkedArea && sk.areaFieldValue) {
                        v.fixedArea = v.areaFieldValue = sk.areaFieldValue;
                        v.noVectorSegment = v.CheckFixedArea = v.isUnSketchedArea = true;
                    }
                    else if (!v.isMarkedArea && sketch.config.EnableOBYEditlabel && (!v.vectorString || (!v.startNode && v.vectorString.indexOf('N') > -1 && v.vectorString.split('N')[1].trim() == '')))
                        v.MarkedAreaValue = sk.areaFieldValue ? sk.areaFieldValue.toString() : '0';
                    editor.vectors.pop();
                    sketch.vectors.push(v);
                }
            }
            catch (e) {
                editor.vectors = []; sketch.vectors = []; isError = true;
            }

            sketch.isModified = false;
            editor.loadNotesForSketch(sketch, s.notes);
            editor.sketches.push(sketch);
        }

        if (isError) {
            editor.sketches.forEach((sk) => { sk.vectors = []; });
            throw "Sketch cannot be rendered due to the wrong sketch data.";
        }
    }
}
