﻿function MvPOtherImpDefinition ( data, config, options, sketch ){

    if ( options && options.type == 'outBuilding' ) {
        var val = '', mvpData = {};
        $( '.Current_vector_details .head' ).html( 'OtherImprovement' )
        $( '.mask' ).show();
        $( '.Current_vector_details' ).css( 'width', 480 );        
        $( '.Current_vector_details' )
        var validation = true;
        $( '.skNote' ).hide(); $( '.Current_vector_details .vectorPage , .Current_vector_details .footradio,.Current_vector_details .obsketch' ).parent().remove()
        $( '.assocValidation,.UnSketched' ).remove();
        $( '.dynamic_prop div' ).remove();
        $('.dynamic_prop').html('')
        $( '.Current_vector_details' ).show()
        $( '.Current_vector_details #Btn_cancel_vector_properties' ).unbind( touchClickEvent )
        $( '.Current_vector_details #Btn_cancel_vector_properties' ).bind( touchClickEvent, function () {            
            $( '.Current_vector_details .outbdiv' ).remove();
            $( '.Current_vector_details' ).hide();
            if ( !sketchApp.isScreenLocked ) $( '.mask' ).hide();
            sketchApp.currentSketch.isNewSketch = false;
            sketchApp.currentSketch.isAfterSaveNewSketch = false;          
            return false;
        } )
        $( '.Current_vector_details .head' ).after( '<div class ="outbdiv"><div class="obsketch"><label class ="checkboxlabel" style="float: left;"><input class="obcheckbox" type="checkbox"  style="width: 20px;margin-right:7px;margin-top: -3px;"/>Draw Sketch</label></div><div><label style="float: left;width: 98px;"> Label</label><select class="obddl"></select><div class = "outType" style = "Display: none; padding: 10px 0px"></div></div></div>' );
        function appenlookup(pattern,callback){
            $( '.obddl' ).empty();
	         for ( var x in MVP_Outbuliding_Lookup ){
	             if(MVP_Outbuliding_Lookup[x].label_code.match(pattern)){
	                 $( '.obddl' ).append( '<option value="' + MVP_Outbuliding_Lookup[x].label_code + '">' + MVP_Outbuliding_Lookup[x].mdescr + '</option>' );
	             }
	         } 
         	if(callback) callback();        
        }

        if(options.isEdit != true){
               appenlookup('O');
           }
        function edit_appenlookup(){
            pattern=/O/;        
            appenlookup(pattern,function (){
              var OI_code = sketchApp.currentVector.mvpData['Outbuilding_Label_Code'];
              var value = MVP_Outbuliding_Lookup.filter(function(x){ return x.label_code == OI_code})[0];
              if (!value){
              	$( '.obddl' ).append( '<option value="' + OI_code + '">' + OI_code + '</option>' );
              }
              $( '.Current_vector_details .obddl' ).val( OI_code );
           });
        }        
        
        if ( options.isEdit == true && !sketchApp.currentVector.isUnSketchedArea ){
            $( ".Current_vector_details .obcheckbox" ).prop( 'checked',true);
            $( ".Current_vector_details .checkboxlabel" ).css('color','#a2a2a2')
            $( ".Current_vector_details .obcheckbox" ).prop( 'disabled','Disabled');
			edit_appenlookup();            
        }
        if ( options.isEdit == true && sketchApp.currentVector.isUnSketchedArea && sketchApp.currentVector.name ){
            $('.Current_vector_details .obcheckbox1').prop("checked", true);
            edit_appenlookup();
        }
        
        $( '.Current_vector_details #Btn_Save_vector_properties' ).unbind( touchClickEvent )
        $( '.Current_vector_details #Btn_Save_vector_properties' ).bind( touchClickEvent, function () {            
            val = $( '.obddl' ).val();
            var OI_imptype = val, mvpDisplayLabel = val;
            if (MVP_Outbuliding_Lookup.filter(function (OI){return OI.label_code == val})[0]) {
            	OI_imptype = MVP_Outbuliding_Lookup.filter(function (OI){return OI.label_code == val})[0].label_short;
            	mvpDisplayLabel = MVP_Outbuliding_Lookup.filter(function (OI){return OI.label_code == val})[0].mdescr;
            	if (typeof(mvpDisplayLabel) == 'string' && mvpDisplayLabel.length > 15)
                    mvpDisplayLabel = mvpDisplayLabel.substring(0, 15) + '...';
            }
            OI_imptype = OI_imptype.replace('&lt;','<');
            OI_imptype = OI_imptype.replace('&gt;','>');
            var skscale, OISketched, draw_outbuilding = $( ".obcheckbox" ).prop( 'checked' );
            if(draw_outbuilding){
            	skscale = ( sketchApp.currentSketch && sketchApp.currentSketch.boundaryScale )? sketchApp.currentSketch.boundaryScale: 80;
            	OISketched = 1;
            }
            else{
            	skscale = 0;
            	OISketched = 0;
            }
            if (options.isEdit == true){
            	if(draw_outbuilding && sketchApp.currentVector.isUnSketchedArea){
            		sketchApp.currentVector.mvpData['Scale'] = skscale;
            		sketchApp.currentVector.mvpData['Sketched_Other_Improvement_Indicator'] = OISketched;
            	}
            	sketchApp.currentVector.mvpData['Outbuilding_Label_Code'] = val;
            	mvpData = JSON.parse(JSON.stringify(sketchApp.currentVector.mvpData));
            }
            else	
            	mvpData = {Sketch_Pointer: '', Improvement_ID: '', Level_1_Default_Exterior_Cover : '', Level_1_Construction_Code: '', Level_1_N: '', Level_1_Prefix_For_Part_1: '', Level_1_Extra_Feature_Code: '', Level_1_Component_Code_For_Part_1: '', Level_1_Modifier_Code_For_Part_1: '', Level_1_Prefix_For_Part_2: '', Level_1_Component_Code_For_Part_2: '', Level_1_Modifier_Code_For_Part_2: '', Level_1_Prefix_For_Part_3: '', Level_1_Component_Code_For_Part_3: '', Level_1_Modifier_Code_For_Part_3: '', Level_1_Prefix_For_Part_4: '', Level_1_Component_Code_For_Part_4: '', Level_1_Modifier_Code_For_Part_4: '', Level_1_FreeForm_Label_Code: '', Level_2_Default_Exterior_Cover: '', Level_2_Construction_Code: '', Level_2_N: '', Level_2_Extra_Feature_Code: '', Level_2_Prefix: '', Level_2_Component_Code: '', Level_2_ModifierCode: '', Level_2_FreeForm_Label_Code: '', Level_3_Default_Exterior_Cover: '', Level_3_Construction_Code: '', Level_3_N: '', Level_3_Extra_Feature_Code: '', Level_3_Prefix: '', Level_3_Component_Code: '', Level_3_Modifier_Code: '', Level_3_FreeForm_Label_Code: '', Outbuilding_Label_Code: val, Reference: 0, Label_Position_X: 0, Label_Position_Y: 0, Area_Label_Position_X: 0, Area_Label_Position_Y: 0, Scale : skscale, Area: 0, Perimeter: 0, Level_1_Fin_Area: 0, Level_1_Unfin_Area: 0, Level_2_Fin_Area: 0, Level_2_Unfin_Area: 0, Level_3_Fin_Area:0, Level_3_Unfin_Area: 0, Symbol: 0, Symbol_Size: 0, Area_Font_Size: 0, Sketched_Other_Improvement_Indicator: OISketched}            
            if ( options.callback ){
                if (options.isEdit == true){
                	sketchApp.currentVector.labelFields[0].labelDetails = [{ labelValue: OI_imptype, out_label_Code: val }];
                	sketchApp.currentVector.labelFields[0].Value = OI_imptype;
                }
                options.callback( [{ Value: OI_imptype, out_label_Code: val}], { 'drawSketch': draw_outbuilding, 'OBlabelValue': val, 'mvpData': mvpData, 'mvpDisplayLabel': mvpDisplayLabel } )
            }
            if ( !sketchApp.isScreenLocked ) $( '.mask' ).hide();
            $( '.Current_vector_details' ).hide();
            if ( options.isEdit == true && draw_outbuilding && !sketchApp.currentVector.isUnSketchedArea) {
                sketchApp.currentVector.name = OI_imptype;
                sketchApp.currentVector.labelEdited = true;
                sketchApp.currentVector.isUnSketchedArea = false;
                sketchApp.currentVector.label = '[' + sketchApp.currentVector.index + '] ' + sketchApp.currentVector.name;
            	sketchApp.currentVector.mvpDisplayLabel = '[' + sketchApp.currentVector.index + '] ' + mvpDisplayLabel;
            }
            else if ( options.isEdit == true && draw_outbuilding && sketchApp.currentVector.isUnSketchedArea) {
            	sketchApp.currentVector.vectorString = null;
                sketchApp.currentVector.startNode = null;
                sketchApp.currentVector.endNode = null;
                sketchApp.currentVector.commands = [];
                sketchApp.currentVector.endNode = null;
                sketchApp.render()
                sketchApp.startNewVector( true );
                sketchApp.currentVector.isUnSketchedArea = false;
                sketchApp.currentVector.hideAreaValue = false;
                sketchApp.currentVector.name = OI_imptype;
                sketchApp.currentVector.labelEdited = true;
                sketchApp.currentVector.isClosed = false;
                sketchApp.currentVector.label = '[' + sketchApp.currentVector.index + '] ' + OI_imptype;
           		sketchApp.currentVector.mvpDisplayLabel = '[' + sketchApp.currentVector.index + '] ' + mvpDisplayLabel;
           }
           else if( options.isEdit == true && !draw_outbuilding && sketchApp.currentVector.isUnSketchedArea){
           		sketchApp.currentVector.name = OI_imptype;
                sketchApp.currentVector.labelEdited = true;
                sketchApp.currentVector.label = '[' + sketchApp.currentVector.index + '] ' + OI_imptype;
           		sketchApp.currentVector.mvpDisplayLabel = '[' + sketchApp.currentVector.index + '] ' + mvpDisplayLabel;
          }
          $( '.Current_vector_details .outbdiv' ).remove();
        } )
    }
 }   

function mvpLabelConfig(head, items, lookups, callback, editor, type) {
	if(head == "New Vector - Labels")
		head = "New Sketch - Labels";
	function BasementFunctionality(isChangSketchLabel, basecallback){
		var labelValues = '',BasementFunctionalityDetail = [], BFIndex =  0 ,ValidateLabelsString = '',sketch_type = '', CurrentLabelValue = '', editLabelChanged = false;
		sketch_type = ((editor.currentSketch.parentRow.MainBuildingType)=='D'?'Res':'Comm')
		var segment_Parent_Type = (sketch_type == 'Res' ? 'dwellings' : 'comm_bldgs');
		var grade_col = 'GradeLevel';	
		var prefix_filter_col = 'Prefix';		
		var constr_filter_col = 'ConstrOption';
		var material_filter_col = 'Material';
		
		var option = '', headers = [{name: 'Prefix', value: 'prefix', filter: prefix_filter_col, grade_filter: grade_col},{name: 'Component', value: 'comp'}];
		var lbl_level = [{name: 'Below Grade', value: 'Below_Grade', value1: 1, value2: 'Below'}];
		var html = '<table style="width: 100%;font-size: 18px;"><tbody><tr><th>Fractional Basement Functionality</th></tr></tbody></table>';
	
		$('.Current_vector_details .head').html(head);
		$( '.dynamic_prop div,.outbdiv' ).remove();
		$('.mask').show(); $('.dynamic_prop').html('');
   		$('.dynamic_prop').append('<div class="divvectors"></div>');
    
    	var getDescription = function(lbl_data, isField_1){
    		if (isField_1) return (lbl_data? (lbl_data.field_1 && lbl_data.field_1.trim() != '')? lbl_data.field_1: lbl_data.tbl_element_desc: '');
    		return (lbl_data? (lbl_data.tbl_element_desc && lbl_data.tbl_element_desc.trim() != '')? lbl_data.tbl_element_desc: lbl_data.field_1: '');
    	}
		headers.forEach(function(hdr) {
    		if(hdr.name == 'Prefix')
    			hdr.lookup = MVP_Lookup.filter(function(pro){ return (pro.tbl_type_code == hdr.name) && (pro.tbl_element_desc == '1/4' || pro.tbl_element_desc == '1/2' || pro.tbl_element_desc == '3/4') });
    		else
    			hdr.lookup = MVP_Lookup.filter(function(pro){ return pro.tbl_type_code == hdr.name &&  pro.AdditionalScreen && (pro.AdditionalScreen == "BGAreaEntry" || (pro.AdditionalScreen.split("/")[0] == "BGAreaEntry"))});
    	});
    
    	lbl_level.forEach(function(lbl, index) {
    		headers.forEach(function(hdr, hdr_index) {
    			html += '<div class="Below'+ hdr.name +'" style="width: 30%;border-right: 1px solid #c7cdcf;height:200px;"><span style="float:left;width:200px;justify-content: center;font-weight: bold;font-size: 14px;">'+hdr.name+'</span>';
    			if (hdr_index >= 0)
					hdr.lookup.filter(function(lk){ return (lk[grade_col] && lk[grade_col].indexOf(lbl.value2) > -1); }).forEach(function(lk) {
						if(lk.tbl_element != 1202 || lk.tbl_element != '1202'){
							if(lk.tbl_element_desc == "Bsmt")
								html += '<span style"float:left;width:200px;";><input type="radio" id="'+lk.tbl_element+'" name="'+hdr.name+'" value="' + getDescription(lk, true) + '" style="float:left;width:100px;height:20px"> ' + getDescription(lk) + '<input id="walkout" type="checkbox" style="width:50px;height:20px;" >walkout</span>'
							else
	    						html += '<span style"float:left;width:200px;";><input type="radio" id="'+lk.tbl_element+'" name="'+hdr.name+'" value="' + getDescription(lk, true) + '" style="float:left;width:50px;height:20px"> ' + getDescription(lk) + '<br></span>';
						}
	    			});
	    			html+='</div>'
    		});
    		html += '<div class="Belowmodifier" style="width: 20%;height:200px;"><span style="float:left;width:188px;justify-content: center;font-weight: bold;font-size: 14px;">Modifier</span>';
    		html += '<span style"float:left;width:200px;";><input type="radio" id="302" name="modifer" value="Fin" style="float:left;width:50px;height:20px;">Fin';
    		html += '<input type="radio" id="M303" name="modifierSign" value="+" style="float:left;width:50px;height:20px;" disabled>+<br></span>';
    		html += '<span style"float:left;width:200px;";><input type="radio" id="301" name="modifer" value="UF" style="float:left;width:50px;height:20px;"disabled>UF';
    		html += '<input type="radio" id="M304" name="modifierSign" value="-" style="float:left;width:56px;height:20px;" disabled>-<br></span></div>';
    	});
    	var previousLabelValue = items[0].Newlabel;
    	var notBelowLabel = '';
    	var previousLabelLength = previousLabelValue.split("|").length;
    	if(previousLabelLength > 1){
    		for(var t =0; t< previousLabelLength-1; t++)
    			notBelowLabel = notBelowLabel + previousLabelValue.split("|")[t] + '|';
    		labelValues = labelValues + notBelowLabel + previousLabelValue.split("|")[previousLabelLength - 1];
    	}
    	else
    		labelValues = labelValues + previousLabelValue;

    	$('.divvectors').append(html);
    	$('.dynamic_prop').append('<span class="ValidateLabels" style="display: none; width:450px; color:Red;margin-left: 150px;">'+ValidateLabelsString+'</span><span class="CurrentLabel" style="display: none; width:450px; color:Black;margin-left: 15px;">'+CurrentLabelValue+'</span>');
    	$('.Current_vector_details').show();   
 		$('.Current_vector_details').width(650);
 		$('.divvectors').height(380);
 		$('.skNote').show();
    	$('.skNote').html('<span><b>Sketch Label Preview:</b>&nbsp;</span><span class="label_preview">'+labelValues+'</span>'); 
		if(type == 'edit')
			$('#Btn_cancel_vector_properties').attr('disabled','disabled');
		var labelDetailsval1 = items[0].labelDetails.details[1];
		var cid = (labelDetailsval1.comp1 != 0 && labelDetailsval1.comp1 != '0') ? ($("#"+labelDetailsval1.comp1+"")[0].checked = true) : 0;
		//$("#"+labelDetailsval1.prefix1+"")[0].checked = true;
		$("input[name='Prefix'][id='"+ labelDetailsval1.prefix1 +"']")[0].checked = true;
		var mid = (labelDetailsval1.modifier1 != '' && labelDetailsval1.modifier1 != 0)? ((labelDetailsval1.modifier1 == 'Fin')? ($("#302")[0].checked = true): ($('#301')[0].checked= true)): '';
		var midsign = (labelDetailsval1.modifierSign1 != '' && labelDetailsval1.modifierSign1 != 0) ? ((labelDetailsval1.modifierSign1 == '+' ) ? ($("#M303")[0].checked = true) : ($("#M304")[0].checked = true)): '';
		var walko = labelDetailsval1.walkout1 != '' ?  ((labelDetailsval1.walkout1 == "Y" ? ($("#walkout")[0].checked = true) :'' )) : '';
		if(labelDetailsval1.comp1 != '202'){
			$('#walkout')[0].checked=false;
    		$('#walkout').attr('disabled','disabled');
		}
		if(labelDetailsval1.modifier1 == "Fin"){
        	$('#M303').removeAttr("disabled");
            $('#M304').removeAttr("disabled");
        }
    	$("input[type='radio'],input[type='checkbox']").change(function(){
    		if(type == 'edit')
    			editLabelChanged = false;
    		$('.ValidateLabels').hide();
			var lbl_val = $(this).val();
			var lbl_Id = $(this).attr('id'); 
			var comp_current_select_val = '',pre_current_select_val= '' ,modi_current_select_val = '', modifierSign_current_select_val = '', walk_value = '';
			var lookupVal = MVP_Lookup.filter(function(lkup){ return lkup.tbl_element == lbl_Id; })[0];
    		lookupVal = lookupVal? lookupVal: {};
    		var bel_comp = $('.BelowComponent span input[type=radio]:checked');
    		var bel_pre = $('.BelowPrefix span input[type=radio]:checked');
    		var bel_modi = $('.Belowmodifier span input[type=radio]:checked');
    		comp_current_select_val =bel_comp[0]? bel_comp[0].value:''; 
    		var comp_current_select_Id =bel_comp[0]? bel_comp[0].id:'';
    		pre_current_select_val = bel_pre[0]?bel_pre[0].value:''; 
    		var pre_current_select_Id = bel_pre[0]?bel_pre[0].id:'';
    		if(bel_modi.length > 1){
    			modi_current_select_val = bel_modi[0]?bel_modi[0].value:''; 
    			var modi_current_select_Id =bel_modi[0]? bel_modi[0].id:'';
    			modifierSign_current_select_val =bel_modi[1]? bel_modi[1].value:''; 
    			var modifierSign_current_select_Id =bel_modi[1]? bel_modi[1].id:'';
    		}
    		else{
    			modi_current_select_val =bel_modi[0]? bel_modi[0].value:''; 
    			var modi_current_select_Id =bel_modi[0]? bel_modi[0].id:'';
    		}
    		if(lbl_Id == 202 || comp_current_select_Id == 202){
    			$('#walkout').removeAttr("disabled");
    		}
    		else{
    			$('#walkout')[0].checked=false;
    			$('#walkout').attr('disabled','disabled');
    		}
    		if(lbl_Id == 302 || modi_current_select_Id == 302){
				$('#M303').removeAttr("disabled");
				$('#M304').removeAttr("disabled");
    		}
    		else{
    			$('#M303').attr('disabled','disabled');
				$('#M304').attr('disabled','disabled');
    		}
    		if($('#walkout').prop("checked") == true){
				walk_value = '-wo'
    		}
    		var new_Label = pre_current_select_val + ' ' + comp_current_select_val + ' ' + (walk_value!= '' ? walk_value : '') + (modi_current_select_val != '' ? modi_current_select_val: '') + modifierSign_current_select_val;
    		var gradelabel = labelValues;
    		var gradeLength = labelValues.split('|').length;
			if(gradeLength > 1) 
				gradelabel = labelValues.split('|')[gradeLength-1];			
			var LabelSplit = gradelabel.split(" + ");
			if(LabelSplit.length > 1) {
				switch(LabelSplit.length)
				{
					case 2: labelValues = notBelowLabel + LabelSplit[0] + ' + ' + new_Label ; break;
					case 3: labelValues = notBelowLabel + LabelSplit[0] + ' + ' + LabelSplit[1] + ' + ' + new_Label ; break;
					case 4: labelValues = notBelowLabel + LabelSplit[0] + ' + ' + LabelSplit[1] + ' + ' + LabelSplit[2] + ' + ' + new_Label ; break;
				}
			}
			else
				labelValues = notBelowLabel + new_Label;
			$('.label_preview').html(labelValues);
		 });
	

		 $('.Current_vector_details #Btn_Save_vector_properties').unbind(touchClickEvent);
    	 $('.Current_vector_details #Btn_Save_vector_properties').bind(touchClickEvent, function () {
    	 	valid = true;
    	 	if(!editLabelChanged){
    			var nsVal, BasementLabel = '', total_value = 0, rem_comp = [];
    			var lokValue = MVP_Lookup.filter(function (c) {return c[grade_col] && c.AdditionalScreen && (c.AdditionalScreen == "BGAreaEntry" || (c.AdditionalScreen.split("/")[0] == "BGAreaEntry"))})
    			lokValue.forEach(function (lval){
    				if(lval.tbl_element != 1202 || lval.tbl_element != '1202')
						rem_comp.push(lval.field_1);
    			});
				var prefixVal, prefixId, compVal, compId, modiferVal, modifierSignVal, walkval = '';
				items[0].Value = $('.label_preview').html();
    			items[0].Newlabel = $('.label_preview').html();
    			BasementLabel = $('.label_preview').html();
    			if(BasementLabel.split("|").length > 1)
    				BasementLabel = BasementLabel.split("|")[BasementLabel.split("|").length -1];
				prefixVal = $("input[name='Prefix']:checked").val();
				compVal = $("input[name='Component']:checked").val();
				modiferVal = $("input[name='modifer']:checked").val() ? $("input[name='modifer']:checked").val() : '' ;
				modifierSignVal = $("input[name='modifierSign']:checked").val() ? $("input[name='modifierSign']:checked").val() : '' ;
				if(!prefixVal || !compVal){
					ValidateLabelsString = "Please select label from both Prefix and Component."
					$('.ValidateLabels').html(ValidateLabelsString);
					$('.ValidateLabels').show();
					return;
				}
				prefixId = $("input[name='Prefix']:checked")[0].id;
				compId = $("input[name='Component']:checked")[0].id;
				if(compId == 202 ){
					if($('#walkout').prop("checked") == true)
						walkval = 'Y';
				}
				BasementFunctionalityDetail[BFIndex] = _.clone({comp: compId, prefix: prefixId, walkout: walkval, modifier: modiferVal, modifierSign: modifierSignVal});
				var BasementLabelSplit = BasementLabel.split(" + ");
				var BasementLabelSplitLength = BasementLabelSplit.length, k=0, PrefixSplit = [], compSplit = [];
				for(k =0; k<BasementLabelSplitLength ; k++){
					var FinSplit = BasementLabelSplit[k].split("Fin");
					PrefixSplit[k] = FinSplit[0].match('[0-9]/[0-9]');
					switch(PrefixSplit[k][0]){
						case "1/4": total_value = total_value + 1/4;break;
						case "1/2": total_value = total_value + 1/2;break;
						case "3/4": total_value = total_value + 3/4;break;
					}
					compSplit[k] = FinSplit[0].split(PrefixSplit[k][0])[1];
					compSplit[k] = compSplit[k].replace(/ /g,'');
					if(compSplit[k].indexOf("B-wo")>-1)
						compSplit[k] = compSplit[k].split("-wo")[0];
				}
				if(total_value > 1){
					ValidateLabelsString = "Total prefix sum greater than 1.Please choose correct prefix value";
					$('.ValidateLabels').html(ValidateLabelsString);
					$('.ValidateLabels').show();
					return;
				}
				else if(total_value < 1){
					var remaingvalue = 1- total_value;
					var rem_prefix = '';
					switch(remaingvalue){
						case .25:rem_prefix = '1/4';break;
						case .5:rem_prefix = '1/2';break;
						case .75: rem_prefix = '3/4';break;
					}
					for(k =0; k<BasementLabelSplitLength ; k++){
						var r_index = _.indexOf(rem_comp,rem_comp.filter(function(t){return t == compSplit[k]})[0])
						rem_comp.splice(r_index,1);
					}
					if(rem_comp.length <= 0){
						ValidateLabelsString = "No New Label Available.Please choose correct prefix value";
						$('.ValidateLabels').html(ValidateLabelsString);
						$('.ValidateLabels').show();
						return;
					}
					BFIndex = BFIndex + 1;
					var new_comp = rem_comp.shift();
					var new_Labels = BasementLabel + ' + ' + rem_prefix + ' ' + new_comp;	
					labelValues = notBelowLabel + new_Labels;
					$('.label_preview').html(labelValues);
					$("input[name='Prefix'][value='"+rem_prefix+"']")[0].checked = true;
					$("input[name='Component'][value='"+new_comp+"']")[0].checked = true;
					if($("input[name='modifer']:checked").length>0)
						$("input[name='modifer']:checked")[0].checked = false;
					if($('input[name=modifierSign]:checked').length>0)
						$('input[name=modifierSign]:checked')[0].checked = false;
					if($('#walkout')[0].checked){
						$('#walkout')[0].checked = false;
					}
					$('#'+compId+'').attr('disabled','disabled')
					if(new_comp =='B')
						$('#walkout').removeAttr('disabled','disabled');
					return;
				}
				else{
					var BFDCount = 0;
					items[0].labelDetails.details[1] = ( _.clone({ext_feat_code1: 0, comp1: 0, prefix1: 0, constr1: 0, ns1: 0, walkout1: 0, ext_cover1: '', modifier1: 0, modifierSign1: 0, ext_feat_code2: 0, comp2: 0, prefix2: 0, constr2: 0, ns2: 0, walkout2: 0, ext_cover2: '', modifier2: 0, modifierSign2: 0, ext_feat_code3: '', comp3: 0, prefix3: 0, constr3: 0, ns3: 0, walkout3: 0, ext_cover3: '', modifier3: 0, modifierSign3: 0, ext_feat_code4: '', comp4: 0, prefix4: 0, constr4: 0, ns4: 0, walkout4: 0, ext_cover4: '', modifier4: 0, modifierSign4: 0 ,FreeForm1: ''}));
					items[0].labelDetails.labelValue = $('.label_preview').html()
					BasementFunctionalityDetail.forEach(function(BFD){
						BFDCount = BFDCount + 1;
						if(BFDCount == 1){
							items[0].labelDetails.details[1].comp1 = BFD.comp;
							items[0].labelDetails.details[1].prefix1 = BFD.prefix;
							items[0].labelDetails.details[1].walkout1 = BFD.walkout;
							items[0].labelDetails.details[1].modifier1 = BFD.modifier;
							items[0].labelDetails.details[1].modifierSign1 = BFD.modifierSign;
						}
						else if(BFDCount == 2){
							items[0].labelDetails.details[1].comp2 = BFD.comp;
							items[0].labelDetails.details[1].prefix2 = BFD.prefix;
							items[0].labelDetails.details[1].walkout2 = BFD.walkout;
							items[0].labelDetails.details[1].modifier2 = BFD.modifier;
							items[0].labelDetails.details[1].modifierSign2 = BFD.modifierSign;
						}
						else if(BFDCount == 3){
							items[0].labelDetails.details[1].comp3 = BFD.comp;
							items[0].labelDetails.details[1].prefix3 = BFD.prefix;
							items[0].labelDetails.details[1].walkout3 = BFD.walkout;
							items[0].labelDetails.details[1].modifier3 = BFD.modifier;
							items[0].labelDetails.details[1].modifierSign3 = BFD.modifierSign;
						}
						else if(BFDCount == 4){
							items[0].labelDetails.details[1].comp4 = BFD.comp;
							items[0].labelDetails.details[1].prefix4 = BFD.prefix;
							items[0].labelDetails.details[1].walkout4 = BFD.walkout;
							items[0].labelDetails.details[1].modifier4 = BFD.modifier;
							items[0].labelDetails.details[1].modifierSign4 = BFD.modifierSign;
						}
					})
				}
			}
    		var validateLabel = true, i = 1, ValidateArea = false, CheckUnsketch = false;
    		if (valid) {
    			if(type == 'edit')
    				$('#Btn_cancel_vector_properties').removeAttr('disabled')
				if(basecallback) basecallback();
        	}
        	else messageBox('Please check the values.')
    	});
    
    	$('.Current_vector_details #Btn_cancel_vector_properties').unbind(touchClickEvent)
    	$('.Current_vector_details #Btn_cancel_vector_properties').bind(touchClickEvent, function() {
			hideKeyboard();
			$('.CurrentLabel').remove();
			if(type == 'edit')
    			$('#Btn_cancel_vector_properties').removeAttr('disabled')
			$('.Current_vector_details').css('top','25%');
        	$('.Current_vector_details').hide();
        	if (!sketchApp.isScreenLocked) $('.mask').hide();
       		return false;
    	}); 

	}
	
	var deleteVectorInEdit = [];
	var changeVar = 0, labelValueSelect, ImpIdMethod, headers = [], changeBase = 0, checkedTrue = true, checkedArea = '', checkedPerimeter ='', ImpType = '',sketch_Type ='', changeFinandExtcover = false;
	var CurrentLabelValue ='', clval ='', lblCurrentField = '', partialLabelss = '', DrawSketchTrue = false;
	var sketch_type=((editor.currentSketch.parentRow.MainBuildingType)=='D'?'Res':'Comm')
	sketch_Type = sketch_type;
	var grade_col = 'GradeLevel';	
	var prefix_filter_col = 'Prefix';		
	var constr_filter_col = 'ConstrOption';
	var material_filter_col = 'Material';
    lblCurrentField = (items[0] &&  items[0].Value) ? items[0].Value : '';
	if(type == 'edit' && sketchApp.currentVector && sketchApp.currentVector.BasementFunctionality){
		partialLabelss =  items[0] &&  items[0].Value;
		if(partialLabelss.split('|').length > 0)
			partialLabelss = partialLabelss.split('|')[partialLabelss.split('|').length - 1];
		lblCurrentField = lblCurrentField.split(' + ')[0];
	}
    
    var getDescription = function(lbl_data, isField_1){
    	if (isField_1) return (lbl_data? (lbl_data.field_1 && lbl_data.field_1.trim() != '')? lbl_data.field_1: lbl_data.tbl_element_desc: '');
    	return (lbl_data? (lbl_data.tbl_element_desc && lbl_data.tbl_element_desc.trim() != '')? lbl_data.tbl_element_desc: lbl_data.field_1: '');
    }
    
    var html = "", isEditTrue = true;
	var option = ''; 
	headers = [{name: 'Prefix', value: 'prefix', filter: prefix_filter_col, grade_filter: grade_col}, {name: 'Construction', value: 'constr', filter: constr_filter_col, grade_filter: grade_col}, {name: 'Component', value: 'comp'}, {name: 'Exterior Feature', value: 'ext_feat_code'}];
	var lbl_level = [{name: 'Below Grade', value: 'Below_Grade', value1: 1, value2: 'Below'}, {name: 'At Grade', value: 'At_Grade', value1: 2, value2: 'At'}, {name: 'Above Grade', value: 'Above_Grade', value1: 3, value2: 'Above'}];
	html = '<table style="width: 100% ;"><tr style="background: #e4e4e4;height: 22px;"><th></th>';
	headers.forEach(function(hdr) {
    	html += ('<th>' + hdr.name + '</th>');
		hdr.lookup = MVP_Lookup.filter(function(pro){ return pro.tbl_type_code == hdr.name });
    });
           
	$('.Current_vector_details .head').html(head);
	$( '.dynamic_prop div,.outbdiv,.CurrentLabel' ).remove();
	$('.mask').show(); 
	$('.dynamic_prop').html('');	

	let symbolArray = [{ Id: 1, symbol: '▼' }, { Id: 2, symbol: '◄' }, { Id: 3, symbol: '▲' }, { Id: 4, symbol: '►' }, { Id: 5, symbol: '↓' }, { Id: 6, symbol: '←' }, { Id: 7, symbol: '↑' }, { Id: 8, symbol: '→' }, { Id: 9, symbol: '■' }], smhtml = '';
	symbolArray.forEach((x) => {
		smhtml += '<option value="' + x.Id + '" style="font-size: 15px;">'+ x.symbol +'</option>';
	}); 
	$('.dynamic_prop').append('<div class="UnSketched"> <div class="UnSketchedarea" style="height:30px;padding: 0px;"><label style="float: left;width: 85px;"> Draw Sketch</label><label style="float: left"><input class="checkbox1" type="checkbox" checked="checked" style="width: 20px;margin-right:7px;margin-top: -3px;">Yes</label></div><div class="Unarea" style="height:30px;display: none;padding: 0px;padding-left: 15px;"><span class="ValidateArea" style="display: none; font-weight: bold; min-width: 5px; color:Red;">*</span><label style="float: left;"> Area</label><input class="sqft" type="number" style="width: 75px; height:25px; margin-right:7px;margin-top: -3px;"></div><div class="Unperimeter"  style="height:30px;display: none;padding: 0px;padding-left: 15px;"><label style="float: left;"> Perimeter</label><input class="perimeter" type="number" style="width: 75px; height:25px; margin-right:7px;margin-top: -3px;"></div><div class="symbol" style="height: 30px; display: none; padding: 0px 0px 0px 10px;"><label style="float: left;"> Symbol</label><select class="symbol_class" style="font-size: 15px; width: 75px; margin-right: 7px; margin-top: -3px;">' + smhtml + '</select></div></div>');
    
    $('.dynamic_prop').append('<div class="divvectors"></div>');
	
    lbl_level.forEach(function(lbl, index) {	
    	html += ('<tr class="lbl_level" name="' + lbl.value2 + '"><td style="font-weight:500;">' + lbl.name + '</td>');
    	option = '';
    	headers.forEach(function(hdr, hdr_index) {
    		html += '<td><select  style="width:110px;" type="' + hdr.value + '" grade="' + lbl.value + '" class="segmentLbl ' + hdr.value + '">';
    		html += '<option value="0"></option>';
    		if (hdr_index >= 0){
    			if(hdr.name == 'Construction' && lbl.value1 != 1)
    				hdr.lookup.forEach(function(lk) {
	    				html += '<option value="' + lk.tbl_element + '" value1="' + getDescription(lk, true) + '">' + getDescription(lk) + '</option>';
	    			});
    			else
					hdr.lookup.filter(function(lk){ return (lk[grade_col] && lk[grade_col].indexOf(lbl.value2) > -1) ; }).forEach(function(lk) {
	    				if((lk.tbl_element != 1202 || lk.tbl_element != '1202') && !(hdr.name == 'Component' && lk.tbl_element == '416' ) && !(hdr.name == 'Exterior Feature' && lk.tbl_element == '204'))
	    					html += '<option value="' + lk.tbl_element + '" value1="' + getDescription(lk, true) + '">' + getDescription(lk) + '</option>';
	    			});
	    	}
    		html += '</select></td>';
    	});

    	var materialLookup = MVP_sketch_codes.filter(function(t){return t.tbl_type_code== "Material"});
    	html += '<tr class="extraval" style="display: none;"><td></td><td><input style="height: 20px; width: 96px;" onkeypress="if(this.value.length==20) return false;" placeholder="N-" type="number" class="' + lbl.value + ' ns" readonly="readonly"></td><td style="float: right; font-style: italic;">Walkout:</td><td><select grade="' + lbl.value + '" class="' + lbl.value + ' walkout" style="height: 24px; width: 100px;" disabled><option value=" "></option><option value="Y">Y</option><option value="N">N</option></select></td></tr>';
    	
    	html += '<tr class="add_vals"><td style="float: right; font-style: italic;">Default Exterior Cover:</td><td><select class="' + lbl.value + ' ext_cover" style="width: 110px;">';
    	html += '<option value="0" value1="None">None</option>';
    	MVP_Default_Exterior_Cover.filter(function(pro){ return pro.tbl_type_code == 'extcover' && pro.tbl_element_desc != "None" }).forEach(function(extCover){
    		html += '<option value="' + extCover.tbl_element + '" value1="' + getDescription(extCover, true) + '">' + getDescription(extCover) + '</option>';
    	}); 	
    	
    	html += '</select></td><td style="padding-left: 10px; font-style: italic;text-align: right;" >Modifier:</td><td><select style="width: 65px; float: inherit;" class="' + lbl.value + ' modifier"><option value=""></option>'; 
    	MVP_sketch_codes.filter(function(pro){ return (pro.tbl_type_code == 'Modifier' && getDescription(pro) && ( pro[grade_col] && pro[grade_col].indexOf(lbl.value2) > -1) ); }).forEach(function(mod){
    		if (getDescription(mod) != '+' && getDescription(mod) != '-')
    			html += '<option value="' + getDescription(mod) + '" value1="' + getDescription(mod, true) + '">' + getDescription(mod) + '</option>';
    	});
    	
    	html += '</select><select style="width: 44px;margin-left: 2px;float: inherit;" class="' + lbl.value + ' modifierSign" disabled="disabled"><option value=""></option><option value="+">+</option><option value="-">-</option></select></td>';
    	
    	html += '</tr><tr><td style="float:right;padding-top: 8px;" >Material:</td><td colspan="4" style="font-style: italic;padding-top: 5px;">';
    	var matAdd = 1;
    	for(i = 0; i < 4;i++  ){
    		if(lbl.value1 != 1)
    			html += '<span style="height: 20px; width: 104px; padding: 4px 2px;font-size: 14px;"><label style="float: left"><input class="ext_Materials" type="checkbox" value = "'+materialLookup[i].field_1+'" value1= "'+matAdd+'" name="' + lbl.value + 'ext_Materials" style="width: 15px;margin-right: 6px;margin-top: -3px;margin-bottom: -3px;">'+materialLookup[i].tbl_element_desc+'</label></span>';
    		else
    			html += '<span style="height: 20px; width: 104px; padding: 4px 2px;font-size: 14px;"><label style="float: left"><input class="ext_Materials" type="checkbox" value = "'+materialLookup[i].field_1+'" value1= "'+matAdd+'" name="' + lbl.value + 'ext_Materials" style="width: 15px;margin-right: 6px;margin-top: -3px;margin-bottom: -3px;" Disabled = "Disabled">'+materialLookup[i].tbl_element_desc+'</label></span>';
    		matAdd = matAdd * 2;
    	}
    		
		html += '</td></tr>'
		html += (index != lbl_level.length -1)? '<tr ><td colspan="5" style="Height:0px;border-bottom: 1.5px solid #b9b9b9;"></td></tr>': '';
    });
    
    $('.divvectors').append(html + '</table>');
    $('.divvectors').append('<span class="ValidateLabel" style="height: 12px; display: none; width:450px; color:Red;margin-left: 100px;">Please choose any label from Prefix, Components or Exterior Feature.</span>');
    $('.Current_vector_details').show();   
 	$('.Current_vector_details').width(690);
 	$('.Current_vector_details').css('top','15%');
 	$('.divvectors').width(655);
 	$('.divvectors').css('max-height','370px')
 	$('.divvectors').height(370);
	$('.lbl_level td').css('padding','3px 0px');
    $('.divvectors').css('padding-bottom','0px');
    $('.dynamic_prop').css('padding-bottom','0px');
    $('.skNote').show();
    $('.skNote').html('<span><b>Sketch Label Preview:</b>&nbsp;</span><span class="label_preview">' + (lblCurrentField || '') + '</span>'); 

	let mds = null, dvs = null;
    if (sketchSettings.MVPDefaultSettings) {
    	try {
             mds = JSON.parse(sketchSettings.MVPDefaultSettings);
             dvs = mds.DefaultValueSymbol? mds.DefaultValueSymbol: null;
             dvs =  (dvs > 0 && dvs < 10)? dvs: null;                
    	}
    	catch(ex) { }
    }
	
	if (type == 'edit' && sketchApp.currentVector && sketchApp.currentVector.BasementFunctionality){
    	$('.dynamic_prop').after('<span class="CurrentLabel" style="display: none; max-width:550px; color:#a01e1e;margin-left: 4px;font-size: 14px; margin-left: 25px;">'+CurrentLabelValue+'</span>');
    	var extlabel = "Existing Label : ";
    	clval = items[0].Value;
		CurrentLabelValue = extlabel + items[0].Value;
		$('.CurrentLabel').html(CurrentLabelValue);
		$('.CurrentLabel').show();
	}
	
	if (type == 'edit') {
		var vectorscopy= JSON.parse(JSON.stringify(sketchApp.currentVector.labelFields));
		var vUid = sketchApp.currentVector.uid;
		var eVuid = sketchApp.currentVector.sketch.uid;
		var mvDat = JSON.parse(JSON.stringify(sketchApp.currentVector.mvpData))
		deleteVectorInEdit.push({uid : vUid.toString(), labelFields: vectorscopy, islabelEditedTrue: true, eEid: eVuid.toString(), mvpData: mvDat})
		isEditTrue = false;
	}
	else if (mds && dvs) {
		$('.symbol_class').val(dvs);		
	}
	
	if (type == 'edit' && sketchApp.currentVector.isUnSketchedTrueArea) {
        let ar = sketchApp.currentVector.fixedAreaTrue, pr = sketchApp.currentVector.fixedPerimeterTrue;
        let sy = (sketchApp.currentVector.mvpData? sketchApp.currentVector.mvpData.Symbol: (dvs? dvs: '1'));
		$('.UnSketchedarea').hide(); $('.Unarea, .Unperimeter, .symbol').show();
		$('.sqft').val(ar); $('.perimeter').val(pr); $('.symbol_class').val(sy);
		$('.sqft, .perimeter').css('border', '');
    }
    else if (type == 'edit')
    	$('.UnSketched').hide()
	$('.symbol').hide();
	var isWalkout = false;
 	$('.lbl_level[name=Below] .segmentLbl.ext_feat_code').attr('disabled','true');
 	$('.segmentLbl,.modifier,.modifierSign,.ns,.ext_cover,.ext_Materials').unbind();
    $('.segmentLbl,.modifier,.modifierSign,.ns,.ext_cover,.ext_Materials').change(function(index){
    	$('.ValidateLabel').hide();
    	//$('.At_Grade.modifier').removeAttr('disabled');
    	isEditTrue = true;
    	var lbls = [], codes = [], lbl = '', code = '', that = this, lbl_value = null, extraValues = '', FirstSelectFin = false;
		var tr = $(that).parents('.lbl_level'), thisValue = $(that).val();
		var _type_code = ( $(that).attr('class') ? ( $(that).attr('class').contains("comp")? "Component": ( $(that).attr('class').contains('ext_feat_code') ? "Exterior Feature" : null ) ): null );
		if(thisValue == '209' ||thisValue == '207' || thisValue == '208')
			$('.At_Grade.modifier').attr('disabled','disabled');
			if($(that).attr('class').search('modifier')>-1){
				if(thisValue == 'Fin' || thisValue == 'UF' ||thisValue ==""){
					FirstSelectFin = true;
					var next_select = $(this).next('select');
    				if ($(this).val() == 'Fin') $(next_select).removeAttr('disabled');
    				else {
    					$(next_select).val('');
    					$(next_select).attr('disabled', 'disabled');
    				}
				}
			}
			var extra_tr = $(tr).next('tr'), extraVal = false;
			var lookupVal = {};
			var labelLevel=$(tr).attr('name');
    		var tttype=$(that).attr('type');
			//if(thisValue != '212')
			lookupVal = MVP_Lookup.filter(function(lkup){ return lkup.tbl_element == thisValue; });
			if(lookupVal && lookupVal.length > 1 && _type_code)
                lookupVal = lookupVal.filter(function(lkop) {return lkop.tbl_type_code == _type_code } )[0];
            else
                lookupVal = lookupVal[0];
			//else
				//lookupVal = Proval_Lookup.filter(function(lkup){ return lkup.tbl_element == thisValue && lkup.CommGradeLevel == labelLevel ; })[0];
    		lookupVal = lookupVal? lookupVal: {};
    		var chanvalue = thisValue;
    		var sketch_type = ((sketchApp.currentSketch.parentRow.RorC)=='R'?'Res':'Comm')
		if((chanvalue=="202" || chanvalue==202) && labelLevel=="Below" && changeBase==0){
			var rd=$('.extraval')[0];
			$(rd).attr('style','inline');
			$('.Below_Grade.walkout').removeAttr('disabled')
		}
		if((chanvalue == "15" || chanvalue == "10") && changeBase==0 ){
			var nsIndex = 0;
			if(labelLevel=="Below")
				nsIndex = 0
			else if(labelLevel=="At")
				nsIndex = 1;
			else if(labelLevel == "Above")
				nsIndex = 2;
			var kr=$('.extraval')[nsIndex];
			$(kr).show();
			var rx=$('.ns')[nsIndex];
			$(rx).removeAttr('readonly');

		}
		changeFinandExtcover = false; FirstSelectFin = false;
		changeBase=1;
    	if (!isWalkout) {
	    		//if (lookupVal && lookupVal[material_filter_col] && lookupVal[material_filter_col] == 'Y') 
					//$('.material', $(extra_tr).next('tr')).removeAttr('disabled');
				//else $('.material', $(extra_tr).next('tr')).attr('disabled', 'disabled');
	    	if ($(that).hasClass('prefix')) {
	    		if (thisValue == '10' ||thisValue == '15' ) {
	    			$(extra_tr).show();
	    			$('.ns', extra_tr).val('N-')
	    			$('.ns', extra_tr).removeAttr('readonly');
				}
	    		else {
	    			if ($('.walkout', extra_tr).attr('disabled')) $(extra_tr).hide();
	    			$('.ns', extra_tr).attr('readonly', 'readonly');
				}
	    	}
	    	if ($(that).hasClass('comp')) {
	    		if (thisValue == '202') { 
	    			$(extra_tr).show(); 
	    			$('.walkout', extra_tr).removeAttr('disabled'); 
				}
	    		else {
	    			$('.walkout', extra_tr).attr('disabled', '');
	    			$('.walkout', extra_tr).val('');    		
	    			if ($('.walkout', extra_tr).attr('disabled')) $(extra_tr).hide();
				}
				var html = '<option value="0"></option>';
				if (thisValue == '0') {
					$('.prefix', tr).html(html);
					MVP_Lookup.filter(function(lk){ return (lk[grade_col] && lk.tbl_type_code =='Prefix'  && lk[grade_col].indexOf($(tr).attr('name')) > -1); }).forEach(function(lk) {
	    				html += '<option value="' + lk.tbl_element + '" value1="' + getDescription(lk, true) + '">' + getDescription(lk) + '</option>';
	    			});
	    			$('.prefix', tr).html(html);
	    			var html = '<option value="0"></option>';
					$('.constr', tr).html(html);
					MVP_Lookup.filter(function(lk){ return (lk[grade_col] && lk.tbl_type_code =='Construction'  && lk[grade_col].indexOf($(tr).attr('name')) > -1); }).forEach(function(lk) {
	    				html += '<option value="' + lk.tbl_element + '" value1="' + getDescription(lk, true) + '">' + getDescription(lk) + '</option>';
	    			});
	    			$('.constr', tr).html(html);
				}
				else {
					var array = [0,1];
					array.forEach(function(idx){
						var hdr = headers[idx];
						var currentLookupFilValue = $('.' + hdr.value, tr).val();
						var currentlooFilValTrue = false;
						//var filter_filed = (idx == 0) ? impGrade_colprefix_filter_col : impConstr_filter_col ;
						lookupVal = MVP_Lookup.filter(function(lkup){ return lkup.tbl_element == thisValue && ((lkup[hdr.grade_filter]  && lkup[hdr.grade_filter].indexOf($(tr).attr('name')) > -1)) ; });
						if(lookupVal && lookupVal.length > 1 && _type_code)
                            lookupVal = lookupVal.filter(function(lkop) {return lkop.tbl_type_code == _type_code } )[0];
						else
							lookupVal = lookupVal[0];
						lookupVal = lookupVal? lookupVal: {};
						var valid_fields = lookupVal[hdr.filter] ? lookupVal[hdr.filter].split(',') : [];
						hdr.lookup.filter(function(lkp){ return (valid_fields.indexOf(lkp.tbl_element_desc) > -1); }).forEach(function(lkup){
							if(lkup.tbl_element == currentLookupFilValue)
								currentlooFilValTrue = true;
							html += '<option value="' + lkup.tbl_element +'" value1="' + getDescription(lkup, true) + '">' + getDescription(lkup) + '</option>';
						});
						$('.' + hdr.value, tr).html(html);
						if(currentlooFilValTrue)
							$('.' + hdr.value, tr).val(currentLookupFilValue);
						html = '<option value="0"></option>';
					});
				}
	    	}
    	}
    	$('.lbl_level').each(function(index, lvl){
    		var ext_feature_selected = false;
    		var mValue=$('.add_vals').eq(index).find('.modifier').val();
    		var modSignValue=$('.add_vals').eq(index).find('.modifierSign').val();
    		var ext_covers='';
    		ext_covers = $('.add_vals').eq(index).find('.ext_cover').val();
    		lbl = ''; code = ''; lbl_value = null;
    		extraValues=$('.extraval').eq(index).find('.ns').val();
    		$('select', lvl).each(function(index, select){
    			lbl_value = $('option[value="'+ $(select).val() +'"]', select).attr('value1');
    			if(index == '3' && lbl_value)
    				ext_feature_selected = true;
    			if((lbl_value == 'N s' || lbl_value == 'N c') && index == 0 ){
    				if(extraValues && extraValues != '' && lbl_value == 'N s')
    					lbl_value = extraValues + ' s';
    					else if(extraValues && extraValues != '' && lbl_value == 'N c')
    					lbl_value = extraValues + ' c';
    			}
    			lbl += lbl_value? (lbl_value + ' '): '';
    			if ($(select).hasClass('comp') && $(select).val() == '202') {
    				if ($('.walkout[grade="' + $(select).attr('grade') + '"]').val() == 'Y')
    						lbl += '-wo ';
				}
    			code += $(select).val() + ',';
    		});
    		var materialArray = [];
 			if(index == '1'){
    			$.each($("input[name='At_Gradeext_Materials']:checked"), function(){
					materialArray.push($(this). val());
				});
    		}
    		else if(index == '2'){
    			$.each($("input[name='Above_Gradeext_Materials']:checked"), function(){
					materialArray.push($(this). val());
				});
    				
    		}
    		var fselect = 0;
    		if(ext_feature_selected && materialArray.length > 0){
    			materialArray.forEach(function(matr){
    				matr = matr.split("-")[1];
    				lbl += (fselect == 0 ? "-"+ matr : matr);
    				fselect++;
    			});
    			lbl =lbl + ' ';
    		}
    		else if(materialArray.length > 0){
    			materialArray.forEach(function(matr){
    				matr = matr.split("-")[1];
    				lbl += (fselect == 0 ? "155-"+ matr : matr);
    				fselect++;
    			});
    			lbl =lbl + ' ';
    		}
    		if(ext_covers && ext_covers != '' && ext_covers != '0' && ext_covers != '99' && !(sketchSettings && sketchSettings.DoNotDisplayExteriorCover == '1') )
    		    lbl = lbl + ext_covers + ' ';
    		if ((lbl && lbl != '') ||( mValue && mValue != '')){
    			if(mValue && mValue != '' || modSignValue && modSignValue !='')
    				 lbl=lbl+(mValue ? mValue: '')+(modSignValue ? modSignValue: ''); 
    			 lbls.push(lbl);
    		}
    		if (code && code != '') codes.push(code.substring(0, code.length - 1));
    	});
    	$('.label_preview').html(lbls.reverse().join('|'));
    });
	
	$('.walkout').unbind();
    $('.walkout').bind('change', function() {    	
    	isWalkout = true;
		$(this).parents('.extraval').prev().find('.comp').trigger('change');
		isWalkout = false;
    })
    $('.ns').keyup(function(){
        $('.ns').change();
    });
    
    $("input[type='checkbox'].checkbox1").change(function () {
    	if ($('.checkbox1').prop("checked") == "checked") {
    		$('.checkbox1').prop('checked', false);
			$('.Unarea').show();
			$('.Unperimeter').show();
			//$('.symbol').show();
			$('.sqft').css('border', '');
			$('.perimeter').css('border', '');
		}
		else {
			$('.checkbox1').prop('checked', 'checked');	
			$('.ValidateArea').hide();
			$('.Unarea').hide();
			$('.Unperimeter').hide();
			$('.symbol').hide();
			//$('.symbol_class').val('0');
			$('.perimeter').val("");
			$('.sqft').val("");
		}
    });
    
    $('.sqft').on('change keyup', function (t) {
		$('.ValidateArea').hide();
		$('.sqft').css('border', '');
		var sqfttt = $('.sqft').val();
		if (sqfttt && sqfttt.length > 10) {
			sqfttt = sqfttt.slice(0,-1);
			$('.sqft').val(sqfttt);
		}
	});
	
	$('.perimeter').on('change keyup', function (t) {
		var peri = $('.perimeter').val();
		$('.perimeter').css('border', '');
		if (peri && peri.length > 10) {
			peri = peri.slice(0,-1);
			$('.perimeter').val(peri);
		}
	});
        
    $('.Current_vector_details #Btn_Save_vector_properties').unbind(touchClickEvent);
    $('.Current_vector_details #Btn_Save_vector_properties').bind(touchClickEvent, function () {
    	if ($("#Btn_Save_vector_properties").is(":disabled")) return false;
		if(isEditTrue){
			var valid = true, nsVal;
    		var detailObj = {ext_feat_code1: 0, comp1: 0, prefix1: 0, constr1: 0, ns1: 0, walkout1: 0, ext_cover1: '', modifier1: 0, modifierSign1: 0, ext_feat_code2: 0, comp2: 0, prefix2: 0, constr2: 0, ns2: 0, walkout2: 0, ext_cover2: '', modifier2: 0, modifierSign2: 0, ext_feat_code3: '', comp3: 0, prefix3: 0, constr3: 0, ns3: 0, walkout3: 0, ext_cover3: '', modifier3: 0, modifierSign3: 0, ext_feat_code4: '', comp4: 0, prefix4: 0, constr4: 0, ns4: 0, walkout4: 0, ext_cover4: '', modifier4: 0, modifierSign4: 0 ,FreeForm1: ''};
    		items[0].Value = $('.label_preview').html();
    		items[0].Newlabel = $('.label_preview').html();
    		if ((!items[0].labelDetails) || (type == 'edit' && sketchApp.currentVector.BasementFunctionality)) 
    			items[0].labelDetails = { 
    				details: {
    					1: _.clone(detailObj),
						2: _.clone(detailObj),
						3: _.clone(detailObj)
			 		},
    				labelValue: $('.label_preview').html()
			};
    		$('.lbl_level').each(function(lvl_idx, lvl){
    			$('select', lvl).each(function(lkp_idx, select){
    				items[0].labelDetails.details[lvl_idx + 1][$(select).attr('type') + (1)] = $(select).val();
    			});
    			items[0].labelDetails.details[lvl_idx + 1]['ext_cover' + (1)] = $('.ext_cover', $(lvl).next().next()).val()!= '99' ?$('.ext_cover', $(lvl).next().next()).val() : '';
    			items[0].labelDetails.details[lvl_idx + 1]['modifier' + (1)] = $('.modifier', $(lvl).next().next()).val();
    			items[0].labelDetails.details[lvl_idx + 1]['modifierSign' + (1)] = $('.modifierSign', $(lvl).next().next()).val();
    			if ($('.prefix', lvl).val() == '10' || $('.prefix', lvl).val() == '15') {
	    			nsVal = $('.ns', $(lvl).next()).val();
	    			valid = valid && !isNaN(nsVal);
    				items[0].labelDetails.details[lvl_idx + 1]['ns' + (1)] = (isNaN(nsVal)|| nsVal == "")? 0: parseInt(nsVal);
				}
    			if ($('.comp', lvl).val() == '202')
    				items[0].labelDetails.details[lvl_idx + 1]['walkout'  + (1)] = $('.walkout', $(lvl).next()).val();
    			var materialArray = [];
 				if(lvl_idx == '1'){
    				$.each($("input[name='At_Gradeext_Materials']:checked"), function(){
						materialArray.push($(this).attr('value1'));
					});
    			}
    			else if(lvl_idx == '2'){
    				$.each($("input[name='Above_Gradeext_Materials']:checked"), function(){
						materialArray.push($(this).attr('value1'));
					});
    			}
    			
    			var exter_feat_pre = items[0].labelDetails.details[lvl_idx + 1].ext_feat_code1;
    			if(materialArray.length > 0){
    				var matSum = 0;
    				materialArray.forEach(function(matr){
    					matSum = matSum + parseInt(matr);
    				});
    				var ext_de = items[0].labelDetails.details[lvl_idx + 1].ext_feat_code1;
    				if((ext_de != 0 && ext_de != '0')){
    					ext_de = matSum + ext_de;
    					items[0].labelDetails.details[lvl_idx + 1].ext_feat_code1 = ext_de;
    				}
    				else{
    					ext_de = matSum + '155';
    					items[0].labelDetails.details[lvl_idx + 1].ext_feat_code1 = ext_de;
    				}
    			}
    		});

    		var validateLabel = false, i = 1, ValidateArea = false, CheckUnsketch = false;
    		for(i=1; i<=3;i++){
    			var Details = items[0].labelDetails.details[i];
    			if((Details['prefix' + 1] != '0' &&  Details['prefix' + 1] != 0 && Details['prefix' + 1] != '' && Details['prefix' + 1] != ' ') || (Details['comp' + 1] != '0' && Details['comp' + 1] != 0 && Details['comp' + 1] != '' && Details['comp' + 1] != ' ') || (Details['ext_feat_code' + 1] != '0' && Details['ext_feat_code' + 1] != 0 && Details['ext_feat_code' + 1] != '' && Details['ext_feat_code' + 1] != ' ')){
    				validateLabel = true;
    				break;
    			}
    		}
    		if(!validateLabel){
				$('.ValidateLabel').show();
				return false;
			}
			var labelDatailsFeat = items[0].labelDetails.details;
			var extfeatAlone = false, MapExteriorFeature = false, parentTypeArrayss = [];
			$.each(labelDatailsFeat,function(det){
				var lbfeat = labelDatailsFeat[det];
				if((lbfeat.comp1 != 0 && lbfeat.comp1 != '0') || (lbfeat.prefix1 != 0 && lbfeat.prefix1 != '0') || (lbfeat.constr1 != 0 && lbfeat.constr1 != '0'))  //|| (lbfeat.ext_cover1 != '' && lbfeat.ext_cover1 !='0') || (lbfeat.modifier1 != '' && lbfeat.modifier1 != '0')
					extfeatAlone = true;
			});
			if(!extfeatAlone) {
				MapExteriorFeature = true;
			}
			var mvpData = {};
			
			let areaSketch = false, sqftvalue = 0, perivalue = 0, symbol = 0, symbol_size = 0;
			if (($( '.UnSketchedarea .checkbox1' ).length > 0 && !$( '.UnSketchedarea .checkbox1' )[0].checked) || (type == 'edit' && sketchApp.currentVector.isUnSketchedTrueArea)) {
				areaSketch = true;
				sqftvalue = $('.sqft').val() ? $('.sqft').val(): 0;
				perivalue = $('.perimeter').val() ? $('.perimeter').val() : 0;
				symbol = $('.symbol_class').val();
				symbol_size = 32;
				if (!sqftvalue || parseFloat(sqftvalue) < 0) {
                	$('.sqft').css('border', '1px solid red');
                	return false;
                }
                else if (parseFloat(perivalue) < 0) {
                    $('.perimeter').css('border', '1px solid red');
                	return false;
                }
			}
			
    		if (valid) {
    			var basefunctionTrue = false;
    			function sktsave(){
    				if(type == 'edit' && !sketchApp.currentVector.newRecord) {
						sketchApp.deletedVectors.push(_.clone(deleteVectorInEdit[0]));
    					sketchApp.currentVector.newRecord = true;
    				}
    				
    				if (type == 'edit' && sketchApp.currentVector.isUnSketchedTrueArea) {
                        sketchApp.currentVector.fixedAreaTrue = sqftvalue;
                        sketchApp.currentVector.fixedPerimeterTrue = perivalue;
                        if (sketchApp.currentVector.jsonSegment)
                            sketchApp.currentVector.jsonSegment.Symbol = symbol;
                    }
    				
    				var Labdetails = items[0].labelDetails.details;
    				var skScale = (type == 'edit' && sketchApp.currentVector && sketchApp.currentVector.mvpData && sketchApp.currentVector.mvpData.Scale) ? sketchApp.currentVector.mvpData.Scale : ( ( sketchApp.currentSketch && sketchApp.currentSketch.boundaryScale )? sketchApp.currentSketch.boundaryScale: '80' );
    				var modi1 = Labdetails[1].modifier1, modiSign1 = Labdetails[1].modifierSign1, modi2 = Labdetails[2].modifier1 ,modiSign2 = Labdetails[2].modifierSign1, modi3 = Labdetails[3].modifier1, modiSign3 = Labdetails[3].modifierSign1, modi12 = Labdetails[1].modifier2, modiSign12 = Labdetails[1].modifierSign2, modi13 = Labdetails[1].modifier3, modiSign13 = Labdetails[1].modifierSign3, modi14 = Labdetails[1].modifier4, modiSign14 = Labdetails[1].modifierSign4;
    				function modifierCal(mod, modsign){
    					if(modsign == '+'){
							if(mod == 'Fin')
    							return 2302;
    						else if(mod == 'UF')
    							return 2301;
    					}
    					else if(modsign == '-'){
							if(mod == 'Fin')
    							return 1302;
    						else if(mod == 'UF')
    							return 1301;
    					}
    					else if(mod == 'Fin')
    						return 302;
    					else if(mod == 'UF')
    						return 301;
    					else
    						return 0;
    				}
    				modi1 = modifierCal(modi1, modiSign1);
    				modi2 = modifierCal(modi2, modiSign2);
    				modi3 = modifierCal(modi3, modiSign3);
					modi12 = modifierCal(modi12, modiSign12);
					modi13 = modifierCal(modi13, modiSign13);
					modi14 = modifierCal(modi14, modiSign14);
    				var compp1 = Labdetails[1].comp1, wal1 = Labdetails[1].walkout1, compp2 = Labdetails[1].comp2, wal2 = Labdetails[1].walkout2, compp3 = Labdetails[1].comp3, wal3 = Labdetails[1].walkout3, compp4 = Labdetails[1].comp4, wal4 = Labdetails[1].walkout4;
    				if(compp1 == 202 && wal1 == 'Y')
    					compp1 = 1202;	
    				else if(compp2 == 202 && wal2 == 'Y')
    					compp2 = 1202;
   					else if(compp3 == 202 && wal3 == 'Y')
    					compp3 = 1202;
    				else if(compp4 == 202 && wal4 == 'Y')
    					compp4 = 1202;	
    				mvpData = {Sketch_Pointer: '', Improvement_ID: '', Level_1_Default_Exterior_Cover : Labdetails[1].ext_cover1, Level_1_Construction_Code: Labdetails[1].constr1, Level_1_N: Labdetails[1].ns1, Level_1_Prefix_For_Part_1: Labdetails[1].prefix1, Level_1_Extra_Feature_Code: Labdetails[1].ext_feat_code1, Level_1_Component_Code_For_Part_1: compp1, Level_1_Modifier_Code_For_Part_1: modi1, Level_1_Prefix_For_Part_2: Labdetails[1].prefix2, Level_1_Component_Code_For_Part_2: compp2, Level_1_Modifier_Code_For_Part_2: modi12, Level_1_Prefix_For_Part_3: Labdetails[1].prefix3, Level_1_Component_Code_For_Part_3: compp3, Level_1_Modifier_Code_For_Part_3: modi13, Level_1_Prefix_For_Part_4: Labdetails[1].prefix4, Level_1_Component_Code_For_Part_4: compp4, Level_1_Modifier_Code_For_Part_4: modi14, Level_1_FreeForm_Label_Code: Labdetails[1].FreeForm1, Level_2_Default_Exterior_Cover: Labdetails[2].ext_cover1, Level_2_Construction_Code: Labdetails[2].constr1, Level_2_N: Labdetails[2].ns1, Level_2_Extra_Feature_Code: Labdetails[2].ext_feat_code1, Level_2_Prefix: Labdetails[2].prefix1, Level_2_Component_Code: Labdetails[2].comp1, Level_2_ModifierCode: modi2, Level_2_FreeForm_Label_Code: Labdetails[2].FreeForm1, Level_3_Default_Exterior_Cover: Labdetails[3].ext_cover1, Level_3_Construction_Code: Labdetails[3].constr1, Level_3_N: Labdetails[3].ns1, Level_3_Extra_Feature_Code: Labdetails[3].ext_feat_code1, Level_3_Prefix: Labdetails[3].prefix1, Level_3_Component_Code: Labdetails[3].comp1, Level_3_Modifier_Code: modi3, Level_3_FreeForm_Label_Code: Labdetails[3].FreeForm1, Outbuilding_Label_Code: '', Reference: 0, Label_Position_X: 0, Label_Position_Y: 0, Area_Label_Position_X: 0, Area_Label_Position_Y: 0, Scale : skScale, Area: 0, Perimeter: 0, Level_1_Fin_Area: 0, Level_1_Unfin_Area: 0, Level_2_Fin_Area: 0, Level_2_Unfin_Area: 0, Level_3_Fin_Area:0, Level_3_Unfin_Area: 0, Symbol: symbol, Symbol_Size: symbol_size, Area_Font_Size: 10, Sketched_Other_Improvement_Indicator: 0}
	    			if (callback) callback(items, {'BasementFunctionality':basefunctionTrue, 'MapExteriorFeature': MapExteriorFeature, 'mvpData': mvpData, 'unSketchedTrue': ( areaSketch ? areaSketch : false), 'Sqft': (sqftvalue ? sqftvalue: '0'), 'perimeter':(perivalue ? perivalue : '0') });
	    			if (!editor.isScreenLocked) $('.mask').hide();
					hideKeyboard();
					$('.CurrentLabel').remove();
					$('.Current_vector_details').css('top','25%');
	        		$('.Current_vector_details').hide();
	        	}
    			if(((items[0].labelDetails.details[1]['prefix1'] == '1' || items[0].labelDetails.details[1]['prefix1'] == 1) || (items[0].labelDetails.details[1]['prefix1'] == '2' || items[0].labelDetails.details[1]['prefix1'] == 2) || (items[0].labelDetails.details[1]['prefix1'] == '3' || items[0].labelDetails.details[1]['prefix1'] == 3)) && (items[0].labelDetails.details[1]['ext_feat_code1'] == 0 ||items[0].labelDetails.details[1]['ext_feat_code1'] == '0') && (items[0].labelDetails.details[1]['ext_cover1'] == 0 || items[0].labelDetails.details[1]['ext_cover1'] == '0') && (items[0].labelDetails.details[1]['constr1'] == 0 || items[0].labelDetails.details[1]['constr1'] == '0')){
    				basefunctionTrue = true;
    				var txtcomp = false, compval = false;
    				if(items[0].labelDetails.details[1]['comp1']!= '0' && items[0].labelDetails.details[1]['comp1'] != 0 && items[0].labelDetails.details[1]['comp1'] != ''){
    					var fiid = items[0].labelDetails.details[1]['comp1'];
    					var plookup = MVP_Lookup.filter(function(x){return x.tbl_element == fiid})[0];
    					if(plookup.AdditionalScreen && (plookup.AdditionalScreen == "BGAreaEntry" || (plookup.AdditionalScreen.split("/")[0] == "BGAreaEntry")))
    						compval = true;
    				}
    				if(!compval){
    					basefunctionTrue = false;
						sktsave();
						return;
    				}
    				else{
    					if(type == 'edit'){
    						if(clval != ($('.label_preview').html()))
    							txtcomp = true;
    					}	
    					BasementFunctionality(txtcomp, function(){
    						sktsave();
    					});
    				}
    			}
    			else{
    				if(type == 'edit'){
    					if(sketchApp.currentVector.BasementFunctionality)
    						sketchApp.currentVector.BasementFunctionality = false;
    				}
    				sktsave();
    			}
        	}
        	else messageBox('Please check the values.')
      	}
		else{
			hideKeyboard();
			$('.CurrentLabel').remove();
			$('.Current_vector_details').css('top','25%');
        	$('.Current_vector_details').hide();
        	if (!sketchApp.isScreenLocked) $('.mask').hide();
        	return false;
		}      
    });
    
    $('.Current_vector_details #Btn_cancel_vector_properties').unbind(touchClickEvent)
    $('.Current_vector_details #Btn_cancel_vector_properties').bind(touchClickEvent, function() {
		hideKeyboard();
		$('.CurrentLabel').remove();
		$('.Current_vector_details').css('top','25%');
        $('.Current_vector_details').hide();
        $('#Btn_Save_vector_properties').removeAttr('disabled');
        if (!sketchApp.isScreenLocked) $('.mask').hide();
        return false;
    });
    
    
    if (type == 'edit') {
	    [1,2,3].forEach(function(i) {
	    	if (!items[0].labelDetails.details) return;
	    	var element = items[0].labelDetails.details[i];
	    	var extraVal = false;
	    	if(element && sketchApp.currentVector && sketchApp.currentVector.BasementFunctionality && i == 1){
	    			var com = items[0].labelDetails.details[i].comp1;
	    			if(com > 1000)
						com = com % 1000;
	    			var pre = items[0].labelDetails.details[i].prefix1;
	    			var modi = items[0].labelDetails.details[i].modifier1;
	    			var msig = items[0].labelDetails.details[i].modifierSign1;
	    			var walkout = items[0].labelDetails.details[i].walkout1;
	    			var className = '.' + lbl_level.filter(function(l){ return l.value1 == i; })[0].value2;
	    			$(className + '_Grade.walkout' + '.ns').attr('readonly', 'readonly');
					
					$('.' + 'comp', $('.lbl_level').eq(i-1)).val(com);
					if (com && com.toString() == '202') {
						$('.segmentLbl').eq(((i - 1) * 4) + 2).trigger('change');
						$(className + '_Grade.walkout').removeAttr('readonly');
						$(className + '_Grade.walkout').val(walkout);
						extraVal = true;
					}
					else $(className + '.walkout').attr('readonly', 'readonly');
					$('.' + 'prefix', $('.lbl_level').eq(i-1)).val(pre);
					$(className + '_Grade.' + 'modifier').val(modi);
					$(className + '_Grade.' + 'modifierSign').val(msig);
	    	}
	    	else if (element){
				$.each(element, function(type, value) {
					if(type == 'comp1' || type == 'ext_cover1' || type == 'ext_feat_code1' || type == 'modifierSign1' || type == 'ns1' || type == 'prefix1' || type == 'modifier1' || type == 'walkout1' || type=='constr1'){
						type = type.split('1')[0];
						 if(type == 'comp' && value >1000)
                            value = value % 1000;
						if(type == 'ext_feat_code' && i != 1){
							var cNames = (i == 2)? 'At_Gradeext_Materials' : 'Above_Gradeext_Materials';
							var ext_split_code = parseInt(value);
							var ext_temp = ext_split_code % 1000;
							$('.' + type, $('.lbl_level').eq(i-1)).val(ext_temp);
							var mtValues = parseInt((ext_split_code/1000));
							if(mtValues != '0' || mtValues != 0){
								mtValues = parseInt(mtValues);
								var baseValue = parseInt(parseInt(mtValues,10).toString(2));
								var j = 0;
								while(baseValue > 0){
									j = j + 1;
									var rem = baseValue % 10;
									if(rem > 0){
										switch(j){
											case 1: $(".ext_Materials[name='" + cNames + "'][value1='1']").prop('checked','checked'); break;
											case 2: $(".ext_Materials[name='" + cNames + "'][value1='2']").prop('checked','checked'); break;
											case 3: $(".ext_Materials[name='" + cNames + "'][value1='4']").prop('checked','checked'); break;
											case 4: $(".ext_Materials[name='" + cNames + "'][value1='8']").prop('checked','checked'); break;
										}
									}
									baseValue = parseInt(baseValue / 10);
								}
							}
						}
						else
							$('.' + type, $('.lbl_level').eq(i-1)).val(value);
						var className = '.' + lbl_level.filter(function(l){ return l.value1 == i; })[0].value2;
						if (type == 'prefix') {
								if (value && (value.toString() == '10') || value.toString() == '15') { 
									$(className + '_Grade' + '.ns').removeAttr('readonly');
									$(className + '.' + type).val(value);
									extraVal = true;
								}
								else{
									$(className + '_Grade.walkout' + '.ns').attr('readonly', 'readonly');
									$(className + '.' + type).val(value);
								}
						}
						if(type=='constr'){
							if(value>0)
								$(className + '.' + type).val(value);
						}
						if (type == 'comp') {
							if(value>0){
	    						$('.segmentLbl').eq(((i - 1) * 4) + 2).trigger('change');
	    						if (value && value.toString() == '202') {
									$(className + '_Grade.walkout').removeAttr('readonly');
									$(className + '_Grade.walkout').val(element['walkout1']);
									extraVal = true;
								}
								else $(className + '.walkout').attr('readonly', 'readonly');
							}	    				
						}
						if (type == 'modifier' || (type == 'ext_cover' && value != '99')|| type == 'modifierSign' || (type == 'ns' && value != '0')) $(className + '_Grade.' + type).val(value);
					}
				});
			}
			if (!extraVal) $('.lbl_level').eq(i-1).next('tr').hide();
			else $('.lbl_level').eq(i-1).next('tr').show();
	    });
	    $('.modifier').trigger('change');
	    $('.skNote').html('<span><b>Sketch Label Preview:</b>&nbsp;</span><span class="label_preview">' + (lblCurrentField || '') + '</span>');
    	let impId = sketchApp.currentVector.mvpData.Improvement_ID;
		if (impId && impId != 'D' && impId != 'B' && !impId.contains('{')) {
			$('.Current_vector_details').find('input, button, select').attr('disabled', 'disabled');
			$('#Btn_cancel_vector_properties').removeAttr('disabled');
		}
    }
}

function MVPSketchSave(sketchDataArray, noteData, sketchSaveCallBack, onError) {
	var DBRecordArray = [], AdditionArray = [], deleteVectorArray = [], deleteVectorRowuids = [];
	if (sketchDataArray.length == 0 && noteData.length == 0) { 
		if (sketchSaveCallBack) sketchSaveCallBack(afterSketchSave); return; 
	}
	
	var DBFnames = ['TotalHalfFloorAreaInclAdditions', 'TotalAtticAreaInclAdditions', 'TotalFirstFloorAreaInclAdditions', 'TotalUpperFloorArea', 'BaseAreaTotal', 'BaseArea', 'FinishedLivingArea', 'HalfFloorArea', 'FinishedAtticArea', 'FirstFloorArea', 'UpperFloorArea', 'BasementAreaTotal', 'TotalUpperFloorAreaInclAdditions'];
	var OIFnames = ['OtherImprovementCode', 'Width', 'Length', 'TotalArea', 'CalcTotalArea'];
	
	sketchApp.deletedVectors.filter(function(ve){ return (ve.sketchType != 'outBuilding')}).forEach(function(delvect){
		var skRowuid = null;
		if(delvect.eEid){
			var skts = sketchApp.sketches.filter(function(skda){ return skda.uid == delvect.eEid})[0];
			if(skts)
				skRowuid = skts.uid;
		}
		else
			skRowuid = delvect.sketch.uid;
		var deletedVectSketch = deleteVectorRowuids.filter(function(x){ return x.sketchRowuid == skRowuid})[0];
		if(!deletedVectSketch && skRowuid)
			deleteVectorRowuids.push({sketchRowuid: skRowuid, isSktModified: true});
	});
	
	sketchDataArray.forEach(function(skd){
		var sid = skd.sid;
		sketchApp.sketches.filter(function(skda){ return skda.sid == sid}).forEach(function(skt) {
			
			var ParentRowCard = skt.parentRow, sketch_Type = ParentRowCard.MainBuildingType, OI_Record, nAdditionArray = [];
			var DBTable = (sketch_Type == 'D' ? 'CC_Dwelling': 'CC_Building');
			var AddTable = (sketch_Type == 'D' ? 'CC_Addition' : 'CC_Addition_Building');
			var DBRecord = activeParcel[DBTable].filter(function(rd){return rd.ParentROWUID == ParentRowCard.ROWUID})[0];
			var OI_Record = activeParcel['CC_OtherImprovement'].filter(function(rd){return rd.ParentROWUID == ParentRowCard.ROWUID})[0];
			
			var  _tHalfIA = 0, _taIA = 0, _tfIA = 0, _tuFA = 0, _bat = 0, _ba = 0, _fla = 0, _hfa = 0, _faa = 0, _bfa = 0, _ffa = 0, _ufa = 0, _thfa = 0, _udba = false, label_text, updateOIarray = false, sketchedOI, _bsmtat = 0, _oOSM = false, _tufaIA = 0; 
			
			skt.vectors.filter(function(ve){ return ((ve.sketchType != 'outBuilding') && (ve.wasModified || ve.isModified))}).forEach(function(vect){
				_oOSM = true;
			});
			if(!_oOSM && deleteVectorRowuids.length >0){
				var _dvs = deleteVectorRowuids.filter(function(x){ return x.sketchRowuid == skt.uid})[0];
				if(_dvs)
					_oOSM = true;
			}

			if(_oOSM){
				skt.vectors.filter(function(ve){ return ve.sketchType != 'outBuilding'}).forEach(function(vect) {
					var mvpData = vect.mvpData, _va = vect.isUnSketchedTrueArea? Math.round(vect.fixedAreaTrue): Math.round(vect.area()), _ubat = false;
					var ImpIdval = mvpData.Improvement_ID; 
					if(ImpIdval.split('{').length > 1) {
						if((ImpIdval.split('{')[1].split('#')).length > 1)
							ImpIdval = ImpIdval.split('{')[1].split('#')[1].split('$')[0];
						else
							ImpIdval = ImpIdval.split('{')[1].split('}')[0];
					}
					
					var addnfun = function() {
						_udba = true;
						if( !_ubat ) {
							_bat = Math.round(_bat + _va); _ubat = true;
						}
						
						var imId = mvpData.Improvement_ID;
						if(imId.split('{').length > 1){
							if((imId.split('{')[1].split('#')).length > 1) imId = imId.split('{')[1].split('#')[1].split('$')[0];
							else imId = imId.split('{')[1].split('}')[0];
						}
						var AddRecord = activeParcel[AddTable].filter(function(rd){ return (rd.SegmentID == imId || rd.ROWUID == imId) })[0];
						if(AddRecord) {
							if((vect.wasModified || vect.isModified)) {
								var existsAddition = AdditionArray.filter(function(a) { return a.AdditionRowuid == AddRecord.ROWUID })[0];
								if (existsAddition) existsAddition.BaseArea = existsAddition.BaseArea + _va;
								else AdditionArray.push(_.clone({ AdditionRowuid: AddRecord.ROWUID, AddRecord: AddRecord, AddTable: AddTable, BaseArea: Math.round(_va) }));
							}
							else 
								nAdditionArray.push(_.clone({ AdditionRowuid: AddRecord.ROWUID, BaseArea: Math.round(_va) }));
						}					
					}
					
					let garageFin = false;
					if (vect.labelFields[0].labelDetails && vect.labelFields[0].labelDetails.details) {
						let det2 = vect.labelFields[0].labelDetails.details[2], isaddn = false, det3 = vect.labelFields[0].labelDetails.details[3];
						if (det2.comp1 && det2.comp1 != 0 && det2.comp1 != '0' && det2.comp1 != '') isaddn = isAdditionSketch(det2);
						if (isaddn && (det3.prefix1 && det3.prefix1 != '0') || (det3.comp1 == '201' && (det3['modifier1'] == "Fin" || det3['modifier1'] == "UF"))) garageFin = true;
					}
									
					if((ImpIdval == 'D' || ImpIdval == 'B' || (DBRecord && DBRecord.ROWUID == ImpIdval) || garageFin) && vect.vectorString != '' && vect.vectorString != null && vect.vectorString != ' ' ){
						_udba = true;

						if(vect.labelFields[0].labelDetails && vect.labelFields[0].labelDetails.details) {
							var base_array = vect.labelFields[0].labelDetails.base_array, updateBaseArea = false, updateUpperFloorArea = false;
							
                            $.each(vect.labelFields[0].labelDetails.details, function(lvl_code, det) {
								var valid = false, _cexists = false;
								det.fin_area = det.fin_area? det.fin_area : 0;
								$.each(det, function(idx, k) {
									if ( k && k.toString() != '0' && k.toString() != '' ) valid = valid || true;
									if( lvl_code.toString() != '1' && idx == 'comp1' && k && k.toString() != '0' && k.toString() != '' ) _cexists = _cexists || true;
								});

								if (valid) {

									if(lvl_code.toString() == '1'){
										
                                        if( (det.comp1 == '202' || det.comp1 == '206') || (det.comp2 == '202' || det.comp2 == '206') || (det.comp3 == '202' || det.comp3 == '206') || (det.comp4 == '202' || det.comp4 == '206') ){
											var _fa = 0;
											if(base_array)	{
												var _bay = ( ( base_array.filter(function(x){return x.Id == 'Fin' || x.Id == 'UnFin'}).length > 0 ) ? ( base_array.filter(function(x){return x.Id == 'Fin' || x.Id == 'UnFin'}) ) : ( ( base_array.filter(function(x){return x.Id == '202'}).length > 0 )? ( base_array.filter(function(x){return x.Id == '202'}) )  : ( ( base_array.filter(function(x){return x.Id == '206'}).length > 0 )? ( base_array.filter(function(x){return x.Id == '206'}) )  : null ) ) );  
												if( _bay )
													_fa = _bay.length > 1 ? ( parseFloat( _bay[0].area ) + parseFloat( _bay[1].area ) ) : parseFloat( _bay[0].area ) ;
											} 
											else 
												_fa = _va;
											_bsmtat = _bsmtat + Math.round( _fa ) ;									
										}

									}
									else {

										var _pl, _pdesc = '', _hf = 0, _rmval = '', _pIntRmvl = 0;
										
										if(det.prefix1 && det.prefix1 != '' && det.prefix1 != '0'){
											_pl = MVP_Lookup.filter(function(lk){ return lk.tbl_element == det.prefix1 })[0];
											if(_pl) {
												_pdesc =  _pl.tbl_element_desc ? _pl.tbl_element_desc: '';
												_rmval = _pl.tbl_element_desc.split(' ')[0];
												_pIntRmvl = ( _rmval != 'N'? ( ( _rmval.split("-").length > 1 )? ( parseInt( _rmval ) ): ( ( _rmval.split("/").length > 1 )? 1: ( parseInt( _rmval ) ) ) ): 0 )
												_hf = _rmval.split('-').length > 1 ? _rmval.split('-')[1] : _rmval;
												_hf = ( ( _hf == '1/2' ) ? 0.5 : ( ( _hf == '3/4' ) ? 0.75 :( ( _hf == '1/4' )? 0.25: 0 ) ) );
											}
										}
										
										if(lvl_code.toString() == '2') {
											
                                            var _sh = 1;
											if( ( _pdesc != '' && !_pdesc.match(/[c]/g)) || ( det.comp1 && det.comp1.toString() == '204' ) ) {
												var _sa = Math.round( (det.comp1 && det.comp1.toString() == '204'? _va : ( _pdesc.split("/").length > 1 ? ( _pdesc.split("-").length > 1 ? _va: 0 ): _va )) );
                                                _ba = _ba + _sa;
												_ffa = _ffa + Math.round( (_pdesc != '1-3/4 s'? _sa : 0) );
												_sh = ( ( _rmval == 'N' ) ? parseInt( det.ns1 ): _pIntRmvl );
												_ufa = _ufa + Math.round( ( _sh > 1 ? ( _va * (_sh - 1) ): 0 ) );
												_fla = _fla + Math.round( ( ( _rmval == '1/2' || _rmval == '3/4' ) ? 0 : ( ( _rmval.split("-").length > 1 ) ? ( ( _pIntRmvl * _va ) + Math.round( det.fin_area ) ): ( _rmval == 'N' ? ( parseInt( det.ns1 ) * _va ) : ( ( _pIntRmvl > 1 )? ( _pIntRmvl * _va ): _va ) ) ) ) );
											}
											_hfa = _hfa + Math.round( ( ( _rmval == '1/2' || _rmval == '3/4' ) ? 0: (_hf ? (det.fin_area ? Math.round( det.fin_area ) : ( _va * _hf ) ) : 0) ) );
											if( !_ubat ) {
												_bat = _bat + Math.round( ( _cexists? _va : ( _pdesc.split("/").length > 1 ? ( _pdesc.split("-").length > 1 ? _va: 0 ): _va ) ) );
											    _ubat = true;
											}
											
										}
										else {

											var _sh = 1, _1s = false;
											if( _pdesc != '' && !_pdesc.match(/[c]/g) || ( det.comp1 && det.comp1.toString() == '204' ) ) {
												_sh = ( ( _rmval == 'N' ) ? parseInt( det.ns1): _pIntRmvl );
												if(( _sh == 1 && ( (_rmval == '1') || (_rmval == '1-1/2') || (_rmval == '1-3/4') ) ) || ( det.comp1 && det.comp1.toString() == '204' ) )
													_1s = true;
												_ufa = _ufa + Math.round( ( _sh > 1 ? ( (_rmval == '2-1/2') ? ( 2 * _va ): (  _rmval == 'N' ? (_va * 3):  ( _sh * _va ) ) ) : (_1s ? _va : 0 ) ) );
												_fla = _fla + Math.round( ( ( _rmval == '1/2' || _rmval == '3/4' ) ? Math.round( det.fin_area ) : ( ( _rmval.split("-").length > 1 ) ? ( ( _pIntRmvl * _va ) + Math.round( det.fin_area ) ): ( _rmval == 'N' ? ( 3 * _va ) : ( ( _pIntRmvl > 1 )? ( _pIntRmvl * _va ): _va ) ) ) ) );
											}
											
											if( ( det.comp1.toString() == '201' ) && det.fin_area) {
												_faa = Math.round( _faa + Math.round(det.fin_area) );
												_fla = Math.round(_fla + Math.round(det.fin_area));
											}
											_hfa = _hfa + Math.round( (_hf ? (det.fin_area ? Math.round( det.fin_area ) : ( _va * _hf ) ) : 0) );
											if( !_ubat ) {
												_bat = _bat + Math.round( ( _pdesc.split("c").length > 1 ? _va: 0 )  );
											    _ubat = true;
											}

										}
									}

								}
							});
							
							if (garageFin) addnfun();
						}

					}
					else if(vect.vectorString != '' && vect.vectorString != null && vect.vectorString != ' ' ) {
                        addnfun();
					}
				});

			}
			
			nAdditionArray.forEach(function(nAddition) {
				var aRowid = nAddition.AdditionRowuid;
				var existsAddition = AdditionArray.filter(function(a) { return a.AdditionRowuid == aRowid })[0];
				if (existsAddition)
					existsAddition.BaseArea = existsAddition.BaseArea + nAddition.BaseArea;
			});
			
			skt.vectors.filter(function(OI_Vect){ return (OI_Vect.isModified && OI_Vect.sketchType == 'outBuilding')}).forEach(function(vect){
				var points, mvpData = vect.mvpData, OI_label_code = mvpData['Outbuilding_Label_Code'], impId = mvpData.Improvement_ID, refNum = mvpData.Reference;
				impId = impId.replace(/[\{$\$}']+/g,'').split('#');
				refNum = (refNum && (!isNaN(refNum)))? parseInt(refNum): refNum;
				var OI_impId = (impId.length > 1)? impId[1]: impId[0];
				OI_impId = parseFloat(OI_impId)? parseFloat(OI_impId): OI_impId.trim();
				OI_Record = activeParcel['CC_OtherImprovement'].filter(function(rd){return (rd.ROWUID == OI_impId || OI_impId == rd.SegmentID)})[0];
				if(vect.vectorString != '' && vect.vectorString != null && vect.vectorString != ' ' ){
					points = vect.vectorString.split(':')[1].split(' S ');
					var pointsLen = (points.length > 1 && points[1].split(/\s/) && points[1].split(/\s/).length < 5)? true: false;
					var _width = 0 ,_length = 0, _area = 0;
					if(OI_Record && !vect.isUnSketchedArea){
						var hLength = 0,RLength = 0, lT=0, uT=0;
						points.length > 1 && points[1].split(/\s/).forEach(function(p, i) {
						 	if ( p.trim() != '' && p.trim() != 'P2' ){
								if(p.split("/").length > 1)
									return;
								else{
									if((p.split("L").length > 1 || p.split("R").length > 1) && lT == 0 ){
										hLength = p.split("L").length > 1 ? parseFloat(p.split("L")[1]) : parseFloat(p.split("R")[1]);
										lT = 1;
									}
									else if((p.split("U").length > 1 || p.split("D").length > 1) && uT == 0 ){
										RLength = p.split("U").length > 1 ? parseFloat(p.split("U")[1]) : parseFloat(p.split("D")[1]);
										uT =1;
									}	
								}
						 	}
						});												
						if(hLength > RLength) {
							_length = hLength;
							_width = RLength;
						}
						else{
							_length = RLength;
							_width = hLength;
						}
						_area = Math.round(vect.area());
						sketchedOI = true;
					}
					if(OI_Record && Object.keys(OI_Record).length > 0 && vect.isUnSketchedArea){
						sketchedOI = false;
					}
					if(MVP_Outbuliding_Lookup.filter(function (OI){return OI.label_code == OI_label_code})[0]){					
						updateOIarray = true;
						label_text = MVP_Outbuliding_Lookup.filter(function (OI){return OI.label_code == OI_label_code})[0].label_text;						
					}
					if(updateOIarray && sketchedOI)
						DBRecordArray.push(_.clone({Card_Rowuid: ParentRowCard.ROWUID, OITable: 'CC_OtherImprovement', OI_Record: OI_Record, OtherImprovementCode: label_text, Width: _width, Length: _length, TotalArea: _area, CalcTotalArea: _area, sketchType: 'outBuilding'}));						
					else if(!updateOIarray && sketchedOI)
						DBRecordArray.push(_.clone({Card_Rowuid: ParentRowCard.ROWUID, OITable: 'CC_OtherImprovement', OI_Record: OI_Record, Width: _width, Length: _length, TotalArea: _area, CalcTotalArea: _area, sketchType: 'outBuilding'}));				
					else if(updateOIarray && !sketchedOI)
						DBRecordArray.push(_.clone({Card_Rowuid: ParentRowCard.ROWUID, OITable: 'CC_OtherImprovement', OI_Record: OI_Record, OtherImprovementCode: label_text, sketchType: 'outBuilding'}));					
				}
			});

			_tuFA = parseInt( _ufa );
			_tfIA = parseInt( _ffa );
			_taIA = parseInt(_faa);	
			_tHalfIA = parseInt( _hfa );
			_tufaIA = parseInt( _ufa );				
			
			if(_udba)
				DBRecordArray.push(_.clone({Card_Rowuid: ParentRowCard.ROWUID, DBTable: DBTable, DBRecord: DBRecord, TotalHalfFloorAreaInclAdditions: _tHalfIA, TotalAtticAreaInclAdditions: _taIA, TotalFirstFloorAreaInclAdditions: _tfIA, TotalUpperFloorArea: _tuFA, BaseAreaTotal: Math.round(_bat), BaseArea: Math.round(_ba), FinishedLivingArea: Math.round(_fla), HalfFloorArea: Math.round(_hfa), FinishedAtticArea: Math.round(_faa), BasementFinishedArea: Math.round(_bfa), FirstFloorArea: Math.round(_ffa), UpperFloorArea: Math.round(_ufa), BasementAreaTotal: Math.round(_bsmtat), TotalUpperFloorAreaInclAdditions: _tufaIA }));
		});
	});
	
	sketchApp.deletedVectors.filter((v) => { return !(v.newRecord && v.isPasteVector)}).forEach(function(delvect){
		if(delvect.sketchType != 'outBuilding'){
			var PRow;
			if(delvect.eEid)
				PRow = activeParcel['CC_Card'].filter(function(rd){return rd.ROWUID == (delvect.eEid)})[0];
			else
				PRow = delvect.sketch.parentRow;
			var sketch_Type = PRow.MainBuildingType;
			var DBTable = (sketch_Type == 'D' ? 'CC_Dwelling': 'CC_Building');
			var AddTable = (sketch_Type == 'D' ? 'CC_Addition' : 'CC_Addition_Building');
			var DBcat = getCategoryFromSourceTable(DBTable).Id;
			var AddCat = getCategoryFromSourceTable(AddTable).Id;
			var DBRecord = activeParcel[DBTable].filter(function(rd){return rd.ParentROWUID == PRow.ROWUID})[0];
			var mvpDatas = delvect.mvpData;
			var imId = mvpDatas.Improvement_ID;
			if(imId.split('{').length > 1){
				if((imId.split('{')[1].split('#')).length > 1)
					imId = imId.split('{')[1].split('#')[1].split('$')[0];
				else
					imId = imId.split('{')[1].split('}')[0];
			}
			if(imId == 'D' || imId == 'B' || (DBRecord && imId == DBRecord.ROWUID)) {
				var DBRec = DBRecordArray.filter(function(re){return re.Card_Rowuid == PRow.ROWUID})
				if(DBRec.length == 0)
					deleteVectorArray.push(_.clone({catId: DBcat, ROWUID: DBRecord.ROWUID}))
			}
			else{
				var AddRecord = activeParcel[AddTable].filter(function(rd){ return (rd.SegmentID == imId || rd.ROWUID == imId) })[0];
				if(AddRecord)
					deleteVectorArray.push(_.clone({catId: AddCat, ROWUID: AddRecord.ROWUID}))
			}
		}
		if(delvect.sketchType == 'outBuilding'){
			var PRow = delvect.sketch.parentRow;
			var OITable = 'CC_OtherImprovement';
			var OIcat = getCategoryFromSourceTable(OITable).Id;
			var mvpDatas = delvect.mvpData, impId = mvpDatas.Improvement_ID, refNum = mvpDatas.Reference;
			impId = impId.replace(/[\{$\$}']+/g,'').split('#');
			refNum = (refNum && (!isNaN(refNum)))? parseInt(refNum): refNum;
			var OI_impId = (impId.length > 1)? impId[1]: impId[0];
			OI_impId = parseFloat(OI_impId)? parseFloat(OI_impId): OI_impId.trim();
			var OIRecord = activeParcel[OITable].filter(function(rd){return rd.ROWUID == OI_impId || OI_impId == rd.SegmentID})[0];
			if(OIRecord)
				deleteVectorArray.push(_.clone({catId: OIcat, ROWUID: OIRecord.ROWUID}))
			
		}
	});	
	
	function updateCC_SKETCH() {
		var CC_Sketch_Array = [], CC_sketch_field = getDataField('CC_SKETCH', 'CC_Card');
		if (sketchDataArray.length != noteData.length) {
			noteData.forEach(function(note) {
				var skuid = note.parentId;
				var sketchArray = sketchDataArray.filter(function(sk) { return sk.sid == skuid })[0];
				if (!sketchArray)
					sketchDataArray.push(_.clone({sid: skuid}));
			});
		}
		/* old format
		sketchDataArray.forEach(function(skd) {
			sketchApp.sketches.filter(function(skda){ return skda.sid == skd.sid}).forEach(function(skt){ 
				var CC_Sketch = '', parentRow = skt.parentRow, new_scale = null;
                if(skt.isBoundaryScaleModified)
                	new_scale = Number(skt.boundaryScale);
				skt.vectors.forEach(function(vect) {
					if(vect.vectorString != '' && vect.vectorString != null && vect.vectorString != ' ' ){
						var vectConfig = vect.vectorString;
						var dimPart = vectConfig.split(":")[1];
						var mvData = vect.mvpData;
						if(new_scale && !vect.isUnSketchedArea)
                        	mvData['Scale'] = new_scale;
                        if (vect.isUnSketchedTrueArea) {
                        	dimPart = dimPart.split('S')[0] + 'S';
                        	mvData['Symbol_Size'] = '32';
                        }

						if(vect.isModified){
							if(vect.sketchType != 'outBuilding'){
								if (vect.isUnSketchedTrueArea) {
									mvData['Area'] = Math.round(vect.fixedAreaTrue);
									mvData['Perimeter'] = Math.round(vect.fixedPerimeterTrue);
								}
								else {
									mvData['Area'] = Math.round(vect.area());
									mvData['Perimeter'] = Math.round(vect.perimeter());
								}
								mvData['Sketched_Other_Improvement_Indicator'] = 0;
							}
							if(vect.sketchType == "outBuilding" && vect.isUnSketchedArea == true) {
	                    		mvData.Label_Position_X = 10000, mvData.Label_Position_Y = 10000, mvData.Area_Label_Position_X = 0, mvData.Area_Label_Position_Y = 0;
	                    		if (!vect.isMVPSpecialUnsketchedArea) {
	                    			mvData.Scale = 0, mvData.Sketched_Other_Improvement_Indicator = 0;
	                    		}
							}
							else if(vect.sketchType == "outBuilding" && !vect.isUnSketchedArea){
								mvData['Sketched_Other_Improvement_Indicator'] = 1;
								if(mvData['Scale'] > 0){
									mvData['Scale'] = mvData['Scale'];
								}
								else{
									mvData['Scale'] = 0;
								}
								mvData['Area'] = Math.round(vect.area()),	mvData['Perimeter'] = Math.round(vect.perimeter());
							}
							var skScale = mvData['Scale'];
							mvData['Area_Font_Size'] = (skScale > 0 && skScale < 80)? 9: (skScale >= 80 && skScale < 100)? 10: (skScale >= 100 && skScale <= 120)? 12: 0; 						
        				
						}
						if(vect.sketchType == "outBuilding" && vect.isUnSketchedArea == true)
        					mvData['Symbol_Size'] = '';
						var headpart = ['Sketch_Pointer','Improvement_ID', 'Level_1_Default_Exterior_Cover', 'Level_1_Construction_Code', 'Level_1_N', 'Level_1_Prefix_For_Part_1', 'Level_1_Extra_Feature_Code', 'Level_1_Component_Code_For_Part_1','Level_1_Modifier_Code_For_Part_1', 'Level_1_Prefix_For_Part_2', 'Level_1_Component_Code_For_Part_2', 'Level_1_Modifier_Code_For_Part_2', 'Level_1_Prefix_For_Part_3', 'Level_1_Component_Code_For_Part_3', 'Level_1_Modifier_Code_For_Part_3', 'Level_1_Prefix_For_Part_4', 'Level_1_Component_Code_For_Part_4', 'Level_1_Modifier_Code_For_Part_4', 'Level_1_FreeForm_Label_Code', 'Level_2_Default_Exterior_Cover', 'Level_2_Construction_Code', 'Level_2_N', 'Level_2_Extra_Feature_Code', 'Level_2_Prefix', 'Level_2_Component_Code', 'Level_2_ModifierCode', 'Level_2_FreeForm_Label_Code', 'Level_3_Default_Exterior_Cover', 'Level_3_Construction_Code', 'Level_3_N', 'Level_3_Extra_Feature_Code', 'Level_3_Prefix', 'Level_3_Component_Code', 'Level_3_Modifier_Code', 'Level_3_FreeForm_Label_Code', 'Outbuilding_Label_Code', 'Reference', 'Label_Position_X', 'Label_Position_Y', 'Area_Label_Position_X', 'Area_Label_Position_Y', 'Scale', 'Area', 'Perimeter', 'Level_1_Fin_Area', 'Level_1_Unfin_Area', 'Level_2_Fin_Area', 'Level_2_Unfin_Area', 'Level_3_Fin_Area', 'Level_3_Unfin_Area', 'Symbol', 'Symbol_Size', 'Area_Font_Size', 'Sketched_Other_Improvement_Indicator'];						
       					var h_part = '[';
        				var headpart_len =  headpart.length;
        				headpart.forEach( function(head_P,i){
        					if(mvData[head_P] == 'NaN')
        						mvData[head_P] = '';
        					if(i < headpart_len - 1)
        						h_part = h_part + mvData[head_P] + ',';
        					else
               					h_part = h_part + mvData[head_P];
        				});
       					var lng = h_part.split(',').length, ntlen = 23; 
        				for (var k = lng; k < (lng + ntlen); k++) {
        					h_part = h_part + ',';
        				}
       					var headerValue = h_part + ",]:" + dimPart + ';';
       					CC_Sketch = CC_Sketch + headerValue;
       				}
				});
				
				var noteString = '[,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,0', ncount = 0, notsixnote = false, isNotexists = false;
				skt.notes.filter(function(n) { return !n.isDeleted }).forEach(function(nt, index) {
					if (!nt.noteText)
						return;
					nt.noteText = nt.noteText.replaceAll('>', '&gt;').replaceAll('<', '&lt;');
					notsixnote =  false; isNotexists = true;
					noteString = noteString + ',' + (nt.fontSize? nt.fontSize: 12) + ',' + sRound(nt.notePosition.x) + ',' + sRound(nt.notePosition.y) + ',' + nt.noteText;
					ncount++;
					if (ncount == 6) {
						ncount = 0;
						notsixnote =  true;
						noteString = noteString + ']:;';
						CC_Sketch = CC_Sketch + noteString;
						noteString = '[,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,0';
					}
				});
				if (!notsixnote && isNotexists) {
    				var nlng = noteString.split(',').length;
    				for (var k = nlng; k < 77; k++) {
    					noteString = noteString + ',';
    				}
   					var noteString = noteString + ",]:;";
   					CC_Sketch = CC_Sketch + noteString;
				}
				
				console.log(CC_Sketch);
				CC_Sketch_Array.push(_.clone({CC_Record: parentRow, CC_Sketch: CC_Sketch}))
			});
		});
		*/
		//new json format
		let mds = null;
		if (sketchSettings.MVPDefaultSettings) {
			try {
				mds = JSON.parse(sketchSettings.MVPDefaultSettings);			
			}
			catch(ex) { }			
		}

		var dimensionPositionNew = function (p1, p2) {
			let res = { x: 0, y: 0 };

			if (p1.match('[\A]') || p1.match('[\B]'))
				res = { x: 0, y: 0 };
			else if (p1.split("/").length == 1) {

				if (p1.match('R') && p2.match('D')) {        //clock-wise
					res = { x: 0, y: 1.5 };
				} else if (p1.match('L') && p2.match('U')) { //clock-wise
					res = { x: 0, y: -1.5 };
				} else if (p1.match('U') && p2.match('R')) { //clock-wise
					res = { x: 1.5, y: 0 };
				} else if (p1.match('D') && p2.match('L')) { //clock-wise
					res = { x: -1.5, y: 0 };
				} else if (p1.match('R') && p2.match('U')) { //anti-clock-wise
					res = { x: 0, y: 1.5 };
				} else if (p1.match('L') && p2.match('D')) { //anti-clock-wise
					res = { x: 0, y: -1.5 };
				} else if (p1.match('U') && p2.match('L')) { //anti-clock-wise
					res = { x: 1.5, y: 0 };
				} else if (p1.match('D') && p2.match('R')) { //anti-clock-wise
					res = { x: -1.5, y: 0 };
				}

			}
			else {

				let dir1 = '', dir2 = '', directions = p1.split("/");

				if (directions.length > 1) {
					dir1 = directions[0];
					dir2 = directions[1];
				}

				if (dir1.match('U') && dir2.match('R') && (p2.match('R') || p2.match('D'))) {        //clock-wise
					res = { x: 1.8, y: 1.4 };
				} else if (dir1.match('U') && dir2.match('L') && (p2.match('R') || p2.match('U'))) { //clock-wise
					res = { x: 1.8, y: -1.4 };
				} else if (dir1.match('D') && dir2.match('R') && (p2.match('L') || p2.match('D'))) { //clock-wise
					res = { x: -1.8, y: 1.4 };
				} else if (dir1.match('D') && dir2.match('L') && (p2.match('L') || p2.match('U'))) { //clock-wise
					res = { x: -1.8, y: -1.4 };
				} else if (dir1.match('U') && dir2.match('R') && (p2.match('L') || p2.match('U'))) { //anti-clock-wise
					res = { x: 1.8, y: 1.4 };
				} else if (dir1.match('U') && dir2.match('L') && (p2.match('L') || p2.match('D'))) { //anti-clock-wise
					res = { x: 1.8, y: -1.4 };
				} else if (dir1.match('D') && dir2.match('R') && (p2.match('R') || p2.match('U'))) { //anti-clock-wise
					res = { x: -1.8, y: 1.4 };
				} else if (dir1.match('D') && dir2.match('L') && (p2.match('R') || p2.match('D'))) { //anti-clock-wise
					res = { x: -1.8, y: -1.4 };
				}

			}

			return res;
		}

		var dimensionPosition = function(p1, p2) {
			let res = { x: 0, y: 0 };
			if (p1.match('[\A]') || p1.match('[\B]'))
				res = { x: 0, y: 0 };
			else if (!(p1.split("/").length > 1 || p2.split("/").length > 1)) {
				if (p1.match('[\L]') || p1.match('[\R]')) {
					if(p2.match('[\U]') || p2.match('[\D]'))
						res = (p2.match('[\U]')) ? { x: 0, y: -1.5 }: { x: 0, y: 1.5 };						
					else	
						res = { x: 0, y: -1.5 };
				}
				else {
					if(p2.match('[\L]') || p2.match('[\R]'))
						res = (p2.match('[\L]')) ? { x: 1.5, y: 0 }: { x: -1.5, y: 0 };	
					else	
						res = { x: 1.5, y: 0 };
				}
			}
			else {			
				let len1 = 0, len2 = 0, pointVal = '', p1Split = p1.split("/");
				if (p1Split.length > 1 ) {
					len1 = parseFloat((p1Split[0].match(/[a-zA-Z]+|[0-9]+(?:\.[0-9]+|)/g))[1]);
					len2 = parseFloat((p1Split[1].match(/[a-zA-Z]+|[0-9]+(?:\.[0-9]+|)/g))[1]);
				}
				else
					len1 = parseFloat((p1Split[0].match(/[a-zA-Z]+|[0-9]+(?:\.[0-9]+|)/g))[1]);
				
				if (len1 >= len2 ) pointVal = p1Split[0];
				else pointVal = p1Split[1];
				
				if (pointVal.match('[\L]') || pointVal.match('[\R]')) {
					if (pointVal.match('[\L]'))
						res = (p2.match('[\U]') || (!(p2.match('[\D]')) && (p2.match('[\R]')))) ? { x: 2.5, y: -2.5 }: { x: -2.5, y: 2.5 };
					else
						res = (p2.match('[\U]') || (!(p2.match('[\D]')) && (p2.match('[\L]')))) ? { x: -2.5, y: -2.5 }: { x: 2.5, y: 2.5 };
				}
				else {
					if (pointVal.match('[\U]'))
						res = ((!(p2.match('[\U]')) && (p2.match('[\R]'))) || p2.match('[\D]')) ? { x: 2.5, y: -2.5 }: { x: -2.5, y: 2.5 };
					else
						res = ((!(p2.match('[\U]')) && (p2.match('[\L]'))) || p2.match('[\D]')) ? { x: -2.5, y: 2.5 }: { x: 2.5, y: -2.5 };
				}
			}
			return res;
		}
		
		sketchDataArray.forEach((skd) => {
			sketchApp.sketches.filter((skda) => { return skda.sid == skd.sid }).forEach((skt) => { 
				let parentRow = skt.parentRow, new_scale = null, jsonObj = null, isJson = false, sketch_Type = parentRow.MainBuildingType;
                let DBTable = (sketch_Type == 'D' ? 'CC_Dwelling': 'CC_Building');
				let DBRecord = activeParcel[DBTable].filter(function(rd){return rd.ParentROWUID == parentRow.ROWUID})[0];
				
                if (skt.isBoundaryScaleModified)
                	new_scale = Number(skt.boundaryScale);
                try {
		        	jsonObj = JSON.parse(skt.parentRow.CC_SKETCH);
		        	if (jsonObj && jsonObj.Version) isJson = true;
		        }
		        catch(ex) { jsonObj = null; }  
		        
				if (!isJson) 
					jsonObj = { Version: "1.0.0", Scale: (new_scale? new_scale: '80'), Color: "000000", SktFont: null, SktFontSize: null, Segments: [], Notes: [], SktOps: [], SktLinks: [] };
				else if (new_scale) 
					jsonObj['Scale'] = new_scale;

				jsonObj.Segments = []; jsonObj.Notes = [];
				
				skt.vectors.forEach((vect, i) => {
					let jns = vect.jsonSegment? vect.jsonSegment: null, isJson = jns? true: false, segGrades = []; 
					let mvData = vect.mvpData;
					if (!isJson) {
						jns = JSON.parse('{"SegmentNo":0,"SegType":null,"SegScale":80,"Color":null,"FillStyle":null,"FillColor":null,"SegArea":0,"SegAreaLabelX":0,"SegAreaLabelY":0,"SegAreaLabelFontSize":"10","SegAreaLabelFontColor":null,"SegTotArea":0,"SegFinArea":0,"SegPerimeter":0,"SegLinks":[],"SegOps":[],"Label":null,"LabelX":0,"LabelY":0,"LabelFontSize":"10","LabelFontColor":null,"Symbol":0,"SymbolSize":"0","SymbolColor":"000000","StartX":0,"StartY":0,"Lines":[],"NegSegArea":0,"NegSegPeremeter":0,"NegStartX":0,"NegStartY":0,"NegAreaLines":[],"SegGrades":[]}');
						jns['SegScale'] = mvData['Scale'];						
						jns['LabelFontSize'] = mds && mds['LabelDefaultFontSize'] && !isNaN(mds['LabelDefaultFontSize'])? mds['LabelDefaultFontSize']: 10;						
					}
					
					jns['SegScale'] = mvData['Scale'] = new_scale && !vect.isUnSketchedArea? new_scale: jns.SegScale;				
        			jns['SymbolSize'] = vect.isUnSketchedTrueArea? '32': (vect.sketchType == "outBuilding" && vect.isUnSketchedArea == true? '': jns.SymbolSize);	
                    jns['SegmentNo'] = i; 
                    
                    if (vect.isModified || !isJson) {
						if (vect.sketchType != 'outBuilding') {
							if (vect.isUnSketchedTrueArea) {
								mvData['Area'] = Math.round(vect.fixedAreaTrue);
								mvData['Perimeter'] = Math.round(vect.fixedPerimeterTrue);
							}
							else {
								mvData['Area'] = Math.round(vect.area());
								mvData['Perimeter'] = Math.round(vect.perimeter());
							}
							mvData['Sketched_Other_Improvement_Indicator'] = 0;
						}
						if (vect.sketchType == "outBuilding" && vect.isUnSketchedArea == true) {
                    		mvData.Label_Position_X = 10000, mvData.Label_Position_Y = 10000, mvData.Area_Label_Position_X = 0, mvData.Area_Label_Position_Y = 0;
                    		if (!vect.isMVPSpecialUnsketchedArea) { 
                    			mvData.Scale = 0, mvData.Sketched_Other_Improvement_Indicator = 0;
                    		}
						}
						else if (vect.sketchType == "outBuilding" && !vect.isUnSketchedArea) {
							mvData['Sketched_Other_Improvement_Indicator'] = 1;
							if(mvData['Scale'] > 0) mvData['Scale'] = mvData['Scale'];
							else mvData['Scale'] = 0;
							mvData['Area'] = Math.round(vect.area()); mvData['Perimeter'] = Math.round(vect.perimeter());
						}
						let skScale = mvData['Scale'];
						mvData['Area_Font_Size'] = (skScale > 0 && skScale < 80)? 9: (skScale >= 80 && skScale < 100)? 10: (skScale >= 100)? 12: 0; 						
    					let s = vect.vectorString? vect.vectorString.split(':')[1].split('S')[0].trim(): 'U0 R0';
    					let p = new RegExp(/[ABUDLR#PC]|[0-9]+(.[0-9]+)?/g);
    					let ss = s.split(' '), ss1 = ss[0].match(p), ss2 = ss[1].match(p), dir1 = ss1[0], d1 = ss1[1], dir2 = ss2[0], d2 = ss2[1];    					
    					let sx = (dir2 == 'L'? -(Math.abs(d2)): (dir2 == 'R'? Math.abs(d2): 0));
    					let sy = (dir1 == 'D'? -(Math.abs(d1)): (dir1 == 'U'? Math.abs(d1): 0));
    					   					
    					jns['SegArea'] = mvData['Area']; jns['SegPerimeter'] = mvData['Perimeter']; jns['LabelX'] = Math.round(mvData['Label_Position_X']); jns['LabelY'] = Math.round(mvData['Label_Position_Y']); 
    					jns['SegAreaLabelX'] = Math.round(mvData['Area_Label_Position_X']); jns['SegAreaLabelY'] = Math.round(mvData['Area_Label_Position_Y']); jns['Symbol'] = mvData['Symbol'];
    					jns['SegAreaLabelFontSize'] = mvData['Area_Font_Size']; jns['StartX'] = sx; jns['StartY'] = sy; jns['SegType'] = mvData['Improvement_ID'];
						jns.Lines = []; jns.SegGrades = [];
						
						if (!isJson && jns.SegLinks.length == 0) 
							jns.SegLinks.push({ SegLink: mvData['Sketch_Pointer']? mvData['Sketch_Pointer']: null, SegLinkLabel: null, SegLinkName: "SketchPointer", SegLinkNo: 0 });
						
						if ((vect.sketchType != 'outBuilding' && !vect.isUnSketchedTrueArea) || (vect.sketchType == 'outBuilding' && !vect.isUnSketchedArea)) {
							let n = vect.startNode.nextNode, j = 0;
					        while (n != null) {
					        	if (!(n.dy > 0 || n.dx > 0)) { n = n.nextNode; continue };
					            let lineObj = _.clone( {Angle: null, ArcDirection: null, Direction: null, FinishX: 0, FinishY: 0, LineColor: "000000", LineDimensionalFont: null, LineDimensionalFontColor: "000000", LineDimensionalFontSize: (n.hideDimensions? '0': (n.lineFontSize? n.lineFontSize: ((mds && mds['DimensionDefaultFontSize'] && !isNaN(mds['DimensionDefaultFontSize']))? mds['DimensionDefaultFontSize']: "8"))), LineDimensionalValue: "0", LineDimensionalX: 0, LineDimensionalY: 0, LineLength: 0, LineNo: j, LineWidth: 0, Rise: 0, Run: 0, SagittaHeight: 0, StartX: 0, StartY: 0 });
					        	let ls = '', ll = '', lss = '', nls = '', dimPos = {x: 0, y: 0 };
					        	if (n.arcLength > 0) {
						            lineObj.ArcDirection = "A";
						            lineObj.SagittaHeight = n.arcLength;
						        }
						        else if (n.arcLength < 0) {
						        	lineObj.ArcDirection = "B";
						            lineObj.SagittaHeight = Math.abs((n.arcLength));
						        }
						        
						        if (n.dy > 0) {
						        	ls = n.sdy; ll = n.dy;
						        	lss += n.sdy + (n.dy);
						        }
						        if (n.dx > 0) {
						            ls += ls != ''? '/' + n.sdx: n.sdx; ll += ll != ''? '/' + n.dx: n.dx;
						            if (lss != "") lss += "/"; lss += n.sdx + (n.dx);
						        }
						        
						        lineObj.Direction = ls;
						        ll = ll.toString();
						        if (ll.contains('/')) {
						        	lineObj.Rise = parseFloat(ll.split('/')[0]); lineObj.Run = parseFloat(ll.split('/')[1]);
						        }
						        else lineObj.LineLength = parseFloat(ll);
						        
						        nn = n.nextNode? n.nextNode: vect.startNode.nextNode;
						        if (!(nn.dy > 0 || nn.dx > 0)) 
									nn = nn.nextNode? nn.nextNode: vect.startNode.nextNode;
						        if (nn.dy > 0)
						            nls += nn.sdy + (nn.dy);
						        if (nn.dx > 0) {
						            if (nls != "") nls += "/";
						            nls += nn.sdx + (nn.dx);
								}

								dimPos = dimensionPositionNew(lss, nls);
						        //dimPos = dimensionPosition(lss, nls);

						        lineObj.LineDimensionalX = dimPos? dimPos.x: 0; lineObj.LineDimensionalY = dimPos? dimPos.y: 0;
						        						        
						        jns.Lines.push(lineObj); j = j + 1;
						        n = n.nextNode;
					        }
				        }
				        
				        if (vect.sketchType != 'outBuilding') {
				        	let lblDetail = vect.labelFields[0] && vect.labelFields[0].labelDetails && vect.labelFields[0].labelDetails.details? vect.labelFields[0].labelDetails.details: null;					        
					        let ImpIdval = mvData.Improvement_ID; 
							if(ImpIdval.split('{').length > 1) {
								if((ImpIdval.split('{')[1].split('#')).length > 1) ImpIdval = ImpIdval.split('{')[1].split('#')[1].split('$')[0];
								else ImpIdval = ImpIdval.split('{')[1].split('}')[0];
							}
							
					        for (k = 1; k < 4; k++) {
					        	let gfa = mvData['Level_'+ k +'_Fin_Area'], gufa = mvData['Level_'+ k +'_Unfin_Area'];
					        	if ((k == 2 || k == 3) && parseFloat(gfa) <= 0 && parseFloat(gufa) <= 0 && (lblDetail[2].comp1 == 208 || lblDetail[2].comp1 == 209 || ((lblDetail[2].ext_feat_code1 != '' && lblDetail[2].ext_feat_code1 != '0') && (ImpIdval != 'D' && ImpIdval != 'B' && (!DBRecord || (DBRecord && DBRecord.ROWUID != ImpIdval))))))
					        		gufa = Math.round(vect.area());

					        	let sgObj = _.clone({ GradeArea: 0, GradeConstructionCode: mvData['Level_' + k + '_Construction_Code'], GradeDefaultExteriorCover: mvData['Level_'+ k +'_Default_Exterior_Cover'], GradeExtraFeatCode: (mvData['Level_'+ k +'_Extra_Feature_Code']? mvData['Level_'+ k +'_Extra_Feature_Code']: '0'), GradeFinArea: gfa, GradeLabel: mvData['Level_'+ k +'_FreeForm_Label_Code'], GradeLevel: k, GradeN: mvData['Level_'+ k +'_N'], GradeUnfinArea: gufa, Parts: [] });
					        	if (k == 1) {
					        		for (z = 1; z <= 4; z++) {
					        			sgObj.Parts.push(_.clone({ PartComponentCode: mvData['Level_'+ k +'_Component_Code_For_Part_'+ z], PartModifierCode: mvData['Level_'+ k +'_Modifier_Code_For_Part_'+ z], PartNumber: z-1, PartPrefix: mvData['Level_'+ k +'_Prefix_For_Part_'+ z] }));
					        		}
					        	}
					        	else
					        		sgObj.Parts.push({ PartComponentCode: mvData['Level_'+ k +'_Component_Code'], PartModifierCode: (k == 2? mvData['Level_'+ k +'_ModifierCode']: mvData['Level_'+ k +'_Modifier_Code']), PartNumber: 0, PartPrefix: mvData['Level_'+ k +'_Prefix'] });	
					        	segGrades.push(sgObj);
					        }
					        jns.SegGrades = segGrades;
				        }
				        else if (vect.sketchType == 'outBuilding') {
				        	jns['Label'] = mvData['Outbuilding_Label_Code'];
				        }
					}
					jsonObj.Segments.push(jns);
				});
				
				skt.notes.filter((x) => { return !x.isDeleted }).forEach((note, i) => {
					jsonObj.Notes.push(_.clone({ NoteNumber: i, Font: null, FontSize: (note.fontSize? note.fontSize: ((mds && mds['NoteDefaultFontSize'] && !isNaN(mds['NoteDefaultFontSize']))? mds['NoteDefaultFontSize']: '12')), FontColor: '000000', StartX: note.notePosition.x, StartY: note.notePosition.y, NoteText: note.noteText }));
				});
				
				let jsonStr = JSON.stringify(jsonObj);
				CC_Sketch_Array.push(_.clone({ CC_Record: parentRow, CC_Sketch: jsonStr }));				
			});
		});
		
		function update_CC_sketch_value(CC_Sketch_Arrays){
			if(CC_Sketch_Arrays.length == 0){
				getParcel(activeParcel.Id, function () {
					if (sketchSaveCallBack) sketchSaveCallBack(afterSketchSave); return;
				});
				return;				
			}
			var cc_array = CC_Sketch_Arrays.pop();
			var val = cc_array.CC_Sketch;
			var Crecords = cc_array.CC_Record;
			var CC_Card_Record = activeParcel['CC_Card'].filter(function(rd){return rd.ROWUID == Crecords.ROWUID})[0];
			update_data(CC_sketch_field, val, Crecords, function(){
				CC_Card_Record['CC_SKETCH'] = val;
				update_CC_sketch_value(CC_Sketch_Arrays);
			})
		}
		update_CC_sketch_value(CC_Sketch_Array);
	}
	function calculateDBRecordFields(DBRecordArray){
		if(DBRecordArray.length == 0){
			updateCC_SKETCH();
			return false;
		}
		var DBRecordDetails = DBRecordArray.pop(), DBFields = [],OIFields = [], OIrecord;
		var Drecord = DBRecordDetails.DBRecord;
		DBFnames.forEach(function(fname){ DBFields.push(getDataField(fname, DBRecordDetails.DBTable)); });
		function updateDataFields(DBField, DBRecordDetail, Drecords){
			if(DBField.length == 0){
				calculateDBRecordFields(DBRecordArray);
				return false;
			}
			var fields = DBField.pop();
			if(fields){
				var val = DBRecordDetail[fields.Name];
				if(!val)
					val = 0;
				update_data(fields, val, Drecords, function(){
					updateDataFields(DBField, DBRecordDetail, Drecords);
				})
			}
			else
				updateDataFields(DBField, DBRecordDetail, Drecords);
		}
		function update_OI_DataFields(OIField, DBRecordDetail, OIrecords){
			var rindex = OIrecords? activeParcel['CC_OtherImprovement'].findIndex(function(x){ return x.ROWUID == OIrecords.ROWUID}):'';
			if(OIField.length == 0){
				calculateDBRecordFields(DBRecordArray);
				return false;
			}
			var fields = OIField.pop();
			if(fields && OIrecords && Object.keys(OIrecords).length > 0 ){
				var val = DBRecordDetail[fields.Name];
				if(val != undefined){
					update_data(fields, val, OIrecords, function(){
						if(OIrecords)
							activeParcel.CC_OtherImprovement[rindex][fields.Name] = val;
						update_OI_DataFields(OIField, DBRecordDetail, OIrecords);					
					})
				}
				else
					update_OI_DataFields(OIField, DBRecordDetail, OIrecords);
			}
			else
				update_OI_DataFields(OIField, DBRecordDetail, OIrecords);
		}
		if(DBRecordDetails.sketchType == 'outBuilding'){
			OIFnames.forEach(function(fname){ OIFields.push(getDataField(fname, DBRecordDetails.OITable)); });
			OIrecord = DBRecordDetails.OI_Record;
			update_OI_DataFields(OIFields, DBRecordDetails, OIrecord);
		}
		else
			updateDataFields(DBFields, DBRecordDetails, Drecord);
	}
	function calculateAdditionFields(AdditionArrays){
		if(AdditionArrays.length == 0){
			calculateDBRecordFields(DBRecordArray);
			return false;
		}
		var addArray = AdditionArrays.pop();
		var baseField = getDataField('BaseArea', addArray.AddTable);
		update_data(baseField, addArray.BaseArea, addArray.AddRecord, function(){
			calculateAdditionFields(AdditionArrays);
		})
	}
	function deleteVector(deleteVectorArrays){
		if(deleteVectorArrays.length == 0){
			calculateAdditionFields(AdditionArray);
			return false;
		}
		var deleVect = deleteVectorArrays.pop();
		ccma.Data.Controller.DeleteAuxRecordWithDescendants(deleVect.catId, deleVect.ROWUID, null, null, function () {
			if(deleteVectorArrays.length == 0){
				getParcel(activeParcel.Id, function () {
					deleteVector(deleteVectorArrays);
				});
			}
			else
				deleteVector(deleteVectorArrays);
		}, null, null, true);
	}
	deleteVector(deleteVectorArray);
}

function fixedToOnePoint(n) {
     return ((((n * 10).toString().split('.')[0])/10).toString());
} 


function isAdditionSketch(idetails) {
	let isAdditionRecord = false;
	if(idetails.comp1 == '207' || idetails.comp1 == '208' || idetails.comp1 == '209') isAdditionRecord = true;
	else {
		var mLookupData = MVP_Lookup.filter(function(pro){ return pro.tbl_element == idetails.comp1})[0];
		if(mLookupData) {
			if(mLookupData.BuildingRecord == 'CC_Addition') isAdditionRecord = true;
		}
	}
	return isAdditionRecord;
}

function MVPSketchBeforeSaveNew(sketches, callback) {
	var labels = [], lookupVal,BasementFunctionalitys = [], level_code = {'1': 'Below', '2': 'At', '3': 'Above'}, skt_type = '', var_Check = false, deleteVect_Ext_Feat_Array = [], delete_Ext_Feat_window_show = [];
	function sketchsavee(){
		$('#maskLayer').hide()
		sketches.filter(function(skda){ return skda.isModified }).forEach(function(skd){
			skd.vectors.filter(function(vect){ return vect.isModified; }).forEach(function(vector){
				if(vector.vectorString != '' && vector.vectorString != null && vector.vectorString != ' '){
					if(vector.BasementFunctionality){
						if(vector.labelFields[0] && vector.labelFields[0].labelDetails){
							var B_area = vector.isUnSketchedTrueArea? Math.round(vector.fixedAreaTrue): parseInt(vector.area());
							BasementFunctionalitys.push({ labelValue: vector.labelFields[0].labelDetails.labelValue, sid: skd.sid, sketch: vector, vect_uid: vector.uid, screen: 'BGAreaEntry', lvl_code: '1', labelDetails: vector.labelFields[0].labelDetails, Area: parseInt(B_area)});
							$.each(vector.labelFields[0].labelDetails.details, function(lvl_code, details) {
								if(lvl_code != '1' && lvl_code != 1){
									$.each(details, function(type, value) {
										if (value != 0) {
											lookupVal = MVP_Lookup.filter(function(lk){ return lk.tbl_element == value && lk.AdditionalScreen && lk['GradeLevel'] && lk['GradeLevel'].indexOf(level_code[lvl_code]) > -1; });
											if (lookupVal.length > 0){
												var addScreen = '';
												if(lookupVal[0].AdditionalScreen && lookupVal[0].AdditionalScreen.split("/").length > 1)
													addScreen = lookupVal[0].AdditionalScreen.split("/")[0];
												else
													addScreen = lookupVal[0].AdditionalScreen;
												var areaEnt = true;
												if(addScreen == 'AreaEntry' &&  lookupVal[0].tbl_type_code == "Component")
													areaEnt = (details['modifier'] == "Fin" || details['modifier'] == "UF") ? true: false;
												if(areaEnt)
													labels.push({ lookupDetail: lookupVal[0], sid: skd.sid, sketch: vector, screen: addScreen, lvl_code: lvl_code });
											}
										}
									});
								}
							});
						}	
					}
					else{
						if(vector.labelFields[0] && vector.labelFields[0].labelDetails && vector.labelFields[0].labelDetails.details){
							$.each(vector.labelFields[0].labelDetails.details, function(lvl_code, details) {
								$.each(details, function(type, value) {
									if (value != 0) {
										lookupVal = MVP_Lookup.filter(function(lk){ return lk.tbl_element == value && lk.AdditionalScreen && lk['GradeLevel'] && lk['GradeLevel'].indexOf(level_code[lvl_code]) > -1; });
										if (lookupVal.length > 0){
											var addScreens = '';
											if(lookupVal[0].AdditionalScreen && lookupVal[0].AdditionalScreen.split("/").length > 1)
												addScreens = ((lookupVal[0].AdditionalScreen.split("/")[0] != "BGAreaEntry") ? lookupVal[0].AdditionalScreen.split("/")[0] : lookupVal[0].AdditionalScreen.split("/")[1]);
											else
												addScreens = lookupVal[0].AdditionalScreen;
											var areaEnt = true;
											if(addScreens == 'AreaEntry' && lookupVal[0].tbl_type_code == "Component")
												areaEnt = (details['modifier1'] == "Fin" || details['modifier1'] == "UF") ? true: false;
											if(areaEnt) {
												let modifiedAction = null;
												if (!vector.newRecord) {
													let cva = parseFloat(vector.mvpData.Area), ncva = vector.area();													 
													if (!(cva + 1 > ncva && cva - 1 < ncva)) modifiedAction = 'New';
												}
												else if (vector.newRecord)
													modifiedAction = 'New';
												labels.push({ lookupDetail: lookupVal[0], sid: skd.sid, sketch: vector, screen: addScreens, lvl_code: lvl_code, vectorStatus: modifiedAction, fin_area: details['fin_area']? parseFloat(details['fin_area']): null, aw: true });
											}
										}
									}
								});
							});
						}
					}
				}
			});
		});
	function nBasementFunctionality(){
		if (labels.length > 0) {
	 		$('.mask').show();	
	 		$('.Current_vector_details').show();
	 		$('.outbdiv').hide();
	 		$('.skNote').html('');
	 		var cancelCallback = function(isChecked) {
	 			isChecked = !isChecked? false: true;
	       		$('.Current_vector_details').hide();
	        	$('.mask').hide();
	        	hideKeyboard();
	 			callback(isChecked, isChecked);
	        	return false;
	 		}
	 	
	 		var checkAreaEntry = function(){
		 		var area_entry_lbl = labels.filter(function(lbl){ return lbl.screen == 'AreaEntry' && (!lbl.aw || (lbl.aw && lbl.vectorStatus)) });
		 		if (area_entry_lbl.length > 0) {
		 			$('.Current_vector_details .head').html('Area Entry');
		 			var table = '<table class="area_entry"><tr>';
		 			table += '<th rowspan="2">Label</th><th rowspan="2">Area</th><th colspan="2">Finished Area</th><th  rowspan="2">Unfinished Area</th></tr>';
		 			table += '<tr><th>Value</th><th>%</th></tr>';
			 		area_entry_lbl.forEach(function(label) {
			 			let vt = label.sketch;
			 			let varea = vt.isUnSketchedTrueArea? Math.round(vt.fixedAreaTrue): vt.area();
			 			let lblk = label.lookupDetail, PrefEntry = null;
			 			let finArea = '', unfinArea = varea, perc = '';
			 			if (label.vectorStatus == 'New') {
				 			if (lblk.tbl_element == '201')
				 				PrefEntry = MVP_preferences_lookup.filter((x) => { return x.PrefEntry == 'FinishPct1' })[0];
				 			else if (lblk.tbl_element_desc.indexOf('1/2') > -1)
				 				PrefEntry = MVP_preferences_lookup.filter((x) => { return x.PrefEntry == 'FinishPct2' })[0];
				 			else if (lblk.tbl_element_desc.indexOf('3/4') > -1)
				 				PrefEntry = MVP_preferences_lookup.filter((x) => { return x.PrefEntry == 'FinishPct3' })[0];
				 			if (PrefEntry && PrefEntry.PrefValue) {
				 				finArea = (varea * ((parseFloat(PrefEntry.PrefValue)) / 100));
				 				finArea = fixedToOnePoint(finArea);
				 				finArea = finArea.indexOf('.9') > -1? Math.round(finArea): parseInt(finArea);				 			
			 				}
			 			}
			 			else if (label.vectorStatus == 'Modified') {
			 				finArea = label.fin_area;
			 			}
			 			if (finArea) {
			 				perc = ((PrefEntry && PrefEntry.PrefValue)? parseFloat(PrefEntry.PrefValue):  Math.round((finArea/varea) * 100));
	 						unfinArea = Math.round(varea - finArea);
			 			}
			 			table += '<tr class="trow" sid="' + label.sid + '" vid="' + label.sketch.uid + '" lbl="' + label.lookupDetail.tbl_element_desc + '" lvl_code="' + label.lvl_code + '" type="' + label.lookupDetail.tbl_type_code + '" area = "' + varea + '">'
			 			table += '<td>' + label.lookupDetail.tbl_element_desc + '</td><td>' + varea + '</td>';
			 			table += '<td><input type="number" class="fin_area fin_area_value" value="'+ finArea +'"/></td>';
			 			table += '<td><input type="number" class="fin_area fin_area_perc" value="'+ perc +'" /></td>';
			 			table += '<td style="text-align: center;"><span class="unfin_area">' + unfinArea + '</spin></td></tr>';
			 		});
			 		$('.Current_vector_details .dynamic_prop').html(table + '</table>');
			 		$('.fin_area').css({'width': '90px', 'height': '20px'})
			 		$('.fin_area').unbind();
			 		$('.fin_area').bind('keyup change', function(){
			 			var that = this;
			 			var thisVal = $(that).val();
		 				thisVal = thisVal && !isNaN(thisVal)? parseInt(thisVal): 0;
		 				if(isNaN(thisVal))
		 					thisVal=0;
		 				var trow = $(that).parents('.trow')
		 				var area = $(trow).attr('area')
		 				area = area && !isNaN(area)? Math.round(area): 0;
			 			if ($(that).hasClass('fin_area_value')) {
			 				if (thisVal > area) {
			 					$(that).val($(that).val().substring(0,$(that).val().length-1))
			 					return false;
		 					}
		 					else {
		 						var perc = Math.round((thisVal/area) * 100);
		 						$('.fin_area_perc', trow).val(perc);
		 						$('.unfin_area', trow).html(Math.round(area - thisVal));
		 					}
			 			}
			 			if ($(that).hasClass('fin_area_perc')) {
			 				if (thisVal > 100) {
			 					$(that).val($(that).val().substring(0,$(that).val().length-1))
			 					return false;
		 					}
		 					else {
		 						var value = (area * thisVal)/100;
		 						value = fixedToOnePoint(value);
				 				value = value.indexOf('.9') > -1? Math.round(value): parseInt(value);					 				
		 						$('.fin_area_value', trow).val(value);
		 						$('.unfin_area', trow).html(Math.round(area - value));
		 					}
			 			}
			 		});
			   		$('#Btn_Save_vector_properties').unbind(touchClickEvent);
			    	$('#Btn_Save_vector_properties').bind(touchClickEvent, function () {
			        	var sid, vid, lvl_code, type, lookupVal;
			        	$('.trow').each(function(index, trow){
			        		sid = $(trow).attr('sid');
			        		vid = $(trow).attr('vid');
			        		lvl_code = $(trow).attr('lvl_code');
			        		type = $(trow).attr('type');
			        		var t_areas = $(trow).attr('area');
			        		var f_areas = $('.fin_area_value', $(trow)).val();
			        		f_areas = (f_areas && f_areas != "") ? f_areas : '0';
			        		var u_areas = (parseFloat(t_areas) - parseFloat(f_areas)).toString();
			        		
			        		var _lbl_val = $(trow).attr('lbl');
			        		var _specialLabels = false;
			        		if( lvl_code.toString() == '2' && ( _lbl_val == '1-1/2 s' || _lbl_val == '1-3/4 s' || _lbl_val == '2-1/2 s' ) )
			        			_specialLabels = true;
			        		
			        		sketchApp.sketches.filter(function(sk){ return sk.sid == sid })[0].vectors.filter(function(vect){ return vect.uid == vid })[0].labelFields[0].labelDetails.details[lvl_code].fin_area = f_areas;
			        		if( _specialLabels ) {
			        			sketchApp.sketches.filter(function(sk){ return sk.sid == sid })[0].vectors.filter(function(vect){ return vect.uid == vid })[0].mvpData['Level_'+ lvl_code +'_Fin_Area'] = t_areas;
			        			sketchApp.sketches.filter(function(sk){ return sk.sid == sid })[0].vectors.filter(function(vect){ return vect.uid == vid })[0].mvpData['Level_'+ lvl_code +'_Unfin_Area'] = '0';
			        			sketchApp.sketches.filter(function(sk){ return sk.sid == sid })[0].vectors.filter(function(vect){ return vect.uid == vid })[0].mvpData['Level_3_Fin_Area'] = f_areas;
			        			sketchApp.sketches.filter(function(sk){ return sk.sid == sid })[0].vectors.filter(function(vect){ return vect.uid == vid })[0].mvpData['Level_3_Unfin_Area'] = u_areas;
			        		}
			        		else {
			        			sketchApp.sketches.filter(function(sk){ return sk.sid == sid })[0].vectors.filter(function(vect){ return vect.uid == vid })[0].mvpData['Level_'+ lvl_code +'_Fin_Area'] = f_areas;
			        			sketchApp.sketches.filter(function(sk){ return sk.sid == sid })[0].vectors.filter(function(vect){ return vect.uid == vid })[0].mvpData['Level_'+ lvl_code +'_Unfin_Area'] = u_areas;
			        		}
			        	})
			        	$('.Current_vector_details').hide();
			        	$('.mask').hide();
			        	hideKeyboard();
			    		if (callback) callback(true, true);
			    	});	
		 		}
		 		else cancelCallback(true);
		 	
		 		$('#Btn_cancel_vector_properties').unbind(touchClickEvent);
				$('#Btn_cancel_vector_properties').bind(touchClickEvent, function(){ cancelCallback(false); });	    
		 		$('.Current_vector_details').width(440)
			}
			checkAreaEntry();
 		}
 		else if (callback) callback(true, true);	
	} 	

	if (BasementFunctionalitys.length > 0) {
		function BasementFunctionalityss(){
			var basementFunctionalitys = BasementFunctionalitys.pop();
			var html = '',components = [],default_area=[];
			var bfun = basementFunctionalitys.labelDetails.details[1];
			var to_Area = basementFunctionalitys.Area;
			var Vect_uid = basementFunctionalitys.vect_uid;
			var Sid = basementFunctionalitys.sid;
			var skt_type = basementFunctionalitys.skt_type;
			$('.Current_vector_details .head').html('Area Entry');
			$('.dynamic_prop').html('');
			$('.dynamic_prop').append('<div class="divvectors"></div>');
	 		html += '<span style="width: 375px;justify-content: center;font-weight: bold;font-size: 18px;">Enter Below Ground Areas</span>';
	 		if(bfun.comp1!=0)
	 			components.push({comp:bfun.comp1, prefix: bfun.prefix1, modifier:bfun.modifier1});
	 		if(bfun.comp2!=0)
	 			components.push({comp:bfun.comp2, prefix: bfun.prefix2, modifier:bfun.modifier2});
	 		if(bfun.comp3!=0)
	 			components.push({comp:bfun.comp3, prefix: bfun.prefix3, modifier:bfun.modifier3});
	 		if(bfun.comp4!=0)
	 			components.push({comp:bfun.comp4, prefix: bfun.prefix4, modifier:bfun.modifier4});
	 		var i = 0;
	 		html += '<div class="BfunBeforeSave"><table style="border-spacing: 6px;">';
	 		components.forEach(function(com){
	 			i= i+1;
	 			var lookupVal = MVP_Lookup.filter(function(lk){ return lk.tbl_element == com.comp && lk.AdditionalScreen && (lk.AdditionalScreen == "BGAreaEntry" || (lk.AdditionalScreen.split("/")[0] == "BGAreaEntry")) && lk['GradeLevel'] })[0];
	 			var t_area = com.prefix == '1' ? parseInt( 0.25 * basementFunctionalitys.Area) : (com.prefix == '2' ? parseInt( 0.5 * basementFunctionalitys.Area) : parseInt( 0.75 * basementFunctionalitys.Area))
	 			default_area.push(t_area);
	 			if(i == components.length){
	 				html += '<tr><td style="padding-right: 75px;font-weight: bold;font-size: 15px;">'+ lookupVal.tbl_element_desc +'</td>';
		 			html += '<td><input style="width: 100px; height: 20px;" type="number" id='+com.comp+' uid="' + basementFunctionalitys.sketch.uid + '" sid="' + basementFunctionalitys.sid + '" value="'+t_area+'" disabled /></td></tr>';
	 			}
	 			else{
	 				if(com.comp == '202' && com.modifier != ''){
	 					html += '<tr><td style="padding-right: 75px;font-weight: bold;font-size: 15px;">Finished Basement</td>';
		 				html += '<td><input style="width: 100px; height: 20px;" type="number" id="Fin" uid="' + basementFunctionalitys.sketch.uid + '" sid="' + basementFunctionalitys.sid + '" value="'+t_area+'" /></td></tr>';
		 				html += '<tr><td style="padding-right: 75px;font-weight: bold;font-size: 15px;">Unfinished Basement</td>';
		 				html += '<td><input style="width: 100px; height: 20px;" type="number" id="UnFin" uid="' + basementFunctionalitys.sketch.uid + '" sid="' + basementFunctionalitys.sid + '" value="'+0+'"/></td></tr>';
	 				}
	 				else{
	 					html += '<tr ><td style="padding-right: 75px;font-weight: bold;font-size: 15px;">'+ lookupVal.tbl_element_desc +'</td>';
		 				html += '<td><input style="width: 100px; height: 20px;" type="number" id='+com.comp+' uid="' + basementFunctionalitys.sketch.uid + '" sid="' + basementFunctionalitys.sid + '" value="'+t_area+'" /></td></tr>';
	 				}
	 			}
	 		});
	 		html += '</table></div>'
			$('.divvectors').append(html);
			$('.skNote').hide();
			$('#Btn_cancel_vector_properties').html('Defaults ');
			$('.Current_vector_details').show()
			$('.Current_vector_details').width(425)
			$('.BfunBeforeSave input').unbind();
			$('.BfunBeforeSave input').bind('keyup', function(){
  				var input_length = $(".BfunBeforeSave input").length;
  				var area_array = [], tot_area=0, tt_area = 0, last_area = 0;
  				$('.BfunBeforeSave input').forEach(function(Inp){
					area_array.push(Inp.value);
				});
  				for(var i=0; i<area_array.length-1; i++){
  					if(area_array[i] == '')
  						area_array[i] = '0';
  					tot_area = tot_area + Math.round(area_array[i]);
  				}
  				tt_area = to_Area - tot_area;
  				if(tt_area >= 0){
  					last_area = tt_area;
  					$('.BfunBeforeSave input')[input_length-1].value = last_area;
  				}
  				else
  				{	
  					this.value = Math.round(this.value) + tt_area;
  					last_area = 0;
  					$('.BfunBeforeSave input')[input_length-1].value = last_area;
  				}
			});
			$('#Btn_Save_vector_properties').unbind(touchClickEvent);
			$('#Btn_Save_vector_properties').bind(touchClickEvent, function () {
				var sdetails = {}; 
				var vect = sketchApp.sketches.filter(function(sk){ return sk.sid == Sid })[0].vectors.filter(function(vect){ return vect.uid == Vect_uid })[0];
				vect.labelFields[0].labelDetails.base_array = [];
				$('.BfunBeforeSave input').forEach(function(sav){
					sdetails = {Id: sav.id, area: Math.round(sav.value)};
					vect.labelFields[0].labelDetails.base_array.push(_.clone(sdetails));
				});
				$('.Current_vector_details').hide();
				$('#Btn_cancel_vector_properties').html('Cancel ');
				if(BasementFunctionalitys.length > 0)
					BasementFunctionalityss();
				else{
					$('.mask').hide();
					hideKeyboard();
					nBasementFunctionality();
				}
			});
			$('#Btn_cancel_vector_properties').unbind(touchClickEvent);
			$('#Btn_cancel_vector_properties').bind(touchClickEvent, function(){ 
				var k = 0;
				$('.BfunBeforeSave input').forEach(function(Inp){
					if(Inp.id == 'UnFin')
						Inp.value = '0';
					else{
						Inp.value = default_area[k];
						k = k + 1;
					}
				});
			});	 
		}
		$('.mask').show();
		BasementFunctionalityss();
	}
	else
		nBasementFunctionality();
	}
	
  	function RecCretation() {
	    var modified_sketches = sketchApp.sketches.filter(function(skda){ return skda.isModified })
		var new_vectors = [];
		modified_sketches.forEach(function(Md_sketches){
			Md_sketches.vectors.filter(function(vect) { return vect.newRecord && !(vect.exterior_new_reocrd); }).forEach(function(vt) {
                if (vt.isPasteVector && vt.sketchType != 'outBuilding') {
                    var labelDatailsFeat = vt.labelFields[0].labelDetails.details, extfeatAlone = false;
					$.each(labelDatailsFeat,function(det) {
						var lbfeat = labelDatailsFeat[det];
						if((lbfeat.comp1 != 0 && lbfeat.comp1 != '0') || (lbfeat.prefix1 != 0 && lbfeat.prefix1 != '0') || (lbfeat.constr1 != 0 && lbfeat.constr1 != '0'))
							extfeatAlone = true;
					});
					if(!extfeatAlone)
						vt.MapExteriorFeature = true;
                }        
                new_vectors.push(vt);
            });
		});
		new_vectors = new_vectors.reverse();
		function new_vector_record_creation(new_vectors_rec){
        	if(new_vectors_rec.length == 0){
				sketchsavee();
				return false;
            }
			var n_vect_rec = new_vectors_rec.pop();
			var Card_Rowuid = n_vect_rec.sketch.uid, ParentRowCard = n_vect_rec.sketch.parentRow, details = {}, vectorString = n_vect_rec.vectorString;
			var sketch_Type = ParentRowCard.MainBuildingType;
			var DBTable = (sketch_Type == 'D' ? 'CC_Dwelling': 'CC_Building');
			var	DBCatId = getCategoryFromSourceTable(DBTable).Id
			var improvement_Id = (sketch_Type == 'D' ? 'D': 'B');
			if(vectorString == '' || vectorString == null || vectorString == ' '){
				new_vector_record_creation(new_vectors_rec);
				return false;
			}
			
			function MaptoExteriorFeature(n_vect_recss, Card_Rowuid, extCheckedValues) {	
				var ATable = sketch_Type == 'D'  ? 'CC_Addition' : 'CC_Addition_Building';
				if (extCheckedValues == 'StandAlone') {
					var newAddRowuid = ccTicks();
					var labelDatailsFeat = n_vect_recss.labelFields[0].labelDetails.details, ext_feat_code_Value = 0, ext_feat_code_True = false;
					$.each(labelDatailsFeat,function(det) {
						if(det != 1 && det != '1'){
							var lbfeat = labelDatailsFeat[det];
							if(det != 1 && det != '1'){
								var lbfeat = labelDatailsFeat[det];
								if(lbfeat.ext_feat_code1 != 0 && lbfeat.ext_feat_code1 != '0' && lbfeat.ext_feat_code1 != '' && !ext_feat_code_True){
									ext_feat_code_Value = lbfeat.ext_feat_code1;
									ext_feat_code_True = true;
								}
							}
						}
					});
					
					var DBRecord = [];
					DBRecord = activeParcel[DBTable].filter(function(rd){return rd.ParentROWUID == Card_Rowuid});
					var cateeId = getCategoryFromSourceTable(ATable).Id;
					var mLookupData = MVP_Lookup.filter(function(pro){ return (pro.tbl_element == ext_feat_code_Value) });
					var AdditionCode = mLookupData[0] ? mLookupData[0].CodesToSysType: '', RateType = '32757', BaseArea = spArea;
					var AdditionValue,AdditionRCNLD, AdditionID = '0', Rate;
					if(DBRecord.length == 0){
						$('#maskLayer').hide(); 
						messageBox('No parent '+ DBTable +' exists. You must first create a '+ DBTable +' record', function() {
	                        $('#maskLayer').show(); 
	                        insertNewAuxRecord(null, ParentRowCard, DBCatId, null, null,function (DBrowid) {
								var DBPRecord = activeParcel[DBTable].filter(function(rd){return rd.ROWUID == DBrowid})[0];
								insertNewAuxRecord(null, DBPRecord, cateeId, newAddRowuid,{AdditionCode: AdditionCode, RateType: RateType, BaseArea: BaseArea, SegmentID: (newAddRowuid.toString())},function (Addrowid) {
									n_vect_recss.mvpData.Improvement_ID = '{$' + ATable + '#' + Addrowid +'$}';
	        						n_vect_recss.mvpData.Area = spArea;
	        						n_vect_recss.mvpData.Perimeter = spPerimeter;
	        						new_vector_record_creation(new_vectors_rec);
								});
							});
	               		});	
					}
					else{
						insertNewAuxRecord(null, DBRecord[0], cateeId, newAddRowuid,{AdditionCode: AdditionCode, RateType: RateType, BaseArea: BaseArea, SegmentID: (newAddRowuid.toString()) },function (Adrowid) {
							n_vect_recss.mvpData.Improvement_ID = '{$' + ATable + '#' + Adrowid +'$}';
							n_vect_recss.mvpData.Area = spArea;
	        				n_vect_recss.mvpData.Perimeter = spPerimeter;
	        				new_vector_record_creation(new_vectors_rec);
						});
					}
				}
				else {
					 var ADBrowuid = extCheckedValues.split('{')[1].split('}')[0];
					 var tbl = extCheckedValues.split('{')[0];
					 var improvement_Id = '';
				 	 var AdRecord = activeParcel[tbl].filter(function(rd) { return rd.ROWUID == ADBrowuid })[0];
				 	 if (tbl == 'CC_Dwelling' || tbl == 'CC_Building') {
						improvement_Id =  (tbl == 'CC_Dwelling'? 'D': (tbl == 'CC_Building'? 'B': ''));
					 }
				 	 else if (AdRecord) {
				 		if (AdRecord.CC_RecordStatus == 'I' && parseFloat(ADBrowuid) < 0)
				 			improvement_Id = '{$' + tbl + '#' + AdRecord.ROWUID +'$}';
				 		else
				 			improvement_Id = '{' + AdRecord.ROWUID + '}';
				 	 }
			 	 	 n_vect_recss.mvpData.Improvement_ID = improvement_Id;
			 	 	 n_vect_recss.mvpData.Area = spArea;
    				 n_vect_recss.mvpData.Perimeter = spPerimeter;
    				 new_vector_record_creation(new_vectors_rec);
				}
			}
			
			function MaptoExteriorFeaturewindow(n_vect_recss, Card_Rowuid) {
				$('.mask').show();
				var html = '', ext_change_var = 'StandAlone', mis_Im_Type = '155', selectedTable;
				var labelDatailsFeat = n_vect_recss.labelFields[0].labelDetails.details, ext_feat_code_Value = 0, ext_feat_code_True = false;
				$.each(labelDatailsFeat,function(det) {
					if(det != 1 && det != '1'){
						var lbfeat = labelDatailsFeat[det];
						if(lbfeat.ext_feat_code1 != 0 && lbfeat.ext_feat_code1 != '0' && lbfeat.ext_feat_code1 != '' && !ext_feat_code_True){
							ext_feat_code_Value = lbfeat.ext_feat_code1;
							var ext_split_codes = parseInt(ext_feat_code_Value);
							var ext_temps = ext_split_codes % 1000;
							var m_look = MVP_Lookup.filter(function(lkp){ return lkp.tbl_element == ext_temps && lkp.tbl_type_code == 'Exterior Feature'})[0];
							mis_Im_Type = (m_look && m_look.field_1 && m_look.field_1 != '') ? m_look.field_1 : ext_temps;
							ext_feat_code_True = true;
						}
					}
				});	
				
				$('.Current_vector_details .head').html('Exterior Feature Improvement');
				$('.dynamic_prop').html('');
				$('.dynamic_prop').append('<div class="divvectors"></div>');
	 			html += '<span style="width: 460px;font-weight: bold;justify-content: center;font-size: 16px;">Please select the improvement for the exterior feature "'+ mis_Im_Type +'":</span>';
				html += '<span class ="ext_feature_value" style="width: 400px;font-weight: bold; justify-content: center; font-size: 16px;">'+ ext_change_var+'</span>';
				html += '<div class="Ext_features">';
				html += '<span style"float:left;width:300px;font-size: 16px;"><input type="radio" id="standAlone" name="Exterior_Features" value="StandAlone" value1="StandAlone" checked="checked" style="float:left;width:50px;height:20px"><label for="standAlone" style="font-size: 16px;">[StandAlone]</label></span></br>';
				
				var ATable = sketch_Type == 'D'  ? 'CC_Addition' : 'CC_Addition_Building', addCDwellRecords = [];
				selectedTable = ATable;
				var cardRecord = activeParcel['CC_Card'].filter(function(rd) { return rd.ROWUID == Card_Rowuid && rd.CC_Deleted != true })[0];
				var DBRecord = cardRecord[DBTable].filter(function(rd) { return rd.ParentROWUID == Card_Rowuid && rd.CC_Deleted != true })[0];
				if (DBRecord) {
					addCDwellRecords.push(_.clone({ ACDFrecords: DBRecord, soTable: DBTable }));
					/*DBRecord[ATable].filter(function(at) { return at.CC_Deleted != true }).forEach(function(addion) {
						if(addion.SegmentID != null && addion.SegmentID != 'null' && addion.SegmentID != '' && addion.SegmentID != ' ')
							addCDwellRecords.push(_.clone({ ACDFrecords: addion, soTable: ATable }));
					});*/
				}
				
				addCDwellRecords.forEach(function(ACDW) {
					var acdRecord = ACDW.ACDFrecords;
					var tabl = ACDW.soTable;
					var roId = acdRecord.ROWUID;
					var imp_Id = (tabl == 'CC_Dwelling'? 'D': (tabl == 'CC_Building'? 'B': (acdRecord.CC_RecordStatus != 'I'? acdRecord.SegmentID: '')));
					var distext = ':';
					if (imp_Id != 'D' && imp_Id != 'B') {
						var addcode = acdRecord.AdditionCode? acdRecord.AdditionCode: '';
						var mvlk = MVP_Lookup.filter(function(lkp){ return lkp.CodesToSysType == addcode && (lkp.tbl_type_code == 'Exterior Feature' || lkp.tbl_type_code == 'Component') })[0];
						if (addcode && mvlk && mvlk.field_1) {
							distext = ':'+ mvlk.field_1;
						}
						else
							distext = ':' + '155';
					}
					var displayText = (imp_Id == 'D' ? imp_Id + ':DWELL': (imp_Id == 'B'? imp_Id + ':Building': (imp_Id + distext))); 
					var delte_rec = deleteVect_Ext_Feat_Array.filter(function(del_v){ return ((del_v.card_rowid == Card_Rowuid ) && ( del_v.souTable == tabl) && (del_v.Improvement_ID == roId) )})[0];
					if(!delte_rec) {
						html += '<span style"float:left; width:300px; font-size: 16px;"><input type="radio" id="'+ tabl + '{' + acdRecord.ROWUID + '}' +'" name="Exterior_Features" value="' + tabl + '{' + acdRecord.ROWUID + '}' +'" value1 = "'+ displayText +'" style="float:left;width:50px;height:20px;"><label for="'+ tabl + '{' + acdRecord.ROWUID + '}' +'" style="font-size: 16px;">'+ displayText +'</label></span></br>';
					}
				});
				
				html += '</div>'
				$('.divvectors').append(html);
				$('.skNote').hide();
				$('#Btn_cancel_vector_properties').hide();
				$('#Btn_Save_vector_properties').html('OK ');
				$('#Btn_Save_vector_properties').css('margin-right','140px');
				$('.Current_vector_details').show()
				$('.Current_vector_details').width(520);
				$('.Ext_features span').width('300px !important');
				
				$("input[type='radio']").change(function(){
					var chtm = $(this).attr('value1'); 
					//if(chtm && chtm.split(':').length > 1)
					//	chtm = chtm.split(':')[0];
					ext_change_var = chtm;
					$('.ext_feature_value').html(ext_change_var);
					$('.ext_feature_value').show();
				});
				
				$('#Btn_Save_vector_properties').unbind(touchClickEvent);
				$('#Btn_Save_vector_properties').bind(touchClickEvent, function () {
					var extCheckedValue = $("input[name='Exterior_Features']:checked").val();
					$('#Btn_Save_vector_properties').html('Save ');
					$('#Btn_Save_vector_properties').css('margin-right','4px');
					$('#Btn_cancel_vector_properties').show();
					$('.Current_vector_details').hide();
					$('.mask').hide();
					MaptoExteriorFeature(n_vect_recss, Card_Rowuid, extCheckedValue);
					return false;
				});
			}
			
			var spArea = n_vect_rec.isUnSketchedTrueArea? Math.round(n_vect_rec.fixedAreaTrue): n_vect_rec.area();
			var spPerimeter = n_vect_rec.isUnSketchedTrueArea? Math.round(n_vect_rec.fixedPerimeterTrue): n_vect_rec.perimeter();
			if(n_vect_rec.MapExteriorFeature){
				MaptoExteriorFeaturewindow(n_vect_rec, Card_Rowuid);
				return false;
			}
			
			var imp_detail_array = [];
			if(n_vect_rec.labelFields[0].labelDetails){
				if(n_vect_rec.labelFields[0].labelDetails.details){
					details = n_vect_rec.labelFields[0].labelDetails.details;			
					$.each(details,function(det){
						imp_detail_array.push( _.clone(details[det]));
					});
				}
			}
			imp_detail_array = imp_detail_array.reverse();
			var j = 0;
			function new_vector_record(imp_detail_arrays){ 
				if(imp_detail_arrays == 0){
					new_vector_record_creation(new_vectors_rec);
					return;
				}
				var imp_detail_arr = imp_detail_arrays.pop();j++;
				if((imp_detail_arr.comp1 != 0 && imp_detail_arr.comp1 != '0') || (imp_detail_arr.prefix1 != 0 && imp_detail_arr.prefix1 != '0') || (imp_detail_arr.constr1 != 0 && imp_detail_arr.constr1 != '0' && imp_detail_arr.constr1 != '') || (imp_detail_arr.ext_cover1 != '' && imp_detail_arr.ext_cover1 !='0') || (imp_detail_arr.modifier1 != '' && imp_detail_arr.modifier1 != '0') || (imp_detail_arr.ext_feat_code1 != 0 && imp_detail_arr.ext_feat_code1 != '0')){
					var isAdditionRecord = false;
					if(j == 2 && imp_detail_arr.comp1 && (imp_detail_arr.comp1 != 0) && (imp_detail_arr.comp1 !='0') && (imp_detail_arr.comp1 !='')){
						isAdditionRecord = isAdditionSketch(imp_detail_arr);
						if (isAdditionRecord)
							n_vect_rec.isAdditionRecord = true;
					}
					
					let skipRCreation =  true;
					if (j == 3 && n_vect_rec.isAdditionRecord && ((imp_detail_arr.prefix1 && imp_detail_arr.prefix1 != '0') || (imp_detail_arr.comp1 == '201' && (imp_detail_arr['modifier1'] == "Fin" || imp_detail_arr['modifier1'] == "UF"))) ) {
						skipRCreation = false;
					}
					
					var sTable = isAdditionRecord ? (sketch_Type == 'D'  ? 'CC_Addition' : 'CC_Addition_Building') : (sketch_Type == 'D' ? 'CC_Dwelling': 'CC_Building');
					var dRecord = [];
					dRecord = activeParcel[DBTable].filter(function(rd){return rd.ParentROWUID == Card_Rowuid});
					var cateeId = getCategoryFromSourceTable(sTable).Id;
					dRecord = dRecord ? dRecord :[];
					if (skipRCreation) {
						if(dRecord.length > 0 && !isAdditionRecord){
							n_vect_rec.mvpData.Improvement_ID = improvement_Id;
							n_vect_rec.mvpData.Area = spArea;
	        				n_vect_rec.mvpData.Perimeter = spPerimeter;
							new_vector_record(imp_detail_arrays);
						}
						else{
							if(isAdditionRecord){
								var mLookupData = MVP_Lookup.filter(function(pro){ return (pro.tbl_element == imp_detail_arr.comp1) });
								var AdditionCode = mLookupData[0].CodesToSysType, RateType = '32757', BaseArea = spArea;
								//var AdditionValue,AdditionRCNLD, var AdditionID = '0', Rate;
								var newAddRowuid = ccTicks();
								if(dRecord.length == 0){
									$('#maskLayer').hide(); 
									messageBox('No parent '+ DBTable +' exists. You must first create a '+ DBTable +' record', function() {
	                        			$('#maskLayer').show(); 
	                        			insertNewAuxRecord(null, ParentRowCard, DBCatId, null, null,function (DBrowid) {
											var DBPRecord = activeParcel[DBTable].filter(function(rd){return rd.ROWUID == DBrowid})[0];
											insertNewAuxRecord(null, DBPRecord, cateeId, newAddRowuid,{AdditionCode: AdditionCode, RateType: RateType, BaseArea: BaseArea, SegmentID: (newAddRowuid.toString())},function (Addrowid) {
												n_vect_rec.mvpData.Improvement_ID = '{$' + sTable + '#' + Addrowid +'$}';
	        									n_vect_rec.mvpData.Area = spArea;
	        									n_vect_rec.mvpData.Perimeter = spPerimeter;
	        									new_vector_record(imp_detail_arrays);
											});
										});
	               			 		});	
								}
								else{
									insertNewAuxRecord(null, dRecord[0], cateeId, newAddRowuid,{AdditionCode: AdditionCode, RateType: RateType, BaseArea: BaseArea, SegmentID: (newAddRowuid.toString()) },function (Adrowid) {
										n_vect_rec.mvpData.Improvement_ID = '{$' + sTable + '#' + Adrowid +'$}';
										n_vect_rec.mvpData.Area = spArea;
	        							n_vect_rec.mvpData.Perimeter = spPerimeter;
	        							new_vector_record(imp_detail_arrays);
									});
								}
							}
							else{
								insertNewAuxRecord(null, ParentRowCard, cateeId, null, null, function (rowid) {
									n_vect_rec.mvpData.Improvement_ID = improvement_Id;
									n_vect_rec.mvpData.Area = spArea;
	        						n_vect_rec.mvpData.Perimeter = spPerimeter;
	        						new_vector_record(imp_detail_arrays);
								});
							}
						}
					}
					else	
						new_vector_record(imp_detail_arrays);					
				}
				else
					new_vector_record(imp_detail_arrays);
			}
        if(n_vect_rec.sketchType == 'outBuilding'){
            var out_label_Code = n_vect_rec.mvpData['Outbuilding_Label_Code'], ParentRowCard = n_vect_rec.sketch.parentRow, OI_SegIDField = getDataField('SegmentID','CC_OtherImprovement'), cateId = getCategoryFromSourceTable('CC_OtherImprovement').Id;            
            var OI_imptype = MVP_Outbuliding_Lookup.filter(function (OI){return OI.label_code == out_label_Code})[0] ? MVP_Outbuliding_Lookup.filter(function (OI){return OI.label_code == out_label_Code})[0].label_text : '';
            var CostFactor = '100', RateType = '32757', SegmentID, UnitCount = '1', OIFeatureRate = '0', TotalArea = '0', OtherImprovementCode = OI_imptype;            
            insertNewAuxRecord(null, ParentRowCard, cateId,null,{CostFactor: CostFactor, RateType: RateType, UnitCount: UnitCount, OIFeatureRate: OIFeatureRate, TotalArea: TotalArea, OtherImprovementCode: OtherImprovementCode},function (rowid) {
                var parentROEW=activeParcel?activeParcel['CC_OtherImprovement'].filter(function(OI_record){return OI_record.ROWUID==rowid})[0]:[];
                n_vect_rec.mvpData.Improvement_ID = '{$CC_OtherImprovement#'+ rowid +'$}';
                update_data(OI_SegIDField,rowid, parentROEW,function(){
                    new_vector_record(imp_detail_array);
                });                                
            });
        }  
		else
			new_vector_record(imp_detail_array);
	}
	new_vector_record_creation(new_vectors);			
	   
  }
  	$('#maskLayer').show(); 

	sketchApp.deletedVectors.filter((v) => { return !(v.newRecord && v.isPasteVector) }).forEach(function(deletVect) {
  		var card_rowid = (deletVect.islabelEditedTrue) ? deletVect.eEid: deletVect.sketch.uid;
  		var currentSketch = sketchApp.sketches.filter(function(dk) { return dk.uid == card_rowid })[0];
  		var sketch_Type = currentSketch.parentRow.MainBuildingType;
  	    var DBTable = (sketch_Type == 'D' ? 'CC_Dwelling': 'CC_Building');
  	    var ATable = sketch_Type == 'D'  ? 'CC_Addition' : 'CC_Addition_Building';
  		var delVectuid = deletVect.uid;
  		var card_record =  activeParcel['CC_Card'].filter(function(x) { return x.ROWUID == card_rowid && x.CC_Deleted != true })[0];
  		var dRecord = card_record[DBTable].filter(function(x) { return x.CC_Deleted != true })[0];
  		var dRowuid = dRecord? dRecord.ROWUID: null;
  		var sketchVectors = currentSketch.vectors.filter(function(vk) { return vk.sketchType != 'outBuilding' });
		if(deletVect.sketchType != 'outBuilding') {
			var Improvement_ID = deletVect.mvpData.Improvement_ID, copyImprovement_ID = deletVect.mvpData.Improvement_ID, findDBvectors = false, skipMapping = false;
			if (Improvement_ID && Improvement_ID != '') {
				var matchedCount = 0;
				if (Improvement_ID.indexOf('{') > -1) {
					if (Improvement_ID.indexOf('#') > -1) 
						Improvement_ID = Improvement_ID.split('#')[1].split('$')[0];
					else
						Improvement_ID = Improvement_ID.split('{')[1].split('}')[0];
				}
				var addRec = null;	
				if (Improvement_ID == 'D' || Improvement_ID == 'B' || Improvement_ID == dRowuid) {
					// (x in sketchVectors) {
					//	var vect = sketchVectors[x];
					//	var vectorMvpData = vect.mvpData;
					//	if (vectorMvpData.Improvement_ID == 'D' || vectorMvpData.Improvement_ID == 'B' || vectorMvpData.Improvement_ID == dRowuid) {
							skipMapping = true;
							//break;
					//	}
					//}
				}
				else {
					addRec = dRecord[ATable].filter(function(x) { return x.CC_Deleted != true && (x.SegmentID == Improvement_ID || x.ROWUID == Improvement_ID)} )[0];
					if (addRec)
						Improvement_ID = addRec.ROWUID;
				}
				
				if (!skipMapping) {
					sketchVectors.forEach(function(vector) {
						var vectorMvpData = vector.mvpData;
						var imp_id = vectorMvpData.Improvement_ID, copy_imp_id = vectorMvpData.Improvement_ID;
						if (imp_id.indexOf('{') > -1) {
							if (imp_id.indexOf('#') > -1) 
								imp_id = imp_id.split('#')[1].split('$')[0];
							else
								imp_id = imp_id.split('{')[1].split('}')[0];
						}
						if (imp_id != 'D' && imp_id != 'B' && imp_id != dRowuid) {
							var addnRec = dRecord[ATable].filter(function(x) { return x.CC_Deleted != true && (x.SegmentID == imp_id || x.ROWUID == imp_id)} )[0];
							if(addnRec && addnRec.ROWUID == Improvement_ID) {
								var cvectDetails = vector.labelFields[0].labelDetails.details;
								var imp_type = '';
								$.each(cvectDetails,function(cvectdet) {
									if(cvectdet != 1 && cvectdet != '1') {
										var lbcvectdet = cvectDetails[cvectdet];
										if(lbcvectdet.ext_feat_code1 != 0 && lbcvectdet.ext_feat_code1 != '0' && lbcvectdet.ext_feat_code1 != ''){
											var ext_feat_code_Value = lbcvectdet.ext_feat_code1;
											var ext_split_codes = parseInt(ext_feat_code_Value);
											var ext_temps = ext_split_codes % 1000;
											var m_look = MVP_Lookup.filter(function(lkp){ return lkp.tbl_element == ext_temps && lkp.tbl_type_code == 'Exterior Feature'})[0];
											imp_type = (m_look && m_look.field_1 && m_look.field_1 != '') ? m_look.field_1 : ext_split_codes;
										}
									}
								});	
								matchedCount = matchedCount + 1;
								delete_Ext_Feat_window_show.push(_.clone({ card_rowid: card_rowid, vectuid: vector.uid, souTable: ATable, Improvement_ID: addnRec.ROWUID, imp_type: imp_type }));
							}
						}
					});
					
					//var addRec = dRecord[ATable].filter(function(x) { return x.CC_RecordStatus != true && (x.SegmentID == Improvement_ID || x.ROWUID == Improvement_ID)} )[0];
					if (addRec)
						deleteVect_Ext_Feat_Array.push(_.clone({ card_rowid: card_rowid, souTable: ATable, Improvement_ID: addRec.ROWUID }));
				}					
			}
		}
  	});

	function delete_exterior_window(delete_Ext_Feat_window) {
		if(delete_Ext_Feat_window.length == 0){
   			RecCretation();
   			return;
   		}
   		
   		var delete_window = delete_Ext_Feat_window.pop();
   		var curSketch = sketchApp.sketches.filter(function(dk) { return dk.uid == delete_window.card_rowid })[0];
  		var curVector = curSketch.vectors.filter(function(vk) { return vk.uid == delete_window.vectuid })[0];
	    var card_rec = activeParcel['CC_Card'].filter(function(sk){ return (sk.ROWUID == delete_window.card_rowid); })[0];
		var card_type = card_rec.MainBuildingType, addCDwellRecords = [];
		var DBTable = (card_type == 'D' ? 'CC_Dwelling': 'CC_Building');
	    var ATable = card_type == 'D'  ? 'CC_Addition' : 'CC_Addition_Building';
	    
		function changeExterior(n_vect_recss, extCheckedValue, card_rowuid, value_select){
			var spArea = n_vect_recss.isUnSketchedTrueArea? Math.round(n_vect_recss.fixedAreaTrue): n_vect_recss.area(), spPerimeter = n_vect_recss.isUnSketchedTrueArea? Math.round(n_vect_recss.fixedPerimeterTrue): n_vect_recss.perimeter();
			if(extCheckedValue == 'StandAlone'){
				var newAddRowuid = ccTicks();
				var labelDatailsFeat = n_vect_recss.labelFields[0].labelDetails.details, ext_feat_code_Value = 0, ext_feat_code_True = false;
				$.each(labelDatailsFeat,function(det) {
					if(det != 1 && det != '1'){
						var lbfeat = labelDatailsFeat[det];
						if(det != 1 && det != '1'){
							var lbfeat = labelDatailsFeat[det];
							if(lbfeat.ext_feat_code1 != 0 && lbfeat.ext_feat_code1 != '0' && lbfeat.ext_feat_code1 != '' && !ext_feat_code_True){
								ext_feat_code_Value = lbfeat.ext_feat_code1;
								ext_feat_code_True = true;
							}
						}
					}
				});
				
				var DBRecord = [];
				DBRecord = activeParcel[DBTable].filter(function(rd){return rd.ParentROWUID == card_rowuid});
				var cateeId = getCategoryFromSourceTable(ATable).Id;
				var mLookupData = MVP_Lookup.filter(function(pro){ return (pro.tbl_element == ext_feat_code_Value) });
				var AdditionCode = mLookupData[0] ? mLookupData[0].CodesToSysType: '', RateType = '32757', BaseArea = spArea;
				var AdditionValue,AdditionRCNLD, AdditionID = '0', Rate;
				if(DBRecord.length == 0){
					$('#maskLayer').hide(); 
					messageBox('No parent '+ DBTable +' exists. You must first create a '+ DBTable +' record', function() {
                        $('#maskLayer').show(); 
                        insertNewAuxRecord(null, ParentRowCard, DBCatId, null, null,function (DBrowid) {
							var DBPRecord = activeParcel[DBTable].filter(function(rd){return rd.ROWUID == DBrowid})[0];
							insertNewAuxRecord(null, DBPRecord, cateeId, newAddRowuid,{AdditionCode: AdditionCode, RateType: RateType, BaseArea: BaseArea, SegmentID: (newAddRowuid.toString())},function (Addrowid) {
								n_vect_recss.mvpData.Improvement_ID = '{$' + ATable + '#' + Addrowid +'$}';
        						n_vect_recss.mvpData.Area = spArea;
        						n_vect_recss.mvpData.Perimeter = spPerimeter;
        						delete_exterior_window(delete_Ext_Feat_window);
							});
						});
               		});	
				}
				else{
					insertNewAuxRecord(null, DBRecord[0], cateeId, newAddRowuid,{AdditionCode: AdditionCode, RateType: RateType, BaseArea: BaseArea, SegmentID: (newAddRowuid.toString()) },function (Adrowid) {
						n_vect_recss.mvpData.Improvement_ID = '{$' + ATable + '#' + Adrowid +'$}';
						n_vect_recss.mvpData.Area = spArea;
        				n_vect_recss.mvpData.Perimeter = spPerimeter;
        				delete_exterior_window(delete_Ext_Feat_window);
					});
				}
			}
			else if(extCheckedValue == 'Select_improvements'){
				 var ADBrowuid = value_select.split('{')[1].split('}')[0];
				 var tbl = value_select.split('{')[0];
				 var improvement_Id = '';
				 var AdRecord = activeParcel[tbl].filter(function(rd) { return rd.ROWUID == ADBrowuid })[0];
				 if (tbl == 'CC_Dwelling' || tbl == 'CC_Building') {
					improvement_Id =  (tbl == 'CC_Dwelling'? 'D': (tbl == 'CC_Building'? 'B': ''));
				 }
				 else if (AdRecord) {
			 		if (AdRecord.CC_RecordStatus == 'I' && parseFloat(ADBrowuid) < 0)
			 			improvement_Id = '{$' + tbl + '#' + AdRecord.ROWUID +'$}';
			 		else
			 			improvement_Id = '{' + AdRecord.ROWUID + '}';
				 }
				 n_vect_recss.mvpData.Improvement_ID = improvement_Id;
				 n_vect_recss.mvpData.Area = spArea;
				 n_vect_recss.mvpData.Perimeter = spPerimeter;
				 delete_exterior_window(delete_Ext_Feat_window);
			}
			else if(extCheckedValue == 'DeleteExterior') {
				var cvi = curSketch.vectors.indexOf(n_vect_recss);
            	sketchApp.deletedVectors.push(n_vect_recss);
            	sketchApp.sketches.filter(function(dk){ return dk.uid == card_rowuid })[0].vectors.splice(cvi, 1);
            	delete_exterior_window(delete_Ext_Feat_window);
			}
  		}

		function deleteExteriorFeature(n_vect, card_uid, imp_dd, imp_tp) {
			var html = '', ext_change_var = 'StandAlone', valCondition = '';
			$('.Current_vector_details .head').html('Exterior Feature(S) On Deleted Improvement');
			$('.dynamic_prop').html('');
			$('.dynamic_prop').append('<div class="divvectors"></div>');
	 		html += '<span style="width: 500px;justify-content:font-weight: bold;justify-content: center;font-size: 15px;">The deleted Improvement  has Exterior Feature(S) attached to it: ' + imp_tp +'</span>';
			html += '<span style="width: 500px;justify-content:font-weight: bold; justify-content: center; font-size: 15px;">How do you want handle the attached Exterior Feature(s)</span>';
			html += '<div class="Ext_features_delete">';
			html += '<div style="float:left;width:400px;font-size: 15px"><table><tr><td><input type="radio" id="Select_improvements" name="Exterior_Features_delete" value="Select_improvements" style="float:left;width:50px;height:20px"><label for="Select_improvements" style="font-size: 16px;">Select improvements</label></td><td><select style="width: 160px;" type="improvements" class="class_select_improvement" disabled><option value="0">Select improvements</option>';

			var DBRecord = card_rec[DBTable].filter(function(rd) { return rd.ParentROWUID == card_uid && rd.CC_Deleted != true })[0];
			if (DBRecord) {
				addCDwellRecords.push(_.clone({ ACDFrecords: DBRecord, soTable: DBTable }));
				/*DBRecord[ATable].filter(function(at) { return at.CC_Deleted != true }).forEach(function(addion) {
					if(addion.SegmentID != null && addion.SegmentID != 'null' && addion.SegmentID != '' && addion.SegmentID != ' ')
						addCDwellRecords.push(_.clone({ ACDFrecords: addion, soTable: ATable }));
				});*/
			}
			
			addCDwellRecords.forEach(function(ACDW) {
				var acdRecord = ACDW.ACDFrecords;
				var tabl = ACDW.soTable;
				var rowId = acdRecord.ROWUID;
				var imp_Id = (tabl == 'CC_Dwelling'? 'D': (tabl == 'CC_Building'? 'B': (acdRecord.CC_RecordStatus != 'I'? acdRecord.SegmentID: '')));
				var distext = ':';
				if (imp_Id != 'D' && imp_Id != 'B') {
					var addcode = acdRecord.AdditionCode? acdRecord.AdditionCode: '';
					var mvlk = MVP_Lookup.filter(function(lkp){ return  lkp.CodesToSysType == addcode && (lkp.tbl_type_code == 'Exterior Feature' || lkp.tbl_type_code == 'Component') })[0];
					if (addcode && mvlk && mvlk.field_1) {
						distext = ':'+ mvlk.field_1;
					}
					else
						distext = ':' + '155';
				}
				var displayText = (imp_Id == 'D' ? imp_Id + ':DWELL': (imp_Id == 'B'? imp_Id + ':Building': (imp_Id + distext))); 
				var delte_rec = deleteVect_Ext_Feat_Array.filter(function(del_v){ return ((del_v.card_rowid == card_uid ) && ( del_v.souTable == tabl) && (del_v.Improvement_ID == rowId) )})[0];
				if(!delte_rec) {
					html += '<option value="' + tabl + '{' + acdRecord.ROWUID + '}' + '" value1="' + displayText + '">' + displayText + '</option>';
				}
			});
			
    		html += '</selected></td></tr></table></div>';
			html += '<div style="float:left;width:300px;font-size: 15px"><input type="radio" id="Stand_Alone" name="Exterior_Features_delete" value="StandAlone"  style="float:left;width:50px;height:20px"><label for="Stand_Alone" style="font-size: 16px;">Stand Alone</label></div>';
			html += '<div style="float:left;width:300px;font-size: 15px"><input type="radio" id="DeleteExterior" name="Exterior_Features_delete" value="DeleteExterior"  style="float:left;width:50px;height:20px"><label for="DeleteExterior" style="font-size: 16px;">Delete</label></div>';
			html += '<span class="ext_validation" style="width: 400px;font-weight: bold;margin-left: 68px;display: none; font-size: 15px;">'+ valCondition +'</span>';
			html += '</div>'
			$('.divvectors').append(html);
			$('.skNote').hide();
			$('#Btn_cancel_vector_properties').hide();
			$('#Btn_Save_vector_properties').html('OK ');
			$('#Btn_Save_vector_properties').css('margin-right','180px');
			$('.Current_vector_details').show()
			$('.Current_vector_details').width(550);
			if(addCDwellRecords && addCDwellRecords.length <= 0)
				$('#Select_improvements').attr('disabled','disabled');	
			$("input[type='radio'], .class_select_improvement").change(function(){
				$('.ext_validation').hide();
				var chtm = $("input[name='Exterior_Features_delete']:checked").val(); 
				if(chtm == 'Select_improvements')
					$('.class_select_improvement').removeAttr( 'disabled' );
				else
					$('.class_select_improvement').attr('disabled','disabled');
			});
			$('#Btn_Save_vector_properties').unbind(touchClickEvent);
			$('#Btn_Save_vector_properties').bind(touchClickEvent, function () {
				var extCheckedValue = $("input[name='Exterior_Features_delete']:checked").val();
				var select_val, selct_val1;
				if(extCheckedValue){
					if(extCheckedValue == 'Select_improvements'){
						select_val = $('.class_select_improvement').val();
						if(select_val == '0'){
							valCondition = 'Please Choose Improvements From DropDown';
							$('.ext_validation').html(valCondition);
							$('.ext_validation').show();
							return;
						}	
					}
				}
				else{
					valCondition = 'Please Choose Any Option';
					$('.ext_validation').html(valCondition);
					$('.ext_validation').show();
					return;
				}
				$('#Btn_Save_vector_properties').html('Save ');
				$('#Btn_Save_vector_properties').css('margin-right','4px');
				$('#Btn_cancel_vector_properties').show();
				$('.Current_vector_details').hide();
				$('.mask').hide();
				changeExterior(n_vect, extCheckedValue, card_uid, select_val);
				return false;
			});
			$('.mask').show();
		}
		deleteExteriorFeature(curVector, delete_window.card_rowid, delete_window.Improvement_ID, delete_window.imp_type);
	}
	delete_exterior_window(delete_Ext_Feat_window_show);
}