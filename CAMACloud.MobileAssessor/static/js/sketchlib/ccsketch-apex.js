﻿
CAMACloud.Sketching.Formatters.Apex = {
    name: "Apex",
    saveAllSegments: true,
    allowSegmentAddition: false,
    allowSegmentDeletion: false,
    arcMode: 'ARC',
    vectorSeperator: ";",
    allowSegmentAdditionOnMidsketch: true,
    newVectorHeaderSuffix: '[-1,-1]:',
    nodeToString: function (n) {
        var str = "";
        if (n.arcLength > 0) {
            str += "A" + sRound(n.arcLength);
        }
        if (n.arcLength < 0) {
            str += "B" + Math.abs(sRound(n.arcLength));
        }
        if (n.dy > 0) {
            if (n.isStart) {
                if (str != "") str += " ";
            } else {
                if (str != "") str += "/";
            }
            str += n.sdy + sRound(n.dy);
        }
        if (n.dx > 0) {
            if (n.isStart) {
                if (str != "") str += " ";
            } else {
                if (str != "") str += "/";
            }
            str += n.sdx + sRound(n.dx);
        }
        if (n.hideDimensions) {
            if (str != "") str += "/";
            str += "#";
        }
        if (n.isStart) {
            str += " S";
        }
        return str;
    },
    vectorToString: function (v) {
        var str = "";
        if (v.startNode != null) {
            if (!v.labelEdited && !v.newRecord) {
                if (v.header) {
                    str += v.header;
                } else {
                    if (v.referenceIds) {
                        str += v.name + "[" + v.referenceIds + "]:";
                    } else {
                        if (v.rowId)
                            str += v.name + "[" + v.rowId + ",-1]:";
                        else
                            str += v.name + "[-1,-1]:";
                    }
                };
            }
            else{

                if (v.newRecord) {
                    str += v.name + "[-1," + (v.sectionNum ? v.sectionNum : '-1') + "," + this.labelPositionToString( v.labelPosition ) + ",-1," + v.code + "," + ( v.areaMultiplier ? v.areaMultiplier : 1 ) + "," + ( v.linkedUniqueId ? v.linkedUniqueId : -1 ) + "]:";
                }
                else {
                    if (!v.referenceIds || v.referenceIds == null || v.referenceIds == "null" || v.referenceIds == "") return;
                    var t = v.referenceIds.split(',')
                    if (t.length > 3)
                        str += v.name + "[" + t[0] + "," + t[1] + "," + this.labelPositionToString( v.labelPosition ) + "," + ( v.uniqueId ? v.uniqueId : "" ) + "," + v.code + "," + ( v.areaMultiplier ? v.areaMultiplier : 1 ) + "," + ( v.linkedUniqueId ? v.linkedUniqueId : -1 ) + "]:";
                    else
                        str += v.name + "[" + v.referenceIds + "]:";
                }
            }
            str += v.startNode.vectorString();
            var nn = v.startNode.nextNode;
            while (nn != null) {
                str += " ";
                str += nn.vectorString();
                nn = nn.nextNode;
            }
        }
        return str;
    },
    vectorFromString: function (editor, s) {
        if ((s || '').trim() == '') {
            var nv = new Vector(editor);
            nv.isClosed = false;
            nv.label = "Blank"
            nv.uid = -1;
            return nv;
        }

        var o = editor.origin;
        var p = o.copy();

        var v = new Vector(editor);
        this.updateVectorFromString(editor, v, s);
        v.isModified = false;
        return v;
    },
    updateVectorFromString: function (editor, v, s) {
        v.startNode = null;
        v.endNode = null;
        var o = editor.origin;
        var p = o.copy();
        var hi = s.split(':');
        var head = hi[0];
        var lineString = hi[1] || '';
        v.header = head + ":";
        var l = head.split('[')[1] ? head.split('[')[1].match(/[UDLR][0-9.]+/g) : [];
        if (l && l.length == 2)
            v.labelPosition = this.labelPositionFromString(editor, l.join(''))
        var isStart = false;
        var hasStarted = false;
        var isVeer = false;
        var veerSteps = 0;
        var nodeStrings = lineString.split(' ');
        for (var i in nodeStrings) {
            var arcLength = 0;
            var hideDimensions = false;
            var ns = nodeStrings[i];
            if (ns.trim() != '') {
                var cmds = ns.split('/');
                for (var c in cmds) {
                    var cmd = cmds[c];
                    if (cmd == '') continue;
                    if (cmd == 'S') {
                        isStart = true;
                    } else {
                        var cp = cmd.match(/[ABUDLR#]|[0-9]+(.[0-9]+)?/g);
                        var dir = cp[0];
                        distance = cp[1] || 0;
                        var pix = CAMACloud.Sketching.Utilities.toPixel(distance);
                        switch (dir) {
                            case "A": arcLength = Number(distance); break;
                            case "B": arcLength = -Number(distance); break;
                            case "U": p.moveBy(0, pix); break;
                            case "D": p.moveBy(0, -pix); break;
                            case "L": p.moveBy(-pix, 0); break;
                            case "R": p.moveBy(pix, 0); break;
                            case "#": hideDimensions = true; break;
                        }
                    }
                }
                p = p.copy();

                if (isStart) {
                    v.start(p.copy());
                    hasStarted = true;
                    isStart = false;
                } else if (hasStarted) {
                    var q = p.copy().alignToGrid(editor);
                    if (!v.startNode.overlaps(q))
                        v.connect(p.copy());
                    else
                        v.terminateNode(v.startNode);
                }

                if (v.endNode) {
                    v.endNode.hideDimensions = hideDimensions;
                    v.endNode.arcLength = arcLength;
                    if (arcLength != 0)
                        v.endNode.isArc = true;
                }

            }
        }

        if (v.startNode)
            if (v.startNode.overlaps(v.endNode.p.copy()))
                v.isClosed = true;
        return v;
    },
    labelPositionFromString: function (editor, l) {
        var ins = l.match(/[UDLR]|[0-9.]*/g);
        if (ins.length > 3) {
            var x = ins[2] == "R" ? ins[3] : -ins[3];
            var y = ins[0] == "U" ? ins[1] : -ins[1];
            return new PointX(parseFloat(x), (parseFloat(y)))
        }

    },
    labelPositionToString: function (p) {
        var x = p.x > 0 ? "R" + sRound( p.x ) : "L" + sRound (- p.x);
        var y = p.y > 0 ? "U" + sRound( p.y ) : "D" + sRound (- p.y);
        return y + ',' + x
    },
    open: function (editor, data) {
        editor.vectors = [];
        editor.sketches = [];
        for (var x in data) {
            var sketch = new Sketch(editor);
            var s = data[x];
            sketch.parentRow = s.parentRow;
            sketch.uid = s.uid;
            sketch.label = s.label;
            sketch.vectors = [];
            sketch.config = s.config || {};
            sketch.sid = s.sid;
            sketch.lookUp = s.lookUp;
            sketch.isShowSectNum = s.isShowSectNum;
            sketch.maxNotelen = s.maxNotelen;
            var counter = 0;
            for (var i in s.sketches) {
                var sk = s.sketches[i];
                var segments = (sk.vector || '').split(';');
                var scount = 0;
                if (segments.length < 2) scount = -1;
                for (var si in segments) {
                    counter += 1;
                    scount += 1;
                    var sv = segments[si];
                    var v = this.vectorFromString(editor, sv);
                    var name_element = sk.label.filter(function(lbl){ return lbl.Target == 'name' })[0];
                    v.sketch = sketch;
                    v.uid = sk.uid + (scount ? '/' + scount : '');
                    v.index = counter;
                    v.name = name_element ? name_element.Value : ( sk.label[0].NameValue ? sk.label[0].NameValue : sk.label[0].Value );
                    if (sk.label[0] && sk.label[0].colorCode) {
                        var clrcode = sk.label[0].colorCode.split('~');
                        v.colorCode = clrcode[0]? clrcode[0]: null;
	                    v.newLineColor = (clrcode[1] && sketchSettings['SketchLineColorCustomizations'] && sketchSettings['SketchLineColorCustomizations'].contains('MA'))? clrcode[1]: null;
	                    v.newLabelColorCode = (clrcode[2] && sketchSettings['SketchTextColorCustomizations'] && sketchSettings['SketchTextColorCustomizations'].contains('MA'))? clrcode[2]: null;
                    }
                    v.label = '[' + counter + '] ' + sk.label.filter( function ( a ) { return !a.hiddenFromShow } ).map( function ( a ) { return ( ( a.Description && !a.DoNotShowLabelDescription && sketchSettings["DoNotShowLabelDescriptionSketch"] != '1' && clientSettings["DoNotShowLabelDescriptionSketch"] != '1' && a.Description.Name ) ? ( ( sketchSettings["DoNotShowLabelCode"] == '1' || editor.formatter.showLabelDescriptionOnly || a.showLabelDescriptionOnly || clientSettings.DoNotShowLabelCodeSketch == '1' ) ? a.Description.Name : a.Value + '-' + a.Description.Name ) : a.Value ) } ).join( sk.LabelDelimiter )
                    v.labelFields = sk.label;
                    v.vectorString = sv;
                    v.referenceIds = sk.referenceIds;
                    var ridParts = (sk.referenceIds || '').split(",");
                    v.vectorConfig = sk.vectorConfig;
                    if (ridParts.length > 5) {
                        v.code = ridParts[5];
                        if(sketch.config.Specific_Identifier_00_Code && sketch.config.Specific_Identifier_00_Code == true){                        	
                        	if(v.code.substring(0,2)== '00'){
                        		v.label =v.label.endsWith('**')? v.label: v.label+'**';
                        	}
                        }
                    }
                    v.areaMultiplier = sk.otherValues.areaMultiplier;
                    v.uniqueId = sk.otherValues.uniqueId;
                    v.linkedUniqueId = sk.otherValues.linkedUniqueId;
                    v.tableRowId = sk.otherValues.tableRowId;
                    v.sectionNum = sk.otherValues.sectionNum;
                    if(sketch.isShowSectNum && v.sectionNum){
                    	if(v.sectionNum.split('{').length < 2)
                    		v.label = v.label + ' (S:'	+ v.sectionNum + ')';
                    }
                    if ( v.areaMultiplier == "-1" )
                        v.calculationType = 'negative'
                    if ( v.linkedUniqueId && v.linkedUniqueId != '' && v.linkedUniqueId > -1 )
                        v.calculationType = 'auto'
                    v.rowId = sk.rowId;
                    v.isChanged = sk.isChanged;
                    editor.vectors.pop();
                    sketch.vectors.push(v);
                }
            }
            sketch.isModified = false;
            editor.loadNotesForSketch(sketch, s.notes);
            editor.sketches.push(sketch);
        }
    },
    getParts: function (vectorString) {
        var p = [];
        vectorString = (vectorString || '');
        if ( vectorString.includes('"JSON":') || vectorString.includes('"GlobalSettings":') )
            throw 'Invalid Sketch String!';
        var parts = (vectorString || '').split(";");
        for (var i in parts) {
            var part = parts[i];
            if (/(.*?):(.*?)/.test(part)) {
                var hi = part.split(':');
                var head = hi[0];
                var info = hi[1];
                var labels, refString;

                var r1 = /(.*?)\[(.*?)\]/;
                var hparts = r1.exec(head);
                if (hparts == null) {
                    labels = "";
                    refString = "-1,-1";
                } else {
                    labels = hparts[1].replace(/\{(.*?)\}/g, '');
                    refString = hparts[2];
                }
                var tableRowId;
                var code = refString && refString.split( ',' )[5] ? refString.split( ',' )[5] : null;
                var multiplier = refString && refString.split( ',' )[6] ? refString.split( ',' )[6] : null;
                var uniqueId = refString && refString.split( ',' )[4] ? refString.split( ',' )[4] : null;
                var linkedUniqueId = refString && refString.split( ',' )[7] ? refString.split( ',' )[7] : null;
                var sectionNum = refString && refString.split( ',' )[1] ? refString.split( ',' )[1] : null;
                if ( refString && refString.indexOf( "{" ) > -1 )
                    tableRowId = refString.split( ',' )[refString.split( ',' ).length-1]
                else 
                	tableRowId=null;
                p.push({
                    label: labels,
                    name: labels,
                    vector: part,
                    code: code,
                    referenceIds: refString,
                    otherValues: { areaMultiplier: multiplier, uniqueId: uniqueId, linkedUniqueId: linkedUniqueId, tableRowId: tableRowId, sectionNum: sectionNum }
                })

            }
        }

        return p;
    }
}