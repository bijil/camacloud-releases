﻿CAMACloud.Sketching.Formatters.BrightCama = {
    name: "CAMACloud.SketchFormat",
    saveAllSegments: true,
    allowSegmentAddition: false,
    allowSegmentDeletion: false,
    vectorSeperator: ";",
    newVectorHeaderSuffix: '[-1,-1]:',
    nodeToString: function (n) {
        let str = "";
        if (n.arcLength > 0) {
            str += "A" + sRound(n.arcLength);
        }
        if (n.arcLength < 0) {
            str += "B" + Math.abs(sRound(n.arcLength));
        }
        if (n.dy > 0) {
            if (n.isStart) {
                if (str != "") str += " ";
            } 
            else {
                if (str != "") str += "/";
            }
            str += n.sdy + sRound(n.dy);
        }
        if (n.dx > 0) {
            if (n.isStart) {
                if (str != "") str += " ";
            } 
            else {
                if (str != "") str += "/";
            }
            str += n.sdx + sRound(n.dx);
        }
        if (n.hideDimensions) {
            if (str != "") str += "/";
            str += "#";
        }
        if (n.isStart)
            str += " S";
        return str;
    },
    vectorToString: function (v) {
        let str = "";
        let label = v.name;
        if (v.startNode != null) {
            if (!v.labelEdited && !v.newRecord) {
                if (v.header) {
                    let r1 = /(.*?)\[(.*?)\]/;
                    let headpart = r1.exec(v.header);
                    let labels = "", refString = "[-1,-1]";
                    if (headpart != null) {
                        labels = headpart[1];
                        refString = "[" + headpart[2] + "]"
                    }
                    let headerValue = labels + refString + ":";
                    str += headerValue;
                } else {
                    if (v.referenceIds) {
                        str += label + "[" + v.referenceIds + "]:";
                    } else {
                        if (v.rowId)
                            str += label + "[" + v.rowId + ",-1]:";
                        else
                            str += label + "[-1,-1]:";
                    }

                };
            }
            else {
            	 str += label + "[-1,-1" + "," + (v.isModified? 1: 0) + "]:";       
            }
            
            str += v.startNode.vectorString();
            let nn = v.startNode.nextNode;
            while (nn != null) {
                str += " ";
                str += nn.vectorString();
                nn = nn.nextNode;
            }
        }
        return str;
    },
    vectorFromString: function (editor, s) {
        if ((s || '').trim() == '') {
            var nv = new Vector(editor);
            nv.isClosed = false;
            nv.label = "Blank"
            nv.uid = -1;
            return nv;
        }

        let o = editor.origin;
        let p = o.copy();

        let v = new Vector(editor);
        this.updateVectorFromString(editor, v, s);
        v.isModified = false;
        return v;
    },
    updateVectorFromString: function (editor, v, s) {
        v.startNode = null;
        v.endNode = null;
        let o = editor.origin;
        let p = o.copy();
		let hideDimensions = false;
        let hi = s.split(':');
        let head = hi[0];
        let lineString = hi[1] || '';
        let r1 = /(.*?)\[(.*?)\]/;
        let headpart = r1.exec(head);
        let labels = "", refString = "[-1,-1]";
        if (headpart != null) {
            labels = headpart[1];
            refString = "[" + headpart[2] + "]"
        }
        v.header = labels + refString + ":";
        let isStart = false;
        let hasStarted = false;
        let isVeer = false;
        let veerSteps = 0;
        let nodeStrings = lineString.split(' ');
        for (var i in nodeStrings) {
            let arcLength = 0;
            let ns = nodeStrings[i];
            if (ns.trim() != '') {
            	ns = ns.split('{')[0];
                let cmds = ns.split('/');
                for (var c in cmds) {
                    let cmd = cmds[c];
                    if (cmd == '') continue;
                    if (cmd == 'S') {
                        isStart = true;
                    } 
                    else {
                        let cp = cmd.match(/[ABUDLR]|[0-9]+(.[0-9]+)?/g);
                        let dir = cp[0];
                        distance = cp[1] || 0;
                        let pix = CAMACloud.Sketching.Utilities.toPixel(distance);
                        switch (dir) {
                            case "A": arcLength = Number(distance); break;
                            case "B": arcLength = -Number(distance); break;
                            case "U": p.moveBy(0, pix); break;
                            case "D": p.moveBy(0, -pix); break;
                            case "L": p.moveBy(-pix, 0); break;
                            case "R": p.moveBy(pix, 0); break;
                        }
                    }
                }

                p = p.copy();

                if (isStart) {
                    v.start(p.copy());
                    hasStarted = true;
                    isStart = false;
                } 
                else if (hasStarted) {
                    let q = p.copy().alignToGrid(editor);
                    if (!v.startNode.overlaps(q))
                        v.connect(p.copy());
                    else
                        v.terminateNode(v.startNode);
                }

                if (v.endNode) {
                    v.endNode.hideDimensions = hideDimensions;
                    v.endNode.arcLength = arcLength;
                    if (arcLength != 0)
                        v.endNode.isArc = true;
                }
            }
        }

        if (v.startNode)
            if (v.startNode.overlaps(v.endNode.p.copy()))
                v.isClosed = true;
        return v;
    },
    labelPositionFromString: function (editor, l) {
        if (!l)
            return null
        l = l.replace(' ', '');
        let ins = l.match(/[UDLR]|[0-9.]*/g);
        if (ins.length > 3) {
            let x = ins[0] == "R" ? ins[1] : -ins[1];
            let y = ins[2] == "U" ? ins[3] : -ins[3];
            return new PointX(parseFloat(x), (parseFloat(y)))
        }

    },
    labelPositionToString: function (p) {
        let x = p.x > 0 ? "R" + p.x : "L" + -p.x;
        let y = p.y > 0 ? "U" + p.y : "D" + -p.y;
        return x + " " + y
    },
    open: function (editor, data) {
        editor.vectors = [];
        editor.sketches = [];
        let isError = false;

        for (var x in data) {
            let sketch = new Sketch(editor);
            let s = data[x];
            sketch.parentRow = s.parentRow;
            sketch.uid = s.uid;
            sketch.label = s.label;
            sketch.vectors = [];
            sketch.config = s.config || {};
            sketch.sid = s.sid;
            sketch.lookUp = s.lookUp;
            let counter = 0;
            try {
                for (var i in s.sketches) {
                    let sk = s.sketches[i];
                    let segments = (sk.vector || '').split(';');
                    let scount = 0;
                    if (segments.length < 2) scount = -1;
                    if (!sk.otherValues) sk.OtherValues = {};
                    for (var si in segments) {
                        counter += 1;
                        scount += 1;
                        let sv = segments[si];
                        let v = this.vectorFromString(editor, sv);
                        v.sketch = sketch;
                        v.uid = sk.uid + (scount ? '/' + scount : '');
                        v.index = counter;
                        v.name = sk.label[0].Value;
                        v.label = '[' + counter + '] ' + sk.label.map(function (a) { return ((a.Description && sketchSettings["DoNotShowLabelDescriptionSketch"] != '1' && clientSettings["DoNotShowLabelDescriptionSketch"] != '1' && a.Description.Name) ? ((sketchSettings["DoNotShowLabelCode"] == '1' || a.showLabelDescriptionOnly) ? a.Description.Name : a.Value + '-' + a.Description.Name) : a.Value) }).filter(function (a) { return a }).join('/');
                        v.labelFields = sk.label;
                        if (sk.label[0] && sk.label[0].colorCode) {
                            let clrcode = sk.label[0].colorCode.split('~');
                            v.colorCode = clrcode[0] ? clrcode[0] : null;
                            v.newLineColor = (clrcode[1] && sketchSettings['SketchLineColorCustomizations'] && sketchSettings['SketchLineColorCustomizations'].contains('MA')) ? clrcode[1] : null;
                            v.newLabelColorCode = (clrcode[2] && sketchSettings['SketchTextColorCustomizations'] && sketchSettings['SketchTextColorCustomizations'].contains('MA')) ? clrcode[2] : null;
                        }
                        v.vectorString = sv;
                        v.referenceIds = sk.referenceIds;
                        let ridParts = (sk.referenceIds || '').split(",");
                        v.rowId = sk.rowId;
                        v.isChanged = sk.isChanged;
                        v.vectorConfig = sk.vectorConfig;
                        this.AreaUnit = sk.areaUnit;
                        v.labelPosition = this.labelPositionFromString(editor, sk.labelPosition);
                        if (sk.otherValues) {
                            for (var keys in sk.otherValues) {
                                v[keys] = sk.otherValues[keys];
                            }
                        }
                        editor.vectors.pop();
                        sketch.vectors.push(v);
                    }
                }
            }
            catch (e) {
                editor.vectors = []; sketch.vectors = []; isError = true;
            }
            sketch.isModified = false;
            editor.loadNotesForSketch(sketch, s.notes);
            editor.sketches.push(sketch);
        }

        if (isError) {
            editor.sketches.forEach((sk) => { sk.vectors = []; });
            throw "Sketch cannot be rendered due to the wrong sketch data.";
        }
    },
    getParts: function (vectorString) {
        let p = [], parts = (vectorString || '').split(";");
        
        for (var i in parts) {
            let part = parts[i];
            if (/(.*?):(.*?)/.test(part)) {
                let hi = part.split(':');
                let head = hi[0], info = hi[1], labels = '', refString = '-1, -1', isModified = false,
               		r1 = /(.*?)\[(.*?)\]/, hparts = r1.exec(head);
               		
                if (hparts != null) {
                    labels = hparts[1].replace(/\{(.*?)\}/g, ''); refString = hparts[2];
                    let t = refString.split(',');
                    if (t.length > 2) { isModified = t[2] == '1'? true: false; }
                } 
                
                p.push({
                    label: labels,
                    vector: part,
                    referenceIds: refString,
                    brightModified: { isModified: isModified },
                    otherValues: { brightModified: isModified }
                });
            }
        }
        return p;
    }
}

var brightConvert = function (n, vectors, uid, segments, index, isStart) {
	let str = "";
    
    if (n.dy > 0) {
        if (n.isStart) {
            if (str != "") str += " ";
        } 
        else {
            if (str != "") str += "/";
        }
        str += n.sdy + sRound(n.dy);
    }
    if (n.dx > 0) {
        if (n.isStart) {
            if (str != "") str += " ";
        } 
        else {
            if (str != "") str += "/";
        }
        str += n.sdx + sRound(n.dx);
    }
    
    if (n.isStart) {
        str += " S";
    }
    else {
		let a = 'A';	
		vectors.forEach((v, i) => {
			if (i > index && v.uid != uid && !segments.includes(v.uid) && n.overlaps(v.startNode.p)) {
				str += '{' + a + '}';
				segments.push(v.uid);
			}
			a = String.fromCharCode(a.charCodeAt(0) + 1)
		});    	
    }
    return {str: str, segments: segments};
}

var brightSketchSave = function(sketchDataArray, noteData, sketchSaveCallBack, onError, invalidMessage, auditTrailEntry) {
	if (sketchDataArray.length == 0) { 
		if (sketchSaveCallBack) sketchSaveCallBack(afterSketchSave); return; 
	}
	
	_PCIBatchArray = [];
	
	var updateRecordPCI = function() {
    	ccma.Sync.enqueueBulkParcelChanges( _PCIBatchArray, function() {  
			getParcel(activeParcel.Id, function () {
				if (sketchSaveCallBack) sketchSaveCallBack(afterSketchSave); return; 
			});
		});
    }
	
	sketchDataArray.forEach((skd) => {
		let sid = skd.sid;
		sketchApp.sketches.filter((skda) => { return skda.sid == sid; }).forEach((skt) => {
			let segments = [], vectorString = '', vectors = skt.vectors,
				tbl = skt.config.SketchSource.Table, sktRecord = skt.parentRow,
				sketchField = getDataField('cc_sketch', tbl);				
			segments.push(skt.vectors[0].uid);
			
			vectors.forEach((vect, i) => {
				let uid = vect.uid;
				if (segments.includes(uid)) {
					let str = "", label = vect.name;
            	 	str += label + "[-1,-1" + "," + ((vect.isModified || vect.brightModified)? 1: 0) + "]:";       
		            
		            let res = brightConvert(vect.startNode, vectors, uid, segments, i, true);
		            str += res.str; segments = res.segments;
		            let nn = vect.startNode.nextNode;
		            while (nn != null) {
		                str += " ";
		                res = brightConvert(nn, vectors, uid, segments, i);
		                str += res.str; segments = res.segments;
		                nn = nn.nextNode;
		            }
		            vectorString += str + ';';
				}	
			});			
			_update_PCIChanges(sketchField, vectorString, sktRecord);			
		});
	});
	
	if (_PCIBatchArray.length > 0) updateRecordPCI();
    else if (sketchSaveCallBack) { sketchSaveCallBack(afterSketchSave); return; }
}
