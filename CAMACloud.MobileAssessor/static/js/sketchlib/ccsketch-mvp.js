﻿CAMACloud.Sketching.Formatters.mvp = {
    name: "CAMACloud.SketchFormat",
    saveAllSegments: true,
    allowSegmentAddition: false,
    allowSegmentDeletion: false,
    vectorSeperator: ";",
    originPosition: "topLeft",
    arcMode: 'ARC',
    nodeToString: function (n) {
        var str = "";
        if (n.arcLength > 0) {
            str += "A" + sRound(n.arcLength);
        }
        if (n.arcLength < 0) {
            str += "B" + Math.abs(sRound(n.arcLength));
        }
        if (n.dy > 0) {
            if (n.isStart) {
                if (str != "") str += " ";
            } else {
                if (str != "") str += "/";
            }
            str += n.sdy + sRound(n.dy);
        }
        if (n.dx > 0) {
            if (n.isStart) {
                if (str != "") str += " ";
            } else {
                if (str != "") str += "/";
            }
            str += n.sdx + sRound(n.dx);
        }
        if (n.hideDimensions) {
            if (str != "") str += "/";
            str += "#";
        }
        if (n.isStart) {
            str += " S";
        }
        if (n.vector.placeHolder && n.isStart) {
            str += " P" + n.vector.placeHolderSize.toString();
        }
        return str;
    },
    vectorToString: function (v) {
        var str = "";
        var label =( v.name || '' );
        if (v.startNode != null) {
            if (!v.labelEdited) {
                if (v.header) {
                	var lblX = sRound((this.labelPositionToString( v.labelPosition, v).split(" ")[0])), lblY = sRound((this.labelPositionToString( v.labelPosition, v).split(" ")[1])), areaX = 0, areaY = 0, headVal = '';
            		if(v.AreaLabelPosition){
            			areaX = sRound((this.labelPositionToString( v.AreaLabelPosition, v, true).split(" ")[0]));
            			areaY = sRound((this.labelPositionToString( v.AreaLabelPosition, v, true).split(" ")[1]));
            		}
            		v.mvpData.Label_Position_X = lblX; v.mvpData.Label_Position_Y = lblY; v.mvpData.Area_Label_Position_X = areaX; v.mvpData.Area_Label_Position_Y = areaY, v.mvpData.Sketched_Other_Improvement_Indicator = 0;
                    if(v.sketchType == "outBuilding" && v.isUnSketchedArea == true)
                    	v.mvpData.Label_Position_X = 10000, v.mvpData.Label_Position_Y = 10000, v.mvpData.Area_Label_Position_X = 0, v.mvpData.Area_Label_Position_Y = 0, v.mvpData.Sketched_Other_Improvement_Indicator = 0;
                    if(v.sketchType == "outBuilding" && v.isUnSketchedArea != true)
                     	v.mvpData.Sketched_Other_Improvement_Indicator = 1;
                    var mvData = v.mvpData; 
                    var headpart = ['Sketch_Pointer','Improvement_ID', 'Level_1_Default_Exterior_Cover', 'Level_1_Construction_Code', 'Level_1_N', 'Level_1_Prefix_For_Part_1', 'Level_1_Extra_Feature_Code', 'Level_1_Component_Code_For_Part_1','Level_1_Modifier_Code_For_Part_1', 'Level_1_Prefix_For_Part_2', 'Level_1_Component_Code_For_Part_2', 'Level_1_Modifier_Code_For_Part_2', 'Level_1_Prefix_For_Part_3', 'Level_1_Component_Code_For_Part_3', 'Level_1_Modifier_Code_For_Part_3', 'Level_1_Prefix_For_Part_4', 'Level_1_Component_Code_For_Part_4', 'Level_1_Modifier_Code_For_Part_4', 'Level_1_FreeForm_Label_Code', 'Level_2_Default_Exterior_Cover', 'Level_2_Construction_Code', 'Level_2_N', 'Level_2_Extra_Feature_Code', 'Level_2_Prefix', 'Level_2_Component_Code', 'Level_2_ModifierCode', 'Level_2_FreeForm_Label_Code', 'Level_3_Default_Exterior_Cover', 'Level_3_Construction_Code', 'Level_3_N', 'Level_3_Extra_Feature_Code', 'Level_3_Prefix', 'Level_3_Component_Code', 'Level_3_Modifier_Code', 'Level_3_FreeForm_Label_Code', 'Outbuilding_Label_Code', 'Reference', 'Label_Position_X', 'Label_Position_Y', 'Area_Label_Position_X', 'Area_Label_Position_Y', 'Scale', 'Area', 'Perimeter', 'Level_1_Fin_Area', 'Level_1_Unfin_Area', 'Level_2_Fin_Area', 'Level_2_Unfin_Area', 'Level_3_Fin_Area', 'Level_3_Unfin_Area', 'Symbol', 'Symbol_Size', 'Area_Font_Size', 'Sketched_Other_Improvement_Indicator'];
                    var h_part = '[';
                    var headpart_len =  headpart.length
                    headpart.forEach( function(head_P,i){
                    	if(i < headpart_len - 1)
                    		h_part = h_part + mvData[head_P] + ',';
                    	else
                    		h_part = h_part + mvData[head_P] + ']';
                    });
                    var labels = "", refString = "[-1,-1]";
                   
                    var headerValue = h_part + ":";
                    v.header = headerValue;
                    str += headerValue;
                } else {
                    if (v.referenceIds) {
                        str += label + "[" + v.referenceIds + "]:";
                    } else {
                        if (v.rowId)
                            str += label + "[" + v.rowId + ",-1]:";
                        else
                            str += label + "[-1,-1]:";
                    }

                };
            }
            else {
            	var lblX = sRound((this.labelPositionToString( v.labelPosition, v).split(" ")[0])), lblY = sRound((this.labelPositionToString( v.labelPosition, v).split(" ")[1])), areaX = sRound((this.labelPositionToString( v.AreaLabelPosition, v, true).split(" ")[0])), areaY = sRound((this.labelPositionToString( v.AreaLabelPosition, v, true).split(" ")[1])), headVal = '';
            	v.mvpData.Label_Position_X = lblX; v.mvpData.Label_Position_Y = lblY; v.mvpData.Area_Label_Position_X = areaX; v.mvpData.Area_Label_Position_Y = areaY, v.mvpData.Sketched_Other_Improvement_Indicator = 0;
                if(v.sketchType == "outBuilding" && v.isUnSketchedArea == true)
                    v.mvpData.Label_Position_X = 10000, v.mvpData.Label_Position_Y = 10000, v.mvpData.Area_Label_Position_X = 0, v.mvpData.Area_Label_Position_Y = 0, v.mvpData.Sketched_Other_Improvement_Indicator = 0;
                if(v.sketchType == "outBuilding" && v.isUnSketchedArea != true)
                    v.mvpData.Sketched_Other_Improvement_Indicator = 1;
                var headSplit = v.header.split(","), headSplitLength = headSplit.length;
                headSplit[37] = lblX; headSplit[38] = lblY;  headSplit[39] = areaX;  headSplit[40] = areaX;
                headSplit.forEach(function(hSplit, i){
                	if(i < headSplitLength - 1)
                		headVal = headVal + hSplit + ',';
                	else
                		headVal = headVal + hSplit;
                });
                v.header = headVal;
                str += v.header;
            }
            str += v.startNode.vectorString();
            var nn = v.startNode.nextNode;
            while (nn != null) {
                if( str[str.length - 1] != " " )
                    str += " ";
                str += nn.vectorString();
                nn = nn.nextNode;
            }
        }
        return str;
    },
    vectorFromString: function (editor, s) {
        if ((s || '').trim() == '') {
            var nv = new Vector(editor);
            nv.isClosed = false;
            nv.label = "Blank"
            nv.uid = -1;
            return nv;
        }

        var o = editor.origin;
        var p = o.copy();

        var v = new Vector(editor);
        this.updateVectorFromString(editor, v, s);
        v.isModified = false;
        return v;
    },
    updateVectorFromString: function (editor, v, s) {
        v.startNode = null;
        v.endNode = null;
        var o = editor.origin;
        var p = o.copy();

        var hi = s.split(':');
        var head = hi[0];
        var lineString = hi[1] || '';
        var r1 = /(.*?)\[(.*?)\]/;
        var headpart = r1.exec(head);
        var labels = "", refString = "[-1,-1]";
        if (headpart != null) {
            labels = headpart[1];
            refString = "[" + headpart[2] + "]"
        }
        v.header = labels + refString + ":";
        var isStart = false;
        var hasStarted = false;
        var isVeer = false;
        var veerSteps = 0;
        var nodeStrings = lineString.split(' ');
        for (var i in nodeStrings) {
            var arcLength = 0;
            var hideDimensions = false;
            var isPlaceholder = false;
            var lineFontSize = null;
            var ns = nodeStrings[i];
            if (ns.trim() != '') {
            	if (ns.indexOf('~') > -1) {
                    lineFontSize = ns.split('~')[1];
                    ns = ns.split('~')[0];
                }
                var cmds = ns.split('/');
                for (var c in cmds) {
                    var cmd = cmds[c];
                    if (cmd == '') continue;
                    if (cmd == 'S') {
                        isStart = true;
                    } else {
                        var cp = cmd.match(/[ABUDLR#PC]|[0-9]+(.[0-9]+)?/g);
                        var dir = cp[0];
                        distance = cp[1] || 0;
                        var pix = CAMACloud.Sketching.Utilities.toPixel(distance);
                        switch (dir) {
                            case "A": arcLength = Number(distance); break;
                            case "B": arcLength = -Number(distance); break;
                            case "U": p.moveBy(0, pix); break;
                            case "D": p.moveBy(0, -pix); break;
                            case "L": p.moveBy(-pix, 0); break;
                            case "R": p.moveBy(pix, 0); break;
                            case "#": hideDimensions = true; break;
                            case "P": isPlaceholder = true; break;
                        }
                    }
                }

                p = p.copy();

                if (isPlaceholder) {
                    v.placeHolder = true;
                    v.placeHolderSize = Number(distance);
                    v.terminateNode(v.startNode);
                    // v.radiusNode = new Node(v.startNode.p.copy().moveBy(v.placeHolderSize, 0), v)   //for circle radiusNode
                }
                else if (isStart) {
                    v.start(p.copy());
                    hasStarted = true;
                    isStart = false;
                } else if (hasStarted) {
                    var q = p.copy().alignToGrid(editor);
                    if (!v.startNode.overlaps(q))
                        v.connect(p.copy());
                    else
                        v.terminateNode(v.startNode);
                }

                if (v.endNode) {
                    v.endNode.hideDimensions = hideDimensions;
                    if (lineFontSize) 
                    	v.endNode.lineFontSize = lineFontSize;
                    v.endNode.arcLength = arcLength;
                    if (arcLength != 0)
                        v.endNode.isArc = true;
                }

            }
        }

        if (v.startNode)
            if (v.startNode.overlaps(v.endNode.p.copy()))
                v.isClosed = true;
        return v;
    },
    labelPositionFromString: function (editor, l) {
        if (!l)
            return null
        l = l.replace(' ', '');
        var ins = l.match(/[UDLR]|[0-9.]*/g);
        if (ins.length > 3) {
            var x = ins[0] == "R" ? ins[1] : -ins[1];
            var y = ins[2] == "U" ? ins[3] : -ins[3];
            return new PointX(parseFloat(x), (parseFloat(y)))
        }
    },
    labelPositionToString: function (p, v, al) {
    	let us = false, lb = '';
    	if (v && v.startNode && v.isUnSketchedTrueArea) { 
    		p = v.startNode.p; us = true; 
    	}
        let x = ((p)? p.x: 0), y = ((p)? -p.y: 0);
        if (x != 0 && y != 0) 
        	lb = ((us && al)? ((x * 10) + " " + ((y * 10) + 67)): ((us)? ((x * 10) + " " + ((y * 10) + 24)): ((x * 10) + " " + (y * 10))));
        return lb;
    },
    positionCalculas: function (Position_X, Position_Y) {
        var str = '';
        var x = parseFloat(Position_X);
        var y = parseFloat(Position_Y);
       	if (x != 0) {
       		var d = x < 0 ? 'L' : 'R';
       		str += d + Math.abs( x / 10 );
    	}
    	if (y != 0) {
       		if (str != '') str += '/';
        	var d = y < 0 ? 'U' : 'D';
        	str += d + Math.abs( y / 10 );
    	}
    	return (str.replace('/', ' '));
    },
    open: function (editor, data) {
        editor.vectors = [];
        editor.sketches = [];
        let isError = false;

        for (var x in data) {
            var sketch = new Sketch(editor);
            var s = data[x];
            sketch.parentRow = s.parentRow;
            sketch.uid = s.uid;
            sketch.label = s.label;
            sketch.vectors = [];
            sketch.config = s.config || {};
            sketch.sid = s.sid;
            sketch.GroupingValue = s.GroupingValue;
            sketch.boundaryScale = s.boundaryScale;
            sketch.maxNotelen = s.maxNotelen;
            var counter = 0;
            try {
                for (var i in s.sketches) {
                    var sk = s.sketches[i];
                    var segments = (sk.vector || '').split(';');
                    var scount = 0;
                    if (segments.length < 2) scount = -1;
                    for (var si in segments) {
                        counter += 1;
                        scount += 1;
                        var sv = segments[si];
                        var v = this.vectorFromString(editor, sv);
                        v.sketch = sketch;
                        v.uid = sk.uid + (scount ? '/' + scount : '');
                        v.index = counter;
                        v.name = sk.label && sk.label[0].Value ? sk.label[0].Value : (sk.imp_Type || '');
                        v.label = sk.label && sk.label[0].Value ? '[' + counter + '] ' + sk.label.map(function (a) { return a.Description && a.Description.Name ? a.Value + '-' + a.Description.Name : a.Value }).filter(function (a) { return a }).join('/') : '[' + counter + '] ' + (sk.imp_Type || '')
                        var imp_id = sk.mvpData['Improvement_ID'], OIlabel;
                        if (sketchSettings['ShowOI_IDandLabel'] == 1 && sk.sketchType == 'outBuilding') {
                            if (imp_id && imp_id.split('{').length < 2)
                                v.label = '[' + counter + '] ' + imp_id + '-' + v.name;
                        }
                        else if (sketchSettings['ShowOI_IDandLabel'] == 0 && sk.sketchType == 'outBuilding') {
                            if (imp_id && imp_id.split('{').length < 2)
                                v.label = '[' + counter + '] ' + imp_id;
                        }
                        v.labelFields = sk.label;
                        v.vectorString = sv;
                        v.referenceIds = sk.referenceIds;
                        v.rowId = sk.rowId;
                        v.isChanged = sk.isChanged;
                        v.vectorConfig = sk.vectorConfig;
                        v.noVectorSegment = sk.noVectorSegment;
                        v.Line_Type = sk.Line_Type;
                        v.sketchType = sk.sketchType;
                        v.isUnSketchedArea = sk.isUnSketchedArea;
                        v.Disable_CopyVector_OI = sk.Disable_CopyVector_OI;
                        v.isUnSketchedTrueArea = sk.isUnSketchedTrueArea;
                        v.hideAreaValue = sk.hideAreaValue;
                        v.areaFieldValue = sk.areaFieldValue;
                        v.BasementFunctionality = sk.BasementFunctionality;
                        v.perimeterFieldValue = sk.perimeterFieldValue;
                        v.impType = sk.imp_Type;
                        v.mvpData = sk.mvpData;
                        this.AreaUnit = sk.areaUnit;
                        v.labelPosition = this.labelPositionFromString(editor, sk.labelPosition);
                        v.AreaLabelPosition = this.labelPositionFromString(editor, sk.AreaLabelPosition);
                        if (sk.otherValues) {
                            for (var keys in sk.otherValues) {
                                v[keys] = sk.otherValues[keys];
                            }
                        }
                        if (sk.sketchType == 'outBuilding')
                            v.mvpDisplayLabel = '[' + counter + '] ' + sk.mvpDisplayLabel;
                        editor.vectors.pop();
                        sketch.vectors.push(v);
                    }
                }
            }
            catch (e) {
                editor.vectors = []; sketch.vectors = []; isError = true;
            }
            sketch.isModified = false;
            editor.loadNotesForSketch(sketch, s.notes, null, ['fontSize', 'mvpData']);
            editor.sketches.push(sketch);
        }

        if (isError) {
            editor.sketches.forEach((sk) => { sk.vectors = []; });
            throw "Sketch cannot be rendered due to the wrong sketch data.";
        }
    },
	mvpLabelDefinition: function (mvpData) {
    	var labelValue = '', label_Val = '', label_Val1 = '', label_Val2 = '', label_Val3 = '', label_val_orig = '', lblData, segmentLbl = {}, isChanged = false, base_array = [],count = 0, basementfract = false;
    	var getDescription = function(lbl_data){
    		return (lbl_data? (lbl_data.field_1 && lbl_data.field_1.trim() != '')? lbl_data.field_1: lbl_data.tbl_element_desc: '');
    	}
    	var outBuildingCode = mvpData['Outbuilding_Label_Code'];
    	if(outBuildingCode != '0' && outBuildingCode != 0 && outBuildingCode != '' && outBuildingCode != '  '){
    		segmentLbl.sketchType = 'outBuilding';
    		segmentLbl.segment_id  = mvpData.Improvement_ID;
    		segmentLbl.Reference = mvpData.Reference;
    		segmentLbl.out_label_Code = outBuildingCode;
    		var mvp_Out_LookUp = MVP_Outbuliding_Lookup.filter(function(out){return out.label_code == outBuildingCode})[0];
    		segmentLbl.out_label_description = ''; segmentLbl.out_label_short = ''; segmentLbl.out_label_text = ''; segmentLbl.mvpDisplayLabel = outBuildingCode;
    		if(mvp_Out_LookUp){
    			segmentLbl.out_label_description = mvp_Out_LookUp.label_description;
    			segmentLbl.out_label_short = mvp_Out_LookUp.label_short;
    			segmentLbl.out_label_text = mvp_Out_LookUp.label_text;
                segmentLbl.mvpDisplayLabel = (mvp_Out_LookUp.mdescr || mvp_Out_LookUp.mdescr == '' || mvp_Out_LookUp.mdescr == 0)? mvp_Out_LookUp.mdescr: outBuildingCode;
                if (typeof(segmentLbl.mvpDisplayLabel) == 'string' && segmentLbl.mvpDisplayLabel.length > 15)
                    segmentLbl.mvpDisplayLabel = segmentLbl.mvpDisplayLabel.substring(0, 15) + '...';
    		}
    		labelValue = segmentLbl.out_label_short? segmentLbl.out_label_short: outBuildingCode;
    		return {labelValue: labelValue, sketchType: segmentLbl.sketchType, segment_id: segmentLbl.segment_id, out_label_Code: segmentLbl.out_label_Code, out_label_description: segmentLbl.out_label_description, out_label_text: segmentLbl.out_label_text, mvpDisplayLabel: segmentLbl.mvpDisplayLabel};
    	}
    	else {
    		var segment_id =  mvpData.Improvement_ID, BasementFunctionality = false;
    		var BasementValue = mvpData.Level_1_Prefix_For_Part_2;
    		
    		segmentLbl["1"] = { ext_feat_code1: mvpData['Level_1_Extra_Feature_Code'], comp1: mvpData['Level_1_Component_Code_For_Part_1'], prefix1: mvpData['Level_1_Prefix_For_Part_1'], 
	    		constr1: mvpData['Level_1_Construction_Code'], ns1: mvpData['Level_1_N'], walkout1: 0, 
	    		ext_cover1: mvpData['Level_1_Default_Exterior_Cover'], modifier1: mvpData['Level_1_Modifier_Code_For_Part_1'], 
	    		modifierSign1: 0, ext_feat_code2: '', comp2: mvpData['Level_1_Component_Code_For_Part_2'], 
	    		prefix2: mvpData['Level_1_Prefix_For_Part_2'], constr2: 0, ns2: 0, walkout2: 0, ext_cover2: '', 
	    		modifier2: mvpData['Level_1_Modifier_Code_For_Part_2'], modifierSign2: 0, ext_feat_code3: '', 
	    		comp3: mvpData['Level_1_Component_Code_For_Part_3'], prefix3: mvpData['Level_1_Prefix_For_Part_3'], 
	    		constr3: 0, ns3: 0, walkout3: 0, ext_cover3: '', modifier3: mvpData['Level_1_Modifier_Code_For_Part_3'], 
	    		modifierSign3: 0, ext_feat_code4: '', comp4: mvpData['Level_1_Component_Code_For_Part_4'], 
	    		prefix4: mvpData['Level_1_Prefix_For_Part_4'], constr4: 0, ns4: 0, walkout4: 0, ext_cover4: '', 
	  			modifier4: mvpData['Level_1_Modifier_Code_For_Part_4'], modifierSign4: 0, FreeForm1: mvpData['Level_1_FreeForm_Label_Code']
  			}
    		if(mvpData['Level_1_Fin_Area'] && mvpData['Level_1_Fin_Area'] != "0" && mvpData['Level_1_Fin_Area'] != '')
                segmentLbl["1"].fin_area = mvpData['Level_1_Fin_Area'];
    		
    		var ext_feat_code1 = mvpData['Level_1_Extra_Feature_Code'], comp1 = mvpData['Level_1_Component_Code_For_Part_1'], prefix1 =  mvpData['Level_1_Prefix_For_Part_1'] , 
    		constr1 = mvpData['Level_1_Construction_Code'], ext_cover1 = mvpData['Level_1_Default_Exterior_Cover'] , ns1 = mvpData['Level_1_N'], 
    		modifier1 = mvpData['Level_1_Modifier_Code_For_Part_1'], comp2 = mvpData['Level_1_Component_Code_For_Part_2'], 
    		prefix2 = mvpData['Level_1_Prefix_For_Part_2'], modifier2 = mvpData['Level_1_Modifier_Code_For_Part_2'],
    		comp3 = mvpData['Level_1_Component_Code_For_Part_3'], prefix3 = mvpData['Level_1_Prefix_For_Part_3'],
    		modifier3 = mvpData['Level_1_Modifier_Code_For_Part_3'], comp4 = mvpData['Level_1_Component_Code_For_Part_4'], 
    		prefix4 = mvpData['Level_1_Prefix_For_Part_4'], modifier4 = mvpData['Level_1_Modifier_Code_For_Part_4'], FreeForm1 = mvpData['Level_1_FreeForm_Label_Code'];
    		function getlabeldetail(ext_feat_code1, comp1, prefix1, constr1, ext_cover1, ns1, modifier1, FreeForm1, comp2,	prefix2, modifier2,	comp3, prefix3,	modifier3, comp4, prefix4, modifier4, levelNum){ 
    			var mat_Ext_Feat = '', walk = false, fin = false, finPlus = false, finMin = false, uf = false, i=0;
    			var baselblvalue = '';
				var walkout = '', modifierSign1 = '';
				
				function basementFunction(prefix, comp, walk, walkout, modifier, i, ns1){
    				
    					walkout = '';
    					if(comp>1000){ 
							comp=comp%1000;
							walk=true;
						} 
    					if (comp && comp.toString() != '0' && comp.toString() != 0 && comp.toString() != '') {
							var tmp_comp = MVP_Lookup.filter(function(pro){ return pro.tbl_type_code == 'Component' && pro.tbl_element == comp; });
							comp = tmp_comp.length > 0? getDescription(tmp_comp[0]): comp;
							if (comp.toString() == 'B' ) {
								if(walk == true){
									walkout = 'Y';
									segmentLbl[levelNum]['walkout'+i] = walkout;
								}
							}
						}
						if (prefix && prefix.toString() != '0' && prefix.toString() != 0 && prefix.toString() != '') {
							var tmp_prefix = MVP_Lookup.filter(function(pro){ return pro.tbl_type_code == 'Prefix' && pro.tbl_element == prefix; });
							prefix = tmp_prefix.length > 0? getDescription(tmp_prefix[0]): prefix;
							if(prefix == 'N s' && ns1 != '' && ns1 != 0)
								prefix = ns1.toString() + ' s';
							else if (prefix == 'N c' && ns1 != '' && ns1 != 0)
								prefix = ns1.toString() + ' c';
						}
						fin = false; finPlus = false;
    					if(modifier && modifier.toString() != '0' && modifier.toString() != 0 && modifier.toString() != ''){
    						if(modifier == 302)
    							fin = 'Fin';
    						else if(modifier == 301)
    							fin = 'UF';
    						else if(modifier == 1302){
    							fin = 'Fin';
    							finPlus = '-';
    						}	
    						else if(modifier == 2302){
    							fin = 'Fin';
    							finPlus = '+';
    						}
    						else if(modifier == 1301){
    							fin = 'UF';
    							finPlus = '-';
    						}
    						else if(modifier == 2301){
    							fin = 'UF';
    							finPlus = '+';
    						}
    						segmentLbl[levelNum]['modifier'+i] = (!fin) ? '0': fin;
    						segmentLbl[levelNum]['modifierSign'+i] = (!finPlus) ? '0': finPlus;
    					}
    					baselblvalue = ((prefix && prefix.toString() != '0' && prefix != '' )? (prefix + ' ') : '') + ((constr1 && constr1.toString() != '0' && constr1 != '' )? (constr1 + ' ') : '') + ((comp && comp.toString() != '0' && comp != '' )? (comp + ' ') : '') + ((walkout == 'Y')? '-wo ' : '') + ((!(sketchSettings && sketchSettings.DoNotDisplayExteriorCover == '1') && ext_cover1 && ext_cover1.toString() != '0' && ext_cover1 != '' )? (ext_cover1 + ' ') : '') + ((ext_feat_code1 && ext_feat_code1.toString() != '0' && ext_feat_code1 != '' )? (ext_feat_code1 + ' ') : '') + ((mat_Ext_Feat && mat_Ext_Feat.toString() != '0' && mat_Ext_Feat != '' )? ('-' + mat_Ext_Feat + ' ') : '') + ((fin && fin.toString() != '0' && fin != '' )? (fin) : '') + ((finPlus && finPlus.toString() != '0' && finPlus != '' )? (finPlus) : '') + ((FreeForm1 && FreeForm1.toString() != '0' && FreeForm1 != '') ? FreeForm1 : '');
    			  		return baselblvalue;
    			
    			}   			
				function extfeatCalculator(ext_feat_codes){
					var ext_split_code = parseInt(ext_feat_codes);
					var ext_temp = ext_split_code % 1000;
					var maatValuess = parseInt((ext_split_code/1000));
					if(maatValuess != '0' || maatValuess != 0){
						maatValuess = parseInt(maatValuess);
						var baseValue = parseInt(parseInt(maatValuess,10).toString(2));
						var j = 0;
						while(baseValue > 0){
							j = j + 1;
							var rem = baseValue % 10;
							if(rem > 0){
								switch(j){
										case 1: mat_Ext_Feat = mat_Ext_Feat + 'T'; break;
										case 2: mat_Ext_Feat = mat_Ext_Feat + 'R'; break;
										case 3: mat_Ext_Feat = mat_Ext_Feat + 'S'; break;
										case 4: mat_Ext_Feat = mat_Ext_Feat + 'W'; break;
								}
							}
							baseValue = parseInt(baseValue / 10);
						}
					}
					return ext_temp;
				}
    			if (constr1 && constr1.toString() != '0' && constr1.toString() != 0 && constr1.toString() != '') {
					var tmp_const_code = MVP_Lookup.filter(function(pro){ return pro.tbl_type_code == 'Construction' && pro.tbl_element == constr1 }); 
					constr1 = tmp_const_code.length > 0? getDescription(tmp_const_code[0]): constr1;
				}
			
				if (ext_feat_code1 && ext_feat_code1.toString() != '0' && ext_feat_code1.toString() != 0 && ext_feat_code1.toString() != '') {
   					var ext_tempe = extfeatCalculator(ext_feat_code1);
    				var tmp_ext_feat_code = MVP_Lookup.filter(function(pro){ return pro.tbl_type_code == 'Exterior Feature' && pro.tbl_element == ext_tempe; });
					ext_feat_code1 = tmp_ext_feat_code.length > 0? getDescription(tmp_ext_feat_code[0]): ext_tempe;
				}
				if(ext_cover1 && ext_cover1.toString() != '0' && ext_cover1.toString() != 0 && ext_cover1.toString() != ''){
					var tmp_ext_feat_code = MVP_Default_Exterior_Cover.filter(function(pro){ return pro.tbl_element == ext_cover1; });
					ext_cover1 = tmp_ext_feat_code.length > 0? getDescription(tmp_ext_feat_code[0]): ext_cover1;
				}    			
    			if(FreeForm1 && FreeForm1.toString() != '0' && FreeForm1.toString() != 0 && FreeForm1.toString() != ''){
    				var tmp_FreeForm1 = MVP_sketch_labels.filter(function(pro){ return pro.sketch_label_code == FreeForm1; });
					FreeForm1 = tmp_FreeForm1.length > 0? tmp_FreeForm1[0].sketch_label_desc: FreeForm1;
    			}
    			
    			labelValue = basementFunction(prefix1, comp1, walk, walkout, modifier1,1,ns1);
    			
    			if(BasementValue != '0' && BasementValue != 0 && BasementValue != '' && BasementValue != ' '){
    					BasementFunctionality = true;    					
    					if(prefix2 && prefix2.toString() != '0' && prefix2.toString() != 0 && prefix2.toString() != ''){
    						labelValue = labelValue + ' + ' + basementFunction(prefix2, comp2, walk, walkout, modifier2,2);
    					}
    					if(prefix3 && prefix3.toString() != '0' && prefix3.toString() != 0 && prefix3.toString() != ''){
    						labelValue  = labelValue + ' + ' +  basementFunction(prefix3, comp3, walk, walkout, modifier3,3);
    					}
    					if(prefix4 && prefix4.toString() != '0' && prefix4.toString() != 0 && prefix4.toString() != ''){
    						labelValue  = labelValue + ' + ' +  basementFunction(prefix4, comp4, walk, walkout, modifier4,4);
    					}
    			}
    			
    			return labelValue;
    		}
    		label_Val1 = getlabeldetail(ext_feat_code1, comp1, prefix1, constr1, ext_cover1, ns1, modifier1, FreeForm1, comp2,	prefix2, modifier2,	comp3, prefix3,	modifier3, comp4, prefix4, modifier4, 1);
    		//if (label_Val1 && label_Val1 != '')
				//label_Val = label_Val + label_Val1;
    		segmentLbl["2"] = { ext_feat_code1: mvpData['Level_2_Extra_Feature_Code'], 
	    		comp1: mvpData['Level_2_Component_Code'], prefix1: mvpData['Level_2_Prefix'], 
	    		constr1: mvpData['Level_2_Construction_Code'], ns1: mvpData['Level_2_N'], 
	    		walkout1: 0, ext_cover1: mvpData['Level_2_Default_Exterior_Cover'], 
	    		modifier1: mvpData['Level_2_ModifierCode'], modifierSign1: 0, ext_feat_code2: '', comp2: 0, prefix2: 0, constr2: 0, ns2: 0, walkout2: 0, ext_cover2: '', modifier2: 0, modifierSign2: 0, ext_feat_code3: '', comp3: 0, prefix3: 0, constr3: 0, ns3: 0, walkout3: 0, ext_cover3: '', modifier3: 0, modifierSign3: 0, ext_feat_code4: '', comp4: 0, prefix4: 0, constr4: 0, ns4: 0, walkout4: 0, ext_cover4: '', modifier4: 0, modifierSign4: 0 ,FreeForm1: mvpData['Level_2_FreeForm_Label_Code']
    		}
    		
    		var _updateFinArea2 = false, _additionalwinprefixexists = false;
    		if( mvpData['Level_2_Prefix'] == '6' || mvpData['Level_2_Prefix'] == '7' || mvpData['Level_2_Prefix'] == '9')
    			_additionalwinprefixexists = true;
    		if( _additionalwinprefixexists &&  mvpData['Level_3_Fin_Area'] && mvpData['Level_3_Fin_Area'] != "0" && mvpData['Level_3_Fin_Area'] != '' ) {
                segmentLbl["2"].fin_area = mvpData['Level_3_Fin_Area'];
                _updateFinArea2 = true;
            }
            else if( !_additionalwinprefixexists && mvpData['Level_2_Fin_Area'] && mvpData['Level_2_Fin_Area'] != "0" && mvpData['Level_2_Fin_Area'] != '' )
                segmentLbl["2"].fin_area = mvpData['Level_2_Fin_Area'];
                
    		var ext_feat_code1 = mvpData['Level_2_Extra_Feature_Code'], comp1 = mvpData['Level_2_Component_Code'], prefix1 = mvpData['Level_2_Prefix'], 
    		constr1 = mvpData['Level_2_Construction_Code'], ext_cover1 = mvpData['Level_2_Default_Exterior_Cover'], ns1 = mvpData['Level_2_N'], 
    		modifier1 = mvpData['Level_2_ModifierCode'], FreeForm1 = mvpData['Level_2_FreeForm_Label_Code'];
    		
    		BasementValue = '';
    		
    		label_Val2 = getlabeldetail(ext_feat_code1, comp1, prefix1, constr1, ext_cover1, ns1, modifier1, FreeForm1, comp2,	prefix2, modifier2,	comp3, prefix3,	modifier3, comp4, prefix4, modifier4, 2);
    		//if (label_Val2 && label_Val2 != '')
				//label_Val = label_Val1? label_Val + '|'+ label_Val2: label_Val + label_Val2;
    		
    		segmentLbl["3"] = {ext_feat_code1: mvpData['Level_3_Extra_Feature_Code'], 
	    		comp1: mvpData['Level_3_Component_Code'], prefix1: mvpData['Level_3_Prefix'], 
	    		constr1: mvpData['Level_3_Construction_Code'], ns1: mvpData['Level_3_N'], 
	    		walkout1: 0, ext_cover1:  mvpData['Level_3_Default_Exterior_Cover'], 
	    		modifier1: mvpData['Level_3_Modifier_Code'], modifierSign1: 0, ext_feat_code2: '', comp2: 0, prefix2: 0, constr2: 0, ns2: 0, walkout2: 0, ext_cover2: '', modifier2: 0, modifierSign2: 0, ext_feat_code3: '', comp3: 0, prefix3: 0, constr3: 0, ns3: 0, walkout3: 0, ext_cover3: '', modifier3: 0, modifierSign3: 0, ext_feat_code4: '', comp4: 0, prefix4: 0, constr4: 0, ns4: 0, walkout4: 0, ext_cover4: '', modifier4: 0, modifierSign4: 0 ,  FreeForm1: mvpData['Level_3_FreeForm_Label_Code']
    		}
    		if( !_updateFinArea2 && mvpData['Level_3_Fin_Area'] && mvpData['Level_3_Fin_Area'] != "0" && mvpData['Level_3_Fin_Area'] != '' )
                segmentLbl["3"].fin_area = mvpData['Level_3_Fin_Area'];
                
    		var ext_feat_code1 = mvpData['Level_3_Extra_Feature_Code'], comp1 = mvpData['Level_3_Component_Code'], prefix1 = mvpData['Level_3_Prefix'], 
    		constr1 = mvpData['Level_3_Construction_Code'], ext_cover1 = mvpData['Level_3_Default_Exterior_Cover'], ns1 = mvpData['Level_3_N'], 
    		modifier1 = mvpData['Level_3_Modifier_Code'],  FreeForm1 = mvpData['Level_3_FreeForm_Label_Code'];
    		
    		label_Val3 = getlabeldetail(ext_feat_code1, comp1, prefix1, constr1, ext_cover1, ns1, modifier1, FreeForm1, comp2,	prefix2, modifier2,	comp3, prefix3,	modifier3, comp4, prefix4, modifier4, 3);
    		//if (label_Val3 && label_Val3 != '')
				//label_Val = (label_Val1 || label_Val2)? label_Val + '|'+ label_Val3: label_Val + label_Val3;
    		
    		if (label_Val3 && label_Val3 != '')
    			label_Val = label_Val + label_Val3;
    		if (label_Val2 && label_Val2 != '')
				label_Val = label_Val3? label_Val + '|'+ label_Val2: label_Val + label_Val2;
    		if (label_Val1 && label_Val1 != '')
				label_Val = (label_Val3 || label_Val2)? label_Val + '|'+ label_Val1: label_Val + label_Val1;
    		return {labelValue: label_Val, details: segmentLbl,  sketchType: 'sketch', segment_id: segment_id, BasementFunctionality : BasementFunctionality};
    	}
    	
    },
    convertJson: function(jsObj) {
    	let notes = [], segments = [];
    	if (jsObj && jsObj.Version == "1.0.0") {
    		if (jsObj.Notes.length > 0) {
    			let jn = jsObj.Notes.sort((a, b) => { return (parseInt(a.NoteNumber) - parseInt(b.NoteNumber)) });
    			jn.forEach((n) => {
    				notes.push({ isMVPNote: true, mvpNotes: [{ noteText: n.NoteText, xPosition: n.StartX, yPosition: n.StartY, fontSize: n.FontSize, FontColor: n.FontColor, Font: n.Font }] });
    			});
    		}
    		
    		if (jsObj.Segments.length > 0) {
    			let segs = jsObj.Segments.sort((a, b) => { return (parseInt(a.SegmentNo) - parseInt(b.SegmentNo)) });
    			segs.forEach((s) => {
    				let sketchType = s.SegGrades.length > 0? 'sketch': 'outBuilding', segGds = s.SegGrades, sqStr = '', us = true;
    				let sx = s.StartX? (parseFloat(s.StartX) < 0? 'L' + Math.abs(parseFloat(s.StartX)): 'R' + Math.abs(parseFloat(s.StartX))): 'R0';
    				let sy = s.StartY? (parseFloat(s.StartY) < 0? 'D' + Math.abs(parseFloat(s.StartY)): 'U' + Math.abs(parseFloat(s.StartY))): 'U0';
    				let lineStr = sy + ' ' + sx + ' ' + 'S ';
    				let sklink = s.SegLinks[0].SegLink? s.SegLinks[0].SegLink: '';
					let impId = s.SegType && s.SegType != 'null'? s.SegType.trim(): null;
					
    				if (s.Lines.length > 0) {
    					let lines = s.Lines.sort((a, b) => { return (parseInt(a.LineNo) - parseInt(b.LineNo)) });
    					lines.forEach((l) => {
    						if(l.ArcDirection)
    							lineStr += l.ArcDirection + parseFloat(l.SagittaHeight) + '/';
    							
    						if (l.Direction.contains('/')) {
    							let ds = l.Direction.split('/');
    							lineStr += ds[0] + l.Rise + '/' + ds[1] + l.Run;
    						} 
    						else {
    							lineStr += l.Direction + l.LineLength;
    						}
    						
    						if (l.LineDimensionalFontSize == '0')
    							lineStr += '/#';
    						else if (l.LineDimensionalFontSize)
    							lineStr += '~' + l.LineDimensionalFontSize;
    						lineStr += ' ';
    					});
    					lineStr = lineStr.trim() + '';
    					us = false; 
    				}
    				else if (sketchType == 'outBuilding') {
    					lineStr += 'U4 R4 D4 L4';
    				}
    				else if (sketchType == 'sketch') {
						lineStr += ' ';    
					}
										
					if (sketchType == 'outBuilding') {
						sqStr = '[' + sklink + ',' + impId + ',,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,' + s.Label + ','
 + '0,' + s.LabelX + ',' + s.LabelY + ',' + s.SegAreaLabelX + ',' + s.SegAreaLabelY	+ ',' + s.SegScale + ',' + s.SegArea + ',' + s.SegPerimeter + ',0,0,0,0,0,0,' + s.Symbol + ',' + s.SymbolSize + ',' + s.SegAreaLabelFontSize + ',' + (us? 0: 1) + ']:';			
 					}
					else {
						let sqArr = [], larea = [];
						let sgs = segGds.sort((a, b) => { return (parseInt(a.GradeLevel) - parseInt(b.GradeLevel)) });
						sgs.forEach((sg, h) => {
							sqArr.push(sg.GradeDefaultExteriorCover, sg.GradeConstructionCode, sg.GradeN);
							let parts = sg.Parts.sort((a, b) => { return (parseInt(a.PartNumber) - parseInt(b.PartNumber)) }); 
							parts.forEach((p, i) => {
								if (i == 0 && h == 0)
									sqArr.push(p.PartPrefix, sg.GradeExtraFeatCode, p.PartComponentCode, p.PartModifierCode);
								else if (i == 0 && h != 0)
									sqArr.push(sg.GradeExtraFeatCode, p.PartPrefix, p.PartComponentCode, p.PartModifierCode);
								else
									sqArr.push(p.PartPrefix, p.PartComponentCode, p.PartModifierCode);				
							});
							sqArr.push((sg.GradeLabel? sg.GradeLabel: '0'));
							larea.push(sg.GradeFinArea, sg.GradeUnfinArea);
						});
						
						sqStr = '[' + sklink + ',' + impId + ',' + sqArr.toString() + ',,0,' + s.LabelX + ',' + s.LabelY + ',' + s.SegAreaLabelX + ',' + s.SegAreaLabelY	+ ',' + s.SegScale + ',' + s.SegArea + ',' + s.SegPerimeter + ',' + larea.toString() + ',' + s.Symbol + ',' + s.SymbolSize + ',' + s.SegAreaLabelFontSize + ',0]:';
					}
					segments.push({ VectorString: (sqStr + lineStr), SegmentObj: s });
    			});
    		}
    	}
    	
    	return { notes: notes, segments: segments };
    },
    getParts: function (vectorString) {
        let p = [], isUnSketchedArea, Disable_CopyVector_OI = false, isJson = false, jsObj = null, jsonSegs;
        vectorString = vectorString && vectorString.replaceAll('&gt;', '>').replaceAll('&lt;', '<');
        try {
        	jsObj = JSON.parse(vectorString);
        	if (jsObj && jsObj.Version) {
        		isJson = true;
        		jsonSegs = this.convertJson(jsObj);
        	}
        }
        catch(ex) { }
        
        var parts = isJson? jsonSegs.segments: (vectorString || '').split(";");
        for (var i in parts) {
            var part = isJson? parts[i].VectorString: parts[i];
            isUnSketchedArea = false;
            var isMVPSpecialUnsketchedArea =  false;
            if (/(.*?):(.*?)/.test(part)) {
            	var partFirst = part.split(':')[0], partSecond = part.split(':')[1];
                var hi = part.split(':');
                var head = hi[0];
                var info = hi[1];
                var labels, refString = '-1,-1', labelPosition = null, AreaLabelPosition = null, mvpData = {}, sketchType, boundScale = 0, mvpNotes = [], mvpLabelDetails = {};
                var hparts = head.replace(/[\[\]']+/g,'');
                var splitHparts = hparts.split(",");
                var mvpFields = ['Sketch_Pointer','Improvement_ID', 'Level_1_Default_Exterior_Cover', 'Level_1_Construction_Code', 'Level_1_N', 'Level_1_Prefix_For_Part_1', 'Level_1_Extra_Feature_Code', 'Level_1_Component_Code_For_Part_1','Level_1_Modifier_Code_For_Part_1', 'Level_1_Prefix_For_Part_2', 'Level_1_Component_Code_For_Part_2', 'Level_1_Modifier_Code_For_Part_2', 'Level_1_Prefix_For_Part_3', 'Level_1_Component_Code_For_Part_3', 'Level_1_Modifier_Code_For_Part_3', 'Level_1_Prefix_For_Part_4', 'Level_1_Component_Code_For_Part_4', 'Level_1_Modifier_Code_For_Part_4', 'Level_1_FreeForm_Label_Code', 'Level_2_Default_Exterior_Cover', 'Level_2_Construction_Code', 'Level_2_N', 'Level_2_Extra_Feature_Code', 'Level_2_Prefix', 'Level_2_Component_Code', 'Level_2_ModifierCode', 'Level_2_FreeForm_Label_Code', 'Level_3_Default_Exterior_Cover', 'Level_3_Construction_Code', 'Level_3_N', 'Level_3_Extra_Feature_Code', 'Level_3_Prefix', 'Level_3_Component_Code', 'Level_3_Modifier_Code', 'Level_3_FreeForm_Label_Code', 'Outbuilding_Label_Code', 'Reference', 'Label_Position_X', 'Label_Position_Y', 'Area_Label_Position_X', 'Area_Label_Position_Y', 'Scale', 'Area', 'Perimeter', 'Level_1_Fin_Area', 'Level_1_Unfin_Area', 'Level_2_Fin_Area', 'Level_2_Unfin_Area', 'Level_3_Fin_Area', 'Level_3_Unfin_Area', 'Symbol', 'Symbol_Size', 'Area_Font_Size', 'Sketched_Other_Improvement_Indicator']; 
                for(m = 0; m < mvpFields.length; m++) {
                	if(mvpFields[m] == 'Symbol_Size' || mvpFields[m] == 'Area_Font_Size')
                		splitHparts[m] = splitHparts[i] ? splitHparts[m] : (mvpData['Sketched_Other_Improvement_Indicator'] == '0' && mvpData['Outbuilding_Label_Code'] != '')? '': 0;
                    mvpData[mvpFields[m]] = splitHparts[m];
                    if((mvpFields[m] == 'Scale') && (splitHparts[m] && splitHparts[m] != ''))
                    	boundScale =  parseInt(splitHparts[m]);
                }
                
                var isNote = false;
                if (!isJson) {
	                for (var k = mvpFields.length; k < splitHparts.length; k++) {
	                	if (splitHparts[k]) {
	                		isNote =  true;
	                		break;
	                	}
	                }
	                isNote = (isNote && splitHparts[1] == '')? true: false;
	                if (isNote) {
	                	var noteFields = ['fontSize', 'xPosition', 'yPosition', 'noteText'], counter = 0, note = {};
	                	for (var j = mvpFields.length; j < splitHparts.length; j++) {
	                		note[noteFields[counter]] = splitHparts[j];
	                		counter = counter + 1;
	                		if (counter == noteFields.length) {
	                			if (note.noteText)
	                				mvpNotes.push(_.clone(note));
	                			note = {};
	                			counter = 0;
							}                			
	                	}
	                	p.push({
	                		isMVPNote: true,
	                		mvpData: mvpData,
	                		mvpNotes: mvpNotes
	                	});
	                	continue;
	                }
				}
				
                if (mvpData['Sketched_Other_Improvement_Indicator'] == '0' && mvpData['Outbuilding_Label_Code'] != '') {
                	isUnSketchedArea = true;
                	Disable_CopyVector_OI = false;
                }
                else if (mvpData['Sketched_Other_Improvement_Indicator'] == '1' && mvpData['Outbuilding_Label_Code'] != '' && mvpData['Label_Position_X'] == '10000' &&  mvpData['Label_Position_Y'] == '10000') {
                	isUnSketchedArea = true;
                	Disable_CopyVector_OI = false;
                	isMVPSpecialUnsketchedArea = true;
                }
                else if(mvpData['Sketched_Other_Improvement_Indicator'] == '1' && mvpData['Outbuilding_Label_Code'] != '') {
                	isUnSketchedArea = false;
                	Disable_CopyVector_OI = true;
                }	
                mvpLabelDetails = this.mvpLabelDefinition(mvpData);
                if (hparts == null) {
                    labels = "";
                    refString = "-1,-1";
                } else {
                    labels = hparts[1].replace(/\{(.*?)\}/g, '');
                    refString = "-1,-1";
                    labelPosition = this.positionCalculas(mvpData.Label_Position_X, mvpData.Label_Position_Y);
                    AreaLabelPosition = this.positionCalculas(mvpData.Area_Label_Position_X, mvpData.Area_Label_Position_Y);
                }
                
                var  hPartFirst = partFirst.replace(/[\[\]']+/g,'');
                hPartFirst = hPartFirst.split(",");
                var bbScale = hPartFirst[41];
                hPartFirst[41] = 0;                
                var otherValues = { isMVPSpecialUnsketchedArea: isMVPSpecialUnsketchedArea };
                
                let partSecondSplit = partSecond.split('S');
                if (sketchType != 'outBuilding' && (!partSecondSplit[1] || partSecondSplit[1].trim() == "")) {
                	otherValues.isUnSketchedTrueArea = true;
                	otherValues.fixedAreaTrue = mvpData['Area'];
                	otherValues.fixedPerimeterTrue = mvpData['Perimeter'];
                	otherValues.Line_Type = true;
                	partSecond = partSecond.trim() + ' P2';
                	labelPosition = null;
                	AreaLabelPosition = null;
                }
                
                if (isJson)
                	otherValues.jsonSegment = parts[i].SegmentObj;
                
                part = '[' + hPartFirst.toString() + ']:' + partSecond;
                p.push({
                    label: mvpLabelDetails.labelValue,
                    vector: part,
                    referenceIds: refString,
                    labelPosition: labelPosition,
                    mvpData: _.clone(mvpData),
                    AreaLabelPosition: AreaLabelPosition,
                    sketchType: mvpLabelDetails.sketchType,
                    isUnSketchedArea: isUnSketchedArea,
                    Disable_CopyVector_OI: Disable_CopyVector_OI,
                    mvpLabelDetails: mvpLabelDetails,
                    BasementFunctionality: mvpLabelDetails.BasementFunctionality,
                    boundScale: boundScale,
                    bbScale: bbScale,
                    mvpDisplayLabel: mvpLabelDetails.mvpDisplayLabel,
                    otherValues: otherValues
                });

            }
        }
		if (isJson && jsonSegs.notes.length > 0) {
			jsonSegs.notes.forEach((jn) => {
				p.push(jn);
			});
		}
        return p;
    }
}