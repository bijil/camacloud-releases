﻿function wrightAfterSave(sketchData, callback) {
    if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }

    _PCIBatchArray = [];

    let newRecords = [], deletedRecords = [], newRecId = ccTicks(), addCatId = getCategoryFromSourceTable('ADDN').Id, ctextCatId = getCategoryFromSourceTable('COMINTEXT').Id, cfeatCatId = getCategoryFromSourceTable('COMFEAT').Id;

    var updateRecordPCI = function () {
        ccma.Sync.enqueueBulkParcelChanges(_PCIBatchArray, function () {
            getParcel(activeParcel.Id, function () {
                if (callback) callback(); return;
            });
        });
    }

    var createNewRecord = function (newRecds) {
        if (newRecds.length == 0) {
            updateRecordPCI(); return;
        }

        let newRec = newRecds.shift();
        insertNewAuxRecord(null, newRec.parentRecord, newRec.catId, newRec.rowId, newRec.AdditionalFields, (rowid) => {
            createNewRecord(newRecds);
        });
    }

    var deleteRecord = function (delRecords, delCallback) {
        if (delRecords.length == 0) {
            createNewRecord(newRecords); return;
        }

        let delRec = delRecords.pop(), rel = (delRecords.length == 0 && (typeof (cachedAuxOptions) != 'undefined') && !(isEmpty(cachedAuxOptions)) && cachedAuxOptions[delRec.categoryId]) ? true : false;
        ccma.Data.Controller.DeleteAuxRecordWithDescendants(delRec.categoryId, delRec.rowuid, null, null, function () {
            deleteRecord(deletedRecords, delCallback);
        }, rel, null, true);
    }

    var findAssociatedRecord = function (sk, tbl, prowId) {
        let svs = sk.vectorString.split('[')[1].split(']')[0], rowuid = null, apex_id = -1; apex_id = parseInt(svs.split(/\,/g)[4]);
        if (apex_id < 0) {
            if (svs.indexOf('{') > -1 && svs.indexOf('}') > -1) {
                rowuid = svs.indexOf('#') > -1 ? (svs.split('#')[1].split('$')[0]) : svs.split('{')[1].split('}')[0];
            }
            else if (sk.tableRowId) {
                rowuid = sk.tableRowId.indexOf('#') > -1 ? (sk.tableRowId.split('#')[1].split('$')[0]) : sk.tableRowId.split('{')[1].split('}')[0];
            }
        }

        let rec = activeParcel[tbl].filter((x) => { return x.ParentROWUID == prowId && ((apex_id > -1 && x.AREAID == (parseInt(apex_id) + 1)) || (rowuid && x.ROWUID == rowuid)) })[0];
        return { record: rec, apex_id: apex_id, rowuid: rowuid };
    }

    var createString = function (vs, table, rowId, sp) {
        let tempstr = vs.split('[')[1].split(']')[0].split(/\,/);
        if (sp && tempstr[tempstr.length - 1].indexOf('{') > -1 && tempstr[tempstr.length - 1].indexOf('}') > -1) tempstr.pop();
        tempstr[tempstr.length] = sp ? rowId: ('{$' + table + '#' + parseFloat(rowId) + '$}');
        return vs.split('[')[0] + '[' + tempstr.toString() + ']' + vs.split(']')[1] + ';';
    }

    var processSketchData = function (sketchRecords) {
        if (sketchRecords.length == 0) {
            deleteRecord(deletedRecords); return;
        }

        let sketchRecord = sketchRecords.shift(), sketch = sketchApp.sketches.filter((s) => { return s.sid == sketchRecord.rowid })[0],
            vectors = sketch.vectors.filter((x) => { return !x.isFreeFormLineEntries }), vs = '', addnAreaField = getDataField('AREA', 'ADDN'), addnEffField = getDataField('EFF_AREA', 'ADDN'), pages = {}, skField = getDataField('CC_SKETCH');

        for (var x in vectors) {
            let v = vectors[x];
            if (v.pageNum && !pages[v.pageNum]) {
                let parentRecord = activeParcel['CCV_DWELDAT_APEXSKETCH'].filter((x) => { return x.CARD == v.pageNum && x.CC_Deleted != true })[0];
                if (parentRecord)
                    pages[v.pageNum] = { sketchTable: 'CCV_DWELDAT_APEXSKETCH', parentRecord: parentRecord, baseArea: 0, totalArea: 0, lzArea: 0 }
                else {
                    parentRecord = activeParcel['CCV_COMDAT_APEXSKETCH'].filter((x) => { return x.CARD == v.pageNum && x.CC_Deleted != true })[0];
                    if (parentRecord) pages[v.pageNum] = { sketchTable: 'CCV_COMDAT_APEXSKETCH', parentRecord: parentRecord, comtextTotalArea: 0 }
                }
                if (pages[v.pageNum]) pages[v.pageNum]['lzFlag'] = false;
            }
        }

        vectors.forEach((v, i) => {
            let pg = v.pageNum, parentRecord = pages[pg]?.parentRecord, sketchTable = pages[pg]?.sketchTable, code = v.code, area = Math.round(v.area()), peri = Math.round(v.perimeter());

            if (sketchTable == 'CCV_DWELDAT_APEXSKETCH') {
                let lk = WrightAddn_Lookup.filter((x) => { return x.Id == code })[0];

                if (!pages[pg]['lzFlag']) {
                    pages[v.pageNum]['lzFlag'] = true; pages[v.pageNum].lzArea = area;
                }

                if (v.newRecord) {
                    newRecId = newRecId + 5; pages[v.pageNum].totalArea = pages[v.pageNum].totalArea + area;
                    let nrec = { rowId: newRecId, catId: addCatId, parentRecord: parentRecord, AdditionalFields: { AREAID: newRecId, AREA: area, EFF_AREA: 0 } }

                    if (lk) {
                        ['FIRST', 'LOWER', 'SECOND', 'THIRD'].forEach((x) => { if (lk[x] && lk[x] != '') nrec.AdditionalFields[x] = lk[x]; });
                        if (lk.BASEAREA == 'Y') { pages[v.pageNum].baseArea = pages[v.pageNum].baseArea + area; nrec.AdditionalFields.EFF_AREA = area; }
                    }
                    newRecords.push(nrec); vs += createString(v.vectorString, 'ADDN', newRecId);
                }
                else if (v.isModified) {
                    let rec = findAssociatedRecord(v, 'ADDN', parentRecord.ROWUID);

                    if (rec.record) {
                        if (rec.record.LLINE == 0) {
                            pages[v.pageNum]['lzFlag'] = true; pages[v.pageNum].lzArea = area;
                        }

                        pages[v.pageNum].totalArea = pages[v.pageNum].totalArea + area;
                        _update_PCIChanges(addnAreaField, area, rec.record);

                        if (lk) {
                            ['FIRST', 'LOWER', 'SECOND', 'THIRD'].forEach((x) => {
                                if (lk[x] && lk[x] != '')
                                    _update_PCIChanges(getDataField(x, 'ADDN'), lk[x], rec.record);
                            });
                            if (lk.BASEAREA == 'Y') {
                                pages[v.pageNum].baseArea = pages[v.pageNum].baseArea + area;
                                _update_PCIChanges(addnEffField, area, rec.record);
                            }
                        }

                        if (rec.record.CC_RecordStatus == 'I') {
                            let nassociateRowuid = parseFloat(rec.record.ROWUID) < 0 ? ('{$ADDN#' + parseFloat(rec.record.ROWUID) + '$}') : '{' + rec.record.ROWUID + '}';
                            vs += createString(v.vectorString, null, nassociateRowuid, true);
                        }
                        else if (!v.newRecord) { vs += v.vectorString + ';'; }
                    }
                    else {
                        pages[v.pageNum].baseArea = lk?.BASEAREA == 'Y' ? (pages[v.pageNum].baseArea + area) : pages[v.pageNum].baseArea;
                        vs += v.vectorString + ';';
                    }
                }
                else {
                    let rec = findAssociatedRecord(v, 'ADDN', parentRecord.ROWUID);
                    if (rec?.record?.LLINE == 0) {
                        pages[v.pageNum]['lzFlag'] = true; pages[v.pageNum].lzArea = area;
                    }
                    pages[v.pageNum].baseArea = lk?.BASEAREA == 'Y' ? (pages[v.pageNum].baseArea + area) : pages[v.pageNum].baseArea;
                    vs += v.vectorString + ';';
                }

            }
            else if (sketchTable == 'CCV_COMDAT_APEXSKETCH') {
                let lk = WrightCom_Lookup.filter((x) => { return x.Id == code })[0];
                if (v.newRecord) {
                    newRecId = newRecId + 5, nrec = null;

                    if (lk?.TBLE == 'COMINTEXT') {
                        pages[v.pageNum].comtextTotalArea = pages[v.pageNum].comtextTotalArea + area;
                        nrec = { rowId: newRecId, catId: ctextCatId, parentRecord: parentRecord, AdditionalFields: { AREAID: newRecId, AREA: area, CUBICFT: (area * 10), PERIM: peri, SF: area } };
                        if (v.vectorExtraParameters?.wrightParams) {
                            let pm = v.vectorExtraParameters.wrightParams;
                            if (pm.FLRFROM) nrec.AdditionalFields['FLRFROM'] = pm.FLRFROM;
                            if (pm.FLRTO) nrec.AdditionalFields['FLRTO'] = pm.FLRTO;
                        }

                        let ulk = WrightComType_Lookup.filter((x) => { return x.Id == code })[0];
                        if (ulk?.USETYPE && ulk?.USETYPE != '') nrec.AdditionalFields['USETYPE'] = ulk.USETYPE;
                        vs += createString(v.vectorString, 'COMINTEXT', newRecId);
                    }
                    else if (lk?.TBLE == 'COMFEAT') {
                        nrec = { rowId: newRecId, catId: cfeatCatId, parentRecord: parentRecord, AdditionalFields: { AREAID: newRecId, AREA: area, STRUCT: code } };

                        /*if (v.vectorExtraParameters?.wrightParams) {
                            let pm = v.vectorExtraParameters.wrightParams;
                            if (pm.MEAS1) nrec.AdditionalFields['MEAS1'] = pm.MEAS1;
                            if (pm.MEAS2) nrec.AdditionalFields['MEAS2'] = pm.MEAS2;
                        }*/

                        vs += createString(v.vectorString, 'COMFEAT', newRecId);
                    }

                    if (nrec) newRecords.push(nrec);
                }
                else if (v.isModified) {
                    if (lk?.TBLE == 'COMINTEXT') {
                        let rec = findAssociatedRecord(v, 'COMINTEXT', parentRecord.ROWUID);
                        pages[v.pageNum].comtextTotalArea = pages[v.pageNum].comtextTotalArea + area;

                        if (rec.record) {
                            ['AREA', 'SF', 'CUBICFT', 'PERIM'].forEach((x) => {
                                let fld = getDataField(x, 'COMINTEXT');
                                if (fld) {
                                    if (x == 'PERIM')
                                        _update_PCIChanges(fld, peri, rec.record);
                                    else if (x == 'CUBICFT')
                                        _update_PCIChanges(fld, (area * 10), rec.record);
                                    else
                                        _update_PCIChanges(fld, area, rec.record);
                                }
                            });

                            if (v.vectorExtraParameters?.wrightParams) {
                                let pm = v.vectorExtraParameters.wrightParams;
                                if (pm.FLRFROM)
                                    _update_PCIChanges(getDataField('FLRFROM', 'COMINTEXT'), pm.FLRFROM, rec.record);
                                if (pm.FLRTO)
                                    _update_PCIChanges(getDataField('FLRTO', 'COMINTEXT'), pm.FLRTO, rec.record);
                            }

                            if (rec.record.CC_RecordStatus == 'I') {
                                let nassociateRowuid = parseFloat(rec.record.ROWUID) < 0 ? ('{$COMINTEXT#' + parseFloat(rec.record.ROWUID) + '$}') : '{' + rec.record.ROWUID + '}';
                                vs += createString(v.vectorString, null, nassociateRowuid, true);
                            }
                            else if (!v.newRecord) vs += v.vectorString + ';';
                        }
                        else if (!v.newRecord) vs += v.vectorString + ';';
                    }
                    else if (lk?.TBLE == 'COMFEAT') {
                        let rec = findAssociatedRecord(v, 'COMFEAT', parentRecord.ROWUID);

                        if (rec.record) {
                            _update_PCIChanges(getDataField('AREA', 'COMFEAT'), area, rec.record);
                            _update_PCIChanges(getDataField('STRUCT', 'COMFEAT'), code, rec.record);

                            /*if (v.vectorExtraParameters?.wrightParams) {
                                let pm = v.vectorExtraParameters.wrightParams;
                                if (pm.MEAS1)
                                    _update_PCIChanges(getDataField('MEAS1', 'COMFEAT'), pm.MEAS1, rec.record);
                                if (pm.MEAS2)
                                    _update_PCIChanges(getDataField('MEAS2', 'COMFEAT'), pm.MEAS2, rec.record);
                            }*/

                            if (rec.record.CC_RecordStatus == 'I') {
                                let nassociateRowuid = parseFloat(rec.record.ROWUID) < 0 ? ('{$COMINTEXT#' + parseFloat(rec.record.ROWUID) + '$}') : ('{' + rec.record.ROWUID + '}');
                                vs += createString(v.vectorString, null, nassociateRowuid, true);
                            }
                            else if (!v.newRecord) vs += v.vectorString + ';';
                        }
                        else if (!v.newRecord) vs += v.vectorString + ';';
                    }
                    else {
                        vs += v.vectorString + ';';
                    }
                }
                else {
                    pages[v.pageNum].comtextTotalArea = lk?.TBLE == 'COMINTEXT' ? (pages[v.pageNum].comtextTotalArea + area) : pages[v.pageNum].comtextTotalArea;
                    vs += v.vectorString + ';';
                }
            }
            else vs += v.vectorString + ';';
        });

        for (k in pages) {
            let pg = pages[k];
            if (pg.sketchTable == 'CCV_DWELDAT_APEXSKETCH') {
                ['ACAREA', 'ADDNAREA', 'HEATAREA', 'SFLA', 'ADJAREA', 'AREASUM', 'EFF_AREA', 'FLR1AREA', 'MGFA'].forEach((x) => {
                    let fld = getDataField(x, 'CCV_DWELDAT_APEXSKETCH');
                    if (fld) {
                        if (x == 'ADJAREA') {
                            _update_PCIChanges(fld, (Math.round(pg.lzArea / 10) * 10), pg.parentRecord);
                        }
                        else if (['AREASUM', 'EFF_AREA', 'FLR1AREA', 'MGFA'].includes(x)) {
                            _update_PCIChanges(fld, pg.lzArea, pg.parentRecord);
                        }
                        else {
                            _update_PCIChanges(fld, pg.baseArea, pg.parentRecord);
                        }
                    }
                });
            }
            else if (pg.sketchTable == 'CCV_COMDAT_APEXSKETCH') {
                _update_PCIChanges(getDataField('BUSLA', 'CCV_COMDAT_APEXSKETCH'), pg.comtextTotalArea, pg.parentRecord);
                _update_PCIChanges(getDataField('CUBICFT', 'CCV_COMDAT_APEXSKETCH'), (pg.comtextTotalArea * 10), pg.parentRecord);
            }
        }

        if (skField) {
            let jsonString = sketch.jsonString; jsonString.DCS = vs; vs = JSON.stringify(jsonString);
            _update_PCIChanges(skField, vs, activeParcel);
        }

        processSketchData(sketchRecords);
    }

    sketchApp.deletedVectors.filter((x) => { return !x.newRecord && !x.isFreeFormLineEntries }).forEach((v) => {
        let vTable = '', parentRecord = activeParcel['CCV_DWELDAT_APEXSKETCH'].filter((x) => { return x.CARD == v.pageNum && x.CC_Deleted != true })[0];

        if (parentRecord) vTable = 'CCV_DWELDAT_APEXSKETCH';
        else {
            parentRecord = activeParcel['CCV_COMDAT_APEXSKETCH'].filter((x) => { return x.CARD == v.pageNum && x.CC_Deleted != true })[0];
            vTable = parentRecord ? 'CCV_COMDAT_APEXSKETCH' : '';
        }

        if (vTable == 'CCV_DWELDAT_APEXSKETCH') {
            let rec = findAssociatedRecord(v, 'ADDN', parentRecord.ROWUID);
            if (rec.record) deletedRecords.push({ categoryId: addCatId, rowuid: rec.record.ROWUID });
        }
        else if (vTable == 'CCV_COMDAT_APEXSKETCH') {
            let lk = WrightCom_Lookup.filter((x) => { return x.Id == v.code })[0];

            if (lk?.TBLE == 'COMINTEXT') {
                let rec = findAssociatedRecord(v, 'COMINTEXT', parentRecord.ROWUID);
                if (rec.record) deletedRecords.push({ categoryId: ctextCatId, rowuid: rec.record.ROWUID });
            }
            else if (lk?.TBLE == 'COMFEAT') {
                let rec = findAssociatedRecord(v, 'COMFEAT', parentRecord.ROWUID);
                if (rec.record) deletedRecords.push({ categoryId: cfeatCatId, rowuid: rec.record.ROWUID });
            }
        }
    });

    var createCardRecords = function (newCards) {
        if (newCards.length == 0) {
            delete sketchApp.createdWrightSketchCards;
            processSketchData(sketchData);
            return;
        }

        let nr = newCards.pop(), cat = getCategoryFromSourceTable(nr.SoureTable);
        insertNewAuxRecord(null, null, cat.Id, nr.ROWUID, { CARD: nr.CARD }, function (rowid) {
            createCardRecords(newCards);
        });
    }

    let newcards = sketchApp.createdWrightSketchCards ? sketchApp.createdWrightSketchCards : [];

    var deleteCardRecords = function (delCards) {
        if (delCards.length == 0) {
            delete sketchApp.deletedWrightSketchCards;
            createCardRecords(newcards);
            return;
        }

        let delCard = delCards.pop(), cat = null, parentRecord = activeParcel['CCV_DWELDAT_APEXSKETCH'].filter((x) => { return x.CARD == delCard.Card && x.CC_Deleted != true })[0];

        if (parentRecord) {
            cat = getCategoryFromSourceTable('CCV_DWELDAT_APEXSKETCH');
        }
        else {
            parentRecord = activeParcel['CCV_COMDAT_APEXSKETCH'].filter((x) => { return x.CARD == delCard.Card && x.CC_Deleted != true })[0];
            cat = getCategoryFromSourceTable('CCV_COMDAT_APEXSKETCH');
        }
        if (cat && parentRecord) {
            ccma.Data.Controller.DeleteAuxRecordWithDescendants(cat.Id, parentRecord.ROWUID, null, null, function () {
                deleteCardRecords(delCards);
            }, false, null, true);
        }
        else deleteCardRecords(delCards);
    }
    
    let delcards = sketchApp.deletedWrightSketchCards ? sketchApp.deletedWrightSketchCards : [];
    deleteCardRecords(delcards);
}