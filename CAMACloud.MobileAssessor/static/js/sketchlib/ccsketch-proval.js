﻿//Rinsheed - 28-07-2018
var Proval_Lookup = [];

function convertPointToStr(p) {
	if (!p) return '';
    var d = p.split( '/' ),arclength;
    if (p.length > 1) {
        p = d[0];
        arclength = d[1];
    }
    var x = p << 16 >> 16;
    var y = p >> 16;
    var str = '';
    if (x != 0) {
        var d = x < 0 ? 'L' : 'R';
        str += d + Math.abs( x / 10 );
    }
    if (y != 0) {
        if (str != '') str += '/';
        var d = y < 0 ? 'U' : 'D';
        str += d + Math.abs( y / 10 );
    }
    if (arclength > 0)
        str = 'A' + (arclength/10) + '/' + str;
    else if ( arclength < 0 )
        str = 'B' + ( arclength / 10 ) + '/' + str;
    return str;
}

function convertPointsArrayToString(p) {
    var str = '';
    p.forEach( function (x) { str += convertPointToStr( x ) + ' '; })
    return str;
}

function convertStrToPoint(str, getLength) {
	if (str.trim() == '') return 0;
	var arc = '';
	if (str.startsWith('A') || str.startsWith('B')) { 
		arc = str.split('/')[0]; 
		arc = '/' + (arc.startsWith('B')? (parseFloat(arc.split('B')[1]) * -10): (parseFloat(arc.split('A')[1]) * 10));
		str = str.split('/')[1] + (str.split('/')[2]? ('/' + str.split('/')[2]): ''); 
	}
	var coordinates = str.split('/');
	var x = null, y = null, y1 = '0', x1 = '0', point = null, x2 = '0', y2 = '0';

	coordinates.forEach(function(c){
		if (c.startsWith('U') || c.startsWith('D')) y = c;
		else x = c;
	})

	if (y) {
		y1 = parseFloat(y.substring(1, y.length)) * 10;
		y2 = ((y.startsWith('U') ? -1 * y1: y1) << 16).toString(2);
	}
	if (x) {
		x1 = parseFloat(x.substring(1, x.length)) * 10;
		x2 = ((x.startsWith('L'))? ((-1 * x1) >>> 0).toString(2).substring(16,32): x1.toString(2)); // if L, take 2's complement.
	}	
	if(getLength)
		return (Math.sqrt(Math.pow(parseInt(x1)- 0 , 2) +Math.pow(parseInt(y1)- 0 , 2)) )/10
	else
		return parseInt(y2,2) + parseInt(x2,2) + arc;
}

function ProvalLabelPositionToString(data) {
	if (!data.label_position) return null;
	return convertPointToStr(data.label_position.toString()).replace('/', ' ');
}

function ProvalLabelPositionFromString(position) {
	var y = (position.y < 0)? 'D' + position.y * -1: 'U' + position.y;
	var x = (position.x < 0)? 'L' + position.x * -1: 'R' + position.x;
	return convertStrToPoint(x + '/' + y);
}

function OutBuildingDefinition( data, config, options, sketch ) {
    var sketchType = ( config.Table == 'ccv_sktsegment_dwellings' ) ? '_dwellings' : ( config.Table == 'ccv_sktsegment_comm_bldgs' ? '_comm_bldgs' : '_imps' );
    var sketches = [];
    var outB = ( options && options.type != 'outBuilding' ) && activeParcel.SktOutbuilding.filter( function ( s ) { return ( ( s.improvement_id == data.ClientROWUID && s.extension == data.extension ) || ( s.improvement_id == data.improvement_id && s.extension == data.extension ) ) } )
    if ( ( outB && outB.length > 0 ) && ( options && options.type != 'outBuilding' ) ) {
        outB.forEach( function ( outB ) {
        	var Improvement_Record = activeParcel.ccv_improvements.filter(function(imp){ return imp.ROWUID == outB.improvement_id })[0];
            var sk = {
                uid: outB.ROWUID.toString(),
                label: [{ Value: outB.improvement_id }],
                vector: ':' + convertPointToStr( outB.vector_start ).replace( '/', ' ' ) + ' S P1',
                hideAreaValue: true,
                vectorConfig: config,
                sketchType: 'outbuilding',
                isUnSketchedArea: true,
                imp_Type: Improvement_Record ? Improvement_Record.imp_type : null
            }
            sketches.push( sk )
        } )
        return sketches;
    }
    else if ( ( options && options.type == 'outBuilding' ) ) {
       
        $( '.Current_vector_details .head' ).html( 'OutBuilding' )
        $( '.mask' ).show();
        $( '.Current_vector_details' ).css( 'width', 460 );
        $( '.Current_vector_details' )
        var validation = true;
        $( '.skNote' ).hide(); $( '.Current_vector_details .vectorPage , .Current_vector_details .footradio,.Current_vector_details .obsketch' ).parent().remove()
        $( '.assocValidation,.UnSketched' ).remove();
        $( '.dynamic_prop div' ).remove();
        $('.dynamic_prop').html('')
        $( '.Current_vector_details' ).show()
        $( '.Current_vector_details #Btn_cancel_vector_properties' ).unbind( touchClickEvent )
        $( '.Current_vector_details #Btn_cancel_vector_properties' ).bind( touchClickEvent, function () {
            $( '.Current_vector_details' ).hide();
            if ( !sketchApp.isScreenLocked ) $( '.mask' ).hide();
            sketchApp.currentSketch.isNewSketch = false;
            sketchApp.currentSketch.isAfterSaveNewSketch = false;
           // sketchApp.sketches = sketchApp.sketches.filter( a => a.isNewSketch != true || a.vectors.length > 0 )
          
            return false;
        } )
        $( '.Current_vector_details .head' ).after( '<div class ="outbdiv"> <div class="obsketch"><label style="float: left;width: 98px;"> Draw Sketch</label><label style="float: left"><input class="obcheckbox" type="checkbox"  style="width: 20px;margin-right:7px;margin-top: -3px;"/>Yes</label></div><div><label style="float: left;width: 98px;"> Label</label><select class="obddl"></select></div></div>' );
        for ( var x in Proval_Outbuliding_Lookup ){
            $( '.obddl' ).append( '<option value="' + Proval_Outbuliding_Lookup[x].Code + '">' + Proval_Outbuliding_Lookup[x].label_description + '</option>' );
        }
        if ( options.isEdit == true && !sketchApp.currentVector.isUnSketchedArea )
            $( '.Current_vector_details .obsketch' ).remove()
        if ( options.isEdit == true && sketchApp.currentVector.isUnSketchedArea && sketchApp.currentVector.impType )
            $( '.Current_vector_details .obddl' ).val( sketchApp.currentVector.impType );
        $( '.Current_vector_details #Btn_Save_vector_properties' ).unbind( touchClickEvent )
        $( '.Current_vector_details #Btn_Save_vector_properties' ).bind( touchClickEvent, function () {
           
            if ( options.callback ) options.callback( [{ Value: $( '.obddl' ).val() }], { 'drawSketch': $( ".obcheckbox" ).prop( 'checked' ), 'OBlabelValue': $( '.obddl' ).val() } )
            if ( !sketchApp.isScreenLocked ) $( '.mask' ).hide();
            $( '.Current_vector_details' ).hide();
            if ( options.isEdit == true && $( ".obcheckbox" ).prop( 'checked' ) ) {
                sketchApp.currentVector.vectorString = null;
                sketchApp.currentVector.startNode = null;
                sketchApp.currentVector.endNode = null;
                sketchApp.currentVector.commands = [];
                sketchApp.currentVector.endNode = null;
                sketchApp.currentVector.placeHolder = false;
                sketchApp.render()
                sketchApp.startNewVector( true );
                sketchApp.currentVector.isUnSketchedArea = false;
                sketchApp.currentVector.hideAreaValue = false;
                sketchApp.currentVector.newRecord = true;
                sketchApp.currentVector.name = $( '.obddl' ).val();
                sketchApp.currentVector.labelEdited = true;
                sketchApp.currentVector.isClosed = false;
                sketchApp.currentVector.label = '[' + sketchApp.currentVector.index + '] ' + $( '.obddl' ).val();
            }

        } )
    }
    // }
}
function ProvalVectorDefinition(data, config) {
	var sketchType = (config.Table == 'ccv_sktsegment_dwellings')? '_dwellings': (config.Table == 'ccv_sktsegment_comm_bldgs'? '_comm_bldgs': '_imps');
    var vector_table = 'ccv_sktvector' + sketchType;
    var label_table = 'ccv_sktsegmentlabel' + sketchType;
    var vector_data = data[vector_table], startValue = data['vector_start'];
	var areaValue = data['area'], original_vector = [], original_start_value = data.Original['vector_start'];
	var perimeterValue = data['perimeter'];
	var vector_string = '', original_vect_str = '', isChanged = false;
	vector_data.forEach(function(vData){ original_vector.push(vData.Original); });
	if ( (sketchType=='_comm_bldgs' || sketchType=='_dwellings') && vector_data.length == 0 ) {
	    var outB = activeParcel.SktOutbuilding.filter( function ( s ) { return ( s.improvement_id == data.improvement_id && s.extension == data.extension ) } )
	}
    if (!vector_data) return '';
    vector_data = vector_data.sort(function (a, b) { return a.seq_number - b.seq_number }).map(function (a) { return a.run_rise + (a.curve_height > 0 ? '/' + a.curve_height : '') });
    original_vector = original_vector.sort(function (a, b) { return a.seq_number - b.seq_number }).map(function (a) { return a.run_rise + (a.curve_height > 0 ? '/' + a.curve_height : '') });
    if (!vector_data) return '';
    if (vector_data == '' || vector_data.length == 0) 
    	return { isChanged: false, vector_string: ':' + convertPointToStr(startValue).replace('/',' ') + ' S P2', noVectorSegment: true, areaFieldValue: areaValue, areaUnit: ' ' ,Line_Type: true,isUnSketchedTrueArea: true, perimeterFieldValue: perimeterValue};
	
    vector_string = convertPointToStr(startValue).replace('/',' ') + ' S ' + convertPointsArrayToString(vector_data);
    original_vect_str = convertPointToStr(original_start_value).replace('/',' ') + ' S ' + convertPointsArrayToString(original_vector);
    if (vector_string != original_vect_str) isChanged = true;
    return { isChanged: isChanged, vector_string: ':' + vector_string, noVectorSegment: false };
}

function ProvalNoteDefinition(sketchsource) {
    var notearray = [], note_y_positions = [];
    data = activeParcel['SktNote'] && activeParcel['SktNote'].filter(function (d) { return ( ( d.ParentROWUID && d.ParentROWUID == sketchsource.ParentROWUID ) || (d.extension == sketchsource.extension )) });
    var mod = function(val) {
    	if (val < 0) return val * -1;
    	else return val;
    }
    for (var x in data) {
        var note = data[x], mod_diff;
        var x = parseInt(note['note_position']) << 16 >> 16;
        var y = -1 *parseInt(note['note_position']) >> 16;
        if(y == 0 || y == '0' || y == NaN){
        	note_y_positions.forEach(function(y_pos){
        		mod_diff = mod(mod(y_pos) - mod(y));
        		if (mod_diff < 40) {
        			y = y - (40 - mod_diff);
        			return;
    			}
        	});
        }
        note_y_positions.push(y);
        var n = {
            uid: note[ 'ROWUID'],
            text: (note['note_text'] || '').replace( /&lt;/g, '<' ).replace( /&gt;/g, '>' ),
            x: x/10,
            y: y/10
        }
        notearray.push(n)
    }
    return notearray;
}

function ProvalLabelDefinition(data, config) {
	var seg_type = (config.Table == 'ccv_sktsegment_dwellings')? 'dwellings': ((config.Table == 'ccv_sktsegment_comm_bldgs')? 'comm_bldgs': 'imps');
	var labelTable = 'ccv_sktsegmentlabel_' + seg_type;
	var flr_table = (seg_type == 'dwellings'? 'res_floor': (seg_type == 'comm_bldgs'? 'comm_floors': null));
	var labelValue = '', label_val_orig = '', lblData, segmentLbl = {}, isChanged = false, base_array = [],count = 0, basementfract = false;
    var seg_area = parseInt(data.area);
    var getDescription = function(lbl_data){
    	return (lbl_data? (lbl_data.field_1 && lbl_data.field_1.trim() != '')? lbl_data.field_1: lbl_data.tbl_element_desc: '');
    }
    
	if (data[labelTable]) {
		var labelData = _.sortBy(data[labelTable].filter(function(lb){ return lb.status == 'A' }), 'lbl_level_code');
	
		var getBasementDetails = function(lbl_value, labelData, isOriginal) {
			if(lbl_value.length > 0)
				lbl_value = lbl_value + '|';
			basementfract = true;
			var isorginall = true;
			var lblData = labelData;
			if (isOriginal) {
				lblData = lblData.Original
				isorginall = false;
			}
			else
				segmentLbl['1'] = {comp1: 0, comp2: 0, comp3: 0,comp4: 0,modifier1: "",modifier2: "",modifier3: "",modifier4: "",modifierSign1: "",modifierSign2: "",modifierSign3: "",modifierSign4: "",prefix1: 0,prefix2: 0,prefix3: 0,prefix4: 0,walkout1: "",walkout2: "",walkout3: "",walkout4: ""}
			var frac1 = parseInt(lblData.lbl_frac1);
			var frac2 = parseInt(lblData.lbl_frac2);
			var frac3 = parseInt(lblData.lbl_frac3);
			var frac4 = parseInt(lblData.lbl_frac4);
			var finish_area = parseInt(lblData.finish_area);
			var unfin_area = parseInt(lblData.unfin_area);
			var area2 = parseInt(lblData.area2);
			var area3 = parseInt(lblData.area3);

			if(frac1 != 0 && frac1 > 0){
				var prefix1 = frac1<<16>>16;
				var pref = (prefix1 == 1) ? '1/4' : (prefix1 == 2) ? '1/2' : '3/4';
				var comp1 = frac1>>16;
				var farct= frac1.toString();
				var fin = '', finPlus = '', finMin = '', uf = '', walk = '', walkValue = '';
				if(farct.substring(0,4)=="1297")
					fin='Fin';
				else if(farct.substring(0,4)=="9887"){
					fin='Fin';
					finPlus='+';
				}
				else if(farct.substring(0,4)=="5592"){
					fin='Fin';
					finMin='-';
				}
				if(comp1>1000){
					comp1 = comp1%1000;
					walk = 'Y';
					walkValue = '-wo';
				}					
				var tmp_comp = Proval_Lookup.filter(function(pro){ return pro.tbl_type_code == 'Component' && pro.tbl_element == comp1; });
				var comps = tmp_comp.length > 0? getDescription(tmp_comp[0]): comp1;
				var lblvalue = pref + comps + walkValue + fin + finPlus + finMin;
				if(isorginall){
					segmentLbl['1'].comp1 = comp1; segmentLbl['1'].prefix1 = prefix1;segmentLbl['1'].modifier1 = fin;
					segmentLbl['1'].modifierSign1 = (finPlus != '')? finPlus : (finMin !='') ? finMin : '';
					segmentLbl['1'].walkout1 = walk;
					if(comp1 == 202 && fin !='')
					{
						base_array.push({Id:'Fin',area:finish_area})
						base_array.push({Id:'UnFin',area:unfin_area})
					}
					else{
						base_array.push({Id:comp1.toString(),area:finish_area})
					}
				}
				lbl_value = lbl_value + lblvalue;
			}
			if(frac2 != 0 && frac2 > 0){
				var prefix2 = frac2<<16>>16;
				var pref = (prefix2 == 1) ? '1/4' : (prefix2 == 2) ? '1/2' : '3/4';
				var comp2 = frac2>>16;
				var farct = frac2.toString();
				var fin = '', finPlus = '', finMin = '', uf = '', walk = '', walkValue = '';
				if(farct.substring(0,4)=="1297")
					fin='Fin';
				else if(farct.substring(0,4)=="9887"){
					fin='Fin';
					finPlus='+';
				}
				else if(farct.substring(0,4)=="5592"){
					fin='Fin';
					finMin='-';
				}
				if(comp2>1000){
					comp2 = comp2%1000;
					walk = 'Y';
					walkValue = '-wo';
				}
				var tmp_comp = Proval_Lookup.filter(function(pro){ return pro.tbl_type_code == 'Component' && pro.tbl_element == comp2; });
				var comps = tmp_comp.length > 0? getDescription(tmp_comp[0]): comp2;
				var lblvalue = pref + comps + walkValue + fin + finPlus + finMin;
				if(isorginall){
					segmentLbl['1'].comp2 = comp2; segmentLbl['1'].prefix2 = prefix2;segmentLbl['1'].modifier2 = fin;
					segmentLbl['1'].modifierSign2 = (finPlus != '')? finPlus : (finMin !='') ? finMin : '';
					segmentLbl['1'].walkout2 = walk;
					base_array.push({Id:comp2.toString(),area:area2})
				}
				lbl_value = lbl_value + ' + ' + lblvalue;
			}
			if(frac3 != 0 && frac3 > 0){
				var prefix3 = frac3<<16>>16;
				var pref = (prefix3 == 1) ? '1/4' : (prefix3 == 2) ? '1/2' : '3/4';
				var comp3 = frac3>>16;
				var farct = frac3.toString();
				var fin = '', finPlus = '', finMin = '', uf = '', walk = '', walkValue = '';
				if(farct.substring(0,4)=="1297")
					fin='Fin';
				else if(farct.substring(0,4)=="9887"){
					fin='Fin';
					finPlus='+';
				}
				else if(farct.substring(0,4)=="5592"){
					fin='Fin';
					finMin='-';
				}
				if(comp3>1000){
					comp3 = comp2%1000;
					walk = 'Y';
					walkValue = '-wo';
				}
				var tmp_comp = Proval_Lookup.filter(function(pro){ return pro.tbl_type_code == 'Component' && pro.tbl_element == comp3; });
				var comps = tmp_comp.length > 0? getDescription(tmp_comp[0]): comp3;
				var lblvalue = pref + comps + walkValue + fin + finPlus + finMin;
				if(isorginall){
					segmentLbl['1'].comp3 = comp3; segmentLbl['1'].prefix3 = prefix3;segmentLbl['1'].modifier3 = fin;
					segmentLbl['1'].modifierSign3 = (finPlus != '')? finPlus : (finMin !='') ? finMin : '';
					segmentLbl['1'].walkout3 = walk;
					base_array.push({Id:comp3.toString(),area:area3})
				}
				lbl_value = lbl_value + ' + ' + lblvalue;
			}
			if(frac4 != 0 && frac4 > 0){
				var prefix4 = frac4<<16>>16;
				var pref = (prefix4 == 1) ? '1/4' : (prefix4 == 2) ? '1/2' : '3/4';
				var comp4 = frac4>>16;
				var farct = frac4.toString();
				var fin = '', finPlus = '', finMin = '', uf = '', walk = '', walkValue = '';
				if(farct.substring(0,4)=="1297")
					fin='Fin';
				else if(farct.substring(0,4)=="9887"){
					fin='Fin';
					finPlus='+';
				}
				else if(farct.substring(0,4)=="5592"){
					fin='Fin';
					finMin='-';
				}
				if(comp4>1000){
					comp4 = comp2%1000;
					walk = 'Y';
					walkValue = '-wo';
				}
				var area4 = seg_area - (finish_area + unfin_area + area2 + area3);
				var tmp_comp = Proval_Lookup.filter(function(pro){ return pro.tbl_type_code == 'Component' && pro.tbl_element == comp4; });
				var comps = tmp_comp.length > 0? getDescription(tmp_comp[0]): comp4;
				var lblvalue = pref + comps + walkValue + fin + finPlus + finMin;
				if(isorginall){
					segmentLbl['1'].comp4 = comp4; segmentLbl['1'].prefix4 = prefix4;segmentLbl['1'].modifier4 = fin;
					segmentLbl['1'].modifierSign4 = (finPlus != '')? finPlus : (finMin !='') ? finMin : '';
					segmentLbl['1'].walkout4 = walk;
					base_array.push({Id: comp4.toString(), area: area4})
				}
				lbl_value = lbl_value + ' + ' + lblvalue;
			}
			return lbl_value;
		}
		
		var getLabelDetails = function(labelData, isOriginal) {
			var lbl_value = '';
			for (idx = labelData.length - 1; idx != -1; idx--) {
			
				if((labelData[idx].lbl_frac2 != '0' && labelData[idx].lbl_frac2 != 0) && (labelData[idx].lbl_level_code == '1' || labelData[idx].lbl_level_code == 1))
				lbl_value =	getBasementDetails(lbl_value, labelData[idx], isOriginal);
				else{
					var lblData = labelData[idx];
					if (isOriginal) lblData = lblData.Original;
					var flr_records;
					if (flr_table == 'res_floor') 
						flr_records = labelData[idx].parentRecord.parentRecord[flr_table];
					var frac1 = parseInt(lblData.lbl_frac1), const_code = parseInt(lblData.lbl_const_code), ext_feat_code = parseInt(lblData.lbl_ext_feat_code);
					var prefix = frac1<<16>>16;
					var ns='';
					if(prefix == '10' || prefix == '15'){
						ns=(lblData.lbl_n && lblData.lbl_n != '0')?lblData.lbl_n: 0 ;
						ns=parseInt(ns.toString().split(".")[0]);
					}
					var comp = frac1>>16;
					var walk=false,fin=false,finPlus=false,finMin=false,uf=false;
					var farct=frac1.toString();
					if(farct.substring(0,4)=="1297")
						fin=true;
					else if(farct.substring(0,4)=="9887"){
						fin=true;
						finPlus=true;
					}
					else if(farct.substring(0,4)=="5592"){
						fin=true;
						finMin=true;
					}
					else if(farct.substring(0,4)=="1292"){
						uf=true;
					}
					if(comp>1000){
						comp=comp%1000;
						walk=true;
					}
					var walkout = '';
					var Modifier='';
					var modifierSign='';
					var ext_cover = '';
				
					prefix = (prefix == 0 && comp == 0 && lblData.lbl_frac && lblData.lbl_frac1.toString() != '0')? frac1: prefix;
					if (['1','2','3'].indexOf(lblData.lbl_level_code) > -1 && !isOriginal) {
						segmentLbl[lblData.lbl_level_code] = { ext_feat_code: ext_feat_code, comp: comp, prefix: prefix, constr: const_code, ns: ns, walkout: '', ext_cover: lblData.default_ext_cover, modifier: '',modifierSign:'' }					
						if(uf==true){
							segmentLbl[lblData.lbl_level_code].modifier = 'UF';
							Modifier='UF'
						}
						//else if (lblData.unfin_area && lblData.unfin_area.toString() != '0'){
							//segmentLbl[lblData.lbl_level_code].modifier = 'UF';
							//Modifier='UF'
						//}
						if(fin==true){
							segmentLbl[lblData.lbl_level_code].modifier = 'Fin';
							Modifier='Fin';
							if(finPlus==true){
								segmentLbl[lblData.lbl_level_code].modifierSign = '+';
								modifierSign='+';
							}
							else if(finMin==true){
								segmentLbl[lblData.lbl_level_code].modifierSign = '-';
								modifierSign='-';
							}
						}
						//if (lblData.finish_area && lblData.finish_area.toString() != '0'){
							//segmentLbl[lblData.lbl_level_code].modifier = 'Fin';
							//Modifier='Fin';
						//}
					}
					if (prefix && prefix.toString() != '0') {
						var tmp_prefix = Proval_Lookup.filter(function(pro){ return pro.tbl_type_code == 'Prefix' && pro.tbl_element == prefix; });
						prefix = tmp_prefix.length > 0? getDescription(tmp_prefix[0]): prefix;
						if(prefix == 'N s' && ns != '')
							prefix =ns.toString() + ' s'
						else if (prefix == 'N c' && ns != '')
							prefix =ns.toString() + ' c';
						
					}
				
					if (comp && comp.toString() != '0') {
						var tmp_comp = Proval_Lookup.filter(function(pro){ return pro.tbl_type_code == 'Component' && pro.tbl_element == comp; });
						comp = tmp_comp.length > 0? getDescription(tmp_comp[0]): comp;
						if (comp.toString() == 'B' && flr_records) {
							var bsmt_flr = flr_records.filter(function(flr){ return flr.floor_key == 'B'; })[0];
							if (isOriginal) bsmt_flr = bsmt_flr.Original;
							if(walk==true){
								walkout='Y';
								segmentLbl[lblData.lbl_level_code].walkout = walkout;
							}
							//else{
								//walkout = bsmt_flr && bsmt_flr != 'undefined'? bsmt_flr.walkout_bsmt: '';
								//if (!isOriginal) segmentLbl[lblData.lbl_level_code].walkout = walkout;
							//}
						}
					}
					ext_cover = (seg_type =='dwellings' || seg_type == 'comm_bldgs') ? ((lblData.default_ext_cover && (lblData.default_ext_cover != '0' && lblData.default_ext_cover != 'null')) ? lblData.default_ext_cover : '') : '';
					if (const_code && const_code.toString() != '0') {
						var tmp_const_code = Proval_Lookup.filter(function(pro){ return pro.tbl_type_code == 'Construction' && pro.tbl_element == const_code; });
						const_code = tmp_const_code.length > 0? getDescription(tmp_const_code[0]): const_code;
					}
				
					if (ext_feat_code && ext_feat_code.toString() != '0') {
						var tmp_ext_feat_code = Proval_Lookup.filter(function(pro){ return pro.tbl_type_code == 'Exterior Feature' && pro.tbl_element == ext_feat_code; });
						ext_feat_code = tmp_ext_feat_code.length > 0? getDescription(tmp_ext_feat_code[0]): ext_feat_code;
					}
					if (['1','2','3'].indexOf(lblData.lbl_level_code) > -1) {
						lbl_value += (lbl_value.length > 0? '|': '') + (prefix || '') + (const_code || '') + (comp || '') + ((walkout == 'Y')? '-wo': '') + (ext_cover || '') + (ext_feat_code || '') + (Modifier || '') + (modifierSign || '');
					}
				}
			}
			return lbl_value;
		}
		labelValue = getLabelDetails(labelData, false);
		if (getLabelDetails(labelData, true) != labelValue) isChanged = true;
	}
	['1','2','3'].forEach(function(lvl_code) {
		if(!segmentLbl[lvl_code]) segmentLbl[lvl_code] = { ext_feat_code: 0, comp: 0, prefix: 0, constr: 0 }
	})
	if(basementfract)
		var retVal = { labelValue: labelValue, isChanged: isChanged, base_array: base_array, BasementFunctionality:true };
	else
		var retVal = { labelValue: labelValue, isChanged: isChanged };
	retVal.details = segmentLbl;
	return retVal;
}

function ProvalLabelConfig(head, items, lookups, callback, editor, type) {
	if(head == "New Vector - Labels")
		head = "New Sketch - Labels";
	function BasementFunctionality(isChangSketchLabel, basecallback){
		var labelValues = '',BasementFunctionalityDetail = [], BFIndex =  0 ,ValidateLabelsString = '',sketch_type = '', CurrentLabelValue = '', editLabelChanged = false;
		if(editor.currentSketch.isNewSketch)
			sketch_type = ((editor.currentSketch.parentRow.RorC)=='R'?'Res':'Comm')
		else
			sketch_type = ((editor.currentSketch.config.SketchLabelPrefix == 'Com - ')? 'Comm': (editor.currentSketch.config.SketchLabelPrefix == 'Dwe - ')? 'Res': 'Imp');
		var grade_col = sketch_type + 'GradeLevel';	
		var prefix_filter_col = sketch_type + 'Prefix';		
		var constr_filter_col = sketch_type + 'ConstrOption';
		var material_filter_col = sketch_type + 'Material';
	
		var option = '', headers = [{name: 'Prefix', value: 'prefix', filter: prefix_filter_col, grade_filter: grade_col},{name: 'Component', value: 'comp'}];
		var lbl_level = [{name: 'Below Grade', value: 'Below_Grade', value1: 1, value2: 'Below'}];
		var html = '<table style="width: 100%;font-size: 18px;"><tbody><tr><th>Fractional Basement Functionality</th></tr></tbody></table>';
	
		$('.Current_vector_details .head').html(head);
		$( '.dynamic_prop div,.outbdiv' ).remove();
		$('.mask').show(); $('.dynamic_prop').html('');
   		$('.dynamic_prop').append('<div class="divvectors"></div>');
    
    	var getDescription = function(lbl_data, isField_1){
    		if (isField_1) return (lbl_data? (lbl_data.field_1 && lbl_data.field_1.trim() != '')? lbl_data.field_1: lbl_data.tbl_element_desc: '');
    		return (lbl_data? (lbl_data.tbl_element_desc && lbl_data.tbl_element_desc.trim() != '')? lbl_data.tbl_element_desc: lbl_data.field_1: '');
    	}
	headers.forEach(function(hdr) {
    		if(hdr.name == 'Prefix')
    			hdr.lookup = Proval_Lookup.filter(function(pro){ return (pro.tbl_type_code == hdr.name) && (pro.tbl_element_desc == '1/4' || pro.tbl_element_desc == '1/2' || pro.tbl_element_desc == '3/4') });
    		else
    			hdr.lookup = Proval_Lookup.filter(function(pro){ return pro.tbl_type_code == hdr.name &&  pro.AdditionalScreen == "BGAreaEntry"});
    	});
    
    	lbl_level.forEach(function(lbl, index) {
    		headers.forEach(function(hdr, hdr_index) {
    			html += '<div class="Below'+ hdr.name +'" style="width: 30%;border-right: 1px solid #c7cdcf;height:200px;"><span style="float:left;width:200px;justify-content: center;font-weight: bold;font-size: 14px;">'+hdr.name+'</span>';
    			if (hdr_index >= 0)
					hdr.lookup.filter(function(lk){ return (lk[grade_col] && lk[grade_col].indexOf(lbl.value2) > -1); }).forEach(function(lk) {
						if(lk.tbl_element_desc == "Bsmt")
							html += '<span style"float:left;width:200px;";><input type="radio" id="'+lk.tbl_element+'" name="'+hdr.name+'" value="' + getDescription(lk, true) + '" style="float:left;width:100px;height:20px"> ' + getDescription(lk) + '<input id="walkout" type="checkbox" style="width:50px;height:20px;" >walkout</span>'
						else
	    					html += '<span style"float:left;width:200px;";><input type="radio" id="'+lk.tbl_element+'" name="'+hdr.name+'" value="' + getDescription(lk, true) + '" style="float:left;width:50px;height:20px"> ' + getDescription(lk) + '<br></span>';

	    			});
	    			html+='</div>'
    		});
    		html += '<div class="Belowmodifier" style="width: 20%;height:200px;"><span style="float:left;width:188px;justify-content: center;font-weight: bold;font-size: 14px;">Modifier</span>';
    		html += '<span style"float:left;width:200px;";><input type="radio" id="302" name="modifer" value="Fin" style="float:left;width:50px;height:20px;">Fin';
    		html += '<input type="radio" id="M303" name="modifierSign" value="+" style="float:left;width:50px;height:20px;" disabled>+<br></span>';
    		html += '<span style"float:left;width:200px;";><input type="radio" id="301" name="modifer" value="UF" style="float:left;width:50px;height:20px;"disabled>UF';
    		html += '<input type="radio" id="M304" name="modifierSign" value="-" style="float:left;width:56px;height:20px;" disabled>-<br></span></div>';
    	});
    	var previousLabelValue = items[0].Newlabel;
    	var notBelowLabel = '';
    	var previousLabelLength = previousLabelValue.split("|").length;
    	if(previousLabelLength > 1){
    		for(var t =0; t< previousLabelLength-1; t++)
    			notBelowLabel = notBelowLabel + previousLabelValue.split("|")[t] + '|';
    		labelValues = labelValues + notBelowLabel + previousLabelValue.split("|")[previousLabelLength - 1];
    	}
    	else
    		labelValues = labelValues + previousLabelValue;
		 
    	$('.divvectors').append(html);
    	$('.dynamic_prop').append('<span class="ValidateLabels" style="display: none; width:450px; color:Red;margin-left: 150px;">'+ValidateLabelsString+'</span><span class="CurrentLabel" style="display: none; width:450px; color:Black;margin-left: 15px;">'+CurrentLabelValue+'</span>');
    	$('.Current_vector_details').show();   
 		$('.Current_vector_details').width(650);
 		$('.divvectors').height(380);
 		//$('.Current_vector_details').css('left','10%');
 		$('.skNote').show();
    	$('.skNote').html('<span><b>Sketch Label Preview:</b>&nbsp;</span><span class="label_preview">'+labelValues+'</span>'); 
		var labelDetailsval1 = items[0].labelDetails.details[1];
		var cid = (labelDetailsval1.comp != 0 && labelDetailsval1.comp != '0') ? ($("#"+labelDetailsval1.comp+"")[0].checked = true) : 0;
		$("#"+labelDetailsval1.prefix+"")[0].checked = true;
		var mid = labelDetailsval1.modifier != ''? ((labelDetailsval1.modifier == 'Fin')? ($("#302")[0].checked = true): ($('#301')[0].checked= true)): '';
		var midsign = labelDetailsval1.modifierSign != ''? ((labelDetailsval1.modifierSign == '+' ) ? ($("#M303")[0].checked = true) : ($("#M304")[0].checked = true)): '';
		var walko = labelDetailsval1.walkout != '' ?  ((labelDetailsval1.walkout == "Y" ? ($("#walkout")[0].checked = true) :'' )) : '';
		if(labelDetailsval1.comp != '202'){
			$('#walkout')[0].checked=false;
    		$('#walkout').attr('disabled','disabled');
		}
		
		/*		
    	$("#Drawsketch").change( function(){
    		if($(this).prop("checked") == "checked"){
    			$(this).prop('checked', false);
				$('.Unarea').show();
				$('.Unperimeter').show();
			}
			else
			{
				$(this).prop('checked', 'checked');	
				$('.ValidateArea').hide();
				$('.Unarea').hide();
				$('.Unperimeter').hide();
				$('.perimeter').val("");
				$('.sqft').val("");
			}
    	});
    	
    			$('.sqft').focusout(function(t){
				$('.ValidateArea').hide();
		})
		*/
    	$("input[type='radio'],input[type='checkbox']").change(function(){
    		if(type = 'edit')
    			editLabelChanged = false;
    		$('.ValidateLabels').hide();
			var lbl_val = $(this).val();
			var lbl_Id = $(this).attr('id'); 
			var comp_current_select_val = '',pre_current_select_val= '' ,modi_current_select_val = '', modifierSign_current_select_val = '', walk_value = '';
			var lookupVal = Proval_Lookup.filter(function(lkup){ return lkup.tbl_element == lbl_Id; })[0];
    		lookupVal = lookupVal? lookupVal: {};
    		var bel_comp = $('.BelowComponent span input[type=radio]:checked');
    		var bel_pre = $('.BelowPrefix span input[type=radio]:checked');
    		var bel_modi = $('.Belowmodifier span input[type=radio]:checked');
    		comp_current_select_val =bel_comp[0]? bel_comp[0].value:''; 
    		var comp_current_select_Id =bel_comp[0]? bel_comp[0].id:'';
    		pre_current_select_val = bel_pre[0]?bel_pre[0].value:''; 
    		var pre_current_select_Id = bel_pre[0]?bel_pre[0].id:'';
    		if(bel_modi.length > 1){
    			modi_current_select_val = bel_modi[0]?bel_modi[0].value:''; 
    			var modi_current_select_Id =bel_modi[0]? bel_modi[0].id:'';
    			modifierSign_current_select_val =bel_modi[1]? bel_modi[1].value:''; 
    			var modifierSign_current_select_Id =bel_modi[1]? bel_modi[1].id:'';
    		}
    		else{
    			modi_current_select_val =bel_modi[0]? bel_modi[0].value:''; 
    			var modi_current_select_Id =bel_modi[0]? bel_modi[0].id:'';
    		}
    		if(lbl_Id == 202 || comp_current_select_Id == 202){
    			$('#walkout').removeAttr("disabled");
    		}
    		else{
    			$('#walkout')[0].checked=false;
    			$('#walkout').attr('disabled','disabled');
    		}
    		if(lbl_Id == 302 || modi_current_select_Id == 302){
				$('#M303').removeAttr("disabled");
				$('#M304').removeAttr("disabled");
    		}
    		else{
    			$('#M303').attr('disabled','disabled');
				$('#M304').attr('disabled','disabled');
    		}
    		if($('#walkout').prop("checked") == true){
				walk_value = '-wo'
    		}
    		var new_Label = pre_current_select_val + comp_current_select_val + walk_value + modi_current_select_val + modifierSign_current_select_val;
    		var gradelabel = labelValues;
    		var gradeLength = labelValues.split('|').length;
			if(gradeLength > 1) 
				gradelabel = labelValues.split('|')[gradeLength-1];			
			var LabelSplit = gradelabel.split(" + ");
			if(LabelSplit.length > 1) {
				switch(LabelSplit.length)
				{
					case 2: labelValues = notBelowLabel + LabelSplit[0] + ' + ' + new_Label ; break;
					case 3: labelValues = notBelowLabel + LabelSplit[0] + ' + ' + LabelSplit[1] + ' + ' + new_Label ; break;
					case 4: labelValues = notBelowLabel + LabelSplit[0] + ' + ' + LabelSplit[1] + ' + ' + LabelSplit[2] + ' + ' + new_Label ; break;
				}
			}
			else
				labelValues = notBelowLabel + new_Label;
			$('.label_preview').html(labelValues);
			
		 });
		 $('.Current_vector_details #Btn_Save_vector_properties').unbind(touchClickEvent);
    	 $('.Current_vector_details #Btn_Save_vector_properties').bind(touchClickEvent, function () {
    	 	valid = true;
    	 	if(!editLabelChanged){
    			var nsVal, BasementLabel = '', total_value = 0, rem_comp = [];
    			var lokValue = Proval_Lookup.filter(function (c) {return c[grade_col] && c.AdditionalScreen== "BGAreaEntry"})
    			lokValue.forEach(function (lval){
    				rem_comp.push(lval.field_1);
    			});
    			//var rem_comp = ["B","Slab","C","L","Bsmt G"];
				var prefixVal, prefixId, compVal, compId, modiferVal, modifierSignVal, walkval = '';
				items[0].Value = $('.label_preview').html();
    			items[0].Newlabel = $('.label_preview').html();
    			BasementLabel = $('.label_preview').html();
    			if(BasementLabel.split("|").length > 1)
    				BasementLabel = BasementLabel.split("|")[BasementLabel.split("|").length -1];
				prefixVal = $("input[name='Prefix']:checked").val();
				compVal = $("input[name='Component']:checked").val();
				modiferVal = $("input[name='modifer']:checked").val() ? $("input[name='modifer']:checked").val() : '' ;
				modifierSignVal = $("input[name='modifierSign']:checked").val() ? $("input[name='modifierSign']:checked").val() : '' ;
				if(!prefixVal || !compVal){
					ValidateLabelsString = "Please select label from both Prefix and Component."
					$('.ValidateLabels').html(ValidateLabelsString);
					$('.ValidateLabels').show();
					return;
				}
				prefixId = $("input[name='Prefix']:checked")[0].id;
				compId = $("input[name='Component']:checked")[0].id;
				if(compId == 202 ){
					if($('#walkout').prop("checked") == true)
						walkval = 'Y';
				}
				BasementFunctionalityDetail[BFIndex] = _.clone({comp: compId, prefix: prefixId, walkout: walkval, modifier: modiferVal, modifierSign: modifierSignVal});
				BFIndex = BFIndex + 1;
				var BasementLabelSplit = BasementLabel.split(" + ");
				var BasementLabelSplitLength = BasementLabelSplit.length, k=0, PrefixSplit = [], compSplit = [];
				for(k =0; k<BasementLabelSplitLength ; k++){
					var FinSplit = BasementLabelSplit[k].split("Fin");
					PrefixSplit[k] = FinSplit[0].match('[0-9]/[0-9]');
					switch(PrefixSplit[k][0]){
						case "1/4": total_value = total_value + 1/4;break;
						case "1/2": total_value = total_value + 1/2;break;
						case "3/4": total_value = total_value + 3/4;break;
					}
					compSplit[k] = FinSplit[0].split(PrefixSplit[k][0])[1];
					if(compSplit[k].indexOf("B-wo")>-1)
						compSplit[k] = compSplit[k].split("-wo")[0];
				}
				if(total_value > 1){
					ValidateLabelsString = "Total prefix sum greater than 1.Please choose correct prefix value";
					$('.ValidateLabels').html(ValidateLabelsString);
					$('.ValidateLabels').show();
					return;
				}
				else if(total_value < 1){
					var remaingvalue = 1- total_value;
					var rem_prefix = '';
					switch(remaingvalue){
						case .25:rem_prefix = '1/4';break;
						case .5:rem_prefix = '1/2';break;
						case .75: rem_prefix = '3/4';break;
					}
					for(k =0; k<BasementLabelSplitLength ; k++){
						var r_index = _.indexOf(rem_comp,rem_comp.filter(function(t){return t == compSplit[k]})[0])
						rem_comp.splice(r_index,1);
					}
					if(rem_comp.length <= 0){
						ValidateLabelsString = "No New Label Available.Please choose correct prefix value";
						$('.ValidateLabels').html(ValidateLabelsString);
						$('.ValidateLabels').show();
						return;
					}
					var new_comp = rem_comp.shift();
					var new_Labels = BasementLabel + ' + ' + rem_prefix + new_comp;	
					labelValues = notBelowLabel + new_Labels;
					$('.label_preview').html(labelValues);
					$("input[name='Prefix'][value='"+rem_prefix+"']")[0].checked = true;
					$("input[name='Component'][value='"+new_comp+"']")[0].checked = true;
					if($("input[name='modifer']:checked").length>0)
						$("input[name='modifer']:checked")[0].checked = false;
					if($('input[name=modifierSign]:checked').length>0)
						$('input[name=modifierSign]:checked')[0].checked = false;
					if($('#walkout')[0].checked){
						$('#walkout')[0].checked = false;
					}
					$('#'+compId+'').attr('disabled','disabled')
					if(new_comp =='B')
						$('#walkout').removeAttr('disabled','disabled');
					return;
				}
				else{
					var BFDCount = 0;
					items[0].labelDetails.details[1] = ( _.clone({comp1: 0, prefix1: 0, walkout1: '', modifier1: '', modifierSign1: '', comp2: 0, prefix2: 0, walkout2: '', modifier2: '', modifierSign2: '', comp3: 0, prefix3: 0, walkout3: '', modifier3: '', modifierSign3: '',comp4: 0, prefix4: 0, walkout4: '', modifier4: '', modifierSign4: ''}));
					//items[0].labelDetails = { 
    				//	details: {
    					//	1: _.clone(detailObj),
			 		//	},
    					//labelValue: $('.label_preview').html()
					//};
					items[0].labelDetails.labelValue = $('.label_preview').html()
					BasementFunctionalityDetail.forEach(function(BFD){
						BFDCount = BFDCount + 1;
						if(BFDCount == 1){
							items[0].labelDetails.details[1].comp1 = BFD.comp;
							items[0].labelDetails.details[1].prefix1 = BFD.prefix;
							items[0].labelDetails.details[1].walkout1 = BFD.walkout;
							items[0].labelDetails.details[1].modifier1 = BFD.modifier;
							items[0].labelDetails.details[1].modifierSign1 = BFD.modifierSign;
						}
						else if(BFDCount == 2){
							items[0].labelDetails.details[1].comp2 = BFD.comp;
							items[0].labelDetails.details[1].prefix2 = BFD.prefix;
							items[0].labelDetails.details[1].walkout2 = BFD.walkout;
							items[0].labelDetails.details[1].modifier2 = BFD.modifier;
							items[0].labelDetails.details[1].modifierSign2 = BFD.modifierSign;
						}
						else if(BFDCount == 3){
							items[0].labelDetails.details[1].comp3 = BFD.comp;
							items[0].labelDetails.details[1].prefix3 = BFD.prefix;
							items[0].labelDetails.details[1].walkout3 = BFD.walkout;
							items[0].labelDetails.details[1].modifier3 = BFD.modifier;
							items[0].labelDetails.details[1].modifierSign3 = BFD.modifierSign;
						}
						else if(BFDCount == 4){
							items[0].labelDetails.details[1].comp4 = BFD.comp;
							items[0].labelDetails.details[1].prefix4 = BFD.prefix;
							items[0].labelDetails.details[1].walkout4 = BFD.walkout;
							items[0].labelDetails.details[1].modifier4 = BFD.modifier;
							items[0].labelDetails.details[1].modifierSign4 = BFD.modifierSign;
						}
					})
				}
    			var validateLabel = true, i = 1, ValidateArea = false, CheckUnsketch = false;
    		/*
    		for(i=1; i<=3;i++){
    			var Details = items[0].labelDetails.details[i];
    			if(Details.prefix != '0'  || Details.comp != '0' || Details.ext_feat_code != '0'){
    				validateLabel = true;
    				break;
    			}
    		}
    		if(!validateLabel){
    			$('.ValidateArea').hide();
				$('.ValidateLabel').show();
				return false;
    		}
    		
    			if(!$( '#Drawsketch' )[0].checked){
					var areaa = $('.sqft').val();
					CheckUnsketch = true;
					if(areaa != '')
						ValidateArea = true;
				}
				if(validateLabel && CheckUnsketch && !ValidateArea){
					$('.ValidateArea').show();
					return false;
				}
				*/
			}
			/*
			function FlabelSave(){
				if(type == 'edit' && sketchApp.currentVector && sketchApp.currentVector.isUnSketchedTrueArea && $( '#Drawsketch' )[0].checked ){
    				sketchApp.currentVector.vectorString = null;
                	sketchApp.currentVector.startNode = null;
                	sketchApp.currentVector.endNode = null;
                	sketchApp.currentVector.commands = [];
                	sketchApp.currentVector.endNode = null;
                	sketchApp.currentVector.placeHolder = false;
                	sketchApp.currentVector.Line_Type = false;
                	sketchApp.render()
                	sketchApp.startNewVector( true );
                	sketchApp.currentVector.isUnSketchedTrueArea = false;
                	sketchApp.currentVector.isClosed = false;
    			}
    			else if(type == 'edit' && sketchApp.currentVector && sketchApp.currentVector.isUnSketchedTrueArea && !$( '#Drawsketch' )[0].checked ){
    				sketchApp.currentVector.fixedAreaTrue = $('.sqft').val() != '' ? $('.sqft').val() : sketchApp.currentVector.areaFieldValue;
    				sketchApp.currentVector.fixedPerimeterTrue = $('.perimeter').val() != '' ? $('.perimeter').val() : sketchApp.currentVector.perimeterFieldValue; 
    			}
	    		if (callback) callback(items, {'unSketchedTrue': ( $( '#Drawsketch' ).length > 0 ? ( editor.currentSketch.config.CustomToolForVector ? $( '#Drawsketch' )[0].checked : !$( '#Drawsketch' )[0].checked ) : false), 'Sqft': ($('.sqft').val() ? $('.sqft').val() : 0), 'perimeter':($('.perimeter').val() ? $('.perimeter').val() : 0), 'BasementFunctionality':true});
	    		if (!editor.isScreenLocked) $('.mask').hide();
				hideKeyboard();
				$('.Current_vector_details').css('top','25%');
	       		$('.Current_vector_details').hide();
			
			}
			*/
    		if (valid) {
				if(basecallback) basecallback();
        	}
        	else messageBox('Please check the values.')
    	});
    
    	$('.Current_vector_details #Btn_cancel_vector_properties').unbind(touchClickEvent)
    	$('.Current_vector_details #Btn_cancel_vector_properties').bind(touchClickEvent, function() {
			hideKeyboard();
			$('.CurrentLabel').remove();
			$('.Current_vector_details').css('top','25%');
        	$('.Current_vector_details').hide();
        	if (!sketchApp.isScreenLocked) $('.mask').hide();
       		return false;
    	});  
  }


  function NoNBasementFunctionality(){
	if(editor.currentSketch.isNewSketch){
		var changeVar = 0, labelValueSelect, ImpIdMethod, headers = [], changeBase = 0, firstChangeTrue = false, checkedTrue = true, checkedArea = '', checkedPerimeter ='', ImpType = '',sketch_Type ='', changeFinandExtcover = false;
	var sketch_type=((editor.currentSketch.parentRow.RorC)=='R'?'Res':'Comm')
	sketch_Type = sketch_type;
	var grade_col = sketch_type + 'GradeLevel';	
	var prefix_filter_col = sketch_type + 'Prefix';		
	var constr_filter_col = sketch_type + 'ConstrOption';
	var material_filter_col = sketch_type + 'Material';
	
	var impGrade_col='ImpGradeLevel';
	var impGrade_colprefix_filter_col ='ImpPrefix';		
	var impConstr_filter_col = 'ImpConstrOption';
	var impMaterial_filter_col = 'ImpMaterial';
    
    var getDescription = function(lbl_data, isField_1){
    	if (isField_1) return (lbl_data? (lbl_data.field_1 && lbl_data.field_1.trim() != '')? lbl_data.field_1: lbl_data.tbl_element_desc: '');
    	return (lbl_data? (lbl_data.tbl_element_desc && lbl_data.tbl_element_desc.trim() != '')? lbl_data.tbl_element_desc: lbl_data.field_1: '');
    }
    
    function changesegment(rang){
    	var html="";
    	if(rang){
    		 grade_col = rang + 'GradeLevel';	
			 prefix_filter_col = rang + 'Prefix';		
			 constr_filter_col = rang + 'ConstrOption';
			 material_filter_col = rang + 'Material';
			 impGrade_col="";
	 impGrade_colprefix_filter_col ="";		
	 impConstr_filter_col = "";
	 impMaterial_filter_col = "";
    	}

	var option = ''; 
	headers = [{name: 'Prefix', value: 'prefix', filter: prefix_filter_col, grade_filter: grade_col}, {name: 'Construction', value: 'constr', filter: constr_filter_col, grade_filter: grade_col}, {name: 'Component', value: 'comp'}, {name: 'Exterior Feature', value: 'ext_feat_code'}];
	var lbl_level = [{name: 'Below Grade', value: 'Below_Grade', value1: 1, value2: 'Below'}, {name: 'At Grade', value: 'At_Grade', value1: 2, value2: 'At'}, {name: 'Above Grade', value: 'Above_Grade', value1: 3, value2: 'Above'}];
	 html = '<table style="width: 100%;"><tr><th>Sketch Label </th>';
	
		 headers.forEach(function(hdr) {
    	html += ('<th>' + hdr.name + '</th>');
    	hdr.lookup = Proval_Lookup.filter(function(pro){ return pro.tbl_type_code == hdr.name });
    });

	$('.Current_vector_details .head').html(head);
	$( '.dynamic_prop div,.outbdiv' ).remove();
	$('.mask').show(); $('.dynamic_prop').html('');
	$('.dynamic_prop').append('<div class="UnSketched"> <div class="UnSketchedarea" style="height:45px;"><label style="float: left;width: 98px;"> Draw Sketch</label><label style="float: left"><input class="checkbox" type="checkbox" checked="checked" style="width: 20px;margin-right:7px;margin-top: -3px;">Yes</label></div><div class="Unarea" style="height:45px;display: none;"><label style="float: left;width: 50px;"> Area</label><input class="sqft" type="number" style="width: 100px;margin-right:7px;margin-top: -3px;"></div><div class="Unperimeter" style="height:45px;display: none;"><label style="float: left;width: 80px;"> Perimeter</label><input class="perimeter" type="number" style="width: 100px;margin-right:7px;margin-top: -3px;"></div><span class="ValidateArea" style="display: none; width:250px; padding-left:40px; color:Red;">Please fill Area Field.</span></div>');
    $('.dynamic_prop').append('<div class="divvectors"></div>');

    lbl_level.forEach(function(lbl, index) {
    	html += ('<tr class="lbl_level" name="' + lbl.value2 + '"><td style="font-weight:500;">' + lbl.name + '</td>');
    	option = '';
    	headers.forEach(function(hdr, hdr_index) {
    		html += '<td><select type="' + hdr.value + '" grade="' + lbl.value + '" class="segmentLbl ' + hdr.value + '">';
    		html += '<option value="0"></option>';
    		if (hdr_index >= 0)
				hdr.lookup.filter(function(lk){ return ((lk[grade_col] && lk[grade_col].indexOf(lbl.value2) > -1) ||lk[impGrade_col] && lk[impGrade_col].indexOf(lbl.value2) > -1 ); }).forEach(function(lk) {
	    			html += '<option value="' + lk.tbl_element + '" value1="' + getDescription(lk, true) + '">' + getDescription(lk) + '</option>';
	    		});
    		html += '</select></td>';
    	});
    	html += '</tr>';
    	html += '<tr class="extraval" style="display: none;"><td></td><td><input style="height: 20px; width: 96px;" placeholder="N-" onkeypress="if(this.value.length==20) return false;" type="number" class="' + lbl.value + ' ns" readonly="readonly"></td><td style="float: right; font-style: italic;">Walkout:</td><td><select grade="' + lbl.value + '" class="' + lbl.value + ' walkout" style="height: 24px; width: 100px;" disabled><option value=" "></option><option value="Y">Y</option><option value="N">N</option></select></td></tr>';
    	
    	html += '<tr class="add_vals"><td style="float: right; font-style: italic;">Default Exterior Cover:</td><td><select class="' + lbl.value + ' ext_cover">';
    	
    	if(sketch_Type =='Res')
    		Proval_Default_Exterior_Cover.filter(function(pro){ return pro.tbl_type_code == 'extcover' }).forEach(function(extCover){
    			html += '<option value="' + extCover.tbl_element + '" value1="' + getDescription(extCover, true) + '">' + getDescription(extCover) + '</option>';
    		});
    	else if(sketch_Type =='Comm'){
    		html +='<option value="0" value1="None">None</option>';
    		Proval_Default_Exterior_Cover.filter(function(pro){ return pro.tbl_type_code == 'MSExtWall' }).forEach(function(extCover){
    			html += '<option value="' + extCover.tbl_element + '" value1="' + getDescription(extCover, true) + '">' + getDescription(extCover) + '</option>';
    		});
    	}
    	
    	html += '</select></td><td style="padding-left: 10px; font-style: italic;" colspan="2">Modifier:<select style="width: 100px; float: inherit;" class="' + lbl.value + ' modifier"><option value=""></option>'; 
    	
    	Proval_Lookup.filter(function(pro){ return (pro.tbl_type_code == 'Modifier' && getDescription(pro) &&(( pro[grade_col] && pro[grade_col].indexOf(lbl.value2) > -1) ||(pro[impGrade_col] && pro[impGrade_col].indexOf(lbl.value2) > -1))); }).forEach(function(mod){
    		if (getDescription(mod) != '+' && getDescription(mod) != '-')
    			html += '<option value="' + getDescription(mod) + '" value1="' + getDescription(mod, true) + '">' + getDescription(mod) + '</option>';
    	});
    	
    	html += '</select><select style="width: 40px;float: inherit;" class="' + lbl.value + ' modifierSign" disabled="disabled"><option value=""></option><option value="+">+</option><option value="-">-</option></select></td><td><select class="' + lbl.value + ' material" disabled="disabled"><option value="">-Material-</option>';
    	
    	Proval_Lookup.filter(function(pro){ return (pro.tbl_type_code == 'Material' &&(( pro[grade_col] && pro[grade_col].indexOf(lbl.value2) > -1) ||(pro[impGrade_col] && pro[impGrade_col].indexOf(lbl.value2) > -1))); }).forEach(function(mat){
    		html += '<option value="' + getDescription(mat) + '" value1="' + getDescription(mat, true) + '">' + getDescription(mat) + '</option>';
    	});
    	
    	html += '</select></td></tr>';
		html += (index != lbl_level.length -1)? '<tr><td colspan="5" style="border-bottom: 1.5px solid #b9b9b9;"></td></tr><tr></tr><tr></tr>': '';
    });


    $('.divvectors').append(html + '</table>');
    $('.divvectors').append('<span class="ValidateLabel" style="display: none; width:450px; color:Red;">Please choose any label from Prefix, Components or Exterior Feature.</span>');
    //$('.divvectors').append('<span class="ValidateArea" style="display: none; width:250px; color:Red;">Please fill Area Field.</span>');
    $('.Current_vector_details').show();   
 	$('.Current_vector_details').width(690);
 	$('.divvectors').width(655);
 	$('.divvectors').height(300);
 	$('.Current_vector_details').css('top','15%');
 	//$('.Current_vector_details').css('left','5%');
 	if(firstChangeTrue){
 		if(!checkedTrue){
    		$('.checkbox').removeAttr('checked')
			$('.Unarea').show();
			$('.Unperimeter').show();
			$('.perimeter').val(checkedPerimeter);
			$('.sqft').val(checkedArea);	
 		}
 	}
    $('.lbl_level td').css('padding','3px 0px');
    $('.divvectors').css('padding-bottom','0px');
    $('.dynamic_prop').css('padding-bottom','0px');
    $('.skNote').show();
    $('.skNote').html('<span><b>Sketch Label Preview:</b>&nbsp;</span><span class="label_preview">' + (items[0].Value || '') + '</span>'); 


 var isWalkout = false;
 $('.segmentLbl').unbind();
    $('.segmentLbl,.modifier,.modifierSign,.ns,.ext_cover').change(function(index){
    $('.ValidateLabel').hide();
    		var lbls = [], codes = [], lbl = '', code = '', that = this, lbl_value = null, extraValues = '', FirstSelectFin = false;
			var tr = $(that).parents('.lbl_level'), thisValue = $(that).val();
			if($(that).attr('class').search('modifier')>-1){
			if(thisValue == 'Fin' || thisValue == 'UF' ||thisValue ==""){
				FirstSelectFin = true;
				var next_select = $(this).next('select');
    			if ($(this).val() == 'Fin') $(next_select).removeAttr('disabled');
    			else {
    				$(next_select).val('');
    				$(next_select).attr('disabled', 'disabled');
    			}
			}
			}
		var extra_tr = $(tr).next('tr'), extraVal = false;
		var lookupVal = {};
		var labelLevel=$(tr).attr('name');
    	var tttype=$(that).attr('type');
		if(thisValue != '212')
			lookupVal = Proval_Lookup.filter(function(lkup){ return lkup.tbl_element == thisValue; })[0];
		else
			lookupVal = Proval_Lookup.filter(function(lkup){ return lkup.tbl_element == thisValue && lkup.CommGradeLevel == labelLevel ; })[0];
    	lookupVal = lookupVal? lookupVal: {};
    	var chanvalue=thisValue;
    	var sketch_type=((editor.currentSketch.parentRow.RorC)=='R'?'Res':'Comm')
    	if(changeVar==0){
    		firstChangeTrue = true;
    		var ext =false;
    		if($(that)[0]){
    			var cname = $(that)[0].className ? $(that)[0].className : '';
    			var t = cname.search('ext_cover');
				if(t > -1)
					ext = true;				
    		}
    		if(FirstSelectFin || ext)
    			changeFinandExtcover = true;
    		if($('.checkbox').prop("checked") == "checked"){
    			checkedTrue = true;
    		}
    		else{
    			checkedTrue = false;
    			checkedArea = $('.sqft').val() ;
    			checkedPerimeter = $('.perimeter').val();
    		}
    		if(sketch_type=='Res'){
    			if(lookupVal.ResGradeLevel!=null || (ext || FirstSelectFin)){
    				labelValueSelect='Res';
    				changesegment(sketch_type);
    			}
    			else{
    				if(lookupVal.ImpGradeLevel!=null){
    					var resketch='Imp';
    					sketch_Type ='Imp';
    					labelValueSelect='Imp';
    					changesegment(resketch);
    				}
    			}
    		}
    		else if(sketch_type=='Comm'){
    			if(lookupVal.CommGradeLevel!=null || (ext || FirstSelectFin)){
					labelValueSelect='Comm';
    				changesegment(sketch_type);
    			}
    			else{
    				if(lookupVal.ImpGradeLevel!=null){
    					var resketch='Imp';
    					labelValueSelect='Imp';
    					changesegment(resketch);
    				}
    			}
    		}	
		$('.divvectors tr[name="'+ labelLevel +'"] td select[type="'+ tttype +'"] option[value="'+ chanvalue +'"]').parent('select').val(chanvalue)
		changeVar=1;
	}
	if(labelValueSelect == 'Res'){
		if(changeFinandExtcover){
			ImpIdMethod = 'D';
			ImpType = 'DWELL'
		}
		else{
			if(lookupVal && lookupVal.ImpIDMethod)
				ImpIdMethod = (ImpIdMethod == 'M') ? ImpIdMethod : lookupVal.ImpIDMethod;
			if(lookupVal && lookupVal.ImpType)	
				ImpType = (ImpType == 'MHOME') ? ImpType : lookupVal.ImpType;
		}
	}
	else if(labelValueSelect=='Comm'){
		if(changeFinandExtcover){
			ImpIdMethod = 'C';
			ImpType = 'COMBLDG'
		}
		else{
			if(lookupVal && lookupVal.CommImpIDMethod)
				ImpIdMethod=lookupVal.CommImpIDMethod;
			if(lookupVal && lookupVal.CommImpType)	
				ImpType = lookupVal.CommImpType;
		}
	}
	else{
		if(lookupVal && lookupVal.ImpIDMethod1)
			ImpIdMethod=lookupVal.ImpIDMethod1;
		if(lookupVal && lookupVal.ImpType1)	
			ImpType = lookupVal.ImpType1;
	}
	if((chanvalue=="202" || chanvalue==202) && labelLevel=="Below" && changeBase==0){
		var rd=$('.extraval')[0];
		$(rd).attr('style','inline');
		$('.Below_Grade.walkout').removeAttr('disabled')
	}
	if((chanvalue == "15" || chanvalue == "10") && changeBase==0 ){
		var nsIndex = 0;
		if(labelLevel=="Below")
			nsIndex = 0
		else if(labelLevel=="At")
			nsIndex = 1;
		else if(labelLevel == "Above")
			nsIndex = 2;
		var kr=$('.extraval')[nsIndex];
		$(kr).show();
		var rx=$('.ns')[nsIndex];
		$(rx).removeAttr('readonly');

	}
	changeFinandExtcover = false; FirstSelectFin = false;
		changeBase=1;
    	if (!isWalkout) {
	    	if (lookupVal && lookupVal[material_filter_col] && lookupVal[material_filter_col] == 'Y') 
				$('.material', $(extra_tr).next('tr')).removeAttr('disabled');
			else $('.material', $(extra_tr).next('tr')).attr('disabled', 'disabled');
	    	if ($(that).hasClass('prefix')) {
	    		if (thisValue == '10' ||thisValue == '15' ) {
	    				$(extra_tr).show();
	    				$('.ns', extra_tr).val('N-')
	    				$('.ns', extra_tr).removeAttr('readonly');
					}
	    		else {
	    			if ($('.walkout', extra_tr).attr('disabled')) $(extra_tr).hide();
	    			$('.ns', extra_tr).attr('readonly', 'readonly');
				}
	    	}
	    	if ($(that).hasClass('comp')) {
	    		if (thisValue == '202' && sketch_type == 'Res') { 
	    			$(extra_tr).show(); 
	    			$('.walkout', extra_tr).removeAttr('disabled'); 
				}
	    		else {
	    			$('.walkout', extra_tr).attr('disabled', '');
	    			$('.walkout', extra_tr).val('');    		
	    			if ($('.walkout', extra_tr).attr('disabled')) $(extra_tr).hide();
				}
				var html = '<option value="0"></option>';
				if (thisValue == '0') {
					$('.prefix', tr).html(html);
					Proval_Lookup.filter(function(lk){ return (lk[grade_col] && lk.tbl_type_code =='Prefix'  && lk[grade_col].indexOf($(tr).attr('name')) > -1); }).forEach(function(lk) {
	    				html += '<option value="' + lk.tbl_element + '" value1="' + getDescription(lk, true) + '">' + getDescription(lk) + '</option>';
	    			});
	    			$('.prefix', tr).html(html);
	    			var html = '<option value="0"></option>';
					$('.constr', tr).html(html);
					Proval_Lookup.filter(function(lk){ return (lk[grade_col] && lk.tbl_type_code =='Construction'  && lk[grade_col].indexOf($(tr).attr('name')) > -1); }).forEach(function(lk) {
	    				html += '<option value="' + lk.tbl_element + '" value1="' + getDescription(lk, true) + '">' + getDescription(lk) + '</option>';
	    			});
	    			$('.constr', tr).html(html);
				}
				else {
					var array = [0,1];
					array.forEach(function(idx){
						var hdr = headers[idx];
						lookupVal = Proval_Lookup.filter(function(lkup){ return lkup.tbl_element == thisValue && lkup[hdr.grade_filter] && lkup[hdr.grade_filter].indexOf($(tr).attr('name')) > -1; })[0];
						lookupVal = lookupVal? lookupVal: {};
						var valid_fields = lookupVal[hdr.filter]? lookupVal[hdr.filter].split(','): [];
						hdr.lookup.filter(function(lkp){ return (valid_fields.indexOf(lkp.tbl_element_desc) > -1); }).forEach(function(lkup){
							html += '<option value="' + lkup.tbl_element +'" value1="' + getDescription(lkup, true) + '">' + getDescription(lkup) + '</option>';
						});
						$('.' + hdr.value, tr).html(html);
						html = '<option value="0"></option>';
					});
				}
	    	}
    	}
    	$('.lbl_level').each(function(index, lvl){
    			var mValue=$('.add_vals').eq(index).find('.modifier').val();
    			var modSignValue=$('.add_vals').eq(index).find('.modifierSign').val();
    			var ext_covers='';
    			ext_covers = $('.add_vals').eq(index).find('.ext_cover').val();
    			lbl = ''; code = ''; lbl_value = null;
    			extraValues=$('.extraval').eq(index).find('.ns').val();
    			$('select', lvl).each(function(index, select){
    				lbl_value = $('option[value="'+ $(select).val() +'"]', select).attr('value1');
    				if((lbl_value == 'N s' || lbl_value == 'N c') && index == 0 ){
    					if(extraValues && extraValues != '' && lbl_value == 'N s')
    						lbl_value = extraValues + ' s';
    						else if(extraValues && extraValues != '' && lbl_value == 'N c')
    						lbl_value = extraValues + ' c';
    				}
    				lbl += lbl_value? lbl_value: '';
    				if ($(select).hasClass('comp') && $(select).val() == '202') {
    					if ($('.walkout[grade="' + $(select).attr('grade') + '"]').val() == 'Y')
    						lbl += '-wo';
					}
    				code += $(select).val() + ',';
    			});
    			if(ext_covers && ext_covers != '' && ext_covers != '0' )
    		    	lbl=lbl+ext_covers;
    			if ((lbl && lbl != '') ||( mValue && mValue != '')){
    				if(mValue && mValue != '' || modSignValue && modSignValue !='')
    				 	lbl=lbl+mValue+modSignValue; 
    				 lbls.push(lbl);
    			}
    			if (code && code != '') codes.push(code.substring(0, code.length - 1));
    		});
    	$('.label_preview').html(lbls.reverse().join('|'));
    });
    $('.walkout').unbind();
    $('.walkout').bind('change', function() {    	
    	isWalkout = true;
		$(this).parents('.extraval').prev().find('.comp').trigger('change');
		isWalkout = false;
    })
    
    $("input[type='checkbox']").change(function(){
    		if($('.checkbox').prop("checked") == "checked"){
    			$('.checkbox').prop('checked', false);
				$('.Unarea').show();
				$('.Unperimeter').show();
			}
			else
			{
				$('.checkbox').prop('checked', 'checked');	
				$('.ValidateArea').hide();
				$('.Unarea').hide();
				$('.Unperimeter').hide();
				$('.perimeter').val("");
				$('.sqft').val("");
			}
    	});
    $('.sqft').focusout(function(t){
		$('.ValidateArea').hide();
	})
    $('.Current_vector_details #Btn_Save_vector_properties').unbind(touchClickEvent);
    $('.Current_vector_details #Btn_Save_vector_properties').bind(touchClickEvent, function () {
        var ssId=sketchApp.currentSketch.sid;
		var cateeId;
		function labelSave(){
			sketchApp.currentSketch.ImpIdMethod=ImpIdMethod;
			sketchApp.currentSketch.ImpType = ImpType;
			sketchApp.currentSketch.isNewSketch=false;
			sketchApp.currentSketch.isAfterSaveNewSketch=true;
			var valid = true, nsVal;
    		var detailObj = {ext_feat_code: 0, comp: 0, prefix: 0, constr: 0, ns: 0, walkout: '', ext_cover: '', modifier: '', modifierSign:''};
    		items[0].Value = $('.label_preview').html();
    		items[0].Newlabel = $('.label_preview').html();
    		if (!items[0].labelDetails) 
    			items[0].labelDetails = { 
    			details: {
    				1: _.clone(detailObj),
					2: _.clone(detailObj),
					3: _.clone(detailObj)
			 	},
    			labelValue: $('.label_preview').html()
			};
    		$('.lbl_level').each(function(lvl_idx, lvl){
    			$('select', lvl).each(function(lkp_idx, select){
    				items[0].labelDetails.details[lvl_idx + 1][$(select).attr('type')] = $(select).val();
    			});
    			items[0].labelDetails.details[lvl_idx + 1].ext_cover = $('.ext_cover', $(lvl).next().next()).val();
    			items[0].labelDetails.details[lvl_idx + 1].modifier = $('.modifier', $(lvl).next().next()).val();
    			items[0].labelDetails.details[lvl_idx + 1].modifierSign = $('.modifierSign', $(lvl).next().next()).val();
    			if ($('.prefix', lvl).val() == '10' || $('.prefix', lvl).val() == '15') {
	    			nsVal = $('.ns', $(lvl).next()).val();
	    			valid = valid && !isNaN(nsVal);
    				items[0].labelDetails.details[lvl_idx + 1].ns = (isNaN(nsVal)|| nsVal == "")? 0: parseInt(nsVal);
				}
    			if ($('.comp', lvl).val() == '202')
    				items[0].labelDetails.details[lvl_idx + 1].walkout = $('.walkout', $(lvl).next()).val();
    		});
    		var validateLabel = false, i = 1, ValidateArea = false, CheckUnsketch = false;
    		for(i=1; i<=3;i++){
    			var Details = items[0].labelDetails.details[i];
    			if(Details.prefix != '0'  || Details.comp != '0' || Details.ext_feat_code != '0'){
    				validateLabel = true;
    				break;
    			}
    		}
    		if(!validateLabel){
    			$('.ValidateArea').hide();
				$('.ValidateLabel').show();
				return false;
    		}
    		if(!$( '.UnSketchedarea .checkbox' )[0].checked){
				var areaa = $('.sqft').val();
				CheckUnsketch = true;
				if(areaa != '')
					ValidateArea = true;
			}
			if(validateLabel && CheckUnsketch && !ValidateArea){
				$('.ValidateArea').show();
				return false;
			}
			var areaSketch = false, sqftvalue = 0, perivalue = 0;
			if( $( '.UnSketchedarea .checkbox' ).length > 0 && !$( '.UnSketchedarea .checkbox' )[0].checked){
				areaSketch = true;
				sqftvalue = $('.sqft').val() ? $('.sqft').val(): 0;
				perivalue = $('.perimeter').val() ? $('.perimeter').val() : 0;
			}
    		if (valid) {
    			var basefunctionTrue = false;
    			function sktsave(){
    	    		editor.refreshControlList()
            		$( editor.sketchSelector ).prop( "selectedIndex", ( editor.sketches.length ) )
            		$( editor.sketchSelector ).change();
	    			if (callback) callback(items, {'unSketchedTrue': ( areaSketch ? areaSketch : false), 'Sqft': (sqftvalue ? sqftvalue: 0), 'perimeter':(perivalue ? perivalue : 0), 'BasementFunctionality':basefunctionTrue});
	    			if (!editor.isScreenLocked) $('.mask').hide();
					hideKeyboard();
					$('.Current_vector_details').css('top','25%');
	        		$('.Current_vector_details').hide();
	        	}
    			if(((items[0].labelDetails.details[1].prefix == '1' || items[0].labelDetails.details[1].prefix == 1) || (items[0].labelDetails.details[1].prefix == '2' || items[0].labelDetails.details[1].prefix == 2) || (items[0].labelDetails.details[1].prefix == '3' || items[0].labelDetails.details[1].prefix == 3)) && (items[0].labelDetails.details[1].ext_feat_code == 0 ||items[0].labelDetails.details[1].ext_feat_code == '0')){
    				basefunctionTrue = true;
    				var compval = true;
    				if(items[0].labelDetails.details[1].comp != '0' && items[0].labelDetails.details[1].comp != '0'){
    					var fiid = items[0].labelDetails.details[1].comp;
    					var plookup = Proval_Lookup.filter(function(x){return x.tbl_element == fiid})[0];
    					if(plookup.AdditionalScreen != "BGAreaEntry")
    						compval = false;
    				}
    				if(!compval){
    					basefunctionTrue = false;
						sktsave();
						return;
    				}
    				else{
    					BasementFunctionality(false, function(){
    						sktsave();
    					});
    				}
    			}
    			else
    				sktsave();
        	}
        	else messageBox('Please check the values.')
		}
    	if(labelValueSelect=='Imp'){
    		var categor = getCategoryFromSourceTable("ccv_improvements");
    		cateeId= categor.Id;
            var maxRecord = categor.MaxRecords;
            var recordd = sketchApp.currentSketch.parentRow.ccv_improvements;
           if(maxRecord != null && (recordd.length >= maxRecord) ){
            	 changeVar=0
            	 $('.Current_vector_details').hide();
                 messageBox('Maximum number of records has been reached for this category', function() {
                         if (!sketchApp.isScreenLocked) $('.mask').hide();
                         return false;
                 })
            }
            else
            {
            	sketchApp.currentSketch.config = ccma.Sketching.Config.sources[0];
    			sketchApp.currentSketch.label = "Imp - /null [ ]";
            	labelSave();   
            }
    	}
    	else if(labelValueSelect=='Comm'){
    		var categor = getCategoryFromSourceTable("ccv_improvements_comm_bldgs");
    		cateeId= categor.Id;
            var maxRecord = categor.MaxRecords;
            var recordd = sketchApp.currentSketch.parentRow.ccv_improvements_comm_bldgs;
            if(maxRecord != null && (recordd.length >= maxRecord) ){
            	changeVar=0
            	$('.Current_vector_details').hide();
                messageBox('Maximum number of records has been reached for this category', function() {
                         if (!sketchApp.isScreenLocked) $('.mask').hide();
                         return false;
                })
            }
            else
            {
				sketchApp.currentSketch.config=ccma.Sketching.Config.sources[2];
    			sketchApp.currentSketch.label="Com - /null [ ]";
            	labelSave();   
            }
    	}
    	else if(labelValueSelect=='Res'){
    		var categor = getCategoryFromSourceTable("ccv_improvements_dwellings");
            cateeId= categor.Id;
            var maxRecord = categor.MaxRecords;
            var recordd = sketchApp.currentSketch.parentRow.ccv_improvements_dwellings;
            if(maxRecord != null && (recordd.length >= maxRecord) ){
            	 changeVar=0;
            	 $('.Current_vector_details').hide();
                 messageBox('Maximum number of records has been reached for this category', function() {
                         if (!sketchApp.isScreenLocked) $('.mask').hide();
                         return false;
                 })
            }
            else
            {
            	sketchApp.currentSketch.config=ccma.Sketching.Config.sources[1];
            	sketchApp.currentSketch.label="Dwe - /null [ ]"; 
            	labelSave();   
            }
    	}
    	else
    		labelSave();
    });
    
    $('.Current_vector_details #Btn_cancel_vector_properties').unbind(touchClickEvent)
    $('.Current_vector_details #Btn_cancel_vector_properties').bind(touchClickEvent, function() {
		hideKeyboard();
		$('.Current_vector_details').css('top','25%');
        $('.Current_vector_details').hide();
        if (!sketchApp.isScreenLocked) $('.mask').hide();
        return false;
    }); 

	}

    changesegment();
	}
	
	else{
		var sketch_type = ((editor.currentSketch.config.SketchLabelPrefix == 'Com - ')? 'Comm': (editor.currentSketch.config.SketchLabelPrefix == 'Dwe - ')? 'Res': 'Imp');
		var DrawSketchTrue = false, CurrentLabelValue ='', clval ='', lblCurrentField = '';
		var partialLabelss = '';
		lblCurrentField = (items[0] &&  items[0].Value) ? items[0].Value : '';
		if(type == 'edit' && sketchApp.currentVector && sketchApp.currentVector.BasementFunctionality){
			partialLabelss =  items[0] &&  items[0].Value;
			if(partialLabelss.split('|').length > 0)
				partialLabelss = partialLabelss.split('|')[partialLabelss.split('|').length - 1];
			lblCurrentField = lblCurrentField.split(' + ')[0];
		}
		var grade_col = sketch_type + 'GradeLevel';	
		var prefix_filter_col = sketch_type + 'Prefix';		
		var constr_filter_col = sketch_type + 'ConstrOption';
		var material_filter_col = sketch_type + 'Material';
	
		var option = '', headers = [{name: 'Prefix', value: 'prefix', filter: prefix_filter_col, grade_filter: grade_col}, {name: 'Construction', value: 'constr', filter: constr_filter_col, grade_filter: grade_col}, {name: 'Component', value: 'comp'}, {name: 'Exterior Feature', value: 'ext_feat_code'}];
		var lbl_level = [{name: 'Below Grade', value: 'Below_Grade', value1: 1, value2: 'Below'}, {name: 'At Grade', value: 'At_Grade', value1: 2, value2: 'At'}, {name: 'Above Grade', value: 'Above_Grade', value1: 3, value2: 'Above'}];
		var html = '<table style="width: 100%;"><tr><th>Sketch Label </th>';
	
		$('.Current_vector_details .head').html(head);
		$( '.dynamic_prop div,.outbdiv' ).remove();
		$('.mask').show(); $('.dynamic_prop').html('');
		if(type == 'new' || (type == 'edit' && sketchApp.currentVector.isUnSketchedTrueArea)){
			DrawSketchTrue = true;
			$('.dynamic_prop').append('<div class="UnSketched"> <div class="UnSketchedarea" style="height:45px;"><label style="float: left;width: 98px;"> Draw Sketch</label><label style="float: left"><input class="checkbox" type="checkbox" checked="checked" style="width: 20px;margin-right:7px;margin-top: -3px;">Yes</label></div><div class="Unarea" style="height:45px;display: none;"><label style="float: left;width: 50px;"> Area</label><input class="sqft" type="number" style="width: 100px;margin-right:7px;margin-top: -3px;"></div><div class="Unperimeter" style="height:45px;display: none;"><label style="float: left;width: 80px;"> Perimeter</label><input class="perimeter" type="number" style="width: 100px;margin-right:7px;margin-top: -3px;"></div><span class="ValidateArea" style="display: none; width:250px; padding-left:40px; color:Red;">Please fill Area Field.</span></div>');
		}
   		$('.dynamic_prop').append('<div class="divvectors"></div>');
    
    	var getDescription = function(lbl_data, isField_1){
    		if (isField_1) return (lbl_data? (lbl_data.field_1 && lbl_data.field_1.trim() != '')? lbl_data.field_1: lbl_data.tbl_element_desc: '');
    		return (lbl_data? (lbl_data.tbl_element_desc && lbl_data.tbl_element_desc.trim() != '')? lbl_data.tbl_element_desc: lbl_data.field_1: '');
    	}
    
    	headers.forEach(function(hdr) {
    		html += ('<th>' + hdr.name + '</th>');
    		hdr.lookup = Proval_Lookup.filter(function(pro){ return pro.tbl_type_code == hdr.name });
    	});
    
    	lbl_level.forEach(function(lbl, index) {
    		html += ('<tr class="lbl_level" name="' + lbl.value2 + '"><td style="font-weight:500;">' + lbl.name + '</td>');
    		option = '';
    		headers.forEach(function(hdr, hdr_index) {
    			html += '<td><select type="' + hdr.value + '" grade="' + lbl.value + '" class="segmentLbl ' + hdr.value + '">';
    			html += '<option value="0"></option>';
    			if (hdr_index >= 0)
					hdr.lookup.filter(function(lk){ return (lk[grade_col] && lk[grade_col].indexOf(lbl.value2) > -1); }).forEach(function(lk) {
	    				html += '<option value="' + lk.tbl_element + '" value1="' + getDescription(lk, true) + '">' + getDescription(lk) + '</option>';
	    			});
    			html += '</select></td>';
    		});
    		html += '</tr>';
    		html += '<tr class="extraval" style="display: none;"><td></td><td><input style="height: 20px; width: 96px;" onkeypress="if(this.value.length==20) return false;" placeholder="N-" type="number" class="' + lbl.value + ' ns" readonly="readonly"></td><td style="float: right; font-style: italic;">Walkout:</td><td><select grade="' + lbl.value + '" class="' + lbl.value + ' walkout" style="height: 24px; width: 100px;" disabled><option value=" "></option><option value="Y">Y</option><option value="N">N</option></select></td></tr>';
    	
    		html += '<tr class="add_vals"><td style="float: right; font-style: italic;">Default Exterior Cover:</td><td><select class="' + lbl.value + ' ext_cover">';
    	
    		if(sketch_type =='Res')
    			Proval_Default_Exterior_Cover.filter(function(pro){ return pro.tbl_type_code == 'extcover' }).forEach(function(extCover){
    				html += '<option value="' + extCover.tbl_element + '" value1="' + getDescription(extCover, true) + '">' + getDescription(extCover) + '</option>';
    			});
    		else if(sketch_type =='Comm'){
    			html +='<option value="0" value1="None">None</option>'
    			Proval_Default_Exterior_Cover.filter(function(pro){ return pro.tbl_type_code == 'MSExtWall' }).forEach(function(extCover){
    				html += '<option value="' + extCover.tbl_element + '" value1="' + getDescription(extCover, true) + '">' + getDescription(extCover) + '</option>';
    			});
			}
    	
    		html += '</select></td><td style="padding-left: 10px; font-style: italic;" colspan="2">Modifier:<select style="width: 100px; float: inherit;" class="' + lbl.value + ' modifier"><option value=""></option>'; 
    	
    		Proval_Lookup.filter(function(pro){ return (pro.tbl_type_code == 'Modifier' && getDescription(pro) && pro[grade_col] && pro[grade_col].indexOf(lbl.value2) > -1); }).forEach(function(mod){
    			if (getDescription(mod) != '+' && getDescription(mod) != '-')
    				html += '<option value="' + getDescription(mod) + '" value1="' + getDescription(mod, true) + '">' + getDescription(mod) + '</option>';
    		});
    	
    		html += '</select><select style="width: 40px;float: inherit;" class="' + lbl.value + ' modifierSign" disabled="disabled"><option value=""></option><option value="+">+</option><option value="-">-</option></select></td><td><select class="' + lbl.value + ' material" disabled="disabled"><option value="">-Material-</option>';
    	
    		Proval_Lookup.filter(function(pro){ return (pro.tbl_type_code == 'Material' && pro[grade_col] && pro[grade_col].indexOf(lbl.value2) > -1); }).forEach(function(mat){
    			html += '<option value="' + getDescription(mat) + '" value1="' + getDescription(mat, true) + '">' + getDescription(mat) + '</option>';
    		});
    	
    		html += '</select></td></tr>';
			html += (index != lbl_level.length -1)? '<tr><td colspan="5" style="border-bottom: 1.5px solid #b9b9b9;"></td></tr><tr></tr><tr></tr>': '';
    	});
    	$('.divvectors').append(html + '</table>');
    	$('.divvectors').append('<span class="ValidateLabel" style="display: none; width:450px; color:Red;">Please choose any label from Prefix, Components or Exterior Feature.</span></span>');
    	//$('.divvectors').append('<span class="ValidateArea" style="display: none; width:250px; color:Red;">Please fill Area Field.</span>');
    	$('.Current_vector_details').show();   
 		$('.Current_vector_details').width(690);
 		$('.divvectors').width(655);
 		//$('.divvectors').height(300);
 		if(DrawSketchTrue)
 			$('.Current_vector_details').css('top','15%');
 		//$('.Current_vector_details').css('left','5%');
    	$('.lbl_level td').css('padding','3px 0px');
    	$('.divvectors').css('padding-bottom','0px');
    	$('.dynamic_prop').css('padding-bottom','0px');
    	$('.skNote').show();
    	$('.skNote').html('<span><b>Sketch Label Preview:</b>&nbsp;</span><span class="label_preview">' + (lblCurrentField || '') + '</span>'); 
    	if(type == 'edit' && sketchApp.currentVector && sketchApp.currentVector.BasementFunctionality){
    		$('.dynamic_prop').after('<span class="CurrentLabel" style="display: none; max-width:550px; color:#a01e1e;margin-left: 4px;font-size: 14px; margin-left: 25px;">'+CurrentLabelValue+'</span>');
    		var extlabel = "Existing Label : ";
    		clval = items[0].Value;
			CurrentLabelValue = extlabel + items[0].Value;
			$('.CurrentLabel').html(CurrentLabelValue);
			$('.CurrentLabel').show();
		} 
    	var isWalkout = false;
    
    	$('.segmentLbl').unbind();
    	$('.segmentLbl,.modifier,.modifierSign,.ns,.ext_cover').change(function(index){
    		$('.ValidateLabel').hide();
    		var lbls = [], codes = [], lbl = '', code = '', that = this, lbl_value = null, extraValues = '';
			var tr = $(that).parents('.lbl_level'), thisValue = $(that).val();
			if($(that).attr('class').search('modifier')>-1){
			if(thisValue == 'Fin' || thisValue == 'UF' ||thisValue ==""){
				var next_select = $(this).next('select');
    			if ($(this).val() == 'Fin') $(next_select).removeAttr('disabled');
    			else {
    				$(next_select).val('');
    				$(next_select).attr('disabled', 'disabled');
    			}
			}
			}
			var extra_tr = $(tr).next('tr'), extraVal = false;
			var lookupVal = Proval_Lookup.filter(function(lkup){ return lkup.tbl_element == thisValue; })[0];
    		lookupVal = lookupVal? lookupVal: {};
    		if (!isWalkout) {
	    		if (lookupVal && lookupVal[material_filter_col] && lookupVal[material_filter_col] == 'Y') 
					$('.material', $(extra_tr).next('tr')).removeAttr('disabled');
				else $('.material', $(extra_tr).next('tr')).attr('disabled', 'disabled');
	    		if ($(that).hasClass('prefix')) {
	    			if (thisValue == '10' ||thisValue == '15' ) {
	    				$(extra_tr).show();
	    				$('.ns', extra_tr).val('N-')
	    				$('.ns', extra_tr).removeAttr('readonly');
					}
	    			else {
	    				if ($('.walkout', extra_tr).attr('disabled')) $(extra_tr).hide();
	    				$('.ns', extra_tr).attr('readonly', 'readonly');
					}
	    		}
	    		if ($(that).hasClass('comp')) {
	    			if (thisValue == '202' && sketch_type == 'Res') { 
	    				$(extra_tr).show(); 
	    				$('.walkout', extra_tr).removeAttr('disabled'); 
					}
	    			else {
	    				$('.walkout', extra_tr).attr('disabled', '');
	    				$('.walkout', extra_tr).val('');    		
	    				if ($('.walkout', extra_tr).attr('disabled')) $(extra_tr).hide();
					}
					var html = '<option value="0"></option>';
					if (thisValue == '0') {
						$('.prefix', tr).html(html);
						Proval_Lookup.filter(function(lk){ return (lk[grade_col] && lk.tbl_type_code =='Prefix'  && lk[grade_col].indexOf($(tr).attr('name')) > -1); }).forEach(function(lk) {
	    					html += '<option value="' + lk.tbl_element + '" value1="' + getDescription(lk, true) + '">' + getDescription(lk) + '</option>';
	    				});
	    				$('.prefix', tr).html(html);
	    				var html = '<option value="0"></option>';
						$('.constr', tr).html(html);
						Proval_Lookup.filter(function(lk){ return (lk[grade_col] && lk.tbl_type_code =='Construction'  && lk[grade_col].indexOf($(tr).attr('name')) > -1); }).forEach(function(lk) {
	    					html += '<option value="' + lk.tbl_element + '" value1="' + getDescription(lk, true) + '">' + getDescription(lk) + '</option>';
	    				});
	    				$('.constr', tr).html(html);
					}
					else {
						var array = [0,1];
						array.forEach(function(idx){
							var hdr = headers[idx];
							lookupVal = Proval_Lookup.filter(function(lkup){ return lkup.tbl_element == thisValue && lkup[hdr.grade_filter] && lkup[hdr.grade_filter].indexOf($(tr).attr('name')) > -1; })[0];
							lookupVal = lookupVal? lookupVal: {};
							var valid_fields = lookupVal[hdr.filter]? lookupVal[hdr.filter].split(','): [];
							hdr.lookup.filter(function(lkp){ return (valid_fields.indexOf(lkp.tbl_element_desc) > -1); }).forEach(function(lkup){
								html += '<option value="' + lkup.tbl_element +'" value1="' + getDescription(lkup, true) + '">' + getDescription(lkup) + '</option>';
							});
							$('.' + hdr.value, tr).html(html);
							html = '<option value="0"></option>';
						});
					}
	    		}
    		}
    		$('.lbl_level').each(function(index, lvl){
    			var mValue=$('.add_vals').eq(index).find('.modifier').val();
    			var modSignValue=$('.add_vals').eq(index).find('.modifierSign').val();
    			var ext_covers='';
    			ext_covers = $('.add_vals').eq(index).find('.ext_cover').val();
    			lbl = ''; code = ''; lbl_value = null;
    			extraValues=$('.extraval').eq(index).find('.ns').val();
    			$('select', lvl).each(function(index, select){
    				lbl_value = $('option[value="'+ $(select).val() +'"]', select).attr('value1');
    				if((lbl_value == 'N s' || lbl_value == 'N c') && index == 0 ){
    					if(extraValues && extraValues != '' && lbl_value == 'N s')
    						lbl_value = extraValues + ' s';
    						else if(extraValues && extraValues != '' && lbl_value == 'N c')
    						lbl_value = extraValues + ' c';
    				}
    				lbl += lbl_value? lbl_value: '';
    				if ($(select).hasClass('comp') && $(select).val() == '202') {
    					if ($('.walkout[grade="' + $(select).attr('grade') + '"]').val() == 'Y')
    						lbl += '-wo';
					}
    				code += $(select).val() + ',';
    			});
    			if(ext_covers && ext_covers != '' && ext_covers != '0' )
    		    	lbl=lbl+ext_covers;
    			if ((lbl && lbl != '') ||( mValue && mValue != '')){
    				if(mValue && mValue != '' || modSignValue && modSignValue !='')
    				 	lbl=lbl+mValue+modSignValue; 
    				 lbls.push(lbl);
    			}
    			if (code && code != '') codes.push(code.substring(0, code.length - 1));
    		});
    		$('.label_preview').html(lbls.reverse().join('|'));
    	});

    	$('.walkout').unbind();
    	$('.walkout').bind('change', function() {    	
    		isWalkout = true;
			$(this).parents('.extraval').prev().find('.comp').trigger('change');
			isWalkout = false;
    	})
    	
    	$("input[type='checkbox']").change(function(){
    		if($('.checkbox').prop("checked") == "checked"){
    			$('.checkbox').prop('checked', false);
				$('.Unarea').show();
				$('.Unperimeter').show();
			}
			else
			{
				$('.checkbox').prop('checked', 'checked');	
				$('.ValidateArea').hide();
				$('.Unarea').hide();
				$('.Unperimeter').hide();
				$('.perimeter').val("");
				$('.sqft').val("");
			}
    	});
    	$('.sqft').focusout(function(t){
			$('.ValidateArea').hide();
		})
    	$('.Current_vector_details #Btn_Save_vector_properties').unbind(touchClickEvent);
    	$('.Current_vector_details #Btn_Save_vector_properties').bind(touchClickEvent, function () {
    		var valid = true, nsVal;
    		var detailObj = {ext_feat_code: 0, comp: 0, prefix: 0, constr: 0, ns: 0, walkout: '', ext_cover: '', modifier: '', modifierSign:''};
    		items[0].Value = $('.label_preview').html();
    		items[0].Newlabel = $('.label_preview').html();
    		if ((!items[0].labelDetails) || (type == 'edit' && sketchApp.currentVector.BasementFunctionality)) 
    			items[0].labelDetails = { 
    				details: {
    					1: _.clone(detailObj),
						2: _.clone(detailObj),
						3: _.clone(detailObj)
			 		},
    				labelValue: $('.label_preview').html()
				};
    		$('.lbl_level').each(function(lvl_idx, lvl){
    			$('select', lvl).each(function(lkp_idx, select){
    				items[0].labelDetails.details[lvl_idx + 1][$(select).attr('type')] = $(select).val();
    			});
    			items[0].labelDetails.details[lvl_idx + 1].ext_cover = $('.ext_cover', $(lvl).next().next()).val();
    			items[0].labelDetails.details[lvl_idx + 1].modifier = $('.modifier', $(lvl).next().next()).val();
    			items[0].labelDetails.details[lvl_idx + 1].modifierSign = $('.modifierSign', $(lvl).next().next()).val();
    			if ($('.prefix', lvl).val() == '10' || $('.prefix', lvl).val() == '15') {
	    			nsVal = $('.ns', $(lvl).next()).val();
	    			valid = valid && !isNaN(nsVal);
    				items[0].labelDetails.details[lvl_idx + 1].ns =(isNaN(nsVal)|| nsVal == "")? 0: parseInt(nsVal);
				}
    			if ($('.comp', lvl).val() == '202')
    				items[0].labelDetails.details[lvl_idx + 1].walkout = $('.walkout', $(lvl).next()).val();
    		});
    		var validateLabel = false, i = 1, ValidateArea = false, CheckUnsketch = false;
    		for(i=1; i<=3;i++){
    			var Details = items[0].labelDetails.details[i];
    			if(Details.prefix != '0'  || Details.comp != '0' || Details.ext_feat_code != '0'){
    				validateLabel = true;
    				break;
    			}
    		}
    		if(!validateLabel){
    			$('.ValidateArea').hide();
				$('.ValidateLabel').show();
				return false;
    		}
    		if($( '.UnSketchedarea .checkbox' )[0] && !$( '.UnSketchedarea .checkbox' )[0].checked){
				var areaa = $('.sqft').val();
				CheckUnsketch = true;
				if(areaa != '')
					ValidateArea = true;
			}
			if(validateLabel && CheckUnsketch && !ValidateArea){
				$('.ValidateArea').show();
				return false;
			}
			var areaSketch = false, sqftvalue = 0, perivalue = 0;
			var unsketchedAreaStatus = ($( '.UnSketchedarea .checkbox' ).length > 0 && $( '.UnSketchedarea .checkbox' )[0].checked)
			if( $( '.UnSketchedarea .checkbox' ).length > 0 && !$( '.UnSketchedarea .checkbox' )[0].checked){
				areaSketch = true;
				sqftvalue = $('.sqft').val() ? $('.sqft').val(): 0;
				perivalue = $('.perimeter').val() ? $('.perimeter').val() : 0;
			}
    		if (valid) {
    			var basefunctionTrue = false;
    			function skktsave(){
    				if(type == 'edit' && sketchApp.currentVector && sketchApp.currentVector.isUnSketchedTrueArea && unsketchedAreaStatus){
    					sketchApp.currentVector.vectorString = null;
                		sketchApp.currentVector.startNode = null;
                		sketchApp.currentVector.endNode = null;
                		sketchApp.currentVector.commands = [];
                		sketchApp.currentVector.endNode = null;
                		sketchApp.currentVector.placeHolder = false;
                		sketchApp.currentVector.Line_Type = false;
                		sketchApp.render()
                		sketchApp.startNewVector( true );
                		sketchApp.currentVector.isUnSketchedTrueArea = false;
                		sketchApp.currentVector.fixedAreaTrue = null;
                		sketchApp.currentVector.fixedPerimeterTrue = null;
                		sketchApp.currentVector.isClosed = false;
    				}
    				else if(type == 'edit' && sketchApp.currentVector && sketchApp.currentVector.isUnSketchedTrueArea && !unsketchedAreaStatus){
    					sketchApp.currentVector.fixedAreaTrue = $('.sqft').val() != '' ? $('.sqft').val() : sketchApp.currentVector.areaFieldValue;
    					sketchApp.currentVector.fixedPerimeterTrue = $('.perimeter').val() != '' ? $('.perimeter').val() : sketchApp.currentVector.perimeterFieldValue; 
    				}
    				if (callback) callback(items, {'unSketchedTrue': areaSketch? areaSketch : false, 'Sqft': (sqftvalue ? sqftvalue : 0), 'perimeter':(perivalue ? perivalue : 0), 'BasementFunctionality':basefunctionTrue});
	    			if (!editor.isScreenLocked) $('.mask').hide();
	    			$('.CurrentLabel').remove();
					hideKeyboard();
					$('.Current_vector_details').css('top','25%');
	       			$('.Current_vector_details').hide();
    			}
    			if(((items[0].labelDetails.details[1].prefix == '1' || items[0].labelDetails.details[1].prefix == 1) || (items[0].labelDetails.details[1].prefix == '2' || items[0].labelDetails.details[1].prefix == 2) || (items[0].labelDetails.details[1].prefix == '3' || items[0].labelDetails.details[1].prefix == 3)) && (items[0].labelDetails.details[1].ext_feat_code == 0 ||items[0].labelDetails.details[1].ext_feat_code == '0')){
    				basefunctionTrue = true;
    				var txtcomp = false, compval = true;
    				if(items[0].labelDetails.details[1].comp != '0' && items[0].labelDetails.details[1].comp != '0'){
    					var fiid = items[0].labelDetails.details[1].comp;
    					var plookup = Proval_Lookup.filter(function(x){return x.tbl_element == fiid})[0];
    					if(plookup.AdditionalScreen != "BGAreaEntry")
    						compval = false;
    				}
    				if(!compval){
    					basefunctionTrue = false;
						skktsave();
						return;
    				}
    				else{
    					if(type == 'edit'){
    						if(clval != ($('.label_preview').html()))
    							txtcomp = true;
    					}	
    					BasementFunctionality(txtcomp,function(){
    						skktsave();
    					});
    				}
    			}
    			else{
    				if(type == 'edit'){
    					if(sketchApp.currentVector.BasementFunctionality)
    						sketchApp.currentVector.BasementFunctionality = false;
    				}
    				skktsave();
    			}
	    		
        	}
        	else messageBox('Please check the values.')
    	});
    
    	$('.Current_vector_details #Btn_cancel_vector_properties').unbind(touchClickEvent)
    	$('.Current_vector_details #Btn_cancel_vector_properties').bind(touchClickEvent, function() {
			hideKeyboard();
			$('.CurrentLabel').remove();
			$('.Current_vector_details').css('top','25%');
        	$('.Current_vector_details').hide();
        	if (!sketchApp.isScreenLocked) $('.mask').hide();
       		return false;
    	});   
    
    	if (type == 'edit') {
	    	[1,2,3].forEach(function(i) {
	    		if (!items[0].labelDetails.details) return;
	    		var element = items[0].labelDetails.details[i];
	    		var extraVal = false;
	    		if(element && sketchApp.currentVector && sketchApp.currentVector.BasementFunctionality && i == 1){
	    			var com = items[0].labelDetails.details[i].comp1;
	    			var pre = items[0].labelDetails.details[i].prefix1;
	    			var modi = items[0].labelDetails.details[i].modifier1;
	    			var msig = items[0].labelDetails.details[i].modifierSign1;
	    			var walkout = items[0].labelDetails.details[i].walkout1;
	    			var className = '.' + lbl_level.filter(function(l){ return l.value1 == i; })[0].value2;
	    			$(className + '_Grade.walkout' + '.ns').attr('readonly', 'readonly');
					
					$('.' + 'comp', $('.lbl_level').eq(i-1)).val(com);
					if (com && com.toString() == '202') {
						$('.segmentLbl').eq(((i - 1) * 4) + 2).trigger('change');
						$(className + '_Grade.walkout').removeAttr('readonly');
						$(className + '_Grade.walkout').val(walkout);
						extraVal = true;
					}
					else $(className + '.walkout').attr('readonly', 'readonly');
					$('.' + 'prefix', $('.lbl_level').eq(i-1)).val(pre);
					$(className + '_Grade.' + 'modifier').val(modi);
					$(className + '_Grade.' + 'modifierSign').val(msig);
	    		}
	    		else if (element){
					$.each(element, function(type, value) {
						$('.' + type, $('.lbl_level').eq(i-1)).val(value);
						var className = '.' + lbl_level.filter(function(l){ return l.value1 == i; })[0].value2;
						if (type == 'prefix') {
							if (value && (value.toString() == '10') || value.toString() == '15') { 
								$(className + '_Grade' + '.ns').removeAttr('readonly');
								$(className + '.' + type).val(value);
								extraVal = true;
							}
							else{
								$(className + '_Grade.walkout' + '.ns').attr('readonly', 'readonly');
								$(className + '.' + type).val(value);
							}
						}
						if(type=='constr'){
							if(value>0)
								$(className + '.' + type).val(value);
						}
						if (type == 'comp') {
							if(value>0){
	    						$('.segmentLbl').eq(((i - 1) * 4) + 2).trigger('change');
	    						if (value && value.toString() == '202') {
									$(className + '_Grade.walkout').removeAttr('readonly');
									$(className + '_Grade.walkout').val(element['walkout']);
									extraVal = true;
								}
								else $(className + '.walkout').attr('readonly', 'readonly');
							}	    				
						}
						if (type == 'modifier' || type == 'ext_cover'|| type == 'modifierSign' || type == 'ns') $(className + '_Grade.' + type).val(value);
					});
				}
				if (!extraVal) $('.lbl_level').eq(i-1).next('tr').hide();
				else $('.lbl_level').eq(i-1).next('tr').show();
	    	});
	    	$('.modifier').trigger('change');
	    	$('.skNote').html('<span><b>Sketch Label Preview:</b>&nbsp;</span><span class="label_preview">' + (lblCurrentField || '') + '</span>');
    	}
	}
	
 }	
	
	
		$('.mask').hide();
		NoNBasementFunctionality();
}

function NumericScaleHandle(dFields,NewValue,callback){
	var NAME = dFields.Name ;
	var NameCheck = false ;
	if(NAME){
		if(NAME.indexOf('area') > -1 || NAME.indexOf('perimeter') > -1)
			NameCheck = true;
	}
	if(NameCheck && (dFields.InputType == '8' || dFields.InputType == '2') ){
		var Numscale =   dFields.NumericScale;
		if(Numscale == 0 || Numscale == '0')
			NewValue = parseInt(NewValue);
		if(callback) callback(NewValue);
    }
	else
		if(callback) callback(NewValue);
}

function ProvalSketchSave(sketchDataArray, noteData, sketchSaveCallBack, onError) {
	var totalLength = 0,savedataLength = 0,TotalValueLength = 0,ValueLength = 0, deleteMHRecords = [];
	if (sketchDataArray.length == 0 && noteData.length == 0) { 
		if (sketchSaveCallBack) sketchSaveCallBack(afterSketchSave); return; 
	}
	if(sketchApp.currentSketch && sketchApp.currentSketch.isAfterSaveNewSketch){
		sketchDataArray[0].sourceName=sketchApp.currentSketch.config.VectorSource[0].Table;
		sketchApp.currentSketch.isAfterSaveNewSketch=false;
	}
	else{
		if(sketchApp.currentSketch)
			sketchApp.currentSketch.isAfterSaveNewSketch=false;
	}
	var floor_records = [];
	var saveData = [], deleteData = [], saveNoteData = [], newRecords = [], deleteNoteData = [], parentData = []; 
	var noteTxtField = getDataField('note_text', 'SktNote'), notePosField = getDataField('note_position', 'SktNote');
	var last_update_field = getDataField('last_update', 'SktHeader'), extensions = [], seg_id = 0;
	
	var processNoteData = function() {
		var delCount = 0, noteCatId = getCategoryFromSourceTable('SktNote').Id;
		var deleteNote = function(callback) {
			if (deleteNoteData.length == 0) callback();
			else
				deleteNoteData.forEach(function(delNote){
					ccma.Data.Controller.DeleteAuxRecordWithDescendants(noteCatId, delNote, null, null, function(){ delCount++; if (delCount == deleteNoteData.length) callback(); });
				});
		}
		deleteNote(function() {
			if (saveNoteData.length == 0) {
				var hdr_data;
				if (activeParcel['SktHeader'])
					extensions.forEach(function(ext) {
						if(ext && (ext!=" ")){
							hdr_data = activeParcel['SktHeader'].filter(function(hdr){ return hdr.extension == ext })[0];
							if (hdr_data)
								update_data(last_update_field, ext, hdr_data);
						}
					})
					if((totalLength == savedataLength) && (TotalValueLength == ValueLength))	
						sketchSaveCallBack(afterSketchSave); return; 
			}
			var noteData = saveNoteData.pop();
			var SaveNotePosition = 0;
			if(isNaN(noteData.position))
				SaveNotePosition = 0;
			else
				SaveNotePosition = noteData.position;
			var note_record = activeParcel.SktNote.filter(function(ntr){ return ntr.ROWUID == noteData.rowid })[0];
			update_data(noteTxtField, noteData.text, note_record, function () {
				update_data(notePosField, SaveNotePosition, note_record, processNoteData);
			});
		});
	}
	
	var saveParentData = function() {	
		var res_fields = ['attic_area', 'attic_fin_area', 'bsmnt_area', 'bsmnt_fin_area'], fields = [], values = [], fnames = [];
		var comm_fields = ['total_area', 'number_floors', 'imp_type', 'misc_imp_type'];
		var val = {}, fin_area, unfin_area, floor_recordd = [], flr_val = {};
		
		
		parentData.forEach(function(pdata) {
			var flr_rec_num = 0;
			var record = activeParcel[pdata.tbl].filter(function(pd){ return pd.ROWUID == pdata.rowid })[0]
			if (pdata.tbl.indexOf('ext_features') > -1) {
				//values.push({ tbl: pdata.tbl, rowid: pdata.rowid, val: val, record: record, val: { area: pdata.area }, fields: [getDataField('area', pdata.tbl)] });
				return;
			}
			fnames = (pdata.tbl.indexOf('comm') > -1)? comm_fields: res_fields;
			fnames.forEach(function(fname){ fields.push(getDataField(fname, pdata.tbl)); val[fname] = 0;});
			if(pdata.seg_tbl && record[pdata.seg_tbl] ){
				record[pdata.seg_tbl].forEach(function(seg_data){
					seg_data[pdata.lbl_tbl].forEach(function(lb_data){
						if (pdata.tbl.indexOf('dwelling') > -1) {
							fin_area = lb_data.unfin_area? parseFloat(lb_data.unfin_area): 0;
							unfin_area = lb_data.finish_area? parseFloat(lb_data.finish_area): 0;
							val.attic_area += (unfin_area + fin_area);
							val.attic_fin_area += fin_area;
							if (lb_data.lbl_level_code.toString() == '1') {
								val.bsmnt_area += (unfin_area + fin_area);
								val.bsmnt_fin_area += fin_area;
							}
						} else {
							if(seg_data.area==null || seg_data.area=="null")
								seg_data.area=0;
							val.total_area +=  parseFloat(seg_data.area);
							if(seg_data.eff_perimeter==null || seg_data.eff_perimeter=="null")
								seg_data.eff_perimeter=0;
							val.eff_perimeter += parseFloat(seg_data.eff_perimeter);
						}
					});
				});
			}
			val.imp_type = ''; val.misc_imp_type = '', val.number_floors = '';
			if (pdata.tbl.indexOf('comm') > -1) {
				val.imp_type = record ? record.imp_type : '';
				record[pdata.flr_tbl].forEach(function(flr){
					flr_rec_num = flr_rec_num + 1;
					flr_val = {flr_recorded: flr, flr_rec_num: flr_rec_num }
					floor_recordd.push(_.clone(flr_val));
					if (!isNaN(flr.floor_number) && val.number_floors < parseInt(flr.floor_number)) 
						val.number_floors = parseInt(flr.floor_number);
				})
			}
			if (pdata.tbl != 'ccv_improvements')
				values.push({ tbl: pdata.tbl, rowid: pdata.rowid, val: val, record: record, fields: _.clone(fields) });
			if(pdata && pdata.otherfields){
				pdata.otherfields.forEach(function(other_field){
					var rowId = pdata.rowid; val = {}
					/*
					if (other_field.table.indexOf('ext_feature') > -1) {
						if(record && record[other_field.table]){
							rowId =record[other_field.table].filter(function(ext){ return ext.improvement_id == 'D' || ext.improvement_id == 'C' })[0];
							rowId = rowId? rowId.ROWUID: null;
							if (rowId) record = record[other_field.table][0];
						}
					}
					*/
					if (rowId && other_field.table && record) {
						val[other_field.fieldName] = other_field.value;
						values.push({ tbl: other_field.table, rowid: rowId, val: val, record: record, fields: [getDataField(other_field.fieldName,other_field.table)] });
					}
				})
			}
		});
		var length = 0, count = 0;
		TotalValueLength = values.length;
		var saveValueData = function() {	
			if (values.length == 0) processNoteData();
			else
				values.forEach(function(value) {
					if(value.fields[0]){
						ValueLength = ValueLength + 1;
						length += value.fields.length;
						value.fields.forEach(function(field) {
							NumericScaleHandle(field,value.val[field.Name],function(NewValuess){
								value.val[field.Name] = NewValuess;
							});
							update_data(field, value.val[field.Name], value.record, function () {
								if (++count == length) processNoteData();
							});
						});
					}
					else {
						ValueLength = ValueLength + 1;
						if(TotalValueLength == ValueLength)
							processNoteData();
					}
				});
		}
		if(floor_recordd.length > 0){
			var flr_id_field = getDataField('floor_id','comm_floors');
			var saveFlrData = function() {
				var f_record = floor_recordd.pop();
				update_data(flr_id_field, f_record.flr_rec_num, f_record.flr_recorded, function () {
					if(floor_recordd.length > 0)
						saveFlrData();
					else saveValueData();
						
				});
			}
			saveFlrData();
		}
		else
			saveValueData();
	}
	
	var process_floorRecs = function(uniq_flr_records) {
		if (uniq_flr_records.length == 0) { saveParentData(); return; }
		var res_flr_names = ['base_area', 'finish_living_area', 'floor_key', 'walkout_bsmt', 'ext_cover1'];
		var comm_flr_name = ['base_area', 'floor_id', 'eff_perimeter', 'floor_number', 'par']
		var flr_key, old_rec, record_count = 0, flrFields = [];
		
		var update = function(rec, field_idx, field_count){
			var extt = false;
			NumericScaleHandle(flrFields[field_idx],rec[flrFields[field_idx].Name],function(NewValuess){
				rec[flrFields[field_idx].Name] = NewValuess;
			}); 
			if(flrFields[field_idx].Name == 'floor_number'){ 
				if(rec[flrFields[field_idx].Name] == undefined || rec[flrFields[field_idx].Name] == null)
					rec[flrFields[field_idx].Name] = '';
			}
			if(rec.table == 'res_floor')
    			rec.perimeter = rec.eff_perimeter ? rec.eff_perimeter : 0;
			if(rec.table == 'res_floor' && flrFields[field_idx].Name == 'ext_cover1'){
				if(rec[flrFields[field_idx].Name] == undefined || rec[flrFields[field_idx].Name] == null)
					extt = true;	
			}
			if(rec.table == 'res_floor' && flrFields[field_idx].Name == 'walkout_bsmt'){
				if(rec[flrFields[field_idx].Name] == undefined || rec[flrFields[field_idx].Name] == null || rec[flrFields[field_idx].Name] == '' || rec[flrFields[field_idx].Name] == " ")
					extt = true;	
			}
			if(rec.table == 'res_floor' && flrFields[field_idx].Name == 'finish_living_area'){
				if(rec[flrFields[field_idx].Name] == undefined || rec[flrFields[field_idx].Name] == null || rec[flrFields[field_idx].Name] == '' || rec[flrFields[field_idx].Name] == '0' || rec[flrFields[field_idx].Name] == 0)
					extt = true;	
			}
			if(extt){
				if ((field_idx + 1) < field_count)
					update(rec, ++field_idx, field_count); 
				else process_records();
			}
			else{
				update_data(flrFields[field_idx], rec[flrFields[field_idx].Name], rec.record, function(){ 
					if ((field_idx + 1) < field_count)
						update(rec, ++field_idx, field_count); 
					else process_records();
				});
			}
		}
		
		var process_records = function() {
			if (uniq_flr_records.length == 0) { saveParentData(); return; }
			var flr_rec = uniq_flr_records.pop();
			flrFields = [];
			var flr_names = ((flr_rec.table.indexOf('res') > -1)? res_flr_names: comm_flr_name)
			var fin_qual=getDataField('floor_fin_qual','res_floor')
			flr_names.forEach(function(fname){ flrFields.push(getDataField(fname, flr_rec.table)); });
			if (flr_rec.new_rec)
				insertNewAuxRecord(null, flr_rec.segmentData, getCategoryFromSourceTable(flr_rec.table).Id, null, null, function (rowid) {
					flr_rec.record = activeParcel[flr_rec.table].filter(function(r){ return r.ROWUID == rowid })[0];
					update(flr_rec, 0, flr_names.length);
					if(flr_rec.floor_fin_qual>0)
						update_data(fin_qual, flr_rec.floor_fin_qual, flr_rec.record)
				});
			else {
				update(flr_rec, 0, flr_names.length);
				if(flr_rec.floor_fin_qual>0)
						update_data(fin_qual, flr_rec.floor_fin_qual, flr_rec.record)
			}
		}
		process_records();
	}
	
	var processor = function() {
		if (saveData.length == 0) {
			var uniq_flr_records = [], uniq_rec = [], floor_key_col;
			floor_records.forEach(function(floor){
				floor_key_col = (floor.tblName.indexOf('res') > -1)? 'floor_key': 'floor_number';
				floor.record.forEach(function(dt, i){
					if (!isNaN(dt.floorKey)) {
						for(i = 1; i < parseFloat(dt.floorKey); i++) {
							var duplicate_dt = _.clone(dt);
							duplicate_dt.floorKey = i;
							floor.record.push(duplicate_dt);
						}
					}
				});
			});
			
			floor_records.forEach(function(floor){
				floor.record.forEach(function(dt, i){
					var rec = { extension: dt.extension, base_area: parseFloat(dt.area), eff_perimeter: parseFloat(dt.perimeter), par: parseFloat(dt.PAR), finish_living_area: parseFloat(dt.fin_area), walkout_bsmt: dt.walkout_bsmt, table: floor.tblName, segmentData: floor.segData.parentRecord, floor_number: dt.floor_no, perimeter:0, ext_cover1:dt.ext_cover1 }
					rec[floor_key_col] = dt.floorKey;
					var fl_key1 = dt.floorKey;
					var fl_key2 = dt.floorKey + '.0';
					uniq_rec = uniq_flr_records.filter(function(uniq){ return ((uniq[floor_key_col] == fl_key1 ||  uniq[floor_key_col] == fl_key2) && uniq.table == floor.tblName) })
					dt.otherValues.forEach(function(otherVal){
						if (otherVal.filter) {
							if (otherVal.filter.value == uniq_rec[0][floor_key_col]) 
								rec[otherVal.fieldName] = otherVal.value;
						}
						else rec[otherVal.fieldName] = otherVal.value;
					});
					if (uniq_rec.length > 0) {
						uniq_rec[0].base_area += parseFloat(dt.area);
						uniq_rec[0].eff_perimeter += parseFloat(dt.perimeter);
						uniq_rec[0].par += parseFloat(dt.PAR);
						uniq_rec[0].finish_living_area += parseFloat(dt.fin_area);
						/*
						if (!uniq_rec[0].floor_number) uniq_rec[0].floor_number = dt.floor_no;
						else if (!isNaN(dt.floor_no) && !isNaN(uniq_rec[0].floor_number)) {
							if (parseInt(dt.floor_no) > parseInt(uniq_rec[0].floor_number))
								uniq_rec[0].floor_number = dt.floor_no;
						}
						else if (!isNaN(dt.floor_no)) uniq_rec[0].floor_number = dt.floor_no;
						else uniq_rec[0].floor_number = null;
						*/
						if (dt.walkout_bsmt != '')
							uniq_rec[0].walkout_bsmt = dt.walkout_bsmt;
						if(dt.ext_cover1)
							uniq_rec[0].ext_cover1 = dt.ext_cover1;
							
					}
					else {						
						if(floor.segData && floor.segData.ParentROWUID)
							old_rec = activeParcel[floor.tblName].filter(function(flr_dt){ return flr_dt.ParentROWUID == floor.segData.ParentROWUID && (flr_dt[floor_key_col] == fl_key1 || flr_dt[floor_key_col] == fl_key2 ) })
						else
							old_rec = activeParcel[floor.tblName].filter(function(flr_dt){ return flr_dt.extension == dt.extension && (flr_dt[floor_key_col] == fl_key1 || flr_dt[floor_key_col] == fl_key2 ) })
						if (old_rec.length > 0){
							rec.new_rec = false;
							rec.record = old_rec[0];
						}
						else rec.new_rec = true;
						uniq_flr_records.push(rec);
					}
				});
			});
			process_floorRecs(uniq_flr_records); 
			return;			
		}
		
		var data = saveData.pop();
		if (!data.isSegmentIdOnly) {
			savedataLength = savedataLength + 1;
		    var vectFnames = ['curve_height', 'run_rise', 'seq_number', 'status', 'line_type', 'vector', 'dimension_font_size', 'dimension_position'];
			var lblFnames = [ 'lbl_level_code', 'lbl_frac1', 'lbl_const_code', 'lbl_ext_feat_code', 'status', 'finish_area', 'unfin_area', 'default_ext_cover' , 'lbl_n', 'lbl_frac2', 'lbl_frac3', 'lbl_frac4','area2','area3'];
			var seg_fnames = [ 'status', 'vector_start', 'area', 'perimeter', 'label_position', 'displayed_area_position'];
			var imp_fnames = ['improvement_id', 'imp_type', 'imp_width', 'imp_length', 'mh_length', 'mh_width'];

			var vectFields = [], lblFields = [], segFields = [], imp_dwell_field = [];
			var par_data = parentData.filter(function(pd){ return pd.tbl == data.parentTbl && pd.rowid == data.parentRowid })
			if (par_data.length == 0)
				parentData.push({ tbl: data.parentTbl, rowid: data.parentRowid, seg_tbl: data.segment_tbl, lbl_tbl: data.lbl_table, flr_tbl: data.floor_table, ext_feature_tbl: data.extFeatTbl, otherfields: [] })
			par_data = parentData.filter(function(pd){ return pd.tbl == data.parentTbl && pd.rowid == data.parentRowid })[0];
			par_data.otherfields = _.union(par_data.otherfields, data.otherValues);
			
			seg_fnames.forEach(function(fname){ segFields.push(getDataField(fname, data.segment_tbl)); });
			vectFnames.forEach(function(fname){ vectFields.push(getDataField(fname, data.vect_tbl)); });
			lblFnames.forEach(function(fname){ lblFields.push(getDataField(fname, data.lbl_table)); });
			imp_fnames.forEach(function(fname){ imp_dwell_field.push(getDataField(fname, 'ccv_improvements_dwellings')); });

			var segment_data = activeParcel[data.segment_tbl].filter(function(d){ return d.ROWUID == data.rowId })[0];
			
			var update = function(ins_data, dt, tbl_name, fields, rowid, count, ins_callback, extraValue) {
				if (count == fields.length) { 
					if (tbl_name.split(/_/g)[1] == 'sktsegment') ins_callback();
					else insert(ins_data, tbl_name, fields, ins_callback, extraValue); 
					return; 
				}
				var f = fields[count++], valid = true;
				var value = dt[f.Name];
				if(value == undefined && f.Name == 'lbl_n')
					value = f.DefaultValue;
				NumericScaleHandle(f,value,function(NewValuess){
					value = NewValuess;
				});
				if (!valid) { update(ins_data, dt, tbl_name, fields, rowid, count, ins_callback, extraValue); return; }
				var record = activeParcel[tbl_name].filter(function(ac){ return ac.ROWUID == rowid })[0];
				update_data(f, value, record, function(isUpdated){
					if (isUpdated && tbl_name.indexOf('label') > -1 && f.Name == 'lbl_ext_feat_code') {
						var feat_table = tbl_name.indexOf('label');
						var rowId =activeParcel[extraValue]?activeParcel[extraValue].filter(function(ext_ft){ return ext_ft.improvement_id == dt.improvement_id })[0]:null;
						rowId = rowId? rowId.ROWUID: null;
						if (rowId) {
							var pData = parentData.filter(function(prec){ return prec.tbl == extraValue && prec.rowid == rowId })
							if (pData.length > 0)
								pData[0].area += parseFloat(dt.area);
							else 
								parentData.push({ tbl: extraValue, rowid: rowId, area: parseFloat(dt.area) });
						}
					}
					update(ins_data, dt, tbl_name, fields, rowid, count, ins_callback, extraValue); 
				});
			}
			var insert = function(ins_data, tbl_name, fields, ins_callback, extraValue) {
				if (ins_data.length == 0) { ins_callback(); return; }
				var dt = ins_data.pop(), count = 0;
				var existing_rec = segment_data[tbl_name].filter(function(r){ return r.segment_id == dt.segment_id && r.lbl_level_code == dt.lbl_level_code });
				
				if (tbl_name.indexOf('ccv_sktsegmentlabelDwe') > -1 && existing_rec.length > 0)
					update(ins_data, dt, tbl_name, fields, existing_rec[0].ROWUID, 0, ins_callback, extraValue);
				else
					insertNewAuxRecord(null, segment_data, getCategoryFromSourceTable(tbl_name).Id, null, null, function (rowid) {
						update(ins_data, dt, tbl_name, fields, rowid, 0, ins_callback, extraValue);
					});
			}
			var updated = function(tbl_name, fields,dt, rowid, count,recordd, Mcallback) {
				if (count == fields.length) {
					if(Mcallback) Mcallback(); 
					return
				}
				var f = fields[count++], valid = true;
				var value = dt[f.Name];
				update_data(f, value, recordd, function(isUpdated){
					updated(tbl_name, fields, dt,rowid, count,recordd, Mcallback); 
				});
			}
			update(null, data, data.segment_tbl, segFields, data.rowId, 0, function() {
				insert(data.vects, data.vect_tbl, vectFields, function(){
					insert(data.labels, data.lbl_table, lblFields, function(){
						if (data.floor_record.length > 0)
							floor_records.push({ record: data.floor_record, tblName: data.floor_table, segData: segment_data });
							if(data.MHOMEValuesTrue && data.parentTbl == 'ccv_improvements_dwellings'){
								var mRecord = [];
								var record = activeParcel[data.parentTbl].filter(function(ac){ return ac.ROWUID == data.parentRowid })[0];
								updated(data.parentTbl, imp_dwell_field, data.MHOMEValues[0],data.parentRowid, 0, record,function() {
									mRecord = activeParcel['manuf_housing'] && activeParcel['manuf_housing'].filter(function(ac){ return ac.ParentROWUID == record.ParentROWUID });
									var PRecord = activeParcel['ccv_extensions'] && activeParcel['ccv_extensions'].filter(function(pc){ return pc.ROWUID == record.ParentROWUID })[0];
									if(mRecord && mRecord.length == 0){
										insertNewAuxRecord(null, PRecord, getCategoryFromSourceTable('manuf_housing').Id, null, null, function (rowid) {
											processor();					
										});
									}
									else
										processor();
								});	
							}
							else
								processor();
						//process_floorRecs(data.floor_record, data.floor_table, flrFields, segment_data, processor);
					}, data.extFeatTbl);
				});
			});
		}
		else {
			savedataLength = savedataLength + 1;
			var seg_data = activeParcel[data.seg_tbl].filter(function(sg_dt){ return sg_dt.ROWUID == data.rowId })[0];
			//var seg_segId_field = getDataField('segment_id', data.seg_tbl);
			//var vect_segId_field = getDataField('segment_id', data.vect_tbl);
			//var lbl_segId_field = getDataField('segment_id', data.lbl_tbl);
			if (seg_data) {
				if (data.floor_Records && data.floor_Records.length > 0)
					floor_records.push({ record: data.floor_Records, tblName: data.floor_Table, segData: seg_data });
				//update_data(seg_segId_field, data.segment_id, seg_data, function(){
					var rec_count = seg_data[data.vect_tbl].length, count = 0;
					var lbl_count = seg_data[data.lbl_tbl].length, lCount = 0;
					if (rec_count == 0) processor();
					else 
						seg_data[data.vect_tbl].forEach(function(vect_data){
							//update_data(vect_segId_field, data.segment_id, vect_data, function(){
								if (rec_count == ++count) {
									rec_count = seg_data[data.lbl_tbl].length; count = 0;
									if (rec_count == 0) processor();
									else
										seg_data[data.lbl_tbl].forEach(function(lbl_data){
											//update_data(lbl_segId_field, data.segment_id, lbl_data, function(){
												if (rec_count == ++count) processor();
											//});
										});
								}
							//});
						})
				//});
			}
			else processor();
		}
	}
	
	var createNew = function() {
		if (newRecords.length == 0) { 
			totalLength = saveData.length;
			processor(); return; }
		var new_rec = newRecords.pop();		
		var newAuxRowuid = new_rec.newAuxRowuid? new_rec.newAuxRowuid: null;
		insertNewAuxRecord(null, new_rec.parentData, new_rec.catId, newAuxRowuid, null, function (rowid) {
			if (!new_rec.doNotUpdate) {
				new_rec.saveObj.parentRowid = new_rec.parentData.ROWUID;
				new_rec.saveObj.rowId = rowid;
				new_rec.saveObj.status = 'A';
				new_rec.saveObj.extension = new_rec.extension;
				saveData.push(_.clone(new_rec.saveObj));
			}
			createNew();
		});
	}
	
	var deleteMHFun = function() {
		if(deleteMHRecords.length == 0){
			createNew(); 
			return;
		}
		var MHRec = deleteMHRecords.pop();
		var MHTrue = true, DwellVectors = [];
		var cattId = getCategoryFromSourceTable("manuf_housing").Id;
		var paRowuid = MHRec.parent.ParentROWUID;
		var ParRecord = activeParcel["ccv_extensions"].filter(function(pa){return pa.ROWUID == paRowuid})[0];
		if(ParRecord.manuf_housing.length == 0)
			deleteMHFun();
		else
		{
			var chilRecords = ParRecord.ccv_improvements_dwellings;
			chilRecords.forEach(function(chrecord){
				if(chrecord.ROWUID == MHRec.ROWUID)
					return;
				var sketche = [];
				sketche = sketchApp.sketches.filter(function(te){return te.sid ==chrecord.ROWUID })[0];
				sketche.forEach(function(ve){
					DwellVectors.push(ve);
				});	
			});
			for(var i=0;i<DwellVectors.length;i++){
				var dve = DwellVectors[i];
				var mhr = dve.name.split("MH");
				if(mhr.length>1){
					MHTrue = false;
					break
				}
			}
			if(MHTrue){
				if(!(_.isEmpty(cachedAuxOptions)))
					ccma.Data.Controller.DeleteAuxRecordWithDescendants(cattId, ParRecord.manuf_housing[0].ROWUID, null, null, function () {
						deleteMHFun();
					}, true, null, true);
				else
					ccma.Data.Controller.DeleteAuxRecordWithDescendants(cattId, ParRecord.manuf_housing[0].ROWUID, null, null, function () {
						deleteMHFun();
					}, null, null, true);
			}
			else
				deleteMHFun();
		}
	}
	
	var deleteRecs = function() {
		if (deleteData.length == 0) { 
			if(deleteMHRecords.length > 0)
				deleteMHFun();
			else	
				createNew(); 
			return; 
		}			
		var rec = deleteData.pop();
		if(rec.deleteMHRecord && rec.source == "ccv_improvements_dwellings")
			deleteMHRecords.push(_.clone(rec));
		var del_Recs = (rec.isCurrentRecord? [rec.parent]: rec.parent[rec.source]);
		var recCount = del_Recs.length, count = 0;
		if (rec.isCurrentRecord && rec.source.indexOf('improvement') == -1) {
			del_Recs[0][rec.vector_tbl].forEach(function(vect) {
				deleteData.push({ source: rec.vector_tbl, parent: del_Recs[0] });
			});
			del_Recs[0][rec.lbl_tbl].forEach(function(vect) {
				deleteData.push({ source: rec.lbl_tbl, parent: del_Recs[0] });
			});
		}
		var catId = getCategoryFromSourceTable(rec.source).Id;
		function callback() { if (++count == recCount) deleteRecs(); }	
		if (rec.source.indexOf('sktvector') > -1 || rec.source.indexOf('improvement') > -1) {
			if (recCount > 0){
				if(deleteData.length == 0 && rec.source.indexOf('improvement') > -1 && !(_.isEmpty(cachedAuxOptions)))
					del_Recs.forEach(function(rec) {
						ccma.Data.Controller.DeleteAuxRecordWithDescendants(catId, rec.ROWUID, null, null, callback, true, null, true);
					});
				else
					del_Recs.forEach(function(rec) {
						ccma.Data.Controller.DeleteAuxRecordWithDescendants(catId, rec.ROWUID, null, null, callback, null, null, true);
					});
			}
			else deleteRecs(deleteData);
		}
		else {
			var status_field = getDataField('status', rec.source);
			if (recCount == 0) deleteRecs();
			else
				del_Recs.forEach(function(del_rec) {
					ccma.Data.Controller.DeleteAuxRecordWithDescendants(catId, del_rec.ROWUID, null, null, callback, null, null, true);
					//update_data(status_field, 'H', del_rec, callback);
				});
		}
	}
	
	var evaluate_otherColumns = function(sketch_codes, sketch_type, area, floor_no, nValue) {
		if (sketch_type == 'I') return [];
		var otherValues = [];
		area = area ? Math.abs(area) : area;
		var other_column = 'OtherColumns';
		other_column = (sketch_type == 'Comm'? 'Comm': (sketch_type == 'Imp'? 'Imp': '')) + other_column;
		var other_col_data = sketch_codes[other_column];
		
		try {
			other_col_data = JSON.parse(other_col_data);
			other_col_data.forEach(function(other_val){
				if (other_val.value == '{area}') other_val.value = area;
				if (other_val.value == '{field_2}') other_val.value = (sketch_codes.field_2 ? sketch_codes.field_2 : '');
				if (other_val.value == '{startingfloor}') other_val.value = floor_no;
				if (other_val.value == '{n}') other_val.value = nValue;				
			});
			return other_col_data;
		} catch(ex) { return []; }
	}
	
	var sketchRowuids = _.uniq(sketchDataArray.map(function(skda){ return skda.parentRow.ROWUID }));
	
	sketchRowuids.forEach(function(skrowid){
		seg_id = 0;
		var sketchOutBuilding = sketchApp.sketches.filter( function ( sBuild ) { return sBuild.uid == skrowid } )[0];
		var extRocord = sketchOutBuilding.parentRow.parentRecord;
		var scaleField = getDataField('scale','SktHeader');
		if(extRocord.SktHeader[0]){
			if(extRocord.RorC == 'R')
				update_data(scaleField, '80', extRocord.SktHeader[0]);
			else if(extRocord.RorC == 'C')
				update_data(scaleField, '200', extRocord.SktHeader[0]);
		}
		sketchOutBuilding.vectors.filter(function(vBuild){return ( vBuild.sketchType == "outbuilding")}).forEach(function(vectOutBuilding){
			if(vectOutBuilding.newRecord == true){
		    	var improvId = sketchOutBuilding.parentRow.improvement_id || sketchOutBuilding.parentRow.ROWUID
				var parentRowOutBuilding = sketchOutBuilding.parentRow.parentRecord;
				var vs = convertStrToPoint( vectOutBuilding.vectorString.split( ':' )[1].split( ' S ' )
                	[0].replace( ' ', '/' ) );
				if ( activeParcel["SktOutbuilding"].filter( function ( o ) { return o.ROWUID == vectOutBuilding.uid } ).length == 0 )
				insertNewAuxRecord( null, parentRowOutBuilding, getCategoryFromSourceTable( 'SktOutbuilding' ).Id, null, { vector_start: vs, label_position: vs, label_font_size: 7, label_code: vectOutBuilding.name, extension: sketchOutBuilding.GroupingValue, improvement_id: improvId }, function ( outBuildingRowid ) {
			    	var outBuildingRecord = activeParcel["SktOutbuilding"].filter( function ( oBuild ) { return oBuild.ROWUID == outBuildingRowid } )[0];	  
			
				});
			}
			else{
				var out_vect_start = getDataField("vector_start","SktOutbuilding");
				var out_Record = activeParcel.SktOutbuilding.filter(function(outbuild){ return outbuild.ROWUID == vectOutBuilding.uid})[0];
				var vst = convertStrToPoint( vectOutBuilding.vectorString.split( ':' )[1].split( ' S ' )[0].replace( ' ', '/' ) );
                if(out_Record){
                	out_Record["vector_start"] = vst;
					update_data(out_vect_start, vst, out_Record);
				}
			}
		});
		sketchDataArray = sketchDataArray.filter( function ( a ) { return !a.isUnSketchedArea && a.type != 'outbuilding' } );
		sketchDataArray.filter(function(skda){ return skda.parentRow.ROWUID == skrowid }).forEach(function(skd){
			var saveObj = { parentRowid: null, parentTbl: null, rowId: null, vector_start: null, oldStartVal: null, vects: [], labels: [], floor_record: [], segment_tbl: skd.sourceName, vect_tbl: null, lbl_table: null, segment_id: seg_id, status: 'A', floor_table: null, otherValues: [], MHOMEValues: [], MHOMEValuesTrue:false}
			var parentTbl = null, floor_col = 'FloorRecord', floor_key_col = 'FloorKey', grade_col = 'GradeLevel';
			var skt_type = 'I';
			
			if (skd.sourceName == "ccv_sktsegment_imps") {
			    saveObj.vect_tbl = 'ccv_sktvector_imps';
				saveObj.parentTbl = 'ccv_improvements';
				saveObj.lbl_table = 'ccv_sktsegmentlabel_imps';
				saveObj.extFeatTbl = 'ccv_improvements_features';
				skt_type = 'Imp';
				grade_col = skt_type + grade_col;
			}
			else if (skd.sourceName == "ccv_sktsegment_dwellings") {
			    saveObj.vect_tbl = 'ccv_sktvector_dwellings';
				saveObj.parentTbl = 'ccv_improvements_dwellings';
				saveObj.extFeatTbl = 'ccv_dwellings_ext_features';
				saveObj.lbl_table = 'ccv_sktsegmentlabel_dwellings';
				saveObj.floor_table = 'res_floor';
				skt_type = 'Res';
				grade_col = skt_type + grade_col;
			}
			else if (skd.sourceName == "ccv_sktsegment_comm_bldgs") {
			    saveObj.vect_tbl = 'ccv_sktvector_comm_bldgs';
				saveObj.parentTbl = 'ccv_improvements_comm_bldgs';
				saveObj.extFeatTbl = 'ccv_comm_bldgs_ext_features';
				saveObj.lbl_table = 'ccv_sktsegmentlabel_comm_bldgs';
				saveObj.floor_table = 'comm_floors';
				skt_type = 'Comm';
				floor_col = skt_type + floor_col;
				floor_key_col='FloorID';
				floor_key_col = skt_type + floor_key_col;
				grade_col = skt_type + grade_col;
			}
			skd.vectorString.split(';').forEach(function(vectorString, index) {
				var sid = (skd.newRecord && index > 0) ? (ccTicks() - 10 * index): skd.sid;
				saveObj.rowId = sid;
				saveObj.floor_record=[],saveObj.vects=[],saveObj.labels=[],saveObj.otherValues=[];
				if (vectorString == '') return;
				++seg_id;
				
				var points = vectorString.split(':')[1].split(' S ');
				saveObj.vector_start = convertStrToPoint(points[0].replace(' ', '/'));
				saveObj.segment_id = seg_id;
				var segment_data, Mtype = false;
				if (skd.newRecord)
					segment_data = activeParcel[saveObj.parentTbl].filter(function(sk){ return sk.ROWUID == skrowid; })[0];
				else segment_data = activeParcel[skd.sourceName].filter(function(sk){ return sk.ROWUID == sid && sk.status == 'A'; })[0];
				var lvl_code_values = {1: 'Below', 2: 'At', 3: 'Above'};
				function basecalculate(){
					var lbl = { lbl_level_code: 1, lbl_const_code: 0, lbl_ext_feat_code: '0', segment_id: seg_id, status: 'A', modifier: " ", area: parseInt(skd.areas[index]), perimeter: parseInt(skd.perimeters[index]), improvement_id: segment_data['improvement_id'], extension: segment_data['extension'],  default_ext_cover: '', lbl_frac1: 0, lbl_frac2: 0,lbl_frac3: 0, lbl_frac4: 0, finish_area: 0, unfin_area: 0, area2: 0,area3: 0}; 
				 	var flr = {floor_recs: 0, otherValues: []};
				 	var tArea = skd.fixedAreaTrue ? parseInt(skd.fixedAreaTrue) : parseInt(skd.areas[index]);
				 	var lab_details = skd.labelDetails[0][1];
				 	var Ba_details = skd.basementDetails[0];
				 	var bind = 0;
				 	var fin_area1=0,unfin1=0,area2=0,area3=0;
				 	function flrcalculate(comp , pre, modi, modisign,walkout ,fin_area,unfin,area2, area3,area4,type){
				 		var lbl_frac = 0,fin_areas=0;
				 		var prefix_lkup = Proval_Lookup.filter(function(lkp){ return lkp.tbl_element == pre && lkp.tbl_type_code == 'Prefix' && lkp[grade_col] && lkp[grade_col].indexOf(lvl_code_values[1]) > -1 })[0];
				 		prefix_lkup = prefix_lkup? prefix_lkup: {}
				 		var comp_lkup = Proval_Lookup.filter(function(lkp){ return lkp.tbl_element == comp && lkp.tbl_type_code == 'Component' && lkp[grade_col] && lkp[grade_col].indexOf(lvl_code_values[1]) > -1 })[0];
				 		comp_lkup = comp_lkup? comp_lkup: {}
				 		if (pre.toString() != 0) {
				 			flr.floor_recs +=  ((prefix_lkup[floor_col] && !isNaN(prefix_lkup[floor_col]))? parseInt(prefix_lkup[floor_col]): 0);
				 		}
				 		if (comp.toString() != 0) 
				 			flr.floor_recs +=  ((comp_lkup[floor_col] && !isNaN(comp_lkup[floor_col]))? parseInt(comp_lkup[floor_col]): 0);
				 		var comp1 = '', prefix = '', flr_area = 0;
				 		if(walkout=="Y")
				 			comp = 1202;
				 		if (comp.toString() == '0' && pre.toString() != '0') 
				 			lbl_frac = pre.toString();
				 		else {
					 		if (comp.toString() != '0')  comp1 = (parseInt(comp) << 16).toString(2);
					 		if (pre.toString() != '0') prefix = ((parseInt(pre)) >>> 0).toString(2);
					 		lbl_frac = ((comp1 == ''? 0: parseInt(comp1, 2)) + (prefix == ''? 0: parseInt(prefix, 2))).toString();
						}
						if(modi =="Fin"){
							var lbl_update=0;
							fin_areas = fin_area?fin_area : 0;
						 	flr.finish_living_areas = fin_area ? fin_area : 0;
						 	if(modisign=="+"){
						 		flr.floor_fin_qual=2;
						 		lbl_frac=parseInt(lbl_frac)+9887014715392;
						 		lbl_update=1;
						 	}
						 	else if(modisign=="-"){
						 		flr.floor_fin_qual=1;
						 		lbl_frac=parseInt(lbl_frac)+5592047419392;
						 		lbl_update=1;
						 	}
						 	if(lbl_update==0){
						 		lbl_frac=parseInt(lbl_frac)+1297080123392;
						 	}
						}
						else if(modi=="")
							fin_areas = '0';
						if(type == 1)
							lbl.lbl_frac1 = lbl_frac;
						else if(type == 2)
							lbl.lbl_frac2 = lbl_frac;
						else if(type == 3)
							lbl.lbl_frac3 = lbl_frac;
						else
							lbl.lbl_frac4 = lbl_frac;
						flr.ext_cover1 = '';
						[prefix_lkup, comp_lkup].forEach(function(lkup){
							var other_val = evaluate_otherColumns(lkup, skt_type, tArea);
							other_val.forEach(function(ov){
								if (ov.table == saveObj.floor_table) 
									flr.otherValues.push(ov);
								else saveObj.otherValues = _.union(_.clone(saveObj.otherValues), ov);
							});
						});
						 
						if ((fin_areas || prefix_lkup[floor_key_col] || comp_lkup[floor_key_col] || const_lkup[floor_key_col]) && skt_type != 'Imp') {
							flr.fin_area = fin_areas && !isNaN(fin_areas)? Math.round(parseFloat(fin_areas)): 0;
							if (pre == 1 || pre == 2 || pre == 3) 
								flr_area =skd.fixedAreaTrue ? (parseFloat(prefix_lkup[floor_key_col]) * parseFloat(skd.fixedAreaTrue)): (parseFloat(prefix_lkup[floor_key_col]) * parseFloat(skd.areas[index]));
							else flr_area =skd.fixedAreaTrue ? parseFloat(skd.fixedAreaTrue) : parseFloat(skd.areas[index]);
							flr.area = Math.round(flr_area);
							flr.perimeter =skd.fixedPerimeterTrue ? Math.round(parseFloat(skd.fixedPerimeterTrue)): Math.round(skd.perimeters[index]);
							flr.PAR = Math.round(parseFloat((flr.perimeter / flr.area) * 100));
							flr.extension = segment_data['extension'];
							flr.dwelling_number = segment_data.dwelling_number;
							flr.walkout_bsmt = walkout;
							if ((prefix_lkup && prefix_lkup[floor_key_col]) ) {
								flr.floorKey = (prefix_lkup[floor_key_col]);
						 		saveObj.floor_record.push(_.clone(flr));
					 		}
							if (comp_lkup && comp_lkup[floor_key_col]) {
								flr.floorKey = comp_lkup[floor_key_col]
						 		saveObj.floor_record.push(_.clone(flr));
					 		}
						}
				 	}
				 	if(lab_details.comp1 != 0){
				 		if(lab_details.comp1 == '202' && lab_details.modifier1 == 'Fin'){
				 			fin_area1 = Ba_details[0].area;
				 			unfin1 = Ba_details[1].area
				 			bind = bind + 2;
				 		}
				 		else {
				 			fin_area1 = Ba_details[0].area;
				 			unfin1 = 0;
				 			bind = bind + 1;
				 		}
				 		flrcalculate(lab_details.comp1, lab_details.prefix1, lab_details.modifier1, lab_details.modifierSign1, lab_details.walkout1 ,fin_area1,unfin1,null, null,null,1)
				 	}
				 	if(lab_details.comp2 != 0){
				 		area2 = Ba_details[bind].area;
				 		bind = bind + 1;
				 		flrcalculate(lab_details.comp2 , lab_details.prefix2, lab_details.modifier2, lab_details.modifierSign2, lab_details.walkout2, null,null,area2, null,null,2)
				 	}
				 	if(lab_details.comp3 != 0){
				 		area3 = Ba_details[bind].area;
				 		bind = bind + 1;
				 		flrcalculate(lab_details.comp3 , lab_details.prefix3, lab_details.modifier3, lab_details.modifierSign3, lab_details.walkout3,null,null,null, area3,null,3)
				 	}
				 	if(lab_details.comp4 != 0){
				 		var area4 = Ba_details[bind].area;
				 		bind = bind + 1;
				 		flrcalculate(lab_details.comp4 , lab_details.prefix4, lab_details.modifier4, lab_details.modifierSign4, lab_details.walkout4 ,null,null,null, null,area4,4)
				 	}
				 	lbl.finish_area = fin_area1;
				 	lbl.unfin_area = unfin1;
				 	lbl.area2 = area2;
				 	lbl.area3 = area3;
					if (skd.isModified) saveObj.labels.push(_.clone(lbl));				 	
				}
				
				$.each(skd.labelDetails[index], function(lvl_code, det) {
					var valid = false;
					$.each(det, function(idx, k) {
						if (k && k.toString() != '0') valid = valid || true;
					});
					if (valid) {
						if(skd.BasementFunctionality && (lvl_code == '1' || lvl_code == 1))
							basecalculate();
						else{
							if(det.ext_cover == '0' || det.ext_cover == 'null' || det.ext_cover == null)
								det.ext_cover = '';
				 			var lbl = { lbl_level_code: lvl_code, lbl_const_code: det.constr.toString(), lbl_ext_feat_code: det.ext_feat_code.toString(), segment_id: seg_id, status: 'A', modifier: det.modifier, area: parseInt(skd.areas[index]), perimeter: parseInt(skd.perimeters[index]), improvement_id: segment_data['improvement_id'], extension: segment_data['extension'],  default_ext_cover: det.ext_cover, lbl_frac2: 0,lbl_frac3: 0, lbl_frac4: 0,area2: 0,area3: 0, finish_area: 0, unfin_area: 0 }; 
				 			var flr = {floor_recs: 0, otherValues: []};
				 			var tArea = skd.fixedAreaTrue ? parseInt(skd.fixedAreaTrue) : parseInt(skd.areas[index]);
				 			var prefix_lkup = Proval_Lookup.filter(function(lkp){ return lkp.tbl_element == det.prefix && lkp.tbl_type_code == 'Prefix' && lkp[grade_col] && lkp[grade_col].indexOf(lvl_code_values[lvl_code]) > -1 })[0];
				 			prefix_lkup = prefix_lkup? prefix_lkup: {}
				 		
				 			var comp_lkup = Proval_Lookup.filter(function(lkp){ return lkp.tbl_element == det.comp && lkp.tbl_type_code == 'Component' && lkp[grade_col] && lkp[grade_col].indexOf(lvl_code_values[lvl_code]) > -1 })[0];
				 			comp_lkup = comp_lkup? comp_lkup: {}
				 		
				 			var const_lkup = Proval_Lookup.filter(function(lkp){ return lkp.tbl_element == det.constr && lkp.tbl_type_code == 'Construction' && lkp[grade_col] && lkp[grade_col].indexOf(lvl_code_values[lvl_code]) > -1 })[0];
				 			const_lkup = const_lkup? const_lkup: {}
				 		
				 			var ext_feat_lkup = Proval_Lookup.filter(function(lkp){ return lkp.tbl_element == det.ext_feat_code && lkp.tbl_type_code == 'Exterior Feature' && lkp[grade_col] && lkp[grade_col].indexOf(lvl_code_values[lvl_code]) > -1 })[0];
				 			ext_feat_lkup = ext_feat_lkup? ext_feat_lkup: {}
				 		
				 			if (det.prefix.toString() != 0) {
				 				flr.floor_recs +=  ((prefix_lkup[floor_col] && !isNaN(prefix_lkup[floor_col]))? parseInt(prefix_lkup[floor_col]): (prefix_lkup[floor_col] == 'n' && !isNaN(det.ns)? det.ns: 0));
				 				if((det.prefix.toString() == '10' || det.prefix.toString() == '15' ) && !isNaN(det.ns) && det.ns !=0)
				 					lbl.lbl_n = det.ns;
				 			}
				 			if (det.comp.toString() != 0) 
				 				flr.floor_recs +=  ((comp_lkup[floor_col] && !isNaN(comp_lkup[floor_col]))? parseInt(comp_lkup[floor_col]): (comp_lkup[floor_col] == 'n'&& !isNaN(det.ns)? det.ns: 0));
				 			if (det.constr.toString() != 0) 
				 				flr.floor_recs +=  ((const_lkup[floor_col] && !isNaN(const_lkup[floor_col]))? parseInt(const_lkup[floor_col]): (const_lkup[floor_col] == 'n'&& !isNaN(det.ns)? det.ns: 0));
			 			
				 			var comp = '', prefix = '', flr_area = 0;
				 			if(det.walkout=="Y")
				 				det.comp=1202;
				 			if (det.comp.toString() == '0' && det.prefix.toString() != '0') 
				 				lbl.lbl_frac1 = det.prefix.toString();
				 			else {
					 			if (det.comp.toString() != '0')  comp = (parseInt(det.comp) << 16).toString(2);
					 			if (det.prefix.toString() != '0') prefix = ((parseInt(det.prefix)) >>> 0).toString(2);
					 			lbl.lbl_frac1 = ((comp == ''? 0: parseInt(comp, 2)) + (prefix == ''? 0: parseInt(prefix, 2))).toString();
							}
							if(det.comp.toString() == '216' || det.comp.toString() == '217')
								Mtype = true;
							if(det.modifier=="Fin"){
								var lbl_update=0;
								lbl.finish_area = skd.fixedAreaTrue ? parseFloat(skd.fixedAreaTrue) : parseFloat(skd.areas[index]);
								det.fin_area = skd.fixedAreaTrue ? parseFloat(skd.fixedAreaTrue) : parseFloat(skd.areas[index]);
						 		flr.finish_living_areas=  skd.fixedAreaTrue ? parseFloat(skd.fixedAreaTrue) : parseFloat(skd.areas[index]);
						 		if(det.modifierSign=="+"){
						 			flr.floor_fin_qual=2;
						 			lbl.lbl_frac1=parseInt(lbl.lbl_frac1)+9887014715392;
						 			lbl_update=1;
						 		}
						 		else if(det.modifierSign=="-"){
						 			flr.floor_fin_qual=1;
						 			lbl.lbl_frac1=parseInt(lbl.lbl_frac1)+5592047419392;
						 			lbl_update=1;
						 		}
						 		if(lbl_update==0){
						 			lbl.lbl_frac1=parseInt(lbl.lbl_frac1)+1297080123392;
						 		}
							}
					    	else if(det.modifier=="UF"){
								lbl.lbl_frac1=parseInt(lbl.lbl_frac1)+1292785156096;
								lbl.unfin_area = skd.fixedAreaTrue ? parseFloat(skd.fixedAreaTrue) : parseFloat(skd.areas[index]);
							}
							else if(det.modifier==""){
								det.fin_area = '0';
								lbl.finish_area = skd.fixedAreaTrue ? parseFloat(skd.fixedAreaTrue) : parseFloat(skd.areas[index]);
							}
							if(det.ext_cover != '' && det.ext_cover != '0')
								flr.ext_cover1 = det.ext_cover;
							[prefix_lkup, comp_lkup, const_lkup, ext_feat_lkup].forEach(function(lkup){
								var other_val = evaluate_otherColumns(lkup, skt_type, tArea, det.floor_no, det.ns);
								other_val.forEach(function(ov){
									if (ov.table == saveObj.floor_table) 
										flr.otherValues.push(ov);
									else saveObj.otherValues = _.union(_.clone(saveObj.otherValues), ov);
								});
							});
						 
							if ((det.fin_area || det.ns || prefix_lkup[floor_key_col] || comp_lkup[floor_key_col] || const_lkup[floor_key_col]) && skt_type != 'Imp') {
								flr.fin_area = det.fin_area && !isNaN(det.fin_area)? Math.round(parseFloat(det.fin_area)): 0;
								if (det.prefix == 1 || det.prefix == 2 || det.prefix == 3) 
									flr_area =skd.fixedAreaTrue ? (parseFloat(prefix_lkup[floor_key_col]) * parseFloat(skd.fixedAreaTrue)): (parseFloat(prefix_lkup[floor_key_col]) * parseFloat(skd.areas[index]));
								else flr_area =skd.fixedAreaTrue ? parseFloat(skd.fixedAreaTrue) : parseFloat(skd.areas[index]);
								flr.area = Math.round(flr_area);
								flr.perimeter =skd.fixedPerimeterTrue ? Math.round(parseFloat(skd.fixedPerimeterTrue)): Math.round(skd.perimeters[index]);
								flr.PAR = Math.round(parseFloat((flr.perimeter / flr.area) * 100));
								flr.extension = segment_data['extension'];
								flr.dwelling_number = segment_data.dwelling_number;
								flr.walkout_bsmt = det.walkout;
								if ((prefix_lkup && prefix_lkup[floor_key_col]) || det.ns) {
									flr.floorKey = (prefix_lkup[floor_key_col] || det.ns);
						 			saveObj.floor_record.push(_.clone(flr));
					 			}
								if (comp_lkup && comp_lkup[floor_key_col]) {
									flr.floorKey = comp_lkup[floor_key_col]
						 			saveObj.floor_record.push(_.clone(flr));
					 			}
								if (const_lkup && const_lkup[floor_key_col]) {
									flr.floorKey = const_lkup[floor_key_col]
						 			saveObj.floor_record.push(_.clone(flr));
					 			}
							}
							if(det.floor_no) {
								saveObj.floor_record.forEach(function(flr){
									flr.floor_no = det.floor_no;
								});
							}
						
				 			if (skd.isModified) saveObj.labels.push(_.clone(lbl));
				 		}
				 	}
				});
				if (skd.isModified) {	
				    points.length > 1 && points[1].split(/\s/).forEach(function(p, i) {
					    if ( p.trim() != '' && p.trim() != 'P2' ){
					        var rr = convertStrToPoint( p );
					        var ch = 0;
					        if ( rr.indexOf( '/' ) > -1)  {
					            rr = rr.split( '/' )[0]
					            ch = rr.split( '/' )[1]
					        }
					        saveObj.vects.push( { curve_height: ch, run_rise: rr, seq_number: ( i + 1 ), segment_id: seg_id, extension: segment_data['extension'], improvement_id: segment_data['improvement_id'], status: 'A', eff_year: '0', line_type: '0', area: parseInt( skd.areas[index] ), perimeter: parseInt( skd.perimeters[index] ), vector: ( ( convertStrToPoint( p, true ) * 10 ) << 16 ), dimension_font_size: '8', dimension_position: '0' } );
					    }
					});
					if(Mtype){
						var imp_width = 0 ,imp_length = 0,hLength = 0,RLength = 0, improvement_ide = 'M' , imp_typed = 'MHOME', lT=0,uT=0; 
						points.length > 1 && points[1].split(/\s/).forEach(function(p, i) {
							 if ( p.trim() != '' ){
								if(p.split("/").length > 1)
									return;
								else{
									if((p.split("L").length > 1 || p.split("R").length > 1) && lT == 0 ){
										hLength = p.split("L").length > 1 ? parseFloat(p.split("L")[1]) : parseFloat(p.split("R")[1]);
										lT = 1;
									}
									else if((p.split("U").length > 1 || p.split("D").length > 1) && uT == 0 ){
										RLength = p.split("U").length > 1 ? parseFloat(p.split("U")[1]) : parseFloat(p.split("D")[1]);
										uT =1;
									}	
								}

							 }
						});
						if(hLength > RLength) {
							imp_length = hLength;
							imp_width = RLength;
						}
						else{
							imp_length = RLength;
							imp_width = hLength;
						}
						saveObj.MHOMEValues.push(  {improvement_id: improvement_ide, imp_type: imp_typed ,imp_width :imp_width, imp_length: imp_length, mh_length:imp_length, 
mh_width: imp_width});
						saveObj.MHOMEValuesTrue = true;
					}
					if (extensions.indexOf(segment_data['extension']) == -1) extensions.push(segment_data['extension'])
					saveObj.extension = segment_data['extension'];
					saveObj.improvement_id = segment_data['improvement_id'];
					saveObj.label_position = ProvalLabelPositionFromString(skd.labelPositions[index])
					var pos = skd.labelPositions[index];
					pos.y=pos.y - (((skd.label.split('|').length)==1)?4: (skd.label.split('|').length* 3));
					saveObj.displayed_area_position = ProvalLabelPositionFromString(pos);
					if (vectorString && vectorString.trim().length > 0) {
							saveObj.parentRowid = segment_data['ParentROWUID'];
							saveObj.area = skd.fixedAreaTrue ? skd.fixedAreaTrue : parseInt(skd.areas[index]);
							saveObj.perimeter = skd.fixedPerimeterTrue ? skd.fixedPerimeterTrue: parseInt(skd.perimeters[index]);
							saveObj.eff_year = '0';
							saveObj.segment_id = seg_id;
					}
					if (!skd.newRecord) {
						deleteData.push({ source: saveObj.vect_tbl, parent: segment_data });
						deleteData.push({ source: saveObj.lbl_table, parent: segment_data });
						if (vectorString && vectorString.trim().length > 0) 
							saveData.push(_.clone(saveObj));
					}
					else 
						newRecords.push({ catId: getCategoryFromSourceTable(skd.sourceName).Id, parentData: segment_data, saveObj: _.clone(saveObj), segment_id: seg_id, extension: segment_data['extension'], improvement_id: segment_data['improvement_id'] });
				}
				else 
					saveData.push({ isSegmentIdOnly: true, segment_id: seg_id.toString(), rowId: saveObj.rowId, seg_tbl: skd.sourceName, vect_tbl: saveObj.vect_tbl, lbl_tbl: saveObj.lbl_table,floor_Records:saveObj.floor_record,floor_Table:saveObj.floor_table });
			});
			if (sketchApp.sketches.filter(function(sk){ return sk.sid == skrowid })[0].vectors.length == 0 && !deleteData.some(function(delData){ return delData.source == saveObj.parentTbl && delData.parent.ROWUID == skrowid }))
				deleteData.push({ source: saveObj.parentTbl, parent: activeParcel[saveObj.parentTbl].filter(function(pd){ return pd.ROWUID == skrowid })[0], isCurrentRecord: true, deleteMHRecord: true })
		});		
	});
		
	noteData.forEach(function(nt) {
		var note_rec = activeParcel['SktNote'].filter(function(nt_rec) { return nt_rec.ROWUID == nt.noteid })[0];
		if (nt.noteText == '*DEL*') 
			deleteNoteData.push(nt.noteid);
		else {
			if (nt.newRecord) {
				var parentData = sketchApp.sketches.filter(function(sk){ return sk.sid == nt.parentId })[0].parentRow.parentRecord;
				newRecords.push({ newAuxRowuid: nt.clientId, doNotUpdate: true, catId: getCategoryFromSourceTable('SktNote').Id, parentData: parentData })
			}
			var xPos = nt.xPosition, yPos = nt.yPosition, pos = 0;
			pos = parseFloat((xPos > 0)? convertStrToPoint('R' + xPos): convertStrToPoint('L' +(parseFloat(xPos.toString().split("-")[1]))));
			pos += parseFloat((yPos < 0)? convertStrToPoint('D' + (parseFloat(yPos.toString().split("-")[1]))): convertStrToPoint('U' + yPos));
			saveNoteData.push({ position: pos, text: nt.noteText, rowid: (nt.newRecord? nt.clientId: nt.noteid) });
		}
	});
	
	while(sketchApp.deletedVectors.length > 0) {
		var vector = sketchApp.deletedVectors.pop();
		var seg_table = vector.vectorConfig.Table;
		var table_type = seg_table.split('ccv_sktsegment')[1]
		var seg_data = activeParcel[seg_table].filter(function(r){ return r.ROWUID == vector.uid })[0];
		var par_data = deleteData.filter(function(pdata){ return pdata.parent.ROWUID == seg_data.ParentROWUID })[0]
		if (!par_data)
			deleteData.push({ source: vector.vectorConfig.Table, parent: seg_data, isCurrentRecord: true, vector_tbl: 'ccv_sktvector' + table_type, lbl_tbl: 'ccv_sktsegmentlabel' + table_type });
	}
	console.log('saveData: ', saveData);
	console.log('deleteData: ', deleteData);
	deleteRecs(deleteData);
}

function ProvalSketchBeforeSave(sketches, callback) {
	var labels = [], lookupVal,BasementFunctionalitys = [], level_code = {'1': 'Below', '2': 'At', '3': 'Above'}, skt_type = '', var_Check = false, sketch_test = [];
	function sketchsavee(){
		$('.ccse-canvas').removeClass('dimmer');
        $('.dimmer').hide();
		sketches.filter(function(skda){ return skda.isModified }).forEach(function(skd){
			skt_type = '';
			if (skd.config.SketchSource.Table == 'ccv_improvements_dwellings') skt_type = 'Res';
			if (skd.config.SketchSource.Table == 'ccv_improvements_comm_bldgs') skt_type = 'Comm';
			if (skd.config.SketchSource.Table == 'ccv_improvements') skt_type = 'Imp';
			if (skt_type == '') return;
			skd.vectors.filter(function(vect){ return vect.isModified; }).forEach(function(vector){
				if(vector.BasementFunctionality){
					if(vector.labelFields[0] && vector.labelFields[0].labelDetails){
						var B_area = vector.fixedAreaTrue ? parseInt(vector.fixedAreaTrue) : parseInt(vector.area())
						BasementFunctionalitys.push({ labelValue: vector.labelFields[0].labelDetails.labelValue, sid: skd.sid, sketch: vector, vect_uid: vector.uid, screen: 'BGAreaEntry', lvl_code: '1', labelDetails: vector.labelFields[0].labelDetails, Area: parseInt(B_area),skt_type:skt_type });
						$.each(vector.labelFields[0].labelDetails.details, function(lvl_code, details) {
							if(lvl_code != '1' && lvl_code != 1){
								$.each(details, function(type, value) {
									if (value != 0) {
										lookupVal = Proval_Lookup.filter(function(lk){ return lk.tbl_element == value && lk.AdditionalScreen && lk[skt_type + 'GradeLevel'] && lk[skt_type + 'GradeLevel'].indexOf(level_code[lvl_code]) > -1; });
										if (lookupVal.length > 0)
											labels.push({ lookupDetail: lookupVal[0], sid: skd.sid, sketch: vector, screen: lookupVal[0].AdditionalScreen, lvl_code: lvl_code });
									}
								});
							}
						});
					}	
				}
				else{
					if(vector.labelFields[0] && vector.labelFields[0].labelDetails){
						$.each(vector.labelFields[0].labelDetails.details, function(lvl_code, details) {
							$.each(details, function(type, value) {
								if (value != 0) {
									lookupVal = Proval_Lookup.filter(function(lk){ return lk.tbl_element == value && lk.AdditionalScreen && lk[skt_type + 'GradeLevel'] && lk[skt_type + 'GradeLevel'].indexOf(level_code[lvl_code]) > -1; });
									if (lookupVal.length > 0)
										labels.push({ lookupDetail: lookupVal[0], sid: skd.sid, sketch: vector, screen: lookupVal[0].AdditionalScreen, lvl_code: lvl_code });
								}
							});
						});
					}
				}
			});
		});
	function nBasementFunctionality(){
		if (labels.length > 0) {
	 		$('.mask').show();	
	 		$('.Current_vector_details').show();
	 		$('.outbdiv').hide();
	 		$('.skNote').html('');
	 		var cancelCallback = function(isChecked) {
	 			isChecked = !isChecked? false: true;
	       		$('.Current_vector_details').hide();
	        	$('.mask').hide();
	        	hideKeyboard();
	 			callback(isChecked, isChecked);
	        	return false;
	 		}
	 	
	 		var checkAreaEntry = function(){
		 		var area_entry_lbl = labels.filter(function(lbl){ return lbl.screen == 'AreaEntry' });
		 		if (area_entry_lbl.length > 0) {
		 			$('.Current_vector_details .head').html('Area Entry');
		 			var table = '<table class="area_entry"><tr>';
		 			table += '<th rowspan="2">Label</th><th rowspan="2">Area</th><th colspan="2">Finished Area</th><th  rowspan="2">Unfinished Area</th></tr>';
		 			table += '<tr><th>Value</th><th>%</th></tr>';
			 		area_entry_lbl.forEach(function(label){
			 			table += '<tr class="trow" sid="' + label.sid + '" vid="' + label.sketch.uid + '" lbl="' + label.lookupDetail.tbl_element_desc + '" lvl_code="' + label.lvl_code + '" type="' + label.lookupDetail.tbl_type_code + '" area = "' + label.sketch.area() + '">'
			 			table += '<td>' + label.lookupDetail.tbl_element_desc + '</td><td>' + label.sketch.area() + '</td>';
			 			table += '<td><input type="number" class="fin_area fin_area_value" /></td>';
			 			table += '<td><input type="number" class="fin_area fin_area_perc" /></td>';
			 			table += '<td style="text-align: center;"><span class="unfin_area">' + label.sketch.area() + '</spin></td></tr>';
			 		});
			 		$('.Current_vector_details .dynamic_prop').html(table + '</table>');
			 		$('.fin_area').css({'width': '90px', 'height': '20px'})
			 		$('.fin_area').unbind();
			 		$('.fin_area').bind('keyup', function(){
			 			var that = this;
			 			var thisVal = $(that).val();
		 				thisVal = thisVal && !isNaN(thisVal)? parseInt(thisVal): 0;
		 				if(isNaN(thisVal))
		 					thisVal=0;
		 				var trow = $(that).parents('.trow')
		 				var area = $(trow).attr('area')
		 				area = area && !isNaN(area)? parseInt(area): 0;
			 			if ($(that).hasClass('fin_area_value')) {
			 				if (thisVal > area) {
			 					$(that).val($(that).val().substring(0,$(that).val().length-1))
			 					return false;
		 					}
		 					else {
		 						var perc = Math.round((thisVal/area) * 100);
		 						$('.fin_area_perc', trow).val(perc);
		 						$('.unfin_area', trow).html(Math.round(area - thisVal));
		 					}
			 			}
			 			if ($(that).hasClass('fin_area_perc')) {
			 				if (thisVal > 100) {
			 					$(that).val($(that).val().substring(0,$(that).val().length-1))
			 					return false;
		 					}
		 					else {
		 						var value = (area * thisVal)/100;
		 						$('.fin_area_value', trow).val(Math.round(value));
		 						$('.unfin_area', trow).html(Math.round(area - value));
		 					}
			 			}
			 		});
			   		$('#Btn_Save_vector_properties').unbind(touchClickEvent);
			    	$('#Btn_Save_vector_properties').bind(touchClickEvent, function () {
			        	var sid, vid, lvl_code, type, lookupVal;
			        	$('.trow').each(function(index, trow){
			        		sid = $(trow).attr('sid');
			        		vid = $(trow).attr('vid');
			        		lvl_code = $(trow).attr('lvl_code');
			        		type = $(trow).attr('type');
			        		sketchApp.sketches.filter(function(sk){ return sk.sid == sid })[0].vectors.filter(function(vect){ return vect.uid == vid })[0].labelFields[0].labelDetails.details[lvl_code].fin_area = $('.fin_area_value', $(trow)).val();
			        	})
			        	$('.Current_vector_details').hide();
			        	$('.mask').hide();
			        	hideKeyboard();
			    		if (callback) callback(true, true);
			    	});	
		 		}
		 		else cancelCallback(true);
		 	
		 		$('#Btn_cancel_vector_properties').unbind(touchClickEvent);
				$('#Btn_cancel_vector_properties').bind(touchClickEvent, function(){ cancelCallback(false); });	    
		 		$('.Current_vector_details').width(440)
			}
		
	 		var starting_flr_lbl = labels.filter(function(lbl){ return lbl.screen == 'Starting Floor' });
	 		if (starting_flr_lbl.length > 0) {
	 			$('.Current_vector_details .head').html(starting_flr_lbl[0].screen);
	 			var table = '<table class="starting_floor"><tr>';
	 			table += '<th style="width: 125px;">Label</th><th style="width: 75px;">Area</th><th>Starting Floor No</th></tr>'
		 		starting_flr_lbl.forEach(function(label){
		 			table += '<tr><td>' + label.lookupDetail.tbl_element_desc + '</td><td style="text-align: center;">' + label.sketch.area() + '</td>';
		 			table += '<td><input style="width: 100px; height: 20px;" type="number" class="floorNo lbl_" oninput="if(value.length>4)value=value.slice(0,4)" uid="' + label.sketch.uid + '" sid="' + label.sid + '" lvl_code="' + label.lvl_code + '" ' + label.lookupDetail.tbl_type_code + '" /></td></tr>';
		 		});
		 		$('.Current_vector_details .dynamic_prop').html(table + '</table>');
	    	    
		    	$('#Btn_Save_vector_properties').unbind(touchClickEvent);
		    	$('#Btn_Save_vector_properties').bind(touchClickEvent, function () {
		    		var isValid = true;
		    		$('.floorNo').each(function(){
		    			if ($(this).val() == '') isValid = false;
		    		});
		    		if (isValid) {
		    			$('.floorNo').each(function(i, txt_box){
		    				sketchApp.sketches.filter(function(sk){ return sk.sid == $(txt_box).attr('sid') })[0].vectors.filter(function(v){ return v.uid == $(txt_box).attr('uid') })[0].labelFields[0].labelDetails.details[$(txt_box).attr('lvl_code')]['floor_no'] = $(txt_box).val();
		    			});
		    			checkAreaEntry();
	    			}
		    		else return false;
		   		});	
		 	
		 		$('#Btn_cancel_vector_properties').unbind(touchClickEvent);
				$('#Btn_cancel_vector_properties').bind(touchClickEvent, function(){ cancelCallback(false); });	 
		    	$('.Current_vector_details').width(365)
	 		}
	 		else checkAreaEntry();
 		}
 		else if (callback) callback(true, true);	
	} 	

	if (BasementFunctionalitys.length > 0) {
		function BasementFunctionalityss(){
			var basementFunctionalitys = BasementFunctionalitys.pop();
			var html = '',components = [],default_area=[];
			var bfun = basementFunctionalitys.labelDetails.details[1];
			var to_Area = basementFunctionalitys.Area;
			var Vect_uid = basementFunctionalitys.vect_uid;
			var Sid = basementFunctionalitys.sid;
			$('.Current_vector_details .head').html('Area Entry');
			$('.dynamic_prop').html('');
			$('.dynamic_prop').append('<div class="divvectors"></div>');
	 		html += '<span style="width: 400px;justify-content: center;font-weight: bold;font-size: 18px;">Enter Below Ground Areas</span>';
	 		if(bfun.comp1!=0)
	 			components.push({comp:bfun.comp1, prefix: bfun.prefix1, modifier:bfun.modifier1});
	 		if(bfun.comp2!=0)
	 			components.push({comp:bfun.comp2, prefix: bfun.prefix2, modifier:bfun.modifier2});
	 		if(bfun.comp3!=0)
	 			components.push({comp:bfun.comp3, prefix: bfun.prefix3, modifier:bfun.modifier3});
	 		if(bfun.comp4!=0)
	 			components.push({comp:bfun.comp4, prefix: bfun.prefix4, modifier:bfun.modifier4});
	 		var i = 0;
	 		html += '<div class="BfunBeforeSave"><table>';
	 		components.forEach(function(com){
	 			i= i+1;
	 			var lookupVal = Proval_Lookup.filter(function(lk){ return lk.tbl_element == com.comp && lk.AdditionalScreen == 'BGAreaEntry' && lk[skt_type + 'GradeLevel'] })[0];
	 			var t_area = com.prefix == '1' ? parseInt( 0.25 * basementFunctionalitys.Area) : (com.prefix == '2' ? parseInt( 0.5 * basementFunctionalitys.Area) : parseInt( 0.75 * basementFunctionalitys.Area))
	 			default_area.push(t_area);
	 			if(i == components.length){
	 				html += '<tr><td style="padding-right: 75px;font-weight: bold;font-size: 15px;">'+ lookupVal.tbl_element_desc +'</td>';
		 			html += '<td><input style="width: 100px; height: 20px;" type="number" id='+com.comp+' uid="' + basementFunctionalitys.sketch.uid + '" sid="' + basementFunctionalitys.sid + '" value="'+t_area+'" disabled /></td></tr>';
	 			}
	 			else{
	 				if(com.comp == '202' && com.modifier != ''){
	 					html += '<tr><td style="padding-right: 75px;font-weight: bold;font-size: 15px;">Finished Basement</td>';
		 				html += '<td><input style="width: 100px; height: 20px;" type="number" id="Fin" uid="' + basementFunctionalitys.sketch.uid + '" sid="' + basementFunctionalitys.sid + '" value="'+t_area+'" /></td></tr>';
		 				html += '<tr><td style="padding-right: 75px;font-weight: bold;font-size: 15px;">Unfinished Basement</td>';
		 				html += '<td><input style="width: 100px; height: 20px;" type="number" id="UnFin" uid="' + basementFunctionalitys.sketch.uid + '" sid="' + basementFunctionalitys.sid + '" value="'+0+'"/></td></tr>';
	 				}
	 				else{
	 					html += '<tr ><td style="padding-right: 75px;font-weight: bold;font-size: 15px;">'+ lookupVal.tbl_element_desc +'</td>';
		 				html += '<td><input style="width: 100px; height: 20px;" type="number" id='+com.comp+' uid="' + basementFunctionalitys.sketch.uid + '" sid="' + basementFunctionalitys.sid + '" value="'+t_area+'" /></td></tr>';
	 				}
	 			}
	 		});
	 		html += '</table></div>'
			$('.divvectors').append(html);
			$('.skNote').hide();
			$('#Btn_cancel_vector_properties').html('Defaults ');
			$('.Current_vector_details').show()
			$('.Current_vector_details').width(425)
			$('.BfunBeforeSave input').unbind();
			$('.BfunBeforeSave input').bind('keyup', function(){
  				var input_length = $(".BfunBeforeSave input").length;
  				var area_array = [], tot_area=0, tt_area = 0, last_area = 0;
  				$('.BfunBeforeSave input').forEach(function(Inp){
					area_array.push(Inp.value);
				});
  				for(var i=0; i<area_array.length-1; i++){
  					if(area_array[i] == '')
  						area_array[i] = '0';
  					tot_area = tot_area + parseInt(area_array[i]);
  				}
  				tt_area = to_Area - tot_area;
  				if(tt_area >= 0){
  					last_area = tt_area;
  					$('.BfunBeforeSave input')[input_length-1].value = last_area;
  				}
  				else
  				{	
  					this.value = parseInt(this.value) + tt_area;
  					last_area = 0;
  					$('.BfunBeforeSave input')[input_length-1].value = last_area;
  				}
			});
			$('#Btn_Save_vector_properties').unbind(touchClickEvent);
			$('#Btn_Save_vector_properties').bind(touchClickEvent, function () {
				var sdetails = {}; 
				var vect = sketchApp.sketches.filter(function(sk){ return sk.sid == Sid })[0].vectors.filter(function(vect){ return vect.uid == Vect_uid })[0];
				vect.labelFields[0].labelDetails.base_array = [];
				$('.BfunBeforeSave input').forEach(function(sav){
					sdetails = {Id: sav.id, area: sav.value};
					vect.labelFields[0].labelDetails.base_array.push(_.clone(sdetails));
				});
				$('.Current_vector_details').hide();
				$('#Btn_cancel_vector_properties').html('Cancel ');
				if(BasementFunctionalitys.length > 0)
					BasementFunctionalityss();
				else{
					$('.mask').hide();
					hideKeyboard();
					nBasementFunctionality();
				}
			});
			$('#Btn_cancel_vector_properties').unbind(touchClickEvent);
			$('#Btn_cancel_vector_properties').bind(touchClickEvent, function(){ 
				var k = 0;
				$('.BfunBeforeSave input').forEach(function(Inp){
					if(Inp.id == 'UnFin')
						Inp.value = '0';
					else{
						Inp.value = default_area[k];
						k = k + 1;
					}
				});
			});	 
		}
		$('.mask').show();
		BasementFunctionalityss();
	}
	else
		nBasementFunctionality();
	}
	var imp_width = 0 ,imp_length = 0;
	function calculateImpwidth(souTable, impcallback){
		if(souTable != 'ccv_improvements'){
			if(impcallback) impcallback();
		}
		else{
			var vectors = sketchApp.currentSketch && sketchApp.currentSketch.vectors[0];
			var vectorString = vectors.vectorString;
			var points = vectorString.split(':')[1].split(' S ');
			var hLength = 0, RLength = 0, lT=0, uT=0; 
			points.length > 1 && points[1].split(/\s/).forEach(function(p, i) {
				if ( p.trim() != '' ){
					if(p.split("/").length > 1)
						return;
					else{
						if((p.split("L").length > 1 || p.split("R").length > 1) && lT == 0 ){
							hLength = p.split("L").length > 1 ? parseFloat(p.split("L")[1]) : parseFloat(p.split("R")[1]);
							lT = 1;
						}
						else if((p.split("U").length > 1 || p.split("D").length > 1) && uT == 0 ){
							RLength = p.split("U").length > 1 ? parseFloat(p.split("U")[1]) : parseFloat(p.split("D")[1]);
							uT =1;
					}	
				}

			}
			});
			if(hLength > RLength) {
				imp_length = hLength;
				imp_width = RLength;
			}
			else{
				imp_length = RLength;
				imp_width = hLength;
			}
		
			if(impcallback)
				impcallback();
		}
	}
	function RecCretation(){
	  if(!var_Check && sketchApp.currentSketch && sketchApp.currentSketch.isAfterSaveNewSketch){
		var sTable=sketchApp.currentSketch.config.SketchSource.Table;
		var cateeId=getCategoryFromSourceTable(sTable).Id;
		var specialCatId=getCategoryFromSourceTable("comm_special_use").Id;
		var ImpIdMethod_Field, ImpTypee_Field; 
		var ImpIdMethodd = sketchApp.currentSketch.ImpType? sketchApp.currentSketch.ImpType : '';
		var ImpTypee = sketchApp.currentSketch.ImpIdMethod ? sketchApp.currentSketch.ImpIdMethod : '';
		var groupField = sketchApp.currentSketch.GroupingValue;
		if(groupField){
			var vector = sketchApp.currentSketch.vectors.filter(function(fun){ return fun.sketchType == "outbuilding"})[0];
			if(vector){
				var labelcode = vector.name;
				var label_Name = Proval_Outbuliding_Lookup.filter(function(plook){ return plook.Code == labelcode })[0].label_description;
				ImpIdMethodd = labelcode ? labelcode : label_Name;
			}
		}
		if(sTable=='ccv_improvements_comm_bldgs'){
			ImpIdMethod_Field = getDataField('imp_type','ccv_improvements_comm_bldgs');
			ImpTypee_Field = getDataField('improvement_id','ccv_improvements_comm_bldgs');
		}
		else if(sTable=='ccv_improvements') {
			ImpIdMethod_Field = getDataField('imp_type','ccv_improvements');
			ImpTypee_Field = getDataField('improvement_id','ccv_improvements');
		}
		else {
			ImpIdMethod_Field = getDataField('imp_type','ccv_improvements_dwellings');
			ImpTypee_Field = getDataField('improvement_id','ccv_improvements_dwellings');
		}
		
		function createNeww(){
	 		var ParentRowExt=sketchApp.currentSketch.parentRow;
	 		var cuSid=sketchApp.currentSketch.sid;
        	insertNewAuxRecord(null, ParentRowExt, cateeId,cuSid,{imp_width: imp_width, imp_length: imp_length},function (rowid) {
        		var parentROEW=activeParcel?activeParcel[sTable].filter(function(rrrecord){return rrrecord.ROWUID==rowid})[0]:[];
        		update_data(ImpIdMethod_Field,ImpIdMethodd, parentROEW, function(){
        			update_data(ImpTypee_Field,ImpTypee, parentROEW, function(){
        				if(sTable=="ccv_improvements_comm_bldgs"){
        					var use_Field=getDataField('use_code','comm_special_use');
        					var area_Field=getDataField('area','comm_special_use');
        					var Area_special = 0;
        					sketchApp.currentSketch.vectors.forEach(function(vecter){
								Area_special = Area_special + parseFloat(vecter.area());
							});
							insertNewAuxRecord(null, parentROEW, specialCatId,null,null,function (rowwid) {
								var SpecialRecord=activeParcel?activeParcel["comm_special_use"].filter(function(rRecord){return rRecord.ROWUID==rowwid})[0]:[];
								update_data(use_Field,'COMBLDG', SpecialRecord, function(){
									Area_special=parseInt(Area_special);
									update_data(area_Field,Area_special, SpecialRecord, function(){
										sketchApp.currentSketch.parentRow=parentROEW;
        								sketchsavee();
									});
								});
							});
        				}
        				else{
        					sketchApp.currentSketch.parentRow=parentROEW;
        					sketchsavee();
        				}
        			});
        		});
			});
		}
		calculateImpwidth(sTable,function(){
			createNeww();
		});		
	}
	else{
		var total_Area=0;
		var sTable=sketchApp.currentSketch && sketchApp.currentSketch.config.SketchSource.Table;
		var rowUid=sketchApp.currentSketch && sketchApp.currentSketch.parentRow.ROWUID;
		var specRecord=activeParcel["comm_special_use"]?activeParcel["comm_special_use"].filter(function(rRecordID){return rRecordID.ParentROWUID==rowUid})[0]:null;
		if(sTable=="ccv_improvements_comm_bldgs" && specRecord){
			var area_Field=getDataField('area','comm_special_use');
			sketchApp.currentSketch.vectors.forEach(function(vecter){
				total_Area = total_Area + parseFloat(vecter.area());
			});
			total_Area = parseInt(total_Area);
			update_data(area_Field,total_Area, specRecord, function(){
        		sketchsavee();
			});
		}
		else
			sketchsavee();
	}
  }
  sketch_test = sketches.filter(function(skda){ return skda.isModified && (skda.isAfterSaveNewSketch || skda.isNewSketch ) })
  var sketch_test_length = sketch_test.length;
  var Tot_sketch_test_length = 0;
  $('.ccse-canvas').addClass('dimmer');
  $('.dimmer').show();
  if(sketch_test_length > 0){
	sketch_test.forEach(function(sketchesss){
		Tot_sketch_test_length = Tot_sketch_test_length + 1;
		var vector = [];
		var groupField = sketchesss.GroupingValue;
		var ImpIdMethod_Fieldd = getDataField('imp_type','ccv_improvements');
		var ParentRowExn = sketchesss.parentRow;
		var cateId = getCategoryFromSourceTable('ccv_improvements').Id;
		var cuSkid=sketchesss.sid;
		vector = sketchesss.vectors.filter(function(fun){ return fun.sketchType == "outbuilding"});
		var vect_length = vector.length;
		if(vector.length>0){
			var t_len = 0;
			if(sketchApp.currentSketch.sid == sketchesss.sid)
				var_Check = true;
			vector.forEach(function(vect){
				t_len = t_len + 1;
				var labelcode = vect.name;
				var label_Name = Proval_Outbuliding_Lookup.filter(function(plook){ return plook.Code == labelcode })[0].label_description;
				var ImpIdMethoddd = labelcode ? labelcode : label_Name;
				insertNewAuxRecord(null, ParentRowExn, cateId,cuSkid,null,function (rowid) {
					var parentROEW=activeParcel?activeParcel['ccv_improvements'].filter(function(rrrrecord){return rrrrecord.ROWUID==rowid})[0]:[];
					sketchesss.parentRow = parentROEW;
        			update_data(ImpIdMethod_Fieldd,ImpIdMethoddd, parentROEW,function(ret){
        				if(Tot_sketch_test_length == sketch_test_length)
							RecCretation();
        			});
				});
			});
		}
		else
		{
			if(Tot_sketch_test_length == sketch_test_length)
				RecCretation();
		}
		
	});
  }
  else{
   	RecCretation();
  }
}

