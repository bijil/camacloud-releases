﻿var visionPCIs = [], autoInsertRecords = [];

var FarragutAssociateVectorDefinition = function (associateVector, callback) {
	if (!sketchApp.sketches.some(function (sk) { return sk.isModified; })) { if (callback) callback(); return; }
    if (!sketchApp.sketches.some(function (sk) { return sk.vectors.length != 0; })) { if (callback) callback(); return; }
    
    function FarragutLookupDefinition (sktData, fcallback) {
    	if (sktData.length == 0) {
    		if (fcallback) fcallback();
    		return;
    	}
    	
    	var skData = sktData.pop();
    	var sketch = sketchApp.sketches.filter(function(sk) { return sk.sid == skData })[0];
        var sketchLabel = sketch.label;
    	var sketchSourceTable = sketch.config.SketchSource.Table;
    	var associateSource = associateVector.source;
    	$('.control_div div').remove();
        $('.Current_vector_details .head').html('Associate Sketch to CAMA Record');
        $('.mask').show(); $('.dynamic_prop').html('');
        $('.dynamic_prop').append('<span class ="sklbl" style="padding-bottom:10px; font-weight: 500; text-align: center; width: 100%;">'+ sketchLabel +'<span>');
        $('.dynamic_prop').append('<div class="divvectors"></div>');
        var tp = sketchSourceTable == 'CCV_RES_BUILDING'? 'Res': 'Com';
        associateSource.forEach(function (src) {
        	if (src.hidden && eval(src.hidden)) return;
        	var headers = src[tp + 'Headers'], type = src.Type, stable =src[tp + 'SourceTable'], fields = src[tp + 'Fields'];
            var html = '<table class='+ type +' style="width: 100%;"><tr><th style="background: #d6d6d6;height:35px;width:300px">Sketch Label </th><th style="background: #d6d6d6;height:35px;width:140px">Area</th>';
            html += '<th style="background: #d6d6d6;height:35px">' + headers.split(',').join('</th><th style="background: #d6d6d6;height:35px">') + '</th>';
            html += '</tr>';
            var fieldHtml = '', additionalVal = (type == 'Addition'? 'ADDN': 'SOCC');
            var oneSketchExists = false;
            sketch.vectors.filter(function(v) { return (!v.isFreeFormLineEntries && (type == 'MainBuilding'? (v.code == 'BAS'? true: false): true) ) }).forEach(function (sk, j) {
            	if (sk.isFreeFormLineEntries)
            		return;
                var opt = '', addVector = true;
                opt += '<tr sketch="' + skData + '" vector="' + sk.uid + '">';
                var code = sk.code, addnOrSect = null;
	            var fLookup = Farragut_Lookup.filter(function(f) { return f.ATTRIBUTE_VALUE == code })[0];
	            if (fLookup) 
					addnOrSect = fLookup.LOOKUP_TYPE_AdditionalValue1;
				if ((!addnOrSect || addnOrSect != additionalVal) && type != 'MainBuilding')
					return;
				if(!oneSketchExists) oneSketchExists = true;
				var vectType = sk.newRecord? '[N]': (sk.isModified)? '[M]': '';
                var sketchvar = addnOrSect == 'ADDN'? 'NON_SKETCHED_FLAG': 'IS_SKETCHED';
			    var sketchVal = addnOrSect == 'ADDN'? 'N': 'Y';
                if (fieldHtml == '') {
                	if (type == 'MainBuilding')
                		fieldHtml += '<td><select class="popupselect1" sourceType="' + type + '" style=" width: 90%; margin: 0 auto; float: none; display: block;" disabled = "disabled">';
                    else
                    	fieldHtml += '<td><select class="popupselect1" style=" width: 90%; margin: 0 auto; float: none; display: block;" sourceType="' + type + '">';
                    fieldHtml += '<option value="0" value1=""></option>';

                    let srec = type == 'MainBuilding' && tp == 'Res' ? activeParcel['CCV_RES_BUILDING'].filter((rk) => { return rk.ROWUID == skData })  : activeParcel[stable].filter((rec) => { return (rec.ParentROWUID == skData && rec.CC_Deleted != true && (type == 'MainBuilding' ? (rec.MAIN_LOOKUP == "ADDNBSMTUF" ? true : false) : (rec[sketchvar] == sketchVal ? true : false))) });
                    srec.forEach((res) => {
                		var disHtml = '';
                        var len = fields.split('/').length;
                        if (type == 'MainBuilding' && tp == 'Res') {
                            disHtml = 'Card #: ' + (res['CARD_NUM'] ? res['CARD_NUM'] : '') + ' - Building #: ' + (res['BUILDING_NUM'] ? res['BUILDING_NUM'] : '') + ' - ' + (res['MB_FOOTPRINT_AREA'] ? res['MB_FOOTPRINT_AREA'] : '');
                        }
                        else if (addnOrSect == 'ADDN') {
                            let mlField = getDataField('MAIN_LOOKUP', stable), mlValue = res['MAIN_LOOKUP'], asqft = res['FOOTPRINT_AREA'];
                            if (mlField) {
                                let mlId = '#FIELD-' + mlField.Id;
                                if (lookup?.[mlId]?.[mlValue]?.Object?.SHORT_DESC) {
                                    mlValue = lookup[mlId][mlValue].Object.SHORT_DESC;
                                }
                            }
                            disHtml = (mlValue ? mlValue : '') + " - " + (asqft ? asqft : '');
                        }
                        else {
                            let otField = getDataField('OCCUPANCY_TYPE', 'CCV_COM_SECTION_FEATURE'), oField = getDataField('OCCUPANCY', 'CCV_COM_SECTION_FEATURE'), otVal = res['OCCUPANCY_TYPE'], oVal = res['OCCUPANCY'];

                            if (otField && otField.InputType == '5') {
                                let desc = evalLookup('OCCUPANCY_TYPE', otVal, null, otField);
                                otVal = ((desc == '???') || (desc == '') || (desc == null)) ? otVal : desc;
                            }

                            if (oField && oField.InputType == '5') {
                                let desc = evalLookup('OCCUPANCY_TYPE', oVal, null, oField);
                                oVal = ((desc == '???') || (desc == '') || (desc == null)) ? oVal : desc;
                            }

                            disHtml = 'Section #: ' + (res['SECTION_ID'] ? res['SECTION_ID'] : '') + ' - Occupancy Type: ' + (otVal ? otVal: "") + ' - Occupancy: ' + (oVal ? oVal : "") + ' - ' + (res['SQFT'] ? res['SQFT'] : "");
                        }
                		if (type == 'MainBuilding')
                			fieldHtml += '<option value="' + res.ROWUID + '" value1="' + res['MAIN_LOOKUP'] + '">' + disHtml + '</option>';
                		else
                            fieldHtml += '<option value="' + res.ROWUID + '" value1="' + (type == "Addition" ? res['MAIN_LOOKUP'] : res['OCCUPANCY']) + '">' + disHtml + '</option>';
                	});
                }
                opt += ' <td><span style="color:green; width: 20px !important; min-width: 20px !important;float:right">'+ vectType +'</span><span style="width:255px; word-break: break-all">' + sk.label + '</span></td><td><span class="assoc_area" style="width: 140px;text-align:center">' + sk.area() + '</span></td>' + fieldHtml;
                opt += '</tr>';
                if (addVector) html += opt;
            });
            html += '</table>';
            if (oneSketchExists)
         		$('.divvectors').append(html);
       	});
       	
       	if (associateVector.note) {
            $('.skNote').html('<span><b>Note:</b> ' + associateVector.note + '</span>');
            $('.skNote').css('color', '#fb2300')
        }
        
        $('.Current_vector_details').show();
        $('.divvectors').css('width', 710);
        $('.skNote').show();
        $('.divvectors').css('max-height', 325);
        $('.Current_vector_details').css('top', '15%');
       	$('.Current_vector_details').css('max-width', 750);
       	
       	var currentValueOption;
        $('.dynamic_prop .popupselect1').change(function () {
            if ($(this).val() != 0)
                $(this).closest('div').find('tr').find('select').find('option[value="' + $(this).val() + '"]').attr('disabled', 'disabled');
            $(this).closest('div').find('tr').find('select').find('option[value="' + currentValueOption + '"]').removeAttr('disabled');
            $(this).closest('tr').find('select').not($(this)).val($(this).val());
        });

        $('.dynamic_prop .popupselect1').focus(function () {
        	currentValueOption = $(this).val();
        });
        
        $('.Current_vector_details #Btn_cancel_vector_properties').unbind(touchClickEvent)
        $('.Current_vector_details #Btn_cancel_vector_properties').bind(touchClickEvent, function () {
            $('.skNote').html('');
            $('.sklbl').remove();
            $('.Current_vector_details').css('top', '25%');
            $('.Current_vector_details').hide();
            if (!sketchApp.isScreenLocked) $('.mask').hide();
            hideKeyboard();
            return false;
        });
        
        var basOne = false;
        sketch.vectors.filter(function(v) { return !v.isFreeFormLineEntries}).forEach(function (sk, j) {
            if (!sk.vectorString) return;
            if (sk.newRecord && sk.code != 'BAS') {
            	var rownew = $('.divvectors tr[sketch="' + skData + '"][vector="' + sk.uid + '"]').eq(0);
            	$('select', $(rownew)).attr('disabled', 'disabled');
            	return;
            }
            var code = sk.code, addnOrSect = null, lookupCode = null;
            
            if (code == 'BAS') {
            	if(basOne) return;
            	basOne = true;
            	var rownew = $('.divvectors tr[sketch="' + skData + '"][vector="' + sk.uid + '"]').eq(0);
            	var rowSelect = $('select', $(rownew)).eq(0);
            	if ($('option', rowSelect).length > 1) {
            		$('option', rowSelect)[1].selected = true;;
            	}
            	$('select', $(rownew)).attr('disabled', 'disabled');
            	return;
            }
            
            var fLookup = Farragut_Lookup.filter(function(f) { return f.ATTRIBUTE_VALUE == code })[0];
            if (fLookup) {
            	lookupCode = fLookup.LOOKUP_VALUE_PK;
				addnOrSect = fLookup.LOOKUP_TYPE_AdditionalValue1;
			}
			if (lookupCode && addnOrSect) {
				var splitVectorString = sk.vectorString.split('[')[1].split(']')[0], rowuid = -1;
				if (splitVectorString.indexOf('{') > -1 && splitVectorString.indexOf('}') > -1)
					rowuid = splitVectorString.indexOf('#') > -1? (splitVectorString.split('#')[1].split('$')[0]): splitVectorString.split('{')[1].split('}')[0];
				else if (sk.tableRowId)
                    rowuid = sk.tableRowId;
                    
                var stable = addnOrSect == 'ADDN' ? (sketchSourceTable == 'CCV_RES_BUILDING'? 'CCV_RES_ADDITION': 'CCV_COM_ADDITION'): 'CCV_COM_SECTION_FEATURE';
				var row = $('.divvectors tr[sketch="' + skData + '"][vector="' + sk.uid + '"]').eq(0);
				if (rowuid != -1) {
					rowuid = rowuid.toString();
					if (rowuid.indexOf('{') > -1) {
						if (rowuid.indexOf('#') > -1) 
							rowuid = rowuid.split('#')[1].split('$')[0];
						else
							rowuid = rowuid.replace('{', ''). replace('}', '');
					}
						
					var rowSelect = $('select', $(row)).eq(0);
					var value = $('option[value="' + rowuid + '"]', rowSelect).val();
					if (value) {
						$(rowSelect).val(value);
						$(rowSelect).trigger('change');
						$('select', $(row)).attr('disabled', 'disabled');
					}
				}
				else {
					var sketchvar = addnOrSect == 'ADDN'? 'NON_SKETCHED_FLAG': 'IS_SKETCHED';
					var sketchVal = addnOrSect == 'ADDN'? 'N': 'Y';
                    var mainlk = addnOrSect == 'ADDN' ? 'MAIN_LOOKUP' : 'OCCUPANCY';
					var selectRecord = activeParcel[stable].filter(function (rec) { return ( rec.ParentROWUID == skData && rec.CC_Deleted != true && rec[mainlk] != "ADDNBSMTUF" && rec[sketchvar] == sketchVal && rec[mainlk] == lookupCode ) });
					if (selectRecord && selectRecord.length == 1) {
						rowuid = selectRecord[0].ROWUID;
                        var rowSelect = $('select', $(row)).eq(0);
						var value = $('option[value="' + rowuid + '"]', rowSelect).val();
						if (value) {
							$(rowSelect).val(value);
							$(rowSelect).trigger('change');
							$('select', $(row)).attr('disabled', 'disabled');
						}
					}
				}	
			}
		});
       	 
        $('.Current_vector_details #Btn_Save_vector_properties').unbind(touchClickEvent)
        $('.Current_vector_details #Btn_Save_vector_properties').bind(touchClickEvent, function () {
            var valid = true;
            $('.dynamic_prop .popupselect1').forEach(function (el) {
                var rowid = $(el).val();
                var row = $(el).parents('tr').first();
                var sketchuid = $(row).attr('sketch');
                var vectuid = $(row).attr('vector');
                var sketches = sketchApp.sketches.filter(function(sk) { return sk.uid == sketchuid })[0];
                var selectedVector = sketches.vectors.filter(function(v) { return v.uid == vectuid })[0];
                if (rowid && rowid != -1 && selectedVector)
                	selectedVector.associateRowuid = rowid;
            });
            $('.skNote').html('');
            $('.sklbl').remove();
            $('.Current_vector_details').css('top', '25%');
            $('.Current_vector_details').hide();
            if (!sketchApp.isScreenLocked) $('.mask').hide();
            hideKeyboard();
            FarragutLookupDefinition(sktData, fcallback);
            return false;
        });
    }
    
    var modifiedsids  = sketchApp.sketches.filter(function(sk) { return sk.isModified; }).map(function(s) { return s.uid });
	FarragutLookupDefinition(modifiedsids.reverse(), callback);
}

var T2AssociateVectorDefinition = function (associateVector, callback) {
    if (!sketchApp.sketches.some(function (sk) { return sk.isModified; })) { if (callback) callback(); return; }
    if (!sketchApp.sketches.some(function (sk) { return sk.vectors.length != 0; })) { if (callback) callback(); return; }

    $('.control_div div').remove();
    $('.Current_vector_details .head').html('Associate Sketch to CAMA Record');
    $('.mask').show(); $('.dynamic_prop').html('');
    $('.dynamic_prop').append('<div class="divvectors"></div>');

    let sketch = sketchApp.sketches[0], associateSource = associateVector.source, ohtml = '<select class="popupselect1" style=" width: 90%; margin: 0 auto; float: none; display: block;">',
        html = '<table class="skClass" style="width: 100%;"><tr><th style="background: #d6d6d6;height:35px;width:300px">Sketch Label </th><th style="background: #d6d6d6;height:35px;width:140px">Area</th><th style="background: #d6d6d6;height:35px;width:300px">Associated Records</th></tr>',
        currentValueOption = null;

    ohtml += '<option value="" value1="">-- select --</option>';
    associateSource.forEach((src) => {
        let tbl = src['SourceTable'], srec = activeParcel[tbl].filter((rec) => { return (rec.CC_Deleted != true && rec.IsDeleted != '1') }), fields = src['Fields'];

        srec.forEach((res) => {
            let len = fields.split('/').length, dh = '';
            fields.split('/').forEach((field, ix) => {
                if (res[field]) {
                    let f = getDataField(field, tbl), desc = res[field];
                    if (f && f.InputType == '5') {
                        desc = evalLookup(field, res[field], null, f);
                        desc = ((desc == '???') || (desc == '') || (desc == null)) ? res[field] : desc;
                    }
                    dh += ix == len - 1 ? desc + ' sf' : desc + ' - ';
                }
            });

            ohtml += '<option value="' + src.Type + res.ROWUID + '" srcTbl="' + tbl + '">' + dh + '</option>';
        });
    });

    ohtml += '</select>';
    sketch.vectors.filter((v) => { return !v.isFreeFormLineEntries }).forEach(function (sk, j) {
        let opt = '', vectType = sk.newRecord ? '[N]' : (sk.isModified) ? '[M]' : '';

        opt += '<tr sketch="' + sketch.uid + '" vector="' + sk.uid + '">';
        opt += ' <td><span style="color:green; width: 20px !important; min-width: 20px !important;float:right">' + vectType + '</span><span style="width:255px; word-break: break-all">' + sk.label + '</span></td><td><span class="assoc_area" style="width: 140px;text-align:center">' + sk.area() + '</span></td><td>' + ohtml + '</td>';
        opt += '</tr>';
        html += opt;
    });

    html += '</table>'; $('.divvectors').append(html);

    if (associateVector.note) {
        $('.skNote').html('<span><b>Note:</b> ' + associateVector.note + '</span>');
        $('.skNote').css('color', '#fb2300')
    }

    $('.Current_vector_details').show();
    $('.divvectors').css('width', 710);
    $('.skNote').show();
    $('.divvectors').css('max-height', 325);
    $('.Current_vector_details').css('top', '15%');
    $('.Current_vector_details').css('max-width', 750);

    $('.dynamic_prop .popupselect1').change(function () {
        if ($(this).val() != 0) $(this).closest('div').find('tr').find('select').find('option[value="' + $(this).val() + '"]').attr('disabled', 'disabled');
        $(this).closest('div').find('tr').find('select').find('option[value="' + currentValueOption + '"]').removeAttr('disabled');
        $(this).closest('tr').find('select').not($(this)).val($(this).val());
    });

    $('.dynamic_prop .popupselect1').mouseup(function (er) { currentValueOption = $(this).val(); });

    sketch.vectors.filter((v) => { return !v.isFreeFormLineEntries }).forEach((sk, j) => {
        if (!sk.vectorString) return;

        let splitVectorString = sk.vectorString.split('[')[1].split(']')[0], rowuid = -1, apex_id = parseInt(splitVectorString.split(/\,/g)[4]), polyRec = null, polyId = null;
        if (apex_id < 0) {
            if (splitVectorString.indexOf('{') > -1 && splitVectorString.indexOf('}') > -1)
                rowuid = splitVectorString.indexOf('#') > -1 ? (splitVectorString.split('#')[1].split('$')[0]) : splitVectorString.split('{')[1].split('}')[0];
            else if (sk.tableRowId)
                rowuid = sk.tableRowId.indexOf('#') > -1 ? (sk.tableRowId.split('#')[1].split('$')[0]) : sk.tableRowId.split('{')[1].split('}')[0];
        }
       
        polyRec = activeParcel['tAA_ApexPolygon'].filter((x) => {
            return ((x.UniqueID == (apex_id > -1 ? apex_id : -99999)) || (x.ROWUID == (apex_id < 0 && x.CC_RecordStatus == 'I' ? rowuid : -99999))) && x.IsAssociated == 1 && x.CC_Deleted != true && x.IsDeleted != '1'
        })[0];
        if (polyRec) {
            polyId = polyRec.CC_RecordStatus != 'I' ? polyRec.PolygonID : polyRec.ROWUID;
            if (polyId) {
                for (i in associateSource) {
                    let src = associateSource[i], tbl = src['SourceTable'], srec = activeParcel[tbl].filter((rec) => { return (rec.CC_Deleted != true && rec.IsDeleted != '1' && rec.PolygonID == polyId) })[0];
                    if (srec) {
                        let row = $('.divvectors tr[sketch="' + sketch.uid + '"][vector="' + sk.uid + '"]').eq(0), sel = $('select', $(row)).eq(0), vl = src.Type + srec.ROWUID;
                        $(sel).val(vl); $(sel).trigger('change');
                        break;
                    }
                }
            }
        }
    });

    $('.Current_vector_details #Btn_cancel_vector_properties').unbind(touchClickEvent)
    $('.Current_vector_details #Btn_cancel_vector_properties').bind(touchClickEvent, function () {
        $('.skNote').html('');
        $('.Current_vector_details').css('top', '25%');
        $('.Current_vector_details').hide();
        if (!sketchApp.isScreenLocked) $('.mask').hide();
        hideKeyboard();
        return false;
    });

    $('.Current_vector_details #Btn_Save_vector_properties').unbind(touchClickEvent)
    $('.Current_vector_details #Btn_Save_vector_properties').bind(touchClickEvent, function () {
        $('.dynamic_prop .popupselect1').forEach(function (el) {
            let rowid = $(el).val(), row = $(el).parents('tr').first(), sketchuid = $(row).attr('sketch'),
                vectuid = $(row).attr('vector'), sketches = sketchApp.sketches.filter((sk) => { return sk.uid == sketchuid })[0],
                selectedVector = sketches.vectors.filter((v) => { return v.uid == vectuid })[0];

            if (rowid && rowid != -1 && selectedVector) selectedVector.associateRowuid = rowid;
        });

        $('.skNote').html('');
        $('.Current_vector_details').css('top', '25%');
        $('.Current_vector_details').hide();
        if (!sketchApp.isScreenLocked) $('.mask').hide();
        hideKeyboard();
        if (callback) callback();
        return false;
    });
}

var createHtmlLabels = function (skt_fields, container) {
    skt_fields.forEach((item, i) => {
        let field = item.tblName ? getDataField(item.Name, item.tblName) : null, title = field?.Label ? field.Label : item.Title, val = item.Value, itemInput = "";
        let itemContainer = $("<div />", {
            class: "",
        });
        let itemSpan = $("<label />", {
            class: "",
            text: title + " : ",
            style: "width:150px; display:inline-block; font-weight:700;",
        });

        if (item.type != "lookup") {
            itemInput = $("<input />", {
                id: "input" + i,
                class: item.class,
                style: "width:252px; float: right !important;",

            }).on({
                input: function () {
                    this.value = this.value.replace(/[^0-9]/g, '');
                    if (this.value.length > 4) {
                        this.value = this.value.slice(0, 4);
                    }
                }
            });

            if (field) {
                itemInput[0].setAttribute('field_id', field.Id); itemInput[0].setAttribute('fieldValidate', '1');
            }
            if (val) itemInput[0].value = val;
        }
        else {
            itemInput = $("<input />", {
                id: "select" + i,
                class: item.class,
                style: "width:240px; float: right !important;"
            });
            let lkSktList = [];
            if (item.lookup == 'Addn') lkSktList = WrightAddn_Lookup;
            else if (item.lookup == 'Text') lkSktList = WrightCom_Lookup.filter((x) => { return x.TBLE == 'COMINTEXT' });
            else if (item.lookup == 'Feat') lkSktList = WrightCom_Lookup.filter((x) => { return x.TBLE == 'COMFEAT' });

            $(itemInput).empty();
            $(itemInput).lookup({ popupView: true, width: 260, searchActive: true, searchActiveAbove: 15, searchAutoFocus: false, onSave: () => { } }, []);
            $(itemInput)[0].getLookup.setData(lkSktList);

            if (val) $(itemInput).val(val);
            itemInput[0].setAttribute('Field_DT_Type', item.type);
        }

        itemInput[0].setAttribute('Field_Name', item.Name);
        $(itemContainer).append(itemSpan).append(itemInput);
        $(container).append(itemContainer);
    });

    return container;
}

var WrightLabelWindowDefinition = function (head, items, lookups, callback, editor, type) {
    $('.mask').show();
    $('.Current_vector_details .head').html(head);
    $('.dynamic_prop').html('');

    let cp = editor.currentSketch?.currentPage, skt_fields = [], apex_id = -1, rowuid = null, sk_code = '', comSelected = 'Text', prowId = null, skt_table = '',
        addn_fields = [{ Name: 'Label', Title: 'Label', class: 'addn_code', type: 'lookup', lookup: 'Addn' }],
        comText_fields = [{ Name: 'Label', Title: 'Label', class: 'comtext_code', type: 'lookup', lookup: 'Text' }, { Name: 'FLRFROM', Title: 'From', class: 'comtext_from', EnableFieldValidation: true, tblName: 'COMINTEXT' }, { Name: 'FLRTO', Title: 'To', class: 'comtext_to', EnableFieldValidation: true, tblName: 'COMINTEXT' }],
        comFeat_fields = [{ Name: 'STRUCT', Title: 'STRUCT', class: 'comtext_struct', type: 'lookup', lookup: 'Feat' }],
        edRecs = (editor.createdWrightSketchCards ? editor.createdWrightSketchCards : []),
        drec = activeParcel['CCV_DWELDAT_APEXSKETCH'].concat(edRecs.filter((x) => { return x.SoureTable == 'CCV_DWELDAT_APEXSKETCH' })),
        crec = activeParcel['CCV_COMDAT_APEXSKETCH'].concat(edRecs.filter((x) => { return x.SoureTable == 'CCV_COMDAT_APEXSKETCH' }));

    let rr = drec.filter((x) => { return x.CARD == cp && x.CC_Deleted != true })[0];
    if (rr) skt_table = 'CCV_DWELDAT_APEXSKETCH';
    else {
        rr = crec.filter((x) => { return x.CARD == cp && x.CC_Deleted != true })[0];
        skt_table = rr ? 'CCV_COMDAT_APEXSKETCH' : 'CCV_DWELDAT_APEXSKETCH';
    }
    
    if (type == 'edit' && editor.currentVector && editor.currentVector.vectorString) {
        let svs = editor.currentVector.vectorString.split('[')[1].split(']')[0]; apex_id = parseInt(svs.split(/\,/g)[4]);
        if (apex_id < 0) {
            if (svs.indexOf('{') > -1 && svs.indexOf('}') > -1)
                rowuid = svs.indexOf('#') > -1 ? (svs.split('#')[1].split('$')[0]) : svs.split('{')[1].split('}')[0];
            else if (editor.currentVector.tableRowId)
                rowuid = editor.currentVector.tableRowId.indexOf('#') > -1 ? (editor.currentVector.tableRowId.split('#')[1].split('$')[0]) : editor.currentVector.tableRowId.split('{')[1].split('}')[0];
        }
        sk_code = editor.currentVector.code;
        prowId = rr?.ROWUID;
    }

    if (skt_table == 'CCV_DWELDAT_APEXSKETCH') {
        if (sk_code != '') addn_fields[0].Value = sk_code;
        skt_fields = addn_fields;
    }
    else if (skt_table == 'CCV_COMDAT_APEXSKETCH') {
        if (type == 'edit' && sk_code != '') {
            let lk = WrightCom_Lookup.filter((x) => { return x.TBLE == 'COMINTEXT' && x.Id == sk_code })[0];
            if (lk) {
                let rec = activeParcel["COMINTEXT"].filter((x) => { return x.ParentROWUID == prowId && ((apex_id > -1 && x.AREAID == (parseInt(apex_id) + 1)) || (rowuid && x.ROWUID == rowuid)) })[0];
                if (editor.currentVector?.vectorExtraParameters?.wrightParams) {
                    comText_fields[1].Value = editor.currentVector?.vectorExtraParameters?.wrightParams['FLRFROM'];
                    comText_fields[2].Value = editor.currentVector?.vectorExtraParameters?.wrightParams['FLRTO'];
                }
                else if (rec) {
                    comText_fields[1].Value = rec['FLRFROM'];
                    comText_fields[2].Value = rec['FLRTO'];
                }
                comText_fields[0].Value = sk_code;
                skt_fields = comText_fields;
            }
            else {
                let lk = WrightCom_Lookup.filter((x) => { return x.TBLE == 'COMFEAT' && x.Id == sk_code })[0];
                if (lk) {
                    let rec = activeParcel["COMFEAT"].filter((x) => { return x.ParentROWUID == prowId && ((apex_id > -1 && x.AREAID == (parseInt(apex_id) + 1)) || (rowuid && x.ROWUID == rowuid)) })[0];
                    /*if (editor.currentVector?.vectorExtraParameters?.wrightParams) {
                        comFeat_fields[1].Value = editor.currentVector?.vectorExtraParameters?.wrightParams['MEAS1'];
                        comFeat_fields[2].Value = editor.currentVector?.vectorExtraParameters?.wrightParams['MEAS2'];
                    }
                    else if (rec) {
                        comFeat_fields[1].Value = rec['MEAS1'];
                        comFeat_fields[2].Value = rec['MEAS2'];
                    }*/
                    comFeat_fields[0].Value = sk_code;
                    skt_fields = comFeat_fields; comSelected = 'Feat';
                }
                else skt_fields = comText_fields;
            }
        }
        else skt_fields = comText_fields;
    }

    let container = $("<div />", {
        class: "lblclass",
        style: 'display:block;'
    });

    container = createHtmlLabels(skt_fields, container);
    if (type != 'edit' && skt_table != 'CCV_DWELDAT_APEXSKETCH') $('.dynamic_prop').append('<p style="width: 100%; text-align: center;">Select Sketch Type</p><div class="com-tfeat" style="display:flex; margin-left: 50px;"><input type="radio" id = "comt" name="comradios" checked style="display: inline-block; margin-right: 10px; height: 18px; width: 50px;" value = "1" ><label for="comt" style="margin-right: 50px;font-weight: bold;">Int/Ext</label><input type="radio" id="comf" name="comradios" style="display: inline-block; margin-right: 10px; height: 18px; width: 50px;" value="2"><label for="comf" style="margin-right: 50px;font-weight: bold;">Feature</label></div>');
    $('.dynamic_prop').append(container);
    $('.dynamic_prop').append('<span class="validateLabel" style="height: 12px; display: none; width:450px; color:Red; margin-left: 150px;">Please fill values.</span>');

    $('.Current_vector_details').css('width', '500px');
    $('.UnSketched, .outbdiv, .CurrentLabel, .SectNumDropDown').remove();
    $('.Current_vector_details').show();
    $('.cc-drop-pop.cc-drop-mini').css({ 'z-index': '500' });

    $('.lblclass input').change(() => {
        $('.validateLabel').hide();
    });

    $('.com-tfeat input[type="radio"]').change(function () {
        let container = $("<div />", {
            class: "lblclass",
            style: 'display:block;'
        });
        if ($(this).val() == 1) {
            container = createHtmlLabels(comText_fields, container); comSelected = 'Text';
        }
        else {
            container = createHtmlLabels(comFeat_fields, container);
            comSelected = 'Feat';
        }
        $('.dynamic_prop .lblclass').remove();
        $('.dynamic_prop .com-tfeat').after(container);
    });

    $('.Current_vector_details #Btn_cancel_vector_properties').unbind(touchClickEvent)
    $('.Current_vector_details #Btn_cancel_vector_properties').bind(touchClickEvent, function () {
        hideKeyboard();
        $('.Current_vector_details').hide();
        if (!sketchApp.isScreenLocked) $('.mask').hide();
        return false;
    });

    $('.Current_vector_details #Btn_Save_vector_properties').unbind(touchClickEvent);
    $('.Current_vector_details #Btn_Save_vector_properties').bind(touchClickEvent, function () {
        let valid = true;
        $('.lblclass input').each((i, el) => {
            if ($(el).val() == '') valid = false;
        });
        if (!valid) {
            $('.validateLabel').show(); return false;
        }

        let wrightParams = {};
        $('.lblclass input').each((i, el) => {
            let v = $(el).val();
            if ($(el).attr('Field_DT_Type') == 'lookup') {
                let lk;
                if (skt_table == 'CCV_DWELDAT_APEXSKETCH') lk = WrightAddn_Lookup.filter((x) => { return x.Id == v })[0];
                else if (comSelected == 'Text') lk = WrightCom_Lookup.filter((x) => { return x.TBLE == 'COMINTEXT' && x.Id == v })[0];
                else if (comSelected == 'Feat') lk = WrightCom_Lookup.filter((x) => { return x.TBLE == 'COMFEAT' && x.Id == v })[0];
                if (lk) {
                    items[0].Description = { Id: lk.Id, Name: (type == 'edit' ? lk.Name : lk.LName), Description: lk.DESCR };
                    items[0].NameValue = lk.LName;
                    items[0].NewLabel = lk.LName;
                }
                items[0].Value = v;
            }
            else {
                wrightParams[$(el).attr('Field_Name')] = v;
            }
        });

        if (callback) callback(items, { vectorExtraParameters: { wrightParams: wrightParams } });
        hideKeyboard();
        $('.Current_vector_details').hide();
        if (!sketchApp.isScreenLocked) $('.mask').hide();
        return false;
    });
}

var LucasLabelWindowDefinition = function (head, items, lookups, callback, editor, type) {
    var skt_table = editor.currentSketch.config.VectorSource[0].Table, skt_fields = [], stories = '', bsmt = '', isExistingSketch = false, disableStories = false, cdu = '', grade = '', ybuilt = '';
    $('.mask').show();
    $('.Current_vector_details .head').html(head);
    $('.dynamic_prop').html('');

    if (type == 'edit' && editor.currentVector && editor.currentVector.label) {
        var spLabel = editor.currentVector.label.split(']')[1];
        if (spLabel) {
            spLabel = spLabel.trim();
            stories = (spLabel.split('/')[0] && spLabel.split('/')[0] != '???' && spLabel.split('/')[0] != '---') ? spLabel.split('/')[0] : '';
            bsmt = (spLabel.split('/')[1] && spLabel.split('/')[1] != '???' && spLabel.split('/')[1] != '---') ? spLabel.split('/')[1] : '';
        }
    }
    else if (type != 'edit') {
        stories = editor.currentSketch.parentRow['STORIES'] || '';
        bsmt = editor.currentSketch.parentRow['BSMT'] || '';
    }

    if (editor?.currentSketch?.parentRow) {
        cdu = editor.currentSketch.parentRow['CDU'] || '';
        grade = editor.currentSketch.parentRow['GRADE'] || '';
        ybuilt = editor.currentSketch.parentRow['YRBLT'] || '';
    }

    if (editor.currentVector) {
        var currentVectorUid = editor.currentVector.uid;
        var currentVectorRecord = activeParcel['ADDN'].filter(function (addn) { return addn.ROWUID == currentVectorUid })[0];
        isExistingSketch = currentVectorRecord && currentVectorRecord.CC_RecordStatus != 'I' ? true : false;
    }

    skt_fields = [{ Name: 'STORIES', Title: 'STORIES', class: 'stories', value: stories, disable: isExistingSketch }, { Name: 'BSMT', Title: 'BSMT', class: 'bsmt', value: bsmt, disable: true, hidden: true }
        , { Name: 'CDU', Title: 'CDU', class: 'cdu', value: cdu, disable: isExistingSketch }, { Name: 'YRBLT', Title: 'YRBLT', class: 'ybuilt', value: ybuilt, disable: isExistingSketch }
        , { Name: 'GRADE', Title: 'GRADE', class: 'grade', value: grade, disable: isExistingSketch }];

    var container = $("<div />", {
        class: "lblclass",
        style: 'display:block;'
    });

    skt_fields.forEach(function (item, i) {
        var field = getDataField(item.Name, 'DWELDAT');
        var title = field?.Label ? field.Label : item.Title;
        var val = item.value;
        var disabled = item.disable ? 'disabled' : false;
        var itemContainer = $("<div />", {
            class: "",
        });
        var itemSpan = $("<label />", {
            class: "",
            text: title + " : ",
            style: "width:150px; display:inline-block; font-weight:700;",
        });

        let itemInput = "";
        if (item.Name == "YRBLT") {
            itemInput = $("<input />", {
                id: "input" + i,
                class: item.class,
                style: "width:242px; float: right !important;",
                maxlength: 4
            }).on({
                input: function () {
                    this.value = this.value.replace(/[^0-9]/g, '');
                    if (this.value.length > 4) {
                        this.value = this.value.slice(0, 4);
                    }
                }
            });

            itemInput[0].value = val;
        }
        else {
            itemInput = $("<select />", {
                id: "select" + i,
                class: item.class,
                style: "width:250px; float: right !important;"
            });

            let lkValues = {};

            if (field.LookupTable && field.LookupTable != '$QUERY$') lkValues = lookups[field.LookupTable];
            else lkValues = lookups[item.Name];

            lkValues = Object.keys(lkValues).map(function (x) {
                return lkValues[x];
            });

            var opt = document.createElement('option');
            opt.setAttribute('value', '');
            opt.innerHTML = '-- Select --';
            itemInput[0].appendChild(opt);
            for (var x in lkValues) {
                var lval = lkValues[x];
                var opt1 = document.createElement('option');
                opt1.setAttribute('value', lval.Id);
                opt1.innerHTML = lval.Name;
                if (lval.Id == val)
                    opt1.selected = true;
                itemInput[0].appendChild(opt1);
            }
        }

        if (disabled) itemInput[0].setAttribute('disabled', disabled);

        if (item.hidden) $(itemContainer).hide();

        $(itemContainer).append(itemSpan).append(itemInput);
        $(container).append(itemContainer);
    });

    $('.dynamic_prop').append(container);
    $('.dynamic_prop').append('<span class="validateLabel" style="height: 12px; display: none; width:450px; color:Red; margin-left: 150px;">Please fill values.</span>');
    $('.Current_vector_details').css('width', '500px');
    $('.UnSketched, .outbdiv, .CurrentLabel, .SectNumDropDown').remove();
    $('.Current_vector_details').show();

    $('.stories, .bsmt').change(function () {
        $('.validateLabel').hide();
    });

    $('.Current_vector_details #Btn_Save_vector_properties').unbind(touchClickEvent);
    $('.Current_vector_details #Btn_Save_vector_properties').bind(touchClickEvent, function () {
        var stVal = $('.stories').val(), bsmtVal = $('.bsmt').val(), currentLbl = '', lblValue = null, cduval = $('.cdu').val(), ybuiltval = $('.ybuilt').val(), gradeval = $('.grade').val();
        if ((stVal == '' || cduval == '' || ybuiltval == '' || gradeval == '') && !isExistingSketch) {
            $('.validateLabel').show();
            return false;
        }

        if (!isExistingSketch) {
            bsmtVal = bsmtVal ? bsmtVal : '---';
            lblValue = stVal + '/' + bsmtVal;
            var sthtml = $('.stories option[value="' + stVal + '"]').html();
            var currentsktlbl = editor.currentSketch.label;
            var splitcurrentsktlbl = currentsktlbl.split('/');
            splitcurrentsktlbl[1] = sthtml;
            splitcurrentsktlbl.forEach(function (lbl, i) {
                currentLbl = currentLbl + lbl + '/';
            });
            editor.currentSketch.label = currentLbl.slice(0, -1);
            $('.ccse-sketch-select option[value="' + $('.ccse-sketch-select').val() + '"]').html(editor.currentSketch.label);
        }
        if (callback) callback(items, { firstLabelValue: { lblValue: lblValue, stories: stVal, cdu: cduval, ybuilt: ybuiltval, grade: gradeval } });
        hideKeyboard();
        $('.Current_vector_details').hide();
        if (!sketchApp.isScreenLocked) $('.mask').hide();
        return false;
    });

    $('.Current_vector_details #Btn_cancel_vector_properties').unbind(touchClickEvent)
    $('.Current_vector_details #Btn_cancel_vector_properties').bind(touchClickEvent, function () {
        hideKeyboard();
        $('.Current_vector_details').hide();
        if (!sketchApp.isScreenLocked) $('.mask').hide();
        return false;
    });
}

var autoInsertRecord = function (index, callback) {
    let details = autoInsertRecords[index++];
    if (!details) {
        autoInsertRecords = [];
        callback(); return;
    }

    let thisF = details.category, ParentRowuid = details.Rowuid,
        autoInsertItems = thisF.AutoInsertProperties && JSON.parse('[' + thisF.AutoInsertProperties.replace(/}{/g, '},{') + ']');

    if (autoInsertItems) {
        var autoInsertloopCall = function () {
            let autoInsert = autoInsertItems.pop(), isAutoInsertEnabled = (autoInsert && autoInsert.table.trim() != '') ? true : false,
                sourceRecord = activeParcel[thisF.SourceTable].filter((rec) => { return rec.ROWUID == ParentRowuid })[0], yearType = thisF.YearPartitionField && fylEnabled ? (showFutureData ? 'F' : 'A'): '',
                disableInsertOnCopyRecord = disableAutoInsertFun(thisF.SourceTable, sourceRecord, autoInsert),
                existing_record = isAutoInsertEnabled ? autoInsertRecords.some((auto_insert) => { return auto_insert.category.SourceTable == autoInsert.table && auto_insert.parentRowuid == ParentRowuid }) : false;

            ParentRowuid = ParentRowuid && autoInsert.relation.toLowerCase() != 'parcel' ? ParentRowuid : '';
            if (isAutoInsertEnabled && !existing_record && !disableInsertOnCopyRecord) {
                let copyfields = autoInsert.fieldsToCopy.split(','), insertCatId = getCategoryFromSourceTable(autoInsert.table).Id,
                    parentSourceTable = autoInsert.relation.toLowerCase() == 'child' ? thisF.SourceTable : getSourceTable(thisF.parentCategoryId),
                    defaults = Object.keys(datafields).filter(function (k) { return datafields[k].SourceTable && (datafields[k].SourceTable == autoInsert.table && datafields[k].DefaultValue != null) }).map(function (k) { var df = datafields[k]; return { Id: df.Id, Name: df.Name, SourceTable: df.SourceTable, DefaultValue: df.DefaultValue } }),
                    rowuid = ccTicks(), sql = 'INSERT INTO ' + autoInsert.table + ' (ROWUID, ClientROWUID, ParentROWUID, CC_ParcelId',
                    values = [rowuid.toString(), rowuid.toString(), ParentRowuid.toString(), activeParcel.Id.toString()],
                    valueSql = '?, ?, ?, ?', default_count = defaults.length, default_counter = 0;

                if (yearType) { sql += ', CC_YearStatus'; values.push(yearType.toString()); valueSql += ', ?'; }
                defaults.forEach((def) => { sql += (', ' + def.Name); values.push(getDefaultValue(def, def.DefaultValue)); valueSql += ', ?'; });
                sql += ') VALUES (' + valueSql + ')';

                ccma.Sync.enqueueParcelChange(activeParcel.Id, rowuid, (ParentRowuid || ''), 'new', null, 'NEW', (ParentRowuid || ''), autoInsert.table + '$' + yearType, null, () => {
                    defaults.forEach((def) => {
                        ccma.Sync.enqueueParcelChange(activeParcel.Id, rowuid, (ParentRowuid || ''), null, def.Name, def.Id, null, def.DefaultValue, { updateData: true, source: def.SourceTable }, () => {
                            if (++default_counter == default_count)
                                getData(sql, values, function () { if (autoInsertItems.length > 0) autoInsertloopCall(autoInsertItems); else autoInsertRecord(index, callback); });
                        });
                    });
                });
            }
            else {
                if (autoInsertItems.length > 0) autoInsertloopCall(autoInsertItems);
                else autoInsertRecord(index, callback);
            }
        }
        autoInsertloopCall(autoInsertItems)
    }
    else autoInsertRecord(index, callback);
}

var visionIsUniqueInSiblings = function(catId, prowuid, f, val, type) {
    let tbl = getSourceTable(catId), ret = true, rv = '';
    if (tbl) {
        if (f.Name == 'SECT_NUM' && !type) {
            activeParcel[tbl].filter((x) => { return x.ParentROWUID == prowuid && x.CC_RecordStatus != 'I' }).forEach((r) => {
                if (r[f.Name] == val) ret = false;
                rv += r[f.Name] + ', ';
            });
        }
        else {
            activeParcel[tbl].filter((x) => { return x.ParentROWUID == prowuid }).forEach((r) => {
                if (r[f.Name] == val) ret = false;
                rv += r[f.Name] + ', ';
            });
        }

        if (visionPCIs.length > 0) {
            let vrec = visionPCIs.filter((x) => { return (x.SourceTable == tbl && x.ParentRecord.ParentROWUID == prowuid) });
            vrec.forEach((v) => {
                let pc = v.PCIs.filter((x) => { return x.FieldId == f.Id });
                if (pc?.length > 0) {
                    if (pc[pc.length - 1]['Value'] == val) ret = false;
                    rv += pc[pc.length - 1]['Value'] + ', ';
                }
            });
        }
    }

    return { res: !ret, rv: rv.replace(/, $/, '') };
}

var visionCappingCbxChange =  function() {
    $('.flallcheck')[0].checked = false;
    if ($('.flcheck').length == $('.flcheck:checked').length)
        $('.flallcheck')[0].checked = true;
}

var pciBulkExecution = function (pciList, bulkcallback) {
    ccma.Sync.enqueueBulkParcelChanges(pciList, function () {
        if (bulkcallback) bulkcallback();
        return;
    });
}

var getval = function (fname, parentRec) {
    let v = parentRec[fname] ? encodeURI(parentRec[fname]) : parentRec[fname];
    v = (v && v.length > 0) ? v.replaceAll('&lt;', '<').replaceAll('&gt;', '>') : v;
    return v;
}

var getdfval = function (d, parentRec) {
    let v = getDefaultValue(d, d.DefaultValue), t = v.toString().split('.'), l = t.length; t = t[l - 1];
    if (v.toString().contains('parent.') && parentRec && parentRec.parentRecord) v = parentRec.parentRecord[t];
    else if ((v.toString().contains('parent.') && parentRec && parentRec.parentRecord) || v.toString().contains('parcel.')) v = activeParcel[t];
    return v;
}

var getNonBlockedKeys = function (pcat) {
    let retKeys = [], keys = tableKeys.filter((t) => { return t.SourceTable == pcat.SourceTable }).map((tk) => { return tk.Name }), cf = getvfilterField(pcat);

    for (k in keys) {
        let f = getDataField(keys[k], pcat.SourceTable);
        if (f && f.MustIncludeInDataCopy != "true" && cf.split(',').indexOf(f.Name) == -1) retKeys.push(f);
    }
    return retKeys;
}

var getvfilterField = function (pcat) {
    let ff = pcat.FilterFields, cf = '';
    if (ff && ff.indexOf('/') > -1) {
        ff.split(',').forEach(function (a) { if (a.indexOf('/') > -1) { cf += a.split('/')[1] + ','; } else { cf += a + ','; } });
        cf = cf.slice(0, -1);
    }
    else cf = ff;

    if (pcat.YearPartitionField && fylEnabled) {
        if (cf.split(',').indexOf(pcat.YearPartitionField) == -1) cf += ', ' + pcat.YearPartitionField;
        cf += ', CC_YearStatus';
    }

    return cf;
}

var getkeyfieldPCIs = function (pcat, parentRec) {
    let dfa = [], keys = tableKeys.filter((t) => { return t.SourceTable == pcat.SourceTable }).map((tk) => { return tk.Name }),
        keyFieldsBlocked = Object.keys(datafields).map((x) => { return datafields[x] }).filter((df) => { return (df.SourceTable == pcat.SourceTable && df['UI_Settings'] && df['UI_Settings'].DoNotIncludeInDataCopy == '1') }).map((ke) => { return ke.Name });

    for (k in keys) {
        if (keyFieldsBlocked.indexOf(k) == -1) {
            let f = getDataField(keys[k], pcat.SourceTable), v = getval(f.Name, parentRec);
            if (f && v != null) dfa.push({ ParcelId: activeParcel.Id, AuxROWUID: parentRec.ROWUID, ParentROWUID: parentRec.ParentROWUID, Field: f, FieldId: f.Id, FieldName: f.Name, Value: v });
        }
    }

    return dfa;
}

var getCopyFields = function (pcat, parentRec) {
    let cf = '', dfa = [], keys = tableKeys.filter((t) => { return t.SourceTable == pcat.SourceTable }).map((tk) => { return tk.Name }),
        df = Object.keys(datafields).filter((k) => { return datafields[k].SourceTable && (datafields[k].SourceTable == pcat.SourceTable && datafields[k].DefaultValue != null) }).map((x) => { return datafields[x] }),
        keyFieldsBlocked = Object.keys(datafields).map((x) => { return datafields[x] }).filter((df) => { return (df.SourceTable == pcat.SourceTable && df['UI_Settings'] && df['UI_Settings'].DoNotIncludeInDataCopy == '1') }).map((ke) => { return ke.Name });

    cf = getvfilterField(pcat);
    cf.split(',').forEach((c) => {
        if (c?.trim()) {
            let f = getDataField(c.trim(), pcat.SourceTable), v = getval(f.Name, parentRec);
            if (f && v != null) dfa.push({ ParcelId: activeParcel.Id, AuxROWUID: parentRec.ROWUID, ParentROWUID: parentRec.ParentROWUID, Field: f, FieldId: f.Id, FieldName: f.Name, Value: v });
        }
    });

    for (k in keys) {
        if (keyFieldsBlocked.indexOf(k) == -1 ) {
            let f = getDataField(keys[k], pcat.SourceTable), v = getval(f.Name, parentRec);
            if (f && (f.MustIncludeInDataCopy == "true" || cf.split(',').indexOf(k) > -1) && v != null) dfa.push({ ParcelId: activeParcel.Id, AuxROWUID: parentRec.ROWUID, ParentROWUID: parentRec.ParentROWUID, Field: f, FieldId: f.Id, FieldName: f.Name, Value: v });
        }
    }

    df.forEach((d) => {       
        dfa.push({ ParcelId: activeParcel.Id, AuxROWUID: parentRec.ROWUID, parentROWUID: parentRec.ParentROWUID, Field: d, FieldName: d.Name, FieldId: d.Id, Value: getdfval(d, parentRec), CreatePCI: true });
    });

    if (pcat.IncludeParentFieldsOnInsert) {
        pcat.IncludeParentFieldsOnInsert.split(',').forEach(function (p) {
            let f = getDataField(p.trim(), pcat.SourceTable), ps = parentRec.parentRecord ? parentRec.parentRecord : activeParcel;
            if (f) dfa.push({ ParcelId: activeParcel.Id, AuxROWUID: parentRec.ROWUID, ParentROWUID: parentRec.ParentROWUID, Field: f, FieldId: f.Id, FieldName: f.Name, Value: ps[f.Name], CreatePCI: true });
        })
    }
    return dfa;
}

var visionCappingRecordCreation = function (callback) {
    if (visionPCIs.length == 0) {
        autoInsertRecord(0, () => {
            pciBulkExecution(_PCIBatchArray, () => {
                getParcel(activeParcel.Id, () => {
                    _PCIBatchArray = []; if (callback) callback();
                });
            });
        });
        return;
    }

    let copyCapRecord = function (tbl, c, e, nrowuid, copyback, associatetoexsection) {
        if (associatetoexsection) {
            if (copyback) copyback({ ROWUID: nrowuid, ParentROWUID: e.ParentROWUID });
            return;
        }

        let copy = {}, keyFieldsBlocked = Object.keys(datafields).map((x) => { return datafields[x] }).filter((df) => { return (df.SourceTable == tbl && df['UI_Settings'] && df['UI_Settings'].DoNotIncludeInDataCopy == '1') }).map((ke) => { return ke.Name }),
            sqlInsert = 'INSERT INTO ' + tbl + ' (', sqlValues = ') VALUES (', cn = 0,
            keys = tableKeys.filter((t) => { return t.SourceTable == tbl }).map((tk) => { return tk.Name });

        for (key in e) {
            if (key != 'Original' && key != 'ParentRecord' && key != 'CC_FIndex' && (!e[key] || typeof e[key] != 'object') && typeof e[key] != 'function' && (keyFieldsBlocked.indexOf(key) == -1)) copy[key] = e[key];
        }
        copy.ROWUID = nrowuid; copy.ClientROWUID = copy.ROWUID; copy["CC_RecordStatus"] = "I"; activeParcel[tbl].push(copy); activeParcel.Original[tbl].push(copy);

        for (k in keys) {
            let td = getDataField(keys[k], tbl);
            if (td) {
                if (td.MustIncludeInDataCopy != "true" && (!td.DefaultValue || td.DefaultValue.toUpperCase() != "FUTUREYEARSTATUS")) copy[keys[k]] = null;
            }
        }

        for (key in copy) {
            if (cn > 0) {
                sqlInsert += ", "; sqlValues += ", ";
            }

            sqlInsert += "[" + key + "]";
            if (copy[key] == null) {
                let v = 'null', f = getDataField(key, tbl);
                if (f?.DefaultValue && (ps.length == 0 || ps.filter((x) => { return x.CreatePCI }).length == 0)) {
                    v = getdfval(f, e);
                    _PCIBatchArray.push({ parcelId: activeParcel.Id, auxRowId: copy.ROWUID, parentAuxRowId: copy.ParentROWUID, action: 'E', field: f, oldValue: null, newValue: v });
                    if (v !== null) v = "'" + v.replace(/'/g, "''") + "'";
                }
                sqlValues += v;
            }
            else {
                if (copy[key] && copy[key].toString().search("'") > -1) copy[key] = copy[key].replace(/'/g, "''");
                sqlValues += copy[key] ? "'" + copy[key] + "'" : 'null';
            }
            cn++;
        }

        sqlInsert += sqlValues + ')';

        getData(sqlInsert, [], () => {
            autoInsertRecords.push({ category: c, Rowuid: copy.ROWUID, parentRowuid: copy.ParentROWUID });
            let nd = copy.ROWUID + '$' + copy.ParentROWUID, ss = activeParcel.Id + '$' + e.ROWUID, stl = tbl + (copy.CC_YearStatus?.trim() ? ('$' + copy.CC_YearStatus.trim()) : '');
            ccma.Sync.enqueueParcelChange(activeParcel.Id, nd, '', 'vcp', 'VCP', 'VCP', ss, stl, {}, () => {
                if (copyback) copyback(copy);
            });
        });
    }

    let vsPci = visionPCIs.shift(), tbl = vsPci.SourceTable, rec = vsPci.ParentRecord, ps = vsPci.PCIs, cat = getCategoryFromSourceTable(tbl), ntype = vsPci.type == 'New' ? 'A' : 'D', asstoexsection = vsPci.AssociateToOld? true: false;
    copyCapRecord(tbl, cat, rec, vsPci.newRowuid, (nrec) => {
        ps.forEach((p) => { _PCIBatchArray.push({ parcelId: p.ParcelId, auxRowId: nrec.ROWUID, parentAuxRowId: nrec.ParentROWUID, action: 'E', field: p.Field, oldValue: null, newValue: p.Value }); });
        if (getDataField('CC_USER_SELECTED', tbl)) _PCIBatchArray.push({ parcelId: activeParcel.Id, auxRowId: nrec.ROWUID, parentAuxRowId: nrec.ParentROWUID, action: 'E', field: getDataField('CC_USER_SELECTED', tbl), oldValue: null, newValue: '1' });
        if (getDataField('CC_IS_DELETED_SECTION', tbl)) _PCIBatchArray.push({ parcelId: activeParcel.Id, auxRowId: nrec.ROWUID, parentAuxRowId: nrec.ParentROWUID, action: 'E', field: getDataField('CC_IS_DELETED_SECTION', tbl), oldValue: null, newValue: ntype });
        if (getDataField('CNS_RECORD_STATUS', tbl)) _PCIBatchArray.push({ parcelId: activeParcel.Id, auxRowId: nrec.ROWUID, parentAuxRowId: nrec.ParentROWUID, action: 'E', field: getDataField('CNS_RECORD_STATUS', tbl), oldValue: null, newValue: ntype });
        visionCappingRecordCreation(callback);
    }, asstoexsection);
}

var visionDeleteWindowDefinition = function (editor, dv, type, vcallback) {
    let resTable = 'CCV_CONSTRSECTION_RES', comTable = 'CCV_CONSTRSECTION_COM', condoTable = 'CCV_CONSTRSECTION_CDM', condoUnitTable = 'CCV_CONSTRSECTION_CDU',
        bldg = activeParcel['CCV_BLDG_CONSTR'].filter((s) => { return s.ROWUID == editor.currentSketch.sid && s.CC_Deleted != true; })[0], parentTable = null, areaTable = null, parentRec = null,
        sn = dv.sectionNum;

    if (sn) sn = (sn.contains('#') ? sn.match(/#(.*)\$/)[1] : (sn.contains('{') ? sn.match(/{(.*)}/)[1]: sn));

    if (bldg?.[resTable]?.length > 0) {
        parentTable = resTable; areaTable = 'CCV_SUBAREA_RES';
        parentRec = bldg[parentTable].filter((s) => { return (s.SECT_NUM == sn || s.ROWUID == sn) })[0];
    }
    else if (bldg?.[comTable]?.length > 0) {
        parentTable = comTable; areaTable = 'CCV_SUBAREA_COM';
        parentRec = bldg[parentTable].filter((s) => { return (s.SECT_NUM == sn || s.ROWUID == sn) })[0];
    }
    else if (bldg?.[condoTable]?.length > 0) {
        parentTable = condoTable; areaTable = 'CCV_SUBAREA_CDM';
        parentRec = bldg[parentTable].filter((s) => { return (s.SECT_NUM == sn || s.ROWUID == sn) })[0];
    }
    else if (bldg?.[condoUnitTable]?.length > 0) {
        parentTable = condoUnitTable; areaTable = 'CCV_SUBAREA_CDU';
        parentRec = bldg[parentTable].filter((s) => { return (s.SECT_NUM == sn || s.ROWUID == sn) })[0];
    }

    if (parentTable && areaTable && parentRec) {
        let pcat = getCategoryFromSourceTable(parentTable), cbList = [];
        $('.Current_vector_details .head').html('Capping Form - Current Section');
        $('.UnSketched, .outbdiv, .CurrentLabel, .SectNumDropDown').remove();

        let createCapDiv = function (backCall) {
            getData('SELECT * FROM Field WHERE CategoryId = ? AND DoNotShow = "false" AND DoNotShowOnMA = "false" ORDER BY Serial * 1', [parseInt(pcat.Id).toString()], function (fields) {
                $('.mask').show();
                let capDiv = $("<div />", { class: "capClass", style: 'display: block; width: 100%;' }), capHead = $("<span/>", { class: 'capHead', style: 'width: 100%; font-size: 18px; font-weight: bold; margin-bottom: 10px;', text: '' }),
                    capHeadMsg = type == 'New' ? 'Select which field(s) should be copied to the Capped/Created Section for:': 'Select which field(s) should be copied to the Capped / Deleted Section for:';
                capSect = '<span class="capSect" style="width: 100%; margin-bottom: 10px;"><label style="float: left; font-style: italic; font-size: 15px;">' + capHeadMsg + ' <b>' + dv.name + '</b></label><input type="checkBox" class="flallcheck" style="width: 20px; height: 20px; float: right; margin-right: 29px; "></span>',
                    capBody = $("<div />", { class: "capBody", style: 'display: block; width: 99%; padding: 0px; margin-top: 15px; border: 1px solid #dfdcdc; box-shadow: 0px 0px 2px #ccc; border-radius: 5px;' }),
                    rix = activeParcel[parentTable].indexOf(activeParcel[parentTable].filter((x) => { return x.ROWUID == parentRec.ROWUID })[0]),
                    uf = fields.filter((x) => { return x.IsUniqueInSiblings == 'true' });

                fields.forEach((f) => {
                    let v = parentRec[f.Name] ? encodeURI(parentRec[f.Name]) : parentRec[f.Name];
                    v = (v && v.length > 0) ? v.replaceAll('&lt;', '<').replaceAll('&gt;', '>') : v;
                    fieldSet = getFieldSet(f, { category: pcat, trackChange: false, visionCapping: true, visionDisableCheckbox: backCall, visionCapValue: v, source: parentRec, visionRecIndex: (rix >= 0 ? rix : null) });
                    $('.legend', $(fieldSet)).css('font-weight', 'bold'); $('.newvalue', $(fieldSet)).val(v); $(capBody).append(fieldSet);
                });

                appendMultiple(capDiv, [capHead, { el: capSect, $append: true }, capBody]);

                $('.dynamic_prop').html(''); $('.dynamic_prop').append(capDiv);
                $('.Current_vector_details .head').html('Capping Form - Current Section');
                $('#Btn_Save_vector_properties').html('Next '); $('#Btn_cancel_vector_properties').html('Cancel ');
                $('.Current_vector_details').css({ 'max-width': '', 'width': '750px', 'top': '18%' }); $('.skNote').hide();
                $('.skNote').css({ 'text-align': 'center', 'color': 'red', 'font-weight': 'bold' });
                if (type == 'New') $('#Btn_cancel_vector_properties').hide();
                $('.Current_vector_details').show();

                $('.flallcheck').unbind('change');
                $('.flallcheck').bind('change', function () {
                    if ($(this)[0].checked) {
                        $('.flcheck').forEach((fc) => { fc.checked = true; });
                    }
                    else {
                        $('.flcheck').forEach((fc) => { fc.checked = false; });
                    }
                });

                $('#Btn_Save_vector_properties').unbind(touchClickEvent);
                $('#Btn_Save_vector_properties').bind(touchClickEvent, () => {
                    let fc = [], dfa = [];

                    $('.capBody').addClass('vsshimmer');
                    $('.Current_vector_details .head').html('Capping Form - New Section for Capping'); $('.capSect, .flcheck').hide();
                    $('.cc-drop-pop.cc-drop-mini').css({ 'z-index': '100' });
                    $('#Btn_Save_vector_properties, #Btn_cancel_vector_properties').hide();
                    $('#Btn_Save_vector_properties').html('Back '); $('.capBody').scrollTop(0);

                    if (!$('.flallcheck')[0].checked) {
                        $('.capBody .newvalue').forEach((el) => { if ($(el).val != '') $(el).val(''); });
                        dfa = getCopyFields(pcat, parentRec);
                        cbList.push(-1);
                        $('.capBody .flcheck:checked').parent().forEach((fs) => {
                            let fId = $(fs).attr('field-id'), f = datafields[fId]; cbList.push(fId);
                            if (f) dfa.push({ ParcelId: activeParcel.Id, AuxROWUID: parentRec.ROWUID, ParentROWUID: parentRec.ParentROWUID, Field: f, FieldId: f.Id, FieldName: f.Name, Value: getval(f.Name, parentRec), CreatePCI: true });
                        });

                        $('.capBody .reqd').parent().forEach((fs) => {
                            let fId = $(fs).attr('field-id'), f = datafields[fId], v = getval(f.Name, parentRec);
                            if (f && v != null) dfa.push({ ParcelId: activeParcel.Id, AuxROWUID: parentRec.ROWUID, ParentROWUID: parentRec.ParentROWUID, Field: f, FieldId: f.Id, FieldName: f.Name, Value: v, CreatePCI: true });
                        });

                        dfa.forEach((df) => {
                            $('.capBody fieldset[field-id= "' + df.FieldId + '"] .newvalue').val(df.Value);
                        });
                    }
                    else {
                        let nbKeys = getNonBlockedKeys(pcat);
                        nbKeys.forEach((nk) => {
                            $('.capBody fieldset[field-id= "' + nk.Id + '"] .newvalue').val('');
                        });

                        //dfa = getkeyfieldPCIs(pcat, parentRec);

                        $('.capBody .reqd').parent().forEach((fs) => {
                            let fId = $(fs).attr('field-id'), f = datafields[fId], v = getval(f.Name, parentRec);
                            if (f && v != null) dfa.push({ ParcelId: activeParcel.Id, AuxROWUID: parentRec.ROWUID, ParentROWUID: parentRec.ParentROWUID, Field: f, FieldId: f.Id, FieldName: f.Name, Value: v });
                        });

                        dfa.forEach((df) => {
                            $('.capBody fieldset[field-id= "' + df.FieldId + '"] .newvalue').val(df.Value);
                        });
                    }

                    let sve = type == 'New' ? 'A': 'D';
                    $('.capBody fieldset[field-name="CNS_RECORD_STATUS"] .newvalue, .capBody fieldset[field-name="CC_IS_DELETED_SECTION"] .newvalue').val(sve);
                    $('.capBody fieldset[field-name= "CAP_CODE1"] .newvalue, .capBody fieldset[field-name= "CAP_CODE2"] .newvalue').val('');
                    $('.capBody fieldset[field-name="SECT_NUM"] .newvalue').val('');

                    if (type == 'New') {
                        $('.capBody fieldset[field-name= "CAP_CODE2"]').hide();
                        $('.capBody fieldset[field-name= "CC_IS_DELETED_SECTION"]').hide();
                        if (!$('.capBody fieldset[field-name= "CAP_CODE1"]').children('.reqd')[0]) $('.capBody fieldset[field-name= "CAP_CODE1"]').children('.legend').after('<span class="reqd">*</span>')
                    }
                    else {
                        $('.capBody fieldset[field-name= "CAP_CODE1"]').hide();
                        if (!$('.capBody fieldset[field-name= "CAP_CODE2"]').children('.reqd')[0]) $('.capBody fieldset[field-name= "CAP_CODE2"]').children('.legend').after('<span class="reqd">*</span>')
                    }

                    $('.capBody .newvalue').unbind('change');
                    $('.capBody .newvalue').bind('change', function () {
                        $('.skNote').hide();
                        let v = $(this).val(), fId = $(this).parent().attr('field-id'), f = datafields[fId];
                        if (f) {
                            if (f.IsUniqueInSiblings == 'true') {
                                res = visionIsUniqueInSiblings(pcat.Id, parentRec.ParentROWUID, f, v, type);
                                if (res?.['res']) {
                                    let msg = 'You need to input a unique value in ' + f.Label + ' field. </br> Current Field values : ' + res['rv'];
                                    $('.skNote').html(msg); $('.skNote').show();
                                    $(this).val(''); return;
                                }
                            }

                            let rId = parentRec.ROWUID, pId = activeParcel.Id, dx = -1;
                            for (x in fc) {
                                let ce = fc[x];
                                if (ce.ParcelId == pId && ce.AuxROWUID == rId && ce.Field.Id == fId) dx = x;
                            }
                            if (dx > -1) fc.splice(dx, 1);
                            fc.push({ ParcelId: pId, AuxROWUID: rId, Field: f, Value: v, ParentROWUID: parentRec.ParentROWUID, FieldId: f.Id, FieldName: f.Name });
                        }
                    });

                    $('#Btn_Save_vector_properties').unbind(touchClickEvent);
                    $('#Btn_Save_vector_properties').bind(touchClickEvent, () => {
                        $('.dynamic_prop').addClass('vsshimmer');
                        createCapDiv(true);
                        $('#Btn_Save_vector_properties, #Btn_cancel_vector_properties').hide();

                        setTimeout(function () {
                            if (cbList.length == 0) {
                                $('.flallcheck')[0].checked = true;
                                $('.flcheck').forEach((fc) => { fc.checked = true; });
                            }
                            else {
                                cbList.forEach((cl) => {
                                    if (cl != -1) $('.capBody fieldset[field-id= "' + cl + '"] .flcheck')[0].checked = true;
                                })
                            }
                            cbList = [];
                            $('.flallcheck, .flcheck').removeAttr('disabled');
                            $('#Btn_Save_vector_properties').html('Next '); $('#Btn_cancel_vector_properties').html('Cancel ');
                            $('#Btn_Save_vector_properties, #Btn_cancel_vector_properties').show();
                            $('.dynamic_prop').removeClass('vsshimmer');
                        }, 5000);
                        return false;
                    });

                    $('#Btn_cancel_vector_properties').unbind(touchClickEvent);
                    $('#Btn_cancel_vector_properties').bind(touchClickEvent, () => {
                        let cap2 = $('.capBody fieldset[field-name="CAP_CODE2"] .newvalue').val(), cap1 = $('.capBody fieldset[field-name= "CAP_CODE1"] .newvalue').val(), msg = '';

                        /*if (cap2 == '' && cap1 == '') {
                            let c1 = getDataField('CAP_CODE2', parentTable), c2 = getDataField('CAP_CODE1', parentTable);
                            msg = 'Select the ' + c1.Label + ' when deleting, and ' + c2.Label +' when creating/adding new.';
                        }
                        else if (cap2 != '' && cap1 != '') msg = 'You can only select either one of the Cap Change fields.';*/

                        $('.capBody .reqd').siblings('.newvalue').forEach((el) => {
                            if ($(el).val() == '') msg = 'Please complete all the required fields to proceed.';
                        });

                        if (msg) { $('.skNote').html(msg); $('.skNote').show(); return; }

                        for (x in uf) {
                            let val = $('.capBody fieldset[field-id= "' + uf[x].Id + '"] .newvalue').val(),
                                res = visionIsUniqueInSiblings(pcat.Id, parentRec.ParentROWUID, uf[x], val, type);

                            if (res?.['res']) {
                                msg = 'You need to input a unique value in ' + uf[x].Label + ' field. </br> Current Field values : ' + res['rv'];
                                break;
                            }
                        }

                        if (msg) { $('.skNote').html(msg); $('.skNote').show(); return; }

                        let pa = dfa, newRowuid = ccTicks(); pa = pa.concat(fc);
                        let clientId = (type == 'New') ? newRowuid : null;
                        let vpPCI = { SourceTable: parentTable, ParentRecord: parentRec, PCIs: pa, newRowuid: newRowuid, type: type, clientId: clientId };
                        
                        let sns = pa.filter((x) => { return x.FieldName == 'SECT_NUM' }),
                            sn = sns?.length > 0 ? sns[sns.length - 1].Value : '&%' + parentTable + '{$' + parentTable + '#' + newRowuid + '$}&%';
                        if (sn) {
                            let isrecex = activeParcel[parentTable].filter((x) => { return x.ParentROWUID == parentRec.ParentROWUID && x['SECT_NUM'] == sn })[0];
                            if (isrecex) {
                                vpPCI.PCIs = fc;
                                vpPCI.AssociateToOld = true;
                                vpPCI.newRowuid = isrecex.ROWUID;
                            }
                        }

                        dv.sectionNum = sn; dv.visionDeleted = dv.isModified = true;
                        visionPCIs.push(vpPCI);
                        $('.cc-drop-pop.cc-drop-mini').css({ 'z-index': '' }); $('.skNote').css({ 'text-align': '', 'color': '' });
                        $('#Btn_cancel_vector_properties').html('Cancel '); $('#Btn_Save_vector_properties').html('Save ');
                        $('.Current_vector_details').css({ 'top': '25%' });
                        if (!sketchApp.isScreenLocked) $('.mask').hide();
                        $('.Current_vector_details').hide(); hideKeyboard();
                        if (vcallback) vcallback({ sectNum: sn, visionClientId: clientId });
                        return false;
                    });

                    setTimeout(function () {
                        $('.capBody .newvalue').removeAttr('disabled');
                        $('.capBody fieldset[field-name="CNS_RECORD_STATUS"] .newvalue, .capBody fieldset[field-name="CC_IS_DELETED_SECTION"] .newvalue').attr('disabled', 'disabled');
                        $('.capBody fieldset[readonlyfieldset= "1"] .newvalue').attr('disabled', 'disabled');
                        $('#Btn_cancel_vector_properties').html('Save ');
                        $('#Btn_cancel_vector_properties, #Btn_Save_vector_properties').show();
                        $('.capBody').removeClass('vsshimmer');
                    }, 4000);

                    return false;
                });

                $('#Btn_cancel_vector_properties').unbind(touchClickEvent);
                $('#Btn_cancel_vector_properties').bind(touchClickEvent, () => {
                    editor.mode = CAMACloud.Sketching.MODE_EDIT;
                    $('.cc-drop-pop.cc-drop-mini').css({ 'z-index': '' }); $('.skNote').css({ 'text-align': '', 'color': '' });
                    $('#Btn_cancel_vector_properties').html('Cancel '); $('#Btn_Save_vector_properties').html('Save ');
                    $('.Current_vector_details').css({ 'top': '25%' });
                    if (!sketchApp.isScreenLocked) $('.mask').hide();
                    $('.Current_vector_details').hide(); hideKeyboard();
                    return false;
                });

                if (backCall) $('.flallcheck').attr('disabled', 'disabled');
            });
        }
        createCapDiv();
    }
    else {
        if (vcallback) vcallback();
        return;
    }
}
var IASW_USLabelWindowDefinition = function (head, items, lookups, callback, editor, type) {
    var skt_table = editor.currentSketch.config.VectorSource[0].Table, skt_fields = [], stories = '', bsmt = '', isExistingSketch = false, disableStories = false, cdu = '', grade = '', ybuilt = '';
    $('.mask').show();
    $('.Current_vector_details .head').html(head);
    $('.dynamic_prop').html('');

    if (type == 'edit' && editor.currentVector && editor.currentVector.label) {
        var spLabel = editor.currentVector.label.split(']')[1];
        if (spLabel) {
            spLabel = spLabel.trim();
            stories = (spLabel.split('/')[0] && spLabel.split('/')[0] != '???' && spLabel.split('/')[0] != '---') ? spLabel.split('/')[0] : '';
            bsmt = (spLabel.split('/')[1] && spLabel.split('/')[1] != '???' && spLabel.split('/')[1] != '---') ? spLabel.split('/')[1] : '';
        }
    }
    else if (type != 'edit') {
        stories = editor.currentSketch.parentRow['STORIES'] || '';
        bsmt = editor.currentSketch.parentRow['BSMT'] || '';
    }

    if (editor?.currentSketch?.parentRow) {
        cdu = editor.currentSketch.parentRow['CDU'] || '';
        grade = editor.currentSketch.parentRow['GRADE'] || '';
        ybuilt = editor.currentSketch.parentRow['YRBLT'] || '';
    }

    if (editor.currentVector) {
        var currentVectorUid = editor.currentVector.uid;
        var currentVectorRecord = activeParcel['ADDN'].filter(function (addn) { return addn.ROWUID == currentVectorUid })[0];
        isExistingSketch = currentVectorRecord && currentVectorRecord.CC_RecordStatus != 'I' ? true : false;
    }

    skt_fields = [{ Name: 'STORIES', Title: 'STORIES', class: 'stories', value: stories, disable: isExistingSketch }, { Name: 'BSMT', Title: 'BSMT', class: 'bsmt', value: bsmt, disable: true, hidden: true }
        , { Name: 'CDU', Title: 'CDU', class: 'cdu', value: cdu, disable: isExistingSketch }, { Name: 'YRBLT', Title: 'YRBLT', class: 'ybuilt', value: ybuilt, disable: isExistingSketch }
        , { Name: 'GRADE', Title: 'GRADE', class: 'grade', value: grade, disable: isExistingSketch }];

    var container = $("<div />", {
        class: "lblclass",
        style: 'display:block;'
    });

    skt_fields.forEach(function (item, i) {
        var field = getDataField(item.Name, 'DWELDAT');
        var title = field?.Label ? field.Label : item.Title;
        var val = item.value;
        var disabled = item.disable ? 'disabled' : false;
        var itemContainer = $("<div />", {
            class: "",
        });
        var itemSpan = $("<label />", {
            class: "",
            text: title + " : ",
            style: "width:150px; display:inline-block; font-weight:700;",
        });

        let itemInput = "";
        if (item.Name == "YRBLT") {
            itemInput = $("<input />", {
                id: "input" + i,
                class: item.class,
                style: "width:242px; float: right !important;",
                maxlength: 4
            }).on({
                input: function () {
                    this.value = this.value.replace(/[^0-9]/g, '');
                    if (this.value.length > 4) {
                        this.value = this.value.slice(0, 4);
                    }
                }
            });

            itemInput[0].value = val;
        }
        else {
            itemInput = $("<select />", {
                id: "select" + i,
                class: item.class,
                style: "width:250px; float: right !important;"
            });

            let lkValues = {};

            if (field.LookupTable && field.LookupTable != '$QUERY$') lkValues = lookups[field.LookupTable];
            else lkValues = lookups[item.Name];

            lkValues = Object.keys(lkValues).map(function (x) {
                return lkValues[x];
            });

            var opt = document.createElement('option');
            opt.setAttribute('value', '');
            opt.innerHTML = '-- Select --';
            itemInput[0].appendChild(opt);
            for (var x in lkValues) {
                var lval = lkValues[x];
                var opt1 = document.createElement('option');
                opt1.setAttribute('value', lval.Id);
                opt1.innerHTML = lval.Name;
                if (lval.Id == val)
                    opt1.selected = true;
                itemInput[0].appendChild(opt1);
            }
        }

        if (disabled) itemInput[0].setAttribute('disabled', disabled);

        if (item.hidden) $(itemContainer).hide();

        $(itemContainer).append(itemSpan).append(itemInput);
        $(container).append(itemContainer);
    });

    $('.dynamic_prop').append(container);
    $('.dynamic_prop').append('<span class="validateLabel" style="height: 12px; display: none; width:450px; color:Red; margin-left: 150px;">Please fill values.</span>');
    $('.Current_vector_details').css('width', '500px');
    $('.UnSketched, .outbdiv, .CurrentLabel, .SectNumDropDown').remove();
    $('.Current_vector_details').show();

    $('.stories, .bsmt').change(function () {
        $('.validateLabel').hide();
    });

    $('.Current_vector_details #Btn_Save_vector_properties').unbind(touchClickEvent);
    $('.Current_vector_details #Btn_Save_vector_properties').bind(touchClickEvent, function () {
        var stVal = $('.stories').val(), bsmtVal = $('.bsmt').val(), currentLbl = '', lblValue = null, cduval = $('.cdu').val(), ybuiltval = $('.ybuilt').val(), gradeval = $('.grade').val();
        if ((stVal == '' || cduval == '' || ybuiltval == '' || gradeval == '') && !isExistingSketch) {
            $('.validateLabel').show();
            return false;
        }

        if (!isExistingSketch) {
            bsmtVal = bsmtVal ? bsmtVal : '---';
            lblValue = stVal + '/' + bsmtVal;
            var sthtml = $('.stories option[value="' + stVal + '"]').html();
            var currentsktlbl = editor.currentSketch.label;
            var splitcurrentsktlbl = currentsktlbl.split('/');
            splitcurrentsktlbl[1] = sthtml;
            splitcurrentsktlbl.forEach(function (lbl, i) {
                currentLbl = currentLbl + lbl + '/';
            });
            editor.currentSketch.label = currentLbl.slice(0, -1);
            $('.ccse-sketch-select option[value="' + $('.ccse-sketch-select').val() + '"]').html(editor.currentSketch.label);
        }
        if (callback) callback(items, { firstLabelValue: { lblValue: lblValue, stories: stVal, cdu: cduval, ybuilt: ybuiltval, grade: gradeval } });
        hideKeyboard();
        $('.Current_vector_details').hide();
        if (!sketchApp.isScreenLocked) $('.mask').hide();
        return false;
    });

    $('.Current_vector_details #Btn_cancel_vector_properties').unbind(touchClickEvent)
    $('.Current_vector_details #Btn_cancel_vector_properties').bind(touchClickEvent, function () {
        hideKeyboard();
        $('.Current_vector_details').hide();
        if (!sketchApp.isScreenLocked) $('.mask').hide();
        return false;
    });
}