﻿var FTC_Detail_Lookup = [];
var FTC_AddOns_Lookup = [];
var FTC_Floor_Lookup = [];
var Lafayette_Lookup = [];
var bldgSqft = [];
var _PCIBatchArray = []; 
	
var update_data = function(field, value, sourceRecord, callback) {
	if (field && sourceRecord[field.Name] != value) {
		var oldVal = (sourceRecord.Original && sourceRecord.Original[field.Name]) ? sourceRecord.Original[field.Name]: null;
		ccma.Sync.enqueueParcelChange(activeParcel.Id, sourceRecord.ROWUID, sourceRecord.ParentROWUID, 'E', field.Name, field.Id, oldVal, value, { source: field.SourceTable, sourceField: field.Name, updateData: true }, function() { 
			sourceRecord[field.Name] = value; 
			if (callback) callback(true); 
		});
	}
	else if (callback) callback(false); 
}

var _update_PCIChanges = function(field, value, sourceRecord, callback) {
	if (field && sourceRecord[field.Name] != value) {
		var oldVal = (sourceRecord.Original && sourceRecord.Original[field.Name]) ? sourceRecord.Original[field.Name]: null;
		_PCIBatchArray.push({parcelId: activeParcel.Id, auxRowId: sourceRecord.ROWUID, parentAuxRowId: sourceRecord.ParentROWUID, action: 'E', field: field, oldValue: oldVal, newValue: value});
		sourceRecord[field.Name] = value; 
		if (callback) callback(true); 
	}
	else if (callback) callback(false); 
}

function ApexCartSketchAfterSave(sketchData, callback) {
    if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }
    var fprocessor = function (sourceTable, fields, callback) {
        var sketchRowIds = [];
        sketchData.forEach(function (sk) {
            sketchRowIds.push(sketchData.pop().rowid)
        })
        var vectorIndex = 0, line;
        var field = [];
        for (f in fields)
            field.push(getDataField(fields[f], sourceTable))
        var cat = getCategoryFromSourceTable(sourceTable);
        var processor = function (sketchRowId, fields, callback) {
            line = 0;
            var sk = sketchRowId.pop();
            var parentSourceData = activeParcel.CAMBL.filter(function (a) { return a.ROWUID == sk })[0];
            var vectors = sketchApp.sketches.filter(function (s) { return s.sid == sk })[0].vectors;
            vectors = vectors.filter(function(x) { return !(x.isFreeFormLineEntries) });
            var area = 0, label = '', year = '';
            var recordValues = [], pushed = false;
            for (v = 0; vectors[v] != null; v++) {
                area = Math.round(vectors[v].area());
                label = (vectors[v].name).toString().trim();
                year = label.slice(-4)
                if (!isNaN(year))
                    label = label.replace(year, '')
                pushed = false;
                if (sourceTable == 'CAMBS')
                    for (var r in recordValues) {
                        if (recordValues[r].Label == label && recordValues[r].Year == year) {
                            recordValues[r].Area += area;
                            pushed = true;
                        }
                    }
                if (!pushed)
                    recordValues.push({ Label: label, Year: year, Area: area });
            }
            if (sourceTable == 'CAMBS') recordValues = recordValues.reverse();
            var insertFunction = function (callback) {
                insertNewAuxRecord(null, parentSourceData, cat.Id, null, null, function (rowid) {
                    var recordData = activeParcel[sourceTable].filter(function (s) { return s.ROWUID == rowid })[0];
                    var travValue = '';
                    if (sourceTable == 'CAMBT')
                        for (v = 0; recordValues[vectorIndex + v] != null && v < 2; v++) {
                            travValue += recordValues[vectorIndex + v].Label + recordValues[vectorIndex + v].Year + '=' + Math.round(recordValues[vectorIndex + v].Area).toString() + '$ ';
                        }
                    if ((travValue != '' && sourceTable == 'CAMBT') || (sourceTable == 'CAMBS' && recordValues.length > 0)) {
                        travValue = travValue.slice(0, -1);
                        var recValues = {};
                        if (sourceTable == 'CAMBS') recValues = recordValues.pop();
                        if (vectorIndex + 2 >= vectors.length) travValue = travValue + '.';
                        var field1Value = (sourceTable == 'CAMBT' ? travValue : recValues.Label);
                        var field2Value = (sourceTable == 'CAMBT' ? parentSourceData.ITEM : recValues.Year);
                        var field3Value = (sourceTable == 'CAMBT' ? ++line : recValues.Area);
						ccma.Sync.enqueueParcelChange(activeParcel.Id, rowid, sk, 'E', field[0].Name, field[0].Id, null, field1Value, { source: sourceTable, sourceField: field[0].Name, updateData: true }, function () {
							ccma.Sync.enqueueParcelChange(activeParcel.Id, rowid, sk, 'E', field[1].Name, field[1].Id, null, field2Value, { source: sourceTable, sourceField: field[1].Name, updateData: true }, function () {
								ccma.Sync.enqueueParcelChange(activeParcel.Id, rowid, sk, 'E', field[2].Name, field[2].Id, null, field3Value, { source: sourceTable, sourceField: field[2].Name, updateData: true }, function () {
									recordData[field[0].Name] = field1Value;
									recordData[field[1].Name] = field2Value;
									recordData[field[2].Name] = field3Value;
                                    vectorIndex += 2;
                                    if ((vectorIndex < vectors.length && sourceTable == 'CAMBT') || (recordValues.length > 0 && sourceTable == 'CAMBS')) insertFunction(callback);
                                    else if (sketchRowId.length > 0) {
                                        vectorIndex = 0;
                                        processor(sketchRowId, fields, callback);
                                    }
                                    else {
                                        if (callback) callback();
                                        return;
                                    }
                                });
                            });
                        });
                    }

                }, function () {
                    //console.error('Error fired during insertion');
                });
            }
            if ((vectors.length > 0 && sourceTable == 'CAMBT') || (recordValues.length > 0 && sourceTable == 'CAMBS'))
                insertFunction(callback);
            else if (callback) callback();
        }
        if (sketchRowIds.length > 0) {
            var deleteRecord = function (rowuids) {
                var rowuid = rowuids.pop();
                ccma.Data.Controller.DeleteAuxRecordWithDescendants(cat.Id, rowuid, null, null, function () {
                    if (rowuids.length == 0)
                        processor(sketchRowIds, fields, function () {
                            if (callback) callback();
                        });
                    else
                        deleteRecord(rowuids);
                });
            }
            var recRowIds = [];
            for (r in sketchRowIds) {
                var records = activeParcel.CAMBL.filter(function (c) { return c.ROWUID == sketchRowIds[r] })[0][sourceTable];
                for (r in records)
                    recRowIds.push(records[r].ROWUID)
            }
            if (recRowIds.length > 0)
                deleteRecord(recRowIds)
            else {
                processor(sketchRowIds, fields, function () {
                    if (callback) callback();
                });
            }
        }
    }
    var sketchDataCopy = sketchData.slice();
    fprocessor('CAMBT', ['BTTRAV', 'ITEM', 'BTLINE'], function () {
        sketchData = sketchDataCopy.slice();
        fprocessor('CAMBS', ['BSSUB', 'BSYR', 'BSACTL'], function () {
            var xprocessor = function () {
                var skData = sketchDataCopy.pop();
                var field = getDataField('BLSFAC', 'CAMBL');
                var totalArea = 0;
                sketchApp.sketches.filter(function (s) { return s.sid == skData.rowid })[0].vectors.filter(function(x) { return !(x.isFreeFormLineEntries) }).forEach(function (v) {
                    totalArea += parseFloat(v.area());
                })
                var record = activeParcel.CAMBL.filter(function (c) { return c.ROWUID == skData.rowid; })[0];
                ccma.Sync.enqueueParcelChange(activeParcel.Id, skData.rowid, null, 'E', 'BLSFAC', field.Id, record['BLSFAC'], Math.round(totalArea), { source: 'CAMBL', sourceField: field.Name, updateData: true }, function () {
                    record['BLSFAC'] = totalArea;
                    if (sketchDataCopy.length > 0) xprocessor();
                    else if (callback) callback();
                });
            }
            xprocessor();
        })
    });
}

function Vision6_5SketchAfterSave(sketchData, callback) {
    if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }
    var errRecords = [], isBristol = ( ( clientSettings.SketchConfig == 'Vision6_5' || sketchSettings["SketchConfig"] == 'Vision6_5' )? true: false );
    var fields = ['MNC', 'PID', 'BID', 'SUB_CODE', 'SUB_AREA_GROSS','SECT_NUM'];
    var resTable = 'CCV_CONSTR_RESDEP';
    var comTable = 'CCV_CONSTR_COMDEP';
  	var condoTable = 'CCV_CONSTR_CDMDEP';
  	var condoUnitTable = 'CCV_CONSTR_CDUDEP';
  	var sketchSectNumData = _.clone(sketchData);
    var processor = function (sketchRecord, fields, callback) {
        line = 0;
        var sk = sketchRecord.pop();
        var sourceTable, parentSourceData,sourceParentTable;
        var bldgSourceData = activeParcel['CCV_BLDG_CONSTR'].filter(function (a) { return a.ROWUID == sk.rowid })[0];
		var resData = activeParcel[resTable]? activeParcel[resTable].filter(function (com) { return (com.ParentROWUID == bldgSourceData.ROWUID ) }): [];
		var comData = activeParcel[comTable]? activeParcel[comTable].filter(function (com) { return (com.ParentROWUID == bldgSourceData.ROWUID ) }): [];
		var conData = activeParcel[condoTable]? activeParcel[condoTable].filter(function (com) { return (com.ParentROWUID == bldgSourceData.ROWUID) }): [];
		var conunitData = activeParcel[condoUnitTable]? activeParcel[condoUnitTable].filter(function (com) { return (com.ParentROWUID == bldgSourceData.ROWUID) }): [];
        var recordDeleted = false;
        if (resData.length > 0) {
        	sourceParentTable = resTable;
        	sourceTable = 'CCV_SUBAREA_RES';
        }
        else if(comData.length > 0) {
        	sourceParentTable = comTable;
        	sourceTable = 'CCV_SUBAREA_COM';
        }
        else if(conData.length > 0) {
        	sourceParentTable = condoTable;
        	sourceTable = 'CCV_SUBAREA_CDM';
        }
        else if(conunitData.length > 0) {
        	sourceParentTable = condoUnitTable;
        	sourceTable = 'CCV_SUBAREA_CDU';
        }
        var vectors = sketchApp.sketches.filter(function (s) { return s.sid == sk.rowid })[0].vectors;
        var area = 0, label = '', recordValues = [], pushed = false, secNums;
        var sprocessor = function (lbl, secNums) {
            for (var r in recordValues) {
                if (recordValues[r].Label == lbl && recordValues[r].SECT_NUM == secNums) {
                    recordValues[r].Area += area;
                    pushed = true;
                }
            }
            if (!pushed) recordValues.push({ Label: lbl, Area: area, BID: parentSourceData.BID, PID: parentSourceData.PID, MNC: parentSourceData.MNC, SECT_NUM: secNums, parentSourceData :_.clone(parentSourceData) });
        }
        for (v = 0; vectors[v] != null; v++) {
        	secNums = vectors[v].sectionNum;
        	if(secNums.split("{").length > 1){
        		if(secNums.split("#").length > 1)
        			secNums = secNums.split("#")[1].split("$")[0];
        		else
        			secNums = secNums.split("{")[1].split("}")[0];
        	}
        	parentSourceData = activeParcel[sourceParentTable].filter(function (spt) { return ((spt.ParentROWUID == bldgSourceData.ROWUID ) && ((spt.SECT_NUM == secNums || spt.ROWUID == secNums)))})[0];
            area = (isBristol? Math.round(parseFloat(vectors[v].area())): parseFloat(vectors[v].area()));
            label = (vectors[v].code || vectors[v].name).toString().trim();
            if (isBristol) {
            	label.split('/').forEach(function(lbl) {            		
            		pushed = false;
            		sprocessor(lbl,secNums,area);
            	});
            }
        }
		        
        var cat = getCategoryFromSourceTable(sourceTable);
        var field = [];
        fields.forEach(function (f) {
            var fd = getDataField(f, sourceTable);
            field.push({ Id: fd.Id, AssignedName: fd.AssignedName, Name: fd.Name, Value: null });
        })
        recordValues = recordValues.reverse();
        var insertFunction = function (callback) {
        	var recValue = {};
            recValue = recordValues.pop();
            var parentSourceDatat = recValue.parentSourceData;
            insertNewAuxRecord(null, parentSourceDatat, cat.Id, null, null, function (rowid) {
                var recordData = activeParcel[sourceTable].filter(function (s) { return s.ROWUID == rowid })[0];
                    //parcelId, auxRowId, parentAuxRowId, action, fieldName, fieldId, oldValue, newValue, options, successCallback, failureCallback
                    field[0].Value = recValue.MNC;
                    field[1].Value = recValue.PID;
                    field[2].Value = recValue.BID;
                    field[3].Value = recValue.Label;
                    field[4].Value = recValue.Area;
					field[5].Value = recValue.SECT_NUM;
					
                    var tempField = field.slice();
                    var updateFunction = function (callback) {
                        var tfd = tempField.pop();
						ccma.Sync.enqueueParcelChange(activeParcel.Id, rowid, sk.rowid, 'E', tfd.Name, tfd.Id, null, tfd.Value, { source: sourceTable, sourceField: tfd.Name, updateData: true }, function () {
                            if (tempField.length > 0)
                                updateFunction(callback);
                            else if (callback) callback();
                        });
                    }
                    updateFunction(function () {
                        field.forEach(function (fd) {
                            recordData[fd.Name] = fd.Value;
                        })
                        if (recordValues.length > 0)
                            insertFunction(callback);
                        else if (sketchRecord.length > 0)
                            processor(sketchRecord, fields, callback);
                        else {
                            if (callback) callback();
                        }
                    });
            }, function () {
                //console.error('Error fired during insertion');
            });
        }
        if (recordValues.length > 0)
            insertFunction(callback);
        else if (callback) callback();
    }
    var deleteRecord = function (rowuids, catId, skData, fields, callback) {
        var rowuid = rowuids.pop();
        var rel = false;
        if(rowuids.length == 0 && (typeof(cachedAuxOptions) != 'undefined') && !(isEmpty(cachedAuxOptions)))
        	rel = true;
        ccma.Data.Controller.DeleteAuxRecordWithDescendants(catId, rowuid, null, null, function () {
            if (rowuids.length == 0)
                processor(skData, fields, function () {
                    if (callback) callback();
                });
            else
                deleteRecord(rowuids, catId, skData, fields ,callback);
        }, rel, null, true);
    }
    var resData = [], comData = [], condoData = [], condoUnitData = [], skSourceData, recRowIds = [];
    var fprocessor = function (parentTable, childTable, skData, cat, callback) {
    	skData.forEach(function (sk) {
    		if(sk.CNS_USE_APEX && sk.CNS_USE_APEX.toString() != '1') { 
    			if(callback) callback();
    			return;
			}
	        var record = activeParcel[parentTable].filter(function (s) { return s.ParentROWUID == sk.rowid });
	        var records = [];
	        record.forEach(function(rec){
	        	var childTables = rec[childTable]; 
	        	childTables.forEach(function(che){
	        		records.push(che);
	        	});
	        });
	        for (r in records)
	            recRowIds.push(records[r].ROWUID)
	        if (recRowIds.length > 0)
	            deleteRecord(recRowIds, cat.Id, skData, fields, callback);
	        else
	            processor(skData, fields, function () {
	                if (callback) callback();
	            });            
        });
    }
    var updatesketchData = function () {
    	sketchData.forEach(function(sk) {
    		skSourceData = activeParcel['CCV_BLDG_CONSTR'].filter(function (skd) { return skd.ROWUID == sk.rowid && skd.CC_Deleted != true; })[0];
    		sk.CNS_USE_APEX = skSourceData.CNS_USE_APEX;
			if (activeParcel[resTable] && activeParcel[resTable].filter(function (res) { return (res.ParentROWUID == skSourceData.ROWUID) && res.CC_Deleted != true }).length > 0)
    			resData.push(sk);
			if (activeParcel[comTable] && activeParcel[comTable].filter(function (com) { return (com.ParentROWUID == skSourceData.ROWUID) && com.CC_Deleted != true }).length > 0)
    			comData.push(sk);
			if (activeParcel[condoTable] && activeParcel[condoTable].filter(function (condo) { return (condo.ParentROWUID == skSourceData.ROWUID) && condo.CC_Deleted != true }).length > 0)
    			condoData.push(sk);
			if (activeParcel[condoUnitTable] && activeParcel[condoUnitTable].filter(function (condounit) { return (condounit.ParentROWUID == skSourceData.ROWUID) && condounit.CC_Deleted != true }).length > 0)
    			condoUnitData.push(sk);
    	});
    	
    	if(resData.length > 0) fprocessor(resTable, 'CCV_SUBAREA_RES', resData, getCategoryFromSourceTable('CCV_SUBAREA_RES'), function () {
        	if (comData.length > 0) fprocessor(comTable, 'CCV_SUBAREA_COM', comData, getCategoryFromSourceTable('CCV_SUBAREA_COM'), function () {
            	if (condoData.length > 0) fprocessor(condoTable, 'CCV_SUBAREA_CDM', condoData, getCategoryFromSourceTable('CCV_SUBAREA_CDM'), function () {
            		if (condoData.length > 0) fprocessor(condoUnitTable, 'CCV_SUBAREA_CDU', condoUnitData, getCategoryFromSourceTable('CCV_SUBAREA_CDU'), function () {
            			if (callback) callback();
        			})
					else if (callback) callback(); 
        		})
				else if (callback) callback();        		
        	})
        	else if (callback) callback();
    	})
   		else if (comData.length > 0) fprocessor(comTable, 'CCV_SUBAREA_COM', comData, getCategoryFromSourceTable('CCV_SUBAREA_COM'), function () {
        	if (condoData.length > 0) fprocessor(condoTable, 'CCV_SUBAREA_CDM', condoData, getCategoryFromSourceTable('CCV_SUBAREA_CDM'), function () {
            	if (condoData.length > 0) fprocessor(condoUnitTable, 'CCV_SUBAREA_CDU', condoUnitData, getCategoryFromSourceTable('CCV_SUBAREA_CDU'), function () {
            		if (callback) callback();
        		})
				else if (callback) callback(); 
        	})
			else if (callback) callback(); 
    	})
    	else if (condoData.length > 0) fprocessor(condoTable, 'CCV_SUBAREA_CDM', condoData, getCategoryFromSourceTable('CCV_SUBAREA_CDM'), function () {
            if (condoData.length > 0) fprocessor(condoUnitTable, 'CCV_SUBAREA_CDU', condoUnitData, getCategoryFromSourceTable('CCV_SUBAREA_CDU'), function () {
            	if (callback) callback();
        	})
			else if (callback) callback(); 
        })
        else if (condoData.length > 0) fprocessor(condoUnitTable, 'CCV_SUBAREA_CDU', condoUnitData, getCategoryFromSourceTable('CCV_SUBAREA_CDU'), function () {
            if (callback) callback();
        })
    	else if (callback) callback();
    }
    
    var updateSketchStringData = function (sketchSectNumDatas) {
    	if(sketchSectNumDatas.length == 0){ 
    		updatesketchData();
    		return;
    	}
    	var sksectData = sketchSectNumDatas.pop();
    	var sksectDa = '';
    	var CCField = getDataField('CC_SKETCH', 'CCV_BLDG_CONSTR');
    	var sktch = sketchApp.sketches.filter(function(tes) {return tes.uid == sksectData.rowid})[0];
    	var BldRecord = activeParcel['CCV_BLDG_CONSTR'].filter(function (skd) { return skd.ROWUID == sksectData.rowid && skd.CC_Deleted != true; })[0];
		sktch.vectors.forEach(function(vect) {
			var tvectString = vect.vectorString;
			if(tvectString){			
				if(vect.wasModified){
					var sectn = vect.sectionNum;
					var temp_str = tvectString.split('[')[1].split(']')[0].split(/\,/);
					temp_str[1] = sectn;
					sksectDa = sksectDa + tvectString.split('[')[0] + '[' + temp_str + ']'+ tvectString.split(']')[1] + ';';
				}
				else
					sksectDa = sksectDa + tvectString + ';';
			}
		});	
		update_data(CCField, sksectDa, BldRecord, function(){
			updateSketchStringData(sketchSectNumData);
		})		
    }
    updateSketchStringData(sketchSectNumData);
}


function Vision75ApexSketchAfterSaveNew(sketchData, callback) {
    if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }
    var errRecords = [], isBristol = ( ( clientSettings.SketchConfig == 'Bristol' || sketchSettings["SketchConfig"] == 'Bristol' )? true: false );
    var fields = ['MNC', 'PID', 'BID', 'SUB_CODE', 'SUB_AREA_GROSS', 'SECT_NUM'];
    var resTable = (isBristol ? 'CCV_CONSTR_RESDEP': 'CCV_CONSTRSECTION_RES');
    var comTable = (isBristol ? 'CCV_CONSTR_COMDEP': 'CCV_CONSTRSECTION_COM');
    var condoTable = 'CCV_CONSTRSECTION_CDM';
  	var condoUnitTable = 'CCV_CONSTRSECTION_CDU';
  	var sketchSectNumData = _.clone(sketchData);
    var processor = function (sketchRecord, fields, callback) {
        line = 0;
        var sk = sketchRecord.pop();
        var sourceTable, parentSourceData, sourceParentTable;
        var bldgSourceData = activeParcel['CCV_BLDG_CONSTR'].filter(function (a) { return a.ROWUID == sk.rowid })[0];
		var resData = activeParcel[resTable]? activeParcel[resTable].filter(function (com) { return (bldgSourceData.BID ? com.BID == bldgSourceData.BID : com.ParentROWUID == sk.rowid)}): [];
		var comData = activeParcel[comTable]? activeParcel[comTable].filter(function (com) { return (bldgSourceData.BID ? com.BID == bldgSourceData.BID : com.ParentROWUID == sk.rowid) }): [];
		var conData = activeParcel[condoTable]? activeParcel[condoTable].filter(function (com) { return (com.ParentROWUID == bldgSourceData.ROWUID) }): [];
		var conunitData = activeParcel[condoUnitTable]? activeParcel[condoUnitTable].filter(function (com) { return (com.ParentROWUID == bldgSourceData.ROWUID) }): [];
        var recordDeleted = false;
        if (resData.length > 0) {
        	sourceParentTable = resTable;
        	sourceTable = 'CCV_SUBAREA_RES';
        }
        else if(comData.length > 0) {
        	sourceParentTable = comTable;
        	sourceTable = 'CCV_SUBAREA_COM';
        }
        else if(conData.length > 0) {
        	sourceParentTable = condoTable;
        	sourceTable = 'CCV_SUBAREA_CDM';
        }
        else if(conunitData.length > 0) {
        	sourceParentTable = condoUnitTable;
        	sourceTable = 'CCV_SUBAREA_CDU';
        }
        var vectors = sketchApp.sketches.filter(function (s) { return s.sid == sk.rowid })[0].vectors;
        var area = 0, label = '', recordValues = [], pushed = false, secNums;
        var sprocessor = function (lbl, secNums) {
            for (var r in recordValues) {
                if (recordValues[r].Label == lbl && recordValues[r].SECT_NUM == secNums) {
                    recordValues[r].Area += area;
                    pushed = true;
                }
            }
            if (!pushed) recordValues.unshift({ Label: lbl, Area: area, BID: parentSourceData.BID, PID: parentSourceData.PID, MNC: parentSourceData.MNC, SECT_NUM: secNums, parentSourceData :_.clone(parentSourceData) });
        }
        for (v = 0; vectors[v] != null; v++) {
        	secNums = vectors[v].sectionNum;
        	if(secNums.split("{").length > 1){
        		if(secNums.split("#").length > 1)
        			secNums = secNums.split("#")[1].split("$")[0];
        		else
        			secNums = secNums.split("{")[1].split("}")[0];
        	}
        	parentSourceData = activeParcel[sourceParentTable].filter(function (spt) { return ((spt.ParentROWUID == bldgSourceData.ROWUID ) && ((spt.SECT_NUM == secNums || spt.ROWUID == secNums)))})[0];
			area = vectors[v].fixedArea && vectors[v].fixedArea != 0 && vectors[v].fixedArea != -1 ? parseFloat( vectors[v].fixedArea ) : ( isBristol ? Math.round( parseFloat( vectors[v].area() ) ) : parseFloat( vectors[v].area() ) );
            label = (vectors[v].code || vectors[v].name).toString().trim();
            label.split('/').forEach(function(lbl) {            		
            	pushed = false;
            	sprocessor(lbl,secNums,area);
            });
        }
		        
        var cat = getCategoryFromSourceTable(sourceTable);
        var field = [];
        fields.forEach(function (f) {
            var fd = getDataField(f, sourceTable);
            field.push({ Id: fd.Id, AssignedName: fd.AssignedName, Name: fd.Name, Value: null });
        });
        
        var insertFunction = function (callback) {
        	var recValue = {};
            recValue = recordValues.pop();
            var parentSourceDatat = recValue.parentSourceData;
            insertNewAuxRecord(null, parentSourceDatat, cat.Id, null, null, function (rowid) {
                var recordData = activeParcel[sourceTable].filter(function (s) { return s.ROWUID == rowid })[0];
                    //parcelId, auxRowId, parentAuxRowId, action, fieldName, fieldId, oldValue, newValue, options, successCallback, failureCallback
                field[0].Value = recValue.MNC;
                field[1].Value = recValue.PID;
                field[2].Value = recValue.BID;
                field[3].Value = recValue.Label;
                field[4].Value = recValue.Area;
                field[5].Value = recValue.SECT_NUM;

                var tempField = field.slice();
                var updateFunction = function (callback) {
                        var tfd = tempField.pop();
					ccma.Sync.enqueueParcelChange(activeParcel.Id, rowid, sk.rowid, 'E', tfd.Name, tfd.Id, null, tfd.Value, { source: sourceTable, sourceField: tfd.Name, updateData: true }, function () {
                            if (tempField.length > 0)
                                updateFunction(callback);
                            else if (callback) callback();
                        });
                }
                updateFunction(function () {
                        field.forEach(function (fd) {
							recordData[fd.Name] = fd.Value;
                        })
                        if (recordValues.length > 0)
                            insertFunction(callback);
                        else if (sketchRecord.length > 0)
                            processor(sketchRecord, fields, callback);
                        else {
                            if (callback) callback();
                            return;
                        }
                 });

            }, function () {
                //console.error('Error fired during insertion');
            });
        }
        if (recordValues.length > 0)
            insertFunction(callback);
        else if (callback) callback();
    }
    var deleteRecord = function (rowuids, catId, skData, fields) {
        var rowuid = rowuids.pop();
        ccma.Data.Controller.DeleteAuxRecordWithDescendants(catId, rowuid, null, null, function () {
            if (rowuids.length == 0)
                processor(skData, fields, function () {
                    if (callback) callback();
                });
            else
                deleteRecord(rowuids, catId, skData, fields);
        }, null, null, true);
    }
    var resData = [], comData = [], condoData = [], condoUnitData = [], skSourceData, recRowIds = [];
    
    var fprocessor = function (parentTable, childTable, skData, cat, callback) {
    	skData.forEach(function (sk) {
	        var record = activeParcel[parentTable].filter(function (s) { return s.ParentROWUID == sk.rowid });
	        var records = [];
	        record.forEach(function(rec){
	        	var childTables = rec[childTable]; 
	        	childTables.forEach(function(che){
	        		records.push(che);
	        	});
	        });
	        for (r in records)
	            recRowIds.push(records[r].ROWUID)
	        if (recRowIds.length > 0)
	            deleteRecord(recRowIds, cat.Id, skData, fields, callback);
	        else
	            processor(skData, fields, function () {
	                if (callback) callback();
	            });             
        });
    }
    

    var updatesketchData = function () {
    	sketchData.forEach(function(sk) {
    		skSourceData = activeParcel['CCV_BLDG_CONSTR'].filter(function (skd) { return skd.ROWUID == sk.rowid && skd.CC_Deleted != true; })[0];
    		sk.CNS_USE_APEX = skSourceData.CNS_USE_APEX;
			if (activeParcel[resTable] && activeParcel[resTable].filter(function (res) { return (res.ParentROWUID == skSourceData.ROWUID) && res.CC_Deleted != true }).length > 0)
    			resData.push(sk);
			if (activeParcel[comTable] && activeParcel[comTable].filter(function (com) { return (com.ParentROWUID == skSourceData.ROWUID) && com.CC_Deleted != true }).length > 0)
    			comData.push(sk);
			if (activeParcel[condoTable] && activeParcel[condoTable].filter(function (condo) { return (condo.ParentROWUID == skSourceData.ROWUID) && condo.CC_Deleted != true }).length > 0)
    			condoData.push(sk);
			if (activeParcel[condoUnitTable] && activeParcel[condoUnitTable].filter(function (condounit) { return (condounit.ParentROWUID == skSourceData.ROWUID) && condounit.CC_Deleted != true }).length > 0)
    			condoUnitData.push(sk);
    	});
    	if (resData.length > 0) fprocessor(resTable, 'CCV_SUBAREA_RES', resData, getCategoryFromSourceTable('CCV_SUBAREA_RES'), function () {
        	if (comData.length > 0) fprocessor(comTable, 'CCV_SUBAREA_COM', comData, getCategoryFromSourceTable('CCV_SUBAREA_COM'), function () {
            	if (condoData.length > 0) fprocessor(condoTable, 'CCV_SUBAREA_CDM', condoData, getCategoryFromSourceTable('CCV_SUBAREA_CDM'), function () {
            		if (condoData.length > 0) fprocessor(condoUnitTable, 'CCV_SUBAREA_CDU', condoUnitData, getCategoryFromSourceTable('CCV_SUBAREA_CDU'), function () {
            			if (callback) callback();
        			})
					else if (callback) callback(); 
        		})
				else if (callback) callback();  
        	})
        	else if (callback) callback();
    	})
   		else if (comData.length > 0) fprocessor(comTable, 'CCV_SUBAREA_COM', comData, getCategoryFromSourceTable('CCV_SUBAREA_COM'), function () {
        	if (condoData.length > 0) fprocessor(condoTable, 'CCV_SUBAREA_CDM', condoData, getCategoryFromSourceTable('CCV_SUBAREA_CDM'), function () {
            	if (condoData.length > 0) fprocessor(condoUnitTable, 'CCV_SUBAREA_CDU', condoUnitData, getCategoryFromSourceTable('CCV_SUBAREA_CDU'), function () {
            		if (callback) callback();
        		})
				else if (callback) callback(); 
        	})
			else if (callback) callback(); 
    	})
    	else if (condoData.length > 0) fprocessor(condoTable, 'CCV_SUBAREA_CDM', condoData, getCategoryFromSourceTable('CCV_SUBAREA_CDM'), function () {
            if (condoData.length > 0) fprocessor(condoUnitTable, 'CCV_SUBAREA_CDU', condoUnitData, getCategoryFromSourceTable('CCV_SUBAREA_CDU'), function () {
            	if (callback) callback();
        	})
			else if (callback) callback(); 
        })
        else if (condoData.length > 0) fprocessor(condoUnitTable, 'CCV_SUBAREA_CDU', condoUnitData, getCategoryFromSourceTable('CCV_SUBAREA_CDU'), function () {
            if (callback) callback();
        })
    	else if (callback) callback();
    }
    
     var updateSketchStringData = function (sketchSectNumDatas) {
    	if(sketchSectNumDatas.length == 0){ 
    		updatesketchData();
    		return;
    	}
    	var sksectData = sketchSectNumDatas.pop();
    	var sksectDa = '';
    	var CCField = getDataField('CC_SKETCH', 'CCV_BLDG_CONSTR');
    	var sktch = sketchApp.sketches.filter(function(tes) {return tes.uid == sksectData.rowid})[0];
    	var BldRecord = activeParcel['CCV_BLDG_CONSTR'].filter(function (skd) { return skd.ROWUID == sksectData.rowid && skd.CC_Deleted != true; })[0];
		sktch.vectors.forEach(function(vect) {
			var tvectString = vect.vectorString;
			if(tvectString){			
				if(vect.wasModified){
					var sectn = vect.sectionNum;
					var temp_str = tvectString.split('[')[1].split(']')[0].split(/\,/);
					temp_str[1] = sectn;
					sksectDa = sksectDa + tvectString.split('[')[0] + '[' + temp_str + ']'+ tvectString.split(']')[1] + ';';
				}
				else
					sksectDa = sksectDa + tvectString + ';';
			}
		});	
		update_data(CCField, sksectDa, BldRecord, function(){
			updateSketchStringData(sketchSectNumData);
		})		
	}

	if (visionPCIs.length > 0) {
		_PCIBatchArray = []; autoInsertRecords = [];
		visionCappingRecordCreation(() => {
			updateSketchStringData(sketchSectNumData);
		});
	}
	else updateSketchStringData(sketchSectNumData);
}


function Vision75ApexSketchAfterSave(sketchData, callback) {
    if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }
    var errRecords = [], isBristol = ( ( clientSettings.SketchConfig == 'Bristol' || sketchSettings["SketchConfig"] == 'Bristol' )? true: false );
    var fields = ['MNC', 'PID', 'BID', 'SUB_CODE', 'SUB_AREA_GROSS'];
    var resTable = (isBristol ? 'CCV_CONSTR_RESDEP': 'CCV_CONSTRSECTION_RES');
    var comTable = (isBristol ? 'CCV_CONSTR_COMDEP': 'CCV_CONSTRSECTION_COM');
    var processor = function (sketchRecord, fields, callback) {
        line = 0;
        var sk = sketchRecord.pop();
        var sourceTable, parentSourceData;
        var bldgSourceData = activeParcel['CCV_BLDG_CONSTR'].filter(function (a) { return a.ROWUID == sk.rowid })[0];
		var resData = activeParcel[resTable]? activeParcel[resTable].filter(function (com) { return (bldgSourceData.BID ? com.BID == bldgSourceData.BID : com.ParentROWUID == sk.rowid)}): [];
		var comData = activeParcel[comTable]? activeParcel[comTable].filter(function (com) { return (bldgSourceData.BID ? com.BID == bldgSourceData.BID : com.ParentROWUID == sk.rowid) }): [];
        var recordDeleted = false;
        if (resData.length > 0) {
        	parentSourceData = resData[0];
        	sourceTable = 'CCV_SUBAREA_RES';
        }
        else if(comData.length > 0) {
        	parentSourceData = comData[0];
        	sourceTable = 'CCV_SUBAREA_COM';
        }
        var vectors = sketchApp.sketches.filter(function (s) { return s.sid == sk.rowid })[0].vectors;
        var area = 0, label = '', recordValues = [], pushed = false;
        var sprocessor = function (lbl) {
            for (var r in recordValues) {
                if (recordValues[r].Label == lbl) {
                    recordValues[r].Area += area;
                    pushed = true;
                }
            }
            if (!pushed) recordValues.unshift({ Label: lbl, Area: area, BID: parentSourceData.BID, PID: parentSourceData.PID, MNC: parentSourceData.MNC });
        }
        for (v = 0; vectors[v] != null; v++) {
			area = vectors[v].fixedArea && vectors[v].fixedArea != 0 && vectors[v].fixedArea != -1 ? parseInt( vectors[v].fixedArea ) : ( isBristol ? Math.round( parseFloat( vectors[v].area() ) ) : parseFloat( vectors[v].area() ) );
            label = (vectors[v].code || vectors[v].name).toString().trim();
           // if (isBristol) {
            	label.split('/').forEach(function(lbl) {            		
            		pushed = false;
            		sprocessor(lbl, area);
            	});
           // }
           /*
            else {
            	pushed = false;
            	sprocessor(label, area);
            }
            */
        }
		        
        var cat = getCategoryFromSourceTable(sourceTable);
        var field = [];
        fields.forEach(function (f) {
            var fd = getDataField(f, sourceTable);
            field.push({ Id: fd.Id, AssignedName: fd.AssignedName, Name: fd.Name, Value: null });
        });
        
        var insertFunction = function (callback) {
            insertNewAuxRecord(null, parentSourceData, cat.Id, null, null, function (rowid) {
                var recordData = activeParcel[sourceTable].filter(function (s) { return s.ROWUID == rowid })[0];
                if (recordValues.length > 0) {
                    var recValue = recordValues.pop();
                    //parcelId, auxRowId, parentAuxRowId, action, fieldName, fieldId, oldValue, newValue, options, successCallback, failureCallback
                    field[0].Value = recValue.MNC;
                    field[1].Value = recValue.PID;
                    field[2].Value = recValue.BID;
                    field[3].Value = recValue.Label;
                    field[4].Value = recValue.Area;

                    var tempField = field.slice();
                    var updateFunction = function (callback) {
                        var tfd = tempField.pop();
						ccma.Sync.enqueueParcelChange(activeParcel.Id, rowid, sk.rowid, 'E', tfd.Name, tfd.Id, null, tfd.Value, { source: sourceTable, sourceField: tfd.Name, updateData: true }, function () {
                            if (tempField.length > 0)
                                updateFunction(callback);
                            else if (callback) callback();
                        });
                    }
                    updateFunction(function () {
                        field.forEach(function (fd) {
							recordData[fd.Name] = fd.Value;
                        })
                        if (recordValues.length > 0)
                            insertFunction(callback);
                        else if (sketchRecord.length > 0)
                            processor(sketchRecord, fields, callback);
                        else {
                            if (callback) callback();
                            return;
                        }
                    });
                }

            }, function () {
                //console.error('Error fired during insertion');
            });
        }
        if (recordValues.length > 0)
            insertFunction(callback);
        else if (callback) callback();
    }
    var deleteRecord = function (rowuids, catId, skData, fields) {
        var rowuid = rowuids.pop();
        ccma.Data.Controller.DeleteAuxRecordWithDescendants(catId, rowuid, null, null, function () {
            if (rowuids.length == 0)
                processor(skData, fields, function () {
                    if (callback) callback();
                });
            else
                deleteRecord(rowuids, catId, skData, fields);
        }, null, null, true);
    }
    var resData = [], comData = [], skSourceData;
    sketchData.forEach(function(sk) {
    	skSourceData = activeParcel['CCV_BLDG_CONSTR'].filter(function (skd) { return skd.ROWUID == sk.rowid && skd.CC_Deleted != true; })[0];
    	sk.CNS_USE_APEX = skSourceData.CNS_USE_APEX;
		if (activeParcel[resTable] && activeParcel[resTable].filter(function (res) { return res.BID == skSourceData.BID && res.CC_Deleted != true }).length > 0)
    		resData.push(sk);
		if (activeParcel[comTable] && activeParcel[comTable].filter(function (com) { return com.BID == skSourceData.BID && com.CC_Deleted != true }).length > 0)
    		comData.push(sk);
    });
    var recRowIds = [];
    var fprocessor = function (parentTable, childTable, skData, cat, callback) {
    	skData.forEach(function (sk) {
	        var records = activeParcel[parentTable].filter(function (s) { return s.ParentROWUID == sk.rowid })[0][childTable];
	        for (r in records)
	            recRowIds.push(records[r].ROWUID)
	        if (recRowIds.length > 0)
	            deleteRecord(recRowIds, cat.Id, skData, fields);
	        else
	            processor(skData, fields, function () {
	                if (callback) callback();
	            });            
        });
    }
    if (resData.length > 0) fprocessor(resTable, 'CCV_SUBAREA_RES', resData, getCategoryFromSourceTable('CCV_SUBAREA_RES'), function () {
        if (comData.length > 0) fprocessor(comTable, 'CCV_SUBAREA_COM', comData, getCategoryFromSourceTable('CCV_SUBAREA_COM'), function () {
            if (callback) callback();
        })
        else if (callback) callback();
    })
    else if (comData.length > 0) fprocessor(comTable, 'CCV_SUBAREA_COM', comData, getCategoryFromSourceTable('CCV_SUBAREA_COM'), function () {
        if (callback) callback();
    })
    else if (callback) callback();
}

function Vision75ApexSketchBeforeSave(sketches, callback) {
	var valid = { isValid: true, message: 'The following label(s) are <b>incorrect</b>: ', auditTrailEntry: 'The following label(s) are incorrect: ' };
	var invalid_label_error = 'Sketch labels are invalid, sketch changes were saved but the SUBAREA information was not updated.';
	var multiple_sectnum_error = 'There are multiple Sections for this Building. The sketch changes were saved but the SubArea information was not updated.';
	var multiple_sectnum_msg = 'There are multiple Sections for this Building. The sketch changes will be saved but the SubArea information will not be updated.';
	if (sketches === undefined || sketches == null) {
		if (callback) callback(true);
		return;
	}
	
	sketches.forEach(function (sk) {
		sk.vectors.forEach(function (v) {
		    if ( Object.keys( lookup[v.vectorConfig.LabelLookup] ).filter( function ( label ) { return label == v.code; } ).length == 0 )
		    {
				valid.isValid = valid.isValid && false;
				valid.message += (v.name + ', ');
				valid.auditTrailEntry += (v.name + ', ');
			}
		});
	});
	
	var multipleSectnum_validator = function () {
		var hasDuplicate = false;
		sketches.forEach(function (sk) {
			var resData = activeParcel['CCV_BLDG_CONSTR'].filter(function(bldg) { return bldg.ROWUID == sk.sid; })[0]['CCV_CONSTRSECTION_RES'];
			var comData = activeParcel['CCV_BLDG_CONSTR'].filter(function(bldg) { return bldg.ROWUID == sk.sid; })[0]['CCV_CONSTRSECTION_COM'];
			var sectNum = [];
			if(resData && resData.length > 0) 
				sectNum = resData.map(function (res) { return res.SECT_NUM });				
			else if(comData && comData.length > 0) 
				sectNum = comData.map(function (com) { return com.SECT_NUM });
			
			hasDuplicate = (new Set(sectNum).size !== sectNum.length);
			//There are multiple Sections for this Building. The sketch changes will be saved but the SubArea information will not be updated.
		});
		return hasDuplicate;
	}
	valid.message = valid.message.substring(0, valid.message.length - 2) + '. Tap "OK" and select the sketch segment, press "Edit Label" and select a sketch label from one in the list.';
	valid.message += '<br/><br/><span style="color:red">If you do not correct these labels your Subarea information will not be updated.</span>';
	valid.auditTrailEntry = valid.auditTrailEntry.substring(0, valid.auditTrailEntry.length - 2) + '. If you do not correct these labels your Subarea information will not be updated.'
	if (multipleSectnum_validator()) {
	    messageBox(multiple_sectnum_msg, ['OK'], function () {
	        if (callback) callback(false, true, multiple_sectnum_error);
	    });
	}
	else if (!valid.isValid) {
	    messageBox(valid.message, ['OK'], function () {
	        if (callback) callback(false, true, null, valid.auditTrailEntry);
	    });
	    //else {
	    //    if (multipleSectnum_validator()) {
	    //        if (alert(valid.message))
	    //            if (callback) callback(true, multiple_sectnum_error);
	    //    }
	    //    else if (callback) callback(false);
	    //}
	}
	else {
	    if (callback) callback(true, true);
	}
}

function Vision8ApexSketchAfterSave(sketchData, callback) {
    if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }
    var errRecords = [];
    var fields = ['MNC', 'PID', 'BID', 'SUB_CODE', 'SUB_AREA_GROSS'];
    var resTable = 'CCV_CONSTR_RESDEP';
    var comTable = 'CCV_CONSTR_COMDEP';
    var processor = function (sketchRecord, fields, callback) {
        line = 0;
        var sk = sketchRecord.pop();
        var sourceTable, parentSourceData;
        var bldgSourceData = activeParcel['CCV_BLDG_CONSTR'].filter(function (a) { return a.ROWUID == sk.rowid })[0];
		var resData = activeParcel[resTable]? activeParcel[resTable].filter(function (com) { return bldgSourceData.BID ? com.BID == bldgSourceData.BID : com.ParentROWUID == sk.rowid}): [];
		var comData = activeParcel[comTable]? activeParcel[comTable].filter(function (com) { return bldgSourceData.BID ? com.BID == bldgSourceData.BID : com.ParentROWUID == sk.rowid }): [];
        var recordDeleted = false;
        if (resData.length > 0) {
        	parentSourceData = resData[0];
        	sourceTable = 'CCV_SUBAREA_RES';
        }
        else if(comData.length > 0) {
        	parentSourceData = comData[0];
        	sourceTable = 'CCV_SUBAREA_COM';
        }
        var vectors = sketchApp.sketches.filter(function (s) { return s.sid == sk.rowid })[0].vectors;
        var area = 0, label = '', recordValues = [], pushed = false;
        var sprocessor = function (lbl) {
            for (var r in recordValues) {
                if (recordValues[r].Label == lbl) {
                    recordValues[r].Area += area;
                    pushed = true;
                }
            }
            if (!pushed) recordValues.unshift({ Label: lbl, Area: area, BID: parentSourceData.BID, PID: parentSourceData.PID, MNC: parentSourceData.MNC });
        }
        for (v = 0; vectors[v] != null; v++) {
			area = vectors[v].fixedArea && vectors[v].fixedArea != 0 && vectors[v].fixedArea != -1 ? parseInt( vectors[v].fixedArea ) : (parseFloat( vectors[v].area() ) );
            label = (vectors[v].code || vectors[v].name).toString().trim();
            label.split('/').forEach(function(lbl) {            		
            	pushed = false;
            	sprocessor(lbl, area);
            });
        }
		        
        var cat = getCategoryFromSourceTable(sourceTable);
        var field = [];
        fields.forEach(function (f) {
            var fd = getDataField(f, sourceTable);
            field.push({ Id: fd.Id, AssignedName: fd.AssignedName, Name: fd.Name, Value: null });
        });
        
        var insertFunction = function (callback) {
            insertNewAuxRecord(null, parentSourceData, cat.Id, null, null, function (rowid) {
                var recordData = activeParcel[sourceTable].filter(function (s) { return s.ROWUID == rowid })[0];
                if (recordValues.length > 0) {
                    var recValue = recordValues.pop();
                    //parcelId, auxRowId, parentAuxRowId, action, fieldName, fieldId, oldValue, newValue, options, successCallback, failureCallback
                    field[0].Value = recValue.MNC;
                    field[1].Value = recValue.PID;
                    field[2].Value = recValue.BID;
                    field[3].Value = recValue.Label;
                    field[4].Value = recValue.Area;

                    var tempField = field.slice();
                    var updateFunction = function (callback) {
                        var tfd = tempField.pop();
						ccma.Sync.enqueueParcelChange(activeParcel.Id, rowid, sk.rowid, 'E', tfd.Name, tfd.Id, null, tfd.Value, { source: sourceTable, sourceField: tfd.Name, updateData: true }, function () {
                            if (tempField.length > 0)
                                updateFunction(callback);
                            else if (callback) callback();
                        });
                    }
                    updateFunction(function () {
                        field.forEach(function (fd) {
							recordData[fd.Name] = fd.Value;
                        })
                        if (recordValues.length > 0)
                            insertFunction(callback);
                        else if (sketchRecord.length > 0)
                            processor(sketchRecord, fields, callback);
                        else {
                            if (callback) callback();
                            return;
                        }
                    });
                }

            }, function () {
                //console.error('Error fired during insertion');
            });
        }
        if (recordValues.length > 0)
            insertFunction(callback);
        else if (callback) callback();
    }
    var deleteRecord = function (rowuids, catId, skData, fields) {
        var rowuid = rowuids.pop();
        ccma.Data.Controller.DeleteAuxRecordWithDescendants(catId, rowuid, null, null, function () {
            if (rowuids.length == 0)
                processor(skData, fields, function () {
                    if (callback) callback();
                });
            else
                deleteRecord(rowuids, catId, skData, fields);
        }, null, null, true);
    }
    var resData = [], comData = [], skSourceData;
    sketchData.forEach(function(sk) {
    	skSourceData = activeParcel['CCV_BLDG_CONSTR'].filter(function (skd) { return skd.ROWUID == sk.rowid && skd.CC_Deleted != true; })[0];
    	sk.CNS_USE_APEX = skSourceData.CNS_USE_APEX;
		if (activeParcel[resTable] && activeParcel[resTable].filter(function (res) { return res.BID == skSourceData.BID && res.CC_Deleted != true }).length > 0)
    		resData.push(sk);
		if (activeParcel[comTable] && activeParcel[comTable].filter(function (com) { return com.BID == skSourceData.BID && com.CC_Deleted != true }).length > 0)
    		comData.push(sk);
    });
    var recRowIds = [];
    var fprocessor = function (parentTable, childTable, skData, cat, callback) {
    	skData.forEach(function (sk) {
	        var recordss = activeParcel[parentTable].filter(function (s) { return s.ParentROWUID == sk.rowid })[0];
	        var records = recordss?recordss[childTable] : [];
	        for (r in records)
	            recRowIds.push(records[r].ROWUID)
	        if (recRowIds.length > 0)
	            deleteRecord(recRowIds, cat.Id, skData, fields);
	        else
	            processor(skData, fields, function () {
	                if (callback) callback();
	            });            
        });
    }
    if (resData.length > 0) fprocessor(resTable, 'CCV_SUBAREA_RES', resData, getCategoryFromSourceTable('CCV_SUBAREA_RES'), function () {
        if (comData.length > 0) fprocessor(comTable, 'CCV_SUBAREA_COM', comData, getCategoryFromSourceTable('CCV_SUBAREA_COM'), function () {
            if (callback) callback();
        })
        else if (callback) callback();
    })
    else if (comData.length > 0) fprocessor(comTable, 'CCV_SUBAREA_COM', comData, getCategoryFromSourceTable('CCV_SUBAREA_COM'), function () {
        if (callback) callback();
    })
    else if (callback) callback();
}

function Vision8ApexSketchAfterSaveNew(sketchData, callback) {
    if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }
    var errRecords = [];
    var fields = ['MNC', 'PID', 'BID', 'SUB_CODE', 'SUB_AREA_GROSS', 'SECT_NUM'];
    var resTable = 'CCV_CONSTR_RESDEP';
    var comTable = 'CCV_CONSTR_COMDEP';
    var condoTable = 'CCV_CONSTR_CDMDEP';
  	var condoUnitTable = 'CCV_CONSTR_CDUDEP';
  	var sketchSectNumData = _.clone(sketchData);
    var processor = function (sketchRecord, fields, callback) {
        line = 0;
        var sk = sketchRecord.pop();
        var sourceTable, parentSourceData,sourceParentTable;
        var bldgSourceData = activeParcel['CCV_BLDG_CONSTR'].filter(function (a) { return a.ROWUID == sk.rowid })[0];
		var resData = activeParcel[resTable]? activeParcel[resTable].filter(function (com) { return (com.ParentROWUID == bldgSourceData.ROWUID ) }): [];
		var comData = activeParcel[comTable]? activeParcel[comTable].filter(function (com) { return (com.ParentROWUID == bldgSourceData.ROWUID ) }): [];
		var conData = activeParcel[condoTable]? activeParcel[condoTable].filter(function (com) { return (com.ParentROWUID == bldgSourceData.ROWUID) }): [];
		var conunitData = activeParcel[condoUnitTable]? activeParcel[condoUnitTable].filter(function (com) { return (com.ParentROWUID == bldgSourceData.ROWUID) }): [];
        var recordDeleted = false;
        if (resData.length > 0) {
        	sourceParentTable = resTable;
        	sourceTable = 'CCV_SUBAREA_RES';
        }
        else if(comData.length > 0) {
        	sourceParentTable = comTable;
        	sourceTable = 'CCV_SUBAREA_COM';
        }
        else if(conData.length > 0) {
        	sourceParentTable = condoTable;
        	sourceTable = 'CCV_SUBAREA_CDM';
        }
        else if(conunitData.length > 0) {
        	sourceParentTable = condoUnitTable;
        	sourceTable = 'CCV_SUBAREA_CDU';
        }
        var vectors = sketchApp.sketches.filter(function (s) { return s.sid == sk.rowid })[0].vectors;
        var area = 0, label = '', recordValues = [], pushed = false, secNums;
        var sprocessor = function (lbl) {
            for (var r in recordValues) {
                if (recordValues[r].Label == lbl && recordValues[r].SECT_NUM == secNums) {
                    recordValues[r].Area += area;
                    pushed = true;
                }
            }
            if (!pushed) recordValues.unshift({ Label: lbl, Area: area, BID: parentSourceData.BID, PID: parentSourceData.PID, MNC: parentSourceData.MNC, SECT_NUM: secNums, parentSourceData :_.clone(parentSourceData) });
        }
        for (v = 0; vectors[v] != null; v++) {
            secNums = vectors[v].sectionNum;
        	if(secNums.split("{").length > 1){
        		if(secNums.split("#").length > 1)
        			secNums = secNums.split("#")[1].split("$")[0];
        		else
        			secNums = secNums.split("{")[1].split("}")[0];
        	}
        	parentSourceData = activeParcel[sourceParentTable].filter(function (spt) { return ((spt.ParentROWUID == bldgSourceData.ROWUID ) && ((spt.SECT_NUM == secNums || spt.ROWUID == secNums)))})[0];
			area = vectors[v].fixedArea && vectors[v].fixedArea != 0 && vectors[v].fixedArea != -1? parseInt( vectors[v].fixedArea ) : (parseFloat( vectors[v].area() ) );
            label = (vectors[v].code || vectors[v].name).toString().trim();
            label.split('/').forEach(function(lbl) {            		
            	pushed = false;
            	sprocessor(lbl, secNums, area);
            });
        }
		        
        var cat = getCategoryFromSourceTable(sourceTable);
        var field = [];
        fields.forEach(function (f) {
            var fd = getDataField(f, sourceTable);
            field.push({ Id: fd.Id, AssignedName: fd.AssignedName, Name: fd.Name, Value: null });
        });
        
        var insertFunction = function (callback) {
        	var recValue = {};
            recValue = recordValues.pop();
            var parentSourceDatat = recValue.parentSourceData;
            insertNewAuxRecord(null, parentSourceDatat, cat.Id, null, null, function (rowid) {
                var recordData = activeParcel[sourceTable].filter(function (s) { return s.ROWUID == rowid })[0];
                    //parcelId, auxRowId, parentAuxRowId, action, fieldName, fieldId, oldValue, newValue, options, successCallback, failureCallback
                    field[0].Value = recValue.MNC;
                    field[1].Value = recValue.PID;
                    field[2].Value = recValue.BID;
                    field[3].Value = recValue.Label;
                    field[4].Value = recValue.Area;
                    field[5].Value = recValue.SECT_NUM;

                    var tempField = field.slice();
                    var updateFunction = function (callback) {
                        var tfd = tempField.pop();
						ccma.Sync.enqueueParcelChange(activeParcel.Id, rowid, sk.rowid, 'E', tfd.Name, tfd.Id, null, tfd.Value, { source: sourceTable, sourceField: tfd.Name, updateData: true }, function () {
                            if (tempField.length > 0)
                                updateFunction(callback);
                            else if (callback) callback();
                        });
                    }
                    updateFunction(function () {
                        field.forEach(function (fd) {
							recordData[fd.Name] = fd.Value;
                        })
                        if (recordValues.length > 0)
                            insertFunction(callback);
                        else if (sketchRecord.length > 0)
                            processor(sketchRecord, fields, callback);
                        else {
                            if (callback) callback();
                            return;
                        }
                    });

            }, function () {
                //console.error('Error fired during insertion');
            });
        }
        if (recordValues.length > 0)
            insertFunction(callback);
        else if (callback) callback();
    }
    var deleteRecord = function (rowuids, catId, skData, fields) {
        var rowuid = rowuids.pop();
        ccma.Data.Controller.DeleteAuxRecordWithDescendants(catId, rowuid, null, null, function () {
            if (rowuids.length == 0)
                processor(skData, fields, function () {
                    if (callback) callback();
                });
            else
                deleteRecord(rowuids, catId, skData, fields);
        }, null, null, true);
    }
    var resData = [], comData = [], skSourceData, condoData = [], condoUnitData = [], skSourceData, recRowIds = [];

    var fprocessor = function (parentTable, childTable, skData, cat, callback) {
    	skData.forEach(function (sk) {
	        var record = activeParcel[parentTable].filter(function (s) { return s.ParentROWUID == sk.rowid });
	        var records = [];
	        record.forEach(function(rec){
	        	var childTables = rec[childTable]; 
	        	childTables.forEach(function(che){
	        		records.push(che);
	        	});
	        });
	        for (r in records)
	            recRowIds.push(records[r].ROWUID)
	        if (recRowIds.length > 0)
	            deleteRecord(recRowIds, cat.Id, skData, fields, callback);
	        else
	            processor(skData, fields, function () {
	                if (callback) callback();
	            });             
        });
    }
    
    var updatesketchData = function () {
    	sketchData.forEach(function(sk) {
    		skSourceData = activeParcel['CCV_BLDG_CONSTR'].filter(function (skd) { return skd.ROWUID == sk.rowid && skd.CC_Deleted != true; })[0];
    		sk.CNS_USE_APEX = skSourceData.CNS_USE_APEX;
			if (activeParcel[resTable] && activeParcel[resTable].filter(function (res) { return (res.ParentROWUID == skSourceData.ROWUID) && res.CC_Deleted != true }).length > 0)
    			resData.push(sk);
			if (activeParcel[comTable] && activeParcel[comTable].filter(function (com) { return (com.ParentROWUID == skSourceData.ROWUID) && com.CC_Deleted != true }).length > 0)
    			comData.push(sk);
			if (activeParcel[condoTable] && activeParcel[condoTable].filter(function (condo) { return (condo.ParentROWUID == skSourceData.ROWUID) && condo.CC_Deleted != true }).length > 0)
    			condoData.push(sk);
			if (activeParcel[condoUnitTable] && activeParcel[condoUnitTable].filter(function (condounit) { return (condounit.ParentROWUID == skSourceData.ROWUID) && condounit.CC_Deleted != true }).length > 0)
    			condoUnitData.push(sk);
    	});
    	
    	if (resData.length > 0) fprocessor(resTable, 'CCV_SUBAREA_RES', resData, getCategoryFromSourceTable('CCV_SUBAREA_RES'), function () {
        	if (comData.length > 0) fprocessor(comTable, 'CCV_SUBAREA_COM', comData, getCategoryFromSourceTable('CCV_SUBAREA_COM'), function () {
            	if (condoData.length > 0) fprocessor(condoTable, 'CCV_SUBAREA_CDM', condoData, getCategoryFromSourceTable('CCV_SUBAREA_CDM'), function () {
            		if (condoData.length > 0) fprocessor(condoUnitTable, 'CCV_SUBAREA_CDU', condoUnitData, getCategoryFromSourceTable('CCV_SUBAREA_CDU'), function () {
            			if (callback) callback();
        			})
					else if (callback) callback(); 
        		})
				else if (callback) callback();   
        	})
        	else if (callback) callback();
    	})
    	else if (comData.length > 0) fprocessor(comTable, 'CCV_SUBAREA_COM', comData, getCategoryFromSourceTable('CCV_SUBAREA_COM'), function () {
        	if (condoData.length > 0) fprocessor(condoTable, 'CCV_SUBAREA_CDM', condoData, getCategoryFromSourceTable('CCV_SUBAREA_CDM'), function () {
            	if (condoData.length > 0) fprocessor(condoUnitTable, 'CCV_SUBAREA_CDU', condoUnitData, getCategoryFromSourceTable('CCV_SUBAREA_CDU'), function () {
            		if (callback) callback();
        		})
				else if (callback) callback(); 
        	})
			else if (callback) callback(); 
    	})
    	else if (condoData.length > 0) fprocessor(condoTable, 'CCV_SUBAREA_CDM', condoData, getCategoryFromSourceTable('CCV_SUBAREA_CDM'), function () {
            if (condoData.length > 0) fprocessor(condoUnitTable, 'CCV_SUBAREA_CDU', condoUnitData, getCategoryFromSourceTable('CCV_SUBAREA_CDU'), function () {
            	if (callback) callback();
        	})
			else if (callback) callback(); 
        })
        else if (condoData.length > 0) fprocessor(condoUnitTable, 'CCV_SUBAREA_CDU', condoUnitData, getCategoryFromSourceTable('CCV_SUBAREA_CDU'), function () {
            if (callback) callback();
        })
    	else if (callback) callback();
    
    }
    
    var updateSketchStringData = function (sketchSectNumDatas) {
    	if(sketchSectNumDatas.length == 0){ 
    		updatesketchData();
    		return;
    	}
    	var sksectData = sketchSectNumDatas.pop();
    	var sksectDa = '';
    	var CCField = getDataField('CC_SKETCH', 'CCV_BLDG_CONSTR');
    	var sktch = sketchApp.sketches.filter(function(tes) {return tes.uid == sksectData.rowid})[0];
    	var BldRecord = activeParcel['CCV_BLDG_CONSTR'].filter(function (skd) { return skd.ROWUID == sksectData.rowid && skd.CC_Deleted != true; })[0];
		sktch.vectors.forEach(function(vect) {
			var tvectString = vect.vectorString;
			if(tvectString){			
				if(vect.wasModified){
					var sectn = vect.sectionNum;
					var temp_str = tvectString.split('[')[1].split(']')[0].split(/\,/);
					temp_str[1] = sectn;
					sksectDa = sksectDa + tvectString.split('[')[0] + '[' + temp_str + ']'+ tvectString.split(']')[1] + ';';
				}
				else
					sksectDa = sksectDa + tvectString + ';';
			}
		});

		let nts = sktch.notes.filter((vn) => { return !vn.isDeleted });
		nts.forEach((nt) => {
			if (nt.noteData) sksectDa += nt.noteData;
		});

		update_data(CCField, sksectDa, BldRecord, function () {
			updateSketchStringData(sketchSectNumData);
		});
    }
    updateSketchStringData(sketchSectNumData);

}

function HennepinSketchAfterSave(sketchData, callback) {
    if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }
    var errRecords = [], isBristol = ( ( clientSettings.SketchConfig == 'Bristol' || sketchSettings["SketchConfig"] == 'Bristol' )? true: false );
    var fields = ['MNC', 'PID', 'BID', 'SUB_CODE', 'SUB_AREA_GROSS'];
    var resTable = (isBristol ? 'CCV_CONSTR_RESDEP': 'CCV_CONSTRSECTION_RES');
    var comTable = (isBristol ? 'CCV_CONSTR_COMDEP': 'CCV_CONSTRSECTION_COM');
    var processor = function (sketchRecord, fields, callback) {
        line = 0;
        var sk = sketchRecord.pop();
        var sourceTable, parentSourceData;
        var bldgSourceData = activeParcel['CCV_BLDG_CONSTR'].filter(function (a) { return a.ROWUID == sk.rowid })[0];
		var resData = activeParcel[resTable]? activeParcel[resTable].filter(function (com) { return com.BID == bldgSourceData.BID }): [];
		var comData = activeParcel[comTable]? activeParcel[comTable].filter(function (com) { return com.BID == bldgSourceData.BID }): [];
        var recordDeleted = false;
        if (resData.length > 0) {
        	parentSourceData = resData[0];
        	sourceTable = 'CCV_SUBAREA_RES';
        }
        else if(comData.length > 0) {
        	parentSourceData = comData[0];
        	sourceTable = 'CCV_SUBAREA_COM';
        }
        var vectors = sketchApp.sketches.filter(function (s) { return s.sid == sk.rowid })[0].vectors;
        vectors = vectors.filter(function(svs) { return !(svs.isFreeFormLineEntries) });
        var area = 0, label = '', recordValues = [], pushed = false;
        var sprocessor = function (lbl) {
            for (var r in recordValues) {
                if (recordValues[r].Label == lbl) {
                    recordValues[r].Area += area;
                    pushed = true;
                }
            }
            if (!pushed) recordValues.push({ Label: lbl, Area: area, BID: parentSourceData.BID, PID: parentSourceData.PID, MNC: parentSourceData.MNC });
        }
        for (v = 0; vectors[v] != null; v++) {
            area = (isBristol? Math.round(parseFloat(vectors[v].area())): parseFloat(vectors[v].area()));
            label = (vectors[v].code || vectors[v].name).toString().trim();
            if (isBristol) {
            	label.split('/').forEach(function(lbl) {            		
            		pushed = false;
            		sprocessor(lbl, area);
            	});
            }
            else {
            	pushed = false;
            	sprocessor(label, area);
            }
        }
		        
        var cat = getCategoryFromSourceTable(sourceTable);
        var field = [];
        fields.forEach(function (f) {
            var fd = getDataField(f, sourceTable);
            field.push({ Id: fd.Id, AssignedName: fd.AssignedName, Name: fd.Name, Value: null });
        })
        recordValues = recordValues.reverse();
        var insertFunction = function (callback) {
            insertNewAuxRecord(null, parentSourceData, cat.Id, null, null, function (rowid) {
                var recordData = activeParcel[sourceTable].filter(function (s) { return s.ROWUID == rowid })[0];
                if (recordValues.length > 0) {
                    var recValue = {};
                    recValue = recordValues.pop();
                    //parcelId, auxRowId, parentAuxRowId, action, fieldName, fieldId, oldValue, newValue, options, successCallback, failureCallback
                    field[0].Value = recValue.MNC;
                    field[1].Value = recValue.PID;
                    field[2].Value = recValue.BID;
                    field[3].Value = recValue.Label;
                    field[4].Value = recValue.Area;

                    var tempField = field.slice();
                    var updateFunction = function (callback) {
                        var tfd = tempField.pop();
						ccma.Sync.enqueueParcelChange(activeParcel.Id, rowid, sk.rowid, 'E', tfd.Name, tfd.Id, null, tfd.Value, { source: sourceTable, sourceField: tfd.Name, updateData: true }, function () {
                            if (tempField.length > 0)
                                updateFunction(callback);
                            else if (callback) callback();
                        });
                    }
                    updateFunction(function () {
                        field.forEach(function (fd) {
							recordData[fd.Name] = fd.Value;
                        })
                        if (recordValues.length > 0)
                            insertFunction(callback);
                        else if (sketchRecord.length > 0)
                            processor(sketchRecord, fields, callback);
                        else {
                            if (callback) callback();
                            return;
                        }
                    });
                }

            }, function () {
                //console.error('Error fired during insertion');
            });
        }
        if (recordValues.length > 0)
            insertFunction(callback);
        else if (callback) callback();
    }
    var deleteRecord = function (rowuids, catId, skData, fields) {
        var rowuid = rowuids.pop();
        ccma.Data.Controller.DeleteAuxRecordWithDescendants(catId, rowuid, null, null, function () {
            if (rowuids.length == 0)
                processor(skData, fields, function () {
                    if (callback) callback();
                });
            else
                deleteRecord(rowuids, catId, skData, fields);
        }, null, null, true);
    }
    var resData = [], comData = [], skSourceData;
    sketchData.forEach(function(sk) {
    	skSourceData = activeParcel['CCV_BLDG_CONSTR'].filter(function (skd) { return skd.ROWUID == sk.rowid && skd.CC_Deleted != true; })[0];
    	sk.CNS_USE_APEX = skSourceData.CNS_USE_APEX;
		if (activeParcel[resTable] && activeParcel[resTable].filter(function (res) { return res.BID == skSourceData.BID && res.CC_Deleted != true }).length > 0)
    		resData.push(sk);
		if (activeParcel[comTable] && activeParcel[comTable].filter(function (com) { return com.BID == skSourceData.BID && com.CC_Deleted != true }).length > 0)
    		comData.push(sk);
    });
    var recRowIds = [];
    var fprocessor = function (parentTable, childTable, skData, cat, callback) {
    	skData.forEach(function (sk) {
    		if(sk.CNS_USE_APEX && sk.CNS_USE_APEX.toString() != '1') { 
    			if(callback) callback();
    			return;
			}
	        var records = activeParcel[parentTable].filter(function (s) { return s.ParentROWUID == sk.rowid })[0][childTable];
	        for (r in records)
	            recRowIds.push(records[r].ROWUID)
	        if (recRowIds.length > 0)
	            deleteRecord(recRowIds, cat.Id, skData, fields);
	        else
	            processor(skData, fields, function () {
	                if (callback) callback();
	            });            
        });
    }
    if (resData.length > 0) fprocessor(resTable, 'CCV_SUBAREA_RES', resData, getCategoryFromSourceTable('CCV_SUBAREA_RES'), function () {
        if (comData.length > 0) fprocessor(comTable, 'CCV_SUBAREA_COM', comData, getCategoryFromSourceTable('CCV_SUBAREA_COM'), function () {
            if (callback) callback();
        })
        else if (callback) callback();
    })
    else if (comData.length > 0) fprocessor(comTable, 'CCV_SUBAREA_COM', comData, getCategoryFromSourceTable('CCV_SUBAREA_COM'), function () {
        if (callback) callback();
    })
    else if (callback) callback();
}

function HennepinSketchBeforeSave(sketches, callback) {
	var valid = { isValid: true, message: 'The following label(s) are <b>incorrect</b>: ', auditTrailEntry: 'The following label(s) are incorrect: ' };
	var invalid_label_error = 'Sketch labels are invalid, sketch changes were saved but the SUBAREA information was not updated.';
	var multiple_sectnum_error = 'There are multiple Sections for this Building. The sketch changes were saved but the SubArea information was not updated.';
	var multiple_sectnum_msg = 'There are multiple Sections for this Building. The sketch changes will be saved but the SubArea information will not be updated.';
	if (sketches === undefined || sketches == null) {
		if (callback) callback(true);
		return;
	}
	
	sketches.forEach(function (sk) {
		var svectors = sk.vectors.filter(function(x) { return !(x.isFreeFormLineEntries) });
		svectors.forEach(function (v) {
		    if ( Object.keys( lookup[v.vectorConfig.LabelLookup] ).filter( function ( label ) { return label == v.code; } ).length == 0 )
		    {
				valid.isValid = valid.isValid && false;
				valid.message += (v.name + ', ');
				valid.auditTrailEntry += (v.name + ', ');
			}
		});
	});
	
	var multipleSectnum_validator = function () {
		var hasDuplicate = false;
		sketches.forEach(function (sk) {
			var resData = activeParcel['CCV_BLDG_CONSTR'].filter(function(bldg) { return bldg.ROWUID == sk.sid; })[0]['CCV_CONSTRSECTION_RES'];
			var comData = activeParcel['CCV_BLDG_CONSTR'].filter(function(bldg) { return bldg.ROWUID == sk.sid; })[0]['CCV_CONSTRSECTION_COM'];
			var sectNum = [];
			if(resData && resData.length > 0) 
				sectNum = resData.map(function (res) { return res.SECT_NUM });				
			else if(comData && comData.length > 0) 
				sectNum = comData.map(function (com) { return com.SECT_NUM });
			
			hasDuplicate = (new Set(sectNum).size !== sectNum.length);
			//There are multiple Sections for this Building. The sketch changes will be saved but the SubArea information will not be updated.
		});
		return hasDuplicate;
	}
	valid.message = valid.message.substring(0, valid.message.length - 2) + '. Tap "OK" and select the sketch segment, press "Edit Label" and select a sketch label from one in the list.';
	valid.message += '<br/><br/><span style="color:red">If you do not correct these labels your Subarea information will not be updated.</span>';
	valid.auditTrailEntry = valid.auditTrailEntry.substring(0, valid.auditTrailEntry.length - 2) + '. If you do not correct these labels your Subarea information will not be updated.'
	if (multipleSectnum_validator()) {
	    messageBox(multiple_sectnum_msg, ['OK'], function () {
	        if (callback) callback(false, true, multiple_sectnum_error);
	    });
	}
	else if (!valid.isValid) {
	    messageBox(valid.message, ['OK'], function () {
	        if (callback) callback(false, true, null, valid.auditTrailEntry);
	    });
	    //else {
	    //    if (multipleSectnum_validator()) {
	    //        if (alert(valid.message))
	    //            if (callback) callback(true, multiple_sectnum_error);
	    //    }
	    //    else if (callback) callback(false);
	    //}
	}
	else {
	    if (callback) callback(true, true);
	}
}

function BrillionSketchBeforeSave(sketches, callback) {
	var valid = { isValid: true, message: 'The following label(s) are <b>incorrect</b>: ', auditTrailEntry: 'The following label(s) are incorrect: ' };
	var invalid_label_error = 'Sketch labels are invalid, sketch changes were saved but the SUBAREA information was not updated.';
	var multiple_sectnum_error = 'There are multiple Sections for this Building. The sketch changes were saved but the SubArea information was not updated.';
	var multiple_sectnum_msg = 'There are multiple Sections for this Building. The sketch changes will be saved but the SubArea information will not be updated.';
	if (sketches === undefined || sketches == null) {
		if (callback) callback(true);
		return;
	}
	
	sketches.forEach(function (sk) {
		sk.vectors.forEach(function (v) {
		    if ( Object.keys( lookup[v.vectorConfig.LabelLookup] ).filter( function ( label ) { return label == v.code; } ).length == 0 ){
				valid.isValid = valid.isValid && false;
				valid.message += (v.name + ', ');
				valid.auditTrailEntry += (v.name + ', ');
			}
		});
	});
	
	valid.message = valid.message.substring(0, valid.message.length - 2) + '. Tap "OK" and select the sketch segment, press "Edit Label" and select a sketch label from one in the list.';
	
	if (!valid.isValid) {
	    messageBox(valid.message, ['OK'], function () {
	        if (callback) callback(false, true);
	    });
	}
	else {
	    if (callback) callback(true, true);
	}
}

function ClarkKySketchAfterSave(sketchData, callback) {
	if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }
    
    var areaField = null, skSource = {tblResidential: {vectors: []}, tblCommercial: {vectors: []}, tblMobileHome: {vectors: []}, tblFarm: {vectors: []}}, areaFactor = null;
        
    sketchData.forEach(function (sk) {
    	var currSketch = sketchApp.sketches.filter(function (s) { return s.sid == sk.rowid })[0];
    	var vectors = currSketch.vectors;
    	sourceTable = currSketch.config.SketchSource.Table;
    	vectors.forEach(function (v) {
    		var label = v.name? (v.name).toString().trim(): '';
    		skSource[sourceTable].vectors.push({label: label, area: parseFloat(v.area()), FieldName: null, Rowuid: sk.rowid });
    	});
    });
    
    Object.keys(skSource).forEach(function (source) {
	    var areaSummaryFields = ccma.Sketching.Config.sources.filter(function(con) { return con.SketchSource.Table == source })[0].AreaSumFields;
    	skSource[source].vectors.forEach(function (v) {
    		for (field in areaSummaryFields) {
    			if(_.find(areaSummaryFields[field], function(f) { return f == v.label })) {
    				v.FieldName = field;
    				return;
				}
    		}
		});
    });
    
    var sketchSourceData = {};
    
    Object.keys(skSource).forEach(function (sourceTable) {
    	sourceData = skSource[sourceTable];
    	sourceData.vectors.forEach(function (vect) {
    		if (vect.FieldName == null) return;
	    	if (!sketchSourceData[sourceTable]) sketchSourceData[sourceTable] = [];
	    	
	    	vect.area = vect.area * (lookup['FeatureCodes_DCS'][vect.label]? parseFloat(lookup['FeatureCodes_DCS'][vect.label].NumericValue): 1);
    		
    		if (sketchSourceData[sourceTable].length == 0 || sketchSourceData[sourceTable].filter(function (skd) { return skd.FieldName == vect.FieldName }).length == 0)
	    		sketchSourceData[sourceTable].push({ FieldName: vect.FieldName, area: vect.area, Rowuid: vect.Rowuid });
    		
			else
				sketchSourceData[sourceTable].filter(function (skd) { return skd.FieldName == vect.FieldName })[0].area += vect.area;
			
		});
    });
        
    var enqueuePCI = function (sketchData, sourceTable, callback) {
    	if(!sketchData || sketchData.length == 0) { if (callback) callback(); return;}
    	var sk = sketchData.pop();
    	if(!sk || sk.FieldName == null) {
    		if(sketchData.length > 0) enqueuePCI(sketchData, sourceTable, callback)
			else if (callback) callback();
			return;
    	}
    	var fieldId = getDataField(sk.FieldName, sourceTable).Id;
    	console.log(sk.FieldName,fieldId,sk.area);
    	ccma.Sync.enqueueParcelChange(activeParcel.Id, sk.Rowuid, null, 'E', sk.FieldName, fieldId, null, sk.area, { source: sourceTable, sourceField: sk.FieldName, updateData: true }, function () {
			activeParcel[sourceTable].filter(function (s) { return s.ROWUID == sk.Rowuid })[0][sk.FieldName] = sk.area;
			if(sketchData.length > 0) enqueuePCI(sketchData, sourceTable, callback)
			else if (callback) callback();
    	});
    }
    
    var updateFields = function (source, callback) {
    	if (updateAndReplace && sketchData.filter(function (sk) { return sk.source == source; }).length > 0) { 
    		var rowid = sketchData.filter(function (sk) { return sk.source == source; })[0].rowid;
    		var areaSummaryFields = _.clone(ccma.Sketching.Config.sources.filter(function(con) { return con.SketchSource.Table == source })[0].AreaSumFields);
    		var replaceFields = function (source, areaSummaryFields, rowid, callback) {
				var field = Object.keys(areaSummaryFields)[0];
    			var fieldId = getDataField(field, source).Id;
    			if (activeParcel[source].filter(function (s) { return s.ROWUID == rowid })[0][field] != null) {
    				ccma.Sync.enqueueParcelChange(activeParcel.Id, rowid, null, 'E', field, fieldId, null, null, { source: source, sourceField: field, updateData: true }, function () {
						activeParcel[source].filter(function (s) { return s.ROWUID == rowid })[0][field] = null;
						delete areaSummaryFields[field];
						if(Object.keys(areaSummaryFields).length == 0) {
							if (callback) callback();
							return;
						}
						else replaceFields(source, areaSummaryFields, rowid, callback)
    				});
    			}
    			else {
    				delete areaSummaryFields[field];
					if(Object.keys(areaSummaryFields).length == 0) {
						if (callback) callback();
						return;
					}
					else replaceFields(source, areaSummaryFields, rowid, callback)
    			}
    		}
    		replaceFields(source, areaSummaryFields, rowid, function () {
		    	if (sketchSourceData[source] === undefined) {
		    		if (callback) callback(); return;
		    	}
    			enqueuePCI(sketchSourceData[source], source, callback)
    		});
    	}
    	else {    		
	    	if (sketchSourceData[source] === undefined) {
	    		if (callback) callback(); return;
	    	}
    		enqueuePCI(sketchSourceData[source], source, callback)
		}
    }
    
    var updateAndReplace = false;
    if(sketchData.length > 0)
    	messageBox('Replace and Update ALL SqFt fields whether sketched or not', ['OK', 'Cancel'],function () { updateAndReplace = true; preProcessor(); }, function () { updateAndReplace = false; preProcessor(); });
	else if (callback) callback();
		
	var preProcessor = function () {
		updateFields('tblResidential', function() {
			updateFields('tblCommercial', function() {
				updateFields('tblMobileHome', function() {
					updateFields('tblFarm', function() {
						if (callback) callback();
					});
				});
			});
		});
	}
}

function BaldwinSketchAfterSave(sketchData, callback) {
    if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }
    var skSource = [];
    var existingRowuids = [];
    var fieldDetails = [], bldFieldDetails = [], bldDetails = {};
    var apexLookup = lookup['Apex_Area_Codes'];
    var bldUpdated = true, bldROWUID, bldParentRowid;
    var cc_assocField = getDataField('CC_AssociateValues', 'CCV_BLDG_MASTER');
    var bld_master = [];
    var fields = ['AREA_CODE_REST', 'GROSS_AREA', 'MULTIPLIER', 'BUILDING_CODE', 'FLOOR_CODE', 'HEAT_CODE', 'ADJUSTED_AREA', 'AREA_NAME'], field, cat = getCategoryFromSourceTable('APDSKA01');
    var bldFields = ['BASIC_AREA', 'ADJUSTED_AREA', 'SECOND_FLOOR_AREA'];
    var assocVals = []
    
    fields.forEach(function(fname){
    	var field = getDataField(fname, 'APDSKA01');
    	fieldDetails.push({name: fname, assignedName: field.AssignedName, Id: field.Id, value: null})
    });
    
    bldFields.forEach(function(fname){
    	var field = getDataField(fname, 'APDIM301');
    	bldFieldDetails.push({name: fname, assignedName: field.AssignedName, Id: field.Id, value: 0})
    });
    
    sketchData.forEach(function (sk) {
    	var currSketch = sketchApp.sketches.filter(function (s) { return s.sid == sk.rowid })[0];
    	var vectors = currSketch.vectors.filter(function(sv) { return !(sv.isFreeFormLineEntries) });
    	var ch_record = activeParcel['APDIM301'].filter(function(p){ return p.ParentROWUID == sk.rowid; });
    	ch_record.forEach(function(ch){
    		if (ch['APDSKA01']) ch['APDSKA01'].forEach(function(ap){ existingRowuids.push(ap.ROWUID); });
		})
    	var vects = [], cc_assocVal = '';
    	vectors.forEach(function (v) {
    		var label = v.name? v.name.toString().trim(): '';
    		var code = v.code? v.code.toString().trim(): '';
    		var line = (v.associateValues.APDIM301.CARD_LINE == 'null'? null: v.associateValues.APDIM301.CARD_LINE);
    		vects.push({code: v.code, label: label, area: Math.round(parseFloat(v.area())), LINE: line, Rowuid: sk.rowid });
    	});
    	skSource.push({Rowuid: sk.rowid, vectors: vects});
		
		activeParcel['APDIM301'].filter(function(x){ return x.ParentROWUID == sk.rowid }).forEach(function(bld){
			if (!bldDetails[sk.rowid]) bldDetails[sk.rowid] = {};
			if (!bldDetails[sk.rowid][bld.CARD_LINE]) bldDetails[sk.rowid][bld.CARD_LINE] = {};
			if (!bldDetails[sk.rowid][bld.CARD_LINE]['rowid']) {
				bldDetails[sk.rowid][bld.CARD_LINE]['rowid'] = bld.ROWUID;
				bldDetails[sk.rowid][bld.CARD_LINE]['fieldDetails'] = JSON.parse(JSON.stringify(bldFieldDetails));
				bldDetails[sk.rowid][bld.CARD_LINE]['parentRowid'] = bld.ParentROWUID;
			}
		})
    });
    
    sketchApp.sketches.forEach(function(sk) {
    	var assocVal = '';
    	var svectors = sk.vectors.filter(function(sv) { return !(sv.isFreeFormLineEntries) });
    	svectors.forEach(function(v) {
    		assocVal += (v.associateValues.APDIM301.CARD_LINE == 'null'? null: v.associateValues.APDIM301.CARD_LINE) + ',';
    	});
    	var rec = activeParcel.CCV_BLDG_MASTER.filter(function(ac) { return ac.ROWUID == sk.sid })[0];
    	assocVal = assocVal.substring(0, assocVal.length - 1);
    	if (rec.CC_AssociateValues != assocVal) 
    		assocVals.push({ Rowid: sk.sid, assocVal: assocVal, prevVal: rec.CC_AssociateValues, parentRowid: rec.ParentROWUID });
    });
    
	var updateBld = function(skrowid, upCallback) {
		if (!bldDetails[skrowid]) { upCallback(); return; }
		var bldDetail = bldDetails[skrowid];
		var updBldCount = 0, bldCount = Object.keys(bldDetail).length * bldFieldDetails.length;
		for(line in bldDetail) {
			var OVER_AREA_INPUT = false; // If the user has selected 'Y' for the Override Area Input, the sketchsave logic should *not* update or calculate the SQFT Fd-7772
            if (bldDetail[line].rowid != null) {
			    var rec_bld = activeParcel.APDIM301.filter(function(bld){ return bld.ROWUID == bldDetail[line].rowid })[0];
                OVER_AREA_INPUT = ( rec_bld && rec_bld.OVERRIDE_AREA_INPUT && rec_bld.OVERRIDE_AREA_INPUT == "Y" ) ? true: false;
                bldDetail[line].OVER_AREA_INPUT = OVER_AREA_INPUT;
			}
			if( !OVER_AREA_INPUT ){
				bldDetail[line].fieldDetails.forEach(function(bfd) {
					ccma.Sync.enqueueParcelChange(activeParcel.Id, bldDetail[line].rowid, bldDetail[line].parentRowid, 'E', '', bfd.Id, null, bfd.value.toString(), {source: 'APDIM301', sourceField: bfd.name, updateData: true}, function() {
						updBldCount++
						if (updBldCount == bldCount) { 
							var rec = null;
							for(line in bldDetail) {
								if (bldDetail[line].rowid != null && ( !bldDetail[line].OVER_AREA_INPUT)) {
									rec = activeParcel.APDIM301.filter(function(bld){ return bld.ROWUID == bldDetail[line].rowid })[0];
									bldDetail[line].fieldDetails.forEach(function(fd){rec[fd.name]=fd.value.toString(); })
								}
							}
							upCallback();
						}						
					});
				});
			}
            else{
                updBldCount = updBldCount + bldFieldDetails.length;
                if( updBldCount == bldCount )
                    upCallback();
            }
		}
	}
	var auditTrailEntry = [];
    var processor = function(skDatas) {
    	if (skDatas.length == 0) { 
    		auditTrailEntry.forEach(function(aud) { ccma.Sync.enqueueEvent(activeParcel.Id, 'AuditTrailEntry', aud); });
    		if (callback) callback(); 
    		return; 
		}
    	var skData = skDatas.pop(), audMsg = '', segNo = 1;
    	    	
    	var lines = _.uniq(skData.vectors.map(function(v){ return v.LINE }));
    	var insertFunction = function (vectors, skrowid) {
    		if (vectors.length == 0) { 
    			if (audMsg != '') auditTrailEntry.push(audMsg.substring(0, audMsg.length - 2)); 
    			updateBld(skrowid, function() { processor(skDatas); }); 
    			return; 
			}
    		if (audMsg == '') audMsg = 'The segments in the sketch of Bldg Card Number(' + skrowid + ') has the associate vectors correspondingly: '
    		var vect = vectors.pop();
    		audMsg += 'segment[' + (segNo++) + ']-->LINE ' + vect.LINE + ', ';
    		var parentSourceData = activeParcel['APDIM301'].filter(function(p){ return (p.ParentROWUID == skrowid && p.CARD_LINE == vect.LINE); })[0];
    		//bldROWUID = parentSourceData.ROWUID;
    		bldParentRowid = skrowid;
    		fieldDetails.forEach(function(fd){ fd.value = null; })
			insertNewAuxRecord(null, parentSourceData, cat.Id, null, null, function (rowid) {
			    var multiplier = null, desc = null, no_code = false;
			    if ( !apexLookup[vect.code] )
			        no_code = true
			    var b_code = vect.code.substring( 0, 2 )
			    var F_code = vect.code.substring( 2, 3 )
			    var H_code = vect.code.substring( 3, 5 )
			    var A_code = vect.code.substring( 6, 10 );
			    if ( no_code )
			        fieldDetails.filter( function ( fd ) { return fd.name == 'AREA_CODE_REST'; } )[0].value = 'nocal'
    			else {
			        fieldDetails.filter( function ( fd ) { return fd.name == 'AREA_CODE_REST'; } )[0].value = A_code;
    				multiplier = parseFloat(apexLookup[vect.code].NumericValue);
    				desc = apexLookup[vect.code].Name;
    			}	
    			var calc_area = Math.round(vect.area * ((multiplier != null)? multiplier: 1));
    			
    			fieldDetails.filter(function(fd){ return fd.name == 'GROSS_AREA'; })[0].value = vect.area;
    			fieldDetails.filter(function(fd){ return fd.name == 'MULTIPLIER'; })[0].value = multiplier
    			fieldDetails.filter(function(fd){ return fd.name == 'BUILDING_CODE'; })[0].value = (no_code? '0': (isNaN(b_code) == true || b_code == '' ? '99' : b_code ));
    			fieldDetails.filter(function(fd){ return fd.name == 'FLOOR_CODE'; })[0].value = (isNaN(F_code) == true || F_code == '' ? '1' : F_code);
    			fieldDetails.filter(function(fd){ return fd.name == 'HEAT_CODE'; })[0].value = ((isNaN(H_code) == true || no_code || H_code == '') ? '0' : H_code);
    			fieldDetails.filter(function(fd){ return fd.name == 'ADJUSTED_AREA'; })[0].value = calc_area;
    			fieldDetails.filter(function(fd){ return fd.name == 'AREA_NAME'; })[0].value = (desc? desc: vect.label)
    			
    			if(apexLookup[vect.code] && apexLookup[vect.code].AdditionalValue1 && (apexLookup[vect.code].AdditionalValue2 != 0 || apexLookup[vect.code].AdditionalValue2 !='0') && b_code != '00') {
	    			if (apexLookup[vect.code].AdditionalValue1 == 'Base')
    					bldDetails[skrowid][vect.LINE].fieldDetails.filter(function(bfd){ return bfd.name == 'BASIC_AREA'; })[0].value += vect.area;
					else if (apexLookup[vect.code].AdditionalValue1 == 'OtherFloor' && F_code != '1')
						bldDetails[skrowid][vect.LINE].fieldDetails.filter(function(bfd){ return bfd.name == 'SECOND_FLOOR_AREA'; })[0].value += calc_area;
					bldDetails[skrowid][vect.LINE].fieldDetails.filter(function(bfd){return bfd.name == 'ADJUSTED_AREA';})[0].value += calc_area;
    			}
    			
    			var updatedCount = 0
    			fieldDetails.forEach(function(tfd) {
					ccma.Sync.enqueueParcelChange(activeParcel.Id, rowid, bldDetails[skrowid][vect.LINE].rowid, 'E', tfd.name, tfd.Id, null, tfd.value, { source: 'APDSKA01', sourceField: tfd.name, updateData: true }, function () {
	    				updatedCount++;
	    				if (updatedCount == fieldDetails.length) { 
	    					var recordData = activeParcel['APDSKA01'].filter(function(rec){ return rec.ROWUID == rowid; })[0];
							fieldDetails.forEach(function (fd) { recordData[fd.name] = fd.value; });
	    					insertFunction(vectors, skrowid);
    					}
	    			});
    			});
    		});
    	}
    	
    	insertFunction(skData.vectors, skData.Rowuid)
    }
    
    var deleteRecord = function () {
    	if (existingRowuids.length == 0) { processor(skSource); return; }
        var rowuid = existingRowuids.pop();
        ccma.Data.Controller.DeleteAuxRecordWithDescendants(cat.Id, rowuid, null, null, function () {
           deleteRecord();
        }, null, null, true);
    }
    
    var updateAssocVal = function() {
    	if (cc_assocField == null || assocVals.length == 0) { deleteRecord(); return; }
    	var assoc = assocVals.pop();
    	ccma.Sync.enqueueParcelChange(activeParcel.Id, assoc.Rowid, assoc.parentRowid, 'E', cc_assocField.Name, cc_assocField.Id, null, assoc.assocVal, { source: 'CCV_BLDG_MASTER', sourceField: cc_assocField.name, updateData: true }, function () {
    		activeParcel['CCV_BLDG_MASTER'].filter(function(bld){ return bld.ROWUID == assoc.Rowid })[0]['CC_AssociateValues'] = assoc.assocVal;
    		updateAssocVal();
    	});
    }
    
    updateAssocVal();
}

function LafayetteSketchAfterSave(sketchData, callback) {
	if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }
    var TotalPerimeter=0,actual_Ar=0,gross_Ar =0,heat_Ar=0,eff_Ar=0;
    var trav_field = getDataField('trav', 'ccv_bld_mstr_bld');
    //var perimeter_Field=getDataField('perimeter', 'ccv_bld_mstr_bld');
   // var actual_Ar_Field=getDataField('act_ar', 'ccv_bld_mstr_bld');
   // var gross_Ar_Field=getDataField('gross_ar', 'ccv_bld_mstr_bld');
   // var heat_Ar_Field=getDataField('heat_ar', 'ccv_bld_mstr_bld');
	var apx_flg_Field=getDataField('apx_flg', 'ccv_bld_mstr_bld');
	var apx_pgs_Field=getDataField('apx_pgs', 'ccv_bld_mstr_bld');
    var trav_val = '', perimeter = '', lkVal = null, saveData = {};
	
	//var fields = ['sar_cd', 'act_ar', 'gross_ar', 'eff_ar', 'heat_ar', 'perimeter'];
	var fields = ['sar_cd'];
	var Bld_Table = "ccv_bld_mstr_bld", SubArea_Table = "bld_sar";
	var subAreaRecord = [], recRowIds = [], skSourceData = [], recordValues = [], activeParcelData = [] ,recordDataValues=[];
	var cat = getCategoryFromSourceTable(SubArea_Table);
	var processor = function (sketchRecord, callback) {
        line = 0;
        var sk = sketchRecord.pop();
        var vectors = sketchApp.sketches.filter(function (s) { return s.sid == sk.ROWUID })[0].vectors;
        vectors = vectors.filter(function(svs) { return !(svs.isFreeFormLineEntries) });
        var field = [];
        fields.forEach(function (f) {
            var fd = getDataField(f, SubArea_Table);
            field.push({Fieldd:fd, Id: fd.Id, AssignedName: fd.AssignedName, Name: fd.Name, Value: null });
        });
        recordDataValues = recordValues.filter(function(recval){ return recval.sketchSid == sk.ROWUID})
        var insertFunction = function (callback) {
            insertNewAuxRecord(null, sk, cat.Id, null, null, function (rowid) {
                var recordData = activeParcel[SubArea_Table].filter(function (s) { return s.ROWUID == rowid })[0];
                if (recordDataValues.length > 0) {
                    var recValue = recordDataValues.pop();
                    //parcelId, auxRowId, parentAuxRowId, action, fieldName, fieldId, oldValue, newValue, options, successCallback, failureCallback
                    field[0].Value = recValue.Label_Name;
                   // field[1].Value = recValue.actual_Ar;
                    //field[2].Value = recValue.gross_Ar;
                   // field[3].Value = recValue.eff_Ar;
                   // field[4].Value = recValue.heat_Ar;
					//field[5].Value = recValue.Total_Perimeter;
                    var tempField = field.slice();
                    var updateFunction = function (callback) {
                        var tfd = tempField.pop();
                        update_data(tfd.Fieldd, tfd.Value, recordData, function() {
                            if (tempField.length > 0)
                                updateFunction(callback);
                            else if (callback) callback();
                        });
                    }
                    updateFunction(function () {
                        if (recordDataValues.length > 0)
                            insertFunction(callback);
                        else if (sketchRecord.length > 0)
                            processor(sketchRecord, callback);
                        else {
                            if (callback) callback();
                            return;
                        }
                    });
                }

            }, function () {
                //console.error('Error fired during insertion');
            });
        }

        if (recordDataValues.length > 0)
            insertFunction(callback);
        else if (callback) callback();
    }


	var deleteRecord = function (rowuids, skData) {
        var rowuid = rowuids.pop();
        ccma.Data.Controller.DeleteAuxRecordWithDescendants(cat.Id, rowuid, null, null, function () {
            if (rowuids.length == 0)
                processor(skData, function () {
                    if (callback) callback();
                });
            else
                deleteRecord(rowuids, skData);
        }, null, null, true);
    }
	
    sketchData.forEach(function(skd) {
    	trav_val = '';
    	sketchApp.sketches.filter(function(sk){ return sk.uid == skd.rowid }).forEach(function(sks){
    		var sksid = sks.sid;
    		var svectors = sks.vectors.filter(function(svs) { return !(svs.isFreeFormLineEntries) });
    		svectors.forEach(function(v) {
				var multi = 1;
    			lkVal = Lafayette_Lookup.filter(function(lk){ return lk.sar_cd == v.code })[0]
    			var sketchPerimeter = parseInt((v.perimeter()).toFixed(0));
    			var sketchArea = parseInt((v.area()).toFixed(0));
    			perimeter = ((lkVal && lkVal.perimeter_flg == 'Y')? ' P' + sketchPerimeter: '')
    			actual_Ar = actual_Ar + sketchArea;
				switch(v.code){
                    case "TWO":
						   multi=2;
        				   break;
    				case "THR":
							multi=3;
        					break;
					case "FOR":
							multi=4;
        					break;
					case "FIV":
							multi=5;
        					break;
					case "SIX":
							multi=6;
        					break;
					default:
        					multi=1;
				}
    			gross_Ar = gross_Ar + (sketchArea * multi);
				if(perimeter != ''){
					heat_Ar = heat_Ar + (sketchArea * multi);
					TotalPerimeter = TotalPerimeter + (perimeter !='' ? sketchPerimeter : 0);
                }
    			trav_val += v.code + '(' + sketchArea + perimeter + ')';
				recordValues.push({ Label:v.code, Label_Name: v.name, gross_Ar: (sketchArea * multi), actual_Ar:sketchArea, eff_Ar:(sketchArea * multi), heat_Ar:perimeter != '' ? ( sketchArea * multi) : 0, Total_Perimeter: perimeter != ''? sketchPerimeter : 0, sketchSid:sksid });
    		});
    	});
    	trav_val = trav_val + '.';
    	saveData[skd.rowid] = {trav_val: trav_val, TotalPerimeter:TotalPerimeter, actual_Ar:actual_Ar, gross_Ar:gross_Ar , heat_Ar:heat_Ar, bld_sar: {}}
		skSourceData.push(activeParcel[Bld_Table].filter(function (s) { return s.ROWUID == skd.rowid })[0]);
    });
	function sketchDataSave(){
		skSourceData.forEach(function(skv) {
			subAreaRecord=activeParcel[Bld_Table].filter(function (s) { return s.ROWUID == skv.ROWUID })[0].bld_sar;
			for (r in subAreaRecord)
				recRowIds.push(subAreaRecord[r].ROWUID); 
    	});
    	if (recRowIds.length > 0)
	        deleteRecord(recRowIds, skSourceData);
		else{
	   		processor(skSourceData, function () {
	        	if (callback) callback();
	  		});
	  	}
	}
    activeParcelData = activeParcel.ccv_bld_mstr_bld;
    var saveCount = Object.keys(saveData).length, index = 0;
    $.each(saveData, function(rowid, sd){
    	update_data(trav_field, sd.trav_val, activeParcelData.filter(function(bld){ return bld.ROWUID == rowid })[0], function() {
    		//update_data(perimeter_Field, sd.TotalPerimeter, activeParcelData.filter(function(bld){ return bld.ROWUID == rowid })[0], function() {
    			//update_data(actual_Ar_Field, sd.actual_Ar, activeParcelData.filter(function(bld){ return bld.ROWUID == rowid })[0], function() {
					//update_data(heat_Ar_Field, sd.heat_Ar, activeParcelData.filter(function(bld){ return bld.ROWUID == rowid })[0], function() {
						update_data(apx_flg_Field, 'Y', activeParcelData.filter(function(bld){ return bld.ROWUID == rowid })[0], function() {
							update_data(apx_pgs_Field, 1, activeParcelData.filter(function(bld){ return bld.ROWUID == rowid })[0], function() {
								//update_data(gross_Ar_Field, sd.gross_Ar, activeParcelData.filter(function(bld){ return bld.ROWUID == rowid })[0], function() {
    								if (++index == saveCount){
    									sketchDataSave();
    								}
								//});
							});
                        });
    				//});
    			//});
    		//});
    	});
    })  
}

function PatriotSketchAfterSave(sketchData, callback) {
	if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        	if (callback) callback();
        	return;
    }
	var area_Table = "CCV_SketchedAreas", area_AlternateTypes_Table = "CCV_SketchedAreaAlternateTypes", buiding_Table = "CCV_Buildings_BuildingDepreciaton";
	var deleteVectorRowuids = [], sketchDataArea_TableRowuids = [], area_cat = getCategoryFromSourceTable(area_Table);
	_PCIBatchArray = [], fieldDetails = [];
    var fields = ['xrSubAreaID', 'Area', 'SizedAdjustedArea', 'AdjustedArea', 'Perimeter', 'IsUnsketchedFlag', 'FinishedArea'];
	var skt_field = getDataField('CC_SKETCH', 'CCV_Sketch');
	var sketchDataCopy = _.clone(sketchData);
	
    fields.forEach(function(fname){
    	fieldDetails.push( getDataField(fname, area_Table) );
    });
	
	var processor = function (sketchDataS, processorCallback) {
        var sk = sketchDataS.pop();
        var sketchDData = sketchApp.sketches.filter(function(x){return x.uid == sk.rowid})[0];
		var bldRecord = sketchDData.parentRow.parentRecord;
        var vectors = sketchDData.vectors;
        var area = 0, label = '', recordValues = [], SubAreaId, patriot_lookup = [], FinishedArea, SizeAdjustedArea, AdjustedArea, Perimeter , pushed = false, IsUnsketchedFlag = 0, labelSketch;
        var newAuxRowuid = ccTicks();
        
        for (v = 0; vectors[v] != null; v++) {
        	var vector = vectors[v], newSketchedAreaId = "", vectorSketchIds = ( vector._SketchedAreaID? vector._SketchedAreaID.split(" "): [] ), isNew = vector.clientId? true: false;
        	area = vector.fixedArea == ''? null: parseFloat( vector.fixedArea? vector.fixedArea: vector.area() );
        	area = ( !isNaN(area) ) ? area: 0;
        	IsUnsketchedFlag = vector.isUnSketchedArea? 0: 1;
        	labelSketch = (vectors[v].code || vectors[v].name).toString().trim();
        	vectorSketchIds = vectorSketchIds.length ? vectorSketchIds: [];
        	labelSketch.split('/').forEach(function(label, index) { 
        		patriot_lookup = Patriot_Lookup.filter(function(pat){return ( (pat.xrSubAreaID == label) || (pat.SubArea == label) ) })[0]; 
        		var _sketchAreaId, sourceRecord;
        		newAuxRowuid = newAuxRowuid + 58;
        		if( isNew ) {
        			_sketchAreaId = newAuxRowuid;
        			newSketchedAreaId += '{$' +  area_Table + '#' + _sketchAreaId + '$} ';
        		}
        		else {
        			_sketchAreaId = vectorSketchIds.shift(); _sketchAreaId = _sketchAreaId? _sketchAreaId: 'new'; 
        			var _skAreaId = ( ( _sketchAreaId.indexOf("#") < 0 )? _sketchAreaId.replace(/[${}]/g,'') : _sketchAreaId.replace(/[${}]/g,'').split("#")[1] );
        			var sk_area_record = bldRecord.CCV_SketchedAreas.filter(function (skd) { return ( skd.SketchedAreaID == _skAreaId || skd.ROWUID == _skAreaId ) })[0];
        			if(sk_area_record){
        				newSketchedAreaId += _sketchAreaId + ' ';
        				_sketchAreaId = sk_area_record.ROWUID;
        				sourceRecord = sk_area_record;
        				sketchDataArea_TableRowuids = sketchDataArea_TableRowuids.filter(function(rowId){ return (rowId != _sketchAreaId)});
        			}
        			else {
        				_sketchAreaId = newAuxRowuid;
        				newSketchedAreaId += '{$' +  area_Table + '#' + _sketchAreaId + '$} ';
        			}
        		}
        		
        		Perimeter = vectors[v].perimeter();
        		if( patriot_lookup ) {
        			SubAreaId = patriot_lookup.xrSubAreaID;
    				SizeAdjustedArea = ( ( !area && area != 0 ) ? null:  ( (patriot_lookup.UseInSizeAdjustmentFlag == 'true') ? (area * patriot_lookup.AdjustSketchFactor) : area ) );
    				FinishedArea = ( ( !area && area != 0 ) ? null: ( patriot_lookup.IsFinishedFlag == 'true'? area: null ) );
    				AdjustedArea = ( ( !area && area != 0 ) ? null: ( patriot_lookup.AdjustSketchFactor? (area * patriot_lookup.AdjustSketchFactor) : area ) );
    				recordValues.push( { xrSubAreaID: SubAreaId, Area: area, SizedAdjustedArea: SizeAdjustedArea, FinishedArea: FinishedArea, AdjustedArea: AdjustedArea, Perimeter: Perimeter, IsUnsketchedFlag: IsUnsketchedFlag, sourceRecord: sourceRecord, rowuid: _sketchAreaId } )   
        		} 
        	});
        	vector._SketchedAreaID = newSketchedAreaId.trim();
        }
        
		recordValues = recordValues.reverse();	        

        function insertFunction () {
	    	var recValue = recordValues.pop();
	    	function _patriotRecordUpdate( recRowuid ) {
	    		var recordData = activeParcel[area_Table].filter(function (s) { return s.ROWUID == recRowuid })[0];
            	fieldDetails.forEach(function(fld) {
            		if( recValue[fld.Name] || recValue[fld.Name] == 0 || recValue[fld.Name] == "" )
            			_update_PCIChanges(fld, recValue[fld.Name], recordData);
            	});	
            	
				if (recordValues.length > 0) insertFunction();
            	else if (sketchDataS.length > 0) processor(sketchDataS, processorCallback);	
           		else if (processorCallback) processorCallback();
	    	}
	    	
	    	if( recValue.sourceRecord )
	    		_patriotRecordUpdate( recValue.rowuid );
	    	else{
	        	insertNewAuxRecord(null, bldRecord, area_cat.Id, recValue.rowuid, null, function (rowid) {
	        		_patriotRecordUpdate( rowid );
	        	});
			}
        }
        
        if (recordValues.length > 0) insertFunction();
        else if (processorCallback) processorCallback();
    }
    
    var _updateFinalPCI = function(){
    	ccma.Sync.enqueueBulkParcelChanges( _PCIBatchArray, function() {  
			getParcel(activeParcel.Id, function () {
				if (callback) callback(); return;
			});
		});
    }
    
	var _updateCCSKETCH = function(){
		sketchDataCopy.forEach(function(skt) {
			var skt = sketchApp.sketches.filter(function(x){return x.uid == skt.rowid})[0];
			var vect = skt.vectors, skRecord = skt.parentRow, sk_string = '';
			vect.forEach(function(vt) {
				var _skSreaId =  vt._SketchedAreaID, vect_string = vt.vectorString;
				if( vect_string ){
					var squarePart = vect_string.split("[")[1].split("]")[0].split(",");
					squarePart[5] = _skSreaId; squarePart = squarePart.toString();
					sk_string += vect_string.split("[")[0] + '[' + squarePart + ']' + vect_string.split("]")[1] + ';';
				}
			});
			_update_PCIChanges(skt_field, sk_string, skRecord);
		});	
		_updateFinalPCI();		
	}
	
	var deleteRecord = function ( area_Rowuids ) {
		if(area_Rowuids.length == 0) {
			_updateCCSKETCH();
			return false;
		}
    	var rowuid = area_Rowuids.pop();
        ccma.Data.Controller.DeleteAuxRecordWithDescendants(area_cat.Id, rowuid, null, null, function () {
        	deleteRecord(area_Rowuids);
        }, null, null, true);
    }
    
	sketchData.forEach(function(skt) {
		var sk = sketchApp.sketches.filter(function(x){return x.uid == skt.rowid})[0];
		var pRecord = sk.parentRow.parentRecord;
    	var sketched_Areas_Records = [];
		if( pRecord && pRecord.CCV_SketchedAreas )
			sketched_Areas_Records = pRecord.CCV_SketchedAreas.filter(function (skd) { return (skd.CC_Deleted != true || skd.CC_Deleted != "true") });
		for (r in sketched_Areas_Records)
			sketchDataArea_TableRowuids.push(sketched_Areas_Records[r].ROWUID)
	});	
	
	processor(sketchData, function (){
		deleteRecord( sketchDataArea_TableRowuids );
	});

}

function FTC_associate_vector_config(sketchApp, sourceTable, SketchAssociateFields) {
	var valid = true, html = '', fieldHtml = '', toHideDropDown = 0;
	sketchApp.sketches.filter(function (sk) { return (sk.isModified || (sk.sid == sketchApp.currentSketch.sid)); }).forEach(function (sketch, i) {
		fieldHtml = '';
		var rec = [];
		sketch.vectors.filter(function (v) { return (!v.isFreeFormLineEntries) }).filter(function (vect) { return (vect.isModified || (sketch.sid == sketchApp.currentSketch.sid)); }).forEach(function (vectort) {
			var code = (vectort.code && vectort.code != '') ? vectort.code : vectort.name
			var vector_descrp = (lookup['AREA_CODES'] && lookup['AREA_CODES'][code]?.['Name']) ? lookup['AREA_CODES'][code]['Name'] : (vectort.name ? vectort.name : '');
			var apexLink, recd = [], activeFlag;
			var save_dtl_lookup = FTC_Detail_Lookup.filter(function (lkup) { return (lkup.IMPDETAILDESCRIPTION == vector_descrp && lkup.APEXAREACODE == vectort.code) })[0];
			if (!save_dtl_lookup)
				save_dtl_lookup = FTC_Detail_Lookup.filter(function (lkup) { return (lkup.APEXAREACODE == null && lkup.IMPDETAILDESCRIPTION == vector_descrp) })[0];
			var save_add_on_lookup = FTC_AddOns_Lookup.filter(function (lkup) { return lkup.ADDONCODE == code; })[0];
			var save_floor_lookup = FTC_Floor_Lookup.filter(function (lkup) { return lkup.IMPSFLOORDESCRIPTION == vector_descrp; })[0];
			if (sourceTable == 'CCV_IMPDETAIL_ATTACHED_DETACHED') {
				if (save_dtl_lookup) {
					apexLink = save_dtl_lookup.APEXLINKFLAG;
					activeFlag = save_dtl_lookup.ACTIVEFLAG;
				}
			}
			else if (sourceTable == 'CCV_IMPDETAIL_ADD_ONS') {
				if (save_add_on_lookup) {
					apexLink = save_add_on_lookup.APEXLINKFLAG;
					activeFlag = save_add_on_lookup.ACTIVEFLAG;
				}
			}
			else if (sourceTable == 'IMPBUILTASFLOOR') {
				if (save_floor_lookup) {
					apexLink = save_floor_lookup.APEXLINKFLAG;
					activeFlag = save_floor_lookup.ACTIVEFLAG;
				}
			}
			if (apexLink == 1 && activeFlag == 1) {
				var blt_rec = activeParcel.IMPBUILTAS.filter(function (b_rec) { return b_rec.ParentROWUID == sketch.parentRow.ParentROWUID })[0];
				if (sourceTable == 'IMPBUILTASFLOOR' && blt_rec) {
					recd = blt_rec[sourceTable].filter(function (blt_r) { return blt_r.IMPSFLOORDESCRIPTION == vector_descrp; });
					recd.forEach(function (recort) {
						var sameRowuid = 0;
						rec.forEach(function (ree) {
							if (ree.ROWUID == recort.ROWUID)
								sameRowuid = 1;
						});
						if (sameRowuid == 0)
							rec.push(recort)
					});
				}
				else if (sourceTable == 'CCV_IMPDETAIL_ATTACHED_DETACHED' || sourceTable == 'CCV_IMPDETAIL_ADD_ONS') {
					recd = activeParcel[sourceTable].filter(function (src_rec) { return src_rec.ParentROWUID == sketch.parentRow.ParentROWUID && (src_rec.ADDONCODE == code || src_rec.IMPDETAILDESCRIPTION == vector_descrp); });
					recd.forEach(function (recort) {
						var sameRowuid = 0;
						rec.forEach(function (ree) {
							if (ree.ROWUID == recort.ROWUID)
								sameRowuid = 1;
						});
						if (sameRowuid == 0)
							rec.push(recort)
					});
				}
			}
		});
		sketch.vectors.filter(function (vect) { return (vect.isModified || (sketch.sid == sketchApp.currentSketch.sid)); }).forEach(function (vector, j) {
			var opt = '', code = (vector.code && vector.code != '') ? vector.code : vector.name;
			valid = false;
			//if(vector.code=="NCA" ||vector.code=="LAND" ||vector.code=="OTH"||vector.code=="SITE"||vector.code=="UND" ||vector.code=="REV")
			//return;
			if ((!vector.isFreeFormLineEntries) && (lookup['AREA_CODES'][code] || vector.name)) {
				if (vector.associateValues === undefined) vector.associateValues = {};
				var addVal = lookup['AREA_CODES'][code]?.AdditionalValue1 ? lookup['AREA_CODES'][code].AdditionalValue1 : "";

				var vector_descrp = lookup['AREA_CODES'][code]?.['Name'] ? lookup['AREA_CODES'][code]['Name'] : (vector.name ? vector.name : "");
				var apexLink, identifyApex = 0, activeFlag;
				var save_dtl_lookup = FTC_Detail_Lookup.filter(function (lkup) { return (lkup.IMPDETAILDESCRIPTION == vector_descrp && lkup.APEXAREACODE == vector.code) })[0]
				if (!save_dtl_lookup)
					save_dtl_lookup = FTC_Detail_Lookup.filter(function (lkup) { return (lkup.APEXAREACODE == null && lkup.IMPDETAILDESCRIPTION == vector_descrp) })[0];
				var save_add_on_lookup = FTC_AddOns_Lookup.filter(function (lkup) { return lkup.ADDONCODE == code; })[0];
				var save_floor_lookup = FTC_Floor_Lookup.filter(function (lkup) { return lkup.IMPSFLOORDESCRIPTION == vector_descrp; })[0];
				if (sourceTable == 'CCV_IMPDETAIL_ATTACHED_DETACHED') {
					if (save_dtl_lookup) {
						apexLink = save_dtl_lookup.APEXLINKFLAG;
						activeFlag = save_dtl_lookup.ACTIVEFLAG;
					}
				}
				else if (sourceTable == 'CCV_IMPDETAIL_ADD_ONS') {
					if (save_add_on_lookup) {
						apexLink = save_add_on_lookup.APEXLINKFLAG;
						activeFlag = save_add_on_lookup.ACTIVEFLAG;
					}
				}
				else if (sourceTable == 'IMPBUILTASFLOOR') {
					if (save_floor_lookup) {
						apexLink = save_floor_lookup.APEXLINKFLAG;
						activeFlag = save_floor_lookup.ACTIVEFLAG;
					}
				}
				if (save_dtl_lookup == undefined && save_add_on_lookup == undefined && save_floor_lookup == undefined)
					return
				if (sourceTable == 'CCV_IMPDETAIL_ATTACHED_DETACHED' && save_dtl_lookup == undefined)
					return;
				if (apexLink == '0' || activeFlag == '0')
					var identifyApex = 1;

				if (sourceTable == 'CCV_IMPDETAIL_ATTACHED_DETACHED' && ["GLA", "GBA", "Add On"].indexOf(addVal) == -1)
					valid = true;
				else if (sourceTable == 'CCV_IMPDETAIL_ADD_ONS' && addVal == "Add On")
					valid = true;
				else if (sourceTable == 'IMPBUILTASFLOOR' && ["GLA", "GBA"].indexOf(addVal) > -1)
					valid = true;
				if (valid) {

					if (identifyApex == 1) {
						opt += '<tr sketch="' + i + '" vector="' + j + '" apexlink="' + 0 + '" style="display:none;">';
						identifyApex = 0;
						if (toHideDropDown != 1)
							toHideDropDown = 2;
					}
					else {
						opt += '<tr sketch="' + i + '" vector="' + j + '">';
						toHideDropDown = 1;
					}
					if (fieldHtml == '') {
						$.each(SketchAssociateFields, function (key, table) {
							fieldHtml += '<td><select class="popupselect1" sourceTable="' + sourceTable + '" fieldName="' + key + '" style="width: ' + ((key.indexOf('-') > -1) ? '200' : '100') + 'px">';
							fieldHtml += '<option value="0" value1=""></option>';
							if (table == 'IMPBUILTASFLOOR' && key == "IMPSFLOORDESCRIPTION-BLTASSF") {
								key = "IMPSFLOORDESCRIPTION-BLTASFLOORSF";
							}
							var field = key.split('/')[0];
							var isRowuid = key.split('/')[1] ? true : false;
							var value = rec.map(function (r) {
								var val = field.split('-').map(function (fld) { return (r[fld] ? r[fld] : (r[fld] === null ? 'null' : '')) }).join(' - ');
								return { [field]: val, rowid: r.ROWUID }
							});
							if (table == 'IMPBUILTAS' && blt_rec) {
								blt_rec.IMPBUILTASFLOOR.forEach(function (flr, idx) { fieldHtml += '<option rowid="' + flr.ROWUID + '" value="' + (idx + 1) + '" value1="' + blt_rec[field] + '">' + blt_rec[field] + (isRowuid ? '/' + flr.ROWUID : '') + '</option>'; })
							}
							else
								value.forEach(function (a, idx) { fieldHtml += '<option rowid="' + a.rowid + '" value="' + (idx + 1) + '" value1="' + a[field] + '">' + a[field] + (isRowuid ? '/' + a.rowid : '') + '</option>' });
							fieldHtml += '</select></td>';
						});
					}
					opt += ' <td><span>' + vector.code + " - " + (vector.label.split("]").length > 1 ? vector.label.split("]")[1] : vector.label) + '</span></td><td><span class="assoc_area">' + vector.area() + '</span></td>' + fieldHtml;
					opt += '</tr>';
				}
				else {
					if (((apexLink) || apexLink == 0) && sourceTable == 'CCV_IMPDETAIL_ATTACHED_DETACHED' && save_dtl_lookup)
						vector.isKeepAfterSave = true;
					else if ((apexLink || apexLink == 0) && sourceTable == 'CCV_IMPDETAIL_ADD_ONS' && save_add_on_lookup)
						vector.isKeepAfterSave = true;
					else if ((apexLink || apexLink == 0) && sourceTable == 'IMPBUILTASFLOOR' && save_floor_lookup)
						vector.isKeepAfterSave = true;
				}
				html += opt;
			}
		});
	});
	if (toHideDropDown == 1) {
		var tHid = '<tr toHide="' + 0 + '"></tr>'
		html += tHid;
	}
	else if (toHideDropDown == 2) {
		var tHid = '<tr toHide="' + 1 + '"></tr>'
		html += tHid;
	}
	return html;
}


function FTC_associate_autoSelect() {
	$('.divvectors table tr[tohide="'+1+'"]').closest('table').closest('div').css("display","none")
	sketchApp.sketches.filter(function(sk){ return (sk.isModified || (sk.sid == sketchApp.currentSketch.sid)); }).forEach(function(sketch, sk_i) {
		sketch.vectors.filter(function (v){return (!v.isFreeFormLineEntries)}).filter(function(vect){ return (vect.isModified || (sketch.sid == sketchApp.currentSketch.sid)); }).forEach(function(vector, vct_i) {
			if (!vector.vectorString) return;
			var apex_id = vector.vectorString.split('[')[1].split(']')[0].split(/\,/g)[4];
			var row = $('.divvectors tr[sketch="' + sk_i + '"][vector="' + vct_i + '"]').eq(0);
			if (apex_id != -1) {
				var apex = $('select', $(row)).eq(0);
				var value = $('option[value1="' + apex_id + '"]', apex).val();
				$(apex).val(value);
				$(apex).trigger('change');
			}
			else {
			    if ( vector.tableRowId || vector.header.split( '{' )[1] )			    {
					var rowwwid;
					if ( vector.tableRowId ){
					    rowwwid = vector.tableRowId.indexOf( "$" ) == -1 ? vector.tableRowId.split("{")[1].split("}")[0] : vector.tableRowId.split( "#" )[1].split( "$" )[0];
					}
					else{
						if(!vector.isPasteVector){
							var headeridd=vector.header.split('{')[1].split(']')[0].split(/\,/g)[0];
							if(headeridd)
						 		rowwwid=headeridd.indexOf("$")==-1?headeridd.split("}")[0]:headeridd.split("#")[1].split("$")[0];
						}
					}
					var apex = $('select', $(row)).eq(0);
					var value = $('option[rowid="' + rowwwid + '"]', apex).val();
					if(!vector.labelValueEdited){
					$(apex).val(value);
					$(apex).trigger('change');
					}
				}
			}
		});
	});
}
function FTCSketchAfterSave(sketchData, callback) {
	if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }
    var imp_dtl_tbl = 'CCV_IMPDETAIL_ATTACHED_DETACHED', add_on_tbl = 'CCV_IMPDETAIL_ADD_ONS', blt_as_tbl = 'IMPBUILTAS', bltas_flr_tbl = 'IMPBUILTASFLOOR', sketch_tbl = 'IMPAPEXOBJECT';
    
	var i=0, addonDescr;
	var rowidd=[] , rowiddAddon=[],rowiddFloor=[];
	var sketchParentRowid=[];
	var cateIdd=getCategoryFromSourceTable(imp_dtl_tbl).Id;
	var addOnCatId=getCategoryFromSourceTable(add_on_tbl).Id;
	var floorCatId=getCategoryFromSourceTable(bltas_flr_tbl).Id;
	
    var labelValidations = {
    	imp: ['GBA', 'GLA'],
    	det_val: ['BSMT', 'Porch', 'GAR', 'OTH', 'CARPT', 'COM SUB', 'Balcony', 'Storage', 'Add On']
    }
    var sketchSaveData = [], deleted_rec = [], perimeter = 0, area = 0, new_recs = [], refreshParcel = false, sketchSave = [], assoc_val = {}, cc_sketch_save_data = [];
    
	var sketch_field = getDataField('CC_SKETCH',sketch_tbl);
	var perimeter_field = getDataField('IMPPERIMETER', 'IMP');
	var assoc_val_field = getDataField('CC_AssociateValues', 'IMP');
	
	var det_cat = getCategoryFromSourceTable(imp_dtl_tbl);
	var det_area_field = getDataField('DETAILUNITCOUNT', imp_dtl_tbl);
	var det_apexid_field = getDataField('APEXID', imp_dtl_tbl);
	var det_apx_lnk_field = getDataField('APEXLINKFLAG',imp_dtl_tbl);
	var det_descr_field = getDataField('IMPDETAILDESCRIPTION',imp_dtl_tbl);
	var det_type_field = getDataField('IMPDETAILTYPE',imp_dtl_tbl);
	var det_calc_type_field = getDataField('DETAILCALCULATIONTYPE',imp_dtl_tbl);
	var det_type_id_field = getDataField('IMPDETAILTYPEID',imp_dtl_tbl);
	
	var add_on_cat = getCategoryFromSourceTable(add_on_tbl);
	var addon_apexid_field = getDataField('APEXID', add_on_tbl);
	var addon_apx_lnk_field = getDataField('APEXLINKFLAG', add_on_tbl);
	var addOn_descr_field = getDataField('IMPDETAILDESCRIPTION',add_on_tbl);
	//var addOn_type_field = getDataField('IMPDETAILTYPE',add_on_tbl);
	var addOn_type_field = getDataField('ADDONCODE',add_on_tbl);
	var addOn_area_field = getDataField('DETAILUNITCOUNT',add_on_tbl);
	var addOn_filter_type_field = getDataField('ADDONFILTERTYPE',add_on_tbl);
	var addOn_type_id_field = getDataField('IMPDETAILTYPEID',add_on_tbl);
	
	var floor_cat = getCategoryFromSourceTable(bltas_flr_tbl);
	var bltas_area_field = getDataField('BLTASSF',blt_as_tbl);
	var floor_apx_lnk_field = getDataField('APEXLINKFLAG',bltas_flr_tbl);
	var floor_descr_field = getDataField('IMPSFLOORDESCRIPTION',bltas_flr_tbl);
	var floor_story_height_field = getDataField('STORYHEIGHT',bltas_flr_tbl);
	var floor_area_field = getDataField('BLTASFLOORSF',bltas_flr_tbl);
	var floor_apex_id_field = getDataField('APEXID',bltas_flr_tbl);
	
	sketchApp.sketches.filter(function(sk){ return (sk.wasModified || (sk.sid == sketchApp.currentSketch.sid)); }).forEach(function(sketch) {
		var skRowid = sketch.parentRow.ParentROWUID;
		var imp_data = sketch.parentRow.parentRecord;
		sketchParentRowid.push(skRowid);
		
		var DetailrecRecord = [] , AddOnsrecRecord = [], FloorrecRecord = [], _cnBuiltasRec = false;
		if(sketch.sid==sketchApp.currentSketch.sid){
			var DetailRecord = [] , AddOnsRecord = [], FloorRecord = [];
			DetailRecord=activeParcel["CCV_IMPDETAIL_ATTACHED_DETACHED"].filter(function(src_rec){ return src_rec.ParentROWUID == skRowid });
			DetailRecord.forEach(function(recort){
				var sameRowuid = 0,IMPDETAILDESCRIPTION,IMPDETAILTYPEID,apexLink, activeFlag;
				IMPDETAILDESCRIPTION = recort.IMPDETAILDESCRIPTION;
				IMPDETAILTYPEID = recort.IMPDETAILTYPEID;
				var save_dtl_lookup = FTC_Detail_Lookup.filter(function(lkup){ return (lkup.IMPDETAILDESCRIPTION == IMPDETAILDESCRIPTION && lkup.IMPDETAILTYPEID == IMPDETAILTYPEID) })[0]
				if (!save_dtl_lookup)
					save_dtl_lookup = FTC_Detail_Lookup.filter(function(lkup){ return (lkup.APEXAREACODE == null && lkup.IMPDETAILDESCRIPTION == IMPDETAILDESCRIPTION) })[0];
				if(save_dtl_lookup) {
					apexLink = save_dtl_lookup.APEXLINKFLAG;
					activeFlag = save_dtl_lookup.ACTIVEFLAG;
                }
				if(apexLink==1 && activeFlag == 1){
					DetailrecRecord.forEach(function(ree){
						if(ree.ROWUID==recort.ROWUID)
							sameRowuid = 1;
					});
					if (sameRowuid == 0)
						DetailrecRecord.push(recort);
				}
			});
			AddOnsRecord=activeParcel["CCV_IMPDETAIL_ADD_ONS"].filter(function(src_rec){ return src_rec.ParentROWUID == skRowid });
			AddOnsRecord.forEach(function(recort){
				var sameRowuid = 0,IMPDETAILDESCRIPTION,apexLink,ADDONCODE, activeFlag;
				IMPDETAILDESCRIPTION=recort.IMPDETAILDESCRIPTION;
				ADDONCODE=recort.ADDONCODE;
				var save_add_on_lookup = FTC_AddOns_Lookup.filter(function(lkup){ return (lkup.ADDONCODE == ADDONCODE || lkup.ADDONDESCRIPTION == IMPDETAILDESCRIPTION); })[0];
				if(save_add_on_lookup){
					apexLink = save_add_on_lookup.APEXLINKFLAG;
					activeFlag = save_add_on_lookup.ACTIVEFLAG;
                }
				if(apexLink==1 && activeFlag == 1){
					AddOnsrecRecord.forEach(function(ree){
						if(ree.ROWUID==recort.ROWUID)
							sameRowuid = 1;
					});
					if (sameRowuid == 0)
						AddOnsrecRecord.push(recort);
				}
			});
			var IMPBUILTASRecord=[];
			IMPBUILTASRecord=activeParcel["IMPBUILTAS"].filter(function(src_rec){ return src_rec.ParentROWUID == skRowid })[0];
			if(IMPBUILTASRecord)
				FloorRecord=IMPBUILTASRecord.IMPBUILTASFLOOR;
			FloorRecord.forEach(function(recort){
				var sameRowuid = 0,IMPSFLOORDESCRIPTION,IMPDETAILTYPEID,apexLink, activeFlag;
				IMPSFLOORDESCRIPTION=recort.IMPSFLOORDESCRIPTION;
				var save_floor_lookup = FTC_Floor_Lookup.filter(function(lkup){ return lkup.IMPSFLOORDESCRIPTION == IMPSFLOORDESCRIPTION; })[0];
				if(save_floor_lookup){
					apexLink = save_floor_lookup.APEXLINKFLAG;
					activeFlag = save_floor_lookup.ACTIVEFLAG;
                }
				if(apexLink==1  && activeFlag == 1){
					FloorrecRecord.forEach(function(ree){
						if(ree.ROWUID==recort.ROWUID)
							sameRowuid = 1;
					});
					if (sameRowuid == 0)
						FloorrecRecord.push(recort);
				}
			});
			
		}
		else{
			var recdDetail=[],recdFloor=[],recdAddon=[];
			sketch.vectors.filter(function(vect){ return (vect.wasModified); }).forEach(function (vectort) {
				var code = (vectort.code && vectort.code != '')? vectort.code: vectort.name
				var vector_descrp = lookup['AREA_CODES'][code]['Name'];
				var apexLink,recd=[], activeFlag;
				var save_dtl_lookup = FTC_Detail_Lookup.filter(function(lkup){ return (lkup.IMPDETAILDESCRIPTION == vector_descrp && lkup.APEXAREACODE == vectort.code) })[0];
				if (!save_dtl_lookup)
					save_dtl_lookup = FTC_Detail_Lookup.filter(function(lkup){ return (lkup.APEXAREACODE  == null && lkup.IMPDETAILDESCRIPTION == vector_descrp) })[0]
				var save_add_on_lookup = FTC_AddOns_Lookup.filter(function(lkup){ return lkup.ADDONCODE == code; })[0];
			 	var save_floor_lookup = FTC_Floor_Lookup.filter(function(lkup){ return lkup.IMPSFLOORDESCRIPTION == vector_descrp; })[0];
				if(save_dtl_lookup) {
					apexLink = save_dtl_lookup.APEXLINKFLAG;
					activeFlag = save_dtl_lookup.ACTIVEFLAG;
				}
				else if(save_add_on_lookup){
					apexLink = save_add_on_lookup.APEXLINKFLAG;
					activeFlag = save_add_on_lookup.ACTIVEFLAG;
				}
				else if(save_floor_lookup){
					apexLink = save_floor_lookup.APEXLINKFLAG;
					activeFlag = save_floor_lookup.ACTIVEFLAG;
				}
				if(apexLink==1 &&  activeFlag == 1 && save_dtl_lookup){
					recdDetail=activeParcel["CCV_IMPDETAIL_ATTACHED_DETACHED"].filter(function(src_rec){ return src_rec.ParentROWUID == skRowid && src_rec.IMPDETAILDESCRIPTION==vector_descrp; });
					recdDetail.forEach(function(recort){
						var sameRowuid = 0;
						DetailrecRecord.forEach(function(ree){
							if(ree.ROWUID==recort.ROWUID)
								sameRowuid = 1;
						});
						if (sameRowuid == 0)
							DetailrecRecord.push(recort);
					});
				}
				else if(apexLink==1 && activeFlag == 1 && save_add_on_lookup){
					recdAddon=activeParcel["CCV_IMPDETAIL_ADD_ONS"].filter(function(src_rec){ return src_rec.ParentROWUID == skRowid && (src_rec.ADDONCODE ==code ||  src_rec.IMPDETAILDESCRIPTION==vector_descrp); });
					recdAddon.forEach(function(recort){
						var sameRowuid = 0;
						AddOnsrecRecord.forEach(function(ree){
							if(ree.ROWUID==recort.ROWUID)
								sameRowuid = 1;
						});
						if (sameRowuid == 0)
							AddOnsrecRecord.push(recort);
					});
				}
				else if(apexLink==1 && activeFlag == 1 && save_floor_lookup){
					var IMPBUILTASRecord=[];
					IMPBUILTASRecord=activeParcel["IMPBUILTAS"].filter(function(src_rec){ return src_rec.ParentROWUID == skRowid && src_rec.IMPSFLOORDESCRIPTION==vector_descrp; })[0];
					if(IMPBUILTASRecord)
						recdFloor=IMPBUILTASRecord.IMPBUILTASFLOOR;
					recdFloor.forEach(function(recort){
						var sameRowuid = 0;
						FloorrecRecord.forEach(function(ree){
							if(ree.ROWUID==recort.ROWUID)
								sameRowuid = 1;
						});
						if (sameRowuid == 0)
							FloorrecRecord.push(recort);
					});
				}

			});
		}

		for(i=0;i<DetailrecRecord.length;i++){
			rowidd.push(DetailrecRecord[i].ROWUID);
	    }
	    for(i=0;i<AddOnsrecRecord.length;i++){
			rowiddAddon.push(AddOnsrecRecord[i].ROWUID);
	    }
	    for(i=0;i<FloorrecRecord.length;i++){
			rowiddFloor.push(FloorrecRecord[i].ROWUID);
	    }
	    
		var bltas_record = imp_data[blt_as_tbl][0]? imp_data[blt_as_tbl][0]: {ROWUID: 'sk-'+skRowid, BLTASSTORYHEIGHT: null, [bltas_flr_tbl]: []};
		area = 0, perimeter = 0;
		sketch.vectors.filter(function(vect){ return (vect.wasModified || (sketch.sid == sketchApp.currentSketch.sid)); }).forEach(function(vector, vect_index) {
			var vectuid=vector.clientId;
			var code = ((vector.code && vector.code != '')? vector.code: vector.name)
			if (!lookup['AREA_CODES'][code]) return;
			/*
			if(vector.code=="NCA" ||vector.code=="LAND" ||vector.code=="OTH"||vector.code=="SITE"||vector.code=="UND" ||vector.code=="REV"){
				cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: null, vect_index: vect_index, tbl: null  });
				return;
			}
			*/
			var condition = false, impFlrDesc;
			var apex_id = vector.vectorString.split('[')[1].split(']')[0].split(/\,/g)[4];
			var assocVal = vector.associateValues;
			var addVal = lookup['AREA_CODES'][code]['AdditionalValue1'];
			var vector_descr = lookup['AREA_CODES'][code]['Name'];
			var save_dtl_lookup = FTC_Detail_Lookup.filter(function(lkup) { return (lkup.IMPDETAILDESCRIPTION == vector_descr && lkup.APEXAREACODE == vector.code) })[0];
			if (!save_dtl_lookup) {
				save_dtl_lookup = FTC_Detail_Lookup.filter(function(lkup) { return (lkup.APEXAREACODE == null && lkup.IMPDETAILDESCRIPTION == vector_descr) })[0];
			}
			var save_add_on_lookup = FTC_AddOns_Lookup.filter(function(lkup){ return lkup.ADDONCODE == code; })[0];
			var save_floor_lookup = FTC_Floor_Lookup.filter(function(lkup){ return lkup.IMPSFLOORDESCRIPTION == vector_descr; })[0];
			var det_type = code, det_type_id = null, det_field_1 = null, apexLink = null, activeFlag = null;
			if (save_dtl_lookup) {
				det_type = save_dtl_lookup.IMPDETAILTYPE;
				det_type_id = save_dtl_lookup.IMPDETAILTYPEID;
				det_field_1= save_dtl_lookup.DETAILCALCULATIONTYPE;
				apexLink = save_dtl_lookup.APEXLINKFLAG;
				activeFlag = save_dtl_lookup.ACTIVEFLAG;
				addonDescr=save_dtl_lookup.IMPDETAILDESCRIPTION;
			}
			else if (save_add_on_lookup) {
				det_type = save_add_on_lookup.ADDONCODE;
				det_type_id = save_add_on_lookup.ADDONCODE;
				det_field_1 = save_add_on_lookup.ADDONFILTERTYPE;
				apexLink = save_add_on_lookup.APEXLINKFLAG;
				activeFlag = save_add_on_lookup.ACTIVEFLAG;
				addonDescr=save_add_on_lookup.ADDONDESCRIPTION;
			}
			else if (save_floor_lookup){ 
				apexLink = save_floor_lookup.APEXLINKFLAG;
				activeFlag = save_floor_lookup.ACTIVEFLAG;
				impFlrDesc=save_floor_lookup.IMPSFLOORDESCRIPTION;
			}
			var GLAIdLabel = (code.indexOf("GLA") > -1 || code.indexOf("GBA") > -1) ? true: false;
			_cnBuiltasRec = ( !_cnBuiltasRec && (GLAIdLabel || (save_dtl_lookup==undefined && save_add_on_lookup==undefined && save_floor_lookup==undefined && ((labelValidations.imp.indexOf(addVal) > -1) )) )) ? true: false;
			area += ((labelValidations.imp.indexOf(addVal) > -1 || GLAIdLabel)? vector.area(): 0);
			perimeter += ((labelValidations.imp.indexOf(addVal) > -1 || GLAIdLabel)? vector.perimeter(): 0);
			
			if(save_dtl_lookup==undefined && save_add_on_lookup==undefined && save_floor_lookup==undefined)
			{
				cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: null, vect_index: vect_index, tbl: null  });
				return;
			}
			
			var areat=vector.area()
			sketch.vectors.filter(function(vect){ return (vect.wasModified || (sketch.sid == sketchApp.currentSketch.sid)); }).forEach(function(vecter) {
				if(vector.uid!=vecter.uid){
					if(vector.associateValues && vecter.associateValues){
						if(vector.associateValues.CCV_IMPDETAIL_ATTACHED_DETACHED&&vecter.associateValues.CCV_IMPDETAIL_ATTACHED_DETACHED){
							if(vector.associateValues.CCV_IMPDETAIL_ATTACHED_DETACHED){
								if(vector.associateValues.CCV_IMPDETAIL_ATTACHED_DETACHED["IMPDETAILTYPE-IMPDETAILDESCRIPTION-DETAILUNITCOUNT"]==vecter.associateValues.CCV_IMPDETAIL_ATTACHED_DETACHED["IMPDETAILTYPE-IMPDETAILDESCRIPTION-DETAILUNITCOUNT"]){
								areat=areat+vecter.area();
								}
							}
						}
					}
				}
			});
			if (assocVal[imp_dtl_tbl]) {
				
				if(apexLink=="0" || activeFlag == "0"){
					if(vectuid)
					{
						cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: null, vect_index: vect_index, tbl: imp_dtl_tbl  });
						return;
					}
					else
					{
							var rowwwId;
							if(vector.header.split( '{' )[1]){
								var headeridd=vector.header.split('{')[1].split(']')[0].split(/\,/g)[0];
								if(headeridd)
						 			rowwwId=headeridd.indexOf("$")==-1?headeridd.split("}")[0]:headeridd.split("#")[1].split("$")[0];
							}
							else
							{
								if(vector.vectorString){
									var apex_id = vector.vectorString.split('[')[1].split(']')[0].split(/\,/g)[4];
									if(apex_id>-1){
										var recordAdd = activeParcel[imp_dtl_tbl].filter(function(r){ return r.APEXID == apex_id })[0];
										if(recordAdd)
											rowwwId=recordAdd.ROWUID;
									}
								}
							}
						sketchSaveData.push({ sourceTable: imp_dtl_tbl, rowid: rowwwId ? rowwwId : null, apexId: apex_id, area: (areat ? areat : vector.area()), parentrowid: skRowid, story_ht: null, description: addonDescr, type: assocVal[imp_dtl_tbl].IMPDETAILTYPE ? assocVal[imp_dtl_tbl].IMPDETAILTYPE : det_type, field_1: det_field_1, apexLink: apexLink, activeFlag: activeFlag, type_id: det_type_id });
								cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: rowwwId?rowwwId:null, vect_index: vect_index, tbl: imp_dtl_tbl  });
						   
					}
				}

				else{
					if (!assocVal[imp_dtl_tbl].rowuid ) {
					//var exist_new = new_recs.filter(function(nw){ return nw.apexId == apex_id && nw.parentrowid == skRowid && nw.sourceTable == imp_dtl_tbl && nw.description == vector_descr; });
					//if (exist_new.length == 0)
						new_recs.push({ area: vector.area(), apexId: apex_id,rowwid:vectuid?vectuid:ccTicks(), parentrowid: skRowid, sourceTable: imp_dtl_tbl, story_ht: null, description: vector_descr, type: det_type, type_id: det_type_id, field_1: det_field_1, vect_string: vector.vectorString, sid: sketch.sid, vect_index: vect_index, apexLink: apexLink, activeFlag: activeFlag });
					//else
						//exist_new[0].area += vector.area();
					}
					else {
						
							if(assocVal[imp_dtl_tbl]["IMPDETAILTYPE-IMPDETAILDESCRIPTION-DETAILUNITCOUNT"]){
								var Typee=assocVal[imp_dtl_tbl]["IMPDETAILTYPE-IMPDETAILDESCRIPTION-DETAILUNITCOUNT"].split('-')[0];
								var Descr=assocVal[imp_dtl_tbl]["IMPDETAILTYPE-IMPDETAILDESCRIPTION-DETAILUNITCOUNT"].split('-')[1];
							}
							if(rowidd){
								rowidd.forEach(function(ridd){
									if(ridd==assocVal[imp_dtl_tbl].rowuid){
										rowidd.splice(rowidd.indexOf(ridd),1);
									}
								});
							}
							sketchSaveData.push({ sourceTable: imp_dtl_tbl, rowid: assocVal[imp_dtl_tbl].rowuid, apexId: apex_id, area:areat?areat:vector.area(), parentrowid: skRowid, story_ht: null, description: assocVal[imp_dtl_tbl].IMPDETAILDESCRIPTION?assocVal[imp_dtl_tbl].IMPDETAILDESCRIPTION:$.trim(Descr), type: assocVal[imp_dtl_tbl].IMPDETAILTYPE?assocVal[imp_dtl_tbl].IMPDETAILTYPE:$.trim(Typee), type_id: det_type_id, field_1: det_field_1, apexLink: apexLink, activeFlag: activeFlag });
							cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: assocVal[imp_dtl_tbl].rowuid, vect_index: vect_index, tbl: imp_dtl_tbl  });
					}
				}
			}
			
			if (assocVal[add_on_tbl]) {
				if(apexLink=="0"||  activeFlag == "0"){
					if(vectuid)
					{
						cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: null, vect_index: vect_index, tbl: add_on_tbl  });
						return;
					}
					else
					{
							var rowwwId;
							if(vector.header.split( '{' )[1]){
								var headeridd=vector.header.split('{')[1].split(']')[0].split(/\,/g)[0];
								if(headeridd)
						 			rowwwId=headeridd.indexOf("$")==-1?headeridd.split("}")[0]:headeridd.split("#")[1].split("$")[0];
							}
							else
							{
								if(vector.vectorString){
									var apex_id = vector.vectorString.split('[')[1].split(']')[0].split(/\,/g)[4];
									if(apex_id>-1){
										var recordAdd = activeParcel[add_on_tbl].filter(function(r){ return r.APEXID == apex_id })[0];
										if(recordAdd)
											rowwwId=recordAdd.ROWUID;
									}
								}
							}
							if(rowwwId)
								sketchSaveData.push({ sourceTable: add_on_tbl, rowid:rowwwId?rowwwId:null, apexId: apex_id, area: ((labelValidations.det_val.indexOf(addVal) > -1)? vector.area(): 0), parentrowid: skRowid, story_ht: null, description:addonDescr, type: assocVal[add_on_tbl].IMPDETAILTYPE?assocVal[add_on_tbl].IMPDETAILTYPE:det_type, field_1: det_field_1, apexLink: apexLink, activeFlag: activeFlag });
							cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: rowwwId?rowwwId:null, vect_index: vect_index, tbl: add_on_tbl  });
						   
					}
				}
				else
				{			
					if (!assocVal[add_on_tbl].rowuid) {
					//var exist_new = new_recs.filter(function(nw){ return nw.apexId == apex_id && nw.parentrowid == skRowid && nw.sourceTable == add_on_tbl && nw.description == vector_descr; });
					//if (exist_new.length == 0)
						new_recs.push({ area: ((labelValidations.det_val.indexOf(addVal) > -1)? vector.area(): 0), apexId: apex_id,rowwid:vectuid?vectuid:ccTicks(), parentrowid: skRowid, sourceTable: add_on_tbl, story_ht: null, story_ht: null, description: vector_descr, type: det_type, field_1: det_field_1, vect_string: vector.vectorString, sid: sketch.sid, vect_index: vect_index, apexLink: apexLink, activeFlag: activeFlag });
					//else
						//exist_new[0].area += vector.area();
					}
					else{
						if(assocVal[add_on_tbl]["IMPDETAILTYPE-IMPDETAILDESCRIPTION-DETAILUNITCOUNT"]){
							var Typee=assocVal[add_on_tbl]["IMPDETAILTYPE-IMPDETAILDESCRIPTION-DETAILUNITCOUNT"].split('-')[0];
							var Descr=assocVal[add_on_tbl]["IMPDETAILTYPE-IMPDETAILDESCRIPTION-DETAILUNITCOUNT"].split('-')[1];
						}
						if($.trim(Descr))
							var save_add_on_lookup = FTC_AddOns_Lookup.filter(function(lkup){ return lkup.ADDONDESCRIPTION == $.trim(Descr); })[0];
							if(rowiddAddon){
								rowiddAddon.forEach(function(ridd){
									if(ridd==assocVal[add_on_tbl].rowuid){
										rowiddAddon.splice(rowiddAddon.indexOf(ridd),1);
									}
								});
						}
						sketchSaveData.push({ sourceTable: add_on_tbl, rowid: assocVal[add_on_tbl].rowuid, apexId: apex_id, area: ((labelValidations.det_val.indexOf(addVal) > -1)? vector.area(): 0), parentrowid: skRowid, story_ht: null, description: assocVal[add_on_tbl].IMPDETAILDESCRIPTION?assocVal[add_on_tbl].IMPDETAILDESCRIPTION:$.trim(Descr), type: assocVal[add_on_tbl].IMPDETAILTYPE?assocVal[add_on_tbl].IMPDETAILTYPE:save_add_on_lookup?save_add_on_lookup.ADDONCODE:det_type, field_1: det_field_1, apexLink: apexLink, activeFlag: activeFlag });
						cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: assocVal[add_on_tbl].rowuid, vect_index: vect_index, tbl: add_on_tbl });
					}
				}
			}
			
			if (assocVal[bltas_flr_tbl] && labelValidations.imp.indexOf(addVal) > -1) {
				if(apexLink=="0" || activeFlag == "0"){
					if(vectuid){
						cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: null, vect_index: vect_index, tbl: bltas_flr_tbl  });
						return;
					}
					else{
						var rowwwId;
						if(vector.header.split( '{' )[1]){
							var headeridd=vector.header.split('{')[1].split(']')[0].split(/\,/g)[0];
							if(headeridd)
						 		rowwwId=headeridd.indexOf("$")==-1?headeridd.split("}")[0]:headeridd.split("#")[1].split("$")[0];
						}
						else{
							if(vector.vectorString){
								var apex_id = vector.vectorString.split('[')[1].split(']')[0].split(/\,/g)[4];
								if(apex_id>-1){
									var recordAdd = activeParcel[bltas_flr_tbl].filter(function(r){ return r.APEXID == apex_id })[0];
									if(recordAdd)
										rowwwId=recordAdd.ROWUID;
								}
							}
						}
						if(rowwwId)
							sketchSaveData.push({ sourceTable: bltas_flr_tbl, rowid: rowwwId?rowwwId:null, apexId: apex_id, area: vector.area(), parentrowid: bltas_record.ROWUID, story_ht: bltas_record.BLTASSTORYHEIGHT, description: impFlrDesc, type: assocVal[bltas_flr_tbl].IMPBUILTASFLOORID, apexLink: apexLink, activeFlag: activeFlag });
						cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: rowwwId?rowwwId:null, vect_index: vect_index, tbl: bltas_flr_tbl  });
						   
					}
				}
				else{
					if (!assocVal[bltas_flr_tbl].rowuid) {
					//var exist_new = new_recs.filter(function(nw){ return nw.apexId == apex_id && nw.parentrowid == skRowid && nw.sourceTable == bltas_flr_tbl && nw.description == vector_descr; });
					//if (exist_new.length == 0)
						new_recs.push({ area: vector.area(), apexId: apex_id,rowwid:vectuid?vectuid:ccTicks() ,parentrowid: bltas_record.ROWUID, sourceTable: bltas_flr_tbl, story_ht: bltas_record.BLTASSTORYHEIGHT, description: vector_descr, vect_string: vector.vectorString, sid: sketch.sid, vect_index: vect_index, apexLink: apexLink, activeFlag: activeFlag });
					//else
						//exist_new[0].area += vector.area();
					}
					else {
						if(assocVal[bltas_flr_tbl]["IMPSFLOORDESCRIPTION-BLTASSF"])
							var descr=assocVal[bltas_flr_tbl]["IMPSFLOORDESCRIPTION-BLTASSF"].split('-')[0]
						if(rowiddFloor){
							rowiddFloor.forEach(function(ridd){
								if(ridd==assocVal[bltas_flr_tbl].rowuid){
									rowiddFloor.splice(rowiddFloor.indexOf(ridd),1);
								}
							});
						}
						sketchSaveData.push({ sourceTable: bltas_flr_tbl, rowid: assocVal[bltas_flr_tbl].rowuid, apexId: apex_id, area: vector.area(), parentrowid: bltas_record.ROWUID, story_ht: bltas_record.BLTASSTORYHEIGHT, description: assocVal[bltas_flr_tbl].IMPSFLOORDESCRIPTION?assocVal[bltas_flr_tbl].IMPSFLOORDESCRIPTION:$.trim(descr), type: assocVal[bltas_flr_tbl].IMPBUILTASFLOORID, apexLink: apexLink, activeFlag: activeFlag });
						cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: assocVal[bltas_flr_tbl].rowuid, vect_index: vect_index, tbl: bltas_flr_tbl  });
					}
			 	}
			}
			//vector.wasModified = false;
		});
		sketchSave.push({ rowid: skRowid, area: Math.round(area), perimeter: Math.round(perimeter), _cnBuiltasRec: _cnBuiltasRec });
		//sketch.wasModified = false;		
	});
	var currRowuid=[];
	var forDelete=[];
	var currentUid=sketchApp.currentSketch.uid;
	var sketchee=sketchApp.sketches.filter(function(x){return x.uid!=currentUid});
	sketchee.forEach(function(sketchees){
		var vectorrs=sketchees.vectors.filter(function(y){return y.isModified!=true})
		vectorrs.forEach(function(vectorrrs){
			var tRowuid;

			if(vectorrrs.tableRowId){
				if(vectorrrs.tableRowId.split('#')[1])
					tRowuid=vectorrrs.tableRowId.split('#')[1].split('$')[0];
				else if(vectorrrs.tableRowId.split('{')[1])
					tRowuid=vectorrrs.tableRowId.split('{')[1].split('}')[0];
				currRowuid.push(tRowuid);
				forDelete.push(tRowuid);
			}
			else if(vectorrrs.vectorString){
				var apex_id = vectorrrs.vectorString.split('[')[1].split(']')[0].split(/\,/g)[4];
				var vector_descrp = lookup['AREA_CODES'][vectorrrs.code]['Name'];
				var code=vectorrrs.code;
				var save_dtl_lookup = FTC_Detail_Lookup.filter(function(lkup){ return (lkup.IMPDETAILDESCRIPTION == vector_descrp && lkup.APEXAREACODE == vectorrs.code) })[0];
				if (!save_dtl_lookup) {
					save_dtl_lookup = FTC_Detail_Lookup.filter(function(lkup){ return (lkup.APEXAREACODE == null && lkup.IMPDETAILDESCRIPTION == vector_descrp) })[0];
				}
				var save_add_on_lookup = FTC_AddOns_Lookup.filter(function(lkup){ return lkup.ADDONCODE == code; })[0];
				var save_floor_lookup = FTC_Floor_Lookup.filter(function(lkup){ return lkup.IMPSFLOORDESCRIPTION == vector_descrp; })[0];
				if(apex_id>-1 && save_dtl_lookup ){
					var recordAdd = activeParcel["CCV_IMPDETAIL_ATTACHED_DETACHED"].filter(function(r){ return r.APEXID == apex_id })[0];
					if(recordAdd)
					tRowuid=recordAdd.ROWUID;
					currRowuid.push(tRowuid);
					forDelete.push(tRowuid);
				}
				else if(apex_id>-1 && save_add_on_lookup ){
					var recordAdd = activeParcel["CCV_IMPDETAIL_ADD_ONS"].filter(function(r){ return r.APEXID == apex_id })[0];
					if(recordAdd)
					tRowuid=recordAdd.ROWUID;
					forDelete.push(tRowuid);
				}
				else if(apex_id>-1 && save_floor_lookup ){
					var recordAdd = activeParcel["IMPBUILTASFLOOR"].filter(function(r){ return r.APEXID == apex_id })[0];
					if(recordAdd)
					tRowuid=recordAdd.ROWUID;
					forDelete.push(tRowuid);
				}

			}

		});
	});
	
	sketchApp.sketches.filter(function(sk){ return (sk.wasModified && (sk.sid != sketchApp.currentSketch.sid)); }).forEach(function(sketchin) {
		sketchin.vectors.filter(function(vect){ return (vect.wasModified); }).forEach(function(vecteri,vect_inde) {
			var assocValu = vecteri.associateValues;
			var rowwwwwId;
			var noRecord=false;
			var code = ((vecteri.code && vecteri.code != '')? vecteri.code: vecteri.name)
			var vector_descr = lookup['AREA_CODES'][code]['Name'];
			var save_dtl_lookup = FTC_Detail_Lookup.filter(function(lkup){ return (lkup.IMPDETAILDESCRIPTION == vector_descr && lkup.APEXAREACODE == vecteri.code) })[0];
			if (!save_dtl_lookup) {
				save_dtl_lookup = FTC_Detail_Lookup.filter(function(lkup){ return (lkup.APEXAREACODE == null && lkup.IMPDETAILDESCRIPTION == vector_descr) })[0];
			}
			var save_add_on_lookup = FTC_AddOns_Lookup.filter(function(lkup){ return lkup.ADDONCODE == code; })[0];
			var save_floor_lookup = FTC_Floor_Lookup.filter(function(lkup){ return lkup.IMPSFLOORDESCRIPTION == vector_descr; })[0];
			if(save_dtl_lookup==undefined && save_add_on_lookup == undefined && save_floor_lookup==undefined)
				noRecord=true;
			if(!noRecord){
				if(_.isEmpty(assocValu)){
					if(vecteri.vectorString.split('#')[1] && vecterin.vectorString.split('#')[1].split('$')[0])
						rowwwwwId=vecteri.vectorString.split('#')[1].split('$')[0];
					cc_sketch_save_data.push({ vect_string: vecteri.vectorString, sid: sketchin.sid, rowid: rowwwwwId?rowwwwwId:null, vect_index: vect_inde, tbl: null  });
				}
			}
		});	
		sketchin.vectors.filter(function(vect){ return (!vect.wasModified); }).forEach(function(vecterin,vect_index) {
			var rowwwwId;
			if(vecterin.vectorString.split('#')[1] && vecterin.vectorString.split('#')[1].split('$')[0])
				rowwwwId=vecterin.vectorString.split('#')[1].split('$')[0];
			cc_sketch_save_data.push({ vect_string: vecterin.vectorString, sid: sketchin.sid, rowid: rowwwwId?rowwwwId:null, vect_index: vect_index, tbl: null  });
		});
	});
	
	
	var rRowuid=_.clone(rowidd);
	if(rRowuid && currRowuid){
		rRowuid.forEach(function(rowiddd){
			var i=0;
			for(i=0;i<currRowuid.length;i++)
			{
				if(rowiddd==currRowuid[i])
				{
					rowidd.splice(rowidd.indexOf(currRowuid[i]),1);
				}
			}
		});
	}
	
	if(rowidd){
		rowidd.forEach(function(rowiddd){
		deleted_rec.push({ rowid: rowiddd, catid:cateIdd});
		});
	}
	if(rowiddAddon)
	{
		rowiddAddon.forEach(function(rowiddd){
		deleted_rec.push({ rowid: rowiddd, catid:addOnCatId});
		});
	}
	if(rowiddFloor)
	{
		rowiddFloor.forEach(function(rowiddd){
		deleted_rec.push({ rowid: rowiddd, catid:floorCatId});
		});
	}
	var update_CC_Sketch = function(skSaveData, index) {
		var save_count = Object.keys(skSaveData).length;
		if (index == save_count) { if (callback) callback(); return; }
		var sid = Object.keys(skSaveData)[index];
		var sourceRecord = activeParcel.IMPAPEXOBJECT.filter(function(obj){ return obj.ROWUID == sid; })[0];
		update_data(sketch_field, skSaveData[sid], sourceRecord, function(){
			update_CC_Sketch(skSaveData, ++index);
		})
	}
	
	var updateRecs = function() {
		if (sketchSaveData.length == 0) { 
			var skSaveData = {};
			sketchApp.sketches.forEach(function(sketch) {
				_.sortBy(cc_sketch_save_data.filter(function(sk){ return sk.sid == sketch.sid }), 'vect_index').forEach(function(skSave){
					var temp_str = skSave.vect_string.split('[')[1].split(']')[0].split(/\,/);
					var vect_str = '';
					if(skSave.rowid==null){
						if(skSave.vect_string.indexOf("{")>-1){
							var t=skSave.vect_string.split('{')[0];
							vect_str=t.substr(0,t.length-1) + ']' +  skSave.vect_string.split(']')[1] + ';'; 
						}
						else
						{
							vect_str=skSave.vect_string + ';';
						}
					}
					else if (skSave.vect_string.indexOf("{")>-1) 
					    vect_str =skSave.vect_string.split('{')[0] + '{$' + skSave.tbl + '#' + skSave.rowid  + '$}]' + skSave.vect_string.split(']')[1] + ';';
					else {
						vect_str = skSave.vect_string.split(']')[0] + ',{$' + skSave.tbl + '#' + skSave.rowid + '$}]' + skSave.vect_string.split(']')[1] + ';';
					}					
					if (skSaveData[skSave.sid]) skSaveData[skSave.sid] += vect_str;
					else skSaveData[skSave.sid] = vect_str;
				});
			});
			update_CC_Sketch(skSaveData, 0);
			return; 
		}
		var rec = sketchSaveData.pop();
		var apex_field = (rec.sourceTable == add_on_tbl? addon_apexid_field: (rec.sourceTable == imp_dtl_tbl? det_apexid_field: floor_apex_id_field));
		var area_field = (rec.sourceTable == add_on_tbl? addOn_area_field: (rec.sourceTable == imp_dtl_tbl? det_area_field: floor_area_field));
		var descr_field = (rec.sourceTable == add_on_tbl? addOn_descr_field: (rec.sourceTable == imp_dtl_tbl? det_descr_field: floor_descr_field));
		var type_field = (rec.sourceTable == add_on_tbl? addOn_type_field: (rec.sourceTable == imp_dtl_tbl? det_type_field: null));
		var type_id_field = (rec.sourceTable == add_on_tbl? addOn_type_id_field: (rec.sourceTable == imp_dtl_tbl? det_type_id_field: null));
		var field1 = (rec.sourceTable == add_on_tbl? addOn_filter_type_field: (rec.sourceTable == imp_dtl_tbl? det_calc_type_field: null));
		var apex_link_field = (rec.sourceTable == add_on_tbl? addon_apx_lnk_field: (rec.sourceTable == imp_dtl_tbl? det_apx_lnk_field: floor_apx_lnk_field));
				
		var modify_record = function() {			
			//if (sketchData.map(function(sk){ return sk.rowid; }).indexOf(parentRowId) > -1) {
				update_data(area_field, Math.round(rec.area), record, function() {
					if (rec.sourceTable == bltas_flr_tbl){
						if(record.CC_RecordStatus == "I" && record.STORYHEIGHT == null){
							update_data(floor_story_height_field, rec.story_ht, record, updateRecs);
						}
						else
							updateRecs();
					}
					else updateRecs();
				});
			//}
			//else updateRecs();
		}
		
		var record = activeParcel[rec.sourceTable].filter(function(r){ return r.ROWUID == rec.rowid })[0];
		if(record){
			update_data(apex_link_field, rec.apexLink, record, function() {
			//if (rec.apexLink == '0') updateRecs();
			//else
				update_data(apex_field, rec.apexId, record, function() {
					update_data(descr_field, rec.description, record, function() {
						if (type_field) {
							update_data(type_field, rec.type, record, function() {
								update_data(type_id_field, rec.type_id, record, function() {
									update_data(field1, rec.field_1, record, modify_record);
								});
							});
						}
						else modify_record();
					});
				});
			});
		}
		else
			updateRecs();
	}
	
	var insertNew = function() {
		if (new_recs.length == 0) { 
			if (refreshParcel) getParcel( activeParcel.Id, updateRecs);
			else updateRecs(); 
			return; 
		}
		var rec = new_recs.pop();
		var parent_data = (rec.sourceTable == imp_dtl_tbl || rec.sourceTable == add_on_tbl)? 'IMP': blt_as_tbl;
		parent_data = activeParcel[parent_data].filter(function(pdata){ return pdata.ROWUID == rec.parentrowid })[0];
		var category = ((rec.sourceTable == imp_dtl_tbl)? det_cat: (rec.sourceTable == add_on_tbl? add_on_cat: floor_cat));
		var autoInsertProp = (category.AutoInsertProperties && JSON.parse(category.AutoInsertProperties.replace(/'/g, '"')));
		var reloadParcel = (autoInsertProp && autoInsertProp.fieldsToCopy && autoInsertProp.table.trim() != '')? true: false;
		insertNewAuxRecord(null, parent_data, category.Id, rec.rowwid, null, function (rowid) {
			refreshParcel = reloadParcel || refreshParcel;
			sketchSaveData.push({ rowid: rowid, apexId: rec.apexId, area: rec.area, parentrowid: rec.parentrowid, sourceTable: rec.sourceTable, story_ht: rec.story_ht, description: rec.description, type: rec.type, type_id: rec.type_id, field_1: rec.field_1, apexLink: rec.apexLink, activeFlag: rec.activeFlag });
			insertNew();
			cc_sketch_save_data.push({ vect_string: rec.vect_string, sid: rec.sid, rowid: rowid, vect_index: rec.vect_index, tbl: rec.sourceTable, });
		}, null, reloadParcel);
	}
	
	var deleteRecs = function() {
		if (deleted_rec.length == 0) { 
			if (refreshParcel) getParcel(activeParcel.Id, insertNew);
			else insertNew(); 
			return; 
		}
		var del_rec = deleted_rec.pop();
		ccma.Data.Controller.DeleteAuxRecordWithDescendants(del_rec.catid, del_rec.rowid, null, null, function(){
			refreshParcel = true; deleteRecs();
		});
	}
	var update_sketchData = function() {
		if (sketchSave.length == 0) { deleteRecs(); return; }
 		var skData = sketchSave.pop();
 		var imp_data = activeParcel.IMP.filter(function(imp){ return imp.ROWUID == skData.rowid; })[0];
		update_data(perimeter_field, skData.perimeter, imp_data, function() {
			update_data(assoc_val_field, skData.assoc_val, imp_data, function() {
				if (imp_data[blt_as_tbl][0]) 
					update_data(bltas_area_field, skData.area, imp_data[blt_as_tbl][0], update_sketchData);
				else if (new_recs.filter(function(nw_rec){ return nw_rec.parentrowid == 'sk-' + skData.rowid }).length > 0){ 
					var neww_rec=new_recs.filter(function(nw_rec){ return nw_rec.parentrowid == 'sk-' + skData.rowid })
					insertNewAuxRecord(null, imp_data, getCategoryFromSourceTable(blt_as_tbl).Id, null, null, function (rowid) {
						new_recs.filter(function(nw_rec){ return nw_rec.parentrowid == 'sk-' + skData.rowid }).forEach(function(new_rec) {
							new_rec.parentrowid = rowid;
						});
						var blt_data = activeParcel[blt_as_tbl].filter(function(blt){ return blt.ROWUID == rowid; })[0];
						update_data(bltas_area_field, skData.area, blt_data, update_sketchData);
					});
				}	
				else if( skData._cnBuiltasRec ) {
					insertNewAuxRecord(null, imp_data, getCategoryFromSourceTable(blt_as_tbl).Id, null, null, function (rowid) {
						var blt_data = activeParcel[blt_as_tbl].filter(function(blt){ return blt.ROWUID == rowid; })[0];
						update_data(bltas_area_field, skData.area, blt_data, update_sketchData);
					});	
				}
				else update_sketchData();
			});
		});
	}
	
	sketchSaveData.forEach(function(skd){
		var impRowid = (skd.sourceTable == bltas_flr_tbl)? activeParcel[blt_as_tbl].filter(function(bltas){ return bltas.ROWUID == skd.parentrowid })[0].ParentROWUID: skd.parentrowid;
		if (!assoc_val[impRowid]) assoc_val[impRowid] = {};
		if (!assoc_val[impRowid][skd.sourceTable]) assoc_val[impRowid][skd.sourceTable] = [];
		assoc_val[impRowid][skd.sourceTable].push(skd.rowid);
	});
	new_recs.forEach(function(skd){
		var impppRowid =skd.parentrowid.split("sk-").length>1?skd.parentrowid.split("sk-")[1]:skd.parentrowid;
		var impRowid = (skd.sourceTable == bltas_flr_tbl)? ((activeParcel[blt_as_tbl].filter(function(bltas){ return bltas.ROWUID == impppRowid })).length>0?activeParcel[blt_as_tbl].filter(function(bltas){ return bltas.ROWUID == impppRowid })[0].ParentROWUID:impppRowid): impppRowid;
		if (!assoc_val[impRowid]) assoc_val[impRowid] = {};
		if (!assoc_val[impRowid][skd.sourceTable]) assoc_val[impRowid][skd.sourceTable] = [];
		assoc_val[impRowid][skd.sourceTable].push(skd.rowwid.toString());
	});
	var isValid = true, oldValue;
	if (assoc_val_field)
		sketchParentRowid.forEach(function(sketchRowwid){
			activeParcel.IMP.filter(function(imps){ return imps.CC_AssociateValues && imps.ROWUID==sketchRowwid }).forEach(function(imp){
				isValid = true
				try {
					oldValue = JSON.parse(imp.CC_AssociateValues);
				} catch(ex) { console.warn(ex); isValid = false; }
				if (isValid)
					$.each(oldValue,function(st, old) {	
						old.forEach(function(oldRowuid){
							if(forDelete.indexOf(oldRowuid)>-1)
							{
								return;
							}						
							var deleteArray=[];			
							if(st=="CCV_IMPDETAIL_ATTACHED_DETACHED")
								deleteArray=assoc_val[imp.ROWUID] && assoc_val[imp.ROWUID].CCV_IMPDETAIL_ATTACHED_DETACHED;
							else if(st=="IMPBUILTASFLOOR")
								deleteArray=assoc_val[imp.ROWUID] && assoc_val[imp.ROWUID].IMPBUILTASFLOOR;
							else if(st=="CCV_IMPDETAIL_ADD_ONS")
								deleteArray=assoc_val[imp.ROWUID] && assoc_val[imp.ROWUID].CCV_IMPDETAIL_ADD_ONS;
							if((deleteArray && deleteArray.indexOf(oldRowuid) == -1)||deleteArray==undefined)
								deleted_rec.push({ rowid: oldRowuid, catid: ((st ==imp_dtl_tbl)? det_cat.Id: (st == add_on_tbl)? add_on_cat.Id: floor_cat.Id) });
						});						
					});	
			});
		});	
	sketchSave.filter(function(sks){ return assoc_val[sks.rowid] }).forEach(function(skSave){
		var assoc_json_val = JSON.stringify(assoc_val[skSave.rowid]);
		skSave['assoc_val'] = (assoc_val[skSave.rowid] && assoc_json_val)? JSON.stringify(assoc_val[skSave.rowid]): null;
	});
	update_sketchData();
}

function WyandottSketchDefinition(data, isChanged) {
	var vector = '', vectorStart = true, sketches = [], vectorSource = ccma.Sketching.Config.sources[0].VectorSource[0], vectorCounter = 0, startPos = '';
	
	var convertToSketch = function(d, type, isAngled, isInverted) {
		var updownData = isInverted? parseInt(d['CASKLDU' + type]) * -1: parseInt(d['CASKLDU' + type]);
		var leftrightData = isInverted? parseInt(d['CASKLRL' + type]) * -1: parseInt(d['CASKLRL' + type]);
		
		return ' ' + (leftrightData > 0? 'R' + leftrightData: 'L' + leftrightData * -1) + (isAngled? '/': ' ') + (updownData > 0? 'D' + updownData: 'U' + updownData * -1);
	}
	
	data.map(function(d){ if (d.CASKLRECNUM) d.CASKLRECNUM = parseInt(d.CASKLRECNUM); })
	
	data = _.sortBy(data, 'CASKLRECNUM')
	data.forEach(function(d){
		++vectorCounter;
		if (d.CASKLDIAG1 != '+' && d.CASKLDIAG1 != '-') {
			if (vectorStart) {
				startPos = convertToSketch(d, 1);
				vector += ':' + startPos + ' S '
				vectorStart = false;
			}
			else vector += convertToSketch(d, 1);
			vector += convertToSketch(d, 2);
		}
		else {
			vector += convertToSketch(d, 1, true);
			vector += convertToSketch(d, 2);			
		}
		if (d.CASKLSQFT != '0') {
		
			if (vectorCounter == 1) vector += convertToSketch(d, 2, false, true);
			
	     	sketches.push({
		        uid: (d.CASKLPARCEL? d.CASKLPARCEL.toString(): ''),
		        label: [{ Field: 'CASKLCONS', Value: d.CASKLCONS, Description: null, IsEdited: isChanged, hiddenFromEdit: vectorSource.false, hiddenFromShow: false, lookup: null, Caption: vectorSource.labelCaption, ShowCurrentValueOnly: vectorSource.ShowCurrentValueOnly, lookUpQuery: null, splitByDelimiter: null, Target: vectorSource.LabelTarget, UseLookUpNameAsValue: null, IsLargeLookup: false }],
		        vector: d.CASKLCONS + '[-1,-1,' + startPos + ']' + vector,
		        labelPosition: null,
		        isChanged: isChanged,
		        vectorConfig: vectorSource
		    });
			vectorStart = true;
			vector = '';
			startPos = '';
			vectorCounter = 0;
	    }
	});
	console.log(sketches);
	return sketches;
}

function WinGapRSSketchAfterSave(sketchData, callback) {
	if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }
    var sketch_Modified = [];
    sketch_Modified = sketchApp.sketches.filter(function(sk){return (sk.wasModified)});
  	sketch_Modified = sketch_Modified ? sketch_Modified : [];
	var updateSketchData = function(sketchDatas) {
		if(sketchDatas.length == 0){
			if(callback) callback();  
            return;
		}
		var skDatas = sketchDatas.pop();
		var SketchTable = skDatas.config.VectorSource[0].Table;
		var existsField = getDataField("EXISTS",SketchTable);
		var RowuiD, Vectors = [], recorData = [];
		Vectors = skDatas.vectors? skDatas.vectors : [];

		var updateVectorData = function(VectorData) {
			if(VectorData.length == 0){
				updateSketchData(sketchDatas);
                return false;
			}
			var VectData = VectorData.pop();
			var VectDataRowuid = VectData.uid.split("/");
			if(VectDataRowuid.length > 1)
				RowuiD = VectData.clientId;
			else
				RowuiD = VectData.uid;
			recorData = activeParcel[SketchTable].filter(function(re){ return re.ROWUID == RowuiD})[0];
			if(recorData){
				update_data(existsField, 1, recorData, function() {
					updateVectorData(VectorData);
				});
			}
			else
				updateVectorData(VectorData);
		}
		updateVectorData(Vectors);
	}
	updateSketchData(sketch_Modified);
}

function PnASketchBeforeSave(sketches, sqcallback) {
	var sketchData = [];
	sketches[0].vectors.filter(function (x) { return (x.isModified && !x.isDeletedVector) }).forEach(function (v){
		sketchData.push({'source': v.sourceName, 'rowid': v.uid})
	})
	bldgSqft = [];
    $('#maskLayer').hide();
	$( '.Current_vector_details .head' ).html( 'Sketch Sqft' )
    $( '.mask' ).show();
    $( '.Current_vector_details' ).css( 'left', '');
    $( '.Current_vector_details' ).css( 'width', 700 );        
    $( '.Current_vector_details' )
    var validation = true;
    $( '.skNote' ).hide(); $( '.Current_vector_details .vectorPage , .Current_vector_details .footradio,.Current_vector_details .obsketch' ).parent().remove()
    $( '.assocValidation,.UnSketched' ).remove();
    $( '.dynamic_prop div' ).remove();
    $('.dynamic_prop').html('')
    $( '.Current_vector_details .PnANewValue ' ).html('');
    $( '.Current_vector_details' ).css( 'max-width', 700 );
    $( '.Current_vector_details' ).show()
    //$( '.Current_vector_details #Btn_cancel_vector_properties' ).hide();
	$( '.Current_vector_details #Btn_cancel_vector_properties' ).css ('margin-right','229px');
	$( '.Current_vector_details #Btn_cancel_vector_properties' ).bind( touchClickEvent, function () {
            $( '.Current_vector_details #Btn_cancel_vector_properties' ).css ('margin-right','');
            $( '.Current_vector_details' ).hide();
            $( '.Current_vector_details .sktsqft' ).remove();
            if ( !sketchApp.isScreenLocked ) $( '.mask' ).hide();
            if (sqcallback) sqcallback(true, false);  
        } )	
    $( '.Current_vector_details .head' ).after( '<div class = "sktsqft"><p>The calculated sqft from the drawing does not match the total sqft on the building. Would you like to update the building sqft to match the sketch area?</p><br><div class = "pnaSqft" style = "max-height: 200px;overflow-y: auto"></div></div>' );
    var html = '<table style="width: 100%;"><tr><th style="background: #d6d6d6;height:35px;width:200px">Building Type/Sequence</th><th style="background: #d6d6d6;height:35px;width:150px">Building Total Sqft</th><th style="background: #d6d6d6;height:35px;width:150px">Sketch Sqft</th><th style="background: #d6d6d6;height:35px;width:150px">Update Building Sqft</th></tr>';
    var value = false;
    sketchData.forEach(function (skDatas,i){
    	var cVector, newSketch = false, NewBldg;
    	cVector = sketchApp.sketches[0].vectors.filter(function(vect){return (vect.uid == skDatas.rowid)})[0];
    	if(cVector.uid.split("/").length > 1){
			BldRowuid =  cVector.clientId;
			newSketch = true;
		}
		else
			BldRowuid =  cVector.uid;
		NewBldg = cVector.BldgIsNew? cVector.BldgIsNew: false;
		BldRec = activeParcel['bldg'] ? activeParcel['bldg'].filter(function(bl){return bl.ROWUID == BldRowuid})[0] :[];
		var blgtotsqft,bldgId;
		if(BldRec){
			blgtotsqft = BldRec['Total_Sqft']? BldRec['Total_Sqft']: 0;
			bldgId = BldRec['Bldg_Id'];
		}
		var area = parseInt(cVector.area());
    	if(!newSketch){
    		if(!NewBldg && blgtotsqft != area){
	    		value = true;
	    		html += '<tr><td><span style="word-break: break-all;text-align: left">'+cVector.labelFields[0].Description.Name+'&nbsp;/&nbsp;'+BldRec.Sequence+'</span</td><td><span style="word-break: break-all;text-align: center">'+blgtotsqft+'</span></td><td><span style="word-break: break-all;text-align: center">'+area+'</span></td><td><select class="sqftddl" style="width: 150px;float: right;height: 20px;"><option value="1" rowid = '+skDatas.rowid+'>Yes</option><option value="0" rowid = '+skDatas.rowid+'>No</option></select></td></tr>' 
	    	}
	    	else
	    		bldgSqft.push({Rec_rowid: skDatas.rowid, sqftUpdate: '2'});
    	}
    })
    html += '</table>';
    $( '.pnaSqft' ).append( html ); 
    if(!value){
      $('#maskLayer').show();
      $( '.Current_vector_details .sktsqft' ).html('');
      $( '.Current_vector_details #Btn_cancel_vector_properties' ).css ('margin-right','');
      $( '.Current_vector_details').hide();
      if (sqcallback) sqcallback(true, true);
    }
    $( '.Current_vector_details #Btn_Save_vector_properties' ).unbind( touchClickEvent )
    $( '.Current_vector_details #Btn_Save_vector_properties' ).bind( touchClickEvent, function () {            
      $('#maskLayer').show(); 
	  $('.pnaSqft .sqftddl').forEach(function (el) {
	  	var rowid = $('option[value="' + $(el).val() + '"]', $(el)).attr('rowid');
	  	var val_update = $(el).val();
	  	bldgSqft.push({Rec_rowid: rowid, sqftUpdate: val_update});
	  });           
        if ( !sketchApp.isScreenLocked ) $( '.mask' ).hide();           
      $( '.Current_vector_details .sktsqft' ).html('');
      $( '.Current_vector_details #Btn_cancel_vector_properties' ).css ('margin-right','');
      $( '.Current_vector_details').hide();
      if (sqcallback) sqcallback(true,true);
    } )      
}

function PnASketchAfterSave(sketchData, callback) {
	if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }
    var updateSketchData = function(sketchDatas) {
		if(sketchDatas.length == 0){
			if(callback) callback();  
            return;
		}
		var skDatas = sketchDatas.pop();
		var cVector = sketchApp.sketches[0].vectors.filter(function(vect){return (vect.uid == skDatas.rowid)})[0];
		var isNewField = getDataField('Is_New','bldg');
		var totSqftField = getDataField('Total_Sqft','bldg');
		var newSqftField = getDataField('New_Sqft','bldg');
		var sqftField = getDataField('SqFt','bldg');
		var sketch_sqftField = getDataField('Sketch_Sqft','bldg');
		if(cVector){
			var newSketch = false, BldRowuid, BldRec = [];
			if(cVector.uid.split("/").length > 1){
				BldRowuid =  cVector.clientId;
				newSketch = true;
			}
			else
				BldRowuid =  cVector.uid;
			BldRec = activeParcel['bldg'] ? activeParcel['bldg'].filter(function(bl){return bl.ROWUID == BldRowuid})[0] :[];
			//BldRec = BldRec ? BldRec : [];
			var blgtotsqft, areaUpdate,sqftRec;
			if(BldRec && bldgSqft){
				sqftRec = bldgSqft.filter(function(x){return x.Rec_rowid == BldRec.ROWUID})[0];
				areaUpdate = sqftRec? sqftRec.sqftUpdate: 0;
				blgtotsqft = BldRec['Total_Sqft'];
			}
			var area = parseInt(cVector.area());
			if(BldRec){
				update_data(sketch_sqftField, area, BldRec, function() {
					if(newSketch){
						var isNewPnASk = cVector.isPNANewValue;
						var isnewUpdate = (isNewPnASk == "1") ? true: false;
						update_data(isNewField, isnewUpdate, BldRec, function() {
							if(isNewPnASk == '0' || !isnewUpdate){
								update_data(sqftField, area, BldRec, function() {
									updateSketchData(sketchDatas);
								});
							}
							else{
								update_data(newSqftField, area, BldRec, function() {
									updateSketchData(sketchDatas);
								});
							}
						});
	
					}
					else{
						var isNewPnASk = BldRec.Is_New;
						if(isNewPnASk == 'false' || isNewPnASk == false){
							if(['1','2',1,2].indexOf(areaUpdate) > -1){
								update_data(sqftField, area, BldRec, function() {
									updateSketchData(sketchDatas);
								});
							}
							else{
								updateSketchData(sketchDatas);
							}
						}
						else{
							if(['1','2',1,2].indexOf(areaUpdate) > -1){
								update_data(newSqftField, area, BldRec, function() {
									updateSketchData(sketchDatas);
								});
							}
							else{
								updateSketchData(sketchDatas);
			        		}
						}
					}
				});
			}
			else
				updateSketchData(sketchDatas);
		}
		else
			updateSketchData(sketchDatas); 
    }
   updateSketchData(sketchData);  
}


function FTCJsonSketchAfterSave(sketchData, callback) {
	if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
		if (callback) callback();
		return;
	}
	var imp_dtl_tbl = 'CCV_IMPDETAIL_ATTACHED_DETACHED', add_on_tbl = 'CCV_IMPDETAIL_ADD_ONS', blt_as_tbl = 'IMPBUILTAS', bltas_flr_tbl = 'IMPBUILTASFLOOR', sketch_tbl = 'IMPAPEXOBJECT';

	var i = 0, addonDescr;
	var rowidd = [], rowiddAddon = [], rowiddFloor = [];
	var sketchParentRowid = [];
	var cateIdd = getCategoryFromSourceTable(imp_dtl_tbl).Id;
	var addOnCatId = getCategoryFromSourceTable(add_on_tbl).Id;
	var floorCatId = getCategoryFromSourceTable(bltas_flr_tbl).Id;

	var labelValidations = {
		imp: ['GBA', 'GLA'],
		det_val: ['BSMT', 'Porch', 'GAR', 'OTH', 'CARPT', 'COM SUB', 'Balcony', 'Storage', 'Add On']
	}
	var sketchSaveData = [], deleted_rec = [], perimeter = 0, area = 0, new_recs = [], refreshParcel = false, sketchSave = [], assoc_val = {}, cc_sketch_save_data = [];

	var sketch_field = getDataField('CC_SKETCH', sketch_tbl);
	var perimeter_field = getDataField('IMPPERIMETER', 'IMP');
	var assoc_val_field = getDataField('CC_AssociateValues', 'IMP');

	var det_cat = getCategoryFromSourceTable(imp_dtl_tbl);
	var det_area_field = getDataField('DETAILUNITCOUNT', imp_dtl_tbl);
	var det_apexid_field = getDataField('APEXID', imp_dtl_tbl);
	var det_apx_lnk_field = getDataField('APEXLINKFLAG', imp_dtl_tbl);
	var det_descr_field = getDataField('IMPDETAILDESCRIPTION', imp_dtl_tbl);
	var det_type_field = getDataField('IMPDETAILTYPE', imp_dtl_tbl);
	var det_calc_type_field = getDataField('DETAILCALCULATIONTYPE', imp_dtl_tbl);
	var det_type_id_field = getDataField('IMPDETAILTYPEID', imp_dtl_tbl);

	var add_on_cat = getCategoryFromSourceTable(add_on_tbl);
	var addon_apexid_field = getDataField('APEXID', add_on_tbl);
	var addon_apx_lnk_field = getDataField('APEXLINKFLAG', add_on_tbl);
	var addOn_descr_field = getDataField('IMPDETAILDESCRIPTION', add_on_tbl);
	//var addOn_type_field = getDataField('IMPDETAILTYPE',add_on_tbl);
	var addOn_type_field = getDataField('ADDONCODE', add_on_tbl);
	var addOn_area_field = getDataField('DETAILUNITCOUNT', add_on_tbl);
	var addOn_filter_type_field = getDataField('ADDONFILTERTYPE', add_on_tbl);
	var addOn_type_id_field = getDataField('IMPDETAILTYPEID', add_on_tbl);

	var floor_cat = getCategoryFromSourceTable(bltas_flr_tbl);
	var bltas_area_field = getDataField('BLTASSF', blt_as_tbl);
	var floor_apx_lnk_field = getDataField('APEXLINKFLAG', bltas_flr_tbl);
	var floor_descr_field = getDataField('IMPSFLOORDESCRIPTION', bltas_flr_tbl);
	var floor_story_height_field = getDataField('STORYHEIGHT', bltas_flr_tbl);
	var floor_area_field = getDataField('BLTASFLOORSF', bltas_flr_tbl);
	var floor_apex_id_field = getDataField('APEXID', bltas_flr_tbl);

	sketchApp.sketches.filter(function (sk) { return (sk.wasModified || (sk.sid == sketchApp.currentSketch.sid)); }).forEach(function (sketch) {
		var skRowid = sketch.parentRow.ParentROWUID;
		var imp_data = sketch.parentRow.parentRecord;
		sketchParentRowid.push(skRowid);

		var DetailrecRecord = [], AddOnsrecRecord = [], FloorrecRecord = [], _cnBuiltasRec = false;
		if (sketch.sid == sketchApp.currentSketch.sid) {
			var DetailRecord = [], AddOnsRecord = [], FloorRecord = [];
			DetailRecord = activeParcel["CCV_IMPDETAIL_ATTACHED_DETACHED"].filter(function (src_rec) { return src_rec.ParentROWUID == skRowid });
			DetailRecord.forEach(function (recort) {
				var sameRowuid = 0, IMPDETAILDESCRIPTION, IMPDETAILTYPEID, apexLink, activeFlag;
				IMPDETAILDESCRIPTION = recort.IMPDETAILDESCRIPTION;
				IMPDETAILTYPEID = recort.IMPDETAILTYPEID;
				var save_dtl_lookup = FTC_Detail_Lookup.filter(function (lkup) { return (lkup.IMPDETAILDESCRIPTION == IMPDETAILDESCRIPTION && lkup.IMPDETAILTYPEID == IMPDETAILTYPEID) })[0];
				if (!save_dtl_lookup) {
					save_dtl_lookup = FTC_Detail_Lookup.filter(function (lkup) { return (lkup.APEXAREACODE == null && lkup.IMPDETAILDESCRIPTION == IMPDETAILDESCRIPTION) })[0];
				}
				if (save_dtl_lookup) {
					apexLink = save_dtl_lookup.APEXLINKFLAG;
					activeFlag = save_dtl_lookup.ACTIVEFLAG;
				}
				if (apexLink == 1 && activeFlag == 1) {
					DetailrecRecord.forEach(function (ree) {
						if (ree.ROWUID == recort.ROWUID)
							sameRowuid = 1;
					});
					if (sameRowuid == 0)
						DetailrecRecord.push(recort);
				}
			});
			AddOnsRecord = activeParcel["CCV_IMPDETAIL_ADD_ONS"].filter(function (src_rec) { return src_rec.ParentROWUID == skRowid });
			AddOnsRecord.forEach(function (recort) {
				var sameRowuid = 0, IMPDETAILDESCRIPTION, apexLink, ADDONCODE, activeFlag;
				IMPDETAILDESCRIPTION = recort.IMPDETAILDESCRIPTION;
				ADDONCODE = recort.ADDONCODE;
				var save_add_on_lookup = FTC_AddOns_Lookup.filter(function (lkup) { return (lkup.ADDONCODE == ADDONCODE || lkup.ADDONDESCRIPTION == IMPDETAILDESCRIPTION); })[0];
				if (save_add_on_lookup) {
					apexLink = save_add_on_lookup.APEXLINKFLAG;
					activeFlag = save_add_on_lookup.ACTIVEFLAG;
				}
				if (apexLink == 1 && activeFlag == 1) {
					AddOnsrecRecord.forEach(function (ree) {
						if (ree.ROWUID == recort.ROWUID)
							sameRowuid = 1;
					});
					if (sameRowuid == 0)
						AddOnsrecRecord.push(recort);
				}
			});
			var IMPBUILTASRecord = [];
			IMPBUILTASRecord = activeParcel["IMPBUILTAS"].filter(function (src_rec) { return src_rec.ParentROWUID == skRowid })[0];
			if (IMPBUILTASRecord)
				FloorRecord = IMPBUILTASRecord.IMPBUILTASFLOOR;
			FloorRecord.forEach(function (recort) {
				var sameRowuid = 0, IMPSFLOORDESCRIPTION, IMPDETAILTYPEID, apexLink, activeFlag;
				IMPSFLOORDESCRIPTION = recort.IMPSFLOORDESCRIPTION;
				var save_floor_lookup = FTC_Floor_Lookup.filter(function (lkup) { return lkup.IMPSFLOORDESCRIPTION == IMPSFLOORDESCRIPTION; })[0];
				if (save_floor_lookup) {
					apexLink = save_floor_lookup.APEXLINKFLAG;
					activeFlag = save_floor_lookup.ACTIVEFLAG;
				}
				if (apexLink == 1 && activeFlag == 1) {
					FloorrecRecord.forEach(function (ree) {
						if (ree.ROWUID == recort.ROWUID)
							sameRowuid = 1;
					});
					if (sameRowuid == 0)
						FloorrecRecord.push(recort);
				}
			});

		}
		else {
			var recdDetail = [], recdFloor = [], recdAddon = [];
			sketch.vectors.filter(function (v) { return (!v.isFreeFormLineEntries) }).filter(function (vect) { return (vect.wasModified); }).forEach(function (vectort) {
				var code = (vectort.code && vectort.code != '') ? vectort.code : vectort.name
				var vector_descrp = lookup['AREA_CODES'][code]?.['Name'] ? lookup['AREA_CODES'][code]['Name'] : (vectort.name ? vectort.name : '');
				var apexLink, recd = [], activeFlag;
				var save_dtl_lookup = FTC_Detail_Lookup.filter(function (lkup) { return (lkup.IMPDETAILDESCRIPTION == vector_descrp && lkup.APEXAREACODE == vectort.code) })[0];
				if (!save_dtl_lookup) {
					save_dtl_lookup = FTC_Detail_Lookup.filter(function (lkup) { return (lkup.APEXAREACODE == null && lkup.IMPDETAILDESCRIPTION == vector_descrp) })[0];
				}
				var save_add_on_lookup = FTC_AddOns_Lookup.filter(function (lkup) { return lkup.ADDONCODE == code; })[0];
				var save_floor_lookup = FTC_Floor_Lookup.filter(function (lkup) { return lkup.IMPSFLOORDESCRIPTION == vector_descrp; })[0];
				if (save_dtl_lookup) {
					apexLink = save_dtl_lookup.APEXLINKFLAG;
					activeFlag = save_dtl_lookup.ACTIVEFLAG;
				}
				else if (save_add_on_lookup) {
					apexLink = save_add_on_lookup.APEXLINKFLAG;
					activeFlag = save_add_on_lookup.ACTIVEFLAG;
				}
				else if (save_floor_lookup) {
					apexLink = save_floor_lookup.APEXLINKFLAG;
					activeFlag = save_floor_lookup.ACTIVEFLAG;
				}
				if (apexLink == 1 && activeFlag == 1 && save_dtl_lookup) {
					recdDetail = activeParcel["CCV_IMPDETAIL_ATTACHED_DETACHED"].filter(function (src_rec) { return src_rec.ParentROWUID == skRowid && src_rec.IMPDETAILDESCRIPTION == vector_descrp; });
					recdDetail.forEach(function (recort) {
						var sameRowuid = 0;
						DetailrecRecord.forEach(function (ree) {
							if (ree.ROWUID == recort.ROWUID)
								sameRowuid = 1;
						});
						if (sameRowuid == 0)
							DetailrecRecord.push(recort);
					});
				}
				else if (apexLink == 1 && activeFlag == 1 && save_add_on_lookup) {
					recdAddon = activeParcel["CCV_IMPDETAIL_ADD_ONS"].filter(function (src_rec) { return src_rec.ParentROWUID == skRowid && (src_rec.ADDONCODE == code || src_rec.IMPDETAILDESCRIPTION == vector_descrp); });
					recdAddon.forEach(function (recort) {
						var sameRowuid = 0;
						AddOnsrecRecord.forEach(function (ree) {
							if (ree.ROWUID == recort.ROWUID)
								sameRowuid = 1;
						});
						if (sameRowuid == 0)
							AddOnsrecRecord.push(recort);
					});
				}
				else if (apexLink == 1 && activeFlag == 1 && save_floor_lookup) {
					var IMPBUILTASRecord = [];
					IMPBUILTASRecord = activeParcel["IMPBUILTAS"].filter(function (src_rec) { return src_rec.ParentROWUID == skRowid && src_rec.IMPSFLOORDESCRIPTION == vector_descrp; })[0];
					if (IMPBUILTASRecord)
						recdFloor = IMPBUILTASRecord.IMPBUILTASFLOOR;
					recdFloor.forEach(function (recort) {
						var sameRowuid = 0;
						FloorrecRecord.forEach(function (ree) {
							if (ree.ROWUID == recort.ROWUID)
								sameRowuid = 1;
						});
						if (sameRowuid == 0)
							FloorrecRecord.push(recort);
					});
				}

			});
		}

		for (i = 0; i < DetailrecRecord.length; i++) {
			rowidd.push(DetailrecRecord[i].ROWUID);
		}
		for (i = 0; i < AddOnsrecRecord.length; i++) {
			rowiddAddon.push(AddOnsrecRecord[i].ROWUID);
		}
		for (i = 0; i < FloorrecRecord.length; i++) {
			rowiddFloor.push(FloorrecRecord[i].ROWUID);
		}

		var bltas_record = imp_data[blt_as_tbl][0] ? imp_data[blt_as_tbl][0] : { ROWUID: 'sk-' + skRowid, BLTASSTORYHEIGHT: null, [bltas_flr_tbl]: [] };
		area = 0, perimeter = 0;
		sketch.vectors.filter(function (v) { return (!v.isFreeFormLineEntries) }).filter(function (vect) { return (vect.wasModified || (sketch.sid == sketchApp.currentSketch.sid)); }).forEach(function (vector, vect_index) {
			var vectuid = vector.clientId;
			var code = ((vector.code && vector.code != '') ? vector.code : vector.name)
			if (!lookup['AREA_CODES'][code] && !vector.name) return;
			/*
			if(vector.code=="NCA" ||vector.code=="LAND" ||vector.code=="OTH"||vector.code=="SITE"||vector.code=="UND" ||vector.code=="REV"){
				cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: null, vect_index: vect_index, tbl: null  });
				return;
			}
			*/
			var condition = false, impFlrDesc;
			var apex_id = vector.vectorString.split('[')[1].split(']')[0].split(/\,/g)[4];
			var assocVal = vector.associateValues;
			var addVal = lookup['AREA_CODES'][code] ? lookup['AREA_CODES'][code]['AdditionalValue1'] : "";
			var vector_descr = lookup['AREA_CODES'][code]?.['Name'] ? lookup['AREA_CODES'][code]['Name'] : (vector.name ? vector.name : "");
			var save_dtl_lookup = FTC_Detail_Lookup.filter(function (lkup) { return (lkup.IMPDETAILDESCRIPTION == vector_descr && lkup.APEXAREACODE == vector.code) })[0];
			if (!save_dtl_lookup) {
				save_dtl_lookup = FTC_Detail_Lookup.filter(function (lkup) { return (lkup.APEXAREACODE == null && lkup.IMPDETAILDESCRIPTION == vector_descr) })[0];
			}
			var save_add_on_lookup = FTC_AddOns_Lookup.filter(function (lkup) { return lkup.ADDONCODE == code; })[0];
			var save_floor_lookup = FTC_Floor_Lookup.filter(function (lkup) { return lkup.IMPSFLOORDESCRIPTION == vector_descr; })[0];
			var det_type = code, det_type_id = null, det_field_1 = null, apexLink = null, activeFlag = null;
			if (save_dtl_lookup) {
				det_type = save_dtl_lookup.IMPDETAILTYPE;
				det_type_id = save_dtl_lookup.IMPDETAILTYPEID;
				det_field_1 = save_dtl_lookup.DETAILCALCULATIONTYPE;
				apexLink = save_dtl_lookup.APEXLINKFLAG;
				activeFlag = save_dtl_lookup.ACTIVEFLAG;
				addonDescr = save_dtl_lookup.IMPDETAILDESCRIPTION;
			}
			else if (save_add_on_lookup) {
				det_type = save_add_on_lookup.ADDONCODE;
				det_type_id = save_add_on_lookup.ADDONCODE;
				det_field_1 = save_add_on_lookup.ADDONFILTERTYPE;
				apexLink = save_add_on_lookup.APEXLINKFLAG;
				activeFlag = save_add_on_lookup.ACTIVEFLAG;
				addonDescr = save_add_on_lookup.ADDONDESCRIPTION;
			}
			else if (save_floor_lookup) {
				apexLink = save_floor_lookup.APEXLINKFLAG;
				activeFlag = save_floor_lookup.ACTIVEFLAG;
				impFlrDesc = save_floor_lookup.IMPSFLOORDESCRIPTION;
			}
			var GLAIdLabel = (code.indexOf("GLA") > -1 || code.indexOf("GBA") > -1) ? true : false;
			_cnBuiltasRec = (!_cnBuiltasRec && (GLAIdLabel || (save_dtl_lookup == undefined && save_add_on_lookup == undefined && save_floor_lookup == undefined && ((labelValidations.imp.indexOf(addVal) > -1))))) ? true : false;
			area += ((labelValidations.imp.indexOf(addVal) > -1 || GLAIdLabel) ? Math.round(vector.area()) : 0);
			perimeter += ((labelValidations.imp.indexOf(addVal) > -1 || GLAIdLabel) ? vector.perimeter() : 0);

			if ((save_dtl_lookup == undefined && save_add_on_lookup == undefined && save_floor_lookup == undefined) || vector.isKeepAfterSave) {
				cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: null, vect_index: vect_index, tbl: null });
				return;
			}
			var areat = vector.area()
			/*
			sketch.vectors.filter(function(v){return (!v.isFreeFormLineEntries)}).filter(function(vect){ return (vect.wasModified || (sketch.sid == sketchApp.currentSketch.sid)); }).forEach(function(vecter) {
				if(vector.uid!=vecter.uid){
					if(vector.associateValues && vecter.associateValues){
						if(vector.associateValues.CCV_IMPDETAIL_ATTACHED_DETACHED&&vecter.associateValues.CCV_IMPDETAIL_ATTACHED_DETACHED){
							if(vector.associateValues.CCV_IMPDETAIL_ATTACHED_DETACHED){
								if(vector.associateValues.CCV_IMPDETAIL_ATTACHED_DETACHED["IMPDETAILTYPE-IMPDETAILDESCRIPTION-DETAILUNITCOUNT"]==vecter.associateValues.CCV_IMPDETAIL_ATTACHED_DETACHED["IMPDETAILTYPE-IMPDETAILDESCRIPTION-DETAILUNITCOUNT"]){
								areat=areat+vecter.area();
								}
							}
						}
					}
				}
			});
			*/
			if (assocVal[imp_dtl_tbl]) {

				if (apexLink == "0" || activeFlag == "0") {
					if (vectuid) {
						cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: null, vect_index: vect_index, tbl: imp_dtl_tbl });
						return;
					}
					else {
						var rowwwId;
						if (vector.header.split('{')[1]) {
							var headeridd = vector.header.split('{')[1].split(']')[0].split(/\,/g)[0];
							if (headeridd)
								rowwwId = headeridd.indexOf("$") == -1 ? headeridd.split("}")[0] : headeridd.split("#")[1].split("$")[0];
						}
						else {
							if (vector.vectorString) {
								var apex_id = vector.vectorString.split('[')[1].split(']')[0].split(/\,/g)[4];
								if (apex_id > -1) {
									var recordAdd = activeParcel[imp_dtl_tbl].filter(function (r) { return r.APEXID == apex_id })[0];
									if (recordAdd)
										rowwwId = recordAdd.ROWUID;
								}
							}
						}
						sketchSaveData.push({ sourceTable: imp_dtl_tbl, rowid: rowwwId ? rowwwId : null, apexId: apex_id, area: (areat ? areat : vector.area()), parentrowid: skRowid, story_ht: null, description: addonDescr, type: assocVal[imp_dtl_tbl].IMPDETAILTYPE ? assocVal[imp_dtl_tbl].IMPDETAILTYPE : det_type, field_1: det_field_1, apexLink: apexLink, activeFlag: activeFlag, type_id: det_type_id });
						cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: rowwwId ? rowwwId : null, vect_index: vect_index, tbl: imp_dtl_tbl });

					}
				}

				else {
					if (!assocVal[imp_dtl_tbl].rowuid) {
						//var exist_new = new_recs.filter(function(nw){ return nw.apexId == apex_id && nw.parentrowid == skRowid && nw.sourceTable == imp_dtl_tbl && nw.description == vector_descr; });
						//if (exist_new.length == 0)
						new_recs.push({ area: vector.area(), apexId: apex_id, rowwid: vectuid ? vectuid : ccTicks(), parentrowid: skRowid, sourceTable: imp_dtl_tbl, story_ht: null, description: vector_descr, type: det_type, type_id: det_type_id, field_1: det_field_1, vect_string: vector.vectorString, sid: sketch.sid, vect_index: vect_index, apexLink: apexLink, activeFlag: activeFlag });
						//else
						//exist_new[0].area += vector.area();
					}
					else {

						if (assocVal[imp_dtl_tbl]["IMPDETAILTYPE-IMPDETAILDESCRIPTION-DETAILUNITCOUNT"]) {
							var Typee = assocVal[imp_dtl_tbl]["IMPDETAILTYPE-IMPDETAILDESCRIPTION-DETAILUNITCOUNT"].split('-')[0];
							var Descr = assocVal[imp_dtl_tbl]["IMPDETAILTYPE-IMPDETAILDESCRIPTION-DETAILUNITCOUNT"].split('-')[1];
						}
						if (rowidd) {
							rowidd.forEach(function (ridd) {
								if (ridd == assocVal[imp_dtl_tbl].rowuid) {
									rowidd.splice(rowidd.indexOf(ridd), 1);
								}
							});
						}
						sketchSaveData.push({ sourceTable: imp_dtl_tbl, rowid: assocVal[imp_dtl_tbl].rowuid, apexId: apex_id, area: areat ? areat : vector.area(), parentrowid: skRowid, story_ht: null, description: assocVal[imp_dtl_tbl].IMPDETAILDESCRIPTION ? assocVal[imp_dtl_tbl].IMPDETAILDESCRIPTION : $.trim(Descr), type: assocVal[imp_dtl_tbl].IMPDETAILTYPE ? assocVal[imp_dtl_tbl].IMPDETAILTYPE : $.trim(Typee), type_id: det_type_id, field_1: det_field_1, apexLink: apexLink, activeFlag: activeFlag });
						cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: assocVal[imp_dtl_tbl].rowuid, vect_index: vect_index, tbl: imp_dtl_tbl });
					}
				}
			}

			if (assocVal[add_on_tbl]) {
				if (apexLink == "0" || activeFlag == "0") {
					if (vectuid) {
						cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: null, vect_index: vect_index, tbl: add_on_tbl });
						return;
					}
					else {
						var rowwwId;
						if (vector.header.split('{')[1]) {
							var headeridd = vector.header.split('{')[1].split(']')[0].split(/\,/g)[0];
							if (headeridd)
								rowwwId = headeridd.indexOf("$") == -1 ? headeridd.split("}")[0] : headeridd.split("#")[1].split("$")[0];
						}
						else {
							if (vector.vectorString) {
								var apex_id = vector.vectorString.split('[')[1].split(']')[0].split(/\,/g)[4];
								if (apex_id > -1) {
									var recordAdd = activeParcel[add_on_tbl].filter(function (r) { return r.APEXID == apex_id })[0];
									if (recordAdd)
										rowwwId = recordAdd.ROWUID;
								}
							}
						}
						if (rowwwId)
							sketchSaveData.push({ sourceTable: add_on_tbl, rowid: rowwwId ? rowwwId : null, apexId: apex_id, area: ((labelValidations.det_val.indexOf(addVal) > -1) ? vector.area() : 0), parentrowid: skRowid, story_ht: null, description: addonDescr, type: assocVal[add_on_tbl].IMPDETAILTYPE ? assocVal[add_on_tbl].IMPDETAILTYPE : det_type, field_1: det_field_1, apexLink: apexLink, activeFlag: activeFlag });
						cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: rowwwId ? rowwwId : null, vect_index: vect_index, tbl: add_on_tbl });

					}
				}
				else {
					if (!assocVal[add_on_tbl].rowuid) {
						//var exist_new = new_recs.filter(function(nw){ return nw.apexId == apex_id && nw.parentrowid == skRowid && nw.sourceTable == add_on_tbl && nw.description == vector_descr; });
						//if (exist_new.length == 0)
						new_recs.push({ area: ((labelValidations.det_val.indexOf(addVal) > -1) ? vector.area() : 0), apexId: apex_id, rowwid: vectuid ? vectuid : ccTicks(), parentrowid: skRowid, sourceTable: add_on_tbl, story_ht: null, story_ht: null, description: vector_descr, type: det_type, field_1: det_field_1, vect_string: vector.vectorString, sid: sketch.sid, vect_index: vect_index, apexLink: apexLink, activeFlag: activeFlag });
						//else
						//exist_new[0].area += vector.area();
					}
					else {
						if (assocVal[add_on_tbl]["IMPDETAILTYPE-IMPDETAILDESCRIPTION-DETAILUNITCOUNT"]) {
							var Typee = assocVal[add_on_tbl]["IMPDETAILTYPE-IMPDETAILDESCRIPTION-DETAILUNITCOUNT"].split('-')[0];
							var Descr = assocVal[add_on_tbl]["IMPDETAILTYPE-IMPDETAILDESCRIPTION-DETAILUNITCOUNT"].split('-')[1];
						}
						if ($.trim(Descr))
							var save_add_on_lookup = FTC_AddOns_Lookup.filter(function (lkup) { return lkup.ADDONDESCRIPTION == $.trim(Descr); })[0];
						if (rowiddAddon) {
							rowiddAddon.forEach(function (ridd) {
								if (ridd == assocVal[add_on_tbl].rowuid) {
									rowiddAddon.splice(rowiddAddon.indexOf(ridd), 1);
								}
							});
						}
						sketchSaveData.push({ sourceTable: add_on_tbl, rowid: assocVal[add_on_tbl].rowuid, apexId: apex_id, area: ((labelValidations.det_val.indexOf(addVal) > -1) ? vector.area() : 0), parentrowid: skRowid, story_ht: null, description: assocVal[add_on_tbl].IMPDETAILDESCRIPTION ? assocVal[add_on_tbl].IMPDETAILDESCRIPTION : $.trim(Descr), type: assocVal[add_on_tbl].IMPDETAILTYPE ? assocVal[add_on_tbl].IMPDETAILTYPE : save_add_on_lookup ? save_add_on_lookup.ADDONCODE : det_type, field_1: det_field_1, apexLink: apexLink });
						cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: assocVal[add_on_tbl].rowuid, vect_index: vect_index, tbl: add_on_tbl });
					}
				}
			}

			if (assocVal[bltas_flr_tbl] && labelValidations.imp.indexOf(addVal) > -1) {
				if (apexLink == "0" || activeFlag == "0") {
					if (vectuid) {
						cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: null, vect_index: vect_index, tbl: bltas_flr_tbl });
						return;
					}
					else {
						var rowwwId;
						if (vector.header.split('{')[1]) {
							var headeridd = vector.header.split('{')[1].split(']')[0].split(/\,/g)[0];
							if (headeridd)
								rowwwId = headeridd.indexOf("$") == -1 ? headeridd.split("}")[0] : headeridd.split("#")[1].split("$")[0];
						}
						else {
							if (vector.vectorString) {
								var apex_id = vector.vectorString.split('[')[1].split(']')[0].split(/\,/g)[4];
								if (apex_id > -1) {
									var recordAdd = activeParcel[bltas_flr_tbl].filter(function (r) { return r.APEXID == apex_id })[0];
									if (recordAdd)
										rowwwId = recordAdd.ROWUID;
								}
							}
						}
						if (rowwwId)
							sketchSaveData.push({ sourceTable: bltas_flr_tbl, rowid: rowwwId ? rowwwId : null, apexId: apex_id, area: vector.area(), parentrowid: bltas_record.ROWUID, story_ht: bltas_record.BLTASSTORYHEIGHT, description: impFlrDesc, type: assocVal[bltas_flr_tbl].IMPBUILTASFLOORID, apexLink: apexLink, activeFlag: activeFlag });
						cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: rowwwId ? rowwwId : null, vect_index: vect_index, tbl: bltas_flr_tbl });

					}
				}
				else {
					if (!assocVal[bltas_flr_tbl].rowuid) {
						//var exist_new = new_recs.filter(function(nw){ return nw.apexId == apex_id && nw.parentrowid == skRowid && nw.sourceTable == bltas_flr_tbl && nw.description == vector_descr; });
						//if (exist_new.length == 0)
						new_recs.push({ area: vector.area(), apexId: apex_id, rowwid: vectuid ? vectuid : ccTicks(), parentrowid: bltas_record.ROWUID, sourceTable: bltas_flr_tbl, story_ht: bltas_record.BLTASSTORYHEIGHT, description: vector_descr, vect_string: vector.vectorString, sid: sketch.sid, vect_index: vect_index, apexLink: apexLink, activeFlag: activeFlag });
						//else
						//exist_new[0].area += vector.area();
					}
					else {
						if (assocVal[bltas_flr_tbl]["IMPSFLOORDESCRIPTION-BLTASSF"])
							var descr = assocVal[bltas_flr_tbl]["IMPSFLOORDESCRIPTION-BLTASSF"].split('-')[0]
						if (rowiddFloor) {
							rowiddFloor.forEach(function (ridd) {
								if (ridd == assocVal[bltas_flr_tbl].rowuid) {
									rowiddFloor.splice(rowiddFloor.indexOf(ridd), 1);
								}
							});
						}
						sketchSaveData.push({ sourceTable: bltas_flr_tbl, rowid: assocVal[bltas_flr_tbl].rowuid, apexId: apex_id, area: vector.area(), parentrowid: bltas_record.ROWUID, story_ht: bltas_record.BLTASSTORYHEIGHT, description: assocVal[bltas_flr_tbl].IMPSFLOORDESCRIPTION ? assocVal[bltas_flr_tbl].IMPSFLOORDESCRIPTION : $.trim(descr), type: assocVal[bltas_flr_tbl].IMPBUILTASFLOORID, apexLink: apexLink, activeFlag: activeFlag });
						cc_sketch_save_data.push({ vect_string: vector.vectorString, sid: sketch.sid, rowid: assocVal[bltas_flr_tbl].rowuid, vect_index: vect_index, tbl: bltas_flr_tbl });
					}
				}
			}
			//vector.wasModified = false;
		});
		sketchSave.push({ rowid: skRowid, area: Math.round(area), perimeter: Math.round(perimeter), _cnBuiltasRec: _cnBuiltasRec });
		//sketch.wasModified = false;		
	});
	var currRowuid = [];
	var forDelete = [];
	var currentUid = sketchApp.currentSketch.uid;
	var sketchee = sketchApp.sketches.filter(function (x) { return x.uid != currentUid });
	sketchee.forEach(function (sketchees) {
		var vectorrs = sketchees.vectors.filter(function (y) { return y.isModified != true })
		vectorrs.filter(function (v) { return (!v.isFreeFormLineEntries) }).forEach(function (vectorrrs) {
			var tRowuid;

			if (vectorrrs.tableRowId) {
				if (vectorrrs.tableRowId.split('#')[1])
					tRowuid = vectorrrs.tableRowId.split('#')[1].split('$')[0];
				else if (vectorrrs.tableRowId.split('{')[1])
					tRowuid = vectorrrs.tableRowId.split('{')[1].split('}')[0];
				currRowuid.push(tRowuid);
				forDelete.push(tRowuid);
			}
			else if (vectorrrs.vectorString) {
				var apex_id = vectorrrs.vectorString.split('[')[1].split(']')[0].split(/\,/g)[4];
				var vector_descrp = lookup['AREA_CODES'][vectorrrs.code]?.['Name'] ? lookup['AREA_CODES'][vectorrrs.code]['Name'] : (vectorrrs.name ? vectorrrs.name : "");
				var code = vectorrrs.code;
				var save_dtl_lookup = FTC_Detail_Lookup.filter(function (lkup) { return (lkup.IMPDETAILDESCRIPTION == vector_descrp && lkup.APEXAREACODE == vectorrs.code) })[0];
				if (!save_dtl_lookup) {
					save_dtl_lookup = FTC_Detail_Lookup.filter(function (lkup) { return (lkup.APEXAREACODE == null && lkup.IMPDETAILDESCRIPTION == vector_descrp) })[0];
				}
				var save_add_on_lookup = FTC_AddOns_Lookup.filter(function (lkup) { return lkup.ADDONCODE == code; })[0];
				var save_floor_lookup = FTC_Floor_Lookup.filter(function (lkup) { return lkup.IMPSFLOORDESCRIPTION == vector_descrp; })[0];
				if (apex_id > -1 && save_dtl_lookup) {
					var recordAdd = activeParcel["CCV_IMPDETAIL_ATTACHED_DETACHED"].filter(function (r) { return r.APEXID == apex_id })[0];
					if (recordAdd)
						tRowuid = recordAdd.ROWUID;
					currRowuid.push(tRowuid);
					forDelete.push(tRowuid);
				}
				else if (apex_id > -1 && save_add_on_lookup) {
					var recordAdd = activeParcel["CCV_IMPDETAIL_ADD_ONS"].filter(function (r) { return r.APEXID == apex_id })[0];
					if (recordAdd)
						tRowuid = recordAdd.ROWUID;
					forDelete.push(tRowuid);
				}
				else if (apex_id > -1 && save_floor_lookup) {
					var recordAdd = activeParcel["IMPBUILTASFLOOR"].filter(function (r) { return r.APEXID == apex_id })[0];
					if (recordAdd)
						tRowuid = recordAdd.ROWUID;
					forDelete.push(tRowuid);
				}

			}

		});
	});

	sketchApp.sketches.filter(function (sk) { return (sk.wasModified && (sk.sid != sketchApp.currentSketch.sid)); }).forEach(function (sketchin) {
		sketchin.vectors.filter(function (v) { return (!v.isFreeFormLineEntries) }).filter(function (vect) { return (vect.wasModified); }).forEach(function (vecteri, vect_inde) {
			var assocValu = vecteri.associateValues;
			var rowwwwwId;
			var noRecord = false;
			var code = ((vecteri.code && vecteri.code != '') ? vecteri.code : vecteri.name)
			var vector_descr = lookup['AREA_CODES'][code]?.['Name'] ? lookup['AREA_CODES'][code]['Name'] : (vecteri.name ? vecteri.name : "");
			var save_dtl_lookup = FTC_Detail_Lookup.filter(function (lkup) { return (lkup.IMPDETAILDESCRIPTION == vector_descr && lkup.APEXAREACODE == vecteri.code) })[0];
			if (!save_dtl_lookup) {
				save_dtl_lookup = FTC_Detail_Lookup.filter(function (lkup) { return (lkup.APEXAREACODE == null && lkup.IMPDETAILDESCRIPTION == vector_descr) })[0];
			}
			var save_add_on_lookup = FTC_AddOns_Lookup.filter(function (lkup) { return lkup.ADDONCODE == code; })[0];
			var save_floor_lookup = FTC_Floor_Lookup.filter(function (lkup) { return lkup.IMPSFLOORDESCRIPTION == vector_descr; })[0];
			if (save_dtl_lookup == undefined && save_add_on_lookup == undefined && save_floor_lookup == undefined)
				noRecord = true;
			if (!noRecord) {
				if (_.isEmpty(assocValu)) {
					let tblname = save_dtl_lookup ? "CCV_IMPDETAIL_ATTACHED_DETACHED" : (save_add_on_lookup ? "CCV_IMPDETAIL_ADD_ONS" : (save_floor_lookup ? "IMPBUILTASFLOOR" : null));
					if (vecteri.vectorString.split('#')[1] && vecterin.vectorString.split('#')[1].split('$')[0])
						rowwwwwId = vecteri.vectorString.split('#')[1].split('$')[0];
					cc_sketch_save_data.push({ vect_string: vecteri.vectorString, sid: sketchin.sid, rowid: rowwwwwId ? rowwwwwId : null, vect_index: vect_inde, tbl: tblname });
				}
			}
		});
		sketchin.vectors.filter(function (v) { return (!v.isFreeFormLineEntries) }).filter(function (vect) { return (!vect.wasModified); }).forEach(function (vecterin, vect_index) {
			var rowwwwId;

			let code = ((vecterin.code && vecterin.code != '') ? vecterin.code : vecterin.name)
			let vector_descr = lookup['AREA_CODES'][code]?.['Name'] ? lookup['AREA_CODES'][code]['Name'] : (vecterin.name ? vecterin.name : "");
			let save_dtl_lookup = FTC_Detail_Lookup.filter(function (lkup) { return (lkup.IMPDETAILDESCRIPTION == vector_descr && lkup.APEXAREACODE == vecterin.code) })[0];
			if (!save_dtl_lookup) {
				save_dtl_lookup = FTC_Detail_Lookup.filter(function (lkup) { return (lkup.APEXAREACODE == null && lkup.IMPDETAILDESCRIPTION == vector_descr) })[0];
			}
			let save_add_on_lookup = FTC_AddOns_Lookup.filter(function (lkup) { return lkup.ADDONCODE == code; })[0];
			let save_floor_lookup = FTC_Floor_Lookup.filter(function (lkup) { return lkup.IMPSFLOORDESCRIPTION == vector_descr; })[0];
			let tblname = save_dtl_lookup ? "CCV_IMPDETAIL_ATTACHED_DETACHED" : (save_add_on_lookup ? "CCV_IMPDETAIL_ADD_ONS" : (save_floor_lookup ? "IMPBUILTASFLOOR" : null));
			if (vecterin.vectorString.split('#')[1] && vecterin.vectorString.split('#')[1].split('$')[0])
				rowwwwId = vecterin.vectorString.split('#')[1].split('$')[0];
			cc_sketch_save_data.push({ vect_string: vecterin.vectorString, sid: sketchin.sid, rowid: rowwwwId ? rowwwwId : null, vect_index: vect_index, tbl: tblname });
		});
	});


	var rRowuid = _.clone(rowidd);
	if (rRowuid && currRowuid) {
		rRowuid.forEach(function (rowiddd) {
			var i = 0;
			for (i = 0; i < currRowuid.length; i++) {
				if (rowiddd == currRowuid[i]) {
					rowidd.splice(rowidd.indexOf(currRowuid[i]), 1);
				}
			}
		});
	}

	if (rowidd) {
		rowidd.forEach(function (rowiddd) {
			deleted_rec.push({ rowid: rowiddd, catid: cateIdd });
		});
	}
	if (rowiddAddon) {
		rowiddAddon.forEach(function (rowiddd) {
			deleted_rec.push({ rowid: rowiddd, catid: addOnCatId });
		});
	}
	if (rowiddFloor) {
		rowiddFloor.forEach(function (rowiddd) {
			deleted_rec.push({ rowid: rowiddd, catid: floorCatId });
		});
	}
	var update_CC_Sketch = function (skSaveData, index) {
		var save_count = Object.keys(skSaveData).length;
		if (index == save_count) { if (callback) callback(); return; }
		var sid = Object.keys(skSaveData)[index];
		var skSketch = sketchApp.sketches.filter(function (sk) { return (sk.sid == sid) })[0];
		var stringVal = '';
		if (skSketch) {
			var jstring = skSketch.jsonString;
			jstring.DCS = skSaveData[sid];
			stringVal = JSON.stringify(jstring);
		}
		var sourceRecord = activeParcel.IMPAPEXOBJECT.filter(function (obj) { return obj.ROWUID == sid; })[0];
		update_data(sketch_field, stringVal, sourceRecord, function () {
			update_CC_Sketch(skSaveData, ++index);
		})
	}

	var updateRecs = function () {
		if (sketchSaveData.length == 0) {
			var skSaveData = {};
			sketchApp.sketches.forEach(function (sketch) {
				_.sortBy(cc_sketch_save_data.filter(function (sk) { return sk.sid == sketch.sid }), 'vect_index').forEach(function (skSave) {
					var temp_str = skSave.vect_string.split('[')[1].split(']')[0].split(/\,/);
					var vect_str = '';
					if (skSave.rowid == null) {
						if (skSave.vect_string.indexOf("{") > -1) {
							var t = skSave.vect_string.split('{')[0];
							vect_str = t.substr(0, t.length - 1) + ']' + skSave.vect_string.split(']')[1] + ';';
						}
						else {
							vect_str = skSave.vect_string + ';';
						}
					}
					else if (skSave.vect_string.indexOf("{") > -1)
						vect_str = skSave.vect_string.split('{')[0] + '{$' + skSave.tbl + '#' + skSave.rowid + '$}]' + skSave.vect_string.split(']')[1] + ';';
					else {
						vect_str = skSave.vect_string.split(']')[0] + ',{$' + skSave.tbl + '#' + skSave.rowid + '$}]' + skSave.vect_string.split(']')[1] + ';';
					}
					if (skSaveData[skSave.sid]) skSaveData[skSave.sid] += vect_str;
					else skSaveData[skSave.sid] = vect_str;
				});
			});
			update_CC_Sketch(skSaveData, 0);
			return;
		}
		var rec = sketchSaveData.pop();
		var apex_field = (rec.sourceTable == add_on_tbl ? addon_apexid_field : (rec.sourceTable == imp_dtl_tbl ? det_apexid_field : floor_apex_id_field));
		var area_field = (rec.sourceTable == add_on_tbl ? addOn_area_field : (rec.sourceTable == imp_dtl_tbl ? det_area_field : floor_area_field));
		var descr_field = (rec.sourceTable == add_on_tbl ? addOn_descr_field : (rec.sourceTable == imp_dtl_tbl ? det_descr_field : floor_descr_field));
		var type_field = (rec.sourceTable == add_on_tbl ? addOn_type_field : (rec.sourceTable == imp_dtl_tbl ? det_type_field : null));
		var type_id_field = (rec.sourceTable == add_on_tbl ? addOn_type_id_field : (rec.sourceTable == imp_dtl_tbl ? det_type_id_field : null));
		var field1 = (rec.sourceTable == add_on_tbl ? addOn_filter_type_field : (rec.sourceTable == imp_dtl_tbl ? det_calc_type_field : null));
		var apex_link_field = (rec.sourceTable == add_on_tbl ? addon_apx_lnk_field : (rec.sourceTable == imp_dtl_tbl ? det_apx_lnk_field : floor_apx_lnk_field));

		var modify_record = function () {
			//if (sketchData.map(function(sk){ return sk.rowid; }).indexOf(parentRowId) > -1) {
			update_data(area_field, Math.round(rec.area), record, function () {
				if (rec.sourceTable == bltas_flr_tbl) {
					if (record.CC_RecordStatus == "I" && record.STORYHEIGHT == null) {
						update_data(floor_story_height_field, rec.story_ht, record, updateRecs);
					}
					else
						updateRecs();
				}
				else updateRecs();
			});
			//}
			//else updateRecs();
		}

		var record = activeParcel[rec.sourceTable].filter(function (r) { return r.ROWUID == rec.rowid })[0];
		if (record) {
			update_data(apex_link_field, rec.apexLink, record, function () {
				//if (rec.apexLink == '0') updateRecs();
				//else
				update_data(apex_field, rec.apexId, record, function () {
					update_data(descr_field, rec.description, record, function () {
						if (type_field) {
							update_data(type_field, rec.type, record, function () {
								update_data(type_id_field, rec.type_id, record, function () {
									update_data(field1, rec.field_1, record, modify_record);
								});
							});
						}
						else modify_record();
					});
				});
			});
		}
		else
			updateRecs();
	}

	var insertNew = function () {
		if (new_recs.length == 0) {
			if (refreshParcel) getParcel(activeParcel.Id, updateRecs);
			else updateRecs();
			return;
		}
		var rec = new_recs.pop();
		var parent_data = (rec.sourceTable == imp_dtl_tbl || rec.sourceTable == add_on_tbl) ? 'IMP' : blt_as_tbl;
		parent_data = activeParcel[parent_data].filter(function (pdata) { return pdata.ROWUID == rec.parentrowid })[0];
		var category = ((rec.sourceTable == imp_dtl_tbl) ? det_cat : (rec.sourceTable == add_on_tbl ? add_on_cat : floor_cat));
		var autoInsertProp = (category.AutoInsertProperties && JSON.parse(category.AutoInsertProperties.replace(/'/g, '"')));
		var reloadParcel = (autoInsertProp && autoInsertProp.fieldsToCopy && autoInsertProp.table.trim() != '') ? true : false;
		insertNewAuxRecord(null, parent_data, category.Id, rec.rowwid, null, function (rowid) {
			refreshParcel = reloadParcel || refreshParcel;
			sketchSaveData.push({ rowid: rowid, apexId: rec.apexId, area: rec.area, parentrowid: rec.parentrowid, sourceTable: rec.sourceTable, story_ht: rec.story_ht, description: rec.description, type: rec.type, type_id: rec.type_id, field_1: rec.field_1, apexLink: rec.apexLink, activeFlag: rec.activeFlag });
			insertNew();
			cc_sketch_save_data.push({ vect_string: rec.vect_string, sid: rec.sid, rowid: rowid, vect_index: rec.vect_index, tbl: rec.sourceTable, });
		}, null, reloadParcel);
	}

	var deleteRecs = function () {
		if (deleted_rec.length == 0) {
			if (refreshParcel) getParcel(activeParcel.Id, insertNew);
			else insertNew();
			return;
		}
		var del_rec = deleted_rec.pop();
		ccma.Data.Controller.DeleteAuxRecordWithDescendants(del_rec.catid, del_rec.rowid, null, null, function () {
			refreshParcel = true; deleteRecs();
		});
	}
	var update_sketchData = function () {
		if (sketchSave.length == 0) { deleteRecs(); return; }
		var skData = sketchSave.pop();
		var imp_data = activeParcel.IMP.filter(function (imp) { return imp.ROWUID == skData.rowid; })[0];
		update_data(perimeter_field, skData.perimeter, imp_data, function () {
			update_data(assoc_val_field, skData.assoc_val, imp_data, function () {
				if (imp_data[blt_as_tbl][0])
					update_data(bltas_area_field, skData.area, imp_data[blt_as_tbl][0], update_sketchData);
				else if (new_recs.filter(function (nw_rec) { return nw_rec.parentrowid == 'sk-' + skData.rowid }).length > 0) {
					var neww_rec = new_recs.filter(function (nw_rec) { return nw_rec.parentrowid == 'sk-' + skData.rowid })
					insertNewAuxRecord(null, imp_data, getCategoryFromSourceTable(blt_as_tbl).Id, null, null, function (rowid) {
						new_recs.filter(function (nw_rec) { return nw_rec.parentrowid == 'sk-' + skData.rowid }).forEach(function (new_rec) {
							new_rec.parentrowid = rowid;
						});
						var blt_data = activeParcel[blt_as_tbl].filter(function (blt) { return blt.ROWUID == rowid; })[0];
						update_data(bltas_area_field, skData.area, blt_data, update_sketchData);
					});
				}
				else if (skData._cnBuiltasRec) {
					insertNewAuxRecord(null, imp_data, getCategoryFromSourceTable(blt_as_tbl).Id, null, null, function (rowid) {
						var blt_data = activeParcel[blt_as_tbl].filter(function (blt) { return blt.ROWUID == rowid; })[0];
						update_data(bltas_area_field, skData.area, blt_data, update_sketchData);
					});
				}
				else update_sketchData();
			});
		});
	}

	sketchSaveData.forEach(function (skd) {
		var impRowid = (skd.sourceTable == bltas_flr_tbl) ? activeParcel[blt_as_tbl].filter(function (bltas) { return bltas.ROWUID == skd.parentrowid })[0].ParentROWUID : skd.parentrowid;
		if (!assoc_val[impRowid]) assoc_val[impRowid] = {};
		if (!assoc_val[impRowid][skd.sourceTable]) assoc_val[impRowid][skd.sourceTable] = [];
		assoc_val[impRowid][skd.sourceTable].push(skd.rowid);
	});
	new_recs.forEach(function (skd) {
		var impppRowid = skd.parentrowid.split("sk-").length > 1 ? skd.parentrowid.split("sk-")[1] : skd.parentrowid;
		var impRowid = (skd.sourceTable == bltas_flr_tbl) ? ((activeParcel[blt_as_tbl].filter(function (bltas) { return bltas.ROWUID == impppRowid })).length > 0 ? activeParcel[blt_as_tbl].filter(function (bltas) { return bltas.ROWUID == impppRowid })[0].ParentROWUID : impppRowid) : impppRowid;
		if (!assoc_val[impRowid]) assoc_val[impRowid] = {};
		if (!assoc_val[impRowid][skd.sourceTable]) assoc_val[impRowid][skd.sourceTable] = [];
		assoc_val[impRowid][skd.sourceTable].push(skd.rowwid.toString());
	});
	var isValid = true, oldValue;
	if (assoc_val_field)
		sketchParentRowid.forEach(function (sketchRowwid) {
			activeParcel.IMP.filter(function (imps) { return imps.CC_AssociateValues && imps.ROWUID == sketchRowwid }).forEach(function (imp) {
				isValid = true
				try {
					oldValue = JSON.parse(imp.CC_AssociateValues);
				} catch (ex) { console.warn(ex); isValid = false; }
				if (isValid)
					$.each(oldValue, function (st, old) {
						old.forEach(function (oldRowuid) {
							if (forDelete.indexOf(oldRowuid) > -1) {
								return;
							}
							var deleteArray = [];
							if (st == "CCV_IMPDETAIL_ATTACHED_DETACHED")
								deleteArray = assoc_val[imp.ROWUID] && assoc_val[imp.ROWUID].CCV_IMPDETAIL_ATTACHED_DETACHED;
							else if (st == "IMPBUILTASFLOOR")
								deleteArray = assoc_val[imp.ROWUID] && assoc_val[imp.ROWUID].IMPBUILTASFLOOR;
							else if (st == "CCV_IMPDETAIL_ADD_ONS")
								deleteArray = assoc_val[imp.ROWUID] && assoc_val[imp.ROWUID].CCV_IMPDETAIL_ADD_ONS;
							if ((deleteArray && deleteArray.indexOf(oldRowuid) == -1) || deleteArray == undefined)
								deleted_rec.push({ rowid: oldRowuid, catid: ((st == imp_dtl_tbl) ? det_cat.Id : (st == add_on_tbl) ? add_on_cat.Id : floor_cat.Id) });
						});
					});
			});
		});
	sketchSave.filter(function (sks) { return assoc_val[sks.rowid] }).forEach(function (skSave) {
		var assoc_json_val = JSON.stringify(assoc_val[skSave.rowid]);
		skSave['assoc_val'] = (assoc_val[skSave.rowid] && assoc_json_val) ? JSON.stringify(assoc_val[skSave.rowid]) : null;
	});
	update_sketchData();
}

function FarragutSketchAfterSave (sketchData, callback) {
	if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }
    
    var copySketchData = [], deletedRecords = [], resAddnTable = 'CCV_RES_ADDITION', comAddnTable = 'CCV_COM_ADDITION', secAddnTable = 'CCV_COM_SECTION_FEATURE', newRecordRowuid = ccTicks();
    var resAddnCatId = getCategoryFromSourceTable(resAddnTable).Id, comAddnCatId = getCategoryFromSourceTable(comAddnTable).Id, secCatId = getCategoryFromSourceTable(secAddnTable).Id;
	var resSketchFields = ['TOTAL_LIVING_AREA', 'MB_FOOTPRINT_AREA', 'MB_IS_SKETCHED', 'MB_SKETCH_VECTOR', 'MB_PERIMETER'], resFields = [];
    var comSketchFields = ['SECTION_GROSS_SQFT', 'ADDITION_GROSS_SQFT', 'BUILDING_GROSS_SQFT'], comFields = [];
    var resAddnFields = ['FOOTPRINT_AREA', 'LIVING_AREA_PCT', 'SIZE_FACTOR_FLAG', 'SIZE_FACTOR', 'NON_SKETCHED_FLAG', 'MAIN_LOOKUP'], rAddnFields = [];
    var comAddnFields = ['FOOTPRINT_AREA', 'LIVING_AREA_PCT', 'SIZE_FACTOR_FLAG', 'SIZE_FACTOR', 'NON_SKETCHED_FLAG', 'MAIN_LOOKUP'], cAddnFields = [];
	var sectFields = ['SQFT', 'SQFT_REFERENCE', 'IS_SKETCHED', 'OCCUPANCY', 'PERIMITER_INPUT'], snFields = [];
    _PCIBatchArray = [];

    resSketchFields.forEach(function(field) {
		resFields.push(getDataField(field, 'CCV_RES_BUILDING'));
    });
    comSketchFields.forEach(function(field) {
		rAddnFields.push(getDataField(field, 'CCV_COM_BUILDING'));
    });
    resAddnFields.forEach(function(field) {
        rAddnFields.push(getDataField(field, resAddnTable));
    });
    comAddnFields.forEach(function(field) {
        cAddnFields.push(getDataField(field, comAddnTable));
    });
    sectFields.forEach(function(field) {
        snFields.push(getDataField(field, secAddnTable));
    });

    sketchData.forEach(function(sdata) {
        var skt = sketchApp.sketches.filter(function(s) { return s.sid == sdata.rowid })[0];
        var sktTable = skt.config.SketchSource.Table;
        var sktRecord = activeParcel[sktTable].filter(function (s) { return s.ROWUID == sdata.rowid && s.CC_Deleted != true })[0];
        
        copySketchData.push({ sid: sdata.rowid, sketchTable: sktTable, parentRecord: sktRecord });
		if (sktTable == 'CCV_RES_BUILDING') {
            sktRecord[resAddnTable].filter(function(addn) { return addn.CC_Deleted != true && addn.NON_SKETCHED_FLAG == 'N' }).forEach(function(resAddn) {
                deletedRecords.push(_.clone({ rowuid: resAddn.ROWUID, categoryId: resAddnCatId }));
            });
        }
		else if (sktTable == 'CCV_COM_BUILDING') {
            sktRecord[comAddnTable].filter(function(addn) { return addn.CC_Deleted != true && addn.NON_SKETCHED_FLAG == 'N' }).forEach(function(comAddn) {
                deletedRecords.push(_.clone({ rowuid: comAddn.ROWUID, categoryId: comAddnCatId }));
            });
            sktRecord[secAddnTable].filter(function(sec) { return sec.CC_Deleted != true && sec.IS_SKETCHED == 'Y' }).forEach(function(resSec) {
                deletedRecords.push(_.clone({ rowuid: resSec.ROWUID, categoryId: secCatId }));
            });
        }
    });
    
    var updateRecordPCI = function(){
    	ccma.Sync.enqueueBulkParcelChanges( _PCIBatchArray, function() {  
			getParcel(activeParcel.Id, function () {
				if (callback) callback(); return;
			});
		});
    }  

	var deleteRecord = function (delRecords) {
        if (delRecords.length == 0) {
            updateRecordPCI();  return;
        }

        var delRec = delRecords.pop();
        var rel = ( delRecords.length == 0 && ( typeof(cachedAuxOptions) != 'undefined') && !(isEmpty(cachedAuxOptions) ) && cachedAuxOptions[delRec.categoryId])? true: false;
        ccma.Data.Controller.DeleteAuxRecordWithDescendants(delRec.categoryId, delRec.rowuid, null, null, function () {
            deleteRecord(deletedRecords);
        }, rel, null, true);
    }
    
    var processor = function (sketchRecords) {
        if (sketchRecords.length == 0) {
        	deleteRecord(deletedRecords); return;
        }

        var sketchRecord = sketchRecords.shift();
        var sketh = sketchApp.sketches.filter(function(s) { return s.sid == sketchRecord.sid })[0];
        var vectors = sketh.vectors.filter(function(svs) { return !(svs.isFreeFormLineEntries) });
        var sketchTable = sketchRecord.sketchTable;
        var parentRecord = activeParcel[sketchTable].filter(function (s) { return s.ROWUID == sketchRecord.sid && s.CC_Deleted != true })[0];
		var sketchFields = [], newRecordList = [], isSketch = 'N', totLivingArea = 0, footPrintArea = 0, sectGrossSqft = 0, addnGrossSqft = 0, buidGrossSqft = 0, mb_footPrintArea = 0, mb_perimeter = '';
        var vectorString = '', basUpdated = false;
		
		vectors.forEach(function (vector) {
			
			var area = parseFloat(vector.area()), perimeter = vector.perimeter();
			var code = vector.code;
			var fLookup = Farragut_Lookup.filter(function (f) { return f.ATTRIBUTE_VALUE == code })[0];
			var apexLookup = lookup['Apex_Area_Codes'][code];
			if (vector.vectorString != "") {
				if (fLookup) {
					var lookupCode = fLookup.LOOKUP_VALUE_PK;
					var addnOrSect = fLookup.LOOKUP_TYPE_AdditionalValue1;
					var associateRowuid = vector.associateRowuid;
					isSketch = 'Y';
					var isNewRecord = false;
					if (vector.newRecord || !associateRowuid || associateRowuid == '0') {
						newRecordRowuid = newRecordRowuid + 50;
						associateRowuid = newRecordRowuid;
						isNewRecord = true;
					}
					var newRecordTable = null;
					if (addnOrSect && addnOrSect == 'ADDN') {
						if (sketchTable == 'CCV_RES_BUILDING') {
							totLivingArea = fLookup.LIVING_AREA_FLAG == 'Y' ? (totLivingArea + ((apexLookup.NumericValue && apexLookup.NumericValue != '' && !isNaN(apexLookup.NumericValue)) ? (area * parseFloat(apexLookup.NumericValue)) : area)) : totLivingArea;
							footPrintArea = footPrintArea + area;
							mb_footPrintArea += ((code == 'BAS') ? mb_footPrintArea : 0);
							newRecordList.push(_.clone({ SOURCE_TABLE: resAddnTable, CATEGORY_ID: resAddnCatId, PARENT_RECORD: parentRecord, FOOTPRINT_AREA: Math.round(area), LIVING_AREA_PCT: (fLookup.LIVING_AREA_FLAG == 'Y' ? 100 : 0), SIZE_FACTOR_FLAG: 'N', SIZE_FACTOR: 1, NON_SKETCHED_FLAG: 'N', MAIN_LOOKUP: fLookup.LOOKUP_VALUE_PK, isNewSketch: isNewRecord, rowuid: associateRowuid }));
							newRecordTable = resAddnTable;
							deletedRecords = deletedRecords.filter(function (d) { return !(d.rowuid == associateRowuid && d.categoryId == resAddnCatId) });
						}
						else if (sketchTable == 'CCV_COM_BUILDING') {
							addnGrossSqft = (fLookup.LIVING_AREA_FLAG == 'Y' ? (addnGrossSqft + area) : addnGrossSqft);
							newRecordList.push(_.clone({ SOURCE_TABLE: comAddnTable, CATEGORY_ID: comAddnCatId, PARENT_RECORD: parentRecord, FOOTPRINT_AREA: Math.round(area), LIVING_AREA_PCT: (fLookup.LIVING_AREA_FLAG == 'Y' ? 100 : 0), SIZE_FACTOR_FLAG: 'N', SIZE_FACTOR: 1, NON_SKETCHED_FLAG: 'N', MAIN_LOOKUP: fLookup.LOOKUP_VALUE_PK, isNewSketch: isNewRecord, rowuid: associateRowuid }));
							newRecordTable = comAddnTable;
							deletedRecords = deletedRecords.filter(function (d) { return !(d.rowuid == associateRowuid && d.categoryId == comAddnCatId) });
						}
					}
					else if (addnOrSect && addnOrSect == 'SOCC' && sketchTable == 'CCV_COM_BUILDING') {
						sectGrossSqft = sectGrossSqft + ((apexLookup.NumericValue && apexLookup.NumericValue != '' && !isNaN(apexLookup.NumericValue)) ? (area * parseFloat(apexLookup.NumericValue)) : area);
						newRecordList.push(_.clone({ SOURCE_TABLE: secAddnTable, CATEGORY_ID: secCatId, PARENT_RECORD: parentRecord, SQFT: Math.round(area), SQFT_REFERENCE: 0, IS_SKETCHED: 'Y', OCCUPANCY: fLookup.LOOKUP_VALUE_PK, PERIMITER_INPUT: perimeter, isNewSketch: isNewRecord, rowuid: associateRowuid }));
						newRecordTable = secAddnTable;
						deletedRecords = deletedRecords.filter(function (d) { return !(d.rowuid == associateRowuid && d.categoryId == secCatId) });
					}
					if (parseFloat(associateRowuid) < 0)
						associateRowuid = '{$' + newRecordTable + '#' + parseFloat(associateRowuid) + '$}';
					else
						associateRowuid = '{' + associateRowuid + '}';

					var tempstr = vector.vectorString.split('[')[1].split(']')[0].split(/\,/);
					if (tempstr[tempstr.length - 1].indexOf('{') > -1 && tempstr[tempstr.length - 1].indexOf('}') > -1)
						tempstr.pop();
					tempstr[tempstr.length] = associateRowuid;
					vectorString += vector.vectorString.split('[')[0] + '[' + tempstr.toString() + ']' + vector.vectorString.split(']')[1] + ';';
				}
				else {
					if (code == 'BAS') {
						if (sketchTable == 'CCV_RES_BUILDING') {
							totLivingArea = totLivingArea + area;
							mb_footPrintArea = mb_footPrintArea + area;
							mb_perimeter = ((mb_perimeter == '') ? perimeter : (mb_perimeter + perimeter));
						}
						else if (sketchTable == 'CCV_COM_BUILDING')
							addnGrossSqft = addnGrossSqft + area;
						if (!basUpdated) basUpdated = true;
					}
					vectorString += vector.vectorString + ';';
				}
			}
			else {
				console.log('vector string is null');
			}						
		});
		
        
        var jsonString = sketh.jsonString;
        if (jsonString) {
        	jsonString.DCS = vectorString;
            vectorString = JSON.stringify(jsonString);
        }
        
		var sketchRecordResult = { TOTAL_LIVING_AREA: totLivingArea, MB_FOOTPRINT_AREA: Math.round(mb_footPrintArea), BUILDING_GROSS_SQFT: 0, ADDITION_GROSS_SQFT: addnGrossSqft, SECTION_GROSS_SQFT: sectGrossSqft, MB_IS_SKETCHED: isSketch, MB_SKETCH_VECTOR: '' };
        
		if (sketchTable == 'CCV_RES_BUILDING') {
        	var nonSketchedRecords = parentRecord[resAddnTable].filter(function(rd) { return rd.CC_Deleted != true && rd.NON_SKETCHED_FLAG != 'N' });
        	nonSketchedRecords.forEach(function(nonSketch) {
        		var lkCode = nonSketch['MAIN_LOOKUP'];
        		if (basUpdated && lkCode == "ADDNBSMTUF")
        			return;
        		if (lkCode) {
        			var sklook = Farragut_Lookup.filter(function(f) { return f.LOOKUP_VALUE_PK == lkCode })[0];
        			var aplook = lookup['Apex_Area_Codes'][sklook.ATTRIBUTE_VALUE];
        			sketchRecordResult.TOTAL_LIVING_AREA = sketchRecordResult.TOTAL_LIVING_AREA + ( sklook.LIVING_AREA_FLAG == 'Y' && nonSketch.FOOTPRINT_AREA? ( ( aplook && aplook.NumericValue && aplook.NumericValue != '' && !isNaN(aplook.NumericValue) )? ( parseFloat(nonSketch.FOOTPRINT_AREA) * parseFloat(aplook.NumericValue) ): parseFloat(nonSketch.FOOTPRINT_AREA) ): 0 );
        		}
        	});
        	sketchRecordResult.TOTAL_LIVING_AREA = Math.round(sketchRecordResult.TOTAL_LIVING_AREA);
			sketchRecordResult.MB_PERIMETER = ((mb_perimeter == '') ? mb_perimeter : Math.round(mb_perimeter));

            resFields.forEach(function(rField) {
                if (rField) _update_PCIChanges(rField, sketchRecordResult[rField.Name], parentRecord);
            });
        }
		else if (sketchTable == 'CCV_COM_BUILDING') {
        	var nonAddSketchedRecords = parentRecord[comAddnTable].filter(function(rd) { return rd.CC_Deleted != true && rd.NON_SKETCHED_FLAG != 'N' });
			nonAddSketchedRecords.forEach(function (nonAddSketch) {
				var lkCode = nonAddSketch['MAIN_LOOKUP'];
        		if (basUpdated && lkCode == "ADDNBSMTUF")
        			return;
        		sketchRecordResult.ADDITION_GROSS_SQFT = sketchRecordResult.ADDITION_GROSS_SQFT + (nonAddSketch.LIVING_AREA_PCT == '100' && nonAddSketch.FOOTPRINT_AREA? parseFloat(nonAddSketch.FOOTPRINT_AREA): 0);
        	});
        	
        	var nonSectSketchedRecords = parentRecord[secAddnTable].filter(function(rd) { return rd.CC_Deleted != true && rd.IS_SKETCHED != 'Y' });
        	nonSectSketchedRecords.forEach(function(nonSectSketch) {
        		sketchRecordResult.SECTION_GROSS_SQFT = sketchRecordResult.SECTION_GROSS_SQFT + (nonSectSketch.SQFT? ( (nonSectSketch.AREA_MODIFIER && nonSectSketch.AREA_MODIFIER != '' && !isNaN(nonSectSketch.AREA_MODIFIER))? ( parseFloat(nonSectSketch.AREA_MODIFIER) * parseFloat(nonSectSketch.SQFT) ): parseFloat(nonSectSketch.SQFT) ): 0);
        	});
        	
        	sketchRecordResult.BUILDING_GROSS_SQFT = Math.round(sketchRecordResult.ADDITION_GROSS_SQFT + sketchRecordResult.SECTION_GROSS_SQFT);
        	sketchRecordResult.ADDITION_GROSS_SQFT = Math.round(sketchRecordResult.ADDITION_GROSS_SQFT);
        	sketchRecordResult.SECTION_GROSS_SQFT = Math.round(sketchRecordResult.SECTION_GROSS_SQFT);
            
            comFields.forEach(function(cField) {
                if (cField) _update_PCIChanges(cField, sketchRecordResult[cField.Name], parentRecord);
            });
        }

        var insertFunction = function (newRecordLists) {
            if (newRecordList.length == 0) {
            	 var sketchField = getDataField('CC_SKETCH', sketchTable);
        		_update_PCIChanges(sketchField, vectorString, parentRecord);
                processor(copySketchData); return;
            }

            var newRecList = newRecordLists.shift();
            var recSourceTable = newRecList.SOURCE_TABLE, recCatId = newRecList.CATEGORY_ID, pRecord = newRecList.PARENT_RECORD, rowuid = newRecList.rowuid, newSketch = newRecList.isNewSketch;
            var recordUpdate = function () {
            	var currentRecord = activeParcel[recSourceTable].filter(function (s) { return s.ROWUID == rowuid })[0];
                if (recSourceTable == resAddnTable) {
                    rAddnFields.forEach(function (rfield) {
                        if (rfield && (newRecList[rfield.Name] || newRecList[rfield.Name] == 0)) _update_PCIChanges(rfield, newRecList[rfield.Name], currentRecord);
                    });
                }
                else if (recSourceTable == comAddnTable) {
                    cAddnFields.forEach(function (cfield) {
                        if (cfield && (newRecList[cfield.Name] || newRecList[cfield.Name] == 0)) _update_PCIChanges(cfield, newRecList[cfield.Name], currentRecord);
                    });
                }
                else if (recSourceTable == secAddnTable) {
                    snFields.forEach(function (sfield) {
                        if (sfield && (newRecList[sfield.Name] || newRecList[sfield.Name] == 0)) _update_PCIChanges(sfield, newRecList[sfield.Name], currentRecord);
                    });
                }
	        	insertFunction(newRecordLists);  
            }
            if (newSketch) {
	            insertNewAuxRecord(null, pRecord, recCatId, rowuid, null, function (rowid) {
	                recordUpdate();
		        });
	        }
	        else 
	        	recordUpdate();	
        }
        
        insertFunction(newRecordList);           
    }
    processor(copySketchData);	
}

function T2SketchAfterSave(sketchData, callback) {
	if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
		if (callback) callback();
		return;
	}

	let sketch = sketchApp.sketches.filter((s) => { return s.sid == sketchData[0].rowid })[0],
		resTable = 'ccv_tAA_VSTerraGon_Res', comTable = 'ccv_tAA_VSTerraGon_Com', miscTable = 'ccv_tAA_Misc', apextable = 'tAA_ApexPolygon', newRecordRowuid = ccTicks(),
		resCat = getCategoryFromSourceTable(resTable), comCat = getCategoryFromSourceTable(comTable), miscCat = getCategoryFromSourceTable(miscTable), apexCat = getCategoryFromSourceTable(apextable),
		resFields = ['PolygonID', 'SketchBaseArea', 'SketchMultiplier', 'SketchTotalArea'], resDataFields = {},
		comFields = ['PolygonID', 'SketchBaseArea', 'SketchMultiplier', 'SketchTotalArea'], comDataFields = {},
		apexFields = [ 'AreaCode', 'AreaSize', 'Multiplier', 'Description', 'Page', 'Perimeter'], apexDataFields = {}, updatedRecordList = [],
		miscFields = ['PolygonID'], rcmFields = ['PolygonID', 'V04', 'V05', 'V01'], miscDataFields = [], vectorString = '', newRecords = [], isAssociatedField = getDataField('IsAssociated', apextable),
		vsField = getDataField('CC_SKETCH'), associateSource = sketchApp.config.AssociateVector.source, IsDeletedField = getDataField('IsDeleted', apextable);

	_PCIBatchArray = [];

	resFields.forEach((f) => { resDataFields[f] = getDataField(f, resTable); });
	comFields.forEach((f) => { comDataFields[f] = getDataField(f, comTable); });
	miscFields.forEach((f) => { miscDataFields[f] = getDataField(f, miscTable); });
	apexFields.forEach((f) => { apexDataFields[f] = getDataField(f, apextable); });

	var updateRecordPCI = function () {
		ccma.Sync.enqueueBulkParcelChanges(_PCIBatchArray, function () {
			getParcel(activeParcel.Id, function () {
				if (callback) callback(); return;
			});
		});
	}

	var widthLengthCalculator = function(points) {
		var hLength = 0, RLength = 0, lT = 0, uT = 0, imp_length = 0, imp_width = 0;
		points.length > 1 && points[1].split(/\s/).forEach(function (p, i) {
			if (p.trim() != '') {
				if (p.split("/").length > 1)
					return;
				else {
					if ((p.split("L").length > 1 || p.split("R").length > 1) && lT == 0) {
						hLength = p.split("L").length > 1 ? parseFloat(p.split("L")[1]) : parseFloat(p.split("R")[1]);
						lT = 1;
					}
					else if ((p.split("U").length > 1 || p.split("D").length > 1) && uT == 0) {
						RLength = p.split("U").length > 1 ? parseFloat(p.split("U")[1]) : parseFloat(p.split("D")[1]);
						uT = 1;
					}
				}
			}
		});

		if (hLength > RLength) {
			imp_length = hLength; imp_width = RLength;
		}
		else {
			imp_length = RLength; imp_width = hLength;
		}
		return ({ imp_length: imp_length, imp_width: imp_width });
	}

	var createNewApexRecords = function (nRecords) {
		if (nRecords.length == 0) {
			updateRecordPCI(); return;
		}

		let nr = nRecords.pop();
		insertNewAuxRecord(null, null, nr.Category.Id, nr.Rowuid, null, (rowid) => {
			createNewApexRecords(nRecords);
		});
	}

	var findAssociatedRecord = function (sk) {
		let splitVectorString = sk.vectorString.split('[')[1].split(']')[0], rowuid = -1, apex_id = parseInt(splitVectorString.split(/\,/g)[4]), polyRec = null, polyId = null, srecord = null, type = null;
		if (apex_id < 0) {
			if (splitVectorString.indexOf('{') > -1 && splitVectorString.indexOf('}') > -1)
				rowuid = splitVectorString.indexOf('#') > -1 ? (splitVectorString.split('#')[1].split('$')[0]) : splitVectorString.split('{')[1].split('}')[0];
			else if (sk.tableRowId)
				rowuid = sk.tableRowId.indexOf('#') > -1 ? (sk.tableRowId.split('#')[1].split('$')[0]) : sk.tableRowId.split('{')[1].split('}')[0];
		}

		polyRec = activeParcel['tAA_ApexPolygon'].filter((x) => {
			return ((x.UniqueID == (apex_id > -1 ? apex_id : -99999)) || (x.ROWUID == (apex_id < 0 && x.CC_RecordStatus == 'I' ? rowuid : -99999))) && x.CC_Deleted != true && x.IsDeleted != '1'
		})[0];
		if (polyRec) {
			polyId = polyRec.CC_RecordStatus != 'I' ? polyRec.PolygonID : polyRec.ROWUID;
			if (polyId) {
				for (i in associateSource) {
					let src = associateSource[i], tbl = src['SourceTable'], srec = activeParcel[tbl].filter((rec) => { return (rec.CC_Deleted != true && rec.IsDeleted != '1' && rec.PolygonID == polyId) })[0];
					if (srec) {
						srecord = srec; type = src.Type;
						break;
					}
				}
			}
		}

		return { PolygonRecord: polyRec, SourceRecord: srecord, Type: type };
	}

	sketchApp.deletedVectors.forEach((v) => {
		let rec = findAssociatedRecord(v);
		if (rec) {
			if (rec.PolygonRecord) {
				ccma.Data.Controller.DeleteAuxRecordWithDescendants(apexCat.Id, rec.PolygonRecord.ROWUID, null, null, () => { }, false, null, true);
				_update_PCIChanges(IsDeletedField, '1', rec.PolygonRecord);
			}

			if (rec.SourceRecord) {
				let datFields = rec.Type == 'Res' ? resDataFields : (rec.Type == 'Com' ? comDataFields : ((rec.Type == 'Imp' || rec.Type == 'Mrs' || rec.Type == 'Mcm') ? miscDataFields : null));
				for (k in datFields) {
					let fl = datFields[k]; _update_PCIChanges(fl, '', rec.SourceRecord);
				}
			}
		}
	});

	sketch.vectors.forEach((vector) => {
		if (vector.isFreeFormLineEntries) {
			vectorString += vector.vectorString + ';';
			return;
		}

		let area = parseFloat(vector.area()), perimeter = vector.perimeter(), code = vector.code, apexLookup = lookup['Apex_Area_Codes'][code],
			associateRowuid = vector.associateRowuid, polyRowuId = null, polyRecord = null, oldSrcRecord = null, oldType = null, len = 0, wid = 0;

		if (vector.vectorString && vector.vectorString != '') {
			let pts = vector.vectorString.split(':')[1].split(' S '), lenWidth = widthLengthCalculator(pts);
			len = Math.round(lenWidth.imp_length);wid = Math.round(lenWidth.imp_width);
        }

		if (vector.newRecord) {
			newRecordRowuid = polyRowuId = newRecordRowuid + 10;
			newRecords.push({ Category: apexCat, Rowuid: newRecordRowuid, Parent: null });
			let apRec = { 'AreaCode': code, 'AreaSize': area, 'Multiplier': (apexLookup?.NumericValue ? parseFloat(apexLookup.NumericValue) : 1), 'Description': (apexLookup?.Description ? apexLookup.Description : vector.name), 'Perimeter': perimeter, 'Page': vector.pageNum };
			for (k in apRec) {
				let fl = apexDataFields[k];
				_PCIBatchArray.push({ parcelId: activeParcel.Id, auxRowId: newRecordRowuid, parentAuxRowId: null, action: 'E', field: fl, oldValue: '', newValue: apRec[k] });
			}
			_PCIBatchArray.push({ parcelId: activeParcel.Id, auxRowId: newRecordRowuid, parentAuxRowId: null, action: 'E', field: isAssociatedField, oldValue: '', newValue: associateRowuid? '1': '0' });

			let tempstr = vector.vectorString.split('[')[1].split(']')[0].split(/\,/);
			tempstr[tempstr.length] = '{$' + apextable + '#' + parseFloat(newRecordRowuid) + '$}';
			vectorString += vector.vectorString.split('[')[0] + '[' + tempstr.toString() + ']' + vector.vectorString.split(']')[1] + ';';
		}
		else {
			let asrec = findAssociatedRecord(vector);
			if (asrec?.PolygonRecord) {
				polyRecord = asrec.PolygonRecord; polyRowuId = asrec.PolygonRecord.ROWUID;
				oldSrcRecord = asrec.SourceRecord; oldType = asrec.Type;
			}
		}

		if (polyRowuId) {
			let tp = associateRowuid?.substring(0, 3), roId = associateRowuid?.substring(3), mul = (apexLookup?.NumericValue ? parseFloat(apexLookup.NumericValue) : 1), desc = (apexLookup?.Description ? apexLookup.Description : vector.name),
				ntbl = tp == 'Res' ? resTable : (tp == 'Com' ? comTable : (tp == 'Imp' ? miscTable : (tp == 'Mrs' ? 'ccv_tAA_VS_Misc_Res' : 'ccv_tAA_VS_Misc_Com'))),
				recs = ntbl ? activeParcel[ntbl].filter((x) => { return x.ROWUID == roId })[0] : null, apRec = null;

			if (polyRecord?.CC_RecordStatus == 'I') {
				let nassociateRowuid = null;
				if (parseFloat(polyRowuId) < 0) nassociateRowuid = '{$' + apextable + '#' + parseFloat(polyRowuId) + '$}';
				else nassociateRowuid = '{' + polyRowuId + '}';

				let tempstr = vector.vectorString.split('[')[1].split(']')[0].split(/\,/);
				if (tempstr[tempstr.length - 1].indexOf('{') > -1 && tempstr[tempstr.length - 1].indexOf('}') > -1) tempstr.pop();
				tempstr[tempstr.length] = nassociateRowuid;
				vectorString += vector.vectorString.split('[')[0] + '[' + tempstr.toString() + ']' + vector.vectorString.split(']')[1] + ';';
			}
			else if (!vector.newRecord) {
				vectorString += vector.vectorString + ';';
			}

			if (vector.isModified && polyRecord) {
				let apexRec = { 'AreaCode': code, 'AreaSize': area, 'Multiplier': mul, 'Description': desc, 'Perimeter': perimeter, 'Page': vector.pageNum };
				for (k in apexRec) {
					let fl = apexDataFields[k];
					_update_PCIChanges(fl, apexRec[k], polyRecord);
					//_PCIBatchArray.push({ parcelId: activeParcel.Id, auxRowId: polyRecord.ROWUID, parentAuxRowId: null, action: 'E', field: fl, oldValue: polyRecord.Original[fl.Name] ? polyRecord.Original[fl.Name]: null, newValue: apexRec[k] });
				}
			}

			if (vector.newRecord && associateRowuid && recs) {
				oldType = tp;
				apRec = { 'PolygonID': polyRowuId, 'SketchBaseArea': area, 'SketchMultiplier': mul, 'SketchTotalArea': Math.round(area * mul) };
			}
			else if (oldSrcRecord && !associateRowuid) {
				let apexRec = { 'PolygonID': '', 'SketchBaseArea': '', 'SketchMultiplier': '', 'SketchTotalArea': '' };

				for (k in apexRec) {
					let fl = null;
					if (oldType == 'Res') fl = resDataFields[k];
					else if (oldType == 'Com') fl = comDataFields[k];
					else if (oldType == 'Imp' || oldType == 'Mrs' || oldType == 'Mcm') fl = getDataField(k, ntbl);

					if (fl) _PCIBatchArray.push({ parcelId: activeParcel.Id, auxRowId: oldSrcRecord.ROWUID, parentAuxRowId: oldSrcRecord.ParentROWUID, action: 'E', field: fl, oldValue: oldSrcRecord.Original[fl.Name] ? oldSrcRecord.Original[fl.Name]: null, newValue: apexRec[k] });
				}
				_update_PCIChanges(isAssociatedField, '0', polyRecord);
				//_PCIBatchArray.push({ parcelId: activeParcel.Id, auxRowId: polyRecord.ROWUID, parentAuxRowId: null, action: 'E', field: isAssociatedField, oldValue: polyRecord.Original[isAssociatedField.Name], newValue: '0' });
			}
			else if (recs && polyRecord && ((polyRecord.CC_RecordStatus == 'I' && polyRecord.ROWUID != recs.PolygonID) || (polyRecord.CC_RecordStatus != 'I' && polyRecord.PolygonID != recs.PolygonID))) {
				apRec = { 'PolygonID': polyRecord.CC_RecordStatus == 'I' ? polyRecord.ROWUID : polyRecord.PolygonID, 'SketchBaseArea': area, 'SketchMultiplier': mul, 'SketchTotalArea': Math.round(area * mul) };
				_update_PCIChanges(isAssociatedField, '1', polyRecord);

				let modRec = updatedRecordList.filter((x) => { return x.rowuid == oldSrcRecord?.ROWUID && x.type == oldType })[0];
				if (!modRec && oldSrcRecord) {
					let apexRec = { 'PolygonID': '', 'SketchBaseArea': '', 'SketchMultiplier': '', 'SketchTotalArea': '' };

					for (k in apexRec) {
						let fl = null;
						if (oldType == 'Res') fl = resDataFields[k];
						else if (oldType == 'Com') fl = comDataFields[k];
						else if (oldType == 'Imp' || oldType == 'Mrs' || oldType == 'Mcm') fl = getDataField(k, ntbl);
						if (fl) _PCIBatchArray.push({ parcelId: activeParcel.Id, auxRowId: oldSrcRecord.ROWUID, parentAuxRowId: oldSrcRecord.ParentROWUID, action: 'E', field: fl, oldValue: oldSrcRecord.Original[fl.Name] ? oldSrcRecord.Original[fl.Name] : null, newValue: apexRec[k] });
					}
				}
				oldType = tp;
				updatedRecordList.push({ rowuid: recs.ROWUID, type: tp });
				//_PCIBatchArray.push({ parcelId: activeParcel.Id, auxRowId: polyRecord.ROWUID, parentAuxRowId: null, action: 'E', field: isAssociatedField, oldValue: polyRecord.Original[isAssociatedField.Name] ? polyRecord.Original[isAssociatedField.Name]: null, newValue: '1' });
			}
			else if (recs && polyRecord && ((polyRecord.CC_RecordStatus == 'I' && polyRecord.ROWUID == recs.PolygonID) || (polyRecord.CC_RecordStatus != 'I' && polyRecord.PolygonID == recs.PolygonID)) && vector.isModified) {
				oldType = tp;
				apRec = { 'PolygonID': polyRecord.CC_RecordStatus == 'I' ? polyRecord.ROWUID : polyRecord.PolygonID, 'SketchBaseArea': area, 'SketchMultiplier': mul, 'SketchTotalArea': Math.round(area * mul) };
			}

			if (apRec) {
				apRec['V04'] = len, apRec['V05'] = wid; apRec['V01'] = area;
				for (k in apRec) {
					let fl = null;
					if (oldType == 'Res') fl = resDataFields[k];
					else if (oldType == 'Com') fl = comDataFields[k];
					else if (oldType == 'Imp' || oldType == 'Mrs' || oldType == 'Mcm') fl = getDataField(k, ntbl);
					if (fl) _update_PCIChanges(fl, apRec[k], recs);
				}
			}
		}
		else if (!vector.newRecord) { vectorString += vector.vectorString + ';'; }
	});

	let jsonString = sketch.jsonString;
	if (jsonString) { jsonString.DCS = vectorString; vectorString = JSON.stringify(jsonString); }
	if (vsField) _PCIBatchArray.push({ parcelId: activeParcel.Id, auxRowId: null, parentAuxRowId: null, action: 'E', field: vsField, oldValue: activeParcel.Original[vsField.Name] ? activeParcel.Original[vsField.Name]: null, newValue: vectorString });
	createNewApexRecords(newRecords);
}

function LucasDTRAfterSave(sketchData, callback) {
	if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }
    
    _PCIBatchArray = [];
	var storyField = getDataField('STORIES', 'DWELDAT'), cduField = getDataField('CDU', 'DWELDAT'),
		yearField = getDataField('YRBLT', 'DWELDAT'), gradeField = getDataField('GRADE', 'DWELDAT');
    
    var updateRecordPCI = function() {
    	ccma.Sync.enqueueBulkParcelChanges( _PCIBatchArray, function() {  
			getParcel(activeParcel.Id, function () {
				if (callback) callback(); return;
			});
		});
    }
    
    sketchApp.sketches.forEach(function(skt) {
        if (skt.config.DoNotAddOrEditFirstRecordlabel) {
        	var sktRecord = skt.parentRow;
        	var firstRecordVector = skt.vectors.filter(function(vect) { return vect.firstLabelValueModified })[0];
			if (firstRecordVector && sktRecord && firstRecordVector.label) {
				var valMod = firstRecordVector.firstLabelValueModified;
        		var spLabel = firstRecordVector.label.split(']')[1];
		    	if (spLabel) {
		    		spLabel = spLabel.trim();
		    		var stories = (spLabel.split('/')[0] && spLabel.split('/')[0] != '???' && spLabel.split('/')[0] != '---') ? spLabel.split('/')[0]: '';
        			if (stories)
        				_update_PCIChanges(storyField, stories, sktRecord);
				}

				if (cduField && valMod.cdu) _update_PCIChanges(cduField, valMod.cdu, sktRecord);
				if (yearField && valMod.ybuilt) _update_PCIChanges(yearField, valMod.ybuilt, sktRecord);
				if (gradeField && valMod.grade) _update_PCIChanges(gradeField, valMod.grade, sktRecord);
        	}
        }
    }); 
    
    if (_PCIBatchArray.length > 0) updateRecordPCI();
    else if (callback) { callback();  return;  }
}

function AACAfterSave(sketchData, callback) {
	if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
        if (callback) callback();
        return;
    }
    
    _PCIBatchArray = [];
    
    let res_tble = 'res_building_tb', res_att_tble = 'res_attachments_sup',
    	copySketchData = [], res_lookup = lookup.res_sketch_lookup, res_building_fields = {}, deletedRecords = [],
    	res_att_catId = getCategoryFromSourceTable(res_att_tble).Id, newRowuid = null, newRecords = [],
    	areaField = getDataField('area', res_att_tble), typeField = getDataField('attachment_type_cd', res_att_tble);
    
    for (l in res_lookup) {
	    let lk = res_lookup[l];
	    if (!res_building_fields[lk.Object.column_name])
	    	res_building_fields[lk.Object.column_name] = { value: 0, field: getDataField(lk.Object.column_name, res_tble) };
	}
    res_building_fields['sfla'] = { value: 0, field: getDataField('sfla', res_tble) };
    
    var updateRecordPCI = function() {
    	ccma.Sync.enqueueBulkParcelChanges( _PCIBatchArray, function() {  
			getParcel(activeParcel.Id, function () {
				if (callback) callback(); return;
			});
		});
    } 
    
    var createNewRecord = function(newRecds) {
    	if (newRecds.length == 0) {
    		updateRecordPCI();
    		return;
    	}
    	
    	let newRec = newRecds.shift();
    	newRowuid += 12;
    	insertNewAuxRecord(null, newRec.parentRecord, newRec.catId, newRowuid, { area: newRec.area, attachment_type_cd: newRec.attachment_type_cd }, (rowid) => {
            createNewRecord(newRecds);
        });
    }
    
    var deleteRecord = function (delRecords, delCallback) {
        if (delRecords.length == 0) {
            if (delCallback) delCallback();
            else {
            	newRowuid = ccTicks(); createNewRecord(newRecords);
            }
            return;
        }

        let delRec = delRecords.pop(),
        	rel = ( delRecords.length == 0 && ( typeof(cachedAuxOptions) != 'undefined') && !(isEmpty(cachedAuxOptions) ) && cachedAuxOptions[delRec.categoryId])? true: false;
        ccma.Data.Controller.DeleteAuxRecordWithDescendants(delRec.categoryId, delRec.rowuid, null, null, function () {
            deleteRecord(deletedRecords, delCallback);
        }, rel, null, true);
    }
    
    var processor = function(sketchRecords) {
	    if (sketchRecords.length == 0) {
	    	deleteRecord(deletedRecords); 
	    	return;
	    }
	    
	    let sketchRecord = sketchRecords.shift(),
        	sketch = sketchApp.sketches.filter(function(s) { return s.sid == sketchRecord.sid })[0],
        	vectors = sketch.vectors,
        	sketchTable = sketchRecord.sketchTable,
        	parentRecord = activeParcel[sketchTable].filter(function (s) { return s.ROWUID == sketchRecord.sid && s.CC_Deleted != true })[0],
    		res_building_fields_clone = _.clone(res_building_fields);
    	
    	vectors.forEach((v) => {
    		let vname = v.name, area = Math.round(v.area()), currentVectorArea = parseFloat(v.currentVectorArea);
    		vname.split('/').forEach((vn) => {
    			let res_lk = res_lookup[vn];
    			if (res_lk && res_lk['Object']) {
    				let res_lk_ob = res_lk['Object'];
    				if (res_lk_ob['table_name'] == 'res_building_tb') {
    					let ml = parseFloat(res_lk_ob['multiplier']), is_sfla = parseFloat(res_lk_ob['is_sfla']);
    					if (res_lk_ob['column_name'] && ml > 0) {
    						res_building_fields_clone[res_lk_ob['column_name']].value = (res_building_fields_clone[res_lk_ob['column_name']].value + (area * ml));
    						res_building_fields_clone['sfla'].value = (res_building_fields_clone['sfla'].value + (area * ml * is_sfla));
    					}
    				}
    				else if (res_lk_ob['table_name'] == 'res_attachments_sup') {
    					if (v.newRecord) newRecords.push({ catId: res_att_catId, parentRecord: parentRecord, area: area, attachment_type_cd: vn });
			    		else {
			    			if (v.isModified) {
			    				let exeRec = activeParcel[res_att_tble].filter((r) => { return ((r.ParentROWUID == parentRecord.ROWUID) && (r.attachment_type_cd == vn) && ((currentVectorArea - 1) <= (Math.round(r.area)) && (Math.round(r.area)) <= (currentVectorArea + 1))) })[0];
				    			if (exeRec) {
				    				_update_PCIChanges(areaField, area, exeRec); _update_PCIChanges(typeField, vn, exeRec);
				    			}
				    			else newRecords.push({ catId: res_att_catId, parentRecord: parentRecord, area: area, attachment_type_cd: vn });
			    			}
			    		} 
    				}	    			
		    	}
    		});    		
    	});
    	
    	for (x in res_building_fields_clone) {
    		let res_field = res_building_fields_clone[x];
    		_update_PCIChanges(res_field.field, Math.round(res_field.value), parentRecord);
    	}
    	
    	processor(sketchRecords); 
    }
    
    sketchApp.deletedVectors.filter((d) => { return !d.newRecord }).forEach((del) => {
    	let vname = del.name, area = Math.round(del.area()), skt = del.sketch, 
    		type = skt && skt.config? skt.config.SketchLabelPrefix: null,
    		prec = activeParcel[res_tble].filter(function (s) { return s.ROWUID == skt.sid && s.CC_Deleted != true })[0], 
    		currentVectorArea = parseFloat(del.currentVectorArea);
    		
    	if (type && type.indexOf('RES -') > -1) {		  	
    		vname.split('/').forEach((vn) => {
    			let delRec = activeParcel[res_att_tble].filter((r) => { return ((r.ParentROWUID == prec.ROWUID) && (r.attachment_type_cd == vn) && ((currentVectorArea - 1) <= (Math.round(r.area)) && (Math.round(r.area)) <= (currentVectorArea + 1))) })[0];
    			if (delRec) deletedRecords.push({ rowuid: delRec.ROWUID, categoryId: res_att_catId });
    		});
    	}   	
    });
    
    deleteRecord(deletedRecords, () => {
    	sketchData.forEach((sdata) => {
	        let skt = sketchApp.sketches.filter(function(s) { return s.sid == sdata.rowid })[0];
	        let sktTable = skt.config.SketchSource.Table;
	        let sktRecord = activeParcel[sktTable].filter(function (s) { return s.ROWUID == sdata.rowid && s.CC_Deleted != true })[0];
	       
	        if (sktTable == res_tble)
		    	copySketchData.push({ sid: sdata.rowid, sketchTable: sktTable, parentRecord: sktRecord });
	    });
	    
    	processor(copySketchData); 
    }); 
}

function SigmaCuyahogaAfterSave(sketchData, callback) {
	if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
		if (callback) callback();
		return;
	}

	_PCIBatchArray = [];

	let amenityTbl = 'RES_AMENITY', measField = getDataField('AMENITY_MEAS', amenityTbl), typeField = getDataField('AMENITY_TYPE', amenityTbl),
		newRecords = [], deleteRecords = [], amenityCat = getCategoryFromSourceTable(amenityTbl);

	var updateRecordPCI = function () {
		ccma.Sync.enqueueBulkParcelChanges(_PCIBatchArray, function () {
			getParcel(activeParcel.Id, function () {
				_PCIBatchArray = [];
				if (callback) callback(); return;
			});
		});
	}

	var createNewRecord = function (newRecds) {
		if (newRecds.length == 0) {
			updateRecordPCI();
			return;
		}

		let newRec = newRecds.shift();
		insertNewAuxRecord(null, newRec.parentRecord, newRec.catId, newRec.rowuid, {}, (rowid) => {
			createNewRecord(newRecds);
		});
	}

	var deleteRecord = function (delRecords) {
		if (delRecords.length == 0) {
			createNewRecord(newRecords);
			return;
		}

		let delRec = delRecords.pop(), rel = (delRecords.length == 0 && (typeof (cachedAuxOptions) != 'undefined') && !(isEmpty(cachedAuxOptions)) && cachedAuxOptions[delRec.categoryId]) ? true : false;
		ccma.Data.Controller.DeleteAuxRecordWithDescendants(delRec.categoryId, delRec.rowuid, null, null, function () {
			deleteRecord(delRecords);
		}, rel, null, true);
	}

	sketchData.forEach((sketchRecord) => {
		let sketch = sketchApp.sketches.filter(function (s) { return s.sid == sketchRecord.rowid })[0], vectors = sketch?.vectors ? sketch.vectors : [],
			parentRecord = activeParcel['RESIDENTIAL_BUILDING'].filter(function (s) { return s.ROWUID == sketchRecord.rowid && s.CC_Deleted != true })[0],
			amenityRecords = parentRecord[amenityTbl], amenityProcessingRecords = [], newRowuid = ccTicks();

		amenityRecords.forEach((r) => { amenityProcessingRecords.push({ Rowuid: r.ROWUID, Type: r.AMENITY_TYPE, Area: 0, isProcessed: false, record: r }); });
		sketch.MSKetchArray.forEach((x) => {
			vectors.push({ name: x.msketchLabel, area: x.mSketchArea, marea: true });
		});
		let delVect = sketchApp.deletedVectors.filter((x) => { return x.sketch?.sid == sketchRecord.rowid });
		delVect.forEach((x) => { x.IsSkDeleted = true; vectors.push(x); });

		vectors.forEach((vect) => {
			let vname = vect.name, area = vect.marea ? Math.round(vect.area): Math.round(vect.area());
			if (vect.IsSkDeleted) area = 0;

			vname.split('/').forEach((vn) => {
				let slk = _.filter(lookup["sketch_label_lookup"], (obj) => { return obj.Id == vn })[0],
					lk = SigmaCuyahoga_Lookup.filter((x) => { return slk ? caseInsensitiveCompare(x.Desc, slk.Name) : x.CODE == vn })[0];

				if (lk) {
					let rec = amenityProcessingRecords.filter((x) => { return x.Type == lk.CODE })[0];
					if (rec) {
						rec.Area = rec.Area + area; rec.isProcessed = true;
					}
					/*else {
						newRowuid = newRowuid + 10;
						newRecords.push({ rowuid: newRowuid, catId: amenityCat.Id, parentRecord: parentRecord });
						amenityProcessingRecords.push({ Rowuid: newRowuid, Type: lk.CODE, Area: area, isProcessed: true });
					}*/
				}
			});
		});

		amenityProcessingRecords.forEach((r) => {
			if (r.isProcessed && r.record) {
				_update_PCIChanges(measField, r.Area, r.record); _update_PCIChanges(typeField, r.Type, r.record);
			}
			/*else if (r.isProcessed) {
				_PCIBatchArray.push({ parcelId: activeParcel.Id, auxRowId: r.Rowuid, parentAuxRowId: parentRecord.ROWUID, action: 'E', field: measField, oldValue: null, newValue: r.Area });
				_PCIBatchArray.push({ parcelId: activeParcel.Id, auxRowId: r.Rowuid, parentAuxRowId: parentRecord.ROWUID, action: 'E', field: typeField, oldValue: null, newValue: r.Type });
			}
			else {
				deleteRecords.push({ rowuid: r.Rowuid, categoryId: amenityCat.Id });
			}*/
		});

		sketch.vectors = sketch.vectors.filter((x) => { return !x.marea });
	});

	deleteRecord(deleteRecords);
}

function LucasDTRSketchBeforeSave(sketches, callback) {
	let msg = "";
	if (sketches === undefined || sketches == null) {
		if (callback) callback(true);
		return;
	}

	sketches.filter((x) => { return x.label.contains("COM") }).forEach((sk) => {
		sk.vectors.filter((m) => { return m.isModified }).forEach((v) => {
			if (v.vectorConfig?.Table == "COMINTEXT" && !v.newRecord) {
				let uId = v.uid, rec = activeParcel['COMINTEXT'].filter((r) => { return r.ROWUID == uId })[0];
				if (rec) {
					let area = v.fixedArea ? parseFloat(v.fixedArea) : parseFloat(v.area()), peri = v.perimeter(), rarea = rec.AREA, rperi = rec.PERIM, areac = Math.ceil(area), areaf = Math.floor(area);
					if (!(area == 0 && peri == 0)) {
						if (!(rarea >= areaf && rarea <= areac) && peri != rperi) {
							msg += "The " + v.label + " sketch Area/SQFT has changed from " + rarea + " to " + area + ", and the Perimeter has changed from " + rperi + " to " + peri + ". <br>";
						}
						else if (!(rarea >= areaf && rarea <= areac)) {
							msg += "The " + v.label + " sketch Area/SQFT has changed from " + rarea + " to " + area + ". <br>";
						}
						else if (peri != rperi) {
							msg += "The " + v.label + " sketch Perimeter has changed from " + rperi + " to " + peri + ". <br>";
						}
					}
				}
			}
		});
	});

	if (msg != "") {
		msg += 'Pressing "Okay" will proceed with the sketch Save.'
		messageBox(msg, ['OK', 'Cancel'], () => {
			if (callback) callback(true, true);
		}, () => {
			if (callback) callback(false, false);
		});
	}
	else if (callback) callback(true, true);
}

function IASUSSketchBeforeSave(sketches, callback) {
	let html = "";
	if (sketches === undefined || sketches == null) {
		if (callback) callback(true);
		return;
	}

	sketches.filter((x) => { return x.label.contains("COM") }).forEach((sk) => {
		sk.vectors.filter((m) => { return m.isModified }).forEach((v) => {
			if (v.vectorConfig?.Table == "COMINTEXT" && !v.newRecord) {
				let uId = v.uid, rec = activeParcel['COMINTEXT'].filter((r) => { return r.ROWUID == uId })[0];
				if (rec) {
					let area = v.fixedArea ? parseFloat(v.fixedArea) : parseFloat(v.area()), peri = v.perimeter(), rarea = rec.AREA, rperi = rec.PERIM, areac = Math.ceil(area), areaf = Math.floor(area);
					if (!(area == 0 && peri == 0)) {
						if (!(rarea >= areaf && rarea <= areac) && peri != rperi) {
							html += "<span style='display: block; width: 90% !important; font-size: 14px !important; margin-bottom: 10px;'><input type='checkbox' class='com-diff-area' suid='" + sk.uid +"' uid='" + v.uid + "' rarea='" + rarea + "' rperi='" + rperi +"' style='width: 22px; margin-right: 20px;'/> <label>The " + v.label + " sketch Area/SQFT has changed from " + rarea + " to " + area + ",<br> and the Perimeter has changed from " + rperi + " to " + peri + ". </label></span><br>";
						}
						else if (!(rarea >= areaf && rarea <= areac)) {
							html += "<span style='display: block; width: 90% !important; font-size: 14px !important; margin-bottom: 10px;'><input type='checkbox' class='com-diff-area' suid='" + sk.uid +"' uid='" + v.uid + "' rarea='" + rarea +"' style='width: 22px; margin-right: 20px;'/> <label>The " + v.label + " sketch Area/SQFT has changed from " + rarea + " to " + area + ". </label></span><br>";
						}
						else if (peri != rperi) {
							html += "<span style='display: block; width: 90% !important; font-size: 14px !important; margin-bottom: 10px;'><input type='checkbox' class='com-diff-area' suid='" + sk.uid +"' uid='" + v.uid + "' rperi='" + rperi +"' style='width: 22px; margin-right: 20px;'/><label>The " + v.label + " sketch Perimeter has changed from " + rperi + " to " + peri + ". </label></span><br>";
						}
					}
				}
			}
		});
	});

	if (html != "") {
		$('.control_div div, .UnSketched').remove();
		$('.Current_vector_details .head').html("Sketch Area Updation");
		$('.mask').show(); $('.dynamic_prop').html('');
		$('.dynamic_prop').append('<div class="divvectors"></div>');
		$('.divvectors').append(html);
		$('.skNote').html("<span><b>Note:</b> Enable the checkbox to keep the COMINTEXT area different than the sketch's area. <br>(Sketch area will not update in COMINTEXT)</span>");
		$('.skNote').css('color', '#fb2300')
		$('.Current_vector_details').show();
		$('.divvectors').css('width', 710);
		$('.skNote').show();
		$('.divvectors').css('max-height', 400);
		$('.Current_vector_details').css('max-width', 750);

		$('.Current_vector_details #Btn_cancel_vector_properties').unbind(touchClickEvent)
		$('.Current_vector_details #Btn_cancel_vector_properties').bind(touchClickEvent, function () {
			$('.skNote').html('');
			$('.Current_vector_details').hide();
			if (!sketchApp.isScreenLocked) $('.mask').hide();
			hideKeyboard();
			return false;
		});

		$('.Current_vector_details #Btn_Save_vector_properties').unbind(touchClickEvent)
		$('.Current_vector_details #Btn_Save_vector_properties').bind(touchClickEvent, function () {
			sketchApp.EAConfigComTextKeepAreaVects = [];
			$('.com-diff-area').each(function () {
				let sk = sketchApp.sketches.filter((x) => { return x.uid == $(this).attr('suid') })[0];
				if (sk) {
					let vk = sk.vectors.filter((x) => { return x.uid == $(this).attr('uid') })[0];
					if (vk) {
						let ra = $(this).attr('rarea'), pe = $(this).attr('rperi');
						if ($(this)[0].checked) {
							if (ra) vk.comTextKeepArea = parseFloat(ra);
							if (pe) vk.comTextKeepPeri = parseFloat(pe);
							sketchApp.EAConfigComTextKeepAreaVects.push({ uid: $(this).attr('uid'), keepArea: true });
						}
						else {
							sketchApp.EAConfigComTextKeepAreaVects.push({ uid: $(this).attr('uid'), keepArea: false });
                        }	
					}
				}
			});

			$('.skNote').html('');
			$('.Current_vector_details').hide();
			if (!sketchApp.isScreenLocked) $('.mask').hide();
			hideKeyboard();
			if (callback) callback(true, true);
			return false;
		});
	}
	else if (callback) callback(true, true);
}

function IASUSSketchAfterSave(sketchData, callback) {
	if (!(sketchApp.EAConfigComTextKeepAreaVects)) sketchApp.EAConfigComTextKeepAreaVects = [];
	if (sketchData == null || sketchData == undefined || sketchData.length == 0 ) {
		if (callback) callback();
		return;
	}

	sketchApp.EAConfigComTextKeepAreaVects.forEach((vc) => {
		let rec = activeParcel['COMINTEXT'].filter((r) => { return r.ROWUID == vc.uid })[0];
		if (rec) {
			let msg = vc.keepArea ? "Keep the COMINTEXT record #" + vc.uid + " area different than the sketch's area" : "Update the COMINTEXT record #" + vc.uid +" to match the sketch's area";
			ccma.Sync.enqueueParcelChange(activeParcel.Id, '', '', 'audit', 'AUDIT', 'AUDIT', '', msg, {});
        }
	});
	sketchApp.EAConfigComTextKeepAreaVects = [];

	_PCIBatchArray = [];
	var storyField = getDataField('STORIES', 'DWELDAT'), cduField = getDataField('CDU', 'DWELDAT'),
		yearField = getDataField('YRBLT', 'DWELDAT'), gradeField = getDataField('GRADE', 'DWELDAT');

	var updateRecordPCI = function () {
		ccma.Sync.enqueueBulkParcelChanges(_PCIBatchArray, function () {
			getParcel(activeParcel.Id, function () {
				if (callback) callback(); return;
			});
		});
	}

	sketchApp.sketches.forEach(function (skt) {
		if (skt.config.DoNotAddOrEditFirstRecordlabel) {
			var sktRecord = skt.parentRow;
			var firstRecordVector = skt.vectors.filter(function (vect) { return vect.firstLabelValueModified })[0];
			if (firstRecordVector && sktRecord && firstRecordVector.label) {
				var valMod = firstRecordVector.firstLabelValueModified;
				var spLabel = firstRecordVector.label.split(']')[1];
				if (spLabel) {
					spLabel = spLabel.trim();
					var stories = (spLabel.split('/')[0] && spLabel.split('/')[0] != '???' && spLabel.split('/')[0] != '---') ? spLabel.split('/')[0] : '';
					if (stories)
						_update_PCIChanges(storyField, stories, sktRecord);
				}

				if (cduField && valMod.cdu) _update_PCIChanges(cduField, valMod.cdu, sktRecord);
				if (yearField && valMod.ybuilt) _update_PCIChanges(yearField, valMod.ybuilt, sktRecord);
				if (gradeField && valMod.grade) _update_PCIChanges(gradeField, valMod.grade, sktRecord);
			}
		}
	});

	if (_PCIBatchArray.length > 0) updateRecordPCI();
	else if (callback) { callback(); return; }
}

function HelionRSBeforeSave(sketches, callback) {
	let newSketches = sketchApp.sketches.filter((x) => { return x.rsNewPage }), delSketches = sketchApp.rsDeletedPages ? sketchApp.rsDeletedPages: [],
		catId = getCategoryFromSourceTable('CC_APPRAISAL_DRAWING').Id, cField = getDataField('CHANGE_TYPE', 'CC_APPRAISAL_DRAWING'),
		insertData = { YEAR: ccma.CurrentYear, SOURCE: 'Account', ACCOUNT_ID: activeParcel.ACCOUNT_ID, TRANSACTION_ID: activeParcel.TRANSACTION_ID, CHANGE_TYPE: 'Insert' };

	sketchApp.sketches.filter((x) => { return x.isModified && !x.rsNewPage }).forEach((sk) => {
		let rec = activeParcel['CC_APPRAISAL_DRAWING'].filter((r) => { return r.ROWUID == sk.uid })[0];
		if (rec && rec.CC_RecordStatus != 'I' && cField) _update_PCIChanges(cField, 'Update', rec);
	});

	if (newSketches.length == 0 && delSketches.length == 0) {
		if (callback) callback(true, true);
		return;
	}

	let deletePages = function (dsketches) {
		if (dsketches.length == 0) {
			if (callback) callback(true, true);
			return;
		}

		let dsketch = dsketches.shift();
		ccma.Data.Controller.DeleteAuxRecordWithDescendants(catId, dsketch.uid, null, null, function () {
			deletePages(dsketches);
		}, false, null, true);
	}

	let createNewPage = function (nsketches) {
		if (nsketches.length == 0) {
			deletePages(delSketches);
			return;
		}

		let nsketch = nsketches.shift();
		insertNewAuxRecord(null, null, catId, nsketch.uid, insertData, () => {
			createNewPage(nsketches);
		});
	}

	createNewPage(newSketches);
}

function MSGovernAfterSave(sketchData, callback) {
	if (sketchData == null || sketchData == undefined || sketchData.length == 0) {
		if (callback) callback();
		return;
	}

	let bldgTbl = 'CCV_MA_Buildings_2_3_BLDG_INFO', areaTbl = 'MA_BLDG_AREA', deleteRecords = [], areaCat = getCategoryFromSourceTable(areaTbl), newRecords = [],
		bldgFields = ['Total_Base', 'Total_Actual', 'Total_Effective', 'Total_Heated'], bldgDataFields = {},
		areaFields = ['AREA_CODE', 'BASE_AREA', 'ACTUAL_AREA', 'EFFECTIVE_AREA', 'LIVING_AREA', 'HEATED_AREA', 'PERCENT_HEATED', 'PERIMETER'];

	_PCIBatchArray = [];
	bldgFields.forEach((f) => { bldgDataFields[f] = getDataField(f, bldgTbl); });

	var updateRecordPCI = function () {
		ccma.Sync.enqueueBulkParcelChanges(_PCIBatchArray, function () {
			getParcel(activeParcel.Id, function () {
				_PCIBatchArray = [];
				if (callback) callback(); return;
			});
		});
	}

	var createNewRecord = function (newRecds) {
		if (newRecds.length == 0) {
			updateRecordPCI();
			return;
		}

		let newRec = newRecds.shift();
		insertNewAuxRecord(null, newRec.parentRecord, newRec.catId, null, newRec.arObj, (rowid) => {
			createNewRecord(newRecds);
		});
	}

	var deleteRecord = function (delRecords) {
		if (delRecords.length == 0) {
			createNewRecord(newRecords);
			return;
		}

		let delRec = delRecords.pop(), rel = (delRecords.length == 0 && (typeof (cachedAuxOptions) != 'undefined') && !(isEmpty(cachedAuxOptions)) && cachedAuxOptions[delRec.categoryId]) ? true : false;
		ccma.Data.Controller.DeleteAuxRecordWithDescendants(delRec.categoryId, delRec.rowuid, null, null, function () {
			deleteRecord(delRecords);
		}, rel, null, true);
	}

	sketchData.forEach((sketchRecord) => {
		let sketch = sketchApp.sketches.filter((s) => { return s.sid == sketchRecord.rowid })[0],
			parentRecord = activeParcel[bldgTbl].filter((s) => { return s.ROWUID == sketchRecord.rowid && s.CC_Deleted != true && s.OVERRIDE_AREA != '1' })[0];
		if (parentRecord?.MA_BLDG_AREA) {
			parentRecord.MA_BLDG_AREA.forEach((ba) => { deleteRecords.push({ rowuid: ba.ROWUID, categoryId: areaCat.Id }); });
			let totalArea = 0;

			sketch.vectors.forEach((vect) => {
				let ar = Math.round(vect.area()), peri = Math.round(vect.perimeter()), arObj = {};
				totalArea = totalArea + vect.area();
				areaFields.forEach((f) => {
					if (f == 'AREA_CODE') { arObj[f] = vect.code ? vect.code : vect.name; }
					else if (f == 'PERIMETER') { arObj[f] = peri; }
					else { arObj[f] = ar; }
				});

				newRecords.push({ arObj: arObj, parentRecord: parentRecord, catId: areaCat.Id });
			});

			totalArea = Math.round(totalArea);
			for (f in bldgDataFields) {
				if (bldgDataFields[f]) _PCIBatchArray.push({ parcelId: activeParcel.Id, auxRowId: parentRecord.ROWUID, parentAuxRowId: parentRecord.ParentROWUID, action: 'E', field: bldgDataFields[f], oldValue: null, newValue: totalArea });
			}
		}
	});

	deleteRecord(deleteRecords);
}
