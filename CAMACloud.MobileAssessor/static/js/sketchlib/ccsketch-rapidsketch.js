﻿
CAMACloud.Sketching.Formatters.RapidSketch = {
    name: "RapidSketch",
    saveAllSegments: false,
    allowSegmentAddition: false,
    allowSegmentDeletion: false,
    DoNotAllowLabelMove: true,
    arcMode: 'ARC',
    vectorSeperator: ";",
    newVectorHeaderSuffix: '[-1,-1]:',
    nodeToString: function (n) {
        var str = "";
        if (n.arcLength > 0) {
            str += "A" + sRound(n.arcLength);
        }
        if (n.arcLength < 0) {
            str += "B" + Math.abs(sRound(n.arcLength));
        }
        if (n.dy > 0) {
            if (n.isStart) {
                if (str != "") str += " ";
            } else {
                if (str != "") str += "/";
            }
            str += n.sdy + sRound(n.dy);
        }
        if (n.dx > 0) {
            if (n.isStart) {
                if (str != "") str += " ";
            } else {
                if (str != "") str += "/";
            }
            str += n.sdx + sRound(n.dx);
        }
        if (n.hideDimensions) {
            if (str != "") str += "/";
            str += "#";
        }
        if (n.isStart) {
            str += " S";
        }
        return str;
    },
    vectorToString: function (v, fromSketchSave) {
        var str = "";
        if (v.startNode != null) {
            if (!v.labelEdited || !v.referenceIds) {
                if (v.header) {
                    var headerString = fromSketchSave? this.convertLineColour(v, v.header, true): v.header;
                    str += headerString;
                } else {
                    if (v.referenceIds) {
                        var refStringId = fromSketchSave? this.convertLineColour(v, v.referenceIds, false): v.referenceIds;
                        str += v.name + "[" + refStringId + "]:";
                    } else {
                        if (v.rowId)
                            str += v.name + "[" + v.rowId + ",-1]:";
                        else
                            str += v.name + "[-1,-1]:";
                    }

                };
            }
            else {
                var refStringId = fromSketchSave? this.convertLineColour(v, v.referenceIds, false): v.referenceIds;
                str += v.name + "[" + refStringId + "]:";
            }
            str += v.startNode.vectorString();
            var nn = v.startNode.nextNode;
            while (nn != null) {
                str += " ";
                str += nn.vectorString();
                nn = nn.nextNode;
            }
        }
        return str;
    },
    vectorFromString: function (editor, s) {
        if ((s || '').trim() == '') {
            var nv = new Vector(editor);
            nv.isClosed = false;
            nv.label = "Blank"
            nv.uid = -1;
            return nv;
        }

        var o = editor.origin;
        var p = o.copy();

        var v = new Vector(editor);
        this.updateVectorFromString(editor, v, s);
        v.isModified = false;
        return v;
    },
    updateVectorFromString: function (editor, v, s) {
        v.startNode = null;
        v.endNode = null;


        var o = editor.origin;
        var p = o.copy();

        var hi = s.split(':');
        if(hi.length > 2) {
        	hi = s.split(']:');
        	hi[0] = hi[0] + ']';
        }
        var head = hi[0];
        var lineString = hi[1] || '';

        v.header = head + ":";

        var isStart = false;
        var hasStarted = false;
        var isVeer = false;
        var veerSteps = 0;

        var nodeStrings = lineString.split(' ');
        for (var i in nodeStrings) {
            var arcLength = 0;
            var hideDimensions = false;
            var ns = nodeStrings[i];
            if (ns.trim() != '') {
                var cmds = ns.split('/');
                for (var c in cmds) {
                    var cmd = cmds[c];
                    if (cmd == '') continue;
                    if (cmd == 'S') {
                        isStart = true;
                    } else {
                        var cp = cmd.match(/[ABUDLR#]|[0-9]+(.[0-9]+)?/g);
                        var dir = cp[0];
                        distance = cp[1] || 0;
                        var pix = CAMACloud.Sketching.Utilities.toPixel(distance);
                        switch (dir) {
                            case "A": arcLength = Number(distance); break;
                            case "B": arcLength = -Number(distance); break;
                            case "U": p.moveBy(0, pix); break;
                            case "D": p.moveBy(0, -pix); break;
                            case "L": p.moveBy(-pix, 0); break;
                            case "R": p.moveBy(pix, 0); break;
                            case "#": hideDimensions = true; break;
                        }
                    }
                }

                p = p.copy();

                if (isStart) {
                    v.start(p.copy());
                    hasStarted = true;
                    isStart = false;
                } else if (hasStarted) {
                    var q = p.copy().alignToGrid(editor);
                    if (!v.startNode.overlaps(q))
                        v.connect(p.copy());
                    else
                        v.terminateNode(v.startNode);
                }

                if (v.endNode) {
                    v.endNode.hideDimensions = hideDimensions;
                    v.endNode.arcLength = arcLength;
                    if (arcLength != 0)
                        v.endNode.isArc = true;
                }

            }
        }

        if (v.startNode)
            if (v.startNode.overlaps(v.endNode.p.copy()))
                v.isClosed = true;
        return v;
    },
    open: function (editor, data) {
        editor.vectors = [];
        editor.sketches = [];
        let isError = false;

        for (var x in data) {
            var sketch = new Sketch(editor);
            var s = data[x];
            sketch.parentRow = s.parentRow;
            sketch.uid = s.uid;
            sketch.label = s.label;
            sketch.vectors = [];
            sketch.config = s.config || {};
            sketch.sid = s.sid;
            sketch.lookUp = s.lookUp;
            sketch.maxNotelen = s.maxNotelen;
            var counter = 0;
            try {
                for (var i in s.sketches) {
                    var sk = s.sketches[i];
                    var segments = (sk.vector || '').split(';');
                    var scount = 0;
                    if (segments.length < 2) scount = -1;
                    for (var si in segments) {
                        counter += 1;
                        scount += 1;
                        var sv = segments[si];
                        var v = this.vectorFromString(editor, sv);
                        v.sketch = sketch;
                        v.uid = sk.uid + (scount ? '/' + scount : '');
                        v.index = counter;
                        v.name = sk.label[0].Value;
                        if (sk.label[0] && sk.label[0].colorCode) {
                            var clrcode = sk.label[0].colorCode.split('~');
                            v.colorCode = clrcode[0] ? clrcode[0] : null;
                            v.newLineColor = (clrcode[1] && sketchSettings['SketchLineColorCustomizations'] && sketchSettings['SketchLineColorCustomizations'].contains('MA')) ? clrcode[1] : null;
                            v.newLabelColorCode = (clrcode[2] && sketchSettings['SketchTextColorCustomizations'] && sketchSettings['SketchTextColorCustomizations'].contains('MA')) ? clrcode[2] : null;
                        }
                        v.label = '[' + counter + '] ' + sk.label.map(function (a) { return ((a.Description && sketchSettings["DoNotShowLabelDescriptionSketch"] != '1' && clientSettings["DoNotShowLabelDescriptionSketch"] != '1' && a.Description.Name) ? ((sketchSettings["DoNotShowLabelCode"] == '1' || a.showLabelDescriptionOnly) ? a.Description.Name : a.Value + '-' + a.Description.Name) : a.Value) }).filter(function (a) { return a }).join('/');
                        v.labelFields = sk.label;
                        v.vectorString = sv;
                        v.referenceIds = sk.referenceIds;
                        v.vectorConfig = sk.vectorConfig;
                        v.rowId = sk.rowId;
                        v.isChanged = sk.isChanged;
                        editor.vectors.pop();
                        sketch.vectors.push(v);
                    }
                }
            }
            catch (e) {
                editor.vectors = []; sketch.vectors = []; isError = true;
            }
            sketch.isModified = false;
            editor.loadNotesForSketch(sketch, s.notes, null, ["noteData"]);
            editor.sketches.push(sketch);
        }
        if (isError) {
            editor.sketches.forEach((sk) => { sk.vectors = []; });
            throw "Sketch cannot be rendered due to the wrong sketch data.";
        }
    },
    getParts: function (vectorString, doNotDecodeEncode, dmData, customParameters) {
        var p = [], nc = 0, noteList = [];
        vectorString = doNotDecodeEncode? (vectorString || ''): Base64.decode((vectorString || '').substr(13));
        var parts = (vectorString || '').split(";");
        for (var i in parts) {
            var part = parts[i];
            if (/(.*?):(.*?)/.test(part)) {
                var hi = part.split(':');
                if(hi.length > 2) {
                	hi = part.split(']:');
                	hi[0] = hi[0] + ']';
                }
                var head = hi[0];
                var info = hi[1];
                var labels, refString;

                if (customParameters?.isHelionRS && head.split('[')[0].trim() == 'Text') {
                    let nd = null;
                    try {
                        nd = JSON.parse(Base64.decode(/(.*?)\[(.*?)\]/.exec(head)[2]));
                    }
                    catch (ex) { }
                        
                    if (nd && info) {
                        let sl = info.split(' '), x = 0, y = 0;
                        for (s in sl) {
                            if (sl[s]?.trim() != '') {
                                let cp = sl[s].trim().match(/[UDLR]|[0-9]+(.[0-9]+)?/g), dir = cp[0], dis = cp[1] || 0;
                                switch (dir) {
                                    case "U": y = parseFloat(dis); break;
                                    case "D": y = parseFloat(-dis); break;
                                    case "L": x = parseFloat(-dis); break;
                                    case "R": x = parseFloat(dis); break;
                                }
                            }
                        }

                        noteList.push({
                            isHelionNote: true,
                            note: { uid: nc, text: nd.Text, x: x, y: y, noteData: { Font: nd.Font, FontColor: nd.FontColor, Rotation: nd.Rotation, encodedText: part } }
                        });
                        nc = nc + 1;
                    }
                    continue; 
                }

				var count_of_sq_br = head.match(/\[/gi).length;
                var r1 = /(.*?)\[(.*?)\]/, hparts = [];
                if ( count_of_sq_br > 1) {
                	head = head? head: "";
                	var lIndex = head.lastIndexOf('[');
                	hparts = ["", head.substring(0, lIndex), head.substring((lIndex + 1), (head.length - 1))]
                }
                else
                	hparts = r1.exec(head);
                if (hparts == null) {
                    labels = "";
                    refString = "-1,-1";
                } else {
                    labels = hparts[1].replace(/\{(.*?)\}/g, '');
                    refString = hparts[2];
                }

                p.push({
                    label: labels,
                    vector: part,
                    referenceIds: refString
                })

            }
        }
        if (customParameters?.isHelionRS) p = p.concat(noteList);
        return p;
    },
    encodeSketch: function (data) {
        var ison = (new Date()).getTime().toString();
        var salt2 = ""
        for (var i = 0; i < ison.length; i++) { salt2 = ison[i] + salt2; };
        var salt = Base64.encode(salt2).substring(0, 13);
        return salt + Base64.encode(data);
    },
    decodeSketch: function (data) {
        return Base64.decode((data || '').substr(13));
    },
    convertLineColour: function ( vector, data, isHeader )
    {
    	if ( vector.isModified ) {
    		try {
	    		var head_or_ref = data, osbl_Index = -1, split_head_or_ref;
	    		
	    		if (isHeader) {
	    			osbl_Index = head_or_ref.lastIndexOf('[');
	    			if (osbl_Index > -1)
	            		head_or_ref = head_or_ref.substring( (osbl_Index + 1), ( head_or_ref.lastIndexOf(']') ) );
	            	else
	            		head_or_ref = "";
	            }
	            split_head_or_ref = head_or_ref.split(',');
	            
	            if ( split_head_or_ref.length > 6 ) {
	            	split_head_or_ref[6] = "";
	            	split_head_or_ref = split_head_or_ref.toString();
	            	data = ( isHeader ? ( osbl_Index > -1 ? ( data.substring( 0, (osbl_Index + 1) ) + split_head_or_ref + ']:' ): vector.name + '[-1,-1]:' ): split_head_or_ref );
	            }
            }
            catch(err) {
            	return data;
        	}
    	}
    	return data;
    }
}