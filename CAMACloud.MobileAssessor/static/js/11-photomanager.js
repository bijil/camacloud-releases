﻿//    DATA CLOUD SOLUTIONS, LLC ("COMPANY") CONFIDENTIAL 
//    Unpublished Copyright (c) 2010-2013 
//    DATA CLOUD SOLUTIONS, an Ohio Limited Liability Company, All Rights Reserved.
//    NOTICE: All information contained herein is, and remains the property of COMPANY.
//    The intellectual and technical concepts contained herein are proprietary to COMPANY
//    and may be covered by U.S. and Foreign Patents, patents in process, and are protected
//    by trade secret or copyright law. Dissemination of this information or reproduction
//    of this material is strictly forbidden unless prior written permission is obtained
//    from COMPANY. Access to the source code contained herein is hereby forbidden to
//    anyone except current COMPANY employees, managers or contractors who have executed
//    Confidentiality and Non-disclosure agreements explicitly covering such access.</p>
//    The copyright notice above does not evidence any actual or intended publication
//    or disclosure of this source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
//    DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
//    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
//    IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
//    OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
//    TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
//    ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 
var PhotoPropElement, reloadFlag = false, PropFieldFocus = false;
function UpdateActiveParcelFirstPhoto(callback) {
    if (!activeParcel) return false;
    //getData("SELECT * FROM Images WHERE Type = '1' AND ParcelId = ? LIMIT " + ccma.UI.Camera.MaxPhotosPerParcel, [activeParcel.Id.toString()], function (images) {
    //IDB_getData('Images',['ParcelId'],['='],[parseInt(activeParcel.Id)],[], function (images) {
    IDB_getData('Images', ['ParcelId', 'Type'], ['=', '='], [parseInt(activeParcel.Id), 1], ['AND'], function (images) {
		images = images.slice(0,ccma.UI.Camera.MaxPhotosPerParcel);
        if (images.length > 0) {
            var prcimage = (images.filter(function (x) { return checkIsFalse(x.IsPrimary); }).length > 0) ? images.filter(function (x) { return checkIsFalse(x.IsPrimary); })[0].Image : images[0].Image;
            activeParcel.FirstPhoto = prcimage;
            //   activeParcelData.FirstPhoto = prcimage;
            if (callback)
                callback();
        } else {
           getDataUrl('/static/images/residential.png',function(res){
								 activeParcel.FirstPhoto = res;
							}); 
		   if (callback)
                callback();
        }
    });
}
function downloadPhoto(pid, element) {
    if ($(element).hasClass("loading")) { return false };    
	$(element).html('<img src="/static/css/images/comploader-old.gif" />');
	$(element).addClass('loading');
	pid = pid.toString();
	var params = {};
	params["pid"] = pid;
	$$$("DownloadPhoto", params, function (data) {
		if (data.status) {
			if (data.status == "OK") {
				console.log(data, pid);
				todoDB.indexedDB.update('Images', [{name: 'Image', value: data.value}], ['Id'], ['='], [parseInt(pid)], [], function() {
					showPhotos();
					$(element).remove();
				});
				/*db.transaction(function (dx) {
					dx.executeSql("UPDATE Images SET Image = ? where  Id =?", [data.value, pid], function (tx, res) {
						showPhotos();
						$(element).remove();
					});
				});*/
			}
		}
	});
}
function deleteimages(imagearray, o, linkFlag, back) {
    var img = o.images[imagearray[imagearray.length - 1]];
    o.adddelete = true;
    var callback = imagearray.length > 1 ? function () { imagearray.pop(); deleteimages(imagearray, o, linkFlag, back) } : back;

    if (img.Accepted == 0) {
        pstream.removePhoto(img.LocalId, callback);
    } else if (img.Synced == 0) {
    	todoDB.indexedDB.deleteRecord('UnsyncedImages',['LocalId'],['='],[img.LocalId.toString()],[], function() {
	    	todoDB.indexedDB.deleteRecord('Images',['LocalId'],['='],[img.LocalId.toString()],[], function() {
	        //getData('DELETE FROM Images WHERE LocalId = ?', [img.LocalId.toString()], function () {
	            getData('DELETE FROM ParcelChanges WHERE ((Field = "PhotoMetaData" OR Field = "MarkAsPrimaryPhoto") AND (ParcelId IN (?) OR NewValue IN(?) ))', [img.LocalId.toString(), img.LocalId.toString()], function () {
	                ccma.Sync.enqueueEvent(img.ParcelId, 'DeleteParcelPhoto', img.LocalId, {}, function () {
	                    if (callback) callback();
	                });
	            })
	        });
        });
    } else {
 		//getData('DELETE FROM Images WHERE Id = ?', [img.Id.toString()], function () {
		todoDB.indexedDB.deleteRecord('Images',['Id'],['='],[parseInt(img.Id.toString())],[], function() {
            getData('DELETE FROM ParcelChanges WHERE ((Field = "PhotoMetaData" OR Field = "MarkAsPrimaryPhoto") AND (ParcelId IN(?,?) OR NewValue IN(?,?) ))', [img.Id.toString(), img.LocalId.toString(), img.Id.toString(), img.LocalId.toString()], function () {
                ccma.Sync.enqueueEvent(img.ParcelId, 'DeleteParcelPhoto', img.Id, {}, function () {
                    if(linkFlag == true) generateLinkedPropertiesPCI(img);
                    if (callback) callback();
                });
            });
        });

    }
}

function deleteImagesIfLimitExceeded(img, callback) {
    if (img.Accepted == 0) {
        pstream.removePhoto(img.LocalId, callback);
    } else if (img.Synced == 0) {
    	todoDB.indexedDB.deleteRecord('UnsyncedImages',['LocalId'],['='],[img.LocalId.toString()],[], function() {
	    	todoDB.indexedDB.deleteRecord('Images',['LocalId'],['='],[img.LocalId.toString()],[], function() {
	        //getData('DELETE FROM Images WHERE LocalId = ?', [img.LocalId.toString()], function () {
	            getData('DELETE FROM ParcelChanges WHERE ((Field = "PhotoMetaData" OR Field = "MarkAsPrimaryPhoto") AND (ParcelId IN (?) OR NewValue IN(?) ))', [img.LocalId.toString(), img.LocalId.toString()], function () {
	                ccma.Sync.enqueueEvent(img.ParcelId, 'DeleteParcelPhoto', img.LocalId, {}, function () {
	                    if (callback) callback();
	                });
	            })
	        });
        });
    } else {
    	todoDB.indexedDB.deleteRecord('Images',['Id'],['='],[parseInt(img.Id.toString())],[], function() {
        //getData('DELETE FROM Images WHERE Id = ?', [img.Id.toString()], function () {
            getData('DELETE FROM ParcelChanges WHERE ((Field = "PhotoMetaData" OR Field = "MarkAsPrimaryPhoto") AND (ParcelId IN(?,?) OR NewValue IN(?,?) ))', [img.Id.toString(), img.LocalId.toString(), img.Id.toString(), img.LocalId.toString()], function () {
                ccma.Sync.enqueueEvent(img.ParcelId, 'DeleteParcelPhoto', img.Id, {}, function () {
                    if (callback) callback();
                });
            });
        });

    }
}
var photoMetaDataValues = {};
function sketchImageManager( container, box ){
    var o = this;
    this.container = container;
    this.box = box;
    if ( iPad )
        $( 'span.adj' ).css( 'padding-top', '7px' )
    this.openSketch = function ( id, lid ){
        $( '.overlay-photo-backlay' ).show();
        $( this.container ).show();
        $( this.box ).show();
        var img = activeParcel.sketchImages.filter( function ( a ) { return ((id && a.Id == id) || (a.LocalId != 0 && a.LocalId == lid)) } );
        if ( img[0] ) img = img[0];
        
        sketchMarkup.pushImage( img.Image, true )
    }
    this.close = function () {
        $( '.overlay-photo-backlay' ).hide();
        $( '.markuptext' ).hide();
        $( this.container ).hide();
        $( this.box ).hide();
    }
    this.save = function ()
    {
        if ( sketchMarkup.storedElement.length == 0 ){
            messageBox( 'No changes found ' );
            return;
        }
        else if (activeParcel.sketchImages.length >= ccma.UI.Camera.MaxPhotosPerParcel) {
        	messageBox('This property has exceeded the limit of sketch images.');
            return;
        }
        var exportOptions = {
            type: "image/jpeg",
            quality: 0.75	
        }
        sketchMarkup.exportImage( exportOptions, function (img) {
            var t = ticks();
            var  lid = "pu-" + tick().toString()
            var imageData = [{ ParcelId: parseInt( activeParcel.Id ), Type: 2, LocalId: lid, Accepted: 1, Synced: 0, UploadTime: t, Image: img }];
            var unSyncedImageData = [{ ParcelId: parseInt( activeParcel.Id ), Image: img, UploadTime: t, Synced: 0, LocalId: lid, Type: 2 }];
            todoDB.indexedDB.Insert( "UnsyncedImages", unSyncedImageData, function ( data, dx ){
                todoDB.indexedDB.Insert( "Images", dx[0], function ( data, dp ) {
                  //  uploadQueue.splice( dp.di, 1 );
                    executeQueries("UPDATE Parcel SET Reviewed = 'false' WHERE Id = " + parseInt(activeParcel.Id))
                    let qry = EnableNewPriorities == 'True' ? ("UPDATE Parcel SET CC_Priority = 3 WHERE CC_Priority = 0 AND Id = " + parseInt(activeParcel.Id)) : ("UPDATE Parcel SET CC_Priority = 1 WHERE CC_Priority = 0 AND Id = " + parseInt(activeParcel.Id));
                    executeQueries(qry);
                      showSketchImages()
                    o.close();
                }, {
                    pid: imageData[0].ParcelId,
                    di: dx[1]
                } );
            }, [imageData, 0] );
        } );
    }
}
function PhotoManager(container, box) {
    var o = this;
    this.container = container;
    this.box = box;
    this.adddelete = false;
    this.metadataedit = false;
    this.active = false;
    this.pid = null;
    this.keyvalue = null;
    this.altKeyValue = null;
    this.index;
    this.images = [];

    this.openParcel = function (pid, keyvalue1, options, recordPhoto,altKeyValue) {
    	$('section.current .scrollable').addClass('tempScrollable');
    	$('section.current .scrollable').removeClass('scrollable');
        $('.overlay-photo-backlay').show();
        $(this.container).show();
        $('.pm-left').unbind(touchClickEvent);
        $('.pm-left').bind(touchClickEvent, function (e) {
            e.preventDefault();
            photos.showPrevious();
        });
        $('.pm-right').unbind(touchClickEvent);
        $('.pm-right').bind(touchClickEvent, function (e) {
            e.preventDefault();
            photos.showNext();
        });
        $('.pm-add').unbind(touchClickEvent);
        $('.pm-add').bind(touchClickEvent, function (e) {
            photos.addPhoto();
        });
        $('.pm-accept').unbind(touchClickEvent);
        $('.pm-accept').bind(touchClickEvent, function (e) {
            e.preventDefault();
            photos.acceptPhoto();
        });
        $('.pm-trash').unbind(touchClickEvent);
        $('.pm-trash').bind(touchClickEvent, function (e) {
            e.preventDefault();
            photos.deletePhoto();
        });
        $('.pm-prmflag').unbind(touchClickEvent);
        $('.pm-prmflag').bind(touchClickEvent, function (e) {
            e.preventDefault();
            photos.markAsPrimaryImage();
        });
        $('.pm-hide').unbind(touchClickEvent);
        $('.pm-hide').bind(touchClickEvent, function (e) {
	    	$('.tempScrollable').addClass('scrollable');
            $('.tempScrollable').removeClass('tempScrollable');
            e.preventDefault();
            if (ccma.UI.ActiveScreenId == "sort-screen" && activeParcel)
            	activeParcel = null;
            photos.hide();
        });
        $('.pm-props').unbind(touchClickEvent);
        $('.pm-props').bind(touchClickEvent, function (e) {
            e.preventDefault();
            photos.showProperties();
            photos.showPhotoPreview();
        });
        $('.pm-hidepro').unbind(touchClickEvent);
        $('.pm-hidepro').bind(touchClickEvent, function (e) {
            $(inputElementInFocus).blur();
            hideKeyboard();
            e.preventDefault(); 
            photos.hideProperties();
        	$('section.current .scrollable').addClass('tempScrollable');
    		$('section.current .scrollable').removeClass('scrollable');
        });
        $('.pm-leftpro').unbind(touchClickEvent);
        $('.pm-leftpro').bind(touchClickEvent, function (e) {
            e.preventDefault();
            photos.showPreviousPro();
        });
        $('.pm-rightpro').unbind(touchClickEvent);
        $('.pm-rightpro').bind(touchClickEvent, function (e) {
            e.preventDefault();
            photos.showNextPro();
        });
        log("Opening Photo Manager for " + keyvalue1);
        o.index = -1;
        o.images = [];
        o.pid = pid;
        o.keyvalue = keyvalue1;
        o.altKeyValue = altKeyValue;
        o.loadImages(function () {
            if (options) {
                if (options.id) {
                    for (x in o.images) {
                        if (o.images[x].Id == options.id) {
                            o.showImage(x);
                        }
                    }
                } else if (options.localId) {
                    for (x in o.images) {
                        if (o.images[x].LocalId == options.localId) {
                            o.showImage(x);
                        }
                    }
                } else if (options.index) {
                    o.showImage(index);
                } else if (options.showPrimary) {
                    var primary = o.images.filter(function (c) { return c.IsPrimary == 1 || c.IsPrimary == true || c.IsPrimary == 'true' });
                    if (primary.length > 0) o.showImage(o.images.indexOf(primary[0]));
                }
            }

        }, null, recordPhoto);
        if (o.length >= ccma.UI.Camera.MaxPhotosPerParcel || !iPad)
            $('#camera').hide();
        else $('#camera').show();
        this.active = true;
    }
    this.loadImages = function (callback, doNotResetIndex, recordPhoto, recPhotoCount) {
    	if(!o.pid) o.pid = activeParcel ? activeParcel.Id : null;
        //IDB_getData('Images',['ParcelId'],['='],[parseInt(o.pid)],[], function (im) {
        IDB_getData('Images', ['ParcelId', 'Type'], ['=', '='], [parseInt(o.pid), 1], ['AND'], function (im) {
			im = im.slice(0,ccma.UI.Camera.MaxPhotosPerParcel);
			var totalPhotoAccepted = im.length;
		//getData("SELECT * FROM Images WHERE Type = '1' AND ParcelId = ? LIMIT " + ccma.UI.Camera.MaxPhotosPerParcel, [parseInt(o.pid).toString()], function (im) {
			o.allimages = im;
			if (appState.sketchDccPhoto && appState.sketchDccPhoto.stable && appState.sketchDccPhoto.sourceRowuid) {
	            let sourceTable = appState.sketchDccPhoto.stable;
	    		let cat = getCategoryFromSourceTable(sourceTable);
	    		let rowuId = appState.sketchDccPhoto.sourceRowuid;
				let data = activeParcel[sourceTable]? activeParcel[sourceTable].filter(function(x) { return (x.ROWUID == rowuId) })[0]: null;
				if(data && cat && cat.PhotoLinkTableKey && cat.PhotoTag) {
					var rowId = data.ROWUID
					var metaData = clientSettings["PhotoTableLinkMetaField"];
					var regexp = new RegExp(+rowId+'$');
					im = im.filter(function(image) { if(image[metaData]) { return image[metaData].search(regexp) > -1 } });
				}
			}
			else if(recordPhoto) {
				var catId = appState.photoCatId;
				if(catId && catId > 0){
					var auxForm = $('section[catid="'+catId+'"]');
		            var findex = $(auxForm).attr('findex');
		            var sourceTable = $(auxForm).attr('auxdata');
		    		var cat = getCategory(catId);
					var data = activeParcel[sourceTable][findex];
					if(data != undefined){
						var rowId = data.ROWUID
						var metaData = clientSettings["PhotoTableLinkMetaField"];
						var regexp = new RegExp(+rowId+'$');
						im = im.filter(function(image){if(image[metaData]) {return image[metaData].search(regexp) > -1}});
					}
				}
			}
			
            im = copyDataRows(im);
            var cpics = pstream.getPhotos(o.pid);
            for (i in cpics) {
                var pu = cpics[i];
                if (!im.some(function (d) { return d.LocalId == pu.uid })) {
                    if (pu.dataUrl) {
                        var img = new Object();
                        img.Id = null;
                        img.Image = pu.dataUrl;
                        img.ParcelId = o.pid;
                        img.LocalId = pu.uid;
                        img.Path = null;
                        img.Type = "1";
                        img.Accepted = pu.accepted ? 1 : 0;
                        img.IsPrimary = checkIsFalse(pu.IsPrimary) ? 1 : 0;
                        img.Synced = "0";
                        im.push(img);
                        o['allimages'].push(img);
                    }
                }
            }
            o.images = im;
            this.adddelete = false;
            if (o.images.length > 0 && (photos.images.filter(function (d) { return checkIsFalse(d.Accepted); }).length > 0)) {
                if (!o.images.some(function (s) { return checkIsFalse(s.IsPrimary); }) && appState.photoCatId == 0 && !appState.sketchDccPhoto) {
                    o.markAsPrimaryImage(0);
                }
            }
            if (!doNotResetIndex)
                o.index = 0;
            appState.totalPhotoCount = totalPhotoAccepted + cpics.length
            appState.recordPhotoCount = im.length;
            if(!recPhotoCount){	
	            o.showImage(o.index);
	            if (callback) callback();
            }
            else
            	if(callback) callback();
        });
    }

    this.refreshImageSpanCount = function () {
        if (clientSettings?.PhotoTagMetaField && clientSettings?.PhotoTagMetaField != '' && clientSettings?.PhotoTableLinkMetaField && clientSettings?.PhotoTableLinkMetaField != '' && o.metadataedit) {
            let catId = appState.photoCatId;

            if (catId && catId > 0) {
                photos.loadImages(() => {
                    let auxForm = $('section[catid="' + catId + '"]'), findex = $(auxForm).attr('findex'), sourceTable = $(auxForm).attr('auxdata'), cat = getCategory(catId), data = activeParcel[sourceTable][findex];

                    if (data != undefined) {
                        let rowId = data.ROWUID, metaData = clientSettings["PhotoTableLinkMetaField"], regexp = new RegExp(+rowId + '$'), im = o.images.filter((x) => { if (x[metaData]) { return x[metaData].search(regexp) > -1 } });
                        $('span.photocount').html(im.length ? im.length : 0);
                        $('section[catid="' + appState.photoCatId + '"] .aux-grid-view .data-collection-table tbody tr.multigridSelected td:last-child span').html(im.length ? im.length : 0);
                    }

                    let ParentSpec = cachedAuxOptions[catId] == undefined ? null : cachedAuxOptions[catId].parentSpec, filteredIndexes = null, sourceData = activeParcel[cat.SourceTable];

                    if (cachedAuxOptions[catId] && cachedAuxOptions[catId].filters) {
                        filteredIndexes = getFilteredIndexes(sourceData, cachedAuxOptions[catId].filters, cachedAuxOptions[catId].filterValues, ParentSpec);
                    }

                    if (!filteredIndexes) {
                        filteredIndexes = [];
                        filteredIndexes = sourceData.map(function (f, n) { return n.toString() })
                    }

                    $('.aux-grid-view .data-collection-table tbody tr', auxForm).each((i, el) => {
                        let ix = filteredIndexes[i], rec = sourceData[ix], metaData = clientSettings["PhotoTableLinkMetaField"];
                        if (rec) {
                            let regexp = new RegExp(+rec.ROWUID + '$');
                            let im = photos['allimages'].filter(function (image) { if (image[metaData]) { return image[metaData].search(regexp) > -1 } });
                            $('.record-grid-photo-count', el).html((im ? im.length : 0));
                        }
                    });
                }, null, true, true);
            }
        }
    }

    this.hide = function () {
        var callback = function () {
        	$('span.photocount').html( o.images.length ? o.images.length : 0);
        	if(appState.photoCatId >=0)
                $('section[catid="' + appState.photoCatId + '"] .aux-grid-view .data-collection-table tbody tr.multigridSelected td:last-child span').html(o.images.length ? o.images.length : 0);
            o.refreshImageSpanCount();
            o.metadataedit = false;
            $( '.mask' ).hide();
            $('.maskText').remove();
            o.closePM();
           // if(activeParcel== null)
            	//showSortScreen();
        }
        if (o.images.some(function (d) { return !checkIsFalse(d.Accepted); })) {

            messageBox('You have unaccepted photos in this list. Please select OK if they are all suitable, or CANCEL to accept/delete each photo individually.', ["OK", "CANCEL "], function () {
                $( '.mask' ).show().append( '<span class="maskText"></span>' )
                o.bulkAcceptPhoto( function ()
                {
                    console.log('return from bulk')
                    o.removeStreamedPhotos(pstream.getPhotos(photos.pid), function () {
                        if ((!o.images.some(function (s) { return checkIsFalse(s.IsPrimary); }) && o.images.length > 0) && !(appState.photoCatId > 0) && !appState.sketchDccPhoto) {
                        //getData("SELECT * FROM Images WHERE Type = '1' AND ParcelId = ? ORDER BY UploadTime LIMIT " + ccma.UI.Camera.MaxPhotosPerParcel, [o.pid.toString()], function (im) {
                        	//IDB_getData('Images',['ParcelId'],['='],[parseInt(o.pid)],[], function (im) {
                        	IDB_getData('Images', ['ParcelId', 'Type'], ['=', '='], [parseInt(o.pid), 1], ['AND'], function (im) {
	                        	im = _.sortBy(im, 'UploadTime').slice(0, ccma.UI.Camera.MaxPhotosPerParcel);
	                        	var index = 0;
	                            for (var i=0;i < o.images.length;i++) {
		                            if (o.images[i].LocalId == im[0].LocalId)
		                            index = i;
	                            }
	                            o.markAsPrimaryImage(index, function () {
	                                callback();
	                            });                            
            				});
                        }
                        else {
                            refreshImagesInSortScreen("my-parcels", o.pid, function(){ callback(); } );
                        }

                        $('.cc-drop-pop.cc-drop-mini').css({ 'z-index': '' });
                        $('.overlay-photo-viewer').css('z-index', '');
                        $('.overlay-photo-properties').css('z-index', '');
                        $('.window-message-box').css('z-index', '');
                    });
                });
            });
        }
        else
        {

            $('.cc-drop-pop.cc-drop-mini').css({ 'z-index': '' });
            $('.overlay-photo-viewer').css('z-index', '');
            $('.overlay-photo-properties').css('z-index', '');
            $('.window-message-box').css('z-index', '');
            if (o.adddelete == true)
                refreshImagesInSortScreen( "my-parcels", o.pid, function () { o.adddelete = false; callback(); } );
            else
                callback();
        }
    }
    this.removeStreamedPhotos = function (stream, callback) {
        if (stream.length > 0) {
            var cur = stream.shift();
            console.log(stream.length + ' photo in strean removed');
            pstream.removePhoto(cur.uid, function () { photos.removeStreamedPhotos(stream, function () { if (callback) callback(); }); });
        }
        else {
            if (callback) callback();
        }
    }
    this.closePM = function () {
        if (o.adddelete == true && ccma.UI.ActiveScreenId == "photos") {
            UpdateActiveParcelFirstPhoto();
            o.adddelete = false;
        }
        $('.overlay-photo-backlay').hide();
        $(this.container).hide();
        this.active = false;
        if (ccma.UI.ActiveScreenId == "photos")
            o.fillCatalogue(o.pid, '#photo-catalogue');   
    }
    this.showImage = function (sindex) {      
        $(o.box).css('background-image', "url('/static/css/images/loading-photo.gif')");
        $('.pm-accept').hide();
        var len = o.images.length;
        var index = parseInt(sindex) > (len - 1) ?  len - 1 : parseInt(sindex); 
        if (len == 0) {
            o.index = -1;
            $(o.box).css('background-image', "none");
            $('.photo-index').html('Click + button to add new photo');
            $('.pm-prmflag').hide();
            $('.pm-trash').hide();
            $('.delete-selected-photo').hide();
            return;
        }
        if (len > 0) {
        	if(appState.photoCatId > 0 || appState.sketchDccPhoto)
        		$('.pm-prmflag').show();
            $('.delete-selected-photo').show();
            $('.pm-trash').show();
            o.index = index;
            $('.photo-index').html('Photo ' + (index + 1) + '/' + len);
            var img = o.images[index] ? o.images[index] : o.images[len - 1]
			//messageBox(img.Image)
            $(o.box).css('background-image', "url('" + img.Image + "')");
            checkIsFalse((img.IsPrimary)) ? $('.pm-prmflag').attr('disable', '') : $('.pm-prmflag').removeAttr('disable');
            $('.pm-prmflag[disable]').css('background-position: -381px 48px;')
            if (img.Accepted == 0) {
                $('.pm-accept').show();
                $('.pm-prmflag').show();
            }

        }
    }
    this.showNext = function () {
        var len = o.images.length;
        o.index++;
        if (o.index >= len) {
            o.index = len - 1;
            messageBox('There are no more images to show for this parcel.');
        } else {
            o.showImage(o.index);
        }
    }

    this.showPrevious = function () {
        var len = o.images.length;
        o.index--;
        if (o.index < 0) {
            o.index = 0;
            messageBox('There are no more images to show for this parcel.');
        } else {
            o.showImage(o.index); pstream.openCamera

        }
    }
    
    this.showNextPro = function () {
        var len = o.images.length;
        o.index++;
        if (o.index >= len) {
            o.index = len - 1;
            messageBox('There are no more image properties to show for this parcel.');
        } else {
            o.showProperties(o.index);
            o.showPhotoPreview(o.index);
        }
    }

    this.showPreviousPro = function () {
        var len = o.images.length;
        o.index--;
        if (o.index < 0) {
            o.index = 0;
            messageBox('There are no more image properties to show for this parcel.');
        } else {
            o.showProperties(o.index); 
            o.showPhotoPreview(o.index);

        }
    }

    this.showLast = function () {
        var len = o.images.length;
        o.index = len - 1;
        o.showImage(o.index);
    }

    this.addPhoto = function () {
        photoMetaDataValues = {};
        catId = appState.photoCatId;
    	if((catId && catId > 0) || appState.sketchDccPhoto) {
    		let data  = null, sourceTable = null, cat = null;
    		if (catId && catId > 0) {
	    		var auxForm = $('section[catid="'+catId+'"]');
	            var findex = $(auxForm).attr('findex');
	    		cat = getCategory(catId);
	    		sourceTable = $(auxForm).attr('auxdata');
				data = findex >= 0 && sourceTable ? activeParcel[sourceTable][findex] :null;
			}
			else {
				let rowuId = appState.sketchDccPhoto.sourceRowuid;
				sourceTable = appState.sketchDccPhoto.stable;
	    		cat = getCategoryFromSourceTable(sourceTable);
				data = activeParcel[sourceTable]? activeParcel[sourceTable].filter(function(x) { return (x.ROWUID == rowuId) })[0]: null;
			}
			if (data != undefined) {
				if(cat.PhotoTag && cat.PhotoLinkTableKey){
					var PhotoKey = data[cat.PhotoLinkTableKey];	
					if(PhotoKey == null)
						PhotoKey = "";				
					PhotoKey = PhotoKey+'$'+data["ROWUID"];	
					photoMetaDataValues[clientSettings["PhotoTagMetaField"]] = cat.PhotoTag;
					photoMetaDataValues[clientSettings["PhotoTableLinkMetaField"]] = PhotoKey;
				}
			}					
		}
        var images = o.images.filter(function (i) { return i.IsPrimary == null || i.IsPrimary == 0 });
        var oldestImage = images[0];
        for (var i = 0; i < images.length; i++) {
            if (images[i].UploadTime < images[0].UploadTime)
                oldestImage = images[i];
        }
        var msg = 'This property has exceeded the limit of photos.The oldest photo will be deleted.Do you want to continue ?';
        if(appState.photoCatId > 0 || appState.sketchDccPhoto) {
        	if(appState.totalPhotoCount >= parseInt(clientSettings.MaxPhotosPerParcel)){
        		if(images.length == 0){
        			messageBox('This property has exceeded the limit of photos.');
        			return false
        		}
        		msg = 'This property has exceeded the limit of photos.The oldest photo will be deleted.Do you want to continue ?';
        	}
        }
        var photolabel = ShowAlternateField == "True" ? (ShowAlternateField == "True" && ShowKeyValue1 == "True" && o.altKeyValue) ? o.keyvalue +' | '+ o.altKeyValue : o.altKeyValue : o.keyvalue
        if (o.images.length >= parseInt(clientSettings.MaxPhotosPerParcel) || appState.totalPhotoCount >= parseInt(clientSettings.MaxPhotosPerParcel)) {
            messageBox(msg, ["Yes", "No"], function () {
                deleteImagesIfLimitExceeded(oldestImage, function () {
                    o.loadImages(function () {
                        o.showLast();
                    },null,true);
                });

            }, function () {
                return false;
            });
        }
        else if ( _enableWarehouse ){
            var options = new CCWarehouseAPI.CCWImageCaptureOptions();
            options.Width = ccma.UI.Camera.MaxCaptureWidth;
            options.Height = ccma.UI.Camera.MaxCaptureHeight;
            options.Watermark = photolabel;
            if(photolabel < 0) options.Watermark = "";
            options.CameraOnly = false; 
            options.MenuX = $( '.pm-add' ).offset().left + 25
            options.MenuY = $( '.pm-add' ).offset().top + 60
            CCWarehouse.CaptureImage( options , function ( file, errorMessage ){
                var pu = {};
                pu.parcelId = o.pid;
                pu.keyvalue = o.keyvalue;
                pu.timeStamp = new Date();
                pu.file = null;
                var source = "data:image/jpeg;base64," + file.Data.replace( /\n|\r/g, "" );
                pu.dataUrl = source;
                var imgWH=new Image();
                imgWH.src=source;
                function ResizeWH (pu) {
	                pu.accepted = false;
	                pu.uid = "pu-" + tick().toString()
	                pu.metadata = [];
	                var options = new CCWarehouseAPI.CCWImageResizeOptions();
	                options.Width = ccma.UI.Camera.MaxStorageWidth;
	                options.Height = ccma.UI.Camera.MaxStorageHeight;	                
	                if(imgWH.width<imgWH.height)
	                	[options.Width,options.Height]=[options.Height,options.Width];
	                options.Data = file.Data;
	                CCWarehouse.ResizeImage( options, function ( file, errorMessage ) {
	                    pu.smallImage = "data:image/jpeg;base64," + file.Data.replace( /\n|\r/g, "" );
	                    pstream.push( pu );
	                    o.loadImages( function (){
	                        o.showLast();
	                    },null,true);
	                } );
                }
                imgWH.onload=function(){
	                ResizeWH(pu);
                }
                
                imgWH.onerror = function(){
				    console.error('image source loading error');
				    ResizeWH(pu);
				}                
            } );
        }
        else {
            pstream.openCamera("camera", o.pid, o.keyvalue, function () {
                o.loadImages(function () {
                    o.showLast();
                },null,true);
            },o.altKeyValue);
        }
    }

    this.addNewFromParcel = function () {
    	photoMetaDataValues = {};
    	appState.photoCatId = 0;
        var images = o.images.filter(function (i) { return i.IsPrimary == null || i.IsPrimary == 0 });
        var oldestImage = images[0];
        for (var i = 0; i < images.length; i++) {
            if (images[i].UploadTime < images[0].UploadTime)
                oldestImage = images[i];
        }
        var altFieldvalue = ShowAlternateField == "True" ? eval("activeParcel."+AlternateKey) : null;
        var photolabel = ShowAlternateField == "True" ? (ShowAlternateField == "True" && ShowKeyValue1 == "True" && eval("activeParcel."+AlternateKey)) ? activeParcel.KeyValue1 +' | '+ eval("activeParcel."+AlternateKey) : eval("activeParcel."+AlternateKey) : activeParcel.KeyValue1
        if (o.images.length >= parseInt(clientSettings.MaxPhotosPerParcel)) {
            messageBox('This property has exceeded the limit of photos. The oldest photo will be deleted. Do you want to continue?', ["Yes", "No"], function () {
                deleteImagesIfLimitExceeded(oldestImage, function () {
                	   var altKeyValue = ShowAlternateField == "True" ? eval("activeParcel."+AlternateKey) : null;
                     o.openParcel(activeParcel.Id, activeParcel.KeyValue1,null,null, altKeyValue);
                o.loadImages(function () {
                    o.showLast();
                },null,true);
                photos.fillCatalogue(activeParcel.Id, '#photo-catalogue');
                });
            }, function () {
                return false;
            });
        }
        else if ( _enableWarehouse )
        {
            var options = new CCWarehouseAPI.CCWImageCaptureOptions();
            options.Width = ccma.UI.Camera.MaxCaptureWidth;
            options.Height = ccma.UI.Camera.MaxCaptureHeight;
            options.Watermark = photolabel;
            if(photolabel < 0) options.Watermark = "";
            options.CameraOnly = false; 
            options.MenuX = $( '.add-photo' ).offset().left + 60
            options.MenuY = $( '.add-photo' ).offset().top + 40
            CCWarehouse.CaptureImage( options , function ( file, errorMessage ){
                var pu = {};
                pu.parcelId = o.pid;
                pu.keyvalue = o.keyvalue;
                pu.timeStamp = new Date();
                pu.file = null;
                var source = "data:image/jpeg;base64," + file.Data.replace( /\n|\r/g, "" );
                pu.dataUrl = source;
                var imgWH=new Image();
                imgWH.src=source;
                function ResizeWH (pu) {
                	pu.accepted = false;
	                pu.uid = "pu-" + tick().toString()
	                pu.metadata = [];
	                var options = new CCWarehouseAPI.CCWImageResizeOptions();
	                options.Width = ccma.UI.Camera.MaxStorageWidth;
	                options.Height = ccma.UI.Camera.MaxStorageHeight;
	                if(imgWH.width<imgWH.height)
	                	[options.Width,options.Height]=[options.Height,options.Width];
	                options.Data = file.Data;
	                CCWarehouse.ResizeImage( options, function ( file, errorMessage )
	                {
	                    pu.smallImage = "data:image/jpeg;base64," + file.Data.replace( /\n|\r/g, "" );
	                    pstream.push( pu );
	                    var altKeyValue = ShowAlternateField == "True" ? eval("activeParcel."+AlternateKey) : null;
	                    o.openParcel( activeParcel.Id, activeParcel.KeyValue1,null,null, altKeyValue );
	                    o.loadImages( function (){
	                        o.showLast();
	                    },null, true );
	                } );              	
                }
                imgWH.onload=function(){
	                ResizeWH(pu);
                }
                
                imgWH.onerror = function(){
				    console.error('image source loading error');
				    ResizeWH(pu);
				}                
            });
        }
    else {
        pstream.openCamera("camera1", activeParcel.Id, activeParcel.KeyValue1, function () {
            var altKeyValue = ShowAlternateField == "True" ? eval("activeParcel."+AlternateKey) : null;
            o.openParcel(activeParcel.Id, activeParcel.KeyValue1,null,null, altKeyValue);
            o.loadImages(function () {
                o.showLast();
            },null,true);
        },altFieldvalue);
    }
    }
    
   	this.addNewFromRecord = function () { 
        photoMetaDataValues = {};
        catId = appState.photoCatId;
    	if(catId && catId > 0){
    		var auxForm = $('section[catid="'+catId+'"]');
            var findex = $(auxForm).attr('findex');
            var sourceTable = $(auxForm).attr('auxdata');
    		var cat = getCategory(catId);
			var data = activeParcel[sourceTable][findex];
			if(cat.PhotoTag && cat.PhotoLinkTableKey){
				var PhotoKey = data[cat.PhotoLinkTableKey];	
				if(PhotoKey == null)
					PhotoKey = "";				
				PhotoKey = PhotoKey+'$'+data["ROWUID"];	
				photoMetaDataValues[clientSettings["PhotoTagMetaField"]] = cat.PhotoTag;
				photoMetaDataValues[clientSettings["PhotoTableLinkMetaField"]] = PhotoKey;
			}				
		}
        var images = o.images.filter(function (i) { return i.IsPrimary == null || i.IsPrimary == 0 });
        var oldestImage = images[0];
        for (var i = 0; i < images.length; i++) {
            if (images[i].UploadTime < images[0].UploadTime)
                oldestImage = images[i];
        }
        var altFieldvalue = ShowAlternateField == "True" ? eval("activeParcel."+AlternateKey) : null;
        var photolabel = ShowAlternateField == "True" ? (ShowAlternateField == "True" && ShowKeyValue1 == "True" && eval("activeParcel."+AlternateKey)) ? activeParcel.KeyValue1 +' | '+ eval("activeParcel."+AlternateKey) : eval("activeParcel."+AlternateKey) : activeParcel.KeyValue1
        if (o.images.length >= parseInt(clientSettings.MaxPhotosPerParcel)) {
            messageBox('This property has exceeded the limit of photos.The oldest photo will be deleted.Do you want to continue ?', ["Yes", "No"], function () {
                deleteImagesIfLimitExceeded(oldestImage, function () {
                		var altKeyValue = ShowAlternateField == "True" ? eval("activeParcel."+AlternateKey) : null;
                     o.openParcel(activeParcel.Id, activeParcel.KeyValue1,null,null, altKeyValue);
                o.loadImages(function () {
                    o.showLast();
                },null,true);
                photos.fillCatalogue(activeParcel.Id, '#photo-catalogue');
                });
            }, function () {
                return false;
            });
        }
        else if ( _enableWarehouse )
        {
            var options = new CCWarehouseAPI.CCWImageCaptureOptions();
            options.Width = ccma.UI.Camera.MaxCaptureWidth;
            options.Height = ccma.UI.Camera.MaxCaptureHeight;
            options.Watermark = photolabel;
            if(photolabel < 0) options.Watermark = "";
            options.CameraOnly = false; 
            options.MenuX = $( '.record-add-photo' ).offset().left + 30
            options.MenuY = $( '.record-add-photo' ).offset().top + 40
            CCWarehouse.CaptureImage( options , function ( file, errorMessage ){
                var pu = {};
                pu.parcelId = o.pid;
                pu.keyvalue = o.keyvalue;
                pu.timeStamp = new Date();
                pu.file = null;
                var source = "data:image/jpeg;base64," + file.Data.replace( /\n|\r/g, "" );
                pu.dataUrl = source;
                var imgWH=new Image();
                imgWH.src=source;
                function ResizeWH (pu) {
	                pu.accepted = false;
	                pu.uid = "pu-" + tick().toString()
	                pu.metadata = [];
	                var options = new CCWarehouseAPI.CCWImageResizeOptions();
	                options.Width = ccma.UI.Camera.MaxStorageWidth;
	                options.Height = ccma.UI.Camera.MaxStorageHeight;
	                if(imgWH.width<imgWH.height)
	                	[options.Width,options.Height]=[options.Height,options.Width];
	                options.Data = file.Data;
	                CCWarehouse.ResizeImage( options, function ( file, errorMessage )
	                {
	                    pu.smallImage = "data:image/jpeg;base64," + file.Data.replace( /\n|\r/g, "" );
	                    pstream.push( pu );
	                    var altKeyValue = ShowAlternateField == "True" ? eval("activeParcel."+AlternateKey) : null;
	                    o.openParcel( activeParcel.Id, activeParcel.KeyValue1,null,null, altKeyValue );
	                    o.loadImages( function (){
	                        o.showLast();
	                    },null,true );
	                } );
	            }
                imgWH.onload=function(){
	                ResizeWH(pu);
                }
                
                imgWH.onerror = function(){
				    console.error('image source loading error');
				    ResizeWH(pu);
				}
            } );
        }
    else {
        pstream.openCamera("camera2", activeParcel.Id, activeParcel.KeyValue1, function () {
        var altKeyValue = ShowAlternateField == "True" ? eval("activeParcel."+AlternateKey) : null;
            o.openParcel(activeParcel.Id, activeParcel.KeyValue1,null,null, altKeyValue);
            o.loadImages(function () {
                o.showLast();
            },null,true);
        },altFieldvalue);
    }
    }
  
    this.acceptPhoto = function () {
        o.adddelete = true;
        refreshMiniSS = true;
        var img = o.images[o.index];
        pstream.acceptPhoto(img.LocalId, function () {
            img.Accepted = true;
            if (!o.images.some(function (s) { return checkIsFalse(s.IsPrimary); }) && !appState.photoCatId &&  !appState.sketchDccPhoto) {
                $('.pm-prmflag').attr('disable', '')
                o.markAsPrimaryImage(0);
            }
            savePhotoDefaultProperties(img, null, photoMetaDataValues, true);
            $('.pm-accept').hide();
            $('.delete-selected-photo').show();
        });
    }

    this.bulkAcceptPhoto = function (callback) {
        var no = 0, tot = 0;
        o.adddelete = true;
        refreshMiniSS = true;
        var acceptThis = function (indexAcc) {
            var img = o.images[indexAcc];
            if ( img.Accepted != 0 ) {
                if ( indexAcc == o.images.length - 1 ) {
                    if ( callback ) callback();
                    return;
                }
                acceptThis( indexAcc + 1 );
                return;
            }
            no = no + 1;
            $( '.maskText' ).html( 'Accepting photos - ' + no + '/' + tot )
            IDB_getData('Images', ['LocalId'], ['='], [img.LocalId.toString()], [], function (duplicateCount) {
            //getData('select * from images where LocalId=?', [img.LocalId.toString()], function (duplicate) {
                if (duplicateCount <= 0) {
                    pstream.acceptPhoto(img.LocalId, function () {
                        img.Accepted = true;
                        savePhotoDefaultProperties(img, null, photoMetaDataValues, true);
                        $('.delete-selected-photo').show();
                        if ( indexAcc == o.images.length - 1 ) {
                            if ( callback ) callback();
                        }
                        else
                            acceptThis( indexAcc + 1 );
                    });
                }
            }, null, null, null, true);
        }
        tot = o.images.filter( function ( img ) { return img.Accepted == 0 } ).length
        if (o.images.length > 0) 
            acceptThis( 0 );
        else 
            if (callback) callback();
    }

    this.deletemultiple = function () {
       var checkedImages = $('.selectedimage').map(function () {
            return $(this).parent().parent().index();
        }).get();
        if (checkedImages.length == 0) {
            messageBox('Select at least one photo to delete.');
            return false;
        }
        messageBox('You are permanently deleting selected photos from the cloud. Are you sure you want to continue?', ["Yes", "No"], function () {
            checkLinkedProperties(o.pid, function(linkFlag) {
                deleteimages(checkedImages, o, linkFlag, function () {
                //getData("SELECT * FROM Images WHERE Type = '1' AND ParcelId = ? LIMIT " + ccma.UI.Camera.MaxPhotosPerParcel, [o.pid.toString()], function (im) {
                        //IDB_getData('Images',['ParcelId'],['='],[parseInt(o.pid)],[], function (im) {
                    IDB_getData('Images', ['ParcelId', 'Type'], ['=', '='], [parseInt(o.pid), 1], ['AND'], function (im) {
                        refreshImagesInSortScreen("my-parcels", o.pid);
                        im = im.slice(0,ccma.UI.Camera.MaxPhotosPerParcel);
                        o.images = im;   
                        if (im.length > 0 && !o.images.some(function (s) { return checkIsFalse(s.IsPrimary); })) {
                                    o.markAsPrimaryImage(0,function(){
                                    photos.closePM();                                
                                    });
                        } 
                        else photos.closePM();       
                    });
                });
                if (checkedImages.length == o.images.length) {
                    $('.delete-selected-photo').hide();
                }
                else {
                    $('.delete-selected-photo').show();
                }
            });
        });    
    }
    
    this.deletePhoto = function () {
        if (o.images.length <= 0) {//safety check will return function if there is no data
            return false;
        }
        var callback = function () {
            (nextIndex) ? o.loadImages(function () { o.showImage(nextIndex); },null,true) : o.loadImages(null,null,true);
        }
        var img = o.images[o.index];
        o.adddelete = true;
        var nextIndex = !(o.index == 0 || o.index == o.images.length - 1) ? o.index : ((o.index == 0) ? (o.images.length > 1 ? o.index : null) : o.images.length - 2);

        if (img.Accepted == 0) {
            messageBox('You are permanently deleting an unaccepted parcel photo. Are you sure you want to continue?', ["Yes", "No"], function () {
                pstream.removePhoto(img.LocalId, callback);
            });
        } else if (img.Synced == 0) {
            messageBox('You are permanently deleting an unsynced parcel photo. Are you sure you want to continue?', ["Yes", "No"], function () {
			    todoDB.indexedDB.deleteRecord('UnsyncedImages',['LocalId'],['='],[img.LocalId.toString()],[], function() {
	    		    todoDB.indexedDB.deleteRecord('Images', ['LocalId'], ['='], [img.LocalId.toString()], [], function() {
	                //getData('DELETE FROM Images WHERE LocalId = ?', [img.LocalId.toString()], function () {
	                    getData('DELETE FROM ParcelChanges WHERE ((Field = "PhotoMetaData" OR Field = "MarkAsPrimaryPhoto") AND (ParcelId IN (?) OR NewValue IN(?) ))', [img.LocalId.toString(), img.LocalId.toString()], callback)
	                	    if(clientSettings.PhotoTagMetaField && clientSettings.PhotoTableLinkMetaField != null && appState.photoCatId == 0 && !appState.sketchDccPhoto) reloadFlag = true;
	                	    UpdateActiveParcelFirstPhoto(function () {
		               		    if(ccma.UI.ActiveScreenId == "digital-prc") refreshDigitalPRC();
		                    })   
	                  });
                });
            });
        } else {
            messageBox('You are permanently deleting a parcel photo from the cloud. Are you sure you want to continue?', ["Yes", "No"], function () {
                checkLinkedProperties(o.pid, function(linkFlag){
                    //getData('DELETE FROM Images WHERE Id = ?', [img.Id.toString()], function () {
                    todoDB.indexedDB.deleteRecord('Images', ['Id'], ['='], [parseInt(img.Id.toString())], [], function() {
                        getData('DELETE FROM ParcelChanges WHERE ((Field = "PhotoMetaData" OR Field = "MarkAsPrimaryPhoto") AND (ParcelId IN(?,?) OR NewValue IN(?,?) ))', [img.Id.toString(), img.LocalId.toString(), img.Id.toString(), img.LocalId.toString()], function () {
                            ccma.Sync.enqueueEvent(img.ParcelId, 'DeleteParcelPhoto', img.Id, {}, callback);
                            if(linkFlag == true) generateLinkedPropertiesPCI(img);
                            if(clientSettings.PhotoTagMetaField && clientSettings.PhotoTableLinkMetaField != null && appState.photoCatId == 0 && !appState.sketchDccPhoto) reloadFlag = true;
                            UpdateActiveParcelFirstPhoto(function () {
                                refreshDigitalPRC();
                            })
                        });
                    });
                });
            });
        }
    }

    this.markAsPrimaryImage = function (imgIndex, callback) {
        //console.log('inside mark as primary calee:' + this.markAsPrimaryImage.caller);

        if (checkIsNull(imgIndex)) {
            if (!o.images[imgIndex]) {
                return false;
            }
        }
        var img = checkIsNull(imgIndex) ? o.images[imgIndex] : o.images[o.index];
        var currIndex = checkIsNull(imgIndex) ? imgIndex : o.index;
        if ((img.Accepted == true) && !checkIsFalse(img.IsPrimary)) {
            todoDB.indexedDB.update('Images', [{name: 'IsPrimary', value: 0}], ['ParcelId'], ['='], [parseInt(o.pid.toString())], [], function() {
            //getData('UPDATE Images SET IsPrimary=0 WHERE ParcelId=?', [o.pid.toString()], function () {
            	todoDB.indexedDB.update('Images', [{name: 'IsPrimary', value: 1}], [(checkIsFalse(img.Synced) ? 'Id' : 'LocalId'),], ['='], [checkIsFalse(img.Synced) ? parseInt(img.Id) : img.LocalId], [], function() {
                //getData('UPDATE Images SET IsPrimary=1 WHERE ' + (checkIsFalse(img.Synced) ? 'Id = ?' : 'LocalId = ?'), [checkIsFalse(img.Synced) ? img.Id : img.LocalId], function () {
                    UpdateActiveParcelFirstPhoto(function () {
                        refreshDigitalPRC();
                    })
                    ccma.Sync.enqueueEvent(img.ParcelId, 'MarkAsPrimaryPhoto', (img.Id == null) ? img.LocalId.toString() : img.Id.toString(), {});
                    o.images[currIndex].IsPrimary = 'true';
                    getData("SELECT * FROM Parcel WHERE  Id = "+ parseInt(img.ParcelId).toString() +"", [], function (p) {
						if(p[0].Reviewed) refreshSortScreen = true ;		
                    	executeQueries("UPDATE Parcel SET Reviewed = 'false' WHERE Id = " + parseInt(img.ParcelId).toString());
                    	executeQueries("UPDATE Parcel SET CC_Priority = 3 WHERE CC_Priority = 0 AND Id = " + parseInt(img.ParcelId).toString());
                    });
                    o.loadImages(function () {
                        refreshImagesInSortScreen("my-parcels",img.ParcelId, function ()
                        {
                            o.showImage(currIndex);
                            if (callback) callback();
                        });
                    },true,true);
                });
            });
        }
    }

    this.fillCatalogue = function ( pid, catalog, type )
    {
        if ( !type && this.type )
            type = this.type
        if ( type == 2 )
            $( '.add-photo' ).hide();
        else
        { type = 1; $( '.add-photo' ).show(); }

        this.type = type
        var catframe = catalog;
        $( catframe ).html( '' );
        //getData("SELECT * FROM Images WHERE Type = '1' AND ParcelId = ? LIMIT " + ccma.UI.Camera.MaxPhotosPerParcel, [pid.toString()], function (im) {
       // IDB_getData('Images',['ParcelId'],['='],[parseInt(pid)],[], function (im) {
        IDB_getData( 'Images', ['ParcelId', 'Type'], ['=', '='], [parseInt( pid ), type], ['AND'], function ( im ) {
			im = im.slice(0,ccma.UI.Camera.MaxPhotosPerParcel);
			if (im.length >= ccma.UI.Camera.MaxPhotosPerParcel || !iPad)
			    $( '#camera1,#camera2' ).hide()
			else $( '#camera1,#camera2' ).show();
			if ( type == 2 ) {
			    activeParcel.sketchImages = im;
			    activeParcel.sketchImages.filter((xImg) => { return xImg.IsPrimary == 1; }).forEach((xIm) => { xIm.IsPrimary = 0; });
			} 
            im.forEach(function (w) {
                if (w.Image == 'Download') { w.download = "Download"; } else { w.download = ""; };
            })
            var cpics = pstream.getPhotos(pid)
            for (i in cpics) {
                var pu = cpics[i];
                if (!im.some(function (d) { return d.LocalId == pu.uid })) {
                    if (pu.dataUrl) {
                        var img = new Object();
                        img.Id = null;
                        img.Image = pu.dataUrl;
                        img.ParcelId = pid;
                        img.LocalId = pu.uid;
                        img.Path = null;
                        img.Type = 1;
                        img.Accepted = pu.accepted ? 1 : 0;
                        img.Synced = 0;
                        img.IsPrimary = pu.isprimary ? 1 : 0;
                        im.push(img);
                    }
                }
            }
            o.images = im;
            o.allimages = im;
            if ( type == 1 && o.images.length > 0 && !o.images.some(function (s) { return checkIsFalse(s.IsPrimary); })){ 
    			o.markAsPrimaryImage(0);
				im[0].IsPrimary = 1;		
				}
            var url = "/static/css/images/img1.jpg";
            // $(catframe).html('<div class="photo-cat-item-frame"> <div class="photo-cat-item" catpid="' + pid + '" iid="${Id}" lid="${LocalId}" acc="${Accepted}" sync="${Synced}" primary="${IsPrimary}" style="background-image:url(${Image})"></div></div>');
            $( catframe ).html( '<div class="photo-cat-item-frame"><span  class="selectimagespan"><span class="selectimagecheckbox"> </span></span> <div class="photo-cat-item" catpid="' + pid + '" iid="${Id}" lid="${LocalId}" type="${Type}" acc="${Accepted}" sync="${Synced}" download="${download}" primary="${IsPrimary}"  style="background-image:url(${Image})"></div><div class="downloadphoto" style="background-image:url(' + url + ')"  onclick="downloadPhoto(${Id},this)"></div></div>' );
            $(catframe).fillTemplate(im);

            $('.selectimagespan').unbind(touchClickEvent);
            $('.selectimagespan').bind(touchClickEvent, function (e) { $(this).find('.selectimagecheckbox').hasClass('selectedimage') ? $(this).find('.selectimagecheckbox').removeClass("selectedimage") : $(this).find('.selectimagecheckbox').addClass("selectedimage"); e.stopPropagation(); });
            $('.photo-cat-item', catframe).each(function () {
                var img = this;
                var iid = $(this).attr('iid');
                var lid = $( this ).attr( 'lid' );
                var type = $( this ).attr( 'type' );
                var download = $(this).attr('download');
                var primary = $(this).attr('primary');
                if (iid == '')
                    iid = null;
                if (lid == '')
                    lid = null;

                if (checkIsFalse(primary)) 
                    $('<a class="tabPrimary" ></a>').insertBefore(this);
                if (download == "") { $('.downloadphoto', $(img).parent()).remove(); }

                $(img).unbind(touchClickEvent);
                $(img).bind(touchClickEvent, function (e) {
                    e.preventDefault();
                    if ( type == 2 )
                        sketchImage.openSketch( iid, lid );
                    else if ( download == ""  && !scrolling ){
                        $('.downloadphoto', img).remove();
                        var altKeyValue = ShowAlternateField == "True" ? eval("activeParcel."+AlternateKey) : null;
                        photos.openParcel(activeParcel.Id, activeParcel.KeyValue1, { id: iid, localId: lid, index: null },null,altKeyValue);
                    }
                });
            });
            //refreshScrollable()
        });
    }
    
    
    $('.photo-meta-photoPreview').unbind(touchClickEvent);    
	$('.photo-meta-photoPreview').bind(touchClickEvent, function (e) {
	 e.preventDefault();
	  $('.overlay-photo-properties').hide();
	    hideKeyboard();
	});
	
    this.showPhotoPreview = function () {
    if(o.images && o.images.length > 0) {
    	var img = o.images[o.index];
        var index=o.index;
        var len=o.images.length;
        $('.photo-meta-photoPreview').attr('iid',img.Id);
        $('.photo-meta-photoPreview').attr('lid',img.LocalId);
        $('.photo-meta-photoPreview').attr('pid',img.ParcelId);
        $('.photo-meta-photoPreview').css('background-image', "url('" + img.Image + "')");
       } 
    }
    
    this.showProperties = function () {
        if (o.images == null) { console.log('o.images is in an invalid state.'); return false; }
        if (o.images.length == 0) { console.log('o.images no not images loaded.'); return false; }
        if (o.index == -1) { console.log('o.index value is unassigned.'); return false; }
        var img = o.images[o.index];
        var index=o.index;
        var len=o.images.length;
        if (!img.Accepted) { messageBox('You cannot edit properties of unaccepted photos.'); return false; }
        loadPhotoProperties(img, function () {
            if ((img.DownSynced == '1' || img.DownSynced == 'true') && (clientSettings["PhotoNoDownsyncOnFlagging"] == '1' || clientSettings["PhotoNoDownsyncOnFlagging"] == 'true')) {
                $('.photo-meta-fields .newvalue').prop("disabled", true);
                $('.photo-meta-fields .newvalue input').prop("disabled", true);
            }
			else {
				$('.photo-meta-fields .newvalue').removeAttr("disabled");
                $('.photo-meta-fields .newvalue input').removeAttr("disabled");
			}
        });
        $('.photo-index').html('Photo ' + (index + 1) + '/' + len);
        $('.overlay-photo-properties').show();
        var Keyboardhide=false;
		$('.overlay-photo-properties').on(touchClickEvent,function(){
			Keyboardhide = true ;
     		if(Keyboardhide){ 
     			hideKeyboard();
     			Keyboardhide=false;
     		}
        });
        $('.photo-meta-fields input,.photo-meta-fields textarea').on(touchClickEvent,function(e){
        	Keyboardhide=true;
        });

        //Disable the PushToLinkedProperties to restrict manual user changes
        if(clientSettings && clientSettings['PhotoDeleteAndRecover'] == 1 && clientSettings['DeletePhotoPromptForLinkedProperties'] == 1) {
            let field = getDataField('linked');
            $(`fieldset[field-id="${field.Id}"]`).find('select').attr("disabled", true);
        }
        
     	/*$('.photo-meta-fields input,.photo-meta-fields textarea').on(touchClickEvent,function(e){
     		PhotoPropElement = this;
	        if (Math.abs(window.orientation) == 90 || Math.abs(window.orientation) == 270) return;
     		var position = $(PhotoPropElement).position();
			var element = document.getElementsByClassName('photo-meta-fields')[0];
			var offset = position.top - 520;			
			if (offset > 0) {
				PropFieldFocus = true;
				element.scrollTop = element.scrollTop + offset;
			}
			window.setTimeout(function(){ PropFieldFocus = false; }, 200)
     		
     		e.stopPropagation();
     	});*/
		/*$('.photo-meta-fields').on('scroll', function () {
	        if (PhotoPropElement && !PropFieldFocus) $(PhotoPropElement).blur();
	    });*/
    }

    this.hideProperties = function () {
        var auxFormFields = $('fieldset[ro="false"]', '.photo-meta-fields')

        var elems = $('[required]', auxFormFields);
        if (elems.length == 0)
            $('.overlay-photo-properties').hide();
        var requiredElems = $('[required]', auxFormFields);
        var valid = true;
        if (requiredElems.length > 0) {
            valid = $(requiredElems).map(function (y, x) { return ( $(x).val() && $(x).val().trim() != "" ) }).reduce(function (x, y) { return x && y })
        }
        if (!valid) {
            messageBox('All required fields are not filled in. Are you sure you want to go back to the photo?', ["Yes", "No"], function () {
                $('.overlay-photo-properties').hide();
                o.showImage(o.index);

            });
            return false;
        }

        else $('.overlay-photo-properties').hide();
        o.showImage(o.index);

    }
}

function checkIsFalse(source) {
    (typeof source == 'string') ? (isNaN(source) ? '' : source = parseInt(source)) : '';
    var type = typeof source, returnVal = false;
    switch (type) {
        case 'string': returnVal = (source == 'false' || source == '0') ? false : ((source == 'true' || source == '1') ? true : false); break;
        case 'number': returnVal = (source) ? true : false; break;
        case 'boolean': returnVal = source; break;
        case 'object': returnVal = (source) ? true : false; break;
        case 'undefined': returnVal = (source) ? true : false; break;
    }
    return returnVal;
}

function checkLinkedProperties(pid, callback) {
    if(!(clientSettings && clientSettings['PhotoDeleteAndRecover'] == 1 && clientSettings['DeletePhotoPromptForLinkedProperties'] == 1)) return callback ? callback(false) : false;
    let linkTable = clientSettings['DeletePhotoPromptTableCheck'];
    if(!linkTable) return callback ? callback(false) : false;
    getRecords(pid, linkTable).then(records =>{
        if(records>0) {
            messageBox(`This image is linked to ${records} other properties, do you want to delete the photo from all other linked properties as well?`,['YES','NO'],()=>{
                //YES
                if(callback && typeof callback ==='function') return callback(true);
            },
            ()=>{
                if(callback && typeof callback ==='function') return callback(false);
            });
        } else if (callback) return callback(false);
    })
}

function generateLinkedPropertiesPCI(img){
    if(!img) return false;
    let field = getDataField('linked');   //linked is the metadata field name of Push To Linked Properties
    if(!(field && field.AssignedName)) return false;
    let metaField = field.AssignedName.replace('PhotoMetaField', 'MetaData');
    let value = false;  // Setting dropdown as No(0) as default value is Yes(1)
    ccma.Sync.enqueueCommand(img.Id || img.LocalId, 'PhotoMetaData', metaField, value, { updateData: true, source: 'Images', sourceKey: (img.Id != null ? 'Id' : 'LocalId'), sourceKeyValue: img.Id || img.LocalId, sourceField: metaField }, function () {
    })
}

var getRecords=(pid, linkTable)=>{
    return new Promise((resolve, reject) => {
        let count;
        if(activeParcel && typeof activeParcel !== 'undefined'){
            count = activeParcel[linkTable]?.length;
            resolve(count);
        } else {
            getData('SELECT COUNT(*) FROM ? where ParcelId = ?', [pid.toString()], function (c) {
                console.log('Inside getData ',c)
                resolve(c);
            });
        }
    })
};