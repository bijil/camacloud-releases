﻿//    DATA CLOUD SOLUTIONS, LLC ("COMPANY") CONFIDENTIAL 
//    Unpublished Copyright (c) 2010-2013 
//    DATA CLOUD SOLUTIONS, an Ohio Limited Liability Company, All Rights Reserved.

//    NOTICE: All information contained herein is, and remains the property of COMPANY.
//    The intellectual and technical concepts contained herein are proprietary to COMPANY
//    and may be covered by U.S. and Foreign Patents, patents in process, and are protected
//    by trade secret or copyright law. Dissemination of this information or reproduction
//    of this material is strictly forbidden unless prior written permission is obtained
//    from COMPANY. Access to the source code contained herein is hereby forbidden to
//    anyone except current COMPANY employees, managers or contractors who have executed
//    Confidentiality and Non-disclosure agreements explicitly covering such access.</p>

//    The copyright notice above does not evidence any actual or intended publication
//    or disclosure of this source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
//    DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
//    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
//    IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
//    OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
//    TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
//    ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

if (CAMACloud === undefined) CAMACloud = {};
var template_Call = false ;
var cpyfirsttemplate_fail = false;

CAMACloud.DataController = function () {

    this.startProcess = function (label) { }
    this.updateProgress = function (label, progress) { }
    this.endProcess = function () { }
    this.autoInsertDetails = [];

    function recursive( list, onItem, onFinish ){
        if (list.length == 0) {
            if (onFinish) onFinish();
        } else {
            var item = list.shift();
            var noNeedToInsert = item.noNeedToInsertRecord ? true: false; // if same sourceTable for more than one category then only one category record insert. other wise duplicate the record
            onItem(item, function () {
                recursive(list, onItem, onFinish);
            }, noNeedToInsert);
        }
    }

    function blankCopyAction(c, callback) {

        if (callback) callback();
    }

    //sourceParcel, category, parentCategory, parentElement, targetParentElement, copyMethod, callback
      function copyCategoryRecords( source, c, pc, pe, npe, objectCopyMethod, proc_callback, ignore, options, mcis_callback){
        if ( !options ) options = {};
        if ( objectCopyMethod ){
            recursive( c, function ( cat, getNextcat, noNeedToInsert ){
                var parentcatId = getParentCategories(cat.Id);
                if(cat.SourceTable == getSourceTable(parentcatId) || noNeedToInsert)
                    elements=[];  
                else{
                    var elements = source[cat.SourceTable] ? source[cat.SourceTable].slice() : [source];
                    var allElements = elements.slice();
                    if (parentcatId != null && ((options.type == 'futureYear' && !(options.CopyRecordNotChildOfParcel && getSourceTable(parentcatId) == clientSettings['CopyRecordNotChildOfParcel']?.split(',')[0])) || options.type == 'template')){
                      	 ignore = null;
                    }
                    if(copyingCopyCondition && elements.length > 0) {
                    	elements = elements.filter(function(x, index){ return source.EvalValue(cat.SourceTable+'['+index+']', copyingCopyCondition) });
                    }
                    if ( copyingIgnoreCondition && elements.length > 0 && ignore ){
                        var regexCopyingIgnoreCondition = copyingIgnoreCondition;
                        for ( x in elements[0] ) { var regex = new RegExp( "(?=\\b)(" + x + ")(?=\\b)", "g" ); regexCopyingIgnoreCondition = regexCopyingIgnoreCondition.replace( regex, 'thisObject.' + x ) };
                        elements = eval( 'elements.filter(function (thisObject) { return !(' + regexCopyingIgnoreCondition + ') })' );
                        regexCopyingIgnoreCondition = null; 
                    }

                	if ( options.type == 'futureYear' && !cat.YearPartitionField )
                    	elements = [];
                	else if ( cat.Id == 0 )
                    	elements = elements.filter( function ( record ) { return ( ( record.CC_YearStatus == 'A' || !record.CC_YearStatus ) && record[cat["YearPartitionField"]] ) } )
                    else if (elements && elements.length && (options.type == 'futureYear' && options.mode != 1)) {
                        elements = elements.filter((record) => { return ((record[cat.YearPartitionField] && record.CC_YearStatus != 'F') && ((cat.DisableCopyRecordExpression && cat.SourceTable) ? !(activeParcel.EvalValue(cat.SourceTable +'[' + activeParcel[cat.SourceTable].findIndex(x => x.ROWUID == record.ROWUID) + ']', cat.DisableCopyRecordExpression)) : true )) } ) //Disable record copy to futureyear on eval DisablecopyRecord expression
                    }
                    else if (elements && elements.length && options.type == "Imprv" && cat.DisableCopyRecordExpression && cat.SourceTable) {
                        elements = elements.filter((record) => { return !(source.EvalValue(cat.SourceTable + '[' + source[cat.SourceTable].findIndex(x => x.ROWUID == record.ROWUID) + ']', cat.DisableCopyRecordExpression)) });
                    }

                	if ( pc ){
                    	if ( ( cat.FilterFields == '' ) || ( cat.FilterFields == '' ) ){
                        	if ( proc_callback ) proc_callback();
                        	return;
                    	}
                    	var ff = cat.FilterFields.split( ',' );
                    	if ( cat.YearPartitionField )
                    	    ff.push( cat.YearPartitionField );
                    	 //elements = pe[cat.SourceTable] || [];
                    	elements =elements.filter( function ( x ){
                        	return ff.map( function ( f ){
                            	var cf, pf; pf = cf = f.trim();
                            	if ( cf.indexOf( '/' ) > -1 ) { cf = f.split( '/' )[1]; pf = f.split( '/' )[0]; }
                            	return ( !x.ParentROWUID && ( x[cf] == pe[pf] ) ) || ( x.ParentROWUID && x.ParentROWUID == pe.ROWUID )
                        	} ).reduce( function ( x, y ) { return x && y } )
                    	} );
                    	if(elements.length > 1 && options.selectSpecifier == 'first'){
                        	elements = elements.splice(0, 1);
                        }
                	}
              
                }
                var isCopiedRecord = false;
                recursive( elements, function ( e, getNextElement ){
                    //if ( allElements.map( function ( a ) { return a.CC_LinkedROWUID } ).indexOf( e.ROWUID ) > -1 || allElements.map( function ( a ) { return a.ROWUID } ).indexOf( e.CC_LinkedROWUID ) > -1 )
                    //    isCopiedRecord = true;
                    objectCopyMethod(cat, e, pe, npe, function (ne){
                        var scats = fieldCategories.filter( function ( f ) { return ( (f.OverrideParentCategory ? f.OverrideParentCategory == cat.Id : f.ParentCategoryId == cat.Id) ) } )
                        getData(  'SELECT * FROM FieldCategory WHERE ParentCategoryId = ' + cat.Id +' or OverrideParentCategory = '+cat.Id, [], function ( subcats )
                        {
                        	var scats = [];
                            subcats.forEach( function ( item ){
                                var doNotIncludeInCategoryDataCopy = false;
                                var occ = scats.filter( function ( p ) { return p.SourceTable == item.SourceTable } ).length
                                if( options.type == "Imprv" && ( fieldCategories.filter( function(x){return ( ( x.Id == item.Id ) && x.UI_CateogorySettings && x.UI_CateogorySettings.DoNotIncludeInDataCopy== "1") } ).length > 0  ) )
                                	doNotIncludeInCategoryDataCopy = true;
                                if ( item.SourceTable && !doNotIncludeInCategoryDataCopy ){
                            	    if(occ > 0)
                            	        item.noNeedToInsertRecord = true;
                                    scats.push( item );
                                }
                            } );
                            copyCategoryRecords(source, scats, cat, e, ne, objectCopyMethod, getNextElement, null, options);
                        });
                    }, isCopiedRecord, source);
                }, getNextcat);
            }, proc_callback )
        }
    }
    
    function autoInsert_function(index, callback) {
		var details = DC.autoInsertDetails[index++];
		if (!details) { DC.autoInsertDetails = []; callback(); return; }
        let thisF = details.category, ParentRowuid = details.Rowuid, CC_YearStatus = details.CC_YearStatus,
            autoInsertItems = thisF.AutoInsertProperties && JSON.parse('[' + thisF.AutoInsertProperties.replace(/}{/g, '},{') + ']');

        if (autoInsertItems) {
            var autoInsertloopCall = function() {
                let autoInsert = autoInsertItems.pop(), isAutoInsertEnabled = (autoInsert && autoInsert.table.trim() != '') ? true : false,
                    sourceRecord = activeParcel[thisF.SourceTable].filter((rec) => { return rec.ROWUID == ParentRowuid })[0],
                    disableInsertOnCopyRecord = disableAutoInsertFun(thisF.SourceTable, sourceRecord, autoInsert),
                    yearType = thisF.YearPartitionField && fylEnabled && CC_YearStatus && CC_YearStatus != 'NULL' ? CC_YearStatus : '',
				    existing_record = isAutoInsertEnabled? DC.autoInsertDetails.some((auto_insert) => { return auto_insert.category.SourceTable == autoInsert.table && auto_insert.parentRowuid == ParentRowuid }): false;

                if (isAutoInsertEnabled && !existing_record && !disableInsertOnCopyRecord) {
					let copyfields = autoInsert.fieldsToCopy.split(','), insertCatId = getCategoryFromSourceTable(autoInsert.table).Id,
					    parentSourceTable = autoInsert.relation.toLowerCase() == 'child' ? thisF.SourceTable: getSourceTable(thisF.parentCategoryId),
					    defaults = Object.keys(datafields).filter(function (k) { return datafields[k].SourceTable && (datafields[k].SourceTable == autoInsert.table && datafields[k].DefaultValue != null) }).map(function (k) { var df = datafields[k]; return { Id: df.Id, Name: df.Name, SourceTable: df.SourceTable, DefaultValue: df.DefaultValue } }),
                        rowuid = ccTicks();

                    ParentRowuid = ParentRowuid && autoInsert.relation.toLowerCase() != 'parcel' ? ParentRowuid : '';
                    let sql = 'INSERT INTO ' + autoInsert.table + ' (ROWUID, ClientROWUID, ParentROWUID, CC_ParcelId', default_count = defaults.length, default_counter = 0;
                        values = [rowuid.toString(), rowuid.toString(), ParentRowuid.toString(), activeParcel.Id.toString()], valueSql = '?, ?, ?, ?';

                    if (yearType) { sql += ', CC_YearStatus'; values.push(yearType.toString()); valueSql += ', ?'; }
					defaults.forEach(function(def) {
						sql += (', ' + def.Name);
                        values.push(getDefaultValue(def, def.DefaultValue));
						valueSql += ', ?';				
					});
					sql += ') VALUES (' + valueSql + ')';

					ccma.Sync.enqueueParcelChange(activeParcel.Id, rowuid, (ParentRowuid || ''), 'new', null, 'NEW', (ParentRowuid || ''), autoInsert.table  +'$'+ yearType, null, function (){
						defaults.forEach(function(def){
							ccma.Sync.enqueueParcelChange(activeParcel.Id, rowuid, (ParentRowuid || ''), null, def.Name, def.Id, null, def.DefaultValue, { updateData: true, source: def.SourceTable }, function() {					
								if (++default_counter == default_count)
									getData(sql, values, function() { if(autoInsertItems.length > 0) autoInsertloopCall(autoInsertItems); else autoInsert_function(index, callback); })
							});
						});				
					});
				}
				else {
                    if(autoInsertItems.length > 0) autoInsertloopCall(autoInsertItems);
                    else autoInsert_function(index, callback);
				}
            }
            autoInsertloopCall(autoInsertItems)
        }
        else autoInsert_function(index, callback);
	}
    
    this.CopyFormData = function (sourceParcelId, targetParcelId, categories, onCompletion, options, mcis_callback, listLength_callback) {
        var HasFYear = activeParcel.HasFutureYear
        showMask('Copying...');
        var FYvalue = '0';
        if (clientSettings.FutureYearValue) {
            FYvalue = clientSettings.FutureYearValue;
        }
        if (!options) options = {};
        if(!options.type == 'BPPCopy') 
        	DC.startProcess('Preparing copy ...')
        var target;
        if (targetParcelId == activeParcel.Id)
            target = activeParcel;
        var defaultValueList = []
        var copyObjectName;
        if(!options.type == 'BPPCopy')
        DC.updateProgress('Preparing copy ...', 10);
        var oCount = 0, sourceActParcel, MapToSourceTableFieldProperties = [];
        if( options.type == 'futureYear' ){
        	var mapFieldPropertyArray = Object.keys(datafieldsettings).map(function (x) { return datafieldsettings[x] }).filter(function (x) { return x.PropertyName == 'MapToSourceTableKeyRelaionShip'}); //load MapToSourceTableKeyRelaionShip property data from datafieldsettings
        	mapFieldPropertyArray.forEach(function(mapFild){
        		var mapDataField =	datafields[mapFild.FieldId];
        		if(mapDataField)
        			MapToSourceTableFieldProperties.push({Id: mapFild.FieldId, fieldName: mapDataField.Name, fieldSourceTable: mapDataField.SourceTable, PropertyName: mapFild.PropertyName, Value: mapFild.Value});
        	});
        }
        getParcelWithAllData(sourceParcelId, function (source) {
            sourceActParcel = source;

            if (options.type == 'Imprv' && options.copySketch) {
                let fld = options.copySketch.Field, fldName = fld.Name, skValue = sourceActParcel[fldName], orgValue = target?.Original?.[fldName] ? target.Original[fldName]: null;
                ccma.Sync.enqueueParcelChange(targetParcelId, null, null, 'E', fld.Name, fld.Id, orgValue, (skValue ? skValue: ''), { source: null, updateData: true });
            }

        if(!options.type == 'BPPCopy')
            DC.updateProgress('Preparing copy ...', 15);
         //   getData('SELECT * FROM FieldCategory WHERE Id = ' + categoryId, [], function (_cats) {
            var _c = categories[0];
            copyObjectName = categories[0].Name;
                copyCategoryRecords( source, categories.slice(), options.parentCategory, options.parentElement, options.targetParentElement, function ( _c, _e, _pe, _npe, _callback )
                {
                    oCount++;
                    if (_callback) _callback();
                }, function () {
                    var totalItems = oCount;
                    var counter = 0;

                    if (totalItems == 0) {
                    	emptyCopyCategories.push(copyObjectName);
                        //DC.endProcess();
                        //$('#maskLayer').hide();
                        //$('#loadText').html('Loading...');
				//if(!(options.type == 'BPPCopy'))
					//refreshAuxSubLinks(ccma.UI.ActiveFormTabId);
                        //messageBox('There are no items to copy from the selected parcel.')
                        //return;
                    }

                    //Counting run complete - now real copy;
                   	getParcelWithAllData(sourceParcelId, function (source) {
                   	if(!options.type == 'BPPCopy')
                    	DC.updateProgress('Preparing copy ...', 15);
                  //      getData('SELECT * FROM FieldCategory WHERE Id = ' + categoryId, [], function (cats) {
                        var clientROWUIDs = '', copyImprvToFuture = false, pciTemplateROWIIDs ='';
                        copyCategoryRecords( source, categories, options.parentCategory, options.parentElement, options.targetParentElement, function ( c, e, pe, npe, callback, isCopiedRecord, source ) {
                            var copy = {};
                            var avoidcopy = [], parcelFields = [];
                            if ( c.IncludeParentFieldsOnInsert )
                                avoidcopy = c.IncludeParentFieldsOnInsert.split( ',' )
                            if ( c.Id == 0 )
                                parcelFields = Object.keys( datafields ).map( function ( x ) { return datafields[x] } ).filter( function ( x ) { return x.SourceTable == null } ).map( function ( a ) { return a.Name } )
                            var keyDatafields = [];
                            var keyFieldsBlocked = [];
                            if (c.SourceTable != null) {
                                var keyFields = tableKeys.filter(function(k) { return k.SourceTable == c.SourceTable }).map(function(keyf){ return keyf.Name });
                                keyDatafields = keyFields.map(function(field){ return getDataField(field, c.SourceTable); }).filter(function(df){ if(df !== null)  return ( ( df.MustIncludeInDataCopy && (df.MustIncludeInDataCopy == '1' || df.MustIncludeInDataCopy == 'true') ) || ( df.DefaultValue && df.DefaultValue.toUpperCase() == "FUTUREYEARSTATUS" ) ) }).map(function(keys){ return keys.Name });
                                keyFieldsBlocked = Object.keys(datafields).map( function ( x ) { return datafields[x] } ).filter(function(df){ return (df['UI_Settings'] && df['UI_Settings'].DoNotIncludeInDataCopy == '1') }).map(function(keys){ return keys.Name });
                            }
                            for (key in e) {
                                if ((key != 'Original' && key != 'ParentRecord' && (!e[key] || typeof e[key] != 'object') && typeof e[key] != 'function' && $.inArray(key, avoidcopy) == -1 && (c.Id != 0 || $.inArray(key, parcelFields) > -1) && (keyFieldsBlocked.indexOf(key) == -1)) ||  (keyDatafields.indexOf(key) != -1))
                                    copy[key] = e[key]
                            };
                            copy["CC_RecordStatus"] = "I"; //new change all copied records CC_RecordStatus changed to I to indicate new record
                            if ( fylEnabled && c.YearPartitionField && showFutureData && (options.type == 'template' ||activeParcel.HasFutureYear == 'true') ) {
                                 copy["CC_YearStatus"] = "F"; // new change all copied records CC_YearStatus changed to F to indicate Future year 
                            }
                             if ( fylEnabled && c["YearPartitionField"]  && (options.type == 'template' )) {
                                 copy[c["YearPartitionField"]] = showFutureData ? FYvalue : clientSettings['CurrentYearValue'];
                                 var keys = tableKeys.filter(function(t){ return t.SourceTable == c.SourceTable}).map(function(tk){ return tk.Name});          
                                     for(k in keys) {
                                     var td = getDataField(keys[k], c.SourceTable); 
                                        if(datafields[td.Id].Name == c["YearPartitionField"]){ // To check whether tablekey fields are given as YearPartitionField for the category.
                                      	datafields[td.Id].MustIncludeInDataCopy ='true'
                                        }
                                     }
                             }
                            copy.ROWUID = ccTicks();
                            copy.CC_ParcelId = targetParcelId;
                            if ( options.type == 'futureYear' ){
                                if ( options.mode != 1 ){
                                    copy.CC_LinkedROWUID = e.ROWUID;
                                    copy.CC_YearStatus = 'F'
                                }
                                else {
                                    copy.CC_LinkedROWUID = null;
                                    copy.CC_YearStatus = 'A'
                                }
                                if ( e[c["YearPartitionField"]] )
                                    copy[c["YearPartitionField"]] = (options.mode != 1 ? FYvalue : clientSettings.CurrentYearValue );
                                if ( c.SourceTable == 'parcel' ) copy.Id = e.Id;
                                var FYIDefaultValFields = Object.keys( datafields ).map( function ( x ) { return datafields[x] } ).filter( function ( x ) { return x.SourceTable == c.SourceTable && x.DefaultValue && x.DefaultValue.toUpperCase() == "FUTUREYEARSTATUS"  } ).map( function ( a ) { return a.Name } )//if DefaultValue == "FutureYearStatus" then value must be 'F' for copied future year records
                                FYIDefaultValFields.forEach(function (FyiDVFName){
									if(options.mode != 1)
									    copy[FyiDVFName] = 'F';	
                                    else
                                    	copy[FyiDVFName] = 'A';
                                });
                            }
                            else if ( options.type == 'Imprv' && showFutureData ) {
                                copy.CC_YearStatus = 'F';
                                if ( e[c["YearPartitionField"]] )
                                    copy[c["YearPartitionField"]] = FYvalue
                                copyImprvToFuture = true;
                            }
                            if ( c.Id == 0 ){
                                copy.Keyvalue1 = e.KeyValue1; copy.Keyvalue2 = e.KeyValue2;
                                copy.StreetAddress = e.StreetAddress;
                            }
                            if (npe) { copy.ParentROWUID = npe.ROWUID };
                            if ( target && c.Id > 0 ){
                                target[c.SourceTable].push(copy);
                                target.Original[c.SourceTable].push(copy);
                            }
                            
                            var mapToSourceTableFields = MapToSourceTableFieldProperties.filter(function (x) { return x.fieldSourceTable == c.SourceTable }); // handle maptosourceTable condition .
                            var mapToSourceTableFieldsCopy =_.clone(mapToSourceTableFields);
							mapToSourceTableFields = mapToSourceTableFields.reverse();
							
                            var updateSqlCopyData = function(){
                            	var sqlInsert1 = "INSERT INTO " + c.SourceTable + " (";
                                var sqlInsert2 = ") VALUES (";
                                var sqlUpdate = "Update  " + c.SourceTable + " SET ";
                                var cnt = 0,uCnt=0;
                                for (key in copy) {
                                    if ( cnt > 0 ) { sqlInsert1 += ", "; sqlInsert2 += ", ";  }
                                    if ( isCopiedRecord && key != 'ROWUID' && key != 'ParentROWUID' && key != 'CC_YearStatus' && key != 'ClientROWUID' && key != 'ClientParentROWUID' )
                                    {
                                        if ( uCnt > 0 ) sqlUpdate += ", ";
                                        sqlUpdate += "[" + key + "] = " + ( copy[key] ? ( "'" + copy[key].toString().replace( /'/g, "''" ) + "'" ) : "null" );
                                        uCnt++;
                                    }

                                    sqlInsert1 += "[" + key + "]";
                                    if ( copy[key] == null && options.type != 'futureYear' ){
                                        var value='null'
                                        var f = getDataField( key, c.SourceTable )
                                         if ( f && f.DefaultValue && ( options.type == 'template' || f.MustIncludeInDataCopy ) ){
                                           value = getDefaultValue( f, f.DefaultValue )
                                           var t = value.toString().split('.')
											var l = t.length;
											t = t[l - 1]
											if (value.toString().contains('parent.') && options && options.targetParentElement) {
												value = options.targetParentElement[t];
											} else if ((value.toString().contains('parent.') && options && !options.targetParentElement) || value.toString().contains('parcel.')) {
												value = activeParcel[t];
											}
                                           defaultValueList.push( { fieldName: f.Name, fieldId: f.Id, value: value, clientROWUID: copy.ROWUID } )
                                           if(value !== null) value = "'" + value.replace( /'/g, "''" ) + "'"
                                       }
                                        sqlInsert2 += value;
                                    }
                                    else {
                                    	var keys = tableKeys.filter(function(t){ return t.SourceTable == c.SourceTable}).map(function(tk){ return tk.Name});          
                                        for(k in keys) {
                                            var td = getDataField(keys[k], c.SourceTable);
                                            if(td !== null){
                                              var mapFieldExits = mapToSourceTableFieldsCopy.filter(function(mcopy){return mcopy.Id == td.Id});
                                              if(datafields[td.Id].MustIncludeInDataCopy != "true" && ( !datafields[td.Id].DefaultValue || datafields[td.Id].DefaultValue.toUpperCase() != "FUTUREYEARSTATUS" ) && mapFieldExits.length == 0)
                                              {
                                                    copy[keys[k]] = null;
                                              }
                                            }
                                        }
                                        if (copy[key] && copy[key].toString().search("'") > -1) copy[key] = copy[key].replace(/'/g, "''");
                                        sqlInsert2 += copy[key] ? "'" + copy[key] + "'" : 'null';
                                    }
                                    cnt++;
                                };
                                var sql = '';
                                if ( !isCopiedRecord ) sql = sqlInsert1 + sqlInsert2 + ')';
                                //else
                                //{
                                //    if ( options.mode == 1 )
                                //        sql = sqlUpdate + ' WHERE ROWUID = ' + e.CC_LinkedROWUID;
                                //    else
                                //        sql = sqlUpdate + ' WHERE CC_LinkedROWUID = ' + e.ROWUID;
                                //}
                                getData(sql, [], function () {
                                    DC.autoInsertDetails.push({ category: c, Rowuid: copy.ROWUID, parentRowuid: copy.ParentROWUID, CC_YearStatus: copy.CC_YearStatus });
                                    //autoInsert_function(c, copy.ROWUID, function() {
                                    var newDataSpec = copy.ROWUID + '$';
                                    if (npe) newDataSpec += npe.ROWUID;
                                    var sourceSpec = sourceParcelId + '$' + e.ROWUID;
                                    if ( options.type == 'template' ){                                      
										if(options.targetCategory.split('$')[0] != c.Id){
											if(templateChildInsert.filter((x) => { return x.parentRowuid == copy.ParentROWUID }).length > 0) templateChildInsert.filter((x) => { return x.parentRowuid == copy.ParentROWUID }).map((x) => { x.pciTemplateROWUIDs +=e.ROWUID+"^"+copy.ROWUID + '$'; x.clientROWUIDs += copy.ROWUID + '$' });
											else templateChildInsert.push(({ category: c, Rowuid: copy.ROWUID, parentRowuid: copy.ParentROWUID, pciTemplateROWUIDs : e.ROWUID+"^"+copy.ROWUID + '$' ,filterParams: options.targetCategory.split('$')[1]+ '$' + 'CC_parcelid=-99 AND ROWUID = '+e.ParentROWUID, clientROWUIDs : copy.ROWUID + '$'}))
										}else{
											clientROWUIDs += copy.ROWUID + '$';
											pciTemplateROWIIDs += e.ROWUID+"^"+copy.ROWUID + '$'
										}
									}else
										clientROWUIDs += copy.ROWUID + '$';
                                    if ( options.type != 'template' ){
                                        var action = ( options.type == 'futureYear' ? ( options.mode == 2 ? 'FCPA' : 'FCP' ) : 'ICP' );
                                        ccma.Sync.enqueueParcelChange( targetParcelId, newDataSpec, '', action.toLowerCase(), action, action, sourceSpec, c.SourceTable + (copyImprvToFuture ? '$F' : ''), {}, function () {
                                        	if((options.type == 'futureYear') && mapToSourceTableFieldsCopy.length > 0){
                                        		mapToSourceTableFieldsCopy.forEach(function (mapFieldCopy){
                                           			ccma.Sync.enqueueParcelChange(targetParcelId, copy.ROWUID, copy.ParentROWUID, 'E', mapFieldCopy.fieldName, mapFieldCopy.Id, null, copy[mapFieldCopy.fieldName], { source: mapFieldCopy.fieldSourceTable, sourceField: mapFieldCopy.fieldName, updateData: true });
                                           		});
                                        	}                                     
                                            counter += 1;
                                            var progress = 20 + ( counter / totalItems ) * 70;
                                            if(!options.type == 'BPPCopy')
                                        	    DC.updateProgress( 'Copying in progress', progress );
                                            if (callback) callback(copy);
                                        });
                                    }
                                    else if (callback) callback(copy);
                                //});
                                })
                            }
                           
                            var mapToSourceTableCal = function (mapToSourceTableFieldProperty){ //function to update maptosourceTable field with new rowuid of mapped record
                            	if(mapToSourceTableFieldProperty.length == 0){
                                    updateSqlCopyData();
                                    return false;
                            	}
                            	else{
                                    var mapField = mapToSourceTableFieldProperty.pop();
                                    var maptoSourceTableVal = JSON.parse(mapField.Value), mapFieldName = mapField.fieldName, mapFieldVal = e[mapFieldName], sTable, siblRecord, siblRowuid, newRec = false;
                            	    if(mapFieldVal){
                            	    	if(mapFieldVal.split("{").length > 1){
                            		   		mapFieldVal = mapFieldVal.split('{')[1].split('}')[0];
                            		    	newRec = true;
                            			}
                                    	for(var i in maptoSourceTableVal ){
                                        	var keyFieldCon = maptoSourceTableVal[i].KeyFieldConnectToMapToSourceTableParent;
                                        	if(newRec) 
                                    	    	siblRecord = sourceActParcel[maptoSourceTableVal[i].MapToSourceTableName].filter(function(actRec){ return (actRec.ROWUID == mapFieldVal)})[0]; //new Record then only check rowuid 
                                    		else if( e['CC_RecordStatus'] != 'I' ) 
                                    	    	siblRecord = sourceActParcel[maptoSourceTableVal[i].MapToSourceTableName].filter(function(actRec){ return (actRec[mapFieldName] == mapFieldVal) && (actRec[keyFieldCon] == e[keyFieldCon]) })[0]; //record existing then check improvement_id
                                    		else 
                                    			siblRecord = sourceActParcel[maptoSourceTableVal[i].MapToSourceTableName].filter(function(actRec){ return ( (actRec[mapFieldName] == mapFieldVal) || (actRec.ROWUID == mapFieldVal) ) && (actRec[keyFieldCon] == e[keyFieldCon]) })[0]; //if new synced to qc and redownload outbuilding {rowuid} changed to rowuid 
                                    		
                                    		if(siblRecord){
                                    	    	siblRowuid = siblRecord.ROWUID;
                                    			sTable = maptoSourceTableVal[i].MapToSourceTableName;
                                    			break;
                                    		}
                                    	}
                            	    	if(sTable && siblRowuid){
                                        	getData("SELECT ROWUID FROM " + sTable + " WHERE CC_LinkedROWUID = "+ siblRowuid +" ", [], function (newSiblROWUID) {
                                            	if(newSiblROWUID[0])
                                            		copy[mapFieldName] = "&%" + sTable + "{" + newSiblROWUID[0].ROWUID + "}&%";
                                            	mapToSourceTableCal(mapToSourceTableFieldProperty);
                                        	});
                            	    	}
                            	    	else
                                        	mapToSourceTableCal(mapToSourceTableFieldProperty);
                                    }
                                    else
                                    	mapToSourceTableCal(mapToSourceTableFieldProperty);
                            	}    
                            }
                            mapToSourceTableCal(mapToSourceTableFields);
                        }, function () {
                        if(!options.type == 'BPPCopy')
                            DC.updateProgress('Finalizing data', 85);
                            executeQueries("UPDATE Parcel SET Reviewed = 'false' WHERE Id = " + targetParcelId)
                            let qrt = EnableNewPriorities == 'True' ? ("UPDATE Parcel SET CC_Priority = 3 WHERE CC_Priority = 0 AND Id = " + targetParcelId) : ("UPDATE Parcel SET CC_Priority = 1 WHERE CC_Priority = 0 AND Id = " + targetParcelId);
                            executeQueries(qrt);
                            if (options.type == 'futureYear')
                                executeQueries("UPDATE Parcel SET HasFutureYear = 'true' WHERE Id = " + targetParcelId)
                           	if (options.type == 'template'){
                            		if(options.selectSpecifier){
                            			clientROWUIDs += options.selectSpecifier+'$'
                            			pciTemplateROWIIDs += options.selectSpecifier+'$'
                            		}
                            		if ( fylEnabled && _c["YearPartitionField"] && showFutureData && activeParcel.HasFutureYear == 'true'){   //to identify template copy for future year 
                            			options.filterParameters = options.filterParameters + '$F'
                            		}
                            		ccma.Sync.enqueueParcelChange(targetParcelId, pciTemplateROWIIDs !='' ? pciTemplateROWIIDs.slice(0, -1) : clientROWUIDs.slice(0, -1), '', 'tcp', 'TCP', 'TCP', options.filterParameters, _c.SourceTable + '$' + options.targetParentElement.ROWUID);
                                	templateChildInsert.forEach((item)=>{
										ccma.Sync.enqueueParcelChange(targetParcelId, item.pciTemplateROWUIDs !='' ? item.pciTemplateROWUIDs.slice(0, -1) : item.clientROWUIDs.slice(0, -1), '', 'tcp', 'TCP', 'TCP', item.filterParams, item.category.SourceTable + '$' + item.parentRowuid);
									});
                                }
                                defaultValueList.forEach(function (obj){
                                    ccma.Sync.enqueueParcelChange(targetParcelId, obj.clientROWUID, '', null, obj.fieldName, obj.fieldId, null, obj.value)
                                })
                			autoInsert_function(0, function(){
                            	getParcel( activeParcel.Id, function (){
                            		if(totalItems != 0 && !_.contains(copiedCategories, copyObjectName))
                            			copiedCategories.push(copyObjectName);
                            	    if(mcis_callback && !template_Call) { 
                            	    	mcis_callback(); 
                            	    	return 
                            	    } else if(listLength_callback &&  (copiedCategories.length + emptyCopyCategories.length) != catIdList.length) {
	                            	    listLength_callback(); 
	                            	    return;
                            	    }
                            	    mcis_templatecpy = false;
                                    console.log('Copy completed')
                                    if(!options.type == 'BPPCopy')
                                	DC.updateProgress( 'Copy completed', 100 );
                                	$('.mask').hide();
                                	$('#maskLayer').hide();
                                	$('#loadText').html('Loading...');
                                	var finalText = "";
                                	for(categories in copiedCategories) {
                                		finalText += copiedCategories[categories] += ', ';
                                	}
                                	if(copiedCategories.length > 0) 
                                	    finalText = finalText.slice(0,finalText.length-2) + ' copied successfully.';
                                	if(emptyCopyCategories.length > 0) 
                                	    finalText += ' There are no items to copy from '
                                	for(categories in emptyCopyCategories) {
                                		finalText += emptyCopyCategories[categories] + ', ';
                                	}
									if(emptyCopyCategories.length > 0) 
										finalText = finalText.slice(0,finalText.length-2) + ' parcel(s)';                                	
                                    messageBox(options.type == 'BPPCopy' ? 'New Business Personal Property ' + options.pID + ' created successfully' : (options.type == 'futureYear' ? (HasFYear == 'true' ? (showFutureData == true ? 'Future Year data copied to Current Year successfully.' : 'Future Year data copied successfully.') : 'Future Year data copied successfully.') : finalText), function () {
                                        DC.endProcess();
                                        if(!(options.type == 'BPPCopy'))
                                        	refreshAuxSubLinks(ccma.UI.ActiveFormTabId);
                                        if(onCompletion) onCompletion();
                                    });
                                });
                            });
                        }, true, options,mcis_callback);
                   }, null, options.type == 'futureYear');
                 //   });

                    //Real copy ends
                },true,options);
            
        }, null,  options.type == 'futureYear' );
    }


    this.CopyTemplateData = function (targetParcelId, categoryId, templateCategoryId, filterFields,options,mcis_callback, mcisFlag) {
        var templateCat = _.findWhere(ccma.Data.FieldCategories, { Id: parseInt(templateCategoryId).toString() });
        var templateName = templateCat.Name;
        var templateNameLower = templateCat.Name.toLowerCase();
        var replaceGroup = templateCat.SourceTable;
        getParcelWithAllData(-99, function (template) {
            if (template.length == 0) {
                messageBox('Template Data Parcel is not defined. Unable to proceed with Default Template Copy.')
                return false;
            }
            template_Call = false;
            var filter = filterFields.split(",");
            var rootCat = _.findWhere(ccma.Data.FieldCategories, { Id: categoryId.toString() });
            var sectionControl = $('section[catid="' + rootCat.Id + '"]');
            var sourceTable = (sectionControl) ? $(sectionControl).attr('auxdata') : null;
            var findex = (sectionControl) ? $(sectionControl).attr('findex') : null;

            var targetParent = activeParcel[sourceTable][findex];
            var sourceParentTable = template[sourceTable];

            if ((sourceParentTable == null) || (sourceParentTable.length == 0)) {
                messageBox('You are trying to copy from an invalid source - bad configurations.')
                return false;
            }

            if ((filter == null) || (filter.length == 0)) {
                messageBox('Default Template parent filters are not defined.')
                return false;
            }

            var templateFilter = _.pick(targetParent, filter)
            if(!options) options = {};
            Object.keys(options).forEach(function(x){
				    if(Object.keys(templateFilter).includes(x) && templateFilter[x] == options[x]){
				        template_Call = true;
				    }
				});
           
            
            if(!mcisFlag){
	            if(!options) options = {};
		        Object.keys(options).forEach(function(x){
				    if(Object.keys(templateFilter).includes(x) && templateFilter[x] != options[x]){
				        templateFilter[x] = options[x];
				    }
				});
			}
            var sourceParents = _.where(sourceParentTable, templateFilter);		            
           if (sourceParents.length == 0) {
            	if(mcis_callback && !template_Call) { 
            	  cpyfirsttemplate_fail = true;
            	  mcis_callback(); 
                  return 
            	}
            	else if(mcis_templatecpy && (!template_Call && !cpyfirsttemplate_fail)){  // checks first mcis template success/fail and is not template call
                       messageBox('' + templateCat.Name + ' copied successfully.')
                       refreshAuxSubLinks(ccma.UI.ActiveFormTabId);
                       mcis_templatecpy = false;
                       $('#maskLayer').hide();
                       return false;
            	       }
            	else{
            	    mcis_templatecpy = false;
            	    cpyfirsttemplate_fail = false;
                    messageBox('No default ' + templateNameLower + ' template is available for the configuration selected.')
                    $('#maskLayer').hide();
                    return false;
                }  
            }
            filterParameters = sourceTable + '$' + 'CC_parcelid=-99'
           for (key in templateFilter) { 
                if(templateFilter[key] !== null)
                    filterParameters += ' AND ' + key + "='" + templateFilter[key] + "'" 
                else
                    filterParameters += ' AND ' + key + " IS NULL" 
            }
            var sourceParent = sourceParents[0];
            var copyOptions = {
                parentCategory: rootCat,
                parentElement: sourceParent,
                targetParentElement: targetParent,
                filterParameters: filterParameters,
                type: 'template',
                targetCategory : templateCat.Id+'$'+ templateCat.SourceTable
            }
			if(!mcisFlag){
	            var promptMessage = "You are about to delete all existing " + templateNameLower + " and insert the default " + templateNameLower + " as configured in the respective template";
	            messageBox(promptMessage, ["Yes", "No"], function () {
	            	showMask('Copying...');
	                DC.DeleteAuxRecordWithDescendants(rootCat.Id, targetParent.ROWUID, true, replaceGroup, function () {
	                    DC.CopyFormData( -99, targetParcelId, [templateCat], null, copyOptions,mcis_callback);
	                }, null, true);
	            })
	        } else {
	        	if(options){
	        		copyOptions.selectSpecifier = 'first';
	        	}
	        	if(cpyfirsttemplate_fail){
	        	      var promptMessage = "You are about to delete all existing " + templateNameLower + " and insert the default " + templateNameLower + " as configured in the respective template";
	                  messageBox(promptMessage, ["Yes", "No"], function () {
	            	  showMask('Copying...');
	                  DC.DeleteAuxRecordWithDescendants(rootCat.Id, targetParent.ROWUID, true, replaceGroup, function () {
	                    DC.CopyFormData( -99, targetParcelId, [templateCat], null, copyOptions);
	                  }, null, true);
	                 })
	                 cpyfirsttemplate_fail = false;
	             }
	             else{
	             	  DC.CopyFormData( -99, targetParcelId, [templateCat], null, copyOptions);
	             }     
	        }
        });
    }

    this.DeleteAuxRecordWithDescendants = function (fcid, rowuid, doNotDeleteSelf, descendentFilter, callback, reloadAndCallback, deleteWithoutPCIs, forceDelete) {
        var deleteList = [];
        forceDelete = (forceDelete == null || forceDelete == undefined)? false: forceDelete;
        fcid = parseInt(fcid);
        rowuid = parseInt(rowuid);
        var fc = _.findWhere(ccma.Data.FieldCategories, { Id: fcid.toString() })
        var rootTableName = fc.SourceTable;

        function deleteChildAuxRecords(fcid, rowuid, pe, forceDelete, callback, isChild) {
            var fc = _.findWhere(ccma.Data.FieldCategories, { Id: fcid.toString() })
            var tableName = fc.SourceTable;

            function recursive(list, onItem, onFinish) {
                if (list.length == 0) {
                    if (onFinish) onFinish();
                } else {
                    var item = list.shift();
                    onItem(item, function () {
                        recursive(list, onItem, onFinish);
                    });
                }
            }

            function objectDeleteMethod(fc, e, forceDelete, afterDelete) {
                if (forceDelete || isChild || (isTrue(fc.AllowAddDelete) && (fc.SourceTable != '')) || (isTrue(fc.AllowDeleteOnly) && (fc.SourceTable != ''))) {
                    var deleteSql = 'DELETE FROM ' + fc.SourceTable + ' WHERE ROWUID = ' + e.ROWUID;
                    deleteList.unshift({ table: tableName, ROWUID: e.ROWUID, sql: deleteSql });
                    if (clientSettings["updatePhotoMetadata"]) {
                        var clearMetadataParameter = JSON.parse(clientSettings["updatePhotoMetadata"])
                        var metdataFieldToClear = clearMetadataParameter.metdataFieldToClear;
                        var metdataValueField = clearMetadataParameter.metdataValueField;
                        var keyarray = clearMetadataParameter.TableKeys.filter(function (c) { return c.TableName == fc.SourceTable });
                        if (keyarray.length > 0) {
                            var keyvalue = keyarray ? keyarray[0].Value : "";
                            todoDB.indexedDB.update('Images', [{name: metdataFieldToClear, value: null}], ['ParcelId', metdataValueField, metdataFieldToClear], ['=', '=', '%like'], [parseInt(activeParcel.Id), keyvalue, '$' + e.ROWUID], ['AND', 'AND'], function () {
                            //getData('UPDATE IMAGES SET ' + metdataFieldToClear + '= NULL WHERE ' + metdataFieldToClear + ' like "%$' + e.ROWUID + '" AND parcelID=? AND ' + metdataValueField + '=?', [activeParcel.Id, keyvalue], function () {
                                ccma.Sync.enqueueCommand(keyvalue + '$' + e.ROWUID + '$' + activeParcel.Id, 'PhotoMetaData', metdataFieldToClear, '', { updateData: false }, function () {
                                });
                            });
                        }
                    }
                }
                if (afterDelete) afterDelete();
            }

            if (objectDeleteMethod) {
                var elements = activeParcel[tableName];
                if (rowuid) {
                    elements = _.where(elements, { ROWUID: rowuid.toString() })
                } else {
                    if ((fc.FilterFields == '') || (fc.FilterFields == '')) {
                        if (callback) callback(deleteList);
                        return;
                    }
                    var ff = fc.FilterFields.split(',');
                    elements = elements && elements.length? elements: [];
                    elements = elements.filter(function (x) {
                        return ff.map(function (f) {
                            var cf, pf; pf = cf = f;
                            if (cf.indexOf('/') > -1) { cf = f.split('/')[1]; pf = f.split('/')[0]; }
                            return ((x[cf] != null) && (x['CC_RecordStatus'] != 'I') && (x[cf] == pe[pf])) || ((x[cf] == null || (x['CC_RecordStatus'] == 'I')) && (x.ParentROWUID == pe.ROWUID))     // keyfields updated in copying if mustincludeincopy given, if more than one time copyied same record then that 2 records having same value, so filter not working correctly. Now checking CC_recordstatus to identify new record, if new record then we can directly use rowuid and parentrowuid ((x[cf] != null) && (x[cf] == pe[pf])) || ((x[cf] == null) && (x.ParentROWUID == pe.ROWUID))
                        }).reduce(function (x, y) { return x && y })
                    });
                }

                recursive(elements, function (e, getNextElement) {
                    objectDeleteMethod(fc, e, forceDelete, function (innercallback) {
                        var scats = ccma.Data.FieldCategories.filter( function ( f ) { return ( f.OverrideParentCategory ? f.OverrideParentCategory == fc.Id.toString() : f.ParentCategoryId == fc.Id.toString() ) } )
                       // getData( 'SELECT * FROM FieldCategory WHERE ParentCategoryId = ' + fc.Id, [], function ( scats ){
                            recursive(scats, function (i, getNextCategory) {
                                deleteChildAuxRecords(i.Id, null, e, forceDelete, getNextCategory, true);
                            }, getNextElement);
                      //  });
                    });
                }, function () {
                    if (callback) callback(deleteList);
                });
            }
        }

        deleteChildAuxRecords(fcid, rowuid, null, forceDelete, function (deleteList) {
            if (doNotDeleteSelf) {
                deleteList = _.reject(deleteList, function (x) { return ((x.table == rootTableName) && (x.ROWUID == rowuid)) });
            }
            if (descendentFilter != null && descendentFilter != '*') {
                deleteList = _.reject(deleteList, function (x) { return ((x.table != descendentFilter)) });
            }
            var deleteSqls = deleteList.map(function (x) { return x.sql });
            for (var x in deleteList) {
                var d = deleteList[x];
                activeParcel[d.table] = _.reject(activeParcel[d.table], function (x) { return d.ROWUID == x.ROWUID });
            }
            var parcelId = activeParcel.Id;
            executeQueries(deleteSqls, function () {
                if (!deleteWithoutPCIs) {
                    ccma.Sync.enqueueParcelChange(parcelId, rowuid, null, (doNotDeleteSelf ? 'deletechild' : 'delete'), null, (doNotDeleteSelf ? 'DELETECHILD' : 'DELETE'), descendentFilter, rootTableName, null, function () {
                        executeQueries("UPDATE Parcel SET Reviewed = 'false' WHERE Id = " + parcelId)
                        executeQueries("UPDATE Parcel SET CC_Priority = 3 WHERE CC_Priority = 0 AND Id = " + parcelId)
                        if (callback && !reloadAndCallback)
                            callback();
                        else {
                            getParcel(parcelId, function () {
                                reloadAuxiliaryRecords(fcid);
                                if (callback) callback();
                            });
                        }
                    });
                } else {
                    if (callback && !reloadAndCallback)
                        callback();
                    else {
                        getParcel(parcelId, function () {
                            reloadAuxiliaryRecords(fcid);
                            if (callback) callback();
                        });
                    }
                }
            });
        });
    }
    var DC = this;
}