﻿
var currentLookup = [];
var currentText = '';
var heatMapitems = [];
var extendedLookup = []
var _currentLookupAll = [];
function getLookupData(field, options, callback, skipError) {
    if (field.IsLargeLookup == 'true' || field.IsLargeLookup == true) {
        if (callback) callback([]);
        return;
    }
    var data = [];
    var cat = options.category;
    var specialType = false;
    if (options.dataSource == 'ccma.YesNo') {
        if (field.RadioFieldValues && field.RadioFieldValues != "") {
            var values = []; values = field.RadioFieldValues.trim().split(',');
            var t = [{ Name: 'Yes', Id: values[0] }, { Name: 'No', Id: values[1] }];
            if (callback) callback(t);
        }
        else
            if (callback) callback(ccma.YesNo);
    } else {
        if (field.LookupTable == '$QUERY$') {
            var query = field.LookupQuery || '';
            if (lookup['#FIELD-' + field.Id] == undefined) {
                var fullQuery = query || '';
				if (query.search(/\{[a-zA-Z0-9_.]*\}/i) > 0){
                   if (query.search(/where/i) > 0){
						if(query.search(/union/i) > 0){
							var unionSplit = query.split(/union/i)
							unionSplit.forEach(function(each,index){ if( unionSplit[index].search(/where/i) > 0 ) unionSplit[index] = unionSplit[index].substr(0, unionSplit[index].search(/where/i))});
							fullQuery = unionSplit.join('union');
						}
						else
							fullQuery = query.substr(0, query.search(/where/i));
                   }
               }
                if (fullQuery == '') {
                    messageBox('No custom lookup query given for the field: ' + field.Name);
                    if (callback) callback([]);
                    return [];
                }
                if ( fullQuery.search( /\{.*?\}/g ) == -1 || (field.LookupQueryForCache && field.LookupQueryForCache != '')){
                    if ( field.LookupQueryFilter )  extendedLookup['#FIELD-' + field.Id] = []
                    if(field.LookupQueryForCache && field.LookupQueryForCache != '') fullQuery = field.LookupQueryForCache;
                    getData(fullQuery, [], function (ld) {
                        if (ld.length > 0) {
                            var f = field.Name;
                            lookup[f] = {};
                            var props = Object.keys(ld[0]);
                            var IdField = props[0]; var NameField = props[1]; var DescField = props[2]
                            if (!NameField) NameField = IdField;
                            if (!DescField) DescField = NameField;
                            ld = convertToCommaSeparatedDropDown(ld, field, IdField);
                            for (var x in ld) {
                                var d = ld[x];
                                var v = d[IdField];
                                if(v && typeof(v) == "string") v = v.replace('&gt;','>').replace('&lt;','<');
                                if (lookup[f][v] === undefined)
                                    lookup[f][v] = { Name: d[NameField], Description: d[DescField], Ordinal: x, SortType: null, Id: v, Object: d };
                                if ( field.LookupQueryFilter ){
                                    d.Ordinal = x;
                                    d.Name = d[NameField];
                                    d.Description = d[DescField];
                                    d.Id = v;
                                    extendedLookup['#FIELD-' + field.Id].push( d )
                                }
                            }

                            lookup['#FIELD-' + field.Id] = lookup[f];
                        }

                    }, null, skipError, function() { if (skipError && callback) callback([]); });
                } else specialType = true;
            }
            var o;
            if (cat.TabularData == "true") {

            } else {
                o = activeParcel;
            }
            if (options.source)
                o = options.source;
            if (o) {
                if (query.search(/\{.*?\}/g) > -1) {
                    query.match(/\{.*?\}/g).forEach(function (fn) {
                        var testFieldName = fn.replace('{', '').replace('}', '');
                        var source, value, lookupField;
                        if (testFieldName.contains('parent.')) {
                            var fieldName = testFieldName.split('parent.');
                            lookupField = fieldName[fieldName.length - 1];
                            source = o;
                            while (testFieldName.search('parent.') > -1) {
                                source = source["parentRecord"] ? (source["parentRecord"].constructor == Array ? source["parentRecord"][0] : source["parentRecord"]) : [];
                                testFieldName = testFieldName.replace('parent.' + lookupField, lookupField);
                            }
                            //source = o["parentRecord"] ? o["parentRecord"] : [];
                            //lookupField = testFieldName.split('.')[1];
                            //if (testFieldName.contains('parent.parent')) {
                            //    source = source["parentRecord"] ? source["parentRecord"] : [];
                            //    lookupField = testFieldName.split('.')[2];
                            //}
                        }
                        else if (testFieldName.contains('parcel.')) {
                            source = activeParcel;
                            lookupField = testFieldName.split('parcel.')[1];
                        }
                        else { lookupField = testFieldName.split('.')[0]; source = o };
                        value = (testFieldName == 'CURRENT_USER')? localStorage.getItem('username'): source[lookupField];
                        if(value && typeof(value) == "string") value = value.replace(/'/g,"''");
                        if (!value && value != "0" && value != '') value = 'null';
                        if (('Image' in source) && ('LocalId' in source) && ('Path' in source) && ('MetaData1' in source)) {
                            var imgfield = getDataField(lookupField, '_photo_meta_data');
                            var valueField = '';
                            if (imgfield) {
                                //                                valueField = imgfield.AssignedName.replace('PhotoMetaField', 'MetaData');
                                //                                value = o[valueField];
                                //                                if ((value == null || value === undefined || value == '') && imgfield.DefaultValue) value = imgfield.DefaultValue;
                                valueField = imgfield.AssignedName.replace('PhotoMetaField', 'MetaData');
                                value = source[valueField];
                            }

                            //                            if (typeof value == "number" || !isNaN(value)) {
                            //                                value = parseFloat(value).toString();
                            //                            }

                            if (testFieldName.split('.') > 0) {
                                var attrName = testFieldName.split('.')[1];
                                var lk = getFieldLookup(lookupField);
                                if (lk) {
                                    if (lk[value]) {
                                        if (lk[value].Object) {
                                            value = lk[value].Object[attrName];
                                        }
                                    }
                                }
                            }

                        }
	
                        query = query.replace(fn, value);

                    });
                }
            }
            if (query.search(/\{.*?\}/g) == -1) {
                getData(query, [], function (ld) {
                    if (ld.length > 0) {
                        var f = field.Name;
                        var props = Object.keys(ld[0]);
                        var IdField = props[0]; var NameField = props[1]; var DescField = props[2]
                        if (!NameField) NameField = IdField;
                        if (!DescField) DescField = NameField; 
                        if (specialType) lookup[f] = {};
                        ld = convertToCommaSeparatedDropDown(ld, field, IdField);
                        for (var x in ld) {
                            var d = ld[x];
                            var name = d[NameField];
                            if (name) name = name.toString().trim();
                            if(name ==='null') { name = ''; d[NameField]= ''; }
                            var v = d[IdField];
                            //if (v && v.trim) {
                            //    v = v.trim();
                            //}
                            if (!name) name = v;
                            //                            if (typeof v == "number" || !isNaN(v)) {
                            //                                v = parseFloat(v).toString();
                            //                            }
                            data.push({ Name: name, Description: d[DescField], Ordinal: x, SortType: null, Id: v });
                            if (specialType) {
                                if (lookup[f][v] === undefined)
                                    lookup[f][v] = { Name: d[NameField], Description: d[DescField], Ordinal: x, SortType: null, Id: v, Object: d };
                            }
                        }
                    }
                    if (options.addNoValue) {
                        data.unshift({ Name: 'NO VALUE', Id: '' });
                    } else if (isTrue(field.LookupShowNullValue)) {
                        data.unshift({ Name: '', Value: '<blank>' });
                    } else {
                        data.unshift({ Name: '', Id: '' });
                    }
                    if (callback) callback(data, options.source, options);

                }, null, skipError, function() { if (skipError && callback) callback([]); });
            } else {
                if (callback) callback([]);
            }
        } else {
            var l = lookup[field.LookupTable]

            for (x in l) {
                var Idfield = x;
                //if (field.IsClassCalculatorAttribute == 'true')
                   // Idfield = l[x].NumericValue || 0
                data.push({
                    Id: Idfield,
                    Name: l[x].Name,
                    Description: l[x].Description,
                    Ordinal: l[x].Ordinal,
                    SortType: (l[x].SortType && !isNaN(l[x].SortType)) ? parseInt(l[x].SortType) : null
                })
            }
            if (options.addNoValue) {
                data.unshift({ Name: 'NO VALUE', Id: '' });
            }
            if (isTrue(field.LookupShowNullValue)) {
                data.unshift({ Name: '', Value: '<blank>' });
            }
            if (callback) callback(data.sort(function (x, y) { return x.Ordinal ? x.Ordinal - y.Ordinal : x.Id > y.Id ? 1 : -1 }), options.source, options);
        }
    }
}

function getFieldLookup(fieldName, fieldId, options) {
    var selector = fieldName;
    if (fieldId) 
        selector = '#FIELD-' + fieldId;

    var lk = ccma.Data.LookupMap[selector];

    if (lk == undefined) {
        return null;
    } else if (lk.LookupTable == "$QUERY$") {
        var lkd; 
        if (!lookup[selector]) {
            lkd = lookup[fieldName];
        } else {
            lkd = lookup[selector];
        }

        if ( options && options.referenceObject && options.referenceFilter && extendedLookup[selector].length > 0 ) {
            var source = options.referenceObject
            var expression = options.referenceFilter
            expression.match( /\{.*?\}/g ).forEach(function ( fn ) {
                var testFieldName = fn.replace( '{', '' ).replace( '}', '' );
                var tempSource = source;
                while ( testFieldName.indexOf( 'parent.' ) > -1 ) {
                    tempSource = source['parentRecord'];
                    testFieldName = testFieldName.replace( 'parent.', '' );
                }
                if (tempSource) {
	                var value = tempSource[testFieldName]
	                expression = expression.replace( fn, value );
                }
            })
            for ( x in extendedLookup[selector][0] ) { var regex = new RegExp( x.replace(/\|/g, '\\|'), "g" ); expression = expression.replace( regex, 'thisObject.' + x ) };
            while(expression.indexOf('thisObject.thisObject.') > -1){
			   expression = expression.replaceAll('thisObject.thisObject.','thisObject.')
			}
            try{
                lkd = eval( 'extendedLookup["' + selector + '"].filter(function (thisObject) { return (' + expression + ') })' );
            }
            catch ( ex ){
                console.error( "lookup cache not loaded properly for the field:" + fieldName )
                return lkd
            }
            var obj = {};
            lkd.forEach( function ( e )
            {
                obj[e.Id] = e
            } )
            return obj;
        } else
            return lkd;
    } else {
        return lookup[lk.LookupTable];
    }
}

function getLookupArray(name, options, callback, fieldID) {
    if (!options) options = {};
    var ld = getFieldLookup(name, fieldID);
    var field = datafields[fieldID]
    var lx = [];
    for (x in ld) {
        var Idfield = x;
        lx.push({
            Id: Idfield,
            Name: ld[x].Name,
            Description: ld[x].Description,
            Ordinal: ld[x].Ordinal,
            SortType: (ld[x].SortType && !isNaN(ld[x].SortType)) ? parseInt(ld[x].SortType) : null
        })
    }
    if (options.addNoValue) 
        lx.unshift({ Name: 'NO VALUE', Id: '' });
    return lx.sort(function (x, y) { return x.Ordinal ? x.Ordinal - y.Ordinal : x.Id > y.Id ? 1 : -1 });
}

function openCustomddlList(source, newCustomSpan) {
    isLookupScreen = true;
    _currentLookupAll = [];
    //$('#data-collection').hide();
    var lookupvalue;
    currentLookup = newCustomSpan ? source: $(source).siblings('.newvalue')[0];
    lookupvalue = $(currentLookup).val();
    var _fiId = $(currentLookup).attr('field-id');
    try {
        currentText = newCustomSpan? $(source).html(): $('option[value="' + lookupvalue + '"]', $(currentLookup))[0].text;
    } catch (ex1) {
        currentText = "";
    }
    $('span[selectedcustomddl]').html(currentText);
    $('.customddlbtnadd').hide();
    $('.customddl, .mask').show();
    $('.lkShowAll').show();
    //$('div[controls] input').focus();
    $('div[controls] input').val('');
    $('div[controls] input').attr('field', newCustomSpan? $(source).attr('field-name'): $(source).siblings('.newvalue').attr('field-name'));
    $('div[controls] input').attr('field-id', newCustomSpan? $(source).attr('field-id'): $(source).siblings('.newvalue').attr('field-id'));
    $('div[controls] input').attr('findex',  $(source).parent().attr('last-findex'));
    $('#loolupul').html('');
    $('.customddl span[legend]').html(newCustomSpan ? $(source).siblings('.legend').html(): $(source).siblings('.legend').html());
    //setScreenDimensions();
    setCustomddl($(window).height(), $(window).width());
    $('div[controls] input').focus();
    FillLookupList(null, _fiId, $(source).parent().attr('last-findex'));    
}

function evalLargeLookup(lookupQuery, callback, lkList, searchErrorCallback) {
    var result = [];
    var ignoreError = searchErrorCallback? true: false;
    var _lkCalaulation = function (ld) {
    	if (ld.length > 0) {
            var props = Object.keys(ld[0]);
            var IdField = props[0]; var NameField = props[1]; var DescField = props[2]
            if (!NameField) NameField = IdField;
            for (var x in ld) {
                var d = ld[x];
                var name = d[NameField];
                if (name) name = name.trim();
                var v = d[IdField];
                if (!name) name = v;
                result.push({ Name: name, Description: d[DescField], Ordinal: x, SortType: null, Id: v });
            }
        }
        if (callback) callback(result);
        else return result;
    }
    if (lkList) 
		_lkCalaulation(lkList);
	else {		
	    getData(lookupQuery, [], function (lds) {
	        _lkCalaulation(lds)
	    }, null, ignoreError, searchErrorCallback);
    }
}

function FillLookupList(source, _fiId, findx) {
    var searchWord = source? $('div[controls] input').val().trim().toLowerCase(): '';
    
    if (searchWord == '' && $('.lkShowAllRec').css('display') != 'none') 
    	return;
    else if (!_fiId && searchWord == '' && _currentLookupAll.length < 31) {
    	$('.lkShowAlllink').hide();
		$('.lkShowAllRec').show();
    }
    else {
    	$('.lkShowAlllink').show();
		$('.lkShowAllRec').hide();
    }
    
    if (!_fiId && _currentLookupAll && _currentLookupAll.length == 0) {
    	$('.lkShowAlllink').hide();
    	$('.lkShowAllRec').hide();
    }
    
    var newLookupFlag = false;
    if (SketchLookup) {
 		if(lookup[SketchLookup] == undefined && ((sketchApp.currentSketch && sketchApp.currentSketch.lookUp == undefined) || (sketchApp.currentSketch && sketchApp.currentSketch.lookUp && sketchApp.currentSketch.lookUp.length == 0)))
    		newLookupFlag=true; 
 		else {
 		    var fLookUp = ccma.Data.LookupMap[SketchLookup]
 		    var isLookUpQueryField = fLookUp ? fLookUp.LookupTable == '$QUERY$' : false;
 		    var lk = [];
        	if(lookup[SketchLookup] != undefined)
        		lk = Object.keys(lookup[SketchLookup]).map(function (x) { return lookup[SketchLookup][x] })
        	else {
        		lk = sketchApp.currentSketch.lookUp;
        		if (typeof(lk) == 'object' && lk.length == undefined) {
					lk = Object.keys(lk).map(function(x) { return lk[x] }).sort(function(a, b) { return a.Ordinal - b.Ordinal });
				}
        	}
        	if(lk)
        		result = (searchWord != '') ? lk.filter(function (d) { return ((d.Id) ? d.Id.toLowerCase().indexOf(searchWord) != -1 : d.Id) || ((d.Description) ? d.Description.toLowerCase().indexOf(searchWord) != -1 : d.Description) || ((d.Name) ? d.Name.toLowerCase().indexOf(searchWord) != -1 : d.Name); }) : [];
        	if ( result.length > 0 ) {
        	    var temp = isLookUpQueryField ? '${Name}' : '${Id} - ${Name}';
        	    if(sketchApp && sketchApp.currentSketch && sketchApp.currentSketch && sketchApp.currentSketch.config && sketchApp.currentSketch.config.EnableSwapLookupIdName)
        	    	temp = '${Name} - ${Id}';
        	    $( '#loolupul' ).html( '<li value="${Id}"  onclick="sketchLookupSelected(this);">' + temp + '</li>' );
        		$('#loolupul').fillTemplate(result);
        	}
        	else
          		newLookupFlag=true;
		}
		if(newLookupFlag == true)
		{
			$('.customddlbtnadd').hide();
			$('#loolupul').html('<li>No matches ...</li>');
		}
        $('span[selectedcustomddl]').html(currentText);
		//refreshScrollable('lookupout');
        return;
    }
    var result = [];
    var field = _fiId? datafields[_fiId] : datafields[$(source).attr('field-id')];
    var fieldProcessor = function () {
    	$('.lkShowAllStatus').html('Showing ' + result.length + ' records out of ' + _currentLookupAll.length);
        if (result.length > 0) {
            ( !isNaN( result[0].SortType ) && result[0].SortType ) ? result = ccma.Data.Evaluable.sortArray( result, eval( 'lookup_' + parseInt( result[0].SortType ) ), true ) : '';
            var temp = field.LookupQuery && field.LookupQuery != '' ? '${Name}' : '${cId} - ${Name}';
            result.forEach(function(x, index) { 
            	result[index].cId = ''; result[index].cName = typeof(x.Name) === "string" ? x.Name.replaceAll('"','&quot;') : x.Name; //donot show encodedURI Id in display part, encodedURI part append to value part
                if (x.Id || x.Id === 0) result[index].cId = x.Id;
                if (!x.Name && x.Name !== 0) result[index].cName = typeof(x.Id) === "string" ? x.Id.replaceAll('"','&quot;') : x.Id;
				if(x.Id) { result[index].Id = encodeURI(x.Id); }
				if( temp == '${Name}' && !x.Name ) result[index].Name = result[index].Id;
            });
            $( '#loolupul' ).html( '<li value="${Id}" displayText="${cName}" onclick="markLookupSelected(this);">' + temp + '</li>' );
            $('#loolupul').fillTemplate(result);
            $( 'span[selectedcustomddl]' ).html( currentText );
        	//refreshScrollable('lookupout');
        	if (newDdlPhotoProperties) {
        		var metaValues = newDdlPhotoProperties.metaValues;
        		metaValues.forEach(function(meta) {
        			if(meta && meta != '') {
        				$('li[value = "'+ meta +'"]', $('#loolupul')).attr('disable', '1');
                		$('li[value = "'+ meta +'"]', $('#loolupul')).attr('onclick', '');
                	}
            	});
        	}
        	if (!(field.LookupQuery && field.LookupQuery != '') &&  $('li[value = ""]', $('#loolupul')) && $('li[value = ""]', $('#loolupul'))[0] && $('li[value = ""]', $('#loolupul')).html() == ' - ') {
        		$('li[value = ""]', $('#loolupul')).html('');
        	}
        	if(!_fiId &&  result.length == _currentLookupAll.length) {
                $('.lkShowAlllink').hide();
		        $('.lkShowAllRec').show();
            }
        }
        else {
        	if (_fiId ) 
        		$('.lkShowAlllink').hide();
        	else {
            	$('#loolupul').html('<li>No matches ...</li>');
            	$('span[selectedcustomddl]').html(currentText);
            }
        }
    }
    
    var searchErrorHandle = function () {
    	if (_currentLookupAll && _currentLookupAll.length > 0) {
    		if(searchWord && typeof(searchWord) == "string") searchWord = searchWord.replace('&gt;', '>').replace('&lt;', '<');
			result = _currentLookupAll.filter(function (d) { return ((d.Id) ? d.Id.toLowerCase().indexOf(searchWord) != -1 : d.Id) || ((d.Description) ? d.Description.toLowerCase().indexOf(searchWord) != -1 : d.Description) || ((d.Name) ? d.Name.toLowerCase().indexOf(searchWord) != -1 : d.Name); });
			result = result? result: [];
			fieldProcessor();
		}
		else {
        	result = [];
    		fieldProcessor();
        }
    }
    
    $('.customddlbtnadd').hide();
    if (field.IsLargeLookup == 'true' || field.IsLargeLookup == true) {
        var lookupQuery = field.LookupQuery;
        var colName = [];
        if (searchWord || searchWord == "") {
			var escapeQuery='';
			if (searchWord.includes('%')) escapeQuery= " ESCAPE '\\'";
			lookupQuery = lookupQuery.replace(/;$/, "");
			var _limitString = searchWord == ''? '' : ' LIMIT 0,1';
            getData(lookupQuery + _limitString, [], function (tld) {
                if (tld.length > 0) {
                   if (searchWord == '' && !SketchLookup) {                   		 
                   		evalLargeLookup(lookupQuery, function (res) {
                   			if (!isTrue(field.DoNotAllowSelectNull)) {
		                        if (isTrue(field.LookupShowNullValue)) 
			                        res.unshift({ Name: '', Id: '' });
								else 
			                        res.unshift({ Name: '', Id: '<blank>' });
		                    }
		                    else {
		                    	if (res[0].Id == '' || res[0].Id == '<blank>')
		                    		res.shift();
			                }
	                        if (!source && _fiId) {
	                        	_currentLookupAll = res;
	                        	if (_currentLookupAll.length < 31) {
					            	$('.lkShowAlllink').hide();
					            	$('.lkShowAllRec').show();
					            }
	                        }
	                        result = res.slice(0, 30);
	                        fieldProcessor();
		                }, tld);	
                   }
                   else {
                   		if(searchWord.includes('"')){
	                           evalLargeLookup(lookupQuery, function (result) {
	                              result = (searchWord != '') ? result.filter(function (d) { return ((d.Id) ? d.Id.toLowerCase().indexOf(searchWord) != -1 : d.Id) || ((d.Description) ? d.Description.toLowerCase().indexOf(searchWord) != -1 : d.Description) || ((d.Name) ? d.Name.toLowerCase().indexOf(searchWord) != -1 : d.Name); }) : [];
	                              fieldProcessor();
	                           });
	                    }
	                    else{
		                    var querySplit = lookupQuery.split(/where/i);
		                    colName = Object.keys(tld[0]);
		                    if (colName[1])
		                        var columnName = colName[1];
		                    else
		                        columnName = colName[0];
		                    if (querySplit[1])
		                        querySplit[1] = ' WHERE (' + columnName + ' like "%' + searchWord.replace('%','\\%')+ '%" '+escapeQuery+' ) AND ' + querySplit[1] +' LIMIT 20';
		                    else
		                        querySplit.push(' WHERE (' + columnName + ' like "%' + searchWord.replace('%','\\%') + '%" '+escapeQuery+' )' + ' LIMIT 20');
		                    lookupQuery = querySplit[0] + querySplit[1];
		                    evalLargeLookup(lookupQuery, function (res) {
		                        result = res;
		                        fieldProcessor();
		                    });
	                   }
                   }
                   
                }
            });
        }
        else
            result = [];
        fieldProcessor();
    } else if (field.LookupTable == '$QUERY$' && field.LookupQuery) {
     	var i =  (!source && findx) ? findx: ($(source).attr('findex') ?  $(source).attr('findex') : "0") ;
        var lookupQuery = field.LookupQuery;
        var ds;
        var o;
        if (field.SourceTable && field.SourceTable !='_photo_meta_data')
            ds = activeParcel[field.SourceTable][i];
        else if (field.SourceTable && field.SourceTable == '_photo_meta_data' && ddlPhotoSource)
        	ds = ddlPhotoSource;
        if (ds)
            o = ds;
        if (o) {
            if (lookupQuery.search(/\{.*?\}/g) > -1) {
                lookupQuery.match(/\{.*?\}/g).forEach(function (fn) {
                    var testFieldName = fn.replace('{', '').replace('}', '');
                    var source, value, lookupField;
                    if (testFieldName.contains('parent.')) {
                        var fieldName = testFieldName.split('parent.');
                        lookupField = fieldName[fieldName.length - 1];
                        source = o;
                        while (testFieldName.search('parent.') > -1) {
                            source = source["parentRecord"] ? source["parentRecord"] : [];
                            testFieldName = testFieldName.replace('parent.' + lookupField, lookupField);
                        }
                        //source = o["parentRecord"] ? o["parentRecord"] : [];
                        //lookupField = testFieldName.split('.')[1];
                        //if (testFieldName.contains('parent.parent')) {
                        //    source = source["parentRecord"] ? source["parentRecord"] : [];
                        //    lookupField = testFieldName.split('.')[2];
                        //}
                    }
                    else if (testFieldName.indexOf('parcel.') > -1) {
                            source = activeParcel;
                            lookupField = testFieldName.split('parcel.')[1];
                        }
                    else { lookupField = testFieldName.split('.')[0]; source = o };
                    value = source[lookupField];
                    if (!value && value != "0") value = 'null';
                    if (('Image' in source) && ('LocalId' in source) && ('Path' in source) && ('MetaData1' in source)) {
                        var imgfield = getDataField(lookupField, '_photo_meta_data');
                        var valueField = '';
                        if (imgfield) {
                            //                                valueField = imgfield.AssignedName.replace('PhotoMetaField', 'MetaData');
                            //                                value = o[valueField];
                            //                                if ((value == null || value === undefined || value == '') && imgfield.DefaultValue) value = imgfield.DefaultValue;
                            valueField = imgfield.AssignedName.replace('PhotoMetaField', 'MetaData');
                            value = source[valueField];
                        }

                        //                            if (typeof value == "number" || !isNaN(value)) {
                        //                                value = parseFloat(value).toString();
                        //                            }

                        if (testFieldName.split('.') > 0) {
                            var attrName = testFieldName.split('.')[1];
                            var lk = getFieldLookup(lookupField);
                            if (lk) {
                                if (lk[value]) {
                                    if (lk[value].Object) {
                                        value = lk[value].Object[attrName];
                                    }
                                }
                            }
                        }

                    }
                    lookupQuery = lookupQuery.replace(fn, value);
                });
            }
            var colName = [];
            if (searchWord || searchWord == "") {
            	lookupQuery = lookupQuery.replace(/;$/, "");
            	var _limitString = searchWord == ''? '' : ' LIMIT 0,1';
            	if(searchWord && typeof(searchWord) == "string") searchWord = searchWord.replace('>','&gt;').replace('<','&lt;');
                getData(lookupQuery + _limitString, [], function (tld) {
                    if (tld.length > 0) {
                    	if (searchWord == '' && !SketchLookup) {                   		 
	                   		evalLargeLookup(lookupQuery, function (res) {
	                   			if (!isTrue(field.DoNotAllowSelectNull)) {
			                        if (isTrue(field.LookupShowNullValue)) 
				                        res.unshift({ Name: '', Id: '' });
				                    else 
				                        res.unshift({ Name: '', Id: '' });
			                    }
			                    else {
			                    	res = res.filter(function(r) { return (r.Id != '' && r.Id != '<blank>') });
			                    }
		                        if (!source && _fiId) {
		                        	_currentLookupAll = res;
		                        	if (_currentLookupAll.length < 31) {
						            	$('.lkShowAlllink').hide();
						            	$('.lkShowAllRec').show();
						            }
		                        }
		                        result = res.slice(0, 30);
		                        fieldProcessor();
			                }, tld);	
	                   }
                    	else { 
                    		try {
		                        var orderBySplit = lookupQuery.split(/order by/i);
		                        var unionSplit = orderBySplit[0].split(/UNION/i);
		                        var querySplit = unionSplit[0].split(/where/i);
									var escapeQuery='';
								if (searchWord.includes('%') || searchWord.includes('_')) escapeQuery= " ESCAPE '\\'";
		                        colName = Object.keys(tld[0]);
		                        if (colName[1])
		                            var columnName = colName[1];
		                        else
		                            columnName = colName[0];
		                        var unionquery = '';
		                        var count = 1;
		                        var availabelWord;
		                        var query;
		                        var queryunion;
		                        var availabelWordFrom;
		                        if(searchWord.includes('"')){
		                           evalLargeLookup(unionSplit, function (result) {
		                              result = (searchWord != '') ? result.filter(function (d) { return ((d.Id) ? d.Id.toLowerCase().indexOf(searchWord) != -1 : d.Id) || ((d.Description) ? d.Description.toLowerCase().indexOf(searchWord) != -1 : d.Description) || ((d.Name) ? d.Name.toLowerCase().indexOf(searchWord) != -1 : d.Name); }) : [];
		                              fieldProcessor();
		                            }, null, function () { searchErrorHandle(); });
		                        }
		                        else{
			                        $.map(unionSplit, function (i, j) {
			                        	searchWord=searchWord.replace('%','\\%');
	                        			searchWord=searchWord.replace('_','\\_');
			                            availabelWord = RegExp('\\b' + 'WHERE' + '\\b').test(i) || RegExp('\\b' + 'where' + '\\b').test(i);
			                            availabelWordFrom = RegExp('\\b' + 'FROM' + '\\b').test(i) || RegExp('\\b' + 'from' + '\\b').test(i);
			                            query = '(' + columnName + ' like "%' +  searchWord + '%"'+escapeQuery+')'+(orderBySplit[1] ? ' ORDER BY ' + orderBySplit[1] : '') + ' LIMIT 20 ';
			                            queryunion = '(' + columnName + ' like "%' +  searchWord + '%" '+escapeQuery+')' +  escapeQuery + ' UNION ';
			                            if (unionSplit.length == 1) {
			                                 (querySplit.length == 1?unionquery += i + " WHERE " + query : unionquery += i + " AND " + query )
			                            }
			                            else {                             	
			                        		if (unionSplit.length > count) {
			                        		    (!availabelWordFrom ? unionquery += i : (availabelWord?unionquery += i + " AND " + queryunion : unionquery += i + " WHERE " + queryunion ))
			                                	count++;
			                        		}
			                            	else {
			                            	   (!availabelWordFrom ? unionquery += i : (availabelWord?unionquery += i + " AND " + query : unionquery += i + " WHERE " + query ))    
			                                }                                                        
			                         	}
			                    	})
			                        //if (querySplit[1])
			                        //    querySplit[1] = ' WHERE (' + columnName + ' like "%' + searchWord + '%") AND ' + querySplit[1] + ' WHERE (' + columnName + ' like "%' + searchWord + '%")' + (orderBySplit[1] ? ' ORDER BY ' + orderBySplit[1] : '') + ' LIMIT 20';
			                        //else
			                        //    querySplit.push(' WHERE (' + columnName + ' like "%' + searchWord + '%")' + (orderBySplit[1] ? ' ORDER BY ' + orderBySplit[1] : '') + ' LIMIT 20');			  
			                        evalLargeLookup(unionquery, function (res) {
			                            result = res;
			                            fieldProcessor();
			                        }, null, function () { searchErrorHandle();  });
		                        }
		                    }
							catch (ex) {
								searchErrorHandle();
							}		                    
                        }
                    }
                    else {
                    	result = [];
                    	if (searchWord == '' && !SketchLookup) {
							if (!isTrue(field.DoNotAllowSelectNull)) {
								if (isTrue(field.LookupShowNullValue)) 
									result.unshift({ Name: '', Id: '' });
							}
							else {
								result = result.filter(function(r) { return (r.Id != '' && r.Id != '<blank>') });
							}
							if (!source && _fiId) {
								_currentLookupAll = result;
								if (_currentLookupAll.length < 31) {
									$('.lkShowAlllink').hide();
									$('.lkShowAllRec').show();
								}
							}
							result = result.slice(0, 30);
						}
                		fieldProcessor();
                    }
                });
            } else {
                result = [];
                fieldProcessor();
            }
        } else {
            result = (searchWord != '') ? getLookupArray(field.Id, null, null, field.Id).filter(function (d) { return ((d.Id) ? d.Id.toLowerCase().indexOf(searchWord) != -1 : d.Id) || ((d.Description) ? d.Description.toLowerCase().indexOf(searchWord) != -1 : d.Description) || ((d.Name) ? d.Name.toLowerCase().indexOf(searchWord) != -1 : d.Name); }) : getLookupArray(field.Id, null, null, field.Id);
            if (searchWord == '' && !SketchLookup) {
	            if (!isTrue(field.DoNotAllowSelectNull)) {
                    if (isTrue(field.LookupShowNullValue)) 
                        result.unshift({ Name: '', Id: '' });
                }
                else {
                	result = result.filter(function(r) { return (r.Id != '' && r.Id != '<blank>') });
                }
	            if (!source && _fiId) {
	            	_currentLookupAll = result;
	            	if (_currentLookupAll.length < 31) {
		            	$('.lkShowAlllink').hide();
		            	$('.lkShowAllRec').show();
	            	}
	            }
	            result = result.slice(0, 30);
            }
            fieldProcessor();
        }
    }
    else {
        result = (searchWord != '') ? getLookupArray(field.Id, null, null, field.Id).filter(function (d) { return ((d.Id) ? d.Id.toLowerCase().indexOf(searchWord) != -1 : d.Id) || ((d.Description) ? d.Description.toLowerCase().indexOf(searchWord) != -1 : d.Description) || ((d.Name) ? d.Name.toLowerCase().indexOf(searchWord) != -1 : d.Name); }) : getLookupArray(field.Id, null, null, field.Id);
        if (searchWord == '' && !SketchLookup) {
        	if (!isTrue(field.DoNotAllowSelectNull)) {
                if (isTrue(field.LookupShowNullValue)) 
                    result.unshift({ Name: '', Id: '' });
            }
            else {
            	result = result.filter(function(r) { return (r.Id != '' && r.Id != '<blank>') });
            }
            if (!source && _fiId) {
            	_currentLookupAll = result;
            	if (_currentLookupAll.length < 31) {
	            	$('.lkShowAlllink').hide();
	            	$('.lkShowAllRec').show();
	            }
            }
            result = result.slice(0, 30);
        }
        fieldProcessor();
    }
}

function markLookupSelected(source) {
    $('.customddlbtnadd').show();
    $('.customddl_desc ul li').removeAttr('selected');
    $('span[selectedcustomddl]').html($(source).html())
    $(source).attr('selected', '');
}

function setLookupValue(source) {
	relationcycle = [];
	//$('#data-collection').show();
	$('.lkShowAllStatus').html('Showing 0 records out of 0');
	$('.lkShowAlllink').show();
	$('.lkShowAllRec').hide();
	 _currentLookupAll = [];
    (source) ? $(source).hide() : '';
   	var saveType = saveInputValue;
    var Field = $('div[controls] input').attr('field');
    var pval = $('.customddl_desc ul li[selected]').attr('value') === 0 ? '<blank>': decodeURI($('.customddl_desc ul li[selected]').attr('value'));
	var f = datafields[$('div[controls] input').attr('field-id')];
 	if(f && f.CategoryId==-2) saveType = savePhotoProperty;
    if (pval && !sketchopened) {
        var dfield = datafields[$('div[controls] input').attr('field-id')];
       // if (dfield.IsLargeLookup == 'true' || dfield.IsLargeLookup == true) {
       //     $('.newvalue[field-id="' + dfield.Id + '"]').html('<option value="' + encodeURI(pval) + '">' + $('.customddl_desc ul li[selected]').html() + '</option>');
       // }
        var selectedValue = [];

        /*if (dfield.IsLargeLookup == 'true' || dfield.IsLargeLookup == true) {
            var lookupQuery = dfield.LookupQuery;
            lookupQuery = lookupQuery.replace(/;$/, "");
            getData(lookupQuery + ' LIMIT 0,1', [], function (tld) {
                if (tld.length > 0) {
                    var querySplit = lookupQuery.split(/where/i);
                    var colName = Object.keys(tld[0]);
                    if (querySplit(1))
                        querySplit[1] = ' WHERE (' + colName[0] + ' = "' + pval + '") AND ' + querySplit[1];
                    else
                        querySplit.push(' WHERE ' + colName[0] + ' = "' + pval + '" ');
                    lookupQuery = querySplit[0] + querySplit[1];
                    selectedValue = evalLargeLookup(lookupQuery);
                }
            });
        }
        else*/
        selectedValue = getLookupArray(Field, null, null, $('div[controls] input').attr('field-id')).filter(function (d) { return d.Id == pval; });
        if (selectedValue.length > 0 || (selectedValue.length == 0 && pval == '<blank>') || (selectedValue.length == 0 && (pval || pval == 0)) || dfield.IsLargeLookup == 'true' || dfield.IsLargeLookup == true) {
            var sval = 	(pval == '<blank>')? '': (selectedValue[0]? selectedValue[0].Id: ((selectedValue.length == 0 && (pval || pval == 0))? pval: ''));
            if (dfield.InputType == 5 /* && $('.newvalue[field-id="' + dfield.Id + '"][customlkd="1"]').length > 0 && $('[value = "'+ encodeURI(sval) +'"]', currentLookup).length == 0 */ ) {
               var dText = typeof($('.customddl_desc ul li[selected]').attr('displayText')) == "string"? $('.customddl_desc ul li[selected]').attr('displayText').replace('<', '&lt;').replace('>', '&gt;'): $('.customddl_desc ul li[selected]').attr('displayText');
               $('.newvalue[field-id="' + dfield.Id + '"]').html(dText);
	               /*var _pval = (pval == '<blank>')? '': pval;
				    $('.newvalue[field-id="' + dfield.Id + '"]').unbind('touchstart mousedown');
	                if ($('option', currentLookup)[0] && ($('option', currentLookup)[0].value == '' || $('option', currentLookup)[0].value == '<blank>'))
	                else
	                
	                $('.newvalue[field-id="' + dfield.Id + '"]').html('<option value="' + encodeURI(_pval) + '">' + $('.customddl_desc ul li[selected]').html() + '</option>'); 
					$('.newvalue[field-id="' + dfield.Id + '"]').siblings('.cusDpdown').children('span').html($('.customddl_desc ul li[selected]').html());
					
					if (dfield.DoNotAllowSelectNull == true || dfield.DoNotAllowSelectNull == 'true')
				        $('.newvalue[field-id="' + dfield.Id + '"]').find('option[value=""]').attr('disabled', true);
				   */
			}		
            $(currentLookup).val(encodeURI(sval));
        }
        relationcycle = [];
        saveType(currentLookup, function () {

        });
        if (ccma.UI.ActiveScreenId == "mass-update-div") {
            addToMassChanges(currentLookup);
        }
        $('.customddl').hide();
		var cFieldSet = $('.newvalue[field-id="' + dfield.Id + '"]').parent();
		if (cFieldSet[0] && dfield.InputType == "5") recalculateCustomDropdownWidth(cFieldSet);
		$('.mask').hide();
        isLookupScreen = false;
        currentLookup = [];
        currentText = '';

    }
    else if (currentLookupValue && SketchLookup && sketchopened) {
        $(currentLookup).val(currentLookupValue);
        $( '.dynamic_prop select' ).trigger("change");
        closeddlDefault();
        currentLookupValue = null;
        SketchLookup = null;
        $( '.Current_vector_details .validationFailMsg' ).hide();
    }
    else {
        closeddlDefault();
    }
}

function closeddlDefault() {
	$('.lkShowAllStatus').html('Showing 0 records out of 0');
	$('.lkShowAlllink').show();
	$('.lkShowAllRec').hide();
    currentLookup = [];
    _currentLookupAll = [];
    $('div[controls] input').val('');
    //$('#data-collection').show();
    $('.customddl, .mask').hide();
    isLookupScreen = false;
    currentLookupValue = null;
    SketchLookup = null;
    if(appState.screenId=="sketchview")
     	$('.mask').show();
}

function setHeatmapFields() {
    $('.heatmap-items').empty();
    var heatMapitems = Object.keys(datafields).map(function (x) { return datafields[x] }).filter(function (x) { return x.isHeatmap == 'true' });
    $('<option value="">--Select--</option>').appendTo('.heatmap-items');
    for (var field in heatMapitems) {
        $('<option value="' + heatMapitems[field].Name + '" Id="' + heatMapitems[field].Id + '">' + heatMapitems[field].Label + '</option>').appendTo('.heatmap-items');
    }
    getData("SELECT FunctionName,TableName,Fieldname,RowUId FROM AggregateFieldSettings WHERE IncludeInHeatmap ='true'", null, function (items) {
        for (var x in items) {
            var Id = items[x].ROWUID;
            var FunctionName = items[x].FunctionName;
            var TableName = items[x].TableName;
            var Fieldname = items[x].FieldName;
            var temp = { Id: Id, Name: FunctionName + '_' + TableName + '_' + Fieldname, Label: FunctionName + '_' + TableName + '_' + Fieldname };
            //heatMapitems.push(temp);
            $('<option value="' + temp.Name + '" Id="' + temp.Id + '">' + temp.Label + '</option>').appendTo('.heatmap-items');
        }

    });

    //if (callback) callback();
}

function loadExtraLookups(callback) {
	if (clientSettings["SketchConfig"] || sketchSettings["SketchConfig"]) {
		var _clientSketchConfig = clientSettings["SketchConfig"] || sketchSettings["SketchConfig"];
		if ( _clientSketchConfig == 'FTC' || _clientSketchConfig == 'ApexJson' )
			getData('SELECT DETAILCALCULATIONTYPE, IMPDETAILTYPEID, IMPDETAILDESCRIPTION, IMPDETAILTYPE, APEXLINKFLAG, ACTIVEFLAG, APEXAREACODE FROM TLKPIMPSDETAILTYPE ORDER BY SORTORDER', [], function(d) {
				FTC_Detail_Lookup = JSON.parse(JSON.stringify(d));
				getData('SELECT ADDONFILTERTYPE, ADDONDESCRIPTION, IMPDETAILTYPE, JURISDICTIONID, APEXLINKFLAG, ACTIVEFLAG, ADDONCODE FROM TLKPIMPSADDONS ORDER BY SORTORDER', [], function(d) {
					FTC_AddOns_Lookup = JSON.parse(JSON.stringify(d));
					getData('SELECT IMPSFLOORDESCRIPTION, APEXLINKFLAG, ACTIVEFLAG FROM TLKPIMPSFLOOR ORDER BY SORTORDER', [], function(d) {
						FTC_Floor_Lookup = JSON.parse(JSON.stringify(d));
						if (callback) callback();
					});
				});
			});
		else if ( _clientSketchConfig == 'ProVal' || _clientSketchConfig == 'ProValNew' || _clientSketchConfig == 'ProValNewFYL' )
			getData("SELECT s.*,c.tbl_element,c.field_2 FROM sketch_codes s join codes_table c on s.tbl_element_desc=c.tbl_element_desc and s.field_1=c.field_1 ORDER BY s.Ordinal", [], function(d) {
				Proval_Lookup = JSON.parse(JSON.stringify(d));
				getData("SELECT (label_code) Code,label_description,UPPER(label_short) label_short FROM imp_labels WHERE (label_code like 'O%') OR (label_code like 'S%') ORDER BY label_description ASC", [], function(d) {
					Proval_Outbuliding_Lookup = JSON.parse(JSON.stringify(d));
					getData("select tbl_element, tbl_element_desc, tbl_type_code from codes_table where tbl_type_code = 'extcover' or tbl_type_code = 'MSExtWall' or tbl_type_code = 'extframe' or tbl_type_code = 'const'", [], function(d) {
						Proval_Default_Exterior_Cover = JSON.parse(JSON.stringify(d));
						getData("select * from sketch_codes", [], function(d) {
							Proval_sketch_codes = JSON.parse(JSON.stringify(d));
							if (callback) callback();
				 		});
				 	});
				 });
			});
		else if ( _clientSketchConfig == 'MVP' )
			getData("SELECT s.*,c.tbl_element,c.field_2,c.CodesToSysType FROM sketch_codes s join codes_table c on s.tbl_element_desc != 'N/A' and s.tbl_element_desc=c.tbl_element_desc and s.field_1=c.field_1 ORDER BY s.Ordinal", [], function(d) {
				MVP_Lookup = JSON.parse(JSON.stringify(d));
				getData("SELECT label_code, label_short, mdescr, label_text FROM imp_labels i JOIN t_systype t ON i.label_text = t.msysTypeId ORDER BY label_description", [], function(d) {
					MVP_Outbuliding_Lookup = JSON.parse(JSON.stringify(d));
					getData("select * from codes_table where tbl_type_code = 'extcover'", [], function(d) {
						MVP_Default_Exterior_Cover = JSON.parse(JSON.stringify(d));
						getData("select * from sketch_codes", [], function(d) {
							MVP_sketch_codes = JSON.parse(JSON.stringify(d));
							getData("select * from sketch_labels", [], function(d) {
								MVP_sketch_labels = JSON.parse(JSON.stringify(d));
								getData("select * from preferences where PrefSection = 'Sketch' AND PrefEntry IN ('FinishPct1', 'FinishPct2', 'FinishPct3')", [], function(d) {
									MVP_preferences_lookup = JSON.parse(JSON.stringify(d));
									if (callback) callback();
					 			});	
				 			});
				 		});
				 	});
				 });
			});
		else if ( _clientSketchConfig == 'Lafayette' )
			getData("SELECT * FROM lu_sar", [], function(d) {
				Lafayette_Lookup = JSON.parse(JSON.stringify(d));
				if (callback) callback();
			});
		else if ( _clientSketchConfig == 'Patriot' )
			getData("SELECT * FROM CCV_xrSubArea", [], function(d) {
				Patriot_Lookup = JSON.parse(JSON.stringify(d));
				if (callback) callback();
			});
		else if ( _clientSketchConfig == 'Bristol' || _clientSketchConfig == 'Vision6_5' )
			getData("SELECT AREA_TYPE Id, AREA_TYPE || ' - ' || DSC_DESC Name, MDL FROM AREACODE INNER JOIN DESCRIPT ON AREA_TYPE = DSC_CODE WHERE DSC_TYPE = 'sat'", [], function(d) {
				Bristol_Lookup = JSON.parse(JSON.stringify(d));
				if (callback) callback();
			});
		else if ( _clientSketchConfig == 'FarragutJson' )
			getData("SELECT * FROM SKETCH_CODES_LOOKUP", [], function(d) {
				Farragut_Lookup = JSON.parse(JSON.stringify(d));
				if (callback) callback();
            });
        else if (_clientSketchConfig == 'SigmaCuyahoga')
            getData("SELECT CODE, DESCRIPTION Desc FROM VALUE_LIST WHERE TABLE_NAME = 'RES_AMENITY' AND COLUMN_NAME = 'AMENITY_TYPE'", [], function (d) {
                SigmaCuyahoga_Lookup = JSON.parse(JSON.stringify(d));
                if (callback) callback();
            });
        else if (_clientSketchConfig == 'WrightJson') {
            getData("SELECT p.[CODE] Id, p.[CODE] || ' - ' || p.[NAME] Name, p.[DESCR], p.[NAME] LName, f.[BASEAREA], f.[FIRST], f.[LOWER], f.[SECOND], f.[THIRD], t.TBLE FROM CCV_APEX_CODES_APEX_CODES_AUD p JOIN CCV_APEX_IAS_TAB_REF_APEX_IAS_TAB_REF_AUD t ON p.CODE = t.CODE JOIN CCV_APEX_ADDN_APEX_ADDN_AUD f ON p.CODE = f.CODE WHERE t.TBLE = 'ADDN' AND p.AUD_OP='I' AND f.AUD_OP='I'", [], function (d) {
                WrightAddn_Lookup = JSON.parse(JSON.stringify(d));
                getData("SELECT p.[CODE] Id, p.[CODE] || ' - ' || p.[NAME] Name, p.[DESCR], p.[NAME] LName, t.TBLE FROM CCV_APEX_CODES_APEX_CODES_AUD p JOIN CCV_APEX_IAS_TAB_REF_APEX_IAS_TAB_REF_AUD t ON p.CODE = t.CODE WHERE t.TBLE IN ('COMINTEXT', 'COMFEAT') AND p.AUD_OP='I'", [], function (d) {
                    WrightCom_Lookup = JSON.parse(JSON.stringify(d));
                    getData("select [CODE] Id, [USETYPE] from CCV_APEX_COMINTEXT_APEX_COMINTEXT_AUD where USETYPE IS NOT NULL", [], function (d) {
                        WrightComType_Lookup = JSON.parse(JSON.stringify(d));
                        if (callback) callback();
                    });
                });
            });
        }
		else if (callback) callback();
		
	}
	else if (callback) callback();
} 

function fillAllLookupList() {
	//if ($('.lkShowAll').hasClass('lkShowAllactive'))
		//return false;
	$('.customddl_desc ul li').removeAttr('selected');
	$('.customddlbtnadd').hide();
	$('.customddl_header input').val('');
	var _le = _currentLookupAll.length;
	$('.lkShowAllStatus').html('Showing ' + _le + ' records out of ' + _le);
	$('.lkShowAlllink').hide();
	$('.lkShowAllRec').show();
	var _fId = $('.customddl_header input').attr('field-id');
	var field = datafields[_fId];
	var result = _currentLookupAll;
	if (result.length > 0) {
        ( !isNaN( result[0].SortType ) && result[0].SortType ) ? result = ccma.Data.Evaluable.sortArray( result, eval( 'lookup_' + parseInt( result[0].SortType ) ), true ) : '';
        var temp = field.LookupQuery && field.LookupQuery != '' ? '${Name}' : '${cId} - ${Name}';
        result.forEach(function(x, index) { 
        	result[index].cId = ''; result[index].cName = x.Name;
        	if (x.Id || x.Id === 0) result[index].cId = x.Id; 
        	if (!x.Name && x.Name !== 0) result[index].cName = x.Id;
			if(x.Id) result[index].Id = encodeURI(x.Id);
			if( temp == '${Name}' && !x.Name ) result[index].Name = result[index].Id;
        });
        $( '#loolupul' ).html( '<li value="${Id}" displayText="${cName}" onclick="markLookupSelected(this);">' + temp + '</li>' );
        $('#loolupul').fillTemplate(result);
        $( 'span[selectedcustomddl]' ).html( currentText );
    }
}

function convertToCommaSeparatedDropDown(list, field, idField) {
    let lk = list;
    if (datafieldsettings && list.length == 1 && field && datafieldsettings.filter((x) => { return x.FieldId == field.Id && x.PropertyName == "CommaSeparatedDropDown" && x.Value == '1' })[0]) {
        try {
            let lst = list[0][idField].split(","); lk = [];
            lst.forEach((l) => {
                l = l ? l.trim() : l;
                lk.push({ [idField]: l });
            });
        }
        catch (ex) {
            return list;
        }
    }
    return lk;
}
