﻿//    DATA CLOUD SOLUTIONS, LLC ("COMPANY") CONFIDENTIAL 
//    Unpublished Copyright (c) 2010-2013 
//    DATA CLOUD SOLUTIONS, an Ohio Limited Liability Company, All Rights Reserved.

//    NOTICE: All information contained herein is, and remains the property of COMPANY.
//    The intellectual and technical concepts contained herein are proprietary to COMPANY
//    and may be covered by U.S. and Foreign Patents, patents in process, and are protected
//    by trade secret or copyright law. Dissemination of this information or reproduction
//    of this material is strictly forbidden unless prior written permission is obtained
//    from COMPANY. Access to the source code contained herein is hereby forbidden to
//    anyone except current COMPANY employees, managers or contractors who have executed
//    Confidentiality and Non-disclosure agreements explicitly covering such access.</p>

//    The copyright notice above does not evidence any actual or intended publication
//    or disclosure of this source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
//    DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
//    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
//    IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
//    OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
//    TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
//    ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.



var heading;
var located = false;
var locationDenied = false;
var gisAccuracy = 3000;         //rename variable
var isFirstTime = true;


function GPSTracker() {
    var ticks = () => { return (new Date()).getTime(); }

    const SLOW = 'SLOW', FAST = 'FAST';
    var _t = this;
    var _mode = null;
    var _lastTick = ticks();
    var _accurate = false;
    var _tolerance = 20;
    var _logging = false;
    var _status = {
        success: false,
        denied: false,
        result: {
            accuracy: 0,
            latitude: null,
            longitude: null,
            updated: null
        },
        counter: {
            gets: 0,
            updates: 0,
            nosignal: 0,
            timeout: 0,
            errors: 0,
            recentErrors: 0
        },
        lastError: {
            code: null,
            error: null
        }
    };

    Object.defineProperties(_status.result, {
        age: {
            enumerable: true,
            get: function () {
                if (!_status.result.updated) return 0;
                return ticks() - _status.result.updated;
            }
        },
        ageInSeconds: {
            enumerable: true,
            get: function () {
                return Math.round(_status.result.age / 1000);
            }
        }
    });

    var lat, lng;

    var _callbackRun = false;

    var _get_seconds = 30;
    var _options_watch = { timeout: 3000, enableHighAccuracy: false, maximumAge: 1000 }
    var _options_get = { timeout: 3000, enableHighAccuracy: true, maximumAge: 1000 }

    var _geo_get, _geo_watch;

    this.getCurrentPosition = function (callback) {
        if (!iPad && !windowsTouch) {
            var loc = setMyLocation({
                coords: {
                    latitude: 32.433334,
                    longitude: -99.733330
                }
            });
            if (callback) callback(loc);
            return;
        }
        navigator.geolocation.getCurrentPosition(function (position) {
            var loc = setMyLocation(position)
            if (callback) callback(loc);
        }, function (error) {
            locationError(error, callback);
        }, _options_get);
    }

    this.SwitchToSlowMode = function () {
        if (_mode == SLOW) return;
        console.log('geo: Switching to slow mode');
        _geo_watch && navigator.geolocation.clearWatch(_geo_watch);

        var _getPosition = function () {
            _status.counter.gets += 1;

            if (_status.counter.gets > 1 && (_status.counter.updates + _status.counter.errors) == 0) {
                showError("gps-access-denied");
            }

            _geo_get && clearTimeout(_geo_get);
            navigator.geolocation.getCurrentPosition(function (p) { updateGeoLocation(p) }, function (e) { locationError(e); }, _options_get);
            _geo_get = setTimeout(function () { _getPosition(); }, _get_seconds * 1000);
        }

        _getPosition();
        _mode = SLOW;

    }

    this.SwitchToFastMode = function () {
        if (_mode == FAST) return;
        console.log('geo: Switching to fast mode');
        _geo_get && clearTimeout(_geo_get);
        _geo_watch = navigator.geolocation.watchPosition(function (p) { updateGeoLocation(p) }, function (e) { locationError(e); }, _options_watch);
        _mode = FAST;
    }

    this.startGeo = function () {
        $('#gpsstatus').removeClass('gpserror');
        $('#gpsstatus').removeClass('gpstimeout');
        $('#gpsstatus').removeClass('gpsdone');
        _t.SwitchToSlowMode();
    }

    function updateGeoLocation(position, callback) {
        located = true;
        _status.success = true;
        _status.counter.recentErrors = 0;
        _status.result.latitude = position.coords.latitude;
        _status.result.longitude = position.coords.longitude;
        _status.result.accuracy = position.coords.accuracy;
        _status.result.updated = ticks();
        _status.counter.updates += 1;
        gisAccuracy = position.coords.accuracy;

        setMyLocation(position);

        if (_logging)
            console.log((new Date()).toLocaleTimeString(), position.coords.latitude, position.coords.longitude, position.coords.accuracy);

        if (callback && _callbackRun) {
            _callbackRun = false;
            callback();
        }

        //if (trackMe) {                    //disabling this section, as accuracy is also tracked currently.
        //    if (position.coords.accuracy > _tolerance) {
        //        console.log('Accuracy ' + position.coords.accuracy + ' less than tolerance limit ' + _tolerance);
        //        //resetGeoLocationWatch();
        //        return;
        //    }
        //}


        $('#gpsstatus').removeClass('gpsdone');
        var lat = UsingOSM ? (OSMmyLocation ? OSMmyLocation.lat : 0) : (myLocation ? myLocation.lat() : 0);
        var lng = UsingOSM ? (OSMmyLocation ? OSMmyLocation.lng : 0) : (myLocation ? myLocation.lng() : 0);
        localStorage.setItem('gpspos-lat', lat);
        localStorage.setItem('gpspos-lng', lng);
        localStorage.setItem('gpspos-acc', gisAccuracy);
        localStorage.setItem('gpspos-ts', Math.round((new Date(position.timestamp)).tickSeconds()));
        _accurate = true;

        if (ccma.Session.IsLoggedIn) {
            var currentTick = ticks();
            if (((currentTick - _lastTick) / 1000) > 2 * 60) {

                db.transaction(function (x) {
                    x.executeSql("INSERT INTO Location (UpdateTime, Latitude, Longitude, Accuracy, Synced) VALUES (?, ?, ?, ?, 0)", [Math.round((new Date(position.timestamp)).ticks()), position.coords.latitude, position.coords.longitude, gisAccuracy], function (x, result) {
                    }, function (x, e) {
                        log(x);
                        log(e);
                    })
                });
                _lastTick = ticks();
            }
        }

    }

    function setMyLocation(position) {
        var iconNew = ["/static/maps/anchor.png",
            "/static/maps/anchorGray.png"];
        myLocation = new GISPoint(position.coords.latitude, position.coords.longitude);
        if (L)
            OSMmyLocation = new L.latLng(position.coords.latitude, position.coords.longitude);
        if (mapInitialized) {
            if (!UsingOSM && !myMarker) {
                myMarker = new google.maps.Marker({
                    position: myLocation,
                    icon: gisAccuracy < _tolerance ? iconNew[0] : iconNew[1],
                    map: map
                });
            }
            if (UsingOSM && !osMarker) {
                if (L)
                    osMarker = new L.marker(OSMmyLocation, {
                        icon: anchor
                    }).addTo(OsMap);
            }
            if (UsingOSM == true) {
                osMarker.setLatLng(OSMmyLocation);
                if (trackMe) {
                    OsMap.panTo(osMarker.getLatLng());
                }
                if (osCircle) {
                    osCircle.setRadius(gisAccuracy);
                    osCircle.setLatLng(OSMmyLocation);
                }
            }
            else {
                if (gisAccuracy < _tolerance) {
                    myMarker.setPosition(myLocation);
                    myMarker.setIcon(iconNew[0]);
                } else {
                    myMarker.setPosition(myLocation);
                    myMarker.setIcon(iconNew[1]);
                }
                if (trackMe) {
                    map.panTo(myLocation);
                }
                if (myCircle) {
                    myCircle.setRadius(gisAccuracy);
                    myCircle.setCenter(myLocation);
                }
            }
        }
        return myLocation;
    }

    function locationError(error, callback) {
        _status.counter.errors += 1;
        _status.lastError.error = error;
        _status.lastError.code = error.code;
        _status.counter.recentErrors += 1;

        switch (error.code) {
            case 1:      //PERMISSION_DENIED
                locationDenied = true;
                _status.denied = true;
                if (callback) callback();
                break;
            case 2:     //POSITION_UNAVAILABLE
                console.log('GPS: Position unavailable');
                _status.counter.nosignal += 1;
                $('#gpsstatus').removeClass('gpstimeout');
                $('#gpsstatus').addClass('gpserror');
                //Retry???
                if (callback) (_status.counter.recentErrors < 10) ? _t.getCurrentPosition(callback) : callback();
                break;
            case 3:     //TIMEOUT
                console.log('GPS: Timeout Error');
                _status.counter.timeout += 1;
                $('#gpsstatus').removeClass('gpserror');
                $('#gpsstatus').addClass('gpstimeout');
                if (callback) (_status.counter.recentErrors < 10) ? _t.getCurrentPosition(callback) : callback();
                //Retry???
                break;
            default:    //Any other unknown error
                console.log('GPS: Unknown Error (' + error.code + ')');
                console.log(error);
                //Retry???
                if (callback) callback();
                break;
        }
    }

    this.adjustGISTimerForMap = function (screenId) {
        if (screenId == 'gis-map') {
            _t.SwitchToFastMode();
        } else {
            _t.SwitchToSlowMode();
        }
    }

    Object.defineProperties(this, {
        version: { value: "3.1.1" },
        status: { get: function () { return _status } },
        mode: { get: function () { return _mode } },
        location: { get: function () { return { latitude: _status.result.latitude, longitude: _status.result.longitude } } },
        test: { get: function () { return _status.result.latitude + ',' + _status.result.longitude + ' [' + Math.round(_status.result.accuracy) + ']'; } },
        logging: {
            get: function () { return _logging; },
            set: function (v) { _logging = v; }
        }
    })
}

var geo = new GPSTracker();
