﻿// Map related configuration goes here...

var customCountyWMS;  //display a county's imagery via their webservice 
var NearMap; //display nearmap imagery using tile webservice
var NearMapWMS; //display nearmap imagery using tile webservice
var WoolPert;
var sixLayer;
var threeLayer;
var freshload = true;

function ShowMapTypePopUp() {
    $("#divpop").offset({
        left : $('.Setting').position().left - 180,
		top : $('.Setting').position().top + $('.Setting').height() + 4
    });
    AerialMap ? $('#myonoffswitch1').attr('checked', '') : $('#myonoffswitch1').removeAttr('checked');
    ShowBoundaries ? $('#myonoffswitch2').attr('checked', '') : $('#myonoffswitch2').removeAttr('checked');
    ShowAll ? priMapMode == 0 ? $('#myonoffswitch3').attr('checked', '') : $('#myonoffswitch3').removeAttr('checked') : $('#myonoffswitch3').removeAttr('checked');
    HeatLayer ? $('#myonoffswitch4').attr('checked', '') : $('#myonoffswitch4').removeAttr('checked');
    CLayer ? $('#myonoffswitch5').attr('checked', '') : $('#myonoffswitch5').removeAttr('checked');
    StreetLayer ? $('#myonoffswitch6').attr('checked', '') : $('#myonoffswitch6').removeAttr('checked');
    StreetView ? $('#myonoffswitch7').attr('checked', '') : $('#myonoffswitch7').removeAttr('checked');

    if ($('.modalDialog').hasClass('hidden')) {
        // checkMapItems();
        $('.modalDialog').removeClass('hidden');
    } else {
        HideMapPopup();
    }
    window.addEventListener("orientationchange", function() {
  		HideMapPopup();
	}, false);
    
}

function HideMapPopup() {
    $('.modalDialog').addClass('hidden');
}

function SubmitMapType() {
    showMask("Loading...");
    AerialMap = $('#myonoffswitch1').is(":checked");
    ShowBoundaries = $('#myonoffswitch2').is(":checked");
    ShowAll = $('#myonoffswitch3').is(":checked");
    HeatLayer = $('#myonoffswitch4').is(":checked");
    CLayer = $('#myonoffswitch5').is(":checked");
    StreetLayer = $('#myonoffswitch6').is(":checked");
    StreetView = $('#myonoffswitch7').is(":checked");
    if (activeParcel) activeParcel = null;
    setupMap(function() {
        $('#maskLayer').hide();
        showHideLines();
        //  BoundariesONOFF();
        HideMapPopup();
        persistentStore();
    });
}

function persistentStore() {
    localStorage.setItem('AerialMap', AerialMap);
    localStorage.setItem('ShowLines', ShowBoundaries);
    localStorage.setItem('ShowAllCached', ShowAll);
    localStorage.setItem('HeatMap', HeatLayer);
    localStorage.setItem('StreetMap', StreetLayer);
    localStorage.setItem('StreetView', StreetView);
    localStorage.setItem('CLayer', CLayer);
}

function showHideLines() {
    if (ShowAll == true) {
        loadAllParcelsInMap();
    } else {
        loadPrioritiesOnlyInMap();
    }
    if (OsMap) {
        if (ShowBoundaries) {
            HeatMapIcons = {};
            loadParcelsInMap();
        }
        if ($('.heatmap-items').val() == "") {
            CurrentHeatcurrentHeatMapFieldId = null;
            removeLegend();
        }
    }
}

function loadPersistent() {
    AerialMap = JSON.parse(localStorage.getItem('AerialMap'));
    ShowBoundaries = JSON.parse(localStorage.getItem('ShowLines'));
    ShowAll = JSON.parse(localStorage.getItem('ShowAllCached'));
    HeatLayer = JSON.parse(localStorage.getItem('HeatMap'));
    StreetLayer = JSON.parse(localStorage.getItem('StreetMap'));
    StreetView = JSON.parse(localStorage.getItem('StreetView') == undefined ? true : JSON.parse(localStorage.getItem('StreetView')));
    CLayer = JSON.parse(localStorage.getItem('CLayer'));
    currentHeatMapField = localStorage.getItem('currentHeatMapField');
    currentHeatMapFieldId = localStorage.getItem('currentHeatMapFieldId');
}

function osmLoad(callback) {
    if (!google && !UsingOSM) {
        if (callback) callback();
        return;
    }
    clearMap();
    if (UsingOSM) {
        //$(".googleclass").unbind(touchClickEvent);
        $(".googleclass").bind(touchClickEvent, function() {
            $('#myonoffswitch7').attr('checked') == "true" || $('#myonoffswitch7').is(':checked') ? $('#myonoffswitch5').attr('checked') == "true" || $('#myonoffswitch5').is(':checked') ? $('#myonoffswitch5').removeAttr('checked') : '' : $('#myonoffswitch5').prop('checked', true);
        });
        //$(".customclass").unbind(touchClickEvent);
        $(".customclass").bind(touchClickEvent, function() {
            $('#myonoffswitch5').attr('checked') == "true" || $('#myonoffswitch5').is(':checked') ? $('#myonoffswitch7').attr('checked') == "true" || $('#myonoffswitch7').is(':checked') ? $('#myonoffswitch7').removeAttr('checked') : '' : $('#myonoffswitch7').prop('checked', true);
        });
        markerlayerGeo = new L.FeatureGroup();
        markerlayer = new L.FeatureGroup();
        polygonlayer = new L.FeatureGroup();
        offlineLayer = new L.FeatureGroup();
        labelLayer = new L.markerClusterGroup({
            spiderfyOnMaxZoom: false,
            showCoverageOnHover: false,
            iconCreateFunction: function(cluster) {
                var markers = cluster.getAllChildMarkers();
                var html = '<b>' + markers[0].options.icon.options.html + '</b>';
                return L.divIcon({
                    html: html,
                    className: 'leaflet-label'
                });
            },
        });
        loadPersistent();
        //AerialMap = true;
		Aerial = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
			//Aerial = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}', {
			maxZoom: 21,
			minZoom: 10,
			maxNativeZoom: 19
		});
		geoloc = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
			maxZoom: 21,
			minZoom: 10,
			maxNativeZoom: 19
		});
        OSMStreet = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
          	maxZoom: 21,
            minZoom: 10,
            maxNativeZoom:19
        });
        googleStreets = L.tileLayer('https://mt0.google.com/vt/lyrs=m&hl=en&x={x}&y={y}&z={z}&s=Ga', {
            maxZoom: 21,
            minZoom: 10,
            maxNativeZoom:19,
            subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
        });
        googleHybrid = L.tileLayer('https://mt0.google.com/vt/lyrs=y&hl=en&x={x}&y={y}&z={z}&s=Ga', {
            maxZoom: 21,
            minZoom: 10,
            maxNativeZoom:20,
            subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
        });
        
        if(clientSettings.EnableNearmap && clientSettings.EnableNearmap.includes('MA') && UsingOSM)
   		{
   			nearmapStreets = L.tileLayer('https://us0.nearmap.com/maps/z={z}&x={x}&y={y}&version=2&nml=Vert&client=wmts_integration&httpauth=false&apikey='+clientSettings["Nearmap.APIKey"], 
   										{
   										maxZoom: 21,
   										minZoom: 10,
						});
   		}

		if (clientSettings.EnableNearmapWMS && clientSettings.EnableNearmapWMS.includes('MA') && UsingOSM) {
			var nearmapWMSStreets = L.tileLayer.wms('https://api.nearmap.com/wms/v1/latest/apikey/' + clientSettings["Nearmap.APIKey"] + '?service=WMS',
				{ layers: 'Nearmap/Nearmap/USA,Nearmap,Satellite' },
				{
					maxZoom: 19,
					maxNativeZoom: 18,
					minZoom: 10,
				})
		}

        if(clientSettings['customWMS'] && clientSettings['EnableCustomWMS'] && clientSettings['EnableCustomWMS'].includes('MA')){
        	if(clientSettings['customMapIsImage'] && clientSettings['customMapIsImage'] == '1'){
		    	customCountyWMS = L.esri.imageMapLayer({
		     		url: clientSettings['customWMS']
		    	});
        	}else{
	        	customCountyWMS = L.tileLayer(clientSettings['customWMS'] +'{z}/{y}/{x}', {
	        		maxZoom: 21,
	        		maxNativeZoom:19,
	            	minZoom: 10
	        	});
        	}
        }
        
        setupMap();
        drawOfflineMap();
        if (OsMap) {
            OsMap.removeLayer(offlineLayer);
            OsMap.removeLayer(labelLayer);
            //OsMap.removeLayer(Aerial);
            //Aerial.addTo(OsMap);
            $('.googlemapsetup').addClass('hidden');
            $('.osmapsetup').removeClass('hidden');
        }
    } else {
        setupMap();
        $('.googlemapsetup').removeClass('hidden');
        $('.osmapsetup').addClass('hidden');
    }
    if (callback) callback();
}

function showNearmap(){
	if(!checkNearMap()){
		$('.map-items').val('');
		hideNearmap();
		messageBox("Nearmap not loaded. Please try after few seconds or please review your licence.");
		return;
	}
	if (!navigator.onLine) {
		$('.map-items').val('');
		hideNearmap();
		console.error("Failed to connect to the internet.");
		messageBox("Sorry, Nearmap map won't be available in offline mode.", function () {
			return false;
		});
	} else {
		showMask('Loading..');
		hideSvOverlay();
		hideAllMapViews();
		$('#nearmap').show();
		$('#mapCanvas').hide();
		$('.map-items').val('Nearmap');
		var skipError = true;
		/*var vertical = L.tileLayer('https://us0.nearmap.com/maps/z={z}&x={x}&y={y}&version=2&nml=Vert&client=wmts_integration&httpauth=false&apikey='+clientSettings["Nearmap.APIKey"], {maxZoom: 22, minZoom: 10}),
			panorama = L.tileLayer('https://us0.nearmap.com/maps/z={z}&x={x}&y={y}&version=2&nml=North&client=wmts_integration&httpauth=false&apikey=' + clientSettings["Nearmap.APIKey"], {maxZoom: 22, minZoom: 10});
		var mapOptions = {
			center: [39.223585, -84.298704],
			zoom: 19,
			layers: [vertical, panorama]
		}
		if(!NearMap){
			NearMap = L.map('nearmap', mapOptions);
			var baseMaps = {
				"Panorama": panorama,
				"Vertical": vertical
			};
			L.control.layers(baseMaps).addTo(NearMap);
			vertical.on('tileerror', function(){
				if(skipError){
					console.log('Error while loading tile');
					skipError = false;
				}
			});
		}else{
			for(i in NearMap._layers) {
		        if(NearMap._layers[i]._path != undefined) {
		            try {
		                NearMap.removeLayer(NearMap._layers[i]);
		            }
		            catch(e) {
		                console.log("problem with " + e + NearMap._layers[i]);
		            }
		        }
		    }
		}
		if(activeParcel.MapPoints.length > 0){
			var shapes = [];
			for (x in activeParcel.MapPoints.sort(function (x, y) { return x.Ordinal - y.Ordinal })) {
				var p = activeParcel.MapPoints[x];
				if (shapes[p.RecordNumber || 0] == null) {
					shapes[p.RecordNumber || 0] = [];
				}
				shapes[p.RecordNumber || 0].push([p.Latitude, p.Longitude]);
			};
			var polygon = [];
			shapes.forEach(function (w) {
				polygon.push(w);
			});
			L.polygon(polygon, {color: '#87CEEB'}).addTo(NearMap);
			if(activeParcel.Center()) NearMap.panTo(activeParcel.Center());
		}*/

        resetCustomNearMap();

		if(NearMap) NearMap.invalidateSize();
		checkPictometry();
        $('.map-hide-pictometry').hide();
        checkEagleView();
        $('.map-hide-ev').hide();
        removeEVScripts();
        checkImagery();
		$('.map-hide-Imagery').hide();
		checkWoolpert();
		$('.map-hide-woolpert').hide();
		if(clientSettings.EnableqPublic && clientSettings.EnableqPublic == '1'){
			$('.q_public button').html('Show qPublic');
			$('.q_public button').attr('onclick','show_qPublic();');
		}
		$('#maskLayer').hide();
		$('.map-show-nearmap').hide()
		if($('.map-items option').length == 0) $('.map-hide-nearmap').show();
		$('#lblpop').hide();
		$('.heatmap-items').hide();
	}
}

var evSrc1, evSrc2, evSrc3;

function loadEVScripts(callback) {
    try {
        if (!evSrc1) {
            evSrc1 = document.createElement("script");
            evSrc1.src = "https://unpkg.com/react@17/umd/react.production.min.js";
            setTimeout(() => {
                document.body.appendChild(evSrc1);
                evSrc1.onload = function () {
                    if (!evSrc2) {
                        evSrc2 = document.createElement("script");
                        evSrc2.src = "https://unpkg.com/react-dom@17/umd/react-dom.production.min.js";
                        setTimeout(() => {
                            document.body.appendChild(evSrc2);
                            evSrc2.onload = function () {
                                if (!evSrc3) {
                                    evSrc3 = document.createElement("script");
                                    evSrc3.src = "https://embedded-explorer.eagleview.com/static/embedded-explorer-widget.js";
                                    setTimeout(() => {
                                        document.body.appendChild(evSrc3);
                                        evSrc3.onload = function () {
                                            if (callback) callback();
                                        };
                                    }, 200);
                                }
                            };
                        }, 200);
                    }
                };
            }, 200);
        }
        else if (callback) callback();
    }
    catch (ex) {
        messageBox("Eagle view scripts are not loaded!", function () {
            $('#maskLayer').hide();
            return false;
        });
    }
}

function removeEVStyle() {
    var styleTags = document.head.getElementsByTagName('style');
    for (var i = 0; i < styleTags.length; i++) {
        var styleTag = styleTags[i];
        var styleSheet = styleTag.sheet;

        if (styleSheet) {
            var cssRules = styleSheet.cssRules || styleSheet.rules;
            for (var j = 0; j < cssRules.length; j++) {
                var cssRule = cssRules[j];
                if (cssRule.cssText.includes('data:font/woff2;base64,')) {
                    styleSheet?.ownerNode?.remove();
                }
            }
        }
    }
}

function removeEVScripts() {
    if (evSrc1?.parentNode?.removeChild) evSrc1.parentNode.removeChild(evSrc1);
    if (evSrc2?.parentNode?.removeChild) evSrc2.parentNode.removeChild(evSrc2);
    if (evSrc3?.parentNode?.removeChild) evSrc3.parentNode.removeChild(evSrc3);
}

function checkEagleView() {
    if (clientSettings?.["NewEV.ClientID"] && clientSettings?.["NewEV.EVClientSecret"] && UsingOSM) {
        if (activeParcel && $('.map-items option').length == 0) $('.map-show-ev').show();
        else $('.map-show-ev').hide();
        return true;
    }
    else {
        $('.map-ev').hide();
        return false;
    }
}

function showEagleViewInMap() {
    if (!checkEagleView()) {
        $('.map-items').val('');
        hideEagleView();
        messageBox("EagleView not loaded. Please try after few seconds or please review your licence.");
        return;
    }
    if (!navigator.onLine) {
        $('.map-items').val('');
        hideEagleView();
        console.error("Failed to connect to the internet.");
        messageBox("Sorry, EagleView map won't be available in offline mode.", function () {
            return false;
        });
    }
    else {
        showMask('Loading..');
        removeEVScripts();
        loadEVScripts(function () {
            removeEVStyle();
            hideSvOverlay();
            hideAllMapViews();
            $('#eagleView').show();
            $('#mapCanvas').hide();
            let apievUrl = "https://api.eagleview.com/auth-service/v1/token", apiClientId = clientSettings?.["NewEV.ClientID"], apiClientSecret = clientSettings?.["NewEV.EVClientSecret"], CENTER,
                authorization = btoa(apiClientId + ":" + apiClientSecret), requestOptions = {
                    method: "POST",
                    headers: {
                        "Authorization": `Basic ${authorization}`,
                        "Accept": "application/json",
                        "Content-Type": "application/x-www-form-urlencoded"
                    },
                    body: new URLSearchParams({
                        "grant_type": "client_credentials"
                    })
                };

            function getApiToken(callback) {
                fetch(apievUrl, requestOptions)
                    .then(response => {
                        if (!response.ok) {
                            throw new Error(`HTTP error! Status: ${response.status}`);
                        }
                        return response.json();
                    })
                    .then(data => {
                        if (callback) callback(data);
                    })
                    .catch(error => {
                        console.error(error);
                    });
            }

            function createShapes() {
                let shapes = [], polygon = [];
                for (x in activeParcel.MapPoints.sort(function (x, y) { return x.Ordinal - y.Ordinal })) {
                    var p = activeParcel.MapPoints[x];
                    if (shapes[p.RecordNumber || 0] == null) {
                        shapes[p.RecordNumber || 0] = [];
                    }
                    shapes[p.RecordNumber || 0].push([parseFloat(p.Longitude), parseFloat(p.Latitude)]);
                };

                shapes.forEach(function (w) {
                    polygon.push({
                        "type": "Feature",
                        "geometry": {
                            "type": "Polygon",
                            "coordinates": [w]
                        },
                        "properties": {
                            "eagleview": {
                                "style": {
                                    "size": 2,
                                    "color": "#FFF",
                                    "fill": "#f55f5f",
                                    "opacity": 0.5,
                                    "fillOpacity": 0.3
                                }
                            }
                        }
                    });
                });

                return polygon;
            }

            function initMap(token) {
                let mapEV = new window.ev.EmbeddedExplorer().mount('eagleView', {
                    authToken: token.access_token,
                    view: { lonLat: { lon: activeParcel.Center().lng, lat: activeParcel.Center().lat } }
                });
                mapEV.setLonLat({ lon: activeParcel.Center().lng, lat: activeParcel.Center().lat })
                mapEV.removeFeatures();
                mapEV.addFeatures({ geoJson: createShapes() });
                checkPictometry();
                $('.map-hide-pictometry').hide();
                checkImagery();
                $('.map-hide-Imagery').hide();
                checkWoolpert();
                $('.map-hide-woolpert').hide();
                if (clientSettings.EnableqPublic && clientSettings.EnableqPublic == '1') {
                    $('.q_public button').html('Show qPublic');
                    $('.q_public button').attr('onclick', 'show_qPublic();');
                }
                $('#maskLayer').hide();
                $('.map-show-nearmap').hide();
                $('.map-show-nearmapwms').hide();
                $('.map-show-ev').hide();
                if ($('.map-items option').length == 0) $('.map-hide-ev').show();
                $('#lblpop').hide();
                $('.heatmap-items').hide();
            }

            getApiToken((token) => {
                initMap(token);
            });
        });
    }
}

function hideEagleView(doNotRefresh) {
    if ($('.map-items option').length == 0 || ($('.map-items option').length > 0 && !activeParcel)) $('#mapCanvas').show();
    $('#eagleView').hide();
    if (checkEagleView()) {
        $('.map-hide-ev').hide();
        removeEVScripts();
        if (!doNotRefresh) {
            if (activeParcel) {
                $('#lblpop').hide();
                showSvOverlay();
                showGISInteractive();
            }
            else {
                $('#lblpop').show();
                $('header a.map-view', $('#sort-screen')).trigger('click');
                if (HeatLayer) $('.heatmap-items').show();
            }
        }
    }
}

function hideNearmap(doNotRefresh) {
	if($('.map-items option').length == 0  || ($('.map-items option').length >  0 && !activeParcel)) $('#mapCanvas').show();
	$('#nearmap').hide();
	if(checkNearMap()) {
		$('.map-hide-nearmap').hide();
		if (!doNotRefresh) {
			if (activeParcel) {
				$('#lblpop').hide();
				showSvOverlay();
				showGISInteractive();
			}
			else {
				$('#lblpop').show();
				$('header a.map-view', $('#sort-screen')).trigger('click');
				if (HeatLayer) $('.heatmap-items').show();
			}
		}
	}
}

function checkNearMap() {		
	if (clientSettings['EnableNearmap'] && clientSettings['EnableNearmap'].includes('MA') && clientSettings["Nearmap.APIKey"] && clientSettings["Nearmap.APIKey"].trim() !="" && UsingOSM) {
		if (activeParcel && $('.map-items option').length == 0) $('.map-show-nearmap').show();
		else $('.map-show-nearmap').hide();
		return true;
	}
	else {
		$('.map-nearmap').hide();
		return false;
	}
}

function initWMSUiElements() {
	dropdownElement = document.querySelector('select');
	selectedDisplayElement = document.querySelector('#selectedSurveyElementId');
	displayedDisplayElement = document.querySelector('#displayedSurveyElementId');

	northElement = document.querySelector('#northElementId');
	westElement = document.querySelector('#westElementId');
	southElement = document.querySelector('#southElementId');
	eastElement = document.querySelector('#eastElementId');
	vertElement = document.querySelector('#vertElementId');
}

function showNearmapWMS() {
	if (!checkNearMapWMS()) {
		$('.map-items').val('');
		hideNearmapWMS();
		messageBox("WMS Nearmap not loaded. Please try after few seconds or please review your licence.");
		return;
	}
	if (!navigator.onLine) {
		$('.map-items').val('');
		hideNearmapWMS();
		console.error("Failed to connect to the internet.");
		messageBox("Sorry, WMS Nearmap map won't be available in offline mode.", function () {
			return false;
		});
	} else {
		showMask('Loading..');
		hideSvOverlay();
		hideAllMapViews();
		$('#nearmapwms').show();
		$('#mapCanvas').hide();
		$('.map-items').val('NearmapWMS');
		var skipError = true;
		/*
		var vertical = L.tileLayer.wms('https://api.nearmap.com/wms/v1/latest/apikey/' + clientSettings["NearmapWMS.APIKey"] + '?service=WMS')
		,panorama = L.tileLayer.wms('https://api.nearmap.com/wms/v1/latest/apikey/MjEwZjJlYzQtMWMxOS00NmNlLTkyMjUtN2FhN2NmZGVkZWY4?service=WMS&request=GetCapabilities');

		var mapOptions = {
			zoom: 19,
			layers: [vertical]
		}

		if (!NearMapWMS) {
			NearMapWMS = L.map('nearmapwms', mapOptions);
			var baseMaps = {
				"Vertical": vertical
			};

			L.control.layers(baseMaps).addTo(NearMapWMS);

			vertical.on('tileerror', function () {
				if (skipError) {
					console.log('Error while loading tile');
					skipError = false;
				}
			});
		} else {
			for (i in NearMapWMS._layers) {
				if (NearMapWMS._layers[i]._path != undefined) {
					try {
						NearMapWMS.removeLayer(NearMap._layers[i]);
					}
					catch (e) {
						console.log("problem with " + e + NearMapWMS._layers[i]);
					}
				}
			}
		}
		if (activeParcel.MapPoints.length > 0) {
			var shapes = [];
			for (x in activeParcel.MapPoints.sort(function (x, y) { return x.Ordinal - y.Ordinal })) {
				var p = activeParcel.MapPoints[x];
				if (shapes[p.RecordNumber || 0] == null) {
					shapes[p.RecordNumber || 0] = [];
				}
				shapes[p.RecordNumber || 0].push([p.Latitude, p.Longitude]);
			};
			var polygon = [];
			shapes.forEach(function (w) {
				polygon.push(w);
			});
			L.polygon(polygon, { color: '#87CEEB' }).addTo(NearMapWMS);
			if (activeParcel.Center()) NearMapWMS.panTo(activeParcel.Center());
		}*/

        resetCustomWMS();

		if (NearMapWMS) NearMapWMS.invalidateSize();
		checkPictometry();
        $('.map-hide-pictometry').hide();
        checkEagleView();
        $('.map-hide-ev').hide();
        removeEVScripts();
		checkImagery();
		$('.map-hide-Imagery').hide();
		checkWoolpert();
		$('.map-hide-woolpert').hide();
		if (clientSettings.EnableqPublic && clientSettings.EnableqPublic == '1') {
			$('.q_public button').html('Show qPublic');
			$('.q_public button').attr('onclick', 'show_qPublic();');
		}
		$('#maskLayer').hide();
		$('.map-show-nearmapwms').hide()
		if ($('.map-items option').length == 0) $('.map-hide-nearmapwms').show();
		$('#lblpop').hide();
		$('.heatmap-items').hide();
	}
}

function hideNearmapWMS(doNotRefresh) {
	if ($('.map-items option').length == 0 || ($('.map-items option').length > 0 && !activeParcel)) $('#mapCanvas').show();
	$('#nearmapwms').hide();
	if (checkNearMapWMS()) {
		$('.map-hide-nearmapwms').hide();
		if (!doNotRefresh) {
			if (activeParcel) {
				$('#lblpop').hide();
				showSvOverlay();
				showGISInteractive();
			}
			else {
				$('#lblpop').show();
				$('header a.map-view', $('#sort-screen')).trigger('click');
				if (HeatLayer) $('.heatmap-items').show();
			}
		}
	}
}

function checkNearMapWMS() {
	if (clientSettings['EnableNearmapWMS'] && clientSettings['EnableNearmapWMS'].includes('MA') && clientSettings["NearmapWMS.APIKey"] && clientSettings["NearmapWMS.APIKey"].trim() != "" && UsingOSM) {
		if (activeParcel && $('.map-items option').length == 0) $('.map-show-nearmapwms').show();
		else $('.map-show-nearmapwms').hide();
		return true;
	}
	else {
		$('.map-nearmapwms').hide();
		return false;
	}
}

function showWoolpert(){
	$('#woolpert .ol-viewport').remove();
	if(!freshload){
		$('#layertype1').prop('checked',true).trigger('click');
	}
	else{
		freshload = false;
	}
	if(!checkWoolpert()){
		$('.map-items').val('');
		hideWoolpert();
		messageBox("Woolpert Imagery not loaded. Please try after few seconds or please review your licence.");
		return;
	}
	if (!navigator.onLine) {
		$('.map-items').val('');
		hideWoolpert();
		console.error("Failed to connect to the internet.");
		messageBox("Sorry, Woolpert map won't be available in offline mode.", function () {
			return false;
		});
	} else {
		showMask('Loading..');
		hideSvOverlay();
		hideAllMapViews();
		$('#woolpert').show();
		$('#mapCanvas').hide();
		$('.map-items').val('Woolpert Imagery');
		var skipError = true;
			
		var shapes = [];
		var woolpert6inchId=clientSettings['woolpert6inchId'];
		var woolpert3inchId=clientSettings['woolpert3inchId'];
		var woolpertApikey=clientSettings['woolpertApikey'];
	//var woolpertApikey=window.opener.clientSettings['woolpertApikey'];
    var woolpert6url = 'https://raster.stream.woolpert.io/layers/'+woolpert6inchId+'/tiles/{z}/{x}/{y}?key='+woolpertApikey,
        woolpertAttrib = '&copy; ' + 'https://woolpert.com',
        woolpert3url ='https://raster.stream.woolpert.io/layers/'+woolpert3inchId+'/tiles/{z}/{x}/{y}?key='+woolpertApikey ;
	
	var {Circle ,Fill, Stroke, Style} = ol.style;
		var GeoJSON =  ol.format.GeoJSON;
		var styles = [
		  new Style({
		    stroke: new Stroke({
		     color: '#EEE01E',
		      width: 3,
		    })
		  })];
	
	sixLayer= new ol.layer.Tile({
            source: new ol.source.XYZ({
              	url: 'https://raster.stream.woolpert.io/layers/'+ woolpert6inchId +'/tiles/{z}/{x}/{y}?key='+woolpertApikey
             
          }),
        		
                visible : true
            });
    threeLayer=  new ol.layer.Tile({
            source: new ol.source.XYZ({
              	url: 'https://raster.stream.woolpert.io/layers/'+ woolpert3inchId +'/tiles/{z}/{x}/{y}?key='+woolpertApikey
             
          }),
        
                visible : false
            });
	//var CENTER = [33.0119231687913,-96.865146328919];
  	var CENTER = [parcelMapCenter["lng"],parcelMapCenter["lat"]];
	//var CENTER = [window.opener.activeParcel.Center().lng(), window.opener.activeParcel.Center().lat()]
   	view= new ol.View({
           // center : [-8912180.687388677, 3104482.73783103],
            center:ol.proj.transform(CENTER,  'EPSG:4326', 'EPSG:3857'),
            zoom : 19,
            maxZoom : 20,
            minZoom : 4,
     		rotation: 0
        })
    WoolPert = new ol.Map({
        view : view,
        controls: [
              new ol.control.Zoom
        ],
        layers : [
         	  sixLayer,threeLayer
        ],
        target : 'woolpert'
    });
    var View = WoolPert.getView();
			var zoom = View.getZoom();
			var center = View.getCenter();
			var rotation = View.getRotation();
	if (activeParcel.MapPoints.length > 0) {
	        for (x in activeParcel.MapPoints.sort(function(x, y) {
	                return x.Ordinal - y.Ordinal
	            })) {
	            var p = activeParcel.MapPoints[x];
				if (shapes[p.RecordNumber || 0] == null) {
                    shapes[p.RecordNumber || 0] = [];
                }
	            shapes[p.RecordNumber || 0].push(ol.proj.transform([ p.Longitude, p.Latitude],  'EPSG:4326', 'EPSG:3857'));
	        };
	}
	var multiJSON= [];
	shapes.forEach((item) =>{
		multiJSON.push({"type": "Feature","properties": {},"geometry": {"type": "Polygon","coordinates": [item]}})
	})
    
    geoJSON = new ol.layer.VectorImage({
	    source : new ol.source.Vector({
	            features: new ol.format.GeoJSON().readFeatures({
                "type": "FeatureCollection",
                "features": multiJSON
              }),
	    }),
	    style : styles,
	    visible : true
	});

 	WoolPert.addLayer(geoJSON);
 	//Setting map zoom to fit the Polygons
 	var extent = geoJSON.getSource().getExtent();
	WoolPert.getView().fit(extent, {size:WoolPert.getSize(), maxZoom:19});
	var View = WoolPert.getView();
	var zoom = View.getZoom();
	var center = View.getCenter();
	var rotation = View.getRotation();

	//Reset Button in Woolpert Imagery
	document.getElementById('zoom-restore').onclick = function() {
		View.setCenter(ol.proj.transform(CENTER,  'EPSG:4326', 'EPSG:3857'));
		View.setRotation(rotation);
		View.setZoom(zoom);
		};
		
	}
		checkPictometry();
        $('.map-hide-pictometry').hide();
        checkEagleView();
        $('.map-hide-ev').hide();
        checkImagery();
        removeEVScripts();
		$('.map-hide-Imagery').hide();
		checkNearMap();
		$('.map-hide-nearmap').hide();
		checkNearMapWMS();
		$('.map-hide-nearmapwms').hide();
		
		if(clientSettings.EnableqPublic && clientSettings.EnableqPublic == '1'){
			$('.q_public button').html('Show qPublic');
			$('.q_public button').attr('onclick','show_qPublic();');
		}
		$('#maskLayer').hide();
		$('.map-show-woolpert').hide()
		if($('.map-items option').length == 0) $('.map-hide-woolpert').show();
		$('#lblpop').hide();
		$('.heatmap-items').hide();
	
}

function layerSelect(){
 var threeInch= document.getElementById('layertype2').checked;
 if(threeInch){
 	sixLayer.setVisible(false);
  	threeLayer.setVisible(true);
 }
 else
 {
 	sixLayer.setVisible(true);
  	threeLayer.setVisible(false);
  	}
   }

function hideWoolpert(doNotRefresh) {
	if($('.map-items option').length == 0  || ($('.map-items option').length >  0 && !activeParcel)) $('#mapCanvas').show();
	$('#woolpert').hide();
	if(checkWoolpert()) {
		$('.map-hide-woolpert').hide();
		if (!doNotRefresh) {
			if (activeParcel) {
				$('#lblpop').hide();
				showSvOverlay();
				showGISInteractive();
			}
			else {
				$('#lblpop').show();
				$('header a.map-view', $('#sort-screen')).trigger('click');
				if (HeatLayer) $('.heatmap-items').show();
			}
		}
	}
}


function checkWoolpert() {		
	if (clientSettings['woolpertApikey'] && clientSettings['EnableWoolpert'] && clientSettings['EnableWoolpert'].includes('MA') && clientSettings["woolpertApikey"].trim() !="" && UsingOSM) {
		if (activeParcel && $('.map-items option').length == 0) {$('.map-show-woolpert').show();$('#woolpert').hide()}
		else $('.map-show-woolpert').hide();
		return true;
	}
	else {
		$('.map-woolpert').hide();
		return false;
	}
}
function showImagery(){
	if(!checkImagery()){
		$('.map-items').val('');
		hideImagery();
		messageBox("Imagery not loaded. Please try after few seconds or please review your settings.");
		return;
	}
	if (!navigator.onLine) {
		$('.map-items').val('');
		hideImagery();
		console.error("Failed to connect to the internet.");
		messageBox("Sorry, Imagery won't be available in offline mode.", function () {
			return false;
		});
	} else {
		showMask('Loading..');
		hideAllMapViews();
		if(clientSettings.EnableNearmap && clientSettings.EnableNearmap.includes('MA') && UsingOSM)
			CLayer ? OsMap.removeLayer(nearmapStreets) : OsMap.removeLayer(googleHybrid);
		else if (clientSettings.EnableNearmapWMS && clientSettings.EnableNearmapWMS.includes('MA') && UsingOSM)
			CLayer ? OsMap.removeLayer(nearmapWMSStreets) : OsMap.removeLayer(googleHybrid);
        else
        	CLayer ? OsMap.removeLayer(Aerial) : OsMap.removeLayer(googleHybrid);
        customCountyWMS.addTo(OsMap);
        if(OsMap) OsMap.invalidateSize();
        hideSvOverlay();
        $('#mapCanvas').show();
        checkPictometry();
        $('.map-hide-pictometry').hide();
        checkEagleView();
        $('.map-hide-ev').hide();
        removeEVScripts();
		checkNearMap();
		$('.map-hide-nearmap').hide();
		checkWoolpert();
		$('.map-hide-woolpert').hide();
		if(clientSettings.EnableqPublic && clientSettings.EnableqPublic == '1'){
			$('.q_public button').html('Show qPublic');
			$('.q_public button').attr('onclick','show_qPublic();');
		}
	       $('#maskLayer').hide();
		$('.map-show-Imagery').hide()
		if($('.map-items option').length == 0) $('.map-hide-Imagery').show();
		$('#lblpop').hide();
		$('.heatmap-items').hide();
		$('.map-items').val('Web Service Imagery');
	    }
}

function checkImagery() {		
	if (clientSettings['customWMS'] && clientSettings['customWMS'].trim() != '' && clientSettings['EnableCustomWMS'] && clientSettings['EnableCustomWMS'].includes('MA') && UsingOSM) {
		if (activeParcel && $('.map-items option').length == 0) $('.map-show-Imagery').show();
		else $('.map-show-Imagery').hide();
		return true;
	}
	else {
		$('.map-Imagery').hide();
		return false;
	}
}

function hideImagery() {
	if($('.map-items option').length == 0  || ($('.map-items option').length >  0 && !activeParcel)) {
		showSvOverlay();
		$('#mapCanvas').show();
	}
	if(checkImagery()) {
		$('.map-hide-Imagery').hide();
	        OsMap.removeLayer(customCountyWMS);
	     	if(clientSettings.EnableNearmap && clientSettings.EnableNearmap.includes('MA') && UsingOSM)
				  CLayer ? OsMap.removeLayer(nearmapStreets) : OsMap.removeLayer(googleHybrid);
			else if (clientSettings.EnableNearmapWMS && clientSettings.EnableNearmapWMS.includes('MA') && UsingOSM)
				  CLayer ? OsMap.removeLayer(nearmapWMSStreets) : OsMap.removeLayer(googleHybrid);
        	else
        		CLayer ? OsMap.removeLayer(Aerial) : OsMap.removeLayer(googleHybrid);
	}
}

function showDefaultImagery(){
    if(clientSettings.DefaultImagery){
        switch(clientSettings.DefaultImagery.toLowerCase()){
            case 'webservice' :
                showImagery();
                break;
            case 'eagleview' :
                showPictometryInMap();
                break;
            case 'eagleviewNew':
                showEagleViewInMap();
                break;
            case 'woolpert' :
                showWoolpert();
                break;
            case 'nearmap' :
                showNearmap();
				break;
			case 'nearmapwms':
				showNearmapWMS();
				break;
            case 'qpublic' :
                show_qPublic();
                break;
        }
    }
}

function setMapItems(showHide){
	if(clientSettings.DefaultImagery){
		if(showHide){
			$('.osmapsetup button').hide();
			$('.map-items option').length > 1 ? $('.map-items').show() : $('.map-items').hide();
			return;
		}
		$('.map-items').empty();
        var mapItems = [{ map: 'Nearmap', Enabled: checkNearMap() }, { map: 'Nearmapwms', Enabled: checkNearMapWMS() }, { map: 'Woolpert Imagery', Enabled: checkWoolpert() }, { map: 'Eagle View', Enabled: checkPictometry() }, { map: 'EagleViewNew', Enabled: checkEagleView() }, { map : 'Web Service Imagery', Enabled : checkImagery() } , { map : 'qPublic', Enabled : clientSettings.EnableqPublic && clientSettings.EnableqPublic == '1' }];
		if(mapItems.filter((x) => { return x.Enabled }).length > 1){
			$('<option value>--Select--</option>').appendTo('.map-items');
			mapItems.filter((x) => { return x.Enabled }).forEach(function(item){
				$('.map-items').append($('<option></option>').attr('value',item.map).text(item.map))
			});
			$('.osmapsetup button').hide();
			$('.map-items').off('change');
			$('.map-items').on('change', function () {
				//Moved switch condition to a new function
				toggleMapView($(this).val());
			});
		} else{
			$('.map-items').hide();
		}
	}
	else {
		if (showHide) {
			$('.map-items option').length > 1 ? $('.map-items').show() : $('.map-items').hide();
			return;
		}
		$('.map-items').empty();
        var mapItems = [{ map: 'Nearmap', Enabled: checkNearMap() }, { map: 'NearmapWMS', Enabled: checkNearMapWMS() }, { map: 'Woolpert Imagery', Enabled: checkWoolpert() }, { map: 'Eagle View', Enabled: checkPictometry() }, { map: 'EagleViewNew', Enabled: checkEagleView() }, { map : 'Web Service Imagery', Enabled : checkImagery() } , { map : 'qPublic', Enabled : clientSettings.EnableqPublic && clientSettings.EnableqPublic == '1' }];
		if (mapItems.filter((x) => { return x.Enabled }).length > 1) {
			$('<option value>--Select Map--</option>').appendTo('.map-items');
			mapItems.filter((x) => { return x.Enabled }).forEach(function (item) {
				$('.map-items').append($('<option></option>').attr('value', item.map).text(item.map))
			});
			$('.osmapsetup button').hide();
			$('.map-items').off('change');
			$('.map-items').blur();
			$('.map-items').on('change', function () {
				$('.map-items').blur();
				//Moved switch condition to a new function toggleMapView()
				toggleMapView($(this).val());
			});
		} else {
			$('.map-items').hide();
		}
	}
}

//Hides all mapviews initially before showing based on current selection
function hideAllMapViews() {
	if (checkNearMap()) hideNearmap(true);
	if (checkNearMapWMS()) hideNearmapWMS(true);
    if (checkPictometry()) hidePictometryInMap(true);
    if (checkEagleView()) hideEagleView(true);
	if (checkImagery()) hideImagery(true);
	if (checkWoolpert()) hideWoolpert(true);
	if (clientSettings.EnableqPublic && clientSettings.EnableqPublic == '1') hide_qPublic(true);
}

//Changed map-items on change switch to this function
function toggleMapView(currentView) {
	$('#mapCanvas').show();	
	hideAllMapViews();
	let svFlag = true;
	switch (currentView) {
		case 'Woolpert Imagery':
			showWoolpert();
			break;
		case 'Nearmap':
			showNearmap();
			break;
		case 'NearmapWMS':
			showNearmapWMS();
			break;
		case 'Eagle View':
			showPictometryInMap();
            break;
        case 'EagleViewNew':
            showEagleViewInMap();
            break;
		case 'qPublic':
			show_qPublic();
			break;
		case 'Web Service Imagery':
			showImagery();
			break;
		default:
			svFlag = false;
			showSvOverlay();
	}
	if (svFlag) hideSvOverlay();
}

function hideSvOverlay() {
	if (activeParcel && clientSettings["EnableSVInMA"] == "1") {
		$('.svMA').hide();  
		$('.svRoot').hide(); 
		$('.svDropdown').hide();
		$('.sketchOverlay').hide();
		setScreenDimensions();
	}
}

function showSvOverlay() {
	if (activeParcel && clientSettings["EnableSVInMA"] == "1") {
		$('.svMA').show();   
		$('.svRoot').show(); 
		$('.svDropdown').show();
		$('.sketchOverlay').show();
		setScreenDimensions();
	}
}

var wmsOlMap = null, wmsdraw;

function resetCustomWMS() {
    $('#wms-maskwms').show();
    let nearmapWMSApiKey = clientSettings["NearmapWMS.APIKey"], CENTER = [-82.6367814985797, 41.4111097499755], flag = false, MIN_ZOOM = 12,
        MAX_ZOOM = 24, shapes = [], ZOOM = 20, helpTooltip, helpTooltipElement, measureTooltipElement, measureTooltip,
        typeSelect = document.getElementsByName('measure-selector'), geoJSON;
    function urlTemplate(z, x, y, survey, layer) {
        var until = '';
        if (survey) until = '&until=' + survey;
        return 'https://api.nearmap.com/tiles/v3/' +
            layer + '/' + z + '/' + x + '/' + y +
            '.img?tertiary=satellite&apikey=' + nearmapWMSApiKey + until;
    };

    function wmsUrlTemplate() {
        return 'https://api.nearmap.com/wms/v1/latest/' +
            '?apikey=' + nearmapWMSApiKey +
            '&limit=1000';
    }

    function degreesToRadians(deg) {
        return deg * (Math.PI / 180);
    }

    function radiansToDegrees(rad) {
        return rad / (Math.PI / 180);
    }

    function modulus360(deg) {
        if (deg < 0) return 360 + (deg % 360);
        return deg % 360;
    }

    let VERT = 0, NORTH = 0, EAST = 90, SOUTH = 180, WEST = 270, HEADINGS = { Vert: VERT, North: NORTH, East: EAST, South: SOUTH, West: WEST };

    function addProj(code, worldWidth, worldHeight) {
        let projection = new ol.proj.Projection({
            code: code,
            extent: [0, 0, worldWidth, worldHeight],
            worldExtent: [-180, -85, 180, 85],
            units: 'pixels'
        });

        ol.proj.addProjection(projection);

        let cx = worldWidth / 2, cy = worldHeight / 2;

        let pixelsPerLonDegree = worldWidth / 360, pixelsPerLatRadian = worldHeight / (2 * Math.PI);

        ol.proj.addCoordinateTransforms(
            'EPSG:4326',
            projection,
            function (coord) {
                let lng = coord[0], lat = coord[1], x = cx + lng * pixelsPerLonDegree, theta = degreesToRadians(lat), tanTheta = Math.tan(theta),
                    secTheta = 1 / Math.cos(theta), latRadians = Math.log(tanTheta + secTheta), y = cy + latRadians * pixelsPerLatRadian;

                return [x, y];
            },
            function (coord) {
                let x = coord[0], y = coord[1], lng = (x - cx) / pixelsPerLonDegree, latRadians = (y - cy) / -pixelsPerLatRadian,
                    lat = -radiansToDegrees(2 * Math.atan(Math.exp(latRadians)) - Math.PI / 2);

                return [lng, lat];
            }
        );
    };

    addProj('NMV:000', 256, 256);
    addProj('NMO:NS', 256, 192);
    addProj('NMO:EW', 192, 256);

    let ProjLatLng = ol.proj.get('EPSG:4326'), ProjVertical = ol.proj.get('NMV:000'), 
        ProjObliqueNS = ol.proj.get('NMO:NS'), ProjObliqueEW = ol.proj.get('NMO:EW'),
        PROJECTIONS = { North: ProjObliqueNS, East: ProjObliqueEW, South: ProjObliqueNS, West: ProjObliqueEW, Vert: ProjVertical };


    function fetchImageData(url) {
        return fetch(url, { redirect: 'manual' })
            .then(function (resp) {
                if (resp.status === 200) {
                    return resp.blob()
                        .then(function (data) {
                            return window.URL.createObjectURL(data);
                        });
                }
                else if (!resp.ok) {
                    if (flag == false) {
                        var imag = layerType == 'Vert' ? 'Vertical' : layerType;
                        messageBox('WMS Nearmap ' + imag + ' imagery is not available for the selected survey.');
                        resetCustomWMS()
                        flag = true;
                    }
                }

                return null;
            })
            .catch(function (reason) {
                return null;
            });
    }

    function loadImage(data) {
        return new Promise(function (resolve) {
            var img = document.createElement('img');
            img.addEventListener('load', function () { resolve(img); });
            img.addEventListener('error', function () { resolve(img); });
            img.src = data;
        });
    }

    function rotateImage(ctx, img, tileWidth, tileHeight, heading) {
        let rotation = degreesToRadians(heading);

        ctx.save();

        ctx.translate(tileWidth / 2, tileHeight / 2);
        ctx.rotate(rotation);

        switch (heading) {
            case NORTH:
            case SOUTH:
                ctx.drawImage(img, -tileWidth / 2, -tileHeight / 2, tileWidth, tileHeight);
                break;
            case EAST:
            case WEST:
                ctx.drawImage(img, -tileHeight / 2, -tileWidth / 2, tileHeight, tileWidth);
                break;
        }

        ctx.restore();
    }

    function createCanvas(width, height) {
        var canvas = document.createElement('canvas');
        canvas.width = width;
        canvas.height = height;

        return [canvas, canvas.getContext('2d')];
    }

    function rotateTile(data, tileDims, heading) {
        var tileWidth = tileDims[0];
        var tileHeight = tileDims[1];
        var canvasAndCtx = createCanvas(tileWidth, tileHeight);
        var canvas = canvasAndCtx[0];
        var ctx = canvasAndCtx[1];

        return loadImage(data)
            .then(function (img) {
                rotateImage(ctx, img, tileWidth, tileHeight, heading);

                return canvas.toDataURL();
            });
    }

    let layerType = 'Vert', availableSurveys = [], dropdownElement = null, selectedDisplayElement = null, displayedDisplayElement = null,
        northElement = null, westElement = null, southElement = null, eastElement = null, vertElement = null;

    let selectedSurvey = {
        survey: null,
        get value() {
            return this.survey;
        },
        set value(value) {
            this.survey = value;
            selectedDisplayElement.innerHTML = value;
        }
    };

    let displayedSurvey = {
        survey: null,
        get value() {
            return this.survey;
        },
        set value(value) {
            this.survey = value;
            displayedDisplayElement.innerHTML = value;
        }
    };

    let TILE_SIZES = {
        North: [256, 192],
        East: [192, 256],
        South: [256, 192],
        West: [192, 256],
        Vertical: [256, 256]
    };

    function toViewRotation(heading) {
        return degreesToRadians(modulus360(360 - heading));
    }

    function tileUrlFunction(tileCoord) {
        var z = tileCoord[0];
        var x = tileCoord[1];
        var y = tileCoord[2];

        return urlTemplate(z, x, y, selectedSurvey.value, layerType);
    }

    function tileLoadFunction(imageTile, src) {
        var img = imageTile.getImage();

        fetchImageData(src)
            .then(function (imgData) {
                if (imgData && layerType !== 'Vert') {
                    rotateTile(imgData, TILE_SIZES[layerType], HEADINGS[layerType])
                        .then(function (rotatedImgData) {
                            img.src = rotatedImgData || '';
                        });
                }

                img.src = imgData || '';
            });
    }

    function createView(zoom, center) {
        zoom = zoom || ZOOM;
        center = center || CENTER;

        return new ol.View({
            projection: PROJECTIONS[layerType],
            rotation: toViewRotation(HEADINGS[layerType]),
            center: ol.proj.fromLonLat(center, PROJECTIONS[layerType]),
            minZoom: MIN_ZOOM,
            maxZoom: MAX_ZOOM,
            zoom: zoom
        });
    }

    function createLayer() {
        return new ol.layer.Tile({
            source: new ol.source.XYZ({
                projection: PROJECTIONS[layerType],
                tileSize: TILE_SIZES[layerType],
                tileUrlFunction: tileUrlFunction,
                tileLoadFunction: tileLoadFunction
            })
        });
    }

    /**
     * Called when the selected survey date does not exist in the current list of available survey dates.
     * It returns the closest available date.
     *
     * @param {*} surveys            available surveys returned by the coverage API
     * @param {*} selectedDate       user's selected survey date
     */
    function findClosestDate(surveys, selectedDate) {
        var selectedDateInMs = selectedDate ? +new Date(selectedDate) : +new Date();
        var deltaInMs = surveys.map(function (survey) {
            var surveyDateInMs = +new Date(survey.captureDate);
            return Math.abs(selectedDateInMs - surveyDateInMs);
        });

        var closestDateInMs = Math.min.apply(null, deltaInMs);
        return surveys[deltaInMs.findIndex(function (ms) { return ms === closestDateInMs; })].captureDate;
    };

    /**
     * @param {*} availableSurveys    available surveys returned by the coverage API
     * @param {*} selectedDate        user's selected survey date
     */
    function getSurveyDate(availableSurveys, selectedDate) {
        // No dates available
        if (availableSurveys.length === 0) {
            return null;
        }

        // Selects the selected survey date when available
        if (availableSurveys.find(function (survey) { return selectedDate === survey.captureDate; })) {
            return selectedDate;
        }

        // Searches for the closest available survey date when not available
        if (findClosestDate(availableSurveys, selectedDate)) {
            var value = selectedSurvey.value;
            selectedSurvey.value = findClosestDate(availableSurveys, selectedDate);
            if (value && value !== selectedSurvey.value)
                refreshView();
        }
        return findClosestDate(availableSurveys, selectedDate);
    }

    /**
     * Fetches Nearmap wmsUrl API.
     */
    function fetchWMS() {
        /*var bounds = getBounds(olMap);*/
        var wmsUrl = wmsUrlTemplate();

        return fetch(wmsUrl)
            .then(function (response) {
                return response.json();
            });
    }

    /**
     * Refreshes the map tiles whenever the selected survey date changes.
     */
    function refreshTiles() {
        flag = false;
        wmsOlMap
            .getLayers()
            .item(0)
            .getSource()
            .refresh();
    }

    /**
     * Refreshes the map tiles whenever projection changes.
     */
    function clearLayrs() {
        if (wmsdraw)
            wmsOlMap.removeInteraction(wmsdraw);
        if (helpTooltip)
            wmsOlMap.removeOverlay(helpTooltip);
        if (measureTooltip)
            wmsOlMap.addOverlay(measureTooltip);
        document.getElementsByClassName('measureTools')[1].style.display = "block";
        document.getElementById('option-onewms').checked = true;
    }


    function refreshView() {
        if (layerType == 'Vert') {
            clearLayrs();
            resetCustomWMS();
            
        } else {
            var currentProject = wmsOlMap.getView().getProjection();
            var currentCenter = ol.proj.fromLonLat(CENTER, PROJECTIONS[layerType])
            var zoom = wmsOlMap.getView().getZoom();
            var center = ol.proj.toLonLat(currentCenter, currentProject);

            wmsOlMap.setView(createView(zoom, center));
            wmsOlMap.getLayers().clear();
            wmsOlMap.getLayers().push(createLayer());

            document.getElementsByClassName('measureTools')[1].style.display = "none";
            if (wmsdraw)
                wmsOlMap.removeInteraction(wmsdraw);
            if (helpTooltip)
                wmsOlMap.removeOverlay(helpTooltip);
        }
    }


    /**
     * Displays all available survey dates in a dropdown.
     */
    function updateDropDown() {
        // Clears up previous options
        dropdownElement.innerHTML = '';

        // Creates the content of options for select element
        availableSurveys.forEach(function (survey) {
            var optionElement = document.createElement('option');
            optionElement.setAttribute('value', survey.captureDate);
            optionElement.innerText = survey.captureDate;

            dropdownElement.add(optionElement);
        });

        // Assigns default select value
        dropdownElement.value = displayedSurvey.value;
    }


    function getAvailableSurveyDates(response) {
        var surveys = response && response.surveys ? response.surveys : [];

        return surveys.filter(function (survey) {
            return (survey.resources.tiles || [])
                .some(function (tile) { return tile.type === layerType; });
        });
    }

    /**
     * onMapMoveHandler contains logics how to deal with coverage api while map is moving
     */
    function onMapMoveHandler() {
        // Fetches Nearmap coverage API based current view port
        fetchWMS()
            .then(function (response) {
                // Updates internal `availableSurveys` and `displayedSurvey` members
                availableSurveys = getAvailableSurveyDates(response);
                displayedSurvey.value = getSurveyDate(availableSurveys, selectedSurvey.value);

                // Updates available surveys dropdown options
                updateDropDown();
            });
    }

    function initUiElements() {
        dropdownElement = document.querySelector('select');
        selectedDisplayElement = document.querySelector('#selectedSurveyElementIdwms');
        displayedDisplayElement = document.querySelector('#displayedSurveyElementIdwms');

        northElement = document.querySelector('#northElementIdwms');
        westElement = document.querySelector('#westElementIdwms');
        southElement = document.querySelector('#southElementIdwms');
        eastElement = document.querySelector('#eastElementIdwms');
        vertElement = document.querySelector('#vertElementIdwms');
    }

    /**
     * Adds event listeners to map instance and UI elements.
     */
    function addEventListeners() {
        // Adds map moving (panning and zooming) listener
        wmsOlMap.on('moveend', function () {
            onMapMoveHandler(dropdownElement);
        });

        // Adds "onChange" listener to the dropdown
        dropdownElement.addEventListener('change', function (evt) {
            selectedSurvey.value = evt.target.value;
            displayedSurvey.value = evt.target.value;

            refreshTiles();
        });

        [northElement, westElement, southElement, eastElement, vertElement]
            .forEach(function (element) {
                element.addEventListener('click', function (evt) {
                    layerType = evt.target.value;
                    flag = false;
                    refreshView();
                    refreshView();
                });
            });
    }

    if (activeParcel.MapPoints.length > 0) {
        CENTER = [activeParcel.Center().lng, activeParcel.Center().lat]
    }

    let VectorSource = ol.source.Vector;
    let { Tile, Vector } = ol.layer; //import {Tile as TileLayer, Vector as VectorLayer} from 'ol/layer.js';
    let TileLayer = Tile, VectorLayer = Vector;
    let { Circle, Fill, Stroke, Style } = ol.style;
    let GeoJSON = ol.format.GeoJSON, MultiPoint = ol.geom.MultiPoint;
    let styles = [
        new Style({
            stroke: new Stroke({
                color: '#1ab7ea',
                width: 3,
            })
        })];

    if (activeParcel.MapPoints.length > 0) {
        for (x in activeParcel.MapPoints.sort(function (x, y) {
            return x.Ordinal - y.Ordinal
        })) {
            let p = activeParcel.MapPoints[x];
            if (shapes[p.RecordNumber || 0] == null) {
                shapes[p.RecordNumber || 0] = [];
            }
            shapes[p.RecordNumber || 0].push(ol.proj.transform([p.Longitude, p.Latitude], 'EPSG:4326', 'NMV:000'));
        };
    }

    let multiJSON = [];
    shapes.forEach((item) => {
        multiJSON.push({ "type": "Feature", "properties": {}, "geometry": { "type": "Polygon", "coordinates": [item] } })
    });

    geoJSON = new ol.layer.VectorImage({
        source: new ol.source.Vector({
            features: new ol.format.GeoJSON().readFeatures({
                "type": "FeatureCollection",
                "features": multiJSON
            }),
        }),
        style: styles,
        visible: true
    });

    if (wmsOlMap) {
        wmsOlMap.setTarget(null);
        wmsOlMap.dispose();
    }
    wmsOlMap = new ol.Map({
        target: 'nearmapwms',
        controls: [new ol.control.Zoom()],
        layers: [createLayer()],
        view: createView()
    });

    wmsOlMap.addLayer(geoJSON);

    initUiElements();
    addEventListeners();

    let { getArea, getLength } = ol.sphere;
    let { unByKey } = ol.Observable;
    let sketch, Draw = ol.interaction.Draw, Overlay = ol.Overlay, View = ol.View, CircleStyle = ol.style.Circle, Icon = ol.style.Icon;
    let { LineString, Polygon } = ol.geom;
    let source = new VectorSource();
    let vector = new VectorLayer({
        source: source,
        view: new View({
            projection: 'EPSG:4326',
        }),
        style: new Style({
            fill: new Fill({
                color: 'rgba(255, 255, 255, 0.2)',
            }),
            stroke: new Stroke({
                color: '#ffcc33',
                width: 2,
            }),
            image: new CircleStyle({
                radius: 7,
                fill: new Fill({
                    color: '#ffcc33',
                }),
            }),
        }),
    });

    wmsOlMap.addLayer(vector);

    for (var i = 0; i < typeSelect.length; i++) {
        typeSelect[i].addEventListener('change', function () {
            if (this.value == 'none') {
                wmsOlMap.removeInteraction(wmsdraw);
                wmsOlMap.removeOverlay(helpTooltip);
            } else {
                wmsOlMap.removeInteraction(wmsdraw);
                addInteraction(this.value);
            }
        });
    }

    let continuePolygonMsg = 'Click to continue drawing the polygon';
    let continueLineMsg = 'Click to continue drawing the line';
    let pointerMoveHandler = function (evt) {
        if (evt.dragging) {
            return;
        }
        let helpMsg = 'Click to start drawing';
        if (sketch) {
            const geom = sketch.getGeometry();
            if (geom instanceof Polygon) {
                helpMsg = continuePolygonMsg;
            } else if (geom instanceof LineString) {
                helpMsg = continueLineMsg;
            }
        }
        helpTooltipElement.innerHTML = helpMsg;
        helpTooltip.setPosition(evt.coordinate);
        helpTooltipElement.classList.remove('hidden');
    };

    function getDistanceBetweenPoints(lat1, lng1, lat2, lng2) {
        // The radius of the planet earth in meters
        let R = 6378137;
        let dLat = degreesToRadians(lat2 - lat1);
        let dLong = degreesToRadians(lng2 - lng1);
        let a = Math.sin(dLat / 2)
            *
            Math.sin(dLat / 2)
            +
            Math.cos(degreesToRadians(lat1))
            *
            Math.cos(degreesToRadians(lat1))
            *
            Math.sin(dLong / 2)
            *
            Math.sin(dLong / 2);

        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        let distance = R * c;

        return distance;
    }

    function degreesToRadians(degrees) {
        return degrees * Math.PI / 180;
    }

    var formatLength = function (line) {
        var length;
        var coordinates = line.getCoordinates();
        length = 0;
        var sourceProj = wmsOlMap.getView().getProjection();
        for (var i = 0, ii = coordinates.length - 1; i < ii; ++i) {
            var c1 = ol.proj.transform(coordinates[i], sourceProj, 'EPSG:4326');
            var c2 = ol.proj.transform(coordinates[i + 1], sourceProj, 'EPSG:4326');
            length += getDistanceBetweenPoints(c1[1], c1[0], c2[1], c2[0]);
        }

        var output;
        if (length > 100) {
            output = length / 1000
                + ' km<br/>' + Math.round(length * 3.2808 * 1000) / 1000 + ' Ft';
        } else {
            output = Math.round(length * 1000) / 1000
                + ' M<br/>' + Math.round(length * 3.2808 * 1000) / 1000 + ' Ft';
        }
        return output;
    };

    var formatArea = function (polygon) {
        var op = { 'projection': 'EPSG:4326' };
        const area = getArea(polygon, op);
        let output;
        if (area > 10000) {
            output = Math.round((area / 1000000) * 100) / 100 + ' ' + ' km<sup>2</sup><br/>' + Math.round(area * 100 * 10.764) / 100 + ' Sq.Ft';
        } else {

            output = Math.round(area * 100) / 100 + ' ' + ' m<sup>2</sup><br/>' + Math.round(area * 100 * 10.764) / 100 + ' Sq.Ft';
        }
        return output;
    };

    function addInteraction(typeOp) {
        wmsOlMap.on('pointermove', pointerMoveHandler);
        wmsOlMap.getViewport().addEventListener('mouseout', function () {
            helpTooltipElement.classList.add('hidden');
        });
        const type = typeOp == 'area' ? 'Polygon' : 'LineString';
        wmsdraw = new Draw({
            source: source,
            type: type,
            style: new Style({
                fill: new Fill({
                    color: 'rgba(255, 255, 255, 0.2)',
                }),
                stroke: new Stroke({
                    color: 'rgba(0, 0, 0, 0.5)',
                    lineDash: [10, 10],
                    width: 2,
                }),
                image: new Icon({
                    anchor: [-1, 15],
                    anchorXUnits: 'pixels',
                    anchorYUnits: 'pixels',
                    src: '/static/css/images/pencil.png',

                }),
            }),
        });
        wmsOlMap.addInteraction(wmsdraw);
        createMeasureTooltip();
        createHelpTooltip();

        let listener;
        wmsdraw.on('drawstart', function (evt) {
            sketch = evt.feature;
            console.log(wmsOlMap.group);
            let tooltipCoord = evt.coordinate;
            let elements = document.getElementsByClassName('opacity-group');
            for (var i = 0; i < elements.length; i++) {
                elements[i].classList.add("avoidclicks");
                elements[i].style.opacity = .3;
            }
            document.getElementsByClassName('drawUndo')[1].style.display = "inline-block";
            document.getElementsByClassName('heading-btn')[1].classList.add("avoidclicks");
            document.getElementsByClassName('heading-btn')[1].style.opacity = .3;
            if ($('.ol-tooltip-measure')[0].style.display == 'none') $('.ol-tooltip-measure')[0].style.display = '';

            $('.drawUndo').off(touchClickEvent);
            $('.drawUndo').on(touchClickEvent, function () {
                if (document.getElementsByClassName('drawUndo')[1].classList.contains('avoidclicks')) {
                    return;
                }
                else {
                    if (wmsdraw?.sketchCoords_) {
                        if (wmsdraw.type_ == 'LineString') {
                            if (wmsdraw.sketchCoords_.length > 0) wmsdraw.removeLastPoint();
                            if (wmsdraw.sketchCoords_.length == 1) {
                                $('.ol-tooltip-measure')[0].style.display = 'none';
                                document.getElementsByClassName('drawUndo')[1].classList.add("avoidclicks");
                            }
                        }
                        else {
                            if (wmsdraw.sketchCoords_[0].length > 0) wmsdraw.removeLastPoint();
                            if (wmsdraw.sketchCoords_[0].length == 1) {
                                $('.ol-tooltip-measure')[0].style.display = 'none';
                                document.getElementsByClassName('drawUndo')[1].classList.add("avoidclicks");
                            }

                        }
                    }
                }
                return false;
            });

            listener = sketch.getGeometry().on('change', function (evt) {
                const geom = evt.target;
                let output;
                if (geom instanceof Polygon) {
                    output = formatArea(geom);
                    tooltipCoord = geom.getInteriorPoint().getCoordinates();
                } else if (geom instanceof LineString) {
                    output = formatLength(geom);
                    tooltipCoord = geom.getLastCoordinate();
                }
                measureTooltipElement.innerHTML = output;
                measureTooltip.setPosition(tooltipCoord);
                if (wmsdraw?.sketchCoords_) {
                    if (wmsdraw.type_ == 'LineString' && wmsdraw.sketchCoords_.length > 1) {
                        if ($('.ol-tooltip-measure')[0].style.display == 'none') $('.ol-tooltip-measure')[0].style.display = '';
                        document.getElementsByClassName('drawUndo')[1].classList.remove("avoidclicks");
                    }
                    else if (wmsdraw.type_ == 'Polygon' && wmsdraw.sketchCoords_[0].length > 1) {
                        if ($('.ol-tooltip-measure')[0].style.display == 'none') $('.ol-tooltip-measure')[0].style.display = '';
                        document.getElementsByClassName('drawUndo')[1].classList.remove("avoidclicks")
                    }
                }
            });
        }, this);

        wmsdraw.on('drawend', function () {
            measureTooltipElement.className = 'ol-tooltip ol-tooltip-static';
            let elements = document.getElementsByClassName('opacity-group');
            for (var i = 0; i < elements.length; i++) {
                elements[i].classList.remove("avoidclicks");
                elements[i].style.opacity = 1;
            }
            document.getElementsByClassName('drawUndo')[1].style.display = "none";
            document.getElementsByClassName('heading-btn')[1].classList.remove("avoidclicks");
            document.getElementsByClassName('heading-btn')[1].style.opacity = 1;
            measureTooltip.setOffset([0, -15]);
            sketch = null;
            measureTooltipElement = null;
            createMeasureTooltip();
            unByKey(listener);
        }, this);
    }

    function createHelpTooltip() {
        if (helpTooltipElement?.parentNode) {
            helpTooltipElement.parentNode.removeChild(helpTooltipElement);
        }
        helpTooltipElement = document.createElement('div');
        helpTooltipElement.className = 'ol-tooltip hidden';
        helpTooltip = new Overlay({
            element: helpTooltipElement,
            offset: [15, 0],
            positioning: 'center-left',
        });
        wmsOlMap.addOverlay(helpTooltip);
    }

    function createMeasureTooltip() {
        if (measureTooltipElement?.parentNode) {
            measureTooltipElement.parentNode.removeChild(measureTooltipElement);
        }
        $('.ol-tooltip-measure').remove();
        measureTooltipElement = document.createElement('div');
        measureTooltipElement.className = 'ol-tooltip ol-tooltip-measure';
        measureTooltip = new Overlay({
            element: measureTooltipElement,
            offset: [0, -15],
            positioning: 'bottom-center',
            stopEvent: false,
            insertFirst: false,
        });
        wmsOlMap.addOverlay(measureTooltip);
    }

    $('.wms-button').unbind(touchClickEvent);
    $('.wms-button').bind(touchClickEvent, function () {
        clearLayrs();
        resetCustomWMS();
    });

    $('.measureTools').show();
    $('.ol-tooltip-measure').remove();
    let elements = document.getElementsByClassName('opacity-group');
    for (var i = 0; i < elements.length; i++) {
        elements[i].classList.remove("avoidclicks");
        elements[i].style.opacity = 1;
    }
    document.getElementsByClassName('drawUndo')[1].style.display = "none";
    document.getElementsByClassName('heading-btn')[1].classList.remove("avoidclicks");
    document.getElementsByClassName('heading-btn')[1].style.opacity = 1;
    document.getElementById('option-onewms').checked = true;
    $('#wms-maskwms').hide();
}


var nmOlMap = null, nmdraw;

function resetCustomNearMap() {
    $('#wms-mask').show();
    let nearmapWMSApiKey = clientSettings["Nearmap.APIKey"], CENTER = [-82.6367814985797, 41.4111097499755], flag = false, MIN_ZOOM = 12,
        MAX_ZOOM = 24, shapes = [], ZOOM = 20, helpTooltip, helpTooltipElement, measureTooltipElement, measureTooltip,
        typeSelect = document.getElementsByName('measure-selector'), geoJSON;

    function urlTemplate(z, x, y, survey, layer) {
        var until = '';
        if (survey) {
            until = '&until=' + survey;
        }
        return 'https://api.nearmap.com/tiles/v3/' +
            layer + '/' + z + '/' + x + '/' + y +
            '.img?tertiary=satellite&apikey=' + nearmapWMSApiKey + until;
    };

    function coverageUrlTemplate(east, west, north, south) {
        return 'https://api.nearmap.com/coverage/v2/poly/' +
            west + ',' + north + ',' +
            east + ',' + north + ',' +
            east + ',' + south + ',' +
            west + ',' + south + ',' +
            west + ',' + north +
            '?apikey=' + nearmapWMSApiKey +
            '&limit=1000';
    }

    function degreesToRadians(deg) {
        return deg * (Math.PI / 180);
    }

    function radiansToDegrees(rad) {
        return rad / (Math.PI / 180);
    }

    function modulus360(deg) {
        if (deg < 0) return 360 + (deg % 360);
        return deg % 360;
    }

    let VERT = 0, NORTH = 0, EAST = 90, SOUTH = 180, WEST = 270, HEADINGS = { Vert: VERT, North: NORTH, East: EAST, South: SOUTH, West: WEST };

    function addProj(code, worldWidth, worldHeight) {
        let projection = new ol.proj.Projection({
            code: code,
            extent: [0, 0, worldWidth, worldHeight],
            worldExtent: [-180, -85, 180, 85],
            units: 'pixels'
        });

        ol.proj.addProjection(projection);

        let cx = worldWidth / 2, cy = worldHeight / 2;

        let pixelsPerLonDegree = worldWidth / 360, pixelsPerLatRadian = worldHeight / (2 * Math.PI);

        ol.proj.addCoordinateTransforms(
            'EPSG:4326',
            projection,
            function (coord) {
                let lng = coord[0], lat = coord[1], x = cx + lng * pixelsPerLonDegree, theta = degreesToRadians(lat), tanTheta = Math.tan(theta),
                    secTheta = 1 / Math.cos(theta), latRadians = Math.log(tanTheta + secTheta), y = cy + latRadians * pixelsPerLatRadian;

                return [x, y];
            },
            function (coord) {
                let x = coord[0], y = coord[1], lng = (x - cx) / pixelsPerLonDegree, latRadians = (y - cy) / -pixelsPerLatRadian,
                    lat = -radiansToDegrees(2 * Math.atan(Math.exp(latRadians)) - Math.PI / 2);

                return [lng, lat];
            }
        );
    };

    addProj('NMV:000', 256, 256);
    addProj('NMO:NS', 256, 192);
    addProj('NMO:EW', 192, 256);

    let ProjLatLng = ol.proj.get('EPSG:4326'), ProjVertical = ol.proj.get('NMV:000'),
        ProjObliqueNS = ol.proj.get('NMO:NS'), ProjObliqueEW = ol.proj.get('NMO:EW'),
        PROJECTIONS = { North: ProjObliqueNS, East: ProjObliqueEW, South: ProjObliqueNS, West: ProjObliqueEW, Vert: ProjVertical };


    function fetchImageData(url) {
        return fetch(url, { redirect: 'manual' })
            .then(function (resp) {
                if (resp.status === 200) {
                    return resp.blob()
                        .then(function (data) {
                            return window.URL.createObjectURL(data);
                        });
                }
                else if (!resp.ok) {
                    if (flag == false) {
                        var imag = layerType == 'Vert' ? 'Vertical' : layerType;
                        messageBox('WMS Nearmap ' + imag + ' imagery is not available for the selected survey.');
                        resetCustomNearMap()
                        flag = true;
                    }
                }

                return null;
            })
            .catch(function (reason) {
                return null;
            });
    }

    function loadImage(data) {
        return new Promise(function (resolve) {
            var img = document.createElement('img');
            img.addEventListener('load', function () { resolve(img); });
            img.addEventListener('error', function () { resolve(img); });
            img.src = data;
        });
    }

    function rotateImage(ctx, img, tileWidth, tileHeight, heading) {
        let rotation = degreesToRadians(heading);

        ctx.save();

        ctx.translate(tileWidth / 2, tileHeight / 2);
        ctx.rotate(rotation);

        switch (heading) {
            case NORTH:
            case SOUTH:
                ctx.drawImage(img, -tileWidth / 2, -tileHeight / 2, tileWidth, tileHeight);
                break;
            case EAST:
            case WEST:
                ctx.drawImage(img, -tileHeight / 2, -tileWidth / 2, tileHeight, tileWidth);
                break;
        }

        ctx.restore();
    }

    function createCanvas(width, height) {
        var canvas = document.createElement('canvas');
        canvas.width = width;
        canvas.height = height;

        return [canvas, canvas.getContext('2d')];
    }

    function rotateTile(data, tileDims, heading) {
        var tileWidth = tileDims[0];
        var tileHeight = tileDims[1];
        var canvasAndCtx = createCanvas(tileWidth, tileHeight);
        var canvas = canvasAndCtx[0];
        var ctx = canvasAndCtx[1];

        return loadImage(data)
            .then(function (img) {
                rotateImage(ctx, img, tileWidth, tileHeight, heading);

                return canvas.toDataURL();
            });
    }

    let layerType = 'Vert', availableSurveys = [], dropdownElement = null, selectedDisplayElement = null, displayedDisplayElement = null,
        northElement = null, westElement = null, southElement = null, eastElement = null, vertElement = null;

    let selectedSurvey = {
        survey: null,
        get value() {
            return this.survey;
        },
        set value(value) {
            this.survey = value;
            selectedDisplayElement.innerHTML = value;
        }
    };

    let displayedSurvey = {
        survey: null,
        get value() {
            return this.survey;
        },
        set value(value) {
            this.survey = value;
            displayedDisplayElement.innerHTML = value;
        }
    };

    let TILE_SIZES = {
        North: [256, 192],
        East: [192, 256],
        South: [256, 192],
        West: [192, 256],
        Vertical: [256, 256]
    };

    function toViewRotation(heading) {
        return degreesToRadians(modulus360(360 - heading));
    }

    function tileUrlFunction(tileCoord) {
        var z = tileCoord[0];
        var x = tileCoord[1];
        var y = tileCoord[2];

        return urlTemplate(z, x, y, selectedSurvey.value, layerType);
    }

    function tileLoadFunction(imageTile, src) {
        var img = imageTile.getImage();

        fetchImageData(src)
            .then(function (imgData) {
                if (imgData && layerType !== 'Vert') {
                    rotateTile(imgData, TILE_SIZES[layerType], HEADINGS[layerType])
                        .then(function (rotatedImgData) {
                            img.src = rotatedImgData || '';
                        });
                }

                img.src = imgData || '';
            });
    }

    function createView(zoom, center) {
        zoom = zoom || ZOOM;
        center = center || CENTER;

        return new ol.View({
            projection: PROJECTIONS[layerType],
            rotation: toViewRotation(HEADINGS[layerType]),
            center: ol.proj.fromLonLat(center, PROJECTIONS[layerType]),
            minZoom: MIN_ZOOM,
            maxZoom: MAX_ZOOM,
            zoom: zoom
        });
    }

    function createLayer() {
        return new ol.layer.Tile({
            source: new ol.source.XYZ({
                projection: PROJECTIONS[layerType],
                tileSize: TILE_SIZES[layerType],
                tileUrlFunction: tileUrlFunction,
                tileLoadFunction: tileLoadFunction
            })
        });
    }

    function getBounds() {
        var view = nmOlMap.getView();
        var projection = view.getProjection();
        var extent = view.calculateExtent(nmOlMap.getSize());
        var extent = ol.proj.transformExtent(extent, projection, ol.proj.get('EPSG:4326'));
        var west = extent[0];
        var south = extent[1];
        var east = extent[2];
        var north = extent[3];

        return { north: north, east: east, west: west, south: south };
    };

    /**
     * Called when the selected survey date does not exist in the current list of available survey dates.
     * It returns the closest available date.
     *
     * @param {*} surveys            available surveys returned by the coverage API
     * @param {*} selectedDate       user's selected survey date
     */
    function findClosestDate(surveys, selectedDate) {
        var selectedDateInMs = selectedDate ? +new Date(selectedDate) : +new Date();
        var deltaInMs = surveys.map(function (survey) {
            var surveyDateInMs = +new Date(survey.captureDate);
            return Math.abs(selectedDateInMs - surveyDateInMs);
        });

        var closestDateInMs = Math.min.apply(null, deltaInMs);
        return surveys[deltaInMs.findIndex(function (ms) { return ms === closestDateInMs; })].captureDate;
    };

    /**
     * @param {*} availableSurveys    available surveys returned by the coverage API
     * @param {*} selectedDate        user's selected survey date
     */
    function getSurveyDate(availableSurveys, selectedDate) {
        // No dates available
        if (availableSurveys.length === 0) {
            return null;
        }

        // Selects the selected survey date when available
        if (availableSurveys.find(function (survey) { return selectedDate === survey.captureDate; })) {
            return selectedDate;
        }

        // Searches for the closest available survey date when not available
        if (findClosestDate(availableSurveys, selectedDate)) {
            var value = selectedSurvey.value;
            selectedSurvey.value = findClosestDate(availableSurveys, selectedDate);
            if (value && value !== selectedSurvey.value)
                refreshView();
        }
        return findClosestDate(availableSurveys, selectedDate);
    }

    /**
     * Fetches Nearmap wmsUrl API.
     */
    function fetchCoverage() {
        var bounds = getBounds(nmOlMap);
        var coverageUrl = coverageUrlTemplate(bounds.east, bounds.west, bounds.north, bounds.south);

        return fetch(coverageUrl)
            .then(function (response) {
                return response.json();
            });
    }

    /**
     * Refreshes the map tiles whenever the selected survey date changes.
     */
    function refreshTiles() {
        flag = false;
        nmOlMap
            .getLayers()
            .item(0)
            .getSource()
            .refresh();
    }

    /**
     * Refreshes the map tiles whenever projection changes.
     */
    function clearLayrs() {
        if (nmdraw)
            nmOlMap.removeInteraction(nmdraw);
        if (helpTooltip)
            nmOlMap.removeOverlay(helpTooltip);
        if (measureTooltip)
            nmOlMap.addOverlay(measureTooltip);
        document.getElementsByClassName('measureTools')[0].style.display = "block";
        document.getElementById('option-one').checked = true;
    }


    function refreshView() {
        if (layerType == 'Vert') {
            clearLayrs();
            resetCustomNearMap();

        } else {
            var currentProject = nmOlMap.getView().getProjection();
            var currentCenter = ol.proj.fromLonLat(CENTER, PROJECTIONS[layerType])
            var zoom = nmOlMap.getView().getZoom();
            var center = ol.proj.toLonLat(currentCenter, currentProject);

            nmOlMap.setView(createView(zoom, center));
            nmOlMap.getLayers().clear();
            nmOlMap.getLayers().push(createLayer());

            document.getElementsByClassName('measureTools')[0].style.display = "none";
            if (nmdraw)
                nmOlMap.removeInteraction(nmdraw);
            if (helpTooltip)
                nmOlMap.removeOverlay(helpTooltip);
        }
    }


    /**
     * Displays all available survey dates in a dropdown.
     */
    function updateDropDown() {
        // Clears up previous options
        dropdownElement.innerHTML = '';

        // Creates the content of options for select element
        availableSurveys.forEach(function (survey) {
            var optionElement = document.createElement('option');
            optionElement.setAttribute('value', survey.captureDate);
            optionElement.innerText = survey.captureDate;

            dropdownElement.add(optionElement);
        });

        // Assigns default select value
        dropdownElement.value = displayedSurvey.value;
    }


    function getAvailableSurveyDates(response) {
        var surveys = response && response.surveys ? response.surveys : [];

        return surveys.filter(function (survey) {
            return (survey.resources.tiles || [])
                .some(function (tile) { return tile.type === layerType; });
        });
    }

    /**
     * onMapMoveHandler contains logics how to deal with coverage api while map is moving
     */
    function onMapMoveHandler() {
        // Fetches Nearmap coverage API based current view port
        fetchCoverage()
            .then(function (response) {
                // Updates internal `availableSurveys` and `displayedSurvey` members
                availableSurveys = getAvailableSurveyDates(response);
                displayedSurvey.value = getSurveyDate(availableSurveys, selectedSurvey.value);

                // Updates available surveys dropdown options
                updateDropDown();
            });
    }

    function initUiElements() {
        dropdownElement = document.querySelector('.nm-custom-select');
        selectedDisplayElement = document.querySelector('#selectedSurveyElementId');
        displayedDisplayElement = document.querySelector('#displayedSurveyElementId');

        northElement = document.querySelector('#northElementId');
        westElement = document.querySelector('#westElementId');
        southElement = document.querySelector('#southElementId');
        eastElement = document.querySelector('#eastElementId');
        vertElement = document.querySelector('#vertElementId');
    }

    /**
     * Adds event listeners to map instance and UI elements.
     */
    function addEventListeners() {
        // Adds map moving (panning and zooming) listener
        nmOlMap.on('moveend', function () {
            onMapMoveHandler(dropdownElement);
        });

        // Adds "onChange" listener to the dropdown
        dropdownElement.addEventListener('change', function (evt) {
            selectedSurvey.value = evt.target.value;
            displayedSurvey.value = evt.target.value;

            refreshTiles();
        });

        [northElement, westElement, southElement, eastElement, vertElement]
            .forEach(function (element) {
                element.addEventListener('click', function (evt) {
                    layerType = evt.target.value;
                    flag = false;
                    refreshView();
                    refreshView();
                });
            });
    }

    if (activeParcel.MapPoints.length > 0) {
        CENTER = [activeParcel.Center().lng, activeParcel.Center().lat]
    }

    let VectorSource = ol.source.Vector;
    let { Tile, Vector } = ol.layer; //import {Tile as TileLayer, Vector as VectorLayer} from 'ol/layer.js';
    let TileLayer = Tile, VectorLayer = Vector;
    let { Circle, Fill, Stroke, Style } = ol.style;
    let GeoJSON = ol.format.GeoJSON, MultiPoint = ol.geom.MultiPoint;
    let styles = [
        new Style({
            stroke: new Stroke({
                color: '#1ab7ea',
                width: 3,
            })
        })];

    if (activeParcel.MapPoints.length > 0) {
        for (x in activeParcel.MapPoints.sort(function (x, y) {
            return x.Ordinal - y.Ordinal
        })) {
            let p = activeParcel.MapPoints[x];
            if (shapes[p.RecordNumber || 0] == null) {
                shapes[p.RecordNumber || 0] = [];
            }
            shapes[p.RecordNumber || 0].push(ol.proj.transform([p.Longitude, p.Latitude], 'EPSG:4326', 'NMV:000'));
        };
    }

    let multiJSON = [];
    shapes.forEach((item) => {
        multiJSON.push({ "type": "Feature", "properties": {}, "geometry": { "type": "Polygon", "coordinates": [item] } })
    });

    geoJSON = new ol.layer.VectorImage({
        source: new ol.source.Vector({
            features: new ol.format.GeoJSON().readFeatures({
                "type": "FeatureCollection",
                "features": multiJSON
            }),
        }),
        style: styles,
        visible: true
    });

    if (nmOlMap) {
        nmOlMap.setTarget(null);
        nmOlMap.dispose();
    }
    nmOlMap = new ol.Map({
        target: 'nearmap',
        controls: [new ol.control.Zoom()],
        layers: [createLayer()],
        view: createView()
    });

    nmOlMap.addLayer(geoJSON);

    initUiElements();
    addEventListeners();

    let { getArea, getLength } = ol.sphere;
    let { unByKey } = ol.Observable;
    let sketch, Draw = ol.interaction.Draw, Overlay = ol.Overlay, View = ol.View, CircleStyle = ol.style.Circle, Icon = ol.style.Icon;
    let { LineString, Polygon } = ol.geom;
    let source = new VectorSource();
    let vector = new VectorLayer({
        source: source,
        view: new View({
            projection: 'EPSG:4326',
        }),
        style: new Style({
            fill: new Fill({
                color: 'rgba(255, 255, 255, 0.2)',
            }),
            stroke: new Stroke({
                color: '#ffcc33',
                width: 2,
            }),
            image: new CircleStyle({
                radius: 7,
                fill: new Fill({
                    color: '#ffcc33',
                }),
            }),
        }),
    });

    nmOlMap.addLayer(vector);

    for (var i = 0; i < typeSelect.length; i++) {
        typeSelect[i].addEventListener('change', function () {
            if (this.value == 'none') {
                nmOlMap.removeInteraction(nmdraw);
                nmOlMap.removeOverlay(helpTooltip);
            } else {
                nmOlMap.removeInteraction(nmdraw);
                addInteraction(this.value);
            }
        });
    }

    let continuePolygonMsg = 'Click to continue drawing the polygon';
    let continueLineMsg = 'Click to continue drawing the line';
    let pointerMoveHandler = function (evt) {
        if (evt.dragging) {
            return;
        }
        let helpMsg = 'Click to start drawing';
        if (sketch) {
            const geom = sketch.getGeometry();
            if (geom instanceof Polygon) {
                helpMsg = continuePolygonMsg;
            } else if (geom instanceof LineString) {
                helpMsg = continueLineMsg;
            }
        }
        helpTooltipElement.innerHTML = helpMsg;
        helpTooltip.setPosition(evt.coordinate);
        helpTooltipElement.classList.remove('hidden');
    };

    function getDistanceBetweenPoints(lat1, lng1, lat2, lng2) {
        // The radius of the planet earth in meters
        let R = 6378137;
        let dLat = degreesToRadians(lat2 - lat1);
        let dLong = degreesToRadians(lng2 - lng1);
        let a = Math.sin(dLat / 2)
            *
            Math.sin(dLat / 2)
            +
            Math.cos(degreesToRadians(lat1))
            *
            Math.cos(degreesToRadians(lat1))
            *
            Math.sin(dLong / 2)
            *
            Math.sin(dLong / 2);

        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        let distance = R * c;

        return distance;
    }

    function degreesToRadians(degrees) {
        return degrees * Math.PI / 180;
    }

    var formatLength = function (line) {
        var length;
        var coordinates = line.getCoordinates();
        length = 0;
        var sourceProj = nmOlMap.getView().getProjection();
        for (var i = 0, ii = coordinates.length - 1; i < ii; ++i) {
            var c1 = ol.proj.transform(coordinates[i], sourceProj, 'EPSG:4326');
            var c2 = ol.proj.transform(coordinates[i + 1], sourceProj, 'EPSG:4326');
            length += getDistanceBetweenPoints(c1[1], c1[0], c2[1], c2[0]);
        }

        var output;
        if (length > 100) {
            output = length / 1000
                + ' km<br/>' + Math.round(length * 3.2808 * 1000) / 1000 + ' Ft';
        } else {
            output = Math.round(length * 1000) / 1000
                + ' M<br/>' + Math.round(length * 3.2808 * 1000) / 1000 + ' Ft';
        }
        return output;
    };

    var formatArea = function (polygon) {
        var op = { 'projection': 'EPSG:4326' };
        const area = getArea(polygon, op);
        let output;
        if (area > 10000) {
            output = Math.round((area / 1000000) * 100) / 100 + ' ' + ' km<sup>2</sup><br/>' + Math.round(area * 100 * 10.764) / 100 + ' Sq.Ft';
        } else {

            output = Math.round(area * 100) / 100 + ' ' + ' m<sup>2</sup><br/>' + Math.round(area * 100 * 10.764) / 100 + ' Sq.Ft';
        }
        return output;
    };

    function addInteraction(typeOp) {
        nmOlMap.on('pointermove', pointerMoveHandler);
        nmOlMap.getViewport().addEventListener('mouseout', function () {
            helpTooltipElement.classList.add('hidden');
        });
        const type = typeOp == 'area' ? 'Polygon' : 'LineString';
        nmdraw = new Draw({
            source: source,
            type: type,
            style: new Style({
                fill: new Fill({
                    color: 'rgba(255, 255, 255, 0.2)',
                }),
                stroke: new Stroke({
                    color: 'rgba(0, 0, 0, 0.5)',
                    lineDash: [10, 10],
                    width: 2,
                }),
                image: new Icon({
                    anchor: [-1, 15],
                    anchorXUnits: 'pixels',
                    anchorYUnits: 'pixels',
                    src: '/static/css/images/pencil.png',

                }),
            }),
        });
        nmOlMap.addInteraction(nmdraw);
        createMeasureTooltip();
        createHelpTooltip();

        let listener;

        nmdraw.on('drawstart', function (evt) {
            sketch = evt.feature;
            console.log(nmOlMap.group);
            let tooltipCoord = evt.coordinate;
            let elements = document.getElementsByClassName('opacity-group');
            for (var i = 0; i < elements.length; i++) {
                elements[i].classList.add("avoidclicks");
                elements[i].style.opacity = .3;
            }
            document.getElementsByClassName('drawUndo')[0].style.display = "inline-block";
            document.getElementsByClassName('heading-btn')[0].classList.add("avoidclicks");
            document.getElementsByClassName('heading-btn')[0].style.opacity = .3;
            if ($('.ol-tooltip-measure')[0].style.display == 'none') $('.ol-tooltip-measure')[0].style.display = '';

            $('.drawUndo').off(touchClickEvent);
            $('.drawUndo').on(touchClickEvent, function () {
                if (document.getElementsByClassName('drawUndo')[0].classList.contains('avoidclicks')) {
                    return;
                }
                else {
                    if (nmdraw?.sketchCoords_) {
                        if (nmdraw.type_ == 'LineString') {
                            if (nmdraw.sketchCoords_.length > 0) nmdraw.removeLastPoint();
                            if (nmdraw.sketchCoords_.length == 1) {
                                $('.ol-tooltip-measure')[0].style.display = 'none';
                                document.getElementsByClassName('drawUndo')[0].classList.add("avoidclicks");
                            }
                        }
                        else {
                            if (nmdraw.sketchCoords_[0].length > 0) nmdraw.removeLastPoint();
                            if (nmdraw.sketchCoords_[0].length == 1) {
                                $('.ol-tooltip-measure')[0].style.display = 'none';
                                document.getElementsByClassName('drawUndo')[0].classList.add("avoidclicks");
                            }

                        }
                    }
                }
                return false;
            });

            listener = sketch.getGeometry().on('change', function (evt) {
                const geom = evt.target;
                let output;
                if (geom instanceof Polygon) {
                    output = formatArea(geom);
                    tooltipCoord = geom.getInteriorPoint().getCoordinates();
                } else if (geom instanceof LineString) {
                    output = formatLength(geom);
                    tooltipCoord = geom.getLastCoordinate();
                }
                measureTooltipElement.innerHTML = output;
                measureTooltip.setPosition(tooltipCoord);
                if (nmdraw?.sketchCoords_) {
                    if (nmdraw.type_ == 'LineString' && nmdraw.sketchCoords_.length > 1) {
                        if ($('.ol-tooltip-measure')[0].style.display == 'none') $('.ol-tooltip-measure')[0].style.display = '';
                        document.getElementsByClassName('drawUndo')[0].classList.remove("avoidclicks");
                    }
                    else if (nmdraw.type_ == 'Polygon' && nmdraw.sketchCoords_[0].length > 1) {
                        if ($('.ol-tooltip-measure')[0].style.display == 'none') $('.ol-tooltip-measure')[0].style.display = '';
                        document.getElementsByClassName('drawUndo')[0].classList.remove("avoidclicks")
                    }
                }
            });
        }, this);

        nmdraw.on('drawend', function () {
            measureTooltipElement.className = 'ol-tooltip ol-tooltip-static';
            let elements = document.getElementsByClassName('opacity-group');
            for (var i = 0; i < elements.length; i++) {
                elements[i].classList.remove("avoidclicks");
                elements[i].style.opacity = 1;
            }
            document.getElementsByClassName('drawUndo')[0].style.display = "none";
            document.getElementsByClassName('heading-btn')[0].classList.remove("avoidclicks");
            document.getElementsByClassName('heading-btn')[0].style.opacity = 1;
            measureTooltip.setOffset([0, -15]);
            sketch = null;
            measureTooltipElement = null;
            createMeasureTooltip();
            unByKey(listener);
        }, this);
    }

    function createHelpTooltip() {
        if (helpTooltipElement?.parentNode) {
            helpTooltipElement.parentNode.removeChild(helpTooltipElement);
        }
        helpTooltipElement = document.createElement('div');
        helpTooltipElement.className = 'ol-tooltip hidden';
        helpTooltip = new Overlay({
            element: helpTooltipElement,
            offset: [15, 0],
            positioning: 'center-left',
        });
        nmOlMap.addOverlay(helpTooltip);
    }

    function createMeasureTooltip() {
        if (measureTooltipElement?.parentNode) {
            measureTooltipElement.parentNode.removeChild(measureTooltipElement);
        }
        $('.ol-tooltip-measure').remove();
        measureTooltipElement = document.createElement('div');
        measureTooltipElement.className = 'ol-tooltip ol-tooltip-measure';
        measureTooltip = new Overlay({
            element: measureTooltipElement,
            offset: [0, -15],
            positioning: 'bottom-center',
            stopEvent: false,
            insertFirst: false,
        });
        nmOlMap.addOverlay(measureTooltip);
    }

    $('.wms-button').unbind(touchClickEvent);
    $('.wms-button').bind(touchClickEvent, function () {
        clearLayrs();
        resetCustomNearMap();
    });

    $('.measureTools').show();
    $('.ol-tooltip-measure').remove();
    let elements = document.getElementsByClassName('opacity-group');
    for (var i = 0; i < elements.length; i++) {
        elements[i].classList.remove("avoidclicks");
        elements[i].style.opacity = 1;
    }
    document.getElementsByClassName('drawUndo')[0].style.display = "none";
    document.getElementsByClassName('heading-btn')[0].classList.remove("avoidclicks");
    document.getElementsByClassName('heading-btn')[0].style.opacity = 1;
    document.getElementById('option-one').checked = true;
    $('#wms-mask').hide();
}


