﻿var clr = 'white';
var selectedSketch = null, selectedSection = null;
var isNewSketchData =  false;
var currentOverlayPosition = {};
var drawFlag = false;
let mapZoom = 20;
let sketchZoom = 0.6;
var overlay;
var sketchDropDownChange = [];
let container;
let isSvSketchChanged = false;
let degree;
var sketchLatLng = '';
var ParcelDataExist = false;
var sketchOB = {};
var tEvents = {};
var firstTimeSvLoad = true;
var polygonCenter;
var draggableSketch;

function initSVEvents(isOsm) {
	tEvents = {
		start: iPad? 'touchstart': 'mousedown',
		end: iPad? 'touchend': 'mouseup',
		move: iPad? 'touchmove': 'mousemove',
		leave: iPad? 'touchleave': 'mouseleave',
		tclick: iPad? 'touchclick': 'click',
		onMove: iPad? 'ontouchmove': 'onmousemove',
		onclick: 'onclick',
		ondblclick: 'ondblclick'
	}
    $('.svDropdown').remove();
	$('.svRoot').remove();
    $('.svMA').hide();
    if (clientSettings?.HideSketchTab == '1') { $('#btnSktch').hide(); }
    else { $('#btnSktch').show(); }
	if (!isOsm) {
		/*var dropDownDiv = document.createElement('div');
		dropDownDiv.className = 'svDropdown';
		var ddDiv = new setupSVItemsOnMap(dropDownDiv, map); 
		ddDiv.showDropdowns(); */
		var rootDiv = document.createElement('div');
		rootDiv.className = 'svRoot';
		var rDiv = new setupSVItemsOnMap(rootDiv, map);      
		rDiv.showZoomOnMap();
		rDiv.showRotateOnMap();
	}
	else {
		svOsmDivCreation(OsMap);
	}
	$('.svDropdown').hide();
	$('.svRoot').hide();
}

function initDraggableOverlay(saveFrom) {
	$('.svMA').show();
	$('.svDropdown').show();
    $('.svRoot').show();
    setScreenDimensions();                        // Added because map view height not correct when SV is enabled
	currentOverlayPosition = {};
    mapZoom = activeParcel.MapZoom;
    sketchZoom = activeParcel.sketchZoom;
	isNewSketchData = activeParcel.NewSketchData? true: false;
	ParcelDataExist = (activeParcel.NewSketchData || activeParcel.sketchLatLng)? true: false;
    isSvSketchChanged = false;
    polygonCenter = null;
    var tmOut = (saveFrom? 500: (firstTimeSvLoad? 2500: 1000)); //time out given because of map elements need more time to bind in map frame.
	showMask('Loading...');
	setTimeout(function () {
		if (saveFrom) { $('.sketch-select').blur(); $('.section-select').blur(); $('#maskLayer').hide(); }
		previewSVSketch(null, 1, function(sketchURL) { 
			if (!OsMap)		
				populateSketch(sketchURL, null, saveFrom);
			else
				populateSketchOSM(sketchURL, null, saveFrom)
	    });
	}, tmOut);
}


function initSVButton(){
	$(".sketch-select, .section-select").change(function() {
        let selectedSketch = $('.sketch-select').val();
        let selectedSection = $('.section-select').val();
        previewSVSketch();
        let skRowuid = $('.sketch-select option[value="' + selectedSketch + '"]').attr('skrowuid');
        let skPrefix = $('.sketch-select option[value="' + selectedSketch + '"]').attr('skprefix');
        skPrefix = isNewSketchData && skPrefix? skPrefix: '';
        let selecteddpVal = isNewSketchData? skRowuid: selectedSketch;
        if (selecteddpVal && currentOverlayPosition && currentOverlayPosition['sketch-' + selecteddpVal + skPrefix] && currentOverlayPosition['sketch-' + selecteddpVal + skPrefix ]['1'] && currentOverlayPosition['sketch-' + selecteddpVal + skPrefix]['1'].mapZoom) {
            firstTimeLoad = false;
            mapZoom = currentOverlayPosition['sketch-' + selecteddpVal + skPrefix]['1'].mapZoom;
            if (!OsMap)
            	map.setZoom(currentOverlayPosition['sketch-' + selecteddpVal + skPrefix]['1'].mapZoom);
            else
            	OsMap.setZoom(currentOverlayPosition['sketch-' + selecteddpVal + skPrefix]['1'].mapZoom)
        } 
    });
     
    $('#btnSaveRvw').bind(touchClickEvent, function (e) {
        saveSvReview(); 
        return false;
    });
}

function previewSVSketch(colorCode, type, callback, selSketch, selSection, UpdateSketchDropDown) {
    let currentSktScale = sketchRenderer.scale;
    sketchRenderer.scale = 1;
    sketchRenderer.showOrigin = false;
    sketchRenderer.viewOnly = true;
    sketchRenderer.resetOrigin();
    let sketches = [];
    if (type == 1) { sketches = activeParcel.getSketchSegments(); }
    sketches = sketches ? sketches : [];
    if (type == 1) {
        sketchRenderer.open(sketches, null, true, null, null, null, colorCode, "black");
        $('.sketch-select,.section-select').empty().append('<option></option>').empty();
    }
    let sk = sketchRenderer.sketches;
    if (type == 1) {
    	$('#showSketchOptions').empty();
        sk.forEach(function (s, i) {
            if (s.vectors.length > 0) {
            	let skRowuid = (s.parentRow && s.parentRow.ROWUID)? s.parentRow.ROWUID: s.sid;
            	let skPrefix = (s.config && s.config.SketchLabelPrefixInSv)? s.config.SketchLabelPrefixInSv: '';	
                if (parseFloat(skRowuid) < 0 && s.parentRow && s.config && s.config.SketchSource.Table) {
                        let stble = s.config.SketchSource.Table;
                        skRowuid = '$' + stble + '#' + skRowuid + '$';
                }
                $('.sketch-select').append($('<option>', { value: i, text: s.label, skrowuid: skRowuid, skprefix: skPrefix }));
                $('#showSketchOptions').append($('<span>', { value: i, text: s.label, skrowuid: skRowuid, skprefix: skPrefix }));
                if( UpdateSketchDropDown ) {
                	sketchDropDownCount = sketchDropDownCount + 1;
                	sketchDropDownChange.push( {Id: (i.toString()), Name: s.label} );
                }
            }
        });
        $('.sketch-select').val(0);
        if( UpdateSketchDropDown )
        	sketchDropDownChange = $.grep(top.sketchDropDownChange, function(data, index) { return data.Id != $('#sketchid').val() });
    }
    if (selSketch !== undefined) {
        selectedSketch = selSketch;
    } else {
        selectedSketch = $('.sketch-select').val();
    }
	
	let _ah = false, _pExists = false;
	if( selSketch || selSection )
		_pExists = true;
		
    if (selectedSketch > -1 && type == 1 && selectedSketch) {
        for (var key in sk[selectedSketch].sections) {
            $('.section-select').append($('<option>', { value: key, text: key }));
        }
    }
    selectedSection = selSection !== undefined ? selSection : $('.section-select').val();
    let selectedSectionIndex = selectedSketch && sk[selectedSketch].sections ? sk[selectedSketch].sections[selectedSection] : null;
    let ab = selectedSectionIndex ? sectionbound(selectedSectionIndex) : { xmax: 0, ymax: 0, xmin: 0, ymin: 0 };

    scale = 1
    let sHeight = (ab.ymax - ab.ymin) * 10;
    let sWidth = (ab.xmax - ab.xmin) * 10;
    if (sHeight * 1.1 > 300) {
        scale = 3000 / sHeight * 0.9;
    }

    let maxHeight = 300 * 0.9;
    let maxWidth = 300 * 0.9;
    let aspectRatio = parseFloat(sWidth) / parseFloat(sHeight);
    let width, height, sWidthHeight = false;

    if ((sWidth < maxWidth) && (sHeight < maxHeight)) {
        scale = 1;
        width = maxWidth;
        sWidthHeight = true; 
    } else if (aspectRatio > 1) {
        width = maxWidth;
        height = (width / aspectRatio);
        if (height > maxHeight) {
            height = maxHeight;
            width = (height * aspectRatio);
        }
    } else {
        height = maxHeight;
        width = (height * aspectRatio);
        if (width > maxWidth) {
            width = maxWidth;
            height = (width / aspectRatio);
        }
    }
	if( !sWidthHeight )
    	scale = parseFloat((width / sWidth).toFixed(2))
    sketchRenderer.scale = scale;
    $('.ccse-img-container').show();
    sketchRenderer.resizeCanvas(400, 400);
    sketchRenderer.pan(sketchRenderer.formatter.originPosition == "topRight" ? applyRoundToOrigin(-ab.xmax * 10 * scale - 45) : applyRoundToOrigin(-ab.xmin * 10 * scale - 20), (sketchRenderer.formatter.originPosition == "topRight" || sketchRenderer.formatter.originPosition == "topLeft") ? applyRoundToOrigin(-ab.ymax * 10 * scale - 45) : applyRoundToOrigin(-ab.ymin * 10 * scale - 20));
    sketchRenderer.clearCanvas();
    if (selectedSketch && selectedSection && selectedSketch > -1 && selectedSection > -1 && sk[selectedSketch].sections[selectedSection]) {
        sk[selectedSketch].sections[selectedSection].forEach(function (vt) {
        	if (!(vt.isUnSketchedArea || vt.isUnSketchedTrueArea)) {
	            if (!vt.isClosed && vt.isChanged)
	                sketchRenderer.lineColor = '#FFA62F';
	            else
	                sketchRenderer.lineColor = 'black';
	            vt.render();
           	}
        })
    } 
    let url = "";
    $('.ccse-img-container').hide();
    url = sketchRenderer.toDataURL();
    sketchRenderer.scale = currentSktScale;
    if (type != 1) {
    	if (OsMap) $('.sketchOverlay').attr('src', url);
        else $('#sketch').attr('src', url);
    }
    if (!type)
      setPositionOverlayIfDataExist(true);
    if(callback) callback(url);
}

function sectionbound(sk) {
    var x1 = 0, x2 = 0, y1 = 0, y2 = 0;
    for (x in sk) {
        var b = new PointX(0, 0);
        var v = sk[x];
        var sn = v.startNode;
        while (sn != null) {
            x1 = Math.min(x1, isNaN(sn.p.x) ? 0 : sn.p.x);
            x2 = Math.max(x2, isNaN(sn.p.x) ? 0 : sn.p.x);
            y1 = Math.min(y1, isNaN(sn.p.y) ? 0 : sn.p.y);
            y2 = Math.max(y2, isNaN(sn.p.y) ? 0 : sn.p.y);

            if (sn.isArc) {
                var mp = sn.midpoint;
                x1 = Math.min(x1, isNaN(mp.x) ? 0 : mp.x);
                x2 = Math.max(x2, isNaN(mp.x) ? 0 : mp.x);
                y1 = Math.min(y1, isNaN(mp.y) ? 0 : mp.y);
                y2 = Math.max(y2, isNaN(mp.y) ? 0 : mp.y);
            }

            sn = sn.nextNode;
        }
    }
    return { xmax: x2, ymax: y2, xmin: x1, ymin: y1 };
}



function populateSketch(sketchURL, dropDownChange, saveFrom) {
    let ltlng;
    let bounds = new google.maps.LatLngBounds();
    if (activeParcel.MapPoints) {
        activeParcel.MapPoints.sort((x, y) => { return x.Ordinal - y.Ordinal }).forEach((item, index) => {
            let latlng = new google.maps.LatLng(item.Latitude, item.Longitude);
            bounds.extend(latlng);
        });
        polygonCenter = bounds.getCenter();
    }
    if (!(saveFrom && overlay))
        $('.overlay').parent('div').remove();
    if (activeParcel.lat && activeParcel.lng)
        ltlng = new google.maps.LatLng(activeParcel.lat, activeParcel.lng);
    if (activeParcel.SketchData) {
        var skRowuid = $('.sketch-select option[value="' + selectedSketch + '"]').attr('skrowuid');
        var skPrefix = $('.sketch-select option[value="' + selectedSketch + '"]').attr('skprefix');
        skPrefix = isNewSketchData && skPrefix ? skPrefix : '';
        var newSelectedSketch = isNewSketchData ? skRowuid : selectedSketch;
        var skd = JSON.parse(activeParcel.SketchData);
        var tag = "sketch-" + newSelectedSketch + skPrefix;
        if (!_.isEmpty(skd[tag])) {
            var pos = skd[tag][selectedSection].position;
            ltlng = new google.maps.LatLng(pos.lat, pos.lng);
        }
    }

    function DraggableOverlay(map, position, content) {
        if (typeof draw === 'function') {
            this.draw = draw;
        }
        this.setValues({
            position: position,
            container: null,
            content: content,
            map: map
        });
    }

    DraggableOverlay.prototype = new google.maps.OverlayView();

    DraggableOverlay.prototype.onAdd = function () {
        container = document.createElement('div'),
            that = this;
        if (typeof this.get('content').nodeName !== 'undefined') {
            container.appendChild(this.get('content'));
        } else {
            if (typeof this.get('content') === 'string') {
                container.innerHTML = this.get('content');
            } else {
                return;
            }
        }
        container.style.position = 'absolute';
        container.draggable = true;

        google.maps.event.addDomListener(this.get('map').getDiv(), tEvents.leave, function () {
            google.maps.event.trigger(container, tEvents.end);
        });

        google.maps.event.addDomListener(this.get('map').getDiv(), tEvents.tclick, function () {
            google.maps.event.trigger(container, tEvents.end);
        });

        map.addListener("zoom_changed", () => {
            if (activeParcel) {
                let zoomLevel = map.getZoom(), zoomdiff = 0;
                mapZoom = mapZoom ? mapZoom : zoomLevel;
                zoomdiff = Math.abs(mapZoom - zoomLevel);
                if (mapZoom > zoomLevel) zoomSketch(sketchZoom / Math.pow(2, zoomdiff), true, false, 'OUT', zoomLevel);
                else if (mapZoom < zoomLevel) zoomSketch(sketchZoom * Math.pow(2, zoomdiff), true, false, 'IN', zoomLevel);
                window.setTimeout('setPositionOverlayIfDataExist();', 250);
                mapZoom = zoomLevel;
            }
        });

        google.maps.event.addDomListener(container, tEvents.start, function (e) {
            this.style.cursor = 'move';
            that.map.set('draggable', false);
            that.set('origin', e);
            that.moveHandler = google.maps.event.addDomListener(container, tEvents.move, function (e) {
                var origin = that.get('origin');
                var cx = e.clientX, cy = e.clientY, ox, oy;
                if (e.touches) {
                    var t = e.touches[0] ? e.touches[0] : e.changedTouches[0];
                    cx = t.clientX; cy = t.clientY;
                }
                if (origin) {
                    var t = origin.touches ? (origin.touches[0] ? origin.touches[0] : origin.changedTouches[0]) : origin;
                    ox = t.clientX; oy = t.clientY;
                    var left = ox - cx;
                    var top = oy - cy;
                    var pos = that.getProjection().fromLatLngToDivPixel(that.get('position'));
                    if (pos) {
                        var latLng = that.getProjection().fromDivPixelToLatLng(new google.maps.Point(pos.x - left, pos.y - top));
                        that.set('origin', e);
                        that.set('position', latLng);
                        isSvSketchChanged = true;
                        firstTimeLoad = false;
                        that.draw();
                    }
                }
            });
        });

        google.maps.event.addDomListener(container, tEvents.end, function (e) {
            var skRowuid = top.$('.sketch-select option[value="' + selectedSketch + '"]').attr('skrowuid');
            var skPrefix = top.$('.sketch-select option[value="' + selectedSketch + '"]').attr('skprefix');
            skPrefix = isNewSketchData && skPrefix ? skPrefix : '';
            var newSelectedSketch = isNewSketchData ? skRowuid : selectedSketch;
            if (drawFlag) {
                var latLng;
                if (newSelectedSketch && selectedSection) latLng = currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection].position;
                that.set('position', latLng);
                that.draw();
                drawFlag = false;
            }
            else {
                that.map.set('draggable', true);
                this.style.cursor = 'default';
                that.set('origin', e);
                google.maps.event.removeListener(that.moveHandler);
                if (that.position) {
                    sketchLatLng = that.position.lat() + ',' + that.position.lng();
                    let zoom = $('#svZoomInput').val();
                    let angle = $('#svRotateInput').val();
                    if (currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix] && selectedSection) {
                        currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection] = {
                            "position": new google.maps.LatLng(that.position.lat(), that.position.lng()),
                            "sketchZoom": zoom,
                            "sketchRotation": parseFloat(angle),
                            "mapZoom": mapZoom,
                            modified: true
                        }
                    }
                }
            }
        });
        this.set('container', container)
        this.getPanes().floatPane.appendChild(container);
    };

    DraggableOverlay.prototype.draw = function () {
        var pos = this.getProjection().fromLatLngToDivPixel(this.get('position'));
        if (pos) {
            this.get('container').style.left = pos.x + 'px';
            this.get('container').style.top = pos.y + 'px';
            if (activeParcel && !activeParcel.sketchLatLng && !isSvSketchChanged) {
                var divW = parseFloat($('#resizable-wrapper', this.container).css("width")) / 2;
                var divH = parseFloat($('#resizable-wrapper', this.container).css("height")) / 2;
                pos.y = pos.y - divH;
                pos.x = (pos.x - divW);
                this.get('container').style.left = pos.x + 'px';
                this.get('container').style.top = pos.y + 'px';
            }
        }
    };

    DraggableOverlay.prototype.onRemove = function () {
        this.get('container').parentNode.removeChild(this.get('container'));
        this.set('container', null)
    };
    if (!(saveFrom && overlay))
        overlay = new DraggableOverlay(map, ltlng ? ltlng : polygonCenter, '<div class="overlay" style="display: none;" id="resizable-wrapper"><img id="sketch" src="' + sketchURL + '"><div>');
    else
        $('#sketch')[0].src = sketchURL;
    var timeOutOverlay = true;
    if (top.$('.sketch-select option').length == 0) {
        if ($('#maskLayer').css('display') != 'none') $('#maskLayer').hide();
        if (firstTimeSvLoad) firstTimeSvLoad = false;
        timeOutOverlay = false;
    }
    if (ParcelDataExist) {
        var mZoom = null;
        setOverLayPosition(activeParcel.lat, activeParcel.lng, sketchZoom, activeParcel.SketchRotate, activeParcel.SketchData, null, dropDownChange);
        ParcelDataExist = true;
        setPositionOverlayIfDataExist();
        if (timeOutOverlay && (firstTimeSvLoad || !saveFrom)) {
            let tm = (firstTimeSvLoad ? 2000 : 750);
            setTimeout(function () {
                setPositionOverlayIfDataExist();
                $('.overlay').show();
                if ($('#maskLayer').css('display') != 'none') $('#maskLayer').hide();
                if (firstTimeSvLoad) firstTimeSvLoad = false;
            }, tm);
        }
        scale = isNaN(parseFloat(sketchOB.sketchZoom)) ? 0 : parseFloat(sketchOB.sketchZoom);
        RotationDegree = parseFloat(sketchOB.sketchRotation);
        var skRowuid = top.$('.sketch-select option[value="' + selectedSketch + '"]').attr('skrowuid');
        var skPrefix = top.$('.sketch-select option[value="' + selectedSketch + '"]').attr('skprefix');
        skPrefix = isNewSketchData && skPrefix ? skPrefix : '';
        var newSelectedSketch = isNewSketchData ? skRowuid : selectedSketch;
        if (currentOverlayPosition && newSelectedSketch && selectedSection && currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection].position) {
            var pos = LatLngToPoint(currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection].position)
            var divW = divH = (400 * scale) / 2;
            if (divH && divW && pos) {
                pos.y = pos.y + divH;
                pos.x = (pos.x + divW);
                var center = PointToLatLng(pos);;
                map.setCenter(center);
            }
            else
                map.setCenter(currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection].position);
            if (currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection].mapZoom)
                mZoom = currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection].mapZoom;
        } else
            map.setCenter(bounds.getCenter());
        mZoom = mapZoom = mZoom ? mZoom : activeParcel.MapZoom;
        map.setZoom(parseInt(mZoom));
        zoomSketch(sketchOB.sketchZoom);
        rotateSketch(parseFloat(sketchOB.sketchRotation), true);

    } else {
        setOverLayPosition(polygonCenter.lat(), polygonCenter.lng(), 0.6, 0, null, dropDownChange);
        setPositionOverlayIfDataExist(true);
        if (timeOutOverlay && (firstTimeSvLoad || !saveFrom)) {
            let tm = (firstTimeSvLoad ? 2000 : 750);
            setTimeout(function () {
                setPositionOverlayIfDataExist();
                $('.overlay').show();
                if ($('#maskLayer').css('display') != 'none') $('#maskLayer').hide();
                if (firstTimeSvLoad) firstTimeSvLoad = false;
            }, tm);
        }
        map.setCenter(bounds.getCenter());
        map.setZoom(20);
        mapZoom = 20;
        zoomSketch(0.6, true);
        rotateSketch(0, true);
    }

}

function setOverLayPosition(lat, lng, zoom, rotation, sketchData, dropDownChange) {
    if (sketchData && sketchData != "") {
        currentOverlayPosition = $.parseJSON(sketchData);
        for (var key in currentOverlayPosition) {
            for (var key2 in currentOverlayPosition[key]) {
                currentOverlayPosition[key][key2].position = new google.maps.LatLng(currentOverlayPosition[key][key2].position.lat, currentOverlayPosition[key][key2].position.lng);
                currentOverlayPosition[key][key2].modified = true;
            }
        }
        return;
    }
    if (polygonCenter) {
    	let _lat = typeof (polygonCenter.lat) === 'function' ? polygonCenter.lat(): polygonCenter.lat, _lng = typeof (polygonCenter.lng) === 'function' ? polygonCenter.lng(): polygonCenter.lng;
        lat = _lat;
        lng = _lng;
    }
    let sk = sketchRenderer.sketches;
    sk.forEach(function (s, i) {
		let skRowuid = (s.parentRow && s.parentRow.ROWUID)? s.parentRow.ROWUID: s.sid;
        let skPrefix = (s.config && s.config.SketchLabelPrefixInSv)? s.config.SketchLabelPrefixInSv: '';
        if (parseFloat(skRowuid) < 0 && s.parentRow && s.config && s.config.SketchSource.Table) {
            let stble = s.config.SketchSource.Table;
            skRowuid = '$' + stble + '#' + skRowuid + '$';
        }
        i = isNewSketchData? skRowuid: i;
        skPrefix = isNewSketchData? skPrefix: '';
        if(dropDownChange) return;
        currentOverlayPosition["sketch-" + i + skPrefix] = {};
        for (let key in s.sections) {
            if (currentOverlayPosition["sketch-" +  i + skPrefix][key] === undefined)
                currentOverlayPosition["sketch-" +  i + skPrefix][key] = {
                    "position": new google.maps.LatLng(lat, lng),
                    "sketchZoom": zoom,
                    "sketchRotation": rotation,
					"mapZoom": mapZoom,
                    modified: false,
                }
        }
    });
}

function setPositionOverlayIfDataExist(multisketch) {
    var position, type;
    if (currentOverlayPosition && selectedSection) {
        var skRowuid = top.$('.sketch-select option[value="' + selectedSketch + '"]').attr('skrowuid');
        var skPrefix = top.$('.sketch-select option[value="' + selectedSketch + '"]').attr('skprefix');
        skPrefix = isNewSketchData && skPrefix? skPrefix: '';
        var newSelectedSketch = isNewSketchData? skRowuid: selectedSketch;
        if (currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix] === undefined || _.isEmpty(currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix])){
            currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix] = {};
            currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection] = {
                "position": activeParcel.Center(),
                "sketchZoom": 0.6,
                "sketchRotation": 0,
                "mapZoom": 20
            }
        }
        sketchOB = currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection]
        if (sketchOB) { 
        	if (!OsMap)
        		position = LatLngToPoint(sketchOB.position);
        	else 
				position = LatLngToPointOSM(sketchOB.position);        	
        }
    } else {
    	if (!OsMap)
        		position = LatLngToPoint(activeParcel.Center());
        	else 
				position = LatLngToPointOSM(activeParcel.Center()); 
    }
    if (!position) return;
    var x = parseFloat(position.x);
    var y = parseFloat(position.y);
    var mapDiv = $('#map'), reseizeWrap = $('#resizable-wrapper');
    if (OsMap) {
    	mapDiv = $('#mapCanvas'); reseizeWrap = $('.sketchOverlay');
    }
    var mapH = parseFloat(mapDiv.height());
    var divW = parseFloat(reseizeWrap.css("width")) / 2;
    var divH = parseFloat(reseizeWrap.css("height")) / 2;
    y = y - mapH - divH;
    x = (x - divW);
 
 	if(OsMap) 
 		ParcelCenter = PointToLatLngOSM(position);	
    else
    	ParcelCenter = PointToLatLng(new google.maps.Point(position.x, position.y));
    
    if (sketchOB) {
        var mZoom = sketchOB.mapZoom? sketchOB.mapZoom: null;
        rotateSketch(sketchOB.sketchRotation, true);
        zoomSketch(sketchOB.sketchZoom, true, null, null, mZoom);
        drawFlag = true;
        if(OsMap)
        	draggableSketch.fireEvent('dragend')
        else 
        	google.maps.event.trigger(container, tEvents.end);
    }
}

function LatLngToPoint(latlng) {
    if (activeParcel.Id && overlay && overlay.getProjection != null && overlay.getProjection())
        return overlay.getProjection().fromLatLngToContainerPixel(latlng);
}

function PointToLatLng(point) {
	if (overlay)
    	return overlay.getProjection().fromContainerPixelToLatLng(point)
}


function loggingfn(msg){
    console.log(msg);
}

function zoomSketch(zoom, noSketchpostionChange, changeAll, Change, mZoom) {
    if (isNaN(zoom)) return;
    let scale = zoom * 2;
    $('#sketch, .sketchOverlay').width(parseInt($('.ccse-img').attr('width')) * zoom);
    $('#sketch, .sketchOverlay').height(parseInt($('.ccse-img').attr('height')) * zoom);
    $('#resizable-wrapper').width(parseInt($('.ccse-img').attr('width')) * zoom);
    $('#resizable-wrapper').height(parseInt($('.ccse-img').attr('height')) * zoom);
    if (!noSketchpostionChange) setPositionOverlayIfDataExist();
    $('#svZoomSpan').html(parseInt(parseFloat(zoom).toFixed(2) * 100) + '%');
    $('#svZoomInput').val(zoom);
    sketchZoom = zoom;
    if (selectedSection) { 
        var skRowuid = top.$('.sketch-select option[value="' + selectedSketch + '"]').attr('skrowuid');
        var skPrefix = top.$('.sketch-select option[value="' + selectedSketch + '"]').attr('skprefix');
        skPrefix = isNewSketchData && skPrefix? skPrefix: '';
        var newSelectedSketch = isNewSketchData? skRowuid: selectedSketch;
        currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection].sketchZoom = zoom;
        if (mZoom) currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection].mapZoom = mZoom;
    }
    if (!changeAll) return
    var sk = sketchRenderer.sketches;
    sk.forEach(function (s, i) {
        var skRowuid = (s.parentRow && s.parentRow.ROWUID)? s.parentRow.ROWUID: s.sid;
        let skPrefix = (s.config && s.config.SketchLabelPrefixInSv)? s.config.SketchLabelPrefixInSv: '';
        if (parseFloat(skRowuid) < 0 && s.parentRow && s.config && s.config.SketchSource.Table) {
            let stble = s.config.SketchSource.Table;
            skRowuid = '$' + stble + '#' + skRowuid + '$';
        }
        i = isNewSketchData? skRowuid: i;
        skPrefix = isNewSketchData? skPrefix: '';
        for (var key in s.sections) {
            if (currentOverlayPosition["sketch-" + i + skPrefix]) {
                if (currentOverlayPosition["sketch-" +  i + skPrefix][key]) {
                    var keyZoom = currentOverlayPosition["sketch-" + i + skPrefix][key].sketchZoom;
                    if (Change == 'OUT') currentOverlayPosition["sketch-" + i + skPrefix][key].sketchZoom = keyZoom / 2;
                    else if (Change == 'IN') currentOverlayPosition["sketch-" + i + skPrefix][key].sketchZoom = keyZoom * 2;
                    currentOverlayPosition["sketch-" + i + skPrefix][key].modified = true;
                    if (mZoom) currentOverlayPosition["sketch-" + i + skPrefix][key].mapZoom = mZoom;
                }
            }
        }
    });
}

function rotateSketch(deg) {
    if (isNaN(deg)) return;
    degree = deg;
    if (OsMap) 
    	$(".sketchOverlay").css('transform-origin', "center");
    $("#sketch, .sketchOverlay").css('transform', "rotate("+ deg +"deg)");
    $('#svRotateInput').val(deg);
    $('#svRotateSpan').html(deg + '&deg;');
    if (selectedSection) {
		let skRowuid = $('.sketch-select option[value="' + selectedSketch + '"]').attr('skrowuid');
		let skPrefix = $('.sketch-select option[value="' + selectedSketch + '"]').attr('skprefix');
		skPrefix = isNewSketchData && skPrefix? skPrefix: '';
		let newSelectedSketch = isNewSketchData? skRowuid: selectedSketch;
        currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection].sketchRotation = deg;
        currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection].modified = true;
    }
}

function sketchSelectChange() {
	let selectedSketch = $('.sketch-select').val();
	let selectedSection = $('.section-select').val();
	previewSVSketch();
	let skRowuid = $('.sketch-select option[value="' + selectedSketch + '"]').attr('skrowuid');
	let skPrefix = $('.sketch-select option[value="' + selectedSketch + '"]').attr('skprefix');
	skPrefix = isNewSketchData && skPrefix? skPrefix: '';
	let selecteddpVal = isNewSketchData? skRowuid: selectedSketch;
	if (selecteddpVal && currentOverlayPosition && currentOverlayPosition['sketch-' + selecteddpVal + skPrefix] && currentOverlayPosition['sketch-' + selecteddpVal + skPrefix ]['1'] && currentOverlayPosition['sketch-' + selecteddpVal + skPrefix]['1'].mapZoom) {
		firstTimeLoad = false;
		mapZoom = currentOverlayPosition['sketch-' + selecteddpVal + skPrefix]['1'].mapZoom;
		if (OsMap) { 
			OsMap.setZoom(currentOverlayPosition['sketch-' + selecteddpVal + skPrefix]['1'].mapZoom);
			if (mapZoom > 19)
				OsMap.fireEvent('zoomend');
		}
		else map.setZoom(currentOverlayPosition['sketch-' + selecteddpVal + skPrefix]['1'].mapZoom);
	} 
}

class setupSVItemsOnMap {
    constructor(element, map){
        this.element = element;
        this.map = map;
    }
    showDropdowns(){
        let svControls = document.createElement("div");
        let svSketchSelect = document.createElement("select");
        svSketchSelect.className = "sketch-select";
		svSketchSelect.onchange = function(){ sketchSelectChange() };
        svControls.appendChild(svSketchSelect);
        let svSectionSelect = document.createElement("select");
        svSectionSelect.className = "section-select";
		svSectionSelect.onchange = function(){ sketchSelectChange() };
        svControls.appendChild(svSectionSelect);
        this.element.appendChild(svControls);
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(this.element);
    }
    showZoomOnMap(){
        let svZoomIcon = document.createElement("div");
        svZoomIcon.className = "svZoom";
        this.element.appendChild(svZoomIcon);
        let svZoomInput = document.createElement("input");
        svZoomInput.type = "range";
        svZoomInput.className = "zoomInput";
        svZoomInput.id = "svZoomInput";
        svZoomInput.min = "0.25";
        svZoomInput.max = "2.5";
        svZoomInput.step = "0.01";
        svZoomInput.style.width = "170px";
		svZoomInput[tEvents.onMove] = function(){ zoomSketch(this.value, true) };
		svZoomInput.onchange = function(){ zoomSketch(this.value, true) };
        let svZoomLabel = document.createElement("span");
        svZoomLabel.className = "svZRSpan svZoomLabel";
        svZoomLabel.innerHTML = 'Scale';
        svZoomIcon.appendChild(svZoomLabel);
        let svZoomMinus = document.createElement("span");
        svZoomMinus.className = "svZRControl svZoomMinus";
        svZoomMinus[tEvents.onclick] = function() { let zval = parseFloat($('.zoomInput').val()) - 0.01; zval = zval < 0.25? 0.25: zval; zoomSketch(zval, true); $('.zoomInput').val(zval); return false; };
        svZoomIcon.appendChild(svZoomMinus);
        svZoomIcon.appendChild(svZoomInput);
        let svZoomPlus = document.createElement("span");
        svZoomPlus.className = "svZRControl svZoomPlus";
        svZoomPlus[tEvents.onclick] = function() { let zval = parseFloat($('.zoomInput').val()) + 0.01; zval = zval > 2.5? 2.5: zval; zoomSketch(zval, true); $('.zoomInput').val(zval); return false; };
        svZoomIcon.appendChild(svZoomPlus);
        let svZoomSpan = document.createElement("div");
        svZoomSpan.id = "svZoomSpan";
        svZoomSpan.className = "svZRSpan";
        svZoomIcon.appendChild(svZoomSpan);
        this.element.appendChild(svZoomIcon);
        map.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(this.element);
    }
    showRotateOnMap(){
        let svZoomIcon = document.createElement("div");
        svZoomIcon.className = "svRotate";
        let svZoomInput = document.createElement("input");
        svZoomInput.type = "range";
        svZoomInput.className = "RotateInput";
        svZoomInput.id = "svRotateInput";
        svZoomInput.min = "-180";
        svZoomInput.max = "180";
        svZoomInput.step = "1";
        svZoomInput.style.width = "170px";
        svZoomInput[tEvents.onMove] = function(){ rotateSketch(this.value, true) };
		svZoomInput.onchange = function(){ rotateSketch(this.value, true) };
        let svZoomLabel = document.createElement("span");
        svZoomLabel.innerHTML = 'Rotate';
        svZoomLabel.className = "svZRSpan svRotateLabel";
        svZoomIcon.appendChild(svZoomLabel);
        let svRotateMinus = document.createElement("span");
        svRotateMinus.className = "svZRControl svRotateMinus";
        svRotateMinus[tEvents.onclick] = function() { let zval = parseFloat($('.RotateInput').val()) - 1; zval = zval < -180? -180: zval; rotateSketch(zval, true); $('.RotateInput').val(zval); return false; };
        svZoomIcon.appendChild(svRotateMinus);
        svZoomIcon.appendChild(svZoomInput);
        let svRotatePlus = document.createElement("span");
        svRotatePlus.className = "svZRControl svRotatePlus";
        svRotatePlus[tEvents.onclick] = function() { let zval = parseFloat($('.RotateInput').val()) + 1; zval = zval > 180? 180: zval; rotateSketch(zval, true); $('.RotateInput').val(zval); return false; };
        svZoomIcon.appendChild(svRotatePlus);
        let svZoomSpan = document.createElement("div");
        svZoomSpan.id = "svRotateSpan";
        svZoomSpan.className = "svZRSpan";
        svZoomIcon.appendChild(svZoomSpan);
        this.element.appendChild(svZoomIcon);
        map.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(this.element);
    }
}

function saveReview( msg, title ) {
    let data, updateData = {};
    showMask('Saving changes..');
    function saveReviewNew(plyArray) {
        var polygonArray = '';
        if (plyArray) {
            let polyObject = { polygonArray: plyArray, parcelId: activeParcel.Id };
            polygonArray = JSON.stringify(polyObject);
        }
        
    	let newCurrentOverlayPosition = isNewSketchData? currentOverlayPosition: {};
        if (!isNewSketchData) {
        	$('.sketch-select option').each(function(index, opt) {
        		let optval = (opt.value || opt.value == 0)? opt.value.toString(): opt.value;
        		let skRowId = $(opt).attr('skrowuid'), skPrefix = $(opt).attr('skprefix');
				skPrefix = skPrefix? skPrefix: '';
        		if (currentOverlayPosition["sketch-" + optval]) 
        			newCurrentOverlayPosition["sketch-" + skRowId + skPrefix] = currentOverlayPosition["sketch-" + optval];
			});
        }
        
        data = {
            Description: title,
            pid: activeParcel.Id,
            SketchZoom: scale,
            MapZoom: mapZoom,
            SketchRotation: degree,
            SketchData: JSON.stringify(newCurrentOverlayPosition),
            sketchLatLng: sketchLatLng,
            NewSketchData: JSON.stringify(newCurrentOverlayPosition),
            polygonArray: polygonArray
        };
        updateData = data;
	    if(!activeParcel.sketchLatLng && !(isSvSketchChanged || data.isSketchChanged)){
	        data.sketchLatLng = "";
	    }
        data = JSON.stringify(data);
        ccma.Sync.enqueueParcelChange(activeParcel.Id, '', '', 'MASVSAVE', 'MASVSAVE', 'MASVSAVE', "", data, {
	        updateSvData: true,
	        updataSvDataObject: updateData,
            source: activeParcel,
            sourceFields: ['SketchZoom', 'SketchRotation', 'MapZoom', 'SketchData', 'sketchLatLng', 'NewSketchData']
	    }, function () {
			initDraggableOverlay(true);
	    }, function () {
	    	
	    }); 
    }
    saveReviewNew();
            
}

//function saveSvReview() {
//	let rotFlag, currSketRot, sketchoverlay;
//    if (activeParcel.SketchData) sketchoverlay = $.parseJSON(activeParcel.SketchData);
//    if (sketchoverlay && top.$('.sketch-select').val() && top.$('.section-select').val()) {
//        let seletedSketch = top.$('.sketch-select').val();
//        let skRowuid = top.$('.sketch-select option[value="' + seletedSketch + '"]').attr('skrowuid');
//		let skPrefix = top.$('.sketch-select option[value="' + seletedSketch + '"]').attr('skprefix');
//		skPrefix =  isNewSketchData && skPrefix? skPrefix: ''; seletedSketch =  isNewSketchData? (skRowuid + skPrefix) : seletedSketch;
//        currSketRot = (sketchoverlay["sketch-" + seletedSketch] && sketchoverlay["sketch-" + seletedSketch][top.$('.section-select').val()]) ? sketchoverlay["sketch-" + seletedSketch][top.$('.section-select').val()].sketchRotation : null;
//    }
//    rotFlag = currSketRot ? currSketRot : activeParcel.SketchRotation;
//    if ( rotFlag != $('.RotateInput').val() || isSvSketchChanged) {
//        $('#sketchid').val($('.sketch-select').val());
//        saveReview("Details Saved Sucessfully.", 1);
//        isSvSketchChanged = false;
//    }
//    let zoomFlag = mapZoom ? mapZoom : activeParcel.MapZoom; // zoom saving
//    if (zoomFlag != $('#svZoomInput').val() || isSvZoomChanged) {
//        $('#sketchid').val($('.sketch-select').val());
//        saveReview("Details Saved Successfully.", 1);
//        isSvZoomChanged = false;
//    }
//}
function saveSvReview() {
    let rotFlag, currSketRot, sketchoverlay;
    if (activeParcel.SketchData) sketchoverlay = $.parseJSON(activeParcel.SketchData);
    if (sketchoverlay && top.$('.sketch-select').val() && top.$('.section-select').val()) {
        let seletedSketch = top.$('.sketch-select').val();
        let skRowuid = top.$('.sketch-select option[value="' + seletedSketch + '"]').attr('skrowuid');
        let skPrefix = top.$('.sketch-select option[value="' + seletedSketch + '"]').attr('skprefix');
        skPrefix = isNewSketchData && skPrefix ? skPrefix : ''; seletedSketch = isNewSketchData ? (skRowuid + skPrefix) : seletedSketch;
        currSketRot = (sketchoverlay["sketch-" + seletedSketch] && sketchoverlay["sketch-" + seletedSketch][top.$('.section-select').val()]) ? sketchoverlay["sketch-" + seletedSketch][top.$('.section-select').val()].sketchRotation : null;
    }
    rotFlag = currSketRot ? currSketRot : activeParcel.SketchRotation;
    if (rotFlag != $('.RotateInput').val() || isSvSketchChanged) {
        $('#sketchid').val($('.sketch-select').val());
        saveReview("Details Saved Sucessfully.", 1);
        isSvSketchChanged = false;
    }
}
//OSM functions
function svOsmDivCreation(OsMap) {
	function showDropdownsOSM() {
	    let svControls = L.DomUtil.create("div");
	    let svSketchSelect = L.DomUtil.create("select");
	    svSketchSelect.className = "sketch-select";
		svSketchSelect.onchange = function(){ sketchSelectChange() };
	    svControls.appendChild(svSketchSelect);
	    let svSectionSelect = L.DomUtil.create("select");
	    svSectionSelect.className = "section-select";
		svSectionSelect.onchange = function(){ sketchSelectChange() };
	    svControls.appendChild(svSectionSelect);
	    return svControls;
	}
	
	function showZoomOnMapOSM() {
	    let svZoomIcon = L.DomUtil.create("div");
        svZoomIcon.className = "svZoom";
        let svZoomInput = L.DomUtil.create("input");
        svZoomInput.type = "range";
        svZoomInput.className = "zoomInput";
        svZoomInput.id = "svZoomInput";
        svZoomInput.min = "0.25";
        svZoomInput.max = "2.5";
        svZoomInput.step = "0.01";
        svZoomInput.style.width = "170px";
		svZoomInput[tEvents.onMove] = function() { OsMap.dragging.disable(); zoomSketch(this.value, true); OsMap.dragging.enable(); };
		svZoomInput.onchange = function(){ zoomSketch(this.value, true) };
        let svZoomLabel = L.DomUtil.create("span");
        svZoomLabel.className = "svZRSpan svZoomLabel";
        svZoomLabel.innerHTML = 'Scale';
        svZoomIcon.appendChild(svZoomLabel);
        let svZoomMinus = L.DomUtil.create("span");
        svZoomMinus.className = "svZRControl svZoomMinus";
        svZoomMinus[tEvents.onclick] = function(e) { e.stopPropagation(); OsMap.dragging.disable(); let zval = parseFloat($('.zoomInput').val()) - 0.01; zval = zval < 0.25? 0.25: zval; zoomSketch(zval, true); $('.zoomInput').val(zval);  OsMap.dragging.enable(); return false; };
        svZoomMinus[tEvents.ondblclick] = function(e) { e.stopPropagation(); return false; };
        svZoomIcon.appendChild(svZoomMinus);
        svZoomIcon.appendChild(svZoomInput);
        let svZoomPlus = L.DomUtil.create("span");
        svZoomPlus.className = "svZRControl svZoomPlus";
        svZoomPlus[tEvents.onclick] = function(e) { e.stopPropagation(); OsMap.dragging.disable(); let zval = parseFloat($('.zoomInput').val()) + 0.01; zval = zval > 2.5? 2.5: zval; zoomSketch(zval, true); $('.zoomInput').val(zval); OsMap.dragging.enable(); return false; };
        svZoomPlus[tEvents.ondblclick] = function(e) { e.stopPropagation(); return false; };
        svZoomIcon.appendChild(svZoomPlus);
        let svZoomSpan = L.DomUtil.create("div");
        svZoomSpan.id = "svZoomSpan";
        svZoomSpan.className = "svZRSpan";
        svZoomIcon.appendChild(svZoomSpan);
	    return svZoomIcon;
	}
		
	function showRotateOnMapOSM() {
	    let svZoomIcon = L.DomUtil.create("div");
        svZoomIcon.className = "svRotate";
        let svZoomInput = L.DomUtil.create("input");
        svZoomInput.type = "range";
        svZoomInput.className = "RotateInput";
        svZoomInput.id = "svRotateInput";
        svZoomInput.min = "-180";
        svZoomInput.max = "180";
        svZoomInput.step = "1";
        svZoomInput.style.width = "170px";
        svZoomInput[tEvents.onMove] = function() { OsMap.dragging.disable(); rotateSketch(this.value, true); OsMap.dragging.enable(); };
		svZoomInput.onchange = function(){ rotateSketch(this.value, true); };
        let svZoomLabel = document.createElement("span");
        svZoomLabel.innerHTML = 'Rotate';
        svZoomLabel.className = "svZRSpan svRotateLabel";
        svZoomIcon.appendChild(svZoomLabel);
        let svRotateMinus = L.DomUtil.create("span");
        svRotateMinus.className = "svZRControl svRotateMinus";
        svRotateMinus[tEvents.onclick] = function(e) { e.stopPropagation(); OsMap.dragging.disable(); let zval = parseFloat($('.RotateInput').val()) - 1; zval = zval < -180? -180: zval; rotateSketch(zval, true); $('.RotateInput').val(zval); OsMap.dragging.enable(); return false; };
        svRotateMinus[tEvents.ondblclick] = function(e) { e.stopPropagation(); return false; };
        svZoomIcon.appendChild(svRotateMinus);
        svZoomIcon.appendChild(svZoomInput);
        let svRotatePlus = L.DomUtil.create("span");
        svRotatePlus.className = "svZRControl svRotatePlus";
        svRotatePlus[tEvents.onclick] = function(e) { e.stopPropagation(); OsMap.dragging.disable(); let zval = parseFloat($('.RotateInput').val()) + 1; zval = zval > 180? 180: zval; rotateSketch(zval, true); $('.RotateInput').val(zval); OsMap.dragging.enable(); return false; };
        svRotatePlus[tEvents.ondblclick] = function(e) { e.stopPropagation(); return false; };
        svZoomIcon.appendChild(svRotatePlus);
        let svZoomSpan = L.DomUtil.create("div");
        svZoomSpan.id = "svRotateSpan";
        svZoomSpan.className = "svZRSpan";
        svZoomIcon.appendChild(svZoomSpan);
	    return svZoomIcon;
	}
	
	/*var dropDownDiv = L.Control.extend({options : {
	    position: 'topright'
		},
		onAdd : function() {
			let svDropdown = L.DomUtil.create("div"); 
			svDropdown.className = 'svDropdown cssSvDropdown';
			let svdp = showDropdownsOSM();
			svDropdown.appendChild(svdp);
			return svDropdown;
		}
	})
	OsMap.addControl(new dropDownDiv());*/
	
	var rootDiv = L.Control.extend({options : {
		   position: 'bottomleft'
		},
		onAdd : function() {
			let svRoot = L.DomUtil.create("div"); 
			svRoot.className = 'svRoot';
			let svzm = showZoomOnMapOSM();
			let svrt = showRotateOnMapOSM();
			svRoot.appendChild(svzm);
			svRoot.appendChild(svrt);
			return svRoot;
		}
	})
	OsMap.addControl(new rootDiv());
}


function populateSketchOSM(sketchURL, dropDownChange, saveFrom) {
	let ltlng;
	let bounds = L.latLngBounds();
	OsMap.setMinZoom(15); 
	if(activeParcel.MapPoints){
		activeParcel.MapPoints.sort((x,y) => { return x.Ordinal - y.Ordinal }).forEach((item, index) => {
			let latlng = L.latLng(item.Latitude, item.Longitude);
			bounds.extend(latlng);
        });
		polygonCenter = bounds.getCenter();
	}
	if (!(saveFrom && overlay))
		$('.sketchOverlay').remove();
    if (activeParcel.lat && activeParcel.lng)
        ltlng = L.latLng(activeParcel.lat, activeParcel.lng);
    if (activeParcel.SketchData) {
		var skRowuid = $('.sketch-select option[value="' + selectedSketch + '"]').attr('skrowuid');
		var skPrefix = $('.sketch-select option[value="' + selectedSketch + '"]').attr('skprefix');
		skPrefix = isNewSketchData && skPrefix ? skPrefix: '';
		var newSelectedSketch = isNewSketchData ? skRowuid: selectedSketch;
        var skd = JSON.parse(activeParcel.SketchData);
        var tag = "sketch-" + newSelectedSketch + skPrefix;
        if (!_.isEmpty(skd[tag])) {
            var pos = skd[tag][selectedSection].position;
            ltlng = L.latLng(pos.lat, pos.lng);
        }
    }
	
	function dragableimageOverlay(sketchUrl, latLng, oMap, properties) {
		this.overlay = L.imageOverlay(sketchUrl, latLng, properties).addTo(oMap);
		this.overlay._image.style.position = 'absolute';
        this.overlay._image.draggable = true;
        
        L.DomUtil.setPosition(this.overlay._image, latLng[0]);
        draggableSketch = new L.Draggable(this.overlay._image);
        draggableSketch.enable();
        draggableSketch.overlay = this.overlay;
        
        this.overlay.draw = function () {
        	var pos = this._image._leaflet_pos;
			if (pos) {
				this._image.style.left = pos.x + 'px';
				this._image.style.top = pos.y + 'px';
				let zoom = parseFloat($('#svZoomInput').val());
                let angle =  parseFloat($('#svRotateInput').val());
                this.setZIndex(100000000001);
				zoomSketch(zoom, true);
        		rotateSketch(angle, true);
				if (activeParcel && !activeParcel.sketchLatLng && !isSvSketchChanged) {
					var divW = parseFloat($('.sketchOverlay').css("width")) / 2;
					var divH = parseFloat($('.sketchOverlay').css("height")) / 2;
					pos.y = pos.y - divH;
					pos.x = (pos.x - divW);
					this._image.style.left = pos.x + 'px';
					this._image.style.top = pos.y + 'px';
				}
			}
        }
        
        oMap.on('zoomend', function() {
        	if (activeParcel) {
			    let zoomLevel = oMap.getZoom(),  zoomdiff = 0;
	            mapZoom = mapZoom ? mapZoom : zoomLevel;
	            zoomdiff = Math.abs(mapZoom - zoomLevel);
	            if (mapZoom > zoomLevel) zoomSketch(sketchZoom / Math.pow(2, zoomdiff), true, false, 'OUT', zoomLevel);
	            else if (mapZoom < zoomLevel) zoomSketch(sketchZoom * Math.pow(2, zoomdiff), true, false, 'IN', zoomLevel);              
				setPositionOverlayIfDataExist();			 
	            mapZoom = zoomLevel;
            }
		});
		
		this.overlay.on(tEvents.leave, function () {
	        this.fireEvent(tEvents.end)
        });
        
        this.overlay.on(tEvents.tclick, function () {
	        this.fireEvent(tEvents.end)
	    });
		
		draggableSketch.on('dragstart', function (e) {
			this.overlay._image.style.cursor = 'move';
            this.overlay._map.dragging.disable();
			this.on('drag', function (e) {
				var latLng = PointToLatLngOSM({ x: e.target._newPos.x, y: e.target._newPos.y });
                var corner1 = L.latLng(latLng.lat, latLng.lng), corner2 = L.latLng(latLng.lat, latLng.lng);
                this.overlay.setBounds(L.latLngBounds(corner1, corner2));
                isSvSketchChanged = true;
                firstTimeLoad = false;
                this.overlay.draw();
			});
		});
		
		draggableSketch.on('dragend', function (e) {
			var skRowuid = top.$('.sketch-select option[value="' + selectedSketch + '"]').attr('skrowuid');
            var skPrefix = top.$('.sketch-select option[value="' + selectedSketch + '"]').attr('skprefix');
            skPrefix = isNewSketchData && skPrefix? skPrefix: '';
            var newSelectedSketch = isNewSketchData? skRowuid: selectedSketch;
            if (drawFlag) {
                var latLng;
                if (newSelectedSketch && selectedSection) latLng = currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection].position;
                if (latLng) {
	                var _lat = typeof (latLng.lat) === 'function' ? latLng.lat(): latLng.lat, _lng = typeof (latLng.lng) === 'function' ? latLng.lng(): latLng.lng;
	                var corner1 = L.latLng(_lat, _lng), corner2 = L.latLng(_lat, _lng);
	                this.overlay.setBounds(L.latLngBounds(corner1, corner2));
	                this.overlay.draw();
                }
                drawFlag = false;
            }
            else {
                this.overlay._map.dragging.enable();
                this.overlay._image.style.cursor = 'default';
                this.origin = e;
                this.removeEventListener('drag');
                if (this.overlay.getBounds() && this.overlay.getBounds()._northEast.lat) {
                	var pst = this.overlay.getBounds()._northEast;
                    sketchLatLng = pst.lat + ',' + pst.lng;
                    let zoom = $('#svZoomInput').val();
                    let angle =  $('#svRotateInput').val();
                    if (currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix] && selectedSection) {
                        currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection] = {
                            "position": {lat: pst.lat, lng: pst.lng},
                            "sketchZoom": zoom,
                            "sketchRotation": parseFloat(angle),
                            "mapZoom": mapZoom,
                            modified: true
                        }
                    }
                }
            }
		});
	}
	
	if (!(saveFrom && overlay)) {
		ltlng = ltlng ? ltlng : polygonCenter;
    	overlay = new dragableimageOverlay(sketchURL, [[ltlng.lat, ltlng.lng], [ltlng.lat, ltlng.lng]], OsMap, { className: ' sketchOverlay', interactive: true, 'z-index': 100000001}); //L.imageOverlay(sketchURL, [[ltlng.lat, ltlng.lng], [ltlng.lat, ltlng.lng]], { className: ' sketchOverlay', interactive: true, 'z-index': 100000001}).addTo(OsMap);
    }
    else 
    	$('.sketchOverlay')[0].src = sketchURL;
	var timeOutOverlay = true;
	if (top.$('.sketch-select option').length == 0) {
		if ($('#maskLayer').css('display') != 'none') $('#maskLayer').hide();
		if (firstTimeSvLoad) firstTimeSvLoad = false;
		timeOutOverlay = false;
	}
    if (ParcelDataExist) {
		var mZoom = null;
        setOverLayPosition(activeParcel.lat, activeParcel.lng, sketchZoom, activeParcel.SketchRotate, activeParcel.SketchData, null, dropDownChange);
        ParcelDataExist = true;
        setPositionOverlayIfDataExist();
		if (timeOutOverlay && (firstTimeSvLoad || !saveFrom)) {
			let tm = (firstTimeSvLoad? 2000: 750);
			setTimeout(function () {
				setPositionOverlayIfDataExist();
				$('.overlay').show();
				if ($('#maskLayer').css('display') != 'none') $('#maskLayer').hide();
				if (firstTimeSvLoad) firstTimeSvLoad = false;
			}, tm);
		}
        scale = isNaN(parseFloat(sketchOB.sketchZoom)) ? 0 : parseFloat(sketchOB.sketchZoom);
        RotationDegree = parseFloat(sketchOB.sketchRotation);
		var skRowuid = top.$('.sketch-select option[value="' + selectedSketch + '"]').attr('skrowuid');
		var skPrefix = top.$('.sketch-select option[value="' + selectedSketch + '"]').attr('skprefix');
		skPrefix = isNewSketchData && skPrefix? skPrefix: '';
		var newSelectedSketch = isNewSketchData? skRowuid: selectedSketch;
        if(currentOverlayPosition && newSelectedSketch && selectedSection && currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection].position){
            var pos = LatLngToPointOSM(currentOverlayPosition["sketch-" +  newSelectedSketch + skPrefix][selectedSection].position)
            var divW = divH = (400 * scale)/2;
            if(divH && divW && pos){
                pos.y = pos.y + divH;
                pos.x = (pos.x + divW);
                var center = PointToLatLngOSM(pos);
                OsMap.panTo(center);
            }
            else
                OsMap.panTo(currentOverlayPosition["sketch-" +  newSelectedSketch + skPrefix][selectedSection].position);
			if (currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection].mapZoom) 
				mZoom = currentOverlayPosition["sketch-" + newSelectedSketch + skPrefix][selectedSection].mapZoom;
        } else
		    OsMap.panTo(bounds.getCenter());
		mZoom = mapZoom = mZoom ? mZoom: activeParcel.MapZoom;
		var nZoom = mZoom > 19? 19: null;
		OsMap.setZoom(parseInt(mZoom));
        zoomSketch(sketchOB.sketchZoom);
        rotateSketch(parseFloat(sketchOB.sketchRotation), true);
        if (nZoom)
        	OsMap.fireEvent('zoomend');

    } else {
        setOverLayPosition(polygonCenter.lat, polygonCenter.lng, 0.6, 0, null, dropDownChange);
        setPositionOverlayIfDataExist(true);
		if (timeOutOverlay && (firstTimeSvLoad || !saveFrom)) {
			let tm = (firstTimeSvLoad? 2000: 750);
			setTimeout(function () {
				setPositionOverlayIfDataExist();
				$('.overlay').show();
				if ($('#maskLayer').css('display') != 'none') $('#maskLayer').hide();
				if (firstTimeSvLoad) firstTimeSvLoad = false;
			}, tm);
		}
        OsMap.panTo(bounds.getCenter());
        OsMap.setZoom(19);
        mapZoom = 19;
        zoomSketch(0.6, true);
        rotateSketch(0, true);
    }
}

function LatLngToPointOSM (ltlng) {
	var _lat = typeof (ltlng.lat) === 'function' ? ltlng.lat(): ltlng.lat, _lng = typeof (ltlng.lng) === 'function' ? ltlng.lng(): ltlng.lng;
	return OsMap.latLngToLayerPoint(L.latLng(_lat, _lng));
}

function PointToLatLngOSM(points) {
    return OsMap.layerPointToLatLng(L.point(points.x, points.y));
}
