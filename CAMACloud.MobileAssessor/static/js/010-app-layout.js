﻿//    DATA CLOUD SOLUTIONS, LLC ("COMPANY") CONFIDENTIAL 
//    Unpublished Copyright (c) 2010-2013 
//    DATA CLOUD SOLUTIONS, an Ohio Limited Liability Company, All Rights Reserved.

//    NOTICE: All information contained herein is, and remains the property of COMPANY.
//    The intellectual and technical concepts contained herein are proprietary to COMPANY
//    and may be covered by U.S. and Foreign Patents, patents in process, and are protected
//    by trade secret or copyright law. Dissemination of this information or reproduction
//    of this material is strictly forbidden unless prior written permission is obtained
//    from COMPANY. Access to the source code contained herein is hereby forbidden to
//    anyone except current COMPANY employees, managers or contractors who have executed
//    Confidentiality and Non-disclosure agreements explicitly covering such access.</p>

//    The copyright notice above does not evidence any actual or intended publication
//    or disclosure of this source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
//    DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
//    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
//    IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
//    OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
//    TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
//    ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.


//log('Loading layout engine ...');

//var usableAppHeight;
//var usableAppWidth;
//var activeDCTabId;
var sourceDataInfo = {};
var sqlinsert = "";
var sqlDelete = "";
var firstselect = true;
var dateblank = false;
var relationcycle = [];
var screenHeight = 0;
var fieldUpdateStatus = [];
var lastFieldValue = ''
var pIdIndex = 0;  //select first child when click on selectdcTab
var trSelectVar = 0; //select first child when click parent rows
var multigridTable = ""; //move to exact childtable when up button click
var moveIndicator = 0; //move option click indicator
var trackvalue = '';
var trackfieldname;

var lastDim = {};

function setScreenDimensions(ev, donotrecalc, cancelScrollRefresh, callback, orientationChanged) {
    if (ev) ev.preventDefault();
    window.scrollTo(0, 0);
    //var tt = false;
    var dbTab = document.createElement("section");
    var spacer = document.createElement("div");
    $(dbTab).addClass('spacer')
    if (navigator.standalone) {
        $('.app-title-line').show();
        document.body.scrollTop = 0;
    }

    var windowHeight = window.innerHeight;
    var windowWidth = window.innerWidth;

    var inversed = false;
    var winAR = windowHeight / windowWidth;
    var screenAspectRatio = screen.height / screen.width

    var landscape = !WNTD && (Math.abs(window.orientation) == 90 || (screen.orientation && screen.orientation.type.split('-')[0] == "landscape"));
    if (lastDim[landscape] && windowWidth == lastDim[landscape].width && windowHeight < lastDim[landscape].height) windowHeight = lastDim[landscape].height;
    if (!WNTD && ((landscape && windowHeight > windowWidth) || (!landscape && windowHeight < windowWidth))) { [windowHeight, windowWidth] = [windowWidth, windowHeight]; inversed = true; };

    lastDim[landscape] = { width: windowWidth, height: windowHeight };

    var dimensions = '<div>' + (windowHeight + 'x' + windowWidth + ' Screen: [' + screen.height + 'x' + screen.width + ']' + ' ' + Math.abs(window.orientation) + '&deg; ') + (inversed ? 'I' : '') + '</div>';

    /////$('header span', $('#' + ccma.UI.ActiveScreenId)).html(dimensions);

    //if (windowsTouch && windowWidth > windowHeight) 
    //    tt = true;
    //if (Math.abs(window.orientation) == 0 || Math.abs(window.orientation) == 180) {
    //    screenHeight = screen.height;
    //	if (iPad) {
    //		windowHeight = screen.height - 20;
    //		windowWidth = screen.width;
    //		if (document.body.offsetWidth != screen.width) {
    //			$(document.body).width(screen.width)
    //			$(document.body).height(screen.height - 10)
    //			$(spacer).css({ 'height': '400px' });
    //			setTimeout(function(){ setScreenDimensions(ev, donotrecalc, cancelScrollRefresh, callback); }, 200);
    //			return
    //		}
    //	}
    //	else {
    //		if (windowWidth > (screen.width + 10)) {
    //			$(document.body).width(screen.width);
    //			setScreenDimensions(ev, donotrecalc, cancelScrollRefresh, callback);
    //			return;
    //		}
    //	}
    //} else { 
    //	screenHeight = screen.width;
    //	if (iPad) {
    //		windowHeight = screen.width;
    //		windowWidth = screen.height;
    //	}
    //}

    $(document.body).height(windowHeight);
    $(document.body).width(windowWidth);
    setCustomddl(windowHeight, windowWidth);
    if (landscape) {
        $('.page-header').hide();
        $('.page-footer').hide();
        $('.spacer').css('height', '0px');
        $('.sketch-preview').css({ 'height': '550px' });
        if (infoContentScroll) {
            $('.infoContentFooter').css('padding', '7px');
            //$('.infoContentContainer').height(367);
        }
        if ($('.overlay-photo-properties').css('display') != 'none')
            $('.photo-meta-fields').css('max-height', '40%');
    } else {
        $('.page-header').show();
        $('.page-footer').show();
        $('.spacer').css('height', '200px');
        $('.sketch-preview').css({ 'height': '630px' });
        if (infoContentScroll) {
            $('.infoContentFooter').css('padding', '5px');
            //$('.infoContentContainer').height(365);
        }
        if ($('.overlay-photo-properties').css('display') != 'none')
            $('.photo-meta-fields').css('max-height', '55%');
    }
    $('#dc-form-contents .spacer').remove();
    //$(dbTab).append(spacer);
    //$('#dc-form-contents').append(dbTab);
    setCustomddl(windowHeight, windowWidth);
    setMask(windowHeight, windowWidth);
    setGeoLocationScreen(windowHeight, windowWidth);
    var screenWidth = $('.screen').width();
    var titleHeight = $('.app-title-line').height();
    var headerHeight = $('.page-header').height();
    var footerHeight = $('.page-footer').height();
    var pageMargin = 10;
    ccma.UI.Layout.Screen.UsableHeight = windowHeight - titleHeight - headerHeight - footerHeight - pageMargin;
    ccma.UI.Layout.Screen.UsableWidth = windowWidth - pageMargin;
    $('.page-content').height(ccma.UI.Layout.Screen.UsableHeight);
    $('.page-content').css('overflow', 'hidden');
    //$('section').height(ccma.UI.Layout.Screen.UsableHeight);
    //$('section').css('min-height', ccma.UI.Layout.Screen.UsableHeight + 'px');
    //$('section').css('max-height', ccma.UI.Layout.Screen.UsableHeight + 'px');
    if (sketchApp) {
        sketchApp.resizeCanvas(windowWidth, windowHeight - 80);
        var sb = sketchApp.sketchBounds();
        sketchApp.panOrigin(sb, sketchApp.formatter.originPosition);
        sketchApp.render();
    }
    if (!donotrecalc)
        recalculateDimensions(cancelScrollRefresh);
    $('.page-content').scrollTop(0);
    
    $('#dc-form-contents section').forEach(function(axForm) {
    	if($(axForm).css('display') != 'none') {
	    	 $('fieldset[field-type="5"]', axForm).each((i, fieldset)=> { 
                 recalculateCustomDropdownWidth(fieldset, landscape);
                 if ($('.newvalue', $(fieldset))[0] && $('.newvalue', $(fieldset)).attr('customWidth')) {
                     $('.newvalue', $(fieldset)).removeAttr('customWidth');
                     $('.newvalue', $(fieldset)).css('width', '');
                 }
                 recalculateCustomDropdownTextWidth(fieldset);
	    	 });
    	}
    });
    
    //refreshScrollable(undefined, true)
     //keyboardActive = screenHeight > $(window).height() + 100 ? true : false;
    //if ($('.cc-drop-pop').css('display') == 'flex') reCalculateLookupPosition(landscape, orientationChanged);
    //$('.cc-drop-pop.cc-drop-mini').css('width', '300px');
    //if (callback) callback();

    keyboardActive = screenHeight > $(window).height() + 100;

    if ($('.cc-drop-pop').css('display') === 'flex') {
        reCalculateLookupPosition(landscape, orientationChanged);
    }

    //if (ccma.UI.ActiveScreenId == 'user-dashboard') {
    //    $('.cc-drop-pop.cc-drop-mini').css('width', '300px');
    //}

    //if (ccma.UI.ActiveScreenId == 'sort-screen') {
    //    $('.cc-drop-pop.cc-drop-mini').css('width', '300px');

    //}
    
    //if (ccma.UI.ActiveScreenId == 'copyParcel') {
    //    $('.cc-drop-pop.cc-drop-mini').css('width', '197px');
    //}

    //if (ccma.UI.ActiveScreenId == 'search') {
    //    $('.cc-drop-pop.cc-drop-mini').css('width', '288px');
    //}

    if (ccma.UI.ActiveScreenId == 'gis-map' && wmsOlMap && typeof (wmsOlMap.updateSize) == 'function' && $('#nearmapwms')?.css('display') != 'none') {
        wmsOlMap.updateSize();
    }


    if (ccma.UI.ActiveScreenId == 'gis-map' && nmOlMap && typeof (nmOlMap.updateSize) == 'function' && $('#nearmap')?.css('display') != 'none') {
        nmOlMap.updateSize();
    }

    //else {
    //    $('.cc-drop-pop.cc-drop-mini').css('width', '300px');
    //}

    if (callback) {
        callback();
    }
}

Object.defineProperty(window, 'toggleWinTouch', { get: function () { WNTD = !WNTD; $(window).trigger('resize'); return WNTD; } })

var delayedScreenResetTimeout = null;
function setScreenDimensionsDelayed(ev, donotrecalc, cancelScrollRefresh, callback) {
    //console.error('Set Screen Dimenions')
    if (delayedScreenResetTimeout == null) {
        delayedScreenResetTimeout = window.setTimeout(function () {
            delayedScreenResetTimeout = null;
            setScreenDimensions(ev, donotrecalc, cancelScrollRefresh, callback);
        }, 150);
    }
}

function loadDataCollectionCard(callback) {
    log('Loading Data Collection Card ...');
    getData("SELECT fc.*, CASE WHEN fc.ParentCategoryId IS NULL THEN 'ParentCat' ELSE 'SubCat' END IsSubCat, CASE WHEN (Type IS NULL OR Type = 0)   THEN 0  ELSE Type END as Type  FROM FieldCategory fc  ORDER BY fc.Ordinal * 1", [], function (categories) {   //FieldCategory
        ccma.Data.FieldCategories = _.clone(categories);
        $('#dc-form-contents').html($('#dc-form-template').html());
        var catDiv = document.createElement("div");
        catDiv.innerHTML = '<a href="#dctab_${Id}" onclick="return selectDcTab(${Id}, this,undefined,undefined,0,null)" id="dctabkey_${Id}" class="" type="${IsSubCat}" BPPType = "${Type}">${Name}</a>';
        var categoryToShow = categories.filter(function (f) { return f.ShowCategory == '1' })
        $(catDiv).fillTemplate(categoryToShow);
        $('#dc-categories div').html('<a class="selected" special onclick="return selectDcTab(0, this,undefined,undefined,0,null);" id="dctabkey_0">Quick Review</a>');
        $('#dc-categories div').first().append(catDiv);
        $.onswipe('#dc-categories', 'left', function (pi, leftSwiped) {
            $('#dc-categories').css({ 'left': '-' + $('#dc-categories').width() + 'px' });
        });
        $('#dc-form').unbind(touchClickEvent);
        $('#dc-form').bind(touchClickEvent, function () {
            $('#dc-categories').css({ 'left': '-' + 290 + 'px' });
        });
        $('a[type="SubCat"]', catDiv).hide();
        var catIndex = 0;
        var totalCats = categories.length;
        var qrTab = document.getElementsByName('quickreview')[0];
        getData("SELECT F.Id As fieldId, F.Name, Label, QuickReview, DoNotShow, DoNotShowOnMA, ReadOnly, InfoContent FROM Field F LEFT JOIN FieldCategory FC ON (F.CategoryId = FC.Id) WHERE QuickReview = 'true' ORDER BY F.FavoriteOrdinal * 1, FC.Ordinal * 1, F.Serial * 1", [], function (fields) {
            for (var fi in fields) {
                var field = fields[fi];
                var fieldSet = getFieldSet(field, { readOnly: true });
                $(qrTab).append(fieldSet);
            }
            bindInfoButtonClick();
        });

        console.time('Loading DCC Tabs');

        processQueue(categories, _loadDCCTab, function () {
            bindInfoButtonClick();

            console.timeEnd('Loading DCC Tabs');
            callback && callback();
        }, function () {
            console.log('Loading DCC Failed.')
        }, function (t, p) {
            var sp = 45, ep = 70, dp = ep - sp;
            setSplashProgress('Loading data collection tabs ...', Math.round(sp + (p / t) * dp));
            console.log('Completed loading category ', p, ' out of ', t)
        })

    });                                                                                                                                                                                                                        //FieldCategory

}

function _loadDCCTab(cat, callback) {
    console.group(cat.Name);
    getData('SELECT * FROM Field WHERE CategoryId = ? ORDER BY Serial * 1', [parseInt(cat.Id).toString()], function (fields, cat) {
        getData("SELECT *, CASE WHEN (Type IS NULL OR Type = 0)  THEN 0  ELSE Type END as Type FROM FieldCategory WHERE ParentCategoryId = ? AND ShowCategory = 1  ORDER BY Ordinal * 1", [parseInt(cat.Id).toString()], function (subcats, args) {
            var fields = args[0];
            var cat = args[1];
            var dbTab = document.createElement("section");
            $(dbTab).css('display', 'none');
            $(dbTab).attr('catid', cat.Id);
            $(dbTab).attr('pcatid', cat.OverrideParentCategory || cat.ParentCategoryId || '');
            if (cat.SortExpression) {
                $(dbTab).attr('sort-exp', cat.SortExpression);
            }

            var tabContent = document.createElement('div');
            tabContent.className = 'data-entry-tab-content';

            var readOnly = false;
            var allowAddDelete = true;
            var allowDeleteOnly = true;
            var doNotAllowDeleteFirstRecord = false;
            if (cat.IsReadOnly == "true")
                readOnly = true;

            if ((cat.AllowAddDelete == "false") || readOnly)
                allowAddDelete = false;
            if (cat.DoNotAllowDeleteFirstRecord == "true")
                doNotAllowDeleteFirstRecord = true;

            if ((cat.AllowDeleteOnly == "false") || (!cat.AllowDeleteOnly) || readOnly)
                allowDeleteOnly = false;

            if (cat.TabularData == "true") {
                var att;
                att = getAuxTableTemplate(cat, fields);
                if (att) {
                    $(dbTab).append($(att));
                    $(tabContent).hide();
                }
                if (cat.MultiGridStub == 'true') {
                    var MultiGridTemplate = getChildGridTemplate(cat);
                    $(dbTab).append($(MultiGridTemplate));
                }
                if (cat.ShowGridOnly == "true") {
                    $('.aux-grid-switch', $(dbTab)).hide();
                    $(tabContent).hide();
                }

                var dataNav = document.createElement('div');
                dataNav.className = 'aux-data-navigator';
                var navContents = $('.data-navigator-template').html();
                dataNav.innerHTML = navContents;
                $(tabContent).append(dataNav);

                if (!att)
                    $('.record-button[action="list"]', dataNav).hide();

                $(dbTab).attr('auxdata', cat.SourceTable);
                if (doNotAllowDeleteFirstRecord) {
                    $(dbTab).attr('ddfr', 'true');
                }
                if (!allowAddDelete) {
                    $(dbTab).attr('noad', 'true');
                }
                if (allowDeleteOnly) {
                    $(dbTab).removeAttr('noad');
                }
            } else {
                $(dbTab).attr('parceldata', '1');
            }
            if (!allowAddDelete || cat.ShowGridOnly == "true")
                $('.aux-grid-new', $(dbTab)).hide();
            $('.aux-grid-up', dbTab).attr('style', 'display:none;');
            $('.aux-grid-switch', dbTab).unbind(touchClickEvent);
            $('.aux-grid-switch', dbTab).bind(touchClickEvent, function (e) {
                e.preventDefault();
                var catid = $(dbTab).attr('catid');
                var cat = getCategory(catid);
                if (cat && cat.PhotoLinkTableKey && cat.PhotoTag && clientSettings["PhotoTagMetaField"] && clientSettings["PhotoTableLinkMetaField"]) {
                    if ($('.data-entry-tab-content', dbTab).css('display') == "none" && $(dbTab).attr('records') > 0) {
                        $('.record-photo-preview').hide();
                        $('.record-add-photo').show();
                    }
                    else {
                        $('.record-photo-preview').hide();
                        $('.record-add-photo').hide();
                        $('.data-collection-table .record-add-photo', dbTab).show();
                    }
                    appState.photoCatId = appState.catId;
                    photos.loadImages(function () { $('span.photocount').html(appState.recordPhotoCount >= 0 ? appState.recordPhotoCount : 0) }, null, true, true);
                }
                else {
                    $('.record-photo-preview').show();
                    $('.record-add-photo').hide();
                }
                $('.data-entry-tab-content', dbTab).show();
                $('.aux-grid-view', dbTab).hide();
                $('.aux-multigrid-view', dbTab).hide();
                $('.aux-sub-multigrid-view', dbTab).hide();
                $('textarea', dbTab).each(function (i, x) {
                    if (x.style.display != 'none') {
                        var txtarea = $('textarea', dbTab);
                        if (txtarea[i].style.display != 'none') {
                            txtarea[i].style.cssText = 'height:auto';
                            txtarea[i].style.cssText = 'height:' + txtarea[i].scrollHeight + 'px';
                            iPad ? txtarea[i].style.maxHeight = '148px' : txtarea[i].style.maxHeight = '144px';
                        }
                    }
                });
                /*if (formScroller) {
                    formScroller.refresh();
                    formScroller.enable();
                }*/
            });

            if ((subcats.length > 0) || (cat.ParentCategoryId)) {
                var auxhead = {
                    ParentCategoryId: cat.ParentCategoryId,
                    ShowBack: cat.ParentCategoryId ? '' : 'display:none;',
                    showback: cat.ParentCategoryId ? '' : 'display:none;',
                    subcats: subcats
                }

                var auxheader = document.createElement('div');
                $(auxheader).html($('.aux-header-template').html());
                $(auxheader).fillTemplate([auxhead]);
                $(tabContent).append(auxheader);


                $('.aux-grid-up', dbTab).attr('style', auxhead.ShowBack);
                $('.aux-grid-up', dbTab).attr('target', auxhead.ParentCategoryId);

                var navBack = function (tcid, tIndex) {
                    var valid = checkRequiredFieldsInForm();
                    if (!valid) {
                        messageBox(msg_FormNoNavigate);
                        return false;
                    }
                    var cat = getCategory(tcid);
                    var target = $('section[catid="' + tcid + '"]');
                    if ($(target).attr('parceldata') == "1") {
                        refreshParcelSubLinks(target, tcid);
                    } else {
                        if (cat && cat.MultiGridStub != 'true')
                            refreshAuxSubLinks(tcid);
                    }
                    selectDcTab(tcid, null, tIndex, undefined, undefined, multigridTable);
                }
                $('.aux-nav-back', auxheader).unbind(touchClickEvent);
                $('.aux-nav-back', auxheader).bind(touchClickEvent, function (e) {
                    e.preventDefault();
                    var tIndex = $(this).parents('section').eq(0).attr('parentIndex')
                    hideKeyboard();
                    var tcid = $(this).attr('target');
                    navBack(tcid, tIndex);
                });

                $('.aux-grid-up', dbTab).unbind(touchClickEvent);
                $('.aux-grid-up', dbTab).bind(touchClickEvent, function (e) {
                    e.preventDefault();
                    moveIndicator = 0;
                    var tcid = $(this).attr('target');
                    var catid = ccma.UI.ActiveFormTabId;
                    var valid = checkRequiredFieldsInGrid(catid);
                    var tIndex = $(this).parents('section').eq(0).attr('parentIndex')
                    if (!valid) {
                        messageBox(msg_FormNoCreate);
                        return false;
                    }
                    navBack(tcid, tIndex);
                });

                $('.sub-cat-opener', auxheader).unbind(touchClickEvent);
                $('.sub-cat-opener', auxheader).bind(touchClickEvent, function (e) {
                    e.preventDefault();
                    hideKeyboard();
                    var valid = checkRequiredFieldsInForm();
                    if (!valid) {
                        messageBox(msg_FormNoNavigate);
                        return false;
                    }

                    var tcid = $(this).attr('catid');
                    var targetForm = $('section[name="dctab_' + tcid + '"]')[0];
                    var aFilters = $(this).attr('filters');
                    var aValues = $(this).attr('filtervalues');
                    var thisData = cat.SourceTable;
                    var fIndex = $(dbTab).attr('findex');
                    var thisIndex = $(dbTab).attr('index');
                    var thisparentpath = $(dbTab).attr('ParentPath');
                    var parent = {
                        source: thisData,
                        index: fIndex,
                        thisIndex: thisIndex,
                        parentpath: thisparentpath
                    }
                    if (subCatIndex != null) {
                        parent.source = $(this).attr('parentSource');
                        parent.index = $(this).attr('parentIndex');
                    }
                    var targetTable = $(this).attr('targetdata');
                    if (targetTable && targetTable != "") {
                        $('.aux-nav-back', targetForm).attr('target', cat.Id);
                        //var parenttable = parentChild.filter(function (s) { return s.ChildTable == targetTable })[0].ParentTable;
                        //var curent = parenttable == "Parcel" ? '' : '.' + parenttable + '[' + thisIndex + ']';
                        //$('section[name="dctab_' + tcid + '"]').attr('ParentPath', (thisparentpath ? thisparentpath : 'activeParcelData') + curent);
                        loadAuxiliaryRecordsWithFilter(targetForm, aFilters, aValues, parent, subCatIndex);
                    }
                    else {
                        $(targetForm).attr('sourceTable', thisData);
                        //  $(targetForm).attr('parentpath', thisparentpath + "." + thisData + "[" + thisIndex + "]");
                        loadMultiGrid(targetForm)//, tcid, thisparentpath, thisIndex);
                        if (!cachedAuxOptions[tcid]) cachedAuxOptions[tcid] = {};
                        cachedAuxOptions[tcid].filters = aFilters;
                        cachedAuxOptions[tcid].filterValues = aValues;
                        cachedAuxOptions[tcid].parentSpec = parent;
                        cachedAuxOptions[tcid].filteredIndexes = [];
                        $('.sub-cat-opener', dbTab).forEach(function (item) {
                            if ($(item).attr('targetdata') != '' && $(item).attr('targetdata')) {
                                var tcid = $(item).attr('catid');
                                var targetForm = $('section[name="dctab_' + tcid + '"]')[0];
                                var aFilters = $(item).attr('filters');
                                var aValues = $(item).attr('filtervalues');
                                loadAuxiliaryRecordsWithFilter(targetForm, aFilters, aValues, parent, subCatIndex);
                            }
                        });

                    }
                    selectDcTab(tcid);
                });
            }

            var headerInfo = document.createElement("div");
            headerInfo.className = "header-info";
            $(tabContent).append(headerInfo);

            var fieldSetHeader = document.createElement('div');
            fieldSetHeader.className = 'fieldSetHeader';


            var _getFieldSet = function (field, afterGet) {
                getFieldSet(field, {
                    category: cat, readOnly: readOnly, callback: function (fieldSet) {
                        $(fieldSetHeader).append(fieldSet);
                        afterGet && afterGet();
                    }
                });
            }

            processQueue(fields, _getFieldSet, function () {
                $(tabContent).append(fieldSetHeader);
                $(dbTab).append(tabContent);
                //var spacer = document.createElement("div")
                //$(spacer).css({ 'height': '460px' });
                //$(dbTab).append(spacer);
                var headerTemplate = document.createElement("div");
                headerTemplate.className = "header-template hidden";
                $(headerTemplate).html($('<div/>').html(cat.HeaderInfo || '').text());
                $(dbTab).append(headerTemplate);


                //$(dbTab).attr('name', "dctab_" + fields[0].CategoryId);
                $(dbTab).attr('name', "dctab_" + cat.Id);
                $(dbTab).hide();
                $('#dc-form-contents').append(dbTab);

                console.groupEnd(cat.Name);
                callback && callback();
            }, function () {
                console.log('Loading fields failed.')
            }, function () {

            })


            //for (var fi in fields) {
            //    var field = fields[fi];
            //    // if (field.DoNotShow == "false") {
            //    var fieldSet = getFieldSet(field, { category: cat, readOnly: readOnly });
            //    $(fieldSetHeader).append(fieldSet);
            //    //  }
            //}


            //catIndex += 1;
            //if (catIndex == totalCats) {
            //    bindInfoButtonClick();
            //    if (callback) callback();
            //}
        }, [fields, cat]);

    }, cat);
}

function fieldExpEval(tbl, ix, exp) {
    let expres = false;
    try {
        expres = activeParcel.EvalValue(tbl + '[' + ix + ']', exp);
    }
    catch (ex) { }
    return expres;
}

function getFieldSet(field, options, skipTrackedvalue, addSpecialHandler) {
    var defOptions = {
        category: null,
        readOnly: false,
        trackChange: true,
        changeHandler: saveInputValue,
        formType: 'data collection',
        callback: (fieldSet) => { }
    }
    
	if (addSpecialHandler) {
		defOptions.changeHandler = addToMassChanges;
        defOptions.isMassUpdateForm = true;
	}
	
    for (var ii in options) defOptions[ii] = options[ii];
    options = defOptions;

    if (ccma.UI.FieldProps[field.Id] == undefined) ccma.UI.FieldProps[field.Id] = {};

    var category, readOnly;
    if (options) {
        category = options.category;
        readOnly = options.readOnly;
    }
    ["ReadOnlyExpression", "VisibilityExpression", "CalculationExpression", "LookupQuery", "RequiredExpression", "CalculationOverrideExpression", "ConditionalValidationConfig"].forEach(function (x) {
        if (field[x]) mapRelatedFields(field, category, field[x], x);
    });

    if (!readOnly)
        readOnly = (field.ReadOnly == "true" || field.IsReadOnlyMA == "true");
    var fieldSet = document.createElement('fieldset');
    $(fieldSet).attr({
        "field-id": field.Id,
        "ro": readOnly,
        "field-name": field.Name,
        "field-type": field.InputType,
        "calc-exp": field.CalculationExpression,
        "assigned-name": field.AssignedName
    });
    if (checkIsFalse(field.IsMassUpdateField)) {
        $(fieldSet).attr("mass-upd", 'mass-upd')
    }

    var cat = fieldCategories.find((x)=> { return x.Id == field.CategoryId });
    (field.CalculationOverrideExpression) ? $(fieldSet).attr('override-calc-exp', decodeHTML(field.CalculationOverrideExpression)) : $(fieldSet).removeAttr('override-calc-exp');
    (field.ReadOnlyExpression) ? $(fieldSet).attr('readonlyexp', decodeHTML(field.ReadOnlyExpression)) : $(fieldSet).removeAttr('readonlyexp');
    if (cat) (cat.ReadOnlyExpression) ? $(fieldSet).attr('cat_readonlyexp', decodeHTML(cat.ReadOnlyExpression)) : $(fieldSet).removeAttr('cat_readonlyexp');
    (field.VisibilityExpression) ? $(fieldSet).attr('visibilityexp', decodeHTML(field.VisibilityExpression)) : $(fieldSet).removeAttr('visibilityexp');
    (field.RequiredExpression) ? $(fieldSet).attr('Requiredexp', decodeHTML(field.RequiredExpression)) : $(fieldSet).removeAttr('Requiredexp');
    if (field.Calculated == "true") {
        $(fieldSet).attr('calculated', 'true');
    }

    let reqd = null;

    if (field.IsRequired && !massUpdateForm) {
        let reqExp = false;
        if ((options.visionRecIndex || options.visionRecIndex === 0) && options.source && field.RequiredExpression) reqExp = fieldExpEval(field.SourceTable, options.visionRecIndex, decodeHTML(field.RequiredExpression));
        if (field.IsRequired == "true" || field.IsRequiredMAOnly == "true" || reqExp) {
            reqd = document.createElement('span');
            $(reqd).addClass('reqd');
            reqd.innerHTML = '*';
            if (!options.visionCapping) $(fieldSet).append(reqd);
        }
    }

    var legend = document.createElement('span');
    $(legend).addClass("legend");
    $(legend).html(field.Label);
    $(fieldSet).append(legend);

    if (options.visionCapping && reqd) $(fieldSet).append(reqd);

    if (options.trackChange) {
        if (!massUpdateForm) {
            var label = document.createElement('label');
            $(label).addClass("oldvalue");
            skipTrackedvalue? $(label).html(''): $(label).html('NO VALUE');
            $(fieldSet).append(label);
        }
    }

    if (field.InfoContent != null && field.InfoContent.trim() != '') {
        var info = document.createElement('span');
        $(info).attr('fieldId', (field.fieldId ? field.fieldId : field.Id));
        $(info).addClass('infoButton').css({ 'position': 'relative', 'display': 'inline-block' })
        $(fieldSet).append(info);
    }


    if (((field.ReadOnlyExpression != null) && (field.ReadOnlyExpression != "")) || (category && category.ReadOnlyExpression != null && category.ReadOnlyExpression != "") || (category && category.ClassCalculatorParameters && category.ClassCalculatorParameters.split(',')[0] == field.Name) || ((category) ? isEditOlderCategoryDenied(category.Id) : false) || (isBPPUser) || (field.DoNotAllowEditExistingValue == 'true')) {
        var labelroexp = document.createElement('span');
        $(labelroexp).attr('readonlyexp', '');
        $(fieldSet).append(labelroexp);
    }

    if (field.Id) {
        getInputElement(field, {
            category: options.category, changeHandler: options.changeHandler, source: options.source, isMassUpdateForm: options.isMassUpdateForm, ClassCalcField: options.ClassCalcField, visionCapping: options.visionCapping, callback: function (elem) {
                if (field.InputType == 5 && !skipTrackedvalue) {
                	//$(fieldSet).append('<div class="cusDpdown" onclick="openCustomlkd(this, true)" style="display:none"></div>');
                	//$(fieldSet).find('.cusDpdown').append(elem);
                	// $(fieldSet).append('<i class="cusDpdown-arrow" onclick="openCustomlkd(this, true)"></i>');
                	if(iPad) $('.cusDpdown-arrow',$(fieldSet)).addClass('cusIpad-arrow')
                	$(fieldSet).append(elem);
                }
                else
                	$(fieldSet).append(elem);

                if (isCustomddl && !readOnly) {
                    $('.searchCustomddl', $(fieldSet)).remove();
                    var cLink = document.createElement('a');
                    $(cLink).addClass("searchCustomddl");
                    $(cLink).attr({ 'onclick': 'return openCustomddlList(this);' });
                    $(fieldSet).append(cLink);

                }
                
                /*if (field.InputType == 5) {
                	$('.cusDpdown', $(elem).parent('fieldset')).remove();
                	$(elem).parent('fieldset').append('<div class="cusDpdown" onclick="openCustomlkd(this, true)" style="display:none"><span class="cusDpdownSpan"></span></div>');
                }*/
                
                isCustomddl = false;
                if (isGeoLocation) {
                    var cLink = document.createElement('a');
                    $(cLink).addClass("LoadGeolocationIcon");
                    $(cLink).attr({ 'onclick': 'return GeolocationLoad(this);' });
                    $(fieldSet).append(cLink);
                    isGeoLocation = false;
                }

                if ((options.visionRecIndex || options.visionRecIndex === 0) && options.source && field.ReadOnlyExpression) readOnly = fieldExpEval(field.SourceTable, options.visionRecIndex, decodeHTML(field.ReadOnlyExpression));

                if (readOnly) {
                    $(fieldSet).attr({ 'readonlyfieldset': '1' });
                    $('.cusDpdown-arrow',$(fieldSet)).hide();
                }

                let hidExp = false;
                if ((options.visionRecIndex || options.visionRecIndex === 0) && options.source && field.VisibilityExpression) hidExp = fieldExpEval(field.SourceTable, options.visionRecIndex, decodeHTML(field.VisibilityExpression));

                if (field.DoNotShow == "true" || hidExp || field.DoNotShowOnMA == "true" || (field.IsClassCalculatorAttribute == 'true' && options.formType != 'mass-update-div'))
                    $(fieldSet).attr({ 'hiddenfieldset': '1' });

                if (options.visionCapping) {
                    $(fieldSet).append('<input type="checkBox" class="flcheck" onchange="visionCappingCbxChange(this);" style="width: 20px; height: 20px; margin-right: 18px;">');
                    $('.newvalue', $(fieldSet)).attr('disabled', true);
                    if (options.visionDisableCheckbox) $('.flcheck', $(fieldSet)).attr('disabled', true);
                    if (field?.InputType == 5 && options.visionCapValue) {
                        $('.newvalue', $(fieldSet)).val(options.visionCapValue);
                    }
                }

                options.callback && options.callback(fieldSet);

            }
        }, null, skipTrackedvalue, addSpecialHandler)

    }

    if (readOnly && field.CalculationExpression && field.CalculationExpression != '' && field.CalculationOverrideExpression && field.CalculationOverrideExpression != '')
        $(fieldSet).append(getInputElement(field, { category: options.category, changeHandler: options.changeHandler }, true, skipTrackedvalue));
    return fieldSet;
}
/*
function getChildGridTemplate(cat) {
    var childTables = parentChild.filter(function (x) { return x.ParentTable == cat.SourceTable }).map(function (x) { return x.ChildTable });
    var childCategories = fieldCategories.filter(function (d) { return (d.ParentCategoryId == cat.Id && d.SourceTable != null && d.DoNotShowInMultiGrid != 'true'); });
    var template = "<div class=' aux-multigrid-view' style='position:relative;margin-left:20px;'>";
    template += "<div class='multigrid scrollable' style='overflow: hidden;'></div><div class='hiddenGridTemplate' style='display:none'><table>";
    childCategories.forEach(function (cat) {
        var insertDelete = "<span class='multigridActions' style='float: right;font-size: 12px;margin-top: 5px;color: #762323;' catID=" + cat.Id + "> <a class='multtiGridInsert'>Insert</a><a class='multtiGridDelete' style='margin-left:5px'>Delete</a></span>";
        if (cat.AllowAddDelete != 'true' || cat.ShowGridOnly == "true") { insertDelete = "" }
        template += "<tr cid=" + cat.Id + "><td><h3 style='background-color:silver;padding: 5PX;font-size: 19px; '>" + cat.Name + insertDelete + "</h3></td></tr><tr cid=" + cat.Id + "><td>";
        var fields = Object.keys(datafields).map(function (x) { return datafields[x] }).filter(function (x) { return x.CategoryId == cat.Id });
        fields = fields.sort(function (a, b) { return a.Serial - b.Serial; })
        template += getAuxTableTemplate(cat, fields, true);
        template += "</tr></td>";
    });
    template += '</table></div></div>';
    return template;
}
*/

function getSubChildGridTemplate(cat) {
    var childTables = parentChild.filter(function (x) { return x.ParentTable == cat.SourceTable }).map(function (x) { return x.ChildTable });
    var childCategories = fieldCategories.filter(function (d) { return (d.ParentCategoryId == cat.Id && d.SourceTable != null && d.DoNotShowInMultiGrid != 'true'); });
    var pcatId = getParentCategories(cat.Id);
    var template = "<div class='aux-sub-multigrid-view' catid='" + pcatId + "' pcatId='" + cat.Id + "' style='position:relative;margin-left:20px;'>";
    template += "<div class='sub-multigrid' style='overflow: hidden;'></div><div class='subhiddenGridTemplate' style='display:none'><table>";
    childCategories.forEach(function (cat) {
        var insertDelete = "<span class='multigridActions' style='float: right;font-size: 12px;margin-top: -5px;color: #762323;' catID=" + cat.Id + "> <a class='multtiGridInsert button-insert-delete' style='background-position-x: -160px; padding: 0px;'></a><a class='multtiGridDelete button-insert-delete' style='margin-left:5px; background-position-x: -200px; padding: 0px;'></a></span>";
         if(cat.AllowAddDelete != 'true' || cat.ShowGridOnly == "true")
         { insertDelete ="";}
        if ((cat.AllowAddDelete != 'true' && cat.ShowGridOnly != "true") && cat.AllowDeleteOnly =='true'){          
            insertDelete ="<span class='multigridActions' style='float: right;font-size: 12px;margin-top: -5px;color: #762323;' catID=" + cat.Id + "><a class='multtiGridDelete' style='margin-left:5px'>Delete</a></span>";}
         
        template += "<tr cid=" + cat.Id + "><td><h3 style='background-color:#e6dea1;padding: 5PX;font-size: 19px;height: 31px; '>" + cat.Name + insertDelete + "</h3></td></tr><tr cid=" + cat.Id + "><td>";
        var fields = Object.keys(datafields).map(function (x) { return datafields[x] }).filter(function (x) { return x.CategoryId == cat.Id });
        fields = fields.sort(function (a, b) { return a.Serial - b.Serial; })
       // template += getAuxTableTemplate(cat, fields, true);
        var checkgrid = getAuxTableTemplate(cat, fields, true);
        if (checkgrid)
            template += checkgrid

        template += "</tr></td>";
    });
    template += '</table></div></div>';
    return template;
}


function getChildGridTemplate(cat) {
    var childTables = parentChild.filter(function (x) { return x.ParentTable == cat.SourceTable }).map(function (x) { return x.ChildTable });
    var childCategories = fieldCategories.filter(function (d) { return (d.ParentCategoryId == cat.Id && d.SourceTable != null && d.DoNotShowInMultiGrid != 'true'); });
    var template = "<div class=' aux-multigrid-view' catid='" + cat.Id + "' style='position:relative;margin-left:20px;'>";
    template += "<div class='multigrid' style='overflow: hidden;'></div><div class='hiddenGridTemplate' style='display:none'><table style='width: 100%;'>";
    childCategories.forEach(function (cat) {
        var insertDelete = "<span class='multigridActions' style='float: right;font-size: 12px;margin-top: -5px;color: #762323;' catID=" + cat.Id + "> <a class='multtiGridInsert button-insert-delete' style='background-position-x: -160px; padding: 0px;'></a><a class='multtiGridDelete button-insert-delete' style='margin-left:5px; background-position-x: -200px; padding: 0px;'></a></span>";
        if(cat.AllowAddDelete != 'true' || cat.ShowGridOnly == "true")
         { insertDelete ="";}
        if ((cat.AllowAddDelete != 'true' && cat.ShowGridOnly != "true" ) && cat.AllowDeleteOnly =='true'){          
            insertDelete ="<span class='multigridActions' style='float: right;font-size: 12px;margin-top: -5px;color: #762323;' catID=" + cat.Id + "><a class='multtiGridDelete' style='margin-left:5px'>Delete</a></span>";}
        template += "<tr cid=" + cat.Id + "><td><h3 style='/* width: 56%; */background-color:silver;padding: 5PX;font-size: 19px;height:31px; '>" + cat.Name + insertDelete + "</h3></td></tr><tr cid=" + cat.Id + "><td>";
        var fields = Object.keys(datafields).map(function (x) { return datafields[x] }).filter(function (x) { return x.CategoryId == cat.Id });
        fields = fields.sort(function (a, b) { return a.Serial - b.Serial; })
       // template += getAuxTableTemplate(cat, fields, true);
        var checkgrid = getAuxTableTemplate(cat, fields, true);
        if (checkgrid)
            template += checkgrid

        template += "</tr></td>"; 
    });
    template += '</table></div></div>';
    childCategories.forEach(function (cat) {
        template += getSubChildGridTemplate(getCategory(cat.Id));
    });
    return template;
}


function getAuxTableTemplate(category, fields, isContext) {
    var source = category.SourceTable;
    var header = "", body = "", footer = "";
    if (category.ShowGridOnly != "true") {
        header += "<th>&nbsp;</th>"
        footer += "<td class='aux-grid-edit-cell'><span class='gridEditNotify' style='margin-left:-16px;position:absolute;font-size: medium;color: red;font-weight: bold;'>!</span><a class='aux-grid-edit' href='#'>View/Edit</a></td>"
    }

    var gridFields = fields.filter(function (f) { return f.ShowOnGrid == "true" });

    gridFields.forEach(function (field) {
        var exprn = field.Name;
        header += "<th>" + field.Label + "</th>";
        //        if (field.IsCalculated == 'true' || (field.CalculationExpression != null && field.CalculationExpression != '')) {
        //            //exprn = field.CalculationExpression;
        //        }
        var inputType = parseInt(field.InputType);
        var dt = ccma.Data.Types[inputType];
        if (field.GridLinkScript) {
            footer += '<td><a href="about:blank" onClick="' + field.GridLinkScript + '">${' + exprn + ':format,' + dt.formatter.replace(',', '~') + '}</a></td>';
        }
        else
            footer += "<td>${" + exprn + ":table," + field.SourceTable + "}</td>";

    });
    if (category && category.PhotoLinkTableKey && category.PhotoTag && clientSettings["PhotoTagMetaField"] && clientSettings["PhotoTableLinkMetaField"]) {
        header += "<th width='50px'>&nbsp;</th>"
        footer += "<td>${photoRecordCount:table," + source + "}</td>"
    }
    var context = isContext == true ? "context='" + source + "'" : "";
    header = "<thead>" + header + "</thead>";
    body = "<tbody " + context + "></tbody>";
    footer = "<tfoot style='display:none;'>" + footer + "</tfoot>";

    if (gridFields.length == 0 && source != "")
        return false;
    if (isContext == true)
        var tableHtml = "<table class='data-collection-table' source='" + source + "' border='1' style='/* width:100%; */' catid='" + category.Id + "'>" + header + body + footer + "</table>";
    else
        var tableHtml = "<div class='aux-grid' style='position:relative;' catid = '" + category.Id + "'><div class=' aux-grid-view'><div class='aux-grid-head'><a class='aux-grid-up a-button ' style='float: right;'>UP</a><a class='aux-grid-switch a-button'>FORM</a><a class='aux-grid-new record-new a-button' style='float: right;' grid='true'></a><div class='cb'></div></div><div class='aux-grid-header'></div><table class='data-collection-table' source='" + source + "' style = 'margin-left:20px;'border='1'>" + header + body + footer + "</table></div></div>";
    return tableHtml;
}

var inputElementInFocus;
var focussedInputElement;
var isCustomddl = false;
var isGeoLocation = false;
var MarkerArray = [];
var CurrentGeoElement;
var CurrentGeoValue;
function GeolocationLoad(source) {
    var element = $(source).siblings('.newvalue')[0];
    $(element).focusout();
    if (!UsingOSM) {
        for (var i = 0; i < MarkerArray.length; i++) {
            MarkerArray[i].setMap(null);
        }
    }
    
    MarkerArray = [];
    CurrentGeoElement = element;
    $('.mask').show();
    $('.GeoLocationMap').show();
    setGeoLocationScreen($(window).height(), $(window).width());
    resizeMap();
    let value = $(element).val() ? $(element).val().split(",") : "", Location = null, GeoMarker = null;
    
    if (value.length > 1) {
        Location = UsingOSM ? new L.latLng(value[0], value[1]) : new google.maps.LatLng(value[0], value[1]);
        GeoMarker = UsingOSM ? L.marker(Location, { icon: GeoLocationIcon }) : new google.maps.Marker({ position: Location, icon: GeoLocationIcon, map: GeoLocationmap });
    }
    else {
    	Location = UsingOSM? OSMmyLocation: myLocation;    	
    	if (geo && geo.location && geo.location.longitude && geo.location.latitude)
    		Location = UsingOSM ? new L.latLng(geo.location.latitude, geo.location.longitude) : new google.maps.LatLng(geo.location.latitude, geo.location.longitude);
		GeoMarker = UsingOSM ? L.marker(Location, { icon: GeoLocationIcon }) : new google.maps.Marker({ position: Location, icon: GeoLocationIcon, map: GeoLocationmap });
    }
    
    if (UsingOSM) {
    	osmGeomap.removeLayer(markerlayerGeo);
        markerlayerGeo = new L.FeatureGroup();
        markerlayerGeo.addLayer(GeoMarker);
        osmGeomap.addLayer(markerlayerGeo);
        osmGeomap.setView(Location, 19);
        osmGeomap.setZoom(13);
        L.DomEvent.addListener(osmGeomap, 'click', function (event) {
            osmGeomap.removeLayer(markerlayerGeo);
            markerlayerGeo = new L.FeatureGroup();
            var GeoMarker = L.marker(event.latlng, { icon: GeoLocationIcon });
            markerlayerGeo.addLayer(GeoMarker);
            osmGeomap.addLayer(markerlayerGeo);
            CurrentGeoValue = event.latlng;
        });
    }
    else {
    	MarkerArray.push(GeoMarker); 
    	GeoMarker.setPosition(Location); 
    	GeoMarker.setMap(GeoLocationmap); 
    	GeoLocationmap.setCenter(Location);
        GeoLocationmap.setZoom(13);
        google.maps.event.addListener(GeoLocationmap, 'click', function (event) {
            for (var i = 0; i < MarkerArray.length; i++) {
                MarkerArray[i].setMap(null);
            }
            MarkerArray = [];
            var marker = new google.maps.Marker({
                position: event.latLng,
                icon: GeoLocationIcon,
                map: GeoLocationmap
            });
            CurrentGeoValue = event.latLng;
            MarkerArray.push(marker);
        });
    }
    CurrentGeoValue = Location;
    return false;
}

function CloseGeoLocation() {
    $('.mask').hide();
    $('.GeoLocationMap').hide();
}

function SetGeoLocationInField() {
    if (CurrentGeoValue)
        if (UsingOSM) { $(CurrentGeoElement).val(CurrentGeoValue.lat.toFixed(8) + "," + CurrentGeoValue.lng.toFixed(8)); } else { $(CurrentGeoElement).val(CurrentGeoValue.lat().toFixed(8) + "," + CurrentGeoValue.lng().toFixed(8)); }
    else
        alert("Select a point to save");
    CurrentGeoValue = null;
    CloseGeoLocation();
    $(CurrentGeoElement).change();
    $(CurrentGeoElement).show();
    $(CurrentGeoElement).parent().find('.ClickGeoLocation').remove();
}

function getInputElement(field, options, defaultTextBox, skipTrackedvalue, addSpecialHandler) {

    var defOptions = {
        changeHandler: saveInputValue,
        category: null,
        callback: null
    }
    for (var ii in options) defOptions[ii] = options[ii];
    options = defOptions;
    var elem;
    var isRequired = false;
    var inputType = parseInt(field.InputType);
    var d = ccma.Data.Types[inputType];

    var finalizeElement = function (skipevt) {
        $(elem).attr('field-id', field.Id);
        $(elem).attr('field-name', field.Name);
        $(elem).addClass('newvalue');
    
        if (defaultTextBox) $(elem).addClass('calcOverideExp');
        /*$(elem).unbind('focus');
        $(elem).bind('focus', function () {
            inputElementInFocus = this;
            var position = $(inputElementInFocus).position();
            var element = document.getElementById('dc-form-contents');
            var offset = position.top - 270;
            if (Math.abs(window.orientation) == 0 || Math.abs(window.orientation) == 180) 
                offset = position.top - 520;
        	
            if (offset > 0) {
                enableFocus = true;
                element.scrollTop = element.scrollTop + offset;
            }
            window.setTimeout(function(){ enableFocus = false; }, 100)
        });
        $('#dc-form-contents').on('scroll', function () {
            if (inputElementInFocus && !enableFocus) $(inputElementInFocus).blur();
        });*/
        if ((!skipTrackedvalue || !skipevt) && !options.visionCapping) {
            if (d && d.changeEvent) {
                $(elem).unbind(d.changeEvent);
                $(elem).bind(d.changeEvent, function () {
                    var el = this;
                    doNotAllowSpecialCharacters(el);
                    if (!addSpecialHandler)
                    	relationcycle = [];
                    options.changeHandler(el);
                });
            }
        }

        if (field.ReadOnly == 'true' && !defaultTextBox)
            $(elem).attr('readonly', 'readonly');

        options.callback && options.callback(elem);
    }

    if (d && !defaultTextBox) {
    	if (field.InputType == 5 && !skipTrackedvalue) {
    		elem = document.createElement('input');
    		// if(iPad) $(elem).addClass('cusIpad'); 
    		$(elem).addClass('cusDpdownSpan');
            // $(elem).attr('onclick', 'openCustomlkd(this, true)');
            let allowTextInput = field.AllowTextInput == 'true' ? true : false;
            $(elem).lookup( { popupView: true, searchActive: true, searchActiveAbove:8, searchAutoFocus: false, customText: allowTextInput, 
                onSave: function () {
                    if (!skipTrackedvalue && !options.visionCapping) {
                        if (!addSpecialHandler)
                            relationcycle = [];
                        options.changeHandler(elem);
                    }
                }}, [] );
    	}
    	else
        	elem = document.createElement(d.htmltag);
        ["type", "maxlength", "rows", "multiple", "pattern"].forEach(function (x) { if (d[x]) $(elem).attr(x, d[x]); })
        if (field.MaxLength) {
            var ml = parseInt(field.MaxLength);
            if (ml > 0) {
                $(elem).attr('maxlength', ml);
            }
        }
        if (field.NumericScale) {
            var p1 = parseInt(field.NumericScale);
            if (p1 > 0)
                $(elem).attr('scale', p1);
        }
        if (field.NumericPrecision) {
            var p1 = parseInt(field.NumericPrecision);
            if (p1 > 0)
                $(elem).attr('precision', p1);
        }
        if (field.IsRequired && !massUpdateForm) {
            if (field.IsRequired == "true" || field.IsRequiredMAOnly == "true") {
                isRequired = true;
                $(elem).attr('required', 'true');
            }
        }
        if (d.cctype == 12) {
            $(elem).addClass('GeoLocation');
            $(elem).attr('readonly', 'readonly');
            isGeoLocation = true;
        }
        if (d.cctype == 13) {
            $(elem).addClass("RadioButtonGroup");
            $(elem).css({ 'float': 'right' });
        }
        if (d.cctype == 2 || d.cctype == 8 || d.cctype == 10) {
            $(elem).attr("onkeypress", "return numbervalidation(this,event)");
        }

        if (d.cctype == 1 && field.DoNotAllowSpecialCharacters == 'true') {
            $(elem).attr("onkeypress", "return doNotAllowSpecialCharacters(this,event)");
        }

        if (d.cctype == 4) {
            $(elem).attr("onfocusin", "focusdate(this)");
            $(elem).attr("onfocusout", "changedate(this)");
        }



        if (d.dataSource) {
            var ld = null;
            getLookupData(field, { category: options.category, dataSource: d.dataSource, addNoValue: false, source: options.source }, function (ld) {
                isCustomddl = false;
                var emp_opt = false, customlkd = false, islkField = false; 
                /*if (field.InputType != 5 && options && options.isMassUpdateForm && ld.length > 50 && field.AssignedName.search('PhotoMetaField') == -1) {
                   var emp_lk = ld.filter(function(x) { return x.Id == '' || x.Value =='<blank>' })[0]? ld.filter(function(x) { return x.Id == '' || x.Value =='<blank>' })[0]: null;
                   ld = []; customlkd = true;
                   if (emp_lk ) ld.push(emp_lk);
                   if ((emp_lk && ld.length == 1) || ld.length == 0 ) emp_opt = true;
                }*/
                if (ld.length > 0)
                    (ld[0].SortType) ? ld = ccma.Data.Evaluable.sortArray(ld, getSortProperty(ld[0].SortType), true) : '';
                if (field.AssignedName.search('PhotoMetaField') == -1) {
                    ld.forEach(function (x, index) {
                        if (x.Id) ld[index].Id = encodeURI(x.Id);
                    });
                }
                
                if (field.InputType == 5 && !skipTrackedvalue) {
                	islkField = true;
                	if (options.ClassCalcField) {
                		var ccval = options.ClassCalcField.CalcValue;
						var cclkval = ld.filter(function(x) { return x.Id == ccval })[0]? ld.filter(function(x) { return x.Id == ccval })[0]: null;                		
                		var ccdisval = cclkval && cclkval.Name? cclkval.Name: ccval;
                		// $(elem).html(ccdisval);
                	}
                    $(elem)[0].getLookup.setData(ld);
                } 
                else {
                	if (field.InputType == 5 && skipTrackedvalue) islkField = true;
	                $(elem).html('<option value="${Id}">${Name}</option>');
	                $(elem).fillTemplate(ld);
                }
                
                if (field.DoNotAllowSelectNull == true || field.DoNotAllowSelectNull == 'true')
                    $(elem).find('option[value=""]').attr('disabled', true)
                if (field.InputType == 3 && field.RadioFieldValues && field.RadioFieldValues != "")
                    $(elem).find('option:last-child').attr('disabled', true)
                
                var optionsCount = clientSettings.LookUpOptionsCount || 25
                /*$(elem).unbind('touchstart mousedown');
                
                if (field.InputType == 5) {
                	$('.cusDpdown', $(elem).parent('fieldset')).remove();
                	$(elem).parent('fieldset').append('<span class="cusDpdown" style="display:none"></span>');
                }*/
                
                if (field.InputType != 5 && ((ld && ld.length > optionsCount) || customlkd)) {
                    isCustomddl = true;
                    $(elem).attr('customddl', '');
                    /*if (customlkd) $(elem).attr('customlkd', '1');
                    if (customlkd && emp_opt) {
                       $(elem).bind('touchstart mousedown', function () {
                            if (event) {
                                event.preventDefault();
                                var _csel = event.target? event.target: this;
                                openCustomlkd(_csel);
                            }
                        });
                    }*/
                }
                
                if (isCustomddl) {
                    $('.searchCustomddl', $(elem).parent('fieldset')).remove();
                    var cLink = document.createElement('a');
                    $(cLink).addClass("searchCustomddl");
                    $(cLink).attr({ 'onclick': 'return openCustomddlList(this);' });
                    $(elem).parent('fieldset').append(cLink);
                    //isCustomddl = false;
                }

                finalizeElement(islkField);

            }, true);
        } else {
            finalizeElement();
        }
    } else {
        elem = document.createElement('input');
        if (defaultTextBox) { $(elem).attr('type', 'text'); $(elem).hide(); }

        finalizeElement();
    }

    return elem;
}


function saveInputValue(el, callback, autoselectCall, skipTrackedvalue, skipInvalidAlert) {


    //denying null value saving
    if ($(el).val() == '<blank>')
        return false;
    var field = datafields[$(el).attr('field-id')];
    var inputType = parseInt(field.InputType);
    var fieldName = field.Name;
    var catid = field.CategoryId;
    var cat = getCategory(catid)
    var sourceTable = field.SourceTable;
    var d = ccma.Data.Types[inputType];
    // var el = this;
    var aggregate = agFields.filter(function (a) { return (a.FieldName == fieldName && a.TableName == sourceTable); });
    if (((field.isHeatmap == true || field.isHeatmap == 'true') || aggregate.length > 0) && HeatMapIcons[activeParcel.Id.toString()])
        HeatMapIcons[activeParcel.Id.toString()].changed = true;
    var pattern = new RegExp(d.pattern);
    var newValue = inputType == 13 ? $('input:checked', el).val() : $(el).val(),
        customFormat = false, fmt = '';

    if (field.UIProperties && field.UIProperties != null) fmt = checkForCustomFormat(field.Id);
    if (fmt != '') {
        customFormat = true;
        newValue = deFormatvalue(newValue, field.Id);
    }

    if (d.baseType == 'number') {
        text = newValue
        var item = $(el)
        if (text != '' && !isNaN(text) && ((($(item).attr('min') != '' && $(item).attr('min') > parseFloat(text)) || ($(item).attr('max') != '' && $(item).attr('max') < parseFloat(text))) || (text.indexOf('.') > 1 && $(item).attr('allowdecimal') == 'false'))) {
            if (!skipInvalidAlert) {
                $(item).val(lastFieldValue);
                if (clientSettings?.LoadMethodAfterAppLoad == 'loadValidationTablesForVS8') messageBox('Please follow on-screen validation.');
                else messageBox('Invalid input.');
            }
            else if (skipInvalidAlert && callback) callback();
            return;
        }
    }

    if (pattern != '')  {
        if (newValue != "") {
            if (!pattern.test(newValue)) {
                var msg = '';
                if (d.baseType == 'number') {
                    if (d.typename == 'Year')
                        msg = 'Invalid input. Please enter a valid 4-digit year for the field.';
                    else if (d.typename == 'Money' || d.typename == 'Real Number')
                        msg = 'Invalid input. Commas or other characters are not allowed. You can type only numerals and a single period.';
                    else if (d.typename == 'Whole Number' || d.typename == 'Cost Value')
                        msg = 'Invalid input. Commas or other characters are not allowed. You can type only numerals.';
                    else
                        msg = 'Invalid input. Please enter a valid Number for the field.';
                    if (msg != '')
                        setTimeout(function () { document.activeElement.blur(); }, 200);
                    messageBox(msg, function () {
                        $(el).val('');
                        $(el).focus();
                        //allowInputs();
                    });
                }
                if (el.parentNode.parentNode.className == 'data-entry-massupdate-content' && msg == '') {
                    addToMassChanges(el);
                    return false;
                }
                return;
            }
        }
    }

    var maxLength = ccma.Data.Types[field.InputType].maxlength;
    if (navigator.platform == "Win32" && field.InputType == 4 && $(el).val().length > maxLength) {
        messageBox("Invalid Date", function () {
            $(el).val('');
            $(el).focus();
        });
        return false;
    }
    var pa = $(el).parent();
    var form = $(pa).parents('section[catid]');
    var isAux = $(form).attr('auxdata') != null;
    var source = $(form).attr('auxdata');
    var index = parseInt($(form).attr('findex'));
    var childIndex = parseInt($(form).attr('index'));
    var pid = activeParcel.Id;
    var oldValue = $('.oldvalue', pa).attr('oldvalue');
    var auxRowId = $('.oldvalue', pa).attr('rowuid');
    
    //if (inputType == 2 && newValue != "")  //This code was commented for 5460 ticket
    //  if(newValue=='.'||newValue=='-'||newValue=='-.'){
    //  newValue='';
    //  $(el).val('');}
    // else{newValue = parseFloat(newValue).toFixed(2).toString();}
    
    if ($(el).attr('type') == 'date' && !customFormat) {
        if (newValue != '') {

            var targetTime = new Date(newValue);
            //            var timeZoneFromDB = clientSettings["TimeZone"].replace(":", "."); //time zone value from database
            //            var tzDifference = timeZoneFromDB * 60;
            //            var offsetTime = new Date(targetTime.getTime() - tzDifference * 60 * 1000);
            var timevalue = targetTime.toISOString().substring(0, 10);
            newValue = timevalue + ' 12:00:00';
        }

    }
    if (inputType == 5){
        let tValue = newValue === null ? '' : newValue;
        try {
            newValue = decodeURI(tValue);
        } catch {
            newValue = tValue;
        }
    }
    var currentValue = source ? activeParcel[source][index][field.Name] : activeParcel[field.Name];
    if ((currentValue == newValue || (currentValue == null && newValue == '')) && !field.CalculationExpression) {
        if (callback) callback();
        return;
    }
    if (el.multiple)
        newValue = valueOfMultipleSelection(el);
    if (field.DoNotAllowSelectNull == 'true' && (!newValue || newValue == '')) {
        $(el).val(currentValue);
        return;
    }
    if ($('option[nonexist]', $(el)).length > 0) {
        if ($('option[nonexist]', $(el)).attr('value') == $(el).val()) {
            if ($(el).val() == '<blank>' || $(el).val() === '')
                return false;
            else
                messageBox(msg_nonExisting_ddlVal);
            setCurrentValue(el);
            return false;
        }
        else
            $('option[nonexist]', $(el)).remove();
    } else if($(el).hasClass('cc-drop')) {
        let lp = $(el)[0]&&$(el)[0].getLookup;
        let value = $(el).val();
        if(!lp) return false;
        for (var k of Object.keys(lp.rows)) {
            if (lp.rows[k] && lp.rows[k].nonexist && lp.rows[k].Name == value)  {
                if ($(el).val() == '<blank>' || $(el).val() === '') return false;
                else  messageBox(msg_nonExisting_ddlVal);
                setCurrentValue(el);
                return false;
            } else if(lp.rows[k].nonexist) delete lp.rows[k];
        }
    }
    if (checkIsNull(catid) && (field.IsUniqueInSiblings == "true") && newValue != '') {
        if (!IsUniqueInSiblings(catid, index, fieldName, newValue)) {
            setFieldValue(el.parentNode, sourceTable + '[' + index + ']');
            messageBox(msg_unique_in_field, function () {
                $(el).focus();
                // $(el).val('');
                // saveInputValue($(el)[0]);
            });
            return false;
        }
    }
    var numPrecision = field.NumericPrecision;
    var numScale = field.NumericScale;
    var maxLength = field.MaxLength;
    var textLength = newValue ? newValue.length : 0;
    var NumericText = newValue ? newValue.replace('.', '').length : 0;
    numPrecision = numPrecision ? parseInt(numPrecision) : null;
    numScale = numScale ? parseInt(numScale) : null;
    if (newValue && d.baseType == 'number' && isNaN(newValue) == false && newValue.indexOf('.') > -1) {
        if (numPrecision && NumericText > parseInt(numPrecision)) {
            newValue = newValue.substring(0, numPrecision + 1);
        }
        if (numScale) {
            if (numScale == 0)
                newValue = newValue.toString().substring(0, newValue.toString().indexOf('.'))
            else
                newValue = newValue.toString().substring(0, newValue.toString().indexOf('.') + parseInt(numScale) + 1)
        }
    }
    if ((field.UpdateSiblings == true || field.UpdateSiblings == 'true' || (field.InputType == 3 && field.RadioFieldValues && field.RadioFieldValues != '')) && source) {
        var currentvlaue = newValue;
        var siblingsource = [];
        if (activeParcel[source][index] && activeParcel[source][index]["parentRecord"])
            siblingsource = activeParcel[source][index]["parentRecord"][source];
        else
            siblingsource = activeParcel[source]
        if (field.RadioFieldValues && field.RadioFieldValues != "") {
            currentvlaue = field.RadioFieldValues.trim().split(',')[1];
        }
        siblingsource.forEach(function (a) {
            if (auxRowId != a.ROWUID && a[field.Name] != currentvlaue) {
                a[field.Name] = currentvlaue;
                ccma.Sync.enqueueParcelChange(pid, a.ROWUID, null, 'E', field.Name, field.Id, a.Original[field.Name] ? a.Original[field.Name] : null, currentvlaue, {
                    updateData: true,
                    source: source
                })
            }
        })

    }
    if (inputType == 13) {
        $('input', $(el)).removeAttr('checked');
        $('input[value = "' + newValue + '"]', $(el)).prop("checked", true);
    }

    //special case for proval Config to update scale value field of sktHeader if RorC field of extension change
    var _config = (clientSettings && clientSettings.SketchConfig) || (sketchSettings && sketchSettings.SketchConfig)
    if (_config && (_config == 'ProValNew' || _config == 'ProValNewFYL') && source == 'ccv_extensions' && field.Name == 'RorC' && newValue && newValue != '') {
        var headerTableName = _config == 'ProValNew' ? 'SktHeader' : 'ccv_SktHeader';
        var scaleField = getDataField && getDataField('scale', headerTableName);
        var extensionRec = activeParcel[source][index];
        var scalVal = (newValue == 'R') ? '80' : '200';
        if (extensionRec && scaleField && extensionRec[headerTableName][0])
            update_data(scaleField, scalVal, extensionRec[headerTableName][0]);
    }
    //end

    if (source) {
        activeParcel[source][index][field.Name] = newValue;
        if (field.ReadOnly == "false") {
            if (activeParcel.Changes) {
                // if (activeParcel.Changes[source] === undefined) {
                //   activeParcel.Changes[source] = {};
                //    activeParcel.Changes[source][field.Name] = { Action: "E", AuxRowId: auxRowId, ChangedTime: Date.now(), Field: fieldName, FieldId : field.Id ,OldValue : currentvlaue ,NewValue : newValue , ParcelId: pid, SourceTable: source,Index : index};
                // }
                // else
                //   activeParcel.Changes[source][field.Name] = { Action: "E", AuxRowId: auxRowId, ChangedTime: Date.now(), Field: fieldName, FieldId : field.Id ,OldValue : currentvlaue ,NewValue : newValue , ParcelId: pid, SourceTable: source,Index : index };
                //}

                var pci = activeParcel.Changes ? activeParcel.Changes.filter(function (y) { return y.AuxRowId == auxRowId && y.FieldId == field.Id && (datafields[y.FieldId] && datafields[y.FieldId].DoNotShow != "true") }) : [];

                var existingChange = pci.filter(function (y) { return !(y.OldValue == y.NewValue || (y.OldValue == null && y.NewValue == '')) && (datafields[y.FieldId] && datafields[y.FieldId].VisibilityExpression ? !(activeParcel.EvalValue(sourceTable + '[' + index + ']', datafields[y.FieldId].VisibilityExpression)) : true) });

                // activeParcel.Changes.push({Action: "E", AuxRowId: auxRowId, ChangedTime: Date.now(), Field: fieldName, FieldId : field.Id ,OldValue : currentValue ,NewValue : newValue , ParcelId: pid, SourceTable: source,Index : index})
                if (existingChange && existingChange.length > 0) {
                    pci.forEach(function (pciElement) {
                        activeParcel.Changes.splice(_.indexOf(activeParcel.Changes, pciElement), 1);
                    });
                }
                activeParcel.Changes.push({ Action: "E", AuxRowId: auxRowId, ChangedTime: Date.now(), Field: fieldName, FieldId: field.Id, OldValue: oldValue, NewValue: newValue, ParcelId: pid, SourceTable: source, Index: index })
            }

        }
    } else {
        activeParcel[field.Name] = newValue;
        if (activeParcel.Changes) {
            //  if (activeParcel.Changes[field.Name] === undefined) {
            //   activeParcel.Changes[field.Name] = {};
            //    activeParcel.Changes[field.Name] = { Action: "E", ChangedTime: Date.now(), Field: fieldName, FieldId : field.Id ,OldValue : currentvlaue ,NewValue : newValue , ParcelId: pid };
            //}
            // else
            //  activeParcel.Changes[field.Name] = { Action: "E", ChangedTime: Date.now(), Field: fieldName, FieldId : field.Id , ParcelId: pid };
            //	activeParcel.Changes.push({Action: "E", ChangedTime: Date.now(), Field: fieldName, FieldId : field.Id ,OldValue : currentValue ,NewValue : newValue , ParcelId: pid})
            activeParcel.Changes.push({ Action: "E", ChangedTime: Date.now(), Field: fieldName, FieldId: field.Id, OldValue: oldValue, NewValue: newValue, ParcelId: pid })
        }
    }
    if (clientSettings.Trackedvalue == field.Name) {
        Trackchangedvalues(field, el, skipTrackedvalue);
    }
    if (_.contains(ccma.UI.EstimateChartFields, field.Id)) {
        ccma.UI.Events.refreshDataCollectionEstimate = true;
    }

    if (inputType == 5 && $(el).parent().is('fieldset')) {
        let flset = $(el).parent();
        if ($(el)[0] && $(el).attr('customWidth')) {
            $(el).removeAttr('customWidth');
            $(el).css('width', '');
        }
        recalculateCustomDropdownTextWidth(flset);
    }

    refreshRelatedFields(field, saveInputValue, autoselectCall);
    var autoInsert = cat.AutoInsertProperties && JSON.parse('[' + cat.AutoInsertProperties.replace(/}{/g, '},{') + ']');
    if (autoInsert) {
        autoInsert.forEach(function (ai) {
            var copyfields = ai.fieldsToCopy.split(',');
            if (auxRowId < 0 && copyfields.indexOf(field.Name) > -1) {
                var copyField = getDataField(field.Name, ai.table);
                var copyTable = ai.table;
                activeParcel[source][index][ai.table].forEach(function (a) {
                    a[copyField.Name] = newValue;
                    var rindex = a ? activeParcel[copyTable].findIndex(function (x) { return x.ROWUID == a.ROWUID }) : '';
                    activeParcel[copyTable][rindex][copyField.Name] = newValue;
                    ccma.Sync.enqueueParcelChange(pid, a.ROWUID, a.ParentROWUID, 'E', copyField.Name, copyField.Id, a.Original[copyField.Name] ? a.Original[copyField.Name] : null, newValue, {
                        updateData: true,
                        source: copyTable
                    });
                });
            }
        });
    }
    if (ccma.UI.ActiveScreenId == "classcalculator-div") {
        return false;
    }
    var ParentSpec = cachedAuxOptions[catid] == undefined ? null : cachedAuxOptions[catid].parentSpec;

    HideCategoryByExpression(catid, form, source, index, ParentSpec ? ParentSpec.source + '[' + ParentSpec.thisIndex + ']' : null);
    HideInsertDeleteByExpression(catid, form, source, index, ParentSpec ? ParentSpec.source + '[' + ParentSpec.index + ']' : null);
    sqlinsert = "updated";
    fieldUpdateStatus[catid] = true;

    if (getReadOnlyExpressionOverrideVal(field)) setTimeout(() => { try { refreshCatRecord(index, source, catid); } catch (ex) { } }, 100);

    if ((oldValue == newValue) && !((oldValue === 0 && newValue == '') || (oldValue == '' && newValue === 0)) && ($(el).attr('readonly') != 'readonly')) {
        $('.oldvalue', $(el).parent()).hide();
    } else {
        $('.oldvalue', $(el).parent()).show();
    }
    ccma.Sync.enqueueParcelChange(pid, auxRowId, null, 'E', field.Name, field.Id, oldValue, newValue, {
        updateData: true,
        source: source
    }, function () {
        getData("SELECT * FROM Parcel WHERE  Id = " + pid + "", [], function (p) {
            if (p[0].Reviewed) refreshSortScreen = true;
            executeQueries("UPDATE Parcel SET Reviewed = 'false' WHERE Id = " + pid)
        });
        if ((datafields[field.Id].IsCustomField == "false" || datafields[field.Id].IsCustomField == false) && (datafields[field.Id].DoNotIncludeInAuditTrail == "false" || datafields[field.Id].DoNotIncludeInAuditTrail == false)) {
            let qrs = EnableNewPriorities == 'True' ? ("UPDATE Parcel SET CC_Priority = 3 WHERE CC_Priority = 0 AND Id = " + pid) : ("UPDATE Parcel SET CC_Priority = 1 WHERE CC_Priority = 0 AND Id = " + pid);
            executeQueries(qrs);

        }
        if (ccma.UI.SortScreenFields.indexOf(field.Id) > -1) {
            ccma.UI.Events.refreshParcelListAfterDataEntry = true;
        }

        // invokeInputControl(el, inputElementInFocus);
        if (callback) callback();

    }, function () {
        //  invokeInputControl(el, inputElementInFocus);
        if (callback) callback();
    });
    hideCalculator(cat, source + '[' + index + ']', true, form)
}

function Trackchangedvalues(field, el, skipTrackedvalue) {

    trackfieldname = field.Name;
    var trackactiveitem = activeParcel[clientSettings.Trackedvalue];
    if (clientSettings.Trackedvalue == trackfieldname && !(trackactiveitem == null) && !(trackactiveitem == undefined)) {
        if(field.InputType == 5 && !skipTrackedvalue)
           trackvalue =  $(el).html()? $(el).html(): '';
        else
            trackvalue = $('option[value="' + $(el).val() + '"]', el).html();
        $('.fieldtrack-mode').html(trackvalue);
    }
    else {
        trackvalue = '';
        $('.fieldtrack-mode').html(trackvalue);
    }
}

function invokeInputControl(from, to) {
    if (to)
        if ($(to)[0].tagName.toLowerCase() == "select") {
            return;
        }
    if (from != to) {
        $(to).focus();
    }
}

function selectDcTab(id, key, recordIndex, k, piiIndex, multiTable) {
    pIdIndex = piiIndex;
    multigridTable = multiTable;
    var valid = true;
    if (k != 1)
        var valid = checkRequiredFieldsInForm();
    var auxform = $('section[catid="' + id + '"]');
    var pd = $('section[catid="' + id + '"]').attr('parceldata');
    if (!valid) {
        messageBox(msg_FormNoNavigate);
        $('#dc-categories').css({ 'left': '-' + $('#dc-categories').width() + 'px' });
        return false;
    }
    var TopPID = getTopParentCategory(id)
    if (id || id == 0) {
        appState.catId = id;
        saveAppState();
    }
    var autosize = function (txtarea) {
        txtarea.each(function (i) {
            if (!txtarea[i].offsetParent && txtarea[i].offsetWidth === 0 && txtarea[i].offsetHeight === 0) {
                return false;
            }
            txtarea[i].style.cssText = 'height:auto';
            txtarea[i].style.cssText = 'height:' + txtarea[i].scrollHeight + 'px';
            iPad ? txtarea[i].style.maxHeight = '148px' : txtarea[i].style.maxHeight = '144px';
        })
    }
    if (key == undefined) {
        if (id == 0) {
            key = $('a[special]', $('#dc-categories'));
        } else {
            key = $('a[id="dctabkey_' + id + '"]', $('#dc-categories'));
        }
    }
    $('#dc-categories a').removeClass('selected');
    $(key).addClass('selected');
    $('#dc-form section').hide();
    var txtarea;
    if (id == '0') {
        $('section[name="quickreview"]').show();
        txtarea = $('section[name="quickreview"] textarea');
        autosize(txtarea);
        refreshDataCollectionEstimateChart();
    }
    else {
        $('section[name="dctab_' + id + '"]').show();
        txtarea = $('section[name="dctab_' + id + '"] textarea');
        autosize(txtarea);
    }

    recalculateDimensions(true);
    /*if (formScroller == null)
        formScroller = new IScroll('#dc-form', iScrollOptions);   //Pass the id instead of jQuery Selector. 04/17/2014
    else {
        formScroller.refresh(true);
        formScroller.scrollTo(0, 0, 0);
    }*/
    $('#dc-categories').css({ 'left': '-' + $('#dc-categories').width() + 'px' });
    $('#dc-categories-condensed span').text($(key).text())

    ccma.UI.ActiveFormTabId = id;
    var cat = getCategory(id);
    if (cat && cat.PhotoLinkTableKey && cat.PhotoTag && clientSettings["PhotoTagMetaField"] && clientSettings["PhotoTableLinkMetaField"]) {
        appState.photoCatId = appState.catId;
        photos.loadImages(function () {
            $('.record-add-photo span.photocount').html(appState.recordPhotoCount >= 0 ? appState.recordPhotoCount : 0);
            if ($('.data-entry-tab-content', auxform).css('display') == "block" && $(auxform).attr('records') > 0) {
                $('.record-photo-preview').hide();
                $('.record-add-photo').show();
            }
            else {
                $('.record-photo-preview').hide();
                $('.record-add-photo').hide();
                $('.data-collection-table .record-add-photo', auxform).show();
            }
        }, null, true, true);
    }
    else {
        appState.photoCatId = 0;
        photos.loadImages(function () {
            $('.record-photo-preview span.photocount').html(appState.totalPhotoCount >= 0 ? appState.totalPhotoCount : 0);
            $('.record-photo-preview').show();
            $('.record-add-photo').hide();
        }, null, true, true);
    }
    if ((sqlinsert != "" || sqlDelete != "") || (reloadFlag && appState.photoCatId != 0)) {
        //        if (cat && cat.MultiGridStub == 'true')
        //            loadMultiGrid($('section[name="dctab_' + id + '"]')[0], id, cachedAuxOptions[id].parentSpec.parentpath, cachedAuxOptions[id].parentSpec.thisIndex)
        //        else
        if (id != '0' && pd == null)
            reloadAuxiliaryRecords(id, undefined, undefined, recordIndex);
        if (id == TopPID)
            sqlinsert = ""; sqlDelete = ""; reloadFlag = false;
    }
    /*if (childScroller)
        childScroller.disable();*/
    var recalculateCusDDwidth = false;
    if (cat && cat.MultiGridStub == 'true' && cat.ParentCategoryId == null) {
        recalculateCusDDwidth =  true;
        if (recordIndex >= 0 && recordIndex != null) reloadAuxiliaryRecords(id, undefined, undefined, recordIndex);
        else reloadAuxiliaryRecords(id);
    }
    else if (id != '0') {
        if (recordIndex >= 0 && recordIndex != null) { recalculateCusDDwidth =  true; reloadAuxiliaryRecords(id, undefined, undefined, recordIndex); }
        else highlightChangesInGrid(id);
    }
    checkRequiredIfEditsFieldsInForm(auxform);
   if (!recalculateCusDDwidth) {
    	 $('fieldset[field-type="5"][ro="false"]', auxform).each((i, fieldset) => { 
             if ($(fieldset)[0]) {
                 recalculateCustomDropdownWidth(fieldset);
                 if ($('.newvalue', $(fieldset))[0] && $('.newvalue', $(fieldset)).attr('customWidth')) {
                     $('.newvalue', $(fieldset)).removeAttr('customWidth');
                     $('.newvalue', $(fieldset)).css('width', '');
                 }
                 recalculateCustomDropdownTextWidth(fieldset);
             }
    	 });
    }
    //if (formScroller) formScroller.enable();
    //if (id != '0' && fieldCategories[parseInt(id) - 1].MultiGridStub == 'true' && $('section[name="dctab_' + id + '"] .aux-multigrid-view').css("display") != "none") {
    //    var multigridScroll = new IScroll('.aux-grid[catid="' + id + '"]', iScrollOptions);
    //    //formScroller.disable();
    //}
    document.getElementById('dc-form-contents').scrollTo(0, 0)
    return false;
}

function expandCategoryMenu() {

    $('#dc-categories').css({ 'left': '0px' });
    setTimeout('hideKeyboard();', 300);
}

function loadPhotoFrame(frameId, type, callback) {
    var scr = $('#' + frameId);
    var viewer = $('.viewer', scr);
    var slide = $('.slide', scr);
    //$(viewer).html('');
    selectedThumb = null;
    currentViewer = null;
    loadScreen(frameId, function () {
        IDB_getData('Images', ['ParcelId'], ['='], [parseInt(activeParcel.Id)], [], function (im) {
            //IDB_getData('Images', ['ParcelId', 'Type'], ['=', '='], [activeParcel.Id, parseInt(type)], ['AND'], function (images) {

            var cpics = pstream.getPhotos(activeParcel.Id)
            for (i in cpics) {
                var pu = cpics[i];

                var img = new Object();
                img.Id = null;
                img.Image = pu.dataUrl;
                img.ParcelId = null;
                img.Path = null;
                img.Type = 0;
                img.Accepted = pu.accepted ? 1 : 0;
                img.IsPrimary = false;
                img.MetaData1 = null;
                img.MetaData2 = null;
                img.MetaData3 = null;
                img.MetaData4 = null;
                img.MetaData5 = null;
                img.MetaData6 = null;
                img.MetaData7 = null;
                img.MetaData8 = null;
                img.MetaData9 = null;
                img.MetaData10 = null;
                images.push(img);
            }

            var cicon = new Object();
            cicon.Id = null;
            cicon.Image = '/static/images/camera.png';
            cicon.ParcelId = null;
            cicon.Path = null;
            cicon.Type = "-999";

            images.push(cicon);

            $(slide).html('<img src="${Image}" type="${Type}" acc="${Accepted}"/>');
            $(slide).fillTemplate(images);
            $('img', $(slide)).unbind(touchClickEvent);
            $('img', $(slide)).bind(touchClickEvent, function (e) {
                e.preventDefault();
                var action = $(this).attr('type');
                if (action == "-999") {
                    var altFieldvalue = ShowAlternateField == "True" ? eval("activeParcel." + AlternateKey) : null;
                    pstream.openCamera("camera", activeParcel.Id, activeParcel.KeyValue1, function () {
                        loadPhotoFrame(frameId, type, function () {
                            var imgs = $('img', $(slide)).length;
                            if (imgs > 0) {
                                var ni = imgs - 2;
                                var img = $('img', $(slide))[ni];
                                img.onload = function () {
                                    positionImageInViewer(this, viewer);
                                }
                            }
                        });
                    }, altFieldvalue);
                } else {
                    positionImageInViewer(this, viewer);
                }
            });
            if ($('img', $(slide)).length > 1) {
                positionImageInViewer($('img', $(slide))[0], viewer);
            }

            if (callback) callback();
        });
    });
}

var selectedThumb, currentViewer;

function positionImageInViewer(that, viewer) {
    selectedThumb = that;
    currentViewer = viewer;

    clearCanvas(viewer);

    if (selectedThumb == null)
        return;

    canvas = $('canvas', viewer)[0];
    drwg = canvas.getContext('2d');
    currentSketch = new Image();
    currentSketch.src = $(that).attr('src');

    var canvasHeight = canvas.height;
    var canvasWidth = canvas.width;

    picH = currentSketch.height;
    picW = currentSketch.width;

    var zoom = 1;
    cH = picH * zoom; cW = picW * zoom;
    if (canvasHeight < picH) {
        zoom = canvasHeight / picH;
        cH = picH * zoom; cW = picW * zoom;
    }
    if (cW < picW) {
        zoom = cW / picW;
        cH = picH * zoom; cW = picW * zoom;
    }

    var angle = 0;
    picAngle = angle * 0.0174532925199432957;

    drwg.translate(canvas.width / 2, canvas.height / 2);
    drwg.rotate(picAngle);
    drwg.drawImage(currentSketch, -cW / 2, -cH / 2, cW, cH);

    if ($(that).attr('acc')) {
        $('.acceptance').show();
    } else {
        $('.acceptance').hide();
    }
}

var currentSketch;
var picH, picW, cH, cW;
var drwg;
var canvas;
var picAngle = 0;
function loadSketch(index) {
    if ((index == null) || (index == undefined))
        index = 0;
    canvas = document.getElementById('sketchvas');
    drwg = canvas.getContext('2d');

    $('.sketch-resize').val(1.0);
    $('.sketch-rotate').val(0);

    index = parseInt(index);

    if (activeParcel.Sketches.length > 0) {
        currentSketch = new Image();
        currentSketch.src = activeParcel.Sketches[index].Image;
        paintSketch();
        $('.sketch-tool').removeAttr('disabled');
    } else {
        console.warn('Sketch not loaded.');
        cleanCanvas();
        currentSketch = null;
        $('.sketch-tool').attr('disabled', 'disabled');
    }
}

function clearCanvas(viewer) {
    if (viewer != null) {
        $('canvas', viewer)[0].width = $('canvas', viewer).width();
        $('canvas', viewer)[0].height = $('canvas', viewer).height();
    }
}

function cleanCanvas() {
    var sketchvas = document.getElementById('sketchvas');
    sketchvas.height = $('#sketchvas').height();
    sketchvas.width = $('#sketchvas').width();
}

function paintSketch() {
    cleanCanvas();
    if (currentSketch != null) {
        var zoom = $('.sketch-resize').val();
        picH = currentSketch.height;
        picW = currentSketch.width;
        cH = picH * zoom; cW = picW * zoom;

        var angle = $('.sketch-rotate').val();
        picAngle = angle * 0.0174532925199432957;

        drwg.translate(canvas.width / 2, canvas.height / 2);
        drwg.rotate(picAngle);
        drwg.drawImage(currentSketch, -cW / 2, -cH / 2, cW, cH);

        $('.sketch-zoom-value').html(parseInt(parseFloat(zoom).toFixed(2) * 100) + '%');
        $('.sketch-rotate-value').html(angle + '&deg;');
    }
    else {
        console.warn('Current sketch is null.');
    }
}

function setGeoLocationScreen(h, w) {
    var left = (w - (w * 70 / 100)) / 2
    var height = (h - (h * 85 / 100)) / 2
    $('.GeoLocationMap').width(w * 70 / 100);
    $('.GeoLocationMap').height(h * 75 / 100);
    $('.GeoLocationMap').css("top", height);
    $('.GeoLocationMap').css("left", left);
}
function setCustomddl(h, w) {
    var width = $('.customddl').width();
    $('.customddl').width(w);
    $('.customddl').height(h);
    $('.customddl_main_block').height(h);
    $('.customddl_main_block').width(width);
    $('.customddl_header').width(width - 10);
    $('.customddl_desc').height(h - 157);
    $('.customddl_desc').width($('div[controls]').width());
    $('div[headerleft]').width(($('.customddl_header').width() - $('div[controls]').width() - 40) / 2);
    $('div[headerright]').width(($('.customddl_header').width() - $('div[controls]').width() - 40) / 2)
    $('div[descleft]').width(($('.customddl_header').width() - $('div[controls]').width() - 40) / 2);
    // $('div[descright]').width(($('.customddl_header').width() - $('div[controls]').width() - 40) / 2);
    $('div[descleft]').height(h - 117);
    $('#lookupout').height(h - 117);
    $('.customddl_desc').width(width + 2);
    $('.lkShowAll').width(width + 2);
    $('#lookupout').width($('div[controls]').width());
    $('div[dummyscroll]').height(h - 117);
    $('div[dummyscroll]').width($('div[controls]').width());
    // $('div[descright]').height(h - 117);
    //refreshCustomddlScroll(); 
}

function setMask(h, w) {
    $('.mask').height(h);
    $('.mask').width(w);
}

function getSortProperty(index) {
    var ret = null;
    switch (index) {
        case 0: ret = 'Value'; break;
        case 1: ret = 'Name'; break;
        case 2: ret = 'Ordinal'; break;
    }
    return ret;
}

function setCurrentValue(el) {
    var sectionControl = $(el).parents('section[catid]');
    var sourceTable = (sectionControl) ? $(sectionControl).attr('auxdata') : null;
    var findex = (sectionControl) ? $(sectionControl).attr('findex') : null;
    (sectionControl) ? setFieldValue(el.parentNode, sourceTable + '[' + findex + ']') : setFieldValue(el.parentNode);
}

function mapRelatedFields(field, category, expression, expType) {
    var allFields = Object.keys(datafields).map(function (x) { return datafields[x] }).filter(function (x) { return x.CategoryId != null });
    var tfields = [], relfields = [];
    if (expType == 'LookupQuery') {
        tfields = expression.match(/\{.*?\}/g);
        for (len in tfields) {
            while (tfields[len].indexOf('parent.') > -1) {
                tfields[len] = tfields[len].replace('parent.', '');
            }
        }
        relfields = allFields.filter(function (f) { return (category.Id == -2) ? (f.CategoryId == -2) : (category.TabularData == "true") ? ((parseInt(f.CategoryId) == parseInt(category.Id)) || (parseInt(f.CategoryId) == parseInt(category.ParentCategoryId)) || (parseInt(f.CategoryId) == parseInt(category.OverrideParentCategory)) || f.SourceTable == category.SourceTableName) : f.ImportTypeId == 0 }).filter(function (x) { return _.contains(tfields, '{' + x.Name + '}') });
    }
    else if (expType == 'CalculationExpression' && expression.search(childRecCalcPattern) > -1) {
        var expr = expression.split(childRecCalcPattern)[0].split(/\./g);
        relfields = allFields.filter(function (f) { return f.Name == expr[1] && f.SourceTable == expr[0]; })
    }
    else {
        tfields = expression.match(/([a-zA-Z_][a-zA-Z0-9_]+)/g)
        relfields = allFields.filter(function (f) { return (category.Id == -2) ? (f.CategoryId == -2) : (category.TabularData == "true") ? parseInt(f.CategoryId) == parseInt(category.Id) : f.ImportTypeId == 0 }).filter(function (x) { return _.contains(tfields, x.Name) });
    }
    relfields.forEach(function (x) {
        if (ccma.UI.RelatedFields[x.Id] == undefined) ccma.UI.RelatedFields[x.Id] = [];
        if (!_.contains(ccma.UI.RelatedFields[x.Id], field.Id) && (x.Id != field.Id)) ccma.UI.RelatedFields[x.Id].push(field.Id);
    });
}

function mapDisplayFields(expression, collection) {
    var tfields = expression.match(/([a-zA-Z_][a-zA-Z0-9_]+)/g)
    var allFields = Object.keys(datafields).map(function (x) { return datafields[x] }).filter(function (x) { return x.CategoryId != null });
    var relfields = allFields.filter(function (f) { return f.ImportTypeId == 0 }).filter(function (x) { return _.contains(tfields, x.Name) });
    relfields.forEach(function (x) {
        if (collection == undefined) collection = [];
        if (!_.contains(collection, x.Id)) collection.push(x.Id);
    });
}

function refreshCatRecord(ix, s, cId) {
    let rAuxForm = $('section[catid="' + cId + '"]');
    recordSwitching.switches = [];
    $('fieldset', rAuxForm).each(function (eIndex) { recordSwitching.switches[eIndex] = false; });
    $('fieldset', rAuxForm).each(function (eIndex) {
        sd = s ? s + '[' + ix + ']' : null;
        setFieldValue(this, sd, null, function (eIndex) {
            recordSwitching.switches[eIndex] = true;
            recordSwitching.swichingFn();
        }, eIndex, null, null, true);
    });
}

function doNotAllowSpecialCharacters(el, ev) {
    let notAllowedCharsRegex = /[#%&{}\\<>*?\/!$'"@:+=`|]|[\u00A0-\uFFFF]|[\uD800-\uDBFF][\uDC00-\uDFFF]/g;

    function isAltCode(charCode) {
        return (charCode > 126);
    }


    function isEmoji(charCode) {
        return (charCode >= 0x1F600 && charCode <= 0x1F64F) || (charCode >= 0x1F300 && charCode <= 0x1F5FF) || (charCode >= 0x1F680 && charCode <= 0x1F6FF) || (charCode >= 0x2600 && charCode <= 0x26FF);
    }

    if (!ev) {
        let fId = $(el).attr('field-id'), f = datafields[fId];
        if (f && f.InputType == "1" && f.DoNotAllowSpecialCharacters == "true") {
            let ansval = $(el).val();
            ansval = ansval ? ansval.replace(notAllowedCharsRegex, '') : '';
            if (/^\s*$/.test(ansval)) ansval = '';
            $(el).val(ansval);
        }
    } else {
        let charCode = ev.charCode || ev.keyCode, charStr = String.fromCharCode(charCode);

        if (notAllowedCharsRegex.test(charStr) || isAltCode(charCode) || isEmoji(charCode)) {
            ev.preventDefault();
            return false;
        }

        if (charCode === 32 || (charCode >= 48 && charCode <= 57) || (charCode >= 65 && charCode <= 90) || (charCode >= 97 && charCode <= 122)) {
            return true;
        }
        return true;
    }
}