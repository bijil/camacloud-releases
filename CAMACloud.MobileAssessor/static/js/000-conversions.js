﻿//    DATA CLOUD SOLUTIONS, LLC ("COMPANY") CONFIDENTIAL 
//    Unpublished Copyright (c) 2010-2013 
//    DATA CLOUD SOLUTIONS, an Ohio Limited Liability Company, All Rights Reserved.

//    NOTICE: All information contained herein is, and remains the property of COMPANY.
//    The intellectual and technical concepts contained herein are proprietary to COMPANY
//    and may be covered by U.S. and Foreign Patents, patents in process, and are protected
//    by trade secret or copyright law. Dissemination of this information or reproduction
//    of this material is strictly forbidden unless prior written permission is obtained
//    from COMPANY. Access to the source code contained herein is hereby forbidden to
//    anyone except current COMPANY employees, managers or contractors who have executed
//    Confidentiality and Non-disclosure agreements explicitly covering such access.</p>

//    The copyright notice above does not evidence any actual or intended publication
//    or disclosure of this source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
//    DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
//    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
//    IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
//    OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
//    TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
//    ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.


 //ccma.Formatters = function () { };

Number.prototype.format = Date.prototype.format = String.prototype.format = Boolean.prototype.format = function (f) {
    var typeName = this.__proto__.constructor.name;
    switch (f) {
        case "money":
        case "$":
            return this.toMoney();
        case "imoney":
        case "$$":
            return this.toMoney(true);
        case "$$$":
            return this.toMoney(true, true);
        case ",":
            return this.toCommaNumber(true);
        case ",.":
            return this.toCommaNumber();
        case "%":
            return toPercent(this);
        case "int":
        case "i":
            return safeParseInt(this).toFixed(0).toString();
        case "f2":
            return safeParseFloat(this).toFixed(2).toString();
        case "f4":
            return this.toFixed(4).toString();
        case "dl":
            return this.toLiteral();
        case "d":
            return toDateString(this.toString());
        case "dmy":
            return toDateString(this);
        case "ymd":
            return toSQLDateString(this);
        case "m":
            return this.getMonth();
        case "y":
        case "yy":
            return this.getYear();
        case "yyyy":
            return this.getFullYear();
        case "s":
            return safeParseString(this);
        case "f":
            return safeParseFloat(this);
        default:
            return this.toString();
    }
}


Number.prototype.toMoney = String.prototype.toMoney = function (round, value) {
    nStr = 0
    if (this === null || this === undefined || (this == "" && this.toString() != '0'))
        return "";
    if (round == true)
        nStr = safeParseInt(this).toString();
    else if (safeParseInt(this) > 100)
        nStr = safeParseInt(this).toString();
    else
        nStr = parseFloat(this).toFixed(2).toString();
    x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    var money = x1 + x2;

    if (money == NaN) {
        return "***";
    }

    if ((money == NaN) || (money == Infinity)) {
        return "~";
    }
    if (money != "") {
        if (value)
            return money;
        else
            return "$" + money;
    } else
        return "";
}

Number.prototype.toCommaNumber = String.prototype.toCommaNumber = function (round) {
    nStr = 0
    if(this === null || this === undefined || (this == "" && this.toString() != '0') || !isFinite(this))
        return "";
    if (round == true || this.toString().indexOf('.') == -1)
        nStr = parseInt(this).toString();
    else
        nStr = parseFloat(this).toString();
    x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

Boolean.prototype.toCommaNumber = function () {
    if (this == null || this == "")
        return false;
    return this;
}

Date.prototype.toLiteral = function () {
    var now = new Date();
    var dticks = now.tickSeconds() - this.tickSeconds();
    var dsec = dticks / 1000;
    var dmin = Math.round(dsec / 60);
    var dhrs = Math.round(dmin / 60);
    var ddays = Math.round(dhrs / 24);
    if (dmin < 2) {
        return "just now";
    } else if ((dmin >= 2) && (dhrs < 1)) {
        return "within past hour";
    } else if ((dhrs >= 1) && (dhrs < 3)) {
        return "within past 3 hours";
    } else if ((dhrs >= 3) && (dhrs < 8)) {
        return "within past 8 hours";
    } else if ((dhrs >= 8) && (dhrs < 24)) {
        return "within past 24 hours";
    } else if ((ddays >= 1) && (ddays < 7)) {
        return "a day ago";
    } else if ((ddays >= 7) && (ddays < 30)) {
        return "a week ago";
    } else if ((ddays >= 30) && (ddays < 365)) {
        return "a month ago";
    } else if ((ddays > 365)) {
        return "an year ago";
    }
}

function toDateLiteral(x) {
    if (x == null) {
        return "never";
    } else {
        return x.toLiteral();
    }
}

function safeParseString(x) {
    if(x === null || x === undefined || (x == "" && x.toString() != '0'))
        return "";
        //    else if (isFinite(x)) {
        //        if (parseFloat(x) == parseInt(x))
        //            return parseInt(x).toString();
        //        else
        //            return parseFloat(x).toString();
        //    }
    else
        return x.toString();
}

function safeParseInt(x) {
    if ((x == null) || (x === undefined))
        return 0;
    else {
        if (typeof x == "string") {
            x = x.replace(/,/g, '');
        }
        return parseInt(x);
    }
}
function safeParseFloat(x, allowblank, blankToZero, dvRules) { 
 	if(blankToZero && (x == "" || x == null))
		return 0;
    if (allowblank && (x != '0') && (x == "" || x == null))
        return "";
    if (x == null)
        return 0;
    if(x === '0' && dvRules)
    	return '0';
    if (typeof x == "string")
        x = x.replace(/,/g, '');
    if (x.toString() == "NaN")
        return 0;
    if (isNaN(x))
        return 0;
    if (!isFinite(x))
        return 0;
    return parseFloat(x);
}
function toCustomDateFormat(str) {
    var noDate = "";

    if (!str) return noDate;
    var typeName = str.constructor.name;

    if ((str == null) || (str == "")) {
        return noDate;
    }
    var d;
    if (str instanceof Date) {
        d = str;
    } else {
        var noConvert = clientSettings['NoTimeConversion'] || '0';
        str = customFormatValue(str);
        str = noConvert == 1 ? str : dateTolocal(str);
        d = parseDate(str);
        if (isNaN(d)) return str;
    }
    if (d.getMonth() == NaN) { return ""; }
    var ds = (d.getMonth() + 1).toString().padLeft(2, '0') + '/' + d.getDate().toString().padLeft(2, '0') + '/' + d.getFullYear();
    if (ds.startsWith('NaN')) {
        var dt = str.split(" ")[0].split("-");
        var dd = dt[2];
        var mm = dt[1];
        var yyyy = dt[0];
        return mm + "/" + dd + "/" + yyyy;
    }

    return ds;
}


function toDateString(str) {
    var noDate = "";

    if (!str) return noDate;
    var typeName = str.constructor.name;

    if ((str == null) || (str == "")) {
        return noDate;
    }
    
    if (str.length >= 8){
        if(str.length == 8)
            str = str.substring(0,4) + '-' + str.substring(4,6) + '-' + str.substring(6,8);       
        else
            str = str.substring(0,4) + '-' + str.substring(5,7) + '-' + str.substring(8,10) + ' ' + str.substring(11,str.length);
    }
    
    var d;
    if (str instanceof Date) {
        d = str;
    } else {
        var noConvert = clientSettings['NoTimeConversion'] || '0';
        str = noConvert == 1 ? str : dateTolocal(str);
        d = parseDate(str.substr(0, 10));
        //var tz = (new Date()).getTimeZone();
        //        var dts = str.substr(0, 10) + " 00:00" + tz;
        //        var dti = Date.parse(dts);
        //        d = new Date(dti);
        //        return dts + ' ' + dti;
        if (isNaN(d)) return str;
    }

    if (d.getMonth() == NaN) { return ""; }
    var ds = (d.getMonth() + 1).toString().padLeft(2, '0') + '-' + d.getDate().toString().padLeft(2, '0') + '-' + d.getFullYear();
    if (ds.startsWith('NaN')) {
        var dt = str.split(" ")[0].split("-");
        var dd = dt[2];
        var mm = dt[1];
        var yyyy = dt[0];
        return mm + "/" + dd + "/" + yyyy;
    }

    return ds;
}

function toSQLDateString(str) {
    var noDate = "";

    if (!str) return noDate;
    var typeName = str.constructor.name;

    if ((str == null) || (str == "")) {
        return noDate;
    }
    var d;
    if (str instanceof Date) {
        d = str;
    } else {
        d = parseDate(str.substr(0, 10));
        if (isNaN(d)) return str;
    }

    if (d.getMonth() == NaN) { return ""; }
    var ds = d.getFullYear() + '-' + (d.getMonth() + 1).toString().padLeft(2, '0') + '-' + d.getDate().toString().padLeft(2, '0');
    if (ds.startsWith('NaN')) {
        var dt = str.split(" ")[0].split("-");
        var dd = dt[2];
        var mm = dt[1];
        var yyyy = dt[0];
        return yyyy + "-" + mm + "-" + dd;
    }

    return ds;
}

function toPercent(f) {
    if (f == null) { return ""; }
    if (isNaN(f)) { return ""; }
    if (!isFinite(f)) { return ""; }
    return ((f * 100).toFixed(2)) + "%"
}

function toMoney(x) {
    if (x == null)
        return "";
    var ret = x.toMoney();
    return ret;
}


function toCommaFormat(x) {
    if (x == null)
        return "";
    return x.toCommaNumber(true);
}

function toIntMoney(x) {
    if (x == null)
        return "";
    return x.toMoney(true);
}

function toCostValue(x) {
    if (x == null)
        return "";
    return x.toMoney(true, true);
}

function toKms(f) {
    if (f == null) {
        return "--";
    }
    if (f > 1000) {
        return (f / 1000.0).toFixed(2) + ' km'
    }
    return Math.round(f) + ' mtrs from here.';
}

function toImperialDistance(f) {
    if (f == null) {
        return "--";
    }
    if (f > 1760) {
        return (f / 1760.0).toFixed(2) + ' miles'
    }
    return Math.round(f) + ' yds from here.';
}

function isDecimal(o) {
    if (typeof o == "number") {
        if (o.toString().indexOf(".") > -1) {
            return true;
        }
    }
    return false;
}

ccma.NullSafe = function (x, jsType) {

}

Date.prototype.getTimeZone = function () {
    var ts = (new Date()).toTimeString();
    var m = ts.match(/GMT(.*?)\s/);
    if (m.length > 1) {
        return m[1];
    } else {
        return "+00:00"
    }

}
String.prototype.endsWith = function (suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
}
String.prototype.startsWith = function (prefix) {
    return this.indexOf(prefix) == 0;
}
String.prototype.contains = function (infix) {
    return this.indexOf(infix) != -1;
}

function parseDate(input, format) {
try {
	var parts = "";
    format = format || 'yyyy-mm-dd'; // default format
   	if(input!="")
    parts = input.match(/(\d+)/g);
  	var i = 0, fmt = {};
    // extract date-part indexes from the format
    format.replace(/(yyyy|dd|mm)/g, function (part) { fmt[part] = i++; });

    return new Date(parts[fmt['yyyy']], parts[fmt['mm']] - 1, parts[fmt['dd']]);
    }
    
    catch (e) {
        console.log(e.message);
    }
}

function replaceLikeOperatorsOld(st) { // Delete, if below one is correct
    st = st.replace(/\s*(%=%)\s*/g, '%=%');
    st = st.replace(/\s*(%=)\s*/g, '%=');
    st = st.replace(/\s*(=%)\s*/g, '=%');
    var iter = 0, pArray = [], retVal = '';
    while (iter < st.length) {
        pArray.push(st[iter]);
        ++iter;
    }
    //  for (x in pArray) {
    iter = 0;
    while (iter < pArray.length) {
        if (pArray[iter] == '%') {
            if (pArray[parseInt(iter) + 1] == '=') {
                if (pArray[parseInt(iter) + 2] == '%') {//contains
                    retVal += '.contains("';
                    for (var k = parseInt(iter) + 4; pArray[k] != '"'; k++) {
                        retVal += pArray[k];
                    }
                    retVal += '")';
                    iter = k + 1;
                }
                else {//start with
                    retVal += '.startsWith("';
                    for (var k = parseInt(iter) + 3; pArray[k] != '"'; k++) {
                        retVal += pArray[k];
                    }
                    retVal += '")';
                    iter = k + 1;
                }
            }
            else if (pArray[parseInt(iter) - 1] == '=') {//ends with
                retVal = retVal.substring(0, retVal.length - 1);
                retVal += '.endsWith("';
                for (var k = parseInt(iter) + 2; pArray[k] != '"'; k++) {
                    retVal += pArray[k];
                }
                retVal += '")';
                iter = k + 1;
            }
        }
        else {
            retVal += pArray[iter];
            ++iter;
        }
    }
    //}
    return retVal;
}

function replaceLikeOperators(st, jsType) {
    var iter = 0, pArray = [], retVal = '', likeVal = '';
    var appendLikeVal = function (i) {
        likeVal = (pArray[parseInt(iter) + i])
        while (likeVal == " ") {
            iter++;
            likeVal = (pArray[parseInt(iter) + i])
        }
        if (likeVal == "'") {
            likeVal = "";
            while (pArray[parseInt(iter) + i + 1] != "'") {
                likeVal += pArray[parseInt(iter) + i + 1];
                ++iter;
            }
        }
        retVal += likeVal
        retVal += "')";
        iter = parseInt(iter) + i + 2;
    }
    while (iter < st.length) {
        pArray.push(st[iter]);
        ++iter;
    }
    iter = 0;
    while (iter < pArray.length) {
        if (pArray[iter] == '%') {
            if (pArray[parseInt(iter) + 1] == '=') {
                if (jsType == 'Number')
                    retVal += '.toString()';
                if (pArray[parseInt(iter) + 2] == '%') {//contains
                    retVal += ".contains('";
                    appendLikeVal(3)
                }
                else {//start with
                    retVal += ".startsWith('";
                    appendLikeVal(2)
                }
            }
            else if (pArray[parseInt(iter) - 1] == '=') {//ends with
                retVal = retVal.substring(0, retVal.length - 1);
                if (jsType == 'Number')
                    retVal += '.toString()';
                retVal += ".endsWith('";
                appendLikeVal(1);
            }
        }
        else {
            retVal += pArray[iter];
            ++iter;
        }
    }
    return retVal;
}
function isTrue(bVal) {
    (typeof bVal == 'string') ? (bVal != '' ? (isNaN(bVal) ? '' : bVal = parseInt(bVal)) : '') : '';
    var type = typeof bVal, returnType = false;
    switch (type) {
        case 'string': returnType = ((bVal && bVal.toLowerCase() != 'false' && bVal.toLowerCase() != 'null' && bVal != '') || bVal == '1' || bVal.toLowerCase() == 'true') ? true : false; break;
        case 'number': returnType = (bVal == 0) ? false : true; break;
        case 'object': returnType = (bVal) ? true : false; break;
        case 'undefined': returnType = (bVal) ? true : false; break;
        case 'boolean': returnType = (bVal) ? true : false; break;
    }
    return returnType;
}
function Sortscript(fieldsitems, tableName) {//valueObject for inner value object eg:activeparceldata
    if (fieldsitems == null || fieldsitems == '') return ""
    var SortItems = fieldsitems.trim().split(',');
    if (SortItems.indexOf('ROWUID') > -1) {
        SortItems.splice(SortItems.indexOf('ROWUID'), 1);
        if (SortItems.length == 0) return "";
    }
    var sortScript = ".sort(function(x,y){ return(";
    SortItems.forEach(function (Item) {
        var dir = '>', dir1 = '==', dir2 = '<', sortField = Item.trim().split(" ")[0];
        var sortFieldScript = sortField
        var dt = getDataField(sortField, tableName);
        if (dt) dt = dt.InputType;

        if (dt == "2" || dt == "7" || dt == "8" || dt == "9") {
            sortFieldScript = 'parseFloat(' + sortFieldScript + ')';
            //dir = '-'
        }
        else if (dt == "4") {
            sortFieldScript = 'new Date( ( ( (!' + sortFieldScript + ') || ( ' + sortFieldScript + ' ==  -99999999 ) || ( ' + sortFieldScript + '  == 99999999 ) )? ' + sortFieldScript + ': ' + sortFieldScript + '.toString().replace(" ", "T") ) )';  // in ios there is an issue with date + time , need to replace space with T.
            //sortFieldScript = 'new Date(' + sortFieldScript + ')';    //dir = '-'
        }
        else if (dt == "1" || dt == "5") {
            sortFieldScript = '(' + sortFieldScript + '!== null && ' + sortFieldScript + '!== "" && !isNaN(' + sortFieldScript + ')? (' + sortFieldScript + '): ' + sortFieldScript + ')';
        }

        if (dt == "4") {  // in date format, if same date ocuures it's not return true, so perform a sort action. Now condition chaged to execute first less than condition
            var sortFieldRegx = new RegExp(sortField, 'g');
            if (Item.replace(sortField, '').trim().toLowerCase() == "desc")
                sortScript += '(' + sortFieldScript.replace(sortFieldRegx, 'y.' + sortField) + ' ' + dir + sortFieldScript.replace(sortFieldRegx, 'x.' + sortField) + '?1:' + sortFieldScript.replace(sortFieldRegx, 'y.' + sortField) + ' ' + dir2 + sortFieldScript.replace(sortFieldRegx, 'x.' + sortField) + '?-1:0) ||';
            else
                sortScript += '(' + sortFieldScript.replace(sortFieldRegx, 'x.' + sortField) + ' ' + dir + sortFieldScript.replace(sortFieldRegx, 'y.' + sortField) + '?1:' + sortFieldScript.replace(sortFieldRegx, 'x.' + sortField) + ' ' + dir2 + sortFieldScript.replace(sortFieldRegx, 'y.' + sortField) + '?-1:0) ||';
        }
        else {
            if (Item.replace(sortField, '').trim().toLowerCase() == "desc")
                sortScript += '(' + sortFieldScript.replaceAll(sortField, 'y.' + sortField) + ' ' + dir + sortFieldScript.replaceAll(sortField, 'x.' + sortField) + '?1:' + sortFieldScript.replaceAll(sortField, 'y.' + sortField) + ' ' + dir1 + sortFieldScript.replaceAll(sortField, 'x.' + sortField) + '?0:-1) ||';
            else
                sortScript += '(' + sortFieldScript.replaceAll(sortField, 'x.' + sortField) + ' ' + dir + sortFieldScript.replaceAll(sortField, 'y.' + sortField) + '?1:' + sortFieldScript.replaceAll(sortField, 'x.' + sortField) + ' ' + dir1 + sortFieldScript.replaceAll(sortField, 'y.' + sortField) + '?0:-1) ||';
        }
    });
    return sortScript.slice(0, -2) + ")})"
}



function SortscriptCASE(fieldsitems, tableName) {//valueObject for inner value object eg:activeparceldata
    if (fieldsitems == null || fieldsitems == '') return ""
    fieldsitems=fieldsitems.replace(/\s\s+/g, ' ');
    fieldsitems=fieldsitems.replace(/WHEN/gi, "WHEN");
    fieldsitems=fieldsitems.replace(/CASE/gi, "CASE");
    fieldsitems=fieldsitems.replace(/ELSE/gi, "ELSE");
    fieldsitems=fieldsitems.replace(/ASC/gi, "ASC");
    fieldsitems=fieldsitems.replace(/DESC/gi, "DESC");
    var SortItems = fieldsitems.trim().split('WHEN');    
	if (SortItems.indexOf('ROWUID') > -1) {
		SortItems.splice(SortItems.indexOf('ROWUID'),1);
		if (SortItems.length == 0) return "";
	}
	var sortField = SortItems[0].replace('CASE','').trim();
    var sortScript = ".sort(function(a,b){"
    var i,item,con1="",con2="";
    for(i=1;i<SortItems.length;i++){
        item=SortItems[i].split(" ");
        con1+="x=="+item[1]+"? x="+item[3].replace('ELSE','')+": ";
        con2+="y=="+item[1]+"? y="+item[3].replace('ELSE','')+": ";
    }
    con1+="x=x;";
    con2+="y=y;";
    var order=fieldsitems.trim().split(" ");
    if ((order[order.length-1]) == "DESC"){
   		sortScript+='var x=a.'+ sortField +',y=b.'+ sortField +';' + con1 + con2 + 'return((y>x ? 1 : y == x ? 0 : -1) )});';
    }
    else{
    	sortScript+='var x=a.'+ sortField +',y=b.'+ sortField + ';' +con1 + con2 + 'return((x>y ? 1 : x == y ? 0 : -1) )});';
    }
          
    return sortScript;
}

var sort_by = function (){
    var fields = [].slice.call( arguments ),
        n_fields = fields.length;

    return function ( A, B ){
        var a, b, field, key, primer, reverse, result;

        for ( var i = 0, l = n_fields; i < l; i++ ){
            result = 0;
            field = fields[i];
            key = typeof field === 'string' ? field : field.name;
            a = A[key];
            b = B[key];
            if(field.name =='ROWUID')
            {
               field.primer ='parseInt'
               a = a < 0 ? 1000000000000 + parseInt(a) : parseInt(a);
               b = b < 0 ? 1000000000000 + parseInt(b) : parseInt(b);
            }
             else if ( typeof field.primer !== 'undefined' ){

               if(a) a = field.primer( a );
               if (b) b = field.primer( b );
            }
            reverse = ( field.reverse ) ? -1 : 1;
           
            if ( a === null && b )
                result = 1;
            else if ( b === null && a )
                result = -1;
            if ( a === b && i == n_fields - 1 ){
                if ( ( A.ROWUID > 0 ? A.ROWUID : 1000000000000 + parseInt( A.ROWUID ) ) > ( B.ROWUID > 0 ? B.ROWUID : 1000000000000 + parseInt( B.ROWUID ) ) ) result = 1
                if ( ( A.ROWUID > 0 ? A.ROWUID : 1000000000000 + parseInt( A.ROWUID ) ) < ( B.ROWUID > 0 ? B.ROWUID : 1000000000000 + parseInt( B.ROWUID ) ) ) result = -1
            }
            else if ( (a === null && b !== null) || (b === null && a !== null)) b === null ? result = reverse * 1 : result = reverse * -1;
            else if ( a && b && !isNaN(a) && !isNaN(b) && parseFloat(a) < parseFloat(b)) result = reverse * -1;
            else if ( a && b && !isNaN(a) && !isNaN(b) && parseFloat(a) > parseFloat(b)) result = reverse * 1;
            else if ( a && b && a < b ) result = reverse * -1;
            else if ( a && b && a > b ) result = reverse * 1;
            if ( result !== 0 ) break;
        }
        return result;
    }
}
Object.defineProperties( Array.prototype, { any: { enumerable: false, get: function () { return function ( v ) { return this.indexOf( v ) > -1; } } }, all: { enumerable: false, get: function () { return function ( v ) { return this.map( function ( x ) { return x == v } ).reduce( function ( x, y ) { return x && y }, this.length > 0 ); } } }, first: { enumerable: false, get: function () { return function ( v ) { return ( v === undefined && this[0] ) || this.indexOf( v ) == 0; } } }, last: { enumerable: false, get: function () { return function ( v ) { return ( v === undefined && this[this.length - 1] ) || this.indexOf( v ) == this.length - 1; } } }, sum: { enumerable: false, get: function () { return function ( v ) { var r = this.length > 0 && ( this.reduce( function ( a, b ) { return parseFloat( a ) + parseFloat( b ); } ) ); return ( v === undefined && r ) || r == v } } }, avg: { enumerable: false, get: function () { return function ( v ) { var r = this.length > 0 && ( this.reduce( function ( a, b ) { return parseFloat( a ) + parseFloat( b ); } ) ) / this.length; return ( v === undefined && r ) || r == v } } }, count: { enumerable: false, get: function () { return function ( v ) { return ( v === undefined ? this.length : this.length == v ) } } }, min: { enumerable: false, get: function () { return function ( v ) { return ( v === undefined && Math.min.apply( null, this ) ) || Math.min.apply( null, this ) == v } } }, max: { enumerable: false, get: function () { return function ( v ) { return ( v === undefined && Math.max.apply( null, this ) ) || Math.max.apply( null, this ) == v } } } } );