﻿// Define variable _debugger_key with unique identifier for each user scope.
if (!window['WORKER_URL'])
    throw 'WORKER_URL is not defined in the source page.';

const svcmgr = { workerUrl: WORKER_URL, ready: false, onconnect: null, onmessage: null, send: function (d) { return this.ready }, sw: null };

function initWorkerService() {
    if ('Worker' in window) {
        var sw = svcmgr.sw = new Worker(svcmgr.workerUrl);
        sw.onmessage = function (e) {
            svcmgr.onmessage && svcmgr.onmessage(e.data);
        }
        sw.onerror = function (e) {
            console.error(e);
        }
        svcmgr.send = function (d) {
            sw.postMessage(d)
        }
        svcmgr.onconnect && svcmgr.onconnect();
    }
    else if('serviceWorker' in navigator) {
        var connected = false;
        var _swRA = 0;
        var _initWorker = function () {
            svcmgr.sw = navigator.serviceWorker;
            svcmgr.sw.register(svcmgr.workerUrl, { scope: '/' })
                .then(function () {
                    svcmgr.ready = true;
                    svcmgr.sw.addEventListener("message", function (e) { svcmgr.onmessage && svcmgr.onmessage(e.data); });
                    svcmgr.send = function (d) {
                        svcmgr.sw.controller.postMessage(d)
                    }
                    window.setTimeout(function () {
                        if (svcmgr.sw.controller) {
                            svcmgr.onconnect && svcmgr.onconnect();
                        }
                        if (!svcmgr.sw.controller) {
                            (_swRA < 1) ? _initWorker() : console.error('SW-Init Failed');
                        }

                    }, 2000)
                    _swRA += 1;
                })
                .catch(function (e) { console.error(e); })
        }
        _initWorker();
    }
    else {
        //alert('No services available.')
    }
    /*else if ('SharedWorker' in self) {
        var sw = svcmgr.sw = new SharedWorker(svcmgr.workerUrl);
        sw.port.onmessage = function (e) {
            svcmgr.onmessage && svcmgr.onmessage(e.data);
        }
        sw.port.onerror = function (e) {
            console.error(e);
        }
        svcmgr.send = function (d) {
            sw.port.postMessage(d)
        }
        svcmgr.onconnect && svcmgr.onconnect();
    }*/
    

}

$(function () {
    //initWorkerService();

    //window.onfocus = function () {
    //    if (!svcmgr.sw || !svcmgr.sw.controller || svcmgr.sw.controller.state != 'activated') {
    //        initWorkerService();
    //    }
    //}
})

svcmgr.onconnect = function () {
    if (_debugger_key)
        sendToRWI({ cmd: 'setkey', value: _debugger_key });
}

svcmgr.onmessage = function (m) {
    if (m.sender == "rwi")
        processRWIMessage(m)
}


///Inspector Service
{
    var _debugger_temp_log = console.log;
    var _debugger_temp_warn = console.warn;
    var _debugger_temp_error = console.error;
    var _debugger_temp_time = console.time;
    var _debugger_temp_timeEnd = console.timeEnd;
    var _debugger_console_time = {};

    if (!window['_debugger_key'])
        _debugger_key = false;

    function processRWIMessage(m) {
        function a2j(a) {
            for (var i in a) {
                if (a[i] && a[i].constructor.name == "SQLError")
                    a[i] = a[i].message;
            }

            return JSON.stringify(a);
        }

        if (m.sender == "rwi")
            switch (m.type) {
                case '_debugger_auth':
                    console.warn(m.message);
                    break;
                case '_debugger_attached':
                    _debugger_temp_warn('Debugger attached');
                    console.log = function () {
                        sendToRWI({ type: 'debug-log', message: a2j(arguments) });
                    }
                    console.warn = function () {
                        sendToRWI({ type: 'debug-warn', message: a2j(arguments) });
                    }
                    console.error = function () {
                        sendToRWI({ type: 'debug-error', message: a2j(arguments) });
                    }
                    console.time = function (tag) {
                        _debugger_console_time[tag] = (new Date());
                    }
                    console.timeEnd = function (tag) {
                        var st = _debugger_console_time[tag];
                        if (st) {
                            var ds = (new Date() - st) / 1000, dm = 0;
                            var msg = ds + ' s';
                            if (ds > 60) {
                                dm = Math.floor(ds / 60);
                                ds = ds % 60;
                                msg = dm + ' min ' + dm + ' sec'
                            }

                            sendToRWI({ type: 'debug-log', message: a2j([tag + ': ', msg]) });
                            delete _debugger_console_time[tag];
                        } else {
                            sendToRWI({ type: 'debug-error', message: a2j(['time tag not found - ' + tag]) });
                        }
                        _debugger_console_time[tag] = (new Date());
                    }
                    console.log('Working Path: ' + window.location.href);
                    console.log('Page Title: ' + document.title);
                    break;
                case '_debugger_detached':
                    console.log = _debugger_temp_log;
                    console.warn = _debugger_temp_warn;
                    console.error = _debugger_temp_error;
                    console.time = _debugger_temp_time;
                    console.timeEnd = _debugger_temp_timeEnd;
                    console.warn('Debugger detached')
                    break;
                case 'query':
                    if (m.message.toLowerCase() == 'hello') {
                        sendToRWI({ type: 'message', message: 'Hello Developer!' });
                    } else if (m.message.toLowerCase() == 'time') {
                        sendToRWI({ type: 'message', message: 'Local time at the device is ' + (new Date).toLocaleString() });
                    } else {
                        try {
                            var commandProcessor = window['_debugger_processor'] || function (x) { return false };
                            if (!commandProcessor(m.message)) {
                                var ed = eval(m.message);
                                var str = JSON.stringify(ed);
                                sendToRWI({ type: 'eval', message: str, query: m.message });
                            }

                        } catch (e) {
                            sendToRWI({ type: 'error', message: e.message, query: m.message });
                        }
                    }

                    break;
                default:
                    var d = m.message;
                    if (d.substring(0, 5) == 'query') {
                        var data = d.substr(6);
                        try {
                            var ed = eval(data);
                            var str = JSON.stringify(ed);
                            sendToRWI({ type: 'eval', message: str });
                        } catch (e) {
                            sendToRWI({ type: 'error', message: e.message });
                        }
                    }
                    break;
            }
    }

    function sendToRWI(m) {
        if (m) { m.sender = 'rwi'; svcmgr.send(m); }
    }
}