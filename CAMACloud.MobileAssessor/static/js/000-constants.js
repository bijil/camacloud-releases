﻿//    DATA CLOUD SOLUTIONS, LLC ("COMPANY") CONFIDENTIAL 
//    Unpublished Copyright (c) 2010-2013 
//    DATA CLOUD SOLUTIONS, an Ohio Limited Liability Company, All Rights Reserved.

//    NOTICE: All information contained herein is, and remains the property of COMPANY.
//    The intellectual and technical concepts contained herein are proprietary to COMPANY
//    and may be covered by U.S. and Foreign Patents, patents in process, and are protected
//    by trade secret or copyright law. Dissemination of this information or reproduction
//    of this material is strictly forbidden unless prior written permission is obtained
//    from COMPANY. Access to the source code contained herein is hereby forbidden to
//    anyone except current COMPANY employees, managers or contractors who have executed
//    Confidentiality and Non-disclosure agreements explicitly covering such access.</p>

//    The copyright notice above does not evidence any actual or intended publication
//    or disclosure of this source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
//    DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
//    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
//    IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
//    OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
//    TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
//    ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
//var UsingOSM = false;
var ccma = {
    Environment: {
        PlatformType: null,
        Platform: null,
        ExternalWebSql: false
    },
    Session: {
        User: null,
        IsLoggedIn: false
    },
    State: {
        LastError: null
    },
    Constants: {},
    Map: {},
    ClientSettings: {},
    Sync: {},
    UI: {
        ActiveScreenId: null,
        ActivePhoto: null,
        AgreementContent: null,
        ActiveFormTabId: null,
        Camera: {
            MaxPhotosPerParcel: 10,
            MaxCaptureWidth: 1200,
            MaxCaptureHeight: 900,
            MaxStorageWidth: 500,
            MaxStorageHeight: 375,
            DPI: 72
        },
        FieldProps: {},
        RelatedFields: {},
        ParcelLinks: {
            OpenedFromLink: false,
            LinkMap: {}
        },
        SortScreenFields: [],
        SortOptions: [{ Id: 1, Name: 'Geo Location - Closest First' },{ Id: 2, Name: 'Geo Location - Closest Last' }, { Id: 3, Name: 'Parcel ID - Ascending' }, { Id: 4, Name: 'Parcel ID - Descending' }],
        EstimateChartFields: [],
        Events: {
            refreshParcelListAfterDataEntry: false,
            refreshDataCollectionEstimate: false
        },
        Validations: [],
        Layout: {
            Screen: {
                UsableHeight: 0,
                UsableWidth: 0
            }
        }
    },
    Sketching: {
        SketchFormatter: null,
        Config: null
    },
    Data: {
        Controller: null,
        Formatters: {},
        FieldCategories: [],
        LookupMap: {},
        Types: {},
        LookupTable: {}
    }
};


ccma.UI.__defineGetter__("SortOrderType", function () { return localStorage.getItem('sort-screen-sort-type') || 1; });
ccma.UI.__defineSetter__("SortOrderType", function (val) { localStorage.setItem('sort-screen-sort-type', val); });
ccma.Data.Types = {
    1: { "cctype": 1, "typename": "Text", "baseType": "text", "htmltag": "input", "type": "text", "pattern": "", "maxlength": 50, "rows": null, "multiple": null, "dataSource": null, "changeEvent": "change", "dataType": "string", "jsType": "String", "formatter": "s" },
    2: { "cctype": 2, "typename": "Real Number", "baseType": "number", "htmltag": "input", "type": "text", "pattern": "^(?:-?([0-9]+([.][0-9]{1,10})?))?$", "maxlength": 15, "rows": null, "multiple": null, "dataSource": null, "changeEvent": "change", "dataType": "float", "jsType": "Number", "formatter": ",." },
    3: { "cctype": 3, "typename": "Yes/No", "baseType": "select", "htmltag": "select", "type": null, "pattern": "", "maxlength": null, "rows": null, "multiple": null, "dataSource": "ccma.YesNo", "changeEvent": "change", "dataType": "string", "jsType": "String", "formatter": "s" },
    4: { "cctype": 4, "typename": "Date", "baseType": "date", "htmltag": "input", "type": "date", "pattern": "", "maxlength": 10, "rows": null, "multiple": null, "dataSource": null, "changeEvent": "change", "dataType": "date", "jsType": "Date", "formatter": "d" },
    5: { "cctype": 5, "typename": "Lookup", "baseType": "select", "htmltag": "select", "type": null, "pattern": "", "maxlength": null, "rows": null, "multiple": null, "dataSource": "field.LookupTable", "changeEvent": "change", "dataType": "string", "jsType": "String", "formatter": "s" },
    6: { "cctype": 6, "typename": "Long Text", "baseType": "text", "htmltag": "textarea", "type": null, "pattern": "", "maxlength": 200, "rows": 2, "multiple": null, "dataSource": null, "changeEvent": "change", "dataType": "string", "jsType": "String", "formatter": "s" },
    7: { "cctype": 7, "typename": "Money", "baseType": "number", "htmltag": "input", "type": "number", "pattern": "^([0-9]*(.[0-9]{1,4})?)?$", "maxlength": 15, "rows": null, "multiple": null, "dataSource": null, "changeEvent": "change", "dataType": "money", "jsType": "Number", "formatter": "$" },
    8: { "cctype": 8, "typename": "Whole Number", "baseType": "number", "htmltag": "input", "type": "text", "pattern": "^(?:[1-9][0-9]*|0)$", "maxlength": 15, "rows": null, "multiple": null, "dataSource": null, "changeEvent": "change", "dataType": "int", "jsType": "Number", "formatter": "," },
    9: { "cctype": 9, "typename": "Cost Value", "baseType": "number", "htmltag": "input", "type": "number", "pattern": "^[0-9]*$", "maxlength": 15, "rows": null, "multiple": null, "dataSource": null, "changeEvent": "change", "dataType": "value", "jsType": "Number", "formatter": "$$$" },
    10: { "cctype": 10, "typename": "Year", "baseType": "number", "htmltag": "input", "type": "text", "pattern": "^(0|9999|[12][0987][0-9][0-9])?$", "maxlength": 4, "rows": null, "multiple": null, "dataSource": null, "changeEvent": "change", "dataType": "year", "jsType": "Number", "formatter": "i" },
    11: { "cctype": 11, "typename": "Lookup List", "baseType": "select", "htmltag": "select", "type": null, "pattern": "", "maxlength": null, "rows": null, "multiple": "multiple", "dataSource": "field.LookupTable", "changeEvent": "change", "dataType": "string", "jsType": "String", "formatter": "s" },
    12: { "cctype": 12, "typename": "Geo Location", "baseType": "text", "htmltag": "input", "type": "text", "pattern": "", "maxlength": 30, "rows": null, "multiple": null, "dataSource": null, "changeEvent": "change", "dataType": "string", "jsType": "String", "formatter": "g" },
	13: { "cctype": 13, "typename": "Tri State Radio", "baseType": "text", "htmltag": "radiogroup", "type": null, "pattern": "", "maxlength": null, "rows": null, "multiple": null, "dataSource": null, "changeEvent": "change", "dataType": "string", "jsType": "String", "formatter": "s" }
};

/*
GIS Related Constants
*/

var mapTypeId = null;
if (google) {
    mapTypeId = google.maps.MapTypeId.HYBRID;
}

var gisMapOptions = {
    zoom: 18,
    maxZoom: 20,
    minZoom: 10,
    mapTypeId: mapTypeId,
    gestureHandling: 'greedy',
    tilt: 0,
    streetViewControl: false
};

var smallMapOptions = {
    zoom: 18,
    maxZoom: 19,
    minZoom: 14,
    mapTypeId: mapTypeId,
    gestureHandling: 'greedy',
    tilt: 0,
    streetViewControl: false
};

var routeMapOptions = {
    zoom: 15,
    maxZoom: 19,
    minZoom: 13,
    mapTypeId: mapTypeId
};

var defaultFields = ["CURRENT_DATE",
					"CURRENTDATE",
					"CURRENT_USER",
					"CURRENTUSER",
					"CURRENT_YEAR",
					"CURRENTYEAR",
					"CURRENT_YEAR",
					"CURRENTYEAR",
					"ACTIVE_YEAR",
					"ACTIVEYEAR",
					"CURRENT_DAY",
					"CURRENTDAY",
					"CURRENT_MONTH",
					"CURRENTMONTH",
					"FUTUREYEARSTATUS",
					"BLANK"];

var anchor, iconParcel, iconParcelAlert, iconParcelPriority, iconParcelVisited, infoWindow, GeoLocationIcon, ParcelUnsync, ParcelAlertUnsync, ParcelPriorityUnsync, ParcelVisitedUnsync,bppParcel,bppParcelAlert,bppParcelPriority,bppParcelVisited,bppParcelUnsync,bppParcelAlertUnsync,bppParcelPriorityUnsync ;
var iconParcelFirst, iconParcelAlertFirst, iconParcelPriorityFirst, iconParcelVisitedFirst, ParcelUnsyncFirst, ParcelAlertUnsyncFirst, ParcelPriorityUnsyncFirst, ParcelVisitedUnsyncFirst;
var icritical, icriticalFirst, icritcalUnsync, icriticalUnsyncFirst, imedium, imediumFirst, imediumUnsync, imediumUnsyncFirst,
    inormal, inormalFirst, inormalUnsync, inormalUnsyncFirst, bppCritical, bppCriticalUnsync, bppNormal, bppNormalUnsync, bppMedium, bppMediumUnsync;
function setMapIcons(callback) {
    // if (google & UsingOSM == false) {
    if (EnableNewPriorities == 'True') {
        if (UsingOSM == false) {
            if (google == false) return false;
            anchor = new google.maps.MarkerImage('/static/maps/anchor.png', new google.maps.Size(32, 32), new google.maps.Point(0, 0), new google.maps.Point(17, 27));
            iconParcel = new google.maps.MarkerImage('/static/maps/proximity-parcel-1.png', new google.maps.Size(32, 37), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            icritical = new google.maps.MarkerImage('/static/maps/parcel-alert.png', new google.maps.Size(32, 37), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            iconParcelAlert = new google.maps.MarkerImage('/static/maps/parcel-priority.png', new google.maps.Size(32, 37), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            iconParcelPriority = new google.maps.MarkerImage('/static/maps/high_parcel.png', new google.maps.Size(32, 37), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            imedium = new google.maps.MarkerImage('/static/maps/medium_parcel.png', new google.maps.Size(32, 37), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            inormal = new google.maps.MarkerImage('/static/maps/normal_parcel-1.png', new google.maps.Size(32, 37), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            iconParcelVisited = new google.maps.MarkerImage('/static/maps/parcel-done.png', new google.maps.Size(32, 37), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            GeoLocationIcon = new google.maps.MarkerImage('/static/maps/Geo_location.png', new google.maps.Size(32, 37), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            infoWindow = new google.maps.InfoWindow({ content: 'Parcel' });
            ParcelUnsync = new google.maps.MarkerImage('/static/maps/proximity-unsync-1.png', new google.maps.Size(32, 37), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            icritcalUnsync = new google.maps.MarkerImage('/static/maps/Unsync-alert.png', new google.maps.Size(32, 37), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            ParcelAlertUnsync = new google.maps.MarkerImage('/static/maps/Unsync-priority.png', new google.maps.Size(32, 37), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            ParcelPriorityUnsync = new google.maps.MarkerImage('/static/maps/high_unsync.png', new google.maps.Size(32, 37), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            imediumUnsync = new google.maps.MarkerImage('/static/maps/medium_unsync.png', new google.maps.Size(32, 37), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            inormalUnsync = new google.maps.MarkerImage('/static/maps/normal_unsync-1.png', new google.maps.Size(32, 37), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            iconParcelFirst = new google.maps.MarkerImage('/static/maps/proximity-first-1.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            icriticalFirst = new google.maps.MarkerImage('/static/maps/parcel-alert_first.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            iconParcelAlertFirst = new google.maps.MarkerImage('/static/maps/parcel-priority_first.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            iconParcelPriorityFirst = new google.maps.MarkerImage('/static/maps/high_first.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            imediumFirst = new google.maps.MarkerImage('/static/maps/medium_first.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            inormalFirst = new google.maps.MarkerImage('/static/maps/normal_first-1.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            iconParcelVisitedFirst = new google.maps.MarkerImage('/static/maps/parcel-done_first.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            ParcelUnsyncFirst = new google.maps.MarkerImage('/static/maps/proximity-unsync-first-1.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            icriticalUnsyncFirst = new google.maps.MarkerImage('/static/maps/Unsync-alert_first.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            ParcelAlertUnsyncFirst = new google.maps.MarkerImage('/static/maps/Unsync-priority_first.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            ParcelPriorityUnsyncFirst = new google.maps.MarkerImage('/static/maps/high_unsync_first.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            imediumUnsyncFirst = new google.maps.MarkerImage('/static/maps/medium_unsync_first.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            inormalUnsyncFirst = new google.maps.MarkerImage('/static/maps/normal_unsync_first-1.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            bppParcel = new google.maps.MarkerImage('/static/maps/bpp-proximity-1.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            bppCritical = new google.maps.MarkerImage('/static/maps/bpp-parcel-alert.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            bppParcelAlert = new google.maps.MarkerImage('/static/maps/bpp-parcel-priority.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            bppParcelPriority = new google.maps.MarkerImage('/static/maps/Bpp_high.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            bppMedium = new google.maps.MarkerImage('/static/maps/Bpp_medium.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            bppNormal = new google.maps.MarkerImage('/static/maps/bpp_normal-1.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            bppParcelVisited = new google.maps.MarkerImage('/static/maps/bpp-parcel-done.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            bppParcelUnsync = new google.maps.MarkerImage('/static/maps/bpp-proximity-unsync-1.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            bppCriticalUnsync = new google.maps.MarkerImage('/static/maps/bpp-Unsync-alert.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            bppParcelAlertUnsync = new google.maps.MarkerImage('/static/maps/bpp-Unsync-priority.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            bppMediumUnsync = new google.maps.MarkerImage('/static/maps/bpp_medium_unsync.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            bppNormalUnsync = new google.maps.MarkerImage('/static/maps/bpp_normal_unsync-1.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            bppParcelPriorityUnsync = new google.maps.MarkerImage('/static/maps/BPP_high_unsync.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            ParcelLocationIcon = new google.maps.MarkerImage('/static/maps/location.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
        }
        else {
            anchor = L.icon({ iconUrl: '/static/maps/anchor.png', iconRetinaUrl: '/static/maps/anchor.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            iconParcelVisited = L.icon({ iconUrl: '/static/maps/parcel-done.png', iconRetinaUrl: '/static/maps/parcel-done.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            iconParcel = L.icon({ iconUrl: '/static/maps/proximity-parcel-1.png', iconRetinaUrl: '/static/maps/proximity-parcel-1.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            GeoLocationIcon = L.icon({ iconUrl: '/static/maps/Geo_location.png', iconRetinaUrl: '/static/maps/Geo_location.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            iconParcelAlert = L.icon({ iconUrl: '/static/maps/parcel-priority.png', iconRetinaUrl: '/static/maps/parcel-priority.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            iconParcelPriority = L.icon({ iconUrl: '/static/maps/high_parcel.png', iconRetinaUrl: '/static/maps/high_parcel.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            icritical = L.icon({ iconUrl: '/static/maps/parcel-alert.png', iconRetinaUrl: '/static/maps/parcel-alert.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            imedium = L.icon({ iconUrl: '/static/maps/medium_parcel.png', iconRetinaUrl: '/static/maps/medium_parcel.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            inormal = L.icon({ iconUrl: '/static/maps/normal_parcel-1.png', iconRetinaUrl: '/static/maps/normal_parcel-1.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            ParcelUnsync = L.icon({ iconUrl: '/static/maps/proximity-unsync-1.png', iconRetinaUrl: '/static/maps/proximity-unsync-1.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            ParcelAlertUnsync = L.icon({ iconUrl: '/static/maps/Unsync-priority.png', iconRetinaUrl: '/static/maps/Unsync-priority.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            ParcelPriorityUnsync = L.icon({ iconUrl: '/static/maps/high_unsync.png', iconRetinaUrl: '/static/maps/high_unsync.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            icritcalUnsync = L.icon({ iconUrl: '/static/maps/Unsync-alert.png', iconRetinaUrl: '/static/maps/Unsync-alert.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            imediumUnsync = L.icon({ iconUrl: '/static/maps/medium_unsync.png', iconRetinaUrl: '/static/maps/medium_unsync.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            inormalUnsync = L.icon({ iconUrl: '/static/maps/normal_unsync-1.png', iconRetinaUrl: '/static/maps/normal_unsync-1.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            iconParcelVisitedFirst = L.icon({ iconUrl: '/static/maps/parcel-done_first.png', iconRetinaUrl: '/static/maps/parcel-done_first.png', iconSize: [48, 50], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            iconParcelFirst = L.icon({ iconUrl: '/static/maps/proximity-first-1.png', iconRetinaUrl: '/static/maps/proximity-first-1.png', iconSize: [48, 50], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            iconParcelAlertFirst = L.icon({ iconUrl: '/static/maps/parcel-priority_first.png', iconRetinaUrl: '/static/maps/parcel-priority_first.png', iconSize: [48, 50], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            iconParcelPriorityFirst = L.icon({ iconUrl: '/static/maps/high_first.png', iconRetinaUrl: '/static/maps/high_first.png', iconSize: [48, 50], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            icriticalFirst = L.icon({ iconUrl: '/static/maps/parcel-alert_first.png', iconRetinaUrl: '/static/maps/parcel-alert_first.png', iconSize: [48, 50], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            imediumFirst = L.icon({ iconUrl: '/static/maps/medium_first.png', iconRetinaUrl: '/static/maps/medium_first.png', iconSize: [48, 50], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            inormalFirst = L.icon({ iconUrl: '/static/maps/normal_first-1.png', iconRetinaUrl: '/static/maps/normal_first-1.png', iconSize: [48, 50], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            ParcelUnsyncFirst = L.icon({ iconUrl: '/static/maps/proximity-unsync-first-1.png', iconRetinaUrl: '/static/maps/proximity-unsync-first-1.png', iconSize: [48, 50], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            ParcelAlertUnsyncFirst = L.icon({ iconUrl: '/static/maps/Unsync-priority_first.png', iconRetinaUrl: '/static/maps/Unsync-priority_first.png', iconSize: [48, 50], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            ParcelPriorityUnsyncFirst = L.icon({ iconUrl: '/static/maps/high_unsync_first.png', iconRetinaUrl: '/static/maps/high_unsync_first.png', iconSize: [48, 50], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            icriticalUnsyncFirst = L.icon({ iconUrl: '/static/maps/Unsync-alert_first.png', iconRetinaUrl: '/static/maps/Unsync-alert_first.png', iconSize: [48, 50], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            imediumUnsyncFirst = L.icon({ iconUrl: '/static/maps/medium_unsync_first.png', iconRetinaUrl: '/static/maps/medium_unsync_first.png', iconSize: [48, 50], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            inormalUnsyncFirst = L.icon({ iconUrl: '/static/maps/normal_unsync_first-1.png', iconRetinaUrl: '/static/maps/normal_unsync_first-1.png', iconSize: [48, 50], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            bppParcel = L.icon({ iconUrl: '/static/maps/bpp-proximity-1.png', iconRetinaUrl: '/static/maps/bpp-proximity-1.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            bppParcelAlert = L.icon({ iconUrl: '/static/maps/bpp-parcel-priority.png', iconRetinaUrl: '/static/maps/bpp-parcel-priority.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            bppParcelPriority = L.icon({ iconUrl: '/static/maps/Bpp_high.png', iconRetinaUrl: '/static/maps/Bpp_high.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            bppCritical = L.icon({ iconUrl: '/static/maps/bpp-parcel-alert.png', iconRetinaUrl: '/static/maps/Bpp-parcel-alert.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            bppMedium = L.icon({ iconUrl: '/static/maps/Bpp_medium.png', iconRetinaUrl: '/static/maps/Bpp_medium.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            bppNormal = L.icon({ iconUrl: '/static/maps/bpp_normal-1.png', iconRetinaUrl: '/static/maps/bpp_normal-1.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            bppParcelVisited = L.icon({ iconUrl: '/static/maps/bpp-parcel-done.png', iconRetinaUrl: '/static/maps/bpp-parcel-done.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            bppParcelUnsync = L.icon({ iconUrl: '/static/maps/bpp-proximity-unsync-1.png', iconRetinaUrl: '/static/maps/bpp-proximity-unsync-1.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            bppParcelAlertUnsync = L.icon({ iconUrl: '/static/maps/bpp-Unsync-priority.png', iconRetinaUrl: '/static/maps/bpp-Unsync-priority.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            bppParcelPriorityUnsync = L.icon({ iconUrl: '/static/maps/BPP_high_unsync.png', iconRetinaUrl: '/static/maps/BPP_high_unsync.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            bppCriticalUnsync = L.icon({ iconUrl: '/static/maps/bpp-Unsync-alert.png', iconRetinaUrl: '/static/maps/bpp-Unsync-alert.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            bppMediumUnsync = L.icon({ iconUrl: '/static/maps/bpp_medium_unsync.png', iconRetinaUrl: '/static/maps/bpp_medium_unsync.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            bppNormalUnsync = L.icon({ iconUrl: '/static/maps/bpp_normal_unsync-1.png', iconRetinaUrl: '/static/maps/bpp_normal_unsync-1.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            ParcelLocation = L.icon({ iconUrl: '/static/maps/location.png', iconRetinaUrl: '/static/maps/location.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
        }
    }
    else {
        if (UsingOSM == false) {
            if (google == false) return false;
            anchor = new google.maps.MarkerImage('/static/maps/anchor.png', new google.maps.Size(32, 32), new google.maps.Point(0, 0), new google.maps.Point(17, 27));
            iconParcel = new google.maps.MarkerImage('/static/maps/parcel.png', new google.maps.Size(32, 37), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            iconParcelAlert = new google.maps.MarkerImage('/static/maps/parcel-alert.png', new google.maps.Size(32, 37), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            iconParcelPriority = new google.maps.MarkerImage('/static/maps/parcel-priority.png', new google.maps.Size(32, 37), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            iconParcelVisited = new google.maps.MarkerImage('/static/maps/parcel-done.png', new google.maps.Size(32, 37), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            GeoLocationIcon = new google.maps.MarkerImage('/static/maps/Geo_location.png', new google.maps.Size(32, 37), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            infoWindow = new google.maps.InfoWindow({ content: 'Parcel' });
            ParcelUnsync = new google.maps.MarkerImage('/static/maps/Unsync-parcel.png', new google.maps.Size(32, 37), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            ParcelAlertUnsync = new google.maps.MarkerImage('/static/maps/Unsync-alert.png', new google.maps.Size(32, 37), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            ParcelPriorityUnsync = new google.maps.MarkerImage('/static/maps/Unsync-priority.png', new google.maps.Size(32, 37), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            iconParcelFirst = new google.maps.MarkerImage('/static/maps/parcel_first.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            iconParcelAlertFirst = new google.maps.MarkerImage('/static/maps/parcel-alert_first.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            iconParcelPriorityFirst = new google.maps.MarkerImage('/static/maps/parcel-priority_first.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            iconParcelVisitedFirst = new google.maps.MarkerImage('/static/maps/parcel-done_first.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            ParcelUnsyncFirst = new google.maps.MarkerImage('/static/maps/Unsync-parcel_first.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            ParcelAlertUnsyncFirst = new google.maps.MarkerImage('/static/maps/Unsync-alert_first.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            ParcelPriorityUnsyncFirst = new google.maps.MarkerImage('/static/maps/Unsync-priority_first.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            bppParcel = new google.maps.MarkerImage('/static/maps/bpp-parcel.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            bppParcelAlert = new google.maps.MarkerImage('/static/maps/bpp-parcel-alert.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            bppParcelPriority = new google.maps.MarkerImage('/static/maps/bpp-parcel-priority.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            bppParcelVisited = new google.maps.MarkerImage('/static/maps/bpp-parcel-done.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            bppParcelUnsync = new google.maps.MarkerImage('/static/maps/bpp-Unsync-parcel.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            bppParcelAlertUnsync = new google.maps.MarkerImage('/static/maps/bpp-Unsync-alert.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            bppParcelPriorityUnsync = new google.maps.MarkerImage('/static/maps/bpp-Unsync-priority.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
            ParcelLocationIcon = new google.maps.MarkerImage('/static/maps/location.png', new google.maps.Size(40, 47), new google.maps.Point(0, 0), new google.maps.Point(17, 37));
        }
        else {
            anchor = L.icon({ iconUrl: '/static/maps/anchor.png', iconRetinaUrl: '/static/maps/anchor.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            iconParcelVisited = L.icon({ iconUrl: '/static/maps/parcel-done.png', iconRetinaUrl: '/static/maps/parcel-done.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            iconParcel = L.icon({ iconUrl: '/static/maps/parcel.png', iconRetinaUrl: '/static/maps/parcel.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            GeoLocationIcon = L.icon({ iconUrl: '/static/maps/Geo_location.png', iconRetinaUrl: '/static/maps/Geo_location.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            iconParcelAlert = L.icon({ iconUrl: '/static/maps/parcel-alert.png', iconRetinaUrl: '/static/maps/parcel-alert.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            iconParcelPriority = L.icon({ iconUrl: '/static/maps/parcel-priority.png', iconRetinaUrl: '/static/maps/parcel-priority.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            ParcelUnsync = L.icon({ iconUrl: '/static/maps/Unsync-parcel.png', iconRetinaUrl: '/static/maps/Unsync-parcel.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            ParcelAlertUnsync = L.icon({ iconUrl: '/static/maps/Unsync-alert.png', iconRetinaUrl: '/static/maps/Unsync-alert.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            ParcelPriorityUnsync = L.icon({ iconUrl: '/static/maps/Unsync-priority.png', iconRetinaUrl: '/static/maps/Unsync-priority.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            iconParcelVisitedFirst = L.icon({ iconUrl: '/static/maps/parcel-done_first.png', iconRetinaUrl: '/static/maps/parcel-done_first.png', iconSize: [48, 50], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            iconParcelFirst = L.icon({ iconUrl: '/static/maps/parcel_first.png', iconRetinaUrl: '/static/maps/parcel_first.png', iconSize: [48, 50], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            iconParcelAlertFirst = L.icon({ iconUrl: '/static/maps/parcel-alert_first.png', iconRetinaUrl: '/static/maps/parcel-alert_first.png', iconSize: [48, 50], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            iconParcelPriorityFirst = L.icon({ iconUrl: '/static/maps/parcel-priority_first.png', iconRetinaUrl: '/static/maps/parcel-priority_first.png', iconSize: [48, 50], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            ParcelUnsyncFirst = L.icon({ iconUrl: '/static/maps/Unsync-parcel_first.png', iconRetinaUrl: '/static/maps/Unsync-parcel_first.png', iconSize: [48, 50], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            ParcelAlertUnsyncFirst = L.icon({ iconUrl: '/static/maps/Unsync-alert_first.png', iconRetinaUrl: '/static/maps/Unsync-alert_first.png', iconSize: [48, 50], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            ParcelPriorityUnsyncFirst = L.icon({ iconUrl: '/static/maps/Unsync-priority_first.png', iconRetinaUrl: '/static/maps/Unsync-priority_first.png', iconSize: [48, 50], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            bppParcel = L.icon({ iconUrl: '/static/maps/bpp-parcel.png', iconRetinaUrl: '/static/maps/Bpp-parcel.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            bppParcelAlert = L.icon({ iconUrl: '/static/maps/bpp-parcel-alert.png', iconRetinaUrl: '/static/maps/Bpp-parcel-alert.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            bppParcelPriority = L.icon({ iconUrl: '/static/maps/bpp-parcel-priority.png', iconRetinaUrl: '/static/maps/Bpp-parcel-priority.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            bppParcelVisited = L.icon({ iconUrl: '/static/maps/bpp-parcel-done.png', iconRetinaUrl: '/static/maps/bpp-parcel-done.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            bppParcelUnsync = L.icon({ iconUrl: '/static/maps/bpp-Unsync-parcel.png', iconRetinaUrl: '/static/maps/bpp-Unsync-parcel.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            bppParcelAlertUnsync = L.icon({ iconUrl: '/static/maps/bpp-Unsync-alert.png', iconRetinaUrl: '/static/maps/bpp-Unsync-alert.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            bppParcelPriorityUnsync = L.icon({ iconUrl: '/static/maps/bpp-Unsync-priority.png', iconRetinaUrl: '/static/maps/bpp-Unsync-priority.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
            ParcelLocation = L.icon({ iconUrl: '/static/maps/location.png', iconRetinaUrl: '/static/maps/location.png', iconSize: [40, 40], iconAnchor: [22, 50], popupAnchor: [0, -56], shadowUrl: '', shadowRetinaUrl: '', shadowSize: [68, 95], shadowAnchor: [22, 94] });
        }
    }

    if (callback) callback();
}

//ccma.YesNo = [{ Name: 'NO VALUE', Value: '' }, { Name: 'No', Value: 'false' }, { Name: 'Yes', Value: 'true'}];
ccma.YesNo = [{ Name: 'No', Id: 'false' }, { Name: 'Yes', Id: 'true' }];

var msg_FormNoNavigate = 'You are not allowed to navigate from this record as all required fields in this form are not filled in.';
var msg_FormNoCreate = 'You are not allowed to navigate from this record as all required fields in this form are not filled in.';
//msp change starts
var msg_FormNoCopy = 'You are not allowed to navigate from this record as all required fields in this form are not filled in.';
var msg_CopyInProgress = 'Copying in progress. Please wait a while..';
var copy_Complete = 'Copying Completed.';
var msg_nonExisting_ddlVal = 'You are attempting to save a non-existing value.';
//var msg_after_mass_update = 'Changes Updated.You are redirecting to previous page by clicking OK.';
var msg_after_mass_update = 'Changes updated. You will now be redirected to the previous page.';
var msg_cancel = 'Are you sure you want to cancel all changes?';
var msg_mass_upd_in_progress = 'Mass updation is in progress.Please Wait a while..';
var msg_unique_in_field = 'You need to input a unique value in this field.';
var msg_confirm_mass_update = 'Are you sure that you want to update all records in this section?';
var msg_sketcheditor_close = 'There are unsaved changes to the sketch. Tap OK to continue exiting the editor and lose the pending changes or CANCEL to return to the sketch editor to save your changes or to continue editing.';
//msp change ends
var lookup_0 = 'Id', lookup_1 = 'Name'; lookup_2 = 'Ordinal';

//msp standard Encap
function isEmpty(k) {
    var type = typeof k, retVal = false;
    switch (type) {
        case 'string': retVal = (k == '' || k == null || k == undefined); break;
        case 'object': retVal = (k) ? (k.length > 0 ? false : true) : true; break;
    }
    return retVal;
}

Date.prototype.addDays = function (days) { var date = new Date(this.valueOf()); date.setDate(date.getDate() + days); return date; }

var isLookupScreen = false;
ccma.__defineGetter__( "CurrentDay", function () { return ( new Date() ).getDate() } );
ccma.__defineGetter__( "CurrentMonth", function () { return ( new Date() ).getMonth() + 1 } );
ccma.__defineGetter__("CurrentYear", function () { return (new Date()).getFullYear() });
ccma.__defineGetter__("CurrentDate", function () { return (new Date()) });
ccma.__defineGetter__("CurrentDate", function () { return ((new Date()).format("ymd")) + ' 12:00:00'}); 
ccma.__defineGetter__("CurrentUser", function () { return localStorage.getItem('username') });
ccma.__defineGetter__("Today", function () { return (new Date()).format("ymd") });
ccma.__defineGetter__("Tomorrow", function () { return (new Date()).addDays(1).format("ymd") });
ccma.__defineGetter__("FutureYearStatus", function () {  return (showFutureData ? 'F' : 'A') }); 
