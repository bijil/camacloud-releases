﻿//    DATA CLOUD SOLUTIONS, LLC ("COMPANY") CONFIDENTIAL 
//    Unpublished Copyright (c) 2010-2013 
//    DATA CLOUD SOLUTIONS, an Ohio Limited Liability Company, All Rights Reserved.

//    NOTICE: All information contained herein is, and remains the property of COMPANY.
//    The intellectual and technical concepts contained herein are proprietary to COMPANY
//    and may be covered by U.S. and Foreign Patents, patents in process, and are protected
//    by trade secret or copyright law. Dissemination of this information or reproduction
//    of this material is strictly forbidden unless prior written permission is obtained
//    from COMPANY. Access to the source code contained herein is hereby forbidden to
//    anyone except current COMPANY employees, managers or contractors who have executed
//    Confidentiality and Non-disclosure agreements explicitly covering such access.</p>

//    The copyright notice above does not evidence any actual or intended publication
//    or disclosure of this source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
//    DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
//    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
//    IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
//    OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
//    TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
//    ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.


ccma.Data.Evaluable = {};

ccma.Data.Evaluable.__eval = function (source, expression, outType, parentSource, dvRules, isZeroRecord) {
    // Assumptions: Calculations done only on number data types
    // For all other types, exit after the first variable
    if (source && source.indexOf('[') > -1) {
        var t = eval('this.' + source);
        if (!t && expression.contains('parent.') && !isZeroRecord) {
            source = parentSource;
            expression = expression.replace(/parent./g, '')
        }
    }
    if (expression === undefined && (!!source)) {
        outType = null;
        expression = source;
        source = null;
    }
    var tableName, ParentTable, actualTable;
    if (source) {
        if (source.indexOf('[') > -1)
            actualTable = source.substr(0, source.indexOf('['));
        else
            actualTable = source;
    }
    if (parentSource) {
        if (parentSource.indexOf('[') > -1)
            Parenttable = parentSource.substr(0, parentSource.indexOf('['));
        else
            Parenttable = parentSource;
    }
    if (outType) {
        if ((typeof outType == "string") || (typeof outType == "number"))
            outType = ccma.Data.Types[outType];
        else if (typeof outType == "object")
        { } else (outType = null);
    }

    let dummyData = { Original: {} };
    if (isZeroRecord && parentSource) {
        try {
            dummyData.parentRecord = eval('this.' + parentSource);
        }
        catch (ex) {
            dummyData.parentRecord = {};
        } 
    }

    var result = ccma.Data.Evaluable.__getEvalResult();

    var originalExp = expression.toString(), field = getDataField(expression, actualTable), calcField = false;
    
   

    if (field && field.InputType != "5") if ((field.CalculationExpression != null) && (field.CalculationExpression != "")) {
        if ((field.CalculationOverrideExpression != null) && (field.CalculationOverrideExpression != "")) {
            if (activeParcel && !activeParcel.EvalValue(source, decodeHTML(field.CalculationOverrideExpression))) {
                expression = field.CalculationExpression;
                calcField = true;
            }
        } else if (field.CalculationExpression.search(childRecCalcPattern) == -1) {
        	expression = field.CalculationExpression;
        	calcField = true;
    	}
    }

    var isMIFieldTrue = (field?.['UI_Settings']?.MIFields ?? "") === "true";

    var fdt, orgExpression = '', testExp = expression.toString();
    var sumField = testExp?.contains("+") || testExp?.contains("-") ? true : false;
    var ifcondition = /.*?([a-zA-Z][a-zA-Z0-9_#.]+)(?=[^']*(?:'[^']*'[^']*)*$).*?/i.exec(testExp);
    if (ifcondition && ifcondition[1].toUpperCase() == "IF" && isMIFieldTrue && clientSettings?.LoadMethodAfterAppLoad == "loadValidationTablesForMI") {
        try {
            var value = 0;
            var original = 0;
            var exp = expression.toString();
            var mfieldid = exp.match(/([a-zA-Z_][a-zA-Z0-9_]+)/g)
            if (mfieldid != null) {
                var ufieldid = mfieldid.filter((item, index) => mfieldid.indexOf(item) === index);
                var type = '';
                if (isZeroRecord) {
                    if (eval('dummyData.parentRecord')) type += '.parentRecord';
                }
                var iforiginalexp;
                var ifvvalue = exp.replace(/if/g, 'IF');
                ifvvalue = ifvvalue.replace(/If/g, 'IF');
                ifvvalue = ifvvalue.replace(/([^<>=])=([^<>=])/g, '$1==$2');
                for (var v in ufieldid)
                    if (ufieldid[v].toUpperCase() != "IF") {
                        result.Value = eval('this' + (source ? '.' + source : '') + type + "['" + ufieldid[v] + "']");
                        result.Original = eval('this' + (source ? '.' + source : '') + '.Original' + "['" + ufieldid[v] + "']");
                        result.Value = result.Value === null || result.Value === "" ? 0 : result.Value;
                        result.Original = result.Original === null || result.Original === "" ? 0 : result.Original;
                        var v1 = ufieldid[v];
                        ifvvalue = ifvvalue.replaceAll(v1, result.Value);
                        iforiginalexp = ifvvalue.replaceAll(v1, result.Original);
                    }
                var vvalue = ifvvalue;
                var originalvvalue = iforiginalexp;
                value = eval(vvalue);
                original = eval(originalvvalue);
            }
            else {
                value = expression;
            }

        } catch (e) {
            console.warn('Error while evaluating ' + expression, e);
            return ccma.Data.Evaluable.__getEvalResult(null, "****", "****");
        }
      } else {
        while ((match = /.*?([a-zA-Z][a-zA-Z0-9_#.]+)(?=[^']*(?:'[^']*'[^']*)*$).*?/i.exec(testExp)) != null) {
            var v = match[1];
            if (v && (v.toLowerCase() == 'math.round')) {
                var regex = new RegExp(v, "g");
                testExp = testExp.replace(regex, '');
                continue;
            }

            var fieldExp = v, input = match.input, type = '';
            tableName = actualTable;

            while (v.indexOf('parent.') > -1) {
                tableName = parentSource;
                v = v.replace('parent.', '');
                if (isZeroRecord) {
                    if (eval('dummyData.parentRecord')) type += '.parentRecord';
                }
                else {
                    if ((source && source != null) && eval('this.' + source + '.parentRecord') != null && eval('this.' + source + '.parentRecord')) { type += '.parentRecord' }
                    else source = null
                }
            }
            //sif(v) v = v.toLocaleLowerCase();
            let isParcelType = false;
            if (v.contains('parcel.')) {
                tableName = null;
                v = v.replace('parcel.', '');
                source = null;
                isParcelType = true;
            }

            if (v.toLowerCase() == 'ccma.currentyear' || v == 'CurrentYear' || v == 'Current_Year') {
                var regex = new RegExp(v, "g");
                testExp = testExp.replace(regex, ccma.CurrentYear);
                if (v.toLowerCase() == 'ccma.currentyear') {
                    expression = expression.replace(/ccma.currentyear/gi, 'ccma.CurrentYear')
                    orgExpression = orgExpression.replace(/ccma.currentyear/gi, 'ccma.CurrentYear')
                }
                else {
                    expression = expression.replace(/CurrentYear/g, 'ccma.CurrentYear').replace(/Current_Year/g, 'ccma.CurrentYear');
                    orgExpression = orgExpression.replace(/CurrentYear/g, 'ccma.CurrentYear').replace(/Current_Year/g, 'ccma.CurrentYear');
                }
                continue;
            }
            else if (v == 'Current_User' || v == 'CurrentUser' || v == 'CURRENT_USER') {
                var regex = new RegExp(v, "g");
                testExp = testExp.replace(regex, "'" + ccma.CurrentUser + "'")
                expression = expression.replace(/CurrentUser/g, 'ccma.CurrentUser').replace(/Current_User/g, 'ccma.CurrentUser').replace(/CURRENT_USER/g, 'ccma.CurrentUser');
                orgExpression = orgExpression.replace(/CurrentUser/g, 'ccma.CurrentUser').replace(/Current_User/g, 'ccma.CurrentUser').replace(/CURRENT_USER/g, 'ccma.CurrentUser');
                continue;
            }
            else if (v == 'Current_Date' || v == 'CurrentDate') {
                var regex = new RegExp(v, "g");
                testExp = testExp.replace(regex, "'" + ccma.CurrentDate + "'")
                expression = expression.replace(/CurrentDate/g, 'ccma.CurrentDate').replace(/Current_Date/g, 'ccma.CurrentDate');
                orgExpression = orgExpression.replace(/CurrentDate/g, 'ccma.CurrentDate').replace(/Current_Date/g, 'ccma.CurrentDate');
                continue;
            }
            else if (v == 'Today') {
                var regex = new RegExp(v, "g");
                testExp = testExp.replace(regex, "'" + ccma.Today + "'")
                expression = expression.replace(/Today/g, 'ccma.Today');
                orgExpression = orgExpression.replace(/Today/g, 'ccma.Today');
                continue;
            }
            else if (v == 'Tomorrow') {
                var regex = new RegExp(v, "g");
                testExp = testExp.replace(regex, "'" + ccma.Tomorrow + "'")
                expression = expression.replace(/Tomorrow/g, 'ccma.Tomorrow');
                orgExpression = orgExpression.replace(/Tomorrow/g, 'ccma.Tomorrow');
                continue;
            }
            else if (v == 'CurrentDay' || v == 'Current_Day') {
                var regex = new RegExp(v, "g");
                testExp = testExp.replace(regex, "'" + ccma.CurrentDay + "'")
                expression = expression.replace(/CurrentDay/g, 'ccma.CurrentDay').replace(/Current_Day/g, 'ccma.CurrentDay');
                orgExpression = orgExpression.replace(/CurrentDay/g, 'ccma.CurrentDay').replace(/Current_Day/g, 'ccma.CurrentDay');
                continue;
            }
            else if (v == 'Current_Month' || v == 'CurrentMonth') {
                var regex = new RegExp(v, "g");
                testExp = testExp.replace(regex, "'" + ccma.CurrentMonth + "'")
                expression = expression.replace(/CurrentMonth/g, 'ccma.CurrentMonth').replace(/Current_Month/g, 'ccma.CurrentMonth');
                orgExpression = orgExpression.replace(/CurrentMonth/g, 'ccma.CurrentMonth').replace(/Current_Month/g, 'ccma.CurrentMonth');
                continue;
            }
            else if (v == 'FutureYearStatus') {
                var regex = new RegExp(v, "g");
                testExp = testExp.replace(regex, "'" + ccma.ShowFutureYearStatus + "'")
                expression = expression.replace(/FutureYearStatus/g, 'ccma.FutureYearStatus');
                continue;
            }

            if (v && v.contains('.')) {
                split = v.split('.'); tbName = split[0]; fName = split[1];

                if (tableListGlobal.indexOf(tbName) > -1 && ['any', 'all', 'max', 'min', 'count', 'sum', 'avg', 'first', 'last'].indexOf(split[split.length - 1]) > -1) {
                    type += '.' + tbName;
                    v = v.replace(tbName + '.', '');
                    type += '.map(function(i){ return i.' + fName + '})';
                    v = v.replace(fName + '.', '');
                }
            }

            var tf = getDataField(v, tableName);
            if (v == 'null') {
                break;
            }

            if (isZeroRecord && !isParcelType) {
                result.Value = eval('dummyData' + type + "['" + v + "']");
            }
            else {
                result.Value = eval('this' + (source ? '.' + source : '') + type + "['" + v + "']");
            }

            if (result.Value != null && typeof result.Value == "object")
                return ccma.Data.Evaluable.__getEvalResult(null, "***", "***");

            if (result.Value === undefined && !type.contains('map')) {
                //console.warn('Undefined property found while evaluating expression - ' + v)
                if (activeParcel && !isZeroRecord) {
                    if (source) {
                        result.Value = (/\.IN\./g.test(v)) ? isSiblingsTrue(source, v) : activeParcel[actualTable][source.substring(source.indexOf('[') + 1, source.length - 1)][input];
                        return result;
                    }
                    else result.Value = '';
                }
                else {
                    return ccma.Data.Evaluable.__getEvalResult(null, "***", "***");
                }
            }

            var dt, aggfld = false;
            var agUnique = aggregate_fields.filter(function (agr) { return v == agr.UniqueName })[0];
            if (tf) {
                dt = ccma.Data.Types[parseInt(tf.InputType)];
            }
            else if (v.startsWith('SUM') || v.startsWith('AVG') || v.startsWith('COUNT') || v.startsWith('MAX') || v.startsWith('MIN') || v.startsWith('FIRST') || v.startsWith('LAST')) {
                dt = getDatatypeOfAggregateFields(v);
                var aggrfield = true; aggfld = true;
                //dt = ccma.Data.Types[2];
            }
            else if (agUnique?.FunctionName && agUnique?.TableName && agUnique?.FieldName) {
                dt = getDatatypeOfAggregateFields(agUnique.FunctionName + '_' + agUnique.TableName + '_' + agUnique.FieldName); aggfld = true;
            }
            else {
                dt = ccma.Data.Types[1];
            }

            if (!fdt) fdt = dt;
            result.Value = getLargeDecToRound(result.Value, dt);

            if ((dt.jsType != 'Number' && dt.jsType != 'String' && dt.jsType != 'Date') || (v == expression)) {
                if (isZeroRecord && !isParcelType) {
                    result.Original = eval('dummyData' + '.Original' + "['" + v + "']");
                }
                else {
                    result.Original = eval('this' + (source ? '.' + source : '') + '.Original' + "['" + v + "']");
                }

                if (field) {
                    var NumericScale = field.NumericScale;
                    var NumPrecision = field.NumericPrecision;
                    var NumericTextValue = result.Value ? result.Value.toString().replace('.', '').length : 0;
                    var NumericTextOrgValue = result.Original ? result.Original.toString().replace('.', '').length : 0;
                    NumericScale = parseInt(NumericScale);
                    NumPrecision = parseInt(NumPrecision);

                    if (dt.jsType == 'Number' && result.Value && isNaN(result.Value) == false && result.Value.toString().indexOf('.') > -1 && NumPrecision && NumericTextValue > NumPrecision) {
                        result.Value = result.Value.toString().substring(0, NumPrecision + 1);
                    }
                    if (dt.jsType == 'Number' && result.Original && isNaN(result.Original) == false && result.Original.toString().indexOf('.') > -1 && NumPrecision && NumericTextOrgValue > NumPrecision) {
                        result.Original = result.Original.toString().substring(0, NumPrecision + 1);
                    }
                    if (result.Value && NumericScale && result.Value.toString().indexOf('.') > -1 && dt.jsType == 'Number') {
                        if (NumericScale == 0)
                            result.Value = result.Value.toString().substring(0, result.Value.toString().indexOf('.'))
                        else
                            result.Value = result.Value.toString().substring(0, result.Value.toString().indexOf('.') + NumericScale + 1)
                    }
                    if (result.Original && NumericScale && result.Original.toString().indexOf('.') > -1 && dt.jsType == 'Number') {
                        if (NumericScale == 0)
                            result.Original = result.Original.toString().substring(0, result.Original.toString().indexOf('.'))
                        else
                            result.Original = result.Original.toString().substring(0, result.Original.toString().indexOf('.') + NumericScale + 1)
                    }
                }
                if (result.Value && result.Value.trim && result.Value.trim().length > 0 && (fdt ? !(fdt.baseType == 'text') : true))
                    result.Value = result.Value.trim();
                if (result.Original && result.Original.trim && result.Original.trim().length > 0 && (fdt ? !(fdt.baseType == 'text') : true))
                    result.Original = result.Original.trim();

                if (dt.dataType == "Lookup" && typeof result.Value == "number") {
                    result.Value = parseFloat(result.Value);
                    result.Original = parseFloat(result.Original);
                }

                var displayOptions = {};
                if (field && field.LookupQueryFilter && activeParcel) {
                    displayOptions.referenceFilter = field.LookupQueryFilter;
                    displayOptions.referenceObject = eval('activeParcel.' + source);
                }

                let rv = result.Value, rov = result.Original;
                if (field && field.Id && field.UIProperties && field.UIProperties != null) {
                    rv = customFormatValue(rv, field.Id);
                    rov = customFormatValue(rov, field.Id);
                }

                result.DisplayValue = (dt.dataSource == 'ccma.YesNo') ? evalYesNo(rv) : (dt.dataSource == "field.LookupTable") && (!aggrfield) ? evalLookup(v, rv, null, field, null, displayOptions) : (rv != null && rv !== '') ? rv.format((outType || dt).formatter) : '';
                result.DisplayOriginal = (dt.dataSource == 'ccma.YesNo') ? evalYesNo(rov) : (dt.dataSource == "field.LookupTable") && (!aggrfield) ? evalLookup(v, rov, null, field, null, displayOptions) : (rov != null && rov !== '') ? rov.format((outType || dt).formatter) : '&lt;blank&gt;';
                //result.ChangedValue = (result.DisplayOriginal == result.DisplayValue || typeof (result.DisplayValue) != "string" || (field && (field.ReadOnly == "true" || field.ReadOnly == true)) || (tf && tf.Category && (tf.Category.IsReadOnly == true || tf.Category.IsReadOnly == "true"))) ? result.DisplayValue : (result.DisplayValue ? (result.DisplayValue.substr(0, 10) == "data:image" ? result.DisplayValue : '<span class="EditedValue">' + result.DisplayValue + '</span>') : '');
                result.ChangedValue = (result.DisplayOriginal == result.DisplayValue || typeof (result.DisplayValue) != "string") ? result.DisplayValue : (result.DisplayValue ? (result.DisplayValue.substr(0, 10) == "data:image" ? result.DisplayValue : '<span class="EditedValue">' + result.DisplayValue + '</span>') : '');
                return result;
            }

            var valueEx = '';
            if (type.contains('map')) {
                valueEx = isZeroRecord && !isParcelType ? ('dummyData' + type + "." + v) : ('thisACTIVEDATASOURCE' + (source ? '.' + source : '') + type + "." + v + "");
            }
            else {
                valueEx = isZeroRecord && !isParcelType ? ('dummyData' + type + "['" + v + "']") : 'thisACTIVEDATASOURCE' + (source ? '.' + source : '') + type + "['" + v + "']";
            }

            if (dt.jsType == 'Number' || (calcField && dt.jsType == 'Number')) {
                if (sumField && aggfld)
                    var tex = 'safeParseFloat(' + valueEx + ',true, true,' + dvRules + ')';
                else
                    var tex = 'safeParseFloat(' + valueEx + ',true,' + calcField + ',' + dvRules + ')';
            }
            else if ((dt.jsType == 'String' || dt.jsType == 'Date') && !calcField && !type.contains('map'))
                var tex = 'safeParseString(' + valueEx + ')';

            else {
                var tex = valueEx;
            }

            var rr = new RegExp("(?=\\b)(" + fieldExp + ")(?=\\b)", "g");
            if (fieldExp.indexOf('.') == -1) expression = expression.replace(/\./g, '###_');
            expression = expression.replace(rr, tex);
            expression = expression.replace(/###_/g, '.');
            if (fieldExp.indexOf('.') == -1) testExp = testExp.replace(/\./g, '###_');
            testExp = testExp.replace(rr, '');
            testExp = testExp.replace(/###_/g, '.');
            if (testExp.indexOf('"') > -1) {
                var k = testExp.substring(testExp.indexOf('"') + 1, testExp.length);
                testExp = k.substring(k.indexOf('"') + 1, k.length);
            }
            orgExpression = expression.contains('return i.') ? expression.replace(/return i./g, 'return i.Original.') : (expression.contains('parentRecord') ? expression.slice(0, expression.lastIndexOf('parentRecord')) + expression.slice(expression.lastIndexOf('parentRecord')).replace('parentRecord', 'parentRecord.Original') : expression.replace(/ACTIVEDATASOURCE/g, '.Original'));
        }
        var value, original;

        try {
            (fdt) ? ((fdt.jsType == 'String') || (fdt.jsType == 'Number') ? expression = replaceLikeOperators(expression, fdt.jsType) : '') : '';
            (fdt) ? ((fdt.jsType == 'String') || (fdt.jsType == 'Number') ? orgExpression = replaceLikeOperators(orgExpression, fdt.jsType) : '') : '';
            value = eval(expression.replace(/ACTIVEDATASOURCE/g, ''));
            original = eval(orgExpression.replace(/ACTIVEDATASOURCE/g, ''));
        } catch (e) {
            console.warn('Error while evaluating ' + expression, e);
            if (expression.match(/.*?lookup\[.*?\].*?/)) {
                return ccma.Data.Evaluable.__getEvalResult(null, "-", "-");
            }
            else {
                return ccma.Data.Evaluable.__getEvalResult(null, "****", "****");
            }
        }
    }
    

	fdt = (calcField && field)? ccma.Data.Types[field.InputType]: fdt;
    fdt = value === true || value === false ? null : fdt
    outType = outType || fdt;
    if(field && (field.InputType == '8' || field.InputType == '10'))
		value ? value = Math.round(value) : value;
    return ccma.Data.Evaluable.__getEvalResult(outType, value, original, field);
};

ccma.Data.Evaluable.__getEvalResult = function (type, value, original, field) {
    if (value && value.trim && value.trim().length > 0)
        value = value.trim();
    if (original && original.trim && original.trim().length > 0)
        original = original.trim();
    if (!type)
        type = ccma.Data.Types[1];

    let rv = value, rov = original;
    if (field && field.Id && field.UIProperties && field.UIProperties != null) {
        rv = customFormatValue(rv, field.Id);
        rov = customFormatValue(rov, field.Id);
    }

    var DisplayValue = (rv || rv === 0) ? rv.format(type.formatter) : rv == 0 ? 0 : '';
    var DisplayOriginal = (rov || rov === 0) ? rov.format(type.formatter) : rov == 0 ? 0 : '&lt;blank&gt;';
    var result = {
        Value: value,
        DisplayValue: DisplayValue,
        Original: DisplayOriginal,
        DisplayOriginal: original != null ? original.format(type.formatter) : '&lt;blank&gt;',
        ChangedValue: (DisplayOriginal === DisplayValue || typeof (DisplayValue) != "string") ? DisplayValue : (DisplayValue ? (DisplayValue.substr(0, 10) == "data:image" ? DisplayValue : '<span class="EditedValue">' + DisplayValue + '</span>') : ''),
        toString: function () { return this.Value; }
    };

    if (field) {
        var NumericScale = field.NumericScale;
        var NumPrecision = field.NumericPrecision;
        var NumericTextValue = result.Value ? result.Value.toString().replace('.','').length : 0;
        var NumericTextOrgValue = result.Original ? result.Original.toString().replace('.','').length : 0;
        NumericScale = parseInt(NumericScale);
        NumPrecision = parseInt(NumPrecision);

        result.Value = getLargeDecToRound(result.Value, type);

        if (type.jsType == 'Number' && result.Value && isNaN( result.Value ) == false && result.Value.toString().indexOf( '.' ) > -1 && NumPrecision && NumericTextValue > NumPrecision) {
		    result.Value = result.Value.toString().substring( 0, NumPrecision + 1);
        }

	    if (type.jsType == 'Number' && result.Original && isNaN( result.Original ) == false && result.Original.toString().indexOf( '.' ) > -1 && NumPrecision && NumericTextOrgValue >NumPrecision) {
		    result.Original = result.Original.toString().substring( 0, NumPrecision + 1);
        }

        if (type.jsType == 'Number' && result.Value &&  result.Value.toString().indexOf( '.' ) > -1 &&  (NumericScale || NumericScale == 0)){
            if ( NumericScale == 0 ){
                result.Value = result.Value.toString().substring(0, result.Value.toString().indexOf('.') );
                result.DisplayValue = result.DisplayValue.toString().substring(0, result.DisplayValue.toString().indexOf('.') );
            }else{
                result.Value = result.Value.toString().substring( 0, result.Value.toString().indexOf( '.' ) + NumericScale + 1 );
                result.DisplayValue = result.DisplayValue.toString().substring( 0, result.DisplayValue.toString().indexOf( '.' ) + NumericScale + 1 );
            }
        }

        if (type.jsType == 'Number' && result.Original && result.Original.toString().indexOf( '.' ) > -1 && (NumericScale || NumericScale == 0)) {
            if ( NumericScale == 0 ) {
                result.Original = result.Original.toString().substring(0, result.Original.toString().indexOf('.') );
                result.DisplayOriginal = result.DisplayOriginal.toString().substring(0, result.DisplayOriginal.toString().indexOf('.') );
            }
            else {
                result.Original = result.Original.toString().substring( 0, result.Original.toString().indexOf( '.' ) + NumericScale + 1 );
                result.DisplayOriginal = result.DisplayOriginal.toString().substring( 0, result.DisplayOriginal.toString().indexOf( '.' ) + NumericScale + 1 );
		    }                    
        }
    }      
    return result;
}


ccma.Data.Evaluable.OrigEval = function (source, expression) {
    var result = this.__eval(source, expression);
    return result.Original;
};

ccma.Data.Evaluable.EvalValue = function (source, expression, datatype, parentSource, dvRules, isZeroRecord) {
    var result = this.__eval(source, expression, null, parentSource, dvRules, isZeroRecord);
    return result.Value;
};

ccma.Data.Evaluable.Test = function (source, expression) {
    if (expression === undefined && (!!source)) {
        expression = source;
        source = null;
    }
    return this.__eval(source, expression, 3).Value;
}


ccma.Data.Evaluable.EvalText = function (source, expression, datatype, showChanges) {
    if (showChanges == null)
        showChanges = true;
    var result = this.__eval(source, expression);
    if (showChanges)
        return result.ChangedValue;
    else
        return result.DisplayValue;
};

ccma.Data.Evaluable.Eval = function (source, expression) {
    return this.__eval(source, expression);
};

ccma.Data.Evaluable.Lookup = function (field, rvalue) {
    if (rvalue == null)
        rvalue = "n";
    var value = "";
    value = eval("this." + field);
    var lv;
    try {
        lv = getFieldLookup(field)[value];
    } catch (e) {
        console.warn("Error while accessing lookup table: ", field, value, getFieldLookup(field), true);
        return "ERR";
    }
    if (value == '') {
        return value;
    }
    if (lv === undefined) {
        //console.warn('No lookup available for ' + value + ' in ' + field);
        return "-"; //NULL Value Lookup
        //return value + " (N/L)";
    }
    switch (rvalue.toLowerCase()) {
        case "n":
        case "name":
            return lv.Name;
            break;
        case "d":
        case "desc":
        case "description":
            return lv.Description;
            break;
        default:
            return lv.Name;
            break;
    }
}

ccma.Data.Evaluable.setLookupList = function (select, value) {

    try {
        for (var i = 0; i < select.options.length; i++) {
            o = select.options[i];
            o.selected = false;
            if (value.indexOf(o.value) != -1) {
                o.selected = true;
            }
        }
    }
    catch (e) {
        console.error('error in object.js/ccma.Data.Evaluable.setLookupList :' + e.message);
    }
}
ccma.Data.Evaluable.sortArray = function (Array, prop, asc) {
    var k = [];
    k = Array.sort(function (a, b) {
        if (asc) return (a[prop] > b[prop]) ? 1 : ((a[prop] < b[prop]) ? -1 : 0);
        else return (b[prop] > a[prop]) ? 1 : ((b[prop] < a[prop]) ? -1 : 0);
    });
    // k.forEach(function (e) { console.log(e[prop]); });
    return k;
}
ccma.Data.Evaluable.EvalObjectPRC = function (object, expression, f,contextName) {
    if (expression == "")
        return "";
    var aggreField = false;
    var originalExp = expression.toString();
    var tblname = contextName ? contextName : object.DataSourceName;
    var field = getDataField(expression, tblname), calcFieldvalues;
    if (field && field.CalculationExpression && field.CalculationExpression.trim() != '' && field.CalculationExpression.search(childRecCalcPattern) > -1) {
         var tempVal = calculateChildRecords(field, null, object);
             calcFieldvalues = tempVal;
    }
        	   
    var result = ccma.Data.Evaluable.__getEvalResult();
    if (expression.startsWith('SUM_') || expression.startsWith('AVG_') || expression.startsWith('COUNT_') || expression.startsWith('MAX_') || expression.startsWith('MIN_') || expression.startsWith('FIRST_') || expression.startsWith('LAST_') || expression.startsWith('MEDIAN_')){
		aggreField = true;
		var tempObject = _.clone(object);
		if(activeParcel)
			object = activeParcel;
	}
    var Value = eval('object.' + expression);
    var orgValue = object["Original"] ? eval('object.Original.' + expression) : null;
    if (Value && Value != undefined && object["Original"]) {
        result.Value = Value;
        if (f != null) { result.Value = eval(f + '(result.Value);'); }
        result.Original = orgValue;
        if (f != null) { result.Original = eval(f + '(orgValue);'); }

        if (field) {
            let ndt = ccma.Data.Types[parseInt(field.InputType)]; result.Value = getLargeDecToRound(result.Value, ndt);
        }

        if (field && field.Id && field.UIProperties && field.UIProperties != null) {
            result.Value = customFormatValue(result.Value, field.Id);
            result.Original = customFormatValue(result.Original, field.Id);
        }

        //result.ChangedValue = ( result.Value == result.Original || typeof (result.Value) != "string" || ( field && (field.ReadOnly=="true"|| field.ReadOnly == true) ) ) ? result.Value : ( result.Value ? ( result.Value.substr(0, 10) == "data:image" ? result.Value : '<span class="EditedValue">' + result.Value + '</span>' ) : '' );
        //result.ChangedValue = (result.Value == result.Original || (field && (field.ReadOnly == "true" || field.ReadOnly == true))) ? result.Value : (result.Value ? ((typeof (result.Value) == "string" && result.Value.substr(0, 10) == "data:image") ? result.Value : '<span class="EditedValue">' + result.Value + '</span>') : '');
        result.ChangedValue = (result.Value == result.Original) ? result.Value : (result.Value ? ((typeof (result.Value) == "string" && result.Value.substr(0, 10) == "data:image") ? result.Value : '<span class="EditedValue">' + result.Value + '</span>') : '');
    }
    else {
        if (field && field.Id && field.UIProperties && field.UIProperties != null) {
            Value = customFormatValue(Value, field.Id);
            orgValue = customFormatValue(orgValue, field.Id);
        }
        result.ChangedValue = Value; if (f != null) { result.ChangedValue = eval(f + '(Value);'); }
    }
    if (result.ChangedValue == undefined) { result.ChangedValue = ""; }
    if(aggreField) { object = tempObject; }
    
    if(calcFieldvalues && calcFieldvalues.ChangedValue && calcFieldvalues.ChangedValue != NaN && calcFieldvalues.ChangedValue != "" && calcFieldvalues.ChangedValue != 0){
    result.ChangedValue = calcFieldvalues.ChangedValue;
    }
     
    return result.ChangedValue;
}

ccma.Data.Evaluable.EvalObject = function (object, expression, tableName, showDescription) {
    var originalExp = expression.toString();
    var field = getDataField(expression, tableName);
    if (field && field.InputType != "5") if ((field.CalculationExpression != null) && (field.CalculationExpression != "")) {
        if ((field.CalculationOverrideExpression != null) && (field.CalculationOverrideExpression != "")) {
            if (!activeParcel.EvalObject(object, decodeHTML(field.CalculationOverrideExpression), tableName).Value && field.CalculationExpression.search(childRecCalcPattern) == -1 && field.CalculationExpression.search(/parent/i) == -1)
                expression = field.CalculationExpression;
        } else if (field.CalculationExpression.search(childRecCalcPattern) == -1 && field.CalculationExpression.search(/parent/i) == -1) 
        	expression = field.CalculationExpression;
    }
    var fdt;
    var testExp = expression.toString();

    var result = ccma.Data.Evaluable.__getEvalResult();

    var obj = object

    while ((match = /.*?([a-zA-Z][a-zA-Z0-9_]+).*?/i.exec(testExp)) != null) {
        var objectvalue = false;
        var v = match[1];
        var tf = getDataField(v, tableName);
        var tfCat = tf && tf.CategoryId ? getCategory(tf.CategoryId):null; 

        result.Value = eval('obj.' + v);
        // if (result.Value && typeof result.Value == 'object') { result.Value = result.Value.Value; objectvalue = true; }
        if (result.Value === undefined) {
            //console.warn('Undefined property found while evaluating expression - ' + v)
            return ccma.Data.Evaluable.__getEvalResult(null, "***", "***");
        }

        var dt;
        if (tf) {
            dt = ccma.Data.Types[parseInt(tf.InputType)];
        } else {
            dt = ccma.Data.Types[1];
        }

        if (!fdt)
            fdt = dt;
        if ((dt.jsType != 'Number' && dt.jsType != 'String') || (v == expression)) {
            result.Original = obj["Original"] ? eval('obj.Original.' + v) : null;
            //   if (result.Original && typeof result.Original == 'object') result.Original = result.Original.Original;

            result.Value = getLargeDecToRound(result.Value, dt);

            var displayOptions = {};
            if (field && field.LookupQueryFilter){
                displayOptions.referenceFilter = field.LookupQueryFilter;
                displayOptions.referenceObject = obj;
            }

            let rv = result.Value, rov = result.Original;
            if (field && field.Id && field.UIProperties && field.UIProperties != null) {
                rv = customFormatValue(rv, field.Id);
                rov = customFormatValue(rov, field.Id);
            }

            result.DisplayValue = getDisplayValue(v, rv, dt, field, null, showDescription, displayOptions ); // (dt.dataSource == 'ccma.YesNo') ? evalYesNo(result.Value) : (dt.dataSource == "field.LookupTable") ? evalLookup(v, result.Value, null, field) : result.Value ? result.Value.format(dt.formatter) : '';
            result.DisplayOriginal = getDisplayValue(v, rov, dt, field, null, showDescription, displayOptions );  //(dt.dataSource == 'ccma.YesNo') ? evalYesNo(result.Original) : (dt.dataSource == "field.LookupTable") ? evalLookup(v, result.Original, null, field) : result.Original ? result.Original.format(dt.formatter) : '&lt;blank&gt;';
            if (result.DisplayValue) {
                  var copyvalue = (result.DisplayValue).toString();
                  copyvalue = copyvalue.slice(0, (copyvalue.indexOf("."))+3);
            } 
            var roundValue = (result.DisplayValue && (field && field.InputType == 2) && result.DisplayValue.contains('.')) ? copyvalue : result.DisplayValue;     
            //result.ChangedValue = (result.DisplayOriginal == result.DisplayValue || typeof (result.DisplayValue) != "string" || (field && (field.ReadOnly == "true" || field.ReadOnly == true)) || (tfCat && (tfCat.IsReadOnly == true || tfCat.IsReadOnly == "true"))) ? roundValue : (result.DisplayValue ? (result.DisplayValue.substr(0, 10) == "data:image" ? result.DisplayValue : '<span class="EditedValue">' + roundValue + '</span>') : '');
            result.ChangedValue = (result.DisplayOriginal == result.DisplayValue || typeof (result.DisplayValue) != "string") ? roundValue : (result.DisplayValue ? (result.DisplayValue.substr(0, 10) == "data:image" ? result.DisplayValue : '<span class="EditedValue">' + roundValue + '</span>') : '');
            return result;
        }
        var ex = objectvalue == true ? '.Value' : '';
        var valueEx = 'obj.' + v + ex;
        if (dt.jsType == 'Number') {
            var tex = 'safeParseFloat(' + valueEx + ')';
        }
        else if (dt.jsType == 'String') {
            var tex = 'safeParseString(' + valueEx + ')';
        }
        else {
            var tex = valueEx;
        }
        expression = expression.replace(new RegExp(v, "g"), tex);
        testExp = testExp.replace(new RegExp(v, "g"), '');
        if (testExp.indexOf('"') > -1) {
            var k = testExp.substring(testExp.indexOf('"') + 1, testExp.length);
            testExp = k.substring(k.indexOf('"') + 1, k.length);
        }
    }

    var value, original;
    try {
        (fdt) ? ((fdt.jsType == 'String') || (fdt.jsType == 'Number') ? expression = replaceLikeOperators(expression, fdt.jsType) : '') : '';
        value = eval(expression);
        original = eval(expression);
    } catch (e) {
        console.warn('Error while evaluating ' + expression, e);
        if (expression.match(/.*?lookup\[.*?\].*?/)) {
            return ccma.Data.Evaluable.__getEvalResult(null, "-", "-");
        }
        else {
            return ccma.Data.Evaluable.__getEvalResult(null, "****", "****");
        }
    }

    return ccma.Data.Evaluable.__getEvalResult(fdt, value, original, field);
};

function getDisplayValue(fieldName, value, type, field, blank, getDescriptionForLookup, options) {
    if (!blank) blank = '';
    var res = ( type.dataSource == 'ccma.YesNo' ) ? evalYesNo( value == null ? false : value ) : ( type.dataSource == "field.LookupTable" ) ? evalLookup( fieldName, value, getDescriptionForLookup, field, null, options ) : ( (value != "" || value  === 0) && value != null ) ? value.format( type.formatter ) : blank;
    var args = res != null ? res.toString().split('.') : null;
    if (args != null && args[1] == "00") {
        return args[0];
    }
    return res;
}

function getLargeDecToRound(val, dt) {
    if ((dt.jsType == 'Number' || dt.dataSource == "field.LookupTable") && !isNaN(val) && val?.toString().split('.')[1]?.length > 12) {
        let decPart = val?.toString().split('.')[1];
        if (decPart?.substr(-5).slice(0, -1) == '9999') {
            val = parseFloat(val);
            if (dt.jsType == 'Number') { val = val ? parseFloat(val.toFixed(10)) : val; }
            else { val = val ? parseFloat(val.toFixed(2)) : val; }
        }
    }
    return val;
}