﻿var window = self;
var timer;
onmessage = function (e) {
	var message = e.data['message'];
	var ajaxRequest_worker = function (request) {
		var url = request.url;
		var data = request.data;
		var callback = request.success;
		var errorCallback = request.error;
		var type = request.type;
		var data_array, data_string="", idx, req, value;
		if (!navigator.onLine) { if (errorCallback) errorCallback({}); else callback({}); return; }
	  	if (data == null)
	    	data = {};
	  	if (callback == null)
	    	callback = function() {};
	  	if (type == null) //default to a GET request
	    	type = 'POST';
	  	for(x in data)
	  		data_string += (x + '=' + data[x] + '&')
	  	data_string = data_string.length > 0 ? data_string.substring(0, data_string.length - 1): data_string;
	  	req = new XMLHttpRequest();
	  	req.open(type, url, false);
	  	req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	  	req.onreadystatechange = function () {
	  	    var res;
	  	    try{
	  	        res = JSON.parse( req.responseText );
	  	    } catch(ex){
	  	        res = JSON.parse( '{"res": "' + req.responseText + '"}');
	  	    }
	    	if (req.readyState === 4 && req.status === 200 && !res.failed) return callback(res);
	  		else return errorCallback(res);
	  	};
	  	req.send(data_string);
	  	return req;
	};
	var lastTicks = (new Date()).getTime();
	function ticks() {
	    var t = (new Date()).getTime();
	    while (t == lastTicks) {
	        t = (new Date()).getTime();
	    }
	    lastTicks = t;
	    return t;
	}
	
	if (timer) clearTimeout(timer);
	timer = self.setTimeout(function(){
		ajaxRequest_worker({
	    	url: '/mobileassessor/geterrorlog.jrq?zt=' + ticks(),
	    	dataType: 'json',
	    	type: "POST",
	    	data: { type: 'eval' },
	    	success: function(data) { 
	    		if (data.length > 0)  postMessage('evaluate-' + JSON.stringify(data));
	    		else postMessage('recheck')
    		},
	    	error: function(e) { console.log(e); postMessage('recheck'); }
		})
	}, 10000);
	
	if (message != 'check')
		ajaxRequest_worker({
	    	url: '/mobileassessor/recorderrorlog.jrq?zt=' + ticks(),
	    	dataType: 'json',
	    	type: "POST",
	    	data: { message: message },
	    	success: function() {},
	    	error: function(e) { console.log(e) }
		})
}