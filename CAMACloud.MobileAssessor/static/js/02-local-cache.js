﻿//    DATA CLOUD SOLUTIONS, LLC ("COMPANY") CONFIDENTIAL 
//    Unpublished Copyright (c) 2010-2013 
//    DATA CLOUD SOLUTIONS, an Ohio Limited Liability Company, All Rights Reserved.

//    NOTICE: All information contained herein is, and remains the property of COMPANY.
//    The intellectual and technical concepts contained herein are proprietary to COMPANY
//    and may be covered by U.S. and Foreign Patents, patents in process, and are protected
//    by trade secret or copyright law. Dissemination of this information or reproduction
//    of this material is strictly forbidden unless prior written permission is obtained
//    from COMPANY. Access to the source code contained herein is hereby forbidden to
//    anyone except current COMPANY employees, managers or contractors who have executed
//    Confidentiality and Non-disclosure agreements explicitly covering such access.</p>

//    The copyright notice above does not evidence any actual or intended publication
//    or disclosure of this source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
//    DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
//    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
//    IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
//    OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
//    TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
//    ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.


var appUpdating = false;
var firstTimeCached = false;

function initApplicationCache(callback) {

    var updateMessage = 'Updating application ...';
    if (window.applicationCache && false) {
        if (window.applicationCache.status == 0) {
            updateMessage = 'Installing application ...';
        }
        applicationCache.addEventListener( 'updateready', function (){
        	var currentVersion= localStorage.getItem( "Cache-Manifest-Version");
           	hideCacheProgress();
               //  if ( v == currentVersion ) { window.location.reload(); return;}
            if (window.location.hash != '#repairing') {
                messageBox('An updated version of CAMA Cloud is now available. Update now?', ["OK", "Cancel"], function () {
                    window.location.reload();
                }, function () {
                    appUpdating = false;
                    hideSplash(); 
                });
            }
        })
   
     	applicationCache.addEventListener( 'cached', function (){
            hideCacheProgress();
            if (window.location.hash != '#repairing') {
                messageBox('You have successfully completed installation of MobileAssessor. The application will restart now.', function () {
                    window.location.reload();
                });
            }
        });

        applicationCache.addEventListener('error', function (e) {
            hideCacheProgress();
            console.log('Error in application cache ', e);
        });

        applicationCache.addEventListener('noupdate', function (e) {
            hideCacheProgress();
            log('Application Cache: No updates'); 
        });

        applicationCache.addEventListener('progress', function (e, f, g) {
            appUpdating = true;
            hideKeyboard(true);
            //log('Updating Cache: ' + parseInt(e.loaded / e.total * 100) + '% completed');
            showSplash();
            //showCacheProgress(e.loaded / e.total);
            setSplashProgress(updateMessage, (e.loaded / e.total * 100));
        });
    }

    if (navigator.onLine && typeof (caches) != 'undefined' && macacheVersion != '' && (!localStorage.macacheVersion || localStorage.macacheVersion != 'MA-' + macacheVersion || window.location.hash == '#repairing')) {
        storeFilesInCache(macacheVersion, () => {
            if (callback) callback();
        });
    }
    else if (navigator.onLine && typeof (caches) != 'undefined') {
        $$$('getcacheversion', {}, (res) => {
            if (localStorage.macacheVersion != 'MA-' + res) {
                messageBox('A settings update is available.', ["OK", "Cancel"], () => {
                    $('.splash-progress-window').hide(); $('.splash-progress-continuous').show();
                    window.location.reload();
                }, () => {
                    appUpdating = false; if (callback) callback();
                });
            }
            else if (callback) {
                checkCacheExist(() => {
                    appUpdating = false; callback();
                });
            }
        }, (resp) => {
            console.log(resp); appUpdating = false; if (callback) callback();
        });      
    }
    else {
        hideCacheProgress();
        appUpdating = false;
        if (callback) callback(); 
    }
}

var checkCacheExist = (callback) => { // checking the files cached if not cached again
    let cacheInStorage = () => {
        setSplashProgress('Please wait. Caching in progress ...', 2);
        $$$('getcachefiles', {}, (res) => {
            setSplashProgress('Please wait. Caching in progress ...', 5);
            let curls = JSON.parse(res.CacheFiles);
            caches.open(localStorage.macacheVersion).then((cache) => {
                return cache.addAll(curls);
            }).then(() => {
                caches.keys().then(key_list => {
                    return Promise.all(key_list.map(key => {
                        if (localStorage.macacheVersion.indexOf(key) === -1)
                            return caches.delete(key);
                    }));
                }).then(() => {
                    if (callback) callback();
                });
            }).catch((error) => {
                if (callback) callback();
            });
        }, (resp) => {
            console.log(resp); if (callback) callback();
        });
    }

    caches.keys().then((cacheNames) => {
        if (cacheNames.length > 0) {
            caches.open(localStorage.macacheVersion).then((cache) => {
                cache.keys().then((keys) => {
                    if (keys.length > 0) { if (callback) callback(); }
                    else {  cacheInStorage(); }
                });
            });
        }
        else { cacheInStorage(); }
    }).catch(() => {
        if (callback) callback();
    });
}



function storeFilesInCache(macacheVersion, callback) {
    let updateMessage = 'Updating application ...';
    messageBox('An updated version of CAMA Cloud is now available. Update now?', ["OK", "Cancel"], () => {
        appUpdating = true; hideKeyboard(true);
        let cacheName = localStorage.macacheVersion = 'MA-' + macacheVersion;
        showSplash(); setSplashProgress(updateMessage, 10);
        $$$('getcachefiles', {}, (res) => {
            let curls = JSON.parse(res.CacheFiles); setSplashProgress('Please wait. Caching in progress ...', 30);
            setTimeout(() => { setSplashProgress('Please wait. Caching in progress ...', 60); }, 1500);
            caches.open(cacheName).then((cache) => {
                return cache.addAll(curls);
            }).then(() => {
                setSplashProgress('Please wait. Caching in progress ...', 80);
                caches.keys().then(key_list => {
                    return Promise.all(key_list.map(key => {
                        if (cacheName.indexOf(key) === -1)
                            return caches.delete(key);
                    }));
                }).then(() => {
                    setSplashProgress('Cached...', 100);
                    messageBox('You have successfully completed installation of MobileAssessor. The application will restart now.', () => {
                        if (iPad && CCWarehouseAPI?.prototype?.ReOpenMA && typeof (CurrentNativeAppCodeVersion) != 'undefined' && CurrentNativeAppCodeVersion > 0) {
                            CCWarehouseAPI.prototype.ReOpenMA();
                        }
                        else {
                            hideCacheProgress(); appUpdating = false; 
                            setSplashProgress('Initializing application ...', 0);
                            if (callback) callback();
                        } 
                    });
                });
            }).catch((error) => {
                appUpdating = false;
                console.error('Error adding resources to the cache:', error);
                if (callback) callback();
            });
        }, (resp) => {
            console.log(resp); appUpdating = false; if (callback) callback();
        });
    }, () => {
        appUpdating = false; if (callback) callback();
    });
}

function getCacheManifestVersion(callBack){ 
    $.get( "offline.appcache", function ( data ){ 
        var lines = data.split( "\n" );
        var version = lines && lines[1] ? lines[1].split( ":" )[1] : "";
        localStorage.setItem( "Cache-Manifest-Version", version.trim() );
        callBack && callBack( version.trim())
    })
}
function hideCacheProgress() {
    $('.cache-update-bar').hide();
    $('.ma-version').show();
    $('.cache-update-progress').css({ 'background-position': '0px 0px' });
}

/*function showCacheProgress(percent) {
    $('.cache-update-bar').show();
    $('.ma-version').hide();
    var pgwidth = parseInt($('.cache-update-progress').width() * percent);
    $('.cache-update-progress').css({ 'background-position': pgwidth + 'px 0px' });
}*/

function loadLookup(callback) {
    log('Caching Lookup Table ...');
    lookup = {};
    ccma.Data.LookupMap = {};
    ccma.Data.LookupTable = {};
    //getData("SELECT CASE WHEN F.Name IS NULL THEN LV.Source ELSE F.Name END As FieldName, LV.Name, LV.Value, LV.Description, LV.Ordinal, F.SortType FROM LookupValue LV LEFT OUTER JOIN Field F ON (F.LookupTable = LV.Source) ORDER BY 1, LV.Ordinal", [], function (lv) {
    getData("SELECT Id, Name, LookupTable  FROM Field WHERE InputType = 5 or InputType = 11", [], function (lmap) {
        for (var i in lmap) {
            ccma.Data.LookupMap[lmap[i].Name] == undefined ? ccma.Data.LookupMap[lmap[i].Name] = { LookupTable: lmap[i].LookupTable, IdMap: false } : ccma.Data.LookupMap[lmap[i].LookupTable] = { LookupTable: lmap[i].LookupTable, IdMap: false } ;
            //ccma.Data.LookupMap[lmap[i].Name] = { LookupTable: lmap[i].LookupTable, IdMap: false };
            ccma.Data.LookupMap['#FIELD-' + lmap[i].Id] = { LookupTable: lmap[i].LookupTable, IdMap: true };
        }
        getData("SELECT * FROM LookupValue", [], function (lv) {
            for (var x in lv) {
                var f = lv[x].Source;
                var v = lv[x].Value.trim().length > 0 ? lv[x].Value.trim() : lv[x].Value;
                var tb = ccma.Data.LookupTable[f];
                var n = lv[x].Name;
                var d = lv[x].Description;
                var o = parseInt(lv[x].Ordinal);
                var t = lv[x].SortType;
                var color = lv[x].Color;
                var NumericValue = lv[x].NumericValue;
                if (lookup[f] === undefined) {
                    lookup[f] = {};
                }
                //                if (typeof v == "number" || !isNaN(v)) {
                //                    v = parseFloat(v).toString();
                //                }
                if (lookup[f][v] === undefined)
                    lookup[f][v] = { Name: n, Description: d, Ordinal: o, SortType: t, Id: v, color: color, NumericValue: NumericValue, AdditionalValue1: lv[x].AdditionalValue1, AdditionalValue2: lv[x].AdditionalValue2 };
            }
            log('Loaded ' + lv.length + ' lookup items in memory ...');
            if (callback) callback();
        });

    });

}

function loadFieldAlertTypes(callback) {
    log('Caching Field Alert Types ...');
    fieldAlerts = [{ Id: 0, Name: 'No Flag' }];
    getData("SELECT * FROM FieldAlerts", [], function (vals) {
        for (var x in vals) {
            var v = vals[x].Id;
            var d = vals[x].Name;
            var temp = { Id: v, Name: d };
            fieldAlerts.push(temp);
        }
        log('Loaded ' + fieldAlerts.length + ' Validations in memory ...');

        if (callback) callback();
    }, null, true, function () {
        if (callback) callback();
    });
}

function loadValidations(callback) {
    log('Caching Validation Table ...');
    ccma.UI.Validations = [];
    getData("SELECT * FROM ClientValidation ORDER BY Ordinal", [], function (vals) {
        for (var x in vals) {
            if (isTrue(vals[x].Enabled)) {
                var v = vals[x].Name;
                var n = decodeHTML(vals[x].Condition);
                var d = vals[x].ErrorMessage.replace( new RegExp( '&gt;', 'g' ), '>' ).replace( new RegExp( '&lt;', 'g' ), '<' );
                var o = vals[x].SourceTable;
                var vfo = vals[x].ValidateFirstOnly;
                var temp = { Name: v, Condition: n, ErrorMessage: d, SourceTable: o, FirstOnly: vfo, IsSoftWarning: (vals[x].IsSoftWarning? vals[x].IsSoftWarning: 'false') };
                ccma.UI.Validations.push(temp);
            }
        }
        log('Loaded ' + ccma.UI.Validations.length + ' Validations in memory ...');

        if (callback) callback();
    }, null, false, function () {
        if (callback) callback();
    });
}

function loadTableKeys(callback) {
    log('Caching Table Keys...');
    tableKeys = [];
    getData("SELECT * FROM TableKeys ", [], function (vals) {
        for (var x in vals) {
            var v = vals[x].Name;
            var n = vals[x].SourceTable;
            var temp = { Name: v, SourceTable: n };
            tableKeys.push(temp);
        }
        log('Loaded ' + tableKeys.length + ' tableKeys in memory ...');

        if (callback) callback();
    }, null, false, function () {
        if (callback) callback();
    });
}

function loadImportSettings(callback) {
    log('Caching Settings Table ...');
    getData("SELECT  Name,Value FROM  ImportSettings", [], function (vals) {
    clientSettings = {};
        for (var x in vals) {
            var n = vals[x].Name;
            var v = vals[x].Value;
            clientSettings[n.toString()] = v;
        }
        log('Loaded ' + clientSettings.length + ' Import Settings in memory ...');
        if (callback) callback();

    }, null, false, function (){
        if ( callback ) callback();
    } );
}

function loadSketchSettings(callback) {
    log('Caching Settings Table ...');
    getData("SELECT  Name,Value FROM  SketchSettings", [], function (vals) {
    sketchSettings = {};
        for (var x in vals) {
            var n = vals[x].Name;
            var v = vals[x].Value;
            sketchSettings[n.toString()] = v;
        }
        log('Loaded ' + sketchSettings.length + ' Sketch Settings in memory ...');
        if (callback) callback();

    }, null, false, function (){
        if ( callback ) callback();
    } );
}

function loadHeatmapfields(callback) {
    log('Caching Heatmapfields Table ...');
    var Heatmapfield = [];
    getData("select * from UI_HeatMap", [], function (vals) {
        for (var x in vals) {
            var n = vals[x].Id;
            var v = vals[x].FieldId;
            var i = vals[x].ComparisonType;
            var ia = vals[x].IsAggregate;
            var temp = { id: n, FieldId: v, ComparisonType: i, IsAggregate :ia}
            Heatmapfield.push(temp)
        }
        Heatmapfields = Heatmapfield;
        log('Loaded ' + Heatmapfield.length + '  Heatmapfields in memory ...');
        if (callback) callback();
    }, null, false, function (){
        if ( callback ) callback();
    } );
}
function loadAggregateFields(callback) {
    var aField = [];
    getData("SELECT * FROM AggregateFieldSettings", [], function (vals) {
        for (var x in vals) {
            var n = vals[x].ROWUID;
            var v = vals[x].FieldName;
            var t = vals[x].TableName;
            var i = vals[x].FunctionName;
            var m = vals[x].IncludeInHeatMap;
            var temp = { ROWUID: n, TableName: t, FieldName: v, FunctionName: i, IncludeInHeatMap: m };
            aField.push(temp);
        }
        agFields = aField;
        log('Loaded ' + agFields.length + '  Heatmapfields in memory ...');
        if (callback) callback();
    });
}
function loadHeatmaplookup(callback) {
    log('Caching Heatmaplookup Table ...');
    var Hmlookup = [];
    getData("select RowUID,HeatMapID,Value1,Value2 ,ColorCode from UI_HeatMapLookup  ", [], function (vals) {
        for (var x in vals) {
            var n = vals[x].RowUID;
            var h = vals[x].HeatMapID;
            var v = vals[x].Value1;
            var i = vals[x].Value2;
            var s = vals[x].ColorCode;
            var temp = { Rowuid: n, HeatMapID: h, Value1: v, Value2: i, Color: s };
            Hmlookup.push(temp)
        }
        Heatmaplookup = Hmlookup;
        log('Loaded ' + Heatmaplookup.length + '  Heatmaplookups in memory ...');
        if (callback) callback();

    });
}
//function loadStreetMapPoints(callback) {
//    StreetMapPoints = {};
//    console.log('Caching StreetMapPoints Table ...');
//    loadExistingStore(function (vals) {
//        StreetMapPoints = vals;
//        console.log('Loaded ' + StreetMapPoints.length + ' StreetMapPoints in memory ...');
//        if (callback) callback();
//    });
//}
function loadStreetPoints(callback) {
    log('Caching Street map points ...');
    var streetpoints = [];
    getData("SELECT  streetid,Name,points FROM  StreetMapPoints", [], function (vals) {
        for (var x in vals) {
            var id = vals[x].streetid;
            var name = vals[x].Name;
            var points = vals[x].Points;
            var temp = { streetid: id, Name: name, Points: points };
            streetpoints.push(temp);
        }
        StreetMapPoints = streetpoints;
        log('Loaded ' + StreetMapPoints.length + ' StreetMapPoints in memory ...');
        if (callback) callback();

    });
}

function loadAuxTableNames(callback) {
    log('Caching AuxTable Names ...');
    tableListGlobal = [];
    getData("SELECT DISTINCT SourceTable FROM FieldCategory", [], function (auxt) {
        for (var x in auxt) {
        	if(auxt[x].SourceTable != null)
            	tableListGlobal.push( auxt[x].SourceTable );
        }
        function checkAndAddTable( tableName ){
            if ( tableName && tableName != '' )
                if ( tableListGlobal.indexOf( tableName ) == -1 ){
                    tableListGlobal.push( tableName );
                }
        }
        checkAndAddTable( clientSettings['SketchTable'] || sketchSettings['SketchTable'] )
        checkAndAddTable( clientSettings['SketchVectorTable'] || sketchSettings['SketchVectorTable'] )
        if ( ccma && ccma.Sketching && ccma.Sketching.Config ){
            var sources = ccma.Sketching.Config.sources;
            for ( var y in sources ){
                var s = sources[y];
                if ( s.VectorSource ) checkAndAddTable( s.VectorSource.Table );
                if ( s.SketchSource ) checkAndAddTable( s.SketchSource.Table );
                if ( s.NotesSource ) checkAndAddTable( s.NotesSource.Table );
            }
        }
        log( 'Loaded ' + tableListGlobal.length + ' Auxtable Names in memory ...' );
        if (callback) callback();
    }, null, false, function () {
        if (callback) callback();
    });
}

function loadParentChild(callback) {
    log('Caching ParentChild Table ...');
    parentChild = [];
    getData("SELECT * FROM ParentChild ORDER BY ParentTable", [], function (vals) {
        for (var x in vals) {
            var v = vals[x].ParentTable;
            var n = vals[x].ChildTable;
            var temp = { ParentTable: v, ChildTable: n };
            parentChild.push(temp);
        }
        log('Loaded ' + parentChild.length + ' Parent Childs in memory ...');

        if (callback) callback();
    }, null, true, function () {
        if (callback) callback();
    });
}

function loadNeighborhoods(callback) {
    log('Caching Neighborhood ...');
    neighborhoods = [];
    getData("SELECT * FROM Neighborhood", [], function (results) {
        for (x in results) {
            n = results[x];
            neighborhoods[n.Number.toString()] = [n];
        }
        createAggrFieldsForNbhd(function () {
            log('Loaded ' + results.length + ' neighborhood items in memory ...');
            if (callback) callback();
        });
    });
}


function loadFields(callback) {
    log('Caching Fields ...');
    datafields = {};
      var settings={};
    getData("SELECT * FROM Field", [], function (results) {
        for (var x in results) {
            var n = results[x];
            datafields[n.Id.toString()] = n;
			var temp = datafieldsettings.filter(function(f){return f.FieldId == n.Id});
			if(temp.length > 0){
            	for(y in temp){
				settings={};
				settings[temp[y]["PropertyName"]] = temp[y]["Value"];
            	 datafields[temp[y].FieldId.toString()].UI_Settings= settings;
                }
			}
        }
        log('Loaded ' + results.length + ' fields in memory ...');
        fieldCategories = [];
        getData("UPDATE FieldCategory SET HideExpression=REPLACE(REPLACE(HideExpression,'&gt;','>'),'&lt;','<')", [], function () {
            getData("SELECT * FROM FieldCategory", [], function (fc) {
                for (var x in fc) {
                    var fci = fc[x];
                    var settings={};
             		var temp = categorySettings.filter(function(f){return f.CategoryId == fci.Id});
            		if(temp.length > 0){
            			for(x in temp)
							settings[temp[x]["PropertyName"]] = temp[x]["Value"]
            				fci.UI_CateogorySettings= settings;
            			}
                    fieldCategories.push(fci);
                }
                if (callback) callback();
            });
        });

    });
}
var categorySettings=[];
function loadCategorySettings(callback) {
    log('Caching CategorySettings ...');
    getData("SELECT * FROM CategorySettings", [], function (results) {
        for (var x in results) {
            var n = results[x];
            categorySettings[n.Id.toString()] = n;
        }
        if (callback) callback();
    });
}
var datafieldsettings=[]
function loadFieldsettings(callback) {
	datafieldsettings=[];
    log('Caching Fieldssettings ...');
    getData("SELECT * FROM FieldSettings", [], function (results) {
        for (var x in results) {
            var n = results[x];
            datafieldsettings[n.Id.toString()] = n;
        }
        if (callback) callback();
    });
}

function loadCustomFunctionsOnAppLoad(callBack) {
    var fn = clientSettings.LoadMethodAfterAppLoad;
    if (fn) {
        fn = window[fn];
        if (typeof (fn) == "function")
            fn(callBack);
        else {
            alert('Invalid client setting value for LoadMethodAfterAppLoad.');
            callBack();
        }
    } else
        callBack();
}

function loadValidationTablesForVS8(callBack) {
    let arr = [];
    ccma.FieldValidations = {};
    getData("SELECT COMPONENT_ID CODE, DESCRIPTION, INPUT_DEFINITION_NUMBER, INPUT_TIP_TEXT, MAX_VALUE, MIN_VALUE, INPUT_REQUIREMENT_TYPE_ID, DECIMAL_ALLOWED FROM COMPONENT_INPUT_DEFINITION", [], (results) => {
        for (let x in results) {
            let item = results[x];
            if (parseInt(item.INPUT_DEFINITION_NUMBER) < 4) arr.push({ CODE: item.CODE, FIELD_TO_VALIDATE: 'MVA_INPUT_' + item.INPUT_DEFINITION_NUMBER, DESCRIPTION: item.DESCRIPTION, INPUT_TIP_TEXT: item.INPUT_TIP_TEXT, MIN_VALUE: item.MIN_VALUE, MAX_VALUE: item.MAX_VALUE, DECIMAL_ALLOWED: item.DECIMAL_ALLOWED, IS_REQUIRED: (item.INPUT_REQUIREMENT_TYPE_ID == '1' ? true : false) });
        }

        ccma.FieldValidations['COMPONENT_INPUT_DEFINITION'] = arr;
        arr = [];
        getData("SELECT CODE, PERCENT_REQUIREMENT_TYPE_ID, PERCENT_INPUT_TEXT INPUT_TIP_TEXT, PERCENT_MAXIMUM MAX_VALUE, PERCENT_MINIMUM MIN_VALUE FROM COMPONENT  WHERE PERCENT_REQUIREMENT_TYPE_ID IN (1, 2)", [], (results) => {
            for (let x in results) {
                let item = results[x];
                arr.push({ CODE: item.CODE, FIELD_TO_VALIDATE: 'MVA_PERCENTAGE', INPUT_TIP_TEXT: item.INPUT_TIP_TEXT, MIN_VALUE: item.MIN_VALUE, MAX_VALUE: item.MAX_VALUE, PERCENT_REQUIREMENT_TYPE_ID: item.PERCENT_REQUIREMENT_TYPE_ID });
            }

            ccma.FieldValidations['COMPONENT'] = arr;
            callBack && callBack();
        });
    });
}

function loadValidationTablesForNS( callBack ){
    var arr = [];
    ccma.FieldValidations = {};
    getData( "SELECT code,MSBVALIDATIONS FROM CIFEAT ", [], function ( results )    {
        for ( var x in results ) {
            var n = results[x].MSBVALIDATIONS;
            var text = decodeHTML( n ).replace( /&/g, '$$$' );
            parser = new DOMParser();
            xmlDoc = parser.parseFromString( text, "text/xml" );
            var code = results[x].CODE
            var validations = $( xmlDoc ).find( 'INPUT' );
            $( validations ).each( function ( id, item )  {
                arr.push( { COMPONENT_ID: code, FIELD_TO_VALIDATE: $( item ).attr( 'FLD' ), DESCRIPTION: $( item ).attr( 'Description' ), INPUT_TIP_TEXT: $( item ).attr( 'InputTipText' ), MIN_VALUE: $( item ).attr( 'MinValue' ), MAX_VALUE: $( item ).attr( 'MaxValue' ), IS_REQUIRED: ( $( item ).attr( 'InputRequirement' ) == '1' ? true : false ) } );
            } )
        }
        ccma.FieldValidations['CIFEAT'] = arr;
        arr = [];
        var MSVersionYear = ((clientSettings.MSVersionYear)&&(clientSettings.MSVersionYear!=""))?clientSettings.MSVersionYear:'MS20';
        
        getData( "SELECT ld.COMPONENT_ID,ld.DECIMAL_ALLOWED,ld.DESCRIPTION,ld.INPUT_DEFINITION_NUMBER,ld.INPUT_REQUIREMENT_TYPE_ID,ld.INPUT_TIP_TEXT,ld.MIN_VALUE,ld.MAX_VALUE,ob.CODE,ob.MVPINPUT1,ob.MVPINPUT2,ob.MVPINPUT3,ob.MVPINPUT4,ob.MVPINPUT5 from COMPONENT_INPUT_DEFINITION ld left outer join  COMPONENT lc on lc.ID=ld.COMPONENT_ID left outer join  RCOBY ob on ob.CODE_MVP= lc.code where ob.ver = '" + MSVersionYear + "' and ob.valmeth = 'M' UNION SELECT ld.COMPONENT_ID, ld.DECIMAL_ALLOWED,ld.DESCRIPTION, ld.INPUT_DEFINITION_NUMBER,ld.INPUT_REQUIREMENT_TYPE_ID,ld.INPUT_TIP_TEXT,ld.MIN_VALUE,ld.MAX_VALUE,ob.CODE,ob.MVPINPUT1,ob.MVPINPUT2,ob.MVPINPUT3,ob.MVPINPUT4,ob.MVPINPUT5 from CCV_COMPONENT_INPUT_DEF_RES ld left outer join  CCV_COMPONENT_RES lc on lc.ID=ld.COMPONENT_ID left outer join  RCOBY ob on ob.CODE_MVP= lc.code  where ob.ver = '" + MSVersionYear + "' and ob.valmeth = 'R' ORDER BY ld.COMPONENT_ID,ld.INPUT_DEFINITION_NUMBER ", [], function ( results )  {
            for ( var x in results ){
                var item = results[x]
                arr.push( { CODE: item.CODE, FIELD_TO_VALIDATE: item['MVPINPUT' + item.INPUT_DEFINITION_NUMBER], DESCRIPTION: item.DESCRIPTION, INPUT_TIP_TEXT: item.INPUT_TIP_TEXT, MIN_VALUE: item.MIN_VALUE, MAX_VALUE: item.MAX_VALUE, IS_REQUIRED: ( item.INPUT_REQUIREMENT_TYPE_ID == '1' ? true : false ) } );
            }
            ccma.FieldValidations['RCOBY'] = arr;
            arr = [];
            getData( "SELECT ld.COMPONENT_ID,ld.DECIMAL_ALLOWED,ld.DESCRIPTION, ld.INPUT_DEFINITION_NUMBER,ld.INPUT_REQUIREMENT_TYPE_ID,ld.INPUT_TIP_TEXT,ld.MIN_VALUE,ld.MAX_VALUE,ad.CODE,'AREA' as MVPINPUT1,ad.MVPINPUT2,ad.MVPINPUT3 from CCV_COMPONENT_INPUT_DEF_RES ld left outer join  CCV_COMPONENT_RES lc on lc.ID=ld.COMPONENT_ID  join  RCADDN ad on ad.CODE_MVP= lc.code  where ad.CODE_MVP is not null AND ad.ver = '" + MSVersionYear + "' and ad.RESMOD = '1'  ORDER BY ld.COMPONENT_ID,ld.INPUT_DEFINITION_NUMBER ", [], function ( results ) {
                 for ( var x in results ){
                var item = results[x]
                arr.push( { CODE: item.CODE, FIELD_TO_VALIDATE: item['MVPINPUT' + item.INPUT_DEFINITION_NUMBER], DESCRIPTION: item.DESCRIPTION, INPUT_TIP_TEXT: item.INPUT_TIP_TEXT, MIN_VALUE: item.MIN_VALUE, MAX_VALUE: item.MAX_VALUE, IS_REQUIRED: ( item.INPUT_REQUIREMENT_TYPE_ID == '1' ? true : false ) } );
            }
                ccma.FieldValidations['RCADDN'] = arr;
                callBack && callBack();
            })
        })
      
    } )
}