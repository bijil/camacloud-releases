﻿//    DATA CLOUD SOLUTIONS, LLC ("COMPANY") CONFIDENTIAL  
//    Unpublished Copyright (c) 2010-2013 
//    DATA CLOUD SOLUTIONS, an Ohio Limited Liability Company, All Rights Reserved.
//    NOTICE: All information contained herein is, and remains the property of COMPANY.
//    The intellectual and technical concepts contained herein are proprietary to COMPANY
//    and may be covered by U.S. and Foreign Patents, patents in process, and are protected
//    by trade secret or copyright law. Dissemination of this information or reproduction
//    of this material is strictly forbidden unless prior written permission is obtained
//    from COMPANY. Access to the source code contained herein is hereby forbidden to
//    anyone except current COMPANY employees, managers or contractors who have executed
//    Confidentiality and Non-disclosure agreements explicitly covering such access.</p>
//    The copyright notice above does not evidence any actual or intended publication
//    or disclosure of this source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
//    DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
//    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
//    IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
//    OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
//    TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
//    ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
// GIS
//var UsingOSM = false;
var gmaptype = 'ROADMAP';
var googleLayer;
var mapInitialized = false;
var firstLoadmap = true;
var changedMapBounds = true;
var map, routeMap, sketchmap, GeoLocationmap;
var OsMap;
var Aerial, StreetOnline, OSMStreet, CLayer, googleStreets, googleHybrid, osmGeomap, nearmapStreets, nearmapWMSStreets;
var legend;
var markerlayerGeo, markerlayer, polygonlayer, offlineLayer, labelLayer, heatMapLayer;

var myLocation, dummyLocation, OSMmyLocation;
var parcelMapCenter, parcelMapBounds;
if (L) OSMmyLocation = new L.LatLng(40.705817, -82.417757);
if (google) {
    myLocation = new google.maps.LatLng(0, 0);
    dummyLocation = new google.maps.LatLng(40.705817, -82.417757);
}
var myMarker, osMarker, omSpider, osmSpider;
var myCircle;
var osCircle;
var parcelPolygon;
var trackMe = false;
var parcelMapMode = false;
var parcelLinksMode = false;
var parcelMapMarkers = {};
var parcelHeatMap = {};
var HeatMapIcons = {};
var currentMapBounds = {};
var preMacBounds = {};
function getGoogleMap(canvasId, options) {
    var mc = document.getElementById(canvasId);
    if (google) {
        var mapTypeId = google.maps.MapTypeId.HYBRID;
        options.mapTypeId = mapTypeId
        var gmap = new google.maps.Map(mc, options);
        return gmap;
    }
    return {};
}

function onEachFeature(feature, layer) {
    var popupContent = feature.geometry.type;

    if (feature.properties && feature.properties.popupContent) {
        popupContent += feature.properties.popupContent;
    }

    layer.bindPopup(popupContent);
}

function clearLayers() {
    Aerial ? OsMap.removeLayer(Aerial) : '';
    OSMStreet ? OsMap.removeLayer(OSMStreet) : '';
    //googleLayer ? OsMap.removeLayer(googleLayer) : '';
    googleHybrid ? OsMap.removeLayer(googleHybrid) : '';
    googleStreets ? OsMap.removeLayer(googleStreets) : '';
    customCountyWMS ? OsMap.removeLayer(customCountyWMS) : '';
    nearmapStreets ? OsMap.removeLayer(nearmapStreets) : '';
    nearmapWMSStreets ? OsMap.removeLayer(nearmapWMSStreets) : '';
}

function setupMap(callbackMask) {
    if (!google && !UsingOSM) {
        log('Google Maps unavailable. Cannot load map.');
        return;
    }
    if (google) {
        directions = new google.maps.DirectionsService();
        mapDirec = new google.maps.DirectionsRenderer();
    }
    log('Setting up GIS Map UI for Parcels ...');
    var latlng;
    if (activeParcel)
        latlng = activeParcel.Center();
    else if (myLocation)
        latlng = myLocation;
    else
        latlng = dummyLocation;
    if (!UsingOSM) {
        map = getGoogleMap('mapCanvas', gisMapOptions);
        /// <reference path="13-routing.js" />
        GeoLocationmap = getGoogleMap('GeoMAP', smallMapOptions);
        var toolbar = document.createElement('DIV');
        var tb = new MapTools(toolbar, map);
        toolbar.index = 1;
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(toolbar);

        /*if (!isBPPUser) {
            omSpider = new OverlappingMarkerSpiderfier(map,
                {
                    markersWontMove: true,
                    markersWontHide: true,
                    keepSpiderfied: true,
                    circleSpiralSwitchover: 10,
                    basicFormatEvents: true,
                    circleFootSeparation: 50,
                    nearbyDistance: 0.001
                });
        }*/


        google.maps.event.addListener(map, 'idle', function () {
            if (ccma.UI.ActiveScreenId == "gis-map") {
                currentMapBounds = {};
                if (parcelMapMode)
                    refreshParcelsInMap();
                if (parcelLinksMode)
                    refreshParcelsInMap(null, true);
            }
        });
    } else {
        if (!osmGeomap)
            osmGeomap = L.map('GeoMAP', {
                center: new L.LatLng(36, -102),
                //zoom: 18,
                minZoom: 10,
                maxZoom: 19
            });
        if (!OsMap) {
            OsMap = L.map('mapCanvas', {
                center: new L.LatLng(36, -102),
                //zoom: 18,
                minZoom: 10,
                maxZoom: 19
            });
            OsMap.on("click", function (e) {
                $('.map-items').blur();
            });
            var refresh = function () {
                if (ccma.UI.ActiveScreenId == "gis-map") {
                    if (parcelMapMode)
                        refreshParcelsInMap(null, null, true);
                    if (parcelLinksMode)
                        refreshParcelsInMap(null, true);
                }
            }
            OsMap.on('zoomend load', function () {
                var rad = 0,
                    zoomLevel = OsMap.getZoom();
                if (StreetLayer) {
                    if (zoomLevel > 12) {
                        OsMap.hasLayer(offlineLayer) == false ? offlineLayer.addTo(OsMap) : '';
                        OsMap.hasLayer(labelLayer) == false ? labelLayer.addTo(OsMap) : '';
                    }
                    if (zoomLevel <= 12) {
                        OsMap.hasLayer(labelLayer) == true ? OsMap.removeLayer(labelLayer) : '';
                        OsMap.hasLayer(offlineLayer) == true ? OsMap.removeLayer(offlineLayer) : '';
                    }
                }
                if (HeatLayer == true && UsingOSM && currentHeatMapField) {
                    if (zoomLevel >= 18)
                        rad = 21;
                    else if (zoomLevel == 17 || zoomLevel == 16 || zoomLevel == 15)
                        rad = 12;
                    else
                        rad = 3;
                    for (x in Object.keys(HeatMapIcons)) {
                        var k = Object.keys(HeatMapIcons)[x];
                        HeatMapIcons[k.toString()].setRadius(rad);
                    }
                }
                refresh();
            });
            OsMap.on('dragend', function (e) {
                refresh();
            });

            /* if (!isBPPUser) { //MarkerSpiderfier not affect BPP List view functionlity.
                 osmSpider = new OverlappingMarkerSpiderfierOsm(OsMap,
                     {
                         markersWontMove: true,
                         markersWontHide: true,
                         keepSpiderfied: true,
                         circleSpiralSwitchover: 10,
                         basicFormatEvents: true,
                         circleFootSeparation: 50,
                         nearbyDistance: 0.001
                     });
             }*/

        }
        hideLegendIcon();
        clearLayers();
        if (StreetView && google) {
            //vin-str gmaptype = 'HYBRID';
            //googleLayer = new L.Google(gmaptype);
            //OsMap.addLayer(googleLayer); vin-end
            googleHybrid.addTo(OsMap);
            //else{
            //    gmaptype = 'ROADMAP';
            //    googleLayer = new L.Google(gmaptype);
            //    OsMap.addLayer(googleLayer);
            //}
        }

        else if (clientSettings.EnableNearmap && clientSettings.EnableNearmap.includes('MA') && UsingOSM) {
            nearmapStreets.addTo(OsMap);
        }
        else if (clientSettings.EnableNearmapWMS && clientSettings.EnableNearmapWMS.includes('MA') && UsingOSM) {

            var nearmapWMSStreets = L.tileLayer.wms('https://api.nearmap.com/wms/v1/latest/apikey/'+clientSettings["NearmapWMS.APIKey"]+'?service=WMS',
                { layers: 'Nearmap/Nearmap/USA,Nearmap,Satellite', zoom: 18, maxZoom: 19})
            nearmapWMSStreets.addTo(OsMap);
        }
        else {
            Aerial.addTo(OsMap);
        }
        osmGeomap.removeLayer(geoloc);
        geoloc.addTo(osmGeomap);
        //   loadPersistent();
        //mapType
        if (HeatLayer) {
            $('.heatmap-items').show();
            // $('#showLines').hide();
            showLegendIcon();
            $('.heatmap-items').val($(".heatmap-items option:first-child").val());
            $('#mapCanvas').unbind("touchend");
            $('#mapCanvas').bind("touchend", function (e) {   //ios13 focusout not working when touch on map screen
                $('.heatmap-items').blur();
                $('.map-items').blur();
            });
        } else {
            $('.heatmap-items').hide();
            //  $('#showLines').show();
            hideLegendIcon();
            removeLegend();
        }
        loadOfflineMap();

        var otoolbar = document.createElement('DIV');
        var otb = new osMapTools(otoolbar, OsMap);
    }
    if ((myLocation && myLocation.lat() != 0.0) && !UsingOSM) {
        if (GeoLocationmap) {
            GeoLocationmap.setCenter(myLocation);
            GeoLocationmap.setZoom(17);
        }
        if (myMarker === undefined) {
            myMarker = new google.maps.Marker({
                position: myLocation,
                icon: anchor,
                map: map
            });

        }

        if (myCircle === undefined) {
            myCircle = new google.maps.Circle({
                map: map,
                radius: gisAccuracy, // 3000 km
                strokeColor: '#0000FF',
                fillColor: '#0095fc',
                fillOpacity: 0.2,
                strokeWeight: 1
            });
        }

        myMarker.setPosition(myLocation);
        myMarker.setMap(map);
        map.setCenter(myLocation);
        map.setZoom(18);
        myCircle.bindTo('center', myMarker, 'position');
    }
    else if ((OSMmyLocation && OSMmyLocation.lat != 0) && UsingOSM) {
        if (osmGeomap) {
            osmGeomap.setView(OSMmyLocation, 19);
            osmGeomap.setZoom(19);
        }
        var noMarker = (osMarker === undefined || osMarker._map == null) ? true : false;
        var noCircle = (osCircle === undefined || osCircle._map == null) ? true : false;
        if (noMarker) {
            osMarker = new L.marker(OSMmyLocation, {
                icon: anchor
            }).addTo(OsMap);
        }

        if (noCircle) {
            osCircle = new L.Circle(OSMmyLocation, gisAccuracy, {
                color: '#0000FF',
                fillColor: '#0095fc',
                fillOpacity: 0.2,
                strokeWeight: 1
            }).addTo(OsMap);
        }

        osMarker.setLatLng(OSMmyLocation);
        osCircle.setLatLng(OSMmyLocation);
        //OsMap.setView(OSMmyLocation,19);
    }

    log('Map Initialized');
    mapInitialized = true;
    changedMapBounds = true;
    if (callbackMask) {
        callbackMask();
    }

}

function clearMap() {
    if (map && !UsingOSM) {
        polygons.forEach(function (w) {
            w.setMap(null);
        });
        polygons = [];
        if (parcelPolygon != null) {
            if (parcelPolygon.setOptions)
                parcelPolygon.setOptions({
                    paths: []
                });
        }
        if (parcelMapMarkers) {
            var keys = Object.keys(parcelMapMarkers);
            for (x in keys) {
                if (parcelMapMarkers[keys[x]].setMap) {
                    parcelMapMarkers[keys[x]].setMap(null);
                    parcelMapMarkers[keys[x]] = null;
                }
            }
            parcelMapMarkers = [];
        }
    } else if (OsMap) {
        if (OsMap.hasLayer(markerlayer)) {
            OsMap.removeLayer(markerlayer);
        }
        if (OsMap.hasLayer(polygonlayer)) {
            OsMap.removeLayer(polygonlayer);
        }
        if (OsMap.hasLayer(heatMapLayer)) {
            OsMap.removeLayer(heatMapLayer);
        }
        polygonlayer = new L.FeatureGroup();
        markerlayer = new L.FeatureGroup();
        heatMapLayer = new L.FeatureGroup();
        parcelMapMarkers = [];
    }
}

function resizeMap() {
    if (mapInitialized) {
        if (!UsingOSM && google) {
            google.maps.event.trigger(map, 'resize');
            google.maps.event.trigger(GeoLocationmap, 'resize');
        } else if (UsingOSM) {
            OsMap.invalidateSize()
            osmGeomap.invalidateSize()
        }
    }
}

function MapTools(toolbar, map) {

    toolbar.style.padding = '5px';

    var parcelIcon = document.createElement('DIV');
    parcelIcon.className = 'toolbaricon map-parcel'
    toolbar.appendChild(parcelIcon);

    var agentIcon = document.createElement('DIV');
    agentIcon.className = 'toolbaricon map-agent'
    toolbar.appendChild(agentIcon);

    var chkbx = document.createElement('input');
    chkbx.className = "SFswitch-checkbox";
    chkbx.type = "checkbox";
    chkbx.id = 'chkbx';
    var checkdiv = document.createElement('DIV');
    checkdiv.className = "SFswitch";
    checkdiv.appendChild(chkbx);
    checkdiv.insertAdjacentHTML('beforeend', "<label class='SFswitch-label' for='chkbx'><span class='SFswitch-inner'></span><span class='SFswitch-switch'></span></label>");
    toolbar.appendChild(checkdiv);


    // Setup the click event listeners: simply set the map to Chicago.
    google.maps.event.addDomListener(agentIcon, 'click', function () {
        if (mapInitialized) {
            if (myLocation.lat() != 0) {
                map.panTo(myMarker.getPosition());
                trackMe = true;
                shadowToolbar();
            } else {
                messageBox('Location service is not initialized. Please try after sometime.');
            }
        }
    });

    google.maps.event.addDomListener(parcelIcon, 'click', function () {

        if (parcelMapCenter) {
            map.setCenter(parcelMapCenter);
            map.fitBounds(parcelMapBounds);
        } else
            if (activeParcel) {
                activeParcel.LocateOnMap();
            }
        trackMe = false;
        shadowToolbar();
    });


    google.maps.event.addDomListener(chkbx, 'click', function () {

        swichingloc();

    });

}

function swichingloc() {
    $("#chkbx").prop('checked') ? geo.SwitchToFastMode() : geo.SwitchToSlowMode();
}
//osmap
function osMapTools(otoolbar, OsMap) {

    if (UsingOSM && OsMap && firstLoadmap) {
        var OSMAgentIcon = L.Control.extend({
            options: {
                position: 'topleft'
            },
            onAdd: function (OsMap) {
                var OSMAgent = L.DomUtil.create('div', 'toolbaricon map-agent');
                OSMAgent.style.padding = '0px';
                OSMAgent.style.margin = '3px';
                OSMAgent.onclick = function () {
                    var noMarker = (osMarker === undefined || osMarker._map == null) ? true : false;
                    var noCircle = (osCircle === undefined || osCircle._map == null) ? true : false;
                    if (mapInitialized) {
                        if (OSMmyLocation && OSMmyLocation.lat != 0) {
                            if (noCircle) {
                                osCircle = new L.Circle(OSMmyLocation, gisAccuracy, {
                                    color: '#0000FF',
                                    fillColor: '#0095fc',
                                    fillOpacity: 0.2,
                                    strokeWeight: 1
                                }).addTo(OsMap);
                            }
                            if (noMarker)
                                osMarker = new L.marker(OSMmyLocation, {
                                    icon: anchor
                                }).addTo(OsMap);

                            osMarker.setLatLng(OSMmyLocation);
                            osCircle.setLatLng(OSMmyLocation);

                            OsMap.panTo(OSMmyLocation);
                            OsMap.setZoom(18);
                            trackMe = true;
                        }
                        shadowToolbar();
                    } else {
                        messageBox('Location service is not initialized. Please try after sometime.');
                    }
                }

                return OSMAgent;
            }

        });
        var OSMParcelIcon = L.Control.extend({

            options: {
                position: 'topleft'
                //control position - allowed: 'topleft', 'topright', 'bottomleft', 'bottomright'
            },
            onAdd: function (OsMap) {
                var OSMParcel = L.DomUtil.create('div', 'toolbaricon map-parcel');
                OSMParcel.style.padding = '0px';
                OSMParcel.style.margin = '3px';
                OSMParcel.onclick = function () {
                    if (parcelMapCenter) {
                        OsMap.panTo(parcelMapCenter);
                        OsMap.fitBounds(parcelMapBounds);
                    } else
                        if (activeParcel) {
                            activeParcel.LocateOnMap();
                        }
                    trackMe = false;
                    if (!activeParcel)
                        refreshParcelsInMap();
                    shadowToolbar();
                }
                return OSMParcel;

            }

        });

        var OSMcheckbox = L.Control.extend({

            options: {
                position: 'topleft'
            },
            onAdd: function (OsMap) {
                var OSMchkbx = L.DomUtil.create('div');
                OSMchkbx.innerHTML = "<div class='SFswitch'><input class='SFswitch-checkbox' type='checkbox' id='chkbx'><label class='SFswitch-label' for='chkbx'><span class='SFswitch-inner'></span><span class='SFswitch-switch'></span></label></div>";
                OSMchkbx.style.padding = '0px';
                OSMchkbx.style.margin = '1px';
                OSMchkbx.onclick = function () {
                    toggleLegend();
                }
                return OSMchkbx;
            }

        });

        var OSMLegendIcon = L.Control.extend({

            options: {
                position: 'topleft'
            },
            onAdd: function (OsMap) {
                var OSMLegend = L.DomUtil.create('div', 'legendIcon heat-legend');
                OSMLegend.style.padding = '0px';
                OSMLegend.style.margin = '3px';
                OSMLegend.onclick = function () {
                    toggleLegend();
                }
                return OSMLegend;
            }

        });
        var osmAerialIcon = L.Control.extend({

            options: {
                position: 'topright'
            },
            onAdd: function (OsMap) {
                var osmAerial = L.DomUtil.create('div', 'layerIcon aerial-icon');
                osmAerial.style.padding = '5px 0px 0px 11px';
                osmAerial.style.borderradius = '14px';
                osmAerial.style.margin = '6px';
                osmAerial.onclick = function () {
                    $('.map-items').val('');
                    checkPictometry();
                    $('.map-hide-pictometry').hide();
                    checkNearMap();
                    $('.map-hide-nearmap').hide();
                    checkNearMapWMS();  
                    $('.map-hide-nearmapwms').hide();
                    checkEagleView();
                    $('.map-hide-ev').hide();
                    removeEVScripts();
                    checkWoolpert();
                    $('.map-hide-woolpert').hide();
                    checkImagery();
                    $('.map-hide-Imagery').hide();
                    if (clientSettings.EnableqPublic && clientSettings.EnableqPublic == '1') {
                        $('.q_public button').html('Show qPublic');
                        $('.q_public button').attr('onclick', 'show_qPublic();');
                    }
                    loadAerialLayer();
                }
                return osmAerial;
            }

        });
        var osmStreetIcon = L.Control.extend({
            options: {
                position: 'topright'
            },
            onAdd: function (OsMap) {
                var streetview = L.DomUtil.create('div', 'layerIcon street-icon');
                streetview.style.padding = '5px 0px 0px 11px';
                streetview.style.borderradius = '14px';
                streetview.style.margin = '6px';
                streetview.onclick = function () {
                    $('.map-items').val('');
                    checkPictometry();
                    $('.map-hide-pictometry').hide();
                    checkNearMap();
                    $('.map-hide-nearmap').hide();
                    checkNearMapWMS();
                    $('.map-hide-nearmapwms').hide();
                    checkEagleView();
                    $('.map-hide-ev').hide();
                    removeEVScripts();
                    checkWoolpert();
                    $('.map-hide-woolpert').hide();
                    checkImagery();
                    $('.map-hide-Imagery').hide();
                    if (clientSettings.EnableqPublic && clientSettings.EnableqPublic == '1') {
                        $('.q_public button').html('Show qPublic');
                        $('.q_public button').attr('onclick', 'show_qPublic();');
                    }
                    loadStreetLayer();
                }
                return streetview;
            }

        });
        var measureControl = new L.Control.Measure(
            {
                position: 'topleft',
                primaryLengthUnit: 'feet',
                secondaryLengthUnit: 'meters',
                primaryAreaUnit: 'sqfeet',
                secondaryAreaUnit: 'acres',
                activeColor: '#deda0b',
                completedColor: '#f5ca0a'

            });
        OsMap.addControl(new OSMAgentIcon());
        OsMap.addControl(new OSMParcelIcon());
        OsMap.addControl(new OSMLegendIcon());
        OsMap.addControl(new osmStreetIcon());
        OsMap.addControl(new osmAerialIcon());
        OsMap.addControl(new OSMcheckbox());
        firstLoadmap = false;
        measureControl.addTo(OsMap);

    }


}

function calculateParcelCenters(callback) {
    log('Calculating parcel centers..');
    unsortedParcels = {};
    var sql = "SELECT MP.*, P.CC_Priority FROM MapPoints MP INNER JOIN Parcel P ON P.Id = MP.ParcelId WHERE P.IsComparable = 0  ORDER BY ParcelId, Ordinal * 1";
    getData(sql, [], function (points) {
        if (points.length == 0 || (!UsingOSM && !google)) {
            console.log('Unable to calculate parcel centers.')
            if (callback) callback();
            return;
        }

        for (x in points) {
            var point = points[x];
            var pid = point.ParcelId.toString();
            if (unsortedParcels[pid] == null)
                unsortedParcels[pid] = {
                    ParcelId: point.ParcelId,
                    Distance: 0.0,
                    Location: null,
                    Points: [],
                    SortIndex: 9999,
                    CC_Priority: point.CC_Priority
                };
            unsortedParcels[pid].Points.push(point.Point);
        }

        log('Processing ...');

        var totalParcels = Object.keys(unsortedParcels).length;
        var index = 0;
        db.transaction(function (tx) {
            for (x in unsortedParcels) {
                parcel = unsortedParcels[x];
                /*if (!UsingOSM) {
                    var bounds = new google.maps.LatLngBounds();
                } else {
                    var bounds = new L.LatLngBounds();
                }
                for (i in parcel.Points) {
                    pstr = parcel.Points[i].split(',');
                    if (!UsingOSM) {
                        loc = new google.maps.LatLng(parseFloat(pstr[0]), parseFloat(pstr[1]));
                    } else {
                        loc = new L.latLng(pstr[0], pstr[1]);
                    }
                    bounds.extend(loc);
                }
                parcel.Location = bounds.getCenter();*/
                // Since google map js api and leaflet uses the centroid method to find the center of polygon,
                // the above will not work in all cases. Instead of centroid we use an algorithm to solve the 
                // Pole Of Inaccessibility problem to find the appropriate center.
                let polygonArr = [];
                for (i in parcel.Points) {
                    let pstr = parcel.Points[i].split(',');
                    pstr[0] = parseFloat(pstr[0]);
                    pstr[1] = parseFloat(pstr[1]);
                    polygonArr.push(pstr)
                }
                let polyCenter = centerMarker([polygonArr], 1.0);

                tx.executeSql("UPDATE Parcel SET Latitude = ?, Longitude = ? WHERE Id = ?", [polyCenter[0], polyCenter[1], parseInt(parcel.ParcelId).toString()], function () {
                    index += 1;
                    if (index == totalParcels) {
                        log('Completed ...');
                        if (callback) callback();
                    }
                });
            }
        });

    });

}

function refreshParcelsInMap(callback, showLinks, fromZoom) {
    //clearMap();

    var bounds = {};
    var key;
    if (!UsingOSM) {
        bounds = map.getBounds();
    } else {
        bounds = OsMap.getBounds();
    }
    if (Object.keys(currentMapBounds).length > 0 && !UsingOSM) bounds = currentMapBounds;
    var northeast = bounds.getNorthEast();
    var southwest = bounds.getSouthWest();
    if (!UsingOSM) {
        var lat1 = northeast.lat();
        var lat2 = southwest.lat();
        var lng1 = southwest.lng();
        var lng2 = northeast.lng();
    } else {
        var lat1 = northeast.lat
        var lat2 = southwest.lat
        var lng1 = southwest.lng
        var lng2 = northeast.lng
    }
    var CC_Priority = "CC_Priority = 0 AND ";
    if (isBPPUser) {
        CC_Priority = '';
        if (ccma.Session.BPPDownloadType == "0") CC_Priority = '';
        if (listMode == 1) {
            if (priMapMode == 1)
                CC_Priority += EnableNewPriorities == 'True' ? "CC_Priority IN (1,2,3,4,5) AND " : "CC_Priority IN (1,2) AND ";
            else
                CC_Priority += " "
        } else
            CC_Priority += "CC_Priority = 0 AND "
    } else if (!showLinks) {
        if (listMode == 1) {
            if (priMapMode == 0) {
                CC_Priority = "";
            } else if (priMapMode == 1) {
                CC_Priority = EnableNewPriorities == 'True' ? "CC_Priority IN (1,2,3,4,5) AND " : "CC_Priority IN (1,2) AND ";
            }

        }
    } else {
        CC_Priority = 'Id IN (SELECT ChildParcelId FROM ParcelLinks WHERE ParcelId = ' + activeParcel.Id + ') AND';
    }
    var nbhdFilter = '', bppOrderBy = '';

    if (isBPPUser && ccma.Session.bppRealDataEditable == 1 && ["1", "2", "3"].indexOf(clientSettings.bppRPEditableSorting) > -1) {
        if (clientSettings.bppRPEditableSorting == "1") bppOrderBy = "-CC_Priority,";
        else if (clientSettings.bppRPEditableSorting == "2") bppOrderBy = "IsRPProperty, IsPersonalProperty,";
        else if (clientSettings.bppRPEditableSorting == "3") bppOrderBy = "IsPersonalProperty, IsRPProperty,";
    }

    //if (currentNbhd)
    //    nbhdFilter = "NeighborhoodId = '" + currentNbhd + "' AND "
    //  getData("SELECT * FROM MapPoints", [], function (dummy) { // Forgot why this query is used here.. I think this is to induce some delay before running the next querty.

    getData("SELECT * FROM SubjectParcels WHERE " + CC_Priority + " 1 == 1", [], function (subResults) {
        getData("SELECT * FROM SubjectParcels WHERE " + CC_Priority + nbhdFilter + " CAST(Latitude AS FLOAT) BETWEEN ? AND ? AND CAST(Longitude AS FLOAT) BETWEEN ? AND ? ORDER BY "+ bppOrderBy + "DistanceIndex * 1", [Math.min(lat1, lat2), Math.max(lat1, lat2), Math.min(lng1, lng2), Math.max(lng1, lng2)], function (results) {
            var backupResults = _.clone(results);
            if (isBPPUser) {
                for (var i = 0; i < results.length; i++) {
                    var pItem = results[i].ParentParcelID;
                    if (results.findIndex(function (x) { return x.Id == pItem }) > -1) {
                        results.splice(i, 1);
                        i--;
                    }
                }
            }
            if (results.length > 350 && !enableMultipleNbhd) {
                clearMap();
            } else {
                var markers = [];
                var loadHeatMapIcons = function (p, pt) {
                    currentHeatMapFieldValue = p[currentHeatMapField];
                    var color = null,
                        value = "";
                    if (currentHeatMapFieldValue) {
                        color = relatedItemFinder(currentHeatMapFieldId, currentHeatMapFieldValue);
                        var isAggrField = currentHeatMapField.startsWith('SUM_') || currentHeatMapField.startsWith('AVG_') || currentHeatMapField.startsWith('COUNT_') || currentHeatMapField.startsWith('MAX_') || currentHeatMapField.startsWith('MIN_') || currentHeatMapField.startsWith('FIRST_') || currentHeatMapField.startsWith('LAST_') || currentHeatMapField.startsWith('MEDIAN_');
                        value = isAggrField ? currentHeatMapFieldValue.format(getDatatypeOfAggregateFields(currentHeatMapField).formatter) : currentHeatMapFieldValue.format(ccma.Data.Types[datafields[currentHeatMapFieldId].InputType].formatter);
                    }
                    //if (color) 
                    //  color = color;  
                    var opacity = 1;
                    if (!color || color == "null") {
                        color = 'red';
                        opacity = 0;
                    }
                    setHeatCircles(p, pt, color, opacity, value);
                    HeatMapIcons[p.Id.toString()].addTo(heatMapLayer);
                    HeatMapIcons[p.Id.toString()].changed = false;
                }
                var firstParcel = $('.parcel-item').attr('parcelid');

                if (!isBPPUser && !HeatLayer) {
                    results = uniqueGisParcels(results);
                }

                for (x in results) {
                    var p = results[x];
                    var point = UsingOSM == true ? new L.latLng(p.Latitude, p.Longitude) : new google.maps.LatLng(p.Latitude, p.Longitude);

                    if (HeatLayer == true && UsingOSM && currentHeatMapField) {
                        if (HeatMapIcons[p.Id.toString()] == null || HeatMapIcons[p.Id.toString()].changed == true) {
                            if (HeatMapIcons[p.Id.toString()] != null && HeatMapIcons[p.Id.toString()].changed == true)
                                heatMapLayer.remove(p.Id.toString());
                            if (boundaryLine[p.Id.toString()] != null)
                                // polygonlayer.removeLayer(boundaryLine[p.Id.toString()]);
                                boundaryLine[p.Id.toString()].forEach(function (x) {
                                    polygonlayer.removeLayer(x);
                                });

                            if (ShowBoundaries == true) {
                                if (boundaryLine[p.Id.toString()] == null) {
                                    getParcelWithMap(p.Id, function (parcel) {
                                        parcel.ShowOnMap(false, null, function () {
                                            var pt = new L.latLng(parcel.Latitude, parcel.Longitude);
                                            loadHeatMapIcons(parcel, pt);
                                        });

                                    });
                                } else {
                                    //polygonlayer.addLayer(boundaryLine[p.Id.toString()]);
                                    boundaryLine[p.Id.toString()].forEach(function (x) {
                                        polygonlayer.addLayer(x);
                                    });
                                    loadHeatMapIcons(p, point);
                                }
                            } else {
                                loadHeatMapIcons(p, point);
                            }
                        } else {
                            if (ShowBoundaries == true) {
                                boundaryLine[p.Id.toString()].forEach(function (x) {
                                    polygonlayer.addLayer(x);
                                });
                            }
                            heatMapLayer.addLayer(HeatMapIcons[p.Id.toString()]);
                        }
                    }
                    if (HeatLayer == true) continue;
                    var icon = iconParcel;

                    var unsyncedChanges = UnSynced.filter(function (x) {
                        return x.ParcelId == p.Id.toString()
                    }).length > 0 || imagesUnsynced.filter(function (x) {
                        return x.ParcelId == p.Id.toString()
                    }).length > 0;
                    var pendingParcelsRemaining = false, rpPlReviewed = false;
                    if (isBPPUser) {
                        let ppId = (p.IsRPProperty == "true" && !p.ParentParcelID ? p.Id : ((p.ParentParcelID) ? p.ParentParcelID : p.Id));
                        var bppParcelexists = subResults.filter(function (x) { return (x.ParentParcelID == ppId) });
                        var highestPriority = subResults.filter(function (x) { return (x.ParentParcelID == ppId) && x.Reviewed == 'false' }).map(function (y) { return Math.max(y.CC_Priority) });
                        if (highestPriority.length > 0) {
                            p.CC_Priority = highestPriority.max().toString();
                            pendingParcelsRemaining = true;
                            if (ccma.Session.bppRealDataEditable == 1) {
                                let rpPlParcel = subResults.filter(function (x) { return (x.Id == ppId) })[0];
                                if (!rpPlParcel) rpPlParcel = subResults.filter(function (x) { return (x.Id == p.Id)[0] });
                                if (rpPlParcel && parseInt(highestPriority.max().toString()) < parseInt(rpPlParcel.CC_Priority) && rpPlParcel.Reviewed != 'true') {
                                    p.CC_Priority = rpPlParcel.CC_Priority.toString();
                                }
                            }
                        }
                        else {
                            if (ccma.Session.bppRealDataEditable == 1) {
                                let rpPlParcel = subResults.filter(function (x) { return (x.Id == ppId) });
                                if (!rpPlParcel) rpPlParcel = subResults.filter(function (x) { return (x.Id == p.Id) });
                                if (rpPlParcel[0]) {
                                    if (rpPlParcel[0].Reviewed == 'false') {
                                        if (bppParcelexists.length > 0) {
                                            if (ccma.Session.bppRealDataEditable == 1 && ParcelsNotInGroup.includes(parseInt(ppId))) rpPlReviewed = true;
                                            else if (ccma.Session.bppRealDataReadOnly) rpPlReviewed = true;
                                        }
                                    }
                                    else rpPlReviewed = true;
                                }
                            }
                            else rpPlReviewed = true;
                        }
                    }
                    if (isBPPUser && !(p.ParentParcelID == null && (p.IsPersonalProperty == true || p.IsPersonalProperty == "true"))) {
                        if (EnableNewPriorities == 'True') {
                            if (p.CC_Priority == 2 || p.CC_Priority == "2")
                                icon = unsyncedChanges ? bppMediumUnsync : bppMedium;
                            else if (p.CC_Priority == "1")
                                icon = unsyncedChanges ? bppNormalUnsync : bppNormal;
                            else if (p.CC_Priority == "5")
                                icon = unsyncedChanges ? bppCriticalUnsync : bppCritical;
                            else if (p.CC_Priority == "3")
                                icon = unsyncedChanges ? bppParcelPriorityUnsync : bppParcelPriority;
                            else if (p.CC_Priority == "4")
                                icon = unsyncedChanges ? bppParcelAlertUnsync : bppParcelAlert;
                            else if (p.CC_Priority == "0" || p.CC_Priority == 0)
                                icon = unsyncedChanges ? bppParcelUnsync : bppParcel;
                        }
                        else {
                            if (p.CC_Priority == 2 || p.CC_Priority == "2")
                                icon = unsyncedChanges ? bppParcelAlertUnsync : bppParcelAlert;
                            else if (p.CC_Priority == "1")
                                icon = unsyncedChanges ? bppParcelPriorityUnsync : bppParcelPriority;
                            else if (p.CC_Priority == "0" || p.CC_Priority == 0)
                                icon = unsyncedChanges ? bppParcelUnsync : bppParcel;
                        }

                        if ((p.Reviewed == "true" && !pendingParcelsRemaining) || rpPlReviewed) icon = bppParcelVisited;
                    }
                    else {
                        if (firstParcel == p.Id.toString())
                            icon = iconParcelFirst;
                        if (unsyncedChanges) {
                            if (firstParcel == p.Id.toString())
                                icon = ParcelUnsyncFirst;
                            else
                                icon = ParcelUnsync;
                        }
                        if (EnableNewPriorities == 'True') {
                            if (p.CC_Priority == 4 || p.CC_Priority == "4") {
                                if (firstParcel == p.Id.toString())
                                    icon = iconParcelAlertFirst;
                                else
                                    icon = iconParcelAlert;
                                if (unsyncedChanges) {
                                    if (firstParcel == p.Id.toString())
                                        icon = ParcelAlertUnsyncFirst;
                                    else
                                        icon = ParcelAlertUnsync;
                                }
                            } else if (p.CC_Priority == "3") {
                                if (firstParcel == p.Id.toString())
                                    icon = iconParcelPriorityFirst;
                                else
                                    icon = iconParcelPriority;
                                if (unsyncedChanges) {
                                    if (firstParcel == p.Id.toString())
                                        icon = ParcelPriorityUnsyncFirst;
                                    else
                                        icon = ParcelPriorityUnsync;
                                }
                            }
                            else if (p.CC_Priority == "5") {
                                if (firstParcel == p.Id.toString())
                                    icon = icriticalFirst;
                                else
                                    icon = icritical;
                                if (unsyncedChanges) {
                                    if (firstParcel == p.Id.toString())
                                        icon = icriticalUnsyncFirst;
                                    else
                                        icon = icritcalUnsync;
                                }
                            }
                            else if (p.CC_Priority == "2") {
                                if (firstParcel == p.Id.toString())
                                    icon = imediumFirst;
                                else
                                    icon = imedium;
                                if (unsyncedChanges) {
                                    if (firstParcel == p.Id.toString())
                                        icon = imediumUnsyncFirst;
                                    else
                                        icon = imediumUnsync;
                                }
                            }
                            else if (p.CC_Priority == "1") {
                                if (firstParcel == p.Id.toString())
                                    icon = inormalFirst;
                                else
                                    icon = inormal;
                                if (unsyncedChanges) {
                                    if (firstParcel == p.Id.toString())
                                        icon = inormalUnsyncFirst;
                                    else
                                        icon = inormalUnsync;
                                }
                            }
                        }
                        else {
                            if (p.CC_Priority == 2 || p.CC_Priority == "2") {
                                if (firstParcel == p.Id.toString())
                                    icon = iconParcelAlertFirst;
                                else
                                    icon = iconParcelAlert;
                                if (unsyncedChanges) {
                                    if (firstParcel == p.Id.toString())
                                        icon = ParcelAlertUnsyncFirst;
                                    else
                                        icon = ParcelAlertUnsync;
                                }
                            } else if (p.CC_Priority == "1") {
                                if (firstParcel == p.Id.toString())
                                    icon = iconParcelPriorityFirst;
                                else
                                    icon = iconParcelPriority;
                                if (unsyncedChanges) {
                                    if (firstParcel == p.Id.toString())
                                        icon = ParcelPriorityUnsyncFirst;
                                    else
                                        icon = ParcelPriorityUnsync;
                                }
                            }
                        }
                        if (p.Reviewed == "true" && !pendingParcelsRemaining) {
                            if (firstParcel == p.Id.toString())
                                icon = iconParcelVisitedFirst;
                            else
                                icon = iconParcelVisited;
                        }
                    }
                    if (parcelMapMarkers[p.Id.toString()] == null) {
                        if (!UsingOSM) {
                            parcelMapMarkers[p.Id.toString()] = new google.maps.Marker({
                                position: point,
                                icon: icon
                            });

                            //if (!isBPPUser) omSpider.addMarker(parcelMapMarkers[p.Id.toString()]);
                        }
                        else {
                            parcelMapMarkers[p.Id.toString()] = L.marker(point, {
                                icon: icon
                            });

                            //if (!isBPPUser) osmSpider.addMarker(parcelMapMarkers[p.Id.toString()]);
                        }
                        parcelMapMarkers[p.Id.toString()].parcelData = p;
                        if (!UsingOSM) {
                            parcelMapMarkers[p.Id.toString()].setMap(map);
                        };
                        var touchClickEvent = 'click';
                        if (ShowBoundaries == true) {
                            getParcelWithMap(p.Id, function (parcel) {
                                parcel.ShowOnMap(false, null);
                            });
                        }
                        if (UsingOSM) {
                            L.DomEvent.addListener(parcelMapMarkers[p.Id.toString()], 'click', function (e) {
                                let m = this;
                                getParcelWithMap(this.parcelData.Id, function (parcel) {
                                    var childpresent = false;
                                    if (parcel.Reviewed == 'true') {
                                        gvisibility = 'display: none;'
                                    }
                                    else {
                                        gvisibility = ''
                                    }
                                    parcel ? ShowAlternateField == "True" ? ShowKeyValue1 == "True" ? key = parcel.KeyValue1 + " | " + eval("parcel." + AlternateKey) : eval("parcel." + AlternateKey) ? key = eval("parcel." + AlternateKey) : isBPPUser ? key = parcel.KeyValue1 : key = eval("parcel." + AlternateKey) : key = parcel.KeyValue1 : key = '';
                                    if (isBPPUser && (parcel.IsRPProperty == "true" || parcel.ParentParcelID != null)) {
                                        let sbp = clientSettings?.SortBppParcelMAMapView == '1' ? " ORDER BY CASE WHEN StreetNumber IS NULL THEN 1 ELSE 0 END, StreetNumber, CASE WHEN StreetName IS NULL THEN 1 ELSE 0 END, StreetName, CASE WHEN StreetNameSuffix IS NULL THEN 1 ELSE 0 END, StreetNameSuffix, CASE WHEN StreetUnit IS NULL THEN 1 ELSE 0 END, StreetUnit" : "";
                                        var sql = "SELECT * from Parcel WHERE " + CC_Priority + " ParentParcelID = " + parcel.Id + sbp;
                                        if (parcel.ParentParcelID != null)
                                            sql = "SELECT * from Parcel WHERE " + CC_Priority + " (ParentParcelID = " + parcel.ParentParcelID + " OR Id = " + parcel.ParentParcelID + ")" + sbp;
                                        getData(sql, [], function (parcels) {
                                            var formmap = function () {
                                                parcels.forEach(function (pd, index) {
                                                    childpresent = true;
                                                    var visibility = pd.Reviewed == 'true' ? 'display: none;' : ''
                                                    pd ? ShowAlternateField == "True" ? ShowKeyValue1 == "True" ? key = pd.KeyValue1 + " | " + eval("pd." + AlternateKey) : eval("pd." + AlternateKey) ? key = eval("pd." + AlternateKey) : isBPPUser ? key = pd.KeyValue1 : key = eval("pd." + AlternateKey) : key = pd.KeyValue1 : key = '';
                                                    var NewId = clientSettings["PersonalPropertyMapShowField"] ? pd[clientSettings["PersonalPropertyMapShowField"]] : key
                                                    if (NewId) key = NewId;
                                                    var idbpp = 'bppId' + index;
                                                    if (pd.Reviewed == 'true')
                                                        html += "<tr><td><b>" + key + "</b><br>" + pd.StreetAddress + "</td><td style='padding:4px;min-width:90px;'><br><a  class='new-link-button' style='padding:2px 7px 2px 7px;font-size:10pt;' onclick='showParcel(" + pd.CC_ParcelId + ",null,true,true);infoWindow.close()'>Open Parcel</a><a class='macmap new-link-button' style='" + visibility + " padding:2px 7px 2px 7px;font-size:10pt;margin-left:10px' onclick='loadParcel = true; markParcelAsComplete(" + pd.CC_ParcelId + ",null,null,dropActiveParcel,true);infoWindow.close()'>Mark as Complete</a></td></tr>"
                                                    else
                                                        html += "<tr><td><input type='checkbox' class='parcelbpp' Id=" + idbpp + " value=" + pd.Id + " key='" + key.replace(/'/g, "~") + "' style='" + visibility + "'>&nbsp<b>" + key + "</b><br>" + pd.StreetAddress + "</td><td style='padding:4px;min-width:90px;'><br><a  class='new-link-button' style='padding:2px 7px 2px 7px;font-size:10pt;' onclick='showParcel(" + pd.CC_ParcelId + ",null,true,true);infoWindow.close()'>Open Parcel</a><a class='macmap new-link-button' style='" + visibility + " padding:2px 7px 2px 7px;font-size:10pt;margin-left:10px' onclick='loadParcel = true; markParcelAsComplete(" + pd.CC_ParcelId + ",null,null,dropActiveParcel,true);infoWindow.close()'>Mark as Complete</a></td></tr>"
                                                });
                                                var undownloadedParcelsSql = "SELECT * from RPLinkedBPPs WHERE (ParentParcelID = " + parcel.Id + " OR ParentParcelID = " + parcel.ParentParcelID + ")" + sbp;
                                                getData(undownloadedParcelsSql, [], function (parcels) {
                                                    parcels.forEach(function (pd) {
                                                        childpresent = true;
                                                        if (pd.KeyValue1 == '0') pd.KeyValue1 = pd.ClientROWUID;
                                                        pd ? ShowAlternateField == "True" ? ShowKeyValue1 == "True" ? key = pd.KeyValue1 + " | " + eval("pd." + AlternateKey) : eval("pd." + AlternateKey) ? key = eval("pd." + AlternateKey) : isBPPUser ? key = pd.KeyValue1 : key = eval("pd." + AlternateKey) : key = pd.KeyValue1 : key = '';
                                                        var NewId = clientSettings["PersonalPropertyMapShowField"] ? pd[clientSettings["PersonalPropertyMapShowField"]] : key
                                                        if (NewId) key = NewId;
                                                        html += "<tr><td><b>" + key + "</b><br>" + parcel.StreetAddress + "</td></tr>"
                                                    });
                                                    html += "</table>";
                                                    if (!childpresent) {
                                                        RealPropertyOSM(parcel, gvisibility, key);
                                                        return
                                                    }
                                                    parcelMapMarkers[parcel.Id].bindPopup(html).openPopup();
                                                    var popupContent = $('.leaflet-popup-content'), originalWidth = popupContent.width();
                                                    popupContent.css('width', 'max-content'); var newWidth = popupContent.width(), offset = (originalWidth - newWidth) / 2;
                                                    if (offset) $('.leaflet-popup-tip-container').css('margin-left', offset - 20 + 'px');
                                                    else $('.leaflet-popup-tip-container').css('margin-left', -80 + 'px');

                                                    //$('.leaflet-popup-content').css('width', 'max-content');
                                                    setTimeout(function () {
                                                        $('.parcelbpp').unbind(touchClickEvent);
                                                        $('.parcelbpp').bind(touchClickEvent, function () {
                                                            var bb = $(this).attr('Id');
                                                            if (!document.getElementById(bb).checked) {
                                                                document.getElementById(bb).checked = false
                                                            } else {
                                                                document.getElementById(bb).checked = true
                                                            }
                                                        });
                                                    }, 1000)
                                                    return;
                                                });
                                            }
                                            if (parcel.ParentParcelID != null) {
                                                if (parcels.findIndex(function (x) {
                                                    return x.Id == parcel.ParentParcelID
                                                }) > -1) {
                                                    var rpParcel = parcels.splice(parcels.findIndex(function (x) {
                                                        return x.Id == parcel.ParentParcelID
                                                    }), 1)[0];
                                                    rpParcel ? ShowAlternateField == "True" ? ShowKeyValue1 == "True" ? key = rpParcel.KeyValue1 + " | " + eval("rpParcel." + AlternateKey) : eval("rpParcel." + AlternateKey) ? key = eval("rpParcel." + AlternateKey) : isBPPUser ? key = rpParcel.KeyValue1 : key = eval("rpParcel." + AlternateKey) : key = rpParcel.KeyValue1 : key = '';
                                                    let nbhdNo = rpParcel?.NeighborhoodNo ? rpParcel.NeighborhoodNo : (rpParcel?.CC_GROUP ? rpParcel.CC_GROUP : "-1");
                                                    html = "<div style='text-align: center; margin-bottom: 8px;'><input type='checkbox' class='parcelbppheader' mapIcon_Id=" + m.parcelData.Id + " id='selectall' nbhdNo=" + nbhdNo +" value=" + rpParcel.CC_ParcelId + " key='" + key.replace(/'/g, "~") + "' onclick='bindBPPScreenEvents()'>&nbsp<a style='font-size:15px;font-weight: bold;'>" + key + "</a>," + rpParcel.StreetAddress + "<a class='macmap1 new-link-button' style='padding:2px 7px 2px 7px;font-size:10pt;margin-left:10px; margin-right:20px; display: inline-block; background:#ffaf4d;color:white' onclick='multipleMAC(true);infoWindow.close();'>Mark As Complete</a>&nbsp&nbsp</div><table class='bppmap'>"
                                                    formmap();
                                                } else {
                                                    var ParcelsSql = "SELECT * from RPLinkedBPPs WHERE Id = " + parcel.ParentParcelID + sbp;
                                                    getData(ParcelsSql, [], function (PParcel) {
                                                        var ParentParcel = PParcel[0];
                                                        ParentParcel ? ShowAlternateField == "True" ? ShowKeyValue1 == "True" ? key = ParentParcel.KeyValue1 + " | " + eval("ParentParcel." + AlternateKey) : eval("ParentParcel." + AlternateKey) ? key = eval("ParentParcel." + AlternateKey) : isBPPUser ? key = ParentParcel.KeyValue1 : key = eval("ParentParcel." + AlternateKey) : key = ParentParcel.KeyValue1 : key = '';
                                                        let nbhdNo = ParentParcel?.NeighborhoodNo ? ParentParcel.NeighborhoodNo : (ParentParcel?.CC_GROUP ? ParentParcel.CC_GROUP : "-1");
                                                        html = "<div style='text-align: center; margin-bottom: 8px;'><input type='checkbox' class='parcelbppheader' mapIcon_Id=" + m.parcelData.Id + " id='selectall' nbhdNo=" + nbhdNo +" value=" + ParentParcel.Id + " key='" + key.replace(/'/g, "~") + "' onclick='bindBPPScreenEvents()'>&nbsp<a style='font-size:15px;font-weight: bold;'>" + key + "</a>," + ParentParcel.StreetAddress + "<a class='macmap1 new-link-button' style='padding:2px 7px 2px 7px;font-size:10pt;margin-left:10px; margin-right:20px; display: inline-block; background:#ffaf4d;color:white' onclick='multipleMAC(true);infoWindow.close();'>Mark As Complete</a>&nbsp&nbsp</div><table class='bppmap'>"
                                                        formmap();
                                                    });
                                                }
                                            } else {
                                                let nbhdNo = parcel?.NeighborhoodNo ? parcel.NeighborhoodNo : (parcel?.CC_GROUP ? parcel.CC_GROUP : (m?.parcelData?.CC_GROUP ? m.parcelData.CC_GROUP: "-1"));
                                                html = "<div style='text-align: center; margin-bottom: 8px;'><input type='checkbox' class='parcelbppheader rpMAC' mapIcon_Id=" + m.parcelData.Id + " id='selectall' nbhdNo=" + nbhdNo +" mac =" + parcel.Reviewed + " value=" + parcel.CC_ParcelId + " key='" + key.replace(/'/g, "~") + "' onclick='bindBPPScreenEvents()'>&nbsp<a style='font-size:15px;font-weight: bold;' onclick='showParcel(" + parcel.CC_ParcelId + ",null,true);infoWindow.close();'>" + key + "</a>," + parcel.StreetAddress + "<a class='macmap1 new-link-button' style='padding:2px 7px 2px 7px;font-size:10pt;margin-left:10px; margin-right:20px; display: inline-block; background:#ffaf4d;color:white' onclick='multipleMAC(true);infoWindow.close();'>Mark As Complete</a>&nbsp&nbsp</div><table class='bppmap'>"
                                                formmap();
                                            }
                                        });
                                        return;
                                    }
                                    else {
                                        if (m && m.parcelData && m.parcelData.gisList) {
                                            parcel.gisList = [];
                                            parcel.gisList = m.parcelData.gisList;
                                        }
                                        RealPropertyOSM(parcel, gvisibility, key);
                                    }
                                    if (ShowBoundaries != true) {
                                        OsMap.removeLayer(polygonlayer);
                                        polygonlayer = new L.FeatureGroup();
                                        parcel.ShowOnMap(false);
                                    }
                                });
                            });
                            markerlayer.addLayer(parcelMapMarkers[p.Id.toString()]);

                        } else {
                            google.maps.event.addListener(parcelMapMarkers[p.Id.toString()], touchClickEvent, function () {
                                //event.preventDefault();
                                var m = this;
                                getParcelWithMap(this.parcelData.Id, function (parcel) {
                                    if (parcel.Reviewed == 'true')
                                        gvisibility = 'display: none;'
                                    else
                                        gvisibility = ''
                                    if (ShowBoundaries != true) {
                                        polygons.forEach(function (w) {
                                            w.setMap(null);
                                        });
                                        polygons = [];
                                        parcel.ShowOnMap(false);
                                    }
                                    var childpresent = false;
                                    parcel ? ShowAlternateField == "True" ? ShowKeyValue1 == "True" ? key = parcel.KeyValue1 + " | " + eval("parcel." + AlternateKey) : eval("parcel." + AlternateKey) ? key = eval("parcel." + AlternateKey) : isBPPUser ? key = parcel.KeyValue1 : key = eval("parcel." + AlternateKey) : key = parcel.KeyValue1 : key = '';
                                    if (isBPPUser && (parcel.IsRPProperty == "true" || parcel.ParentParcelID != null)) {
                                        let sbp = clientSettings?.SortBppParcelMAMapView == '1' ? " ORDER BY CASE WHEN StreetNumber IS NULL THEN 1 ELSE 0 END, StreetNumber, CASE WHEN StreetName IS NULL THEN 1 ELSE 0 END, StreetName, CASE WHEN StreetNameSuffix IS NULL THEN 1 ELSE 0 END, StreetNameSuffix,CASE WHEN StreetUnit IS NULL THEN 1 ELSE 0 END, StreetUnit" : "";
                                        var html = '';
                                        var sql = "SELECT * from Parcel WHERE " + CC_Priority + " ParentParcelID = " + parcel.Id + sbp;
                                        if (parcel.ParentParcelID != null)
                                            sql = "SELECT * from Parcel WHERE " + CC_Priority + " (ParentParcelID = " + parcel.ParentParcelID + " OR Id = " + parcel.ParentParcelID + ")" + sbp;
                                        getData(sql, [], function (parcels) {
                                            var formmap = function () {
                                                parcels.forEach(function (pd, index) {
                                                    childpresent = true;
                                                    var visibility = pd.Reviewed == 'true' ? 'display: none;' : ''
                                                    pd ? ShowAlternateField == "True" ? ShowKeyValue1 == "True" ? key = pd.KeyValue1 + " | " + eval("pd." + AlternateKey) : eval("pd." + AlternateKey) ? key = eval("pd." + AlternateKey) : isBPPUser ? key = pd.KeyValue1 : key = eval("pd." + AlternateKey) : key = pd.KeyValue1 : key = '';
                                                    var NewId = clientSettings["PersonalPropertyMapShowField"] ? pd[clientSettings["PersonalPropertyMapShowField"]] : key
                                                    if (NewId) key = NewId;
                                                    var idbpp = 'bppId' + index;
                                                    if (pd.Reviewed == 'true')
                                                        html += "<tr><td><b>" + key + "</b><br>" + pd.StreetAddress + "</td><td style='padding:4px;min-width:90px;vertical-align:top;'><a  class='new-link-button' style='padding:2px 7px 2px 7px;font-size:10pt;' onclick='showParcel(" + pd.CC_ParcelId + ",null,true,true);infoWindow.close()'>Open Parcel</a><a class='macmap new-link-button' style='" + visibility + " padding:2px 7px 2px 7px;font-size:10pt;margin-left:10px' onclick='loadParcel = true; markParcelAsComplete(" + pd.CC_ParcelId + ",null,null,dropActiveParcel,true);infoWindow.close()'>Mark as Complete</a></td></tr>"
                                                    else
                                                        html += "<tr><td><input type='checkbox' class='parcelbpp' Id=" + idbpp + " value=" + pd.Id + " key='" + key.replace(/'/g, "~") + "' style='" + visibility + "'>&nbsp<b>" + key + "</b><br>" + pd.StreetAddress + "</td><td style='padding:4px;min-width:215px;vertical-align:top;'><a  class='new-link-button' style='padding:2px 7px 2px 7px;font-size:10pt;' onclick='showParcel(" + pd.CC_ParcelId + ",null,true,true);infoWindow.close()'>Open Parcel</a><a class='macmap new-link-button' style='" + visibility + " padding:2px 7px 2px 7px;font-size:10pt;margin-left:10px' onclick='loadParcel = true; markParcelAsComplete(" + pd.CC_ParcelId + ",null,null,dropActiveParcel,true);infoWindow.close()'>Mark as Complete</a></td></tr>"
                                                });

                                                var undownloadedParcelsSql = "SELECT * from RPLinkedBPPs WHERE (ParentParcelID = " + parcel.Id + " OR ParentParcelID = " + parcel.ParentParcelID + ")" + sbp;
                                                getData(undownloadedParcelsSql, [], function (parcels) {
                                                    parcels.forEach(function (pd) {
                                                        childpresent = true;
                                                        if (pd.KeyValue1 == '0') pd.KeyValue1 = pd.ClientROWUID;
                                                        pd ? ShowAlternateField == "True" ? ShowKeyValue1 == "True" ? key = pd.KeyValue1 + " | " + eval("pd." + AlternateKey) : eval("pd." + AlternateKey) ? key = eval("pd." + AlternateKey) : isBPPUser ? key = pd.KeyValue1 : key = eval("pd." + AlternateKey) : key = pd.KeyValue1 : key = '';
                                                        var NewId = clientSettings["PersonalPropertyMapShowField"] ? pd[clientSettings["PersonalPropertyMapShowField"]] : key
                                                        if (NewId) key = NewId;
                                                        html += "<tr><td><b>" + key + "</b><br>" + parcel.StreetAddress + "</td></tr>"
                                                    });
                                                    html += "</table>";
                                                    if (!childpresent) {
                                                        RealPropertyGMap(parcel, m, gvisibility, key);
                                                        return;
                                                    }
                                                    infoWindow.setContent(html);
                                                    infoWindow.open(map, m);
                                                    setTimeout(function () {
                                                        $('.parcelbpp').unbind(touchClickEvent);
                                                        $('.parcelbpp').bind(touchClickEvent, function () {
                                                            var bb = $(this).attr('Id');
                                                            if (!document.getElementById(bb).checked) {
                                                                document.getElementById(bb).checked = false
                                                            } else {
                                                                document.getElementById(bb).checked = true
                                                            }
                                                        });
                                                    }, 1000)
                                                });
                                            }
                                            if (parcel.ParentParcelID != null) {
                                                if (parcels.findIndex(function (x) {
                                                    return x.Id == parcel.ParentParcelID
                                                }) > -1) {
                                                    var rpParcel = parcels.splice(parcels.findIndex(function (x) {
                                                        return x.Id == parcel.ParentParcelID
                                                    }), 1)[0];
                                                    rpParcel ? ShowAlternateField == "True" ? ShowKeyValue1 == "True" ? key = rpParcel.KeyValue1 + " | " + eval("rpParcel." + AlternateKey) : eval("rpParcel." + AlternateKey) ? key = eval("rpParcel." + AlternateKey) : isBPPUser ? key = rpParcel.KeyValue1 : key = eval("rpParcel." + AlternateKey) : key = rpParcel.KeyValue1 : key = '';
                                                    let nbhdNo = rpParcel?.NeighborhoodNo ? rpParcel.NeighborhoodNo : (rpParcel?.CC_GROUP ? rpParcel.CC_GROUP : "-1");
                                                    html = "<div style='text-align: center; margin-bottom: 8px;display:flex;'><div><input type='checkbox' class='parcelbppheader' mapIcon_Id=" + m.parcelData.Id + " id='selectall' nbhdNo=" + nbhdNo +" value=" + rpParcel.CC_ParcelId + " key='" + key.replace(/'/g, "~") + "' onclick='bindBPPScreenEvents()'>&nbsp</div><div style='flex:2;'><a style='font-size:15px;font-weight: bold;'>" + key + "</a>," + rpParcel.StreetAddress + "</div><div><a class='macmap1 new-link-button' style='padding:2px 7px 2px 7px;font-size:10pt;margin-left:10px; margin-right:20px; display: inline-block; background:#ffaf4d;color:white' onclick='multipleMAC(true);infoWindow.close();'>Mark As Complete</a>&nbsp&nbsp</div></div><table class='bppmap'>"
                                                    formmap();
                                                } else {
                                                    var ParcelsSql = "SELECT * from RPLinkedBPPs WHERE Id = " + parcel.ParentParcelID + sbp;
                                                    getData(ParcelsSql, [], function (PParcel) {
                                                        var ParentParcel = PParcel[0];
                                                        ParentParcel ? ShowAlternateField == "True" ? ShowKeyValue1 == "True" ? key = ParentParcel.KeyValue1 + " | " + eval("ParentParcel." + AlternateKey) : eval("ParentParcel." + AlternateKey) ? key = eval("ParentParcel." + AlternateKey) : isBPPUser ? key = ParentParcel.KeyValue1 : key = eval("ParentParcel." + AlternateKey) : key = ParentParcel.KeyValue1 : key = '';
                                                        let nbhdNo = ParentParcel?.NeighborhoodNo ? ParentParcel.NeighborhoodNo : (ParentParcel?.CC_GROUP ? ParentParcel.CC_GROUP: "-1");
                                                        html = "<div style='text-align: center; margin-bottom: 8px;display:flex;'><div><input type='checkbox' class='parcelbppheader' mapIcon_Id=" + m.parcelData.Id + " id='selectall' nbhdNo=" + nbhdNo +" value=" + ParentParcel.Id + " key='" + key.replace(/'/g, "~") + "' onclick='bindBPPScreenEvents()'>&nbsp</div><div style='flex:2;'><a style='font-size:15px;font-weight: bold;'>" + key + "</a>," + ParentParcel.StreetAddress + "</div><div><a class='macmap1 new-link-button' style='padding:2px 7px 2px 7px;font-size:10pt;margin-left:10px; margin-right:20px; display: inline-block; background:#ffaf4d;color:white' onclick='multipleMAC(true);infoWindow.close();'>Mark As Complete</a>&nbsp&nbsp</div></div><table class='bppmap'>"
                                                        formmap();
                                                    });
                                                }
                                            } else {
                                                let nbhdNo = parcel?.NeighborhoodNo ? parcel.NeighborhoodNo : (parcel?.CC_GROUP ? parcel.CC_GROUP : (m?.parcelData?.CC_GROUP ? m.parcelData.CC_GROUP: "-1"));
                                                html = "<div style='text-align: center; margin-bottom: 8px;display:flex;'><div><input type='checkbox' class='parcelbppheader rpMAC' mapIcon_Id=" + m.parcelData.Id + " id='selectall' mac =" + parcel.Reviewed + " nbhdNo=" + nbhdNo +" value=" + parcel.CC_ParcelId + " key='" + key.replace(/'/g, "~") + "' onclick='bindBPPScreenEvents()'>&nbsp</div><div style='flex:2;'><a style='font-size:15px;font-weight: bold;' onclick='showParcel(" + parcel.CC_ParcelId + ",null,true);infoWindow.close();'>" + key + "</a>," + parcel.StreetAddress + "</div><div><a class='macmap1 new-link-button' style='padding:2px 7px 2px 7px;font-size:10pt;margin-left:10px; margin-right:20px; display: inline-block; background:#ffaf4d;color:white' onclick='multipleMAC(true);infoWindow.close();'>Mark As Complete</a>&nbsp&nbsp</div></div><table class='bppmap'>"
                                                formmap();
                                            }
                                        });
                                        return;
                                    }
                                    else {
                                        if (m && m.parcelData && m.parcelData.gisList) {
                                            parcel.gisList = [];
                                            parcel.gisList = m.parcelData.gisList;
                                        }
                                        RealPropertyGMap(parcel, m, gvisibility, key);
                                    }

                                });
                            });
                        }

                    } else {
                        if (!UsingOSM) {
                            if (parcelMapMarkers[p.Id.toString()].getMap() == null) {
                                parcelMapMarkers[p.Id.toString()].setMap(map);
                            }
                        } else {
                            markerlayer.addLayer(parcelMapMarkers[p.Id.toString()]);
                            currentHeatMapField = null;
                            currentHeatMapFieldId = null;
                            currentHeatMapFieldValue = null;
                        }
                    }

                }
                if (UsingOSM) {
                    if (HeatLayer == true) {
                        if (!currentHeatMapField) OsMap.removeLayer(heatMapLayer);
                        else {
                            if (ShowBoundaries == true) OsMap.addLayer(polygonlayer);
                            //OsMap.removeLayer(heatMapLayer);
                            OsMap.addLayer(heatMapLayer);
                            $('.heatmap-items').val(currentHeatMapField);
                            if (!fromZoom)
                                setLegend(currentHeatMapFieldId);
                            HeatMapRefreshed = false

                        };
                        OsMap.removeLayer(markerlayer);
                    } else {
                        if (ShowBoundaries == true) OsMap.addLayer(polygonlayer);
                        OsMap.addLayer(markerlayer);
                    }
                }
                if (callback) callback();
            }
            resizeMap();
        });
    });
}

function RealPropertyGMap(parcel, m, visibility, key) {
    let html = "<table style='width:100%;'>";

    function naturalSort(a, b) {
        return a.localeCompare(b, undefined, { numeric: true, sensitivity: 'base' });
    }

    function edfn() {
        let uniqueParcelIds = [];
        parcel.gisList.forEach((gl, i) => {
            if (uniqueParcelIds.includes(gl.CC_ParcelId)) {
                return;
            }
            uniqueParcelIds.push(gl.CC_ParcelId);
            let v = gl.visibility ? gl.visibility : (gl.Reviewed == 'true' ? 'display: none;' : ''), key = '', mac = JSON.stringify({ pid: parcel.gisList[0].CC_ParcelId, cid: gl.CC_ParcelId });
            gl.key ? key = gl.key : (ShowAlternateField == "True" ? ShowKeyValue1 == "True" ? key = gl.KeyValue1 + " | " + eval("gl." + AlternateKey) : eval("gl." + AlternateKey) ? key = eval("gl." + AlternateKey) : isBPPUser ? key = gl.KeyValue1 : key = eval("gl." + AlternateKey) : key = gl.KeyValue1);
            html += "<td style='vertical-align: top;'><b>" + key + "</b><br>" + gl.StreetAddress;
            if (clientSettings?.SortMultiParcelByMultiParcelField == "1" && clientSettings?.MultiParcelMAMapViewField != "") {
                let field = clientSettings.MultiParcelMAMapViewField.split(".")[1];
                let fieldValue = gl[field] || 'N/A';
                html += "<br><b>" + field + ":</b> " + fieldValue;
            }
            html += "</td>";

            html += "<td style='padding:4px; min-width:90px; vertical-align:top;'><a class='new-link-button' style='padding:2px 7px 2px 7px; font-size:10pt;' onclick='showParcel(" + gl.CC_ParcelId + ",null,true,true);infoWindow.close()'>Open Parcel</a><a class='macmap new-link-button' style='" + v + " padding:2px 7px 2px 7px; font-size:10pt;margin-left:10px' onclick='loadParcel = true; markParcelAsComplete(" + gl.CC_ParcelId + ", updateMacStatus, null, dropActiveParcel, true," + mac + "); infoWindow.close()'>Mark as Complete</a></td></tr>";
        });
        parcel.gisList = parcel.gisList.filter((x) => { return x.CC_ParcelId != parcel.CC_ParcelId });

        html += "</table>"
        infoWindow.setContent(html);
        infoWindow.open(map, m);
    }

    if (!parcel.gisList || isBPPUser) {
        if (parcel.CautionMessage != null) {
            html += "<tr><td style='background-color:rgb(255, 223, 0); color: rgb(0, 0, 0);padding: 3px 35px;font-weight: bold;'> CAUTION ALERT </td></tr>"
        }
        var extraInfo = clientSettings['InfoOnMap'] || '';
        if (parcel.RP_linked_acct)
            var RpParcel = "Real Parcel: " + parcel.RP_linked_acct + ", "
        else
            var RpParcel = '';
        html += "<tr><td style='width:170px;'><b>" + RpParcel + "</b>Parcel#: " + key + "</td></tr>"

        if (extraInfo != '') html += getMapInfo(extraInfo, parcel);

        html += "<tr><td>" + (parcel.StreetAddress ? parcel.StreetAddress : '') + "</td></tr>"
        html += "<tr><td style='padding:4px;'><button style='padding:2px 7px 2px 7px;font-size:10pt;' onclick='showParcel(" + parcel.CC_ParcelId + ", null, true); infoWindow.close()'>Open Parcel</button><button class='macmap' style='" + visibility + " padding:2px 7px 2px 7px; font-size:10pt; margin-left:10px' onclick='loadParcel = true; markParcelAsComplete(" + parcel.CC_ParcelId + ", null, null, dropActiveParcel,true); infoWindow.close()'>Mark as Complete</button></td></tr>"

        html += "</table>"
        infoWindow.setContent(html);
        infoWindow.open(map, m);
    }
    else {
        html = "<table style='width:100%; border-spacing: 0px 12px;'>"
        parcel.gisList.unshift({ StreetAddress: (parcel.StreetAddress ? parcel.StreetAddress : ''), CC_ParcelId: parcel.CC_ParcelId, key: key, visibility: visibility });
        if (clientSettings?.SortMultiParcelByMultiParcelField == "1" && clientSettings?.MultiParcelMAMapViewField != "") {

            let fname = clientSettings.MultiParcelMAMapViewField.split(".")[1], fl = getDataField(fname);
            if (fl) {
                let CC_pldIds = parcel.gisList.map((x) => { return x.CC_ParcelId }).toString();
                let selectQuery = 'SELECT CC_ParcelId, ' + fname + ' FROM parcel WHERE CC_ParcelId IN (' + CC_pldIds + ')';
                getData(selectQuery, [], (res) => {
                    var plist = [];
                    res.forEach((z) => {
                        let parcelData = parcel.gisList.filter((y) => y.CC_ParcelId == z.CC_ParcelId)[0];
                        if (clientSettings?.SortMultiParcelByMultiParcelField == "1" && clientSettings?.MultiParcelMAMapViewField != "") {
                            parcelData[fname] = z[fname];
                            plist.push(parcelData);
                        }
                    });
                    plist.sort((a, b) => naturalSort(a[fname], b[fname]));
                    parcel.gisList = plist;
                    edfn();
                });
            }
            else if (clientSettings?.SortMultiParcelMAMapView == "1") {
                parcel.gisList.sort((a, b) => a.StreetAddress.localeCompare(b.StreetAddress));
                edfn();
            }
        }
        else if (clientSettings?.SortMultiParcelMAMapView == "1") {
            parcel.gisList.sort((a, b) => a.StreetAddress.localeCompare(b.StreetAddress));
            edfn();
        }
    }
}

function RealPropertyOSM(parcel, visibility, key) {
    let html = "<table style='width:100%;'>", gt = false;;

    function naturalSort(a, b) {
        return a.localeCompare(b, undefined, { numeric: true, sensitivity: 'base' });
    }

    function edfn1() {
        parcel.gisList.forEach((gl, i) => {
            let v = gl.visibility ? gl.visibility : (gl.Reviewed == 'true' ? 'display: none;' : ''), key = '', mac = JSON.stringify({ pid: parcel.gisList[0].CC_ParcelId, cid: gl.CC_ParcelId, osmMap: '1' });
            gl.key ? key = gl.key : (ShowAlternateField == "True" ? ShowKeyValue1 == "True" ? key = gl.KeyValue1 + " | " + eval("gl." + AlternateKey) : eval("gl." + AlternateKey) ? key = eval("gl." + AlternateKey) : isBPPUser ? key = gl.KeyValue1 : key = eval("gl." + AlternateKey) : key = gl.KeyValue1);
            html += "<td style='vertical-align: top;'><b>" + key + "</b><br>" + gl.StreetAddress;
            if (clientSettings?.SortMultiParcelByMultiParcelField == "1" && clientSettings?.MultiParcelMAMapViewField != "") {
                let field = clientSettings.MultiParcelMAMapViewField.split(".")[1];
                let fieldValue = gl[field] || 'N/A';
                html += "<br><b>" + field + ":</b> " + fieldValue;
            }
            html += "</td>";

            html += "<td style='padding:4px;min-width:90px;vertical-align:top;'><a  class='new-link-button' style='padding:2px 7px 2px 7px;font-size:10pt;' onclick = 'showParcel(" + gl.CC_ParcelId + ", null, true, true); OsMap.closePopup();'>Open Parcel</a><a class='macmap new-link-button' style='" + v + " padding:2px 7px 2px 7px;font-size:10pt;margin-left:10px' onclick='loadParcel = true; markParcelAsComplete(" + gl.CC_ParcelId + ", updateMacStatus, null, dropActiveParcel, true, " + mac + "); OsMap.closePopup()'>Mark as Complete</a></td></tr>";
        });
        parcel.gisList = parcel.gisList.filter((x) => { return x.CC_ParcelId != parcel.CC_ParcelId });
         
        html += "</table>";
        parcelMapMarkers[parcel.Id].bindPopup(html).openPopup();
        if (gt) $('.leaflet-popup-content').css('width', 'max-content');
    }
    if (!parcel.gisList || isBPPUser) {
        if (parcel.CautionMessage != null) {
            html += "<tr><td style='background-color:rgb(255, 223, 0); color: rgb(0, 0, 0);padding: 3px 35px;font-weight: bold;'> CAUTION ALERT </td></tr>"
        }
        var extraInfo = clientSettings['InfoOnMap'] || '';
        if (parcel.RP_linked_acct)
            var RpParcel = "Real Parcel: " + parcel.RP_linked_acct + ", "
        else
            var RpParcel = '';
        html += "<tr><td style='width:170px;'><b>" + RpParcel + "</b>Parcel#: " + key + "</td></tr>"

        if (extraInfo != '') html += getMapInfo(extraInfo, parcel);

        html += "<tr><td>" + (parcel.StreetAddress ? parcel.StreetAddress : '') + "</td></tr>"
        html += "<tr><td style='padding:4px;'><button  style='padding:2px 7px 2px 7px;font-size:10pt;' onclick='showParcel(" + parcel.CC_ParcelId + ",null,true);OsMap.closePopup()'>Open Parcel</button><button class='macmap' style='" + visibility + " padding:2px 7px 2px 7px;font-size:10pt;margin-left:10px' onclick='loadParcel = true; markParcelAsComplete(" + parcel.CC_ParcelId + ",null,null,dropActiveParcel,true);OsMap.closePopup()'>Mark as Complete</button></td></tr>"

        html += "</table>";
        parcelMapMarkers[parcel.Id].bindPopup(html).openPopup();
        if (gt) $('.leaflet-popup-content').css('width', 'max-content');
    }
    else {
        gt = true;
        html = "<table style='width:100%; border-spacing: 0px 12px;'>"
        parcel.gisList.unshift({ StreetAddress: (parcel.StreetAddress ? parcel.StreetAddress : ''), CC_ParcelId: parcel.CC_ParcelId, key: key, visibility: visibility });
        if (clientSettings?.SortMultiParcelByMultiParcelField == "1" && clientSettings?.MultiParcelMAMapViewField != "") {

            let fname = clientSettings.MultiParcelMAMapViewField.split(".")[1], fl = getDataField(fname);
            if (fl) {
                let CC_pldIds = parcel.gisList.map((x) => { return x.CC_ParcelId }).toString();
                let selectQuery = 'SELECT CC_ParcelId, ' + fname + ' FROM parcel WHERE CC_ParcelId IN (' + CC_pldIds + ')';
                getData(selectQuery, [], (res) => {
                    var plist = [];
                    res.forEach((z) => {
                        let parcelData = parcel.gisList.filter((y) => y.CC_ParcelId == z.CC_ParcelId)[0];
                        if (clientSettings?.SortMultiParcelByMultiParcelField == "1" && clientSettings?.MultiParcelMAMapViewField != "") {
                            parcelData[fname] = z[fname];
                            plist.push(parcelData);
                        }
                    });
                    plist.sort((a, b) => naturalSort(a[fname], b[fname]));
                    parcel.gisList = plist;
                    edfn1();
                });
            }
            else if (clientSettings?.SortMultiParcelMAMapView == "1") {
                parcel.gisList.sort((a, b) => a.StreetAddress.localeCompare(b.StreetAddress));
                edfn1();
            }
        }
        else if (clientSettings?.SortMultiParcelMAMapView == "1") {
            parcel.gisList.sort((a, b) => a.StreetAddress.localeCompare(b.StreetAddress));
            edfn1();
        }
    }
}

function getAggField(aggr_field) {
    let aggr_Setting = aggregate_fields.filter((agr) => { return ((agr.FunctionName + '_' + agr.TableName + '_' + agr.FieldName) == aggr_field); })[0], df = null;
    if (aggr_Setting) df = getDataField(aggr_Setting.FieldName, aggr_Setting.TableName);
    return df;
}

function getMapInfo(mapInfo, p) {
    let htm = '', mapInfos = mapInfo.split(':');

    mapInfos.forEach((mi) => {
        let val = '', mapins = /(.*?)\[(.*?)\]/.exec(mi.trim()), field = mapins?.[1] ? mapins[1] : mi.trim(), lbl = field, f = getDataField(field), ft = null, lkdesc = false; val = p[field];

        if (mapins?.[2]) {
            let sp = mapins[2].trim().split(',');
            lbl = sp[0].trim() ? sp[0].trim() : lbl; ft = sp[1] ? sp[1].trim() : null;
            lkdesc = ((ft && ft.toString().toLowerCase() == 'lookupdesc') || (sp[2] && sp[2].toString().toLowerCase() == 'lookupdesc')) ? true : false;
        }

        if (field.startsWith('SUM_') || field.startsWith('AVG_') || field.startsWith('COUNT_') || field.startsWith('MAX_') || field.startsWith('MIN_') || field.startsWith('FIRST_') || field.startsWith('LAST_') || field.startsWith('MEDIAN_')) {
            f = getAggField(field); field = f.Name;
        }

        if (f?.InputType == '5') {
            let desc = evalLookup(field, val, lkdesc, f);
            val = ((desc == '???') || (desc == '') || (desc == null)) ? val : desc;
        }

        if (val && val.format && ft) {
            ft = ft.replace('~', ',');
            try {
                val = val.format(ft);
            }
            catch (ex) { }           
            if (val == 'NaN' || val == '$NaN') val = '***';
        }

        if (val && val != '***') htm += "<tr><td>" + lbl + ": " + val + "</td></tr>";
    });

    return htm;
}

function drawOfflineMap() {
    if (OsMap && StreetMapPoints.length > 1) {
        if (offlineLayer.getLayers() == '' || labelLayer.getLayers() == '') {
            var labelpos;
            for (var x in StreetMapPoints) {
                var stringPoints = StreetMapPoints[x].Points;
                var tempPoints = JSON.parse("[" + stringPoints + "]");
                labelpos = tempPoints.length > 1 ? Math.floor(tempPoints.length / 2) : tempPoints.length;
                offlineLayer.addLayer(L.polyline(tempPoints, {
                    weight: 8,
                    color: '#FFA824'
                }));
                var oo = L.marker(tempPoints[labelpos - 1], {
                    icon: L.divIcon({
                        className: 'leaflet-label',
                        html: StreetMapPoints[x].Name,
                    })
                })
                oo.addTo(labelLayer);
            }
        }
        OsMap.setZoom(18);
    }

}

function clearAllLayers() {
    if (OsMap) {
        OsMap.eachLayer(function (layer) {
            OsMap.removeLayer(layer);
        });
    }
}

function loadOfflineMap() {
    if (!StreetLayer) {
        OsMap.hasLayer(offlineLayer) == true ? OsMap.removeLayer(offlineLayer) : '';
        OsMap.hasLayer(labelLayer) == true ? OsMap.removeLayer(labelLayer) : '';
    } else {
        OsMap.hasLayer(offlineLayer) == false ? offlineLayer.addTo(OsMap) : '';
        OsMap.hasLayer(labelLayer) == false ? labelLayer.addTo(OsMap) : '';
    }
}

function loadAerialLayer() {
    clearLayers();
    if (StreetView) {
        //vin-str if (google) {
        //gmaptype = 'HYBRID';
        //  googleLayer = new L.Google(gmaptype);
        // OsMap.addLayer(googleLayer);
        // }
        googleHybrid.addTo(OsMap);
    } else if (CLayer && clientSettings.EnableNearmap && clientSettings.EnableNearmap.includes('MA') && UsingOSM) {
        nearmapStreets.addTo(OsMap);

    } else if (CLayer && clientSettings.EnableNearmapWMS && clientSettings.EnableNearmapWMS.includes('MA') && UsingOSM) {
        nearmapWMSStreets.addTo(OsMap);
    } else if (CLayer) {
        Aerial.addTo(OsMap);
    }

    loadOfflineMap();
}

function loadStreetLayer() {
    clearLayers();
    if (StreetView) {
        //vin -str if (google) {
        //  gmaptype = 'ROADMAP';
        // googleLayer = new L.Google(gmaptype);
        // OsMap.addLayer(googleLayer);
        // } vin- end
        googleStreets.addTo(OsMap);
    } else if (CLayer) {
        OSMStreet.addTo(OsMap);
    }
    loadOfflineMap();
}



//function _computeTravelRoute(parcellist, reference, callback) {
//    routing.routeUsingMapQuest(parcellist, callback)
//    log('Computing travel route ..');
//    var o = routing.sortParcelsByDistance(parcellist);
//    routing.calculateRoute(o.sortedList, reference, o.nearestParcel, 0, function () {
//        callback(o.nearestParcel, o.farthestParcel);
//    });
//}
function hideLegendIcon() {
    $('.heat-legend').hide();
}

function showLegendIcon() {
    $('.heat-legend').show();
}

function setLegend(currentHeatMapFieldId) {
    if (currentHeatMapFieldId != null && !HideLegend && HeatLayer) {
        var cHId = Heatmapfields.filter(function (f) {
            return f.FieldId == currentHeatMapFieldId
        })[0];
        var legendObj = Heatmaplookup.filter(function (x) {
            return x.HeatMapID == cHId.id
        });
        removeLegend();
        legend = L.control({
            position: 'bottomright'
        });
        legend.onAdd = function (OsMap) {
            var isAggrField = currentHeatMapField.startsWith('SUM_') || currentHeatMapField.startsWith('AVG_') || currentHeatMapField.startsWith('COUNT_') || currentHeatMapField.startsWith('MAX_') || currentHeatMapField.startsWith('MIN_') || currentHeatMapField.startsWith('FIRST_') || currentHeatMapField.startsWith('LAST_') || currentHeatMapField.startsWith('MEDIAN_') ;
            var formatter = isAggrField ? getDatatypeOfAggregateFields(currentHeatMapField).formatter : ccma.Data.Types[datafields[currentHeatMapFieldId].InputType].formatter;
            var div = L.DomUtil.create('div', 'infoLegend legend');
            formatter = formatter.replace(',', '~');
            var val1 = '${Value1:format,' + formatter + '}';
            var val2 = '${Value2:format,' + formatter + '}';
            if (cHId.ComparisonType == "2")
                div.innerHTML = '<table rowuid=${Rowuid} style="padding:1px;margin: 3px 0px 3px 0px;"><tr  ><td style=" width:30px;background:${Color}"></td><td>' + val1 + '</td><td> - ' + val2 + '</td></tr></table>';

            else
                div.innerHTML = '<table rowuid=${Rowuid} style="padding:1px; margin: 3px 0px 3px 0px;"><tr><td style=" width:30px;background:${Color}"></td><td>' + val1 + '</td></tr></table>';
            $(div).fillTemplate(legendObj);
            return div;
        };

        legend.addTo(OsMap);
    } else {
        removeLegend();
    }

}
var HideLegend = false;

function toggleLegend() {
    if (OsMap._controlCorners.bottomright.childNodes.length > 1) {
        HideLegend = true;
        $('.heat-legend').css('opacity', '0.7');
        removeLegend();
    } else {
        HideLegend = false;
        $('.heat-legend').css('opacity', '1');
        setLegend(currentHeatMapFieldId);

    }
}
function highlightLegendItem(val) {
    $('.infoLegend table').removeClass('legend-item-selected')
    var itemId = relatedItemFinder(currentHeatMapFieldId, val, true);
    if (itemId)
        $('.infoLegend table[rowuid="' + itemId + '"]').addClass("legend-item-selected")
}

function setHeatCircles(p, point, color, opacity, value) {

    var rad = 0,
        zoomLevel = OsMap.getZoom();
    if (zoomLevel >= 18)
        rad = 21;
    else if (zoomLevel == 17 || zoomLevel == 16 || zoomLevel == 15)
        rad = 12;
    else
        rad = 3;
    HeatMapIcons[p.Id.toString()] = L.circleMarker(point, {
        color: color,
        fillColor: color,
        fillOpacity: opacity,
        radius: rad
    });
    L.DomEvent.addListener(HeatMapIcons[p.Id.toString()], 'click', function (e) {
        getParcelWithMap(p.Id, function (p) {
            if (ShowBoundaries != true) {
                OsMap.removeLayer(polygonlayer);
                heatMapLayer.removeLayer(HeatMapIcons[p.Id.toString()]);
                polygonlayer = new L.FeatureGroup();
                p.ShowOnMap(false);
            }
            if (p.Reviewed == 'true') {
                visibility = 'display: none;'
            } else {
                visibility = '';
            }

            HeatMapIcons[p.Id.toString()].addTo(heatMapLayer);
            HeatMapIcons[p.Id.toString()].bindPopup("<table><tr><td>ParcelID : " + p.KeyValue1 + "</td></tr><tr><td>Value : " + value + "</td></tr> <tr><td style='padding:4px;'><button  style='padding:2px 7px 2px 7px;font-size:10pt;' onclick='showParcel(" + p.CC_ParcelId + ",null,true);OsMap.closePopup()'>Open Parcel</button><button class='macmap' style='" + visibility + " padding:2px 7px 2px 7px;font-size:10pt;margin-left:10px' onclick='loadParcel = true; markParcelAsComplete(" + p.CC_ParcelId + ",null,null,null,true);OsMap.closePopup()'>Mark as Complete</button></td></tr>").openPopup();

            heatMapLayer.addLayer(HeatMapIcons[p.Id.toString()]);
            highlightLegendItem(value)
        });

    });
    HeatMapIcons[p.Id.toString()].on('popupclose', function (e) {
        $('.infoLegend table').removeClass('legend-item-selected')
    });

}

function relatedItemFinder(fieldid, fieldvalue, legendItem) {
    var reqdObj = null;
    var cItem = Heatmapfields.filter(function (x) {
        return x.FieldId == fieldid
    });
    if (cItem.length > 0) {
        var cHeatmapId = cItem[0].id;
        var cCtype = cItem[0].ComparisonType;
        var clkptable = Heatmaplookup.filter(function (x) {
            return x.HeatMapID == cHeatmapId;
        });
        if (clkptable) {
            if (cCtype == "1") {
                reqdObj = clkptable.filter(function (x) {
                    return x.Value1 == fieldvalue;
                });
                if (reqdObj[0])
                    return reqdObj[0].Color;
                else
                    return null;
            }
            var agItem = Heatmapfields.filter(function (h) {
                return h.FieldId == fieldid;
            }); //agFields.filter(function (a) { return (a.FieldName == fieldName && a.TableName == sourceTable); });
            var aggregateItem = agItem.length > 0 ? agFields.filter(function (a) {
                return a.ROWUID == agItem[0].FieldId;
            }) : [];
            if (cCtype == "2") {
                var fieldtype = aggregateItem.length > 0 ? (Object.keys(datafields).map(function (x) {
                    return datafields[x]
                }).filter(function (x) {
                    return (x.Name == aggregateItem[0].FieldName && x.SourceTable == aggregateItem[0].TableName)
                })[0].InputType) : datafields[fieldid].InputType;
                switch (fieldtype) {
                    case 1:
                    case 3:
                    case 5:
                    case 6:
                    case 11:
                        fieldvalue = fieldvalue.toString();
                        reqdObj = clkptable.filter(function (x) {
                            return (x.Value1 <= fieldvalue && x.Value2 >= fieldvalue);
                        });
                        break;
                    case 4:
                        fieldvalue = new Date(fieldvalue);
                        reqdObj = clkptable.filter(function (x) {
                            return (new Date(x.Value1) <= fieldvalue && new Date(x.Value2) >= fieldvalue);
                        });
                        break;
                    default:
                        fieldvalue = safeParseFloat(fieldvalue);
                        reqdObj = clkptable.filter(function (x) {
                            return (safeParseFloat(x.Value1) <= fieldvalue && safeParseFloat(x.Value2) >= fieldvalue);
                        });

                }
                if (reqdObj[0] && legendItem)
                    return reqdObj[0].Rowuid;
                else if (reqdObj[0] && !legendItem)
                    return reqdObj[0].Color;
                else
                    return null;
            }
        }
    }
}

function uniqueGisParcels(res) {
    let pl = [];

    for (i in res) {
        let r = res[i], p = r.CC_Priority, gr = res.filter((x) => { return ((r.Id != x.Id) && (x.Latitude == r.Latitude && x.Longitude == r.Longitude)) }), rv = r.Reviewed; r.CC_current_priority = p; r.current_Reviewed = rv;

        if (gr.length > 0) {
            let pm = gr.filter((x) => { return (!x.Reviewed || x.Reviewed == 'false') }).map(function (y) { return Math.max(y.CC_Priority) });
            r.gisList = [];
            if (!r.Reviewed || r.Reviewed == 'false') pm.push(parseInt(p));
            gr.forEach((g) => {
                let ix = res.findIndex((x) => { return x.Id == g.Id });
                if (!g.Reviewed || g.Reviewed == 'false') rv = 'false';
                r.gisList.push({ StreetAddress: g.StreetAddress ? g.StreetAddress : '', CC_ParcelId: g.CC_ParcelId, Reviewed: g.Reviewed, KeyValue1: g.KeyValue1, [AlternateKey]: g[AlternateKey], CC_Priority: g.CC_Priority });
                if (ix > -1) res.splice(ix, 1);
            });

            r.Reviewed = rv;
            if (pm.length > 0) {
                r.CC_Priority = Math.max(...pm);
            }
        }

        pl.push(r);
    }

    return pl;
}

function updateMacStatus(ob) {
    if (ob && ob.pid && parcelMapMarkers && parcelMapMarkers[ob.pid.toString()]) {
        let pid = ob.pid.toString(), r = parcelMapMarkers[pid].parcelData;
        m = parcelMapMarkers[pid].parcelData && parcelMapMarkers[pid].parcelData.gisList ? parcelMapMarkers[pid].parcelData.gisList : null;

        if (m && ob.cid) {
            if (ob.cid == r.CC_ParcelId) {
                r.current_Reviewed = 'true';
            }
            else {
                let pl = m.filter((x) => { return x.CC_ParcelId == ob.cid }); pl[0].Reviewed = 'true';
            }

            let p = r.CC_current_priority ? r.CC_current_priority : r.CC_Priority, rv = r.current_Reviewed ? r.current_Reviewed : r.Reviewed,
                nr = m.filter((x) => { return (!x.Reviewed || x.Reviewed == 'false') });

            if (nr.length > 0 || rv == 'false') {
                if (rv == 'false')
                    nr.push({ CC_Priority: p, Reviewed: rv, CC_ParcelId: r.CC_ParcelId });

                let pc = nr.map(function (y) { return Math.max(y.CC_Priority) });
                r.CC_Priority = Math.max(...pc);
                let icon = getPriorityIcon({ Id: pid, CC_Priority: r.CC_Priority });
                if (icon) {
                    if (ob.osmMap) parcelMapMarkers[pid].setIcon(icon);
                    else parcelMapMarkers[pid].setIcon(icon);
                }

            }
            else {
                r.Reviewed = 'true';
                if (ob.osmMap) parcelMapMarkers[pid].setIcon(iconParcelVisited);
                else parcelMapMarkers[pid].setIcon(iconParcelVisited);
            }

        }
    }
}

function getPriorityIcon(p) {
    let firstParcel = $('.parcel-item').attr('parcelid'), unsyncedChanges = UnSynced && imagesUnsynced ? (UnSynced.filter((x) => { return x.ParcelId == p.Id.toString() }).length > 0 || imagesUnsynced.filter((x) => { return x.ParcelId == p.Id.toString() }).length > 0) : false, icon;

    if (firstParcel == p.Id.toString())
        icon = iconParcelFirst;
    if (unsyncedChanges) {
        if (firstParcel == p.Id.toString())
            icon = ParcelUnsyncFirst;
        else
            icon = ParcelUnsync;
    }
    if (p.CC_Priority == 2 || p.CC_Priority == "2") {
        if (firstParcel == p.Id.toString())
            icon = iconParcelAlertFirst;
        else
            icon = iconParcelAlert;
        if (unsyncedChanges) {
            if (firstParcel == p.Id.toString())
                icon = ParcelAlertUnsyncFirst;
            else
                icon = ParcelAlertUnsync;
        }
    }
    else if (p.CC_Priority == "1") {
        if (firstParcel == p.Id.toString())
            icon = iconParcelPriorityFirst;
        else
            icon = iconParcelPriority;
        if (unsyncedChanges) {
            if (firstParcel == p.Id.toString())
                icon = ParcelPriorityUnsyncFirst;
            else
                icon = ParcelPriorityUnsync;
        }
    }
    return icon;
}