﻿//    DATA CLOUD SOLUTIONS, LLC ("COMPANY") CONFIDENTIAL 
//    Unpublished Copyright (c) 2010-2013 
//    DATA CLOUD SOLUTIONS, an Ohio Limited Liability Company, All Rights Reserved.

//    NOTICE: All information contained herein is, and remains the property of COMPANY.
//    The intellectual and technical concepts contained herein are proprietary to COMPANY
//    and may be covered by U.S. and Foreign Patents, patents in process, and are protected
//    by trade secret or copyright law. Dissemination of this information or reproduction
//    of this material is strictly forbidden unless prior written permission is obtained
//    from COMPANY. Access to the source code contained herein is hereby forbidden to
//    anyone except current COMPANY employees, managers or contractors who have executed
//    Confidentiality and Non-disclosure agreements explicitly covering such access.</p>

//    The copyright notice above does not evidence any actual or intended publication
//    or disclosure of this source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
//    DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
//    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
//    IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
//    OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
//    TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
//    ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

var isCopying = false;
var copyCatid;
var catIdList = [];
var IgnoreConditions = [];
var copyConditions = [];
var copiedCategories = [];
var emptyCopyCategories = [];
var isMultipleCategories = false;
var loadParcel = false;
var HiddenRecords = [];
var refreshSortScreen = false;
var refreshMiniSS = false;
var redirectToMap = false;
var copyingIgnoreCondition = null;
var copyingCopyCondition = null;
var classCalculatorValidationResult = [];
var infoContentScroll = null;
var childRecCalcPattern = /\\.|(sum|avg)|\(\)/i;
var ClientRId;
var fTime = false;
var validateCyFy = false;
var successValidation = false;
var sameInSsDcc = false;
var disableIcons = false;
var autoselectloop = [];
var autoLoop = false;
var LFields = [];
var trackFlag = false;
var recFlag = false;
var shwcatfrmmsg = false;
var lookupDataCounter = 0;
var newDdlPhotoProperties = null;
var ddlPhotoSource = null;
var fySwitchInPrc = false;


function loadDataCollectionValues(recalculate, fieldname, auxsource, isPrcTrue) {
    firstTimeLoad = true;
    var dc = $('#data-collection');
    var dcf = $('#dc-form'), loadAuxilaryCatIds = [];
    var blurAlert = false;

    function blurFieldAlertMessage() {
        if (blurAlert) {
            $('.field-alert-message').blur();
            blurAlert = false; 
        }
    }
    $('.field-alert-message').unbind('blur');
    $('.field-alert-message').bind('blur', function () {
        var alertMessage = $(this).val();
        if(alertMessage.trim() == '' && (activeParcel.FieldAlert == null || activeParcel.FieldAlert =='')){
        }
		else {
        	ccma.Sync.enqueueParcelAttributeChange(activeParcel.Id, 'FieldAlert', alertMessage, function () {
        		getData("SELECT * FROM Parcel WHERE  Id = " + activeParcel.Id + "", [], function (p) {
		            if (p[0].Reviewed) {
		            	refreshSortScreen = true;
		            	executeQueries("UPDATE Parcel SET Reviewed = 'false' WHERE Id = " + activeParcel.Id)
		            }
		        });
        	});
        }
    });

    /*$('.dc-field-alert-flag').html('<option value="${Id}">${Name}</option>'); $('.dc-field-alert-flag').fillTemplate(fieldAlerts);*/
    let defaultFieldAlertData = [{ Id: '1', Name: 'No Flag' }, { Id: '2', Name: 'New Construction' }, { Id: '3', Name: 'Demolition' }, { Id: '4', Name: 'Data Discrepancy' }]
    let fieldAlertData = fieldAlerts.length > 0 ? fieldAlerts : defaultFieldAlertData;
    $('.dc-field-alert-flag').lookup({ popupView: true, searchActive: true, searchActiveAbove: 8, searchAutoFocus: false, onSave: () => { } }, []);
    $('.dc-field-alert-flag')[0].getLookup.setData(fieldAlertData);
    $('.dc-field-alert-flag').unbind('change');
    $('.dc-field-alert-flag').bind('change', function () {
        refreshSortScreen = true;
        var flag = $(this).val();
        ccma.Sync.enqueueParcelAttributeChange(activeParcel.Id, 'FieldAlertType', flag, function () {
            $('.select-flag', $('nav[parcelid="' + activeParcel.Id + '"]')).val(flag);
            getData("SELECT * FROM Parcel WHERE  Id = " + activeParcel.Id + "", [], function (p) {
	            if (p[0].Reviewed)
	            	executeQueries("UPDATE Parcel SET Reviewed = 'false' WHERE Id = " + activeParcel.Id)
	        });
        });
    });
    $('.dc-field-alert-flag', dcf).val(activeParcel.FieldAlertType);
    $('.dc-field-alert-flag', dcf).attr('pid', activeParcel.Id);
    /*isBPPUser && (activeParcel.CC_Deleted == 'true' || (activeParcel.IsRPProperty == 'true' && activeParcel.IsParentParcel == 'true' && activeParcel.Reviewed == 'true') || (ccma.Session.RealDataReadOnly == true && activeParcel.IsRPProperty == 'true' )) ? $('#dc-form .field-alert-message,#dc-form .dc-field-alert-flag').attr('disabled','disabled') : $('#dc-form .field-alert-message,#dc-form .dc-field-alert-flag').removeAttr('disabled');*/
    isBPPUser && (activeParcel.CC_Deleted == 'true' || (ccma.Session.RealDataReadOnly == true && activeParcel.IsRPProperty == 'true')) ? $('#dc-form .field-alert-message,#dc-form .dc-field-alert-flag').attr('disabled', 'disabled') : $('#dc-form .field-alert-message,#dc-form .dc-field-alert-flag').removeAttr('disabled');
    $('.cautionalert', dcf).val(activeParcel.CautionMessage);
    $('.officealert', dcf).val(activeParcel.ParcelAlert);
    if (EnableNewPriorities == 'True') {
        let prcolr = getPriorityClr(activeParcel.CC_Priority);
        $('.dcc_priority').html(prcolr.prty);
        if (prcolr.clr) { $('.dcc_priority').css('background-color', prcolr.clr); }
        else { $('.dcc_priority').css('background-color', ""); }
        $('.dcc_priority').show();
    }
    else $('.dcc_priority').hide();


    if (activeParcel.CautionMessage != null) {
        var cautionMsg = activeParcel.CautionMessage.replace(/&lt;/g,'<').replace(/&gt;/g,'>') || clientSettings["DefaultCaution"] || 'CAUTION!';
        $('.caution-prc').removeClass('hidden');
        $('.caution-prc').text(cautionMsg);
        $('.cautionalert-qr').removeClass('hidden');
        $('.cautionalert-qr', dcf).text(cautionMsg);
        // $('.officealert', dcf).val(activeParcel.ParcelAlert);
    }
    else {
        $('.cautionalert-qr').addClass('hidden');
        $('.caution-prc').addClass('hidden');
    }

   //if (activeParcel.Changes['FieldAlert'] != null) {
   if(activeParcel.Changes.filter(function(c){return c.FieldName == "FieldAlert" }).length > 0){
        $('.field-alert-message', dcf).val(activeParcel.FieldAlert);
    } else if (activeParcel.FieldAlert != null) {
        $('.field-alert-message', dcf).val(activeParcel.FieldAlert);
    } else {
        $('.field-alert-message', dcf).val('');
    }

    refreshDataCollectionEstimateChart();

    $('.dc-mark-complete', dcf).unbind(touchClickEvent);
    $('.dc-mark-complete', dcf).bind(touchClickEvent, function (e) {
        e.preventDefault();
        var cmpAlrt = $('.field-alert-message').val() == '' ? null : $('.field-alert-message').val()
        
        if (cmpAlrt != activeParcel.FieldAlert) {
            blurAlert = true;
            setTimeout(function () {
                /*ccma.Sync.enqueueParcelAttributeChange(activeParcel.Id, 'FieldAlert', $('.field-alert-message').val());*/
                markParcelAsComplete(activeParcel.Id, function (RPParcel) {
                    if (ParcelopenfromMap) {
                        openMapView();
                        ParcelopenfromMap = false;
                        allowInputs();
                    } else {
                        showSortScreen(function () {
                            reloadParcelsInList(); setTimeout(function () {
                                if (isBPPUser && RPParcel == "true" && activeParentParcel) {
                                    refreshMiniSS = true;
                                    $('#sort-screen .page-back').trigger('click');
                                }
                            }, 300);
                        }, true);
                    }
                });
            }, 500);
        }
        else {
            /*ccma.Sync.enqueueParcelAttributeChange(activeParcel.Id, 'FieldAlert', $('.field-alert-message').val());*/
            markParcelAsComplete(activeParcel.Id, function (RPParcel) {
                if (ParcelopenfromMap) {
                    openMapView();
                    ParcelopenfromMap = false;
                    allowInputs();
                } else {
                    showSortScreen(function () {
                        reloadParcelsInList(); setTimeout(function () {
                            if (isBPPUser && RPParcel == "true" && activeParentParcel) {
                                refreshMiniSS = true;
                                $('#sort-screen .page-back').trigger('click');
                            }
                        }, 300);
                    }, true);
                }
            });
        }
        hideKeyboard();
    });

    var fieldsetSelector = 'fieldset';
    if (recalculate)
        fieldsetSelector = 'fieldset[calculated]';
    var auxSelector = 'section[auxdata]';
    if (auxsource)
        auxSelector = 'section[auxdata="' + auxsource + '"]';

    if (!auxsource && !isPrcTrue) {
        $(fieldsetSelector, '#data-collection section[parceldata]').each(function () {
            setFieldValue(this);
        })
        $(fieldsetSelector, '#data-collection section[name="quickreview"]').each(function () {
            setFieldValue(this);
        })
    }
    var newRecordCatId = null;
	if (isPrcTrue && isPrcTrue.tab) {
		newRecordCatId = isPrcTrue.tab;
		var parCatId = getParentCategories(newRecordCatId);
        parCatId ? loadAuxilaryCatIds.push(parCatId, newRecordCatId): loadAuxilaryCatIds.push(newRecordCatId);
        //loadAuxilaryCatIds = loadAuxilaryCatIds.concat(getChildCategories(newRecordCatId));
    }
    
    $('#data-collection ' + auxSelector).each(function () {
        var auxForm = this;
        var sourceTable = $(this).attr('auxdata');
        var catid = $(this).attr('catid');
        var sourceData = activeParcel[sourceTable] ? activeParcel[sourceTable] : [] ;
        $(auxForm).removeAttr('parentpath');
        //FILTER DATA AFTER THIS LINE, BUT TAKE THIS OUT OF HERE, 
        //Needs to be called in different ways.
        if (!newRecordCatId || ( newRecordCatId && loadAuxilaryCatIds.filter(function(ca) { return ca == catid })[0] )) 
        	loadAuxiliaryRecords( auxForm, sourceData, sourceTable, null, null, null, null, null, true );
    });

    $('#data-collection section[parceldata]').each(function () {
        var auxForm = this;
        refreshParcelSubLinks(auxForm, $(auxForm).attr('catid'));
    });
 	//refreshScrollable();
}

function loadAuxiliaryRecordsWithFilter(auxForm, filters, filterValues, parentSpec, index) {
    var sourceTable = $(auxForm).attr('auxdata');
    var sourceData = activeParcel[sourceTable];

    var catName = $(auxForm).attr('catid');
    if (!cachedAuxOptions[catName]) cachedAuxOptions[catName] = {};
    cachedAuxOptions[catName].filters = filters;
    cachedAuxOptions[catName].filterValues = filterValues;
    cachedAuxOptions[catName].parentSpec = parentSpec;

    var filteredIndexes = getFilteredIndexes(sourceData, filters, filterValues, parentSpec,sourceTable);

    loadAuxiliaryRecords(auxForm, sourceData, sourceTable, filteredIndexes, parentSpec, undefined, undefined, index);

}

function getFilteredIndexes(sourceData, filters, filterValues, parentSpec,sourceTable) {
    var parentROWUID;
    var st= sourceTable;
    if (parentSpec) {
        if (activeParcel[parentSpec.source]) {
            if (activeParcel[parentSpec.source][parentSpec.index]) {
                parentROWUID = activeParcel[parentSpec.source][parentSpec.index].ROWUID;
            }
        }
    }
    var filteredIndexes;
    if ((filters == null) || (filters == '')) {

    } else {
        filteredIndexes = [];
        for (var x in sourceData) {
            var at = sourceData[x];
			st= sourceData[x].DataSourceName ? sourceData[x].DataSourceName : st ;
            if ((parentROWUID != null) && (at.ParentROWUID != null) && (st != parentSpec.source)) {
                if (at.ParentROWUID == parentROWUID) {
                    filteredIndexes.push(x);
                }
            }
            else {
                var allMatched = true;
                var filterFields = filters.split(',');
                var checkValues = filterValues.split(',');
                for (fi in filterFields) {
                    var filter = filterFields[fi];
                    if (filter.indexOf('/') > -1) filter = filter.split('/')[1]
                    var value = checkValues[fi];
                    if (!at[filter]) {
                        allMatched = false;
                        break;
                    }
                    else if (at[filter].toString() == value) {
                        allMatched = allMatched && true;
                    }
                    else if (isFinite(at[filter].toString()) && isFinite(value) && parseFloat(at[filter].toString()) == parseFloat(value)) {
                        allMatched = allMatched && true;
                    }
                    else {
                        allMatched = false;
                        break;
                    }
                }
                if (allMatched) {
                    filteredIndexes.push(x);
                }
            }
        }
    }
    return filteredIndexes;
}

var cachedAuxOptions = {}
var filterind = [];
function reloadAuxiliaryRecords(catid, gridOnly, refreshMultiChild, index, recordPhoto) {

    var auxForm = cachedAuxOptions[catid].auxForm;
    var sourceTable = cachedAuxOptions[catid].sourceTable;
    var sourceData = activeParcel[sourceTable];
    var parentSpec = cachedAuxOptions[catid].parentSpec;
    var filteredIndexes;
    if (cachedAuxOptions[catid].filters) 
        filteredIndexes = getFilteredIndexes(sourceData, cachedAuxOptions[catid].filters, cachedAuxOptions[catid].filterValues, parentSpec,sourceTable);
    loadAuxiliaryRecords(auxForm, sourceData, sourceTable, filteredIndexes, parentSpec, gridOnly, refreshMultiChild, index, null, recordPhoto);
}

function refreshParcelSubLinks(auxForm, catid) {

    $('.sub-cat-opener', auxForm).each(function () {
        var link = this;
        if ($(link).attr('filters') != null) {
            var filters = $(link).attr('filters').split(',');
            var filterValues = '';
            for (x in filters) {
                var ff = filters[x];
                filterValues += activeParcel[ff] + ',';
            }
            $(link).attr('filtervalues', filterValues);
            var targetData = $(link).attr('targetdata');
            var fii = getFilteredIndexes(activeParcel[targetData], $(link).attr('filters'), filterValues) || [];
            var targetCount = fii.length;
            $('span', link).html(targetCount);
            var cat = getCategory($(link).attr('catid'))// fieldCategories.filter(function (a) { return (a.Id == $(link).attr('catid')) })[0]
            //                    $(link).attr('filtervalues', filterValues);
        }
    });
}

function refreshAuxSubLinks( catid ){
if(!catid || !cachedAuxOptions) return;
    var auxForm = cachedAuxOptions[catid] && cachedAuxOptions[catid].auxForm;
    var index = parseInt($(auxForm).attr('index')) || 0;
    var sourceTable = cachedAuxOptions[catid].sourceTable;
    var sourceData = activeParcel[sourceTable];
    var parentSpec = cachedAuxOptions[catid].parentSpec;
    var filteredIndexes = cachedAuxOptions[catid].filteredIndexes;

    if (filteredIndexes && (filteredIndexes.length == 0)) {
        $('.sub-cat-opener', auxForm).each(function () {
            var link = this;
            $('span', link).html(-1);
        });
        return;
    }

    $('.sub-cat-opener', auxForm).each(function () {
        var link = this;
        if ($(link).attr('filters') != null) {
            var filters = $(link).attr('filters').split(',');
            var filterValues = '';
            for (x in filters) {
                var ff = filters[x];
                filterValues += sourceData[filteredIndexes[index]][ff] + ',';
            }
            $(link).attr('filtervalues', filterValues);
            var targetData = $(link).attr('targetdata');
            var thisCatid = $(this).attr('catid');
            var sourceDataNew = activeParcel[targetData]
            let indicesToRemove = [];
            const filteredFieldData = fieldCategories.filter(item => item.Id == thisCatid && item.HideRecordExpression !== null);
            if (filteredFieldData.length > 0) {
                filteredFieldData.forEach(function (categ) {
                    for (let uindex = 0; uindex < activeParcel[targetData].length; uindex++) {
                        var isExp = activeParcel.EvalValue(targetData + '[' + uindex + ']', categ.HideRecordExpression);
                        if (isExp && isExp != "****") {
                            indicesToRemove.push(uindex);
                        }
                    }
                    sourceDataNew = activeParcel[targetData].filter(function (_, uindex) {
                        return !indicesToRemove.includes(uindex);
                    });

                    //filteredIndexes = activeParcel[targetData].map(function (_, index) {return index.toString();}).filter(function (index) {
                    //        return !indicesToRemove.toString().includes(index);
                    //    });
                });
            }
            var fii = getFilteredIndexes(sourceDataNew, $(link).attr('filters'), filterValues, { source: sourceTable, index: $(auxForm).attr('findex') },targetData) || [];
            var targetCount = fii.length;

            if(fii.length > 0){
                $('section[catid="'+thisCatid +'"]').attr('findex',fii[0]);
            }
            //var parenttable = parentChild.filter(function (s) { return s.ChildTable == targetData })[0].ParentTable;
            //if (parenttable == 'Parcel' && !targetCount)
            //    targetCount = activeParcelData[targetData].length;
            $('span', link).html(targetCount);
            //var cat = getCategory($(link).attr('catid'))//   var cat = fieldCategories.filter(function (a) { return (a.Id == $(link).attr('catid')) })[0]
            //                    $(link).attr('filtervalues', filterValues);
        }
    });
}

function copyRecordToFutureYear( cat, findex ){
    let rec = activeParcel[cat.SourceTable][findex], recID = rec['ROWUID'],
        mode = showFutureData ? 1 : 0, copyMsg = showFutureData ? 'Copy record to the Current Year?': 'Copy record to the Future Year?';

    copyingIgnoreCondition = 'ROWUID != ' + recID;
    if ( !showFutureData && activeParcel.HasFutureYear != 'true' ) {
        messageBox( "Future Year property does not exist yet! Please return to the Property Menu screen and tap the 'Copy Data to Future Year' option first" );
        return;
    }

    let copyFun = function () {
        CopyRecordNotChildOfParcel(rec, cat, (crNotChildOfParcel, fAParent, fAparentCat) => {
            messageBox(copyMsg, ['Yes', 'Cancel'], function () {
                let op = { type: 'futureYear', mode: mode };
                if (crNotChildOfParcel && fAParent && fAparentCat) {
                    op.parentCategory = fAparentCat; op.parentElement = rec.parentRecord; op.targetParentElement = fAParent;
                    op.CopyRecordNotChildOfParcel = true; 
                }
                ccma.Data.Controller.CopyFormData(activeParcel.Id, activeParcel.Id, [cat], null, op); // mode 1 for future to active copy
            });
        });       
    }

    if (clientSettings?.FutureYearPKConstraint == '1') {
        if (showFutureData) {
            if (rec?.CC_LinkedROWUID) {
                messageBox("The selected record is already copied from the current year!");
                return;
            }
            else { copyFun(); }
        }
        else {
            getData('SELECT * FROM ' + cat.SourceTable + ' WHERE CC_LinkedROWUID = ? AND CC_YearStatus = "F"', [parseInt(recID).toString()], (rList) => {
                if (rList.length > 0) {
                    messageBox("The selected record is already copied to the future year!");
                    return;
                }
                else { copyFun(); }
            });
        }
    }
    else { copyFun(); }
}

function CopyRecordNotChildOfParcel(rec, cat, callback) {
    if (!(clientSettings['CopyRecordNotChildOfParcel'] && clientSettings['CopyRecordNotChildOfParcel'] != '') || !rec || !cat || !rec.parentRecord) {
        if (callback) callback();
        return;
    }

    let csSetting = clientSettings['CopyRecordNotChildOfParcel'].split(','), stable = csSetting[0], parentName = csSetting[1],
        pTable = parentChild.filter((a) => { return a.ChildTable == cat.SourceTable })[0],
        stPtable = parentChild.filter((a) => { return a.ChildTable == stable })[0];

    if (pTable?.ParentTable == stable && stPtable?.ParentTable == 'Parcel') {
        let fc = 'CC_ParcelId = ' + activeParcel.Id + ' AND ' + (showFutureData ? '(CC_YearStatus = "A" OR CC_YearStatus IS NULL)' : 'CC_YearStatus = "F"'), yField = false, ff = cat.FilterFields;
        if (ff && ff != '') {
            let recP = rec.parentRecord;
            ff.split(',').forEach((f) => {
                f = f ? f.trim() : f; let v = recP[f];
                if (f == cat.YearPartitionField?.trim()) {
                    yField = true;
                    v = !showFutureData ? (clientSettings.FutureYearValue ? clientSettings.FutureYearValue : '0') : clientSettings["CurrentYearValue"];
                }

                if (!(!v && recP.CC_RecordStatus == 'I')) {
                    if (!v) v = '';
                    fc += ' AND ' + f + ' = "' + v + '"';
                }
            });
        }

        if (!yField) {
            let v = showFutureData ? (clientSettings.FutureYearValue ? clientSettings.FutureYearValue : '0') : clientSettings["CurrentYearValue"];
            fc += ' AND ' + cat.YearPartitionField?.trim() + ' = "' + v + '"';
        }

        getData('SELECT * FROM ' + stable + (fc != '' ? ' WHERE ' + fc: ''), [], (rList) => {
            if (rList.length == 0) {
                let msg = "Cannot complete copy process! Cannot find the parent record.";
                messageBox(msg);
                return;
            }
            else if (rList.length > 1) {
                let msg = "Cannot complete copy process! Multiple parent records exist";
                messageBox(msg);
                return;
            }
            else {
                let pCat = parentName ? fieldCategories.filter((x) => { return x.Name == parentName.trim() })[0]: getCategory(cat.ParentCategoryId);
                if (callback) callback(true, rList[0], pCat);
            }
        });
    }
    else if (callback) callback();
}

function loadAuxiliaryRecords(auxForm, sourceData, sourceTable, filteredIndexes, parentSpec, refreshGridOnly, refreshMultiChild, recordIndex,firstTime, recordPhoto ){
    if (filteredIndexes) $(auxForm).attr('filteredIndexes', filteredIndexes.toString());
    var catid = $(auxForm).attr('catid');
    var cat = getCategory(catid); 
    var pcatid = getParentCategories( catid )
    var pcat = getCategory( pcatid );
    disableIcons = false;
    LFields = [];
    
    $('.record-massupdate', auxForm).unbind(touchClickEvent);
    $('.record-massupdate', auxForm).bind(touchClickEvent, function (e) {
    	  if(!checkPPAccess('massUpdate')){
            messageBox("This option is not available for the selected parcel."); 
            return;
        }
        openMassUpdateForm( catid, $( auxForm ).attr( 'findex' ), $( auxForm ).attr( 'parentpath' ), $( auxForm ).attr( 'index' ) );
    });
    
    $('.record-futurecopy', auxForm).unbind( touchClickEvent );
    $('.record-futurecopy', auxForm).bind( touchClickEvent, function ( e ){
        if(!checkPPAccess('recordFutureCopy')){
            messageBox("This option is not available for the selected parcel."); 
            return;
        }
        copyRecordToFutureYear( cat,$( auxForm ).attr( 'findex' ) );
    });
    
    $('.record-classcalculator', auxForm).unbind(touchClickEvent);
    $('.record-classcalculator', auxForm).bind(touchClickEvent, function (e) {
        if(!checkPPAccess('classCalculator')){
            messageBox("This option is not available for the selected parcel."); 
            return;
        }
        classCalculatorFields = []
        getClassCalculatorFields(cat.Id, function () {
            getClassCalculatorUpdateFormTemplate(cat, classCalculatorFields[0].Fields, $(auxForm).attr('findex'), sourceTable);
        })
    });
    
    if ( pcat && pcat.MultiGridStub == 'true' && firstTime )
        return;
    if ( !filteredIndexes && sourceData) {
        filteredIndexes = [];
        filteredIndexes = sourceData.map( function ( f, n ) { return n.toString() } )
    }

    let indicesToRemove = [];
    const filteredFieldData = fieldCategories.filter(item => item.Id == catid && item.HideRecordExpression !== null);
    if (filteredFieldData.length > 0) {
        filteredFieldData.forEach(function (categ) {
            for (let uindex = 0; uindex < activeParcel[sourceTable].length; uindex++) {
                var isExp = activeParcel.EvalValue(sourceTable + '[' + uindex + ']', categ.HideRecordExpression);
                if (isExp && isExp != "****") {
                    indicesToRemove.push(uindex);
                }
            }

            sourceData = activeParcel[sourceTable].filter(function (_, uindex) {
                return !indicesToRemove.includes(uindex);
            });

            let cpfi = filteredIndexes;
            filteredIndexes = activeParcel[sourceTable].map(function (_, index) {return index.toString();}).filter(function (index) {
                return cpfi.includes(index) && !indicesToRemove.includes(parseInt(index));
            });
        });
    }

    var dataSourceObject = []

    var gridData = sourceData ? activeParcel[sourceTable].filter(function (d, i) { return filteredIndexes.indexOf(i.toString()) > -1; }) : null;
    $('.aux-grid-view .aux-grid-header', auxForm).html($('<div/>').html(cat.GridHeader || '').text());
    $('.aux-grid-view .aux-grid-header', auxForm).fillTemplate([activeParcel]);
    $('.aux-grid-view .data-collection-table tbody', auxForm).html($('.data-collection-table tfoot', auxForm).html());
    if(cat && cat.PhotoLinkTableKey && cat.PhotoTag && clientSettings["PhotoTagMetaField"] && clientSettings["PhotoTableLinkMetaField"]){
    	gridData.forEach(function(x){
			var metaData = clientSettings["PhotoTableLinkMetaField"];
			var regexp = new RegExp(+x.ROWUID+'$');
			im = photos['allimages'].filter(function(image){if(image[metaData]) {return image[metaData].search(regexp) > -1}});
			x.photoRecordCount = im ? im.length : 0;
		});
    }	
    $('.aux-grid-view .data-collection-table tbody', auxForm).fillTemplate(gridData);	
   // if(cat && cat.PhotoLinkTableKey && cat.PhotoTag && clientSettings["PhotoTagMetaField"] && clientSettings["PhotoTableLinkMetaField"]){
    if (cat?.PhotoLinkTableKey && cat?.PhotoTag && clientSettings?.["PhotoTagMetaField"] && clientSettings?.["PhotoTableLinkMetaField"]) {
    	$('.aux-grid-view .data-collection-table tbody tr td:last-child', auxForm).append('<a class="record-grid-photo"></a>');
    	$('.aux-grid-view .data-collection-table tbody tr td:last-child span', auxForm).addClass('record-grid-photo-count');
    }
    $('.aux-grid-view .data-collection-table tbody .aux-grid-edit', auxForm).unbind(touchClickEvent);
    $('.aux-grid-view .data-collection-table tbody .aux-grid-edit', auxForm).bind(touchClickEvent, function (e) {
        e.preventDefault();
        moveIndicator=0;
        relationcycle = [];
        $(auxForm).attr('index', $('.aux-grid-view .data-collection-table tbody .aux-grid-edit', auxForm).indexOf(this));
        var index = parseInt($(auxForm).attr('index'));
        $(auxForm).attr('findex', filteredIndexes[index]);

        getRecord(index);
        $('.data-entry-tab-content', auxForm).show();
		$('#dc-form-contents').scrollTop(0);
        var categoryId = $(auxForm).attr('catid');
        var cat = getCategory(categoryId);
        $('.record-photo-preview').show();
        if(cat.PhotoLinkTableKey && cat.PhotoTag && clientSettings["PhotoTagMetaField"] && clientSettings["PhotoTableLinkMetaField"]){
        	$('.record-photo-preview').hide();
        	$('.record-add-photo').show();
        } 
        else{
        	$('.record-photo-preview').show();
            $('.record-add-photo').hide();
        }
        $('.aux-grid-view', auxForm).hide();
        $('.aux-multigrid-view', auxForm).hide();
        $('.aux-sub-multigrid-view', auxForm).hide();
        $(this).attr('state', 'edit');
		//formScroller.enable();
        recalculateDimensions();
    });
    $('.aux-grid-view .data-collection-table tbody .record-grid-photo', auxForm).unbind(touchClickEvent);
    $('.aux-grid-view .data-collection-table tbody .record-grid-photo', auxForm).bind(touchClickEvent, function (e) {
    	e.preventDefault();
    	appState.photoCatId = appState.catId;
		var recIndex = $('.aux-grid-view .data-collection-table tbody .record-grid-photo', auxForm).indexOf(this);
		$(auxForm).attr('index', recIndex);
		var index = parseInt($(auxForm).attr('index'));
		$(auxForm).attr('findex', filteredIndexes[index]);
		getRecord(index);
		$('.aux-grid-view .data-collection-table tbody tr', auxForm).removeClass('multigridSelected');
		$('.aux-grid-view .data-collection-table tbody tr', auxForm).eq(index).addClass('multigridSelected');
		photos.openParcel(activeParcel.Id, activeParcel.KeyValue1, null, true);
    });
    
    $('.aux-grid-view .data-collection-table tbody tr', auxForm).unbind(touchClickEvent);
    if (refreshGridOnly)
        return false;
    $( '.record-futurecopy', auxForm ).hide();
    $('.record-button[action="list"]', auxForm).unbind(touchClickEvent);
    $('.record-button[action="list"]', auxForm).bind(touchClickEvent, function (e) {
        e.preventDefault();
        //if(disableIcons){return false;}
		hideKeyboard();
        var state = $(this).attr('state');
        var valid = checkRequiredFieldsInForm();
        if (!valid) {
           messageBox(msg_FormNoNavigate);
           return false;
           } 
       	
        $('.data-entry-tab-content', auxForm).hide();
        $('.aux-grid-view', auxForm).show();
        $('.aux-multigrid-view', auxForm).show();
        $('.aux-sub-multigrid-view', auxForm).show();
        $(this).attr('state', 'grid')
        var categoryId = $(auxForm).attr('catid');
        var TopPID = getTopParentCategory(categoryId)

        if (sqlinsert != "" || fieldUpdateStatus[categoryId]) {
            reloadAuxiliaryRecords($(auxForm).attr('catid'), null, null, $(auxForm).attr('index'));
            if (categoryId == TopPID){
                sqlinsert = "";
                fieldUpdateStatus[categoryId]=false;
            }

        }
        else loadMultiGrid(auxForm);
        if (cat && cat.PhotoLinkTableKey && cat.PhotoTag && clientSettings["PhotoTagMetaField"] && clientSettings["PhotoTableLinkMetaField"]) {
            $('.record-photo-preview').hide();
            $('.record-add-photo').hide();
            $('.data-collection-table .record-add-photo', auxForm).show();
        }
        else {
            $('.record-photo-preview').show();
            $('.record-add-photo').hide();
        }
        hideKeyboard();
		//var multigridScroll = new IScroll('.aux-grid[catid="' + categoryId + '"]', iScrollOptions);
        highlightChangesInGrid(catid);
    });

    var categoryId = $(auxForm).attr('catid');
    if (!cachedAuxOptions[categoryId]) cachedAuxOptions[categoryId] = {};
    cachedAuxOptions[categoryId].auxForm = auxForm;
    cachedAuxOptions[categoryId].sourceData = sourceData;
    cachedAuxOptions[categoryId].sourceTable = sourceTable;
    cachedAuxOptions[categoryId].filteredIndexes = filteredIndexes;
    cachedAuxOptions[categoryId].parentSpec = parentSpec;
    if(cat.OverrideParentCategory){
           var iform = $('section[catid="' + parseInt(cat.OverrideParentCategory) + '"]')
	    var fIndex = $(iform).attr('findex');
	    var thisIndex = $(iform).attr('index');
	    var thisparentpath = $(iform).attr('ParentPath');
	    var parent = {
	        source: getCategory(cat.OverrideParentCategory).SourceTable,
	        index: fIndex,
	        thisIndex: thisIndex,
	        parentpath: thisparentpath
	    }
	    cachedAuxOptions[categoryId].parentSpec = parent;
    }
    /*if (isBPPUser && ((activeParcel.IsRPProperty == 'true' && ccma.Session.RealDataReadOnly == true) || activeParcel.CC_Deleted == "true" || (activeParcel.IsRPProperty == 'true' && activeParcel.IsParentParcel == 'true' && activeParcel.Reviewed == 'true'))) {*/
    if (isBPPUser && ((activeParcel.IsRPProperty == 'true' && ccma.Session.RealDataReadOnly == true) || activeParcel.CC_Deleted == "true" )) {
        $( '.record-new', auxForm ).addClass('BPP-Hide')
        $( '.record-delete', auxForm ).addClass( 'BPP-Hide' )
    } 
    else {
        $( '.record-new', auxForm ).removeClass( 'BPP-Hide' );
        $( '.record-delete', auxForm ).removeClass( 'BPP-Hide' );
        if( $( '.record-new', auxForm ).css('display') == 'inline' ) 
            $( '.record-new', auxForm ).css("display", '');
    }
    var records = 0;
    records = filteredIndexes.length;
   
    $(auxForm).attr('records', records);
    $(auxForm).attr('index', -1);
    $(auxForm).attr('findex', -1);
    if (parentSpec) $(auxForm).attr('parentIndex', parentSpec.thisIndex);
    var fcid = $(auxForm).attr('catid');

    var parentAuxRecord;
    if (parentSpec) {
        if (parentSpec.source)
            parentAuxRecord = activeParcel[parentSpec.source][parentSpec.index];
        else
            parentAuxRecord = activeParcel;
    }
    
   	if (records == 0) {
        $('.header-info', auxForm).hide();
        HideCategoryByExpression(categoryId, auxForm, sourceTable, filteredIndexes[index], parentSpec ? parentSpec.source + '[' + parentSpec.index + ']' : null, true)
    }
    
    if (records > 0) {
        filterind = filteredIndexes;
        $(auxForm).attr('index', 0);
        $(auxForm).attr('findex', filteredIndexes[0]);
        //################# navigating function

        var index = parseInt($(auxForm).attr('index'));
        //getRecord(index,recordPhoto);

        $('.record-move', auxForm).unbind('touchstart touchend');
        $('.record-move', auxForm).bind('touchstart', function () {
            $(this).attr('on', 'on');
        });

        $('.record-move', auxForm).bind('touchend', function () {
            $(this).removeAttr('on');
        });

        $('a[action="jump"]', auxForm).unbind(touchClickEvent);
        $('a[action="jump"]', auxForm).bind(touchClickEvent, function (e) {
            e.preventDefault();
            var index = parseInt($(auxForm).attr('index'));
            $(auxForm).attr('findex', filteredIndexes[index]);
            try {
                getRecord(index);
            } catch (e) {
                //TODO Error reason unknown, bypassed.
            }

        });
        $('.in-record-op', auxForm).unbind(touchClickEvent);
        $('.in-record-op', auxForm).bind(touchClickEvent, function (e) {
            e.preventDefault();
            relationcycle = [];
            if ($(this).attr('disabled') == 'disabled')
                return;
            var focusedElement = $(':focus');
            var elemOrg = this;
            saveFocusedElement(focusedElement, function () {
                var index = parseInt($(auxForm).attr('index'));
                // var dir = $(this).attr('ddir');
               //if(disableIcons){return false;}
                var dir = $(elemOrg).attr('ddir');
                switch (dir) {
                    case "first":
                        index = 0;
                        disableIcons = true;
                        autoselectloop= [];
                        LFields = [];
                        break;
                    case "prev":
                        index = Math.max(index - 1, 0);
                        disableIcons = true;
                        autoselectloop= [];
                        LFields = [];
                        break;
                    case "next":
                        disableIcons = true;
                        autoselectloop= [];
                        LFields = [];
                        index = Math.min(index + 1, records - 1);
                        break;
                    case "last":
                        index = records - 1;
                        disableIcons = true;
                        autoselectloop= [];
                        LFields = [];
                        break;                    
                    case "del":
                        messageBox('Are you sure you want to delete the selected record and its descendants?', ["Yes", "No"], function () {
                            var rowuid = activeParcel[sourceTable][filteredIndexes[index]]["ROWUID"]
                            ccma.Data.Controller.DeleteAuxRecordWithDescendants(fcid, rowuid);
                            sqlDelete = "deleted";
							  /*setTimeout(function () {   //calls click event after a certain time
                                if (formScroller)
                                    formScroller.refresh();
                            }, 1000);*/
                        })
                        break;
                }
                
                if ((dir == "new") || (dir == "del")) {

                } else {
                    var valid = checkRequiredFieldsInForm();
                    if (!valid) {
                        messageBox(msg_FormNoNavigate);
                        return false;
                    }

                    $(auxForm).attr('index', index);
                    $(auxForm).attr('findex', filteredIndexes[index]);

                    getRecord(index);
                    if (index >= 0) {
                        if ($('.aux-grid-view .data-collection-table tbody .record-grid-photo-count', auxForm).length > 0)
                        {
                            $('.aux-grid-view .data-collection-table tbody tr', auxForm).removeClass('multigridSelected');
                            $('.aux-grid-view .data-collection-table tbody tr', auxForm).eq(index).addClass('multigridSelected');
                        }
                    }
                    moveIndicator=1;
				  	//if (formScroller)
                    //	formScroller.refresh();
                }
            });
            hideKeyboard();
        });
        $('.fieldSetHeader', auxForm).show();
        $('fieldset', auxForm).show();
        $( '.sub-cat-opener', auxForm ).show();
        if ( isBPPUser && activeParcel.IsPersonalProperty == 'true' )
            $( '.sub-cat-opener a[BPPType="0"]' ).hide();
        else
            $( '.sub-cat-opener a[BPPType="1"]' ).hide();

        if (recordIndex >= 0 && recordIndex != null) {
            $(auxForm).attr('index', recordIndex);
            $(auxForm).attr('findex', filteredIndexes[recordIndex]);
            getRecord(recordIndex, recordPhoto);
            //if ($('.aux-grid[catid="3"] .aux-grid-view').css('display') == 'block') $('.aux-grid-switch').trigger('click');
            $('.aux-grid-view .data-collection-table tbody tr', auxForm).removeClass('multigridSelected');
    		$('.aux-grid-view .data-collection-table tbody tr', auxForm).eq(recordIndex).addClass('multigridSelected');
        }
        else getRecord(index, recordPhoto);

    } else {

        // No aux data records;
        if(cat && cat.AllowAddDelete == "true" && cat.IsReadOnly != "true" && cat.AllowDeleteOnly != "true")
        	$('.record-new', auxForm).show(); 
        else
        	$('.record-new', auxForm).hide();
        if (cat.AllowDeleteOnly == "true"  && $(auxForm).attr('index') != 0 )
            $('.record-delete', auxForm).show();
        
        $('.fieldSetHeader', auxForm).hide();
        $('fieldset', auxForm).hide();
        $('.aux-records', auxForm).html(0);
        $('.aux-index', auxForm).html(0);
        $('.record-move', auxForm).attr('disabled', 'disabled');
        $('.in-record-op', auxForm).unbind(touchClickEvent);
        $('.record-move[ddir="new"]', auxForm).removeAttr('disabled');
        $('.sub-cat-opener', auxForm).hide();
        $('.record-massupdate', auxForm).hide();
        $('.record-classcalculator', auxForm).hide();
       /* $('.record-add-photo').hide();*/
        refreshAuxSubLinks(categoryId);
        HideCategoryByExpression(categoryId, auxForm, sourceTable, 0, parentSpec ? parentSpec.source + '[' + parentSpec.index + ']' : null, false, true);
        HideInsertDeleteByExpression(categoryId, auxForm, sourceTable, filteredIndexes[index], parentSpec ? parentSpec.source + '[' + parentSpec.index + ']' : null, records, index, true);
    }
    function getRecord(index,recordPhoto) {

        var categoryId = $(auxForm).attr('catid');
        records = filteredIndexes.length;
        index = parseInt(index);
        $('.aux-records', auxForm).html(records);
        $('.aux-index', auxForm).html(index + 1);
        var data = eval('activeParcel.' + sourceTable  + '[' + filteredIndexes[index] + ']');
        if (data != undefined) {
            refreshAuxSubLinks(categoryId);
            $('.header-info', auxForm).html($('.header-template', auxForm).html());
            $('.header-info', auxForm).fillTemplate([data]);
            //set existingRecord
            appState.rowuid = data.ROWUID;
        	saveAppState();
            var isNotPreviousRecord = canChangeExistAuxDetails(activeParcel, categoryId, filteredIndexes[index]);
            if (isNotPreviousRecord)
                $(auxForm).removeAttr('actualdata');
            else
                $(auxForm).attr('actualdata', 'actualdata');

            var idParentTable = parentChild.filter(function (a) { return a.ChildTable == sourceTable && a.ParentTable == 'Parcel' }), crNotChildOfParcel = false;
            if (clientSettings["CopyRecordNotChildOfParcel"] && clientSettings["CopyRecordNotChildOfParcel"] != '') {
                let crcpSetting = clientSettings['CopyRecordNotChildOfParcel'].split(','), stb = crcpSetting[0], crec = getCategoryFromSourceTable(stb);
                if (crec && (crcpSetting[1] || crec.ParentCategoryId) && parentChild.filter((a) => { return a.ChildTable == sourceTable && a.ParentTable == stb })[0] && parentChild.filter((a) => { return a.ChildTable == stb && a.ParentTable == 'Parcel' })[0]) crNotChildOfParcel = true;
            }

            if (allowMassupdate(fcid) && isNotPreviousRecord && (isBPPUser?(ccma.Session.IsPPOnly? true:(ccma.Session.RealDataReadOnly != true && ccma.Session.BPPDownloadType == '1')):true))
                $('.record-massupdate', auxForm).show();
            else
                $('.record-massupdate', auxForm).hide();

            if (fylEnabled && cat.YearPartitionField && (idParentTable.length > 0 || crNotChildOfParcel) && activeParcel.HasFutureYear == 'true' && cat.AllowAddDelete == 'true')
                $( '.record-futurecopy', auxForm ).show();
            else
                $('.record-futurecopy', auxForm).hide();

            if(cat.PhotoLinkTableKey && cat.PhotoTag && records > 0 && clientSettings["PhotoTagMetaField"] && clientSettings["PhotoTableLinkMetaField"]){
            	if($('.data-entry-tab-content', auxForm).css('display') == "block" || recordPhoto){
	            	$('.record-photo-preview').hide();
	            	$('.record-add-photo').show();
	            }
	            else{
	            	$('.record-photo-preview').hide();
	            	$('.record-add-photo').hide();
	            	$('.data-collection-table .record-add-photo', auxForm).show();
	            }
	            appState.photoCatId = appState.catId;
				photos.loadImages(function(){ $('span.photocount').html(appState.recordPhotoCount >= 0 ? appState.recordPhotoCount : 0) },null,true,true);
            }
            else{
            	$('.record-photo-preview').show();
                $('.record-add-photo').hide();
            }
            recordSwitching.switches = [];
            hideCalculator(cat, sourceTable + '[' + filteredIndexes[index] + ']', isNotPreviousRecord, auxForm)
            $('fieldset', auxForm).each(function (eIndex) { recordSwitching.switches[eIndex] = false; });
            $('fieldset', auxForm).each(function (eIndex) {
                setFieldValue(this, sourceTable + '[' + filteredIndexes[index] + ']',null,function(eIndex){
                	recordSwitching.switches[eIndex] = true;
                	recordSwitching.swichingFn();
                },eIndex,null,null,true);
            });
            HideCategoryByExpression(categoryId, auxForm, sourceTable, filteredIndexes[index], parentSpec ? parentSpec.source + '[' + parentSpec.index + ']' : null)
            HideInsertDeleteByExpression( categoryId, auxForm, sourceTable, filteredIndexes[index], parentSpec ? parentSpec.source + '[' + parentSpec.index + ']' : null, records, index )
            pagingSetup(records, auxForm, index);
            highlightChangesInGrid(categoryId);
            setTimeout(function () { checkRequiredIfEditsFieldsInForm(auxForm);}, 200);
        }
    }
    $('.record-new', auxForm).unbind(touchClickEvent);
    $('.record-new', auxForm).bind(touchClickEvent, function (e) {
    	e.preventDefault();
    	e.stopPropagation();
    	 $('#dc-categories').css({ 'left': '-' + 290 + 'px' });
    	if(recFlag) return;  
    	   fTime=false;
        if ($(this).attr('class') == "aux-grid-new record-new a-button") {
            var valid = checkRequiredFieldsInGrid(cat.Id);
            if (!valid) {
                messageBox(msg_FormNoCreate);
                return false;
            }
			//if (formScroller) formScroller.enable();
        }
        
        relationcycle = [];
        var focusedElement = $(':focus');
        saveFocusedElement(focusedElement, function () {
            var valid = checkRequiredFieldsInForm();
            if (!valid) {
                messageBox(msg_FormNoCreate);
                return false;
            }
            if ( fylEnabled && cat.YearPartitionField && showFutureData && activeParcel.HasFutureYear != 'true' )
            {
                messageBox( "Future Year property does not exist yet! Please return to the Property Menu screen and tap the 'Copy Data to Future Year' option first" );
                return;
            }
            if (cat.MaxRecords && ((parentSpec && filteredIndexes.length >= cat.MaxRecords) || (!parentSpec && sourceData.length >= cat.MaxRecords))) {
                messageBox('Maximum number of records(' + cat.MaxRecords.toString() + ') has been reached for this category')
                return false;
            }
            messageBox('Create a new record?', ["Yes", "No"], function () {
            try{
                recFlag = true;
                var timeOut = setTimeout(function () { recFlag = false }, 3000);
                let notChildParcelParent = null;
                if (!parentSpec) {
                    var catid = $(auxForm).attr('catid');
                    parentSpec = cachedAuxOptions[catid].parentSpec;
                    var parentpath = $(auxForm).attr('parentpath');
                    if (parentpath) {
                        parentAuxRecord = eval(parentpath);
                    }
                    else if (parentSpec) {
                        parentAuxRecord = activeParcel[parentSpec.source][parentSpec.index];
                    }

                    if (!parentAuxRecord && clientSettings['CopyRecordNotChildOfParcel'] && clientSettings['CopyRecordNotChildOfParcel'] != '') {
                        let csSetting = clientSettings['CopyRecordNotChildOfParcel'].split(','), stable = csSetting[0], parentName = csSetting[1],
                            cpcat = getCategory(catid), pTable = parentChild.filter((a) => { return a.ChildTable == cpcat.SourceTable })[0],
                            stPtable = parentChild.filter((a) => { return a.ChildTable == stable })[0];

                        if (pTable?.ParentTable == stable && stPtable?.ParentTable == 'Parcel' && parentName && activeParcel[stable]?.length == 1) {
                            parentAuxRecord = activeParcel[stable][0];
                            notChildParcelParent = fieldCategories.filter((x) => { return x.Name == parentName.trim() })[0];
                        }
                    }
                }
                insertNewAuxRecord(auxForm, parentAuxRecord, fcid, null, null, function () {

                    refreshDataCollectionValues(function () {
                    	sortSourceData(activeParcel);
                        reloadAuxiliaryRecords(fcid, null, true, null, true);
                        if (parseInt($(auxForm).attr('records')) - 1 != parseInt($(auxForm).attr('index'))) {
                            var st = getCategory(fcid).SourceTable;
                            var temp = activeParcel[st].filter(function (y) { return y.ROWUID == ClientRId }).map(function (x) { return x.ParentROWUID });
                            var sourceDataNew = activeParcel[st], tcatId = $(auxForm).attr('catid');
                            let indicesToRemove = [];
                            const filteredFieldData = fieldCategories.filter(item => item.Id == tcatId && item.HideRecordExpression !== null && item.SourceTableName == st);
                            if (filteredFieldData.length > 0) {
                                filteredFieldData.forEach(function (categ) {
                                    for (let uindex = 0; uindex < activeParcel[st].length; uindex++) {
                                        var isExp = activeParcel.EvalValue(st + '[' + uindex + ']', categ.HideRecordExpression);
                                        if (isExp && isExp != "****") {
                                            indicesToRemove.push(uindex);
                                        }
                                    }
                                    sourceDataNew = activeParcel[st].filter(function (_, uindex) {
                                        return !indicesToRemove.includes(uindex);
                                    });

                                    //filteredIndexes = activeParcel[targetData].map(function (_, index) {return index.toString();}).filter(function (index) {
                                    //        return !indicesToRemove.toString().includes(index);
                                    //    });
                                });
                            }
                            index = temp && temp[0] ? sourceDataNew.filter(function (y) { return y.ParentROWUID == temp }).map(function (x) { return x.ClientROWUID }).indexOf(ClientRId.toString()) : sourceDataNew.map(function(x){return x.ClientROWUID}).indexOf(ClientRId.toString());
                            records = index + 1;
                            $(auxForm).attr('index', index);
                            filteredIndexes = filterind;
                            $(auxForm).attr('findex', filterind[index]);
                            getRecord(index, true);
                        }
                         recFlag = false;
                         clearTimeout(timeOut);
                         setTimeout(function () { checkRequiredIfEditsFieldsInForm(auxForm); /*if (formScroller) formScroller.refresh();*/ }, 500);
                    }, null, null, fcid);
                    $('.data-entry-tab-content', auxForm).show();
                    $('.aux-grid-view', auxForm).hide();
                    $('.aux-multigrid-view', auxForm).hide();
                    $('.aux-sub-multigrid-view', auxForm).hide();
                    $(this).attr('state', 'edit');
                    $('.reqdIf', auxForm).hide();
                    fTime=false;
                }, null, null, notChildParcelParent);
                recalculateDimensions();
                }
                catch(e) { recFlag = false; }                
            });

        });


        //recalculateDimensions();
        hideKeyboard();
    });

    $('.aux-grid-view .data-collection-table tbody tr', auxForm).bind(touchClickEvent, function (e) {
        if (cat.MultiGridStub == 'true' && !$(this).hasClass('multigridSelected') && !scrolling) {
        	pIdIndex=0;		
			trSelectVar=0;		
			moveIndicator=0;
            $(auxForm).attr('index', $('.aux-grid-view .data-collection-table tbody tr', auxForm).indexOf(this));
            var index = parseInt($(auxForm).attr('index'));
            $(auxForm).attr('findex', filteredIndexes[index]);
            getRecord(index);
            loadMultiGrid(auxForm);
            recalculateDimensions();
            if (cat.PhotoLinkTableKey && cat.PhotoTag  && clientSettings["PhotoTagMetaField"] && clientSettings["PhotoTableLinkMetaField"]) {
                if ($('.data-entry-tab-content', auxForm).css('display') == "block") {
                    $('.record-photo-preview').hide();
                    $('.record-add-photo').show();
                }
                else {
                    $('.record-photo-preview').hide();
                    $('.record-add-photo').hide();
                }
            }
            else {
                $('.record-photo-preview').show();
                $('.record-add-photo').hide();
            }
        }
    });
    if (cat.MultiGridStub == 'true' && dataSourceObject && !refreshMultiChild)
        loadMultiGrid(auxForm);
    //changes msp starts

    $('fieldset', auxForm).each(function () { if ($(this).attr('eval-hi') == '1') { $(this).hide(); } });
    //changes msp ends
    //$("input[type='date']").each(function () { $(this).attr("onfocusin", "focusdate(this)") });
    //$("input[type='date']").each(function () { $(this).attr("onfocusout", "changedate(this)") });
    //$( "input[type='text']" ).each( function () { $( this ).attr( "onkeypress", "return numbervalidation(this,event)" ) } );
  
}

var recordSwitching = {
	switches : [],
	swichingFn : function(sfield){
		if(autoLoop && autoselectloop.length > 0){
			if(sfield && autoselectloop.indexOf(sfield.Id) > -1){
				autoselectloop.splice(autoselectloop.indexOf(sfield.Id),1);
			}
		}
		if(recordSwitching.switches.reduce(function(x,y){ return x && y}, true)){
			disableIcons = false; autoLoop = false;
		}
    }
}

function pagingSetup(records, auxForm, index) {
    var catCode = $(auxForm).attr('catid');
    if (index == 0) {
        $('.record-move[ddir="first"]', auxForm).attr('disabled', 'disabled');
        $('.record-move[ddir="prev"]', auxForm).attr('disabled', 'disabled');
    } else {
        $('.record-move[ddir="first"]', auxForm).removeAttr('disabled');
        $('.record-move[ddir="prev"]', auxForm).removeAttr('disabled');
    }
    if (index == (records - 1)) {
        $('.record-move[ddir="next"]', auxForm).attr('disabled', 'disabled');
        $('.record-move[ddir="last"]', auxForm).attr('disabled', 'disabled');
    } else {
        $('.record-move[ddir="next"]', auxForm).removeAttr('disabled');
        $('.record-move[ddir="last"]', auxForm).removeAttr('disabled');
    }
    
    $('.record-move[ddir="new"]', auxForm).removeAttr('disabled');

  
}

function highlightChangesInGrid(catId,IsMultiGrid,pcateID){
	var auxform = $('section[catid="' + catId + '"]');
	var sourceTable = getCategory(catId).SourceTable;
	var sourceData = activeParcel[sourceTable];
	var pacatId = getParentCategories(catId)? getParentCategories(catId): '';
	var pcatId = getParentCategories(pacatId)? getParentCategories(pacatId): '';
	if(sourceData){
		var ParentSpec = cachedAuxOptions[catId] == undefined ? null : cachedAuxOptions[catId].parentSpec;
		var filteredIndexes;
	    if (cachedAuxOptions[catId] && cachedAuxOptions[catId].filters) {
	        filteredIndexes = getFilteredIndexes(sourceData, cachedAuxOptions[catId].filters, cachedAuxOptions[catId].filterValues, ParentSpec);
	    }
	 	if ( !filteredIndexes ){
	    	filteredIndexes = [];
	        filteredIndexes = sourceData.map( function ( f, n ) { return n.toString() } )
        }
        let indicesToRemove = [];
        const filteredFieldData = fieldCategories.filter(item => item.Id == catId && item.HideRecordExpression !== null);
        if (filteredFieldData.length > 0) {
            filteredFieldData.forEach(function (categ) {
                for (let uindex = 0; uindex < activeParcel[sourceTable].length; uindex++) {
                    var isExp = activeParcel.EvalValue(sourceTable + '[' + uindex + ']', categ.HideRecordExpression);
                    if (isExp && isExp != "****") {
                        indicesToRemove.push(uindex);
                    }
                }

                sourceData = activeParcel[sourceTable].filter(function (_, uindex) {
                    return !indicesToRemove.includes(uindex);
                });

                let cpfi = filteredIndexes;
                filteredIndexes = activeParcel[sourceTable].map(function (_, index) {return index.toString();}).filter(function (index) {
                    return cpfi.includes(index) && !indicesToRemove.includes(parseInt(index));
                });
            });
        }
          

        var gridData = sourceData ? activeParcel[sourceTable].filter(function (d, i) { return filteredIndexes.indexOf(i.toString()) > -1; }) : null;
		for (x in gridData) {
			var curRecChanges = activeParcel.Changes ? activeParcel.Changes.filter(function (y) { return y.AuxRowId == gridData[x].ROWUID  && !(y.OldValue == y.NewValue || (y.OldValue == null && y.NewValue == '')) && datafields[y.FieldId] && datafields[y.FieldId].SourceTable == sourceTable && datafields[y.FieldId].DoNotShow != "true" &&  datafields[y.FieldId].ReadOnly != "true" && ( datafields[y.FieldId].VisibilityExpression ? !( activeParcel.EvalValue(sourceTable + '[' + x + ']', datafields[y.FieldId].VisibilityExpression) ) : true ) }) : null;
			var NewRecord = activeParcel.Changes.filter(function(p){return p.AuxRowId && gridData[x].ROWUID && p.AuxRowId.toString() == gridData[x].ROWUID.toString() && p.Action=="new" && ( datafields[p.FieldId] ? datafields[p.FieldId].SourceTable == sourceTable: true ) })
			var icpChanges= activeParcel.Changes.filter(function(p){return p.Action=="icp" && (gridData[x].ROWUID  == p.AuxRowId.split('$')[0]) && ( datafields[p.FieldId] ? datafields[p.FieldId].SourceTable == sourceTable: true ) })     
			//if(curRecChanges && curRecChanges.length >0) flag = 1;
			if(!((curRecChanges && curRecChanges.length > 0) || (NewRecord && NewRecord.length > 0) || (icpChanges && icpChanges.length > 0))){
					if(pcateID)
						$('.sub-multigrid .data-collection-table[catid="'+catId+'"] tbody tr').eq(x).find('.gridEditNotify').hide();
					var isChildChanged = checkHighLightInChild(catId, gridData[x]);
					if(!isChildChanged) {
						if(IsMultiGrid)
							$('.multigrid .data-collection-table[catid="'+ catId +'"] tbody tr').eq(x).find('.gridEditNotify').hide();			
						else
							$('.aux-grid[catid = "'+ catId +'"] .aux-grid-view .data-collection-table tbody tr').eq(x).find('.gridEditNotify').hide();
							//$('.aux-multigrid-view[catid = "'+pcatId+'"] .multigrid tbody tr td .data-collection-table tbody tr').eq(x).find('.gridEditNotify').hide();
							//$('.aux-sub-multigrid-view[catid = "'+pcatId+'"] .sub-multigrid tbody tr[cid="'+catId+'"] td .data-collection-table tbody tr').eq(x).find('.gridEditNotify').hide();	
					}						
			}
			
			//if(flag == 0){
				//(IsMultiGrid)
				//	$('.multigrid .data-collection-table[catid="'+catId+'"]').eq(0).attr('style','margin-left:0px;');
				//$('.aux-grid[catid = "'+catId+'"] .aux-grid-view .data-collection-table').eq(0).attr('style','margin-left:0px;');
			//}
		}
	}
}

var n;
function loadMultiGrid(targetForm) {//, tcid, thisparentpath, thisIndex
	var deleteOption=0
    var thisData = $(targetForm).attr('auxdata');
    var cateID = $(targetForm).attr('catid');
    var findex = $(targetForm).attr('findex');
    var index = $(targetForm).attr('index');
    var parentpath = 'activeParcel' + '.' + thisData + '[' + findex + ']';
    var childObject = {}, childObjectCopy = {};
    var multiGridSortScript = function (targetForm) {
        return '';
    }
    
    //var childTables = parentChild.filter(function (s) { return s.ParentTable == thisData })
    var childTables = fieldCategories.filter(function(f) { return f.ParentCategoryId == cateID });
    $('.multigrid', targetForm).html($('.hiddenGridTemplate', targetForm).html());
    $('.multigrid .data-collection-table', targetForm).forEach(function (tt) {
        $('tbody', tt).html($('tfoot', tt).html());
    });

    if (findex < 0) {
        $('.multigrid', targetForm).fillTemplate([]);
        $('.sub-multigrid', targetForm).fillTemplate([]);
        return;
    }
    
    childTables.forEach(function (item) {
        var childCat = item.Id; // getCategoryFromSourceTable(item.ChildTable).Id;
        var childTargetForm = "";
        if (cachedAuxOptions[childCat]) 
            childTargetForm = cachedAuxOptions[childCat].auxForm
        var sourceTable = item.SourceTable;
            
        //childObject[item.ChildTable] = eval(parentpath + "." + item.ChildTable) || [];
		childObject[item.SourceTable] = ( (thisData == item.SourceTable)? ( eval(parentpath)? [eval(parentpath)]: [] ) : ( eval(parentpath + "." + item.SourceTable) || [] ) );	
        
        // Apply HideRecordExpression filtering
        var filteredFieldData = fieldCategories.filter(function (f) {
            return f.Id == childCat && f.HideRecordExpression !== null;
        });
        if (filteredFieldData.length > 0) {
            let indicesToRemove = [];
            filteredFieldData.forEach(function (categ) {
                for (let uindex = 0; uindex < childObject[sourceTable].length; uindex++) {
                    var isExp = activeParcel.EvalValue(sourceTable + '[' + uindex + ']', categ.HideRecordExpression);
                    if (isExp && isExp != "****") {
                        indicesToRemove.push(uindex);
                    }
                }
            });
            childObject[sourceTable] = childObject[sourceTable].filter(function (_, uindex) {
                return !indicesToRemove.includes(uindex);
            });
        }
        childObjectCopy[childCat] = childObject[sourceTable];
   });
    
    $('.multigrid', targetForm).fillTemplate([childObject]);
    var c=0;
    $('.aux-sub-multigrid-view[catid="' + cateID + '"]').hide();
	for (x in childObjectCopy) {
        var cat_gory = getCategory(x);
        if ( childObjectCopy[x].length > 0 && c == 0 ) {
            var cat_gory_table = cat_gory.SourceTable;
			if( multigridTable == cat_gory_table || multigridTable == "" || multigridTable == null || multigridTable == thisData ) {
        		deleteOption = 1;
				loadMultiGridChild(targetForm, cat_gory_table, null, x);
				c++;
            }
        }
		//else
		//	$('.aux-sub-multigrid-view[catid="' + cateID + '"]').hide();
    }
    $('.aux-grid-view .data-collection-table tbody tr', targetForm).removeClass('multigridSelected');
    $('.aux-grid-view .data-collection-table tbody tr', targetForm).eq(index).addClass('multigridSelected');
    $('.multigrid .data-collection-table tbody tr', targetForm).bind(touchClickEvent, function () {
        if (!scrolling) {
        	moveIndicator=0;
            $('.multigrid .data-collection-table tbody tr', targetForm).removeClass('selectedTR');
            $(this).addClass('selectedTR');
            var tr = $('.multigrid .data-collection-table tbody tr.selectedTR', targetForm)
            var pgrid = $(tr).parents('.data-collection-table').first();
            var cid = $(pgrid).attr('catid');
            var tcat = getCategory(cid);
            var sourceTb = $(pgrid).attr('source');
            multigridTable=sourceTb;
            var gridForm = $('section[catid="' + cid + '"]')
        	$(gridForm).attr('index', ($('tr', pgrid).indexOf(this))-1);
        	var index = parseInt($(gridForm).attr('index'));
        	var findex = $(gridForm).attr('filteredIndexes').split(",");
        	$(gridForm).attr('findex', findex[index]);
            $('.multtiGridDelete', targetForm).hide();
            loadMultiGridChild(targetForm, sourceTb, true, cid);
            if (tcat.EnableDeleteExpression && tcat.EnableDeleteExpression != "") {
                tcat.EnableDeleteExpression = tcat.EnableDeleteExpression.replace('&gt;', '>');
                tcat.EnableDeleteExpression = tcat.EnableDeleteExpression.replace('&lt;', '<');
                var isExp = activeParcel.EvalValue(sourceTb + '[' + findex[index] + ']', tcat.EnableDeleteExpression, null, parent ? parent.source + '[' + parent.index + ']' : null);
                if (isExp == true)
                    $('.multigridActions[catid="' + cid + '"]', targetForm).find('.multtiGridDelete').show();
                else
                    $('.multigridActions[catid="' + cid + '"]', targetForm).find('.multtiGridDelete').hide();
            }
            else {
                if (tcat.DoNotAllowInsertDeleteExpression) {
                    let isExp = false, pcat = getCategory(tcat.ParentCategoryId), rec = activeParcel[sourceTb][findex[index]],
                        pi = activeParcel[pcat.SourceTable] ? activeParcel[pcat.SourceTable].findIndex(x => x.ROWUID == rec.ParentROWUID) : null;

                    if ((pi || pi == 0) && pi > -1) {
                        try {
                            isExp = activeParcel.EvalValue(pcat.SourceTable + '[' + pi + ']', decodeHTML(tcat.DoNotAllowInsertDeleteExpression).replace(new RegExp('parent.', 'g'), ''));
                        }
                        catch (ex) { }

                        if (isExp == true || isExp == 'true') { $('.multigridActions[catid="' + cid + '"]', targetForm).find('.multtiGridDelete').hide(); }
                        else { $('.multigridActions[catid="' + cid + '"]', targetForm).find('.multtiGridDelete').show(); }
                    }
                    else { $('.multigridActions[catid="' + cid + '"]', targetForm).find('.multtiGridDelete').show(); }
                }
                else $('.multigridActions[catid="' + cid + '"]', targetForm).find('.multtiGridDelete').show();
            }
        }
	});

    var t = $('.multigrid', targetForm)[0];
    var currentCatID = $(targetForm).attr('catid');
    var fIndex = $(targetForm).attr('findex');
    var thisIndex = $(targetForm).attr('index');
    var thisparentpath = $(targetForm).attr('ParentPath');
    var parent = {
        source: thisData,
        index: fIndex,
        thisIndex: thisIndex,
        parentpath: thisparentpath
    }
    $('.sub-cat-opener', targetForm).forEach(function (item) {
        var tcid = $(item).attr('catid');
        var iform = $('section[catid="' + tcid + '"]')
		var iIndex=$(iform).attr('index');
        var targetForm = $('section[name="dctab_' + tcid + '"]')[0];
        var aFilters = $(item).attr('filters');
        var aValues = $(item).attr('filtervalues');
        //loadAuxiliaryRecordsWithFilter(targetForm, aFilters, aValues, parent);
        loadAuxiliaryRecordsWithFilter(targetForm, aFilters, aValues, parent,iIndex);
    });
    $('.multigrid .aux-grid-edit', targetForm).bind(touchClickEvent, function (e) {
        e.preventDefault();
        moveIndicator=0;
        relationcycle = [];
        var parentgrid = $(this).parents('.data-collection-table').first()
        var sourceTable = $(parentgrid).attr('source')
        multigridTable=sourceTable;
        var catgID = $(parentgrid).attr('catid')
        var gridForm = $('section[catid="' + catgID + '"]')
        $(gridForm).attr('index', $('.aux-grid-edit', parentgrid).indexOf(this));
        var index = parseInt($(gridForm).attr('index'));
        var filteredIndexes = $(gridForm).attr('filteredIndexes').split(",");
        $(gridForm).attr('findex', filteredIndexes[index]);
        refreshAuxSubLinks(catgID);
        $('.record-classcalculator', gridForm).hide();
        $('.record-massupdate', gridForm).hide();
        $('.aux-records', gridForm).html(filteredIndexes.length);
        $('.aux-index', gridForm).html(index + 1);
        $('fieldset', gridForm).each(function () {
            setFieldValue(this, sourceTable + '[' + filteredIndexes[index] + ']');
        });
        $('.aux-multigrid-view', gridForm).hide();
        $('.aux-sub-multigrid-view', gridForm).hide();
        pagingSetup(filteredIndexes.length, gridForm, index)
        $('.data-entry-tab-content', gridForm).show();
        $('.aux-grid-view', gridForm).hide();
        $(this).attr('state', 'edit')
        recalculateDimensions();
        //selectDcTab(catgID, undefined, index, 1);
        selectDcTab(catgID, undefined, index, 1,undefined,multigridTable);
		//if (formScroller) formScroller.enable();
    });

    $('.multigrid .multtiGridInsert', targetForm).bind(touchClickEvent, function (e) {
    	moveIndicator=0;
        var catid = $(this).parents('tr').attr('cid');
        var cat = getCategory(catid)
        var sourceTable = cat.SourceTableName;
        var sourceData = activeParcel[sourceTable];
        var ParentSpec = cachedAuxOptions[catid] == undefined ? null : cachedAuxOptions[catid].parentSpec;
        var filteredIndexes = cachedAuxOptions[catid].filteredIndexes;
        if (cat.MaxRecords && ((ParentSpec && filteredIndexes.length >= cat.MaxRecords) || (!ParentSpec && sourceData.length >= cat.MaxRecords))) {
            messageBox('Maximum number of records(' + cat.MaxRecords.toString() + ') has been reached for this category')
            return false;
        }
        var f = $(targetForm).attr('findex');
        var table = $(targetForm).attr('auxdata');
        parentAuxRecord = activeParcel[table][f];
        var cId = $(this).parent().attr('catid');
        var target = $('section[catid="' + cId + '"]');
        var valid = checkRequiredFieldsInGrid(catid);
        if (!valid) {
            messageBox(msg_FormNoCreate);
            return false;
        }

        setTimeout(function(){ messageBox('Create a new record?', ["Yes", "No"], function () {
            insertNewAuxRecord(target, parentAuxRecord, cId, null, null, function () {
				//if (formScroller) formScroller.enable();
                reloadAuxiliaryRecords(cId);
                // refreshDataCollectionValues(function () {
                //  reloadAuxiliaryRecords(currentCatID);
                loadMultiGrid(targetForm);
                //  });
                sqlinsert = "";
            });
        });},100);
    })
    if (deleteOption == 0) $('.multtiGridDelete', targetForm).hide();

    $('.multigrid .multtiGridDelete', targetForm).bind(touchClickEvent, function (e) {
    	moveIndicator=0;
        var t = this;
        setTimeout(function(){ messageBox('Are you sure you want to delete the selected record and its descendants?', ["Yes", "No"], function () {
            var tr = $('.multigrid .data-collection-table tbody tr.selectedTR', targetForm)
            n=0;
            var index = $(tr).parents('.data-collection-table').first().find('tbody>tr').indexOf($(t).parents('.multigrid').first().find('tr.selectedTR')[0])
            var cId = $(t).parent().attr('catid');
            var target = $('section[catid="' + cId + '"]');
            var table = $(target).attr('auxdata');
            var filteredIndexes = $(target).attr('filteredIndexes').split(",");
            if(filteredIndexes.length<2){
				n=1;}
            var rowuid = activeParcel[table][filteredIndexes[index]]["ROWUID"]
            console.log(cId, rowuid);
            ccma.Data.Controller.DeleteAuxRecordWithDescendants(cId, rowuid, null, null, function () {
                loadMultiGrid(targetForm);
               /* if(n==1){
					$('.multtiGridDelete', targetForm).hide();n=0;}*/
            }, true);
            var parent = thisparentpath + '.' + $(targetForm).attr('auxdata');
            sqlDelete = ""

        });},100);
    })
    $('.multigrid tr', targetForm).show();
    var childcategories = fieldCategories.filter(function (f) { return (f.ParentCategoryId == $(targetForm).attr('catid') && f.HideExpression && f.HideExpression != '') });
    childcategories.forEach(function (c) {
        var isExp = activeParcel.EvalValue(thisData + '[' + fIndex + ']', c.HideExpression.replace(new RegExp('parent.', 'g'), ''))
        if (isExp) $('.multigrid tr[cid="' + c.Id + '"]', targetForm).hide()
    })
 	childTables.forEach(function (item) {
   		//var childCat = getCategoryFromSourceTable(item.ChildTable);
          if (item && Object.keys(item).length > 0 && item.Id) highlightChangesInGrid(item.Id, true);

          /*if ((ccma.Session.RealDataReadOnly == true || (isBPPUser && activeParcel.IsParentParcel == 'true' && activeParcel.Reviewed == 'true')) && activeParcel.IsRPProperty == 'true') {*/
          if (ccma.Session.RealDataReadOnly == true  && activeParcel.IsRPProperty == 'true') {
              $('.multtiGridDelete', targetForm).hide();
              $('.multtiGridInsert', targetForm).hide();
          }
          else {
              if (item.AllowAddDelete == 'true') $('.multigridActions[catid="' + item.Id + '"]', targetForm).find('.multtiGridInsert').show(); //$('.multtiGridInsert', targetForm).show();
              if (item.DoNotAllowInsertDeleteExpression) {
                  let isExp = false;
                  try {
                      isExp = activeParcel.EvalValue(thisData + '[' + fIndex + ']', decodeHTML(item.DoNotAllowInsertDeleteExpression).replace(new RegExp('parent.', 'g'), ''));
                  }
                  catch (ex) { }

                  if (isExp == true || isExp == 'true') {
                      $('.multigridActions[catid="' + item.Id + '"]', targetForm).find('.multtiGridInsert').hide();
                      $('.multigridActions[catid="' + item.Id + '"]', targetForm).find('.multtiGridDelete').hide();
                  }
                  else {
                      $('.multigridActions[catid="' + item.Id + '"]', targetForm).find('.multtiGridInsert').show();
                      //if (!isZeroRecord) $('.record-delete', auxForm).show();
                  }
              }
    	  }
    });
}

function loadMultiGridChild(targetForm, childSourceTable, ccDelIndex, childId) {
    var thisData = childSourceTable;
	var thisdataparent= $(targetForm).attr('auxdata');
    var sourceData = activeParcel[thisData];
    var catid = $(targetForm).attr('catid');
    var childCat = getCategoryFromSourceTable(childSourceTable,childId);
    var childForm = $('section[catid="' + childCat.Id + '"]')
    var findex = $(targetForm).attr('findex');
    var index = $(targetForm).attr('index');
	var cFindex = $(childForm).attr('findex');
	var cIndex = $(childForm).attr('index');
	var subMultigridSelector = $('.aux-sub-multigrid-view[pcatid="'+childCat.Id+'"]', targetForm)
	if( pIdIndex == 0 ||  trSelectVar == 0 || cIndex == null || cFindex == null || cFindex == -1 || moveIndicator == 1 ){
		var	parentpath = (thisData == thisdataparent)? activeParcel[thisdataparent][findex]: activeParcel[thisdataparent][findex][thisData][0];
		cIndex = 0;
		cFindex = cFindex || 0 ;
		if(cFindex == "-1") cFindex = 0;
		trSelectVar = 1;
		pIdIndex = 1;
		moveIndicator = 0;
		$(childForm).attr('findex',0)
		$(childForm).attr('index',0)
    }
	else
		var	parentpath = activeParcel[thisData][cFindex];
    var childObject = {}
    var multiGridSortScript = function (targetForm) {
        return '';
    }
    
    if( childCat.ParentCategoryId ){ //to show only seleted category child in childmultigrid view
        var childcategories = fieldCategories.filter(function (f) { return ( f.ParentCategoryId == childCat.ParentCategoryId ) });
		childcategories.forEach(function (c) {
			if(childCat.Id == c.Id)
			    $('.aux-sub-multigrid-view[pcatid="' + c.Id + '"] .sub-multigrid', targetForm).show();
			else
			    $('.aux-sub-multigrid-view[pcatid="' + c.Id + '"] .sub-multigrid', targetForm).hide();
		});
    }
    
    //var childTables = parentChild.filter(function (s) { return s.ParentTable == thisData })
    var childTables = fieldCategories.filter(function (f) { return ( f.ParentCategoryId == childCat.Id ) });
    $('.sub-multigrid', subMultigridSelector).html($('.subhiddenGridTemplate', subMultigridSelector).html());
    $('.sub-multigrid .data-collection-table', subMultigridSelector).forEach(function (tt) {
        $('tbody', tt).html($('tfoot', tt).html());
    });

    if (findex < 0) {
        $('.sub-multigrid', subMultigridSelector).fillTemplate([]);
        return;
    }
    
    childTables.forEach(function (item) {
        var _childCat = item.Id; //getCategoryFromSourceTable(item.ChildTable).Id;
        var childTargetForm = "";
        if (cachedAuxOptions[_childCat]) 
            childTargetForm = cachedAuxOptions[_childCat].auxForm

        //childObject[item.ChildTable] =parentpath?parentpath[item.ChildTable]:[] || [];
        childObject[item.SourceTable] = ( (thisData == item.SourceTable)? ( parentpath ? [parentpath]: []) : ( parentpath? parentpath[item.SourceTable]: [] || [] ) );	
    });
    
	var subMultigridSelector = $('.aux-sub-multigrid-view[pcatid="'+childCat.Id+'"]', targetForm)
	if(getCategory(childCat.Id).EnableDeleteExpression && getCategory(childCat.Id).EnableDeleteExpression != ""){
            	getCategory(childCat.Id).EnableDeleteExpression = getCategory(childCat.Id).EnableDeleteExpression.replace('&gt;','>');
    	 	 	getCategory(childCat.Id).EnableDeleteExpression = getCategory(childCat.Id).EnableDeleteExpression.replace('&lt;','<');
				var isExp = activeParcel.EvalValue( thisData + '[' + cFindex + ']', getCategory(childCat.Id).EnableDeleteExpression, null, parent ? parent.source + '[' + parent.index + ']' : null );
    			if ( isExp == true )
					$('.multigridActions[catid="' + childCat.Id + '"]', targetForm).find('.multtiGridDelete').show();
    			else
					$('.multigridActions[catid="' + childCat.Id + '"]', targetForm).find('.multtiGridDelete').hide();
    		}
			else
			$('.multigridActions[catid="' + childCat.Id + '"]', targetForm).find('.multtiGridDelete').show();
    $('.sub-multigrid', subMultigridSelector).fillTemplate([childObject]);
	if(childTables.length>0)
    {
			if($('section[catid="' + catid + '"] .data-entry-tab-content').css('display')=="none")
				$('.aux-sub-multigrid-view[catid="' + catid + '"]').show();
    }
	else{
		$('.aux-sub-multigrid-view[catid="' + catid + '"]').hide();
	}
    $('.aux-multigrid-view .data-collection-table tbody tr', targetForm).removeClass('selectedTR');
    $('.aux-multigrid-view .multigrid tr[cid="' + childCat.Id + '"] .data-collection-table tbody tr', targetForm).eq(cIndex).addClass('selectedTR');
    $('.sub-multigrid .data-collection-table tbody tr', targetForm).bind(touchClickEvent, function () {
        if (!scrolling) {
			moveIndicator=0;
			$('.sub-multigrid .selectedTR').css('background-color',"");
            $('.sub-multigrid .data-collection-table tbody tr', targetForm).removeClass('selectedTR');
            $(this).addClass('selectedTR');
			$('.sub-multigrid .selectedTR').css('background-color','#EDDEB1');
            var tr = $('.sub-multigrid .data-collection-table tbody tr.selectedTR', targetForm)
            var pgrid = $(tr).parents('.data-collection-table').first();
            var cid = $(pgrid).attr('catid');
            var tcat = getCategory(cid);
            var sourceTb = $(pgrid).attr('source');
            var gridForm = $('section[catid="' + cid + '"]')
        	$(gridForm).attr('index', ($('tr', pgrid).indexOf(this))-1);
        	var index = parseInt($(gridForm).attr('index'));
        	var findex = $(gridForm).attr('filteredIndexes').split(",");
        	$(gridForm).attr('findex', findex[index]);
            $('.aux-sub-multigrid-view .multtiGridDelete', targetForm).hide();
            if (tcat.EnableDeleteExpression && tcat.EnableDeleteExpression != "") {
                tcat.EnableDeleteExpression = tcat.EnableDeleteExpression.replace('&gt;', '>');
                tcat.EnableDeleteExpression = tcat.EnableDeleteExpression.replace('&lt;', '<');
                var isExp = activeParcel.EvalValue(sourceTb + '[' + findex[index] + ']', tcat.EnableDeleteExpression, null, parent ? parent.source + '[' + parent.index + ']' : null);
                if (isExp == true)
                    $('.multigridActions[catid="' + cid + '"]', targetForm).find('.multtiGridDelete').show();
                else
                    $('.multigridActions[catid="' + cid + '"]', targetForm).find('.multtiGridDelete').hide();
            }
            else {
                if (tcat.DoNotAllowInsertDeleteExpression) {
                    let isExp = false, pcat = getCategory(tcat.ParentCategoryId), rec = activeParcel[sourceTb][findex[index]],
                        pi = activeParcel[pcat.SourceTable] && rec ? activeParcel[pcat.SourceTable].findIndex(x => x.ROWUID == rec.ParentROWUID) : null;

                    if ((pi || pi == 0) && pi > -1) {
                        try {
                            isExp = activeParcel.EvalValue(pcat.SourceTable + '[' + pi + ']', decodeHTML(tcat.DoNotAllowInsertDeleteExpression).replace(new RegExp('parent.', 'g'), ''));
                        }
                        catch (ex) { }

                        if (isExp == true || isExp == 'true') { $('.multigridActions[catid="' + cid + '"]', targetForm).find('.multtiGridDelete').hide(); }
                        else { $('.multigridActions[catid="' + cid + '"]', targetForm).find('.multtiGridDelete').show(); }
                    }
                    else { $('.multigridActions[catid="' + cid + '"]', targetForm).find('.multtiGridDelete').show(); }
                }
                else $('.multigridActions[catid="' + cid + '"]', targetForm).find('.multtiGridDelete').show();
            }
        }
	});

    var t = $('.sub-multigrid', childForm)[0];
   var currentCatID = $(childForm).attr('catid');
    var fIndex = $(childForm).attr('findex');
    var thisIndex = $(childForm).attr('index');
    var thisparentpath = $(childForm).attr('ParentPath');
   var parent = {
       source: thisData,
       index: fIndex,
       thisIndex: thisIndex,
      parentpath: thisparentpath
   }
    $('.sub-cat-opener', childForm).forEach(function (item) {
       var tcid = $(item).attr('catid');
        var targetForm = $('section[name="dctab_' + tcid + '"]')[0];
       var aFilters = $(item).attr('filters');
       var aValues = $(item).attr('filtervalues');
        loadAuxiliaryRecordsWithFilter(targetForm, aFilters, aValues, parent);
    });
    $('.sub-multigrid .aux-grid-edit', targetForm).bind(touchClickEvent, function (e) {
        e.preventDefault();
		moveIndicator=0;
		relationcycle = [];
        var parentgrid = $(this).parents('.data-collection-table').first()
        var sourceTable = $(parentgrid).attr('source')
        var catgID = $(parentgrid).attr('catid')
        var gridForm = $('section[catid="' + catgID + '"]')
        $(gridForm).attr('index', $('.aux-grid-edit', parentgrid).indexOf(this));
        var index = parseInt($(gridForm).attr('index'));
        var filteredIndexes = $(gridForm).attr('filteredIndexes').split(",");
        $(gridForm).attr('findex', filteredIndexes[index]);
        refreshAuxSubLinks(catgID);
        $('.record-classcalculator', gridForm).hide();
        $('.record-massupdate', gridForm).hide();
        $('.aux-records', gridForm).html(filteredIndexes.length);
        $('.aux-index', gridForm).html(index + 1);
        $('fieldset', gridForm).each(function () {
            setFieldValue(this, sourceTable + '[' + filteredIndexes[index] + ']');
        });
		$('.aux-sub-multigrid-view', gridForm).hide();
        $('.aux-multigrid-view', gridForm).hide();
        pagingSetup(filteredIndexes.length, gridForm, index)
        $('.data-entry-tab-content', gridForm).show();
        $('.aux-grid-view', gridForm).hide();
        $(this).attr('state', 'edit')
        recalculateDimensions();
        selectDcTab(catgID, undefined, index, 1,undefined,multigridTable);
		//if (formScroller) formScroller.enable();
    });

    $('.sub-multigrid .multtiGridInsert', targetForm).bind(touchClickEvent, function (e) {
        var catid = $(this).parents('tr').attr('cid');
		moveIndicator=0;
        var cat = getCategory(catid)
		var subchildForm=$('section[catid="' + catid + '"]');
        var sourceTable = cat.SourceTableName;
        var sourceData = activeParcel[sourceTable];
        var ParentSpec = cachedAuxOptions[catid] == undefined ? null : cachedAuxOptions[catid].parentSpec;
        var filteredIndexes = cachedAuxOptions[catid].filteredIndexes;
        if (cat.MaxRecords && ((ParentSpec && filteredIndexes.length >= cat.MaxRecords) || (!ParentSpec && sourceData.length >= cat.MaxRecords))) {
            messageBox('Maximum number of records(' + cat.MaxRecords.toString() + ') has been reached for this category')
            return false;
        }
        var f = $(childForm).attr('findex');
        var table = $(childForm).attr('auxdata');
        parentAuxRecord = activeParcel[table][f];
        var cId = $(this).parent().attr('catid');
        var target = $('section[catid="' + cId + '"]');
        var valid = checkRequiredFieldsInGrid(catid);
        if (!valid) {
            messageBox(msg_FormNoCreate);
            return false;
        }

        messageBox('Create a new record?', ["Yes", "No"], function () {
            insertNewAuxRecord(target, parentAuxRecord, cId, null, null, function () {
				//if (formScroller) formScroller.enable();
                reloadAuxiliaryRecords(cId);
				var filteredIndexese = $(subchildForm).attr('filteredIndexes').split(",");
				var subindex=$(subchildForm).attr('index');
				var subfindex=$(subchildForm).attr('findex');
				var ind=$(childForm).attr('index');
				var fin= $(childForm).attr('findex');
                // refreshDataCollectionValues(function () {
                //  reloadAuxiliaryRecords(currentCatID);
                loadMultiGrid(targetForm);
				$(subchildForm).attr('filteredIndexes',filteredIndexese.toString())
				$(subchildForm).attr('index',subindex);
				$(subchildForm).attr('findex',subfindex);
				$(childForm).attr('index',ind);
				$(childForm).attr('findex',fin);
				cachedAuxOptions[catid].parentSpec=ParentSpec;
                //  });
                sqlinsert = "";
				
            });
        });
    })
    $('.multtiGridDelete', targetForm).hide();
    $('.sub-multigrid .multtiGridDelete', targetForm).bind(touchClickEvent, function (e) {
        var t = this;
        moveIndicator = 0;
        messageBox('Are you sure you want to delete the selected record and its descendants?', ["Yes", "No"], function () {
            var tr = $('.sub-multigrid .data-collection-table tbody tr.selectedTR', targetForm)
            var index = $(tr).parents('.data-collection-table').first().find('tbody>tr').indexOf($(t).parents('.sub-multigrid').first().find('tr.selectedTR')[0])
            var cId = $(t).parent().attr('catid');
            var target = $('section[catid="' + cId + '"]');
            var table = $(target).attr('auxdata');
            var filteredIndexes = $(target).attr('filteredIndexes').split(",");
            var ParentSpec = cachedAuxOptions[cId] == undefined ? null : cachedAuxOptions[cId].parentSpec;
            var pcateid = getParentCategories(cId);
            var subchildForm = $('section[catid="' + pcateid + '"]');
            var ind = $(childForm).attr('index');
            var fin = $(childForm).attr('findex');
            var rowuid = activeParcel[table][filteredIndexes[index]]["ROWUID"]
            var filteredIndexes = filteredIndexes.splice(index, 1);
            var findd = filteredIndexes[0];
            console.log(cId, rowuid);
            ccma.Data.Controller.DeleteAuxRecordWithDescendants(cId, rowuid, null, null, function () {
                loadMultiGrid(targetForm);
                $(target).attr('filteredIndexes', filteredIndexes.toString())
                $(target).attr('index', index);
                $(target).attr('findex', findd);
                $(childForm).attr('index', ind);
                $(childForm).attr('findex', fin);
                cachedAuxOptions[cId].parentSpec = ParentSpec;

            }, true);
            var parent = thisparentpath + '.' + $(targetForm).attr('auxdata');
            sqlDelete = ""

        })
    });

	if (!ccDelIndex) {
        if (getCategory(childCat.Id).EnableDeleteExpression && getCategory(childCat.Id).EnableDeleteExpression != "") {
            getCategory(childCat.Id).EnableDeleteExpression = getCategory(childCat.Id).EnableDeleteExpression.replace('&gt;', '>');
            getCategory(childCat.Id).EnableDeleteExpression = getCategory(childCat.Id).EnableDeleteExpression.replace('&lt;', '<');
            var isExp = activeParcel.EvalValue(thisData + '[' + cFindex + ']', getCategory(childCat.Id).EnableDeleteExpression, null, parent ? parent.source + '[' + parent.index + ']' : null);
            if (isExp == true)
                $('.multigridActions[catid="' + childCat.Id + '"]', targetForm).find('.multtiGridDelete').show();
            else
                $('.multigridActions[catid="' + childCat.Id + '"]', targetForm).find('.multtiGridDelete').hide();
        }
        else {
            if (childCat.DoNotAllowInsertDeleteExpression) {
                let isExp = false, pcat = getCategory(childCat.ParentCategoryId), rec = activeParcel[thisData][cFindex],
                    pi = activeParcel[pcat.SourceTable] ? activeParcel[pcat.SourceTable].findIndex(x => x.ROWUID == rec.ParentROWUID) : null;

                if ((pi || pi == 0) && pi > -1) {
                    try {
                        isExp = activeParcel.EvalValue(pcat.SourceTable + '[' + pi + ']', decodeHTML(childCat.DoNotAllowInsertDeleteExpression).replace(new RegExp('parent.', 'g'), ''));
                    }
                    catch (ex) { }

                    if (isExp == true || isExp == 'true') { $('.multigridActions[catid="' + childCat.Id + '"]', targetForm).find('.multtiGridDelete').hide(); }
                    else { $('.multigridActions[catid="' + childCat.Id + '"]', targetForm).find('.multtiGridDelete').show(); }
                }
                else { $('.multigridActions[catid="' + childCat.Id + '"]', targetForm).find('.multtiGridDelete').show(); }
            }
            else $('.multigridActions[catid="' + childCat.Id + '"]', targetForm).find('.multtiGridDelete').show();
        }
    }

    $('.sub-multigrid tr', targetForm).show();
    /*
    var childcategories = fieldCategories.filter(function (f) { return (f.ParentCategoryId == $(targetForm).attr('catid') && f.HideExpression && f.HideExpression != '') });
    childcategories.forEach(function (c) {
        var isExp = activeParcel.EvalValue(thisData + '[' + index + ']', c.HideExpression.replace(new RegExp('parent.', 'g'), ''))
        if (isExp) $('.sub-multigrid tr[cid="' + c.Id + '"]', targetForm).hide()
    })
    */
    var childcategories = fieldCategories.filter(function (f) { return (f.ParentCategoryId == childCat.Id && f.HideExpression && f.HideExpression != '') });
    if (cFindex > -1){
	    childcategories.forEach(function (c) {
            var isExp = activeParcel.EvalValue(thisData + '[' + cFindex + ']', c.HideExpression.replace(new RegExp('parent.', 'g'), ''))
	        if (isExp) $('.sub-multigrid tr[cid="' + c.Id + '"]', targetForm).hide()
	    })
    }
 	childTables.forEach(function (item) {
   		//var childCat = getCategoryFromSourceTable(item.ChildTable);
          if (item && Object.keys(item).length > 0 && item.Id) highlightChangesInGrid(item.Id, true, item.Id);

          /*if ((ccma.Session.RealDataReadOnly == true || (isBPPUser && activeParcel.IsParentParcel == 'true' && activeParcel.Reviewed == 'true')) && activeParcel.IsRPProperty == 'true') {*/
          if (ccma.Session.RealDataReadOnly == true  && activeParcel.IsRPProperty == 'true') {
              $('.multigridActions[catid="' + item.Id + '"]', targetForm).find('.multtiGridInsert').hide();
              $('.multigridActions[catid="' + item.Id + '"]', targetForm).find('.multtiGridDelete').hide();
          }
          else {
              if (item.AllowAddDelete == 'true') $('.multigridActions[catid="' + item.Id + '"]', targetForm).find('.multtiGridInsert').show(); 
              if (item.DoNotAllowInsertDeleteExpression) {
                  let isExp = false;
                  try {
                      isExp = activeParcel.EvalValue(thisData + '[' + cFindex + ']', decodeHTML(item.DoNotAllowInsertDeleteExpression).replace(new RegExp('parent.', 'g'), ''));
                  }
                  catch (ex) { }

                  if (isExp == true || isExp == 'true') {
                      $('.multigridActions[catid="' + item.Id + '"]', targetForm).find('.multtiGridInsert').hide();
                      $('.multigridActions[catid="' + item.Id + '"]', targetForm).find('.multtiGridDelete').hide();
                  }
                  else {
                      $('.multigridActions[catid="' + item.Id + '"]', targetForm).find('.multtiGridInsert').show();
                  }
              }
          }
    });
}


function numbervalidation(item, e) {   
	function getAttr(n) {return parseInt($(item).attr(n), 0)}	

	var fieldId = getAttr('field-id'), maxlength = getAttr('maxlength'), precision = getAttr('precision'), scale = getAttr('scale');
	var field = datafields[fieldId], itype = parseInt(field.InputType);
	var str,scaleLength;
	if([8,10].indexOf(itype) > -1){
		precision = precision || maxlength || 20;
		scale = 0;
		if(!(e.keyCode >= 48 && e.keyCode <= 57 )) {e.preventDefault(); return false;}
		if(item.value.length >= precision) {e.preventDefault(); return false;};
	} else {
	   var selLength = item.selectionEnd - item.selectionStart;
	   precision = precision || (maxlength - scale - 1);
        maxlength = (precision + (scale || 0) + 1) || 20;
        var maxIntLength = precision - scale;
        if (item.value.indexOf('.') > -1 && item.selectionStart <= item.value.indexOf('.')) {
            str = item.value.split('.');
            intLength = str[0].length;
            if (intLength >= maxIntLength && !(e.keyCode == 46 || selLength > 0)) { 
                e.preventDefault();
                return false;
            }
        }
        else if (item.value.indexOf('.') > -1 && item.selectionStart > item.value.indexOf('.')) {
            str = item.value.split('.');
            scaleLength = str[1].length;
            if (scaleLength >= scale) {
                e.preventDefault();
                return false;
            }
        } else if (item.value.indexOf('.') === -1 && item.value.length >= maxIntLength && !(e.keyCode == 46 || selLength > 0)) {
            e.preventDefault();
            return false;
        }
       if (!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode == 46) || (e.keyCode == 45))) { e.preventDefault(); return false;}
	   if(((e.keyCode == 46)) && item.value && item.value.indexOf('.') > -1) { e.preventDefault(); return false;}
	   if(((e.keyCode == 45)) && item.value) { e.preventDefault(); return false;}
	   if(selLength == 0 && item.value.length >= maxlength) {e.preventDefault(); return false;};
	   if(selLength == 0 && item.value.replace( '.', '' ).length >= precision) {e.preventDefault(); return false;};
	}
	
}

function focusdate(item) {
    if ($(item).val() == '') {
        dateblank = true;
        firstselect = true;
    }
}

function changedate(item) {
    if (firstselect && ($(item).val() == '' || dateblank == true)) 
        saveInputValue(item);
}

function refreshDataCollectionValues(callback, fieldname, auxsource, newRecordCatId) {

    firstTimeLoad = false;
    var dc = $('#data-collection');
    var dcf = $('#dc-form');

    var fieldsetSelector, loadAuxilaryCatIds = [];
    fieldsetSelector = (firstTimeLoad) ? 'fieldset[calculated]' : 'fieldset[calculated],fieldset[readonlyexp],fieldset[cat_readonlyexp],fieldset[visibilityexp],fieldset[mass-upd]';

    if (fieldname)
        fieldsetSelector = 'fieldset[field-name="' + fieldname + '"]';
    var auxSelector = 'section[auxdata]';
    if (auxsource)
        auxSelector = 'section[auxdata="' + auxsource + '"]';
	
	if (newRecordCatId) {
		var parCatId = getParentCategories(newRecordCatId);
        parCatId ? loadAuxilaryCatIds.push(parCatId, newRecordCatId): loadAuxilaryCatIds.push(newRecordCatId);
        loadAuxilaryCatIds = loadAuxilaryCatIds.concat(getChildCategories(newRecordCatId));
    }
	
    $('.dc-field-alert-flag', dcf).val(activeParcel.FieldAlertType);
    refreshDataCollectionEstimateChart();

    if (!auxsource) {
        $(fieldsetSelector, '#data-collection section[parceldata]').each(function () {
            setFieldValue(this);
        })
        $(fieldsetSelector, '#data-collection section[name="quickreview"]').each(function () {
            setFieldValue(this);
        })
    }

    $('#data-collection ' + auxSelector).each(function () {
        var auxForm = this;
        var catid = $(this).attr('catid');
        var sourceTable = $(this).attr('auxdata');
        var sourceData = activeParcel[sourceTable];
        
		if (!newRecordCatId || ( newRecordCatId && loadAuxilaryCatIds.filter(function(ca) { return ca == catid })[0] )) {
	        try {
	
	            reloadAuxiliaryRecords(catid, true, true);
	        } catch (e) {
	            //(auxForm, sourceData, sourceTable, filteredIndexes, parentSpec, refreshGridOnly)
	            loadAuxiliaryRecords(auxForm, sourceData, sourceTable, null, null);
	        }
	
	        var records = 0;
	        records = sourceData.length;
	        if (records > 0) {
	            var findex = parseInt($(auxForm).attr('findex'));
	            if (isNaN(findex)) {
	                findex = 0;
	            }
	            var updateRecord = function (index) {
	                $(fieldsetSelector, auxForm).each(function () {
	                    setFieldValue(this, sourceTable + '[' + index + ']');
	                });
	            }
	            if (findex > -1)
	                updateRecord(findex);
	
	        }
        }
    });

    firstTimeLoad = true;
    if (callback) callback();

}

/*
function refreshRelatedFields(field, saveMethod, autoselectCall) {
    if (relationcycle.includes(field.Id)) {
        console.log('realational field cycling');
        return false;
    }
    var rfields = _.clone(ccma.UI.RelatedFields[field.Id]);
    if ((rfields == undefined) || (rfields == null)) {
        return false;
    }
    relationcycle.push(parseInt(field.Id));
    rfields = rfields.filter(function(item) {
        if (relationcycle.indexOf(item) == -1) return item;
    });
    var processor = function() {
        var el = $('.newvalue', this);
        if (autoselectCall) autoselectloop.push(sfield.Id)
        setFieldValue(this, null, null, function(sfield) {
            if (($(el).val() == null || ($(el).val() == '')) && sfield.AutoSelectFirstitem == "true") {
                if ($(el).find('option:first-child').val() == '')
                    $(el)[0].selectedIndex = 1;
                else
                    $(el)[0].selectedIndex = 0;
                //setFieldValue(this,null,null,null, sfield, '', true)   
            }
            if (el.length > 0) {
                saveMethod(el[0], function() {
                    recordSwitching.swichingFn(sfield);
                    // refreshRelatedFields(sfield, saveMethod);
                    setTimeout(function(){setFieldValue($(el[0]).parent(), null, null, function(){}, sfield, '', null, true)}, 200);                    
                });
            } else {
                recordSwitching.swichingFn(sfield);
                refreshRelatedFields(sfield, saveMethod);
            }

        }, sfield, '', true);
    }
    var auxform = $('section[catid="' + field.CategoryId + '"]');
    for (var x in rfields) {
        var sfield = datafields[rfields[x]];
		if (sfield && getParentCategories(field.CategoryId) == sfield.CategoryId)
			auxform = $('section[catid="' + sfield.CategoryId + '"]');
		if (sfield.CalculationExpression && sfield.CalculationExpression.search(childRecCalcPattern) > -1) 
			$('fieldset[field-id="' + sfield.Id + '"]', $('section[catid="' + sfield.CategoryId + '"]')).each(processor);
        else $('fieldset[field-id="' + sfield.Id + '"]', auxform).each(processor);
    }
}
*/

function refreshRelatedFields(field, saveMethod, autoselectCall) {
    if (relationcycle.includes(parseInt(field.Id))) {
        console.log('realational field cycling');
        return false;
    }

    let rfields = _.clone(ccma.UI.RelatedFields[field.Id]);
    if (!rfields) return false;

    relationcycle.push(parseInt(field.Id));
    rfields = rfields.filter(item => {
        if(relationcycle.indexOf(item) == -1) return item;
    });

    var processor = function() {
        var el = $('.newvalue', this);
        setFieldValue(this, null, null, function(sfield) {
            if (($(el).val() == null || ($(el).val() == '')) && sfield.AutoSelectFirstitem == "true") {
                if ($(el)[0]?.getLookup?.rows?.length > 0) {
                    let rows = $(el)[0].getLookup.rows;
                    if ((rows[0].Id == '' || rows[0].Id == null) && rows[1]?.Id) $(el).val(rows[1].Id);
                    else if (!(rows[0].Id == '' || rows[0].Id == null)) $(el).val(rows[0].Id);
                }
            }
            else {
                saveMethod(el[0], function () {
                    setTimeout(() => { setFieldValue($(el[0]).parent(), null, null, function () { }, sfield, '', null, true) }, 200);
                }, null, null, true);
            }
        }, sfield, null, null, true, true);
    }

    var auxform = $('section[catid="' + field.CategoryId + '"]');
    for (var x in rfields) {
        var sfield = datafields[rfields[x]];
	    if (sfield && getParentCategories(field.CategoryId) == sfield.CategoryId)
	    auxform = $('section[catid="' + sfield.CategoryId + '"]');
	    if (sfield.CalculationExpression && sfield.CalculationExpression.search(childRecCalcPattern) > -1)
		    $('fieldset[field-id="' + sfield.Id + '"]', $('section[catid="' + sfield.CategoryId + '"]')).each(processor);
	    else $('fieldset[field-id="' + sfield.Id + '"]', auxform).each(processor);
    }
}

function dateTolocal(dt) {
    dt = dt.trim();
    //var Notdt = "";
    //if(isNaN(Date.parse(dt))==true){
    //	return toSQLDateString(Notdt);
    //}
    if (dt.length > 12) {
        dt = dt.replace(' ', 'T');
        dt = dt.replace('AM', '').replace('PM', '');
    }
    else { return dt.trim(); }
    var d = new Date(dt);
    if (d == 'Invalid Date') {
        dt = dt.split('T')[0];
        d = new Date(dt);
        return d.toISOString().substring(0, 10);
    }
    var timeZoneFromDB = parseInt((clientSettings["TimeZone"] || '-05:00').replace(":", ".")); //time zone value from database
    var dst = parseInt(clientSettings["DST"]) || 0; //DST value from database
    if (dst == '1') {
        var startDate = getDSTStart(d.getFullYear());
        var endDate = getDSTEnd(d.getFullYear());
        if (d >= startDate && d <= endDate) {
            timeZoneFromDB = timeZoneFromDB + dst;
        }
    }
    var tzDifference = timeZoneFromDB * 60 * 60 * 1000;
    var offsetTime = new Date(d.getTime() + tzDifference);
    return toSQLDateString(offsetTime);
}
function getDSTStart(year) {
    var dstYear, dstStartWeek, dstDay, dstdate;
    dstDay = '03/01/' + year;
    dstDay = new Date(dstDay);
    dstStartWeek = dstDay.getDay() + 1;
    switch (dstStartWeek) {
        case 1:
            dstdate = new Date(dstDay.setTime(dstDay.getTime() + 170 * 3600000));
            return dstdate;
            break;
        case 2:
            dstdate = new Date(dstDay.setTime(dstDay.getTime() + 314 * 3600000));
            return dstdate;
            break;
        case 3:
            dstdate = new Date(dstDay.setTime(dstDay.getTime() + 290 * 3600000));
            return dstdate;
            break;
        case 4:
            dstdate = new Date(dstDay.setTime(dstDay.getTime() + 266 * 3600000));
            return dstdate;
            break;
        case 5:
            dstdate = new Date(dstDay.setTime(dstDay.getTime() + 242 * 3600000));
            return dstdate;
            break;
        case 6:
            dstdate = new Date(dstDay.setTime(dstDay.getTime() + 218 * 3600000));
            return dstdate;
            break;
        case 7:
            dstdate = new Date(dstDay.setTime(dstDay.getTime() + 194 * 3600000));
            return dstdate;
            break;

    }

}
function getDSTEnd(year) {
    var dstYear, dstEndWeek, dstDay, dstDate;
    dstDay = '11/01/' + year;
    dstDay = new Date(dstDay);
    //dstDay=dstDay.setDate(dstDay.getDate() + 7);
    dstEndWeek = dstDay.getDay() + 1;
    switch (dstEndWeek) {
        case 1:
            dstDate = new Date(dstDay.setTime(dstDay.getTime() + 2 * 3600000));
            return dstDate;
            break;
        case 2:
            dstDate = new Date(dstDay.setTime(dstDay.getTime() + 146 * 3600000));
            return dstDate;
            break;
        case 3:
            dstDate = new Date(dstDay.setTime(dstDay.getTime() + 122 * 3600000));
            return dstDate;
            break;
        case 4:
            dstDate = new Date(dstDay.setTime(dstDay.getTime() + 98 * 3600000));
            return dstDate;
            break;
        case 5:
            dstDate = new Date(dstDay.setTime(dstDay.getTime() + 74 * 3600000));
            return dstDate;
            break;
        case 6:
            dstDate = new Date(dstDay.setTime(dstDay.getTime() + 50 * 3600000));
            return dstDate;
            break;
        case 7:
            dstDate = new Date(dstDay.setTime(dstDay.getTime() + 26 * 3600000));
            return dstDate;
            break;
    }

}

function setFieldValue(fieldset, source, customddl, callback, callbackdata, defaultValue, isRefreshCall, autoSelectCallbackCheck, allowTxtRefreshCall) {
    var fieldProcessor = function (callback) { if (callback) callback(); }
    var fval = "";
    var fname = $(fieldset).attr('field-name');
    var ftype = $(fieldset).attr('field-type');
    var fcalc = $(fieldset).attr('calc-exp');
    var fOvcalc = $(fieldset).attr('override-calc-exp');
    //mspchange--s
    if ( !activeParcel ) return;
    var fieldId = $(fieldset).attr('field-id');
    var readonlyexp = $(fieldset).attr('readonlyexp'), visibilityexp = $(fieldset).attr('visibilityexp'), Requiredexp = $(fieldset).attr('Requiredexp'), cat_readonlyexp = $(fieldset).attr('cat_readonlyexp');
    var sectionControl = $(fieldset).parents('section[catid]');
    var sourceTable = (sectionControl) ? $(sectionControl).attr('auxdata') : null;
    var findex = (sectionControl) ? $(sectionControl).attr('findex') : null;
    var index = (sectionControl) ? $(sectionControl).attr('index') : null;
    if (((findex != null || findex != undefined) && parseInt(findex) < 0) || (sourceTable && !findex) || (sourceTable && !activeParcel[sourceTable][findex])) return;
    var rowuid = (sectionControl && checkIsNull(findex)) ? eval('activeParcel.' + sourceTable + '[' + findex + '].ROWUID') : null;
    var roByexp = (readonlyexp) ? ((sectionControl && sourceTable) ? (activeParcel.EvalValue(sourceTable + '[' + findex + ']', readonlyexp)) : (activeParcel.EvalValue(readonlyexp))) : false;
    var cat_roByexp = (cat_readonlyexp) ? ((sectionControl && sourceTable) ? (activeParcel.EvalValue(sourceTable + '[' + findex + ']', cat_readonlyexp)) : (activeParcel.EvalValue(cat_readonlyexp))) : false;
    var viByexp = (visibilityexp) ? ((sectionControl && sourceTable) ? (activeParcel.EvalValue(sourceTable + '[' + findex + ']', visibilityexp)) : (activeParcel.EvalValue(visibilityexp))) : false;
    var isrByexp = (Requiredexp) ? ((sectionControl && sourceTable) ? (activeParcel.EvalValue(sourceTable + '[' + findex + ']', Requiredexp)) : (activeParcel.EvalValue(Requiredexp))) : false;
    var isfOvcalc = (fOvcalc) ? ((sectionControl && sourceTable) ? (activeParcel.EvalValue(sourceTable + '[' + findex + ']', fOvcalc)) : (activeParcel.EvalValue(fOvcalc))) : false;
    var isClassCalcField
    //mspchange-e
    var field = datafields[fieldId];
    if (fcalc && fcalc != '' && !isfOvcalc && field.InputType != "5")
        $(fieldset).attr("ro", true);
    else if (fcalc && fcalc != '' && isfOvcalc && field.InputType != "5")
        $(fieldset).attr("ro", false);
    if (sourceTable && !source)
        source = sourceTable + '[' + findex + ']';
    var cat = getCategory($(sectionControl).attr('catid'))
    var autosize = function(el){
		if(el.scrollHeight==0){
	   		iPad?el.style.cssText = 'height:' + 40 + 'px' : el.style.cssText = 'height:' + 36 + 'px';
	   	}
	   	else {		
			el.style.cssText = 'height:auto';
			el.style.cssText = 'height:' + el.scrollHeight + 'px';
			iPad?el.style.maxHeight = '148px':el.style.maxHeight = '144px';			
	   	}
 	}
	$('textarea' ,fieldset).unbind('input keyup');
 	$('textarea' ,fieldset).bind('input keyup',function(){
 		autosize(this);
	});

    var lookupEval = function (lkd, lkdEval, _val) { 
    	/*var emp_opt = false; 
        if (lkdEval) {
           var _nval = _val, extra_lk = null;
		   if ( ( _nval && _nval != '' ) || _nval === 0) {
			   extra_lk =  lkd.filter(function(x) { return x.Id == _nval.toString(); })[0]? lkd.filter(function(x) { return x.Id == _nval.toString(); })[0]: null;
		   }
		   var emp_lk = lkd.filter(function(x) { return x.Id == '' || x.Value =='<blank>' })[0]? lkd.filter(function(x) { return x.Id == '' || x.Value =='<blank>' })[0]: null;
           lkd = [];
           if (emp_lk && !extra_lk) lkd.push(emp_lk);
		   if (extra_lk) lkd.push(extra_lk);
		   if (lkd.length == 1 || lkd.length == 0 ) emp_opt = true;
        }
        lkd.forEach(function(x,index) { 
			  if(x.Id) lkd[index].Id = encodeURI(x.Id);
		});
    
		var elem = $('.newvalue', $(fieldset));
		$(elem).html('<option value="${Id}">${Name}</option>');
		$(elem).fillTemplate(lkd);
		isCustomddl = false;
		var optionsCount = clientSettings.LookUpOptionsCount || 25
		
		$(elem).unbind('touchstart mousedown');
		
		if ((lkd && lkd.length > optionsCount) || lkdEval || (field.IsLargeLookup == 'true' || field.IsLargeLookup == true)) {
			isCustomddl = true;
			$(elem).attr('customddl', '');
			if (lkdEval) $(elem).attr('customlkd', '1');
			if (lkdEval && emp_opt) {
               $(elem).bind('touchstart mousedown', function () {
                    if (event) {
                        event.preventDefault();
                        var _csel = event.target? event.target: this;
                        openCustomlkd(_csel);
                    }
                });
            }
		}
		if (field.DoNotAllowSelectNull == true || field.DoNotAllowSelectNull == 'true')
			$(elem).find('option[value=""]').attr('disabled', true)
		if (field.InputType == 3 && field.InputType == 3 && field.RadioFieldValues && field.RadioFieldValues != "")
			$(elem).find('option:last-child').attr('disabled', true)
		if (isCustomddl) {
			$('.searchCustomddl', $(fieldset)).remove();
			var cLink = document.createElement('a');
			if (field.ReadOnly == 'false'&& !readOnly)
				$(cLink).addClass("searchCustomddl");
			$(cLink).attr({ 'onclick': 'return openCustomddlList(this);' });
			$(fieldset).append(cLink);
			isCustomddl = false;
		}
		else {
			$('.newvalue', $(fieldset)).removeAttr("customddl");
			$('.searchCustomddl', $(fieldset)).remove();
		}*/
		LFields.splice(LFields.indexOf(field.Id), 1);
		lookupDataCounter = lookupDataCounter - 1;
		recordSwitching.swichingFn();
    }    

    if (field) {
        isClassCalcField = cat && cat.ClassCalculatorParameters && cat.ClassCalculatorParameters.split(',')[0] == field.Name
        if (field.RequiredIfRecordEdited == "true") {
            $(fieldset).attr("requiredIfEdited", true)
        }
        if ( (field.InputType == "5")  /* && (field.LookupTable == '$QUERY$') ( (field.LookupTable == '$QUERY$') || ( (field.LookupTable != '$QUERY$') && lookup[field.LookupTable] && Object.keys(lookup[field.LookupTable]).length > 50 ) )*/ ) {
            fieldProcessor = function (callback) {
                var ds = activeParcel;
                if (sourceTable)
                    ds = activeParcel[sourceTable][findex];
                if (defaultValue)
                    ds[field.Name] == defaultValue;
                LFields.push(field.Id);
                lookupDataCounter = lookupDataCounter + 1;
                getLookupData(field, { category: cat, source: ds }, function (ld) {
                    var clookupEval = true;
                    if (ld?.length < 20) {
                        let ndt = ccma.Data.Types[parseInt(field.InputType)];
                        ld.forEach((dk) => {
                            dk.Id = getLargeDecToRound(dk.Id, ndt); dk.Name = getLargeDecToRound(dk.Name, ndt);
                        });
                    }
                    $('input.cc-drop.newvalue',fieldset)[0].getLookup.setData(ld);
	             	//if (ld.length < 30) { clookupEval = false; lookupEval(ld, clookupEval); }
                    if (callback) callback(ld, clookupEval);
                })
            }
        }
		if(field.InputType=="6") {
         	var txt = $('textarea[field-id="'+fieldId+'"]');
            autosize(txt[0]);
        }        
    }

    var noConvertionReq = clientSettings['NoTimeConversion'] || '0';
    var isExisting = $(fieldset).parents('section[catid]').attr('actualdata') ? true : false;
    var readOnly = ($(fieldset).attr("ro") == "true");
    let isNewStatusRecord = sourceTable && findex && eval('activeParcel["' + sourceTable + '"][' + findex + ']["CC_RecordStatus"]') == "I" ? true : false;
    if (fname != null) {
        fieldProcessor( function (lkdt, clEval) {
            if ( !activeParcel ) return;
            var auxRowId = null;
            if (source)
                if (typeof source == "string")
                    auxRowId = eval('activeParcel.' + (source ? source + '.' : '') + 'ROWUID');
                else if (typeof source == "object")
                    auxRowId = source["ROWUID"];

            $('label', $(fieldset)).attr({ 'value': '', 'oldvalue': '' });
            
            var v = activeParcel.Eval(source, fname);
            
            if (field && !isfOvcalc && field.CalculationExpression && field.CalculationExpression.trim() != '' && field.CalculationExpression.search(childRecCalcPattern) > -1) {
            	var tempVal = calculateChildRecords(field, findex);
            	v = ((tempVal == null)? v: tempVal);
        	}
            
            if(field && !isfOvcalc && field.CalculationExpression && sourceTable && (v.Value || v.Value === 0) && v.Value.toString() != activeParcel[sourceTable][findex][fname]) {
            	var orginalValue = activeParcel[sourceTable][findex].Original[fname];
				ccma.Sync.enqueueParcelChange(activeParcel.Id, checkIsNull(rowuid) ? rowuid : '', null, 'E', fname, fieldId,  orginalValue ? orginalValue : null,(v.Value || v.Value=="0" || v.Value==0) ? v.Value.toString() : null,  { updateData: true, updateObject: true, object: rowuid ? activeParcel[sourceTable][findex] : null, source: sourceTable }, function () { });
				sqlinsert = "updated";
          	}
	      	else if(field && !isfOvcalc && field.CalculationExpression && !sourceTable && field.IsCustomField == "true" && (v.Value || v.Value === 0) && v.Value.toString() != activeParcel[fname]){
				var directSql  = "UPDATE Parcel SET "+fname+" = '"+v.Value+"' WHERE Id = "+activeParcel.Id
				getData(directSql, [], function () { console.log('Query executed successfully') });
			}
            if (fieldId && field.UIProperties && field.UIProperties != null) {
                v.Value = customFormatValue(v.Value, fieldId);
                v.Original = customFormatValue(v.Original, fieldId)
            }
            if (lkdt && clEval) lookupEval(lkdt, clEval, v.Value);
            var fprocessor = function () {
                if (parseInt(ftype) == 4) {
                    if (v.Value && v.Value != '')
                        v.Value = noConvertionReq == '1' ? v.Value.substring(0, 10) : dateTolocal(v.Value);
                    if (v.Original && v.Original != '')
                        v.Original = noConvertionReq == '1' ? v.Original.substring(0, 10) : dateTolocal(v.Original);
                }
                if (parseInt(ftype) == 10) {
                    if (v.Value)
                        v.Value = Math.round(v.Value);
                }
                if (parseInt(ftype) == 13) {
                	var radioOptions = [];
					if (field.RadioFieldValues && field.RadioFieldValues != "") {
            			var values = []; 
            			values = field.RadioFieldValues.trim().split(',');
            			radioOptions = [{ name : field.Id, text: 'Yes', value: values[0] }, { name : field.Id, text: 'No', value: values[1] }];
        			}
        			else
	        			radioOptions = [{ name : field.Id, text: 'Yes', value: 'true' }, { name : field.Id, text: 'No', value: 'false' }];
	        		$('radiogroup', $(fieldset)).css({'width': '131px'});
	    			if(field.AllowRadioNull == "true"){
	    				var nullOption = { name : field.Id, text: 'Blank', value: '' }
	    				radioOptions.push(nullOption);
	    				$('radiogroup', $(fieldset)).css({'width':'210px'});
	    			}
    				var rbutton = $('.RadioButtonGroup',$(fieldset))[0];
        			$(rbutton).html('<input type="radio" class="radioButton" name=${name} value=${value}>${text}</input>&nbsp;');
        			$(rbutton).fillTemplate(radioOptions)
        			$(fieldset).append(rbutton);
                	$('input', $(fieldset)).removeAttr('checked'); 
                	$('input[value = "'+ v.Value +'"]', $(fieldset)).prop( "checked", true );
                }
                else {
                	if (parseInt(ftype) == 5){
                		$('.newvalue', $(fieldset)).val(v.Value);
                    } else
                		$('input', $(fieldset)).val(v.Value);
                }
                $('textarea', $(fieldset)).val(v.Value);
				if(parseInt(ftype) == "6"){
                    var val = $('textarea' ,$(fieldset));
                      autosize(val[0]);
                  }                
                var addNonExit = function () {
                    if(parseInt(ftype) == 5) {
                        let lp = $('.cc-drop',fieldset)[0] && $('.cc-drop',fieldset)[0].getLookup;
                        if(lp){
                            let rows = { ...lp.rows } , isExists = false;
                            for (var k of Object.keys(rows)) {
                                let tId = __decodeURI(rows[k].Id);
                                if (rows[k].Id === '') {
                                    if (v.Value == null || v.Value === '') {
                                        isExists = true;
                                        break;
                                    }
                                } else if (tId == v.Value) {
                                    isExists = true;
                                    break;
                                }
                            }                        
                            if(!isExists) {
                                lp.addNonExist(v.Value);
                            }
                        }
                    } else {
                        $('select option[nonexist]', $(fieldset)).remove();
                        if (v.Value && $('select', $(fieldset)).length > 0 && (!field.RadioFieldValues || field.RadioFieldValues == "") && ('select', $(fieldset)).html().indexOf('value="' + encodeURI(v.Value) + '"') == -1) {
                            var nonexist = document.createElement('option');
                            $(nonexist).attr({ 'nonexist': '', 'value': encodeURI(v.Value) });
                            $(nonexist).html(v.Value);
                            $('select', $(fieldset)).append(nonexist);
                        }
                    }
                }
                if (parseInt(ftype) == 11)
                    ccma.Data.Evaluable.setLookupList($('select', $(fieldset))[0], ((v.Value) ? v.Value : '').split(','));
                else {
                    if ( field )                  
                       addNonExit()
                    if (parseInt(ftype) == 5)
                		$('.newvalue', $(fieldset)).val(v.Value);
                	else
                    	$('select', $(fieldset)).val(encodeURI(v.Value));
                }             
                if (v.Value) $('label', $(fieldset)).attr({ 'value': v.Value });
                if (v.Original) $('label', $(fieldset)).attr({ 'oldvalue': v.Original });
                if (auxRowId) $('label', $(fieldset)).attr('rowuid', auxRowId);
                $('label', $(fieldset)).html(readOnly ? v.DisplayValue : v.DisplayOriginal);
                if (!readOnly) $('label', $(fieldset)).hide();             
                if (v.Value != v.Original) $('label', $(fieldset)).show();
                var lastEvalHidden = $(fieldset).attr("eval-hi");
                var lastEvalReadOnly = $(fieldset).attr("eval-ro");
                var lastFIndex = $(fieldset).attr("last-findex");
                $(fieldset).attr("last-findex", findex);

                let categoryLevelReadOnlyExpressionOverride = getReadOnlyExpressionOverrideVal(field);

                /*if (roByexp || (field && field.DoNotAllowEditExistingValue == 'true' && !isNewStatusRecord) || (cat_roByexp && !categoryLevelReadOnlyExpressionOverride) || isExisting || isClassCalcField || (field && field.DoNotEditFirstRecord == "true" && index == 0) || (cat && cat.DoNotEditFirstRecord == "true" && index == 0) || (isBPPUser && activeParcel.IsRPProperty && (ccma.Session.RealDataReadOnly || (clientSettings['PersonalPropertyDownloadType'] == 'Both' && clientSettings['PersonalPropertyLinkedRPDataReadOnly'] == '1' && ParcelsNotInGroup.indexOf(parseInt(activeParcel.Id)) > -1))) || (isBPPUser && (activeParcel.CC_Deleted == "true" || (activeParcel.IsRPProperty == 'true' && activeParcel.IsParentParcel == 'true' && activeParcel.Reviewed == 'true'))))*/
                if (roByexp || (field && field.DoNotAllowEditExistingValue == 'true' && !isNewStatusRecord) || (cat_roByexp && !categoryLevelReadOnlyExpressionOverride) || isExisting || isClassCalcField || (field && field.DoNotEditFirstRecord == "true" && index == 0) || (cat && cat.DoNotEditFirstRecord == "true" && index == 0) || (isBPPUser && activeParcel.IsRPProperty && (ccma.Session.RealDataReadOnly || (clientSettings['PersonalPropertyDownloadType'] == 'Both' && clientSettings['PersonalPropertyLinkedRPDataReadOnly'] == '1' && ParcelsNotInGroup.indexOf(parseInt(activeParcel.Id)) > -1))) || (isBPPUser && activeParcel.CC_Deleted == "true" ))
                {
                    // $('span[readonlyexp]', $(fieldset)).html(v.Value);
                    $('span[readonlyexp]', $(fieldset)).show();
                    $('.newvalue', $(fieldset)).hide();
                    $('.cusDpdown-arrow', $(fieldset)).hide();
                    $('label', $(fieldset)).hide();
                    $('.searchCustomddl', $(fieldset)).hide();
                    $(fieldset).attr('eval-ro', '1');
                    $('span[readonlyexp]', $(fieldset)).html('');
                    $( 'span[readonlyexp]', $( fieldset ) ).html( (isClassCalcField && parseInt(ftype) != 5) ? v.Value : v.DisplayValue || v.Value );
                    if (parseInt(ftype) == 12) $('.LoadGeolocationIcon', $(fieldset)).hide();
                }
                else {
                    $('span[readonlyexp]', $(fieldset)).hide();
                    $('.newvalue', $(fieldset)).show();
                    if (v.Value != v.Original) $('label', $(fieldset)).show();
                    if (parseInt(ftype) == 5/*&& $('.newvalue', $(fieldset)).attr('customlkd') == 1*/) {
                    	//$('.newvalue', $(fieldset)).hide();
                    	//$('.newvalue', $(fieldset)).siblings('.cusDpdown').show();
                    	//$('.newvalue', $(fieldset)).siblings('.cusDpdown').children('span').html(v.DisplayValue);
                    	//$('.newvalue', $(fieldset)).parent('.cusDpdown').show();
                    	// $('.newvalue', $(fieldset)).html(v.DisplayValue? (v.DisplayValue == '???'? (v.Value || v.Value == 0? v.Value: ''): v.DisplayValue): '');
                    	$('.oldvalue', $(fieldset)).removeAttr('iwidth');
						if ($(fieldset)[0]) recalculateCustomDropdownWidth(fieldset);
                    }
                    $('.searchCustomddl', $(fieldset)).show();
                    $( fieldset ).attr( 'eval-ro', '0' );
                    if (parseInt(ftype) == 12) $('.LoadGeolocationIcon', $(fieldset)).show();
                    if ( readOnly ) { 
                    	$( 'label', $( fieldset ) ).show();
                    	if (parseInt(ftype) == 12) $('.LoadGeolocationIcon', $(fieldset)).hide();
                    }
                    else if(parseInt(ftype) == 5) $('.cusDpdown-arrow', $(fieldset)).show(); 
                }
                

                //FD 24123  
                if (clientSettings.LoadMethodAfterAppLoad == "loadValidationTablesForMI") {
                    if (field && field['UI_Settings'] && field['UI_Settings'].MIFields && field['UI_Settings'].MIFields == "true")
                        applyConditionalValidationForMI(field, fieldset, activeParcel[sourceTable][findex])
                }

                //if ( field && field.ConditionalValidationConfig )
                //     applyConditionalValidation( field, fieldset, activeParcel[sourceTable][findex] )

                //if (clientSettings.LoadMethodAfterAppLoad == "loadValidationTablesForVS8")
                if (clientSettings.LoadMethodAfterAppLoad && clientSettings.LoadMethodAfterAppLoad != ''){
                    if (field && field.ConditionalValidationConfig)
                        applyConditionalValidation(field, fieldset, activeParcel[sourceTable][findex])
                }
                if ((fcalc != '') && (fcalc != undefined)) {
                    $('input', $(fieldset)).hide();
                    if (!(isfOvcalc || (field.ReadOnlyExpression && field.ReadOnlyExpression != '' && !roByexp)) && $('span[readonlyexp]', $(fieldset))[0]) {
                        $('.oldvalue', $(fieldset)).text('');
                        $('label', $(fieldset)).hide();
                        if ($('span[readonlyexp]', $(fieldset)).css('display') == 'none') $('span[readonlyexp]', $(fieldset)).html((isClassCalcField && parseInt(ftype) != 5) ? v.Value : v.DisplayValue || v.Value);
                    }
                    $('span[readonlyexp]', $(fieldset)).show();
                    if (isfOvcalc) {
                    	$('span[readonlyexp]', $(fieldset)).hide();
                        $('input', $(fieldset)).show();
                        if (field.ReadOnly == 'true' && $('.calcOverideExp', $(fieldset))[0]) $('.calcOverideExp', $(fieldset)).css('display', 'inline !important')
                        //$('.oldvalue', $(fieldset)).text('');
                    }
                    else {
                        if (field.ReadOnly == 'true' && $('.calcOverideExp', $(fieldset))[0]) $('.calcOverideExp', $(fieldset)).css('display', '');
                    	var fCalVal = (v.Value || v.Value === 0) ? v.Value.toString() : v.Value
                        if (source) activeParcel[sourceTable][findex][fname] = fCalVal;
                        else activeParcel[fname] = fCalVal;
                    }
                   
                    if (field.ReadOnlyExpression && field.ReadOnlyExpression != '' && !roByexp) {
                    	$('input', $(fieldset)).show();
                    	$('.oldvalue', $(fieldset)).text('');
                    	$('span[readonlyexp]', $(fieldset)).hide();
                    }
                }
                if ((findex != lastFIndex) && (((viByexp && viByexp != 'null') && (lastEvalHidden != $(fieldset).attr("eval-hi"))))) {
                    if (!firstTimeLoad && !isExisting) {
                        $('span[readonlyexp]', $(fieldset)).html('');
                        $('.newvalue', $(fieldset)).val('');
                        if (v.Value != '') {
                            console.log("entered in setfieldvalue");
                            ccma.Sync.enqueueParcelChange(activeParcel.Id, checkIsNull(rowuid) ? rowuid : '', null, 'E', fname, fieldId, v.Value, '', { updateData: true, updateObject: true, object: rowuid ? activeParcel[sourceTable][findex] : null, source: sourceTable }, function () {
                            });
                        }
                    }
                }
                if (Requiredexp && Requiredexp != '') {
                    if (isrByexp) {
                        $('.reqd', $(fieldset)).remove();
                        var reqd = document.createElement('span');
                        $(reqd).addClass('reqd');
                        reqd.innerHTML = '*';
                        $(fieldset).prepend(reqd);
                        $('input, textarea, select, .cusDpdownSpan', $(fieldset)).attr('required', true);
                    }
                    else {
                        $('.reqd', $(fieldset)).remove();
                        $('input, textarea, select, .cusDpdownSpan', $(fieldset)).removeAttr('required');
                    }
                }

                if (viByexp) {

                    $(fieldset).hide();
                    $(fieldset).attr('eval-hi', '1');
                }
                else {
                    $(fieldset).attr('eval-hi', '0');
                    $(fieldset).show();
                }

                if ($('.newvalue', $(fieldset))[0] && $('.newvalue', $(fieldset)).attr('customWidth')) {
                    $('.newvalue', $(fieldset)).removeAttr('customWidth');
                    $('.newvalue', $(fieldset)).css('width', '');
                }

                if (parseInt(ftype) == 5 && $(fieldset)[0] && $(fieldset).css('display') != 'none' && $('.newvalue', $(fieldset)).css('display') != 'none') {
                    recalculateCustomDropdownTextWidth(fieldset);
                }

                /*var autoSelect = function () {
                    if ($(elem).find('option:first-child').val() == '')
                        $(elem)[0].selectedIndex = 1;
                    else
                        $(elem)[0].selectedIndex = 0;
                    var autoSelectField = datafields[$(elem).attr('field-id')];
                    var sectionElement  = $(elem).parents('section[catid]');
                    var sourceTbl = $(sectionElement).attr('auxdata');
                    var sectionFindex = parseInt($(sectionElement).attr('findex'));
                    var currentFieldValue = sourceTbl ? activeParcel[sourceTbl][sectionFindex][autoSelectField.Name] : activeParcel[autoSelectField.Name];
                    if ( (!callback || autoSelectCallbackCheck) && ( $( elem ).val() != '' || v.Value ) && currentFieldValue != decodeURI($( elem ).val())) { autoLoop = true; saveInputValue( $( elem )[0], function () { setFieldValue( fieldset, source, customddl, callback, callbackdata, defaultValue ) }, true ); }
                    //if ( (isRefreshCall) && (v.Value !=null && $('select', $(elem)).find('option[value="' + encodeURI(v.Value) + '"]').length == 0)) { autoLoop = true; saveInputValue( $( elem )[0], function () { setFieldValue( fieldset, source, customddl, callback, callbackdata, defaultValue ) }, true ); }
                }*/
                
                var autoSelect = function(lookupParam) {
                    if (lookupParam) {
                        if (lkdt[0] && (lkdt[0].Id == '' || lkdt[0].Value == '<blank>') && lkdt[1]) {
                                $(elem).val((lkdt[1].Id || lkdt[1].Id == 0)? lkdt[1].Id: '');
                                $(elem).html((lkdt[1].Name || lkdt[1].Name == '' || lkdt[1].Name == 0)? lkdt[1].Name: ((lkdt[1].Id || lkdt[1].Id == 0)? lkdt[1].Id: ''));
                        }
                        else if(!lkdt[0] || (lkdt[0] && lkdt[0].Id == null)) {
                                $(elem).val('');
                                $(elem).html('');                      		
                        }
                        else {
                                $(elem).val((lkdt[0].Id || lkdt[0].Id == 0)? lkdt[0].Id: '');
                                $(elem).html((lkdt[0].Name || lkdt[0].Name == '' || lkdt[0].Name == 0)? lkdt[0].Name: ((lkdt[0].Id || lkdt[0].Id == 0)? lkdt[0].Id: ''));
                        };
                    } else {
                        if ($(elem).find('option:first-child').val() == '') $(elem)[0].selectedIndex = 1;
                        else $(elem)[0].selectedIndex = 0;
                    }
                    const currentFieldValue = source ? eval('activeParcel.' + source + '.' + field.Name) : activeParcel[field.Name];
                    let tValue ;
                    try {
                        tValue = decodeURI($(elem).val());
                    } catch {
                        tValue = $(elem).val();
                    }
                    if ((!callback || autoSelectCallbackCheck) && ($(elem).val() != '' || v.Value) && currentFieldValue != tValue) {
                        autoLoop = true;
                        saveInputValue($(elem)[0], function() {
                            setFieldValue(fieldset, source, customddl, callback, callbackdata, defaultValue);
                        });
                    }
				}
				
                var elem = $('.newvalue', $(fieldset));
                
                /*if (parseInt(ftype) == 3 || parseInt(ftype) == 5 || parseInt(ftype) == 11) {
                    if ($(elem).html() && !isExisting) {
                        if ((v.Value == null || (v.Value == '' && callback) || (v.Value !=null && $('select', $(fieldset)).find('option[value="' + encodeURI(v.Value) + '"]').length == 0)) && v.defaultValue == null && field.AutoSelectFirstitem == "true")
                            autoSelect();
                    }
                }

                if (field  && parseInt(ftype) == 5 && field.LookupTable == '$QUERY$' && field.LookupQuery.search(/parent./) > 0 && field.AutoSelectFirstitem == "true" && $('select', $(fieldset)).find('option[value="' + encodeURI(v.Value) + '"]').length == 0) {
                    autoSelect()
                }*/
                
                if (ftype == 3 || ftype == 11) {
				   if ($(elem).html() && !isExisting) {
				       if ((v.Value == null || (v.Value == '' && callback) || (v.Value != null && $('select', $(fieldset)).find('option[value="' + encodeURI(v.Value) + '"]').length == 0)) && v.defaultValue == null && field.AutoSelectFirstitem == "true") autoSelect();
				   }
				}
				else if (ftype == 5 && field.AutoSelectFirstitem == "true") {
				   if (!isExisting) {
                       if ((v.Value == null || (v.Value == '' && callback) || (v.Value && lkdt.findIndex(element => element.Id == v.Value) === -1 && (field.AllowTextInput != 'true' || (field.AllowTextInput == 'true' && allowTxtRefreshCall))) || (field.LookupQuery && field.LookupQuery.search(/parent./) > 0 && lkdt.findIndex(element => element.Id == v.Value) === -1 && (field.AllowTextInput != 'true' || (field.AllowTextInput == 'true' && allowTxtRefreshCall))))) autoSelect(true);
				   }
				}
                
               // if(field){LFields.splice(LFields.indexOf(field.Id),1);
               // recordSwitching.swichingFn();}
                if (callback) callback(callbackdata);
            }
            if (field && field.InputType == "5" && field.LookupTable == '$QUERY$' && !field.LookupTable && field.LookupTable != "" && (field.IsLargeLookup == 'true' || field.IsLargeLookup == true)) {
                var lookupQuery = field.LookupQuery;
                lookupQuery = lookupQuery.replace(/;$/, "");
                getData(lookupQuery + ' LIMIT 0,1', [], function (tld) {
                    if (tld.length > 0) {
                        var querySplit = lookupQuery.split(/where/i);
                        var colName = Object.keys(tld[0]);
                        if (querySplit[1])
                            querySplit[1] = ' WHERE (' + colName[0] + ' = "' + v.Value + '") AND ' + querySplit[1];
                        else
                            querySplit.push(' WHERE ' + colName[0] + ' = "' + v.Value + '"');
                        lookupQuery = querySplit[0] + querySplit[1];
                        evalLargeLookup(lookupQuery, function (result) {
                            v.Value = result[0].Description ? result[0].Description : result[0].Name;
                            v.DisplayValue = v.Value
                            if (v.Original == '???') v.Original = null;
                            if (v.DisplayOriginal == '???') v.DisplayOriginal = null;
                            v.ChangedValue = (v.DisplayOriginal == v.DisplayValue || typeof (v.DisplayValue) != "string") ? v.DisplayValue : (v.DisplayValue ? (v.DisplayValue.substr(0, 10) == "data:image" ? v.DisplayValue : '<span class="EditedValue">' + v.DisplayValue + '</span>') : '');
                            fprocessor();
                        });
                    }
                });
            }
            else fprocessor();

        });
    }

}

//modified
function refreshDataCollectionEstimateChart() {
    //FILLFROMTEMPLATE
    $('#dc-estimate-chart').html($('#estimate-chart-template').html());
    $('#dc-estimate-chart').fillTemplate([activeParcel], false);
    bindEstimateChartSelection();
    $('.sort-screen-select td[class="select"]', $('#dc-estimate-chart')).removeClass('selected-cell');
    $('.sort-screen-select td[value="' + activeParcel.SelectedAppraisalType + '"]', $('#dc-estimate-chart')).addClass('selected-cell');
}

function customFormatValue(val, fieldId) {
    var fvalue, yr, m, d, dt, fmt;
    fmt = (fieldId != null || fieldId != undefined) ? checkForCustomFormat(fieldId) : 'YYYYMMDD';
    // fmt = checkForCustomFormat(fieldId);
    if (val == '' || val == null || val == undefined) return val;
    if (fmt != '') {
        let v = ''; if (typeof (fmt) == 'object') { v = fmt.Value; fmt = fmt.Name; }
        switch (fmt) {
            case 'YYYYMMDD':
                fvalue = val.toString().trim();
                if(fvalue.length == 8){
                	if (val && val != null) {
						yr = fvalue.substring(0, 4);
						m = fvalue.substring(4, 6);
						d = fvalue.substring(6, 8);
						dt = new Date(yr, m - 1, d);
                	}
                }
                else if(fvalue.length >8){
	                if (val && val != null) {
	                    yr = fvalue.substring(0, 4);
	                    m = fvalue.substring(5, 7);
	                    d = fvalue.substring(8, 10);
	                    dt = new Date(yr, m - 1, d);
	                }
	            }
                else{
                	return fvalue;
                }
                var dtime = dt.getFullYear() + '-' + (dt.getMonth() + 1).toString().padLeft(2, '0') + '-' + dt.getDate().toString().padLeft(2, '0');
                return dtime;
            case 'Factor':
                if (v != '' && !isNaN(parseFloat(v))) {
                    let dc = (v.toString().indexOf('.') > -1) ? v.toString().split('.')[1].length : 0;
                    val = parseFloat(val) * parseFloat(v);
                    if (dc && val.toString().indexOf('.') > -1 && (dc != val.toString().split('.')[1].length)) val = val.toFixed(dc);
                }
                return val;
            default:
                return val;
        }
    }
    else
        return val;
}

function checkForCustomFormat(fieldid) {
    var fmt;
    if (datafields[fieldid].UIProperties)
        var o = JSON.parse(datafields[fieldid].UIProperties.replace(/'/g, '"'));
    else
        return '';
    if (o.CustomFormat && o.CustomFormat != '')
        fmt = o.CustomFormat;
    return ((fmt && fmt != '') ? fmt : '');
}

function deFormatvalue(value, fieldid) {
    var fmt;
    if (value == "" || value == null) return value;
    fmt = checkForCustomFormat(fieldid);

    if (fmt != '') {
        let v = ''; if (typeof (fmt) == 'object') { v = fmt.Value; fmt = fmt.Name; }
        switch (fmt) {
            case 'YYYYMMDD':
                val = value.split(" ")[0].split("-");
                return (val[0] + val[1] + val[2]);
            case 'Factor':
                if (v != '' && !isNaN(parseFloat(v))) {
                    value = parseFloat(value) / parseFloat(v);
                }

                if (datafields[fieldid]?.InputType == '8') value = Math.round(value);
                if (value.toString().indexOf('.') > -1 && (value.toString().split('.')[1].length > 4)) value = value.toFixed(4);
                return value.toString();
            default:
                return value;
        }
    }
    else
        return value;
}
function bindEstimateChartSelection() {
        $('.sort-screen-select td[class="select"]').unbind(touchClickEvent);
    $('.sort-screen-select td[class="select"]').bind(touchClickEvent, function (e) {
        e.preventDefault();
        var pi = $(this).parents('.sort-screen-select').first();
        var ci = $(pi).parent();
        var pid = $(pi).children('tr:first-child').attr('pid');

        if ($(this).hasClass('selected-cell')) {
            return;
        } else {
            $('.sort-screen-select td', ci).removeClass('selected-cell');
            $(this).addClass('selected-cell');
            var newValue = $(this).attr('value');
            $('.sort-screen-select td', $('nav[parcelid="' + pid + '"]')).removeClass('selected-cell');
            $('.sort-screen-select td[value="' + newValue + '"]', $('nav[parcelid="' + pid + '"]')).addClass('selected-cell');

            $(pi).attr('selectedtype', newValue);
            ccma.Sync.enqueueEvent(pid, 'SelectedAppraisalType', newValue, { updateData: true });

            if (activeParcel) {
                activeParcel.SelectedAppraisalType = newValue;
            }

            getData("SELECT * FROM Parcel WHERE Id = " + pid + " AND Reviewed = 'true'", [], function (p) {
                if (p.length > 0) {
                    executeQueries("UPDATE Parcel SET Reviewed = 'false' WHERE Id = " + pid);
                }
            });
        }
    });
}
function checkRequiredIfEditsFieldsInForm(auxform) {
    var flag = 0;
    var requiredFields = $('fieldset[requiredIfEdited="true"]', auxform);
    $(auxform).find('label.oldvalue').each(function (index, el) {
        if ($(el.parentElement).attr('ro') == "false" && (el.offsetWidth > 0 || el.offsetHeight > 0))
            flag = 1;
    });
    if (requiredFields && requiredFields.length > 0) {
        requiredFields.forEach(function (f) {
            if (flag == 1) {
                $('.reqd', $(f)).remove();
                var reqd = document.createElement('span');
                $(reqd).addClass('reqd');
                reqd.innerHTML = '*';
                $(f).prepend(reqd);
                $('input, textarea, select, .cusDpdownSpan', $(f)).attr('required', true);
            }
            else {
                var fId = $(f).attr('field-id')
            	if(!(datafields[fId].IsRequired == 'true')){
            		$('.reqd', $(f)).remove();
                	$('input, textarea, select, .cusDpdownSpan', $(f)).removeAttr('required');
            	}
            }
        });
    }
}

function checkRequiredFieldsInForm() {
    var auxForm = $('section[catid="' + ccma.UI.ActiveFormTabId + '"]');
    var auxFormFields = $('fieldset[eval-ro="0"][eval-hi="0"]', auxForm);
    if (ccma.UI.ActiveFormTabId == 0) {
        return true;
    }
    if (auxForm.attr('index') == "-1")
        return true;
    checkRequiredIfEditsFieldsInForm(auxForm);
    var elems = $('[required="true"]', auxFormFields);
    if (elems.length == 0)
        return true;
    var valid = $('[required="true"]', auxFormFields).map(function (y, x) { return ( (x.tagName =='INPUT' && $(x).val() != "" && $(x).val() != 'null' && $(x).val() != null)|| (x.tagName =='TEXTAREA' && $(x).val() != "" && $(x).val() != 'null' && $(x).val() != null)|| (x.tagName =='SELECT' && $(x).val() != "" && $(x).val() != 'null' && $(x).val() != null)||(x.tagName =='RADIOGROUP' && $('input:checked',$(x)).val() != undefined ) || (x.tagName =='SPAN' && x.className && x.className.contains('cusDpdownSpan') && $(x).val() != "" && $(x).val() != 'null' && $(x).val() != null) ) }).reduce(function (x, y) { return x && y });
    return valid;
}


function checkRequiredFieldsInGrid(cId) {
    var gridForm = $('section[catid="' + cId + '"]');
    var gridFormFields = $('fieldset[eval-ro="0"][eval-hi="0"]', gridForm)
    var valid = true;
    gridFormFields.forEach(function (f) {
        var field = datafields[$(f).attr('field-id')]
        var filteredIndexes = cachedAuxOptions[cId].filteredIndexes;
        filteredIndexes.forEach(function (i) {
            var sourceTable = getCategory(cId).SourceTable;
            var fieldName = field.Name;
            if ((field.IsRequired == "true" || field.IsRequiredMAOnly == "true") && (activeParcel[sourceTable] && (activeParcel[sourceTable][i][fieldName] == "" || activeParcel[sourceTable][i][fieldName] == null)) && field.DoNotShow == "false" && field.DoNotShowOnMA == "false")
                valid = false;
        });
    });
    return valid;
}


function bindBPPScreenEvents() {   



var b= $("#selectall").prop('checked');
if($("#selectall").prop('checked')){
	$('.parcelbpp').each(function(){
		var idbpp= $(this).attr('Id')
		document.getElementById(idbpp).checked = true
	})
}
else{
	$('.parcelbpp').each(function(){
		var idbpp= $(this).attr('Id')
		document.getElementById(idbpp).checked = false
	})
}
}

function MACsuccessfullparcel(bppmap, parentrp, mpId) {
	var countPassedParcel = 0;
    ValidatePassedparcel.forEach(function (pid) {
        deleteHIddenRecordsOnMAC(pid);
        ccma.Sync.enqueueEvent(pid.id, 'Reviewed', 'true', { updateData: true }, function () {
            $('.parcel-item[parcelid="' + pid.id + '"]').remove();
            var msg="Nearby NNN ";
            msg+=(listMode==0)? 'Parcels':'Priority Pracels'
            var nearcount=$('#my-parcels .parcel-item').length;
            setSearchResultsTitle(msg, nearcount);
            if ( nearcount == 0 && !bppmap) showSortScreen();
            if(bppmap) dropActiveParcel();
            if ( isBPPUser && listType == 1 )
			countPassedParcel++;
            if ( isBPPUser && listType == 1 ){
                refreshSortScreen = true;
                refreshMiniSS = true;
				if(countPassedParcel == ValidatePassedparcel.length && nearcount > 0)
					$('#my-parcels .parcel-item-new .Bppcount').html(parseInt($('#my-parcels .parcel-item-new .Bppcount').html()) - countPassedParcel);
            }
            
           //BPP prority icon set
            
           if(isBPPUser && activescreen == "gis-map")
            {
               let CC_Priority = "";
               if (listMode == 1) CC_Priority += EnableNewPriorities == 'True' ? " AND CC_Priority IN (1,2,3,4,5) " : " AND CC_Priority IN (1,2) ";
               else CC_Priority += " AND CC_Priority = 0";

               getData("Select id, Reviewed, ParentParcelID, CC_Priority FROM SubjectParcels where (ParentParcelID = " + parentrp + " or id = " + parentrp + ")" + CC_Priority, [], function (bppprio) {
                   var bppicon, hprty, rpPlReviewed = false;
                   let bppParcelExists = bppprio.filter(function (x) { return (x.ParentParcelID == parentrp) });
                   var highestPriority = bppprio.filter(function (x) { return (x.ParentParcelID == parentrp) && x.Reviewed == 'false' }).map(function (y) { return Math.max(y.CC_Priority) });
                   var MapPrio = 0;

                   if (highestPriority.length > 0) {
                       highestPriority.forEach(function (MP) {
                           if (MapPrio < MP) {
                               MapPrio = MP;
                           }
                       });
                       hprty = MapPrio.toString();
                       if (ccma.Session.bppRealDataEditable == 1) {
                           let rpPlParcel = bppprio.filter(function (x) { return (x.Id == parentrp && x.Reviewed == 'false') })[0];
                           if (rpPlParcel && parseInt(hprty) < parseInt(rpPlParcel.CC_Priority)) {
                               hprty = rpPlParcel.CC_Priority.toString();
                           }
                       }
                   }
                   else {
                       if (ccma.Session.bppRealDataEditable == 1) {
                           let rpPlParcel = bppprio.filter(function (x) { return (x.Id == parentrp) })[0];
                           hprty = rpPlParcel.CC_Priority;
                           if (rpPlParcel.Reviewed == 'false') {
                               if (bppParcelExists.length > 0) {
                                   if (ccma.Session.bppRealDataEditable == 1 && ParcelsNotInGroup.includes(parseInt(parentrp))) rpPlReviewed = true;
                                   else if (ccma.Session.bppRealDataReadOnly) rpPlReviewed = true;
                               }
                           }
                           else rpPlReviewed = true;
                       }
                       else rpPlReviewed = true;
                   }

                   if (rpPlReviewed) {
                       bppicon = bppParcelVisited;
                   }
                   else {
                       if (EnableNewPriorities == "True") {
                       if (hprty == 3) {
                           bppicon = bppParcelPriority;
                       }
                       else if (hprty == 4) {
                           bppicon = bppParcelAlert;
                       }
                       else if (hprty == 5) {
                           bppicon = bppCritical;
                       }
                       else if (hprty == 2) {
                           bppicon = bppMedium;
                       }
                       else if (hprty == 1) {
                           bppicon = bppNormal;
                       }
                       else if (hprty == 0) {
                           bppicon = bppParcelVisited;
                           }
                       }
                       else {
                           if (hprty == 1) {
                               bppicon = bppParcelPriority;
                           }
                           else if (hprty == 2) {
                               bppicon = bppParcelAlert;
                           }
                           else if (hprty == 0) {
                               bppicon = bppParcelVisited;
                           }
                       }

                   }

                   if (parcelMapMarkers)
                       if (parcelMapMarkers[pid.id.toString()])
                           parcelMapMarkers[pid.id.toString()].setIcon(bppicon);
                       else {
                           if (parcelMapMarkers[parentrp.toString()]) parcelMapMarkers[parentrp.toString()].setIcon(bppicon);
                           else if (mpId && parcelMapMarkers[mpId.toString()]) parcelMapMarkers[mpId.toString()].setIcon(bppicon);
                       }
               });
            }
            else
            {      
                if (parcelMapMarkers)
                if (parcelMapMarkers[pid.id.toString()])
                    parcelMapMarkers[pid.id.toString()].setIcon(iconParcelVisited);
            }
            if (clientSettings?.GeneratePRCPDF == '1') {
                uploadPrcHtml(pid.id);
            }
        });
    });
    $(".selectedcheckbox").removeClass('selectedcheckbox');
    showMultipleMACButton();
}
function multipleMAC(bppmap) {
    //var pID = $("#my-parcels .selectedcheckbox").map(function () {
      //  return [{ 'id': $(this).parent().parent().attr('parcelid'), 'no': $(this).parent().parent().find('.ssc-parcel-id').text().trim() }]
    //}).get();
    //Code fetching priority
   /* if(( $(".bppmap .parcelbpp").length - $(".bppmap .parcelbpp:checked").length) > 0)
			{
				alert( $(".bppmap .parcelbpp").length - $(".bppmap .parcelbpp:checked").length);
			}*/
	var parentrp = 	$('#selectall').val();
    var numberofchkbx=$(".bppmap .parcelbpp").length;
    var pID;
	if (bppmap){
		pID=$(".bppmap .parcelbpp:checked").map(function(){
			
			return [{ 'id': $(this).attr('value'), 'no': $(this).attr('key') }]
		}).get();
		if($('#selectall').prop('checked') && $('#selectall').prop('mac') != 'true' && ccma.Session.BPPDownloadType == "1" && ccma.Session.RealDataReadOnly == false){
			$('#selectall:checked').map(function(){
				if($('#selectall.rpMAC').length == 1 && ParcelsNotInGroup.indexOf(parseInt($(this).attr('value'))) === -1)
					pID.push({ 'id': $(this).attr('value'), 'no': $(this).attr('key') })
			});
		}
	}
	else{
		   pID = $("#my-parcels .selectedcheckbox").map(function () {
        return [{ 'id': $(this).parent().parent().attr('parcelid'), 'no': $(this).parent().parent().find('.ssc-parcel-id').text().trim() }]
    }).get();
	}
    classCalculatorValidationResult = []
  	if (pID.length == 0) { 
  			if((bppmap && numberofchkbx!=0) && ($('#selectall').attr('mac') == "true"))
    		{
    			messageBox('Already been Marked As Completed.',function(){ $('.mask').hide(); $('.dimmer').hide();})
				return false; 
    		}
            if (bppmap && numberofchkbx != 0 && $('#selectall.parcelbppheader')[0]?.checked != true) {
    			messageBox('No Parcels selected.',function(){ $('.mask').hide(); $('.dimmer').hide();})
				return false; 
            }

            if (bppmap && numberofchkbx != 0 && $('#selectall.parcelbppheader')[0]?.checked == true) {
                let cg = $('#selectall.parcelbppheader').attr('nbhdNo');
                cg = cg && cg != '-1' ? cg : '';
                let msg = "This parcel is in the " + cg + " Assignment Group, which is not downloaded. </br>If you'd like to edit this parcel, download the  " + cg + " Assignment Group.";
                messageBox(msg, function () { $('.mask').hide(); $('.dimmer').hide(); })
                return false;
            }

    		if(bppmap && numberofchkbx==0){
    			messageBox('Already been Marked As Completed.',function(){ $('.mask').hide(); $('.dimmer').hide();})
				return false; 
    		}
    }
    var pCounter = 0;
	if(isBPPUser && combinedView && ccma.Session.RealDataReadOnly){
	    var ppLength = pID.length;
	    var ppIds = $("#my-parcels .selectedcheckbox").map(function () { if($('.mark-as-delete', $(this).parents('nav')).attr('rp') == "true") return [ {'id': $(this).parent().parent().attr('parcelid'),  'no': $(this).parent().parent().find('.ssc-parcel-id').text().trim() }] }).get();
	    ppIds.forEach(function(item){
		    if(pID.findIndex(function(x){return x.id == item.id}) > -1){
		        pID.splice(pID.findIndex(function(x){return x.id == item.id}),1);
		        pCounter++;
		    }
		});
		if(ppLength === pCounter){
			messageBox('You cannot perform this action for the selected parcel.',function(){ $('.mask').hide(); $('.dimmer').hide();});
			return false;
		}
	}
    ValidatePassedparcel = []
    Validatefailedparcel = [];
    HiddenRecords = [];
	successValidation = false;
    var message = 'This selected parcels will be marked as completed, and will be removed from this list. You will not be able to open this selected parcel again unless it is re-assigned to you. If you are sure that you have made sufficient edits, click OK to continue, else click Cancel.'
    var startMessage = '', endMessage = '', startColor = 'Black', endColor = 'Black';
    if (clientSettings.MACStartMessage && clientSettings.MACStartMessage != 'NULL') startMessage = clientSettings.MACStartMessage;
    if (clientSettings.MACEndMessage && clientSettings.MACEndMessage != 'NULL') endMessage = clientSettings.MACEndMessage;
    if (clientSettings.MACStartMessageColor && clientSettings.MACStartMessageColor != 'NULL' && startMessage != '') startColor = clientSettings.MACStartMessageColor;
    if (clientSettings.MACEndMessageColor && clientSettings.MACEndMessageColor != 'NULL' && endMessage != '') endColor = clientSettings.MACEndMessageColor;
    if (startMessage != '') startMessage = '<div style="color:' + startColor + ';">' + startMessage + ' <\/div>';
    if (endMessage != '') endMessage = '<div style="color:' + endColor + ';">' + endMessage + ' <\/div>';
    message = startMessage + message + endMessage;
    $('.mask').show();
    validateParcel(null, function () {
    	if(fylEnabled)
			showFutureData ? showFutureData = false : showFutureData = true;
		else
			successValidation = true;
		validateParcel(null, function () {
	    	$('.mask').hide();
			successValidation = false;
	    	if(fylEnabled){
				showFutureData ? showFutureData = false : showFutureData = true;
				ValidatePassedparcel.forEach(function(item, index){
					if(_.indexOf(Validatefailedparcel,item) > -1)
						ValidatePassedparcel.splice(ValidatePassedparcel.findIndex(function(m){ return m == item }),1);
				});
				validateCyFy = false;
			}
		    if (Validatefailedparcel.length == 0 && pCounter == 0) {
                messageBox(message, ["OK", "Cancel"], function () {
                    let mpId = $('#selectall').attr('mapicon_id');
                    MACsuccessfullparcel(bppmap, parentrp, mpId);
				    if(bppmap){
					    UsingOSM ? $(".leaflet-popup-close-button").click() : infoWindow.close();
				    }
                }, () => { if (bppmap && activeParcel) activeParcel = null; });
		    }
		    else if (ValidatePassedparcel.length > 0) {
                        var validationMsg='';
		                Validatefailedparcel.forEach(function(x){
		                validationMsg+='<a href=\'javascript: void(0)\' onclick=\'$(".window-message-box").hide(); loadParcel = true; return markParcelAsComplete(' + x.id + ')\' >' +x.no+ '<\/a>'+', '
		                   });
		                if(pCounter > 0){
		                	ppIds.forEach(function(x){
		                		validationMsg+='<a href=\'javascript: void(0)\' onclick=\'$(".window-message-box").hide(); loadParcel = true; return markParcelAsComplete(' + x.id + ')\' >' +x.no+ '<\/a>'+', '
		                   });
		                }
                messageBox("One or more Parcels failed on Validations(" + validationMsg.substring(0, (validationMsg.length - 2)) + ")<br/><P Style='text-align:center;'>To resolve, tap the link for each property to be presented with the errors that need correction, or just individually mark the property as complete.</p><br/> Do you want to continue  with Other parcels, click OK to continue, else click Cancel.", ["OK", "Cancel"], function () { MACsuccessfullparcel(bppmap); }, () => { if (bppmap && activeParcel) activeParcel = null; });
		   }
		   else {
		        var validationMsg='Validations failed for the following Property IDs :<br/>';
                Validatefailedparcel.forEach(function(x){
                validationMsg+='<a href=\'javascript: void(0)\' onclick=\'$(".window-message-box").hide(); loadParcel = true; return markParcelAsComplete(' + x.id + ')\' >' +x.no+ '<\/a>'+', '
                });
                if(pCounter > 0){
	                ppIds.forEach(function(x){
	                	    validationMsg+='<a href=\'javascript: void(0)\' onclick=\'$(".window-message-box").hide(); loadParcel = true; return markParcelAsComplete(' + x.id + ')\' >' +x.no+ '<\/a>'+', '
	                });
	            }
                messageBox(validationMsg.substring(0,(validationMsg.length-2))+'<br/><P Style="text-align:center;">To resolve, tap the link for each property to be presented with the errors that need correction, or just individually mark the property as complete.</p>');
		  }
	    }, null, true, pID);
	}, null, true, pID);
}
var ValidatePassedparcel = []
var Validatefailedparcel = []
var currentActiveParcel;
function IsUniqueFieldsInSiblingsValid(category, fields,fieldSourceTable) {
    var sourceTable = getSourceTable(category), flds = "";
    if (sourceTable && activeParcel[sourceTable] && activeParcel[sourceTable].length > 0) {
        var t = activeParcel[sourceTable].filter(function (f) { return f.CC_Deleted != true });
        var kk = t.length;
        var records = [] , rds = [] , nonUniqueCats = [] , r = false,flds = "",rdsExp = ""; 
        for(var j = 0; j < fields.length ; j++){
			if (fields.length == 1){
				flds += "[r." + fields[j] + "]"
			}
			else if (j == 0){
				flds += "[r." + fields[j] + ",";
			}
			else if (j == fields.length-1) {
				flds += "r." + fields[j] + "]";
				}
			else {
				flds += "r." + fields[j] + ",";				
				}
			
			}
        for (var i = 0; i < kk; i++) {
        if (t[i].parentRecord && t[i].parentRecord[fieldSourceTable] && t[i].parentRecord[fieldSourceTable].length > 0){
			records = t[i].parentRecord[fieldSourceTable].filter(function (d) { return d.CC_Deleted != true });
			rds =  records.map(function(r){return eval(flds)})
			var valid = true;
			for(var k = 0; k < rds.length; k++){
				var rds1 = JSON.parse(JSON.stringify(rds));
				rds1.splice(k,1)
				rds1.forEach(function(g,h){
					if (valid == false) return;
 					var s = 0;
					fields.forEach(function (u,n) {
						var value = rds[k][n];
						var curVal = rds1[h][n];
						if ((curVal !== undefined) && (value !== undefined) && curVal == value)
							s = s + 1;
					});
					if (s == fields.length) {
						valid = false;
					}
			});
			}
		if(valid == false) {
				var cat = getCategoryFromSourceTable(fieldSourceTable).Name
				nonUniqueCats.push(cat)
			}	
		  }	
        }
   }
   return nonUniqueCats;
}
function validateParcel(pid, validCallback, invalidCallback, bulkvalidate, arrayPID) {
	if(successValidation) {validCallback(); return;}
	if(isBPPUser && $('.parcel-item[parcelid="' + pid + '"] .mark-as-delete').hasClass('Bppdeleted') == true){ validCallback(); return;}
    pid = pid ? pid : arrayPID[arrayPID.length - 1].id;
    arrayPID ? '' : arrayPID = [];
    var tempActiveParcel;
    var ignoreValidation = false;
    if (!invalidCallback) invalidCallback = function(){ $('.mask').hide(); $('.dimmer').hide(); }
    if (activeParcel) tempActiveParcel = activeParcel;
    getParcelWithAllDataAndImages(pid, function (p, t) {
        var notSatisfied = [];
        var IsSilbing = true, combination = true;
        var tempactp;
        var returnvalue;
        var cats = p.IsPersonalProperty == 'true' ? fieldCategories.filter( function ( f ) { return ( f.Type == '1' || f.Type == '2' ) } ) : fieldCategories.filter( function ( f ) { return ( !f.Type || f.Type == '0' || f.Type == '2' ) } )
        var fieldsToValidate = getFieldsToValidate( p.IsPersonalProperty == 'true', cats )
        // var t = checkMinRecordExist(p);
        var rec = classCalculatorValidation(p);
        var classValidation = function () {
            if (rec.length > 0) {
            	validationSwitch();
                $('.mask').hide();
                messageBox("The current Class is different than the Calculated Class. Press OK to continue, or CANCEL to return and edit the data.", ["OK", "CANCEL"], function () {
                    //classCalculatorValidationResult.forEach(function (recItem) {
                    //    ccma.Sync.enqueueParcelChange(recItem.ParcelId, recItem.ROWUID, (recItem.ParcelId || ''), null, recItem.FieldName, recItem.FieldID, null, recItem.value, { updateData: true, source: recItem.Sourcetable }, null)
                    //})
                    classCalculatorValidationResult = []
                    validCallback();
                }, function () { $('.dimmer').hide(); });
            }
            else
                validCallback();
        }

        if (t.length > 0) {
            var requiredList = 'Below mentioned categories should have minimum number of records : <br/>';
            t.forEach(function (h) {
                var catId = h.rowId ? h.category.ParentCategoryId : h.category.Id;
                requiredList += changeToLinkText(catId, getCategoryName(h.category.Id).trim() + '(' + h.category.MinRecords + ')' + '<br/>', currentActiveParcel.Id, h.rowId);
            });
            if (bulkvalidate) {
            	if(fylEnabled){
            		 if(_.indexOf(Validatefailedparcel,arrayPID[arrayPID.length - 1]) == -1)
            		 	Validatefailedparcel.push(arrayPID[arrayPID.length - 1]);
            	}else
                	Validatefailedparcel.push(arrayPID[arrayPID.length - 1]);
                if (arrayPID.length > 1)
                    validateParcel(null, validCallback, invalidCallback, true, arrayPID.slice(0, -1));
                else
                    classValidation();
            }
            else {
            	validationSwitch();
                $('.mask').hide();
                messageBox(requiredList, invalidCallback);
            }
            return false;
        }
        var fields = fieldsToValidate.filter( function ( x ) { return x.IsUniqueInSiblings == 'true' } );
     
            tempactp = activeParcel;
            activeParcel = currentActiveParcel;
            for (var i in fields) {
                var f = fields[i];
                if (!f.CategoryId) continue;
                var st = f.SourceTable;
                var catid = f.CategoryId;
                if(!(st == "_photo_meta_data" && catid == "-2")){
	                var yu = IsUniqueInSiblingsValid(f.CategoryId, f.Name);
	                if (yu.Status == false) {
	                    if (!notSatisfied.some(function (d) { return d.MenuName == getCategoryName(f.CategoryId); })) {
	                        notSatisfied.push({ 'MenuName': getCategoryName(f.CategoryId).trim(), 'MenuId': f.CategoryId, 'count': 1, 'ROWUID': yu.ROWUID });
	                    }
	                    else {
	                        for (x in notSatisfied) {
	                            if (notSatisfied[x].MenuName == getCategoryName(f.CategoryId).trim()) {
	                                notSatisfied[x].MenuId = f.CategoryId;
	                                notSatisfied[x].count = notSatisfied[x].count + 1;
	                                notSatisfied[x].ROWUID = yu.ROWUID;
	                            }
	                        }
	                    }
	                    IsSilbing = false;
	                }
	            }
                else{
                	var photouniquefield = true;
                	res = eval('currentActiveParcel.Photos');
                	var jj = f.AssignedName.replace('PhotoMetaField', 'MetaData');
					if(f.IsUniqueInSiblings == 'true' ){
					for (var i = 0; i < res.length; i++) {
						var item = res[i];
						photouniquefield = (res.filter(function(x){ return x[jj] == item[jj]})).length == 1
						if(item[jj] === null) photouniquefield = true;
						if(!photouniquefield)
							break;
					}
					if(!photouniquefield){
	                    if (!notSatisfied.some(function (d) { return d.MenuName == 'Photo Properties : '+ f.Label})) {
		                    var item={}; item.fields=[];
		                    item.MenuName = 'Photo Properties : '+ f.Label;
		                    item.fields[0] = f.Label;
		                    item.MenuId = -2;
		                    item.count = 1;
		                    notSatisfied.push(item);
	                    }
		                else {
	                        for (x in notSatisfied) {
	                            if (notSatisfied[x].MenuName == 'Photo Properties : '+f.Label) {
	                                notSatisfied[x].MenuId = -2;
	                                notSatisfied[x].count = notSatisfied[x].count + 1;
	                                if(!notSatisfied[x].fields.some(function(d) {return d == f.Label;}))
	                          			notSatisfied[x].fields[notSatisfied[x].fields.length]= f.Label;
	                            }
	                        }
	                    }
	                    IsSilbing = false;
	
	                }
                }
                	
            }
            }
            var compfields = cats.filter( function ( x ) { return x.UniqueKeys } );
       
                if (!activeParcel) activeParcel = currentActiveParcel;
                for (var i in compfields) {
                    var combinationIsSilbing = false;
                    var f = compfields[i];

                    if (!f.Id) continue;
                    var st = f.SourceTable;
                    var catid = f.Id;
                    var pp = IsUniqueInSiblingsValid(f.Id, f.UniqueKeys);
                    if (pp.Status == true) {
                        combinationIsSilbing = true;
                    }
                    if (combinationIsSilbing == false) {
                        if (!notSatisfied.some(function (d) { return d.MenuName == getCategoryName(f.Id); })) {
                            notSatisfied.push({ 'MenuName': getCategoryName(f.Id).trim(), 'MenuId': catid, 'count': 1, 'ROWUID': pp.ROWUID });
                        }
                        else {
                            for (x in notSatisfied) {
                                if (notSatisfied[x].MenuName == getCategoryName(f.Id).trim()) {
                                    notSatisfied[x].MenuId = catid;
                                    notSatisfied[x].count = notSatisfied[x].count + 1;
                                    notSatisfied[x].ROWUID = pp.ROWUID;
                                }
                            }
                        }
                        combination = false;
                    }
                }
                var requiredList = 'Please remove or revise any record containing duplicated values on the following screen(s): <br/>';
                if (notSatisfied.length) {
                    for (x in notSatisfied) {
                        if (x < 5) {
                            var listItem = notSatisfied[x].MenuName + ', ';
                            var menuId = notSatisfied[x].MenuId;
                            requiredList += changeToLinkText(menuId, listItem, currentActiveParcel.Id, notSatisfied[x].ROWUID);
                        }
                    }
                    requiredList = requiredList.substring(0, requiredList.length - 10);
                    (notSatisfied.length > 5) ? requiredList += ' and others.' : '';
                    //  activeParcel = null;
                    if (bulkvalidate) {
                        if(fylEnabled){
		            		 if(_.indexOf(Validatefailedparcel,arrayPID[arrayPID.length - 1]) == -1)
		            		 	Validatefailedparcel.push(arrayPID[arrayPID.length - 1]);
		            	}else
		                	Validatefailedparcel.push(arrayPID[arrayPID.length - 1]);
                        if (arrayPID.length > 1)
                            validateParcel(null, validCallback, invalidCallback, true, arrayPID.slice(0, -1));
                        else
                            classValidation();
                    }
                    else {
                    	validationSwitch();
                        $('.mask').hide();
                        messageBox(requiredList, invalidCallback);
                    }

                }
                //   activeParcel = null;
                var uniqSibFields = cats.filter( function ( x ) { return x.UniqueFieldsInSiblings } );
              
                	var nonUniques = [],nonUniqueCategories = [],flds = [];
                	if(uniqSibFields.length > 0){
                		var fieldCategory = [];
                		for(u in uniqSibFields){
                			var cat = uniqSibFields[u].Id;
                			var uniqueFields = uniqSibFields[u].UniqueFieldsInSiblings.split(',');
                			var sourceTable = uniqueFields[0].split('.')[0];
                			uniqueFields.forEach(function(u){
                				flds.push(u.split('.')[1]);
                			})	
                			nonUniques = IsUniqueFieldsInSiblingsValid(cat,flds,sourceTable);
                		}
                	}
                	var nonUniqueCats = [];
                	if(nonUniques && nonUniques.length > 0) {
					$.each(nonUniques, function(i, el){
					    if($.inArray(el, nonUniqueCats) === -1) nonUniqueCats.push(el);
					 });
					}
                	if(nonUniqueCats && nonUniqueCats.length > 0){
                		validationSwitch();
                		messageBox("There are non-unique records in '" + nonUniqueCats.join("','") + "'.", invalidCallback);             		
					            return;
                	}
                if (IsSilbing == false || combination == false) {
                    return false;
                }
                else {
                    var uniqSibExps = cats.filter( function ( x ) { return ( x.UniqueSiblingKeys && x.UniqueSiblingExpression ) } );
                
					    if (uniqSibExps.length > 0) {
					        var isUnique = true, notUniqueCats = [];
					        var checkIsUnique = function(parData, tbl, expr, fields) {
					        	if (!parData[tbl]) return true;
					        	var records = parData[tbl].filter(function(rec){ return (eval('rec.' + expr)) }), t = true;
				                if (records.length > 0) {
									fields.forEach(function(f){
										records.forEach(function(r){
				                    		t = (records.filter(function(rec){ return rec[f] == r[f]; }).length == 1);
				                        })
				                    })
				                }
				                return t;
					        }
					        for(u in uniqSibExps) {
					            var tempIsUnique = true;
					            var uniqSib = uniqSibExps[u];
					            var fields = uniqSib.UniqueSiblingKeys.split(',');
					            var uPID = uniqSib.OverrideParentCategory ? uniqSib.OverrideParentCategory : uniqSib.ParentCategoryId
					            if (uPID) {
						            var parentRecords = activeParcel[getSourceTable(uPID)];
									parentRecords.forEach(function(parRec) {
										tempIsUnique = tempIsUnique && checkIsUnique(parRec, uniqSib.SourceTable, uniqSib.UniqueSiblingExpression, fields);
						            })
					            }
					            else tempIsUnique = checkIsUnique(activeParcel, uniqSib.SourceTable, uniqSib.UniqueSiblingExpression, fields);
					            
					            if (!tempIsUnique) notUniqueCats.push(uniqSib.Name);
					            isUnique = isUnique && tempIsUnique;
					        }
					        if (!isUnique) { 
					        	validationSwitch();
					            messageBox("There are non-unique records in '" + notUniqueCats.join("','") + "'.", invalidCallback); 
					            return;
					        }
					    }
                    var conditionalVal = true, failedConditions = [];
                    if (clientSettings.LoadMethodAfterAppLoad && clientSettings.LoadMethodAfterAppLoad !== '') {
                        var conditionalValidations = fieldsToValidate.filter(function (x) { return x.ConditionalValidationConfig != null }), failedRowuids = [];
                        conditionalValidations.forEach(function (f) {
                            var s = f.SourceTable;
                            var d = ccma.Data.Types[parseInt(f.InputType)];
                            if (d.baseType == 'number') {
                                if ((s == null) || (s == '')) {
                                    var source = currentActiveParcel;
                                    var value = currentActiveParcel[f.name];
                                    var validations = getConditionalValidationInputs(f, source);
                                    if (validations.length > 0) {
                                        validations = validations[0];
                                        if (value != '' && !isNaN(value) && ((validations.MIN_VALUE != '' && validations.MIN_VALUE > parseFloat(value)) || (validations.MAX_VALUE != '' && validations.MAX_VALUE < parseFloat(value)))) {
                                            var cat = getCategoryName(f.CategoryId)
                                            if (failedConditions.indexOf(cat) > -1) failedConditions.push(cat)
                                            return;
                                        }
                                    }
                                }
                                else if (s != '_photo_meta_data') {
                                    let source = currentActiveParcel[f.SourceTable], validatedRecs = source, fieldConfig = f.ConditionalValidationConfig.split(',');
                                    source.forEach(function (rec) {
                                        var value = rec[f.Name];
                                        var validations = getConditionalValidationInputs(f, rec);
                                        if (validations.length > 0) {
                                            //validatedRecs.push(rec);
                                            validations = validations[0];
                                            if (value != '' && !isNaN(value) && ((validations.MIN_VALUE != '' && validations.MIN_VALUE > parseFloat(value)) || (validations.MAX_VALUE != '' && validations.MAX_VALUE < parseFloat(value)))) {
                                                var cat = getCategory(f.CategoryId)
                                                if (failedConditions.indexOf(cat.Name) < 0) failedConditions.push(cat.Name);
                                                failedRowuids.push({ catId: cat?.Id, rowuid: rec.ROWUID, parcelId: currentActiveParcel.Id });
                                                return;
                                            }
                                            if ((!value || value == '') && validations.IS_REQUIRED) {
                                                var cat = getCategory(f.CategoryId)
                                                if (failedConditions.indexOf(cat?.Name) < 0) failedConditions.push(cat?.Name);
                                                failedRowuids.push({ catId: cat?.Id, rowuid: rec.ROWUID, parcelId: currentActiveParcel.Id });
                                                return;
                                            }
                                        }
                                    });

                                    if (fieldConfig[2]?.trim() == 'IsV8MaxPercentage' && validatedRecs.length > 0) {
                                        let groupBy = function (arr, ...keys) {
                                            return arr.reduce((acc, obj) => {
                                                if (obj.bid !== null) {
                                                    let key = keys.map(key => obj[key]).join('-');
                                                    let group = acc.find(group => group[0][keys[0]] === obj[keys[0]] && group[0][keys[1]] === obj[keys[1]] && group[0][keys[2]] === obj[keys[2]] && group[0][keys[3]] === obj[keys[3]]);
                                                    if (group) {
                                                        group.push(obj);
                                                    } else {
                                                        acc.push([obj]);
                                                    }
                                                }
                                                return acc;
                                            }, [])
                                                .filter(group => group.reduce((sum, obj) => parseFloat(sum) + parseFloat(obj.MVA_PERCENTAGE), 0) !== 100);
                                        }
                                        validatedRecs = validatedRecs.filter((x) => { return x.MVA_XML_INPUT_TYPE && ['0', '1', '2', '3', '4'].includes(x.MVA_XML_INPUT_TYPE.toString()) });
                                        let grouped = groupBy(validatedRecs, 'PID', 'BID', 'SECT_ID', 'MVA_XML_INPUT_TYPE');
                                        if (grouped?.length > 0) {
                                            let cat = getCategory(f.CategoryId)
                                            if (failedConditions.indexOf(cat?.Name) < 0) failedConditions.push(cat?.Name);
                                            let minRowuid = Infinity;
                                            grouped.forEach((vd) => {
                                                minRowuid = vd.reduce((min, ct) => (ct.ROWUID < min ? ct.ROWUID : min), minRowuid);
                                            });
                                            failedRowuids.push({ catId: cat?.Id, rowuid: minRowuid, parcelId: currentActiveParcel.Id });
                                            return;
                                        }
                                    }
                                }
                            }
                        });

                        if (failedConditions.length > 0) {
                            validationSwitch();
                            if (failedRowuids?.length > 0 && clientSettings?.LoadMethodAfterAppLoad == 'loadValidationTablesForVS8') {
                                let mp = Math.min(...failedRowuids.map(obj => obj.parcelId)), omp = failedRowuids.filter(obj => obj.parcelId == mp),
                                    mc = Math.min(...omp.map(obj => obj.catId)), omc = omp.filter(obj => obj.catId == mc), mr = Math.min(...omc.map(obj => obj.rowuid));
                                messageBox("There is invalid data on " + failedConditions.join("','") + ". Please return to the " + failedConditions.join("','") + " screen and enter the data according to the data entry rules on screen", ["Return", "Cancel"], function () {
                                    if (mp != activeParcel?.Id) loadParcel = true;
                                    showCategoryFromMessage(mc, mp, mr);
                                }, invalidCallback)
                            }
                            else {
                                messageBox("There is invalid data on " + failedConditions.join("','") + ". Please return to the " + failedConditions.join("','") + " screen and enter the data according to the data entry rules on screen", invalidCallback);
                            }
                            
                            return;
                        }
                    }
						var sumFailedCatRec=[];
						var requiredSumFields = Object.keys(datafields).map(function (x) { return datafields[x] }).filter(function (x) { return (x['UI_Settings'] && x['UI_Settings'].RequiredSum && x['UI_Settings'].RequiredSum !=null) });
						if(requiredSumFields.length > 0){
							for (var r in requiredSumFields){
								var catId=requiredSumFields[r].CategoryId
								var sumField = requiredSumFields[r];
								var sourceTable = sumField.SourceTable;
								var requiredSumExp = requiredSumFields[r]['UI_Settings'].RequiredSum;
								var sumValue = requiredSumExp.split('/')[0];
								var fields = requiredSumExp.indexOf('/') > -1 ? requiredSumExp.substring(requiredSumExp.indexOf('/') + 1).split(',') : null;
								var groupedData = {};
								if(sourceTable && activeParcel[sourceTable]){
									var sourceData = activeParcel[sourceTable].filter(function(a){return a.cc_deleted !=true});
									groupedData = sourceData.reduce(function(l, r) {
										var keys = fields ? r[fields[0]] + (fields[1] ? '~'+ r[fields[1]]:'')+ '~' + r.ParentROWUID : r.ParentROWUID;
		
		  								if (typeof l[keys] === "undefined") {
		    								l[keys] = {
		      									sum: 0,
		      									ParentRowuid:r['ParentROWUID'],
												CatId:catId
		    								};
		  								}
		  								l[keys].sum += !r[sumField.Name] ? 0 : parseFloat( r[sumField.Name]);
		  								return l;
									}, {});			
								}
								if(Object.keys(groupedData).length > 0) {
									var menuID = getParentCategories(requiredSumFields[r].CategoryId);
					 				var sumFailed = Object.keys(groupedData).map(function (x) { return groupedData[x] }).filter(function (x) { return x.sum != sumValue });
									if(sumFailed.length > 0){ 
										sumFailedCatRec.push(sumFailed[0])
									}
								}
	
							}
						}	

			if (sumFailedCatRec.length > 0){
				var msg ='';
				var failedCats=[]
				$.each(sumFailedCatRec, function(i, el){
					 if($.inArray(el, failedCats) === -1) failedCats.push(el);
				});
				for(i= 0; i < failedCats.length; i++){
					var sumFailedParent = failedCats[0].ParentRowuid;
		 			if(!getParentCategories(failedCats[i]['CatId']) || !sumFailedParent){ menuID = failedCats[i]['CatId']; }
		 				msg +='\n'+ changeToLinkText(menuID,getCategoryName(failedCats[i]['CatId']), activeParcel.Id, sumFailedParent);
				}
				validationSwitch();
		 		messageBox('There are Required Sum failed records in below given categories:'+msg,invalidCallback);
		 		return;
			}
													
					    var rfields = fieldsToValidate.filter( function ( x ) { return ( x.IsRequired == 'true' || x.IsRequiredMAOnly == 'true' || x.RequiredIfRecordEdited == 'true' || x.RequiredExpression ) && x.ReadOnly == 'false' && x.DoNotShow == 'false' && x.DoNotShowOnMA == 'false' } );
	                  
	                        var allRequired = true
	                        for (var i in rfields) {
	                            var f = rfields[i];
	                            var s = f.SourceTable;
	                            var n = f.Name;
	                            var reqExp = f.RequiredExpression;
	                            var hiddenExp = f.VisibilityExpression;
	                            var res, checkRequired = true, expRes, isHidden = false;
	                            if (!f.CategoryId) continue;
	                            if (s == '_photo_meta_data') {
	                                var an = f.assignedname;
	                                var jj = f.AssignedName.replace('PhotoMetaField', 'MetaData');
	                                res = eval('currentActiveParcel.Photos');
	
	                                if (res.length) {
	
	                                    for (var i = 0; i < res.length; i++) {
	                                        var si = 'Photos' + '[' + i + ']';
	
	                                        var tt = eval('currentActiveParcel.' + si + '.' + jj);
	
	                                        if (((typeof tt == 'string') && (tt == "")) || tt == null || tt === undefined) {
	                                            if (!notSatisfied.some(function (d) { return d.MenuName == 'Photo Properties' })) {
	                                                //notSatisfied.push({ 'MenuName': 'Photo Properties', 'MenuId': -2, 'count': 1 });
	                                            var item={}; item.fields=[];
	                                            item.MenuName = 'Photo Properties';
	                                            item.fields[0] = f.Label;
	                                            item.MenuId = -2;
	                                            item.count = 1;
	                                            notSatisfied.push(item);
	                                            }
	                                            else {
	                                                for (x in notSatisfied) {
	                                                    if (notSatisfied[x].MenuName == 'Photo Properties') {
	                                                        notSatisfied[x].MenuId = -2;
	                                                        notSatisfied[x].count = notSatisfied[x].count + 1;
	                                                        if(!notSatisfied[x].fields.some(function(d) {return d == f.Label;}))
	                                                  			notSatisfied[x].fields[notSatisfied[x].fields.length]= f.Label;
	                                                    }
	                                                }
	                                            }
	                                            allRequired = false;
	
	                                        }
	                                    }
	                                }
	                            }
	                            if ((s == null) || (s == '')) {
	                                checkRequired = reqExp ? currentActiveParcel.EvalValue(null, reqExp) : true;
	                                isHidden = hiddenExp ? currentActiveParcel.EvalValue(null, hiddenExp) : false;
	                                res = eval('currentActiveParcel.' + n);
	                                var requiredIfRecordEdited = false;
	                                var currentRecordChanges = activeParcel.Changes ? activeParcel.Changes.filter(function (x) { return x.AuxRowId == null }) : null;
	                                if ((currentRecordChanges && currentRecordChanges.length > 0) && f.RequiredIfRecordEdited == "true") {
	                                	requiredIfRecordEdited = true;
	                                }
	                                if ((((typeof res == 'string') && (res == "")) || res == null || res == 'null' || res === undefined) && (requiredIfRecordEdited == true || checkRequired == true) && !isHidden) {
	                                      if (!notSatisfied.some(function (d) { return d.MenuName == getCategoryName(f.CategoryId); })) {
	                                       // notSatisfied.push({ 'MenuName': getCategoryName(f.CategoryId).trim(), 'MenuId': f.CategoryId, 'count': 1 });
	                                        var item={}; item.fields=[];
	                                            item.MenuName = getCategoryName(f.CategoryId).trim();
	                                            item.fields[0] = f.Label;
	                                            item.MenuId = f.CategoryId;
	                                            item.count = 1;
	                                            notSatisfied.push(item);
	                                    }
	                                    else {
	                                        for (x in notSatisfied) {
	                                            if (notSatisfied[x].MenuName == getCategoryName(f.CategoryId).trim()) {
	                                                notSatisfied[x].MenuId = f.CategoryId;
	                                                notSatisfied[x].count = notSatisfied[x].count + 1;
	                                                if(!notSatisfied[x].fields.some(function(d) {return d == f.Label;}))
	                                                  notSatisfied[x].fields[notSatisfied[x].fields.length]= f.Label;
	                                            }
	                                        }
	                                    }
	                                    allRequired = false;
	                                }
	                            } else if (s != '_photo_meta_data') {
	                                var a = eval('currentActiveParcel.' + s);
	                                var rowuid;
	                                if (a && a.length) {
	                                    for (var i = 0; i < a.length; i++) {
	                                        var si = s + '[' + i + ']';
	                                        rowuid = currentActiveParcel[s][i].ROWUID;
	                                        var requiredIfRecordEdited = false, required = false;
	                                       // var currentRecordChanges = activeParcel.Changes[s] ? Object.keys(activeParcel.Changes[s]).map(function (x) { return activeParcel.Changes[s][x] }).filter(function (x) { return x.AuxRowId == rowuid && x.Field != "CC_SKETCH" }) : null;
	                                        var currentRecordChanges = activeParcel.Changes ? activeParcel.Changes.filter(function (x) { return x.AuxRowId == rowuid && x.Field != "CC_SKETCH" }) : null;
	                                        if ((currentRecordChanges && currentRecordChanges.length > 0) && f.RequiredIfRecordEdited == "true") {
	                                            requiredIfRecordEdited = true;
	                                        }
	                                        if (f.IsRequired == "true" || f.IsRequiredMAOnly == "true")
	                                            required = true;
	                                        checkRequired = reqExp ? currentActiveParcel.EvalValue(si, reqExp) : false;
	                                        isHidden = hiddenExp ? currentActiveParcel.EvalValue(si, hiddenExp) : false;
	                                        res = eval('currentActiveParcel.' + si + '.' + n);
	                                        if ((((typeof res == 'string') && (res == "")) || res == null || res == 'null' || res === undefined) && (requiredIfRecordEdited == true || required == true || checkRequired == true) && !isHidden) {
	                                            if (!notSatisfied.some(function (d) { return d.MenuName == getCategoryName(f.CategoryId); })) {
	                                                 var item={}; item.fields=[];
	                                                item.MenuName=getCategoryName(f.CategoryId).trim();
	                                                item.fields[0]=f.Label;
	                                                item.MenuId=f.CategoryId;
	                                                item.count=1;
	                                                item.ROWUID= a[i].ROWUID;
	                                                notSatisfied.push(item);
	                                            }
	                                            else {
	                                                for (x in notSatisfied) {
	                                                    if (notSatisfied[x].MenuName == getCategoryName(f.CategoryId).trim()) {
	                                                        notSatisfied[x].MenuId = f.CategoryId;
	                                                        notSatisfied[x].ROWUID = a[i].ROWUID;
	                                                        notSatisfied[x].count = notSatisfied[x].count + 1;
	                                                        if(!notSatisfied[x].fields.some(function(d) {return d == f.Label;}))
	                                                        notSatisfied[x].fields[notSatisfied[x].fields.length]= f.Label;
	                                                    }
	                                                }
	                                            }
	                                            allRequired = false;
	
	                                        }
	                                    }
	                                }
	                            }
	                        }
	                        if (!allRequired) {
	                            var requiredList = 'Please complete all required fields within the below mentioned categories:<br/><br/>';
	                            if (notSatisfied.length) {
	                                for (x in notSatisfied) {
	                                    if (x < 5) {
	                                    var fieldDecription;
	                                    if(notSatisfied[x].fields.length > 3)fieldDecription  = notSatisfied[x].fields.slice(0,3).join()+'.. more';
	                                    else fieldDecription  = notSatisfied[x].fields.join();
	                                        var listItem = notSatisfied[x].MenuName.bold() +' : '+ fieldDecription;
	                                        var menuId = notSatisfied[x].MenuId;
	                                        requiredList += changeToLinkText(menuId, listItem, currentActiveParcel.Id, notSatisfied[x].ROWUID);
	                                    }
	                                }
	                                requiredList = requiredList.substring(0, requiredList.length - 10);
	                                (notSatisfied.length > 5) ? requiredList += ' and others.' : '';
	                            }
	                            if (bulkvalidate) {
	                                if(fylEnabled){
					            		 if(_.indexOf(Validatefailedparcel,arrayPID[arrayPID.length - 1]) == -1)
					            		 	Validatefailedparcel.push(arrayPID[arrayPID.length - 1]);
					            	}else
					                	Validatefailedparcel.push(arrayPID[arrayPID.length - 1]);
	                                if (arrayPID.length > 1)
	                                    validateParcel(null, validCallback, invalidCallback, true, arrayPID.slice(0, -1));
	                                else
	                                    classValidation();
	                            }
	                            else {
	                            	validationSwitch();
	                                $('.mask').hide();
	                                messageBox(requiredList, invalidCallback);
	                            }
	                        } else {
	                            var validated = true;
	                            var errorMessage = "";
	                            var warningMsg = "";
	                            var errorCount = 0;
	                            for (var x in ccma.UI.Validations) {
	                                if (errorCount < 5) {
	                                    var res = true;
	                                    var v = ccma.UI.Validations[x];
	                                    var c = decodeHTML(v.Condition);
	                                    var s = v.SourceTable;
	                                    var rowId = null;
	                                    if ((s == null) || (s == '')) {
	                                        res = currentActiveParcel.EvalValue(c);
	                                        validated = validated && res;
	                                    } else {
	                                        var a = currentActiveParcel[s];
	                                        if (a && a.length)
	                                            for (var i = 0; i < a.length; i++) {
	                                                var ind = a[i]["CC_FIndex"] === undefined ? i : a[i]["CC_FIndex"];
	                                                if ( v.FirstOnly == 'true' && ind != 0 )
	                                                    continue;
                                                    var si = s + '[' + i + ']';
                                                    if (si != "") {
                                                    	if (c.search(childRecCalcPattern) > -1 && !['SUM_','AVG_'].some(function(x){ return c.indexOf(x) > -1 })) {
                                                    		var fld = c.split('==')[0].trim();
                                                    		var validationSplit = c.split('==')[1].trim().split(childRecCalcPattern);
                                                    		var childTable = validationSplit[0].split('.')[0];
                                                    		var funct = validationSplit[1];
                                                    		var field = validationSplit[0].split('.')[1];
                                                    		var evalVal = 0;
                                                    		if (a[i][childTable] && a[i][childTable].length > 0) {
                                                    			a[i][childTable].forEach(function(chRec){
                                                    				evalVal += parseFloat((chRec[field] != null && chRec[field] != undefined && chRec[field] != '')? chRec[field]: 0);
                                                    			});
                                                    			if (funct.toLowerCase() == 'avg') evalVal = evalVal/a[i][childTable].length;
                                                    		}
                                                    		res = res && (evalVal == a[i][fld]);
                                                    	}
                                                    	else {
                                                    		var dvRules = c.match(/[-+*\/]/g) == null ? true : false;
	                                                        res = res && currentActiveParcel.EvalValue(si, c, null, null, dvRules);
	                                                        if (!currentActiveParcel.EvalValue(si, c, null, null, dvRules)) rowId = a[i].ROWUID;
                                                		}
                                                        validated = validated && res;
                                                    }
                                         		}
	                                    }
	
	                                    if (!res) {
		                                    var cc = c.match(/("[^"]+"|[^"\s]+)/g)[0] ; //to get string before 1st space. 
		                                    var ccc = c.match(/\b(\w)/g)[0]; //to get 1st char
		                                    var fieldName = cc.substring(cc.indexOf(ccc),cc.length);
		                                    var field = Object.keys(datafields).map(function (x) { return datafields[x] }).filter(function (x) { return x.Name == fieldName })[0]
	                                        var menuId = (fieldCategories.some(function (d) { return d.SourceTable == s; })) ? fieldCategories.filter(function (d) { return d.SourceTable == s; })[0].Id : null;
	                                        menuId = s ? menuId : field ? field .CategoryId : 1;
	                                        var msg = changeToLinkText(menuId, v.ErrorMessage.listItem(), currentActiveParcel.Id, rowId);
	                                        if (v.IsSoftWarning == 'true')
	                                        	warningMsg += msg;
	                                        else { errorMessage += msg; errorCount++; }
	                                    }
	                                    console.log(c, res ? 'Passed' : 'Failed');
	                                }
	                                else break;
	                            }
	                            errorMessage = errorMessage.substring(0, errorMessage.length - 10);
	                            errorMessage = (errorMessage.length > 0? errorMessage.ul(): '');
	                            warningMsg = warningMsg.substring(0, warningMsg.length - 10);
	                            warningMsg = (warningMsg.length > 0? warningMsg.ul(): '');
	                            var proc = function () {
	                            	if(fylEnabled){
								    	if(_.indexOf(ValidatePassedparcel,arrayPID[arrayPID.length - 1]) == -1)
						    				ValidatePassedparcel.push(arrayPID[arrayPID.length - 1]);
						    		}else
	                            		ValidatePassedparcel.push(arrayPID[arrayPID.length - 1]);
	                                if (arrayPID.length > 1)
	                                    validateParcel(null, validCallback, invalidCallback, true, arrayPID.slice(0, -1));
	                                else classValidation();
	                            }
	                            if (validated)  proc();
	                            else {
	                                if (bulkvalidate) {
	                                    if(fylEnabled){
						            		 if(_.indexOf(Validatefailedparcel,arrayPID[arrayPID.length - 1]) == -1)
						            		 	Validatefailedparcel.push(arrayPID[arrayPID.length - 1]);
						            	}else
						                	Validatefailedparcel.push(arrayPID[arrayPID.length - 1]);
	                                    if (arrayPID.length > 1)
	                                        validateParcel(null, validCallback, invalidCallback, true, arrayPID.slice(0, -1));
	                                    else
	                                        classValidation();
	                                }
	                                else {
	                                	validationSwitch();
	                                    $('.mask').hide();
	                                    if (errorMessage != '') messageBox(errorMessage, invalidCallback);
	                                    else if (warningMsg != '')  messageBox(warningMsg,['Proceed to MAC', 'Cancel'], proc, invalidCallback);
	                                }
	                            }
	                        }

                }
            //  tempActiveParcel ? activeParcel = tempActiveParcel : '';
            setTimeout(function () { if(!redirectToMap && !fylEnabled) { if(tempActiveParcel) activeParcel = tempActiveParcel;} else {redirectToMap = false}}, 1000);



    },null, function(){ 
    	if (bulkvalidate) {
    		if(fylEnabled){
		    	if(_.indexOf(ValidatePassedparcel,arrayPID[arrayPID.length - 1]) == -1)
    				ValidatePassedparcel.push(arrayPID[arrayPID.length - 1]);
    		}else
    			ValidatePassedparcel.push(arrayPID[arrayPID.length - 1]);
			if (arrayPID.length > 1)
	            validateParcel(null, validCallback, invalidCallback, true, arrayPID.slice(0, -1));
	        else
	        	validCallback();
	    }
	    else
			validCallback(); 
});
}
function checkMinRecordExist(currentParcel) {
    var resp = [];
    var catExist = fieldCategories.filter(function (u) { if (u.MinRecords == null) u.MinRecords = 0; if(isBPPUser) { return  u.MinRecords > 0 && u.Hidden != true && ((u.Type == 1 ) || (u.Type == 2)) } else return u.MinRecords > 0 && u.Hidden != true});
    if (catExist) {
        catExist.forEach(function (ex) {
            var st = ex.SourceTable;
            var recordsLen = ex.MinRecords;
            var targetTable = currentParcel[st];
            if (!st) {
            }
            else if (ex.ParentCategoryId == null) {
                if (targetTable.length < recordsLen) resp.push({ category: ex, rowId: null });
            }
            else {
                var parent = parentChild.filter(function (pt) { return pt.ChildTable == st })[0].ParentTable;
                if (parent) {
                    if (parent == 'Parcel') { if (targetTable.length < recordsLen) resp.push({ category: ex, rowId: null }); }
                    else {
                        if (currentParcel[parent].length > 0) {
                            var t = 0;
                            currentParcel[parent].forEach(function (cp, i) {
                                var isExp = false;
                                if ((!cp[st] || (cp[st] && cp[st].length == 0)) && ex.HideExpression && ex.HideExpression != '')
                                    isExp = currentParcel.EvalValue(parent + '[' + i + ']', ex.HideExpression.replace(new RegExp('parent.', 'g'), ''))
                                if (isExp)
                                    return
                                if (cp[st] && cp[st].filter(function (p) { return p.CC_Temp_Deleted == "true" }).length > 0)
                                    return
                                if ((!cp[st] || cp[st].length < recordsLen) && t == 0) {
                                    resp.push({ category: ex, rowId: cp.ROWUID });
                                    t = 1;
                                }
                            })
                        }
                        // else resp.push(ex);
                    }
                }
            }
        });
    }
    return resp;

}
function changeToLinkText(menuId, listItem, parcelId, rowuid) {
    return '<div style = "height: auto;"><a href=\'javascript: void(0)\' onclick=\'return showCategoryFromMessage(' + menuId + ',' + parcelId + ',' + rowuid + ')\' >' + listItem + '<\/a>' + '</div>';
}

function getTopParentCategory(categoryId) {
    var parents = getAllPredecessor(categoryId);
    if (parents) return parents[parents.length - 1];
    else return categoryId;
}

function showCategoryFromMessage(categoryId, parcelId, rowuid) {
    if (loadParcel) {
        showParcel(parcelId, function() {
            transiting = false;
            if (categoryId == -2) showPhotos();
            else {
                firstTime = true;
                showDcTab(categoryId, rowuid);
            };
        }, ccma.UI.ActiveScreenId == "gis-map", true);
    } else {
        if (categoryId == -2) 
			showPhotos();
        else {
            relationcycle = [];
            firstTime = true;
            showDcTab(categoryId, rowuid);
        };
    }
    alertLock = false;
    $('.window-message-box').hide();
    $('.tempScrollable').addClass('scrollable');
    $('.input-blocker').hide();
    $("input").blur();
    if (trackFlag) {
        $('.mask').hide();
    }
}
function uploadPrcHtml(id, callback) {
    try {
    var htmlContent = $('.prc-card').html();
    var base64Content = btoa(unescape(encodeURIComponent(htmlContent)));
    ccma.Sync.enqueueParcelChange(id, '', '', 'uploadprc', 'UploadPRC', 'UploadPRC', '', base64Content, {}, () => {
        if (callback) callback()
    });
} catch (error) {
    console.error('Error:', error);
}
}
function markParcelAsComplete(pid, callback, failedCallback, cancelCallback, fromApp, gsData) {
    $('.mask').show();
    validateCyFy = false;
    successValidation = false;
    var parentrp = $('#selectall').val();
    var message = 'This parcel will be marked as completed, and will be removed from this list. You will not be able to open this parcel again unless it is re-assigned to you. If you are sure that you have made sufficient edits, click OK to continue, else click Cancel.'
    //var triggerStatus;
    //const FyCyValueRetriver = () => {
    //    if (fylEnabled && triggerStatus != $($('.onoffswitch')[1]).is(':checked')) { //fylEnabled, it's fy enabled county case, currenlty firing all county. 
    //        var typeclick = iPad ? 'touchend' : 'click';
    //        $($('.onoffswitch')[1]).trigger(typeclick);
    //    }
    //}
    if (fromApp == 'true' || fromApp == true) {
        message = 'This parcel will be marked as completed. Click OK to continue, else click Cancel.'
    }
    if (ccma.UI.ActiveScreenId == 'sort-screen' && fylEnabled && showFutureData == showFutureDataInSortscreen)
        sameInSsDcc = true;
    classCalculatorValidationResult = []
    var startMessage = '', endMessage = '', startColor = 'Black', endColor = 'Black';
    if (clientSettings.MACStartMessage && clientSettings.MACStartMessage != 'NULL') startMessage = clientSettings.MACStartMessage;
    if (clientSettings.MACEndMessage && clientSettings.MACEndMessage != 'NULL') endMessage = clientSettings.MACEndMessage;
    if (clientSettings.MACStartMessageColor && clientSettings.MACStartMessageColor != 'NULL' && startMessage != '') startColor = clientSettings.MACStartMessageColor;
    if (clientSettings.MACEndMessageColor && clientSettings.MACEndMessageColor != 'NULL' && endMessage != '') endColor = clientSettings.MACEndMessageColor;
    if (startMessage != '') startMessage = '<div style="color:' + startColor + ';">' + startMessage + ' <\/div>';
    if (endMessage != '') endMessage = '<div style="color:' + endColor + ';">' + endMessage + ' <\/div>';
    message = startMessage + message + endMessage;
    HiddenRecords = [];
    let currentMacBtn = null;
    if (event?.target && $(event.target).hasClass('macmap')) currentMacBtn = event.target;
    getData('SELECT Reviewed,IsRPProperty,IsPersonalProperty FROM Parcel WHERE id=?', [pid.toString()], function (x) {
        if (!checkPPAccess('mac', x[0].IsRPProperty)) {
            messageBox('You cannot perform this action for the selected parcel.');
            $('.mask').hide();
            $('.dimmer').hide();
            return;
        }
        var firsttrigger = true;
        if (x[0].Reviewed != 'true')
            validateParcel(pid, function () {
                if (fylEnabled) {
                    showFutureData ? showFutureData = false : showFutureData = true;
                    validateCyFy = true;
                }
                else
                    successValidation = true;
                validateParcel(pid, function () {
                    if (fylEnabled && firsttrigger) {
                        firsttrigger = false;
                        triggerStatus = $($('.onoffswitch')[1]).is(':checked');
                        var typeclick = iPad ? 'touchend' : 'click';
                        showFutureData ? showFutureData = false : showFutureData = true;
                        /*$($('.onoffswitch')[1]).trigger(typeclick);*/
                    }
                    $('.mask').hide();
                    successValidation = false;
                    messageBox(message, ["OK", "Cancel"], function () {
                        deleteHIddenRecordsOnMAC(pid, () => {
                            ccma.Sync.enqueueEvent(pid, 'Reviewed', 'true', { updateData: true }, function () {
                                $('.parcel-item[parcelid="' + pid + '"]').remove();
                                var msg = 'Nearby NNN ';
                                msg += (listMode == 0) ? 'Parcels' : 'Priority Parcels'
                                var nearcount = $('#my-parcels .parcel-item').length;
                                setSearchResultsTitle(msg, nearcount);
                                if (isBPPUser && activescreen == "gis-map" && parentrp) {
                                    getData("Select id, Reviewed, ParentParcelID, CC_Priority FROM SubjectParcels where (ParentParcelID = " + parentrp + " or id = " + parentrp + ")", [], function (bppprio) {
                                        var bppicon, hprty, rpPlReviewed = false;
                                        let bppParcelExists = bppprio.filter(function (x) { return (x.ParentParcelID == parentrp) });
                                        var highestPriority = bppprio.filter(function (x) { return (x.ParentParcelID == parentrp) && x.Reviewed == 'false' }).map(function (y) { return Math.max(y.CC_Priority) });
                                        if (highestPriority.length > 0) {
                                            hprty = highestPriority.max().toString();
                                            if (ccma.Session.bppRealDataEditable == 1) {
                                                let rpPlParcel = bppprio.filter(function (x) { return (x.Id == parentrp && x.Reviewed == 'false') })[0];
                                                if (rpPlParcel && parseInt(hprty) < parseInt(rpPlParcel.CC_Priority)) {
                                                    hprty = rpPlParcel.CC_Priority.toString();
                                                }
                                            }
                                        }
                                        else {
                                            if (ccma.Session.bppRealDataEditable == 1) {
                                                let rpPlParcel = bppprio.filter(function (x) { return (x.Id == parentrp) })[0];
                                                if (rpPlParcel) {
                                                    hprty = rpPlParcel.CC_Priority;
                                                    if (rpPlParcel.Reviewed == 'false') {
                                                        if (bppParcelExists.length > 0) {
                                                            if (ccma.Session.bppRealDataEditable == 1 && ParcelsNotInGroup.includes(parseInt(parentrp))) rpPlReviewed = true;
                                                            else if (ccma.Session.bppRealDataReadOnly) rpPlReviewed = true;
                                                        }
                                                    }
                                                    else rpPlReviewed = true;
                                                }
                                            }
                                            else rpPlReviewed = true;
                                        }

                                        if (rpPlReviewed) {
                                            bppicon = bppParcelVisited;
                                        }
                                        else {
                                            if (EnableNewPriorities == "True") {
                                                if (hprty == 1) bppicon = bppNormal;
                                                else if (hprty == 2) bppicon = bppMedium;
                                                else if (hprty == 0) bppicon = bppParcelVisited;
                                                else if (hprty == 3) bppicon = bppParcelPriority;
                                                else if (hprty == 4) bppicon = bppParcelAlert;
                                                else if (hprty == 5) bppicon = bppCritical;
                                            }
                                            else {
                                                if (hprty == 1) bppicon = bppParcelPriority;
                                                else if (hprty == 2) bppicon = bppParcelAlert;
                                                else if (hprty == 0) bppicon = bppParcelVisited;
                                            }
                                        }
                                        /*
                                        if (highestPriority.length > 0 && bppprio[0].Highestpriority != null) {
                                            if (bppprio[0].Highestpriority == 1) {
                                                bppicon = bppParcelPriority;
                                            }
                                            else if (bppprio[0].Highestpriority == 2) {
                                                bppicon = bppParcelAlert;
                                            }
                                            else if (bppprio[0].Highestpriority == 0) {
                                                bppicon = bppParcelVisited;
                                            }

                                        }
                                        else {
                                            bppicon = bppParcelVisited;
                                        }*/
                                        if (parcelMapMarkers)
                                            if (parcelMapMarkers[pid.toString()])
                                                parcelMapMarkers[pid.toString()].setIcon(bppicon);
                                            else {
                                                if (parcelMapMarkers[parentrp.toString()])
                                                    parcelMapMarkers[parentrp.toString()].setIcon(bppicon);
                                                else {
                                                    let mpId = $('#selectall').attr('mapicon_id');
                                                    if (parcelMapMarkers[mpId.toString()])
                                                        parcelMapMarkers[mpId.toString()].setIcon(bppicon);
                                                }
                                                
                                            }

                                    });
                                }
                                else {
                                    if (parcelMapMarkers)
                                        if (parcelMapMarkers[pid.toString()])
                                            parcelMapMarkers[pid.toString()].setIcon(isBPPUser ? bppParcelVisited : iconParcelVisited);
                                }
                                //$('.macmap').hide(); 
                                if (currentMacBtn) $(currentMacBtn).hide();
                                $('.macmap1').show();                                
                                dropActiveParcel();
                                if (nearcount == 0 && !(isBPPUser && x[0].IsPersonalProperty == 'true')) {
                                    if (isBPPUser && ccma.Session.BPPDownloadType == '1' && !combinedView && !activeParcel)
                                        listType = 0;
                                    /*showSortScreen();*/
                                }
                                if (isBPPUser && x[0].IsRPProperty == 'true') {
                                    refreshSortScreen = true;
                                    refreshMiniSS = true;
                                }
                                if (callback) {
                                    if (isBPPUser && ParcelopenfromMap)
                                        redirectToMap = true;
                                    if (gsData) { callback(gsData); }
                                    else { callback(x[0].IsRPProperty); }
                                }
                                if (isBPPUser && x[0].IsPersonalProperty == 'true') {
                                    refreshSortScreen = true;
                                    refreshMiniSS = true;
                                    if (ccma.UI.ActiveScreenId != "gis-map")
                                        $('#my-parcels .parcel-item-new .Bppcount').html(parseInt($('#my-parcels .parcel-item-new .Bppcount').html()) - 1);
                                }
                                $(".selectedcheckbox").removeClass('selectedcheckbox');
                                showMultipleMACButton();
                                if (clientSettings?.GeneratePRCPDF == '1') {
                                    uploadPrcHtml(pid);
                                }
                            });
                        });
                    }, cancelCallback);
                }, failedCallback);
            }, failedCallback);
        else {
            //var typeclick = iPad ? 'touchend' : 'click';
            //$($('.onoffswitch')[1]).trigger(typeclick);
            messageBox('Already been Marked As Completed', function () { $('.mask').hide(); $('.dimmer').hide(); })
        }
    })
    /*FyCyValueRetriver();*/
}


function insertNewAuxRecordForCategory(tableName, pAuxRecord, newAuxRowId, data, callback, failedCallback) {
    var auxForm = $('section[auxdata="' + tableName + '"]');
    if (auxForm.length == 0) {
        if (failedCallback) failedCallback();
        return;
    }
    var catId = $(auxForm).attr('catid');
    insertNewAuxRecord(auxForm, pAuxRecord, catId, newAuxRowId, data, callback, failedCallback);
}

function insertNewAuxRecord(auxForm, pAuxRecord, fcid, newAuxRowId, otherData, callback, onError, disableParcelLoad, futureParent) {

    var thisF, parentF;
    thisF = getCategory(fcid);
    var parentID = thisF.OverrideParentCategory ? thisF.OverrideParentCategory : thisF.ParentCategoryId;
    parentF = getCategory(parentID);
    if (!parentF && futureParent) parentF = futureParent;
    if (!newAuxRowId)
        newAuxRowId = ccTicks();
    if (fTime == false) {
        ClientRId = newAuxRowId;
        fTime = true;
    }
    var FYvalue = '0';
    if (clientSettings.FutureYearValue) {
        FYvalue = clientSettings.FutureYearValue;
    }
    var newParentAuxRowId;
    var yearType = ''
    var filterFields = thisF.FilterFields;
    var parentFilter = ""; var childFilter = ""
    if (filterFields && filterFields.indexOf('/') > -1) { filterFields.split(',').forEach(function (a) { if (a.indexOf('/') > -1) { parentFilter += a.split('/')[0] + ','; childFilter += a.split('/')[1] + ','; } else { parentFilter += a + ','; childFilter += a + ','; } }); parentFilter = parentFilter.slice(0, -1); childFilter = childFilter.slice(0, -1) }
    else parentFilter = childFilter = filterFields
    if (thisF.YearPartitionField && fylEnabled) {
        yearType = showFutureData ? 'F' : 'A';
        var index = childFilter && childFilter.split(',').indexOf(thisF.YearPartitionField);
        if (index > 0) {
            var pArray = parentFilter.split(',')(index) = (showFutureData ? FYvalue : clientSettings["CurrentYearValue"]);
            parentFilter = pArray.join(',');
        }
        else {
            childFilter += ',' + thisF.YearPartitionField;
            parentFilter += ',' + (showFutureData ? FYvalue : clientSettings["CurrentYearValue"]);
        }
        childFilter += ',CC_YearStatus';
        parentFilter += ",'" + yearType + "'";
        if (!filterFields) {
            childFilter = ',' + thisF.YearPartitionField + ',CC_YearStatus';
            parentFilter = "," + (showFutureData ? FYvalue : clientSettings["CurrentYearValue"]) + ",'" + yearType + "'";
        }
    }


    if (parentF != null) {
        if (filterFields) {
            if (pAuxRecord)
                sqlinsert = "INSERT INTO " + thisF.SourceTable + " (ROWUID,ClientROWUID, ParentROWUID,CC_ParcelId,CC_RecordStatus, " + childFilter + ") SELECT " + newAuxRowId + ", " + newAuxRowId + ", " + pAuxRecord.ROWUID + ", " + activeParcel.Id + ", 'I', " + parentFilter + " FROM " + parentF.SourceTable + " WHERE ROWUID = " + pAuxRecord.ROWUID;
            else {
                if (!parentF)
                    sqlinsert = "INSERT INTO " + thisF.SourceTable + " (ROWUID,ClientROWUID, ParentROWUID,CC_ParcelId,CC_RecordStatus, " + childFilter + ") SELECT " + newAuxRowId + ", " + newAuxRowId + ", " + pAuxRecord.ROWUID + ", " + activeParcel.Id + ", 'I'," + parentFilter + " FROM Parcel WHERE Id = " + activeParcel.Id;
                else {
                    console.log('Cannot insert new record, parent spec is not specified.', parentF)
                    messageBox('Cannot insert new record, parent spec is not specified.');
                    if (onError) onError();
                    return;
                }
            }
        }
    } else {
        var sql;
        if (filterFields) {
            sqlinsert = "INSERT INTO " + thisF.SourceTable + " (ROWUID,ClientROWUID, CC_ParcelId,CC_RecordStatus, " + childFilter + ") SELECT " + newAuxRowId + ", " + newAuxRowId + ", " + activeParcel.Id + ", 'I'," + parentFilter + " FROM Parcel WHERE Id = " + activeParcel.Id;
        } else {
            sqlinsert = "INSERT INTO " + thisF.SourceTable + " (ROWUID,ClientROWUID, CC_ParcelId,CC_RecordStatus" + (childFilter || '') + ") VALUES (" + newAuxRowId + ", " + newAuxRowId + ", " + activeParcel.Id + ",'I'" + (parentFilter || '') + ");";
        }

    }


    if (sqlinsert != "") {
        getData(sqlinsert, [], function (res) {
            var pRID;
            if (pAuxRecord)
                pRID = pAuxRecord.ROWUID;

            ccma.Sync.enqueueParcelChange(activeParcel.Id, newAuxRowId, (pRID || ''), 'new', null, 'NEW', (pRID || ''), thisF.SourceTable + '$' + yearType, null, function () {
                var defaults = Object.keys(datafields).filter(function (k) { return datafields[k].SourceTable && (datafields[k].SourceTable == thisF.SourceTableName && datafields[k].DefaultValue != null) }).map(function (k) { var df = datafields[k]; return { Id: df.Id, Name: df.Name, SourceTable: df.SourceTable, DefaultValue: df.DefaultValue } });
                var data = [];
                if (otherData) {
                    for (f in otherData) {
                        var fv = otherData[f];
                        var df = Object.keys(datafields).filter(function (k) { return datafields[k].SourceTable && (datafields[k].SourceTable == thisF.SourceTableName && datafields[k].Name == f) }).map(function (k) { var df = datafields[k]; return { Id: df.Id, Name: df.Name, SourceTable: df.SourceTable, DefaultValue: df.DefaultValue } });
                        if (df.length > 0) {
                            df[0].DefaultValue = fv;
                            df[0].otherDataVal = true;
                            defaults.push(df[0]);
                        }
                    }
                }
                var pFields = [];
                if (thisF.IncludeParentFieldsOnInsert) {
                    var parentSource = pAuxRecord ? pAuxRecord : activeParcel;
                    thisF.IncludeParentFieldsOnInsert.split(',').forEach(function (f) {
                        f = f.trim();
                        pFields.push(getDataField(f, thisF.SourceTable))
                    })
                }
                insertDefaultFieldValues(activeParcel.Id, newAuxRowId, (pRID || ''), defaults, pAuxRecord, function () {
                    //updateDefaultPCIs(updateDefaultPCI, function(){
                    insertParentFieldValuesPCI(activeParcel.Id, newAuxRowId, (pRID || ''), pFields, parentSource, function () {
                        var autoInsert = thisF.AutoInsertProperties && JSON.parse('[' + thisF.AutoInsertProperties.replace(/}{/g, '},{') + ']');
                        if (autoInsert)
                            var autoInsertItems = JSON.parse('[' + thisF.AutoInsertProperties.replace(/}{/g, '},{') + ']');
                        var proc = function (Items) {
                            if (autoInsert != null) {
                                var newRecord = activeParcel[thisF.SourceTable].splice(_.indexOf(activeParcel[thisF.SourceTable], activeParcel[thisF.SourceTable].filter(function (x) { return x.ROWUID == newAuxRowId })[0]), 1)[0];
                                activeParcel[thisF.SourceTable].push(newRecord);
                                let ix = _.indexOf(activeParcel[thisF.SourceTable], activeParcel[thisF.SourceTable].filter(function (x) { return x.ROWUID == newAuxRowId })[0]);
                                var insertItem = autoInsertItems.pop();
                                if (insertItem && insertItem.table != null) {
                                    var copyfields = insertItem.fieldsToCopy.split(',');
                                    var insertCat = getCategoryFromSourceTable(insertItem.table)
                                    var insertCatId = insertCat.Id;
                                    if (insertItem.AutoInsertPropertyCondition && ix > -1) {
                                        var isExp = activeParcel.EvalValue(thisF.SourceTable + '[' + ix + ']', decodeHTML(insertItem.AutoInsertPropertyCondition), null, '');
                                        if (!(isExp == true)) {
                                            if (autoInsertItems.length > 0) proc(autoInsertItems);
                                            else if (callback) callback(newAuxRowId, newParentAuxRowId);
                                            return;
                                        }
                                    }
                                    var parentSourceData = insertItem.relation.toLowerCase() == 'child' ? newRecord : (insertItem.relation.toLowerCase() == 'parcel' ? null : newRecord.parentRecord);// 2 relation -- 1,child  2.silbing
                                    var newRecordData = {};
                                    if (insertCat.EnableTemplateInsert == 'true') {
                                        insertMultipleRecords(_.clone(tableData[insertCat.SourceTable]), function () {
                                            if (autoInsertItems.length > 0) proc(autoInsertItems);
                                            else if (callback) callback(newAuxRowId, newParentAuxRowId);
                                        }, copyfields, {}, parentSourceData, insertCatId);
                                    }
                                    else {
                                        copyfields.forEach(function (f) {
                                            if (f && f.trim() != '' && newRecord[f]) newRecordData[f] = newRecord[f];
                                        });
                                        insertNewAuxRecord(null, parentSourceData, insertCatId, null, newRecordData, function () {
                                            if (autoInsertItems.length > 0) proc(autoInsertItems);
                                            else if (callback) callback(newAuxRowId, newParentAuxRowId);
                                        })
                                    }
                                }
                            }
                            else if (callback) callback(newAuxRowId, newParentAuxRowId);
                        }
                        if (disableParcelLoad && autoInsert == null) proc();
                        else getParcel(activeParcel.Id, function (p) { proc(autoInsertItems) });
                    });
                    //});
                });

            });
        });
    }
}

function insertMultipleRecords(insertTableData,callback,copyfields,newRecordData,parentSourceData,insertCatId){
	if(insertTableData.length == 0){if(callback) callback(); return;}
	var eachInsertData = insertTableData.splice(0,1);
	copyfields.forEach(function (f) {
		if (f && f.trim() != '') newRecordData[f] = eachInsertData[0][f];
	 });
	 insertNewAuxRecord( null, parentSourceData, insertCatId, null, newRecordData, function () {
		insertMultipleRecords(insertTableData,callback,copyfields,{},parentSourceData,insertCatId);
	});
}

function insertParentFieldValuesPCI(pid, nrid, prid, pFields, pAuxRecord, callback) {
    var _exec = function (f, callback) {
        var value = pAuxRecord[f.Name]
        ccma.Sync.enqueueParcelChange(pid, nrid, (prid || ''), null, f.Name, f.Id, null, value, { updateData: true, source: f.SourceTable }, function () {
            if (callback) callback();
        })
    }
    var _recExec = function (list, callback) {
        if (list.length == 0) {
            if (callback) callback();
        } else {
           	var first = list.shift();
            _exec(first, function () {
                _recExec(list, callback);
            });
        }
    }
    var list = pFields.join ? pFields : [pFields];
    _recExec(list, callback);
}
function insertDefaultFieldValues(pid, nrid, prid, fields, pAuxRecord, callback) {
    var pciChangesBatch = [], updateDefaultPCI = [];
    var _exec = function(f, callback) {
		var specialDefaults = false;
        var defaultValue = f.DefaultValue;
        var t = f.DefaultValue.toString().split('.')
        var l = t.length;
        t = t[l - 1]
        if (f.DefaultValue.toString().contains('parent.parent.') && pAuxRecord) {
			specialDefaults = true;
            if (pAuxRecord.parentRecord)
                defaultValue = pAuxRecord.parentRecord[t];
        } else if (f.DefaultValue.toString().contains('parent.') && pAuxRecord) {
			specialDefaults = true;
            defaultValue = pAuxRecord[t];
        } else if ((f.DefaultValue.toString().contains('parent.') && !pAuxRecord) || f.DefaultValue.toString().contains('parcel.')) {
			specialDefaults = true;
            defaultValue = activeParcel[t];
            if (!defaultValue || defaultValue === undefined) defaultValue = null;
        }
		if( defaultValue && defaultFields.includes( defaultValue.toString().toUpperCase() ) )
			specialDefaults = true;
        defaultValue = getDefaultValue(f, defaultValue);
        if (defaultValue && defaultValue.toString().contains('parent.'))
            defaultValue = ""
        if( f.otherDataVal )
        	specialDefaults = true;
        if (!specialDefaults){
			var keyfield1 = tableKeys.filter(function (k){ return (k.Name == f.Name && k.SourceTable == f.SourceTable)});
			if (keyfield1[0])
				specialDefaults = true;
		}	
            
        var fieldRec = datafields[f.Id];
        if(specialDefaults){
			pciChangesBatch.push({
				parcelId: pid,
				auxRowId: nrid,
				parentAuxRowId: (prid || ''),
				action: 'E',
				field: fieldRec,
				oldValue: null,
				newValue: defaultValue
			});
		} else {
			updateDefaultPCI.push({
				parcelId : pid,
				auxRowId : nrid,
				field : f.Name,
				sourceTable : f.SourceTable,
				newValue : defaultValue
			});
		}
        if (callback) callback();
    }

    var _recExec = function(list, callback) {
        if (list.length == 0) {
        	updateDefaultPCIs(updateDefaultPCI, function() {
	            ccma.Sync.enqueueBulkParcelChanges(pciChangesBatch, function() {
	                if (callback) callback();
	            });
            });
        } else {
            var first = list.shift();
            _exec(first, function() {
                _recExec(list, callback);
            });
        }
    }

    var list = fields.join ? fields : [fields];
    _recExec(list, callback);
}

function saveFocusedElement(focusedElement, callback) {
    try {
      
        try {
		        if(focusedElement.length <= 0){
		              let el = document.activeElement;
		              var elType = $(el).attr('type');

			          if(elType == 'number' || elType == 'text' || elType == 'textarea'){
						 focusedElement = $(el);
			    	  }
		         }
		}
	    catch (e) {
	        console.warn(e.message);
	    }
        
        var newValue = $(focusedElement).val(), oldValue = $('.oldvalue', $(focusedElement).parent()).attr('oldvalue');
        
        if (focusedElement.length > 0 && (newValue != oldValue)) {
            saveInputValue(focusedElement[0], function () {
                callback();
            });
        }
        else {
            if (callback) callback();
        }
    }
    catch (e) {
        console.warn(e.message);
    }
}

function getDefaultValue(field,defaultValue){
	if(defaultValue == undefined||defaultValue == null)
   		return null;
    switch (defaultValue.toString().toUpperCase()) {
    	case "CURRENT_DATE":
     	case "CURRENTDATE":
	        var dt = (new Date()).format("ymd");
	        defaultValue = checkForCustomFormat(field.Id) == '' ? dt : deFormatvalue(dt, field.Id);
	        break;
        case "CURRENT_USER":
        case "CURRENTUSER":
            defaultValue = localStorage.getItem('username');
            break;
        case "CURRENT_YEAR":
     	case "CURRENTYEAR":
            defaultValue = ccma.CurrentYear;
            break;
        case "CURRENT_YEAR":
        case "CURRENTYEAR":
             case "ACTIVE_YEAR":
        case "ACTIVEYEAR":
            defaultValue = clientSettings.CurrentYearValue
            break;
        case "CURRENT_DAY":
        case "CURRENTDAY":
            defaultValue = ccma.CurrentDay;
            break;
        case "CURRENT_MONTH":
        case "CURRENTMONTH":
            defaultValue = ccma.CurrentMonth;
            break;
        case "FUTUREYEARSTATUS":
        case "FUTUREYEARSTATUS":
            defaultValue = ccma.FutureYearStatus;
            break;
        case "BLANK":
            defaultValue = ' ';
            break;
    }
    return defaultValue;
}

function getTableKey(sourceTable) {
    try {
        if (sourceTable) {
            var FieldRequired = [];
            for (x in tableKeys) {
                (tableKeys[x].SourceTable == sourceTable) ? FieldRequired.push(tableKeys[x].Name) : '';
            }
            return FieldRequired;
        }
        else {
            return null;
        }
    }
    catch (e) {
        console.warn(e.message);

    }
}

function hideOnCopyWindowLoad() {
    //    $('tr[parcelid],tr[streetaddress],tr[pscombined],.btn-parcel-copy,tr[fields]', '.parcel-copy-params').hide();
    $('tr[streetaddress],tr[pscombined],.btn-parcel-copy,tr[fields]', '.parcel-copy-params').hide();
    $('#parcelIds_copy').val('');
    $('#streetAddress_copy').val('');
}




function ToggleParcelID(source) {

    try {
        ($(source).val() == 'op') ? loadOtherParcelSettings() : (($(source).val() == 'tp') ? loadTemplateSettings() : loadSameParcelSetting());
    }
    catch (e) {
        console.warn(e.message);
    }
}
function loadOtherParcelSettings() {
    hideOnCopyWindowLoad();
    $('tr[parcelid],tr[streetaddress]', '.parcel-copy-params').show();
}
function loadTemplateSettings() {
    //load detail fro template settings
    hideOnCopyWindowLoad();
    $('#parcelIds_copy').val(''); $('#streetAddress_copy').val('');
    $('tr[fields]', '.parcel-copy-params').show();
}

function searchCriteriaClick(source) {
    $('tr[parcelid] input,tr[streetaddress] input').val('');
    $('tr[parcelid],tr[streetaddress],tr[pscombined],.btn-parcel-copy,tr[fields]', '.parcel-copy-params').hide();
    ($(source).val() == 'pid') ? $('tr[parcelid]', '.parcel-copy-params').show() : $('tr[streetaddress]', '.parcel-copy-params').show();
}

function searchPSCombined(source) {
    var sql = "";
    var key;
    ShowAlternateField == "True" ? key = AlternateKey : key = 'KeyValue1';
    $('tr[pscombined]').hide();
    $('.btn-parcel-copy').hide();
    var parcel = $('#parcelIds_copy').val(), streetAddress = $('#streetAddress_copy').val();
    $('tr[pscombined] td:first-child').html('Parcel ID, Street Address:');
    var scrhfilter = key +' like \'' + parcel + '%\'' 
    if(ShowAlternateField == "True" && ShowKeyValue1 == "True"){
    	scrhfilter =  AlternateKey +' like \'' + parcel + '%\' OR KeyValue1  like \'' + parcel + '%\'' 
        AlternateKey ? key = AlternateKey + ',KeyValue1' : key = 'KeyValue1';
    }
    if (parcel != '' && streetAddress != '') {

        sql = 'select '+ key +',StreetAddress,Id from parcel where '+ scrhfilter +' and StreetAddress like "%' + streetAddress + '%"  and Id !=' + activeParcel.Id;

    }
    else if (parcel != '') {
        sql = 'select '+ key +',StreetAddress,Id from parcel where '+ scrhfilter +' and Id !=' + activeParcel.Id;
    }
    else if (streetAddress != '') {
        sql = 'select '+ key +',StreetAddress,Id from parcel where  StreetAddress like "%' + streetAddress + '%"  and Id !=' + activeParcel.Id;
    }
    else if (streetAddress == '' && parcel == '') {
        $('tr[pscombined]').hide();
    }
    if (sql != '') {
        getData(sql, [], function (data) {
            if (data.length > 0) {
                sql = "";
                /*$('#psCombined').html('<option value="${Id}">${'+ key +'},&nbsp;${StreetAddress}</option>');
                $('#psCombined').fillTemplate(data);*/
                let ls = [];
                data.forEach((x) => {
                    ls.push({ Id: x.Id, Name: (x[key] + ', ' + x.StreetAddress) });
                })

                $('#psCombined').lookup({ popupView: true, searchActive: true, width: 196, searchActiveAbove: 8, searchAutoFocus: false, onSave: () => { } }, []);
                $('#psCombined')[0].getLookup.setData(ls);
                $('#psCombined').val(ls[0].Id);
                $('tr[pscombined]', '.parcel-copy-params').show();
                $('.btn-parcel-copy').show();

            }
            else { 
                $('.btn-parcel-copy').hide();
            }
        });
    }

}

function loadSameParcelSetting() {
    $('.btn-parcel-copy').show();
} 

function startCopying() {
	showMask('Copying...');
    isMultipleCategories = false;
    IgnoreConditions = appState.ignoreCondition+"";
    copyConditions = appState.copyCondition+"";
    catIdList = appState.catId+"";	
	if(catIdList.includes(',')) {
		catIdList = catIdList.split(',');
		IgnoreConditions = IgnoreConditions.split(',');
		copyConditions = copyConditions.split(',');
		isMultipleCategories = true;
	}
	copiedCategories = [];
	emptyCopyCategories = [];
	var returncpyTrackedValue = trackedValue();
    if(!returncpyTrackedValue) return false;
    var parcelID = $('#psCombined').val(), copySketch = null;

    if (clientSettings?.CopyImprvSketchData) {
        let csd = clientSettings.CopyImprvSketchData.split("."), tb = csd[0], fd = csd[1],
            ct = fieldCategories.filter((x) => { return x.SourceTable == tb })[0], fld = getDataField(fd);

        if (!ct && fld && !fld.SourceTable) copySketch = { TableName: tb, Field: fld };
    }

    if (!isMultipleCategories) {
	    var cat = getCategory( copyCatid )
        ccma.Data.Controller.CopyFormData(parcelID, activeParcel.Id, [cat], null, { type: 'Imprv', copySketch: copySketch });
    } else {
	    var copyFormData = function(list, callback) {
            copyCatid = list[0];
            copyingIgnoreCondition = IgnoreConditions[catIdList.length - list.length];
            copyingCopyCondition = copyConditions[catIdList.length - list.length];
            list = list.slice(1);
            var cat = getCategory(copyCatid);    
            ccma.Data.Controller.CopyFormData(parcelID, activeParcel.Id, [cat], null, { type: 'Imprv', copySketch: copySketch }, null, function () {
                copySketch = null;
	            if(list.length != 0) {
		    		copyFormData(list, callback);
		    	}
	    	});
	    }
	    copyFormData(catIdList, function() {});
    }
}

//function futureYearpopup(){
  
//    var showFutureData = JSON.parse( localStorage.getItem( 'showFutureData' ) );
//    showFutureData ? $( '#myonoffswitch22' ).attr( 'checked', '' ) : $( '#myonoffswitch22' ).removeAttr( 'checked' );
//    if ( $( '.future-popup' ).hasClass( 'hidden' ) ){
//        // checkMapItems(); $( '.future-popup' ).removeClass( 'hidden' );
//        $( '.future-popup' ).removeClass( 'hidden' );
//    }
//    else {
//        HideMapPopup();
//    }
//}
//function SubmitFutureYear(){
//    var showFutureData = $( '#myonoffswitch22' ).is( ":checked" );
//    localStorage.setItem( 'showFutureData', showFutureData )
//}
function copyAllData(){
    var categories = [];
    copyingIgnoreCondition = ''
    if (!checkPPAccess('recordFutureCopy')){
        messageBox("This option is not available for the selected parcel.");
        return;
    }
    var FYCategories = fieldCategories.filter( function ( item ) { return ( item.YearPartitionField && item.YearPartitionField != '' && item.DisableCopyRecord != 'true') } )
    if ( activeParcel.HasFutureYear == 'true' ){
        messageBox( "Future Year already exists!" );
        return;
    }
    if ( FYCategories.length == 0 ){
        messageBox( "No categories are configured to copy Future Year records" );
        return;
    }
    FYCategories.forEach( function ( item ){
        var occ = categories.filter( function ( p ) { return p.SourceTable == item.SourceTable } ).length
        if ( item.ParentCategoryId == null && item.OverrideParentCategory == null && item.SourceTable && occ == 0 )
            categories.push( item )
    } );
    categories.push( { Id: 0, SourceTable: 'parcel' } )
    messageBox( "Copy all property data to the Future Year?", ['Yes', 'Cancel'], function () {
        ccma.Data.Controller.CopyFormData( activeParcel.Id, activeParcel.Id, categories, null, { type: 'futureYear', mode: 2 } );
    } );
}

function showCopyImprv(catid, ignoreCondition, copyCondition) {
    if (!checkPPAccess('copyImprv')){
        messageBox("This option is not available for the selected parcel.");
        return;
    }
    $('#copyType').lookup({ popupView: true, searchActive: true, width: 196, searchActiveAbove: 8, searchAutoFocus: false, onSave: () => { } }, []);
    $('#copyType')[0].getLookup.setData([{ Id: 'op', Name: 'Other Parcel' }]);

    $('#searchCriteria').lookup({ popupView: true, searchActive: true, width: 196, searchActiveAbove: 8, searchAutoFocus: false, onSave: () => { } }, []);
    $('#searchCriteria')[0].getLookup.setData([{ Id: 'tp', Name: 'Street Address' }, { Id: 'pid', Name: 'Parcel ID' }]);
    $('#copyType').val('op')
    isMultipleCategories = false; 
    copyCatid = '';
    if(Array.isArray(catid)) {
        catIdList = [];
        IgnoreConditions = [];
        copyConditions = [];
        isMultipleCategories = true;
        for(let i=0; i< catid.length; i++) {
            catIdList.push(catid[i].catid);
            if(catid[i].ignoreCondition)
            	IgnoreConditions.push(catid[i].ignoreCondition);
            else 
            	IgnoreConditions.push('');
			if(catid[i].copyCondition)
            	copyConditions.push(catid[i].copyCondition);
            else 
            	copyConditions.push('');
        }
    	copyingIgnoreCondition = catid[0].ignoreCondition;
    	copyingCopyCondition = catid[0].copyCondition;
	    if (catid[0].catid) {
	        appState.catId = catIdList;
	        appState.ignoreCondition = IgnoreConditions;
	        appState.copyCondition = copyConditions;
	        saveAppState();
	        loadScreen('copyParcel', function () {
	            copyCatid = catid[0].catid;
	            hideOnCopyWindowLoad();
	            if (clientSettings.CopyDefaultSearchCriteria && clientSettings.CopyDefaultSearchCriteria == 'Parcel ID')
	                $('#searchCriteria').val('pid')
	            else
	                $('#searchCriteria').val('tp')
	            searchCriteriaClick($('#searchCriteria')[0]);
	        });
	    } else {
	        return;
	    }
    } else {
    	copyingIgnoreCondition = ignoreCondition;
	    if (catid) {
	        appState.catId = catid;
	        appState.ignoreCondition = ignoreCondition;
			appState.copyCondition = copyCondition;
	        saveAppState();
	        loadScreen('copyParcel', function () {
	            copyCatid = catid;
	            hideOnCopyWindowLoad();
	            if (clientSettings.CopyDefaultSearchCriteria && clientSettings.CopyDefaultSearchCriteria == 'Parcel ID')
	                $('#searchCriteria').val('pid')
	            else
	                $('#searchCriteria').val('tp')
	            searchCriteriaClick($('#searchCriteria')[0]);
	        });
	    } else {
	        return;
	    }
    }
}

function checkIsNull(num) {
    var type = typeof num, returnType = true;
    switch (type) {
        case 'string': returnType = (num == '') ? false : true; break;
        case 'number': (num == 0) ? returnType = true : ''; break;
        case 'object': returnType = (num) ? true : false; break;
        case 'undefined': returnType = (num) ? true : false; break;
        case 'boolean': returnType = (num) ? true : false; break;
    }
    return returnType;
}

function getChildCategories(categoryId) {
    var childs = [], retVal = [];
    childs = fieldCategories.filter(function (d) { return d.ParentCategoryId == categoryId; });
    if (childs) {
        childs.forEach(function (k) { retVal.push(k.Id); });
    }
    return retVal;
}
function getParentCategories(categoryId) {
   return (fieldCategories.some(function (d) { return d.Id == categoryId; })) ? fieldCategories.filter(function (d) { return d.Id == categoryId; })[0].ParentCategoryId ? fieldCategories.filter(function (d) { return d.Id == categoryId; })[0].ParentCategoryId :
           fieldCategories.filter(function (d) { return d.Id == categoryId; })[0].OverrideParentCategory && (!shwcatfrmmsg) ?  fieldCategories.filter(function (d) { return d.Id == categoryId; })[0].OverrideParentCategory : null
       : null;
}
function getSourceTable(categoryId) {
    return (fieldCategories.some(function (d) { return d.Id == categoryId; })) ? fieldCategories.filter(function (d) { return d.Id == categoryId; })[0].SourceTable : null;
}
function getCategory(categoryId) {
    return fieldCategories.filter(function (d) { return d.Id == categoryId; })[0];
}
function getCategoryName(categoryId) {
    return (fieldCategories.some(function (d) { return d.Id == categoryId; })) ? fieldCategories.filter(function (d) { return d.Id == categoryId; })[0].Name : null;
}
//function getCategoryFromSourceTable(sourceTable) {
  //  return (fieldCategories.some(function (d) { return d.SourceTable == sourceTable; })) ? fieldCategories.filter(function (d) { return d.SourceTable == sourceTable; })[0] : {};
//}

function getCategoryFromSourceTable(sourceTable,Id) {
    return (fieldCategories.some(function (d) { return d.SourceTable == sourceTable; })) ? Id ? fieldCategories.filter(function (d) { return d.SourceTable == sourceTable && d.Id == Id ; })[0] : fieldCategories.filter(function (d) { return d.SourceTable == sourceTable; })[0] : {};
}

function getAllPredecessor(categoryId, sp, index) {
    // console.log('----------getAllPredecessor');
    var preD = [], parent = getParentCategories(categoryId), preCatid = categoryId, preIndex = index;
    while (parent) {
        if (sp && checkIsNull(index)) {
            var parentKeys = getTableKey(getSourceTable(parent));
            //            var parentRowuid = activeParcel[getSourceTable(preCatid)][preIndex][ParentROWUID];
            var parentRowuid = activeParcel[getSourceTable(preCatid)][preIndex] ? activeParcel[getSourceTable(preCatid)][preIndex].ParentROWUID : null;
            var filterCondition = '';
            if (parentKeys) {
                parentKeys.forEach(function (d) {
                    if (sp[getSourceTable(preCatid)][preIndex][d])
                        filterCondition += '&&k.' + d + '==' + '"' + sp[getSourceTable(preCatid)][preIndex][d] + '"';
                });
            }
            (parentKeys.length > 1) ? filterCondition = filterCondition.substring(2, filterCondition.length) : '';
            if (sp[getSourceTable(parent)]) {
                sp[getSourceTable(parent)].forEach(function (k) {
                    if ((!parentRowuid && eval(filterCondition)) || (parentRowuid && (k.ROWUID == parentRowuid))) {
                        preIndex = sp[getSourceTable(parent)].indexOf(k);
                    }
                });
            }
            preD.push({ Id: parent, Index: preIndex });
        }
        else {
            preD.push(parent);
        }
        preCatid = parent;
        parent = getParentCategories(parent);
    }
    return preD.length > 0 ? preD : null;
}

function isExistingAuxDetail(sourceParcel, sourceTable, index) {
    //console.log('----------isExistingAuxDetail ' + sourceTable +'   '+index);
    var pKeys = [], flag = false;
    if (sourceTable) {
        var keySet = tableKeys.filter(function (d) { return d.SourceTable == sourceTable; });
        (isEmpty(keySet)) ? '' : keySet.forEach(function (x) { pKeys.push(x.Name); });
    }
    else {
        flag = false;
    }
    if (sourceParcel && sourceParcel && checkIsNull(index)) {
        var curRecord = activeParcel[sourceTable][index];
        if (curRecord && !isEmpty([curRecord])) {
            flag = (curRecord.ROWUID >= 0);
            if (pKeys) {
                pKeys.forEach(function (k) {
                    var value = eval('curRecord.' + k);
                    flag = checkIsNull(value) && flag;
                });
            }
        }
        else {
            flag = false;
        }
    }
    return flag;
}

function canChangeExistAuxDetails(sourceParcel, categoryId, index) {
    //console.log('----------canChangeExistAuxDetails');
    if (!isEditOlderCategoryDenied(categoryId))
        return true;
    var canChange = true, pList = getAllPredecessor(categoryId, sourceParcel, index);
    canChange = canChange && isExistingAuxDetail(sourceParcel, getSourceTable(categoryId), index);
    if (pList) {
        pList.forEach(function (d) {
            canChange = isExistingAuxDetail(sourceParcel, getSourceTable(d.Id), d.Index) && canChange;
        });
    }
    return !canChange;
}
function isEditOlderCategoryDenied(categoryId) {
    var retVal = false, parent = getAllPredecessor(categoryId), temp;
    retVal = checkIsFalse((fieldCategories.some(function (d) { return d.Id == categoryId; })) ? fieldCategories.filter(function (d) { return d.Id == categoryId; })[0].DoNotAllowEditExistingRecords : null);
    if (parent) {
        parent.forEach(function (si) {
            var parentDoNotAllowEdit = checkIsFalse((fieldCategories.find(function (d) { return d.Id == si; }) || {}).DoNotAllowEditExistingRecords);
            if (parentDoNotAllowEdit) {
                return;
            }
            temp = checkIsFalse((fieldCategories.find(function (d) { return d.Id == si; }) || {}).DoNotAllowEditExistingRecords);
            retVal = temp || retVal;
        });
    }
    return checkIsFalse(retVal) ? true : false;
}

function getValues(sp, cat, index) {
    if (getTableKey(getSourceTable(cat))) {
        getTableKey(getSourceTable(cat)).forEach(function (s) {
            console.log(s + ' :' + sp[getSourceTable(cat)][index][s]);
        });
    }
}

function getSiblings(category, index) {
    //if(activeParcel[getSourceTable(category)][index] && !activeParcel[getSourceTable(category)][index].parentRecord)
    //return [];
    var predTotal = getAllPredecessor(category, activeParcel, index);
    var parentIndex = (predTotal) ? predTotal[0].Index : null;
    var parentCategory = (predTotal) ? predTotal[0].Id : null;
    var source = [];
    if (predTotal) {
        var FilterFields = fieldCategories.filter(function (d) { return d.ParentCategoryId == parentCategory && d.Id == category; })[0].FilterFields;
        var parentSpec = { index: parentIndex, source: getSourceTable(parentCategory) };
        var FilterValues = getFilterValues(FilterFields, parentSpec.source, parentIndex, activeParcel);
        source = getFilteredIndexesTemp(eval('activeParcel.' + getSourceTable(category)), FilterFields, FilterValues, parentSpec, activeParcel);
    }
    else {
        // if there is no parent parcel is the parent
        var st = getSourceTable(category);
        for (var ind in activeParcel[st]) {
            source.push(ind);
        }
    }
    return source;
}

function getParentSpec(categoryId, index, sp) {
    var parentSpec;
    var predecessor = getAllPredecessor(categoryId, (sp) ? sp : activeParcel, index);
    parentSpec = (predecessor) ? { source: getSourceTable(predecessor[0].Id), index: predecessor[0].Index } : null;
    return parentSpec;
}

function getFormIndexFromRowuid(categoryID, rowuid, sp) {
    if (!sp) {
        sp = activeParcel;
    }
    var indexSet = sp[getSourceTable(categoryID)].map(function (d, i) { if (d.ROWUID == rowuid) { return i; } }).filter(isFinite);
    return indexSet.length > 0 ? indexSet[0] : null;
}

function IsUniqueInSiblingsValid(category, fields) {
    var sourceTable = getSourceTable(category), ret = { Status: true, ROWUID: null };
    if (sourceTable) {
        var t = activeParcel[sourceTable].filter(function (f) { return f.CC_Deleted != true });
        var kk = t.length;
        for (var i = 0; i < kk; i++) {
            var siblings = getSiblings(category, i);
            siblings.forEach(function (d) {
                if (ret.Status == false) return;
                ty = (fields).split(','); var s = 0;
                ty.forEach(function (u) {
                    u = u.trim();
                    var value = t[i][u];
                    if (d != i) {
                        var curVal = t[d][u];
                        if ((curVal !== undefined) && (value !== undefined) && curVal == value)
                            s = s + 1;
                    }

                });
                if (s == ty.length) {
                    ret.Status = false;
                    ret.ROWUID = t[d].ROWUID;
                }
            });
        }
    }
    return ret
}
function IsUniqueInSiblings(category, index, field, value) {
    var sourceTable = getSourceTable(category), ret = true;
    if (sourceTable) {
        var siblings = getSiblings(category, index);
        siblings.forEach(function (d) {
            if (d != index) {
                var curVal = activeParcel[sourceTable][d][field];
                if (curVal == value) {
                    ret = false;
                }
            }
        });
    }
    return ret;
}

function getFilterValues(filterFilter, sourceTable, index, sParcel) {
    //    try {
    if (!filterFilter) {
        return null;
    }
    var filterFieldJson = filterFilter.join ? filterFilter : [(filterFilter.indexOf(',') >= 0) ? filterFilter.split(',') : { 0: filterFilter }], filterValues;
    for (x in filterFieldJson[0]) {
        var parentFilter = filterFieldJson[0][x]
        if (parentFilter.indexOf('/') > -1) { parentFilter = parentFilter.split('/')[0] }
        filterValues = (filterValues ? filterValues + ',' : '') + (eval('sParcel.' + sourceTable + '[' + index + ']' + '.' + parentFilter));
    }
    return filterValues;
    //    }
    //    catch (e) {
    //        console.warn(e.message);
    //    }
}

function getFilteredIndexesTemp(source, filter_name, filterValue, parentSpec, sParcel) {
    //    try {
    var filterfJson = [], filterVJason = [];
    filterfJson = filter_name.join ? filter_name : [(filter_name.indexOf(',') >= 0) ? filter_name.split(',') : [filter_name]];
    filterVJson = filterValue.join ? filterValue : [(filterValue.indexOf(',') >= 0) ? filterValue.split(',') : [filterValue]];
    var filterCondition = '';
    var iter_value = 0;
    var d = source;

    while (iter_value < filterfJson[0].length) {
        var childFilter = filterfJson[0][iter_value]
        if (childFilter.indexOf('/') > -1) { childFilter = childFilter.split('/')[1]; }
        filterCondition += 'd[x].' + childFilter + ' == ' + filterVJson[0][iter_value];
        iter_value += 1;
        if (iter_value < filterfJson[0].length) {
            filterCondition += '&&';
        }
    }

    // var pRowuid = (parentSpec) ? eval('activeParcel.' + parentSpec.source + '[' + parentSpec.index + '].ROWUID') : null;
    var pRowuid = (parentSpec) ? eval('sParcel.' + parentSpec.source + '[' + parentSpec.index + '].ROWUID') : null;
    var newArray = [];
    for (x in d) {
        if ((!d[x].ParentROWUID && eval(filterCondition)) || (d[x].ParentROWUID && (d[x].ParentROWUID == pRowuid))) {
            newArray.push(x);
        }
    }
    return newArray;
    //    }
    //    catch (e) {
    //        console.warn(e.message);
    //    }
}
function HideCategoryByExpression(catID, auxForm, sourceTable, findex, parentSource, zeroRecord, isZeroRecord) {
    var cat = getCategory(catID);
    if (!cat) return
    if(zeroRecord) {
    	if (cat.HideExpression && cat.HideExpression != '' && cat.ParentCategoryId == null && cat.HideExpression.search(/parcel/gi) == -1 ) {
			var isExp = activeParcel.EvalValue(sourceTable + '[' + findex + ']', cat.HideExpression, null, parentSource);
            if (isExp) { $('#dctabkey_' + cat.Id).hide(); }
            else { $('#dctabkey_' + cat.Id).show(); }      
        } 
        return;
    } 
    else {
        if (cat.HideExpression && cat.HideExpression != '' && cat.ParentCategoryId == null) {
            let isExp = false;
            try {
                isExp = activeParcel.EvalValue(sourceTable + '[' + findex + ']', cat.HideExpression, null, parentSource, false, isZeroRecord);
            }
            catch (ex) { }

            if (isExp) { $('#dctabkey_' + cat.Id).hide(); }
            else { $('#dctabkey_' + cat.Id).show(); }     
        }
        if (isZeroRecord) return;
    }

    var childRecord, parentRowuid, childcategories = fieldCategories.filter(function (f) { return (f.ParentCategoryId == catID && f.HideExpression && f.HideExpression != '') });

    childcategories.forEach(function (c) {
        childRecord = [];
        var isExp = activeParcel.EvalValue(sourceTable + '[' + findex + ']', c.HideExpression.replace(new RegExp('parent.', 'g'), ''))
        if (isExp) {
            $('.sub-cat-opener[catid="' + c.Id + '"]', auxForm).hide();
            if (!c.DoNotDeleteOnHide || c.DoNotDeleteOnHide == 'false') {
            	parentRowuid = activeParcel[sourceTable][findex].ROWUID;
            	childRecord = activeParcel[getSourceTable(c.Id)].filter(function (f) { return f.ParentROWUID == parentRowuid });
            	childRecord.forEach(function (record) {
                	if (record.CC_Deleted != "true")
                    	ccma.Data.Controller.DeleteAuxRecordWithDescendants(c.Id, record.ROWUID, undefined, undefined, undefined, undefined, undefined, true);
                	record.CC_Deleted = "true";
            	});
            }
            $('.sub-cat-opener[catid="' + c.Id + '"] span').html('0');
        }
        else{
            if (c.DoNotDeleteOnHide && c.DoNotDeleteOnHide != 'false') {
	        	parentRowuid = activeParcel[sourceTable][findex].ROWUID;
	        	var reclength = (parentRowuid && getSourceTable(c.Id) == sourceTable ? 1: (activeParcel[getSourceTable(c.Id)].filter(function (f) { return f.ParentROWUID == parentRowuid })? activeParcel[getSourceTable(c.Id)].filter(function (f) { return f.ParentROWUID == parentRowuid }).length: 0)); //parent and child same sourceTable then only one record exists in child.
	        	$('.sub-cat-opener[catid="' + c.Id + '"] span').html(reclength.toString());
	        }
        	$('.sub-cat-opener[catid="' + c.Id + '"]', auxForm).show();
        } 
    });
}

function deleteHIddenRecordsOnMAC(pid, callback) {
    var HiddenRecs = HiddenRecords.filter(function (f) { return f.pid == pid });
    tempHiddenRecs = HiddenRecs;
    deleteHiddenRecs(tempHiddenRecs);
    function deleteHiddenRecs(tempHiddenRecs) {
        if(tempHiddenRecs.length > 0) {
            ccma.Data.Controller.DeleteAuxRecordWithDescendants(tempHiddenRecs[0].catid, tempHiddenRecs[0].rowuid, null, null, function() {
                if(tempHiddenRecs.length == 1) {
                    if(callback) callback();
                    return;
                }
                tempHiddenRecs.shift();
                deleteHiddenRecs(tempHiddenRecs);
            });
        }
        else {
        	if(callback) callback();
            return;
       }
    }
}

function bindClickToPRC(subElement, sourceTable, sourceData, index) {
    if (sourceTable != undefined) {
        var obj = { Index: null, rowuid: null, pRowuid: null, pIndex: null };
        var categoryId = getCategoryFromSourceTable(sourceTable).Id;
        var attr = '', pCatId = null, gpCatId = null, gpIndex = null, gpRowuid = null, pIndex = null, parcelIndex = null, recordIndex = null;
        attr = 'onclick = "showDcTab(' + categoryId + ',' + sourceData[index].ROWUID + ')"';
        var pos = subElement.search('context');
        if (pos > -1) {
            var tempElem = subElement.substring(0, pos);
            subElement = tempElem + attr + subElement.substring(pos, subElement.length);
        }
        catArray.push(obj);
    }
    subElement = subElement.replace(/showDataCollection\(/g, 'showDcTab(');
    return subElement;
}

var firstTime, subCatIndex;
function showDcTab(catId, rowuid) {
    if (event) {
        event.stopPropagation();
    }
    	
    var returnDcTrackedValue = trackedValue();
    if (!returnDcTrackedValue) {
        return false;
    }
    
    //showMask('Loading...');
    var isUpdatePRC = {isPrcCall: true, notRefreshAll: false };
    if (!showDCCClicked) showDCCClicked = true;
    else if (!sketchSaveClicked && showDCCClicked && !fySwitchInPrc) isUpdatePRC = {isPrcCall: true, notRefreshAll: true };
    sketchSaveClicked = false; prcClick = true; lookupDataCounter = 0; fySwitchInPrc = false;
    
    var cat = getCategory(catId);
    if (isBPPUser && parcelOpenMode == 'RP') {
        if (!((!cat.Type || cat.Type == 0 || cat.Type == 2)))
            return;
    } else if (isBPPUser && activeParcel.IsPersonalProperty == 'true' && (!cat.Type || cat.Type == 0))
        return;
    if (cat == undefined || !cat) return;
    if (firstTime) {
        var allIndex = getAllPredecessorWithIndex(catId, rowuid);
        var child = [];
        if (allIndex == false && catId) {
            if (!rowuid) shwcatfrmmsg = true;
            catId = getTopParentCategory(catId);
            shwcatfrmmsg = false;
            showDataCollection(catId, null, null, isUpdatePRC);
        } else if (allIndex == false && !catId) return false;
        else {
            for (i = 1; i < allIndex.length; i++) {
                child.push({
                    CategoryId: allIndex[i].CategoryId,
                    Index: allIndex[i].Index,
                    parentSpec: allIndex[i].parentSpec
                });
            }
            showDataCollection(allIndex[0].CategoryId, allIndex[0].Index, child, isUpdatePRC);
        }
        //if (prcClick && LFields.length == 0) { $('#maskLayer').hide(); prcClick = false; }
        //else { setTimeout(function() { if (prcClick) { $('#maskLayer').hide(); prcClick = false; } }, 3000); } 	
        $('.mask').hide();
    }
    //else { $('#maskLayer').hide(); prcClick = false; }
    firstTime = false;
}

function loadChild(child, isPrc) {
    var click = iPad ? 'touchend' : 'click';
    var chldLen = child.length
    for (c in child) {
        subCatIndex = child[c].Index;
        $('.sub-cat-opener[catid="' + child[c].CategoryId + '"]').attr('parentSource', child[c].parentSpec.source);
        $('.sub-cat-opener[catid="' + child[c].CategoryId + '"]').attr('parentIndex', child[c].parentSpec.index);
        $('.sub-cat-opener[catid="' + child[c].CategoryId + '"]').trigger(click);
        subCatIndex = null;
        $('.sub-cat-opener[catid="' + child[c].CategoryId + '"]').removeAttr('parentSource');
        $('.sub-cat-opener[catid="' + child[c].CategoryId + '"]').removeAttr('parentIndex');
        if($('.sub-cat-opener[catid="' + child[c].CategoryId + '"]').length == 0 && chldLen == 1){
            if(fieldCategories.some(function(x){return (x.Id == child[c].CategoryId && x.OverrideParentCategory && !x.ParentCategoryId)})){
                showDataCollection(child[c].CategoryId, child[c].Index, null, isPrc, true);
            }
        }
    }
}

function sortNull(sortExp, sourceData, sourceTable, set) {

    var SortItems = sortExp.trim().split(',');
    SortItems.forEach(function (Item) {
        var sortField = Item.trim().split(" ")[0];
        var scriptNullOrder = 99999999
        if (Item.replace(sortField, '').trim().toLowerCase() == "desc")
            scriptNullOrder = -99999999
        for (x in sourceData) {
            if (set == 1) {
                sourceData[x][sortField] = sourceData[x][sortField] ? sourceData[x][sortField] : scriptNullOrder;
                activeParcel.Original[sourceTable][x][sortField] = activeParcel.Original[sourceTable][x][sortField] ? activeParcel.Original[sourceTable][x][sortField] : scriptNullOrder;
            } else {
                sourceData[x][sortField] = sourceData[x][sortField] == scriptNullOrder ? null : sourceData[x][sortField];
                activeParcel.Original[sourceTable][x][sortField] = activeParcel.Original[sourceTable][x][sortField] == scriptNullOrder ? null : activeParcel.Original[sourceTable][x][sortField];
            }
        }
    });
}

function getAllPredecessorWithIndex(categoryId, rowuid) {
    if (rowuid) {
        var sourceTable = getSourceTable(categoryId);
        var result = [];
        var sourceData = activeParcel[sourceTable] ? activeParcel[sourceTable].filter(function (s) { return s.ROWUID == rowuid })[0] : null;
        var allPredecessor = getAllPredecessor(categoryId);
        var index;
        if (allPredecessor) {
            var recordId = [];
            var tempData;
            var findex = [];
            var parentPath = { source: null, index: null };
            allPredecessor.forEach(function (ap) {
                if (sourceData.parentRecord)
                    sourceData = sourceData.parentRecord;
                else if (getSourceTable(ap) == sourceTable) {

                }
                else return;
                recordId.push(parseInt(sourceData.ROWUID));
            });
            recordId.unshift(rowuid);
            allPredecessor.unshift(categoryId);
            for (i = recordId.length - 1; i >= 0; i--) {
                sourceTable = getSourceTable(allPredecessor[i]);
                tempData = activeParcel[sourceTable];
                for (x in tempData) {
                    if (tempData[x].ParentROWUID == null && tempData[x].ROWUID == recordId[i]) {
                        if (getSourceTable(allPredecessor[i + 1]) == sourceTable) {
                            parentPath = { source: sourceTable, index: result[result.length - 1].Index };
                            result.push({ CategoryId: allPredecessor[i], Index: 0, fIndex: 0, parentSpec: parentPath });
                        }
                        else
                            result.push({ CategoryId: allPredecessor[i], Index: parseInt(x), fIndex: parseInt(x) });
                    }
                }
                if (tempData[0].ParentROWUID && result.length > 0) {
                    parentPath = { source: getSourceTable(allPredecessor[i + 1]), index: result[result.length - 1].fIndex };
                    findex = getFilteredIndexes(tempData, 'null', 'null', parentPath);
                }
                for (x in findex)
                    if (tempData[parseInt(findex[x])].ROWUID == recordId[i])
                        result.push({ CategoryId: allPredecessor[i], Index: parseInt(x), fIndex: findex[x], parentSpec: parentPath });
            }
        }
        else {
        
            var sourceData = activeParcel[sourceTable];
            index = getFilteredIndexes(sourceData, 'ROWUID', rowuid.toString(), null)[0];
            result.push({ CategoryId: categoryId, Index: index });
        }
        return result;
    }
    else return false;
}

function HideInsertDeleteByExpression( catID, auxForm, sourceTable, findex, parentSource, records, index, isZeroRecord ) {  
    if ( records > 0 ) {
        $( '.record-move[ddir="del"]', auxForm ).removeAttr( 'disabled' );
        $( '.header-info', auxForm ).show();
        if ( $( auxForm ).attr( 'ddfr' ) == "true"  &&  index > 0 )       
                $( '.record-move[ddir="del"]', auxForm ).show();
    }
    var cat = getCategory(catID);
    
    if (!cat) return

    if ( cat.EnableDeleteExpression && cat.EnableDeleteExpression != '' ) {
    	cat.EnableDeleteExpression = cat.EnableDeleteExpression.replace('&gt;','>');
        cat.EnableDeleteExpression = cat.EnableDeleteExpression.replace('&lt;', '<');

        let isExp = false;
        try {
            isExp = activeParcel.EvalValue(sourceTable + '[' + findex + ']', cat.EnableDeleteExpression, null, parentSource, false, isZeroRecord);
        }
        catch (ex) { }
         
        if (isExp == true) { $('.record-delete', auxForm).removeAttr('disabled') }
        else { $('.record-delete', auxForm).attr('disabled', 'disabled'); }          
    }
    else $('.record-delete', auxForm).removeAttr('disabled')

    if (cat.DoNotAllowInsertDeleteExpression && cat.DoNotAllowInsertDeleteExpression != '') {
        let isExp = false;
        try {
            isExp = activeParcel.EvalValue(sourceTable + '[' + findex + ']', decodeHTML(cat.DoNotAllowInsertDeleteExpression), null, parentSource, false, isZeroRecord);
        }
        catch (ex) { }

        if (isExp == true) {
            $('.record-new', auxForm).hide();
            $('.record-delete', auxForm).hide();
        }
        else {
            $('.record-new', auxForm).show();
            if (!isZeroRecord) $('.record-delete', auxForm).show();
        }
    }
   
    if (cat.DoNotAllowDeleteFirstRecord == "true" && $(auxForm).attr('index') == 0) $('.record-delete', auxForm).hide();

    if (cat.AllowDeleteOnly == "true") {
    	if (!isZeroRecord) $( '.record-delete', auxForm ).show();
    	$('.record-new', auxForm).hide();
    }

    if (cat.DisableCopyRecord !='true') {
        if (cat.DisableCopyRecordExpression && cat.DisableCopyRecordExpression != '') {
            let isExp = false;
            try {
                isExp = activeParcel.EvalValue(sourceTable + '[' + findex + ']', cat.DisableCopyRecordExpression, null, parentSource, false, isZeroRecord);
            }
            catch (ex) { }

	    	if(isExp) $( '.record-futurecopy', auxForm ).hide();
	    }
    }
    else {
    	$( '.record-futurecopy', auxForm ).hide();
    }
}

function hideCalculator(cat, index, isNotPreviousRecord, auxForm) {
    if (cat.ClassCalculatorParameters && cat.ClassCalculatorParameters != '') {
        var calculatorCondition = cat.ClassCalculatorParameters.split(',').length == 3 ? cat.ClassCalculatorParameters.split(',')[2] : null;
        var ShowCalculator;
        if (calculatorCondition)
            ShowCalculator = activeParcel.EvalValue(index, cat.ClassCalculatorParameters.split(',')[2])
        if ((ShowCalculator || !calculatorCondition) && isNotPreviousRecord)
            $('.record-classcalculator', auxForm).show();
        else
            $('.record-classcalculator', auxForm).hide();
    }
    else
        $('.record-classcalculator', auxForm).hide();
}

function sortSourceData(parcel) {
    var sortExp, sourceTable, sourcedataoriginal;
    activeParcel = parcel;
    for ( var x in fieldCategories ){
    	var sortScript = "", sourceData;
        sortExp = fieldCategories[x].SortExpression;
        if ( sortExp ) {
            sourceTable = fieldCategories[x].SourceTable;
                   
            if(sourceTable){
                sourceData = parcel[sourceTable]
                sourcedataoriginal = parcel.Original[sourceTable];
                sortNull(sortExp, sourceData, sourceTable, 1);
                //sortScript = Sortscript( sortExp, sourceTable );
                if((sortExp) && (sortExp.contains("CASE")) || (sortExp.contains("case"))){
					sortScript = SortscriptCASE( sortExp, sourceTable );
				}else{
					sortScript = Sortscript( sortExp, sourceTable );
				}

            }
            if ( sortScript != "" && sourceData ){
                sourceData = eval( 'sourceData' + sortScript )
                sourcedataoriginal = eval( 'sourcedataoriginal' + sortScript );
                sortNull(sortExp, sourceData, sourceTable, 0);
                parcel[sourceTable] = sourceData;
                parcel.Original[sourceTable] = sourcedataoriginal;
            }
        }
    }

    return parcel;
}
function classCalculatorValidation(parcel) {
    var calCategories = fieldCategories.filter(function (ab) { return ab.ClassCalculatorParameters != null && ab.ClassCalculatorParameters != '' });
    calCategories.forEach(function (cat) {
        var element = cat.ClassCalculatorParameters.includes(',') ? cat.ClassCalculatorParameters.split(',')[1] : '';
        var table = element.split('.')[0]
        var field = element.split('.')[1];
        var valField = getDataField(field, table)
        var calcFieldName = cat.ClassCalculatorParameters.split(',')[0]
        var resultField = getDataField(calcFieldName, cat.SourceTable)
        parcel[cat.SourceTable].forEach(function (pt) {
            if (!pt[table]) return
            if (!pt[calcFieldName] || pt[calcFieldName] == '') return;
            pt[table].forEach(function (ct) {
                if (ct[field] === undefined || !pt[resultField.Name]) return;
                if (ct[field] != pt[resultField.Name]) {
                    classCalculatorValidationResult.push({ ParcelId: parcel.Id, Category: cat.Name, ROWUID: ct.ROWUID, FieldName: valField.Name, FieldID: valField.Id, value: pt[resultField.Name], Sourcetable: table });
                }
            })
        })
    })
    return classCalculatorValidationResult;
}
function dropActiveParcel() {
    if (activeParcel){
        activeParcel = null;
        appState.parcelId = -1;
    }
}
function isSiblingsTrue(sourceTable, expression) {
    try {
        var exp = expression.split(/\.IN\./g);
        var comparedSourceData = eval('activeParcel.' + sourceTable + '.parentRecord.' + exp[1].split('.')[0]);
        var value = eval('activeParcel.' + sourceTable + '.' + exp[0]);
        value = (value === null || value === undefined) ? value : value.toString();
        if (comparedSourceData === undefined || comparedSourceData === null || comparedSourceData.length == 0) return true;
        comparedSourceData = comparedSourceData.filter(function (csd) { return csd.CC_Deleted != true });
        return _.contains(comparedSourceData.map(function (cf) { return cf[exp[1].split('.')[1]]; }), value);
    }
    catch (ex) {
        console.error(ex);
        return false;
    }
}

function hideInfoContent() {
	$('#maskLayer').hide();
	$('.infoContentContainer').hide();
	$('.infoContent').html('');
	infoContentScroll = null
}

function bindInfoButtonClick() {
	$('.infoButton').unbind();
	$('.infoButton').bind(touchClickEvent, function (e) {
		var fieldId = $(this).attr('fieldId');
		var infoContent = datafields[fieldId].InfoContent.replace(/&lt;/g,'<').replace(/&gt;/g,'>');
		$('.infoContent').html('<div style="overflow-wrap: break-word;">' + infoContent + '</div>');
		$('.info_fieldName').html(datafields[fieldId].Label);
		$('.infoContentContainer').show();
		$('.infoContent').height($('.infoContent div').height() + 5);		
		showMask(null, true);
		hideKeyboard();
	});
}

function hidecompalertmsg() {
    $('#maskLayer').hide();
    $('.compalertmsgContainer').hide();
    $('.compalertmsg').html('');
}

function bindcompalertClick() {    
    console.log("bindcompalertClick function called");
    var infoContent;
    if (activeParcel.FieldAlert) {
        infoContent = activeParcel.FieldAlert;
    } else {
        infoContent = "";
    }    
    $('.compalertmsg').html('<textarea class="field-alert-message" rows="3" style="width:-webkit-fill-available;height:-webkit-fill-available; overflow-wrap:break-word;font-size:smaller; resize:none;">' + infoContent + '</textarea>');
        $('.compalertmsgContainer').show();
        $('.compalertmsg').height($('.compalertmsg textarea').height() + 5);        
        showMask(null, true);
        hideKeyboard();    
}

function savecompalertmsg() {
    var alertMessage = $('.compalertmsg .field-alert-message').val();
        if (alertMessage.trim() == '' && (activeParcel.FieldAlert == null || activeParcel.FieldAlert == '')) {
        }
        else {
            ccma.Sync.enqueueParcelAttributeChange(activeParcel.Id, 'FieldAlert', alertMessage, function () {
                getData("SELECT * FROM Parcel WHERE  Id = " + activeParcel.Id + "", [], function (p) {
                    if (p[0].Reviewed) {
                        refreshSortScreen = true;
                        executeQueries("UPDATE Parcel SET Reviewed = 'false' WHERE Id = " + activeParcel.Id)
                    }
                });
            });
    }
    $('.scrollable .field-alert-message ').html(alertMessage);   
    hidecompalertmsg();
    
}

function calculateChildRecords(field, index, rec) {
	var childCats = getChildCategories(field.CategoryId)
	if (childCats.length == 0) return null;
	var value = 0;
	var calcExprSplit = field.CalculationExpression.trim().split(childRecCalcPattern)
	var record = rec? rec: activeParcel[field.SourceTable][index];
	var childTbls = childCats.map(function(ch){return fieldCategories[fieldCategories.findIndex(function(s){ return s.Id == ch})].SourceTable });
	var chTbl = calcExprSplit[0].split('.')[0];
	var chField = calcExprSplit[0].split('.')[1];
	if (childTbls.some(function(c){ return c.indexOf(chTbl) > -1 })) {
		if (record[chTbl]) {
			record[chTbl].forEach(function(data){
				value += parseFloat((!data[chField] || data[chField].toString().trim() == '')? 0: data[chField]);
			});
			if (calcExprSplit[1] && calcExprSplit[1].toLowerCase() == 'avg' && record[chTbl].length > 0) value = value/record[chTbl].length;
			value = parseFloat(value.toFixed(2));
		}
	} else return null;
	return ccma.Data.Evaluable.__getEvalResult(ccma.Data.Types[2], value, record.Original[field.Name])
}

function applyConditionalValidation( field, fieldset, source ){

    var validations = getConditionalValidationInputs( field, source )
    $( 'tip', fieldset ).remove();
    $('.newvalue', fieldset).removeAttr('min max msReqVal');
    $( '.conditional-req', fieldset ).remove();
    $( '.newvalue[conditional-req]', fieldset ).removeAttr( 'conditional-req', 'required' );
    if (validations.length > 0) {
        validations = validations[0];
        if (validations.PERCENT_REQUIREMENT_TYPE_ID) {
            $('.newvalue', fieldset).attr({ msReqVal: validations.MAX_VALUE });
            $('.newvalue', fieldset).after('<tip>' + validations.INPUT_TIP_TEXT + ' : ' + validations.MIN_VALUE + ' - ' + validations.MAX_VALUE);
            $('.newvalue', fieldset).attr({ min: validations.MIN_VALUE, max: validations.MAX_VALUE });
        }
        else { 
            $('.newvalue', fieldset).attr({ min: validations.MIN_VALUE, max: validations.MAX_VALUE, allowDecimal: validations.DECIMAL_ALLOWED });
            $('.legend', fieldset).text(validations.DESCRIPTION);
            $('.newvalue', fieldset).after('<tip>' + validations.INPUT_TIP_TEXT + '</tip>');
        }
           
        if ( validations.IS_REQUIRED && $( '.reqd', $( fieldset ) ).length == 0 ) {
            var reqd = document.createElement( 'span' );
            $( reqd ).addClass( 'reqd conditional-req' );
            reqd.innerHTML = '*';
            $( fieldset ).prepend( reqd );
            $( 'input, textarea, select, .cusDpdownSpan', $( fieldset ) ).attr( {'required': true , 'conditional-req' :true } );
        }
    }
    else
        $( '.legend', fieldset ).text( field.Label );
}

function getConditionalValidationInputs( field, source ){
    var fieldConfig = field.ConditionalValidationConfig && field.ConditionalValidationConfig.split( ',' )
    if ( !fieldConfig || ( fieldConfig && fieldConfig.length < 2 ) )
        return false;
    var valTable = fieldConfig[0];
    var expression = fieldConfig[1];
    expression.match( /\{.*?\}/g ).forEach( function ( fn ){
        var testFieldName = fn.replace( '{', '' ).replace( '}', '' );
        var value = source[testFieldName]
        expression = expression.replace( fn, value );
    } )
    for ( x in ccma.FieldValidations[valTable][0] ) { var regex = new RegExp( "(?=\\b)(" + x + ")(?=\\b)", "g" ); expression = expression.replace( regex, 'thisObject.' + x ) };
    var validations = eval( 'ccma.FieldValidations["' + valTable + '"].filter(function (thisObject) { return (' + expression + ') })' );
    return validations;
}

function getDatatypeOfAggregateFields(aggr_field) {
	var aggr_Setting = aggregate_fields.filter(function(agr){ return ((agr.FunctionName + '_' + agr.TableName + '_' + agr.FieldName) == aggr_field); })[0];
	if (aggr_Setting) {
		var df = getDataField(aggr_Setting.FieldName, aggr_Setting.TableName);
		if (df) return ccma.Data.Types[parseInt(df.InputType)];
	}
	if (aggr_field.startsWith('SUM') || aggr_field.startsWith('AVG') || aggr_field.startsWith('COUNT') || aggr_field.startsWith('MAX') || aggr_field.startsWith('MIN')) {
    	return ccma.Data.Types[2];
    } else {
        return ccma.Data.Types[1];
    }
}
function getFieldsToValidate( isBPPParcel, bppCats ) {
    if ( !isBPPParcel )
        fields = Object.keys( datafields ).map( function ( x ) { return datafields[x] } ).filter( function ( x ) { return ( x.CategoryId == -2 ||  bppCats.map( function ( a ) { return a.Id } ).indexOf( x.CategoryId ) > -1 ) } );
    else
        fields = Object.keys( datafields ).map( function ( x ) { return datafields[x] } ).filter( function ( x ) { return ( x.CategoryId == -2 ||  bppCats.map( function ( a ) { return a.Id } ).indexOf( x.CategoryId ) > -1 ) } );
    return fields
}

function validationSwitch(){
	if(fylEnabled && validateCyFy){
		var typeclick = iPad ? 'touchend' : 'click';
		if(!(ccma.UI.ActiveScreenId == 'sort-screen' && sameInSsDcc)){
			showFutureData ? showFutureData = false : showFutureData = true; 
			$($( '.onoffswitch')[1]).trigger(typeclick); 
		}
		else{
			showFutureData ? $('#parcel-dashboard div.onoffswitch.fy-drag .onoffswitch-checkbox,#digital-prc div.onoffswitch.fy-drag .onoffswitch-checkbox').prop('checked', true) : $('#parcel-dashboard div.onoffswitch.fy-drag .onoffswitch-checkbox,#digital-prc div.onoffswitch.fy-drag .onoffswitch-checkbox').removeAttr( 'checked' )
		}
		validateCyFy = sameInSsDcc = false; 
    }
}

function updateDefaultPCIs(updateDefaultPCIs, callback){
	if(updateDefaultPCIs.length == 0){
		if(callback) {
			callback();
			return;
		}
	}
	var changes = '';
	var rowuid = updateDefaultPCIs[0].auxRowId;
	var SourceTable = updateDefaultPCIs[0].sourceTable;
	updateDefaultPCIs.forEach(function(item){
		changes += '[' + item.field + '] = \'' + item.newValue.toString().replace(/'/g, "''") + '\','
	});
	changes = changes.substring(0, changes.length-1);
	var updateSql = 'UPDATE ' + SourceTable + ' SET ' + changes +' WHERE ' + ( rowuid > 0 ? 'ROWUID = ': 'ClientROWUID = ') + rowuid;
	getData(updateSql, [], function () {
		if(callback) {
			callback();
			return;
		}
	}, null, true, function () {
		if(callback) {
			callback();
			return;
		}
	});
}

function checkPPAccess(feature, ...otherData) {
    let res = true;
    switch(feature){
        case "copyImprv":
        case "copyTemplate":
        case "massUpdate":
        case "recordFutureCopy":
        case "classCalculator":
        case "dccPhoto":
        case "saveSketch":
            /*if (isBPPUser && ((activeParcel.IsRPProperty == 'true' && (!ccma.Session.IsPPOnly || ccma.Session.IsPPOnly == 'false') && ccma.Session.RealDataReadOnly) || activeParcel.CC_Deleted == "true" || (activeParcel.IsRPProperty == 'true' && activeParcel.IsParentParcel == 'true' && activeParcel.Reviewed == 'true')))*/
            if (isBPPUser && ((activeParcel.IsRPProperty == 'true' && (!ccma.Session.IsPPOnly || ccma.Session.IsPPOnly == 'false') && ccma.Session.RealDataReadOnly) || activeParcel.CC_Deleted == "true"))
                res = false;
            break; 
        case "mac":
            if (isBPPUser && otherData[0] == "true" && ((!ccma.Session.IsPPOnly || ccma.Session.IsPPOnly == 'false') && ccma.Session.RealDataReadOnly))
                res = false;
            break; 
    }
    return res;
}

function checkHighLightInChild(catId, recData) {
	var childCats = fieldCategories.filter(function (d) { return d.ParentCategoryId == catId; }), childChanged = false;
	if( ( !childCats ) || ( childCats.length == 0 ) || ( !activeParcel.Changes ) || ( activeParcel.Changes.length == 0 ) )
		return childChanged;		
	for(ch in childCats) {
		var sourceTable = childCats[ch].SourceTable;
		var sourceDatas =  recData[sourceTable];
		if(sourceDatas && sourceDatas.length > 0) {
			for(sd in sourceDatas) {
				var sourceData = sourceDatas[sd];
				var sourceIndex = activeParcel[sourceTable].findIndex(x => x.ROWUID == sourceData.ROWUID)
				if(sourceIndex > -1) {
					var curRecChanges = activeParcel.Changes.filter(function (y) { return y.AuxRowId == sourceData.ROWUID  && !( y.OldValue == y.NewValue || ( y.OldValue == null && y.NewValue == '' ) ) && datafields[y.FieldId] && datafields[y.FieldId].SourceTable == sourceTable && ( datafields[y.FieldId].DoNotShow != "true" && datafields[y.FieldId].DoNotShowOnMA != "true" && datafields[y.FieldId].ReadOnly != "true" ) && ( datafields[y.FieldId].VisibilityExpression ? !( activeParcel.EvalValue(sourceTable + '[' + sourceIndex + ']', datafields[y.FieldId].VisibilityExpression) ) : true ) });
					var NewRecord = activeParcel.Changes.filter(function(p){ return p.AuxRowId && sourceData.ROWUID && p.AuxRowId.toString() == sourceData.ROWUID.toString() && p.Action=="new" && ( datafields[p.FieldId] ? datafields[p.FieldId].SourceTable == sourceTable: true ) });
					var icpChanges = activeParcel.Changes.filter(function(p){ return p.Action=="icp" && ( sourceData.ROWUID  == p.AuxRowId.split('$')[0] ) && ( datafields[p.FieldId] ? datafields[p.FieldId].SourceTable == sourceTable: true ) });     
					if( ( curRecChanges && curRecChanges.length > 0 ) || ( NewRecord && NewRecord.length > 0 ) || ( icpChanges && icpChanges.length > 0 ) ) {
						childChanged = true;
						return childChanged;
					}
				}
			}
		}
	}
	return childChanged;
}

function openCustomlkd(custlkd, cusdrop) {
	//if (cusdrop) {
	///	custlkd = $(custlkd).children('span'); 
	//}
    //$(custlkd).blur();
	newDdlPhotoProperties = null;  ddlPhotoSource = null;
    if ($(custlkd).hasClass("cusDpdown-arrow"))
    	custlkd =  $(custlkd).siblings('.cusDpdownSpan')[0];
    if ("activeElement" in document)
    	document.activeElement.blur();
    //$('select.newvalue').blur(); //blur select element when open search page
    //$(custlkd).siblings('.searchCustomddl')[0];
    var src = custlkd;
    var cFieldId = $(src).parent().attr('field-id');
    var cField = datafields[cFieldId];
    if (cField && cField.SourceTable == "_photo_meta_data") {
    	ddlPhotoSource = ccma.UI.ActivePhoto;
    	if (cField.IsUniqueInSiblings == "true") {
	    	var assignedName = cField.AssignedName;
	    	var metaField = assignedName.replace('PhotoMetaField', 'MetaData');
	    	newDdlPhotoProperties = {PhotoField: cField, metaField: metaField, metaValues: []};
	    	photos.images.forEach(function(opt) {
	    		if (opt.Id != ddlPhotoSource.Id)
	        		newDdlPhotoProperties.metaValues.push(opt[metaField]); //attr('disabled','disabled');
	        });
        }
    }
    openCustomddlList(src, true);
    return false;
}

function recalculateCustomDropdownWidth(fieldset) {
    var landscape = !WNTD && (Math.abs(window.orientation) == 90 || (screen.orientation && screen.orientation.type.split('-')[0]
        == "landscape"));
    var iw = $('.oldvalue', $(fieldset)).attr('iwidth');
    if (iw != null && iw > 0 && $('.oldvalue', $(fieldset)).text() != "") $('.oldvalue', $(fieldset)).css({ "width": parseInt(iw) + 2 + "px" });
    var totlen = $(fieldset)[0].offsetWidth - 16; var totLeft = $(fieldset).offset().left, leglen = $('.legend', $(fieldset))[0].offsetWidth;
    var dropLeft = $('.cusDpdownSpan', fieldset).offset().left, oldlen = $('.oldvalue', $(fieldset))[0] ? $('.oldvalue', $(fieldset))[0].offsetWidth : 0;
    var oldLeft = $('.oldvalue', fieldset)[0] ? $('.oldvalue', fieldset).offset().left : 0, waslen;
    if (totlen && leglen && totLeft && oldlen && oldLeft && dropLeft &&
        ((oldlen + oldLeft) > dropLeft)) {
        if ((iw == null || iw === 0) && $('.oldvalue', $(fieldset)).text() != "") $('.oldvalue', $(fieldset)).attr('iwidth', oldlen);
        if (!landscape) {
            waslen = (totlen + totLeft - oldLeft);
            let cs = document.createElement('canvas'), ct = cs.getContext('2d'), cuswd = $('.cusDpdownSpan', fieldset).attr('customWidth');
            ct.font = '14px Arial'; let vw = Math.round(ct.measureText($('.oldvalue', $(fieldset)).text()).width) + 40;
            if (cuswd && cuswd != '')
                waslen = Math.min(waslen, vw);
            $('.oldvalue', $(fieldset)).css({ "width": waslen + "px" });
        }
        else {
            waslen = (totlen - (leglen + 320));				//320 length of drop down and arrow.
            $('.oldvalue', $(fieldset)).css({ "width": waslen + "px" });
        }
    }
}

function recalculateCustomDropdownTextWidth(fieldset) {
    let cs = document.createElement('canvas'), ct = cs.getContext('2d'), v = $('.newvalue', fieldset).html();
    ct.font = '16px Arial'; let vw = ct.measureText(v).width;

    if (vw > 280) {
        let lw = $('.legend', fieldset)[0] ? $('.legend', fieldset)[0].offsetWidth : 0,
            ow = $('.oldvalue', fieldset)[0] ? $('.oldvalue', fieldset)[0].offsetWidth : 0,
            rw = $('.reqd', fieldset)[0] ? $('.reqd', fieldset)[0].offsetWidth : 0,
            iw = $('.infoButton', fieldset)[0] ? $('.infoButton', fieldset)[0].offsetWidth + 20 : 0,
            fw = $(fieldset)[0].offsetWidth, mw = lw + ow + rw + iw + 50;
        if (fw > 0) {
            let aw = fw - mw - 5; vw = iPad ? (vw + 20) : (vw + 5);
            $('.newvalue', fieldset)[0].style.width = Math.min(aw, vw) + 'px';
            $('.newvalue', fieldset).attr('customWidth', Math.min(aw, vw));
        }
    }
}


function getPriorityClr(cc_prty) {
    let prty = "Proximity", clr = "";
    switch (cc_prty) {
        case "5": prty = "Critical"; clr = "#FF0000"; break;
        case "4": prty = "Urgent"; clr = "#FFC000"; break;
        case "3": prty = "High"; clr = "#FFFF00"; break;
        case "2": prty = "Medium"; clr = "#00B0F0"; break;
        case "1": prty = "Normal"; clr = "#8944FA"; break;
    }
    return { prty: prty, clr: clr };
}

function getReadOnlyExpressionOverrideVal(field) {
    let categoryLevelReadOnlyExpressionOverride = false;
    if (field && datafieldsettings?.length > 0) {
        let dps = datafieldsettings.filter((x) => x.FieldId == field.Id)[0];
        if (dps && dps.PropertyName == "CategoryLevelReadOnlyExpressionOverride" && dps.Value == '1')
            categoryLevelReadOnlyExpressionOverride = true;
    }
    return categoryLevelReadOnlyExpressionOverride;
}