﻿//    DATA CLOUD SOLUTIONS, LLC ("COMPANY") CONFIDENTIAL 
//    Unpublished Copyright (c) 2010-2013 
//    DATA CLOUD SOLUTIONS, an Ohio Limited Liability Company, All Rights Reserved.

//    NOTICE: All information contained herein is, and remains the property of COMPANY.
//    The intellectual and technical concepts contained herein are proprietary to COMPANY
//    and may be covered by U.S. and Foreign Patents, patents in process, and are protected
//    by trade secret or copyright law. Dissemination of this information or reproduction
//    of this material is strictly forbidden unless prior written permission is obtained
//    from COMPANY. Access to the source code contained herein is hereby forbidden to
//    anyone except current COMPANY employees, managers or contractors who have executed
//    Confidentiality and Non-disclosure agreements explicitly covering such access.</p>

//    The copyright notice above does not evidence any actual or intended publication
//    or disclosure of this source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
//    DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
//    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
//    IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
//    OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
//    TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
//    ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

var PendingImagesCount = 0;
function PhotoStream()
{
    var ps = this;
    var uploadQueue = [];
    this.syncInProgress = false;

    this.openCamera = function ( camera, parcelId, keyvalue, onready, alternateValue )
    {
        var u = document.getElementById( camera );
        if ( ( navigator.appVersion.indexOf( "Win" ) != -1 )
        && ( camCapture.opened == false ) )
        {
            camCapture.data = { "camera": camera, "parcelId": parcelId, "keyvalue": keyvalue, "onready": onready,"alternateValue":alternateValue };
            camCapture.open();
        }
        else
        {
            $( '#camera1' ).unbind( touchClickEvent );
            $( '#camera1' ).bind( touchClickEvent, function ( e )
            {
                photos.addPhoto();
            } );
            $( '#camera2' ).unbind( touchClickEvent );
            $( '#camera2' ).bind( touchClickEvent, function ( e )
            {
                photos.addPhoto();
            } );
        }
        u.onchange = function ()
        {
            var x = new ps.PhotoUpload( parcelId, keyvalue, camera, onready, null, alternateValue );
            if ( camCapture.opened ) camCapture.close();
        }
       return false;
    }

    this.upload = function ( parcelId, file )
    {
        var x = new this.PhotoUpload( parcelId, null, file );
    }

    this.toString = function ()
    {
        var str = "";
        for ( x in uploadQueue )
        {
            if ( str != "" )
            {
                str += ", ";
            }
            var u = uploadQueue[x];
            str += u.parcelId + ":" + u.fileName;
        }
        return str;
    }

    this.getPhotos = function ( parcelId )
    {
        var xl = [];
        for ( x in uploadQueue )
        {
            var u = uploadQueue[x];
            if ( u.parcelId == parcelId )
            {
                xl.push( u );
            }
        }
        return xl;
    }

    this.acceptPhoto = function ( uid, callback )
    {
        for ( x in uploadQueue )
        {
            var u = uploadQueue[x];
            if ( u.uid == uid )
            {
                var t = ticks();
                var imageData = [{ParcelId:parseInt( u.parcelId ), Type:1, LocalId:u.uid, Accepted:1, Synced:0, UploadTime:t, Image:u.smallImage}];
                var unSyncedImageData = [{ ParcelId: parseInt( u.parcelId ), Image: u.dataUrl, UploadTime: t, Synced: 0, LocalId: u.uid, Type: 1 }];
                todoDB.indexedDB.Insert("UnsyncedImages", unSyncedImageData, function( data, dx) {
	                todoDB.indexedDB.Insert("Images", dx[0], function( data, dp) {
	                    uploadQueue.splice( dp.di, 1 );
                        executeQueries("UPDATE Parcel SET Reviewed = 'false' WHERE Id = " + parseInt(dp.pid))
                        let qry = EnableNewPriorities == 'True' ? "UPDATE Parcel SET CC_Priority = 3 WHERE CC_Priority = 0 AND Id = " + parseInt(dp.pid) : "UPDATE Parcel SET CC_Priority = 1 WHERE CC_Priority = 0 AND Id = " + parseInt(dp.pid);
                        executeQueries(qry)
	                    if ( callback ) callback();
	                }, {
	                    pid: imageData[0].ParcelId,
	                    di: dx[1]
	                });
                }, [imageData, x]);
            }
        }
    }

    this.removePhoto = function ( uid, callback )
    {
        var del = -1;
        for ( x in uploadQueue )
        {
            var u = uploadQueue[x];
            if ( u.uid == uid )
            {
                del = x;
            }
        }

        uploadQueue.splice( del, 1 );
        if ( callback ) callback();
    }

    this.syncNext = function ( successCallback, failedCallback, completedCallback )
    {
        var bypass = function ( data, callback )
        {
            if ( callback ) callback( data );
        }
        showSyncStatus( 'Syncing next photo' );
        if ( photos.active ) { if ( failedCallback ) failedCallback(2); return; }
        if ( ps.syncInProgress ) {
            showSyncStatus( 'Another sync is in progress.' );
            if ( failedCallback ) failedCallback(); return;
        }
        ps.syncInProgress = true;
        try {
        	var syncImage = function(im) {
        		if ( im.length == 0 ) {
                    showSyncStatus( 'No photos to sync ..' );
                    ps.syncInProgress = false;
                    if ( completedCallback ) completedCallback();
                    return;
                } else {
                    showSyncStatus( 'Syncing 1 photo' );
                    var ph = im[0];
                    bypass( ph.Image, function ( smallImage ) {
                        //  scaleImage(ph.Image, ccma.UI.Camera.MaxStorageWidth, ccma.UI.Camera.MaxStorageHeight, function (smallImage) {
                        var tt = ph.UploadTime;
                        var lid = ph.LocalId;
                        if ( tt == null || tt == '' ) {
                            tt = ticks();
                        }
                        ajaxRequest( {
                            url: baseUrl + 'mobileassessor/photosync.jrq?zt=' + ticks(),
                            dataType: 'json',
                            type: "POST",
                            data: {
                                parcelId: ph.ParcelId,
                                photo: ph.Image,
                                Type: ph.Type,
                                localid: lid,
                                timestamp: tt

                            },
                            success: function (data, stat) {
                            	showSyncStatus('Photo sync response: ' + data ? JSON.stringify(data).substring(0, 150) : "");
                                if (data.LoginStatus == '401') {
                                    LogoutErrorInsert("page: syncnext camer.js login");
                                    Logout()
                                }
                                if (data.status == "OK") {
                                	getTransaction(['UnsyncedImages'], false, function(tx) {
										var keyRangeValue = IDBKeyRange.only( ph.UnsyncedImagesKey )
		                                var store = tx.objectStore('UnsyncedImages')
		                                var delReq = store.delete(keyRangeValue);
										tx.oncomplete = function (e) {
                                    		todoDB.indexedDB.update('Images',[{name: 'Synced', value: 1}, {name: 'Id', value: data.imageId}], ['LocalId'], ['='], [ph.LocalId], [], function(){
												ps.syncInProgress = false;
												if (successCallback) successCallback();
											})
										}
	                                
	                                    delReq.onsuccess = function (evt){
	                                    	console.log('Deleted unsynced image')
										}
	                                
                                	});                            	
                            	}
                            	else {
                                    if ( data.message.indexOf( 'Conversion from string' ) > -1 ) {
                                    	todoDB.indexedDB.update('Images', [{name: 'Synced', value: 1}], ['LocalId'], ['='], [lid.toString()], [], function() {
                                        //getData( "UPDATE Images SET Synced = '1' WHERE LocalId = ?", [lid.toString()], function () {
                                            photos.loadImages(function () {
                                                ps.syncInProgress = false;
                                                if (successCallback) successCallback();
                                            }, true);
                                        });
                                    }
                                    else {
                                        debug_mode_log('Error occured while uploading photo.');
                                        console.log(data);
                                        ps.syncInProgress = false;
                                        if (failedCallback) failedCallback();
                                    }

                                }

                            },
                            error: function (req, stat, err) {
                                showSyncStatus(err);
                                ps.syncInProgress = false;
                                if (failedCallback) failedCallback();
                            }
                        } );
                    } );

                }
        	}
        	getTransaction(['UnsyncedImages'], true, function(tx) {
				var obj = tx.objectStore('UnsyncedImages').index('Synced').get(0), res;
				tx.oncomplete = function (e) {
					syncImage(res)
				}
				obj.onsuccess = function(e) {
				    res = ( e.target.result ? [e.target.result] : [] );
				}
				obj.onerror = function() {
					showSyncStatus( 'SQL Error - photo sync failed.' );
	                ps.syncInProgress = false;
	                if ( failedCallback ) failedCallback();
				}
				var objStore2 = tx.objectStore( 'UnsyncedImages' ).count()
				objStore2.onsuccess = function ( e ){
				    PendingImagesCount = e.target.result;
				}
			});
        } catch ( e ) {
            showSyncStatus( e );
            ps.syncInProgress = false;
            console.log( 'Complete photo sync. (error)' )
            if ( failedCallback ) failedCallback();
        }
    }

    this.pop = function ()
    {
        return uploadQueue.pop();
    }

    this.push = function ( o )
    {
        uploadQueue.push( o );
    }

    this.__defineGetter__( "readylength", function ()
    {
        var rl = 0;
        for ( x in uploadQueue )
        {
            var pu = uploadQueue[x];
            if ( pu.accepted )
            {
                rl++;
            }
        }

        return rl;
    } );

    this.__defineGetter__( "length", function ()
    {
        return uploadQueue.length;
    } );


    this.PhotoUpload = function ( parcelId, keyvalue, file, onready, dataUrl, alternateValue )
    {
        var pu = this;
        this.parcelId = parcelId;
        this.keyvalue = keyvalue;
        this.timeStamp = new Date();
        this.file = null;
        this.dataUrl = dataUrl;
        this.accepted = false;
        this.uid = "pu-" + tick().toString()
        this.metadata = [];

        var f;
        if ( file.files )
        {
            if ( file.files.length > 0 )
            {
                f = file.files[0];
            }
            else
            {

            }
        }
        if ( file.lastModifiedDate )
        {
            f = file;
        }
        if ( typeof file == "string" )
        {
            f = document.getElementById( file ).files[0];
        }

        if ( f == null )
        {

        } else
        {
            this.file = f;
            this.fileName = f.name;
        }

        uploadQueue.push( this );

        if ( this.dataUrl )
        {
            console.log( "upload dataurl" );
            if ( onready ) onready( this );
        }
        
        $( '#' + file ).val('');
        if(keyvalue < 0)
        	keyvalue = "";
        var keyvalueLabel = keyvalue
	  keyvalueLabel = ShowAlternateField == "True" ? (ShowAlternateField == "True" && ShowKeyValue1 == "True" && alternateValue) ? keyvalueLabel +' | '+ alternateValue : alternateValue : keyvalueLabel
        //if (f) {
            scaleImage((f? f: dataUrl), ccma.UI.Camera.MaxCaptureHeight, ccma.UI.Camera.MaxCaptureWidth, function  (data, smallData) {
                $( '#' + file ).val('');
                if (onready) onready(pu);
            }, { printTimestamp: true, printLabel: true, label: keyvalueLabel, smallImage: true }, pu);
        //}

        //        if (f) {
        //            var r = new FileReader();
        //            r.onload = function (e) {
        //                var c = e.target.result;
        //                pu.dataUrl = c;
        //                if (onready) onready(pu);
        //            }

        //            r.readAsDataURL(f)
        //        }

    }
}

function getScaledSize( source, maxWidth, maxHeight )
{
    var width, height;
    var aspectRatio = source.Width / source.Height;
    if ( ( maxHeight > 0 ) && ( maxWidth > 0 ) )
    {
        if ( ( source.Width < maxWidth ) && ( source.Height < maxHeight ) )
        {
            return source;
        }
        else if ( aspectRatio > 1 )
        {
            // Calculated width and height,
            // and recalcuate if the height exceeds maxHeight
            width = maxWidth;
            height = parseInt( width / aspectRatio );
            if ( height > maxHeight )
            {
                height = maxHeight;
                width = parseInt( height * aspectRatio );
            }
        }
        else
        {
            // Calculated width and height,
            // and recalcuate if the width exceeds maxWidth
            height = maxHeight;
            width = parseInt( height * aspectRatio );
            if ( width > maxWidth )
            {
                width = maxWidth;
                height = parseInt( width / aspectRatio );
            }
        }
    }

    return { Width: width, Height: height };
}

function scaleImage( file, max_width, max_height, afterResize, options, image )
{
    if ( options == null ) options = {};
    var fileLoader = new FileReader();
    canvas = document.createElement( 'canvas' );
    context = null;
    var imageObj = null;
    imageObj = new Image(),
    blob = null;
    //create a hidden canvas object we can use to create the new resized image data
    canvas.width = max_width;
    canvas.height = max_height;

    //get the context to use 
    context = canvas.getContext( '2d' );

    // setup the file loader onload function
    // once the file loader has the data it passes it to the 
    // image object which, once the image has loaded, 
    // triggers the images onload function
    fileLoader.onload = function ()
    {
        var data = this.result;
        imageObj.src = data;
    };

    fileLoader.onabort = function ()
    {
        alert( "The capture was aborted." );
    };

    fileLoader.onerror = function ()
    {
        alert( "An error occurred while reading the file." );
    };


    // set up the images onload function which clears the hidden canvas context, 
    // draws the new image then gets the blob data from it
    imageObj.onload = function ()
    {
        if ( this.width == 0 || this.height == 0 )
        {
            alert( 'Image is empty' );
            return;
        }
        var resize = function ( w, h, item, callback )
        {
            var size = getScaledSize( { Width: imageObj.width, Height: imageObj.height }, w, h );
            canvas.width = size.Width;
            canvas.height = size.Height;
            context.clearRect( 0, 0, size.Width, size.Height );
            context.drawImage( imageObj, 0, 0, imageObj.width, imageObj.height, 0, 0, size.Width, size.Height );
            if ( options.printTimestamp || options.printLabel )
            {
                context.font = "20px Arial";
                var text1 = ( new Date() ).toLocaleString();
                var text2 = options ? ( options.label || '' ) : '';
                var t1w = context.measureText( text1 ).width;
                var t2w = context.measureText( text2 ).width;
                context.fillRect( 0, size.Height - 28, size.Width, 28 );
                context.fillStyle = 'white';
                if ( options.printTimestamp )
                    context.fillText( text1, 10, size.Height - 4 );
                if ( options.printLabel )
                    context.fillText( text2, size.Width - t2w - 10, size.Height - 4 );
            }
            image[item] = canvas.toDataURL( "image/jpeg", 8 )
            if ( callback ) callback()
        }
        resize( max_width, max_height, "dataUrl", function ()
        {
            resize( ccma.UI.Camera.MaxStorageWidth, ccma.UI.Camera.MaxStorageHeight, "smallImage", function ()
            {
                if ( afterResize ) afterResize()
            } )
        } )
    };

    imageObj.onabort = function ()
    {
        alert( "Image load was aborted." );
    };

    imageObj.onerror = function ()
    {
        alert( "An error occurred while loading image." );
    };

    // check for an image then
    //trigger the file loader to get the data from the image
    //console.log(file)
    if ( typeof file == "object" && ( ( file.constructor.name == "File" ) || ( file.constructor.toString() == "[object FileConstructor]" ) ) )
    {
        if ( file.type.match( 'image.*' ) )
        {
            fileLoader.readAsDataURL( file );
        } else
        {
            messageBox( 'File is not an image' );
        }
    } else if ( typeof file == "string" )
    {
        imageObj.src = file;
    }
}

function resendSyncedPhotos()
{
    var warning = 'This action will flag ALL photos taken on the iPad to be resent to the QC Module. <br/><br/>If the application was online with a strong Internet connection for more than 15 minutes, and you have confirmed photos have not reached the QC Module after 24+ hours, select "OK" to continue. <br/><br/>Only select "OK" if you have confirmed photos have not reached the QC Module, as this could result in duplicate photos being created.';
    //    var oldWarning = 'This is an attempt to resend ALL cached photos taken from this mobile device. This should only be used if recent photos have not reached the QC Module even after 24 hours, with at least 15 minutes of Internet connected application use. Click OK only if you are sure that your photos have not reached the QC Module, even after waiting for such a long period of time with Internet.';
    messageBox( warning, ["OK", "Cancel"], function ()
    {
        window.location.href = '/photorecover.aspx';
    }, function ()
    {

    } )
}