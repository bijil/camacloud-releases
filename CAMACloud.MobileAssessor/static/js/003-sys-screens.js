﻿//    DATA CLOUD SOLUTIONS, LLC ("COMPANY") CONFIDENTIAL
//    Unpublished Copyright (c) 2010-2013
//    DATA CLOUD SOLUTIONS, an Ohio Limited Liability Company, All Rights Reserved.

//    NOTICE: All information contained herein is, and remains the property of COMPANY.
//    The intellectual and technical concepts contained herein are proprietary to COMPANY
//    and may be covered by U.S. and Foreign Patents, patents in process, and are protected
//    by trade secret or copyright law. Dissemination of this information or reproduction
//    of this material is strictly forbidden unless prior written permission is obtained
//    from COMPANY. Access to the source code contained herein is hereby forbidden to
//    anyone except current COMPANY employees, managers or contractors who have executed
//    Confidentiality and Non-disclosure agreements explicitly covering such access.</p>

//    The copyright notice above does not evidence any actual or intended publication
//    or disclosure of this source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
//    DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
//    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
//    IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
//    OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
//    TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
//    ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
var sort = 1;
var currentHeatMapField, currentHeatMapFieldId, currentHeatMapFieldValue, focusedElement;
var datacollectionsscroll = false;
var combinedView = false;
var selectedFilters = 0;
function initScreens() {
    $('.tools a').unbind('touchstart touchend click')
    $('.tools .logout').bind('touchstart click', function (e) {
        e.preventDefault();
        if (onTransition()) return false;
        if (!canClickNow()) return false;
        messageBox('Are you sure you want to logout from the application?', ["Yes", "No"], function () {
            activeParcel = null;
            Logout();
        });
    });

    $('.tools .sync').bind(touchClickEvent, function (e) {
        if (onTransition()) return false;
        if (!canClickNow()) return false;
        e.preventDefault();
        syncInProgress = false;
        showSync();
    });

    $('.tools .sort').bind(touchClickEvent, function (e) {
        focusedElement = $(':focus');
        saveFocusedElement(focusedElement, function () {
            if (onTransition()) return false;
            if (!canClickNow()) return false;
            e.preventDefault();
            refreshNbhdProfile = true;
            currentNbhd = null;
            if ( isBPPUser && ccma.Session.BPPDownloadType == '1' ){
                listType = 0;
				activeParentParcel = null;
				if(combinedView) combinedView = false;
			}
            else if ( isBPPUser && ccma.Session.BPPDownloadType == '0' )
                listType = 2;
            showSortScreen();
            syncInProgress = false;
        });
    });

    $('.tools .search').bind(touchClickEvent, function (e) {
        focusedElement = $(':focus');
        saveFocusedElement(focusedElement, function () {
            if (onTransition()) return false;
            if (!canClickNow()) return false;
            e.preventDefault();
            createAggrFieldsForNbhd(function () {
                showSearch();
                syncInProgress = false;
            });
        });
    });
	$('.sort-toggle').unbind(touchClickEvent);
	$('.sort-toggle').bind(touchClickEvent,function(e){
		parcelOpenMode = null;
		toggleWithBPP();
	});
    $('.tools .dashboard').bind(touchClickEvent, function (e) {
        if (onTransition()) return false;
        if (!canClickNow()) return false;
        e.preventDefault();
        showDashboard();
        syncInProgress = false;
    });

	$('.standalone-bpp').unbind(touchClickEvent);
	$('.standalone-bpp').bind(touchClickEvent, function (e){
		createNewParcel();
	});
	
    $('.page-back').unbind(touchClickEvent);
    $('.page-back').bind(touchClickEvent, function (e) {
        if (!loggedIn)
            return false
        if (ccma.UI.ActiveScreenId == "sort-screen") {
            $('#my-parcels').html('');
            if (!refreshSortScreen && cachedSortScreen && cachedSortScreen.parcels.length > 0 && !refreshMiniSS) {
                cachedSortScreen.parcels.appendTo("#my-parcels");
                $('header .headerspan', $('#sort-screen')).html(cachedSortScreen.headerText);
                listType = 0;
                appState.homeParcelSelectedIndex = 0;
                sortScreenEvents(cachedSortScreen.parcels);
            } else {
                refreshMiniSS = false;
                listType = 0;
                showSortScreen();
            }
            $('#dc-form-contents section').removeAttr('findex index filteredindexes records parentindex');
            $('.select-parcelspanHeader .selectParcelCheckbox').hide('selectedcheckbox');
            $('.map-view').show();
            $('.change-sort-view').show();
            $('a.page-back', $('#sort-screen')).hide();
            if (isBPPUser) { $('.sort-toggle').show(); activeParentParcel = null; }
            cachedSortScreen = {};
            if (isBPPUser && listType == 0 && !combinedView) {
                refreshImagesInSortScreen('my-parcels');
            }
            return
        }
        if (ccma.UI.ActiveScreenId == "parcel-dashboard") {
            if ((ParcelopenfromMap == "true" || ParcelopenfromMap == true) && isBPPUser && !combinedView && !(ccma.Session.BPPDownloadType == "0")) listType = 0;
            if (refreshSortScreen == true) {
                if (isBPPUser)
                    ShowParcelList(false, selectedFilters);
                else
                    refreshParcelInList();
            }
        }
        if (onTransition()) return false;
        e.preventDefault();
        var skipExit = false;
        if (ccma.UI.ActiveScreenId == "gis-map") {
            clearMap();
            if (isBPPUser && ccma.Session.BPPDownloadType == '1' && !combinedView && !activeParcel)
                listType = 0;
            if (parcelMapMode) {
                if (refreshSortScreen == true) {
                    if (isBPPUser)
                        ShowParcelList(false, selectedFilters)
                    else
                        refreshParcelInList();
                }
                parcelMapMode = false;
                showSortScreen(null, true);
                showMultipleMACButton();
                return false;
            }

        }
        //mass-update-div  
        if (ccma.UI.ActiveScreenId == "sketchview") {
            if (sqlinsert != "" && appState.catId > 0 && !_.isEmpty(cachedAuxOptions) && cachedAuxOptions[appState.catId])
                reloadAuxiliaryRecords(appState.catId)
            if ((clientSettings.EnableSVInMA != undefined && clientSettings.EnableSVInMA == '1') && appState.prevScreenId == 'gis-map')
                previewSVSketch();
        }
        //if (ccma.UI.ActiveScreenId == "digital-prc") {
        //    if (sqlinsert != "" && appState.catId > 0 && !_.isEmpty(cachedAuxOptions) && cachedAuxOptions[appState.catId])
        //        reloadAuxiliaryRecords(appState.catId)
        //    if ((clientSettings.EnableSVInMA != undefined && clientSettings.EnableSVInMA == '1') && appState.prevScreenId == 'gis-map')
        //        previewSVSketch();
        //}
        
        if (ccma.UI.ActiveScreenId == "mass-update-div") {
            if (changedValues.length > 0) {
                skipExit = true;
                messageBox(msg_cancel, ["Yes", "No"], function () {
                    changedValues = [];
                    $T.goBack();
                }, function () {
                    return false;
                });
            }
        }
        var isDatacollectionScreen = false;
        if (ccma.UI.ActiveScreenId == "data-collection") {
        	isDatacollectionScreen = true;
			$(focussedInputElement).blur();
			relationcycle = [];
            var valid = checkRequiredFieldsInForm();
            if (!valid) {
                messageBox('All required fields are not filled in. Are you sure you want to go back to main menu?', ["Yes", "No"], function () {
                    $T.goBack();
                    ccma.UI.ActiveFormTabId = 0;
                    if (ccma.UI.ActiveScreenId == "digital-prc") {
                    	if (isDatacollectionScreen) sketchSaveClicked = false;
                        firstTime = true;
                        refreshDigitalPRC();
					}
                });
                $(inputElementInFocus).blur();
			  	/*if (formScroller) {
                    formScroller.scrollTo(0, 0, 0);
                    formScroller.refresh();
                }*/
                if (ccma.UI.ActiveScreenId == "parcel-dashboard") {
					if (isDatacollectionScreen) sketchSaveClicked = false;
                    selectDcTab(0);
                }
                return false;
            }
            if (ccma.UI.Events.refreshParcelListAfterDataEntry) {
                ccma.UI.Events.refreshParcelListAfterDataEntry = false;             
                refreshParcelInList();
            }
        }
        if (ccma.UI.ActiveScreenId == "parcel-dashboard") {
        	if(activeParcel && activeParcel.Id){
	            var fromParcel = activeParcel.Id.toString();
	            var targetParcel = ccma.UI.ParcelLinks.LinkMap[fromParcel];
	            if (targetParcel) {
	            	var openedfmMap = (targetParcel.openedFromLink && appState.openfromMap)? appState.openfromMap: undefined;
	                showParcel(targetParcel.caller, function () {
	                    if (targetParcel.lastScreen == "data-collection") {
	                        showDataCollection(targetParcel.lastTab);
	                    }
	                    ccma.UI.ParcelLinks.LinkMap[fromParcel] = null;
	                    delete ccma.UI.ParcelLinks.LinkMap[fromParcel];
	                }, openedfmMap)
	                return false;
	            } else {
	                activeParcel = null;
	                appState.parcelId = -1;
	                $('#dc-form-contents section').removeAttr('findex index filteredindexes records parentindex');
	                if (ParcelopenfromMap == true || ParcelopenfromMap == "true") {
	                    loadParcelsInMap(true);
						return;
	                }
	                else {
	                    showSortScreen(null, true);   
	                    return false;
	                }
	            }
            }
            else{
	            showSortScreen();
	            return false;
            }
        }
        if (ccma.UI.ActiveScreenId == "user-dashboard") {
            unSyncChanges();
            if ((enableMultipleNbhd && Object.keys(neighborhoods).length > 1) && (changedNbhd != currentNbhd)) {
                $('.nbhd-profile-select').val(changedNbhd);
                $('.nbhd-profile-select').trigger('change');
                changedNbhd = null;
            }
        }
        if (!skipExit) {
            $T.goBack();               
        }
        if ((ccma.UI.ActiveScreenId == "sort-screen") || (ccma.UI.ActiveScreenId == "search")) {
            activeParcel = null;
            appState.parcelId = -1;
            showMultipleMACButton();
        }

        if (ccma.UI.ActiveScreenId == "digital-prc") {
            if (isDatacollectionScreen) sketchSaveClicked = false;
            if (activeParcel)
                sortSourceData(activeParcel)
            appState.photoCatId = null;
            refreshDigitalPRC();
        }

        if (ccma.UI.ActiveScreenId == "gis-map") 
            if (activeParcel == null)
                setActiveScreenTitle('GIS Map View');

		/*if (formScroller) {
            formScroller.refresh();
            formScroller.scrollTo(0, 0, 0);
        }*/
        setScreenDimensionsDelayed(null, false, true);
        if (ccma.UI.ActiveScreenId == "parcel-dashboard") {
        	if (isDatacollectionScreen) sketchSaveClicked = false;
            selectDcTab(0);            
        }
    });

    $('.digital-prc').unbind(touchClickEvent);
    $('.digital-prc').bind(touchClickEvent, function (e) {
        e.preventDefault();
        showDigitalPRC();
    });

    $('.hide-visited').unbind(touchClickEvent);
    $('.hide-visited').bind(touchClickEvent, function (e) {
        e.preventDefault();
        if (searchedCount > 0) {
            SearchParcels(true);
        }
        //        $('.parcel-item[completed="true"]').hide();
    });

    const getSelectedNbhd = ()=>{
        let haveMultipleNbhd = Object.keys(neighborhoods).length > 1 ? 1 : 0;
        if(haveMultipleNbhd)
            return $('.dashboard-nbhd-select').val();
        else
            return neighborhoods[$('.dashboard-nbhd').html()][0].Id
    }

    $('.screen').addClass('hidden');
    $('.agree-nbhd').unbind(touchClickEvent)
    $('.agree-nbhd').bind(touchClickEvent, function (e) {
        e.preventDefault();
        var agreeNbhd = $(this).attr('agree');
        var neg = (agreeNbhd == "true" ? "false" : "true");
        if(currentNbhd.ProfileReasonable != agreeNbhd){
            let currentNbhd = neighborhoods[stats.Neighborhood.filter((x)=>{return x.NbhdId == getSelectedNbhd()})[0]?.Nbhd][0]
            ccma.Sync.enqueueCommand(getSelectedNbhd(), 'NBHD', 'Agree', agreeNbhd, { updateData: true, source: 'Neighborhood', sourceKey: 'Number', sourceKeyValue: getSelectedNbhd(), sourceField: "ProfileReasonable" }, function () {
                $('.agree-nbhd[agree="' + neg + '"]').addClass('unselected');
                $('.agree-nbhd[agree="' + agreeNbhd + '"]').removeClass('unselected');

                currentNbhd.ProfileReasonable = agreeNbhd
                if (agreeNbhd == "false") {
                    $('.nbhd-profile-float').addClass('red');

                } else {
                    $('.nbhd-profile-float').removeClass('red');
                }
            });
        }
    });

    $('.nbhd-profile-comment-update').unbind(touchClickEvent);
    $('.nbhd-profile-comment-update').bind(touchClickEvent, function (e) {
        e.preventDefault();
        hideKeyboard();
        var comment = $('.nbhd-profile-comment').val();
        ccma.Sync.enqueueCommand(getSelectedNbhd(), 'NBHD', 'Comment', comment, { updateData: true, source: 'Neighborhood', sourceKey: 'Number', sourceKeyValue: getSelectedNbhd(), sourceField: "LastComment" }, function () {
            let currentNbhd = neighborhoods[stats.Neighborhood.filter((x)=>{return x.NbhdId == getSelectedNbhd()})[0]?.Nbhd][0]
            currentNbhd.LastComment = comment
            messageBox('Comment on neighborhood is updated.');
        });
    });
    $('header a.change-sort-view', $('#sort-screen')).unbind(touchClickEvent);
    $('header a.change-sort-view', $('#sort-screen')).bind(touchClickEvent, function (e) {
        if (onTransition()) return false;
        if (!canClickNow()) return false;
        e.preventDefault(); toggleSortScreen()
    });
    $('header a.multiple-mac', $('#sort-screen')).unbind(touchClickEvent);
    $('header a.multiple-mac', $('#sort-screen')).bind(touchClickEvent, function (e) {
        e.preventDefault(); multipleMAC();
    });
	$('header a.sort-view', $('#sort-screen')).unbind(touchClickEvent);
    $('header a.sort-view', $('#sort-screen')).bind(touchClickEvent, function (e) {
        if (onTransition()) return false;
        if (!canClickNow()) return false;
        e.preventDefault();
        var MAoptions = $.map(ccma.UI.SortOptions, function (v) {
            return v;
        } );
        var bppFilters=[{Id:'1',desc:'Select All'},{Id:'2',desc:'Real Property'},{Id:'3',desc:'Personal Property'}];
        //cachedSortScreen = {};
        var filtertText = '<span class="bpp-filter-text">Select the type of Filter: </span><br>'
        if(isBPPUser  && !activeParentParcel){
        	$('.message-box-input-filters').html('');
        	$('.message-box-input-filters').prepend(filtertText);
			for (var item in bppFilters){ if(!combinedView && ccma.Session.BPPDownloadType == 1 && bppFilters[item].Id == "3") continue; if(!combinedView && ccma.Session.BPPDownloadType == 0 && bppFilters[item].Id == "2") continue; $('.message-box-input-filters').append( $('<label class="bpp-filter-label"><input type="radio" id="' + bppFilters[item].Id + '" class="bpp-filter-radio" name="bpp-filter-radio" />' +  bppFilters[item].desc + '</label><br/>'));}
        	if(selectedFilters != 0) $('.message-box-input-filters input[id="'+selectedFilters+'"]').attr('checked',true)
        	$('.message-box-input-filters').show();
        	}
			else $('.message-box-input-filters').hide();
        if ( isBPPUser && listType == 1 )
            MAoptions = MAoptions.filter( function ( item ) { return item.Name.indexOf( 'Location' ) < 0 } );
        var SortOptions1 = clientSettings.MAsortFields;
        if (SortOptions1 && SortOptions1 != '') {
            var tt = SortOptions1.split(',');
            tt.forEach(function (q) {
                var q1 = q + "-Asc";
                var q2 = q + "-Desc";
                MAoptions.push({ Id: q1, Name: q1 })
                MAoptions.push({ Id: q2, Name: q2 })
            });
        }
        input('Select the type of Sorting:', MAoptions, function (newlabel) {
            ccma.UI.SortOrderType = newlabel;
            var geoSort = (newlabel == '1' || newlabel == '2') ? true : false;
            selectedFilters = 0;
            //$('.message-box-input-filters input').each(function(i,o){if($(o).attr("checked")==true ) selectedFilters = $(o).attr('id')});
            selectedFilters = $( '.message-box-input-filters input' ).filter( function () { return this.checked } )[0] ? $( '.message-box-input-filters input' ).filter( function () { return this.checked } )[0].id : 0;
            ShowParcelList(geoSort,selectedFilters);
            if(isBPPUser && listType == 1)
            	refreshSortScreen = true; 
            $('.message-box-input-filters').html('');
        }, function () { $('.message-box-input-filters').html(''); }, 'customlookups', null);

        $('.message-box-input-item').val(ccma.UI.SortOrderType);
       //$('.message-box-input-sel').val(ccma.UI.SortOrderType);
        //        messageBox('Tap OK to sort the selected list of parcels with respect to your geographical location.', ["OK", "Cancel"], function () {
        //            ShowParcelList();
        //        });
    });
    $('header a.map-view', $('#sort-screen')).unbind(touchClickEvent);
    $('header a.map-view', $('#sort-screen')).bind(touchClickEvent, function (e) {
        e.preventDefault();
        openMapView();
    });
    $('.heatmap-items').unbind('change');
    $('.heatmap-items').bind('change', function () {
        // showHideLines();
        if ($(this).val() != "") {
            HeatMapIcons = {};
            currentHeatMapField = $(this).val();
            currentHeatMapFieldId = $(this).find('option[value="' + currentHeatMapField + '"]').attr('Id');
            localStorage.setItem('currentHeatMapField', currentHeatMapField);
            localStorage.setItem('currentHeatMapFieldId', currentHeatMapFieldId);
        } else {
            currentHeatMapField = null;
            currentHeatMapFieldId = null;
            currentHeatMapFieldValue = null;
            localStorage.setItem('currentHeatMapField', '');
            localStorage.setItem('currentHeatMapFieldId', '');
        }

        setLegend(currentHeatMapFieldId);
        parcelMapMarkers = [];
        HeatMapRefreshed = true;
        OsMap.removeLayer(heatMapLayer);
        OsMap.removeLayer(polygonlayer);
        polygonlayer = new L.FeatureGroup();
        heatMapLayer = new L.FeatureGroup();
        refreshParcelsInMap();
    });
    //    $('#data-collection input').blur(setScreenDimensionsDelayed);
    //    $('#data-collection textarea').blur(setScreenDimensionsDelayed);
    //    $('#data-collection select').blur(setScreenDimensionsDelayed);
    $('.page-footer').bind(touchClickEvent, setScreenDimensionsDelayed);
    $('.page-header').bind(touchClickEvent, setScreenDimensionsDelayed);
    //    $('#data-collection select').keyup(setScreenDimensionsDelayed);
}

/*function refreshOnFocus(){
    if ( !inputElementInFocus ) return
    var usableHeight = $( '.page-content' ).height() - $( 'header', $( '#' + ccma.UI.ActiveScreenId ) ).height();
    var currentScroll = $( inputElementInFocus ).offset().top - 120;
    var t = currentScroll - usableHeight;
    if ( t > 0 )
    {
        $( '#dc-form' ).scrollTop( t + $( '#dc-form' ).scrollTop() + 100 )
    }
}*/

function loadScreen(screenId, callback, animation, showMenu) {
    if ((!loggedIn && (screenId != 'login' && screenId != 'agreement' && screenId != 'error')))
            return false
    if (onTransition()) return false;
    if (showMenu == null)
        showMenu = true;
    if (animation == null)
        animation = 'slideleft';
    ccma.UI.ActiveScreenId = screenId;
    log("Switching to screen: " + screenId);
    var toPage = $('#' + screenId);
    toPage.bind('pageAnimationEnd', function () {
        toPage.unbind('pageAnimationEnd');
        appState.screenId = screenId;
        saveAppState();
        $('section[id ="'+screenId+'"] .scrollable').scrollTop(0);
        loadMenu(screenId, function () {
            recalculateDimensions(true);
            updateLastActivityTime();
            if (callback) callback();
        });
    });
    animation = null;
    $T.goTo(toPage, animation);
}
/*
function refreshScrollable(screen, scrollto) {
    if (scrollto)
        scrollto = true;
    else
        scrollto = false;
    var scrollNeeded = inputElementInFocus && (clickfocus == false || keyboardActive == false) && ($(inputElementInFocus).attr('type') == 'text' || $(inputElementInFocus).attr('type') == 'number' || $(inputElementInFocus).attr('type') == 'textarea')
    if (scrollNeeded && formScroller) {
        formScroller.scrollToElement(inputElementInFocus, "0.3S");
        formScroller.refresh();
    }
    clickfocus = false;
    var scrollContainer = $('#' + ccma.UI.ActiveScreenId);
    if (screen)
        scrollContainer = $('#' + screen);
    var scrollable = $('.scrollable', scrollContainer);
    scrollable.each(function (i, scroll) {
        var scrollerHeight = ccma.UI.Layout.Screen.UsableHeight - $(scrollable).offset().top + $(scrollContainer).offset().top;
        //        console.log(ticks().toString(), $(scrollContainer).attr('id'), ":", ccma.UI.Layout.Screen.UsableHeight, "-", $(scrollable).offset().top, "+", $(scrollContainer).offset().top, "=", scrollerHeight);

        $(scroll).height(scrollerHeight);
        if (scroll.scroller == null) {
            scroll.scroller = new IScroll(scroll, iScrollOptions);

        }
        else {

            if (ccma.UI.ActiveScreenId != 'mass-update-div' || ccma.UI.ActiveScreenId != 'classcalculator-div') {
                if (!scrollto)
                    scroll.scroller.scrollTo(0, 0, true, false);
                scroll.scroller.refresh();
                if (!scrollto)
                    scroll.scroller.scrollTo(0, 0, true, false);
            }
            if (ccma.UI.ActiveScreenId == 'mass-update-div' || ccma.UI.ActiveScreenId == 'classcalculator-div') {
                if (scrollNeeded) {
                    scroll.scroller.scrollToElement(inputElementInFocus, "0.3S");
                    scroll.scroller.refresh();
                }
            }
        }
    });

}
*/

function toggleSortScreen() {
    if (listMode == 1) {
        listMode = 0;
    } else {
        listMode = 1;
    }
    ShowParcelList(false,selectedFilters);

}
function toggleWithBPP(){
		combinedView = !combinedView;
		combinedView ? listType = 1 : listType = 0
		combinedView ? $('.standalone-bpp').show() : $('.standalone-bpp').hide()
		//avoiding filter
		selectedFilters = 0;
		refreshSortScreen = true;
	    ShowParcelListWithNoSort();
	}
/*
function enableTextButtonEffects() {
    $('.text-button').unbind('touchstart');
    $('.text-button').bind('touchstart', function () {
        //        $(this).anim({
        //            fontWeight: 'bold'
        //        }, 200, 'linear', function () {
        //            alert('Done;');
        //            $(this).anim({
        //                fontWeight: 'normal'
        //            }, 200)
        //        });
    });
}*/
var hideKeyboard = function (noRefresh) {
    if (document.activeElement) document.activeElement.blur();
    $("input").blur();
    inputElementInFocus = '';
    if (!noRefresh) 
	    setTimeout('setScreenDimensions(null, true);', 250);
};

function recalculateDimensions(cancelScrollRefresh) {
    if (ccma.UI.ActiveScreenId != null) {
        var sHeight = $('.page-content').height() - $('header', $('#' + ccma.UI.ActiveScreenId)).height();
     	$( '.scrollable', $( '#' + ccma.UI.ActiveScreenId ) ).height( sHeight );
        $( '#jqt section' ).css( 'min-height', $( '.page-content' ).height() );
        //$( '.iosScroll', $( '#' + ccma.UI.ActiveScreenId ) ).height( sHeight );
        switch (ccma.UI.ActiveScreenId) {
            case "agreement":
                hideKeyboard();
                return;
            case "data-collection":
                $( '#dc-categories' ).height( sHeight );
                if (Math.abs(window.orientation) == 0 || Math.abs(window.orientation) == 180)
    				$('.data-entry-tab-content').css('padding-bottom','330px');
    			else
    				$('.data-entry-tab-content').css('padding-bottom','400px');
                /*if ( cancelScrollRefresh ){
	                $('#dc-form').css('Height', '');
	                $('#dc-form').height(sHeight);
                }*/
                $('.multigrid').height($('table', this).eq(0).height())
			  	//if (!cancelScrollRefresh && formScroller)
                //	formScroller.scrollTo(0, 0, 0);
                //           cancelScroll = true;
                break;
            case "sort-screen":
                //refreshScrollable();
                var h = sHeight - $( '.nbhd-profile-float' ).height(); 
                $( '#parcel-scroll' ).height( h );
                return;
            case "gis-map":
                var mc = document.getElementById('mapCanvas');
                $(mc).height(sHeight);
                $('#pictometry').height(sHeight);
                $('#nearmap').height(sHeight);
                $('#nearmapwms').height(sHeight);
                $('#woolpert').height(sHeight);
                $('#eagleView').height(sHeight);
                resizeMap();
                break;
            case "route-map":
                var mc = document.getElementById('routeMapCanvas');
                $(mc).height(sHeight);
                resizeMap();
                break;
            case "photos":
                var viewer = $('.viewer', $('#photos'));
                var acceptance = $('.acceptance', $('#photos'));
                var slide = $('.slide', $('#photos'));
                $(viewer).height(sHeight - 90);
                $('canvas', viewer).height(sHeight - 90);
                $('canvas', viewer).width($('.page-content').width());
                $(acceptance).width($('.page-content').width());
                positionImageInViewer(selectedThumb, currentViewer);
                break;
            case "digital-prc":
                //                sigmaSketchInPRC(activeParcel.SKETCH);

                break;
            case "county-page":
                $('iframe', $('#county-page')).height(sHeight);
                break;
            default:
                break;
        }
        /*if (formScroller)
            formScroller.refresh();
        if (cancelScrollRefresh)
            refreshScrollable();*/
        //refreshOnFocus();
    }
    $('#dc-form-contents section').each(function(i,x){
		if(x.style.display!='none'){    
		var txtarea = $('textarea',x);
			txtarea.each(function(j){
				if(txtarea[j].style.display!='none'){
			   		txtarea[j].style.cssText = 'height:auto';
					txtarea[j].style.cssText = 'height:' + txtarea[j].scrollHeight + 'px';
					iPad?txtarea[j].style.maxHeight = '148px':txtarea[j].style.maxHeight = '144px';	
				}
			});
			$("input[type='text']").keypress(function(event) {
    			if (event.keyCode == 13) {
        			event.preventDefault();
   				}
			});
		}		
	});
}

function loadMenu(screenId, callback) {

    var screen = document.getElementById(screenId);

    switch (screenId) {
        case "error":
        case "gpsloading":
        case "loading":
        case "login":
        case "progress":
        case "neighborhood":
        case "user-dashboard":
        case "agreement":
        case "viewagreement":
            if (callback) callback();
            return;
    }

    //transiting = true;
    var menus = screen.getElementsByClassName('screen-menu');
    if (menus.length == 0) {
        getData("SELECT * FROM ScreenMenu WHERE ScreenName = '" + screenId + "' ORDER BY CAST(Ordinal AS INTEGER)", null, function (menu) {
            var sketchesMenuIdResult = menu.filter(function (item) {
                return item.Key === 'sketches';
            });
            var sketchesMenuId = sketchesMenuIdResult.length > 0 ? sketchesMenuIdResult[0].Id : null;

            var menuDiv, mph;
            menuDiv = document.createElement("div");
            menuDiv.innerHTML = '<nav id="${Id}" class="screen-menu" action="_nav(this);${Action}"><div class="nav-icon ${Class}"></div><div class="nav-div"><a class="menulink">${Name}</a> <span>${Description}</span></div></nav>';
            menuEnd = document.createElement("div");
            menuEnd.className = 'clearfix';
            var fY;
            if ( fylEnabled && clientSettings.EnableCopyDataToFutureYearOption == '1' )  {
                 fY = {
                    Name: "Copy Data to Future Year",
                    Description: "Copy all data to the Future Year (create the property in the Future Year)",
                    Action: 'copyAllData()',
                    Class: "nav-copy-future-year"
                };
            }
            if (screenId == "synchronization") {
            	if (menu.length == 0)
	                menu = [{
	                    Name: "Update Application",
	                    Description: "Download and install program updates",
	                    Action: 'RunSync(1)',
	                    Class: "nav-sync nav-sync-app"
	                }];
                else if (menu.filter(function(m){ return m.Key == 'reset-app'; }).length > 0) {
                	var action = menu.filter(function(m){ return m.Key == 'reset-app'; })[0].Action;
                	menu.filter(function(m){ return m.Key == 'reset-app'; })[0].Action = 'clearAppState();' + action;
					if(location.href.indexOf('localhost')== -1 && location.href.indexOf('192.168')== -1){
						menu.splice(menu.findIndex(function(m){ return m.Key == 'sync-cama'; }),1);
					}
                }
          
            }
            else if ( screenId == "parcel-dashboard" && fY )
                menu.push( fY )
           
          //  if (clientSettings.GPSNavigationApp == 'Sygic'){
          //  	 menu.splice(menu.findIndex(function(m){ return m.Key == 'navigon-directions'; }),1);
          // }
          //  else if (clientSettings.GPSNavigationApp == 'Navigon'){
          //  	 menu.splice(menu.findIndex(function(m){ return m.Key == 'navigon-directions-sygic'; }),1);
          //   }
          // if (clientSettings.Waze == '1'){
          //  menu.splice(menu.findIndex(function(m){ return m.Key == 'navigon-directions'; }),1);
          //  	 menu.splice(menu.findIndex(function(m){ return m.Key == 'navigon-directions-sygic'; }),1);
          //}
            
            $(menuDiv).fillTemplate(menu);
            menuDiv.appendChild(menuEnd);

            //To insert the newline character.
            var tempMenuDiv = menuDiv.innerHTML.replace('&lt;br&gt;', '<br>');
            menuDiv.innerHTML = tempMenuDiv;

            mph = screen.getElementsByTagName('menu');
            if (mph.length > 0) {
                var parent = $(mph[0]).parent();
                parent[0].replaceChild(menuDiv, mph[0]);
            }
            var hideSketchTab = clientSettings["HideSketchTab"];
            if (screenId == "parcel-dashboard" && hideSketchTab == '1' && sketchesMenuId) {
                $('#' + sketchesMenuId).hide();  
            }
			if(screenId == "parcel-dashboard"){
            	var menulist = menu.filter(function(y){return y.HiddenCondition != null});
				menulist.forEach(function(x){ activeParcel.EvalValue(x.HiddenCondition) ? $('.screen-menu div.'+x.Class+'').parent().hide() : $('.screen-menu div.'+x.Class+'').parent().show() })
                if (clientSettings?.DisableCopyDataToFutureYearOption == '1' && $('.screen-menu div.nav-copy-future-year')[0]) {
                    $('.screen-menu div.nav-copy-future-year').parent().hide();
                }
            }
			if($('nav.screen-menu .nav-copy-imprv', screen).length > 0){
				for(let i=0; i<menu.filter(x => x.Class == 'nav-copy-imprv').length; i++) {
					let menuId = menu.filter(x => x.Class == 'nav-copy-imprv')[i].Id;
					if(menuId) {
					    let act = menu.filter(x => x.Id == menuId)[0].Action;
					    $('#'+menuId).attr('action', '_nav(this);'+act)
					}
				}
		    }   
            //var menuEvent = 'click touchend';
            $('nav.screen-menu', screen).unbind(touchClickEvent);
            $('nav.screen-menu', screen).bind(touchClickEvent, function (e) {
                e.preventDefault();
                datacollectionsscroll = true;
                if (!scrolling) {
                    if ($(this).attr('action') != null) 
                        window.eval($(this).attr('action'));
                    else alert('No action specified.');
				}                    
            });

            //transiting = false;
            if (callback) callback();
        });

    }
    else {
        //transiting = false;
        if (screenId == "parcel-dashboard") {
            getData("SELECT * FROM ScreenMenu WHERE ScreenName = '" + screenId + "' ORDER BY CAST(Ordinal AS INTEGER)", null, function (menu){
                if (menu?.length > 0) {
                    var sketchesMenuIdResult = menu.filter(function (item) {
                        return item.Key === 'sketches';
                    });
                    var sketchesMenuId = sketchesMenuIdResult.length > 0 ? sketchesMenuIdResult[0].Id : null;
                    if (clientSettings["HideSketchTab"] == '1' && sketchesMenuId) {
                        $('#' + sketchesMenuId).hide();
                    }
                }
                var menulist = menu.filter(function (y) { return y.HiddenCondition != null });
				menulist.forEach(function(x){ activeParcel.EvalValue(x.HiddenCondition) ? $('.screen-menu div.'+x.Class+'').parent().hide() : $('.screen-menu div.'+x.Class+'').parent().show() })

                if (clientSettings?.DisableCopyDataToFutureYearOption == '1' && $('.screen-menu div.nav-copy-future-year')[0]) {
                    $('.screen-menu div.nav-copy-future-year').parent().hide();
                }

                if (callback) callback();
	        });
        }
        else
        	if (callback) callback();
    }
}

function _showScreen(screenId, callback) {
    loadScreen(screen, callback);
}

function tl() {
    console.log((new Date()).getMilliseconds());
}

function _nav(nav) {
    var navParent = nav.parentNode;
    $('nav', navParent).removeClass('selected');
    appState.prevScreenId = appState.screenId;
    $(nav).addClass('selected');
}
//set copy progress
function setCopyProgress(now, total, text) {

    var width = (parseFloat(now / total)) * 100;
    width = (parseInt(width) > 10) ? parseInt(width) : 10;
    if (text) {
        setSplashProgress(text, width);
    }
    else {
        setSplashProgress('Copying data... (Please do not close this application when this process is in progress)', width);
    }
}
function openMapView(){
	if (activeParcel){
        activeParcel = null;
        appState.parcelId = -1;
        }
    if(isBPPUser && ccma.Session.BPPDownloadType == '1')
		listType = 1;
    if (onTransition()) return false;
    if (!canClickNow()) return false;
    showGISInteractive(function () {
        loadParcelsInMap();
    });
    if (OsMap && $('.heatmap-items').val() != "" && currentHeatMapFieldId != null && HeatLayer) {
        setLegend(currentHeatMapFieldId);
    }
}