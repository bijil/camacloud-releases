﻿//    DATA CLOUD SOLUTIONS, LLC ("COMPANY") CONFIDENTIAL 
//    Unpublished Copyright (c) 2010-2013 
//    DATA CLOUD SOLUTIONS, an Ohio Limited Liability Company, All Rights Reserved.

//    NOTICE: All information contained herein is, and remains the property of COMPANY.
//    The intellectual and technical concepts contained herein are proprietary to COMPANY
//    and may be covered by U.S. and Foreign Patents, patents in process, and are protected
//    by trade secret or copyright law. Dissemination of this information or reproduction
//    of this material is strictly forbidden unless prior written permission is obtained
//    from COMPANY. Access to the source code contained herein is hereby forbidden to
//    anyone except current COMPANY employees, managers or contractors who have executed
//    Confidentiality and Non-disclosure agreements explicitly covering such access.</p>

//    The copyright notice above does not evidence any actual or intended publication
//    or disclosure of this source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
//    DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
//    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
//    IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
//    OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
//    TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
//    ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

var IDB;
var ver = 1;
var objStores = [];
var store = "";
var keyfields = [];
var tableList = [];
var tableWithFields = {};
var currentVersion;
var CurrentSyncId;
var nbhdStatsId = null;
var removeMulParcel = false;

var tlSystem =
            [
                ['ScreenMenu', 'tquery', { t: 'ScreenMenu' }],
                ['FieldCategory', 'tquery', { t: 'FieldCategory' }],
                ['CategorySettings', 'tquery', { t: 'CategorySettings'}],
                ['FieldInputType', 'tquery', { t: 'FieldInputType' }],
                ['Field', 'tquery', { t: 'Field' }],
                ['FieldSettings', 'tquery', { t: 'FieldSettings' }],
                ['ImportSettings', 'tquery', { t: 'ImportSettings' }],
                ['SketchSettings', 'tquery', { t: 'SketchSettings' }],
                ['ClientValidation', 'tquery', { t: 'ClientValidation' }],
                ['FieldAlerts', 'tquery', { t: 'FieldAlertTypes' }],
                ['LookupValue', 'tquery', { t: 'LookupValue', p: 1 }], 
                ['*ld*', 'lookupdatatables', { pageIndex: 0, tableIndex: 0 }],           
                ['ParentChild', 'tquery', { t: 'ParentChildTable' }],
                ['TableKeys', 'tquery', { t: 'TableKeys' }],
                ['AggregateFieldSettings', 'tquery', { t: 'AggregateFieldSettings' }],
                ['UI_Heatmap', 'tquery', { t: 'heatmap' }],
                ['UI_HeatmapLookup', 'tquery', { t: 'heatmaplookup' }]
            ];



var tlData =
            [
                ['', 'parquery'],
                ['Parcel', 'myparcels'],
                ['*', 'parcelsubtables'],
                ['ParcelChanges', 'getparcelchanges'],
                ['MapPoints', 'parcelmap'],
                ['Images', 'parcelimages'],
				['Comparables', 'comparables'],
                ['ParcelLinks', 'parcellinks'],
                ['Neighborhood', 'tquery', { t: 'Neighborhood' }],
                ['Streets', 'tquery', { t: 'Streets' }],
                ['StreetMapPoints', 'tquery', { t: 'StreetMapPoints' }],
                ['ParcelGISPoints' , 'parcelgispoints'],
                ['UI_Heatmap', 'tquery', { t: 'heatmap' }],
                ['UI_HeatmapLookup', 'tquery', { t: 'heatmaplookup' }],
                ['NbhdStats', 'nbhdstats'],
                ['RPLinkedBPPs',  'rplinkedbpps']
            ];

var tlQuickData =
            [
                ['', 'parquery'],
                ['Parcel', 'myparcels'],
                ['*', 'parcelsubtables'],
                ['Neighborhood', 'tquery', { t: 'Neighborhood' }],
                ['NbhdStats', 'nbhdstats']
            ];

var tlParcelsOnly =
            [
                ['', 'parquery'],
                ['Parcel', 'myparcels']
            ];

function LoginOnAgree(btn){
    //clickLock(btn)
    $(btn).prop('disabled', true);
    doLogin(function () {
        PostLoginStartup();
    });
}

function PostLoginStartup() {
    ccma.Session.IsLoggedIn = true;
    initLocalDatabase(function () {
        log('Checking if LookupValue is available');
        db.transaction(function (x) {
            var applicationTestSql = "SELECT fc.*, CASE WHEN fc.ParentCategoryId IS NULL THEN '' ELSE 'SubCat' END IsSubCat FROM FieldCategory fc ORDER BY fc.Ordinal * 1 ";
            x.executeSql(applicationTestSql, [], function (x, results) {
            	x.executeSql("SELECT * FROM TableKeys", [], function (x, results) {
	                log('Application tables exist!');
	                x.executeSql('SELECT * FROM NbhdStats', [], function (x, parcels) {
	                    x.executeSql('SELECT * FROM Parcel WHERE IsComparable = 0', [], function (x, parcels) {
	                        if (parcels.rows.length > 0) {
                        		if(getCookie('PrevURL') && getCookie('PrevURL') !== "" && getCookie('PrevURL').toLowerCase().indexOf(window.location.hostname.toLowerCase()) == -1){
							log('Environment Changed, do full sync!');
							startApplication(DownloadData, { skipDownload: false, syncAllData: true });
						} else{
		                            log('Starting application at least one parcel.');
		                            startApplication();
	                            }
	                        }
	                        else {
	                        	if(getCookie('PrevURL') && getCookie('PrevURL') !== "" && getCookie('PrevURL').toLowerCase().indexOf(window.location.hostname.toLowerCase()) == -1){
							log('Environment Changed, do full sync!');
							startApplication(DownloadData, { skipDownload: false, syncAllData: true });
						} else{
		                            log('Not enough parcels, downloading CAMA data.');
		                            startApplication(DownloadAppData);
	                            }
	                        }
	                    }, function (x, e) {
	                        log('Parcel table does not exist, downloading CAMA data.');
	                        startApplication(DownloadAppData);
	                    });
				}, function (tx, ex) {
	                log('Application tables are missing, do full sync!');
	                startApplication(DownloadData, { skipDownload: false, syncAllData: true });
	            });
				}, function (tx, ex) {
	                log('Application tables are missing, do full sync!');
	                startApplication(DownloadData, { skipDownload: false, syncAllData: true });
	            });

            }, function (tx, ex) {
                log('Application tables are missing, do full sync!');
                startApplication(DownloadData, { skipDownload: false, syncAllData: true });
            });
        });
        // });
    });
}

function checkNetwork(success, failure){
	var xhr = new XMLHttpRequest();
    xhr.onload = function(){
        success && success();
    }
    xhr.onerror = function(){
		syncInProgress = false;
		uploadInProgress = false;
        failure && failure();
    }
    xhr.open("GET","/favicon.ico",true);
    xhr.send();
}

var syncProgressList = [];
var syncPageCounter = {};
var jobAttemptCounter = {};
var partCounter = 0;
var noOfPages = 1;
var totalParts = 0;

function calculateProgress(tableDownloadList, downloadCount) {
    if (totalParts == 0) {
        var prg = parseInt((downloadCount - tableDownloadList.length) / downloadCount * 100);
        log("Percentage completed: " + prg + "% (" + tableDownloadList.length + "/" + downloadCount + ")");
        setProgress(prg);
    } else {
        var prg = parseInt((partCounter) / totalParts * 100);
        if (prg > 100) prg = 100; //quick fix for progress loader.. need to recheck counter part
        log("Percentage completed: " + prg + "% (" + partCounter + "/" + totalParts + ")");
        setProgress(prg);
    }
}
function syncTables(jobtitle, tableDownloadList, downloadCount, callback, tableDef) {
    var tdef = null, skipProgress = false;
    if (tableDef == null) {
        tdef = tableDownloadList.pop();
    }
    else {
        tdef = tableDef;
    }
    if (tdef == null) {
        log('Reached here..');
        if (callback) callback();
    }
    else {

        var params = tdef[2], tableIndex, pageIndex;
        var tableName = tdef[0];
        if (params == null)
            params = {};
        if (tableName != '') {
            if (syncPageCounter[tableName] == null) {
                syncPageCounter[tableName] = 1;
            } else {
                syncPageCounter[tableName] += 1;
            }
            params["page"] = syncPageCounter[tableName];
        } else {
            params["page"] = 1;
        }
        if (tableName == 'Images')
            params["imageLimit"] = imageLimit;
        if (tdef[1] == 'parquery')
            params["downloadLimit"] = downloadLimit;
        var jobName = 'sync-' + tableName + params["page"];
        if (jobAttemptCounter[jobName] == null) {
            jobAttemptCounter[jobName] = 1;
        } else {
            jobAttemptCounter[jobName] += 1;
        }
        if (tableName == "*ld*") {
        	pageIndex = params.pageIndex;
        	tableIndex = params.tableIndex;
        }

        if (tableName == "Parcel" && nbhdStatsId) {
            params["nbhdStatsId"] = nbhdStatsId;
        }

        if (tableName == "Parcel") removeMulParcel = false;

        removeMultipleParcels(tableName, () => {
            $$$(tdef[1], params, function (data) {
                var processor = _syncTable;
                if ((tableName == "*") || (tableName == "*ld*")) {
                    processor = _syncSubTables;
                }
                if (tableName == "*ld*") {
                    if (!data.status) {
                        skipProgress = true;
                        if (data.length == 2000)
                            tableDownloadList.push(['*ld*', 'lookupdatatables', { pageIndex: ++pageIndex, tableIndex: tableIndex }])
                        else tableDownloadList.push(['*ld*', 'lookupdatatables', { pageIndex: 0, tableIndex: ++tableIndex }])
                    }
                    else if (data.status == 'completed') { skipProgress = false; partCounter += 1; }
                }
                if (tableName == 'Images') processor = IDB_syncTable;
                if ((data.length != undefined) && (tableName != '')) {
                    if (!skipProgress) partCounter += 1;
                    if ((syncPageCounter[tableName] <= noOfPages) || (tableName == 'LookupValue') || (tableName == '*ld*')) {
                        //setProgressLabel(jobtitle + " (" + partCounter + ")");
                        var paged = false;
                        if ((tableName == "Parcel") || (tableName == "ParcelChanges") || (tableName == "MapPoints") || (tableName == "*") || (tableName == "Images") || (tableName == "Sketches") || (tableName == "SketchVectors") || (tableName == "Comparables") || (tableName == "ParcelLinks")) {
                            paged = true;
                        } else {
                            tdef = null;
                        }

                        if (data.length == 0) {
                            calculateProgress(tableDownloadList, downloadCount);
                            syncTables(jobtitle, tableDownloadList, downloadCount, callback, tdef);
                        } else {
                            if (data.length > 0 && tableName == 'LookupValue') {
                                tableDownloadList.push(['LookupValue', 'tquery', { t: 'LookupValue', p: params["page"] + 1 }])
                            }

                            let checkComparableParcelMultipleTIme = function (cpback) {
                                if (!isComparableScreenMenu) {
                                    if (cpback) cpback();
                                    return;
                                }
                                /*
                                if (tableName == "Parcel") {
                                    let cppls = [], cpplss = [];
                                    data.forEach((dp, kx) => {
                                        if (dp.IsComparable == 1) {
                                            let plll = cppls.filter((x) => { return x == dp.Id; })[0];
                                            if (plll) {
                                                data.slice(kx, 1);
                                            } else {
                                                cppls.push(dp.Id);
                                            }
                                        }
                                        else if (dp.IsComparable == 0) {
                                            let plll = cpplss.filter((x) => { return x == dp.Id; })[0];
                                            if (plll) {
                                                data.slice(kx, 1);
                                            } else {
                                                cpplss.push(dp.Id);
                                            }
                                        }
                                    });
                                }
                                if (tableName == "Parcel" && syncProgressList.indexOf(tableName) > -1) {

                                    let cpPacls = data.filter((xp) => { return xp.IsComparable == 1 && xp.Id }), pidStr = '';
                                    if (tableName == "Parcel" && cpPacls.length == 0) {
                                        if (cpback) cpback();
                                    }
                                    else {
                                        pidStr = cpPacls.map((y) => { return y.Id }).toString();
                                        getData("SELECT Id FROM Parcel WHERE IsComparable = 1 AND Id in (" + pidStr + ")", [], (pcList) => {
                                            data = data.filter((xp) => { return !pcList.filter((zx) => { return zx.Id == xp.Id })[0] });
                                            cpPacls = data.filter((xp) => { return xp.IsComparable == 0 && xp.Id });
                                            pidStr = cpPacls.map((y) => { return y.Id }).toString();
                                            getData("SELECT Id FROM Parcel WHERE IsComparable = 0 AND Id in (" + pidStr + ")", [], (pcList) => {
                                                data = data.filter((xp) => { return !pcList.filter((zx) => { return zx.Id == xp.Id })[0] });
                                                if (cpback) cpback();
                                            }, {}, true, () => { if (cpback) cpback(); });
                                        }, {}, true, () => { if (cpback) cpback(); });
                                    }
                                }*/
                                if (tableName == "*") {
                                    let auxTblList = {}, auxTblArr = [];
                                    data.forEach((dp, kx) => {
                                        let dTabName = dp.DataSourceName;
                                        if (!auxTblList[dTabName]) auxTblList[dTabName] = [];
                                        let plll = auxTblList[dTabName].filter((x) => { return x == dp.ROWUID; })[0];
                                        if (plll) {
                                            data.slice(kx, 1);
                                        } else {
                                            auxTblList[dTabName].push(dp.ROWUID);
                                        }
                                    });
                                    for (kx in auxTblList) {
                                        auxTblArr.push({ tbName: kx, roudis: auxTblList[kx].toString() });
                                    }

                                    let checkAuxMultiple = function (auxTblArrs) {
                                        if (auxTblArrs == 0) {
                                            if (cpback) cpback(); return;
                                        }
                                        let auxTblAr = auxTblArrs.pop();
                                        getData("SELECT ROWUID FROM " + auxTblAr.tbName + " WHERE ROWUID in (" + auxTblAr.roudis + ")", [], (pcList) => {
                                            if (syncProgressList.indexOf(auxTblAr.tbName) > -1)
                                                data = data.filter((xp) => { return !pcList.filter((zx) => { return zx.ROWUID == xp.ROWUID && xp.DataSourceName == auxTblAr.tbName })[0] });
                                            checkAuxMultiple(auxTblArrs);
                                        }, {}, true, () => {
                                            checkAuxMultiple(auxTblArrs);
                                        });
                                    }
                                    checkAuxMultiple(auxTblArr);
                                }
                                else if (cpback) cpback(); return;
                            }

                            checkComparableParcelMultipleTIme(() => {
                                processor(tableName, data, function () {
                                    if (!skipProgress) calculateProgress(tableDownloadList, downloadCount);
                                    syncTables(jobtitle, tableDownloadList, downloadCount, callback, tdef);
                                });
                            });
                        }
                    } else {
                        calculateProgress(tableDownloadList, downloadCount);

                        syncTables(jobtitle, tableDownloadList, downloadCount, callback);
                    }
                }
                else {

                    if (data.Pages) {
                        noOfPages = data.Pages;
                        totalParts = 7 * (noOfPages + 1) + 2 + 2;
                    }
                    syncTables(jobtitle, tableDownloadList, downloadCount, callback);
                }

            }, function (req, err, msg) {
                if (tableName == "*") {
                    syncTables(jobtitle, tableDownloadList, downloadCount, callback);
                } else {
                    if (jobAttemptCounter[jobName] < 2) {
                        log('Re-attempting job ' + jobName);
                        if (tableDef == null) {
                            tableDownloadList.push(tdef);
                        }

                        if (tableName != '') {
                            syncPageCounter[tableName] -= 1
                            if (syncPageCounter[tableName] == 0) {
                                syncPageCounter[tableName] = null;
                            }
                        }

                        syncTables(jobtitle, tableDownloadList, downloadCount, callback, tableDef);
                    }
                    else {
                        console.log(err);

                        messageBox('Data synchronization failed!');
                        syncNbhd = [];
                        refreshNbhdProfile = true;
                        currentNbhd = null;
                        showSortScreen();
                    }
                }
            });
        });
    }
}

function notifySyncCompletion(type, callback) {
    let dt = { type: type };
    if (nbhdStatsId) dt.nbhdStatsId = nbhdStatsId;
    $$$('synccomplete', dt, function () {
        if (callback) callback();
    });
}

function afterDownloadData(callback) {
    //setSplashProgress('Merging local changes ...', 80);
    dataDownloadingStarted = false;
    applyParcelChanges(function () {
        deleteEmptyImages(function () {
            localStorage.setItem('last-cama-update', ticks());
            prod.LastCAMAUpdate = new Date();
            createParcelView(function () {
                setProgressLabel('Processing map and routes ...');
                calculateParcelCenters(function () {
                    log('Computing route for priorities and alerts.');
                    hasParcels = true;
                    ComputePriorityAndAlertRoutes(callback);
                });
            }, true);
        });
    });
}
var dataDownloadingStarted = false;
function dropDataBaseifNoChanges(callBack){
    IDB_getData( 'Images', ['Synced'], ['='], [0], [], function ( im ){
        getData( "SELECT * FROM ParcelChanges WHERE (Synced = '0' OR Synced = 0)", [], function ( pci ) {
            if (im == 0 && pci.length==0)
                dropDatabase( function () {
                   if(callBack) callBack();
                } )
            else
                if ( callBack ) callBack();
        } )
    })
}
function DownloadData(callback, options) {
    confirmUpdation();
    clearMapStore();
    localStorage.removeItem("UpdateScreenMenu");
    selectNbhd(function () {
        if (dataDownloadingStarted)
            return;
        ccma.UI.SortOrderType = '1';
        dataDownloadingStarted = true;
        beforeDownloadList();
        if (options.skipDownload) {
            if (callback) callback();
            return;
        }
        DownloadList(tlSystem, function () {
           menuRename(function(){
            if (options.syncAllData) {
                dropDataBaseifNoChanges(function () {
                    initLocalIDBdatabase(ldbname, function () {
                        createNbhdDownloadLog(() => { 
                            DownloadList(tlData, function () {
                                updateForceUpdate(1, () => {
                                    localStorage.forceUpdateNbhd = false;
                                    $('.nbhd-cancel').removeAttr('disabled');
                                    notifySyncCompletion('All', function () {
                                        nbhdStatsId = null;
                                        getLicenseAgreement(function () {
                                            afterDownloadData(callback);
                                        });
                                    });
                                });

                            }, 'Downloading CAMA data ...');
                        });
                    });
                });
            }
            else {
                if ( callback ) callback();
            }
            });
        }, 'Downloading system data ...');
    });

}

function createNbhdDownloadLog(callback) {
    $$$('createnbhddownloadlog/', {}, (res) => {
        nbhdStatsId = res.newId; if (callback) callback();
    }, () => {
        nbhdStatsId = null; if (callback) callback();
    });
}


function menuRename(callback){
	if(location.href.indexOf('localhost')== -1 && location.href.indexOf('192.168')== -1){
		getData("UPDATE ScreenMenu SET Name = 'Update Data & Assignment Groups', Description = 'Download program updates and updated CAMA assignments / data.<br>Select a different Assignment Group from the drop-down to switch Assignment Groups.' WHERE [Key] ='sync-all'", [], function(){
			var mDiv = document.createElement("menu");
			var mParent = $("#synchronization .scrollable").children("div");
			if(mParent.length > 0){
				mParent[0].replaceWith(mDiv);
			}
			localStorage.setItem('UpdateScreenMenu',1);
		if(callback) callback();
		});
	}
	else if (callback) callback();
}

function DownloadAppData(callback) {
    clearMapStore();
    selectNbhd( function () {
        if ( dataDownloadingStarted )
            return;
        dataDownloadingStarted = true;
        beforeDownloadList();
        ClearTablesFromList(tlData, function () {
        	    dropDataBaseifNoChanges( function (){
                    initLocalIDBdatabase(ldbname, function () {
                        createNbhdDownloadLog(() => {
                            DownloadList(tlData, function () {
                                updateForceUpdate(2, () => {
                                    localStorage.forceUpdateNbhd = false;
                                    $('.nbhd-cancel').removeAttr('disabled');
                                    notifySyncCompletion('CAMA', function () {
                                        nbhdStatsId = null;
                                        afterDownloadData(callback);
                                    });
                                });
                            }, 'Downloading CAMA data ...');
                        });
		            });
	         });  
        });

    });
}


function DownloadSystemData(callback) {
    if (dataDownloadingStarted)
        return;
    dataDownloadingStarted = true;
    localStorage.removeItem("UpdateScreenMenu");
    confirmUpdation();
    beforeDownloadList();
    ClearTablesFromList(tlSystem, function () {
        DownloadList(tlSystem, function () {
            menuRename(function () {
                updateForceUpdate(3, () => {
                    notifySyncCompletion('System', function () {
                        dataDownloadingStarted = false;
                        getLicenseAgreement(function () {
                            if (callback) callback();
                        });
                    });
                });
            });
        }, 'Downloading system data ...');
    });
}

function DownloadRefreshData(callback) {
    selectNbhd(function () {
        ClearTablesFromList(tlQuickData, function () {
            DownloadList(tlQuickData, function () {
                afterDownloadData(callback);
            }, 'Downloading CAMA data ...');
        });

    });
}

function DownloadParcelsOnly(callback) {
    selectNbhd(function () {
        ClearTablesFromList(tlParcelsOnly, function () {
            DownloadList(tlParcelsOnly, function () {
                removeMulParcel = false;
                removeMultipleParcels('*', () => {
                    createParcelView(function () {
                        console.log('Ready');
                    }, true);
                });
            }, 'Downloading CAMA data ...');
        });

    });
}

function ClearTablesFromList(list, callback) {
    _clearTablesFromlist(list.slice(0), callback);
}

function _clearTablesFromlist(list, callback) {
    if (list.length == 0) {
        if (callback) callback();
        return;
    }
    var tdef = list.pop();
    var tableName = tdef[0];
    db.transaction(function (tx) {
        log('Dropping table' + tableName);
        if ((tableName == '') || (tableName == '*') || (tableName == '*ld*')) {
            _clearTablesFromlist(list, callback);
        } else {
            if (tableName == "ParcelChanges") {
                tx.executeSql("DELETE FROM " + tableName + " WHERE Synced = '1'", [], function (tx, res) {
                    _clearTablesFromlist(list, callback);
                });
            }
            else if (tableName == "Images") {
                tx.executeSql("DELETE FROM " + tableName + " WHERE Synced = '1'", [], function (tx, res) {
                    _clearTablesFromlist(list, callback);
                });
            }
            else {
                tx.executeSql('DROP TABLE IF EXISTS ' + tableName, [], function (tx, res) {
                    _clearTablesFromlist(list, callback);
                });
            }
        }

    });
}

function beforeDownloadList() {
    loadScreen('progress');
    setProgress(0);
    showProgress();
    setProgressLabel('Please wait ...');
}

var isComparableScreenMenu = false;

function DownloadList(list, callback, title) {
    syncProgressList = [];
    syncPageCounter = {};
    jobAttemptCounter = {};
    partCounter = 0;
    totalParts = 0;
    noOfPages = 1;
    if (title == null)
        title = 'Downloading ...';
    loadScreen('progress');
    setProgress(0);
    showProgress();
    setProgressLabel(title);
    lookupDownloads = [];
    var copylist = list.slice(0);
    if (title == 'Downloading CAMA data ...') {
        getData("SELECT * FROM ScreenMenu WHERE [KEY]  = 'comparables'", [], (res) => {
            if (res.length > 0) isComparableScreenMenu = true;
            syncTables(title, copylist.reverse(), copylist.length, callback);
        });
    }
    else
        syncTables(title, copylist.reverse(), copylist.length, callback);
}

function SyncParcelChanges() {

}

var uploadInProgress = false;

var syncTimer;
var nextSyncAfter;
function _restartSync(afterMins) {
    uploadInProgress = false;
    if (afterMins == null)
        afterMins = 0.5;
    nextSyncAfter = afterMins;
    syncTimer = window.setTimeout('UploadData();', afterMins * 60 * 1000);
}

function UploadData() {
    if (quickDebug || !ccma.Session.IsLoggedIn) {
        return false;
    }
    if (syncTimer) {
        window.clearTimeout(syncTimer);
        syncTimer = null;
    }
    if (syncInProgress) {
        _restartSync(2);
        return false;
    }
    
	checkNetwork(()=>{
        uploadInProgress = true;
        checkForceUpdate(function() {
            checkSchemaUpdate(function() {
                db.transaction(function (x) {
                    var geoDat = "";
                    var delTime = "";
                    var upldcallTimer = window.setTimeout(function () { syncInProgress = false; _restartSync(3); }, 180000);
                    x.executeSql("SELECT * FROM Location WHERE (Synced = 0 OR Synced = '0') ORDER BY UpdateTime", [], function (tx, results) {
                        if (upldcallTimer) {
                            window.clearTimeout(upldcallTimer);
                            upldcallTimer = null;
                        }
                        if (results.rows.length > 0) {
                            for (i = 0; i < results.rows.length; i++) {
                                var res = results.rows.item(i);
                                if (geoDat != '') {
                                    geoDat += "|";
                                    delTime += ',';
                                }
                                geoDat += res.UpdateTime.toString() + ',' + res.Latitude + "," + res.Longitude + "," + res.Accuracy;
                                delTime += res.UpdateTime.toString();
                            }
                            debug_mode_log('Updating GIS location');
                            ajaxRequest({
                                url: baseUrl + 'mobileassessor/gisupdate.jrq?zt=' + ticks(),
                                dataType: 'json',
                                type: "POST",
                                data: {
                                    lastUpdated: localStorage.getItem('gpspos-ts'),
                                    lastLat: localStorage.getItem('gpspos-lat'),
                                    lastLng: localStorage.getItem('gpspos-lng'),
                                    lastAcc: localStorage.getItem('gpspos-acc'),
                                    geo: geoDat
                                },
                                success: function (data, stat) {
                                    if ((data.LoginStatus == "403") || (data.LoginStatus == "401")) {
                                        debug_mode_log('Logging out because login status is ' + data.LoginStatus + ' on ' + baseUrl + 'mobileassessor/gisupdate.jrq');
                                        LogoutErrorInsert('sl: uploaddata712');
                                        showLogin();
                                        //  uploadInProgress = false;
                                        return false;
                                    }
                                    if (data.LoginStatus == "423") {
                                        window.location.href = data.RedirectURL;
                                        //  uploadInProgress = false;
                                        return false;
                                    }

                                    db.transaction(function (dx) {
                                        dx.executeSql("DELETE FROM Location", [], function (tx, res) {
                                        }, function (tx, e) {
                                            messageBox(e.message);
                                        });
                                    });
                                    //UploadPhotos();
                                    UploadParcelChanges();
                                },
                                error: function (req, ex) {
                                    xlog(ex, true);
                                    _restartSync(3);
                                }
                            }); //update server
                        } else {
                            //UploadPhotos();
                            UploadParcelChanges();
                        }

                    }, function (tx, res) {
                        syncInProgress = false;
                        if (upldcallTimer) {
                            window.clearTimeout(upldcallTimer);
                            upldcallTimer = null;
                        }
                        _restartSync(3);
                    }); // select location query
                });             // outer transaction
            });
        });
	}, ()=>{
		_restartSync(3);
	});
    // if online
}  //function
var resetCount = 4;
function checkSchemaUpdate( exitCallback ) {
    resetCount+=1;
    if ( $( '.splash-screen' ).css( 'display' ) == 'none' && ! sketchopened && resetCount == 5 ) {
        if(localStorage.getItem('lastUpdatedCounter') == null)
			localStorage.setItem("lastUpdatedCounter", '0');
		var updatedCounter = parseInt(localStorage.getItem( "lastUpdatedCounter" ));
		checkNetwork(()=>{
			getData( "SELECT COUNT(*) as count FROM PARCELCHANGES WHERE (Synced = '0' OR Synced = 0)", [], function ( results ) {
                var data = localStorage.getItem( "macid" ) + '|' + ccma.Session.User + '|' + PendingImagesCount + '|' + results[0]["count"] + '|' + $( '.sync-message' ).text() + '|' + ( localStorage.getItem( "last-data-sync" ) || '' ) + '|' + window.location.href + ',' + navigator.platform + ',' + window.navigator.standalone;

                ajaxRequest( {
                    url: baseUrl + 'mobileassessor/fieldcatagoryupdation.jrq?zt=' + ticks(),
                    dataType: 'json',
                    type: 'POST',
                    data: {
                        data: data
                    },
                    success: function ( data ){
                        if ( data.message )  {
                            var DBUpdatedCounter = parseInt(data.message);
							if(DBUpdatedCounter > updatedCounter){
                                showAlert();
                                localStorage.setItem("lastUpdatedCounter", data.message);
                            }
                            else
                                $( '.update-box' ).hide();
                        }
                        if ( exitCallback ) exitCallback();
                    },
                    error: function ( req, ex ) { if ( exitCallback ) exitCallback(); }
                } );
            } )
		}, ()=>{
			if ( exitCallback ) exitCallback();
		});	
            resetCount = 0;
        }
    else
        if (exitCallback)  exitCallback(); 
}

function checkForceUpdate(eback) {
    if ($('.splash-screen').css('display') == 'none' && !sketchopened) {
        IDB_getData('UnsyncedImages', [], [], [], [], function (unsyncedCount) {
            getData("SELECT COUNT(*) as count FROM PARCELCHANGES WHERE (Synced = '0' OR Synced = 0)", [], function (results) {
                if (unsyncedCount?.length > 0 || results[0]?.count > 0) {
                    if (eback) eback();
                    return;
                }

                let lastForceCounter = localStorage.lastForceCounter ? localStorage.lastForceCounter : '0';

                ajaxRequest({
                    url: baseUrl + 'mobileassessor/getforceupdate.jrq?zt=' + ticks(),
                    dataType: 'json',
                    type: 'POST',
                    data: { lastForceCounter: lastForceCounter },
                    success: function (data) {
                        if (data.length > 0) {
                            uploadInProgress = false;
                            let sc = false, dc = false;
                            data.forEach((x) => {
                                if (x.UpdateType == "Update Application/Update System Data") sc = true;
                                else dc = true;
                            });

                            if (dc && sc) {
                                messageBox("Data & System update is availble. Please redownload the group!", () => {
                                    localStorage.forceUpdateNbhd = true;
                                    startApplication(DownloadData, { skipDownload: false, syncAllData: true });
                                    $('.nbhd-cancel').attr('disabled', 'disabled');
                                    return;
                                });
                            }
                            else if (dc) {
                                messageBox("Data update is availble. Please redownload the group!", () => {
                                    startApplication(DownloadAppData);
                                    $('.nbhd-cancel').attr('disabled', 'disabled');
                                    return;
                                });
                            }
                            else if (sc) {
                                messageBox("Settings update is availble!", () => {
                                    startApplication(DownloadSystemData);
                                    return;
                                });
                            }
                            else {
                                uploadInProgress = true;
                                if (eback) eback();
                            }
                        }
                        else if (eback) eback();
                    },
                    error: function (req, ex) { if (eback) eback();}
                });

            });
        });
    }
    else if (eback) eback();
}

function updateForceUpdate(tp, cback) {
    let lastForceCounter = localStorage.lastForceCounter ? localStorage.lastForceCounter : '0';

    ajaxRequest({
        url: baseUrl + 'mobileassessor/updateforceupdate.jrq?zt=' + ticks(),
        dataType: 'json',
        type: 'POST',
        data: { lastForceCounter: lastForceCounter, type: tp },
        success: function (data) {
            if (data.message) {
                if (parseInt(data.message) > parseInt(lastForceCounter)) localStorage.lastForceCounter = parseInt(data.message);
            }
            if (cback) cback();
        },
        error: function (req, ex) { if (cback) cback(); }
    });
}

function closeAlert() {
    resetCount = -2;
    $(".update-box").hide();
}

function showAlert() {
    $(".update-box").show();
}

function confirmUpdation() {
    $('.update-box').hide();
    ajaxRequest({
        url: baseUrl + 'mobileassessor/fieldcatagoryupdation.jrq?zt=' + ticks(),
        dataType: 'json',
        type: 'POST',
        data:{data: ""},
        success: function ( data ){
            if ( data.message )
                localStorage.setItem("lastUpdatedCounter", data.message);
            else{
                if(localStorage.getItem('lastUpdatedCounter') == null)
					localStorage.setItem("lastUpdatedCounter", '0');
            }
        },
        error: function ( req, ex ) {
	        if(localStorage.getItem('lastUpdatedCounter') == null)
				localStorage.setItem("lastUpdatedCounter", '0'); 
		}
    });
}

function updateSchema() {
    $(".update-box").hide();
    if(sketchApp)
    	sketchApp.close();
   
   	if($(".overlay-photo-properties").css('display')!="none")
   			photos.hideProperties();
   	if(document.getElementById("_winCameraDiv").style.display != "none")
			camCapture.close();
	if($(".overlay-photo-viewer").css('display')!="none")
   			photos.hide();
   			
    RunSync(1);
}


function UploadParcelChanges() {
    _uploadParcelChanges(function () {
        _restartSync();
    }, function (t) {
        _restartSync(t || 1);
    });
}


function _uploadParcelChanges(successCallback, failedCallback, exitCallback, skipPhotoRelatedChanges) {
    if (quickDebug || isCopying || !ccma.Session.IsLoggedIn) {
        if (exitCallback) exitCallback();
        else
            _restartSync();
        return;
    }

    _uploadPhotos(function () {
		checkNetwork(()=>{
        	debug_mode_log('Uploading PCI');
            var filter = "";
            if (skipPhotoRelatedChanges == true)
                filter = " AND ((Field !='PhotoMetaData' AND Field!='MarkAsPrimaryPhoto') OR Field IS NULL) "
            var delayFilter = 'ChangedTime < ' + ((new Date()).getTime() - (60 * 100))            // Not sending changes which happened in last 3 seconds. 
            //   var photoMetaDataFilter = "((Field = 'PhotoMetaData' AND (ParcelId IN (SELECT LocalId FROM Images WHERE Synced = '1') OR ParcelId IN (SELECT Id FROM Images WHERE Synced = '1'))) OR ((Field != 'PhotoMetaData' AND Field != 'MarkAsPrimaryPhoto') OR Field IS NULL) OR (Field = 'MarkAsPrimaryPhoto' AND NewValue IN (SELECT LocalId FROM Images WHERE Synced = '1') OR NewValue IN (SELECT Id FROM Images WHERE Synced = '1')))"
            var syncStatusFilter = "(Synced = '0' OR Synced = 0)"
            var pcQuery = "SELECT * FROM ParcelChanges WHERE " + delayFilter + " AND " + syncStatusFilter;
            getData(pcQuery + " ORDER BY ChangedTime LIMIT 50", [], function (results) {
                if (results.length > 0) {
                    var dat = ""
                    var maxChangeTime = 0;
                    for (i = 0; i < results.length; i++) {
                        var ch = results[i];
                        //dat += ch.ChangedTime + "|" + ch.ParcelId + "|" + ch.FieldId + "|" + (ch.OldValue || '') + "|" + ((ch.NewValue).toString() || '') + "|" + (ch.AuxRowId || '') + "\n";
                        var oldValue = btoa(encodeURIComponent((ch.OldValue != null && ch.OldValue != undefined)? ch.OldValue: ''));
                        var newValue = btoa(encodeURIComponent((ch.NewValue != null && ch.NewValue != undefined)? ch.NewValue: ''));
                        var auxRowID = (ch.AuxRowId || '');
                        let rlat = ch['Latitude']? ch['Latitude']: 0;
                        let rlng = ch['Longitude']? ch['Longitude']: 0;
                        dat += ch.ChangedTime + "|" + ch.ParcelId + "|" + ch.FieldId + "|" + oldValue + "|" + newValue + "|" + auxRowID + "|!|" + rlat + "|" + rlng + "\n"; //FD_9349 change update latlng values to each PCI to get the exact mac location.
                        //dat += ch.ChangedTime + "|" + ch.ParcelId + "|" + ch.FieldId + "|" + oldValue + "|" + newValue + "|" + auxRowID + "|!\n";
                        if (ch.ChangedTime > maxChangeTime)
                            maxChangeTime = ch.ChangedTime;
                    }
                    ajaxRequest({
                        url: baseUrl + 'mobileassessor/parcelchanges.jrq?zt=' + ticks(),
                        dataType: 'json',
                        type: "POST",
                        data: {
                            dat: dat
                        },
                        success: function ( data, stat )
                        {
                            if (data.LoginStatus == '401') {
                                LogoutErrorInsert("page: pcisync LoginStatus");
                                Logout()
                            }
                            if (data.status) {
                                if (data.status == "OK") {
                                    localStorage.setItem('last-data-sync', ticks());
                                    db.transaction(function (dx) {
                                        dx.executeSql("UPDATE ParcelChanges SET Synced = '1' WHERE ChangedTime <= ? AND " + syncStatusFilter, [maxChangeTime], function (tx, res) {
                                        }, function (tx, e) {
                                            log(e);
                                        });
                                    });
                                    getData("SELECT * FROM ParcelChanges WHERE " + delayFilter + " AND " + syncStatusFilter, [], function (results) {
                                        if (results.length > 0)
                                            _uploadParcelChanges(successCallback, failedCallback, exitCallback, skipPhotoRelatedChanges)
                                        else {
                                            updateSqlLiteDataInIndexedDB(function () {
                                                debug_mode_log('Sync process completed');
                                                if (exitCallback) { uploadInProgress = false; exitCallback(); }
                                                else
                                                    if (successCallback) successCallback();
                                            });
                                        }
                                    });
                                } else {
                                    error(data.message);
                                    uploadInProgress = false;
                                    if (exitCallback) { exitCallback(); }
                                    else
                                        if (failedCallback) failedCallback();
                                }
                            } else {
                                uploadInProgress = false;
                                debug_mode_log('Error while uploading PCI');
                                debug_mode_log(data);
                                if (exitCallback) { exitCallback(); }
                                else
                                    if (failedCallback) failedCallback();
                            }
                        },
                        error: function (req, stat, err) {
                            error(err);
                            showSyncStatus(err);
                            uploadInProgress = false;
                            if (exitCallback) { exitCallback(); }
                            else
                                if (failedCallback) failedCallback();
                        }
                    });
                } else {
                    updateSqlLiteDataInIndexedDB(function () {
                        debug_mode_log('Sync process completed');
                        if (exitCallback) { uploadInProgress = false; exitCallback(); }
                        else
                            if (successCallback) successCallback();
                    });
                }
            });			
		}, ()=>{
            uploadInProgress = false;
            if (exitCallback) { exitCallback(); }
            else
                if (failedCallback) failedCallback();			
		});
    }, function (t) {
        uploadInProgress = false;
        if (exitCallback) { exitCallback(); }
        else
            if (failedCallback) failedCallback(t);
    })
}

function _uploadPhotos(successCallback, failedCallback) {
    if (quickDebug || isCopying) {
        _restartSync();
    }

    if (pstream) {
        pstream.syncNext(function () {
            _uploadPhotos(successCallback, failedCallback);
        }, function (t) {
            uploadInProgress = false;
            if (failedCallback) failedCallback(t);
        }, function () {
            uploadInProgress = false;
            if (successCallback) successCallback();
        });
    }
}


function RefreshNbhds() {
    refreshNeighborhoodsList(function(){
    	if(NbhdLoadStatus==false){
    		syncInProgress= false;
    		dataDownloadingStarted = false;
    		RunSync(1);
		}
    });
}

var syncInProgress = false;

function RunSync(type) {
    if (syncInProgress)
        return false;
    $(".update-box").hide();
    if (uploadInProgress) {
        messageBox('There are changes cached locally on the device that need to be synced off before you can update your data.<br/> Click "OK" to be re-directed to the Appraiser Dashboard. Once the screen shows 0 pending changes for sync, try again.', ["Ok", "Cancel"], function () {
          localStorage.setItem('appState-screenId','user-dashboard');
           window.location.reload();})
        return false;
    }

    syncInProgress = true;
    uploadInProgress = true;
    _uploadParcelChanges(null, null, function () {
        uploadInProgress = false;
        //clearAppState();
        if (type == 1 || type == 4)
            activeParcel = null;
        switch (type) {
            case 0:
                log('Downloading all data ...');
                startApplication(DownloadData, { skipDownload: false, syncAllData: true });
                break;
            case 1:
                log('Downloading system data ...');
                //                startApplication(DownloadData, { skipDownload: false, syncAllData: false });
                startApplication(DownloadSystemData);
                break;
            case 2:
                log('Downloading CAMA data ...');
                startApplication(DownloadAppData);
                break;
            case 4:
                log('Refreshing parcels ...');
                startApplication(DownloadRefreshData);
                break;
        }
    }, false);
}


function CancelSync() {
    dataDownloadingStarted = false;
    if (hasParcels) {
        $T.goBack();
        syncInProgress = false;

    } else {
        LogoutErrorInsert("page: CancelSync");
        Logout();
    }
}

ccma.Sync.dequeueChange = function (parcelId, auxRowId, fieldName, fieldId, oldValue, newValue, options, successCallback, failureCallback) {
    if (!options)
        options = {};
    var nParcelId = -1;
    if (!isNaN(parcelId))
        nParcelId = parseInt(parcelId);
    db.transaction(function (dx) {
        var cn = (fieldId == 'DeleteParcelPhoto' ? 'NewValue = ? ' : ' fieldId = ? ')
        if (fieldId == 'AUDIT') {
            if (successCallback) successCallback({ transaction: dx });
            return;
        }
        dx.executeSql('DELETE FROM ParcelChanges WHERE (ParcelId = ? OR ParcelId = ?) AND ' + cn + (auxRowId ? 'AND AuxRowId = ' + "'" + auxRowId + "'" : '' + (options.IsCommand ? ' AND OldValue = \'' + oldValue + '\'' : '')), [parcelId, nParcelId, fieldId], function (dx, results) {
            if (options.updateObject) {
                if (options.object) {
                    options.object[fieldName] = oldValue;
                }
            }
            if (options.updateData) {
                var updateSql = "";
                var sqlParams = [];
                var sourceField = fieldName;
                if (options.sourceField) {
                    sourceField = options.sourceField;
                }
                if (options.source) {
                    if (options.sourceKey) {
                        updateSql = 'UPDATE ' + options.source + ' SET [' + sourceField + '] = ? WHERE ' + options.sourceKey + ' = ' + options.sourceKeyValue;
                        sqlParams = [oldValue];
                    } else {
                        updateSql = 'UPDATE ' + options.source + ' SET [' + sourceField + '] = ? WHERE ROWUID = ' + auxRowId;
                        sqlParams = [oldValue];
                    }

                } else {
                    updateSql = 'UPDATE Parcel SET [' + sourceField + '] = ? WHERE Id = ' + parcelId;
                    sqlParams = [oldValue]
                }
                dx.executeSql(updateSql, sqlParams, function (dx, res) {
                    if (successCallback) successCallback({ transaction: dx });
                });
            } else {
                if (successCallback) successCallback({ transaction: dx });
            }
        },
        function (dx, e) {
            console.log(e.message);
            if (failureCallback) failureCallback();
        });
    });
}

ccma.Sync.enqueueParcelChange = function (parcelId, auxRowId, parentAuxRowId, action, fieldName, fieldId, oldValue, newValue, options, successCallback, failureCallback) {
    if (!action)
        action = "E";
    var utime = ticks();
    //    if (auxRowId)
    //        auxRowId = parseInt(auxRowId);
    //    if (parcelId)
    //        if (!isNaN(parcelId))
    //            parcelId = parseInt(parcelId);
   	
   	let rlatlng = getLatLngLocations();
   	let rlat = rlatlng.rlat, rlng = rlatlng.rlng;
   	
    if (!options)
        options = {};
    ccma.Sync.dequeueChange(parcelId, auxRowId, fieldName, fieldId, oldValue, newValue, { IsCommand: options.IsCommand }, function (data) {
        data.transaction.executeSql('INSERT INTO ParcelChanges (ParcelId, AuxRowId, ParentAuxRowId, Action, Field, FieldId, OldValue, NewValue, ChangedTime, Synced, Latitude, Longitude) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)',
                            [parcelId, auxRowId, parentAuxRowId, action, fieldName, fieldId, oldValue, newValue, utime, '0', rlat, rlng],
                            function (dx, results) {
                                if (options.updateObject) {
                                    if (options.object) {
                                        options.object[fieldName] = newValue;
                                    }
                                }
                                if (options.updateData) {
                                    var updateSql = "";
                                    var sqlParams = [];
                                    var sourceField = fieldName;
                                    if (sourceField.indexOf("#") > -1) {
                                        sourceField = "'" + sourceField + "'";
                                    }
                                    if (options.sourceField) {
                                        sourceField = options.sourceField;
                                    }
                                    if (options.source) {
                                    	if (options.source == 'Images')
                                    		todoDB.indexedDB.update('Images',[{ name: sourceField, value: newValue }], [(options.sourceKey? options.sourceKey: 'ROWUID')], ['='], [(options.sourceKey? options.sourceKeyValue: auxRowId)], [], function() {
                                    			if (successCallback) successCallback();
                                    		})
                                    	else {
	                                        if (options.sourceKey) {
	                                            updateSql = 'UPDATE ' + options.source + ' SET [' + sourceField + '] = ? WHERE ' + options.sourceKey + ' = \'' + options.sourceKeyValue + '\'';
	                                            sqlParams = [newValue];
	                                        } else {
	                                            updateSql = 'UPDATE ' + options.source + ' SET [' + sourceField + '] = ? WHERE ROWUID = ' + auxRowId;
	                                            sqlParams = [newValue];
	                                        }
                                        }

                                    } else {
                                        updateSql = 'UPDATE Parcel SET [' + sourceField + '] = ? WHERE Id = ' + parcelId;
                                        sqlParams = [newValue]
                                    }
                                    //console.log(updateSql, sqlParams);
                                    if (updateSql != '')
	                                    dx.executeSql(updateSql, sqlParams, function (dx, res) {
	                                        if (successCallback) successCallback();
	                                    });
                                }
								else if (options.updateSvData && options.updataSvDataObject && options.sourceFields) {
									let updateSql = "", sqlParams = [], sourceField = '', sfields = options.sourceFields;
									for (let s in sfields) {
										let sfield = sfields[s];
										sqlParams.push(options.updataSvDataObject[sfield])
										sourceField = sourceField + '[' + sfield + '] = ? ,';
										options.source[sfield] = options.updataSvDataObject[sfield];
									}
                                    sourceField = sourceField.slice(0, -1);
                                    updateSql = 'UPDATE Parcel SET '+ sourceField +' WHERE Id = ' + parcelId;
                                    if (updateSql != '') {
	                                    dx.executeSql(updateSql, sqlParams, function (dx, res) {
	                                        if (successCallback) successCallback();
	                                    });
	                                }
	                                else {
		                                if (successCallback) successCallback();
		                            }
								}
								else {
                                    if (successCallback) successCallback();
                                }

                            },
                            function (dx, e) {
                                console.error(e.message);
                                if (failureCallback) failureCallback();
                            }
                        );
    }, failureCallback);
    //        dx.executeSql('DELETE FROM ParcelChanges WHERE ParcelId = ? AND FieldId = ? ' + (auxRowId ? 'AND AuxRowId = ' + auxRowId : '' + (options.IsCommand ? ' AND OldValue = \'' + oldValue + '\'' : '')), [parcelId, fieldId], function (dx, results) {

    //        },
    //        function (dx, e) {
    //            log(e.message);
    //            if (failureCallback) failureCallback();
    //        });
}

ccma.Sync.enqueueParcelAttributeChange = function (parcelId, event, value, successCallback, failureCallback) {
    var p = activeParcel;
    if (p) {
        ccma.Sync.enqueueParcelChange(parcelId, null, null, null, event, event, '', value, { updateData: true, updateObject: true, object: p }, successCallback, failureCallback);
    } else {
        ccma.Sync.enqueueParcelChange(parcelId, null, null, null, event, event, '', value, { updateData: true, updateObject: false, object: null }, successCallback, failureCallback);
    }
}

ccma.Sync.enqueueEvent = function (parcelId, event, value, options, successCallback, failureCallback) {
    ccma.Sync.enqueueParcelChange(parcelId, null, null, null, event, event, '', value, options, successCallback, failureCallback);
}

ccma.Sync.enqueueCommand = function (objectId, command, option, value, options, successCallback, failureCallback) {
    if (options)
        options.IsCommand = true;
    else
        options = { IsCommand: true };
    ccma.Sync.enqueueParcelChange(objectId, null, null, null, command, command, option, value, options, successCallback, failureCallback);
}

function getLicenseAgreement(callback) {
    try {
        $$$( 'licenseagreement', { macid: localStorage.getItem( "macid" ) }, function ( d ){
            localStorage.setItem("AgreementContent", d[0].AgreementContent || "");
            ccma.UI.AgreementContent = d[0].AgreementContent || "";
            if (callback) callback();
        }, function () { ccma.UI.AgreementContent = "" });
    }
    catch (e) {
        console.log(e.message);
    }
}
if (localStorage.getItem("AgreementContent") == null)
    getLicenseAgreement();    // For Agreement Configuration
else
    ccma.UI.AgreementContent = localStorage.getItem("AgreementContent");

function terminateDevice() {
    var _c = ['MACAUTH', 'MACID'];
    var str = '', _url = ['camacloud.com', location.origin]
    _c.forEach(function (va) {
        _url.forEach(function (u) {
            document.cookie = va + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;domain=' + u + ';';
        });
    });

}

function showSyncStatus(message) {
	debug_mode_log(message)
    $('.sync-status-message').html(message);
}

function loadGoogleMap(){
       var js = document.createElement( "script" );
       js.type = "text/javascript";
       js.src = "static/js/lib/googlemaps.js";
       document.head.appendChild( js )
}

function getLatLngLocations () {
	let rlat = 0, rlng = 0, rlatlng = {};
   	if (geo && geo.location && geo.location.longitude && geo.location.latitude) {
   		rlat = geo.location.latitude;
   		rlng = geo.location.longitude;
   	}
   	else if (myLocation) {
   		rlat = myLocation.lat();
   		rlng = myLocation.lng();
   	}
   	
   	rlatlng = { rlat: rlat, rlng: rlng };
   	return rlatlng;
}

ccma.Sync.enqueueBulkParcelChanges = function (batches, successCallback, failureCallback) {
	let rlatlng = getLatLngLocations();
   	let rlat = rlatlng.rlat, rlng = rlatlng.rlng;
   	
    var processQueue = function(batches,execCall,finalCallback){
        if(batches.length == 0){ if(finalCallback) finalCallback(); }
        else{
            var batch = batches.splice(0,50);
            execCall(batch,function(){
                processQueue(batches,execCall,finalCallback);
            });
        }
    }
    
    processQueue(batches, function(batch,callback){

        var deleteBatch = function(batch, sucesscallback, failedCallback){
            var condition = ""
            batch.forEach(function(item,index){
                condition += '(ParcelId = '+ item.parcelId +' AND AuxRowId = '+ item.auxRowId +' AND fieldId = '+ item.field["Id"] +') OR '
            });
            condition = condition.substring(0, condition.length - 3);
            var deleteSql = 'DELETE FROM ParcelChanges WHERE '+ condition;
            db.transaction(function (dx) {
            	dx.executeSql(deleteSql, [], function (tx, res) {
                	if(sucesscallback) sucesscallback();
            	}, function (x, e) {
               		console.warn(deleteSql);
	                	console.error(e);
	                	if (failedCallback) failedCallback();
            	});
            });
        }

        var insertBatch = function(batch, sucesscallback, failedCallback){
            var values = "",
            	itemData = [];
            batch.forEach(function(item,index){
            	var utime = ticks();
                  values += " SELECT  ?, ?, ?, ?, ?, ?, ?, ?, ?, '0', ?, ? UNION ALL"
                  itemData.push(item.parcelId, item.auxRowId, item.parentAuxRowId, item.action, item.field.Name, item.field.Id, item.oldValue, item.newValue, utime, rlat, rlng)
            });
            values = values.substring(0, values.length-5);
            var insertSql = 'INSERT INTO ParcelChanges (ParcelId, AuxRowId, ParentAuxRowId, Action, Field, FieldId, OldValue, NewValue, ChangedTime, Synced, Latitude, Longitude) '+ values;
            db.transaction(function (dx) {
            dx.executeSql(insertSql, itemData, function (tx1, res) {
                var sqls= []
                    ,updateSql = "";
                for(var i = 0; i< batch.length; i++){
                    var eachItem = batch[i];
                    if (eachItem.field["SourceTable"]) {
                        var updtVal = "" 
                        batch.filter(function(x){ return ( x.field["SourceTable"] == eachItem.field["SourceTable"] && x.auxRowId == eachItem.auxRowId)}).forEach(function(item,index){
                            var newValues = (item.newValue || item.newValue === 0) ? '\'' + item.newValue.toString().replace(/'/g, "''") + '\'' : null;
                            updtVal += '[' + item.field["Name"] + '] ='+ newValues + ','
                            batch.splice(batch.indexOf(item),1);
                        });
                        updtVal = updtVal.substring(0, updtVal.length-1);
                        i--;
                        updateSql = 'UPDATE ' + eachItem.field["SourceTable"] + ' SET ' + updtVal + ' WHERE ROWUID = ' + eachItem.auxRowId;
                        sqls.push(updateSql);
                    } else {
                        var updtVal = "" 
                        batch.filter(function(x){ return ( x.field["SourceTable"] == null )}).forEach(function(item,index){
                            var newValues = (item.newValue || item.newValue === 0) ? '\'' + item.newValue.toString().replace(/'/g, "''") + '\'' : null;
                            updtVal += '[' + item.field["Name"] + '] ='+ newValues + ','
                            batch.splice(batch.indexOf(item),1);
                        });
                        updtVal = updtVal.substring(0, updtVal.length-1);
                        i--;
                        var updateSql = 'UPDATE Parcel SET '+ updtVal +' WHERE Id = ' + eachItem.parcelId;
                        sqls.push(updateSql);
                    }
                }
		executeQueries(sqls, function() {
		    if (sucesscallback) sucesscallback();
		}, function() {
		    if (failedCallback) failedCallback();
		}, null, true);
        }, function (x, e) {
           	console.warn(deleteSql);
        	console.error(e);
        	if (failedCallback) failedCallback();
        });
        });
        }

        deleteBatch(batch, function(){
            insertBatch(batch, function(){
                if(callback) callback();
            },function(){
                if(callback) callback();
            });
        },function(){
            if(callback) callback();
        });
    },function(){
        if(successCallback) successCallback();
    });
}
/* function created for directly sync photos */
function uploadPhotoDirectly(callback) {
    var unsyncedPhotoDetails = [];
    function upPhoto(imgCount) {
        if (imgCount.length == 0) {
            console.log('success');
            if(callback) callback();
            return;
        }
        var ph = imgCount.shift();
        var tt = ph.UploadTime;
        var lid = ph.LocalId;
        if ( tt == null || tt == '' ) {
            tt = ticks();
        }
        if (ph.ParcelId && ph.Image && lid) {
           ajaxRequest({
                url: baseUrl + 'mobileassessor/photosync.jrq?zt=' + ticks(),
                dataType: 'json',
                type: "POST",
                data: {
                    parcelId: ph.ParcelId,
                    photo: ph.Image,
                    Type: ph.Type,
                    localid: lid,
                    timestamp: tt

                },
                success: function (data, stat) {
                    if (data.LoginStatus == '401') {
                        LogoutErrorInsert("page: photosync login");
                        Logout();
                    }
                    if (data.status == "OK") {
                        var unsyncedPhotoDet = unsyncedPhotoDetails.filter(function(x) {return x.LocalId == ph.LocalId})[0];
                        if (!unsyncedPhotoDet) {
                            todoDB.indexedDB.update('Images', [{name: 'Synced', value: 1}, {name: 'Id', value: data.imageId}], ['LocalId'], ['='], [ph.LocalId], [], function() {
                                if (upPhoto) upPhoto(imgCount);
                            });
                        }
                        else {
                            getTransaction(['UnsyncedImages'], false, function(tx) {
                                var keyRangeValue = IDBKeyRange.only(unsyncedPhotoDet.UnsyncedImagesKey);
                                var store = tx.objectStore('UnsyncedImages')
                                var delReq = store.delete(keyRangeValue);
                                tx.oncomplete = function (e) {
                                    todoDB.indexedDB.update('Images', [{name: 'Synced', value: 1}, {name: 'Id', value: data.imageId}], ['LocalId'], ['='], [ph.LocalId], [], function() {
                                       if (upPhoto) upPhoto(imgCount);
                                    });
                                }

                                delReq.onsuccess = function (evt){
                                    console.log('Deleted unsynced image')
                                }

                            }); 
                        }                           	
                    }
                    else {
                        console.log(data.message);
                        if (upPhoto) upPhoto(imgCount);

                    }

                },
                error: function (req, stat, err) {
                    console.log(err);
                    return;
                }
            });
        } 
        else {
        	if (upPhoto) upPhoto(imgCount);
        }  
    }
    IDB_getData('Images', ['Synced'], ['='], [0], [], function (imgCount) {
        IDB_getData('UnsyncedImages', [], [], [], [], function (unsyncedCount) {
            unsyncedPhotoDetails = unsyncedCount? unsyncedCount: [];
            upPhoto(imgCount);
        });
    });

}

/* end */

function removeMultipleParcels(tbl, cpback) {
    if (isComparableScreenMenu && tbl == '*' && !removeMulParcel) {
        removeMulParcel = true;
        getData("SELECT rowid, Id, IsComparable FROM Parcel", [], (pcList) => {
            let delList = [], pList = [];
            pcList.forEach((p) => {
                let pc = pList.filter((x) => { return x.Id == p.Id })[0];
                if (pc) {
                    if (p.IsComparable == 1) delList.push(p.rowid);
                    else if (p.IsComparable == 0) {
                        if (pc.IsComparable == 0) delList.push(p.rowid);
                        else if (pc.IsComparable == 1) {
                            delList.push(pc.rowid);
                            pc.IsComparable = 0; pc.rowid = p.rowid;
                        }
                    }
                } else {
                    pList.push(p);
                }
            });

            if (delList.length > 0) {
                let st = delList.toString();
                console.log(st);
                getData("DELETE FROM Parcel WHERE rowid in (" + st + ")", [], (pcList) => {
                    if (cpback) cpback();
                }, {}, true, () => { if (cpback) cpback(); });
            }
            else if (cpback) cpback();
        }, {}, true, () => { if (cpback) cpback(); });
    }
    else if (cpback) cpback();
}