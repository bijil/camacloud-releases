﻿/*_____________________________________________________________________________________________________
|                                                                                                      |
|                                                                                                      |
|                                   MASS UPDATE FIELDS                                                 |
|                                                                                                      |
|______________________________________________________________________________________________________|*/

var massUpdateForm = false;
var massUpdationInProgress = false;
var massUpdateFields = [];
var classCalculatorFields = []
var classCalculatorResultField = null;
var classCalculationResult = null;
var currentClass = null;
var fieldData = null;
var changedValues = [];
var status = '';
var msg = '';
var classcalculationInProgress = false;
var classCalculatorField = null;
var classCalculatorParameterField = null;
var classCalculationNewValue;
var classCalculationResultDisplayVal

function getMassUpdateFormTemplate(category, fields) {
    var source = category.SourceTable;
	var currentFocussedInput;
    var massUpdateContentDiv = document.createElement('div');
    massUpdateContentDiv.className = 'massupdate-content'

    var massUpdateHeaderDiv = document.createElement('div');
    massUpdateHeaderDiv.className = 'massupdate-header-content'
    massUpdateHeaderDiv.setAttribute("style", "float: right;padding: 10px;clear: 0;width: 100%;")
    $(massUpdateHeaderDiv).html('<span><button id="mass-update-cancel" style="float: right;" >Cancel</button></span><span><button id="mass-update-submit" style="float: right; margin-right: 5px;" >Update</button></span>')

    $(massUpdateContentDiv).append(massUpdateHeaderDiv);

    //massupdatechildrecords
    if (category[0].MassUpdateDescendents == "true") {                 
        $(massUpdateHeaderDiv).html('<span style="margin-left:20px;"><input type="radio" id="updateall" name="update" value="1" checked><label for= "Update All Record"> Update All Records</label><span style="margin: 10px;"></span><input type="radio" id="updatematch" name="update" value="2"><label for="Update only if Value Matches" style="margin-left:5px;">Update only if Value Matches</label></span><span><button id="mass-update-cancel" style="float: right;" >Cancel</button></span><span><button id="mass-update-submit" style="float: right; margin-right: 5px;" >Update</button></span>');
        $(massUpdateContentDiv).append(massUpdateHeaderDiv);
    }

   
    var massUpdateContent = document.createElement('div');
    massUpdateContent.className = 'data-entry-massupdate-content';

    var massUpdateFields = fields.filter(function (f) { return checkIsFalse(f.IsMassUpdateField) });
    massUpdateForm = true;
    massUpdateFields.forEach(function (field) {
        if (field.InputType == 5 && field.LookupQuery) {
            let whereIndex = field.LookupQuery.search(/where/i);
            if (whereIndex !== -1) {
                field.LookupQuery = field.LookupQuery.substr(0, whereIndex);
            }
        }
        var options = { category: category }
		if (category[0].ParentCategoryId) {
			var parentData = activeParcel[getSourceTable(category[0].Id)].filter(function(d){ return d.ROWUID == sourceDataInfo.auxRowUid; })
			if (parentData.length) options.source= parentData[0];
		}
        var fieldSet =  getFieldSet(field, options, null, true);
        if(parseInt($(fieldSet).attr('field-type')) == 13){
			var radioOptions = [];
            if (field.RadioFieldValues && field.RadioFieldValues != "") {
                var values = []; 
                values = field.RadioFieldValues.trim().split(',');
                radioOptions = [{ name : field.Id, text: 'Yes', value: values[0] }, { name : field.Id, text: 'No', value: values[1] }];
            }
            else
                radioOptions = [{ name : field.Id, text: 'Yes', value: 'true' }, { name : field.Id, text: 'No', value: 'false' }];
            $('radiogroup', $(fieldSet)).css({'width': '131px'});
            if(field.AllowRadioNull == "true"){
                var nullOption = { name : field.Id, text: 'Blank', value: '' }
                radioOptions.push(nullOption);
                $('radiogroup', $(fieldSet)).css({'width':'210px'});
            }
            var rbutton = $('.RadioButtonGroup',$(fieldSet))[0];
            $(rbutton).html('<input type="radio" class="radioButton" name=mu${name} value=${value}>${text}</input>&nbsp;');
            $(rbutton).fillTemplate(radioOptions)
            $(fieldSet).append(rbutton);
		}
        $(massUpdateContent).append(fieldSet);
        /*if ($('input', $(fieldSet)).length > 0) {
        	$('input', $(fieldSet)).bind('focus', function () {
        		currentFocussedInput = this;
	        	var x = $(currentFocussedInput).position();
	        	var element = document.getElementById('mass-update-div');
	        	element = element.querySelectorAll('.scrollable')[0];
	        	var offset = x.top - 270;
		        if (Math.abs(window.orientation) == 0 || Math.abs(window.orientation) == 180) 
					offset = x.top - 520;
				
				if (offset > 0) {
					enableFocus = true;
					element.scrollTop = element.scrollTop + offset;
				}
				window.setTimeout(function(){ enableFocus = false; }, 100)
        	});
        }*/
    });
    /*$('#mass-update-div .scrollable').on('scroll', function () {
        if (currentFocussedInput && !enableFocus) $(currentFocussedInput).blur();
    });*/
    
    massUpdateForm = false;
    $(massUpdateContentDiv).append(massUpdateContent);
    $('.dc-massupdate').html('');
    $('.dc-massupdate').append(massUpdateContentDiv);
   //$('.dc-massupdate').addClass("scroller");

    $('.massupdate-content .newvalue').unbind('change');
    $('.massupdate-content .newvalue').bind('change', function (e) {
        addToMassChanges(this);
    });
    
   $('.massupdate-content .newvalue').unbind('keyup');
   $('.massupdate-content .newvalue').bind('keyup', function (e) {
       addToMassChanges(this);
   });

    $('#mass-update-submit').unbind(touchClickEvent);
    $('#mass-update-submit').bind(touchClickEvent, function (e) {
    	var v = changedValues;
    	v.forEach(function(ele,index){
    	 	if(ele.Value === ""){
    	 		v.splice(index,1);
    	 	}
    	 })
    	if(changedValues.length > 0){
	        messageBox(msg_confirm_mass_update, ['YES', 'CANCEL'], function () {
	            massUpdationInProgress = true;
	            showMask(null);
	            massUpdateChanges(function (x) {	
	                console.log('x', x);
	                if (x == '') {
	                    reloadAuxiliaryRecords(category[0].Id);
	                    refreshDataCollectionValues();
	                    massUpdationInProgress = false;
	                    $('#maskLayer').hide();
	                    messageBox(msg_after_mass_update, ['OK'], function () {
	                        $T.goBack();
	                        changedValues = [];
						  	//formScroller.refresh();
	                    })
	                }
	            });
	        });
        }
        else{
        	messageBox("No changes have been made.", ['OK'], function () {                
		  		//formScroller.refresh();
            })
        }
        hideKeyboard();
    });
    $('#mass-update-cancel').unbind(touchClickEvent);
    $('#mass-update-cancel').bind(touchClickEvent, function (e) {
    	var v = changedValues;
    	v.forEach(function(ele,index){
    	 	if(ele.Value === ""){
    	 		v.splice(index,1);
    	 	}
    	 })
    	hideKeyboard();
        if (!massUpdationInProgress) {
            cancelMassUpdates();
        }
        else {
            messageBox(msg_mass_upd_in_progress);
        }
        
    });
    
	bindFocusForInputs();

    loadScreen('mass-update-div');
}
function addToMassChanges(source) {
    var i = 0
    var fieldId = $(source).attr('field-id');
    var fieldName = $(source).attr('field-name');
    var fldType = $(source).parent('fieldset').attr('field-type');
    var value = $(source).val();
    if(parseInt(fldType) == 5){ 
        if(value) {
            try {
                value = decodeURI(value); 
            } catch { }            
        }
    }
	if(parseInt($(source).parent().attr('field-type')) == 13)
		var value = $('input:checked',source).val();
    var existField = changedValues.filter(function (d) { return d.fieldId == fieldId });
    if (existField.length == 0) {
        changedValues.push({ fieldId: fieldId, Value: value, fieldName: fieldName });
    } else {
        if (!changedValues.some(function (d) { return d.fieldId == fieldId && d.Value == value; })) {
            changedValues.forEach(function (g) {
                if (g.fieldId == fieldId) {

                    changedValues[i].Value = value;
                }
                i = i + 1;
            });

        }

    }
}
function getClassCalculatorFields(categoryId, callback) {
    getData("select *,Serial*1 as SortField from Field Where categoryId=?", [categoryId], function (s) {
        s = ccma.Data.Evaluable.sortArray(s, 'SortField', true);
        var massFields = s.filter(function (m) { return (m.IsClassCalculatorAttribute == 'true') });
        classCalculatorFields.push({ category: fieldCategories.filter(function (v) { return v.Id == categoryId; }), Fields: massFields });
        var cat = getCategory(categoryId)
        classCalculatorResultField = getDataField(cat.ClassCalculatorParameters.split(',')[0], cat.SourceTable)
        classCalculatorParameterField = cat.ClassCalculatorParameters.split(',');
        if (callback) { callback(); }
    });

}

function getMassFields(categoryId, callback) {
    getData("select *,Serial*1 as SortField from Field Where categoryId=?", [categoryId], function (s) {
        s = ccma.Data.Evaluable.sortArray(s, 'SortField', true);
        var massFields = s.filter(function (m) { return checkIsFalse(m.IsMassUpdateField) });
        massUpdateFields.push({ category: fieldCategories.filter(function (v) { return v.Id == categoryId; }), Fields: massFields });

        if (callback) { callback(); }
    });

}

function openMassUpdateForm(categoryId, index, parentPath, orderindex) {
    var sourceTable = getSourceTable(categoryId);
    var filteredIndexes = getSiblings(categoryId, index);
    var parentSpec = getParentSpec(categoryId, index);
    massUpdateFields = [];
    sourceDataInfo = {};
    changedValues = [];
    var auxRowUid;
    getMassFields(categoryId, function () {

        if (massUpdateFields.length > 0) {

            // var findex = $(auxForm).attr('findex')
            var parentAuxRowuid;
            var auxRowuid;
            if (parentSpec) {
                parentAuxRowuid = activeParcel[parentSpec.source][parentSpec.index].ROWUID;
                // console.dir(parentAuxRecord);
            } else {
                auxRowuid = activeParcel[sourceTable][index].ROWUID;
                //console.dir(auxRecord);
            }
            // sourceDataInfo.push({ parentRowUid: parentAuxRowuid, auxRowUid: auxRowuid, filteredIndexes: filteredIndexes, sourceTable: sourceTable });
            sourceDataInfo.parentRowUid = parentAuxRowuid;
            sourceDataInfo.auxRowUid = activeParcel[sourceTable][index].ROWUID;
            sourceDataInfo.filteredIndexes = filteredIndexes;
            sourceDataInfo.sourceTable = sourceTable;
            sourceDataInfo.findex = index;
            sourceDataInfo.orderindex = orderindex;
            sourceDataInfo.ParentPath = parentPath;
            getMassUpdateFormTemplate(massUpdateFields[0].category, massUpdateFields[0].Fields);
    		bindInfoButtonClick();
        }

    });
}

function processChildMassUpdate(pc, cat, rowuid, ov, type) {
    let mchildcat = fieldCategories.filter((x) => { return x.ParentCategoryId == cat.Id && x.MassUpdateDescendentFields && x.MassUpdateDescendentFields.contains('{' + pc.fieldName + '}') }), pcis = [];

    if (mchildcat?.length > 0) {
        mchildcat.forEach((mcat) => {
            let massUpdateDescendentFields = mcat.MassUpdateDescendentFields.split(',').filter((x) => { return x.includes('{' + pc.fieldName + '}') })[0];
            if (massUpdateDescendentFields) {
                let cField = massUpdateDescendentFields.split('}')[1].trim(), recs = activeParcel[mcat.SourceTable].filter((x) => { return x.ParentROWUID == rowuid && (type == 1 ? true : x[cField] == ov) }), field = getDataField(cField, mcat.SourceTable);
                if (checkIsFalse(field.IsMassUpdateField)) {
                    recs.forEach((rec) => {
                        let ix = activeParcel[mcat.SourceTable].indexOf(rec);
                        pcis.push({ ROWUID: rec.ROWUID, sourceTable: mcat.SourceTable, fieldId: field.Id, newValue: pc.Value, fieldName: field.Name, oldValue: rec.Original[field.Name] ? rec.Original[field.Name] : null, recIndex: ix });
                    });
                }
            }
        });
    }

    return pcis;
}

function massUpdateChanges(callback) {
    //  console.log('enter');
    //save focused element also
    ($(':focus').length > 0) ? addToMassChanges($(':focus')[0]) : '';
    var auxRowUId;
    var iter = 0;
    var massChangeArray = [];
    //console.log('--------changedValues-----------');
    //console.dir(changedValues);
    
    let cat = fieldCategories.filter((x) => { return x.SourceTable == sourceDataInfo.sourceTable })[0];

    //get the selected radiobutton
        var ele = document.getElementsByName('update');

        for (i = 0; i < ele.length; i++) {
            if (ele[i].checked)
                var type = ele[i].value;
        }
    

    if (!sourceDataInfo.parentRowUid) {
        auxRowUId = sourceDataInfo.auxRowUid;
        // eval(sourceDataInfo.ParentPath + '.' + sourceDataInfo.sourceTable + '[' + sourceDataInfo.orderindex + ']' + '.' + s.fieldName + ".Value='" + s.Value + "'");
        changedValues.forEach(function (s) {
            //console.log('********inside');
            //sconsole.dir(s);
            var oldValue = eval('activeParcel.Original.' + sourceDataInfo.sourceTable + '[' + sourceDataInfo.findex + ']' + '.' + s.fieldName);
            validateMassupdate(s, sourceDataInfo.sourceTable);
            if (activeParcel.Changes)
                activeParcel.Changes.push({ Action: "E", AuxRowId: auxRowUId, ChangedTime: Date.now(), Field: s.fieldName, FieldId: s.fieldId, Index: sourceDataInfo.findex, NewValue: s.Value, OldValue: oldValue, ParcelId: activeParcel.Id, SourceTable: sourceDataInfo.sourceTable });
            massChangeArray.push({ ROWUID: auxRowUId, sourceTable: sourceDataInfo.sourceTable, fieldId: s.fieldId, newValue: s.Value, fieldName: s.fieldName, oldValue: oldValue, recIndex: sourceDataInfo.findex });
            if (cat?.MassUpdateDescendents == 'true') {
                let val = eval('activeParcel.' + sourceDataInfo.sourceTable + '[' + sourceDataInfo.findex + ']' + '.' + s.fieldName),
                    crec = processChildMassUpdate(s, cat, auxRowUId, val, type);
                if (crec?.length > 0) massChangeArray = massChangeArray.concat(crec);
            }
        });

    } else {

        //fetch data from sourceDataInfo.filteredIndexes[index] rowuid, fieldname, fieldId, oldValue, newValue, sourceTable
        sourceDataInfo.filteredIndexes.forEach(function (d, i) {
            // var isNotPreviousRecord = canChangeExistAuxDetails(activeParcel, categoryId, d);
            var rowuid = eval('activeParcel.' + sourceDataInfo.sourceTable + '[' + d + '].ROWUID');
            changedValues.forEach(function (s) {
                //   eval(sourceDataInfo.ParentPath + '.' + sourceDataInfo.sourceTable + '[' + i + ']' + '.' + s.fieldName + ".Value='" + s.Value + "'");
                var oldValue = eval('activeParcel.Original.' + sourceDataInfo.sourceTable + '[' + d + ']' + '.' + s.fieldName);
                validateMassupdate(s, sourceDataInfo.sourceTable, callback);
                if (activeParcel.Changes)
                    activeParcel.Changes.push({ Action: "E", AuxRowId: rowuid, ChangedTime: Date.now(), Field: s.fieldName, FieldId: s.fieldId, Index: d, NewValue: s.Value, OldValue: oldValue, ParcelId: activeParcel.Id, SourceTable: sourceDataInfo.sourceTable });
                massChangeArray.push({ ROWUID: rowuid, sourceTable: sourceDataInfo.sourceTable, fieldId: s.fieldId, newValue: s.Value, fieldName: s.fieldName, oldValue: oldValue, recIndex: d });
                if (cat?.MassUpdateDescendents == 'true') {
                    let val = eval('activeParcel.' + sourceDataInfo.sourceTable + '[' + d + ']' + '.' + s.fieldName),
                        crec = processChildMassUpdate(s, cat, rowuid, val, type);
                    if (crec?.length > 0) massChangeArray = massChangeArray.concat(crec);
                }
            });
        });


    }
    //console.log('--------changedValues-----------');
    //console.dir(changedValues);
    //console.log('-----------------massChangeArray-------------');
    //console.dir(massChangeArray);
    //fieldId: fieldId, Value: value, fieldName: fieldName
    var fetchNext = function (index) {
        updateChanges(massChangeArray[index].ROWUID, massChangeArray[index].fieldName, massChangeArray[index].fieldId, massChangeArray[index].oldValue, massChangeArray[index].newValue, massChangeArray[index].sourceTable, massChangeArray[index].recIndex,
            function () {
                ++iter;
                if (iter == massChangeArray.length) {

                    if (callback) callback(status);
                }
                else {
                    fetchNext(iter);
                }
            });

    }
    if (massChangeArray.length > 0) {
        fetchNext(0);
    }
    else {
        if (callback) callback(status);
    }



    // ccma.Sync.enqueueParcelChange(activeParcel.Id, newAuxRowId, (pRID || ''), 'new', null, 'NEW', (pRID || ''), thisF.SourceTableName, null, function () {
    //sconsole.log(sourceDataInfo);


}

function updateChanges(rowuid, fieldname, fieldId, oldValue, newValue, sourceTable, index, callback) {
    //console.log('rowuid:' + rowuid + ',fieldname:' + fieldname + ',fieldId:' + fieldId + ',oldValue:' + oldValue + ',newValue:' + newValue + ',sourceTable:' + sourceTable + ', index:'+index);
    if (status == '') {
        console.log('updated');
        ccma.Sync.enqueueParcelChange(activeParcel.Id, rowuid, null, 'E', fieldname, fieldId, oldValue, newValue, {
            updateData: true,
            updateObject: true,
            source: sourceTable,
            object: activeParcel[sourceTable][index]
        }, function () {
            if (callback) callback();
        });
    } else {
        status = '';
    }
}

function cancelMassUpdates() {

    if (changedValues.length > 0) {
        messageBox(msg_cancel, ["Yes", "No"], function () {
            changedValues = [];
            $T.goBack();
        });
    } else {
        $T.goBack();
    }

}
function allowMassupdate(catid) {
    var flag = false;
    if (fieldCategories.some(function (d) { return checkIsFalse(d.AllowMassUpdate) && d.Id == catid; })) {
        if ($('section[catid="' + catid + '"] fieldset[mass-upd]').length > 0) {
            return true;
        }
        else {
            flag = false;
        }
    }
    return flag;
}

function showMassUpdateForm(categoryId, Rowuid) {
    descendantsInfo = [];
    var childs = [], pCategoryId = getParentCategories(categoryId);
    var indexOfParent = getFormIndexFromRowuid(pCategoryId, Rowuid);
    getDescendants(pCategoryId, indexOfParent, activeParcel);
    childs = descendantsInfo.filter(function (d) { return d.categoryId == categoryId; });
    descendantsInfo = [];
    if (childs.length > 0) {
        openMassUpdateForm(childs[0].categoryId, childs[0].index);
    }
}

function validateMassupdate(fieldData, sourseTab, callback) {
    var field = fieldData.fieldId;
    var inputType = parseInt(datafields[field].InputType);
    var fieldName = fieldData.fieldName;
    var catid = fieldData.CategoryId;
    var sourceTable = sourseTab;
    var d = ccma.Data.Types[inputType];
    // var el = this;
    var pattern = new RegExp(d.pattern);

    if (!pattern.test(fieldData.Value)) {

		$('#maskLayer').hide();
        if (d.baseType == 'number') {
            if (d.typename == 'Year') {
                msg = 'Invalid input. Please enter a valid 4-digit year for the field.';
            }
            else if (d.typename == 'Money' || d.typename == 'Real Number') {
                msg = 'Invalid input. Commas or other characters are not allowed. You can type only numerals and a single period.';
            }
            else if (d.typename == 'Whole Number' || d.typename == 'Cost Value') {
                msg = 'Invalid input. Commas or other characters are not allowed. You can type only numerals.';
            }
            else {
                msg = 'Invalid input. Please enter a valid Number for the field.';
            }
            status = msg;
            msg = '';
           	
           	if (status != '')
            {
            	massUpdationInProgress=false;
            }
            
            messageBox(status);



        }
        //if (el.parentNode.parentNode.className == 'data-entry-massupdate-content' && msg == '') {
        //    addToMassChanges(el);
        //    return false;
        //}

    }


}
//NAN End

function getClassCalculatorUpdateFormTemplate(category, classCalculatorFields, index, sourceTable) {
    var source = category.SourceTable;
    currentClass = activeParcel[sourceTable][index][classCalculatorResultField.Name];
    let classDisplayValue = currentClass;
    var currentFocussedInput;
    //   var currentClassDiplayVal = activeParcel.Eval(sourceTable + '[' + index + ']', classCalculatorResultField.Name).DisplayValue
    var classCalculatorContentDiv = document.createElement('div');
    classCalculatorContentDiv.className = 'classcalculator-content'

    if (classCalculatorResultField.InputType == 5) {
        if (clientSettings?.ClassCalcDisplayShortDescAndOrLongDesc) {
            let currentClassCalcDisplay = clientSettings.ClassCalcDisplayShortDescAndOrLongDesc;
            let cv = evalLookup(classCalculatorResultField.Name, currentClass, currentClassCalcDisplay, classCalculatorResultField)
            classDisplayValue = cv ? cv : currentClass;
        }
        else {
            let cv = evalLookup(classCalculatorResultField.Name, currentClass, null, classCalculatorResultField)
            classDisplayValue = cv ? cv : currentClass;
        }
	}

    var classCalculatorHeaderDiv = document.createElement('div');
    classCalculatorHeaderDiv.className = 'classcalculator-header-content'
    classCalculatorHeaderDiv.setAttribute("style", "float: right;padding: 10px;clear: 0;width: 100%;")

    $(classCalculatorHeaderDiv).html('<span style="display: inline-block; width: 50%;"><span style="display: block;"><label style="margin-left: 5%;font-size: 20px;">Current: </label><span style="font-size:20px;color:yellowgreen;font-weight: bold;" class="currentClass"></span></span><span style="display: block;"><label style="margin-left: 5%;font-size: 20px;display:none" class="resultlabel"> Calculated Class :</label><span style="margin-left:20px;display:none;font-size: 20px;color:red;font-weight: bold;" class="resultClass"></span></span></span><span><button id="classcalculator-cancel" style="float: right;" >Cancel</button></span><span><button id="classcalculator-update" style="float: right; margin-right: 5px;display:none" >Update</button><button id="classcalculator-submit" style="float: right; margin-right: 5px;" >Calculate Class</button></span><span><button id="classcalculator-update" style="float: right; margin-right: 5px;display:none" >Update</button></span>')
    // $(massUpdateHeaderDiv).html('<span><button id="mass-update-submit" style="float: right; margin-right: 5px;" >Update</button></span>');
    $(classCalculatorContentDiv).append(classCalculatorHeaderDiv);


    var calculatorContent = document.createElement('div');
    calculatorContent.className = 'data-entry-classcalculator-content';
    calculatorContent.setAttribute("style", "float: left;clear: 0;width: 100%;overflow-x: hidden;");
    classCalculatorForm = true;
    classCalculatorFields.forEach(function (field) {
    	var value = '';
    	if (activeParcel[sourceTable][index][field.Name]) {
            value = activeParcel[sourceTable][index][field.Name];
            if(value) value = encodeURI(value);
            value = (value && value.length) ? value.replace('&gt;', '>') : "";
            value = (value && value.length) ? value.replace('&lt;', '<') : "";
            //$('.dc-classcalculator fieldset[field-name="' + field.Name + '"] .newvalue').val(value)
        }
        var fieldSet = getFieldSet(field, { category: category, formType: 'mass-update-div', ClassCalcField: { isTrue: true, CalcValue: value } });
        $('.newvalue', $(fieldSet)).val(value);
        if(field.DoNotShow =='false')
        	$(calculatorContent).append(fieldSet);
    });
    classCalculator = false;
    $(classCalculatorContentDiv).append(calculatorContent);
    $('.dc-classcalculator').html('');
    $('.dc-classcalculator').append(classCalculatorContentDiv);
  //  $('.dc-classcalculator').addClass("scroller");
    $('.dc-classcalculator .currentClass').html(classDisplayValue);
    //$('.massupdate-content .newvalue').unbind('change');
    //$('.massupdate-content .newvalue').bind('change', function (e) {
    //    addToMassChanges(this);
    //});
	/*
	$('.dc-classcalculator input').bind('focus', function(){
		currentFocussedInput = this;
		var input = $(currentFocussedInput).position();
    	var element = document.getElementsByClassName('dc-classcalculator')[0].parentElement;
    	var offset = input.top - 270;
        if (Math.abs(window.orientation) == 0 || Math.abs(window.orientation) == 180) 
			offset = input.top - 520;		
		if (offset > 0) {
			enableFocus = true;
			element.scrollTop = element.scrollTop + offset;
		}
		window.setTimeout(function(){ enableFocus = false; }, 100)
	})
	$('.dc-classcalculator').parent().on('scroll', function () {
        if (currentFocussedInput && !enableFocus) $(currentFocussedInput).blur();
    });*/
    $('#classcalculator-submit').unbind(touchClickEvent);
    $('#classcalculator-submit').bind(touchClickEvent, function (e) {
        //messageBox(msg_confirm_mass_update, ['YES', 'CANCEL'], function () {
        var validationFailed = false;
        $('.maskLayer').show();
        classCalculationResult = 0;
        $('.data-entry-classcalculator-content .newvalue').each(function () {
            var fId = $(this).attr('field-id');
            var fName = datafields[fId].Name;
            var sourceTable = datafields[fId].SourceTable;
            var field = getDataField(fName, sourceTable);
            var decodedval = $(this).val();
            if(decodedval){
                try {
                    decodedval = decodeURI(decodedval);
                } catch { }               
            }
            var value = evalLookup(fName, decodedval, null, field, true)
            if (value == '' || value == null) {
                messageBox("All fields are required ");
                validationFailed = true;
                return;
            }
            classCalculationResult += isNaN( Number(value) )? 0: Number(value);
        });
        if (validationFailed == true) return;
        //messageBox(msg_confirm_mass_update, ['YES', 'CANCEL'], function () {
        //})
        classcalculationInProgress = true;
        var field = getDataField(classCalculatorResultField.Name, source);
        if (clientSettings?.ClassCalcDisplayShortDescAndOrLongDesc) {
            var ClassCalcDisplayShortDescAndOrLongDesc = clientSettings.ClassCalcDisplayShortDescAndOrLongDesc;
             classCalculationNewValue = (field.InputType == 5) ? evalLookup(classCalculatorResultField.Name, classCalculationResult, ClassCalcDisplayShortDescAndOrLongDesc, field) : classCalculationResult;
            $('.resultClass').html(classCalculationNewValue)
             classCalculationResultDisplayVal = (field.InputType == 5) ? evalLookup(classCalculatorResultField.Name, classCalculationResult, null, field) : classCalculationResult;
        }
        else {
             classCalculationResultDisplayVal = (field.InputType == 5) ? evalLookup(classCalculatorResultField.Name, classCalculationResult, null, field) : classCalculationResult;
            $('.resultClass').html(classCalculationResultDisplayVal)
        }

       
        

        $('.resultlabel,.resultClass').show();
        $('.maskLayer').hide();

        //if (classCalculationResult != currentClass)  MA_2397 - there is a possiblity to calculate same result if changing class calculate fields, so that need to save. Also this condition is not checking QC.
        $('#classcalculator-update').show();

        $('#classcalculator-update').bind(touchClickEvent, function (e) {
            messageBox("Are you sure you want to update the Calculated Class ?", ["Yes", "No"], function () {
                currentClass = classCalculationResult;
                if (classCalculatorParameterField[0]) {
                    activeParcel[sourceTable][index][classCalculatorResultField.Name] = classCalculationResult.toString();
                    var auxform = $('section[catid="' + ccma.UI.ActiveFormTabId + '"]');
                    $('fieldset[field-name="' + classCalculatorResultField.Name + '"] span[readonlyexp]', auxform).html(classCalculationResultDisplayVal)                   
                    ccma.Sync.enqueueParcelChange(activeParcel.Id, activeParcel[sourceTable][index].ROWUID, (activeParcel.Id || ''), null, classCalculatorResultField.Name, classCalculatorResultField.Id, null, classCalculationResult.toString(), { updateData: true, source: classCalculatorResultField.SourceTable }, null)
                }

                if (clientSettings?.ClassCalcDisplayShortDescAndOrLongDesc) {
                    if (classCalculatorParameterField[1]) {
                        var parameterField = classCalculatorParameterField[1].includes('.') ? classCalculatorParameterField[1].split('.') : [classCalculatorParameterField[1]];
                        if (sourceTable == parameterField[0]) {
                            var classCalculatorField = getDataField(parameterField[1], sourceTable);
                            if (classCalculationNewValue != classCalculationResultDisplayVal) {
                                if (classCalculatorField != null) {
                                    activeParcel[sourceTable][index][classCalculatorField.Name] = classCalculationResultDisplayVal.toString();
                                    var auxform = $('section[catid="' + ccma.UI.ActiveFormTabId + '"]');
                                    var selValue = $('fieldset[field-name="' + classCalculatorField.Name + '"] input[class="cusDpdownSpan cc-drop newvalue"]').attr('sel');
                                    $('fieldset[field-name="' + classCalculatorField.Name + '"] input[class="cusDpdownSpan cc-drop newvalue"]', auxform).attr('sel', selValue).val(classCalculationResultDisplayVal);
                                    ccma.Sync.enqueueParcelChange(activeParcel.Id, activeParcel[sourceTable][index].ROWUID, (activeParcel.Id || ''), null, classCalculatorField.Name, classCalculatorField.Id, null, classCalculationResultDisplayVal.toString(), { updateData: true, source: classCalculatorField.SourceTable }, null)
                                }
                            }
                            else {
                                messageBox("The Calculated Class does not have a matching \"" + classCalculatorField.Name + "\" value! Please manually change the \"" + classCalculatorField.Name + "\" value and contact Support to resolve this issue.");
                            }
                        }
                    }
                }
                $('#classcalculator-update').hide();
                $('.classcalculator-content .newvalue').forEach(function (elem) {
                	if($(elem).val()){
                		var elValue = $(elem).val();
                        let tValue = elValue;
                        try {
                            elValue = decodeURI(tValue);
                        } catch {
                            elValue = tValue;
                        }
                	}
                    ccma.Sync.enqueueParcelChange(activeParcel.Id, activeParcel[sourceTable][index].ROWUID, (activeParcel.Id || ''), null, $(elem).attr('field-name'), $(elem).attr("field-id"), null, elValue, { updateData: true, source: classCalculatorResultField.SourceTable }, null)
                    activeParcel[sourceTable][index][$(elem).attr('field-name')] = elValue;
                })
                $T.goBack();
            });
        });
        classcalculationInProgress = false;
    });
    //});
    //$('#mass-update-cancel').unbind(touchClickEvent);
    $('#classcalculator-cancel').bind(touchClickEvent, function (e) {
        if (!classcalculationInProgress) {
            cancelclasscalculation();
        }
        else {
            messageBox("Please wait, calculation is in progress.");
        }
    });
    /* classCalculatorFields.forEach(function (field) {
        if (activeParcel[sourceTable][index][field.Name]) {
            var fId = field.Id;
            var fName = field.Name;
            var value = activeParcel[sourceTable][index][fName];
            if(value) value = encodeURI(value);
            value = (value && value.length) ? value.replace('&gt;', '>') : "";
            value = (value && value.length) ? value.replace('&lt;', '<') : "";
            $('.dc-classcalculator fieldset[field-name="' + field.Name + '"] .newvalue').val(value)
        }
    });*/
    loadScreen('classcalculator-div');
    setActiveScreenTitle('Class Calculator', activeParcel);
    bindInfoButtonClick();
    bindFocusForInputs();
}
function cancelclasscalculation() {

    if (!classCalculationResult && classCalculationResult != currentClass) {
        messageBox("Are you sure you want to cancel ?", ["Yes", "No"], function () {
            changedValues = [];
            $T.goBack();
        });
    } else {
        $T.goBack();
    }

}