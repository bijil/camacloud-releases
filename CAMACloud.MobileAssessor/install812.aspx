﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="install812.aspx.vb" Inherits="CAMACloud.MobileAssessor.install812" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CAMA Cloud&#174; </title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="static/app/apple-touch-icon-precomposed-72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="static/app/apple-touch-icon-precomposed-144.png" />
    <meta name="apple-mobile-web-app-title" content="CAMA Cloud&#174;">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="user-scalable=no,initial-scale=1.0,maximum-scale=1.0,width=device-width,height=device-height" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <link rel="stylesheet" href="/static/install/install.css" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="ma-logo-for-installation">
    </div>
    <div class="content">
        <h1>
            Installation Instructions:</h1>
        <ul>
            <li>Tap on the Action button of the browser. <br /><img src="static/install/help812/ios-action-button.png" /><br /></li>
            <li>Touch on the <strong>Add to Homescreen</strong> option.<br /><img src="static/install/help812/add-to-home-screen.png" /><br /></li>
            <li>Start <strong>CAMA Cloud<sup>&#174;</sup> MobileAssessor<sup>&#174;</sup></strong> from the Home Screen.</li>
            <li>Enter License Details, activate client and proceed to the application.</li>
        </ul>
    </div>
    </form>
</body>
</html>
