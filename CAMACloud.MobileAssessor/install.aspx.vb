﻿Public Class install
    Inherits System.Web.UI.Page
    Public macID As String
    Public calauthority As String
    Public iosVersion As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        calauthority = ApplicationSettings.CALAuthorityPageUrl
        Dim ua As String = Request.UserAgent
        iosVersion = IIf(ua.Contains("OS 11") Or ua.Contains("OS 12"), "11+", "")
        If Not HttpContext.Current.Request.Cookies("MACID") Is Nothing Then
            macID = HttpContext.Current.GetKeyValue("MACID")
        ElseIf Request.QueryString("macid") <> ""
            macID = Request.QueryString("macid")
        End If
        If HttpContext.Current.Request.Cookies("MACID") Is Nothing And iosVersion = "11+" Then
            If macID <> "" Then
                Dim drLi As DataRow = Database.System.GetTopRow("Select * FROM DeviceLicense WHERE MachineKey = '" + macID + "'")
                If drLi Is Nothing Then
                Else
                    Dim macid_cookie As New HttpCookie("MACID")
                    macid_cookie.Domain = ApplicationSettings.AuthenticationDomain
                    macid_cookie.Expires = Now.AddYears(1)
                    'auth.Expires = Now.AddDays(1)
                    macid_cookie.Value = macID
                    Context.Response.Cookies.Add(macid_cookie)
                End If
            End If
        ElseIf HttpContext.Current.Request.Cookies("MACID") IsNot Nothing And iosVersion = "11+" Then
            Dim drLi As DataRow = Database.System.GetTopRow("Select * FROM DeviceLicense WHERE MachineKey = '" + macID + "'")
            If drLi Is Nothing Then
                Response.Redirect(calauthority)
            End If
        End If
    End Sub

End Class