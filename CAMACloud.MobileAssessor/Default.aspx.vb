﻿Imports System.IO
Imports System.Net
'Imports System.Net.Mail

Public Module General
    Public DebuggerKey As String
End Module


Partial Class _default
    Inherits System.Web.UI.Page
    Public APIKey As String
    Public SecretKey As String
    Public IPALoadUrl As String
    Public macID As String = ""
    Public licenseId As String
    Public cacheVersion As String = ""
    Public maxModDate As Date = #1/1/2010#
    Public exludePaths As New List(Of String)

    Public ReadOnly Property CacheManifest As String
        Get
            'If Request("resetcache") IsNot Nothing Then
            '    Dim resetId As Long = Date.UtcNow.Ticks
            '    Return " manifest='offline.appcache?reset=" & resetId & "'"
            'End If
            If Request("nocache") IsNot Nothing Then
                Return ""
            End If
            Return " manifest='offline.appcache' type='text/cache-manifest'"
            If Request.IsLocal Or Request.Url.Host.StartsWith("192.168") Then
                Return ""
            Else
                Return " manifest='offline.appcache'"
            End If
        End Get
    End Property

    Public ReadOnly Property EnableNewPriorities As Boolean
        Get
            Dim EnableNewPriority = False
            Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM ClientSettings WHERE Name = 'EnableNewPriorities' AND Value = 1")
            If dr IsNot Nothing Then
                EnableNewPriority = True
            End If
            Return EnableNewPriority
        End Get
    End Property

    Public ReadOnly Property DoNotChangeTrueFalseInPCI As Boolean
        Get
            Dim ChangeTrueFalse = False
            Dim dr As String = Database.Tenant.GetStringValue("SELECT Value FROM ClientSettings WHERE Name = 'DoNotChangeTrueFalseInPCI'")
            If dr = "1" Then
                ChangeTrueFalse = True
            Else
                ChangeTrueFalse = False
            End If
            Return ChangeTrueFalse
        End Get
    End Property

    Public ReadOnly Property ApplicationTitle
        Get
            Return Coalesce(ClientSettings.MobileTitle, "CAMA Cloud&#174;")
        End Get
    End Property

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        If Not HttpContext.Current.Request.Cookies("MACID") Is Nothing Then
            macID = HttpContext.Current.GetKeyValue("MACID")
        ElseIf Request.QueryString("macid") <> "" Then
            macID = Request.QueryString("macid")
        End If
        If macID <> "" Then
            licenseId = BaseCharMap(Database.System.GetIntegerValue("Select id FROM DeviceLicense WHERE MachineKey = '" + macID + "'"))
        Else
            If Session("debuggerkey") Is Nothing Then
                licenseId = BaseCharMap(DateTime.Now.Ticks - DateTime.Today.Ticks)
                Session("debuggerkey") = licenseId
            Else
                licenseId = Session("debuggerkey")
            End If

        End If
        General.DebuggerKey = licenseId

        Dim ua As String = Request.UserAgent
        'Sendmail(ua)
        If Request.Headers("X-Forwarded-Proto") IsNot Nothing AndAlso Request.Headers("X-Forwarded-Proto").ToLower = "http" Then
            Response.Redirect("https://" + Request.ServerVariables("HTTP_HOST") + HttpContext.Current.Request.RawUrl)
            Return
        End If

        If Not HttpContext.Current.Request.Cookies("MACID") Is Nothing And ua.Contains("iPad") AndAlso Regex.IsMatch(ua, "Safari") And HttpContext.Current.GetCAMASession.OrganizationId <> -1 And Request.QueryString("type") = "" And Request.QueryString("macid") = "" And (ua.Contains("OS 11") Or ua.Contains("OS 12")) Then
            Response.Redirect("default.aspx?macid=" + HttpContext.Current.GetKeyValue("MACID").ToString())
            Return
        End If
        '   CreateCookieForMacId(Request.QueryString("macid"), ua)
        If ua.Contains("iPad") AndAlso HttpContext.Current.GetCAMASession.OrganizationId <> -1 And Regex.IsMatch(ua, "Safari") And Not Request.QueryString("type") = "webapp" Then
            Server.Transfer("~/install.aspx?t=" & Now.Ticks.ToString + "&macid=" + macID.ToString())
            Exit Sub
        End If
        If HttpContext.Current.Request.Cookies("MACID") Is Nothing And (ua.Contains("OS 11") Or ua.Contains("OS 12")) Then
            If macID <> "" Then
                Dim drLi As DataRow = Database.System.GetTopRow("Select * FROM DeviceLicense WHERE MachineKey = '" + macID + "'")
                If drLi Is Nothing Then
                Else
                    Dim macid_cookie As New HttpCookie("MACID")
                    macid_cookie.Domain = ApplicationSettings.AuthenticationDomain
                    macid_cookie.Expires = Now.AddYears(1)
                    'auth.Expires = Now.AddDays(1)
                    macid_cookie.Value = macID
                    Context.Response.Cookies.Add(macid_cookie)
                End If
            End If

        End If

        Response.AddHeader("Access-Control-Allow-Origin", "*")

        If Not IsPostBack Then
            If HttpContext.Current.GetCAMASession.OrganizationId = -1 Then
                Response.Redirect("http://auth.camacloud.com/info/noclient.aspx?wrongmaurl=true")
                Return
            End If

            ''---------------- cache list ----------------------------------------------------------------
            Dim maxDate As Date = #1/1/2010#
            If ClientSettings.CustomizationDate > maxDate Then
                maxDate = ClientSettings.CustomizationDate
            End If

            If ClientSettings.LastClientTemplateUpdateDate > maxDate Then
                maxDate = ClientSettings.LastClientTemplateUpdateDate
            End If

            If Data.Database.Tenant.Application.SchemaVersionDate > maxDate Then
                maxDate = Data.Database.Tenant.Application.SchemaVersionDate
            End If

            Dim buildDate As Date = Date.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings("AppBuildTime"))
            If buildDate > maxDate Then
                maxDate = buildDate
            End If
            Dim s = HttpContext.Current.Server
            exludePaths.AddRange({s.MapPath("~/logs").TrimEnd("/"), s.MapPath("~/static/js/unused").TrimEnd("/"), s.MapPath("~/static/js/worker").TrimEnd("/")})
            GetLastModifiedDate(s.MapPath("~/static"))
            Dim modDate As Date = Date.Parse(IO.File.GetLastWriteTime(s.MapPath("~/rwi-worker.js")))
            If modDate > maxModDate Then
                maxModDate = modDate
            End If
            modDate = Date.Parse(IO.File.GetLastWriteTime(s.MapPath("~/Default.aspx")))
            If modDate > maxModDate Then
                maxModDate = modDate
            End If

            If maxModDate > maxDate Then
                maxDate = maxModDate
            End If

            Dim version As Long = (maxDate.Ticks - #3/3/2013#.Ticks) / (10000 * 1000)
            cacheVersion = version.ToString()
            ''------------------------------------------------------------------------------------------

            APIKey = ClientSettings.PropertyValue("EagleViewInMA.APIKey")  ' dt.GetString("ApiKey")
            SecretKey = ClientSettings.PropertyValue("EagleViewInMA.SecretKey") ' dt.GetString("SecretKey")
            IPALoadUrl = ClientSettings.PropertyValue("EagleViewInMA.IPALoadURL") ' dt.GetString("IPALoadUrl")
            Dim ccHost As CAMACloudHost = CAMACloudHost.GetWorkingHost()

            Select Case ccHost.Application
                Case HostApplication.MobileAssessor
                    Return
                Case HostApplication.Console
                    Response.Redirect("https://console.camacloud.com")
                Case HostApplication.Unknown
                    Response.Redirect("http://www.camacloud.com", True)
            End Select
        End If
    End Sub

    Public Sub GetLastModifiedDate(path As String)
        For Each fn In Directory.GetFiles(path)
            Dim ext = IO.Path.GetExtension(fn).TrimStart(".").ToLower
            If ext = "css" Or ext = "js" Then
                Dim modDate As Date = Date.Parse(IO.File.GetLastWriteTime(fn))
                If modDate > maxModDate Then
                    maxModDate = modDate
                End If
            End If
        Next

        For Each dn In Directory.GetDirectories(path)
            If Not exludePaths.Contains(dn.TrimEnd("/")) Then
                GetLastModifiedDate(dn)
            End If
        Next
    End Sub

    '    Public Sub Sendmail(info As String)
    '    	Dim msg As New MailMessage
    '        msg.From = New MailAddress("camacloud@datacloudsolutions.net", "Info@CAMACloud")
    '        msg.To.Add("georgekuttymathai.r@techneurons.com")
    '        msg.To.Add("jubertj.george@techneurons.com")
    '        msg.Subject = "Client Info - User Agent"
    '        msg.Body = "<table><tr><td>User Agent</td><td>" + info + "</td></tr><table>"
    '        msg.IsBodyHtml = True
    '        msg.Priority = MailPriority.High
    '        Try
    '            AWSMailer.SendMail(msg)
    '        Catch ex As Exception
    '            Alert("Your message cannot be sent now.")
    '        End Try
    '    End Sub
End Class
