﻿Imports System.Net.Mail
Imports CAMACloud

Public Class awstest
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim subject = Now.ToString("yyyy-MM-dd-HH-mm-ss") + Now.ToLongDateString + Now.ToLongTimeString

        Dim s3 As New S3FileManager()
        Dim downloadUrl = s3.GetDownloadURL("org1014/pi/73/801/000/30-Sample.jpg")

        Dim msg As New MailMessage
        msg.To.Add("sarath@techneurons.com")
        msg.From = New MailAddress(ClientSettings.DefaultSender)
        msg.Subject = subject
        msg.Body = "Your download URL is: " + downloadUrl
        msg.IsBodyHtml = False
        CAMACloud.AWSMailer.SendMail(msg)

        lblResult.Text = subject

    End Sub

End Class