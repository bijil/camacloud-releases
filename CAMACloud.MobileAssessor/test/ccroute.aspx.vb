﻿Imports CAMACloud.Routing
Public Class ccroute
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Log("Welcome")

        Dim lo As New Location With {.Latitude = 42.317861, .Longitude = -86.182123}
        Log(lo.GetBlock(1).Code)
    End Sub

    Sub Log(message As String)
        lbl.Text += "&gt;" + message + "</br>"
    End Sub
End Class