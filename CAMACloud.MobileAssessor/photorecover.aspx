﻿<%@ Page Language="vb" AutoEventWireup="false" %>
<!DOCTYPE html>
<html>
<head>
    <title>CAMA Cloud&#174; </title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="static/app/apple-touch-icon-precomposed-72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="static/app/apple-touch-icon-precomposed-144.png" />
    <meta name="apple-mobile-web-app-title" content="CAMA Cloud&#174;">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="user-scalable=no,initial-scale=1.0,maximum-scale=1.0,width=device-width,height=device-height" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <script src="/static/js/lib/zepto.min.js"></script>
    <script>
        var db;
        var localDBName = 'MA<%=Right(Database.Tenant.Application.SchemaVersion.ToString.PadLeft(6, "0"), 6) %>';
        var totalPages = 0;
        var currentPage = -1;

        function initLocalDatabase(callback) {
            var dbSize = 500;
            if (navigator.platform == 'iPad')
                if (!navigator.standalone)
                    dbSize = 50;
            var uname = localStorage.getItem('username');
            var ldbname = uname ? (localDBName + '_' + uname.replace(/\s/g, '')) : localDBName;
            console.log(uname, ldbname);
            db = openDatabase(ldbname, '1.0', 'CAMA - MobileAssessor', dbSize * 1024 * 1024);

            if (callback) callback();
        }

        var lastQueryOutput;
        function getData(query, params, callback, otherData, ignoreError, errorCallback) {
            if (params == null)
                params = [];
            var data = [];
            db.transaction(function (x) {
                x.executeSql(query, params, function (x, results) {
                    var len = results.rows.length, i;
                    for (i = 0; i < len; i++) {
                        data.push(results.rows.item(i));
                    }
                    if (callback) callback(data, otherData, results); else { lastQueryOutput = data; if (data.length > 0) console.log(data[0]); };
                }, function (x, e) {
                    if (errorCallback) errorCallback();
                    if (!ignoreError) {
                        console.error(e.message);
                        console.error(query);
                    }
                });
            });
        }


        function executeSql(tx, query, params, success, failure) {
            tx.executeSql(query, params, function (x, results) { if (success) success(x, results) }, function (x, e) { if (failure) failure(x, e); else console.error(query, e.message) });
        }

        function getValue(query, params, callback) {
            if (!params) params = [];
            getData(query, params, function (res) {
                var value = null;
                if (res.length > 0)
                    for (var x in res[0]) {
                        value = res[0][x];
                        break;
                    }
                if (callback) callback(value);
            });
        }

        $(function () {
            initLocalDatabase(function () {
                getValue('SELECT COUNT(*) FROM Images', [], function (ic) {
                    totalPages = Math.ceil(ic / 40);
                    currentPage = Math.min(totalPages, 1);
                    console.log(ic, totalPages, currentPage);
                    getData("UPDATE Images SET Synced = '0' WHERE LocalId LIKE 'pu-%' AND Id IS NULL OR (Synced = 0)", [], function () {
                        window.location.href = '/';
                    }, null, true, function () {
                        window.location.href = '/';
                    });
                });
            });
        });

        function showPage(pageNo) { 
            
        }
    </script>
</head>
<body>
    <h3>Recovering photos, please wait ...</h3>
</body>
</html>
