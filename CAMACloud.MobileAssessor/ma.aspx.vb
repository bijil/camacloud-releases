﻿Imports System.Net
Partial Class _ma
    Inherits System.Web.UI.Page
    Public APIKey As String
    Public SecretKey As String
    Public IPALoadUrl As String
    Public ReadOnly Property CacheManifest As String
        Get
            'If Request("resetcache") IsNot Nothing Then
            '    Dim resetId As Long = Date.UtcNow.Ticks
            '    Return " manifest='offline.appcache?reset=" & resetId & "'"
            'End If
            If Request("nocache") IsNot Nothing Then
                Return ""
            End If
            Return " manifest='offline.appcache' type='text/cache-manifest'"
            If Request.IsLocal Or Request.Url.Host.StartsWith("192.168") Then
                Return ""
            Else
                Return " manifest='offline.appcache'"
            End If
        End Get
    End Property

    Public ReadOnly Property ApplicationTitle
        Get
            Return Coalesce(ClientSettings.MobileTitle, "CAMA Cloud&#174;")
        End Get
    End Property

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        'If Not Request.IsLocal And Not Request.IsSecureConnection Then
        '    Response.Redirect("https://" + Request.ServerVariables("HTTP_HOST") + HttpContext.Current.Request.RawUrl)
        '    Return
        'End If
        If Request.Headers("X-Forwarded-Proto") IsNot Nothing AndAlso Request.Headers("X-Forwarded-Proto").ToLower = "http" Then
            'Database.Tenant.Execute("insert into test_abc values(" + Request.IsSecureConnection.ToString.ToSqlValue + "," + Request.Headers("X-Forwarded-Proto").ToString.ToSqlValue + ")")
            Response.Redirect("https://" + Request.ServerVariables("HTTP_HOST") + HttpContext.Current.Request.RawUrl)
            Return
        End If


        'If qs = "safari" Then
        '    Server.Transfer("~/install.aspx?t=" & Now.Ticks)
        '    Exit Sub
        'End If
        Response.AddHeader("Access-Control-Allow-Origin", "*")

        If Not IsPostBack Then
            If HttpContext.Current.GetCAMASession.OrganizationId = -1 Then
                Response.Redirect("http://auth.camacloud.com/info/noclient.aspx?wrongmaurl=true")
                Return
            End If
            APIKey = ClientSettings.PropertyValue("PictometryInMA.APIKey")  ' dt.GetString("ApiKey")
            SecretKey = ClientSettings.PropertyValue("PictometryInMA.SecretKey") ' dt.GetString("SecretKey")
            IPALoadUrl = ClientSettings.PropertyValue("PictometryInMA.IPALoadURL") ' dt.GetString("IPALoadUrl")
            Dim ccHost As CAMACloudHost = CAMACloudHost.GetWorkingHost()

            Select Case ccHost.Application
                Case HostApplication.MobileAssessor
                    Return
                Case HostApplication.Console
                    Response.Redirect("https://console.camacloud.com")
                Case HostApplication.Unknown
                    Response.Redirect("http://www.camacloud.com", True)
            End Select
        End If
    End Sub

End Class
