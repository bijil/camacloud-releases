# Mobile Assessor Client Unit Tests

[Jasmine](https://jasmine.github.io/) unit tests for the Mobile Assessor client-side web application.

## Recommended tools

- [nvm](https://github.com/nvm-sh/nvm) (on [Windows](https://github.com/coreybutler/nvm-windows)) for Node.js version management
- [Visual Studio Code](https://code.visualstudio.com/) for development and debug

## Dependencies

- [Node.js](https://nodejs.org/) v16+

## Setup

Install dependencies:

```sh
npm install
```

## Run

```sh
npm test
```
