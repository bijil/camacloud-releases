# Sync

The application has two procedures that drive parcel change uploads:

- `RunSync` for manual processes i.e. user invoked
- `UploadData` for scheduled processes i.e. event/timer invoked

These processes should not be allowed to overlap execution.

## RunSync

User actions invoke varying degrees of synchronization.

Reentrancy should be prevented (barrier below).

```mermaid
flowchart TD
  subgraph User
    B[RunSync] -- "Yes" --> C[UploadParcelChanges]
    B -- No --> D[Done]
    C --> E[Download]
    subgraph Sync
      E --> F[Initialize]
    end
    F --> D
  end
```

Sync downloads data appropriate for the type of sync and then re-initializes, regardless of whether upload parcel changes failed.

## UploadData

Initialization, timer, and online (event) invoke uploads (location and parcel changes).

Reentrancy should be prevented.

```mermaid
flowchart TD
  subgraph Init/Timer/Online
    B[UploadData] -- Yes --> C[UploadParcelChanges]
    B -- No --> D[RestartTimer]
    C --> D
    D --> E[Done]
  end
```

## UploadParcelChanges / _uploadParcelChanges

Shared process between RunSync and UploadData that may upload photos and drain the parcel changes queue [^1] (i.e. upload).

Reentrancy should be prevented.

[^1]: The ParcelChanges table is used as a queue for unsynced changes ordered by change time

## checkNetwork

Makes an XMLHttpRequest to determine network status.

Clears barriers upon failure, thus should only be used within a context where the appropriate barriers are raised; this *should not* be a responsability of this function.
