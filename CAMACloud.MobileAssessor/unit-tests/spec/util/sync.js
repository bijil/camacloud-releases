import { LocalStorage } from 'node-localstorage';
import jsdom from 'jsdom';
import $ from 'jquery';

export function createGlobalFixture() {
  const dom = new jsdom.JSDOM('');
  const window = dom.window;
  // JSDOM seems to ignore string handlers by default, and even with `window._runScripts === "dangerously"`
  // it doesn't seem to run with adequate context (global).  We'll use Node's setTimeout directly.  Another
  // alternative would be to wrap JSDOM's implementation to wrap string handlers in a deferred eval.
  window.setTimeout = setTimeout;
  // JSDOM doesn't seem to clearTimeout well, so use Node's clearTimeout directly.
  window.clearTimeout = clearTimeout;
  let lastTick = 0;
  global.baseUrl = '';
  global.window = window;
  global.$ = $(window);
  global.$$$ = () => { };
  global.ajaxRequest = (params) => {
    setTimeout(() => params.success());
  };
  global.ticks = () => ++lastTick;
  global.error = () => { };
  global.log = () => { };
  global.debug_mode_log = () => { };
  global.xlog = () => { };
  global.messageBox = () => { };
  global.startApplication = (callback) => {
    callback(() => {
      syncInProgress = false;
    });
  };
  global.getData = (_sql, _params, success, _failure) => {
    setTimeout(() => success([]));
  };
  global.ccma = {
    Sync: {},
    Session: {
      IsLoggedIn: true,
    },
  };
  global.executeSqlSpy = jasmine.createSpy('executeSql').and.callFake((_sql, _params, success, _failure) => {
    setTimeout(() => {
      success({}, { rows: [] });
    });
  });
  global.db = {
    transaction: (callback) => {
      setTimeout(() => {
        callback({
          executeSql: global.executeSqlSpy,
        });
      });
    },
  };
  global.localStorage = new LocalStorage('./scratch');
  global.quickDebug = false;
  global.activeParcel = null;
  global.isCopying = false;
}
